.class public final enum LX/DHr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DHr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DHr;

.field public static final enum BOOKMARK:LX/DHr;

.field public static final enum FEED_PYMK:LX/DHr;

.field public static final enum FRIENDS_TAB_FRIEND_OVERFLOW_MENU:LX/DHr;

.field public static final enum FRIENDS_TAB_SEE_ALL_FRIENDS:LX/DHr;

.field public static final enum FRIENDS_TAB_SPROUT_LAUNCHER:LX/DHr;

.field public static final enum FRIEND_LIST_ALL_TAB:LX/DHr;

.field public static final enum FRIEND_LIST_MUTUAL_TAB:LX/DHr;

.field public static final enum FRIEND_LIST_PYMK_TAB:LX/DHr;

.field public static final enum FRIEND_LIST_RECENT_TAB:LX/DHr;

.field public static final enum FRIEND_LIST_SUGGESTIONS_TAB:LX/DHr;

.field public static final enum FRIEND_LIST_WITH_NEW_POSTS_TAB:LX/DHr;

.field public static final enum SEARCH:LX/DHr;

.field public static final enum TIMELINE_ABOUT_FRIENDS_APP:LX/DHr;

.field public static final enum TIMELINE_ABOUT_FRIENDS_APP_MUTUAL_FRIENDS_LINK:LX/DHr;

.field public static final enum TIMELINE_CONTEXT_ITEM:LX/DHr;

.field public static final enum TIMELINE_FRIENDS_NAVTILE:LX/DHr;

.field public static final enum TIMELINE_FRIENDS_PROTILE:LX/DHr;

.field public static final enum TIMELINE_SURFING_RECOMMENDATIONS:LX/DHr;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1982565
    new-instance v0, LX/DHr;

    const-string v1, "FRIEND_LIST_ALL_TAB"

    invoke-direct {v0, v1, v3}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FRIEND_LIST_ALL_TAB:LX/DHr;

    .line 1982566
    new-instance v0, LX/DHr;

    const-string v1, "FRIEND_LIST_MUTUAL_TAB"

    invoke-direct {v0, v1, v4}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FRIEND_LIST_MUTUAL_TAB:LX/DHr;

    .line 1982567
    new-instance v0, LX/DHr;

    const-string v1, "FRIEND_LIST_PYMK_TAB"

    invoke-direct {v0, v1, v5}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FRIEND_LIST_PYMK_TAB:LX/DHr;

    .line 1982568
    new-instance v0, LX/DHr;

    const-string v1, "FRIEND_LIST_RECENT_TAB"

    invoke-direct {v0, v1, v6}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FRIEND_LIST_RECENT_TAB:LX/DHr;

    .line 1982569
    new-instance v0, LX/DHr;

    const-string v1, "FRIEND_LIST_SUGGESTIONS_TAB"

    invoke-direct {v0, v1, v7}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FRIEND_LIST_SUGGESTIONS_TAB:LX/DHr;

    .line 1982570
    new-instance v0, LX/DHr;

    const-string v1, "FRIEND_LIST_WITH_NEW_POSTS_TAB"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FRIEND_LIST_WITH_NEW_POSTS_TAB:LX/DHr;

    .line 1982571
    new-instance v0, LX/DHr;

    const-string v1, "FRIENDS_TAB_SEE_ALL_FRIENDS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FRIENDS_TAB_SEE_ALL_FRIENDS:LX/DHr;

    .line 1982572
    new-instance v0, LX/DHr;

    const-string v1, "FRIENDS_TAB_FRIEND_OVERFLOW_MENU"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FRIENDS_TAB_FRIEND_OVERFLOW_MENU:LX/DHr;

    .line 1982573
    new-instance v0, LX/DHr;

    const-string v1, "FRIENDS_TAB_SPROUT_LAUNCHER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FRIENDS_TAB_SPROUT_LAUNCHER:LX/DHr;

    .line 1982574
    new-instance v0, LX/DHr;

    const-string v1, "TIMELINE_ABOUT_FRIENDS_APP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->TIMELINE_ABOUT_FRIENDS_APP:LX/DHr;

    .line 1982575
    new-instance v0, LX/DHr;

    const-string v1, "TIMELINE_ABOUT_FRIENDS_APP_MUTUAL_FRIENDS_LINK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->TIMELINE_ABOUT_FRIENDS_APP_MUTUAL_FRIENDS_LINK:LX/DHr;

    .line 1982576
    new-instance v0, LX/DHr;

    const-string v1, "TIMELINE_CONTEXT_ITEM"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->TIMELINE_CONTEXT_ITEM:LX/DHr;

    .line 1982577
    new-instance v0, LX/DHr;

    const-string v1, "TIMELINE_FRIENDS_NAVTILE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->TIMELINE_FRIENDS_NAVTILE:LX/DHr;

    .line 1982578
    new-instance v0, LX/DHr;

    const-string v1, "TIMELINE_FRIENDS_PROTILE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->TIMELINE_FRIENDS_PROTILE:LX/DHr;

    .line 1982579
    new-instance v0, LX/DHr;

    const-string v1, "TIMELINE_SURFING_RECOMMENDATIONS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->TIMELINE_SURFING_RECOMMENDATIONS:LX/DHr;

    .line 1982580
    new-instance v0, LX/DHr;

    const-string v1, "BOOKMARK"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->BOOKMARK:LX/DHr;

    .line 1982581
    new-instance v0, LX/DHr;

    const-string v1, "SEARCH"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->SEARCH:LX/DHr;

    .line 1982582
    new-instance v0, LX/DHr;

    const-string v1, "FEED_PYMK"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/DHr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DHr;->FEED_PYMK:LX/DHr;

    .line 1982583
    const/16 v0, 0x12

    new-array v0, v0, [LX/DHr;

    sget-object v1, LX/DHr;->FRIEND_LIST_ALL_TAB:LX/DHr;

    aput-object v1, v0, v3

    sget-object v1, LX/DHr;->FRIEND_LIST_MUTUAL_TAB:LX/DHr;

    aput-object v1, v0, v4

    sget-object v1, LX/DHr;->FRIEND_LIST_PYMK_TAB:LX/DHr;

    aput-object v1, v0, v5

    sget-object v1, LX/DHr;->FRIEND_LIST_RECENT_TAB:LX/DHr;

    aput-object v1, v0, v6

    sget-object v1, LX/DHr;->FRIEND_LIST_SUGGESTIONS_TAB:LX/DHr;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DHr;->FRIEND_LIST_WITH_NEW_POSTS_TAB:LX/DHr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DHr;->FRIENDS_TAB_SEE_ALL_FRIENDS:LX/DHr;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DHr;->FRIENDS_TAB_FRIEND_OVERFLOW_MENU:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DHr;->FRIENDS_TAB_SPROUT_LAUNCHER:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/DHr;->TIMELINE_ABOUT_FRIENDS_APP:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/DHr;->TIMELINE_ABOUT_FRIENDS_APP_MUTUAL_FRIENDS_LINK:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/DHr;->TIMELINE_CONTEXT_ITEM:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/DHr;->TIMELINE_FRIENDS_NAVTILE:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/DHr;->TIMELINE_FRIENDS_PROTILE:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/DHr;->TIMELINE_SURFING_RECOMMENDATIONS:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/DHr;->BOOKMARK:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/DHr;->SEARCH:LX/DHr;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/DHr;->FEED_PYMK:LX/DHr;

    aput-object v2, v0, v1

    sput-object v0, LX/DHr;->$VALUES:[LX/DHr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1982564
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DHr;
    .locals 1

    .prologue
    .line 1982584
    const-class v0, LX/DHr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DHr;

    return-object v0
.end method

.method public static values()[LX/DHr;
    .locals 1

    .prologue
    .line 1982563
    sget-object v0, LX/DHr;->$VALUES:[LX/DHr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DHr;

    return-object v0
.end method
