.class public LX/Eiv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13F;
.implements LX/13G;


# instance fields
.field public a:J

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U8;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0Uh;

.field public e:Lcom/facebook/growth/model/Contactpoint;

.field private f:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;


# direct methods
.method public constructor <init>(LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2U8;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2160543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2160544
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Eiv;->a:J

    .line 2160545
    iput-object p1, p0, LX/Eiv;->b:LX/0Ot;

    .line 2160546
    iput-object p2, p0, LX/Eiv;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2160547
    iput-object p3, p0, LX/Eiv;->d:LX/0Uh;

    .line 2160548
    const/4 v0, 0x0

    iput-object v0, p0, LX/Eiv;->e:Lcom/facebook/growth/model/Contactpoint;

    .line 2160549
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 3

    .prologue
    .line 2160550
    iget-object v0, p0, LX/Eiv;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3df;->i:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2160551
    const-wide/16 v0, 0x0

    .line 2160552
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, LX/Eiv;->a:J

    goto :goto_0
.end method

.method public final a(ILandroid/content/Intent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2160556
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 2160553
    iget-object v0, p0, LX/Eiv;->e:Lcom/facebook/growth/model/Contactpoint;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Eiv;->e:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0}, Lcom/facebook/growth/model/Contactpoint;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2160554
    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 2160555
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2160534
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/confirmation/activity/SimpleConfirmAccountActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2160535
    const-string v1, "extra_contactpoint"

    iget-object v2, p0, LX/Eiv;->e:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2160536
    const-string v1, "extra_is_cliff_interstitial"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2160537
    iget-object v1, p0, LX/Eiv;->f:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    sget-object v2, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;->SOFT_CLIFF:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    if-ne v1, v2, :cond_0

    .line 2160538
    const-string v1, "extra_ref"

    const-string v2, "dismissible_cliff"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2160539
    const-string v1, "extra_cancel_allowed"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2160540
    :goto_0
    iget-object v1, p0, LX/Eiv;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3df;->i:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2160541
    return-object v0

    .line 2160542
    :cond_0
    const-string v1, "extra_ref"

    const-string v2, "cliff_seen"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 2160533
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 2160523
    check-cast p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;

    .line 2160524
    invoke-virtual {p1}, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->a()Lcom/facebook/growth/model/Contactpoint;

    move-result-object v0

    iput-object v0, p0, LX/Eiv;->e:Lcom/facebook/growth/model/Contactpoint;

    .line 2160525
    iget-object v0, p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->interstitialType:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    iput-object v0, p0, LX/Eiv;->f:Lcom/facebook/confirmation/model/AccountConfirmationInterstitialType;

    .line 2160526
    iget-wide v0, p1, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;->minImpressionDelayMs:J

    iput-wide v0, p0, LX/Eiv;->a:J

    .line 2160527
    iget-object v0, p0, LX/Eiv;->e:Lcom/facebook/growth/model/Contactpoint;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Eiv;->e:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0}, Lcom/facebook/growth/model/Contactpoint;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2160528
    :cond_0
    :goto_0
    return-void

    .line 2160529
    :cond_1
    iget-object v0, p0, LX/Eiv;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U8;

    iget-object v1, p0, LX/Eiv;->e:Lcom/facebook/growth/model/Contactpoint;

    invoke-virtual {v0, v1}, LX/2U8;->a(Lcom/facebook/growth/model/Contactpoint;)Z

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2160532
    const-string v0, "1907"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2160531
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_FOREGROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2160530
    const-class v0, Lcom/facebook/confirmation/model/AccountConfirmationInterstitialData;

    return-object v0
.end method
