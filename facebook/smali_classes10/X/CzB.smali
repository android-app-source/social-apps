.class public LX/CzB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;
.implements LX/Cz2;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;",
        ">;",
        "LX/Cz2;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1954236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CzB;->a:Ljava/util/List;

    .line 1954238
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;)I
    .locals 1

    .prologue
    .line 1954234
    iget-object v0, p0, LX/CzB;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1954235
    iget-object v0, p0, LX/CzB;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954230
    iget-object v0, p0, LX/CzB;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 1954231
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 1954232
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->m()Ljava/lang/String;

    move-result-object v0

    .line 1954233
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLNode;)I
    .locals 2

    .prologue
    .line 1954225
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/CzB;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1954226
    iget-object v0, p0, LX/CzB;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;->j()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1954227
    :goto_1
    return v1

    .line 1954228
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1954229
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1954224
    iget-object v0, p0, LX/CzB;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
