.class public final LX/CsT;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:LX/CsX;


# direct methods
.method public constructor <init>(LX/CsX;)V
    .locals 0

    .prologue
    .line 1942827
    iput-object p1, p0, LX/CsT;->a:LX/CsX;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 1942828
    iget-object v0, p0, LX/CsT;->a:LX/CsX;

    iget-object v0, v0, LX/CsX;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-object v2, p0, LX/CsT;->a:LX/CsX;

    iget-wide v2, v2, LX/CsX;->i:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1942829
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1942830
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1942831
    const-string v1, "com.android.browser.headers"

    invoke-static {}, LX/Cs3;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1942832
    iget-object v1, p0, LX/CsT;->a:LX/CsX;

    iget-object v1, v1, LX/CsX;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/CsT;->a:LX/CsX;

    invoke-virtual {v2}, LX/CsX;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1942833
    iget-object v0, p0, LX/CsT;->a:LX/CsX;

    iget-object v0, v0, LX/CsX;->c:LX/Ckw;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, LX/Ckw;->b(Ljava/lang/String;Ljava/util/Map;)V

    .line 1942834
    iget-object v0, p0, LX/CsT;->a:LX/CsX;

    iget-object v0, v0, LX/CsX;->c:LX/Ckw;

    const-string v1, "WEBVIEW"

    invoke-virtual {v0, p2, v1}, LX/Ckw;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1942835
    const/4 v0, 0x1

    .line 1942836
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
