.class public final LX/EOl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2115193
    iput-object p1, p0, LX/EOl;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;

    iput-object p2, p0, LX/EOl;->a:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    iput-object p3, p0, LX/EOl;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0x297703cc

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2115194
    iget-object v0, p0, LX/EOl;->a:Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->l()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v3

    .line 2115195
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2115196
    iget-object v0, p0, LX/EOl;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->e:LX/1nD;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->o()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->m()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/EOl;->b:LX/1Pn;

    check-cast v4, LX/CxV;

    invoke-interface {v4}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v4

    .line 2115197
    iget-object v5, v4, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v4, v5

    .line 2115198
    sget-object v5, LX/8ci;->s:LX/8ci;

    iget-object v6, p0, LX/EOl;->b:LX/1Pn;

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    .line 2115199
    iget-object p1, v6, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v6, p1

    .line 2115200
    invoke-virtual/range {v0 .. v6}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v0

    .line 2115201
    iget-object v1, p0, LX/EOl;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/EOl;->b:LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2115202
    :cond_0
    const v0, 0x3b5edf3a

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
