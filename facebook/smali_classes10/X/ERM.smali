.class public LX/ERM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/ERM;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2120791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120792
    iput-object p1, p0, LX/ERM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2120793
    return-void
.end method

.method public static a(LX/0QB;)LX/ERM;
    .locals 4

    .prologue
    .line 2120794
    sget-object v0, LX/ERM;->b:LX/ERM;

    if-nez v0, :cond_1

    .line 2120795
    const-class v1, LX/ERM;

    monitor-enter v1

    .line 2120796
    :try_start_0
    sget-object v0, LX/ERM;->b:LX/ERM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2120797
    if-eqz v2, :cond_0

    .line 2120798
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2120799
    new-instance p0, LX/ERM;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/ERM;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2120800
    move-object v0, p0

    .line 2120801
    sput-object v0, LX/ERM;->b:LX/ERM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2120802
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2120803
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2120804
    :cond_1
    sget-object v0, LX/ERM;->b:LX/ERM;

    return-object v0

    .line 2120805
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2120806
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
