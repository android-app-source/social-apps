.class public LX/CpU;
.super LX/CpT;
.source ""


# instance fields
.field public A:Z

.field public B:Landroid/os/Bundle;

.field private C:LX/CpO;

.field public final D:LX/Ctg;

.field public a:Z

.field private z:LX/Cso;


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;LX/CpO;)V
    .locals 1

    .prologue
    .line 1937895
    invoke-direct {p0, p1, p2}, LX/CpT;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 1937896
    iput-object p3, p0, LX/CpU;->C:LX/CpO;

    .line 1937897
    const v0, 0x7f0d16c7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/Ctg;

    iput-object v0, p0, LX/CpU;->D:LX/Ctg;

    .line 1937898
    return-void
.end method


# virtual methods
.method public final a(LX/0Uh;)LX/Cso;
    .locals 2

    .prologue
    .line 1937949
    iget-object v0, p0, LX/CpU;->z:LX/Cso;

    if-nez v0, :cond_0

    .line 1937950
    new-instance v0, LX/Csp;

    iget-object v1, p0, LX/CpT;->p:LX/0Uh;

    invoke-direct {v0, v1}, LX/Csp;-><init>(LX/0Uh;)V

    iput-object v0, p0, LX/CpU;->z:LX/Cso;

    .line 1937951
    :cond_0
    iget-object v0, p0, LX/CpU;->z:LX/Cso;

    return-object v0
.end method

.method public final a(LX/Cli;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1937909
    if-nez p1, :cond_0

    .line 1937910
    :goto_0
    return-void

    .line 1937911
    :cond_0
    iget-object v2, p0, LX/CpU;->D:LX/Ctg;

    invoke-interface {v2}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1937912
    iget-object v2, p0, LX/CpU;->D:LX/Ctg;

    invoke-interface {v2}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1937913
    iget-object v2, p0, LX/CpU;->D:LX/Ctg;

    invoke-interface {v2}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v2

    invoke-interface {v2}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1937914
    :cond_1
    iget-object v2, p1, LX/Cli;->j:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-object v2, v2

    .line 1937915
    invoke-static {v2}, LX/CoV;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Cqw;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/Cos;->a(LX/Cqw;)V

    .line 1937916
    iget-object v2, p1, LX/Cli;->k:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-object v2, v2

    .line 1937917
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, LX/CpT;->b:LX/1Bv;

    invoke-virtual {v2}, LX/1Bv;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    :cond_2
    iput-boolean v0, p0, LX/CpU;->x:Z

    .line 1937918
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1937919
    invoke-super {p0, v0}, LX/CpT;->a(LX/Ctg;)V

    .line 1937920
    new-instance v2, LX/Ctz;

    invoke-direct {v2, v0}, LX/Ctz;-><init>(LX/Ctg;)V

    invoke-virtual {p0, v2}, LX/Cos;->a(LX/Ctr;)V

    .line 1937921
    invoke-virtual {p0, p1}, LX/CpT;->c(LX/Cli;)V

    .line 1937922
    invoke-virtual {p0, p1}, LX/CpT;->b(LX/Cli;)V

    .line 1937923
    invoke-virtual {p0}, LX/CpT;->t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    move-result-object v0

    .line 1937924
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1937925
    iget v5, p1, LX/Cli;->c:I

    move v5, v5

    .line 1937926
    int-to-float v5, v5

    mul-float/2addr v4, v5

    .line 1937927
    iget v5, p1, LX/Cli;->d:I

    move v5, v5

    .line 1937928
    int-to-float v5, v5

    div-float/2addr v4, v5

    float-to-double v6, v4

    .line 1937929
    iget v4, p1, LX/Cli;->c:I

    move v5, v4

    .line 1937930
    iget v4, p1, LX/Cli;->d:I

    move v4, v4

    .line 1937931
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpg-double v8, v6, v8

    if-gez v8, :cond_3

    move v4, v5

    .line 1937932
    :cond_3
    new-instance v8, LX/0P2;

    invoke-direct {v8}, LX/0P2;-><init>()V

    .line 1937933
    const-string v9, "CoverImageParamsKey"

    .line 1937934
    iget-object v10, p1, LX/Cli;->e:Ljava/lang/String;

    move-object v10, v10

    .line 1937935
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {v10}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1937936
    iget-object v9, p1, LX/Cli;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    move-object v9, v9

    .line 1937937
    invoke-virtual {p0, v9, v6, v7, v8}, LX/CpT;->a(Lcom/facebook/video/engine/VideoPlayerParams;DLX/0P2;)LX/2pa;

    move-result-object v6

    .line 1937938
    invoke-virtual {v0, v6}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1937939
    invoke-virtual {v0, v5, v4}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->a(II)V

    .line 1937940
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1937941
    new-instance v2, LX/CuN;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/CuN;-><init>(Landroid/content/Context;)V

    .line 1937942
    invoke-static {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1937943
    iget-object v2, p1, LX/Cli;->k:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-object v2, v2

    .line 1937944
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    if-ne v2, v3, :cond_4

    .line 1937945
    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 1937946
    :goto_1
    iput-boolean v1, p0, LX/CpU;->a:Z

    .line 1937947
    iget-object v0, p0, LX/CpU;->C:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->n()V

    goto/16 :goto_0

    .line 1937948
    :cond_4
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    goto :goto_1
.end method

.method public final a(LX/Ctg;)V
    .locals 0

    .prologue
    .line 1937908
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1937905
    invoke-super {p0, p1}, LX/CpT;->b(Landroid/os/Bundle;)V

    .line 1937906
    iput-object p1, p0, LX/CpU;->B:Landroid/os/Bundle;

    .line 1937907
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1937902
    iget-boolean v0, p0, LX/CpU;->a:Z

    if-eqz v0, :cond_0

    .line 1937903
    invoke-super {p0}, LX/CpT;->f()V

    .line 1937904
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1937899
    iget-boolean v0, p0, LX/CpU;->a:Z

    if-eqz v0, :cond_0

    .line 1937900
    invoke-super {p0}, LX/CpT;->g()V

    .line 1937901
    :cond_0
    return-void
.end method
