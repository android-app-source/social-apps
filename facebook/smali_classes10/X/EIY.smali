.class public LX/EIY;
.super LX/EH2;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0yH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2S7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/EH3;

.field public e:LX/EH3;

.field public f:Landroid/widget/FrameLayout;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2102128
    invoke-direct {p0, p1}, LX/EH2;-><init>(Landroid/content/Context;)V

    .line 2102129
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2102130
    iput-object v0, p0, LX/EIY;->h:LX/0Ot;

    .line 2102131
    const-class v0, LX/EIY;

    invoke-static {v0, p0}, LX/EIY;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2102132
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2102133
    const v1, 0x7f03161a

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2102134
    const v0, 0x7f0d31b0

    invoke-virtual {p0, v0}, LX/EH2;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EIY;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2102135
    const v0, 0x7f0d31b1

    invoke-virtual {p0, v0}, LX/EH2;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/EIY;->f:Landroid/widget/FrameLayout;

    .line 2102136
    new-instance v1, LX/EH3;

    iget-object v0, p0, LX/EIY;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/EH1;->VOICE_WITH_ADD_CALLEE:LX/EH1;

    :goto_0
    invoke-direct {v1, p1, v0}, LX/EH3;-><init>(Landroid/content/Context;LX/EH1;)V

    iput-object v1, p0, LX/EIY;->d:LX/EH3;

    .line 2102137
    iget-object v0, p0, LX/EIY;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/EIY;->d:LX/EH3;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2102138
    new-instance v0, LX/EH3;

    invoke-virtual {p0}, LX/EIY;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/EH1;->VOICEMAIL:LX/EH1;

    invoke-direct {v0, v1, v2}, LX/EH3;-><init>(Landroid/content/Context;LX/EH1;)V

    iput-object v0, p0, LX/EIY;->e:LX/EH3;

    .line 2102139
    iget-object v0, p0, LX/EIY;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/EIY;->e:LX/EH3;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2102140
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/EIY;->setVoicemailButtonsVisible(Z)V

    .line 2102141
    iget-object v0, p0, LX/EIY;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102142
    invoke-virtual {p0}, LX/EIY;->b()V

    .line 2102143
    :cond_0
    return-void

    .line 2102144
    :cond_1
    sget-object v0, LX/EH1;->VOICE:LX/EH1;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/EIY;

    const/16 v1, 0x3257

    invoke-static {v3, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v3}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v3}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v2

    check-cast v2, LX/0yH;

    invoke-static {v3}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object v3

    check-cast v3, LX/2S7;

    iput-object p0, p1, LX/EIY;->h:LX/0Ot;

    iput-object v1, p1, LX/EIY;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v2, p1, LX/EIY;->b:LX/0yH;

    iput-object v3, p1, LX/EIY;->c:LX/2S7;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2102145
    iget-object v0, p0, LX/EIY;->d:LX/EH3;

    invoke-virtual {v0}, LX/EH3;->a()V

    .line 2102146
    iget-object v0, p0, LX/EIY;->e:LX/EH3;

    invoke-virtual {v0}, LX/EH3;->a()V

    .line 2102147
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2102148
    iget-object v0, p0, LX/EIY;->b:LX/0yH;

    sget-object v1, LX/0yY;->VOIP_CALL_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2102149
    iget-object v0, p0, LX/EIY;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2102150
    iget-object v0, p0, LX/EIY;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/EDJ;->c:LX/0Tn;

    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2102151
    :cond_0
    :goto_0
    return-void

    .line 2102152
    :cond_1
    iget-object v0, p0, LX/EIY;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/EDJ;->c:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2102153
    iget-object v0, p0, LX/EIY;->c:LX/2S7;

    const-string v1, "data_warning"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2102154
    iget-object v0, p0, LX/EIY;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2102155
    iget-object v0, p0, LX/EIY;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/EDJ;->c:LX/0Tn;

    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public setVoicemailButtonsVisible(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2102156
    iget-object v3, p0, LX/EIY;->e:LX/EH3;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, LX/EH3;->setVisibility(I)V

    .line 2102157
    iget-object v0, p0, LX/EIY;->d:LX/EH3;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, LX/EH3;->setVisibility(I)V

    .line 2102158
    return-void

    :cond_0
    move v0, v2

    .line 2102159
    goto :goto_0

    :cond_1
    move v2, v1

    .line 2102160
    goto :goto_1
.end method
