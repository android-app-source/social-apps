.class public LX/DGG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        "T::",
        "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/DGB;

.field public final b:LX/3mL;

.field public final c:LX/1LV;

.field public final d:LX/2xr;

.field public final e:LX/1DR;


# direct methods
.method public constructor <init>(LX/DGB;LX/3mL;LX/1LV;LX/1DR;LX/2xr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1979506
    iput-object p1, p0, LX/DGG;->a:LX/DGB;

    .line 1979507
    iput-object p2, p0, LX/DGG;->b:LX/3mL;

    .line 1979508
    iput-object p3, p0, LX/DGG;->c:LX/1LV;

    .line 1979509
    iput-object p4, p0, LX/DGG;->e:LX/1DR;

    .line 1979510
    iput-object p5, p0, LX/DGG;->d:LX/2xr;

    .line 1979511
    return-void
.end method

.method public static a(LX/0QB;)LX/DGG;
    .locals 9

    .prologue
    .line 1979512
    const-class v1, LX/DGG;

    monitor-enter v1

    .line 1979513
    :try_start_0
    sget-object v0, LX/DGG;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979514
    sput-object v2, LX/DGG;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979515
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979516
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979517
    new-instance v3, LX/DGG;

    const-class v4, LX/DGB;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DGB;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v5

    check-cast v5, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v6

    check-cast v6, LX/1LV;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v7

    check-cast v7, LX/1DR;

    invoke-static {v0}, LX/2xr;->b(LX/0QB;)LX/2xr;

    move-result-object v8

    check-cast v8, LX/2xr;

    invoke-direct/range {v3 .. v8}, LX/DGG;-><init>(LX/DGB;LX/3mL;LX/1LV;LX/1DR;LX/2xr;)V

    .line 1979518
    move-object v0, v3

    .line 1979519
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979520
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DGG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979521
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979522
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
