.class public LX/CpF;
.super LX/Coi;
.source ""

# interfaces
.implements LX/CoW;


# instance fields
.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1936823
    invoke-direct {p0, p1}, LX/Coi;-><init>(Landroid/view/View;)V

    .line 1936824
    const-class v0, LX/CpF;

    invoke-static {v0, p0}, LX/CpF;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936825
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/CpF;->b:I

    .line 1936826
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CpF;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object p0

    check-cast p0, LX/Ck0;

    iput-object p0, p1, LX/CpF;->a:LX/Ck0;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1936827
    iget v0, p0, LX/CpF;->b:I

    return v0
.end method
