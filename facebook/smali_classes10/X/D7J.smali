.class public LX/D7J;
.super Landroid/preference/PreferenceCategory;
.source ""


# static fields
.field private static final b:[Ljava/lang/CharSequence;

.field private static final c:[Ljava/lang/CharSequence;


# instance fields
.field public a:LX/3H6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1966891
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/CharSequence;

    const-string v1, "No Injection"

    aput-object v1, v0, v3

    const-string v1, "Short Video (7s)"

    aput-object v1, v0, v4

    const-string v1, "Normal Video (15s)"

    aput-object v1, v0, v5

    const-string v1, "Long Video (30s)"

    aput-object v1, v0, v6

    const-string v1, "Square Video"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Landscape Video"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "No Video"

    aput-object v2, v0, v1

    sput-object v0, LX/D7J;->b:[Ljava/lang/CharSequence;

    .line 1966892
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/CharSequence;

    const-string v1, ""

    aput-object v1, v0, v3

    const-string v1, "1009636599122184"

    aput-object v1, v0, v4

    const-string v1, "1036444139722974"

    aput-object v1, v0, v5

    const-string v1, "10154171859924826"

    aput-object v1, v0, v6

    const-string v1, "10154635707256729"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "10154117354498864"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "-1"

    aput-object v2, v0, v1

    sput-object v0, LX/D7J;->c:[Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1966893
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D7J;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1966894
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1966889
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D7J;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966890
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1966870
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966871
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/D7J;

    invoke-static {v0}, LX/3H6;->a(LX/0QB;)LX/3H6;

    move-result-object v0

    check-cast v0, LX/3H6;

    iput-object v0, p0, LX/D7J;->a:LX/3H6;

    .line 1966872
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 2

    .prologue
    .line 1966873
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 1966874
    const-string v0, "Commercial Break"

    invoke-virtual {p0, v0}, LX/D7J;->setTitle(Ljava/lang/CharSequence;)V

    .line 1966875
    new-instance v0, LX/4ok;

    invoke-virtual {p0}, LX/D7J;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1966876
    sget-object v1, LX/3HQ;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 1966877
    const-string v1, "Enable Debug Toasts"

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1966878
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1966879
    invoke-virtual {p0, v0}, LX/D7J;->addPreference(Landroid/preference/Preference;)Z

    .line 1966880
    new-instance v0, LX/4ot;

    invoke-virtual {p0}, LX/D7J;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4ot;-><init>(Landroid/content/Context;)V

    .line 1966881
    sget-object v1, LX/3HQ;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4or;->a(LX/0Tn;)V

    .line 1966882
    const-string v1, "Inject Video Ad"

    invoke-virtual {v0, v1}, LX/4ot;->setTitle(Ljava/lang/CharSequence;)V

    .line 1966883
    const-string v1, ""

    invoke-virtual {v0, v1}, LX/4ot;->setDefaultValue(Ljava/lang/Object;)V

    .line 1966884
    new-instance v1, LX/D7I;

    invoke-direct {v1, p0}, LX/D7I;-><init>(LX/D7J;)V

    invoke-virtual {v0, v1}, LX/4ot;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1966885
    sget-object v1, LX/D7J;->b:[Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, LX/4ot;->setEntries([Ljava/lang/CharSequence;)V

    .line 1966886
    sget-object v1, LX/D7J;->c:[Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, LX/4ot;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1966887
    invoke-virtual {p0, v0}, LX/D7J;->addPreference(Landroid/preference/Preference;)Z

    .line 1966888
    return-void
.end method
