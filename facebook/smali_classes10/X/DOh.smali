.class public final LX/DOh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/DOl;


# direct methods
.method public constructor <init>(LX/DOl;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1992654
    iput-object p1, p0, LX/DOh;->c:LX/DOl;

    iput-object p2, p0, LX/DOh;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/DOh;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 1992640
    iget-object v0, p0, LX/DOh;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/DOl;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1992641
    iget-object v1, p0, LX/DOh;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/DOh;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1992642
    :cond_0
    iget-object v1, p0, LX/DOh;->c:LX/DOl;

    iget-object v1, v1, LX/DOl;->c:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f081055

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1992643
    iget-object v1, p0, LX/DOh;->c:LX/DOl;

    iget-object v1, v1, LX/DOl;->i:LX/03V;

    sget-object v2, LX/DOl;->l:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Group feed story "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/DOh;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "does not have an actor id in group"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "in method deletePostAndRemoveMember"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1992644
    :goto_0
    return-void

    .line 1992645
    :cond_1
    iget-object v1, p0, LX/DOh;->c:LX/DOl;

    iget-object v2, p0, LX/DOh;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    .line 1992646
    new-instance v3, LX/4G2;

    invoke-direct {v3}, LX/4G2;-><init>()V

    iget-object v4, v1, LX/DOl;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/4G2;->a(Ljava/lang/String;)LX/4G2;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/4G2;->b(Ljava/lang/String;)LX/4G2;

    move-result-object v3

    const-string v4, "treehouse_group_mall"

    invoke-virtual {v3, v4}, LX/4G2;->d(Ljava/lang/String;)LX/4G2;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/4G2;->c(Ljava/lang/String;)LX/4G2;

    move-result-object v3

    .line 1992647
    invoke-static {}, LX/DV9;->c()LX/DV6;

    move-result-object v4

    .line 1992648
    const-string p1, "input"

    invoke-virtual {v4, p1, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1992649
    move-object v0, v4

    .line 1992650
    iget-object v1, p0, LX/DOh;->c:LX/DOl;

    iget-object v2, p0, LX/DOh;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1992651
    iget-object v3, v1, LX/DOl;->f:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1992652
    new-instance v4, LX/DOi;

    invoke-direct {v4, v1, v2}, LX/DOi;-><init>(LX/DOl;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    iget-object p0, v1, LX/DOl;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1992653
    goto :goto_0
.end method
