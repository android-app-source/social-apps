.class public final LX/DFt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DFw;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;LX/DFw;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1978921
    iput-object p1, p0, LX/DFt;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iput-object p2, p0, LX/DFt;->a:LX/DFw;

    iput-object p3, p0, LX/DFt;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 15

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x3dec9d6d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v14

    .line 1978922
    iget-object v0, p0, LX/DFt;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v1, p0, LX/DFt;->a:LX/DFw;

    iget-object v1, v1, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->a(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;

    move-result-object v0

    .line 1978923
    if-nez v0, :cond_0

    .line 1978924
    const/4 v0, 0x2

    const/4 v1, 0x2

    const v2, 0x50ddc1ea

    invoke-static {v0, v1, v2, v14}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1978925
    :goto_0
    return-void

    .line 1978926
    :cond_0
    iget-object v0, p0, LX/DFt;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v1, p0, LX/DFt;->a:LX/DFw;

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->d(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;LX/DFw;)V

    .line 1978927
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 1978928
    if-nez v2, :cond_1

    .line 1978929
    iget-object v0, p0, LX/DFt;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->b:Ljava/lang/String;

    const-string v2, "Could not find containing Activity"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1978930
    const v0, -0x3cf3ef59

    invoke-static {v0, v14}, LX/02F;->a(II)V

    goto :goto_0

    .line 1978931
    :cond_1
    iget-object v0, p0, LX/DFt;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNP;

    const/16 v1, 0x6df

    sget-object v3, LX/21D;->NEWSFEED:LX/21D;

    const-string v4, "place_review_feed_unit"

    const-string v5, "native_netego"

    const-string v6, "review_button"

    iget-object v7, p0, LX/DFt;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v8, p0, LX/DFt;->a:LX/DFw;

    iget-object v8, v8, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v7, v8}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->b(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)I

    move-result v7

    iget-object v8, p0, LX/DFt;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v9, p0, LX/DFt;->a:LX/DFw;

    iget-object v9, v9, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v8, v9}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->a(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iget-object v10, p0, LX/DFt;->b:Ljava/lang/String;

    iget-object v11, p0, LX/DFt;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v12, p0, LX/DFt;->a:LX/DFw;

    iget-object v12, v12, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v11, v12}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->c(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, LX/DFt;->c:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    iget-object v13, p0, LX/DFt;->a:LX/DFw;

    iget-object v13, v13, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v12, v13}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->d(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual/range {v0 .. v13}, LX/BNP;->a(ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)V

    .line 1978932
    const v0, 0x769b0c72

    invoke-static {v0, v14}, LX/02F;->a(II)V

    goto :goto_0
.end method
