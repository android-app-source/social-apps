.class public final LX/Dbd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;",
        ">;",
        "LX/3rL",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLInterfaces$PhotoMenusData$PagePhotoMenus$Nodes$PagePhotoMenuPhotos$;",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dbe;


# direct methods
.method public constructor <init>(LX/Dbe;)V
    .locals 0

    .prologue
    .line 2017317
    iput-object p1, p0, LX/Dbd;->a:LX/Dbe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2017318
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v4, 0x79426577

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2017319
    if-eqz p1, :cond_0

    .line 2017320
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2017321
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2017322
    const/4 v0, 0x0

    .line 2017323
    :goto_2
    return-object v0

    .line 2017324
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2017325
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2017326
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2017327
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2017328
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2017329
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2017330
    :cond_5
    new-instance v1, LX/8A4;

    .line 2017331
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2017332
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;->j()LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v0, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-virtual {v1, v0}, LX/8A4;->a(LX/8A3;)Z

    move-result v3

    .line 2017333
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2017334
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2017335
    new-instance v1, LX/3rL;

    const-class v5, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_2

    .line 2017336
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4
.end method
