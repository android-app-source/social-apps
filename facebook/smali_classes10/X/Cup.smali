.class public final enum LX/Cup;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cup;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cup;

.field public static final enum MUTE:LX/Cup;

.field public static final enum NONE:LX/Cup;

.field public static final enum UNMUTE:LX/Cup;

.field public static final enum USE_EXISTING_MUTE_STATE:LX/Cup;

.field public static final enum VOLUME:LX/Cup;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1947175
    new-instance v0, LX/Cup;

    const-string v1, "UNMUTE"

    invoke-direct {v0, v1, v2}, LX/Cup;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cup;->UNMUTE:LX/Cup;

    .line 1947176
    new-instance v0, LX/Cup;

    const-string v1, "MUTE"

    invoke-direct {v0, v1, v3}, LX/Cup;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cup;->MUTE:LX/Cup;

    .line 1947177
    new-instance v0, LX/Cup;

    const-string v1, "VOLUME"

    invoke-direct {v0, v1, v4}, LX/Cup;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cup;->VOLUME:LX/Cup;

    .line 1947178
    new-instance v0, LX/Cup;

    const-string v1, "USE_EXISTING_MUTE_STATE"

    invoke-direct {v0, v1, v5}, LX/Cup;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cup;->USE_EXISTING_MUTE_STATE:LX/Cup;

    .line 1947179
    new-instance v0, LX/Cup;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v6}, LX/Cup;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cup;->NONE:LX/Cup;

    .line 1947180
    const/4 v0, 0x5

    new-array v0, v0, [LX/Cup;

    sget-object v1, LX/Cup;->UNMUTE:LX/Cup;

    aput-object v1, v0, v2

    sget-object v1, LX/Cup;->MUTE:LX/Cup;

    aput-object v1, v0, v3

    sget-object v1, LX/Cup;->VOLUME:LX/Cup;

    aput-object v1, v0, v4

    sget-object v1, LX/Cup;->USE_EXISTING_MUTE_STATE:LX/Cup;

    aput-object v1, v0, v5

    sget-object v1, LX/Cup;->NONE:LX/Cup;

    aput-object v1, v0, v6

    sput-object v0, LX/Cup;->$VALUES:[LX/Cup;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1947183
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cup;
    .locals 1

    .prologue
    .line 1947182
    const-class v0, LX/Cup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cup;

    return-object v0
.end method

.method public static values()[LX/Cup;
    .locals 1

    .prologue
    .line 1947181
    sget-object v0, LX/Cup;->$VALUES:[LX/Cup;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cup;

    return-object v0
.end method
