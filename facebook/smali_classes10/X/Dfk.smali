.class public LX/Dfk;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2027707
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2027708
    invoke-direct {p0}, LX/Dfk;->a()V

    .line 2027709
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2027704
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2027705
    invoke-direct {p0}, LX/Dfk;->a()V

    .line 2027706
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2027701
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2027702
    invoke-direct {p0}, LX/Dfk;->a()V

    .line 2027703
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2027695
    const v0, 0x7f0308f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2027696
    const v0, 0x7f0d1715

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Dfk;->a:Landroid/view/View;

    .line 2027697
    const v0, 0x7f0d1716

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Dfk;->b:Landroid/widget/ImageView;

    .line 2027698
    const v0, 0x7f0d1717

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Dfk;->c:Landroid/widget/TextView;

    .line 2027699
    const v0, 0x7f0d1718

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/Dfk;->d:Landroid/widget/ProgressBar;

    .line 2027700
    return-void
.end method


# virtual methods
.method public setMoreItem(Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;)V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 2027676
    sget-object v0, LX/Dfj;->a:[I

    .line 2027677
    sget-object v1, LX/Dfm;->a:[I

    iget-object v5, p1, Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;->a:LX/Dfn;

    invoke-virtual {v5}, LX/Dfn;->ordinal()I

    move-result v5

    aget v1, v1, v5

    packed-switch v1, :pswitch_data_0

    .line 2027678
    sget-object v1, LX/Dfi;->LOAD_MORE:LX/Dfi;

    :goto_0
    move-object v1, v1

    .line 2027679
    invoke-virtual {v1}, LX/Dfi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2027680
    :goto_1
    iget-object v0, p0, LX/Dfk;->b:Landroid/widget/ImageView;

    .line 2027681
    const v1, 0x7f020f97

    move v1, v1

    .line 2027682
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2027683
    iget-object v0, p0, LX/Dfk;->c:Landroid/widget/TextView;

    .line 2027684
    iget-object v1, p1, Lcom/facebook/messaging/inbox2/morefooter/InboxMoreThreadsItem;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2027685
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2027686
    return-void

    .line 2027687
    :pswitch_0
    iget-object v0, p0, LX/Dfk;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2027688
    iget-object v0, p0, LX/Dfk;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 2027689
    :pswitch_1
    iget-object v0, p0, LX/Dfk;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2027690
    iget-object v0, p0, LX/Dfk;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 2027691
    :pswitch_2
    iget-object v0, p0, LX/Dfk;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2027692
    iget-object v0, p0, LX/Dfk;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 2027693
    :pswitch_3
    sget-object v1, LX/Dfi;->INVISIBLE:LX/Dfi;

    goto :goto_0

    .line 2027694
    :pswitch_4
    sget-object v1, LX/Dfi;->LOADING:LX/Dfi;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
