.class public LX/DHH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/23q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/23q",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1VK;

.field private final c:LX/DH6;

.field public final d:LX/6W7;

.field public final e:LX/2yK;


# direct methods
.method public constructor <init>(LX/23q;LX/1VK;LX/DH6;LX/6W7;LX/2yK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1981645
    iput-object p1, p0, LX/DHH;->a:LX/23q;

    .line 1981646
    iput-object p2, p0, LX/DHH;->b:LX/1VK;

    .line 1981647
    iput-object p3, p0, LX/DHH;->c:LX/DH6;

    .line 1981648
    iput-object p4, p0, LX/DHH;->d:LX/6W7;

    .line 1981649
    iput-object p5, p0, LX/DHH;->e:LX/2yK;

    .line 1981650
    return-void
.end method

.method public static a(LX/DHH;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)LX/3Qx;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;TE;)",
            "LX/3Qx;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1981651
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1981652
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1981653
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1981654
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1981655
    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1981656
    invoke-static {p0, v1, p2}, LX/DHH;->b(LX/DHH;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v2

    .line 1981657
    iget-object v9, p0, LX/DHH;->a:LX/23q;

    new-instance v0, LX/3FV;

    iget-object v4, p0, LX/DHH;->c:LX/DH6;

    invoke-virtual {v4, p1}, LX/DH6;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/D4s;

    move-result-object v4

    sget-object v8, LX/04D;->SINGLE_CREATOR_SET_FOOTER:LX/04D;

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v8}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;LX/D6L;LX/0JG;Ljava/lang/String;LX/04D;)V

    invoke-virtual {v9, v0, p2}, LX/23q;->a(LX/3FV;LX/1Po;)LX/3Qx;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/DHH;
    .locals 9

    .prologue
    .line 1981658
    const-class v1, LX/DHH;

    monitor-enter v1

    .line 1981659
    :try_start_0
    sget-object v0, LX/DHH;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981660
    sput-object v2, LX/DHH;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981661
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981662
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981663
    new-instance v3, LX/DHH;

    invoke-static {v0}, LX/23q;->a(LX/0QB;)LX/23q;

    move-result-object v4

    check-cast v4, LX/23q;

    const-class v5, LX/1VK;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1VK;

    invoke-static {v0}, LX/DH6;->a(LX/0QB;)LX/DH6;

    move-result-object v6

    check-cast v6, LX/DH6;

    invoke-static {v0}, LX/6W7;->a(LX/0QB;)LX/6W7;

    move-result-object v7

    check-cast v7, LX/6W7;

    invoke-static {v0}, LX/2yK;->b(LX/0QB;)LX/2yK;

    move-result-object v8

    check-cast v8, LX/2yK;

    invoke-direct/range {v3 .. v8}, LX/DHH;-><init>(LX/23q;LX/1VK;LX/DH6;LX/6W7;LX/2yK;)V

    .line 1981664
    move-object v0, v3

    .line 1981665
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981666
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DHH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981667
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981668
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/DHH;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LX/3J0;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1981669
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1981670
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1981671
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 1981672
    new-instance v2, LX/2oK;

    iget-object v3, p0, LX/DHH;->b:LX/1VK;

    invoke-direct {v2, v1, v0, v3}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    .line 1981673
    check-cast p2, LX/1Pr;

    .line 1981674
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1981675
    check-cast v0, LX/0jW;

    invoke-interface {p2, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oL;

    move-object v0, v0

    .line 1981676
    invoke-virtual {v0}, LX/2oL;->b()LX/2oO;

    move-result-object v0

    .line 1981677
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v2, LX/DHG;

    invoke-direct {v2, p0, v0}, LX/DHG;-><init>(LX/DHH;LX/2oO;)V

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    return-object v1
.end method
