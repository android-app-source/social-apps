.class public LX/E1s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/E4b;

.field private final b:LX/E1y;

.field public final c:LX/E1v;

.field public final d:LX/E22;

.field public final e:LX/E25;

.field public final f:LX/E28;

.field private final g:LX/E2K;


# direct methods
.method public constructor <init>(LX/E4b;LX/E1y;LX/E1v;LX/E22;LX/E25;LX/E28;LX/E2K;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2071639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2071640
    iput-object p1, p0, LX/E1s;->a:LX/E4b;

    .line 2071641
    iput-object p2, p0, LX/E1s;->b:LX/E1y;

    .line 2071642
    iput-object p3, p0, LX/E1s;->c:LX/E1v;

    .line 2071643
    iput-object p4, p0, LX/E1s;->d:LX/E22;

    .line 2071644
    iput-object p5, p0, LX/E1s;->e:LX/E25;

    .line 2071645
    iput-object p6, p0, LX/E1s;->f:LX/E28;

    .line 2071646
    iput-object p7, p0, LX/E1s;->g:LX/E2K;

    .line 2071647
    return-void
.end method

.method public static a(LX/0QB;)LX/E1s;
    .locals 11

    .prologue
    .line 2071628
    const-class v1, LX/E1s;

    monitor-enter v1

    .line 2071629
    :try_start_0
    sget-object v0, LX/E1s;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2071630
    sput-object v2, LX/E1s;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2071631
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2071632
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2071633
    new-instance v3, LX/E1s;

    invoke-static {v0}, LX/E4b;->a(LX/0QB;)LX/E4b;

    move-result-object v4

    check-cast v4, LX/E4b;

    invoke-static {v0}, LX/E1y;->a(LX/0QB;)LX/E1y;

    move-result-object v5

    check-cast v5, LX/E1y;

    invoke-static {v0}, LX/E1v;->a(LX/0QB;)LX/E1v;

    move-result-object v6

    check-cast v6, LX/E1v;

    invoke-static {v0}, LX/E22;->a(LX/0QB;)LX/E22;

    move-result-object v7

    check-cast v7, LX/E22;

    invoke-static {v0}, LX/E25;->a(LX/0QB;)LX/E25;

    move-result-object v8

    check-cast v8, LX/E25;

    invoke-static {v0}, LX/E28;->a(LX/0QB;)LX/E28;

    move-result-object v9

    check-cast v9, LX/E28;

    invoke-static {v0}, LX/E2K;->a(LX/0QB;)LX/E2K;

    move-result-object v10

    check-cast v10, LX/E2K;

    invoke-direct/range {v3 .. v10}, LX/E1s;-><init>(LX/E4b;LX/E1y;LX/E1v;LX/E22;LX/E25;LX/E28;LX/E2K;)V

    .line 2071634
    move-object v0, v3

    .line 2071635
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2071636
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/E1s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2071637
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2071638
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/E1s;LX/1De;LX/2km;LX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/E2I;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "LX/9uc;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/E2I;"
        }
    .end annotation

    .prologue
    .line 2071600
    invoke-interface {p3}, LX/9uc;->be()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    move-result-object v0

    .line 2071601
    iget-object v1, p0, LX/E1s;->g:LX/E2K;

    const/4 v2, 0x0

    .line 2071602
    new-instance v3, LX/E2J;

    invoke-direct {v3, v1}, LX/E2J;-><init>(LX/E2K;)V

    .line 2071603
    iget-object p0, v1, LX/E2K;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/E2I;

    .line 2071604
    if-nez p0, :cond_0

    .line 2071605
    new-instance p0, LX/E2I;

    invoke-direct {p0, v1}, LX/E2I;-><init>(LX/E2K;)V

    .line 2071606
    :cond_0
    invoke-static {p0, p1, v2, v2, v3}, LX/E2I;->a$redex0(LX/E2I;LX/1De;IILX/E2J;)V

    .line 2071607
    move-object v3, p0

    .line 2071608
    move-object v2, v3

    .line 2071609
    move-object v1, v2

    .line 2071610
    invoke-interface {p3}, LX/9uc;->be()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 2071611
    iget-object v3, v1, LX/E2I;->a:LX/E2J;

    iput-object v2, v3, LX/E2J;->e:Ljava/lang/String;

    .line 2071612
    move-object v1, v1

    .line 2071613
    iget-object v2, v1, LX/E2I;->a:LX/E2J;

    iput-object v0, v2, LX/E2J;->a:LX/9uY;

    .line 2071614
    iget-object v2, v1, LX/E2I;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2071615
    move-object v1, v1

    .line 2071616
    check-cast p2, LX/1Pq;

    .line 2071617
    iget-object v2, v1, LX/E2I;->a:LX/E2J;

    iput-object p2, v2, LX/E2J;->b:LX/1Pq;

    .line 2071618
    iget-object v2, v1, LX/E2I;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2071619
    move-object v1, v1

    .line 2071620
    invoke-interface {v0}, LX/9uY;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v0

    .line 2071621
    iget-object v2, v1, LX/E2I;->a:LX/E2J;

    iput-object v0, v2, LX/E2J;->c:LX/5sc;

    .line 2071622
    iget-object v2, v1, LX/E2I;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2071623
    move-object v0, v1

    .line 2071624
    iget-object v1, v0, LX/E2I;->a:LX/E2J;

    iput-object p4, v1, LX/E2J;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2071625
    iget-object v1, v0, LX/E2I;->e:Ljava/util/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2071626
    move-object v0, v0

    .line 2071627
    return-object v0
.end method

.method public static a(LX/9uc;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2071517
    sget-object v2, LX/E1r;->a:[I

    invoke-interface {p0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 2071518
    :cond_0
    :goto_0
    return v0

    .line 2071519
    :pswitch_0
    invoke-static {p0}, LX/E1s;->b(LX/9uc;)Z

    move-result v0

    goto :goto_0

    .line 2071520
    :pswitch_1
    invoke-interface {p0}, LX/9uc;->ab()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, LX/9uc;->ab()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {p0}, LX/9uc;->T()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 2071521
    :pswitch_2
    invoke-static {p0}, LX/E1s;->b(LX/9uc;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, LX/9uc;->ab()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, LX/9uc;->ab()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {p0}, LX/9uc;->X()Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    move-result-object v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2071522
    :pswitch_3
    invoke-static {p0}, LX/E1s;->b(LX/9uc;)Z

    move-result v0

    goto :goto_0

    .line 2071523
    :pswitch_4
    invoke-interface {p0}, LX/9uc;->be()Lcom/facebook/reaction/protocol/graphql/ReactionUnitCoreComponentsGraphQLModels$ReactionCoreToggleStateComponentFragmentModel$InitialComponentModel;

    move-result-object v2

    .line 2071524
    if-eqz v2, :cond_3

    invoke-interface {v2}, LX/9uY;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, LX/9uY;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SAVE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v3, v4, :cond_3

    invoke-interface {v2}, LX/9uY;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-static {v2}, LX/E2i;->a(LX/5sc;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static b(LX/9uc;)Z
    .locals 1

    .prologue
    .line 2071599
    invoke-interface {p0}, LX/9uc;->b()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/9uc;->b()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/9uc;->c()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/E1s;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/E4Z;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/E4Z;"
        }
    .end annotation

    .prologue
    .line 2071526
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2071527
    iget-object v1, p0, LX/E1s;->a:LX/E4b;

    invoke-virtual {v1, p1}, LX/E4b;->c(LX/1De;)LX/E4Z;

    move-result-object v1

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/E4Z;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/E4Z;

    move-result-object v1

    invoke-interface {v0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v2

    .line 2071528
    iget-object v3, v1, LX/E4Z;->a:LX/E4a;

    iput-object v2, v3, LX/E4a;->c:Ljava/lang/String;

    .line 2071529
    move-object v1, v1

    .line 2071530
    invoke-virtual {v1, p3}, LX/E4Z;->a(LX/2km;)LX/E4Z;

    move-result-object v1

    iget-object v2, p0, LX/E1s;->b:LX/E1y;

    invoke-virtual {v2, p1}, LX/E1y;->c(LX/1De;)LX/E1w;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->U()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1w;->b(Ljava/lang/String;)LX/E1w;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->V()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1w;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentBorderSpecFieldsModel;)LX/E1w;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->Y()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1w;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentMarginSpecFieldsModel;)LX/E1w;

    move-result-object v2

    invoke-interface {v0}, LX/9uc;->Z()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/E1w;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentPaddingSpecFieldsModel;)LX/E1w;

    move-result-object v2

    .line 2071531
    sget-object v4, LX/E1r;->a:[I

    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2071532
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    :goto_0
    move-object v0, v4

    .line 2071533
    invoke-virtual {v2, v0}, LX/E1w;->a(LX/1X1;)LX/E1w;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/E4Z;->a(LX/1X5;)LX/E4Z;

    move-result-object v0

    .line 2071534
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2071535
    invoke-virtual {v0, v1}, LX/E4Z;->c(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    .line 2071536
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2071537
    invoke-virtual {v0, v1}, LX/E4Z;->d(Ljava/lang/String;)LX/E4Z;

    move-result-object v0

    return-object v0

    .line 2071538
    :pswitch_0
    iget-object v4, p0, LX/E1s;->c:LX/E1v;

    const/4 v5, 0x0

    .line 2071539
    new-instance v6, LX/E1u;

    invoke-direct {v6, v4}, LX/E1u;-><init>(LX/E1v;)V

    .line 2071540
    sget-object v7, LX/E1v;->a:LX/0Zi;

    invoke-virtual {v7}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/E1t;

    .line 2071541
    if-nez v7, :cond_0

    .line 2071542
    new-instance v7, LX/E1t;

    invoke-direct {v7}, LX/E1t;-><init>()V

    .line 2071543
    :cond_0
    invoke-static {v7, p1, v5, v5, v6}, LX/E1t;->a$redex0(LX/E1t;LX/1De;IILX/E1u;)V

    .line 2071544
    move-object v6, v7

    .line 2071545
    move-object v5, v6

    .line 2071546
    move-object v4, v5

    .line 2071547
    invoke-interface {v0}, LX/9uc;->W()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    move-result-object v5

    .line 2071548
    iget-object v6, v4, LX/E1t;->a:LX/E1u;

    iput-object v5, v6, LX/E1u;->b:Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    .line 2071549
    move-object v4, v4

    .line 2071550
    invoke-interface {v0}, LX/9uc;->aa()LX/5sY;

    move-result-object v5

    .line 2071551
    iget-object v6, v4, LX/E1t;->a:LX/E1u;

    iput-object v5, v6, LX/E1u;->c:LX/5sY;

    .line 2071552
    move-object v4, v4

    .line 2071553
    iget-object v5, v4, LX/E1t;->a:LX/E1u;

    iput-object v0, v5, LX/E1u;->a:LX/9qL;

    .line 2071554
    iget-object v5, v4, LX/E1t;->d:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2071555
    move-object v4, v4

    .line 2071556
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    goto :goto_0

    .line 2071557
    :pswitch_1
    iget-object v4, p0, LX/E1s;->d:LX/E22;

    const/4 v5, 0x0

    .line 2071558
    new-instance v6, LX/E21;

    invoke-direct {v6, v4}, LX/E21;-><init>(LX/E22;)V

    .line 2071559
    sget-object v7, LX/E22;->a:LX/0Zi;

    invoke-virtual {v7}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/E20;

    .line 2071560
    if-nez v7, :cond_1

    .line 2071561
    new-instance v7, LX/E20;

    invoke-direct {v7}, LX/E20;-><init>()V

    .line 2071562
    :cond_1
    invoke-static {v7, p1, v5, v5, v6}, LX/E20;->a$redex0(LX/E20;LX/1De;IILX/E21;)V

    .line 2071563
    move-object v6, v7

    .line 2071564
    move-object v5, v6

    .line 2071565
    move-object v4, v5

    .line 2071566
    invoke-interface {v0}, LX/9uc;->T()D

    move-result-wide v6

    double-to-float v5, v6

    .line 2071567
    iget-object v6, v4, LX/E20;->a:LX/E21;

    iput v5, v6, LX/E21;->b:F

    .line 2071568
    iget-object v6, v4, LX/E20;->d:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2071569
    move-object v4, v4

    .line 2071570
    invoke-interface {v0}, LX/9uc;->ab()LX/5sY;

    move-result-object v5

    .line 2071571
    iget-object v6, v4, LX/E20;->a:LX/E21;

    iput-object v5, v6, LX/E21;->a:LX/5sY;

    .line 2071572
    iget-object v6, v4, LX/E20;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2071573
    move-object v4, v4

    .line 2071574
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    goto/16 :goto_0

    .line 2071575
    :pswitch_2
    iget-object v4, p0, LX/E1s;->e:LX/E25;

    const/4 v5, 0x0

    .line 2071576
    new-instance v6, LX/E24;

    invoke-direct {v6, v4}, LX/E24;-><init>(LX/E25;)V

    .line 2071577
    sget-object v7, LX/E25;->a:LX/0Zi;

    invoke-virtual {v7}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/E23;

    .line 2071578
    if-nez v7, :cond_2

    .line 2071579
    new-instance v7, LX/E23;

    invoke-direct {v7}, LX/E23;-><init>()V

    .line 2071580
    :cond_2
    invoke-static {v7, p1, v5, v5, v6}, LX/E23;->a$redex0(LX/E23;LX/1De;IILX/E24;)V

    .line 2071581
    move-object v6, v7

    .line 2071582
    move-object v5, v6

    .line 2071583
    move-object v4, v5

    .line 2071584
    invoke-interface {v0}, LX/9uc;->X()Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    move-result-object v5

    .line 2071585
    iget-object v6, v4, LX/E23;->a:LX/E24;

    iput-object v5, v6, LX/E24;->c:Lcom/facebook/graphql/enums/GraphQLReactionCoreImageTextImageSize;

    .line 2071586
    move-object v4, v4

    .line 2071587
    invoke-interface {v0}, LX/9uc;->ab()LX/5sY;

    move-result-object v5

    .line 2071588
    iget-object v6, v4, LX/E23;->a:LX/E24;

    iput-object v5, v6, LX/E24;->a:LX/5sY;

    .line 2071589
    iget-object v6, v4, LX/E23;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2071590
    move-object v4, v4

    .line 2071591
    invoke-interface {v0}, LX/9uc;->aX()Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    move-result-object v5

    .line 2071592
    iget-object v6, v4, LX/E23;->a:LX/E24;

    iput-object v5, v6, LX/E24;->d:Lcom/facebook/graphql/enums/GraphQLReactionCoreComponentPadding;

    .line 2071593
    move-object v4, v4

    .line 2071594
    iget-object v5, v4, LX/E23;->a:LX/E24;

    iput-object v0, v5, LX/E24;->b:LX/9qL;

    .line 2071595
    iget-object v5, v4, LX/E23;->d:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2071596
    move-object v4, v4

    .line 2071597
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    goto/16 :goto_0

    .line 2071598
    :pswitch_3
    iget-object v4, p0, LX/E1s;->f:LX/E28;

    invoke-virtual {v4, p1}, LX/E28;->c(LX/1De;)LX/E26;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/E26;->a(LX/9qL;)LX/E26;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1;"
        }
    .end annotation

    .prologue
    .line 2071525
    invoke-static {p0, p1, p2, p3}, LX/E1s;->c(LX/E1s;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/E4Z;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method
