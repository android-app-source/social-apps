.class public final enum LX/DnJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DnJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DnJ;

.field public static final enum APPOINTMENT_HEADER:LX/DnJ;

.field public static final enum APPOINTMENT_NOTE:LX/DnJ;

.field public static final enum APPOINTMENT_TIME:LX/DnJ;

.field public static final enum BOOKING_STATUS:LX/DnJ;

.field public static final enum SERVICE_DATE:LX/DnJ;

.field public static final enum SERVICE_INFO:LX/DnJ;

.field public static final enum SERVICE_INFO_V2:LX/DnJ;

.field public static final enum SERVICE_PRICE:LX/DnJ;

.field public static final enum SERVICE_TIME:LX/DnJ;


# instance fields
.field private final viewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2040180
    new-instance v0, LX/DnJ;

    const-string v1, "APPOINTMENT_HEADER"

    const v2, 0x7f0d01f0

    invoke-direct {v0, v1, v4, v2}, LX/DnJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnJ;->APPOINTMENT_HEADER:LX/DnJ;

    .line 2040181
    new-instance v0, LX/DnJ;

    const-string v1, "SERVICE_INFO"

    const v2, 0x7f0d01e8

    invoke-direct {v0, v1, v5, v2}, LX/DnJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnJ;->SERVICE_INFO:LX/DnJ;

    .line 2040182
    new-instance v0, LX/DnJ;

    const-string v1, "APPOINTMENT_TIME"

    const v2, 0x7f0d01e9

    invoke-direct {v0, v1, v6, v2}, LX/DnJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnJ;->APPOINTMENT_TIME:LX/DnJ;

    .line 2040183
    new-instance v0, LX/DnJ;

    const-string v1, "BOOKING_STATUS"

    const v2, 0x7f0d01ea

    invoke-direct {v0, v1, v7, v2}, LX/DnJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnJ;->BOOKING_STATUS:LX/DnJ;

    .line 2040184
    new-instance v0, LX/DnJ;

    const-string v1, "SERVICE_INFO_V2"

    const v2, 0x7f0d01eb

    invoke-direct {v0, v1, v8, v2}, LX/DnJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnJ;->SERVICE_INFO_V2:LX/DnJ;

    .line 2040185
    new-instance v0, LX/DnJ;

    const-string v1, "SERVICE_DATE"

    const/4 v2, 0x5

    const v3, 0x7f0d01ec

    invoke-direct {v0, v1, v2, v3}, LX/DnJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnJ;->SERVICE_DATE:LX/DnJ;

    .line 2040186
    new-instance v0, LX/DnJ;

    const-string v1, "SERVICE_TIME"

    const/4 v2, 0x6

    const v3, 0x7f0d01ed

    invoke-direct {v0, v1, v2, v3}, LX/DnJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnJ;->SERVICE_TIME:LX/DnJ;

    .line 2040187
    new-instance v0, LX/DnJ;

    const-string v1, "SERVICE_PRICE"

    const/4 v2, 0x7

    const v3, 0x7f0d01ee

    invoke-direct {v0, v1, v2, v3}, LX/DnJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnJ;->SERVICE_PRICE:LX/DnJ;

    .line 2040188
    new-instance v0, LX/DnJ;

    const-string v1, "APPOINTMENT_NOTE"

    const/16 v2, 0x8

    const v3, 0x7f0d01ef

    invoke-direct {v0, v1, v2, v3}, LX/DnJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnJ;->APPOINTMENT_NOTE:LX/DnJ;

    .line 2040189
    const/16 v0, 0x9

    new-array v0, v0, [LX/DnJ;

    sget-object v1, LX/DnJ;->APPOINTMENT_HEADER:LX/DnJ;

    aput-object v1, v0, v4

    sget-object v1, LX/DnJ;->SERVICE_INFO:LX/DnJ;

    aput-object v1, v0, v5

    sget-object v1, LX/DnJ;->APPOINTMENT_TIME:LX/DnJ;

    aput-object v1, v0, v6

    sget-object v1, LX/DnJ;->BOOKING_STATUS:LX/DnJ;

    aput-object v1, v0, v7

    sget-object v1, LX/DnJ;->SERVICE_INFO_V2:LX/DnJ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/DnJ;->SERVICE_DATE:LX/DnJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DnJ;->SERVICE_TIME:LX/DnJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DnJ;->SERVICE_PRICE:LX/DnJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DnJ;->APPOINTMENT_NOTE:LX/DnJ;

    aput-object v2, v0, v1

    sput-object v0, LX/DnJ;->$VALUES:[LX/DnJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2040190
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2040191
    iput p3, p0, LX/DnJ;->viewType:I

    .line 2040192
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DnJ;
    .locals 1

    .prologue
    .line 2040193
    const-class v0, LX/DnJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DnJ;

    return-object v0
.end method

.method public static values()[LX/DnJ;
    .locals 1

    .prologue
    .line 2040194
    sget-object v0, LX/DnJ;->$VALUES:[LX/DnJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DnJ;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 2040195
    iget v0, p0, LX/DnJ;->viewType:I

    return v0
.end method
