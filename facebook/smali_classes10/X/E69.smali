.class public final LX/E69;
.super Landroid/text/style/ClickableSpan;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Landroid/text/style/ClickableSpan;"
    }
.end annotation


# instance fields
.field private final a:LX/Cfl;

.field private final b:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/E1f;LX/2km;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "LX/E1f;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2079757
    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 2079758
    if-nez p1, :cond_0

    :goto_0
    iput-object v3, p0, LX/E69;->a:LX/Cfl;

    .line 2079759
    iput-object p3, p0, LX/E69;->b:LX/2km;

    .line 2079760
    iput-object p4, p0, LX/E69;->c:Ljava/lang/String;

    .line 2079761
    iput-object p5, p0, LX/E69;->d:Ljava/lang/String;

    .line 2079762
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/E69;->e:I

    .line 2079763
    return-void

    :cond_0
    move-object v0, p3

    .line 2079764
    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    move-object v0, p3

    check-cast v0, LX/2kp;

    invoke-interface {v0}, LX/2kp;->t()LX/2jY;

    move-result-object v0

    .line 2079765
    iget-object v1, v0, LX/2jY;->a:Ljava/lang/String;

    move-object v4, v1

    .line 2079766
    move-object v0, p3

    check-cast v0, LX/2kp;

    invoke-interface {v0}, LX/2kp;->t()LX/2jY;

    move-result-object v0

    .line 2079767
    iget-object v1, v0, LX/2jY;->b:Ljava/lang/String;

    move-object v5, v1

    .line 2079768
    move-object v0, p2

    move-object v1, p1

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2079769
    iget-object v0, p0, LX/E69;->b:LX/2km;

    iget-object v1, p0, LX/E69;->c:Ljava/lang/String;

    iget-object v2, p0, LX/E69;->d:Ljava/lang/String;

    iget-object v3, p0, LX/E69;->a:LX/Cfl;

    invoke-interface {v0, v1, v2, v3}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2079770
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2079771
    iget-object v0, p0, LX/E69;->a:LX/Cfl;

    if-eqz v0, :cond_0

    .line 2079772
    iget v0, p0, LX/E69;->e:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2079773
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2079774
    return-void
.end method
