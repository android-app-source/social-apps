.class public final enum LX/DL4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DL4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DL4;

.field public static final enum UPLOAD_GROUP_FILE_BODY:LX/DL4;

.field public static final enum UPLOAD_GROUP_FILE_HANDLE:LX/DL4;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1987787
    new-instance v0, LX/DL4;

    const-string v1, "UPLOAD_GROUP_FILE_BODY"

    invoke-direct {v0, v1, v2}, LX/DL4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DL4;->UPLOAD_GROUP_FILE_BODY:LX/DL4;

    .line 1987788
    new-instance v0, LX/DL4;

    const-string v1, "UPLOAD_GROUP_FILE_HANDLE"

    invoke-direct {v0, v1, v3}, LX/DL4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DL4;->UPLOAD_GROUP_FILE_HANDLE:LX/DL4;

    .line 1987789
    const/4 v0, 0x2

    new-array v0, v0, [LX/DL4;

    sget-object v1, LX/DL4;->UPLOAD_GROUP_FILE_BODY:LX/DL4;

    aput-object v1, v0, v2

    sget-object v1, LX/DL4;->UPLOAD_GROUP_FILE_HANDLE:LX/DL4;

    aput-object v1, v0, v3

    sput-object v0, LX/DL4;->$VALUES:[LX/DL4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1987790
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DL4;
    .locals 1

    .prologue
    .line 1987791
    const-class v0, LX/DL4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DL4;

    return-object v0
.end method

.method public static values()[LX/DL4;
    .locals 1

    .prologue
    .line 1987792
    sget-object v0, LX/DL4;->$VALUES:[LX/DL4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DL4;

    return-object v0
.end method
