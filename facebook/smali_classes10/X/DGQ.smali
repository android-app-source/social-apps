.class public LX/DGQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DGS;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DGQ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DGS;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979759
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1979760
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DGQ;->b:LX/0Zi;

    .line 1979761
    iput-object p1, p0, LX/DGQ;->a:LX/0Ot;

    .line 1979762
    return-void
.end method

.method public static a(LX/0QB;)LX/DGQ;
    .locals 4

    .prologue
    .line 1979763
    const-class v1, LX/DGQ;

    monitor-enter v1

    .line 1979764
    :try_start_0
    sget-object v0, LX/DGQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979765
    sput-object v2, LX/DGQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979766
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979767
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979768
    new-instance v3, LX/DGQ;

    const/16 p0, 0x21a6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DGQ;-><init>(LX/0Ot;)V

    .line 1979769
    move-object v0, v3

    .line 1979770
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979771
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DGQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979772
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979773
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1979774
    check-cast p2, LX/DGP;

    .line 1979775
    iget-object v0, p0, LX/DGQ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DGS;

    iget-object v1, p2, LX/DGP;->a:LX/DGK;

    iget-object v2, p2, LX/DGP;->b:LX/DG8;

    iget-object v3, p2, LX/DGP;->c:LX/1Pd;

    const/4 v9, 0x1

    .line 1979776
    iget-object v4, v1, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v6, v4

    .line 1979777
    iget-object v4, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1979778
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1979779
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v7, 0x0

    invoke-interface {v5, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    iget-object v7, v0, LX/DGS;->h:LX/1DR;

    iget-object v8, v0, LX/DGS;->g:Landroid/content/Context;

    invoke-virtual {v7, v8}, LX/1DR;->a(Landroid/content/Context;)I

    move-result v7

    invoke-interface {v5, v7}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v5

    const v7, 0x7f0b0917

    invoke-interface {v5, v9, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const/4 v7, 0x6

    const v8, 0x7f0b0917

    invoke-interface {v5, v7, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const/4 v7, 0x3

    const v8, 0x7f0b0917

    invoke-interface {v5, v7, v8}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v5

    const v7, 0x7f0213f4

    invoke-interface {v5, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    iget-object v7, v0, LX/DGS;->a:LX/3Dm;

    invoke-virtual {v7, p1}, LX/3Dm;->c(LX/1De;)LX/DH2;

    move-result-object v7

    invoke-virtual {v7, v1}, LX/DH2;->a(LX/DGK;)LX/DH2;

    move-result-object v7

    const v8, 0x7f0b0f29

    .line 1979780
    iget-object p0, v7, LX/DH2;->a:LX/DH3;

    iput v8, p0, LX/DH3;->c:I

    .line 1979781
    move-object v7, v7

    .line 1979782
    const v8, 0x7f0b0f2c

    .line 1979783
    iget-object p0, v7, LX/DH2;->a:LX/DH3;

    iput v8, p0, LX/DH3;->d:I

    .line 1979784
    move-object v7, v7

    .line 1979785
    const v8, 0x7f0b0f2c

    .line 1979786
    iget-object p0, v7, LX/DH2;->a:LX/DH3;

    iput v8, p0, LX/DH3;->e:I

    .line 1979787
    move-object v7, v7

    .line 1979788
    iget-object v8, v7, LX/DH2;->a:LX/DH3;

    iput v9, v8, LX/DH3;->f:I

    .line 1979789
    move-object v7, v7

    .line 1979790
    invoke-virtual {v7, v3}, LX/DH2;->a(LX/1Pd;)LX/DH2;

    move-result-object v7

    invoke-interface {v5, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    iget-object v7, v0, LX/DGS;->b:LX/C74;

    const/4 v8, 0x0

    .line 1979791
    new-instance p0, LX/C73;

    invoke-direct {p0, v7}, LX/C73;-><init>(LX/C74;)V

    .line 1979792
    iget-object p2, v7, LX/C74;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/C72;

    .line 1979793
    if-nez p2, :cond_0

    .line 1979794
    new-instance p2, LX/C72;

    invoke-direct {p2, v7}, LX/C72;-><init>(LX/C74;)V

    .line 1979795
    :cond_0
    invoke-static {p2, p1, v8, v8, p0}, LX/C72;->a$redex0(LX/C72;LX/1De;IILX/C73;)V

    .line 1979796
    move-object p0, p2

    .line 1979797
    move-object v8, p0

    .line 1979798
    move-object v7, v8

    .line 1979799
    iget-object v8, v7, LX/C72;->a:LX/C73;

    iput-object v6, v8, LX/C73;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979800
    iget-object v8, v7, LX/C72;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v8, p0}, Ljava/util/BitSet;->set(I)V

    .line 1979801
    move-object v7, v7

    .line 1979802
    invoke-interface {v5, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    iget-object v7, v0, LX/DGS;->f:LX/DG6;

    const/4 v8, 0x0

    .line 1979803
    new-instance p0, LX/DG5;

    invoke-direct {p0, v7}, LX/DG5;-><init>(LX/DG6;)V

    .line 1979804
    iget-object p2, v7, LX/DG6;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/DG4;

    .line 1979805
    if-nez p2, :cond_1

    .line 1979806
    new-instance p2, LX/DG4;

    invoke-direct {p2, v7}, LX/DG4;-><init>(LX/DG6;)V

    .line 1979807
    :cond_1
    invoke-static {p2, p1, v8, v8, p0}, LX/DG4;->a$redex0(LX/DG4;LX/1De;IILX/DG5;)V

    .line 1979808
    move-object p0, p2

    .line 1979809
    move-object v8, p0

    .line 1979810
    move-object v7, v8

    .line 1979811
    iget-object v8, v7, LX/DG4;->a:LX/DG5;

    iput-object v1, v8, LX/DG5;->a:LX/DGK;

    .line 1979812
    iget-object v8, v7, LX/DG4;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v8, p0}, Ljava/util/BitSet;->set(I)V

    .line 1979813
    move-object v7, v7

    .line 1979814
    iget-object v8, v7, LX/DG4;->a:LX/DG5;

    iput-object v2, v8, LX/DG5;->b:LX/DG8;

    .line 1979815
    iget-object v8, v7, LX/DG4;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v8, p0}, Ljava/util/BitSet;->set(I)V

    .line 1979816
    move-object v7, v7

    .line 1979817
    iget-object v8, v7, LX/DG4;->a:LX/DG5;

    iput-object v3, v8, LX/DG5;->c:LX/1Pd;

    .line 1979818
    iget-object v8, v7, LX/DG4;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v8, p0}, Ljava/util/BitSet;->set(I)V

    .line 1979819
    move-object v7, v7

    .line 1979820
    invoke-interface {v5, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    .line 1979821
    iget-object v5, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1979822
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 p2, 0x2

    const/4 v2, 0x0

    .line 1979823
    invoke-static {v5, v2, v2}, LX/1WY;->a(Lcom/facebook/graphql/model/GraphQLStory;ZZ)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1979824
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    iget-object p0, v0, LX/DGS;->c:LX/1WX;

    invoke-virtual {p0, p1}, LX/1WX;->c(LX/1De;)LX/1XJ;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/1XJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1XJ;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    .line 1979825
    :goto_0
    move-object v5, v8

    .line 1979826
    invoke-interface {v7, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v7

    const v8, 0x7f0a0160

    invoke-virtual {v7, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v8, 0x7f0b0f2a

    invoke-interface {v7, v9, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    const/4 v8, 0x4

    invoke-interface {v7, v8}, LX/1Di;->b(I)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b0f2b

    invoke-interface {v7, v8}, LX/1Di;->q(I)LX/1Di;

    move-result-object v7

    invoke-interface {v5, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    iget-object v5, v0, LX/DGS;->d:LX/Anw;

    invoke-virtual {v5, p1}, LX/Anw;->c(LX/1De;)LX/Anv;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/Anv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Anv;

    move-result-object v8

    move-object v5, v3

    check-cast v5, LX/1Pr;

    invoke-virtual {v8, v5}, LX/Anv;->a(LX/1Pr;)LX/Anv;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v8

    invoke-virtual {v5, v8}, LX/Anv;->a(Z)LX/Anv;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v8

    invoke-virtual {v5, v8}, LX/Anv;->b(Z)LX/Anv;

    move-result-object v5

    invoke-static {v4}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    invoke-virtual {v5, v4}, LX/Anv;->c(Z)LX/Anv;

    move-result-object v4

    sget-object v5, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    invoke-virtual {v4, v5}, LX/Anv;->a(LX/1Wk;)LX/Anv;

    move-result-object v4

    .line 1979827
    iget v5, v1, LX/DGK;->d:I

    move v5, v5

    .line 1979828
    invoke-virtual {v4, v5}, LX/Anv;->h(I)LX/Anv;

    move-result-object v4

    .line 1979829
    new-instance v5, LX/DGR;

    invoke-direct {v5, v0, v6, v3}, LX/DGR;-><init>(LX/DGS;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pd;)V

    move-object v5, v5

    .line 1979830
    invoke-virtual {v4, v5}, LX/Anv;->a(LX/20Z;)LX/Anv;

    move-result-object v4

    invoke-interface {v7, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1979831
    return-object v0

    :cond_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    const p0, 0x7f0b0fe1

    invoke-interface {v8, p0}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    const p2, 0x7f08199c

    invoke-virtual {p0, p2}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1ne;->t(I)LX/1ne;

    move-result-object p0

    const p2, -0x6e685d

    invoke-virtual {p0, p2}, LX/1ne;->m(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b004e

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1979832
    invoke-static {}, LX/1dS;->b()V

    .line 1979833
    const/4 v0, 0x0

    return-object v0
.end method
