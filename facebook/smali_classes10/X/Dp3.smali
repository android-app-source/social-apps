.class public LX/Dp3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DoO;


# instance fields
.field private final a:LX/2Ox;

.field private final b:LX/DoZ;

.field private final c:LX/Dp2;


# direct methods
.method public constructor <init>(LX/2Ox;LX/DoZ;LX/Dp2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2043086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2043087
    iput-object p1, p0, LX/Dp3;->a:LX/2Ox;

    .line 2043088
    iput-object p2, p0, LX/Dp3;->b:LX/DoZ;

    .line 2043089
    iput-object p3, p0, LX/Dp3;->c:LX/Dp2;

    .line 2043090
    return-void
.end method

.method public static b(LX/0QB;)LX/Dp3;
    .locals 4

    .prologue
    .line 2043084
    new-instance v3, LX/Dp3;

    invoke-static {p0}, LX/2Ox;->a(LX/0QB;)LX/2Ox;

    move-result-object v0

    check-cast v0, LX/2Ox;

    invoke-static {p0}, LX/DoZ;->a(LX/0QB;)LX/DoZ;

    move-result-object v1

    check-cast v1, LX/DoZ;

    invoke-static {p0}, LX/Dp2;->a(LX/0QB;)LX/Dp2;

    move-result-object v2

    check-cast v2, LX/Dp2;

    invoke-direct {v3, v0, v1, v2}, LX/Dp3;-><init>(LX/2Ox;LX/DoZ;LX/Dp2;)V

    .line 2043085
    return-object v3
.end method


# virtual methods
.method public final declared-synchronized a()LX/Eaf;
    .locals 1

    .prologue
    .line 2043083
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Dp3;->c:LX/Dp2;

    invoke-virtual {v0}, LX/Dp2;->a()LX/Eaf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/Eap;LX/Eae;)V
    .locals 3

    .prologue
    .line 2043079
    iget-object v0, p0, LX/Dp3;->a:LX/2Ox;

    .line 2043080
    iget-object v1, p1, LX/Eap;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2043081
    invoke-virtual {p2}, LX/Eae;->b()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2Ox;->a(Ljava/lang/String;[B)V

    .line 2043082
    return-void
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 2043078
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Dp3;->c:LX/Dp2;

    invoke-virtual {v0}, LX/Dp2;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(LX/Eap;LX/Eae;)Z
    .locals 2

    .prologue
    .line 2043070
    iget-object v0, p0, LX/Dp3;->b:LX/DoZ;

    .line 2043071
    iget-object v1, p1, LX/Eap;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2043072
    invoke-virtual {v0, v1}, LX/DoZ;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 2043073
    if-nez v0, :cond_0

    .line 2043074
    const/4 v0, 0x1

    .line 2043075
    :goto_0
    return v0

    .line 2043076
    :cond_0
    invoke-virtual {p2}, LX/Eae;->b()[B

    move-result-object v1

    .line 2043077
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method
