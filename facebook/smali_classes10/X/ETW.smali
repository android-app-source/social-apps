.class public final LX/ETW;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;)V
    .locals 1

    .prologue
    .line 2125138
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2125139
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/ETW;->a:Ljava/lang/ref/WeakReference;

    .line 2125140
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 2125141
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 2125142
    :cond_0
    :goto_0
    return-void

    .line 2125143
    :pswitch_0
    iget-object v0, p0, LX/ETW;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;

    .line 2125144
    if-eqz v0, :cond_0

    .line 2125145
    iget-object v1, v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->j:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    .line 2125146
    :goto_1
    iget-object v1, v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->j:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2125147
    if-eqz v1, :cond_1

    .line 2125148
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    .line 2125149
    iget-object p0, v0, Lcom/facebook/video/videohome/data/VideoHomeVideoPrefetchController;->c:LX/3DC;

    sget-object p1, LX/379;->VIDEO_HOME:LX/379;

    invoke-virtual {p0, v1, p1}, LX/3DC;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    goto :goto_1

    .line 2125150
    :cond_1
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
