.class public LX/DxP;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DxW;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11R;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Sa;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wM;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2062777
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2062778
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2062779
    iput-object v0, p0, LX/DxP;->a:LX/0Ot;

    .line 2062780
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2062781
    iput-object v0, p0, LX/DxP;->b:LX/0Ot;

    .line 2062782
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2062783
    iput-object v0, p0, LX/DxP;->c:LX/0Ot;

    .line 2062784
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2062785
    iput-object v0, p0, LX/DxP;->d:LX/0Ot;

    .line 2062786
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2062787
    iput-object v0, p0, LX/DxP;->e:LX/0Ot;

    .line 2062788
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2062789
    iput-object v0, p0, LX/DxP;->f:LX/0Ot;

    .line 2062790
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, LX/DxP;

    const/16 v3, 0x19e

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2ea9

    invoke-static {p1, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2e4

    invoke-static {p1, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2feb

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x5c8

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v0, 0xdf4

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, LX/DxP;->a:LX/0Ot;

    iput-object v4, v2, LX/DxP;->b:LX/0Ot;

    iput-object v5, v2, LX/DxP;->c:LX/0Ot;

    iput-object v6, v2, LX/DxP;->d:LX/0Ot;

    iput-object v7, v2, LX/DxP;->e:LX/0Ot;

    iput-object p1, v2, LX/DxP;->f:LX/0Ot;

    .line 2062791
    const v0, 0x7f030ef2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2062792
    return-void
.end method

.method private static a(LX/DxP;ZLcom/facebook/graphql/model/GraphQLAlbum;)Z
    .locals 4

    .prologue
    .line 2062793
    if-eqz p1, :cond_0

    invoke-static {p2}, LX/DwG;->b(Lcom/facebook/graphql/model/GraphQLAlbum;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DxP;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->gZ:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setAlbumDetailsText(LX/DxP;Lcom/facebook/graphql/model/GraphQLAlbum;)V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2062794
    const v0, 0x7f0d2464

    invoke-virtual {p0, v0}, LX/DxP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2062795
    const v1, 0x7f0d2465

    invoke-virtual {p0, v1}, LX/DxP;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2062796
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2062797
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-ne v2, v6, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->l()Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v3

    .line 2062798
    :goto_0
    if-eqz v2, :cond_0

    .line 2062799
    invoke-virtual {p0}, LX/DxP;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f0f0086

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    invoke-virtual {v2, v6, v7, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2062800
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2062801
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2062802
    invoke-virtual {p0}, LX/DxP;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p1, v2}, LX/9bt;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 2062803
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2062804
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->r()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-eqz v2, :cond_2

    .line 2062805
    iget-object v2, p0, LX/DxP;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11R;

    sget-object v3, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->r()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-virtual {v2, v3, v6, v7}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    .line 2062806
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2062807
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2062808
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v2

    .line 2062809
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2062810
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getCurrentTextColor()I

    move-result v2

    const/4 v10, 0x0

    .line 2062811
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 2062812
    invoke-virtual {p0}, LX/DxP;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f0812dc

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2062813
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 2062814
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v6

    .line 2062815
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v3

    move-object v7, v3

    move-object v8, v6

    .line 2062816
    :goto_1
    iget-object v3, p0, LX/DxP;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0wM;

    iget-object v6, p0, LX/DxP;->d:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/8Sa;

    sget-object v9, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v8, v9}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v6

    invoke-virtual {v3, v6, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/Drawable;

    .line 2062817
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v3, v10, v10, v6, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2062818
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, v7}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2062819
    new-instance v7, LX/34T;

    const/4 v8, 0x2

    invoke-direct {v7, v3, v8}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v8, 0x11

    invoke-virtual {v6, v7, v10, v3, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2062820
    move-object v2, v6

    .line 2062821
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2062822
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x5

    if-ge v2, v3, :cond_5

    .line 2062823
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2062824
    const-string v0, " \u00b7 "

    invoke-static {v0, v5}, LX/0YN;->a(Ljava/lang/CharSequence;Ljava/util/Collection;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2062825
    :goto_2
    return-void

    :cond_4
    move v2, v4

    .line 2062826
    goto/16 :goto_0

    .line 2062827
    :cond_5
    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2062828
    const-string v2, " \u00b7 "

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    invoke-interface {v5, v4, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/CharSequence;Ljava/util/Collection;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2062829
    const-string v0, " \u00b7 "

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v5, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v2}, LX/0YN;->a(Ljava/lang/CharSequence;Ljava/util/Collection;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_6
    move-object v7, v3

    move-object v8, v6

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLAlbum;ZJZLcom/facebook/ipc/composer/intent/ComposerTargetData;)V
    .locals 9
    .param p6    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 2062830
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2062831
    const v0, 0x7f0d0510

    invoke-virtual {p0, v0}, LX/DxP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2062832
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2062833
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2062834
    :goto_0
    invoke-static {p0, p1}, LX/DxP;->setAlbumDetailsText(LX/DxP;Lcom/facebook/graphql/model/GraphQLAlbum;)V

    .line 2062835
    const v0, 0x7f0d2466

    invoke-virtual {p0, v0}, LX/DxP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;

    .line 2062836
    invoke-virtual {v0, p1}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)V

    .line 2062837
    const v0, 0x7f0d0511

    invoke-virtual {p0, v0}, LX/DxP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2062838
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2062839
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2062840
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2062841
    :goto_1
    const v0, 0x7f0d246e

    invoke-virtual {p0, v0}, LX/DxP;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2062842
    const v0, 0x7f0d246f

    invoke-virtual {p0, v0}, LX/DxP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2062843
    invoke-static {p0, p2, p1}, LX/DxP;->a(LX/DxP;ZLcom/facebook/graphql/model/GraphQLAlbum;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2062844
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2062845
    :goto_2
    return-void

    .line 2062846
    :cond_0
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2062847
    :cond_1
    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    .line 2062848
    :cond_2
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2062849
    invoke-virtual {v3, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setVisibility(I)V

    .line 2062850
    iget-object v0, p0, LX/DxP;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DxW;

    iget-object v0, p0, LX/DxP;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2062851
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v5, v2

    .line 2062852
    move-object v2, p1

    move-object v4, p6

    move-wide v6, p3

    move v8, p5

    invoke-virtual/range {v1 .. v8}, LX/DxW;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/resources/ui/FbTextView;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/String;JZ)V

    goto :goto_2
.end method
