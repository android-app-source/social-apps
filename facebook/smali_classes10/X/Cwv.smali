.class public LX/Cwv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/NullStateModuleCollectionUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:J

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1951340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951341
    const/4 v0, 0x0

    iput v0, p0, LX/Cwv;->c:I

    .line 1951342
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cwv;->a:Ljava/util/List;

    .line 1951343
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/model/NullStateModuleCollectionUnit;)V
    .locals 2

    .prologue
    .line 1951344
    iget-object v0, p0, LX/Cwv;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951345
    iget v0, p0, LX/Cwv;->c:I

    .line 1951346
    iget v1, p1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->h:I

    move v1, v1

    .line 1951347
    add-int/2addr v0, v1

    iput v0, p0, LX/Cwv;->c:I

    .line 1951348
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1951349
    iget-object v0, p0, LX/Cwv;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cwv;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1951350
    iget-object v0, p0, LX/Cwv;->a:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
