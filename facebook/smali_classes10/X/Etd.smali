.class public final LX/Etd;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/FriendsCenterHomeFragment;)V
    .locals 0

    .prologue
    .line 2178354
    iput-object p1, p0, LX/Etd;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 6

    .prologue
    .line 2178355
    iget-object v0, p0, LX/Etd;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->v:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5P0;

    .line 2178356
    iget-object v1, p0, LX/Etd;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->h:LX/Eum;

    iget-object v2, v0, LX/5P0;->analyticsTag:Ljava/lang/String;

    iget-object v3, p0, LX/Etd;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    iget-object v3, v3, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->u:LX/5P0;

    iget-object v3, v3, LX/5P0;->analyticsTag:Ljava/lang/String;

    .line 2178357
    iget-object v4, v1, LX/Eum;->a:LX/0Zb;

    sget-object v5, LX/Eul;->FRIENDS_CENTER_TAB_SELECTED:LX/Eul;

    invoke-static {v1, v5}, LX/Eum;->a(LX/Eum;LX/Eul;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "tab"

    invoke-virtual {v5, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "ref_tab"

    invoke-virtual {v5, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2178358
    iget-object v1, p0, LX/Etd;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v2, v0, LX/5P0;->analyticsTag:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(Ljava/lang/String;)V

    .line 2178359
    iget-object v1, p0, LX/Etd;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gh;

    iget-object v2, p0, LX/Etd;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0gh;->a(Landroid/support/v4/app/Fragment;Landroid/content/Context;)V

    .line 2178360
    iget-object v1, p0, LX/Etd;->a:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    .line 2178361
    iput-object v0, v1, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->u:LX/5P0;

    .line 2178362
    return-void
.end method
