.class public LX/E2K;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/3U9;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E2K;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E2M;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E2K",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E2M;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072626
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2072627
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E2K;->b:LX/0Zi;

    .line 2072628
    iput-object p1, p0, LX/E2K;->a:LX/0Ot;

    .line 2072629
    return-void
.end method

.method public static a(LX/0QB;)LX/E2K;
    .locals 4

    .prologue
    .line 2072630
    sget-object v0, LX/E2K;->c:LX/E2K;

    if-nez v0, :cond_1

    .line 2072631
    const-class v1, LX/E2K;

    monitor-enter v1

    .line 2072632
    :try_start_0
    sget-object v0, LX/E2K;->c:LX/E2K;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072633
    if-eqz v2, :cond_0

    .line 2072634
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072635
    new-instance v3, LX/E2K;

    const/16 p0, 0x30bd

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E2K;-><init>(LX/0Ot;)V

    .line 2072636
    move-object v0, v3

    .line 2072637
    sput-object v0, LX/E2K;->c:LX/E2K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072638
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072639
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072640
    :cond_1
    sget-object v0, LX/E2K;->c:LX/E2K;

    return-object v0

    .line 2072641
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 6

    .prologue
    .line 2072643
    check-cast p2, LX/E2J;

    .line 2072644
    iget-object v0, p0, LX/E2K;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2M;

    iget-object v2, p2, LX/E2J;->e:Ljava/lang/String;

    iget-object v3, p2, LX/E2J;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v4, p2, LX/E2J;->b:LX/1Pq;

    iget-object v5, p2, LX/E2J;->c:LX/5sc;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/E2M;->onClick(Landroid/view/View;Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pq;LX/5sc;)V

    .line 2072645
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2072646
    const v0, 0x127ef37d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2072647
    check-cast p2, LX/E2J;

    .line 2072648
    iget-object v0, p0, LX/E2K;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2M;

    iget-object v2, p2, LX/E2J;->a:LX/9uY;

    iget-object v3, p2, LX/E2J;->b:LX/1Pq;

    iget-object v4, p2, LX/E2J;->c:LX/5sc;

    iget-object v5, p2, LX/E2J;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-object v1, p1

    .line 2072649
    invoke-static {v3, v4, v5}, LX/E2M;->a(LX/1Pq;LX/5sc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v7

    .line 2072650
    invoke-interface {v2}, LX/9uY;->W()Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;

    move-result-object p0

    .line 2072651
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    if-eqz v7, :cond_0

    const v6, 0x7f0a00d1

    :goto_0
    invoke-virtual {p1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 2072652
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    if-eqz v7, :cond_1

    const v6, 0x7f08178a

    :goto_1
    invoke-virtual {p2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2072653
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 p2, 0x2

    invoke-interface {v7, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    const/4 p2, 0x1

    invoke-interface {v7, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    invoke-static {p0}, LX/CgW;->a(Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;)I

    move-result p2

    invoke-interface {v7, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    .line 2072654
    iget-object p2, v0, LX/E2M;->b:LX/1vg;

    invoke-virtual {p2, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p2

    const v3, 0x7f020781

    invoke-virtual {p2, v3}, LX/2xv;->h(I)LX/2xv;

    move-result-object p2

    invoke-virtual {p2, p1}, LX/2xv;->i(I)LX/2xv;

    move-result-object p2

    invoke-virtual {p2}, LX/1n6;->b()LX/1dc;

    move-result-object p2

    .line 2072655
    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2}, LX/1X5;->c()LX/1Di;

    move-result-object p2

    invoke-static {p0}, LX/CgW;->b(Lcom/facebook/graphql/enums/GraphQLReactionCoreButtonGlyphAlignment;)I

    move-result v3

    const v4, 0x7f0b163d

    invoke-interface {p2, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object p2

    const v3, 0x7f0b1631

    invoke-interface {p2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object p2

    const v3, 0x7f0b1631

    invoke-interface {p2, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object p2

    move-object p0, p2

    .line 2072656
    invoke-interface {v7, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v2}, LX/9uY;->c()Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;

    move-result-object p0

    .line 2072657
    iget-object p2, v0, LX/E2M;->a:LX/E2B;

    invoke-virtual {p2, v1}, LX/E2B;->c(LX/1De;)LX/E29;

    move-result-object p2

    invoke-virtual {p2, v6}, LX/E29;->a(Ljava/lang/CharSequence;)LX/E29;

    move-result-object p2

    invoke-virtual {p2, p1}, LX/E29;->h(I)LX/E29;

    move-result-object p2

    invoke-virtual {p2, p0}, LX/E29;->a(Lcom/facebook/reaction/protocol/corecomponents/ReactionCoreComponentSpecsGraphQLModels$ReactionCoreComponentTextSpecFieldsModel;)LX/E29;

    move-result-object p2

    invoke-virtual {p2}, LX/1X5;->c()LX/1Di;

    move-result-object p2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {p2, v2}, LX/1Di;->a(F)LX/1Di;

    move-result-object p2

    move-object v6, p2

    .line 2072658
    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    .line 2072659
    const v7, 0x127ef37d

    const/4 p0, 0x0

    invoke-static {v1, v7, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2072660
    invoke-interface {v6, v7}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2072661
    return-object v0

    .line 2072662
    :cond_0
    const v6, 0x7f0a010e

    goto/16 :goto_0

    .line 2072663
    :cond_1
    const v6, 0x7f081788

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2072664
    invoke-static {}, LX/1dS;->b()V

    .line 2072665
    iget v0, p1, LX/1dQ;->b:I

    .line 2072666
    packed-switch v0, :pswitch_data_0

    .line 2072667
    :goto_0
    return-object v2

    .line 2072668
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2072669
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/E2K;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x127ef37d
        :pswitch_0
    .end packed-switch
.end method
