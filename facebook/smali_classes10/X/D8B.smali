.class public LX/D8B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D17;
.implements LX/2mz;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static t:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/D15;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/D1U;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/view/ViewGroup;

.field public e:Landroid/content/Context;

.field public f:Z

.field public g:Landroid/view/ViewGroup$MarginLayoutParams;

.field public h:Lcom/facebook/android/maps/MapView;

.field public i:Lcom/facebook/fbui/glyph/GlyphView;

.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/fbui/glyph/GlyphView;

.field public l:Landroid/view/View;

.field public m:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public n:I

.field public o:LX/697;

.field public p:LX/D0y;

.field public q:Landroid/view/ViewGroup;

.field private r:LX/D8S;

.field private s:LX/68u;


# direct methods
.method public constructor <init>(LX/D0y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1968159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1968160
    iput-object p1, p0, LX/D8B;->p:LX/D0y;

    .line 1968161
    return-void
.end method

.method public static a(LX/0QB;)LX/D8B;
    .locals 6

    .prologue
    .line 1968146
    const-class v1, LX/D8B;

    monitor-enter v1

    .line 1968147
    :try_start_0
    sget-object v0, LX/D8B;->t:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1968148
    sput-object v2, LX/D8B;->t:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1968149
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1968150
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1968151
    new-instance p0, LX/D8B;

    invoke-static {v0}, LX/D0y;->b(LX/0QB;)LX/D0y;

    move-result-object v3

    check-cast v3, LX/D0y;

    invoke-direct {p0, v3}, LX/D8B;-><init>(LX/D0y;)V

    .line 1968152
    invoke-static {v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(LX/0QB;)Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    move-result-object v3

    check-cast v3, Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-static {v0}, LX/D15;->a(LX/0QB;)LX/D15;

    move-result-object v4

    check-cast v4, LX/D15;

    invoke-static {v0}, LX/D1U;->a(LX/0QB;)LX/D1U;

    move-result-object v5

    check-cast v5, LX/D1U;

    .line 1968153
    iput-object v3, p0, LX/D8B;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iput-object v4, p0, LX/D8B;->b:LX/D15;

    iput-object v5, p0, LX/D8B;->c:LX/D1U;

    .line 1968154
    move-object v0, p0

    .line 1968155
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1968156
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D8B;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1968157
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1968158
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/D8B;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 4
    .param p2    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 1968136
    iget-object v0, p0, LX/D8B;->s:LX/68u;

    if-eqz v0, :cond_0

    .line 1968137
    iget-object v0, p0, LX/D8B;->s:LX/68u;

    invoke-virtual {v0}, LX/68u;->g()V

    .line 1968138
    :cond_0
    iget-object v0, p0, LX/D8B;->g:Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, LX/68u;->a(II)LX/68u;

    move-result-object v0

    iput-object v0, p0, LX/D8B;->s:LX/68u;

    .line 1968139
    iget-object v0, p0, LX/D8B;->s:LX/68u;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, LX/68u;->a(J)LX/68u;

    .line 1968140
    iget-object v0, p0, LX/D8B;->s:LX/68u;

    new-instance v1, LX/D8A;

    invoke-direct {v1, p0}, LX/D8A;-><init>(LX/D8B;)V

    invoke-virtual {v0, v1}, LX/68u;->a(LX/67q;)V

    .line 1968141
    iget-object v0, p0, LX/D8B;->s:LX/68u;

    invoke-virtual {v0}, LX/68u;->f()V

    .line 1968142
    iget-object v0, p0, LX/D8B;->r:LX/D8S;

    if-eqz v0, :cond_1

    .line 1968143
    iget-object v0, p0, LX/D8B;->r:LX/D8S;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/D8S;->a(F)Z

    .line 1968144
    :cond_1
    iget-object v0, p0, LX/D8B;->k:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1968145
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 10

    .prologue
    .line 1968129
    const-string v0, "page_set_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/D8B;->j:Ljava/lang/String;

    .line 1968130
    const-string v0, "east"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 1968131
    const-string v2, "north"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 1968132
    const-string v4, "south"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 1968133
    const-string v6, "west"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 1968134
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v8

    new-instance v9, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v9, v2, v3, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v8, v9}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v2

    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v3, v4, v5, v0, v1}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v3}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    invoke-virtual {v0}, LX/696;->a()LX/697;

    move-result-object v0

    iput-object v0, p0, LX/D8B;->o:LX/697;

    .line 1968135
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1968128
    const/4 v0, 0x0

    return v0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1968113
    iget-object v0, p0, LX/D8B;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1968114
    iget-object v0, p0, LX/D8B;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1968115
    iget-object v0, p0, LX/D8B;->g:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1968116
    iget-object v0, p0, LX/D8B;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1968117
    iput-object v1, p0, LX/D8B;->d:Landroid/view/ViewGroup;

    .line 1968118
    :cond_0
    iput-object v1, p0, LX/D8B;->e:Landroid/content/Context;

    .line 1968119
    iput-object v1, p0, LX/D8B;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968120
    iput-boolean v2, p0, LX/D8B;->f:Z

    .line 1968121
    iput-object v1, p0, LX/D8B;->h:Lcom/facebook/android/maps/MapView;

    .line 1968122
    iput-object v1, p0, LX/D8B;->i:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1968123
    iput-object v1, p0, LX/D8B;->k:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1968124
    iput-object v1, p0, LX/D8B;->l:Landroid/view/View;

    .line 1968125
    iput-object v1, p0, LX/D8B;->q:Landroid/view/ViewGroup;

    .line 1968126
    iput-object v1, p0, LX/D8B;->r:LX/D8S;

    .line 1968127
    return-void
.end method

.method public final a(ILandroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D8b;LX/D8V;ILX/D8S;)V
    .locals 3
    .param p9    # LX/D8S;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/D8b;",
            "Lcom/facebook/video/watchandmore/core/OnExitWatchAndMoreListener;",
            "I",
            "Lcom/facebook/video/watchandmore/core/WatchAndMoreContentAnimationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1968044
    iput-object p2, p0, LX/D8B;->d:Landroid/view/ViewGroup;

    .line 1968045
    iput-object p4, p0, LX/D8B;->e:Landroid/content/Context;

    .line 1968046
    iget-object v0, p5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1968047
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {p5, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/D8B;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968048
    iput-object p9, p0, LX/D8B;->r:LX/D8S;

    .line 1968049
    const/4 v0, 0x0

    iput-object v0, p0, LX/D8B;->s:LX/68u;

    .line 1968050
    iput p8, p0, LX/D8B;->n:I

    .line 1968051
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D8B;->f:Z

    .line 1968052
    iget-object v0, p0, LX/D8B;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D8B;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968053
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1968054
    if-nez v0, :cond_1

    .line 1968055
    :cond_0
    :goto_0
    return-void

    .line 1968056
    :cond_1
    iget-object v0, p0, LX/D8B;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968057
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1968058
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1968059
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1968060
    iget-object v1, p0, LX/D8B;->c:LX/D1U;

    iget-object v2, p0, LX/D8B;->e:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1968061
    if-eqz v0, :cond_0

    .line 1968062
    invoke-direct {p0, v0}, LX/D8B;->a(Landroid/content/Intent;)V

    .line 1968063
    iget-object v0, p0, LX/D8B;->e:Landroid/content/Context;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968064
    iget-object v0, p0, LX/D8B;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968065
    iget-object v0, p0, LX/D8B;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1968066
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1968067
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1968068
    const/4 p3, 0x0

    .line 1968069
    iget-object v0, p0, LX/D8B;->e:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1968070
    if-nez v0, :cond_2

    .line 1968071
    :goto_1
    iget-object v0, p0, LX/D8B;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/D8B;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1968072
    goto :goto_0

    .line 1968073
    :cond_2
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03160d

    iget-object v2, p0, LX/D8B;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, LX/D8B;->q:Landroid/view/ViewGroup;

    .line 1968074
    iget-object v0, p0, LX/D8B;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput-object v0, p0, LX/D8B;->g:Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1968075
    iget-object v0, p0, LX/D8B;->g:Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, p0, LX/D8B;->n:I

    invoke-virtual {v0, p3, v1, p3, p3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1968076
    iget-object v0, p0, LX/D8B;->q:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 1968077
    iget-object v0, p0, LX/D8B;->q:Landroid/view/ViewGroup;

    const v1, 0x7f0d3192

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/MapView;

    iput-object v0, p0, LX/D8B;->h:Lcom/facebook/android/maps/MapView;

    .line 1968078
    iget-object v0, p0, LX/D8B;->h:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->requestLayout()V

    .line 1968079
    iget-object v0, p0, LX/D8B;->h:Lcom/facebook/android/maps/MapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/MapView;->a(Landroid/os/Bundle;)V

    .line 1968080
    iget-object v0, p0, LX/D8B;->q:Landroid/view/ViewGroup;

    const v1, 0x7f0d144a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/D8B;->i:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1968081
    iget-object v0, p0, LX/D8B;->q:Landroid/view/ViewGroup;

    const v1, 0x7f0d182f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/D8B;->k:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1968082
    iget-object v0, p0, LX/D8B;->q:Landroid/view/ViewGroup;

    const v1, 0x7f0d3193

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 1968083
    iget-object v1, p0, LX/D8B;->q:Landroid/view/ViewGroup;

    const v2, 0x7f0d1830

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/D8B;->l:Landroid/view/View;

    .line 1968084
    iget-object v1, p0, LX/D8B;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1caa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 1968085
    iget-object v2, p0, LX/D8B;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    new-instance p1, LX/D1Q;

    iget-object p2, p0, LX/D8B;->e:Landroid/content/Context;

    invoke-direct {p1, p2}, LX/D1Q;-><init>(Landroid/content/Context;)V

    iget-object p2, p0, LX/D8B;->j:Ljava/lang/String;

    .line 1968086
    iput-object p2, p1, LX/D1Q;->c:Ljava/lang/String;

    .line 1968087
    move-object p1, p1

    .line 1968088
    sget-object p2, LX/D1T;->WATCH_AND_LOCAL:LX/D1T;

    .line 1968089
    iput-object p2, p1, LX/D1Q;->e:LX/D1T;

    .line 1968090
    move-object p1, p1

    .line 1968091
    iput-object v0, p1, LX/D1Q;->h:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 1968092
    move-object v0, p1

    .line 1968093
    iget-object p1, p0, LX/D8B;->o:LX/697;

    .line 1968094
    iput-object p1, v0, LX/D1Q;->f:LX/697;

    .line 1968095
    move-object v0, v0

    .line 1968096
    iget-object p1, p0, LX/D8B;->l:Landroid/view/View;

    .line 1968097
    iput-object p1, v0, LX/D1Q;->g:Landroid/view/View;

    .line 1968098
    move-object v0, v0

    .line 1968099
    iput-boolean p3, v0, LX/D1Q;->i:Z

    .line 1968100
    move-object v0, v0

    .line 1968101
    iput v1, v0, LX/D1Q;->j:F

    .line 1968102
    move-object v0, v0

    .line 1968103
    invoke-virtual {v0}, LX/D1Q;->a()LX/D1R;

    move-result-object v0

    iget-object v1, p0, LX/D8B;->b:LX/D15;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(LX/D1R;LX/D14;)V

    .line 1968104
    iget-object v0, p0, LX/D8B;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 1968105
    iput-object p0, v0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->y:LX/D17;

    .line 1968106
    iget-object v0, p0, LX/D8B;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1968107
    iget-object v0, p0, LX/D8B;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1968108
    :cond_3
    iget-object v0, p0, LX/D8B;->h:Lcom/facebook/android/maps/MapView;

    iget-object v1, p0, LX/D8B;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 1968109
    iget-object v0, p0, LX/D8B;->l:Landroid/view/View;

    new-instance v1, LX/D86;

    invoke-direct {v1, p0}, LX/D86;-><init>(LX/D8B;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1968110
    iget-object v0, p0, LX/D8B;->k:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/D87;

    invoke-direct {v1, p0}, LX/D87;-><init>(LX/D8B;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1968111
    iget-object v0, p0, LX/D8B;->i:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/D89;

    invoke-direct {v1, p0}, LX/D89;-><init>(LX/D8B;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1968112
    goto/16 :goto_1
.end method

.method public final a(LX/680;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1968019
    iget-object v0, p1, LX/680;->E:LX/68Q;

    move-object v0, v0

    .line 1968020
    invoke-virtual {v0, v1}, LX/68Q;->b(Z)V

    .line 1968021
    iget-object v0, p1, LX/680;->E:LX/68Q;

    move-object v0, v0

    .line 1968022
    iput-boolean v1, v0, LX/68Q;->b:Z

    .line 1968023
    iget-object v0, p0, LX/D8B;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 1968024
    iget-boolean v1, v0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->v:Z

    move v0, v1

    .line 1968025
    if-eqz v0, :cond_0

    .line 1968026
    iget-object v0, p0, LX/D8B;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->c()V

    .line 1968027
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;I)V
    .locals 0

    .prologue
    .line 1968043
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1968042
    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1968031
    invoke-static {p2}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1968032
    if-eqz v0, :cond_0

    .line 1968033
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1968034
    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 1968035
    :goto_0
    return v0

    .line 1968036
    :cond_1
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1968037
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1968038
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 1968039
    goto :goto_0

    .line 1968040
    :cond_3
    iget-object v2, p0, LX/D8B;->c:LX/D1U;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1968041
    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final c()LX/D8g;
    .locals 1

    .prologue
    .line 1968030
    sget-object v0, LX/D8g;->WATCH_AND_LOCAL:LX/D8g;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1968029
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/D7g;
    .locals 1

    .prologue
    .line 1968028
    const/4 v0, 0x0

    return-object v0
.end method
