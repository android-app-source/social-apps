.class public LX/EOg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/EOj;


# direct methods
.method public constructor <init>(LX/EOj;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2114973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2114974
    iput-object p1, p0, LX/EOg;->a:LX/EOj;

    .line 2114975
    return-void
.end method

.method public static a(LX/1De;)LX/1Dh;
    .locals 2

    .prologue
    .line 2114987
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/EOg;
    .locals 4

    .prologue
    .line 2114976
    const-class v1, LX/EOg;

    monitor-enter v1

    .line 2114977
    :try_start_0
    sget-object v0, LX/EOg;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114978
    sput-object v2, LX/EOg;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114979
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114980
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114981
    new-instance p0, LX/EOg;

    invoke-static {v0}, LX/EOj;->a(LX/0QB;)LX/EOj;

    move-result-object v3

    check-cast v3, LX/EOj;

    invoke-direct {p0, v3}, LX/EOg;-><init>(LX/EOj;)V

    .line 2114982
    move-object v0, p0

    .line 2114983
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114984
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114985
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114986
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
