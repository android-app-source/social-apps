.class public final LX/DjR;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$ProductItemFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;)V
    .locals 0

    .prologue
    .line 2033272
    iput-object p1, p0, LX/DjR;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2033270
    iget-object v0, p0, LX/DjR;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->f:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08003c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2033271
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2033257
    check-cast p1, LX/0Px;

    .line 2033258
    iget-object v0, p0, LX/DjR;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    .line 2033259
    iput-object p1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->i:LX/0Px;

    .line 2033260
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2033261
    :cond_0
    iget-object v0, p0, LX/DjR;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2033262
    iget-object v0, p0, LX/DjR;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    .line 2033263
    iget-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->h:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 2033264
    if-eqz v1, :cond_1

    const p0, 0x7f0d2c29

    invoke-virtual {v1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 2033265
    const p0, 0x7f0d2c29

    invoke-virtual {v1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2033266
    :cond_1
    :goto_0
    return-void

    .line 2033267
    :cond_2
    iget-object v0, p0, LX/DjR;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->d:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    iget-object v1, p0, LX/DjR;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->i:LX/0Px;

    .line 2033268
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;->b:LX/0Px;

    .line 2033269
    iget-object v0, p0, LX/DjR;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/PageServiceSelectorFragment;->d:Lcom/facebook/messaging/professionalservices/booking/ui/PageServiceSelectorAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method
