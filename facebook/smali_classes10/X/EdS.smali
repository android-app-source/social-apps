.class public LX/EdS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public a:I

.field public b:[B


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 2149472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2149473
    if-nez p2, :cond_0

    .line 2149474
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "EncodedStringValue: Text-string is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149475
    :cond_0
    iput p1, p0, LX/EdS;->a:I

    .line 2149476
    :try_start_0
    invoke-static {p1}, LX/EdP;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, LX/EdS;->b:[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2149477
    :goto_0
    return-void

    .line 2149478
    :catch_0
    move-exception v0

    .line 2149479
    const-string v1, "EncodedStringValue"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input encoding "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must be supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2149480
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, LX/EdS;->b:[B

    goto :goto_0
.end method

.method public constructor <init>(I[B)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2149481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2149482
    if-nez p2, :cond_0

    .line 2149483
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "EncodedStringValue: Text-string is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149484
    :cond_0
    iput p1, p0, LX/EdS;->a:I

    .line 2149485
    array-length v0, p2

    new-array v0, v0, [B

    iput-object v0, p0, LX/EdS;->b:[B

    .line 2149486
    iget-object v0, p0, LX/EdS;->b:[B

    array-length v1, p2

    invoke-static {p2, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2149487
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2149488
    const/16 v0, 0x6a

    invoke-direct {p0, v0, p1}, LX/EdS;-><init>(ILjava/lang/String;)V

    .line 2149489
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 2149490
    const/16 v0, 0x6a

    invoke-direct {p0, v0, p1}, LX/EdS;-><init>(I[B)V

    .line 2149491
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2149492
    iget v0, p0, LX/EdS;->a:I

    return v0
.end method

.method public final a([B)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2149493
    if-nez p1, :cond_0

    .line 2149494
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "EncodedStringValue: Text-string is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149495
    :cond_0
    array-length v0, p1

    new-array v0, v0, [B

    iput-object v0, p0, LX/EdS;->b:[B

    .line 2149496
    iget-object v0, p0, LX/EdS;->b:[B

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2149497
    return-void
.end method

.method public final b([B)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2149498
    if-nez p1, :cond_0

    .line 2149499
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Text-string is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149500
    :cond_0
    iget-object v0, p0, LX/EdS;->b:[B

    if-nez v0, :cond_1

    .line 2149501
    array-length v0, p1

    new-array v0, v0, [B

    iput-object v0, p0, LX/EdS;->b:[B

    .line 2149502
    iget-object v0, p0, LX/EdS;->b:[B

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2149503
    :goto_0
    return-void

    .line 2149504
    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2149505
    :try_start_0
    iget-object v1, p0, LX/EdS;->b:[B

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2149506
    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2149507
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, LX/EdS;->b:[B

    goto :goto_0

    .line 2149508
    :catch_0
    move-exception v0

    .line 2149509
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 2149510
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "appendTextString: failed when write a new Text-string"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2149511
    iget-object v0, p0, LX/EdS;->b:[B

    array-length v0, v0

    new-array v0, v0, [B

    .line 2149512
    iget-object v1, p0, LX/EdS;->b:[B

    iget-object v2, p0, LX/EdS;->b:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2149513
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2149514
    iget v0, p0, LX/EdS;->a:I

    if-nez v0, :cond_0

    .line 2149515
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/EdS;->b:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 2149516
    :goto_0
    return-object v0

    .line 2149517
    :cond_0
    :try_start_0
    iget v0, p0, LX/EdS;->a:I

    invoke-static {v0}, LX/EdP;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 2149518
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, LX/EdS;->b:[B

    invoke-direct {v0, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2149519
    :catch_0
    :try_start_1
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/EdS;->b:[B

    const-string v2, "iso-8859-1"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 2149520
    :catch_1
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/EdS;->b:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public final clone()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2149521
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2149522
    iget-object v0, p0, LX/EdS;->b:[B

    array-length v0, v0

    .line 2149523
    new-array v1, v0, [B

    .line 2149524
    iget-object v2, p0, LX/EdS;->b:[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2149525
    :try_start_0
    new-instance v0, LX/EdS;

    iget v2, p0, LX/EdS;->a:I

    invoke-direct {v0, v2, v1}, LX/EdS;-><init>(I[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 2149526
    :catch_0
    move-exception v0

    .line 2149527
    const-string v1, "EncodedStringValue"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "failed to clone an EncodedStringValue: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149528
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2149529
    new-instance v1, Ljava/lang/CloneNotSupportedException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
