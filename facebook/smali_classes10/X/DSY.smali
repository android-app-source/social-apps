.class public LX/DSY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWf;
.implements LX/DSX;


# instance fields
.field public a:LX/EWL;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/DSe;",
            "LX/0Pz",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/DSe;",
            "Ljava/util/ArrayList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1999708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1999709
    sget-object v0, LX/EWL;->a:LX/EWL;

    iput-object v0, p0, LX/DSY;->a:LX/EWL;

    .line 1999710
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/DSY;->b:Ljava/util/Map;

    .line 1999711
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/DSY;->c:Ljava/util/Map;

    .line 1999712
    invoke-direct {p0}, LX/DSY;->d()V

    .line 1999713
    return-void
.end method

.method private static a(LX/0Pz;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/EWI;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1999639
    new-instance v0, LX/DSr;

    invoke-direct {v0}, LX/DSr;-><init>()V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1999640
    new-instance v1, LX/EWI;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, LX/EWI;-><init>(LX/0Px;Z)V

    invoke-virtual {p0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1999641
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1999704
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1999705
    invoke-static {v0}, LX/DSY;->a(LX/0Pz;)V

    .line 1999706
    new-instance v1, LX/EWL;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EWL;-><init>(LX/0Px;)V

    iput-object v1, p0, LX/DSY;->a:LX/EWL;

    .line 1999707
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1999701
    iget-object v0, p0, LX/DSY;->a:LX/EWL;

    .line 1999702
    iget p0, v0, LX/EWL;->d:I

    move v0, p0

    .line 1999703
    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1999700
    iget-object v0, p0, LX/DSY;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;LX/0P1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/Enum;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1999651
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_4

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DSf;

    .line 1999652
    invoke-virtual {v0}, LX/DSf;->a()LX/DSa;

    move-result-object v1

    sget-object v5, LX/DSa;->GroupMemberRow:LX/DSa;

    if-ne v1, v5, :cond_3

    .line 1999653
    iget-object v1, v0, LX/DSf;->c:LX/DSe;

    move-object v5, v1

    .line 1999654
    iget-object v1, p0, LX/DSY;->b:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1999655
    iget-object v1, p0, LX/DSY;->b:Ljava/util/Map;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1999656
    sget-object v1, LX/DSe;->SELF:LX/DSe;

    if-eq v5, v1, :cond_1

    .line 1999657
    iget-object v1, p0, LX/DSY;->b:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pz;

    new-instance v6, LX/DSW;

    .line 1999658
    const v7, 0x7f082fd0

    .line 1999659
    sget-object v8, LX/DSe;->FRIEND:LX/DSe;

    if-ne v5, v8, :cond_9

    .line 1999660
    const v7, 0x7f082fd2

    .line 1999661
    :cond_0
    :goto_1
    move v7, v7

    .line 1999662
    invoke-direct {v6, v7}, LX/DSW;-><init>(I)V

    invoke-virtual {v1, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1999663
    :cond_1
    iget-object v1, p0, LX/DSY;->c:Ljava/util/Map;

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1999664
    :cond_2
    iget-object v1, p0, LX/DSY;->c:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1999665
    iget-object v6, v0, LX/DSf;->d:LX/DUV;

    move-object v6, v6

    .line 1999666
    invoke-interface {v6}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1999667
    iget-object v1, p0, LX/DSY;->b:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pz;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1999668
    iget-object v1, p0, LX/DSY;->c:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 1999669
    iget-object v6, v0, LX/DSf;->d:LX/DUV;

    move-object v6, v6

    .line 1999670
    invoke-interface {v6}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1999671
    sget-object v1, LX/DSe;->SELF:LX/DSe;

    if-ne v5, v1, :cond_3

    invoke-virtual {v0}, LX/DSf;->l()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1999672
    iget-object v0, p0, LX/DSY;->b:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    new-instance v1, LX/DSk;

    invoke-direct {v1}, LX/DSk;-><init>()V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1999673
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 1999674
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1999675
    invoke-static {}, LX/DSe;->values()[LX/DSe;

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_8

    aget-object v2, v4, v1

    .line 1999676
    iget-object v0, p0, LX/DSY;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1999677
    iget-object v0, p0, LX/DSY;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1999678
    const/4 v6, 0x0

    .line 1999679
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    .line 1999680
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    instance-of v8, v8, LX/DSW;

    if-eqz v8, :cond_5

    .line 1999681
    add-int/lit8 v7, v7, -0x1

    .line 1999682
    :cond_5
    if-lez v7, :cond_6

    const/4 v6, 0x1

    :cond_6
    move v6, v6

    .line 1999683
    if-eqz v6, :cond_7

    .line 1999684
    new-instance v6, LX/EWI;

    const/4 v7, 0x1

    invoke-direct {v6, v0, v7}, LX/EWI;-><init>(LX/0Px;Z)V

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1999685
    if-eqz p2, :cond_7

    invoke-virtual {p2, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p2, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1999686
    new-instance v0, LX/DSg;

    invoke-direct {v0, v2}, LX/DSg;-><init>(LX/DSe;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1999687
    new-instance v6, LX/EWI;

    const/4 v7, 0x0

    invoke-direct {v6, v0, v7}, LX/EWI;-><init>(LX/0Px;Z)V

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1999688
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1999689
    :cond_8
    invoke-static {v3}, LX/DSY;->a(LX/0Pz;)V

    .line 1999690
    new-instance v0, LX/EWL;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EWL;-><init>(LX/0Px;)V

    iput-object v0, p0, LX/DSY;->a:LX/EWL;

    .line 1999691
    return-void

    .line 1999692
    :cond_9
    sget-object v8, LX/DSe;->INVITES:LX/DSe;

    if-ne v5, v8, :cond_a

    .line 1999693
    const v7, 0x7f082fd1

    goto/16 :goto_1

    .line 1999694
    :cond_a
    sget-object v8, LX/DSe;->ADMIN:LX/DSe;

    if-ne v5, v8, :cond_b

    .line 1999695
    const v7, 0x7f082fd3

    goto/16 :goto_1

    .line 1999696
    :cond_b
    sget-object v8, LX/DSe;->MODERATOR:LX/DSe;

    if-ne v5, v8, :cond_c

    .line 1999697
    const v7, 0x7f082fd4

    goto/16 :goto_1

    .line 1999698
    :cond_c
    sget-object v8, LX/DSe;->COMMUNITY_MEMBER:LX/DSe;

    if-ne v5, v8, :cond_0

    .line 1999699
    const v7, 0x7f082fd5

    goto/16 :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1999650
    invoke-static {}, LX/DSa;->values()[LX/DSa;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 1999649
    invoke-virtual {p0, p1}, LX/DSY;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DSV;

    invoke-interface {v0}, LX/DSV;->a()LX/DSa;

    move-result-object v0

    invoke-virtual {v0}, LX/DSa;->ordinal()I

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1999646
    invoke-direct {p0}, LX/DSY;->d()V

    .line 1999647
    iget-object v0, p0, LX/DSY;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1999648
    return-void
.end method

.method public final c(I)Z
    .locals 1

    .prologue
    .line 1999645
    iget-object v0, p0, LX/DSY;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->c(I)Z

    move-result v0

    return v0
.end method

.method public final getPositionForSection(I)I
    .locals 1

    .prologue
    .line 1999644
    iget-object v0, p0, LX/DSY;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public final getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 1999643
    iget-object v0, p0, LX/DSY;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1999642
    iget-object v0, p0, LX/DSY;->a:LX/EWL;

    invoke-virtual {v0}, LX/EWL;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final v_(I)Z
    .locals 1

    .prologue
    .line 1999638
    iget-object v0, p0, LX/DSY;->a:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->v_(I)Z

    move-result v0

    return v0
.end method
