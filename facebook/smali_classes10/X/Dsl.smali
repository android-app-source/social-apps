.class public LX/Dsl;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/pages/common/friendinviter/protocol/FetchFriendsYouMayInviteMethod$Params;",
        "Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteInterfaces$FriendsYouMayInviteQuery$FriendsYouMayInvite;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2050662
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2050663
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2050664
    const-class v0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;

    .line 2050665
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->a()Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2050666
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid JSON result"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2050667
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel;->a()Lcom/facebook/pages/common/friendinviter/protocol/FriendsYouMayInviteModels$FriendsYouMayInviteQueryModel$FriendsYouMayInviteModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2050668
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 2050669
    check-cast p1, Lcom/facebook/pages/common/friendinviter/protocol/FetchFriendsYouMayInviteMethod$Params;

    .line 2050670
    new-instance v0, LX/Dsm;

    invoke-direct {v0}, LX/Dsm;-><init>()V

    move-object v0, v0

    .line 2050671
    const-string v1, "page_id"

    .line 2050672
    iget-object v2, p1, Lcom/facebook/pages/common/friendinviter/protocol/FetchFriendsYouMayInviteMethod$Params;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2050673
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_image_size"

    .line 2050674
    iget v2, p1, Lcom/facebook/pages/common/friendinviter/protocol/FetchFriendsYouMayInviteMethod$Params;->b:I

    move v2, v2

    .line 2050675
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "num_friends_you_may_invite"

    .line 2050676
    iget v2, p1, Lcom/facebook/pages/common/friendinviter/protocol/FetchFriendsYouMayInviteMethod$Params;->c:I

    move v2, v2

    .line 2050677
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
