.class public LX/EkU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/support/annotation/WorkerThread;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[Ljava/lang/String;

.field private final c:[Ljava/lang/String;

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "LX/EkT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2163540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163541
    new-instance v0, Ljava/util/WeakHashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/WeakHashMap;-><init>(I)V

    iput-object v0, p0, LX/EkU;->e:Ljava/util/WeakHashMap;

    .line 2163542
    iput-object p1, p0, LX/EkU;->a:Ljava/lang/String;

    .line 2163543
    iput-object p2, p0, LX/EkU;->b:[Ljava/lang/String;

    .line 2163544
    iput-object p3, p0, LX/EkU;->c:[Ljava/lang/String;

    .line 2163545
    iput-object p4, p0, LX/EkU;->d:Ljava/lang/String;

    .line 2163546
    return-void
.end method

.method private static c(LX/EkU;Landroid/database/sqlite/SQLiteDatabase;)LX/EkT;
    .locals 2
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2163468
    iget-object v0, p0, LX/EkU;->e:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EkT;

    .line 2163469
    if-nez v0, :cond_0

    .line 2163470
    new-instance v0, LX/EkT;

    invoke-direct {v0}, LX/EkT;-><init>()V

    .line 2163471
    iget-object v1, p0, LX/EkU;->e:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2163472
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 5
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2163473
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/EkU;->c(LX/EkU;Landroid/database/sqlite/SQLiteDatabase;)LX/EkT;

    move-result-object v3

    .line 2163474
    iget-object v1, v3, LX/EkT;->b:Landroid/database/sqlite/SQLiteStatement;

    if-nez v1, :cond_4

    .line 2163475
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2163476
    const-string v1, "INSERT"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163477
    const-string v1, " INTO "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163478
    iget-object v1, p0, LX/EkU;->a:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163479
    const/16 v1, 0x28

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v2, v0

    .line 2163480
    :goto_0
    iget-object v1, p0, LX/EkU;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v2, v1, :cond_1

    .line 2163481
    if-lez v2, :cond_0

    const-string v1, ","

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163482
    iget-object v1, p0, LX/EkU;->c:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163483
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2163484
    :cond_0
    const-string v1, ""

    goto :goto_1

    .line 2163485
    :cond_1
    const/16 v1, 0x29

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2163486
    const-string v1, " VALUES ("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    .line 2163487
    :goto_2
    iget-object v0, p0, LX/EkU;->c:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 2163488
    if-lez v1, :cond_2

    const-string v0, ",?"

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163489
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2163490
    :cond_2
    const-string v0, "?"

    goto :goto_3

    .line 2163491
    :cond_3
    const/16 v0, 0x29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2163492
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, v3, LX/EkT;->b:Landroid/database/sqlite/SQLiteStatement;

    .line 2163493
    :cond_4
    iget-object v0, v3, LX/EkT;->b:Landroid/database/sqlite/SQLiteStatement;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 2163494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2163495
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/EkU;->c(LX/EkU;Landroid/database/sqlite/SQLiteDatabase;)LX/EkT;

    move-result-object v1

    .line 2163496
    const/4 v0, 0x0

    .line 2163497
    array-length v3, p2

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 2163498
    aget-object p1, p2, v2

    if-nez p1, :cond_0

    .line 2163499
    const/4 p1, 0x1

    shl-int/2addr p1, v2

    or-int/2addr v0, p1

    .line 2163500
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2163501
    :cond_1
    move v2, v0

    .line 2163502
    iget-object v0, v1, LX/EkT;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2163503
    if-nez v0, :cond_5

    .line 2163504
    iget-object v0, p0, LX/EkU;->a:Ljava/lang/String;

    iget-object v3, p0, LX/EkU;->b:[Ljava/lang/String;

    .line 2163505
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2163506
    const-string p1, "SELECT _id"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163507
    const-string p1, " FROM "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163508
    const-string p1, " WHERE "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163509
    const/4 p1, 0x0

    :goto_1
    array-length v0, v3

    if-ge p1, v0, :cond_4

    .line 2163510
    if-lez p1, :cond_2

    .line 2163511
    const-string v0, " AND "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163512
    :cond_2
    aget-object v0, v3, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163513
    aget-object v0, p2, p1

    if-nez v0, :cond_3

    .line 2163514
    const-string v0, " IS NULL"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163515
    :goto_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 2163516
    :cond_3
    const-string v0, "=?"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 2163517
    :cond_4
    const-string p1, " LIMIT 1"

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163518
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 2163519
    iget-object v1, v1, LX/EkT;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2163520
    :cond_5
    monitor-exit p0

    return-object v0

    .line 2163521
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Landroid/database/sqlite/SQLiteDatabase;)Landroid/database/sqlite/SQLiteStatement;
    .locals 4
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2163522
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1}, LX/EkU;->c(LX/EkU;Landroid/database/sqlite/SQLiteDatabase;)LX/EkT;

    move-result-object v2

    .line 2163523
    iget-object v0, v2, LX/EkT;->c:Landroid/database/sqlite/SQLiteStatement;

    if-nez v0, :cond_2

    .line 2163524
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2163525
    const-string v0, "UPDATE "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163526
    iget-object v0, p0, LX/EkU;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163527
    const-string v0, " SET "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163528
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EkU;->c:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 2163529
    if-lez v1, :cond_0

    const-string v0, ","

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163530
    iget-object v0, p0, LX/EkU;->c:[Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163531
    const-string v0, "=?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163532
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2163533
    :cond_0
    const-string v0, ""

    goto :goto_1

    .line 2163534
    :cond_1
    const-string v0, " WHERE "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163535
    const-string v0, "_id"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163536
    const-string v0, "=?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163537
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, v2, LX/EkT;->c:Landroid/database/sqlite/SQLiteStatement;

    .line 2163538
    :cond_2
    iget-object v0, v2, LX/EkT;->c:Landroid/database/sqlite/SQLiteStatement;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 2163539
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
