.class public final LX/E5g;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E5h;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public final synthetic b:LX/E5h;


# direct methods
.method public constructor <init>(LX/E5h;)V
    .locals 1

    .prologue
    .line 2078762
    iput-object p1, p0, LX/E5g;->b:LX/E5h;

    .line 2078763
    move-object v0, p1

    .line 2078764
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2078765
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2078766
    const-string v0, "ReactionFigHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2078767
    if-ne p0, p1, :cond_1

    .line 2078768
    :cond_0
    :goto_0
    return v0

    .line 2078769
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2078770
    goto :goto_0

    .line 2078771
    :cond_3
    check-cast p1, LX/E5g;

    .line 2078772
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2078773
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2078774
    if-eq v2, v3, :cond_0

    .line 2078775
    iget-object v2, p0, LX/E5g;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/E5g;->a:Ljava/lang/String;

    iget-object v3, p1, LX/E5g;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2078776
    goto :goto_0

    .line 2078777
    :cond_4
    iget-object v2, p1, LX/E5g;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
