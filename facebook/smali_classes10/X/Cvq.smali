.class public LX/Cvq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fu;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Cvq;


# instance fields
.field public final a:LX/11i;

.field private final b:LX/0So;


# direct methods
.method public constructor <init>(LX/11i;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1949673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1949674
    iput-object p1, p0, LX/Cvq;->a:LX/11i;

    .line 1949675
    iput-object p2, p0, LX/Cvq;->b:LX/0So;

    .line 1949676
    return-void
.end method

.method public static a(LX/0QB;)LX/Cvq;
    .locals 5

    .prologue
    .line 1949660
    sget-object v0, LX/Cvq;->c:LX/Cvq;

    if-nez v0, :cond_1

    .line 1949661
    const-class v1, LX/Cvq;

    monitor-enter v1

    .line 1949662
    :try_start_0
    sget-object v0, LX/Cvq;->c:LX/Cvq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1949663
    if-eqz v2, :cond_0

    .line 1949664
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1949665
    new-instance p0, LX/Cvq;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/Cvq;-><init>(LX/11i;LX/0So;)V

    .line 1949666
    move-object v0, p0

    .line 1949667
    sput-object v0, LX/Cvq;->c:LX/Cvq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1949668
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1949669
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1949670
    :cond_1
    sget-object v0, LX/Cvq;->c:LX/Cvq;

    return-object v0

    .line 1949671
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1949672
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Cvq;LX/0Pq;LX/0Px;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/sequencelogger/SequenceDefinition;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1949658
    iget-object v0, p0, LX/Cvq;->a:LX/11i;

    const-string v1, "results_display_styles"

    invoke-virtual {p2}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "initial_fetch"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v1

    iget-object v2, p0, LX/Cvq;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-interface {v0, p1, v1, v2, v3}, LX/11i;->a(LX/0Pq;LX/0P1;J)LX/11o;

    .line 1949659
    return-void
.end method

.method public static a(LX/Cvq;LX/0Pq;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1949638
    iget-object v0, p0, LX/Cvq;->a:LX/11i;

    invoke-interface {v0, p1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1949639
    if-nez v0, :cond_0

    .line 1949640
    :goto_0
    return-void

    .line 1949641
    :cond_0
    const v1, 0x100ac7c6

    invoke-static {v0, p2, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method

.method public static a(LX/Cvq;LX/0Pq;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1949652
    iget-object v0, p0, LX/Cvq;->a:LX/11i;

    invoke-interface {v0, p1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    .line 1949653
    if-nez v2, :cond_0

    .line 1949654
    :goto_0
    return-void

    .line 1949655
    :cond_0
    if-eqz p3, :cond_1

    .line 1949656
    const-string v0, "canceled"

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    .line 1949657
    :goto_1
    const v3, 0x44881a6e

    invoke-static {v2, p2, v1, v0, v3}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static b(LX/Cvq;LX/0Pq;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1949650
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/Cvq;->a(LX/Cvq;LX/0Pq;Ljava/lang/String;Z)V

    .line 1949651
    return-void
.end method

.method public static g(LX/Cvq;)V
    .locals 2

    .prologue
    .line 1949648
    sget-object v0, LX/Cvx;->a:LX/Cvv;

    const-string v1, "post_processing"

    invoke-static {p0, v0, v1}, LX/Cvq;->a(LX/Cvq;LX/0Pq;Ljava/lang/String;)V

    .line 1949649
    return-void
.end method


# virtual methods
.method public final e()V
    .locals 2

    .prologue
    .line 1949645
    iget-object v0, p0, LX/Cvq;->a:LX/11i;

    sget-object v1, LX/Cvx;->a:LX/Cvv;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1949646
    :goto_0
    return-void

    .line 1949647
    :cond_0
    iget-object v0, p0, LX/Cvq;->a:LX/11i;

    sget-object v1, LX/Cvx;->a:LX/Cvv;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    goto :goto_0
.end method

.method public final ko_()Z
    .locals 2

    .prologue
    .line 1949642
    sget-object v0, LX/Cvx;->a:LX/Cvv;

    const-string v1, "post_processing"

    invoke-static {p0, v0, v1}, LX/Cvq;->b(LX/Cvq;LX/0Pq;Ljava/lang/String;)V

    .line 1949643
    invoke-virtual {p0}, LX/Cvq;->e()V

    .line 1949644
    const/4 v0, 0x1

    return v0
.end method
