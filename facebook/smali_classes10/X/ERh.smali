.class public LX/ERh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/11H;

.field public final c:LX/ERS;

.field public final d:LX/ERQ;

.field public final e:LX/ERU;

.field public final f:LX/0dC;

.field public g:J

.field public h:J

.field public i:LX/2TL;

.field public j:LX/ERI;

.field public k:LX/ERH;

.field public l:LX/ERK;

.field public m:LX/2TI;

.field public n:LX/2TH;

.field public final o:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121293
    const-class v0, LX/ERh;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ERh;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/ERS;LX/ERQ;LX/ERU;LX/0dC;LX/2TL;LX/ERI;LX/ERH;LX/ERK;LX/2TI;LX/2TH;LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2121321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121322
    iput-object p1, p0, LX/ERh;->b:LX/11H;

    .line 2121323
    iput-object p2, p0, LX/ERh;->c:LX/ERS;

    .line 2121324
    iput-object p3, p0, LX/ERh;->d:LX/ERQ;

    .line 2121325
    iput-object p4, p0, LX/ERh;->e:LX/ERU;

    .line 2121326
    iput-object p5, p0, LX/ERh;->f:LX/0dC;

    .line 2121327
    iput-object p6, p0, LX/ERh;->i:LX/2TL;

    .line 2121328
    iput-object p7, p0, LX/ERh;->j:LX/ERI;

    .line 2121329
    iput-object p8, p0, LX/ERh;->k:LX/ERH;

    .line 2121330
    iput-object p9, p0, LX/ERh;->l:LX/ERK;

    .line 2121331
    iput-object p10, p0, LX/ERh;->m:LX/2TI;

    .line 2121332
    iput-object p11, p0, LX/ERh;->n:LX/2TH;

    .line 2121333
    iget-object v0, p0, LX/ERh;->i:LX/2TL;

    invoke-virtual {v0}, LX/2TL;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/ERh;->g:J

    .line 2121334
    iget-object v0, p0, LX/ERh;->j:LX/ERI;

    invoke-virtual {v0}, LX/ERI;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/ERh;->h:J

    .line 2121335
    iput-object p12, p0, LX/ERh;->o:LX/03V;

    .line 2121336
    return-void
.end method

.method public static b(LX/0QB;)LX/ERh;
    .locals 13

    .prologue
    .line 2121304
    new-instance v0, LX/ERh;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v1

    check-cast v1, LX/11H;

    .line 2121305
    new-instance v3, LX/ERS;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v2

    check-cast v2, LX/0lp;

    invoke-direct {v3, v2}, LX/ERS;-><init>(LX/0lp;)V

    .line 2121306
    move-object v2, v3

    .line 2121307
    check-cast v2, LX/ERS;

    .line 2121308
    new-instance v4, LX/ERQ;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v3

    check-cast v3, LX/0lp;

    invoke-direct {v4, v3}, LX/ERQ;-><init>(LX/0lp;)V

    .line 2121309
    move-object v3, v4

    .line 2121310
    check-cast v3, LX/ERQ;

    .line 2121311
    new-instance v4, LX/ERU;

    invoke-direct {v4}, LX/ERU;-><init>()V

    .line 2121312
    move-object v4, v4

    .line 2121313
    move-object v4, v4

    .line 2121314
    check-cast v4, LX/ERU;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v5

    check-cast v5, LX/0dC;

    invoke-static {p0}, LX/2TL;->a(LX/0QB;)LX/2TL;

    move-result-object v6

    check-cast v6, LX/2TL;

    invoke-static {p0}, LX/ERI;->a(LX/0QB;)LX/ERI;

    move-result-object v7

    check-cast v7, LX/ERI;

    invoke-static {p0}, LX/ERH;->a(LX/0QB;)LX/ERH;

    move-result-object v8

    check-cast v8, LX/ERH;

    invoke-static {p0}, LX/ERK;->a(LX/0QB;)LX/ERK;

    move-result-object v9

    check-cast v9, LX/ERK;

    invoke-static {p0}, LX/2TI;->a(LX/0QB;)LX/2TI;

    move-result-object v10

    check-cast v10, LX/2TI;

    invoke-static {p0}, LX/2TH;->a(LX/0QB;)LX/2TH;

    move-result-object v11

    check-cast v11, LX/2TH;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-direct/range {v0 .. v12}, LX/ERh;-><init>(LX/11H;LX/ERS;LX/ERQ;LX/ERU;LX/0dC;LX/2TL;LX/ERI;LX/ERH;LX/ERK;LX/2TI;LX/2TH;LX/03V;)V

    .line 2121315
    return-object v0
.end method

.method public static e(LX/ERh;)V
    .locals 5

    .prologue
    .line 2121316
    iget-object v0, p0, LX/ERh;->i:LX/2TL;

    iget-wide v2, p0, LX/ERh;->g:J

    .line 2121317
    iget-object v1, v0, LX/2TL;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2121318
    sget-object v4, LX/2TR;->i:LX/0Tn;

    invoke-interface {v1, v4, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 2121319
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2121320
    return-void
.end method


# virtual methods
.method public final a(JJ)V
    .locals 5

    .prologue
    .line 2121295
    new-instance v0, LX/ERV;

    invoke-direct {v0, p1, p2}, LX/ERV;-><init>(J)V

    .line 2121296
    const-wide/16 v2, 0x3e8

    div-long v2, p3, v2

    .line 2121297
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, LX/ERV;->b:Ljava/lang/Long;

    .line 2121298
    :try_start_0
    iget-object v1, p0, LX/ERh;->b:LX/11H;

    iget-object v2, p0, LX/ERh;->e:LX/ERU;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2121299
    :cond_0
    :goto_0
    return-void

    .line 2121300
    :catch_0
    move-exception v0

    .line 2121301
    sget-object v1, LX/ERh;->a:Ljava/lang/String;

    const-string v2, "updateDeviceLastSyncOnServer"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2121302
    instance-of v1, v0, LX/2Oo;

    if-eqz v1, :cond_0

    .line 2121303
    iget-object v1, p0, LX/ERh;->o:LX/03V;

    const-string v2, "vault_device_update_api exception"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 2121294
    iget-wide v0, p0, LX/ERh;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
