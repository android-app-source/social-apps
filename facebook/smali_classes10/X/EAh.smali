.class public LX/EAh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/EAh;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public final b:LX/BNP;

.field public final c:LX/Ch5;

.field private final d:LX/0kL;

.field public final e:LX/EB0;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/BNP;LX/EB0;LX/Ch5;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2085824
    iput-object p3, p0, LX/EAh;->e:LX/EB0;

    .line 2085825
    iput-object p1, p0, LX/EAh;->a:Landroid/content/res/Resources;

    .line 2085826
    iput-object p2, p0, LX/EAh;->b:LX/BNP;

    .line 2085827
    iput-object p4, p0, LX/EAh;->c:LX/Ch5;

    .line 2085828
    iput-object p5, p0, LX/EAh;->d:LX/0kL;

    .line 2085829
    return-void
.end method

.method public static a(LX/0QB;)LX/EAh;
    .locals 9

    .prologue
    .line 2085830
    sget-object v0, LX/EAh;->f:LX/EAh;

    if-nez v0, :cond_1

    .line 2085831
    const-class v1, LX/EAh;

    monitor-enter v1

    .line 2085832
    :try_start_0
    sget-object v0, LX/EAh;->f:LX/EAh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2085833
    if-eqz v2, :cond_0

    .line 2085834
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2085835
    new-instance v3, LX/EAh;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/BNP;->a(LX/0QB;)LX/BNP;

    move-result-object v5

    check-cast v5, LX/BNP;

    invoke-static {v0}, LX/EB0;->a(LX/0QB;)LX/EB0;

    move-result-object v6

    check-cast v6, LX/EB0;

    invoke-static {v0}, LX/Ch5;->a(LX/0QB;)LX/Ch5;

    move-result-object v7

    check-cast v7, LX/Ch5;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    invoke-direct/range {v3 .. v8}, LX/EAh;-><init>(Landroid/content/res/Resources;LX/BNP;LX/EB0;LX/Ch5;LX/0kL;)V

    .line 2085836
    move-object v0, v3

    .line 2085837
    sput-object v0, LX/EAh;->f:LX/EAh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2085838
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2085839
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2085840
    :cond_1
    sget-object v0, LX/EAh;->f:LX/EAh;

    return-object v0

    .line 2085841
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2085842
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/55r;Ljava/lang/String;Ljava/lang/String;ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14
    .param p8    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 2085843
    iget-object v0, p0, LX/EAh;->b:LX/BNP;

    invoke-static {p1}, LX/BNJ;->a(LX/55r;)I

    move-result v7

    invoke-static/range {p2 .. p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {p1}, LX/BNJ;->b(LX/55r;)Ljava/lang/String;

    move-result-object v11

    invoke-static {p1}, LX/BNJ;->c(LX/55r;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v12

    const/4 v13, 0x0

    move/from16 v1, p4

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v10, p3

    invoke-virtual/range {v0 .. v13}, LX/BNP;->a(ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)V

    .line 2085844
    return-void
.end method
