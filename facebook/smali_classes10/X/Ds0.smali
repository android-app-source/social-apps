.class public LX/Ds0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Drs;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/Ds0;


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/20i;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/3iS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/Drl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/2c4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/3iU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final i:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2049891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049892
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/Ds0;->i:Ljava/util/Random;

    .line 2049893
    return-void
.end method

.method public static a(LX/0QB;)LX/Ds0;
    .locals 11

    .prologue
    .line 2049894
    sget-object v0, LX/Ds0;->j:LX/Ds0;

    if-nez v0, :cond_1

    .line 2049895
    const-class v1, LX/Ds0;

    monitor-enter v1

    .line 2049896
    :try_start_0
    sget-object v0, LX/Ds0;->j:LX/Ds0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2049897
    if-eqz v2, :cond_0

    .line 2049898
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2049899
    new-instance v3, LX/Ds0;

    invoke-direct {v3}, LX/Ds0;-><init>()V

    .line 2049900
    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v6

    check-cast v6, LX/20i;

    invoke-static {v0}, LX/3iS;->b(LX/0QB;)LX/3iS;

    move-result-object v7

    check-cast v7, LX/3iS;

    invoke-static {v0}, LX/Drl;->b(LX/0QB;)LX/Drl;

    move-result-object v8

    check-cast v8, LX/Drl;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v10

    check-cast v10, LX/2c4;

    invoke-static {v0}, LX/3iU;->a(LX/0QB;)LX/3iU;

    move-result-object p0

    check-cast p0, LX/3iU;

    .line 2049901
    iput-object v4, v3, LX/Ds0;->a:Landroid/content/Context;

    iput-object v5, v3, LX/Ds0;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v6, v3, LX/Ds0;->c:LX/20i;

    iput-object v7, v3, LX/Ds0;->d:LX/3iS;

    iput-object v8, v3, LX/Ds0;->e:LX/Drl;

    iput-object v9, v3, LX/Ds0;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object v10, v3, LX/Ds0;->g:LX/2c4;

    iput-object p0, v3, LX/Ds0;->h:LX/3iU;

    .line 2049902
    move-object v0, v3

    .line 2049903
    sput-object v0, LX/Ds0;->j:LX/Ds0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049904
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2049905
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2049906
    :cond_1
    sget-object v0, LX/Ds0;->j:LX/Ds0;

    return-object v0

    .line 2049907
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2049908
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 2049865
    invoke-static {p1}, LX/3qL;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v0

    .line 2049866
    const-string v1, "notification_id_extra"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2049867
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "post_comment_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2049868
    iget-object v1, p0, LX/Ds0;->c:LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v12

    .line 2049869
    if-eqz v0, :cond_0

    if-nez v12, :cond_1

    .line 2049870
    :cond_0
    iget-object v1, p0, LX/Ds0;->g:LX/2c4;

    invoke-virtual {v1, v10, v4}, LX/2c4;->a(Ljava/lang/String;I)V

    .line 2049871
    :cond_1
    const-string v1, "reply_text_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2049872
    if-nez v3, :cond_2

    .line 2049873
    iget-object v0, p0, LX/Ds0;->g:LX/2c4;

    invoke-virtual {v0, v10, v4}, LX/2c4;->a(Ljava/lang/String;I)V

    .line 2049874
    :goto_0
    return-void

    .line 2049875
    :cond_2
    new-instance v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->LEGACY_API_POST_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move v5, v4

    move-object v7, v6

    move v8, v4

    move v9, v4

    invoke-direct/range {v0 .. v9}, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/facebook/ipc/media/MediaItem;Lcom/facebook/ipc/media/StickerItem;ZI)V

    .line 2049876
    iget-object v1, p0, LX/Ds0;->d:LX/3iS;

    invoke-virtual {v1, v11, v12, v0}, LX/3iS;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    .line 2049877
    invoke-static {}, Lcom/facebook/api/ufiservices/common/AddCommentParams;->a()LX/5Gr;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->a:Ljava/lang/String;

    .line 2049878
    iput-object v3, v2, LX/5Gr;->a:Ljava/lang/String;

    .line 2049879
    move-object v2, v2

    .line 2049880
    iget-object v3, v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->b:Ljava/lang/String;

    .line 2049881
    iput-object v3, v2, LX/5Gr;->b:Ljava/lang/String;

    .line 2049882
    move-object v2, v2

    .line 2049883
    iget-object v3, v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;->c:Ljava/lang/String;

    .line 2049884
    iput-object v3, v2, LX/5Gr;->c:Ljava/lang/String;

    .line 2049885
    move-object v2, v2

    .line 2049886
    iput-object v1, v2, LX/5Gr;->d:Lcom/facebook/graphql/model/GraphQLComment;

    .line 2049887
    move-object v1, v2

    .line 2049888
    iput-object v11, v1, LX/5Gr;->f:Ljava/lang/String;

    .line 2049889
    move-object v1, v1

    .line 2049890
    iget-object v2, p0, LX/Ds0;->h:LX/3iU;

    const/4 v3, 0x1

    invoke-virtual {v2, v11, v0, v1, v3}, LX/3iU;->a(Ljava/lang/String;Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;LX/5Gr;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/Drz;

    invoke-direct {v1, p0, p1, v10}, LX/Drz;-><init>(LX/Ds0;Landroid/content/Intent;Ljava/lang/String;)V

    iget-object v2, p0, LX/Ds0;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Drw;)LX/3pb;
    .locals 6

    .prologue
    .line 2049836
    iget-object v0, p1, LX/Drw;->c:Lorg/json/JSONObject;

    move-object v1, v0

    .line 2049837
    iget-object v2, p0, LX/Ds0;->a:Landroid/content/Context;

    .line 2049838
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v3, v0

    .line 2049839
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->COMMENT:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 2049840
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v0, v0

    .line 2049841
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v3, v4, v0}, Lcom/facebook/notifications/tray/actions/PushNotificationsActionService;->a(Landroid/content/Context;Lcom/facebook/notifications/model/SystemTrayNotification;Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2049842
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->STORY_FEEDBACK_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2049843
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->LEGACY_API_POST_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->LEGACY_API_POST_ID:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2049844
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_PRIMARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_PRIMARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2049845
    const-string v2, "redirect_intent"

    .line 2049846
    iget-object v3, p1, LX/Drw;->e:Landroid/content/Intent;

    move-object v3, v3

    .line 2049847
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2049848
    iget-object v2, p0, LX/Ds0;->a:Landroid/content/Context;

    iget-object v3, p0, LX/Ds0;->i:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v0, v4}, LX/0nt;->c(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2049849
    new-instance v2, LX/3qF;

    const-string v3, "reply_text_key"

    invoke-direct {v2, v3}, LX/3qF;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->INLINE_COMMENT_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2049850
    iput-object v3, v2, LX/3qF;->b:Ljava/lang/CharSequence;

    .line 2049851
    move-object v2, v2

    .line 2049852
    invoke-virtual {v2}, LX/3qF;->a()LX/3qL;

    move-result-object v2

    .line 2049853
    new-instance v3, LX/3pX;

    const v4, 0x7f021566

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v4, v1, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v3, v2}, LX/3pX;->a(LX/3qL;)LX/3pX;

    move-result-object v0

    invoke-virtual {v0}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    .line 2049854
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 2049855
    invoke-direct {p0, p1}, LX/Ds0;->b(Landroid/content/Intent;)V

    .line 2049856
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2049857
    :cond_0
    const-string v0, "notification_id_extra"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2049858
    const-string v0, "redirect_intent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 2049859
    const-string v2, "show_keyboard_on_first_load"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2049860
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2049861
    iget-object v2, p0, LX/Ds0;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Ds0;->a:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2049862
    iget-object v0, p0, LX/Ds0;->g:LX/2c4;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2c4;->a(Ljava/lang/String;I)V

    .line 2049863
    iget-object v0, p0, LX/Ds0;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2049864
    goto :goto_0
.end method
