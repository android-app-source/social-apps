.class public final enum LX/Cqd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cqd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cqd;

.field public static final enum MEDIA_ASPECT_FIT:LX/Cqd;

.field public static final enum MEDIA_FULLSCREEN:LX/Cqd;

.field public static final enum MEDIA_MATCH_FRAME:LX/Cqd;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1940087
    new-instance v0, LX/Cqd;

    const-string v1, "MEDIA_ASPECT_FIT"

    invoke-direct {v0, v1, v2}, LX/Cqd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    .line 1940088
    new-instance v0, LX/Cqd;

    const-string v1, "MEDIA_FULLSCREEN"

    invoke-direct {v0, v1, v3}, LX/Cqd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cqd;->MEDIA_FULLSCREEN:LX/Cqd;

    .line 1940089
    new-instance v0, LX/Cqd;

    const-string v1, "MEDIA_MATCH_FRAME"

    invoke-direct {v0, v1, v4}, LX/Cqd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cqd;->MEDIA_MATCH_FRAME:LX/Cqd;

    .line 1940090
    const/4 v0, 0x3

    new-array v0, v0, [LX/Cqd;

    sget-object v1, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    aput-object v1, v0, v2

    sget-object v1, LX/Cqd;->MEDIA_FULLSCREEN:LX/Cqd;

    aput-object v1, v0, v3

    sget-object v1, LX/Cqd;->MEDIA_MATCH_FRAME:LX/Cqd;

    aput-object v1, v0, v4

    sput-object v0, LX/Cqd;->$VALUES:[LX/Cqd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1940092
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cqd;
    .locals 1

    .prologue
    .line 1940093
    const-class v0, LX/Cqd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cqd;

    return-object v0
.end method

.method public static values()[LX/Cqd;
    .locals 1

    .prologue
    .line 1940091
    sget-object v0, LX/Cqd;->$VALUES:[LX/Cqd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cqd;

    return-object v0
.end method
