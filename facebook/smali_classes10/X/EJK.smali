.class public LX/EJK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2103819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2103820
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EJK;->a:Ljava/lang/String;

    .line 2103821
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->cc()Z

    move-result v0

    iput-boolean v0, p0, LX/EJK;->b:Z

    .line 2103822
    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)V
    .locals 1

    .prologue
    .line 2103813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2103814
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/EJK;->a:Ljava/lang/String;

    .line 2103815
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->w()Z

    move-result v0

    iput-boolean v0, p0, LX/EJK;->b:Z

    .line 2103816
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2103817
    iget-boolean v0, p0, LX/EJK;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2103818
    iget-object v0, p0, LX/EJK;->a:Ljava/lang/String;

    return-object v0
.end method
