.class public LX/DcP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/DcP;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018344
    iput-object p1, p0, LX/DcP;->a:LX/0tX;

    .line 2018345
    iput-object p2, p0, LX/DcP;->b:LX/1Ck;

    .line 2018346
    return-void
.end method

.method public static a(LX/0QB;)LX/DcP;
    .locals 5

    .prologue
    .line 2018347
    sget-object v0, LX/DcP;->c:LX/DcP;

    if-nez v0, :cond_1

    .line 2018348
    const-class v1, LX/DcP;

    monitor-enter v1

    .line 2018349
    :try_start_0
    sget-object v0, LX/DcP;->c:LX/DcP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2018350
    if-eqz v2, :cond_0

    .line 2018351
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2018352
    new-instance p0, LX/DcP;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {p0, v3, v4}, LX/DcP;-><init>(LX/0tX;LX/1Ck;)V

    .line 2018353
    move-object v0, p0

    .line 2018354
    sput-object v0, LX/DcP;->c:LX/DcP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2018355
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2018356
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2018357
    :cond_1
    sget-object v0, LX/DcP;->c:LX/DcP;

    return-object v0

    .line 2018358
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2018359
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2018360
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "task_key_load_categories"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
