.class public final LX/D5F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;)V
    .locals 0

    .prologue
    .line 1963454
    iput-object p1, p0, LX/D5F;->a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1963452
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1963455
    iget-object v0, p0, LX/D5F;->a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    iget-boolean v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c:Z

    if-nez v0, :cond_0

    .line 1963456
    iget-object v0, p0, LX/D5F;->a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1963457
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1963453
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1963449
    iget-object v0, p0, LX/D5F;->a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    iget-boolean v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->c:Z

    if-eqz v0, :cond_0

    .line 1963450
    iget-object v0, p0, LX/D5F;->a:Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedOverlayHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1963451
    :cond_0
    return-void
.end method
