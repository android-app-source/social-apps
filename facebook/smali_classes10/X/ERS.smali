.class public LX/ERS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/ERT;",
        "Lcom/facebook/vault/model/FacebookVaultDevice;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0lp;


# direct methods
.method public constructor <init>(LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2120902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120903
    iput-object p1, p0, LX/ERS;->a:LX/0lp;

    .line 2120904
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2120905
    check-cast p1, LX/ERT;

    .line 2120906
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2120907
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "identifier_value"

    .line 2120908
    iget-object v2, p1, LX/ERT;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2120909
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120910
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "name"

    .line 2120911
    iget-object v2, p1, LX/ERT;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2120912
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120913
    iget-object v0, p1, LX/ERT;->c:Ljava/lang/Boolean;

    move-object v0, v0

    .line 2120914
    if-eqz v0, :cond_0

    .line 2120915
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "enabled"

    .line 2120916
    iget-object v2, p1, LX/ERT;->c:Ljava/lang/Boolean;

    move-object v2, v2

    .line 2120917
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120918
    :cond_0
    iget-object v0, p1, LX/ERT;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2120919
    if-eqz v0, :cond_1

    .line 2120920
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "sync_mode"

    .line 2120921
    iget-object v2, p1, LX/ERT;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2120922
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120923
    :cond_1
    iget-object v0, p1, LX/ERT;->e:Ljava/lang/Boolean;

    move-object v0, v0

    .line 2120924
    if-eqz v0, :cond_2

    .line 2120925
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "sync_older_photos"

    .line 2120926
    iget-object v2, p1, LX/ERT;->e:Ljava/lang/Boolean;

    move-object v2, v2

    .line 2120927
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120928
    :cond_2
    new-instance v0, LX/14N;

    const-string v1, "vaultDeviceGet"

    const-string v2, "POST"

    const-string v3, "me/vaultdevices"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2120929
    iget-object v0, p0, LX/ERS;->a:LX/0lp;

    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 2120930
    const-class v1, Lcom/facebook/vault/model/FacebookVaultDevice;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/model/FacebookVaultDevice;

    .line 2120931
    return-object v0
.end method
