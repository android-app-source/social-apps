.class public final LX/EfT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/7h0;

.field public final synthetic b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;LX/7h0;)V
    .locals 0

    .prologue
    .line 2154595
    iput-object p1, p0, LX/EfT;->b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iput-object p2, p0, LX/EfT;->a:LX/7h0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x2

    const v1, 0x1d45d423

    invoke-static {v0, v5, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2154572
    iget-object v0, p0, LX/EfT;->b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iget-object v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->A:LX/0he;

    iget-object v1, p0, LX/EfT;->a:LX/7h0;

    .line 2154573
    iget-object v2, v1, LX/7h0;->d:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v2

    .line 2154574
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    iget-object v3, p0, LX/EfT;->a:LX/7h0;

    .line 2154575
    iget-boolean v4, v3, LX/7h0;->h:Z

    move v3, v4

    .line 2154576
    iget-object v4, p0, LX/EfT;->a:LX/7h0;

    .line 2154577
    iget-object v8, v4, LX/7h0;->a:LX/7gz;

    move-object v4, v8

    .line 2154578
    sget-object v8, LX/7gz;->SENT:LX/7gz;

    if-ne v4, v8, :cond_0

    move v4, v5

    :goto_0
    iget-object v8, p0, LX/EfT;->a:LX/7h0;

    .line 2154579
    iget-boolean p1, v8, LX/7h0;->l:Z

    move v8, p1

    .line 2154580
    if-nez v8, :cond_1

    .line 2154581
    :goto_1
    if-eqz v4, :cond_2

    .line 2154582
    sget-object v6, LX/7gW;->SENT_STATE:LX/7gW;

    invoke-virtual {v6}, LX/7gW;->getReason()Ljava/lang/String;

    move-result-object v6

    .line 2154583
    :goto_2
    iget-object v8, v0, LX/0he;->h:LX/7gT;

    .line 2154584
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2154585
    sget-object v3, LX/7gS;->TARGET_ID:LX/7gS;

    invoke-virtual {v3}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154586
    sget-object v3, LX/7gS;->ROW_INDEX:LX/7gS;

    invoke-virtual {v3}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2154587
    sget-object v3, LX/7gS;->REASON:LX/7gS;

    invoke-virtual {v3}, LX/7gS;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154588
    sget-object v3, LX/7gR;->OPEN_UNTAPPABLE_ROW:LX/7gR;

    invoke-static {v8, v3, v0}, LX/7gT;->a(LX/7gT;LX/7gR;Landroid/os/Bundle;)V

    .line 2154589
    iget-object v0, p0, LX/EfT;->b:Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;

    iget-object v0, v0, Lcom/facebook/audience/direct/ui/SnacksInboxItemViewHolder;->x:Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/audience/direct/ui/SnacksViewSwitcher;->a(J)V

    .line 2154590
    const v0, 0x507ec276

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    :cond_0
    move v4, v6

    .line 2154591
    goto :goto_0

    :cond_1
    move v5, v6

    goto :goto_1

    .line 2154592
    :cond_2
    if-eqz v3, :cond_3

    if-eqz v5, :cond_3

    .line 2154593
    sget-object v6, LX/7gW;->EXPIRE_STATE:LX/7gW;

    invoke-virtual {v6}, LX/7gW;->getReason()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 2154594
    :cond_3
    sget-object v6, LX/7gW;->SEEN_STATE:LX/7gW;

    invoke-virtual {v6}, LX/7gW;->getReason()Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method
