.class public LX/EtO;
.super Lcom/facebook/fbui/widget/text/ImageWithTextView;
.source ""

# interfaces
.implements LX/3uq;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:I

.field private static final b:[I

.field private static final c:[I


# instance fields
.field public d:LX/3uw;

.field private e:LX/3v3;

.field public f:Landroid/content/res/ColorStateList;

.field private g:Landroid/text/style/ForegroundColorSpan;

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 2177825
    sput v0, LX/EtO;->a:I

    .line 2177826
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/EtO;->b:[I

    .line 2177827
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101009f

    aput v2, v0, v1

    sput-object v0, LX/EtO;->c:[I

    return-void

    .line 2177828
    :array_0
    .array-data 4
        0x101009f
        0x10100a0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2177870
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;)V

    .line 2177871
    iput-boolean v0, p0, LX/EtO;->h:Z

    .line 2177872
    iput-boolean v0, p0, LX/EtO;->i:Z

    .line 2177873
    iput-boolean v0, p0, LX/EtO;->j:Z

    .line 2177874
    const/4 p1, 0x1

    .line 2177875
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, LX/EtO;->setGravity(I)V

    .line 2177876
    invoke-virtual {p0}, LX/EtO;->setSingleLine()V

    .line 2177877
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, LX/EtO;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2177878
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOrientation(I)V

    .line 2177879
    invoke-virtual {p0, p0}, LX/EtO;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2177880
    invoke-virtual {p0, p1}, LX/EtO;->setFocusable(Z)V

    .line 2177881
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/EtO;->setFocusableInTouchMode(Z)V

    .line 2177882
    return-void
.end method

.method public static a(LX/EtO;Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    .line 2177863
    iget-boolean v0, p0, LX/EtO;->j:Z

    if-eqz v0, :cond_1

    .line 2177864
    if-nez p1, :cond_0

    const-string p1, ""

    .line 2177865
    :cond_0
    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u25cf "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2177866
    iget-object v1, p0, LX/EtO;->g:Landroid/text/style/ForegroundColorSpan;

    const/4 v2, 0x0

    sget v3, LX/EtO;->a:I

    const/16 v4, 0x21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2177867
    invoke-virtual {p0, v0}, LX/EtO;->setText(Ljava/lang/CharSequence;)V

    .line 2177868
    :goto_0
    return-void

    .line 2177869
    :cond_1
    invoke-virtual {p0, p1}, LX/EtO;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 2177829
    if-gtz p1, :cond_0

    .line 2177830
    :goto_0
    return-void

    .line 2177831
    :cond_0
    invoke-virtual {p0}, LX/EtO;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/03r;->InlineActionButton:[I

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 2177832
    :try_start_0
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v6

    .line 2177833
    invoke-virtual {p0}, LX/EtO;->getPaddingLeft()I

    move-result v4

    .line 2177834
    invoke-virtual {p0}, LX/EtO;->getPaddingTop()I

    move-result v3

    .line 2177835
    invoke-virtual {p0}, LX/EtO;->getPaddingRight()I

    move-result v2

    .line 2177836
    invoke-virtual {p0}, LX/EtO;->getPaddingBottom()I

    move-result v1

    move v10, v0

    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v10

    .line 2177837
    :goto_1
    if-ge v4, v6, :cond_a

    .line 2177838
    invoke-virtual {v5, v4}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v7

    .line 2177839
    const/16 v8, 0x7

    if-ne v7, v8, :cond_2

    .line 2177840
    invoke-virtual {p0}, LX/EtO;->getContext()Landroid/content/Context;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v5, v7, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    invoke-virtual {p0, v8, v7}, LX/EtO;->setTextAppearance(Landroid/content/Context;I)V

    .line 2177841
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2177842
    :cond_2
    const/16 v8, 0x1

    if-ne v7, v8, :cond_3

    .line 2177843
    invoke-virtual {v5, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/EtO;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 2177844
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 2177845
    :cond_3
    :try_start_1
    const/16 v8, 0x6

    if-ne v7, v8, :cond_4

    .line 2177846
    invoke-virtual {v5, v7}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    .line 2177847
    iput-object v7, p0, LX/EtO;->f:Landroid/content/res/ColorStateList;

    .line 2177848
    goto :goto_2

    .line 2177849
    :cond_4
    const/16 v8, 0x8

    if-ne v7, v8, :cond_5

    .line 2177850
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    const/high16 v9, -0x1000000

    invoke-virtual {v5, v7, v9}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v7

    invoke-direct {v8, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v8, p0, LX/EtO;->g:Landroid/text/style/ForegroundColorSpan;

    goto :goto_2

    .line 2177851
    :cond_5
    const/16 v8, 0x2

    if-ne v7, v8, :cond_6

    .line 2177852
    const/4 v3, 0x0

    invoke-virtual {v5, v7, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    goto :goto_2

    .line 2177853
    :cond_6
    const/16 v8, 0x3

    if-ne v7, v8, :cond_7

    .line 2177854
    const/4 v2, 0x0

    invoke-virtual {v5, v7, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    goto :goto_2

    .line 2177855
    :cond_7
    const/16 v8, 0x4

    if-ne v7, v8, :cond_8

    .line 2177856
    const/4 v1, 0x0

    invoke-virtual {v5, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    goto :goto_2

    .line 2177857
    :cond_8
    const/16 v8, 0x5

    if-ne v7, v8, :cond_9

    .line 2177858
    const/4 v0, 0x0

    invoke-virtual {v5, v7, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    goto :goto_2

    .line 2177859
    :cond_9
    const/16 v8, 0x9

    if-ne v7, v8, :cond_1

    .line 2177860
    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    invoke-virtual {p0, v7}, LX/EtO;->setCompoundDrawablePadding(I)V

    goto :goto_2

    .line 2177861
    :cond_a
    invoke-virtual {p0, v3, v2, v1, v0}, LX/EtO;->setPadding(IIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2177862
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    goto/16 :goto_0
.end method

.method public final a(LX/3v3;I)V
    .locals 1

    .prologue
    .line 2177810
    iput-object p1, p0, LX/EtO;->e:LX/3v3;

    .line 2177811
    invoke-virtual {p1}, LX/3v3;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {p0, v0}, LX/EtO;->a(LX/EtO;Ljava/lang/CharSequence;)V

    .line 2177812
    invoke-virtual {p1}, LX/3v3;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EtO;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2177813
    invoke-virtual {p1}, LX/3v3;->isEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, LX/EtO;->setEnabled(Z)V

    .line 2177814
    invoke-virtual {p1}, LX/3v3;->isCheckable()Z

    move-result v0

    .line 2177815
    iget-boolean p2, p0, LX/EtO;->h:Z

    if-eq p2, v0, :cond_0

    .line 2177816
    iput-boolean v0, p0, LX/EtO;->h:Z

    .line 2177817
    invoke-virtual {p0}, LX/EtO;->refreshDrawableState()V

    .line 2177818
    invoke-virtual {p0}, LX/EtO;->invalidate()V

    .line 2177819
    :cond_0
    invoke-virtual {p1}, LX/3v3;->isChecked()Z

    move-result v0

    .line 2177820
    iget-boolean p1, p0, LX/EtO;->i:Z

    if-eq p1, v0, :cond_1

    .line 2177821
    iput-boolean v0, p0, LX/EtO;->i:Z

    .line 2177822
    invoke-virtual {p0}, LX/EtO;->refreshDrawableState()V

    .line 2177823
    invoke-virtual {p0}, LX/EtO;->invalidate()V

    .line 2177824
    :cond_1
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 2177804
    if-eqz p1, :cond_0

    .line 2177805
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 2177806
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2177807
    invoke-virtual {p0}, LX/EtO;->refreshDrawableState()V

    .line 2177808
    invoke-virtual {p0}, LX/EtO;->invalidate()V

    .line 2177809
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2177883
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 2177800
    invoke-virtual {p0}, LX/EtO;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2177801
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2177802
    invoke-virtual {p0}, LX/EtO;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2177803
    :cond_0
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\u25cf "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final canScrollHorizontally(I)Z
    .locals 1

    .prologue
    .line 2177799
    const/4 v0, 0x0

    return v0
.end method

.method public final drawableStateChanged()V
    .locals 4

    .prologue
    .line 2177791
    invoke-super {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->drawableStateChanged()V

    .line 2177792
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 2177793
    if-nez v0, :cond_0

    .line 2177794
    :goto_0
    return-void

    .line 2177795
    :cond_0
    iget-object v1, p0, LX/EtO;->f:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_1

    .line 2177796
    iget-object v1, p0, LX/EtO;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, LX/EtO;->getDrawableState()[I

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    .line 2177797
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v1, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0

    .line 2177798
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public final getItemData()LX/3v3;
    .locals 1

    .prologue
    .line 2177778
    iget-object v0, p0, LX/EtO;->e:LX/3v3;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x1d8089c8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2177788
    iget-object v1, p0, LX/EtO;->d:LX/3uw;

    if-eqz v1, :cond_0

    .line 2177789
    iget-object v1, p0, LX/EtO;->d:LX/3uw;

    iget-object v2, p0, LX/EtO;->e:LX/3v3;

    invoke-interface {v1, v2}, LX/3uw;->a(LX/3v3;)Z

    .line 2177790
    :cond_0
    const v1, -0x179d4d0c

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 2177781
    iget-boolean v0, p0, LX/EtO;->h:Z

    if-nez v0, :cond_0

    .line 2177782
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 2177783
    :goto_0
    return-object v0

    .line 2177784
    :cond_0
    add-int/lit8 v0, p1, 0x2

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onCreateDrawableState(I)[I

    move-result-object v1

    .line 2177785
    iget-boolean v0, p0, LX/EtO;->i:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/EtO;->b:[I

    :goto_1
    invoke-static {v1, v0}, LX/EtO;->mergeDrawableStates([I[I)[I

    move-object v0, v1

    .line 2177786
    goto :goto_0

    .line 2177787
    :cond_1
    sget-object v0, LX/EtO;->c:[I

    goto :goto_1
.end method

.method public final onPreDraw()Z
    .locals 1

    .prologue
    .line 2177779
    invoke-super {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onPreDraw()Z

    .line 2177780
    const/4 v0, 0x1

    return v0
.end method
