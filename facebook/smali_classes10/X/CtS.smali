.class public final LX/CtS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0xi;


# instance fields
.field public final synthetic a:F

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;FZ)V
    .locals 0

    .prologue
    .line 1944816
    iput-object p1, p0, LX/CtS;->c:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iput p2, p0, LX/CtS;->a:F

    iput-boolean p3, p0, LX/CtS;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 1944817
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1944818
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/CtS;->a:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 1944819
    iget-object v0, p0, LX/CtS;->c:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iget v1, p0, LX/CtS;->a:F

    iget-boolean v2, p0, LX/CtS;->b:Z

    .line 1944820
    invoke-static {v0, v1, v2}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;FZ)V

    .line 1944821
    :goto_0
    return-void

    .line 1944822
    :cond_0
    iget-object v0, p0, LX/CtS;->c:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 1944823
    invoke-static {v0, v1}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;F)V

    .line 1944824
    iget-object v0, p0, LX/CtS;->c:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-static {v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->g(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;)V

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 3

    .prologue
    .line 1944825
    iget-object v0, p0, LX/CtS;->c:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iget v1, p0, LX/CtS;->a:F

    iget-boolean v2, p0, LX/CtS;->b:Z

    .line 1944826
    invoke-static {v0, v1, v2}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;FZ)V

    .line 1944827
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 2

    .prologue
    .line 1944828
    iget-boolean v0, p0, LX/CtS;->b:Z

    if-nez v0, :cond_0

    .line 1944829
    iget-object v0, p0, LX/CtS;->c:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    iget-object v0, v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b:LX/Chv;

    new-instance v1, LX/CiG;

    invoke-direct {v1}, LX/CiG;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1944830
    :cond_0
    return-void
.end method

.method public final d(LX/0wd;)V
    .locals 0

    .prologue
    .line 1944831
    return-void
.end method
