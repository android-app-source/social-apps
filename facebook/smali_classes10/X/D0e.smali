.class public LX/D0e;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0f;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/D0e",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/D0f;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1955700
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1955701
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/D0e;->b:LX/0Zi;

    .line 1955702
    iput-object p1, p0, LX/D0e;->a:LX/0Ot;

    .line 1955703
    return-void
.end method

.method public static a(LX/0QB;)LX/D0e;
    .locals 4

    .prologue
    .line 1955704
    const-class v1, LX/D0e;

    monitor-enter v1

    .line 1955705
    :try_start_0
    sget-object v0, LX/D0e;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1955706
    sput-object v2, LX/D0e;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1955707
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1955708
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1955709
    new-instance v3, LX/D0e;

    const/16 p0, 0x3569

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/D0e;-><init>(LX/0Ot;)V

    .line 1955710
    move-object v0, v3

    .line 1955711
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1955712
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D0e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1955713
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1955714
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1955715
    check-cast p2, LX/D0d;

    .line 1955716
    iget-object v0, p0, LX/D0e;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0f;

    iget-boolean v1, p2, LX/D0d;->a:Z

    const/4 p2, 0x6

    const/4 v3, 0x0

    .line 1955717
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v5, 0x7f0a009a

    invoke-interface {v2, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    iget-object v2, v0, LX/D0f;->a:LX/D0m;

    invoke-virtual {v2, p1}, LX/D0m;->c(LX/1De;)LX/D0k;

    move-result-object v2

    const p0, 0x7f021822

    invoke-virtual {v2, p0}, LX/D0k;->j(I)LX/D0k;

    move-result-object v2

    const-string p0, "link_community_guidelines"

    invoke-virtual {v2, p0}, LX/D0k;->b(Ljava/lang/String;)LX/D0k;

    move-result-object v2

    const-string p0, "/communitystandards"

    invoke-virtual {v2, p0}, LX/D0k;->c(Ljava/lang/String;)LX/D0k;

    move-result-object p0

    if-eqz v1, :cond_0

    const v2, 0x7f082a47

    :goto_0
    invoke-virtual {p0, v2}, LX/D0k;->h(I)LX/D0k;

    move-result-object p0

    if-eqz v1, :cond_1

    const v2, 0x7f082a48

    :goto_1
    invoke-virtual {p0, v2}, LX/D0k;->i(I)LX/D0k;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v3

    const v5, 0x7f0b00d6

    invoke-virtual {v3, v5}, LX/3Ad;->j(I)LX/3Ad;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, LX/D0f;->c:LX/D0i;

    invoke-virtual {v3, p1}, LX/D0i;->c(LX/1De;)LX/D0g;

    move-result-object v3

    const v5, 0x7f082a42

    invoke-virtual {v3, v5}, LX/D0g;->h(I)LX/D0g;

    move-result-object v3

    .line 1955718
    const v5, 0x10d64b52

    const/4 p0, 0x0

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1955719
    invoke-virtual {v3, v5}, LX/D0g;->a(LX/1dQ;)LX/D0g;

    move-result-object v3

    const v5, 0x7f082a43

    invoke-virtual {v3, v5}, LX/D0g;->i(I)LX/D0g;

    move-result-object v3

    .line 1955720
    const v5, -0x7f33041f

    const/4 p0, 0x0

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1955721
    invoke-virtual {v3, v5}, LX/D0g;->b(LX/1dQ;)LX/D0g;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/D0w;->c(LX/1De;)LX/D0u;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1955722
    return-object v0

    :cond_0
    const v2, 0x7f082a49

    goto/16 :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1955723
    invoke-static {}, LX/1dS;->b()V

    .line 1955724
    iget v0, p1, LX/1dQ;->b:I

    .line 1955725
    sparse-switch v0, :sswitch_data_0

    .line 1955726
    :goto_0
    return-object v2

    .line 1955727
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1955728
    check-cast v0, LX/D0d;

    .line 1955729
    iget-object v1, p0, LX/D0e;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/D0f;

    iget-object v3, v0, LX/D0d;->b:LX/1Pn;

    .line 1955730
    iget-object p1, v1, LX/D0f;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/17Y;

    invoke-interface {v3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object p2

    sget-object p0, LX/0ax;->dn:Ljava/lang/String;

    const-string v0, "/help/www/217854714899185"

    invoke-virtual {p0, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1, p2, p0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    .line 1955731
    iget-object p1, v1, LX/D0f;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {p1, p2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1955732
    goto :goto_0

    .line 1955733
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1955734
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1955735
    check-cast v1, LX/D0d;

    .line 1955736
    iget-object v3, p0, LX/D0e;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/D0f;

    iget-object v4, v1, LX/D0d;->b:LX/1Pn;

    iget-object p1, v1, LX/D0d;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1955737
    move-object p2, v4

    check-cast p2, LX/1Pk;

    invoke-interface {p2}, LX/1Pk;->e()LX/1SX;

    move-result-object p0

    .line 1955738
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 1955739
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1955740
    instance-of v1, p0, LX/1dt;

    if-eqz v1, :cond_1

    .line 1955741
    check-cast p0, LX/1dt;

    .line 1955742
    invoke-virtual {p0, p2}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1955743
    invoke-interface {v4}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, LX/1dt;->c(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 1955744
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p2

    invoke-static {v3, p2}, LX/D0f;->a(LX/D0f;Ljava/lang/String;)V

    .line 1955745
    :cond_0
    :goto_1
    goto :goto_0

    .line 1955746
    :cond_1
    instance-of v1, p0, LX/D2z;

    if-eqz v1, :cond_0

    .line 1955747
    check-cast p0, LX/D2z;

    .line 1955748
    invoke-virtual {p0, p2}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1955749
    invoke-virtual {p0, p2, v0}, LX/D2z;->c(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V

    .line 1955750
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p2

    invoke-static {v3, p2}, LX/D0f;->a(LX/D0f;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7f33041f -> :sswitch_1
        0x10d64b52 -> :sswitch_0
    .end sparse-switch
.end method
