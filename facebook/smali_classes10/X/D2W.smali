.class public LX/D2W;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:LX/CSD;

.field public final d:LX/D2T;

.field public final e:LX/BQP;

.field public final f:Landroid/os/Handler;

.field public final g:LX/D2V;

.field public final h:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1958815
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_processed"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "video_failed"

    aput-object v2, v0, v1

    sput-object v0, LX/D2W;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/CSD;LX/D2T;LX/BQP;Landroid/os/Handler;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1958816
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958817
    new-instance v0, LX/D2V;

    invoke-direct {v0, p0}, LX/D2V;-><init>(LX/D2W;)V

    iput-object v0, p0, LX/D2W;->g:LX/D2V;

    .line 1958818
    new-instance v0, Lcom/facebook/timeline/profilevideo/upload/ProfileVideoNotificationListener$1;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/profilevideo/upload/ProfileVideoNotificationListener$1;-><init>(LX/D2W;)V

    iput-object v0, p0, LX/D2W;->h:Ljava/lang/Runnable;

    .line 1958819
    iput-object p1, p0, LX/D2W;->b:Ljava/lang/String;

    .line 1958820
    iput-object p2, p0, LX/D2W;->c:LX/CSD;

    .line 1958821
    iput-object p3, p0, LX/D2W;->d:LX/D2T;

    .line 1958822
    iput-object p4, p0, LX/D2W;->e:LX/BQP;

    .line 1958823
    iput-object p5, p0, LX/D2W;->f:Landroid/os/Handler;

    .line 1958824
    return-void
.end method

.method public static c(LX/D2W;)V
    .locals 2

    .prologue
    .line 1958825
    iget-object v0, p0, LX/D2W;->c:LX/CSD;

    iget-object v1, p0, LX/D2W;->g:LX/D2V;

    invoke-virtual {v0, v1}, LX/CSD;->a(LX/D2V;)V

    .line 1958826
    iget-object v0, p0, LX/D2W;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/D2W;->h:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1958827
    return-void
.end method
