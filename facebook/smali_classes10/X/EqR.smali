.class public final LX/EqR;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)V
    .locals 0

    .prologue
    .line 2171520
    iput-object p1, p0, LX/EqR;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 2171521
    iget-object v0, p0, LX/EqR;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    invoke-static {v0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->n(Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;)Ljava/lang/String;

    move-result-object v0

    .line 2171522
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2171523
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2171524
    const-string v2, "extra_events_note_text"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2171525
    :cond_0
    const-string v0, "extra_events_guests_ids"

    iget-object v2, p0, LX/EqR;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    iget-object v2, v2, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->w:[Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2171526
    iget-object v0, p0, LX/EqR;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->setResult(ILandroid/content/Intent;)V

    .line 2171527
    iget-object v0, p0, LX/EqR;->a:Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;

    invoke-virtual {v0}, Lcom/facebook/events/invite/EventsExtendedInviteAddLannisterNoteActivity;->finish()V

    .line 2171528
    return-void
.end method
