.class public LX/DlS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;


# direct methods
.method private constructor <init>(LX/0tX;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2038068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2038069
    iput-object p1, p0, LX/DlS;->a:LX/0tX;

    .line 2038070
    return-void
.end method

.method public static b(LX/0QB;)LX/DlS;
    .locals 2

    .prologue
    .line 2038071
    new-instance v1, LX/DlS;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/DlS;-><init>(LX/0tX;)V

    .line 2038072
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/1Zp;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchPageServicesModels$PageServicesQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2038073
    new-instance v0, LX/DlL;

    invoke-direct {v0}, LX/DlL;-><init>()V

    move-object v0, v0

    .line 2038074
    const-string v1, "num_items"

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "page_id"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2038075
    iget-object v1, p0, LX/DlS;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
