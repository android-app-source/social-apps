.class public LX/Ddb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:LX/2Og;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2019866
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Ddb;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Og;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019868
    iput-object p1, p0, LX/Ddb;->a:LX/2Og;

    .line 2019869
    return-void
.end method

.method public static a(LX/0QB;)LX/Ddb;
    .locals 7

    .prologue
    .line 2019870
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2019871
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2019872
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2019873
    if-nez v1, :cond_0

    .line 2019874
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2019875
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2019876
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2019877
    sget-object v1, LX/Ddb;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2019878
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2019879
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2019880
    :cond_1
    if-nez v1, :cond_4

    .line 2019881
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2019882
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2019883
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2019884
    new-instance p0, LX/Ddb;

    invoke-static {v0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v1

    check-cast v1, LX/2Og;

    invoke-direct {p0, v1}, LX/Ddb;-><init>(LX/2Og;)V

    .line 2019885
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2019886
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2019887
    if-nez v1, :cond_2

    .line 2019888
    sget-object v0, LX/Ddb;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ddb;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2019889
    :goto_1
    if-eqz v0, :cond_3

    .line 2019890
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2019891
    :goto_3
    check-cast v0, LX/Ddb;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2019892
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2019893
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2019894
    :catchall_1
    move-exception v0

    .line 2019895
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2019896
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2019897
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2019898
    :cond_2
    :try_start_8
    sget-object v0, LX/Ddb;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ddb;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a(LX/6ek;LX/0rS;)LX/DdZ;
    .locals 3
    .param p2    # LX/0rS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2019899
    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2019900
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "upgradeDataFreshnessForThreadListRequest called for:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2019901
    const-string v1, " folder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2019902
    if-eqz p2, :cond_0

    .line 2019903
    const-string v1, " freshness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2019904
    :cond_0
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq p2, v0, :cond_1

    sget-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-eq p2, v0, :cond_1

    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne p2, v0, :cond_2

    .line 2019905
    :cond_1
    new-instance v0, LX/DdZ;

    sget-object v1, LX/Dda;->SPECIFIC_INTENTION:LX/Dda;

    invoke-direct {v0, p2, v1}, LX/DdZ;-><init>(LX/0rS;LX/Dda;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2019906
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2019907
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/Ddb;->a:LX/2Og;

    const/4 v1, 0x0

    .line 2019908
    iget-object v2, v0, LX/2Og;->c:LX/2OQ;

    invoke-virtual {v2, p1}, LX/2OQ;->b(LX/6ek;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2019909
    :cond_3
    :goto_1
    move v0, v1

    .line 2019910
    if-nez v0, :cond_4

    .line 2019911
    new-instance v0, LX/DdZ;

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    sget-object v2, LX/Dda;->DATA_KNOWN_TO_BE_STALE:LX/Dda;

    invoke-direct {v0, v1, v2}, LX/DdZ;-><init>(LX/0rS;LX/Dda;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2019912
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2019913
    :cond_4
    :try_start_2
    new-instance v0, LX/DdZ;

    sget-object v1, LX/Dda;->DEFAULT:LX/Dda;

    invoke-direct {v0, p2, v1}, LX/DdZ;-><init>(LX/0rS;LX/Dda;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2019914
    :cond_5
    invoke-static {v0}, LX/2Og;->d(LX/2Og;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v0, LX/2Og;->d:LX/2OQ;

    invoke-virtual {v2, p1}, LX/2OQ;->b(LX/6ek;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2019915
    :cond_6
    iget-object v2, v0, LX/2Og;->i:LX/26j;

    invoke-virtual {v2}, LX/26j;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, v0, LX/2Og;->f:LX/2OQ;

    invoke-virtual {v2, p1}, LX/2OQ;->b(LX/6ek;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2019916
    :cond_7
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0rS;)LX/DdZ;
    .locals 3
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0rS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2019917
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    sget-object v0, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne p2, v0, :cond_1

    .line 2019918
    :cond_0
    new-instance v0, LX/DdZ;

    sget-object v1, LX/Dda;->SPECIFIC_INTENTION:LX/Dda;

    invoke-direct {v0, p2, v1}, LX/DdZ;-><init>(LX/0rS;LX/Dda;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2019919
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2019920
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/Ddb;->a:LX/2Og;

    const/16 v1, 0x14

    .line 2019921
    invoke-static {v0, p1}, LX/2Og;->d(LX/2Og;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/2OQ;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Z

    move-result v2

    move v0, v2

    .line 2019922
    if-nez v0, :cond_2

    .line 2019923
    new-instance v0, LX/DdZ;

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    sget-object v2, LX/Dda;->DATA_KNOWN_TO_BE_STALE:LX/Dda;

    invoke-direct {v0, v1, v2}, LX/DdZ;-><init>(LX/0rS;LX/Dda;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2019924
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2019925
    :cond_2
    :try_start_2
    new-instance v0, LX/DdZ;

    sget-object v1, LX/Dda;->DEFAULT:LX/Dda;

    invoke-direct {v0, p2, v1}, LX/DdZ;-><init>(LX/0rS;LX/Dda;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
