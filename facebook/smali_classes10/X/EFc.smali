.class public final enum LX/EFc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EFc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EFc;

.field public static final enum CALL_STARTED:LX/EFc;

.field public static final enum SHOWED_DATA_RATE_WARNING_DIALOG:LX/EFc;

.field public static final enum SHOWED_MESSENGER_PROMO:LX/EFc;

.field public static final enum UNABLE_TO_CALL:LX/EFc;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2096032
    new-instance v0, LX/EFc;

    const-string v1, "UNABLE_TO_CALL"

    invoke-direct {v0, v1, v2}, LX/EFc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFc;->UNABLE_TO_CALL:LX/EFc;

    .line 2096033
    new-instance v0, LX/EFc;

    const-string v1, "SHOWED_MESSENGER_PROMO"

    invoke-direct {v0, v1, v3}, LX/EFc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFc;->SHOWED_MESSENGER_PROMO:LX/EFc;

    .line 2096034
    new-instance v0, LX/EFc;

    const-string v1, "CALL_STARTED"

    invoke-direct {v0, v1, v4}, LX/EFc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFc;->CALL_STARTED:LX/EFc;

    .line 2096035
    new-instance v0, LX/EFc;

    const-string v1, "SHOWED_DATA_RATE_WARNING_DIALOG"

    invoke-direct {v0, v1, v5}, LX/EFc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFc;->SHOWED_DATA_RATE_WARNING_DIALOG:LX/EFc;

    .line 2096036
    const/4 v0, 0x4

    new-array v0, v0, [LX/EFc;

    sget-object v1, LX/EFc;->UNABLE_TO_CALL:LX/EFc;

    aput-object v1, v0, v2

    sget-object v1, LX/EFc;->SHOWED_MESSENGER_PROMO:LX/EFc;

    aput-object v1, v0, v3

    sget-object v1, LX/EFc;->CALL_STARTED:LX/EFc;

    aput-object v1, v0, v4

    sget-object v1, LX/EFc;->SHOWED_DATA_RATE_WARNING_DIALOG:LX/EFc;

    aput-object v1, v0, v5

    sput-object v0, LX/EFc;->$VALUES:[LX/EFc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2096037
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EFc;
    .locals 1

    .prologue
    .line 2096038
    const-class v0, LX/EFc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EFc;

    return-object v0
.end method

.method public static values()[LX/EFc;
    .locals 1

    .prologue
    .line 2096039
    sget-object v0, LX/EFc;->$VALUES:[LX/EFc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EFc;

    return-object v0
.end method
