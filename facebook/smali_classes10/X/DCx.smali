.class public final LX/DCx;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/3mb;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/DCy;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 1974739
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1974740
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "cardType"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "communityID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "communityName"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "communityCoverPhotoUri"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/DCx;->b:[Ljava/lang/String;

    .line 1974741
    iput v3, p0, LX/DCx;->c:I

    .line 1974742
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/DCx;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/DCx;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/DCx;LX/1De;IILX/DCy;)V
    .locals 1

    .prologue
    .line 1974769
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1974770
    iput-object p4, p0, LX/DCx;->a:LX/DCy;

    .line 1974771
    iget-object v0, p0, LX/DCx;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1974772
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1974765
    invoke-super {p0}, LX/1X5;->a()V

    .line 1974766
    const/4 v0, 0x0

    iput-object v0, p0, LX/DCx;->a:LX/DCy;

    .line 1974767
    sget-object v0, LX/3mb;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1974768
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/DCx;
    .locals 2

    .prologue
    .line 1974762
    iget-object v0, p0, LX/DCx;->a:LX/DCy;

    iput-object p1, v0, LX/DCy;->b:Ljava/lang/String;

    .line 1974763
    iget-object v0, p0, LX/DCx;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1974764
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/DCx;
    .locals 2

    .prologue
    .line 1974759
    iget-object v0, p0, LX/DCx;->a:LX/DCy;

    iput-object p1, v0, LX/DCy;->c:Ljava/lang/String;

    .line 1974760
    iget-object v0, p0, LX/DCx;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1974761
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/3mb;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1974749
    iget-object v1, p0, LX/DCx;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/DCx;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/DCx;->c:I

    if-ge v1, v2, :cond_2

    .line 1974750
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1974751
    :goto_0
    iget v2, p0, LX/DCx;->c:I

    if-ge v0, v2, :cond_1

    .line 1974752
    iget-object v2, p0, LX/DCx;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1974753
    iget-object v2, p0, LX/DCx;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1974754
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1974755
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1974756
    :cond_2
    iget-object v0, p0, LX/DCx;->a:LX/DCy;

    .line 1974757
    invoke-virtual {p0}, LX/DCx;->a()V

    .line 1974758
    return-object v0
.end method

.method public final d(Ljava/lang/String;)LX/DCx;
    .locals 2

    .prologue
    .line 1974746
    iget-object v0, p0, LX/DCx;->a:LX/DCy;

    iput-object p1, v0, LX/DCy;->d:Ljava/lang/String;

    .line 1974747
    iget-object v0, p0, LX/DCx;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1974748
    return-object p0
.end method

.method public final h(I)LX/DCx;
    .locals 2

    .prologue
    .line 1974743
    iget-object v0, p0, LX/DCx;->a:LX/DCy;

    iput p1, v0, LX/DCy;->a:I

    .line 1974744
    iget-object v0, p0, LX/DCx;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1974745
    return-object p0
.end method
