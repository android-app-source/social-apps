.class public final enum LX/EXQ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/EXH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EXQ;",
        ">;",
        "LX/EXH;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EXQ;

.field public static final enum CORD:LX/EXQ;

.field public static final enum STRING:LX/EXQ;

.field public static final enum STRING_PIECE:LX/EXQ;

.field private static final VALUES:[LX/EXQ;

.field private static internalValueMap:LX/EXE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EXE",
            "<",
            "LX/EXQ;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final index:I

.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2133409
    new-instance v0, LX/EXQ;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v2, v2, v2}, LX/EXQ;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXQ;->STRING:LX/EXQ;

    .line 2133410
    new-instance v0, LX/EXQ;

    const-string v1, "CORD"

    invoke-direct {v0, v1, v3, v3, v3}, LX/EXQ;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXQ;->CORD:LX/EXQ;

    .line 2133411
    new-instance v0, LX/EXQ;

    const-string v1, "STRING_PIECE"

    invoke-direct {v0, v1, v4, v4, v4}, LX/EXQ;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/EXQ;->STRING_PIECE:LX/EXQ;

    .line 2133412
    const/4 v0, 0x3

    new-array v0, v0, [LX/EXQ;

    sget-object v1, LX/EXQ;->STRING:LX/EXQ;

    aput-object v1, v0, v2

    sget-object v1, LX/EXQ;->CORD:LX/EXQ;

    aput-object v1, v0, v3

    sget-object v1, LX/EXQ;->STRING_PIECE:LX/EXQ;

    aput-object v1, v0, v4

    sput-object v0, LX/EXQ;->$VALUES:[LX/EXQ;

    .line 2133413
    new-instance v0, LX/EXP;

    invoke-direct {v0}, LX/EXP;-><init>()V

    sput-object v0, LX/EXQ;->internalValueMap:LX/EXE;

    .line 2133414
    invoke-static {}, LX/EXQ;->values()[LX/EXQ;

    move-result-object v0

    sput-object v0, LX/EXQ;->VALUES:[LX/EXQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2133427
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2133428
    iput p3, p0, LX/EXQ;->index:I

    .line 2133429
    iput p4, p0, LX/EXQ;->value:I

    .line 2133430
    return-void
.end method

.method public static final getDescriptor()LX/EYL;
    .locals 2

    .prologue
    .line 2133426
    sget-object v0, LX/EYC;->w:LX/EYF;

    invoke-virtual {v0}, LX/EYF;->g()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYL;

    return-object v0
.end method

.method public static internalGetValueMap()LX/EXE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EXE",
            "<",
            "LX/EXQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2133425
    sget-object v0, LX/EXQ;->internalValueMap:LX/EXE;

    return-object v0
.end method

.method public static valueOf(I)LX/EXQ;
    .locals 1

    .prologue
    .line 2133420
    packed-switch p0, :pswitch_data_0

    .line 2133421
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2133422
    :pswitch_0
    sget-object v0, LX/EXQ;->STRING:LX/EXQ;

    goto :goto_0

    .line 2133423
    :pswitch_1
    sget-object v0, LX/EXQ;->CORD:LX/EXQ;

    goto :goto_0

    .line 2133424
    :pswitch_2
    sget-object v0, LX/EXQ;->STRING_PIECE:LX/EXQ;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(LX/EYM;)LX/EXQ;
    .locals 2

    .prologue
    .line 2133431
    iget-object v0, p0, LX/EYM;->e:LX/EYL;

    move-object v0, v0

    .line 2133432
    invoke-static {}, LX/EXQ;->getDescriptor()LX/EYL;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2133433
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "EnumValueDescriptor is not for this type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2133434
    :cond_0
    sget-object v0, LX/EXQ;->VALUES:[LX/EXQ;

    .line 2133435
    iget v1, p0, LX/EYM;->a:I

    move v1, v1

    .line 2133436
    aget-object v0, v0, v1

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/EXQ;
    .locals 1

    .prologue
    .line 2133419
    const-class v0, LX/EXQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EXQ;

    return-object v0
.end method

.method public static values()[LX/EXQ;
    .locals 1

    .prologue
    .line 2133418
    sget-object v0, LX/EXQ;->$VALUES:[LX/EXQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EXQ;

    return-object v0
.end method


# virtual methods
.method public final getDescriptorForType()LX/EYL;
    .locals 1

    .prologue
    .line 2133417
    invoke-static {}, LX/EXQ;->getDescriptor()LX/EYL;

    move-result-object v0

    return-object v0
.end method

.method public final getNumber()I
    .locals 1

    .prologue
    .line 2133416
    iget v0, p0, LX/EXQ;->value:I

    return v0
.end method

.method public final getValueDescriptor()LX/EYM;
    .locals 2

    .prologue
    .line 2133415
    invoke-static {}, LX/EXQ;->getDescriptor()LX/EYL;

    move-result-object v0

    invoke-virtual {v0}, LX/EYL;->d()Ljava/util/List;

    move-result-object v0

    iget v1, p0, LX/EXQ;->index:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYM;

    return-object v0
.end method
