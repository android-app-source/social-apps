.class public LX/DJL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/03V;

.field public final e:Ljava/lang/String;

.field public final f:LX/21D;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/DJh;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Yb;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/Executor;LX/03V;LX/0Xl;Ljava/lang/String;Ljava/util/List;LX/21D;)V
    .locals 4
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/21D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Xl;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/DJh;",
            ">;",
            "LX/21D;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1985206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1985207
    const-string v0, "cross_post_failure"

    iput-object v0, p0, LX/DJL;->a:Ljava/lang/String;

    .line 1985208
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1985209
    invoke-interface {p6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1985210
    iput-object p1, p0, LX/DJL;->b:LX/0tX;

    .line 1985211
    iput-object p2, p0, LX/DJL;->c:Ljava/util/concurrent/Executor;

    .line 1985212
    iput-object p3, p0, LX/DJL;->d:LX/03V;

    .line 1985213
    iput-object p5, p0, LX/DJL;->e:Ljava/lang/String;

    .line 1985214
    iput-object p7, p0, LX/DJL;->f:LX/21D;

    .line 1985215
    iput-object p6, p0, LX/DJL;->g:Ljava/util/List;

    .line 1985216
    invoke-interface {p4}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v2, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance v3, LX/DJK;

    invoke-direct {v3, p0}, LX/DJK;-><init>(LX/DJL;)V

    invoke-interface {v0, v2, v3}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/DJL;->h:LX/0Yb;

    .line 1985217
    return-void

    :cond_0
    move v0, v1

    .line 1985218
    goto :goto_0
.end method
