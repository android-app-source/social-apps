.class public LX/Cne;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/ImageBlockView;",
        "LX/Clw;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/1Fb;

.field public e:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/Cow;)V
    .locals 3

    .prologue
    .line 1934097
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934098
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/Cne;

    invoke-static {p1}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v2

    check-cast v2, LX/Ckw;

    const/16 v0, 0x31dc

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, LX/Cne;->e:LX/Ckw;

    iput-object p1, p0, LX/Cne;->f:LX/0Ot;

    .line 1934099
    return-void
.end method


# virtual methods
.method public bridge synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 1934100
    check-cast p1, LX/Clw;

    invoke-virtual {p0, p1}, LX/Cne;->a(LX/Clw;)V

    return-void
.end method

.method public a(LX/Clw;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1934101
    const-string v0, "ImageBlockPresenter.bind"

    const v1, -0x5fac3061

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1934102
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934103
    check-cast v0, LX/Cow;

    invoke-static {p1}, LX/Co1;->a(LX/Clu;)Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1934104
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1934105
    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cne;->g:Ljava/lang/String;

    .line 1934106
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cne;->h:Ljava/lang/String;

    .line 1934107
    :goto_0
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1934108
    const-string v0, "ImageBlockPresenter.bind#getImgFromSection"

    const v1, -0x5a10e188

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1934109
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->l()Ljava/lang/String;

    move-result-object v1

    .line 1934110
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    iput-object v0, p0, LX/Cne;->d:LX/1Fb;

    .line 1934111
    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->j()LX/1Fb;

    move-result-object v0

    .line 1934112
    if-eqz v0, :cond_4

    .line 1934113
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    .line 1934114
    invoke-interface {v0}, LX/1Fb;->c()I

    move-result v6

    .line 1934115
    invoke-interface {v0}, LX/1Fb;->a()I

    move-result v7

    .line 1934116
    :goto_1
    invoke-interface {p1}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v8

    .line 1934117
    const v0, 0x70eb82f7

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1934118
    iget-object v0, p0, LX/Cne;->d:LX/1Fb;

    if-eqz v0, :cond_0

    .line 1934119
    const-string v0, "ImageBlockPresenter.bind#setImage"

    const v2, 0x19545f41

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1934120
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934121
    check-cast v0, LX/Cow;

    iget-object v2, p0, LX/Cne;->d:LX/1Fb;

    invoke-interface {v2}, LX/1Fb;->c()I

    move-result v2

    iget-object v3, p0, LX/Cne;->d:LX/1Fb;

    invoke-interface {v3}, LX/1Fb;->a()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/Cow;->a(Ljava/lang/String;II)V

    .line 1934122
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934123
    check-cast v0, LX/Cow;

    invoke-interface {p1}, LX/Clw;->a()LX/8Yr;

    move-result-object v1

    invoke-interface {v1}, LX/8Yr;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cne;->d:LX/1Fb;

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Cne;->d:LX/1Fb;

    invoke-interface {v3}, LX/1Fb;->c()I

    move-result v3

    iget-object v4, p0, LX/Cne;->d:LX/1Fb;

    invoke-interface {v4}, LX/1Fb;->a()I

    move-result v4

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v0 .. v9}, LX/Cow;->a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Ljava/lang/String;)V

    .line 1934124
    const v0, 0x50d90854

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1934125
    :cond_0
    new-instance v0, Lcom/facebook/richdocument/presenter/ImageBlockPresenter$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/richdocument/presenter/ImageBlockPresenter$1;-><init>(LX/Cne;LX/Clw;)V

    .line 1934126
    invoke-interface {p1}, LX/Cls;->l()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1934127
    invoke-virtual {p0, v0}, LX/CnT;->a(Ljava/lang/Runnable;)V

    .line 1934128
    :cond_1
    :goto_2
    const v0, -0x3e1f9816

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1934129
    return-void

    .line 1934130
    :cond_2
    iput-object v5, p0, LX/Cne;->g:Ljava/lang/String;

    .line 1934131
    iput-object v5, p0, LX/Cne;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 1934132
    :cond_3
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_2

    :cond_4
    move v6, v7

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1934133
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 1934134
    iget-object v0, p0, LX/Cne;->e:LX/Ckw;

    iget-object v1, p0, LX/Cne;->g:Ljava/lang/String;

    iget-object v2, p0, LX/Cne;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/Ckw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1934135
    return-void
.end method
