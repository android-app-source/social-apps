.class public LX/EM8;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "LX/3mX",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;",
        ">;TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/CxA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final d:LX/ELq;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/CxA;LX/25M;LX/ELq;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/CxA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node$ModuleResults$Edges$EdgesNode;",
            ">;>;TE;",
            "LX/25M;",
            "LX/ELq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110223
    move-object v0, p3

    check-cast v0, LX/1Pq;

    invoke-direct {p0, p1, p2, v0, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2110224
    iput-object p3, p0, LX/EM8;->c:LX/CxA;

    .line 2110225
    iput-object p5, p0, LX/EM8;->d:LX/ELq;

    .line 2110226
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2110227
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 2110205
    check-cast p2, LX/CzL;

    .line 2110206
    iget-object v0, p0, LX/EM8;->d:LX/ELq;

    const/4 v1, 0x0

    .line 2110207
    new-instance v2, LX/ELp;

    invoke-direct {v2, v0}, LX/ELp;-><init>(LX/ELq;)V

    .line 2110208
    iget-object v3, v0, LX/ELq;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ELo;

    .line 2110209
    if-nez v3, :cond_0

    .line 2110210
    new-instance v3, LX/ELo;

    invoke-direct {v3, v0}, LX/ELo;-><init>(LX/ELq;)V

    .line 2110211
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/ELo;->a$redex0(LX/ELo;LX/1De;IILX/ELp;)V

    .line 2110212
    move-object v2, v3

    .line 2110213
    move-object v1, v2

    .line 2110214
    move-object v1, v1

    .line 2110215
    iget-object v0, p0, LX/EM8;->c:LX/CxA;

    check-cast v0, LX/CxP;

    .line 2110216
    iget-object v2, v1, LX/ELo;->a:LX/ELp;

    iput-object v0, v2, LX/ELp;->a:LX/CxP;

    .line 2110217
    iget-object v2, v1, LX/ELo;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2110218
    move-object v0, v1

    .line 2110219
    iget-object v1, v0, LX/ELo;->a:LX/ELp;

    iput-object p2, v1, LX/ELp;->b:LX/CzL;

    .line 2110220
    iget-object v1, v0, LX/ELo;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2110221
    move-object v0, v0

    .line 2110222
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2110204
    const/4 v0, 0x0

    return v0
.end method
