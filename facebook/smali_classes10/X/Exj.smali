.class public abstract LX/Exj;
.super LX/1Cv;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:LX/Exs;

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Exz;",
            ">;"
        }
    .end annotation
.end field

.field public e:I


# direct methods
.method public constructor <init>(LX/Exs;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Exs;",
            "LX/0Ot",
            "<",
            "LX/Exz;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2185281
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2185282
    iput v0, p0, LX/Exj;->e:I

    .line 2185283
    iput-boolean v0, p0, LX/Exj;->a:Z

    .line 2185284
    iput-object p1, p0, LX/Exj;->c:LX/Exs;

    .line 2185285
    iput-object p2, p0, LX/Exj;->d:LX/0Ot;

    .line 2185286
    return-void
.end method

.method public static d(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendListItemView;
    .locals 2

    .prologue
    .line 2185278
    new-instance v0, Lcom/facebook/friending/common/list/FriendListItemView;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;-><init>(Landroid/content/Context;)V

    .line 2185279
    sget-object v1, LX/6VF;->XLARGE:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2185280
    return-object v0
.end method

.method public static d(LX/Exj;)Z
    .locals 1

    .prologue
    .line 2185275
    iget-boolean v0, p0, LX/Exj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Exj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Exz;

    .line 2185276
    iget-boolean p0, v0, LX/Exz;->b:Z

    move v0, p0

    .line 2185277
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(J)I
.end method

.method public abstract a(I)LX/Eus;
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2185228
    sget-object v0, LX/Exh;->a:[I

    invoke-static {}, LX/Exi;->values()[LX/Exi;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, LX/Exi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2185229
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2185230
    :pswitch_0
    invoke-static {p0}, LX/Exj;->d(LX/Exj;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Exj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Exz;

    .line 2185231
    iget-boolean v1, v0, LX/Exz;->h:Z

    if-eqz v1, :cond_1

    .line 2185232
    new-instance v1, Lcom/facebook/friending/common/list/FriendListSmallCardView;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/facebook/friending/common/list/FriendListSmallCardView;-><init>(Landroid/content/Context;)V

    .line 2185233
    :goto_0
    const p0, 0x7f0a00d5

    invoke-virtual {v1, p0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2185234
    move-object v0, v1

    .line 2185235
    :goto_1
    move-object v0, v0

    .line 2185236
    :goto_2
    return-object v0

    .line 2185237
    :pswitch_1
    invoke-static {p0}, LX/Exj;->d(LX/Exj;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Exj;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Exz;

    .line 2185238
    iget-boolean v1, v0, LX/Exz;->h:Z

    if-eqz v1, :cond_4

    .line 2185239
    new-instance v1, Lcom/facebook/friending/common/list/FriendListSmallCardView;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/friending/common/list/FriendListSmallCardView;-><init>(Landroid/content/Context;)V

    .line 2185240
    :goto_3
    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2185241
    move-object v0, v1

    .line 2185242
    :goto_4
    move-object v0, v0

    .line 2185243
    goto :goto_2

    .line 2185244
    :pswitch_2
    invoke-static {p2}, LX/Exj;->d(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendListItemView;

    move-result-object v0

    goto :goto_2

    .line 2185245
    :pswitch_3
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03070b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 2185246
    goto :goto_2

    .line 2185247
    :cond_0
    new-instance v0, Lcom/facebook/friending/common/list/FriendRequestItemView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;-><init>(Landroid/content/Context;)V

    .line 2185248
    sget-object v1, LX/6VF;->XLARGE:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2185249
    move-object v0, v0

    .line 2185250
    goto :goto_1

    .line 2185251
    :cond_1
    iget-boolean v1, v0, LX/Exz;->f:Z

    if-eqz v1, :cond_2

    .line 2185252
    invoke-static {v0, p2}, LX/Exz;->d(LX/Exz;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 2185253
    :cond_2
    invoke-static {v0, p2}, LX/Exz;->f(LX/Exz;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :cond_3
    invoke-static {p2}, LX/Exj;->d(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendListItemView;

    move-result-object v0

    goto :goto_4

    .line 2185254
    :cond_4
    iget-boolean v1, v0, LX/Exz;->f:Z

    if-eqz v1, :cond_6

    .line 2185255
    iget-boolean v1, v0, LX/Exz;->e:Z

    if-eqz v1, :cond_5

    invoke-static {v0, p2}, LX/Exz;->d(LX/Exz;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    goto :goto_3

    .line 2185256
    :cond_5
    new-instance v1, Lcom/facebook/friending/common/list/FriendListCardView;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/friending/common/list/FriendListCardView;-><init>(Landroid/content/Context;)V

    .line 2185257
    iget v2, v0, LX/Exz;->g:F

    .line 2185258
    iget-object v0, v1, Lcom/facebook/friending/common/list/FriendListCardView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2185259
    move-object v1, v1

    .line 2185260
    goto :goto_3

    .line 2185261
    :cond_6
    iget-boolean v1, v0, LX/Exz;->e:Z

    if-eqz v1, :cond_7

    invoke-static {v0, p2}, LX/Exz;->f(LX/Exz;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    goto :goto_3

    .line 2185262
    :cond_7
    new-instance v1, LX/EyS;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/EyS;-><init>(Landroid/content/Context;)V

    .line 2185263
    iget v2, v0, LX/Exz;->c:I

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 2185264
    iget-boolean v2, v0, LX/Exz;->d:Z

    const p1, 0x7f0b0060

    const/4 p2, 0x0

    .line 2185265
    invoke-virtual {v1}, LX/EyS;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    .line 2185266
    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 2185267
    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setAuxViewPadding(I)V

    .line 2185268
    if-eqz v2, :cond_8

    .line 2185269
    invoke-virtual {v1, p2, p2, p0, p2}, LX/EyS;->setPadding(IIII)V

    .line 2185270
    iget-object v0, v1, LX/EyS;->j:Landroid/view/View;

    invoke-virtual {v0, p2, p0, p2, p0}, Landroid/view/View;->setPadding(IIII)V

    .line 2185271
    :goto_5
    move-object v1, v1

    .line 2185272
    goto/16 :goto_3

    .line 2185273
    :cond_8
    invoke-virtual {v1, p0, p0, p0, p0}, LX/EyS;->setPadding(IIII)V

    .line 2185274
    iget-object p0, v1, LX/EyS;->j:Landroid/view/View;

    invoke-virtual {p0, p2, p2, p2, p2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2185175
    sget-object v0, LX/Exh;->a:[I

    invoke-static {}, LX/Exi;->values()[LX/Exi;

    move-result-object v1

    aget-object v1, v1, p4

    invoke-virtual {v1}, LX/Exi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2185176
    :goto_0
    return-void

    .line 2185177
    :pswitch_0
    invoke-static {p0}, LX/Exj;->d(LX/Exj;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2185178
    iget-object v0, p0, LX/Exj;->c:LX/Exs;

    check-cast p3, LX/EyP;

    check-cast p2, LX/83X;

    .line 2185179
    move-object v1, p3

    check-cast v1, LX/EyT;

    .line 2185180
    invoke-static {v0, p3, p2}, LX/Exs;->c(LX/Exs;LX/EyP;LX/83X;)V

    .line 2185181
    const/4 p0, 0x1

    invoke-interface {v1, p0}, LX/EyT;->setFriendRequestButtonsVisible(Z)V

    .line 2185182
    invoke-static {v0, v1}, LX/Exs;->a(LX/Exs;LX/EyT;)V

    .line 2185183
    invoke-static {v0, v1}, LX/Exs;->b(LX/Exs;LX/EyT;)V

    .line 2185184
    iget-object p0, v0, LX/Exs;->k:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Exz;

    .line 2185185
    iget-boolean p1, p0, LX/Exz;->h:Z

    move p0, p1

    .line 2185186
    if-nez p0, :cond_0

    iget-object p0, v0, LX/Exs;->k:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Exz;

    .line 2185187
    iget-boolean p1, p0, LX/Exz;->f:Z

    move p0, p1

    .line 2185188
    if-eqz p0, :cond_1

    :cond_0
    move-object p0, p3

    .line 2185189
    check-cast p0, LX/EyQ;

    invoke-virtual {v0, p0, p2}, LX/Exs;->c(LX/EyQ;LX/83X;)V

    .line 2185190
    :cond_1
    invoke-static {v0, v1, p2}, LX/Exs;->b(LX/Exs;LX/EyT;LX/83X;)V

    .line 2185191
    invoke-static {v0, v1, p2}, LX/Exs;->c(LX/Exs;LX/EyT;LX/83X;)V

    .line 2185192
    iget-object v1, v0, LX/Exs;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Exz;

    .line 2185193
    iget-boolean p0, v1, LX/Exz;->h:Z

    move v1, p0

    .line 2185194
    if-eqz v1, :cond_2

    .line 2185195
    invoke-static {v0, p3, p2}, LX/Exs;->e(LX/Exs;LX/EyP;LX/83X;)V

    .line 2185196
    :cond_2
    goto :goto_0

    .line 2185197
    :cond_3
    iget-object v0, p0, LX/Exj;->c:LX/Exs;

    check-cast p3, Lcom/facebook/friending/common/list/FriendRequestItemView;

    check-cast p2, LX/83X;

    invoke-virtual {v0, p3, p2}, LX/Exs;->a(LX/EyT;LX/83X;)V

    goto :goto_0

    .line 2185198
    :pswitch_1
    invoke-static {p0}, LX/Exj;->d(LX/Exj;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2185199
    iget-object v0, p0, LX/Exj;->c:LX/Exs;

    check-cast p3, LX/EyP;

    check-cast p2, LX/83X;

    const/4 p4, 0x0

    const/4 p1, 0x1

    .line 2185200
    move-object v1, p3

    check-cast v1, LX/EyQ;

    .line 2185201
    invoke-static {v0, p3, p2}, LX/Exs;->c(LX/Exs;LX/EyP;LX/83X;)V

    .line 2185202
    invoke-static {p2}, LX/Exs;->c(LX/83X;)Z

    move-result p0

    if-nez p0, :cond_6

    .line 2185203
    invoke-interface {v1, p4}, LX/EyQ;->setShowActionButton(Z)V

    .line 2185204
    :cond_4
    :goto_1
    goto :goto_0

    .line 2185205
    :cond_5
    iget-object v0, p0, LX/Exj;->c:LX/Exs;

    check-cast p3, Lcom/facebook/friending/common/list/FriendListItemView;

    check-cast p2, LX/83X;

    const/4 v1, 0x1

    invoke-virtual {v0, p3, p2, v1}, LX/Exs;->a(LX/EyQ;LX/83X;Z)V

    goto/16 :goto_0

    .line 2185206
    :pswitch_2
    iget-object v0, p0, LX/Exj;->c:LX/Exs;

    check-cast p3, Lcom/facebook/friending/common/list/FriendListItemView;

    check-cast p2, LX/83X;

    invoke-virtual {v0, p3, p2}, LX/Exs;->a(LX/EyQ;LX/83X;)V

    goto/16 :goto_0

    .line 2185207
    :cond_6
    invoke-interface {v1, p1}, LX/EyQ;->setShowActionButton(Z)V

    .line 2185208
    iget-object p0, v0, LX/Exs;->k:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Exz;

    .line 2185209
    iget-boolean p5, p0, LX/Exz;->h:Z

    move p0, p5

    .line 2185210
    if-eqz p0, :cond_7

    move-object p0, p3

    .line 2185211
    check-cast p0, LX/EyT;

    invoke-static {v0, p0, p2}, LX/Exs;->c(LX/Exs;LX/EyT;LX/83X;)V

    .line 2185212
    :cond_7
    iget-object p0, v0, LX/Exs;->k:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Exz;

    .line 2185213
    iget-boolean p5, p0, LX/Exz;->f:Z

    move p0, p5

    .line 2185214
    if-eqz p0, :cond_9

    iget-object p0, v0, LX/Exs;->k:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Exz;

    .line 2185215
    iget-boolean p5, p0, LX/Exz;->e:Z

    move p0, p5

    .line 2185216
    if-eqz p0, :cond_9

    move p0, p1

    :goto_2
    invoke-static {v0, v1, p2, p0}, LX/Exs;->b$redex0(LX/Exs;LX/EyQ;LX/83X;Z)V

    .line 2185217
    iget-object p0, v0, LX/Exs;->k:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Exz;

    .line 2185218
    iget-boolean p5, p0, LX/Exz;->d:Z

    move p0, p5

    .line 2185219
    if-eqz p0, :cond_8

    iget-object p0, v0, LX/Exs;->k:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Exz;

    .line 2185220
    iget-boolean p5, p0, LX/Exz;->e:Z

    move p0, p5

    .line 2185221
    if-nez p0, :cond_8

    move p4, p1

    :cond_8
    invoke-static {v0, v1, p2, p4}, LX/Exs;->c(LX/Exs;LX/EyQ;LX/83X;Z)V

    .line 2185222
    invoke-static {v0, v1, p2, p1}, LX/Exs;->d(LX/Exs;LX/EyQ;LX/83X;Z)V

    .line 2185223
    iget-object v1, v0, LX/Exs;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Exz;

    .line 2185224
    iget-boolean p0, v1, LX/Exz;->h:Z

    move v1, p0

    .line 2185225
    if-eqz v1, :cond_4

    .line 2185226
    invoke-static {v0, p3, p2}, LX/Exs;->e(LX/Exs;LX/EyP;LX/83X;)V

    goto :goto_1

    :cond_9
    move p0, p4

    .line 2185227
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2185172
    iget-object v0, p0, LX/Exj;->c:LX/Exs;

    .line 2185173
    iput-object p1, v0, LX/Exs;->s:Ljava/lang/String;

    .line 2185174
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2185168
    iget-boolean v0, p0, LX/Exj;->a:Z

    if-ne v0, p1, :cond_0

    .line 2185169
    :goto_0
    return-void

    .line 2185170
    :cond_0
    iput-boolean p1, p0, LX/Exj;->a:Z

    .line 2185171
    const v0, -0x9dac613

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 2185167
    iget-boolean v0, p0, LX/Exj;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Exj;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2185165
    iget v0, p0, LX/Exj;->e:I

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/Exj;->e:I

    .line 2185166
    invoke-virtual {p0, p1}, LX/Exj;->a(I)LX/Eus;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2185164
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2185153
    iget-boolean v0, p0, LX/Exj;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Exj;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    .line 2185154
    :goto_0
    if-eqz v0, :cond_1

    .line 2185155
    sget-object v0, LX/Exi;->LOADING_ITEM:LX/Exi;

    invoke-virtual {v0}, LX/Exi;->ordinal()I

    move-result v0

    .line 2185156
    :goto_1
    return v0

    .line 2185157
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2185158
    :cond_1
    iget-boolean v0, p0, LX/Exj;->b:Z

    if-eqz v0, :cond_4

    .line 2185159
    invoke-virtual {p0, p1}, LX/Exj;->a(I)LX/Eus;

    move-result-object v0

    .line 2185160
    invoke-interface {v0}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2185161
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_2

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p0, v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    :cond_2
    const/4 p0, 0x1

    :goto_2
    move v0, p0

    .line 2185162
    if-eqz v0, :cond_3

    sget-object v0, LX/Exi;->PERSON_YOU_MAY_KNOW:LX/Exi;

    invoke-virtual {v0}, LX/Exi;->ordinal()I

    move-result v0

    goto :goto_1

    :cond_3
    sget-object v0, LX/Exi;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/Exi;

    invoke-virtual {v0}, LX/Exi;->ordinal()I

    move-result v0

    goto :goto_1

    .line 2185163
    :cond_4
    sget-object v0, LX/Exi;->REGULAR_LIST_ITEM:LX/Exi;

    invoke-virtual {v0}, LX/Exi;->ordinal()I

    move-result v0

    goto :goto_1

    :cond_5
    const/4 p0, 0x0

    goto :goto_2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2185152
    invoke-static {}, LX/Exi;->values()[LX/Exi;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 2185151
    invoke-virtual {p0, p1}, LX/Exj;->getItemViewType(I)I

    move-result v0

    sget-object v1, LX/Exi;->LOADING_ITEM:LX/Exi;

    invoke-virtual {v1}, LX/Exi;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
