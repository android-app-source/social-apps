.class public LX/ERj;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/11H;

.field private final c:LX/ERW;

.field private final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121353
    const-class v0, LX/ERj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ERj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/11H;LX/ERW;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2121354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121355
    iput-object p1, p0, LX/ERj;->b:LX/11H;

    .line 2121356
    iput-object p2, p0, LX/ERj;->c:LX/ERW;

    .line 2121357
    iput-object p3, p0, LX/ERj;->d:LX/03V;

    .line 2121358
    return-void
.end method

.method private b(Ljava/util/Set;J)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2121359
    new-instance v0, LX/ERX;

    const-wide/16 v4, 0x0

    move-wide v1, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/ERX;-><init>(JLjava/util/Set;J)V

    .line 2121360
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    .line 2121361
    :try_start_0
    iget-object v1, p0, LX/ERj;->b:LX/11H;

    iget-object v2, p0, LX/ERj;->c:LX/ERW;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;

    .line 2121362
    invoke-virtual {v0}, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatusResult;->a()Ljava/util/Map;

    move-result-object v0

    .line 2121363
    invoke-interface {v0}, Ljava/util/Map;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2121364
    :goto_0
    return-object v0

    .line 2121365
    :catch_0
    move-exception v0

    .line 2121366
    sget-object v1, LX/ERj;->a:Ljava/lang/String;

    const-string v2, "getImageStatus"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2121367
    iget-object v1, p0, LX/ERj;->d:LX/03V;

    const-string v2, "vault_local_image_status_api exception"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2121368
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Set;J)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2121369
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2121370
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 2121371
    :goto_0
    return-object v0

    .line 2121372
    :cond_1
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 2121373
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    .line 2121374
    const/16 v1, 0x64

    move v1, v1

    .line 2121375
    if-le v0, v1, :cond_5

    .line 2121376
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2121377
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2121378
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2121379
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    .line 2121380
    const/16 v4, 0x64

    move v4, v4

    .line 2121381
    if-ne v0, v4, :cond_2

    .line 2121382
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "dispatching a set of "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " images"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2121383
    invoke-direct {p0, v2, p2, p3}, LX/ERj;->b(Ljava/util/Set;J)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2121384
    invoke-interface {v2}, Ljava/util/Set;->clear()V

    goto :goto_1

    .line 2121385
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 2121386
    invoke-direct {p0, v2, p2, p3}, LX/ERj;->b(Ljava/util/Set;J)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_4
    move-object v0, v1

    .line 2121387
    goto :goto_0

    .line 2121388
    :cond_5
    invoke-direct {p0, p1, p2, p3}, LX/ERj;->b(Ljava/util/Set;J)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method
