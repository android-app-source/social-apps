.class public LX/DDv;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field public final c:Lcom/facebook/widget/text/BetterTextView;

.field public final d:Lcom/facebook/widget/text/BetterTextView;

.field public final e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1976114
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/DDv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1976115
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1976120
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1976121
    const-class v0, LX/DDv;

    invoke-static {v0, p0}, LX/DDv;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1976122
    const v0, 0x7f0307ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1976123
    const v0, 0x7f0d1512

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DDv;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1976124
    iget-object v0, p0, LX/DDv;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/DDv;->a:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1976125
    const v0, 0x7f0d1513

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DDv;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1976126
    iget-object v0, p0, LX/DDv;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/DDv;->a:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1976127
    const v0, 0x7f0d1510

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/DDv;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1976128
    const v0, 0x7f0d1511

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, LX/DDv;->e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 1976129
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/DDv;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object p0

    check-cast p0, LX/23P;

    iput-object p0, p1, LX/DDv;->a:LX/23P;

    return-void
.end method


# virtual methods
.method public setOnEditPostClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1976118
    iget-object v0, p0, LX/DDv;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1976119
    return-void
.end method

.method public setOnMarkAsSoldClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1976116
    iget-object v0, p0, LX/DDv;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1976117
    return-void
.end method
