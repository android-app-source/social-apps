.class public abstract LX/E6d;
.super LX/Cfk;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/E6b;

.field public c:I

.field public final d:LX/E1i;

.field public final e:Lcom/facebook/reaction/ReactionUtil;

.field public f:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(LX/E1i;LX/3Tx;Lcom/facebook/reaction/ReactionUtil;)V
    .locals 0

    .prologue
    .line 2080406
    invoke-direct {p0, p2}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2080407
    iput-object p1, p0, LX/E6d;->d:LX/E1i;

    .line 2080408
    iput-object p3, p0, LX/E6d;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2080409
    return-void
.end method


# virtual methods
.method public a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V
    .locals 2
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p6    # LX/Cgb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2080410
    invoke-super/range {p0 .. p6}, LX/Cfk;->a(LX/2jb;Landroid/view/ViewGroup;LX/0o8;Ljava/lang/String;Ljava/lang/String;LX/Cgb;)V

    .line 2080411
    const v0, 0x7f03110c

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2080412
    iget-object v1, p0, LX/Cfk;->c:Landroid/view/ViewGroup;

    move-object v1, v1

    .line 2080413
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2080414
    const v1, 0x7f0d2872

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    .line 2080415
    return-void
.end method

.method public final a(LX/E6c;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2080416
    sget-object v0, LX/E6Y;->a:[I

    invoke-virtual {p1}, LX/E6c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2080417
    :goto_0
    return-void

    .line 2080418
    :pswitch_0
    iget-object v0, p0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    goto :goto_0

    .line 2080419
    :pswitch_1
    iget-object v0, p0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    goto :goto_0

    .line 2080420
    :pswitch_2
    iget-object v0, p0, LX/Cfk;->d:Landroid/content/Context;

    move-object v0, v0

    .line 2080421
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x7f0e091b

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x7f010721

    aput v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2080422
    const v1, 0x7f0b1624

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2080423
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2080424
    iget-object v0, p0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)I
    .locals 7

    .prologue
    .line 2080425
    iget-object v0, p0, LX/E6d;->b:LX/E6b;

    if-nez v0, :cond_0

    .line 2080426
    new-instance v0, LX/E6b;

    invoke-direct {v0, p0, p3, p1, p2}, LX/E6b;-><init>(LX/E6d;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/E6d;->b:LX/E6b;

    .line 2080427
    :goto_0
    iget-object v0, p0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, LX/E6d;->b:LX/E6b;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2080428
    iget-object v6, p0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    new-instance v0, LX/E6Z;

    iget-object v4, p0, LX/E6d;->b:LX/E6b;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v4}, LX/E6Z;-><init>(LX/E6d;Ljava/lang/String;Ljava/lang/String;LX/E6b;)V

    invoke-virtual {v6, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2080429
    iget-object v0, p0, LX/E6d;->f:Landroid/support/v4/view/ViewPager;

    iget v1, p0, LX/E6d;->c:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2080430
    iput-object p1, p0, LX/E6d;->a:Ljava/lang/String;

    .line 2080431
    iget-object v0, p0, LX/E6d;->b:LX/E6b;

    invoke-virtual {v0}, LX/E6b;->b()I

    move-result v0

    return v0

    .line 2080432
    :cond_0
    iget-object v0, p0, LX/E6d;->b:LX/E6b;

    .line 2080433
    iget-object v1, v0, LX/E6b;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    if-eq v1, p3, :cond_1

    .line 2080434
    invoke-static {v0, p3}, LX/E6b;->b(LX/E6b;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)V

    .line 2080435
    :cond_1
    goto :goto_0
.end method

.method public i()F
    .locals 1

    .prologue
    .line 2080436
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method
