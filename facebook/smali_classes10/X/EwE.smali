.class public final LX/EwE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ewi;

.field public final synthetic b:LX/EwG;


# direct methods
.method public constructor <init>(LX/EwG;LX/Ewi;)V
    .locals 0

    .prologue
    .line 2182808
    iput-object p1, p0, LX/EwE;->b:LX/EwG;

    iput-object p2, p0, LX/EwE;->a:LX/Ewi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v11, 0x2

    const/4 v0, 0x1

    const v1, -0x33e1d2ec    # -4.1464912E7f

    invoke-static {v11, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2182809
    iget-object v1, p0, LX/EwE;->a:LX/Ewi;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v2}, LX/Eus;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2182810
    iget-object v1, p0, LX/EwE;->a:LX/Ewi;

    const/4 v2, 0x0

    .line 2182811
    iput-boolean v2, v1, LX/Eut;->f:Z

    .line 2182812
    iget-object v1, p0, LX/EwE;->b:LX/EwG;

    const v2, 0x4d7d4821    # 2.65585168E8f

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2182813
    iget-object v1, p0, LX/EwE;->b:LX/EwG;

    invoke-static {v1}, LX/EwG;->e(LX/EwG;)V

    .line 2182814
    iget-object v1, p0, LX/EwE;->b:LX/EwG;

    iget-object v1, v1, LX/EwG;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3UJ;

    iget-object v2, p0, LX/EwE;->a:LX/Ewi;

    invoke-virtual {v2}, LX/Eus;->a()J

    move-result-wide v2

    iget-object v4, p0, LX/EwE;->a:LX/Ewi;

    .line 2182815
    iget-wide v12, v4, LX/Eus;->c:J

    move-wide v4, v12

    .line 2182816
    sget-object v6, LX/2hA;->MEMORIAL_CONTACT_TOOLS:LX/2hA;

    sget-object v7, LX/2na;->REJECT:LX/2na;

    iget-object v8, p0, LX/EwE;->b:LX/EwG;

    iget-object v9, p0, LX/EwE;->a:LX/Ewi;

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v8, v9, v10}, LX/EwG;->a$redex0(LX/EwG;LX/Ewi;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/84H;

    move-result-object v8

    invoke-virtual/range {v1 .. v8}, LX/3UJ;->a(JJLX/2hA;LX/2na;LX/84H;)V

    .line 2182817
    const v1, 0x64f76c21

    invoke-static {v11, v11, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
