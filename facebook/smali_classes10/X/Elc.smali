.class public LX/Elc;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/ElF;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/ElF;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164556
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/ElF;
    .locals 3

    .prologue
    .line 2164544
    sget-object v0, LX/Elc;->a:LX/ElF;

    if-nez v0, :cond_1

    .line 2164545
    const-class v1, LX/Elc;

    monitor-enter v1

    .line 2164546
    :try_start_0
    sget-object v0, LX/Elc;->a:LX/ElF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2164547
    if-eqz v2, :cond_0

    .line 2164548
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2164549
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object p0

    check-cast p0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/Elb;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/ElF;

    move-result-object p0

    move-object v0, p0

    .line 2164550
    sput-object v0, LX/Elc;->a:LX/ElF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2164551
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2164552
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2164553
    :cond_1
    sget-object v0, LX/Elc;->a:LX/ElF;

    return-object v0

    .line 2164554
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2164555
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2164543
    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/Elb;->a(Lcom/facebook/prefs/shared/FbSharedPreferences;)LX/ElF;

    move-result-object v0

    return-object v0
.end method
