.class public final LX/E0A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/ipc/model/PageTopic;

.field public c:Landroid/location/Location;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/places/create/network/PlacePinAppId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/ipc/model/PageTopic;Landroid/location/Location;LX/0am;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ipc/model/PageTopic;",
            "Landroid/location/Location;",
            "LX/0am",
            "<",
            "Lcom/facebook/places/create/network/PlacePinAppId;",
            ">;",
            "Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2067865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067866
    iput-object p1, p0, LX/E0A;->a:Ljava/lang/String;

    .line 2067867
    iput-object p2, p0, LX/E0A;->b:Lcom/facebook/ipc/model/PageTopic;

    .line 2067868
    iput-object p3, p0, LX/E0A;->c:Landroid/location/Location;

    .line 2067869
    iput-object p4, p0, LX/E0A;->i:LX/0am;

    .line 2067870
    const-string v0, ""

    iput-object v0, p0, LX/E0A;->d:Ljava/lang/String;

    .line 2067871
    iput-object p5, p0, LX/E0A;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067872
    const-string v0, ""

    iput-object v0, p0, LX/E0A;->f:Ljava/lang/String;

    .line 2067873
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/E0A;->g:Z

    .line 2067874
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/E0A;->h:LX/0am;

    .line 2067875
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/E0A;
    .locals 1

    .prologue
    .line 2067876
    invoke-static {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    iput-object v0, p0, LX/E0A;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067877
    return-object p0
.end method

.method public final a()Lcom/facebook/places/create/PlaceCreationState;
    .locals 2

    .prologue
    .line 2067878
    new-instance v0, Lcom/facebook/places/create/PlaceCreationState;

    invoke-direct {v0, p0}, Lcom/facebook/places/create/PlaceCreationState;-><init>(LX/E0A;)V

    return-object v0
.end method
