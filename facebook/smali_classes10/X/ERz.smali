.class public LX/ERz;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/ES8;

.field public final d:LX/2TK;

.field private final e:LX/ERr;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2121872
    const-class v0, LX/ERz;

    sput-object v0, LX/ERz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/ES8;LX/2TK;LX/ERr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2121905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121906
    iput-object p1, p0, LX/ERz;->b:Landroid/content/Context;

    .line 2121907
    iput-object p2, p0, LX/ERz;->c:LX/ES8;

    .line 2121908
    iput-object p3, p0, LX/ERz;->d:LX/2TK;

    .line 2121909
    iput-object p4, p0, LX/ERz;->e:LX/ERr;

    .line 2121910
    return-void
.end method

.method public static a(LX/ERz;IZ)I
    .locals 11

    .prologue
    .line 2121896
    iget-object v0, p0, LX/ERz;->d:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->c()Z

    move-result v0

    .line 2121897
    iget-object v1, p0, LX/ERz;->c:LX/ES8;

    .line 2121898
    if-eqz p2, :cond_0

    sget-object v7, LX/ES4;->RETRY_WITH_HARD_FAILURES:LX/ES4;

    .line 2121899
    :goto_0
    if-eqz v0, :cond_1

    sget-object v5, LX/ES7;->ALL:LX/ES7;

    :goto_1
    sget-object v8, LX/ES6;->NONE:LX/ES6;

    const-wide/16 v9, 0x0

    move-object v4, v1

    move v6, p1

    invoke-static/range {v4 .. v10}, LX/ES8;->a(LX/ES8;LX/ES7;ILX/ES4;LX/ES6;J)Ljava/util/List;

    move-result-object v3

    move-object v0, v3

    .line 2121900
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 2121901
    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-static {p0, v0, v1, v2}, LX/ERz;->a(LX/ERz;Ljava/util/List;II)V

    .line 2121902
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    .line 2121903
    :cond_0
    sget-object v7, LX/ES4;->RETRY_SOFT_FAILURES_ONLY:LX/ES4;

    goto :goto_0

    .line 2121904
    :cond_1
    sget-object v5, LX/ES7;->NO_UPGRADES:LX/ES7;

    goto :goto_1
.end method

.method public static a(LX/ERz;Ljava/util/List;II)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 2121881
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 2121882
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, LX/ERz;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/vault/service/VaultSyncJobProcessor;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2121883
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2121884
    iget-object v0, p0, LX/ERz;->e:LX/ERr;

    invoke-virtual {v0, p1}, LX/ERr;->a(Ljava/util/List;)V

    .line 2121885
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2121886
    iget-object v4, p0, LX/ERz;->c:LX/ES8;

    invoke-virtual {v4, v0}, LX/ES8;->a(Lcom/facebook/vault/provider/VaultImageProviderRow;)V

    .line 2121887
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2121888
    const/4 v4, 0x2

    if-ne p3, v4, :cond_0

    .line 2121889
    goto :goto_0

    .line 2121890
    :cond_0
    goto :goto_0

    .line 2121891
    :cond_1
    const-string v0, "queuing_type"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2121892
    const-string v0, "queuing_objects"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2121893
    const-string v0, "sync_reason"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2121894
    iget-object v0, p0, LX/ERz;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2121895
    :cond_2
    return-void
.end method

.method public static b(LX/0QB;)LX/ERz;
    .locals 5

    .prologue
    .line 2121879
    new-instance v4, LX/ERz;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/ES8;->a(LX/0QB;)LX/ES8;

    move-result-object v1

    check-cast v1, LX/ES8;

    invoke-static {p0}, LX/2TK;->b(LX/0QB;)LX/2TK;

    move-result-object v2

    check-cast v2, LX/2TK;

    invoke-static {p0}, LX/ERr;->a(LX/0QB;)LX/ERr;

    move-result-object v3

    check-cast v3, LX/ERr;

    invoke-direct {v4, v0, v1, v2, v3}, LX/ERz;-><init>(Landroid/content/Context;LX/ES8;LX/2TK;LX/ERr;)V

    .line 2121880
    return-object v4
.end method


# virtual methods
.method public final a(II)I
    .locals 3

    .prologue
    .line 2121873
    iget-object v0, p0, LX/ERz;->d:LX/2TK;

    invoke-virtual {v0}, LX/2TK;->c()Z

    move-result v0

    .line 2121874
    iget-object v1, p0, LX/ERz;->c:LX/ES8;

    invoke-virtual {v1, p1, v0}, LX/ES8;->a(IZ)LX/ES5;

    move-result-object v0

    .line 2121875
    iget-object v1, v0, LX/ES5;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    iget-object v1, v0, LX/ES5;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 2121876
    iget-object v1, v0, LX/ES5;->a:Ljava/util/List;

    const/4 v2, 0x1

    invoke-static {p0, v1, p2, v2}, LX/ERz;->a(LX/ERz;Ljava/util/List;II)V

    .line 2121877
    iget-object v1, v0, LX/ES5;->b:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {p0, v1, p2, v2}, LX/ERz;->a(LX/ERz;Ljava/util/List;II)V

    .line 2121878
    iget-object v1, v0, LX/ES5;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v0, v0, LX/ES5;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method
