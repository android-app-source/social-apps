.class public LX/Ds2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Drs;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Ds2;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2049910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049911
    iput-object p1, p0, LX/Ds2;->a:Landroid/content/Context;

    .line 2049912
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/Ds2;->b:Ljava/util/Random;

    .line 2049913
    return-void
.end method

.method public static a(LX/0QB;)LX/Ds2;
    .locals 4

    .prologue
    .line 2049914
    sget-object v0, LX/Ds2;->c:LX/Ds2;

    if-nez v0, :cond_1

    .line 2049915
    const-class v1, LX/Ds2;

    monitor-enter v1

    .line 2049916
    :try_start_0
    sget-object v0, LX/Ds2;->c:LX/Ds2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2049917
    if-eqz v2, :cond_0

    .line 2049918
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2049919
    new-instance p0, LX/Ds2;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/Ds2;-><init>(Landroid/content/Context;)V

    .line 2049920
    move-object v0, p0

    .line 2049921
    sput-object v0, LX/Ds2;->c:LX/Ds2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2049922
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2049923
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2049924
    :cond_1
    sget-object v0, LX/Ds2;->c:LX/Ds2;

    return-object v0

    .line 2049925
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2049926
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/Drw;)LX/3pb;
    .locals 6

    .prologue
    .line 2049927
    iget-object v0, p1, LX/Drw;->c:Lorg/json/JSONObject;

    move-object v1, v0

    .line 2049928
    const/4 v0, 0x0

    .line 2049929
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->FRIENDING_REDIRECT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2049930
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->FRIENDING_REDIRECT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;

    move-result-object v2

    .line 2049931
    sget-object v3, LX/Ds1;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendingRedirectType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2049932
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2049933
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v2, v0

    .line 2049934
    iget-object v3, p0, LX/Ds2;->a:Landroid/content/Context;

    .line 2049935
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v0, v0

    .line 2049936
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, LX/Ds2;->b:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    .line 2049937
    iget-object v5, p1, LX/Drw;->d:Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;

    move-object v5, v5

    .line 2049938
    invoke-static {v2, v3, v0, v4, v5}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->a(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2049939
    :cond_1
    new-instance v2, LX/3pX;

    const v3, 0x7f021568

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->TITLE_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1, v0}, LX/3pX;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v2}, LX/3pX;->b()LX/3pb;

    move-result-object v0

    return-object v0

    .line 2049940
    :pswitch_0
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v2, v0

    .line 2049941
    iget-object v3, p0, LX/Ds2;->a:Landroid/content/Context;

    .line 2049942
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v0, v0

    .line 2049943
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, LX/Ds2;->b:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    .line 2049944
    iget-object v5, p1, LX/Drw;->d:Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;

    move-object v5, v5

    .line 2049945
    invoke-static {v2, v3, v0, v4, v5}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->e(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    .line 2049946
    :pswitch_1
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v2, v0

    .line 2049947
    iget-object v3, p0, LX/Ds2;->a:Landroid/content/Context;

    .line 2049948
    iget-object v0, p1, LX/Drw;->b:Lcom/facebook/notifications/model/SystemTrayNotification;

    move-object v0, v0

    .line 2049949
    invoke-virtual {v0}, Lcom/facebook/notifications/model/SystemTrayNotification;->d()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, LX/Ds2;->b:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    .line 2049950
    iget-object v5, p1, LX/Drw;->d:Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;

    move-object v5, v5

    .line 2049951
    invoke-static {v2, v3, v0, v4, v5}, Lcom/facebook/notifications/service/FriendRequestNotificationService;->f(Lcom/facebook/notifications/model/SystemTrayNotification;Landroid/content/Context;Ljava/lang/String;ILcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 2049952
    const/4 v0, 0x0

    return v0
.end method
