.class public final LX/EXu;
.super LX/EWy;
.source ""

# interfaces
.implements LX/EXt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWy",
        "<",
        "LX/EXv;",
        "LX/EXu;",
        ">;",
        "LX/EXt;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EYB;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2135692
    invoke-direct {p0}, LX/EWy;-><init>()V

    .line 2135693
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXu;->b:Ljava/util/List;

    .line 2135694
    invoke-direct {p0}, LX/EXu;->w()V

    .line 2135695
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2135696
    invoke-direct {p0, p1}, LX/EWy;-><init>(LX/EYd;)V

    .line 2135697
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXu;->b:Ljava/util/List;

    .line 2135698
    invoke-direct {p0}, LX/EXu;->w()V

    .line 2135699
    return-void
.end method

.method private A()LX/EXv;
    .locals 2

    .prologue
    .line 2135700
    invoke-virtual {p0}, LX/EXu;->l()LX/EXv;

    move-result-object v0

    .line 2135701
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2135702
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2135703
    :cond_0
    return-object v0
.end method

.method private D()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EYB;",
            "LX/EY6;",
            "LX/EY5;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2135704
    iget-object v1, p0, LX/EXu;->c:LX/EZ2;

    if-nez v1, :cond_0

    .line 2135705
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EXu;->b:Ljava/util/List;

    iget v3, p0, LX/EXu;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2135706
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2135707
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EXu;->c:LX/EZ2;

    .line 2135708
    const/4 v0, 0x0

    iput-object v0, p0, LX/EXu;->b:Ljava/util/List;

    .line 2135709
    :cond_0
    iget-object v0, p0, LX/EXu;->c:LX/EZ2;

    return-object v0

    .line 2135710
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EXu;
    .locals 1

    .prologue
    .line 2135711
    instance-of v0, p1, LX/EXv;

    if-eqz v0, :cond_0

    .line 2135712
    check-cast p1, LX/EXv;

    invoke-virtual {p0, p1}, LX/EXu;->a(LX/EXv;)LX/EXu;

    move-result-object p0

    .line 2135713
    :goto_0
    return-object p0

    .line 2135714
    :cond_0
    invoke-super {p0, p1}, LX/EWy;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EXu;
    .locals 4

    .prologue
    .line 2135715
    const/4 v2, 0x0

    .line 2135716
    :try_start_0
    sget-object v0, LX/EXv;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXv;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2135717
    if-eqz v0, :cond_0

    .line 2135718
    invoke-virtual {p0, v0}, LX/EXu;->a(LX/EXv;)LX/EXu;

    .line 2135719
    :cond_0
    return-object p0

    .line 2135720
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2135721
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2135722
    check-cast v0, LX/EXv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2135723
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2135724
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2135725
    invoke-virtual {p0, v1}, LX/EXu;->a(LX/EXv;)LX/EXu;

    :cond_1
    throw v0

    .line 2135726
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 2135727
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_0

    .line 2135728
    invoke-direct {p0}, LX/EXu;->D()LX/EZ2;

    .line 2135729
    :cond_0
    return-void
.end method

.method public static x()LX/EXu;
    .locals 1

    .prologue
    .line 2135730
    new-instance v0, LX/EXu;

    invoke-direct {v0}, LX/EXu;-><init>()V

    return-object v0
.end method

.method private y()LX/EXu;
    .locals 2

    .prologue
    .line 2135731
    invoke-static {}, LX/EXu;->x()LX/EXu;

    move-result-object v0

    invoke-virtual {p0}, LX/EXu;->l()LX/EXv;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EXu;->a(LX/EXv;)LX/EXu;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2135732
    invoke-direct {p0, p1}, LX/EXu;->d(LX/EWY;)LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2135733
    invoke-direct {p0, p1, p2}, LX/EXu;->d(LX/EWd;LX/EYZ;)LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/EXv;)LX/EXu;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2135734
    sget-object v1, LX/EXv;->c:LX/EXv;

    move-object v1, v1

    .line 2135735
    if-ne p1, v1, :cond_0

    .line 2135736
    :goto_0
    return-object p0

    .line 2135737
    :cond_0
    iget-object v1, p0, LX/EXu;->c:LX/EZ2;

    if-nez v1, :cond_4

    .line 2135738
    iget-object v0, p1, LX/EXv;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2135739
    iget-object v0, p0, LX/EXu;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2135740
    iget-object v0, p1, LX/EXv;->uninterpretedOption_:Ljava/util/List;

    iput-object v0, p0, LX/EXu;->b:Ljava/util/List;

    .line 2135741
    iget v0, p0, LX/EXu;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, LX/EXu;->a:I

    .line 2135742
    :goto_1
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2135743
    :cond_1
    :goto_2
    invoke-virtual {p0, p1}, LX/EWy;->a(LX/EX1;)V

    .line 2135744
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    .line 2135745
    :cond_2
    iget v0, p0, LX/EXu;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    .line 2135746
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EXu;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EXu;->b:Ljava/util/List;

    .line 2135747
    iget v0, p0, LX/EXu;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EXu;->a:I

    .line 2135748
    :cond_3
    iget-object v0, p0, LX/EXu;->b:Ljava/util/List;

    iget-object v1, p1, LX/EXv;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2135749
    :cond_4
    iget-object v1, p1, LX/EXv;->uninterpretedOption_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2135750
    iget-object v1, p0, LX/EXu;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2135751
    iget-object v1, p0, LX/EXu;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2135752
    iput-object v0, p0, LX/EXu;->c:LX/EZ2;

    .line 2135753
    iget-object v1, p1, LX/EXv;->uninterpretedOption_:Ljava/util/List;

    iput-object v1, p0, LX/EXu;->b:Ljava/util/List;

    .line 2135754
    iget v1, p0, LX/EXu;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/EXu;->a:I

    .line 2135755
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_5

    invoke-direct {p0}, LX/EXu;->D()LX/EZ2;

    move-result-object v0

    :cond_5
    iput-object v0, p0, LX/EXu;->c:LX/EZ2;

    goto :goto_2

    .line 2135756
    :cond_6
    iget-object v0, p0, LX/EXu;->c:LX/EZ2;

    iget-object v1, p1, LX/EXv;->uninterpretedOption_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto :goto_2
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2135678
    move v0, v1

    .line 2135679
    :goto_0
    iget-object v2, p0, LX/EXu;->c:LX/EZ2;

    if-nez v2, :cond_3

    .line 2135680
    iget-object v2, p0, LX/EXu;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2135681
    :goto_1
    move v2, v2

    .line 2135682
    if-ge v0, v2, :cond_2

    .line 2135683
    iget-object v2, p0, LX/EXu;->c:LX/EZ2;

    if-nez v2, :cond_4

    .line 2135684
    iget-object v2, p0, LX/EXu;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EYB;

    .line 2135685
    :goto_2
    move-object v2, v2

    .line 2135686
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2135687
    :cond_0
    :goto_3
    return v1

    .line 2135688
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2135689
    :cond_2
    invoke-virtual {p0}, LX/EWy;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2135690
    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    iget-object v2, p0, LX/EXu;->c:LX/EZ2;

    invoke-virtual {v2}, LX/EZ2;->c()I

    move-result v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/EXu;->c:LX/EZ2;

    invoke-virtual {v2, v0}, LX/EZ2;->a(I)LX/EWp;

    move-result-object v2

    check-cast v2, LX/EYB;

    goto :goto_2
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2135677
    invoke-direct {p0, p1, p2}, LX/EXu;->d(LX/EWd;LX/EYZ;)LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2135691
    invoke-direct {p0}, LX/EXu;->y()LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2135651
    invoke-direct {p0, p1, p2}, LX/EXu;->d(LX/EWd;LX/EYZ;)LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2135652
    invoke-direct {p0}, LX/EXu;->y()LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2135653
    invoke-direct {p0, p1}, LX/EXu;->d(LX/EWY;)LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2135654
    invoke-direct {p0}, LX/EXu;->y()LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2135655
    sget-object v0, LX/EYC;->D:LX/EYn;

    const-class v1, LX/EXv;

    const-class v2, LX/EXu;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2135656
    sget-object v0, LX/EYC;->C:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2135657
    invoke-direct {p0}, LX/EXu;->y()LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2135658
    invoke-virtual {p0}, LX/EXu;->l()LX/EXv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2135659
    invoke-direct {p0}, LX/EXu;->A()LX/EXv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2135660
    invoke-virtual {p0}, LX/EXu;->l()LX/EXv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2135661
    invoke-direct {p0}, LX/EXu;->A()LX/EXv;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/EXv;
    .locals 3

    .prologue
    .line 2135662
    new-instance v0, LX/EXv;

    invoke-direct {v0, p0}, LX/EXv;-><init>(LX/EWy;)V

    .line 2135663
    iget-object v1, p0, LX/EXu;->c:LX/EZ2;

    if-nez v1, :cond_1

    .line 2135664
    iget v1, p0, LX/EXu;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2135665
    iget-object v1, p0, LX/EXu;->b:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXu;->b:Ljava/util/List;

    .line 2135666
    iget v1, p0, LX/EXu;->a:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, LX/EXu;->a:I

    .line 2135667
    :cond_0
    iget-object v1, p0, LX/EXu;->b:Ljava/util/List;

    .line 2135668
    iput-object v1, v0, LX/EXv;->uninterpretedOption_:Ljava/util/List;

    .line 2135669
    :goto_0
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2135670
    return-object v0

    .line 2135671
    :cond_1
    iget-object v1, p0, LX/EXu;->c:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v1

    .line 2135672
    iput-object v1, v0, LX/EXv;->uninterpretedOption_:Ljava/util/List;

    .line 2135673
    goto :goto_0
.end method

.method public final synthetic m()LX/EWy;
    .locals 1

    .prologue
    .line 2135674
    invoke-direct {p0}, LX/EXu;->y()LX/EXu;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2135675
    sget-object v0, LX/EXv;->c:LX/EXv;

    move-object v0, v0

    .line 2135676
    return-object v0
.end method
