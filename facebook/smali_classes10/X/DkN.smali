.class public LX/DkN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;

.field public final c:LX/DkQ;

.field private final d:LX/0Uh;

.field public final e:Ljava/lang/String;

.field public final f:LX/Dka;

.field private final g:Landroid/content/Context;

.field private final h:LX/03V;


# direct methods
.method public constructor <init>(LX/DkQ;LX/0tX;LX/1Ck;LX/0Uh;Ljava/lang/String;LX/Dka;Landroid/content/Context;LX/03V;)V
    .locals 0
    .param p1    # LX/DkQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2034131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2034132
    iput-object p1, p0, LX/DkN;->c:LX/DkQ;

    .line 2034133
    iput-object p2, p0, LX/DkN;->a:LX/0tX;

    .line 2034134
    iput-object p3, p0, LX/DkN;->b:LX/1Ck;

    .line 2034135
    iput-object p4, p0, LX/DkN;->d:LX/0Uh;

    .line 2034136
    iput-object p5, p0, LX/DkN;->e:Ljava/lang/String;

    .line 2034137
    iput-object p6, p0, LX/DkN;->f:LX/Dka;

    .line 2034138
    iput-object p7, p0, LX/DkN;->g:Landroid/content/Context;

    .line 2034139
    iput-object p8, p0, LX/DkN;->h:LX/03V;

    .line 2034140
    return-void
.end method

.method public static c(LX/DkN;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2034141
    iget-object v0, p0, LX/DkN;->c:LX/DkQ;

    invoke-virtual {v0}, LX/DkQ;->b()Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    move-result-object v0

    .line 2034142
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-ne v0, v1, :cond_0

    .line 2034143
    new-instance v0, LX/Dkf;

    invoke-direct {v0}, LX/Dkf;-><init>()V

    move-object v0, v0

    .line 2034144
    const-string v1, "num_appointments"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2034145
    iget-object v1, p0, LX/DkN;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2034146
    new-instance v1, LX/DkK;

    invoke-direct {v1}, LX/DkK;-><init>()V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2034147
    :goto_0
    return-object v0

    .line 2034148
    :cond_0
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->USER_QUERY_APPOINTMENTS_WITH_A_PAGE:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-ne v0, v1, :cond_1

    .line 2034149
    new-instance v0, LX/Dkg;

    invoke-direct {v0}, LX/Dkg;-><init>()V

    move-object v0, v0

    .line 2034150
    const-string v1, "num_appointments"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "page_id"

    iget-object v3, p0, LX/DkN;->c:LX/DkQ;

    invoke-virtual {v3}, LX/DkQ;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2034151
    iget-object v1, p0, LX/DkN;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2034152
    new-instance v1, LX/DkM;

    invoke-direct {v1}, LX/DkM;-><init>()V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2034153
    goto :goto_0

    .line 2034154
    :cond_1
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENT_REQUESTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-ne v0, v1, :cond_2

    .line 2034155
    new-instance v0, LX/Dkc;

    invoke-direct {v0}, LX/Dkc;-><init>()V

    move-object v0, v0

    .line 2034156
    const-string v1, "num_appointments"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "user_id"

    iget-object v3, p0, LX/DkN;->c:LX/DkQ;

    invoke-virtual {v3}, LX/DkQ;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2034157
    iget-object v1, p0, LX/DkN;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2034158
    new-instance v1, LX/DkH;

    invoke-direct {v1}, LX/DkH;-><init>()V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2034159
    goto/16 :goto_0

    .line 2034160
    :cond_2
    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->PAGE_ADMIN_QUERY_APPOINTMENTS_WITH_A_USER:Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;

    if-ne v0, v1, :cond_3

    .line 2034161
    new-instance v0, LX/Dkd;

    invoke-direct {v0}, LX/Dkd;-><init>()V

    move-object v0, v0

    .line 2034162
    const-string v1, "num_appointments"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "user_id"

    iget-object v3, p0, LX/DkN;->c:LX/DkQ;

    invoke-virtual {v3}, LX/DkQ;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2034163
    iget-object v1, p0, LX/DkN;->a:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2034164
    new-instance v1, LX/DkJ;

    invoke-direct {v1}, LX/DkJ;-><init>()V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2034165
    goto/16 :goto_0

    .line 2034166
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Query Scenario "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/AppointmentQueryConfig$QueryScenario;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not supported"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2034167
    iget-object v0, p0, LX/DkN;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2034168
    return-void
.end method

.method public final a(LX/Dj9;)V
    .locals 4

    .prologue
    .line 2034169
    iget-object v0, p0, LX/DkN;->b:LX/1Ck;

    const-string v1, "fetch_appointment_detail"

    new-instance v2, LX/Dk8;

    invoke-direct {v2, p0}, LX/Dk8;-><init>(LX/DkN;)V

    new-instance v3, LX/Dk9;

    invoke-direct {v3, p0, p1}, LX/Dk9;-><init>(LX/DkN;LX/Dj9;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2034170
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/Djl;Ljava/lang/String;LX/DjB;)V
    .locals 8

    .prologue
    .line 2034171
    iget-object v6, p0, LX/DkN;->b:LX/1Ck;

    const-string v7, "admin_create_new_appointment"

    new-instance v0, LX/DkC;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/DkC;-><init>(LX/DkN;Ljava/lang/String;Ljava/lang/String;LX/Djl;Ljava/lang/String;)V

    new-instance v1, LX/DkD;

    invoke-direct {v1, p0, p5}, LX/DkD;-><init>(LX/DkN;LX/DjB;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2034172
    return-void
.end method
