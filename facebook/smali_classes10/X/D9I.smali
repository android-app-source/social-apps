.class public LX/D9I;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final b:LX/0wT;

.field private static final c:LX/0wT;


# instance fields
.field private A:Z

.field private a:Z

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field public final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field public final h:LX/0wd;

.field public final i:LX/0wd;

.field public final j:LX/0wd;

.field public final k:LX/0wd;

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:I

.field private final s:I

.field private t:LX/0wW;

.field private u:Z

.field private v:Z

.field private w:LX/D9G;

.field private x:LX/D9H;

.field public y:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private z:Landroid/graphics/PointF;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1969968
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/D9I;->b:LX/0wT;

    .line 1969969
    const-wide v0, 0x4062c00000000000L    # 150.0

    const-wide/high16 v2, 0x4022000000000000L    # 9.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/D9I;->c:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1969966
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D9I;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1969967
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1969964
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D9I;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1969965
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11

    .prologue
    const-wide v8, 0x3fe6666666666666L    # 0.7

    const-wide/16 v6, 0x0

    const-wide v4, 0x3f747ae140000000L    # 0.004999999888241291

    .line 1969928
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1969929
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, LX/D9I;->z:Landroid/graphics/PointF;

    .line 1969930
    const-class v0, LX/D9I;

    invoke-static {v0, p0}, LX/D9I;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1969931
    const v0, 0x7f030c8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1969932
    const v0, 0x7f0d05d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D9I;->d:Landroid/view/View;

    .line 1969933
    const v0, 0x7f0d1edc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D9I;->e:Landroid/view/View;

    .line 1969934
    const v0, 0x7f0d1edd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D9I;->f:Landroid/view/View;

    .line 1969935
    const v0, 0x7f0d1edf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D9I;->g:Landroid/view/View;

    .line 1969936
    new-instance v0, LX/D9F;

    invoke-direct {v0, p0}, LX/D9F;-><init>(LX/D9I;)V

    .line 1969937
    iget-object v1, p0, LX/D9I;->t:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/D9I;->b:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/D9I;->h:LX/0wd;

    .line 1969938
    iget-object v1, p0, LX/D9I;->t:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/D9I;->b:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    iput-object v1, p0, LX/D9I;->i:LX/0wd;

    .line 1969939
    iget-object v1, p0, LX/D9I;->t:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/D9I;->c:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, LX/0wd;->b(D)LX/0wd;

    move-result-object v1

    .line 1969940
    iput-wide v4, v1, LX/0wd;->l:D

    .line 1969941
    move-object v1, v1

    .line 1969942
    iput-wide v4, v1, LX/0wd;->l:D

    .line 1969943
    move-object v1, v1

    .line 1969944
    iput-object v1, p0, LX/D9I;->j:LX/0wd;

    .line 1969945
    iget-object v1, p0, LX/D9I;->t:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    sget-object v2, LX/D9I;->b:LX/0wT;

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1969946
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1969947
    move-object v0, v0

    .line 1969948
    iput-wide v4, v0, LX/0wd;->l:D

    .line 1969949
    move-object v0, v0

    .line 1969950
    iput-wide v4, v0, LX/0wd;->l:D

    .line 1969951
    move-object v0, v0

    .line 1969952
    iput-object v0, p0, LX/D9I;->k:LX/0wd;

    .line 1969953
    invoke-virtual {p0}, LX/D9I;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b019f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9I;->l:I

    .line 1969954
    invoke-virtual {p0}, LX/D9I;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9I;->m:I

    .line 1969955
    invoke-virtual {p0}, LX/D9I;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/D9I;->n:I

    .line 1969956
    invoke-virtual {p0}, LX/D9I;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/D9I;->o:I

    .line 1969957
    invoke-virtual {p0}, LX/D9I;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/D9I;->p:I

    .line 1969958
    invoke-virtual {p0}, LX/D9I;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9I;->q:I

    .line 1969959
    invoke-virtual {p0}, LX/D9I;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9I;->r:I

    .line 1969960
    invoke-virtual {p0}, LX/D9I;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/D9I;->s:I

    .line 1969961
    invoke-direct {p0}, LX/D9I;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1969962
    invoke-direct {p0}, LX/D9I;->e()V

    .line 1969963
    return-void
.end method

.method private a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 1969903
    iget-boolean v0, p0, LX/D9I;->u:Z

    if-eqz v0, :cond_1

    .line 1969904
    iget-object v0, p0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_0

    .line 1969905
    iget-object v0, p0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1969906
    :goto_0
    return-object v0

    .line 1969907
    :cond_0
    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1969908
    :cond_1
    invoke-direct {p0}, LX/D9I;->b()V

    .line 1969909
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D9I;->v:Z

    .line 1969910
    iput-boolean v4, p0, LX/D9I;->u:Z

    .line 1969911
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1969912
    iget-object v0, p0, LX/D9I;->h:LX/0wd;

    invoke-virtual {v0, v6, v7}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    .line 1969913
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 1969914
    iget-object v0, p0, LX/D9I;->i:LX/0wd;

    iget v1, p0, LX/D9I;->m:I

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    .line 1969915
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 1969916
    iget-boolean v0, p0, LX/D9I;->A:Z

    if-eqz v0, :cond_2

    .line 1969917
    iget-object v0, p0, LX/D9I;->j:LX/0wd;

    .line 1969918
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 1969919
    move-object v0, v0

    .line 1969920
    const-wide v2, 0x3fe6666666666666L    # 0.7

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1969921
    :cond_2
    iget-object v0, p0, LX/D9I;->k:LX/0wd;

    .line 1969922
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 1969923
    move-object v0, v0

    .line 1969924
    invoke-virtual {v0, v6, v7}, LX/0wd;->b(D)LX/0wd;

    .line 1969925
    invoke-static {p0}, LX/D9I;->f(LX/D9I;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1969926
    iget-object v0, p0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x4ac4bd86

    invoke-static {v0, v5, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1969927
    :cond_3
    iget-object v0, p0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    goto :goto_0
.end method

.method private a(LX/0wW;LX/0Or;)V
    .locals 1
    .param p1    # LX/0wW;
        .annotation runtime Lcom/facebook/messaging/chatheads/annotations/ForChatHeads;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/chatheads/annotations/IsChatHeadsHardwareAccelerationDisabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0wW;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1969899
    iput-object p1, p0, LX/D9I;->t:LX/0wW;

    .line 1969900
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/D9I;->A:Z

    .line 1969901
    return-void

    .line 1969902
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/D9I;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, LX/D9I;

    invoke-static {v1}, LX/D9f;->a(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    const/16 v2, 0x1505

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/D9I;->a(LX/0wW;LX/0Or;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1969895
    iget-object v0, p0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_0

    .line 1969896
    iget-object v0, p0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0SQ;->cancel(Z)Z

    .line 1969897
    const/4 v0, 0x0

    iput-object v0, p0, LX/D9I;->y:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1969898
    :cond_0
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 1969881
    iget-object v0, p0, LX/D9I;->h:LX/0wd;

    iget-object v1, p0, LX/D9I;->h:LX/0wd;

    .line 1969882
    iget-wide v4, v1, LX/0wd;->i:D

    move-wide v2, v4

    .line 1969883
    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1969884
    iget-object v0, p0, LX/D9I;->i:LX/0wd;

    iget-object v1, p0, LX/D9I;->i:LX/0wd;

    .line 1969885
    iget-wide v4, v1, LX/0wd;->i:D

    move-wide v2, v4

    .line 1969886
    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1969887
    iget-object v0, p0, LX/D9I;->j:LX/0wd;

    iget-object v1, p0, LX/D9I;->j:LX/0wd;

    .line 1969888
    iget-wide v4, v1, LX/0wd;->i:D

    move-wide v2, v4

    .line 1969889
    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1969890
    iget-object v0, p0, LX/D9I;->k:LX/0wd;

    iget-object v1, p0, LX/D9I;->k:LX/0wd;

    .line 1969891
    iget-wide v4, v1, LX/0wd;->i:D

    move-wide v2, v4

    .line 1969892
    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1969893
    invoke-direct {p0}, LX/D9I;->b()V

    .line 1969894
    return-void
.end method

.method public static f(LX/D9I;)Z
    .locals 1

    .prologue
    .line 1969857
    iget-object v0, p0, LX/D9I;->h:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D9I;->i:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D9I;->j:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D9I;->k:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/D9I;)V
    .locals 4

    .prologue
    .line 1969876
    iget-boolean v0, p0, LX/D9I;->A:Z

    if-nez v0, :cond_0

    .line 1969877
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget-object v2, p0, LX/D9I;->k:LX/0wd;

    invoke-virtual {v2}, LX/0wd;->d()D

    move-result-wide v2

    sub-double/2addr v0, v2

    iget-object v2, p0, LX/D9I;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 1969878
    iget-object v1, p0, LX/D9I;->d:Landroid/view/View;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1969879
    :goto_0
    return-void

    .line 1969880
    :cond_0
    iget-object v0, p0, LX/D9I;->d:Landroid/view/View;

    iget-object v1, p0, LX/D9I;->k:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method private getCloseBaubleCenterXInScreen()I
    .locals 1

    .prologue
    .line 1969875
    invoke-virtual {p0}, LX/D9I;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private getCloseBaubleCenterYInScreen$133adb()F
    .locals 3

    .prologue
    .line 1969873
    iget-object v0, p0, LX/D9I;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 1969874
    invoke-virtual {p0}, LX/D9I;->getHeight()I

    move-result v1

    iget v2, p0, LX/D9I;->l:I

    add-int/2addr v1, v2

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    int-to-float v0, v0

    return v0
.end method

.method public static setBaubleX(LX/D9I;F)V
    .locals 1

    .prologue
    .line 1969871
    iget-object v0, p0, LX/D9I;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationX(F)V

    .line 1969872
    return-void
.end method

.method public static setBaubleY(LX/D9I;F)V
    .locals 1

    .prologue
    .line 1969869
    iget-object v0, p0, LX/D9I;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    .line 1969870
    return-void
.end method


# virtual methods
.method public getRestingCloseBaubleCenterYInScreen()F
    .locals 1

    .prologue
    .line 1969868
    invoke-direct {p0}, LX/D9I;->getCloseBaubleCenterYInScreen$133adb()F

    move-result v0

    return v0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1969864
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1969865
    if-eqz p1, :cond_0

    .line 1969866
    invoke-static {p0}, LX/D9I;->g(LX/D9I;)V

    .line 1969867
    :cond_0
    return-void
.end method

.method public setOnCloseBaublePositionListener(LX/D9G;)V
    .locals 0

    .prologue
    .line 1969862
    iput-object p1, p0, LX/D9I;->w:LX/D9G;

    .line 1969863
    return-void
.end method

.method public setOnCloseBaubleStateChangeListener(LX/D9H;)V
    .locals 0

    .prologue
    .line 1969860
    iput-object p1, p0, LX/D9I;->x:LX/D9H;

    .line 1969861
    return-void
.end method

.method public setShowEndCallBauble(Z)V
    .locals 0

    .prologue
    .line 1969858
    iput-boolean p1, p0, LX/D9I;->a:Z

    .line 1969859
    return-void
.end method
