.class public LX/DOL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/DOL;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1992300
    iput-object p1, p0, LX/DOL;->a:LX/0Or;

    .line 1992301
    return-void
.end method

.method public static a(LX/0QB;)LX/DOL;
    .locals 4

    .prologue
    .line 1992269
    sget-object v0, LX/DOL;->b:LX/DOL;

    if-nez v0, :cond_1

    .line 1992270
    const-class v1, LX/DOL;

    monitor-enter v1

    .line 1992271
    :try_start_0
    sget-object v0, LX/DOL;->b:LX/DOL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1992272
    if-eqz v2, :cond_0

    .line 1992273
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1992274
    new-instance v3, LX/DOL;

    const/16 p0, 0xc

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DOL;-><init>(LX/0Or;)V

    .line 1992275
    move-object v0, v3

    .line 1992276
    sput-object v0, LX/DOL;->b:LX/DOL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1992277
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1992278
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1992279
    :cond_1
    sget-object v0, LX/DOL;->b:LX/DOL;

    return-object v0

    .line 1992280
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1992281
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/DOL;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1992297
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/DOL;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1992298
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1992292
    invoke-static {p0}, LX/DOL;->a(LX/DOL;)Landroid/content/Intent;

    move-result-object v0

    .line 1992293
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1992294
    const-string v1, "group_feed_title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1992295
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUPS_MALL_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1992296
    return-object v0
.end method

.method public final b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1992287
    invoke-static {p0}, LX/DOL;->a(LX/DOL;)Landroid/content/Intent;

    move-result-object v0

    .line 1992288
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUPS_YOUR_POSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1992289
    const-string v1, "group_feed_model"

    invoke-static {v0, v1, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1992290
    const-string v1, "group_feed_id"

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1992291
    return-object v0
.end method

.method public final d(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1992282
    invoke-static {p0}, LX/DOL;->a(LX/DOL;)Landroid/content/Intent;

    move-result-object v0

    .line 1992283
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUPS_FOR_SALE_POSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1992284
    const-string v1, "group_feed_model"

    invoke-static {v0, v1, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1992285
    const-string v1, "group_feed_id"

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1992286
    return-object v0
.end method
