.class public final LX/DSF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;)V
    .locals 0

    .prologue
    .line 1999313
    iput-object p1, p0, LX/DSF;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 1999317
    iget-object v0, p0, LX/DSF;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    const/4 v1, 0x0

    .line 1999318
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c()LX/1Cv;

    move-result-object p1

    invoke-virtual {p1}, LX/1Cv;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1999319
    :cond_0
    :goto_0
    move v0, v1

    .line 1999320
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/DSF;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1999321
    iget-object v0, p0, LX/DSF;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o()V

    .line 1999322
    :cond_1
    return-void

    .line 1999323
    :cond_2
    if-lez p3, :cond_0

    if-lez p4, :cond_0

    .line 1999324
    add-int p1, p2, p3

    add-int/lit8 p1, p1, 0x3

    if-le p1, p4, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 1999314
    packed-switch p2, :pswitch_data_0

    .line 1999315
    :goto_0
    return-void

    .line 1999316
    :pswitch_0
    iget-object v0, p0, LX/DSF;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    iget-object v1, p0, LX/DSF;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->t:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a$redex0(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
