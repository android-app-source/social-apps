.class public final LX/Dxu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V
    .locals 0

    .prologue
    .line 2063475
    iput-object p1, p0, LX/Dxu;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x2420ea04

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2063476
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v0

    sget-object v2, LX/9jG;->PHOTO:LX/9jG;

    .line 2063477
    iput-object v2, v0, LX/9jF;->q:LX/9jG;

    .line 2063478
    move-object v2, v0

    .line 2063479
    iget-object v0, p0, LX/Dxu;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-boolean v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->W:Z

    if-nez v0, :cond_0

    .line 2063480
    iget-object v0, p0, LX/Dxu;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->V:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v0, :cond_1

    .line 2063481
    iget-object v0, p0, LX/Dxu;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->V:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 2063482
    iput-object v0, v2, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2063483
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Dxu;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Dxu;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-virtual {v2}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v2

    invoke-static {v3, v2}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x3

    iget-object v4, p0, LX/Dxu;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-interface {v0, v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2063484
    const v0, 0x3e352133

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2063485
    :cond_1
    iget-object v0, p0, LX/Dxu;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2063486
    iget-object v0, p0, LX/Dxu;->a:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    .line 2063487
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2063488
    new-instance v3, LX/5m9;

    invoke-direct {v3}, LX/5m9;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v4

    .line 2063489
    iput-object v4, v3, LX/5m9;->f:Ljava/lang/String;

    .line 2063490
    move-object v3, v3

    .line 2063491
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v0

    .line 2063492
    iput-object v0, v3, LX/5m9;->h:Ljava/lang/String;

    .line 2063493
    move-object v0, v3

    .line 2063494
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 2063495
    iput-object v0, v2, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2063496
    goto :goto_0
.end method
