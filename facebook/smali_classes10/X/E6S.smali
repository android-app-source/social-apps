.class public final LX/E6S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E6T;


# direct methods
.method public constructor <init>(LX/E6T;)V
    .locals 0

    .prologue
    .line 2080240
    iput-object p1, p0, LX/E6S;->a:LX/E6T;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x2eb8bad1    # -5.34912E10f

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2080233
    iget-object v0, p0, LX/E6S;->a:LX/E6T;

    iget-object v0, v0, LX/E6T;->e:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_0

    .line 2080234
    const v0, -0x3a079c9b

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2080235
    :goto_0
    return-void

    .line 2080236
    :cond_0
    iget-object v0, p0, LX/E6S;->a:LX/E6T;

    const v2, 0x7f0215de

    const v3, 0x7f082225

    iget-object v4, p0, LX/E6S;->a:LX/E6T;

    invoke-virtual {v4}, LX/E6T;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f040023

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, LX/E6T;->a$redex0(LX/E6T;IILandroid/view/animation/Animation;)V

    .line 2080237
    iget-object v0, p0, LX/E6S;->a:LX/E6T;

    iget-object v0, v0, LX/E6T;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9A1;

    iget-object v2, p0, LX/E6S;->a:LX/E6T;

    iget-object v2, v2, LX/E6T;->e:Lcom/facebook/graphql/model/GraphQLStory;

    sget-object v3, LX/5Ro;->REACTION:LX/5Ro;

    invoke-virtual {v0, v2, v3}, LX/9A1;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;)V

    .line 2080238
    iget-object v0, p0, LX/E6S;->a:LX/E6T;

    invoke-static {v0}, LX/E6T;->c$redex0(LX/E6T;)V

    .line 2080239
    const v0, -0x57045fcd

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
