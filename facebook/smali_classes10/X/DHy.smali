.class public LX/DHy;
.super LX/16T;
.source ""


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field private b:LX/10S;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1982730
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEARBY_FRIENDS_WAVE_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/DHy;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1982731
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1982732
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    iput-object v0, p0, LX/DHy;->b:LX/10S;

    .line 1982733
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 2

    .prologue
    .line 1982734
    iget-object v0, p0, LX/DHy;->b:LX/10S;

    .line 1982735
    sget-object v1, LX/10S;->INELIGIBLE:LX/10S;

    iput-object v1, p0, LX/DHy;->b:LX/10S;

    .line 1982736
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1982737
    const-string v0, "4541"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1982738
    sget-object v0, LX/DHy;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
