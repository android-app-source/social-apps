.class public final LX/E4s;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/E4u;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/E4t;

.field private b:[Ljava/lang/String;

.field private c:I

.field public d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 2077423
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2077424
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "delegate"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "unitComponentNode"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "interactionTracker"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hasInvalidate"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "expandableComponentPersistentState"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/E4s;->b:[Ljava/lang/String;

    .line 2077425
    iput v3, p0, LX/E4s;->c:I

    .line 2077426
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/E4s;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/E4s;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/E4s;LX/1De;IILX/E4t;)V
    .locals 1

    .prologue
    .line 2077419
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2077420
    iput-object p4, p0, LX/E4s;->a:LX/E4t;

    .line 2077421
    iget-object v0, p0, LX/E4s;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2077422
    return-void
.end method


# virtual methods
.method public final a(LX/1Pq;)LX/E4s;
    .locals 2

    .prologue
    .line 2077410
    iget-object v0, p0, LX/E4s;->a:LX/E4t;

    iput-object p1, v0, LX/E4t;->d:LX/1Pq;

    .line 2077411
    iget-object v0, p0, LX/E4s;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2077412
    return-object p0
.end method

.method public final a(LX/2ja;)LX/E4s;
    .locals 2

    .prologue
    .line 2077413
    iget-object v0, p0, LX/E4s;->a:LX/E4t;

    iput-object p1, v0, LX/E4t;->c:LX/2ja;

    .line 2077414
    iget-object v0, p0, LX/E4s;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2077415
    return-object p0
.end method

.method public final a(LX/E2a;)LX/E4s;
    .locals 2

    .prologue
    .line 2077416
    iget-object v0, p0, LX/E4s;->a:LX/E4t;

    iput-object p1, v0, LX/E4t;->e:LX/E2a;

    .line 2077417
    iget-object v0, p0, LX/E4s;->d:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2077418
    return-object p0
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/E4s;
    .locals 2

    .prologue
    .line 2077393
    iget-object v0, p0, LX/E4s;->a:LX/E4t;

    iput-object p1, v0, LX/E4t;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2077394
    iget-object v0, p0, LX/E4s;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2077395
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2077396
    invoke-super {p0}, LX/1X5;->a()V

    .line 2077397
    const/4 v0, 0x0

    iput-object v0, p0, LX/E4s;->a:LX/E4t;

    .line 2077398
    sget-object v0, LX/E4u;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2077399
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/E4u;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2077400
    iget-object v1, p0, LX/E4s;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/E4s;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/E4s;->c:I

    if-ge v1, v2, :cond_2

    .line 2077401
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2077402
    :goto_0
    iget v2, p0, LX/E4s;->c:I

    if-ge v0, v2, :cond_1

    .line 2077403
    iget-object v2, p0, LX/E4s;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2077404
    iget-object v2, p0, LX/E4s;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2077405
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2077406
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2077407
    :cond_2
    iget-object v0, p0, LX/E4s;->a:LX/E4t;

    .line 2077408
    invoke-virtual {p0}, LX/E4s;->a()V

    .line 2077409
    return-object v0
.end method
