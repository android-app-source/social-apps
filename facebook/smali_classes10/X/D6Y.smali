.class public LX/D6Y;
.super LX/7N3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lj;",
        ">",
        "LX/7N3",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final o:Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;

.field public p:LX/0sV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final r:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1965889
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D6Y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1965890
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1965891
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D6Y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1965892
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1965893
    invoke-direct {p0, p1, p2, p3}, LX/7N3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1965894
    const-class v0, LX/D6Y;

    invoke-static {v0, p0}, LX/D6Y;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1965895
    const v0, 0x7f0d090a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;

    iput-object v0, p0, LX/D6Y;->o:Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;

    .line 1965896
    const v0, 0x7f0d090c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    iput-object v0, p0, LX/D6Y;->r:Lcom/facebook/video/player/plugins/ChannelFeedInlineSaveButtonPlugin;

    .line 1965897
    const v0, 0x7f0d0909

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/plugins/ChannelFeedInlineSeekBarPlugin;

    .line 1965898
    const v1, 0x7f0d090b

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    .line 1965899
    invoke-virtual {v1, v0}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->setOtherControls(LX/7Mr;)V

    .line 1965900
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/D6Y;

    invoke-static {p0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v1

    check-cast v1, LX/0sV;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object p0

    check-cast p0, LX/0iA;

    iput-object v1, p1, LX/D6Y;->p:LX/0sV;

    iput-object p0, p1, LX/D6Y;->q:LX/0iA;

    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1965901
    if-eqz p2, :cond_0

    .line 1965902
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "IsAutoplayKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1965903
    if-eqz v0, :cond_1

    sget-object v0, LX/7OA;->AUTO_WITH_INITIALLY_HIDDEN:LX/7OA;

    :goto_0
    invoke-virtual {p0, v0}, LX/7N3;->a(LX/7OA;)V

    .line 1965904
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1965905
    if-nez v0, :cond_2

    .line 1965906
    :goto_1
    iget-object v0, p0, LX/D6Y;->q:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object p1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->CHANNEL_FEED_SAVE_OVERLAY_BUTTON_VISIBLE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, p1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-virtual {v0, v1}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1965907
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/D6b;

    invoke-direct {v1, p0}, LX/D6b;-><init>(LX/D6Y;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1965908
    :cond_0
    return-void

    .line 1965909
    :cond_1
    sget-object v0, LX/7OA;->AUTO_WITH_INITIALLY_VISIBLE:LX/7OA;

    goto :goto_0

    .line 1965910
    :cond_2
    invoke-static {v0}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/D6Y;->p:LX/0sV;

    iget-boolean v1, v1, LX/0sV;->t:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 1965911
    if-eqz v1, :cond_3

    const/16 v1, 0x8

    .line 1965912
    :goto_3
    iget-object p1, p0, LX/D6Y;->o:Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;

    invoke-virtual {p1, v1}, Lcom/facebook/video/player/plugins/FullscreenButtonPlugin;->setPluginVisibility(I)V

    goto :goto_1

    .line 1965913
    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1965914
    const v0, 0x7f03026c

    return v0
.end method
