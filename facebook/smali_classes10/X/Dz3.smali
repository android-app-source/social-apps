.class public LX/Dz3;
.super LX/Dyu;
.source ""


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066389
    invoke-direct {p0}, LX/Dyu;-><init>()V

    .line 2066390
    iput-object p1, p0, LX/Dz3;->a:Landroid/view/LayoutInflater;

    .line 2066391
    return-void
.end method


# virtual methods
.method public final a()LX/9jL;
    .locals 1

    .prologue
    .line 2066380
    sget-object v0, LX/9jL;->UseAsText:LX/9jL;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2066381
    if-nez p1, :cond_0

    .line 2066382
    iget-object v0, p0, LX/Dz3;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f0309ae

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 2066383
    new-instance v1, LX/Dz2;

    invoke-direct {v1}, LX/Dz2;-><init>()V

    .line 2066384
    const v0, 0x7f0d18af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LX/Dz2;->a:Landroid/widget/TextView;

    .line 2066385
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2066386
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dz2;

    .line 2066387
    iget-object v0, v0, LX/Dz2;->a:Landroid/widget/TextView;

    iget-object v1, p0, LX/Dz3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2066388
    return-object p1
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    .line 2066379
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2066378
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2066375
    iget-object v0, p0, LX/Dz3;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2066376
    new-instance v0, Landroid/util/Pair;

    sget-object v1, LX/9jL;->UseAsText:LX/9jL;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2066377
    :cond_0
    return-void
.end method
