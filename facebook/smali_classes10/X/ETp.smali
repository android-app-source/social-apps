.class public LX/ETp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ETh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/ETh",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:[Lcom/facebook/graphql/enums/GraphQLStorySeenState;

.field private static final c:[Lcom/facebook/graphql/enums/GraphQLStorySeenState;


# instance fields
.field public d:I

.field public final e:LX/ETQ;

.field private final f:LX/7Qv;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2125375
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_LIVE_NOTIFICATIONS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_CHANNEL_FEED_UNIT_NOTIF:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/ETp;->a:LX/0Px;

    .line 2125376
    new-array v0, v3, [Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    aput-object v1, v0, v2

    sput-object v0, LX/ETp;->b:[Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 2125377
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    aput-object v1, v0, v3

    sput-object v0, LX/ETp;->c:[Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-void
.end method

.method public constructor <init>(LX/ETQ;LX/7Qv;)V
    .locals 1
    .param p1    # LX/ETQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125470
    const/4 v0, -0x1

    iput v0, p0, LX/ETp;->d:I

    .line 2125471
    iput-object p1, p0, LX/ETp;->e:LX/ETQ;

    .line 2125472
    iput-object p2, p0, LX/ETp;->f:LX/7Qv;

    .line 2125473
    return-void
.end method

.method private static a(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 4

    .prologue
    .line 2125474
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2125475
    invoke-interface {v0}, LX/9uc;->bJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v1

    .line 2125476
    if-nez v1, :cond_0

    .line 2125477
    :goto_0
    return-object p0

    .line 2125478
    :cond_0
    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;->a(LX/9uc;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/9vT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;)LX/9vT;

    move-result-object v0

    invoke-static {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v1

    .line 2125479
    new-instance v2, LX/9wU;

    invoke-direct {v2}, LX/9wU;-><init>()V

    .line 2125480
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/9wU;->a:Ljava/lang/String;

    .line 2125481
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    iput-object v3, v2, LX/9wU;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 2125482
    move-object v1, v2

    .line 2125483
    iput-object p1, v1, LX/9wU;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 2125484
    move-object v1, v1

    .line 2125485
    invoke-virtual {v1}, LX/9wU;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v1

    .line 2125486
    iput-object v1, v0, LX/9vT;->bz:Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    .line 2125487
    move-object v0, v0

    .line 2125488
    invoke-virtual {v0}, LX/9vT;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v0

    .line 2125489
    invoke-virtual {p0, v0}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a(LX/9uc;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object p0

    goto :goto_0
.end method

.method public static varargs a(LX/0Pz;Lcom/facebook/video/videohome/data/VideoHomeItem;[Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/video/videohome/data/VideoHomeItem;",
            "[",
            "Lcom/facebook/graphql/enums/GraphQLStorySeenState;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2125503
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2125504
    invoke-interface {v0}, LX/9uc;->bJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v0

    .line 2125505
    invoke-static {v0, p2}, LX/ETp;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;[Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2125506
    const/4 v0, 0x0

    .line 2125507
    :goto_0
    return v0

    .line 2125508
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2125509
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2125490
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move v0, v1

    .line 2125491
    :goto_0
    invoke-virtual {p0}, LX/ETQ;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2125492
    invoke-virtual {p0, v0}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v3

    .line 2125493
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2125494
    invoke-static {v3, v2}, LX/ETp;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/ETQ;->b(ILcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v1

    .line 2125495
    :cond_0
    :goto_1
    return v1

    .line 2125496
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2125497
    invoke-virtual {v3}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v3

    invoke-static {v3, p1}, LX/ETp;->a(LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2125498
    const/4 v1, 0x1

    goto :goto_1

    .line 2125499
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static varargs a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;[Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z
    .locals 2
    .param p0    # Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2125500
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->c()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2125501
    :cond_0
    const/4 v0, 0x0

    .line 2125502
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 4

    .prologue
    .line 2125428
    sget-object v0, LX/ETp;->b:[Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 2125429
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2125430
    invoke-static {v2, p1, v0}, LX/ETp;->a(LX/0Pz;Lcom/facebook/video/videohome/data/VideoHomeItem;[Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    .line 2125431
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2125432
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    invoke-virtual {v1}, LX/ETQ;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2125433
    invoke-static {v2, v1, v0}, LX/ETp;->a(LX/0Pz;Lcom/facebook/video/videohome/data/VideoHomeItem;[Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    goto :goto_0

    .line 2125434
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2125435
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2125436
    const/4 v0, 0x0

    .line 2125437
    :goto_1
    return v0

    .line 2125438
    :cond_1
    iget-object v1, p0, LX/ETp;->f:LX/7Qv;

    const/4 v2, 0x0

    .line 2125439
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2125440
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2125441
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 2125442
    :cond_2
    new-instance v3, LX/4KQ;

    invoke-direct {v3}, LX/4KQ;-><init>()V

    .line 2125443
    const-string p0, "seen"

    invoke-virtual {v3, p0}, LX/4KQ;->a(Ljava/lang/String;)LX/4KQ;

    .line 2125444
    invoke-virtual {v3, v0}, LX/4KQ;->a(Ljava/util/List;)LX/4KQ;

    .line 2125445
    const-string p0, "video_home"

    invoke-virtual {v3, p0}, LX/4KQ;->b(Ljava/lang/String;)LX/4KQ;

    .line 2125446
    const-string p0, "VIDEO_HOME"

    .line 2125447
    const-string p1, "environment"

    invoke-virtual {v3, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2125448
    if-eqz v2, :cond_3

    .line 2125449
    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/4KQ;->a(Ljava/lang/Integer;)LX/4KQ;

    .line 2125450
    :cond_3
    new-instance p0, LX/7Qq;

    invoke-direct {p0}, LX/7Qq;-><init>()V

    move-object p0, p0

    .line 2125451
    const-string p1, "0"

    invoke-virtual {p0, p1, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2125452
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {v0, p0}, LX/7Qv;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/0jT;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v3

    .line 2125453
    iget-object p0, v1, LX/7Qv;->c:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_2
.end method

.method private d(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 3

    .prologue
    .line 2125454
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2125455
    invoke-interface {v0}, LX/9uc;->bJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v0

    .line 2125456
    sget-object v1, LX/ETp;->c:[Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {v0, v1}, LX/ETp;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;[Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2125457
    const/4 v0, 0x0

    .line 2125458
    :goto_0
    return v0

    .line 2125459
    :cond_0
    iget-object v1, p0, LX/ETp;->f:LX/7Qv;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2125460
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2125461
    new-instance v2, LX/4HP;

    invoke-direct {v2}, LX/4HP;-><init>()V

    .line 2125462
    invoke-virtual {v2, v0}, LX/4HP;->a(Ljava/lang/String;)LX/4HP;

    .line 2125463
    const-string p0, "video_home"

    invoke-virtual {v2, p0}, LX/4HP;->b(Ljava/lang/String;)LX/4HP;

    .line 2125464
    new-instance p0, LX/7Qp;

    invoke-direct {p0}, LX/7Qp;-><init>()V

    move-object p0, p0

    .line 2125465
    const-string p1, "0"

    invoke-virtual {p0, p1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2125466
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {p0, p1}, LX/7Qv;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)LX/0jT;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v2

    .line 2125467
    iget-object p0, v1, LX/7Qv;->c:LX/0tX;

    invoke-virtual {p0, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2125468
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static f(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;
    .locals 2

    .prologue
    .line 2125424
    iget-object v0, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2125425
    invoke-interface {v0}, LX/9uc;->bJ()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;

    move-result-object v0

    .line 2125426
    sget-object v1, LX/ETp;->b:[Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {v0, v1}, LX/ETp;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitVideoChannelFeedUnitComponentFragmentModel$NotifStoryModel;[Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2125427
    :goto_0
    return-object p0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {p0, v0}, LX/ETp;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2125410
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2125411
    invoke-interface {v1}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v1

    .line 2125412
    sget-object v2, LX/ETp;->a:LX/0Px;

    invoke-virtual {v2, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, LX/ETp;->c(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2125413
    :cond_0
    :goto_0
    return v0

    .line 2125414
    :cond_1
    iget-object v1, p0, LX/ETp;->e:LX/ETQ;

    invoke-virtual {v1, p1}, LX/ETQ;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)I

    move-result v1

    iput v1, p0, LX/ETp;->d:I

    .line 2125415
    iget v1, p0, LX/ETp;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    .line 2125416
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->r()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2125417
    invoke-virtual {p1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v3

    move v2, v1

    .line 2125418
    :goto_1
    invoke-virtual {v3}, LX/ETQ;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 2125419
    invoke-virtual {v3, v1}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v4

    invoke-static {v4}, LX/ETp;->f(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, LX/ETQ;->b(ILcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v4

    or-int/2addr v2, v4

    .line 2125420
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v2, v1

    .line 2125421
    :cond_3
    iget-object v1, p0, LX/ETp;->e:LX/ETQ;

    iget v3, p0, LX/ETp;->d:I

    invoke-static {p1}, LX/ETp;->f(Lcom/facebook/video/videohome/data/VideoHomeItem;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/ETQ;->b(ILcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v1

    or-int/2addr v1, v2

    .line 2125422
    move v1, v1

    .line 2125423
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2125408
    const/4 v0, -0x1

    iput v0, p0, LX/ETp;->d:I

    .line 2125409
    return-void
.end method

.method public final b(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z
    .locals 2

    .prologue
    .line 2125403
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2125404
    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    .line 2125405
    sget-object v1, LX/ETp;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, LX/ETp;->d(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2125406
    :cond_0
    const/4 v0, 0x0

    .line 2125407
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/ETp;->e:LX/ETQ;

    invoke-static {v0, p1}, LX/ETp;->a(LX/ETQ;Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final synthetic c(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2125402
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {p0, p1}, LX/ETp;->a(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x0

    .line 2125379
    iget v0, p0, LX/ETp;->d:I

    if-ne v0, v3, :cond_1

    .line 2125380
    :cond_0
    :goto_0
    return v1

    .line 2125381
    :cond_1
    iget v0, p0, LX/ETp;->d:I

    add-int/lit8 v2, v0, -0x1

    .line 2125382
    iput v3, p0, LX/ETp;->d:I

    .line 2125383
    if-ltz v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2125384
    iget-object v0, p0, LX/ETp;->e:LX/ETQ;

    invoke-virtual {v0}, LX/ETQ;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 2125385
    iget-object v0, p0, LX/ETp;->e:LX/ETQ;

    invoke-virtual {v0, v2}, LX/ETQ;->b(I)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    .line 2125386
    if-eqz v0, :cond_0

    .line 2125387
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2125388
    if-eqz v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->VIDEO_HOME_NOTIFICATIONS_HEADER:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2125389
    iget-object v4, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2125390
    invoke-interface {v4}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 2125391
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2125392
    invoke-interface {v3}, LX/9uc;->ac()I

    move-result v3

    if-nez v3, :cond_3

    .line 2125393
    :goto_2
    move-object v0, v0

    .line 2125394
    iget-object v1, p0, LX/ETp;->e:LX/ETQ;

    invoke-virtual {v1, v2, v0}, LX/ETQ;->b(ILcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2125395
    goto :goto_1

    .line 2125396
    :cond_3
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2125397
    invoke-static {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;->a(LX/9uc;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v3

    invoke-static {v3}, LX/9vT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;)LX/9vT;

    move-result-object v3

    .line 2125398
    iput v1, v3, LX/9vT;->U:I

    .line 2125399
    move-object v3, v3

    .line 2125400
    invoke-virtual {v3}, LX/9vT;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel;

    move-result-object v3

    .line 2125401
    invoke-virtual {v0, v3}, Lcom/facebook/video/videohome/data/VideoHomeItem;->a(LX/9uc;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v0

    goto :goto_2
.end method

.method public final synthetic d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2125378
    check-cast p1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {p0, p1}, LX/ETp;->b(Lcom/facebook/video/videohome/data/VideoHomeItem;)Z

    move-result v0

    return v0
.end method
