.class public final LX/DEK;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

.field public final synthetic c:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;LX/0Px;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;)V
    .locals 0

    .prologue
    .line 1976720
    iput-object p1, p0, LX/DEK;->c:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

    iput-object p2, p0, LX/DEK;->a:LX/0Px;

    iput-object p3, p0, LX/DEK;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<",
            "LX/1Pf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1976721
    iget-object v0, p0, LX/DEK;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/DEK;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 1976722
    iget-object v3, p0, LX/DEK;->c:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->b:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;

    new-instance v4, LX/DEP;

    iget-object v5, p0, LX/DEK;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-direct {v4, v5, v0}, LX/DEP;-><init>(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;)V

    invoke-virtual {p1, v3, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1976723
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1976724
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 1976725
    iget-object v0, p0, LX/DEK;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    if-eqz v0, :cond_0

    .line 1976726
    iget-object v0, p0, LX/DEK;->c:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->g:LX/1LV;

    iget-object v1, p0, LX/DEK;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v0, v1, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 1976727
    :cond_0
    iget-object v0, p0, LX/DEK;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    iget-object v1, p0, LX/DEK;->a:LX/0Px;

    invoke-static {v0, v1, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 1976728
    return-void
.end method
