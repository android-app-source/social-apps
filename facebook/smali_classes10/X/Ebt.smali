.class public final LX/Ebt;
.super LX/EWp;
.source ""

# interfaces
.implements LX/Ebr;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ebt;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/Ebt;


# instance fields
.field public bitField0_:I

.field public id_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public privateKey_:LX/EWc;

.field public publicKey_:LX/EWc;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2144834
    new-instance v0, LX/Ebq;

    invoke-direct {v0}, LX/Ebq;-><init>()V

    sput-object v0, LX/Ebt;->a:LX/EWZ;

    .line 2144835
    new-instance v0, LX/Ebt;

    invoke-direct {v0}, LX/Ebt;-><init>()V

    .line 2144836
    sput-object v0, LX/Ebt;->c:LX/Ebt;

    invoke-direct {v0}, LX/Ebt;->w()V

    .line 2144837
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2144876
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2144877
    iput-byte v0, p0, LX/Ebt;->memoizedIsInitialized:B

    .line 2144878
    iput v0, p0, LX/Ebt;->memoizedSerializedSize:I

    .line 2144879
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2144880
    iput-object v0, p0, LX/Ebt;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2144844
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2144845
    iput-byte v0, p0, LX/Ebt;->memoizedIsInitialized:B

    .line 2144846
    iput v0, p0, LX/Ebt;->memoizedSerializedSize:I

    .line 2144847
    invoke-direct {p0}, LX/Ebt;->w()V

    .line 2144848
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2144849
    const/4 v0, 0x0

    .line 2144850
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2144851
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2144852
    sparse-switch v3, :sswitch_data_0

    .line 2144853
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2144854
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2144855
    goto :goto_0

    .line 2144856
    :sswitch_1
    iget v3, p0, LX/Ebt;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/Ebt;->bitField0_:I

    .line 2144857
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/Ebt;->id_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2144858
    :catch_0
    move-exception v0

    .line 2144859
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2144860
    move-object v0, v0

    .line 2144861
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2144862
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/Ebt;->unknownFields:LX/EZQ;

    .line 2144863
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2144864
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/Ebt;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/Ebt;->bitField0_:I

    .line 2144865
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Ebt;->publicKey_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2144866
    :catch_1
    move-exception v0

    .line 2144867
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2144868
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2144869
    move-object v0, v1

    .line 2144870
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2144871
    :sswitch_3
    :try_start_4
    iget v3, p0, LX/Ebt;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/Ebt;->bitField0_:I

    .line 2144872
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/Ebt;->privateKey_:LX/EWc;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2144873
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ebt;->unknownFields:LX/EZQ;

    .line 2144874
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2144875
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2144839
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2144840
    iput-byte v1, p0, LX/Ebt;->memoizedIsInitialized:B

    .line 2144841
    iput v1, p0, LX/Ebt;->memoizedSerializedSize:I

    .line 2144842
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ebt;->unknownFields:LX/EZQ;

    .line 2144843
    return-void
.end method

.method private static a(LX/Ebt;)LX/Ebs;
    .locals 1

    .prologue
    .line 2144838
    invoke-static {}, LX/Ebs;->u()LX/Ebs;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/Ebs;->a(LX/Ebt;)LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method private w()V
    .locals 1

    .prologue
    .line 2144800
    const/4 v0, 0x0

    iput v0, p0, LX/Ebt;->id_:I

    .line 2144801
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebt;->publicKey_:LX/EWc;

    .line 2144802
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ebt;->privateKey_:LX/EWc;

    .line 2144803
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2144832
    new-instance v0, LX/Ebs;

    invoke-direct {v0, p1}, LX/Ebs;-><init>(LX/EYd;)V

    .line 2144833
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2144823
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2144824
    iget v0, p0, LX/Ebt;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2144825
    iget v0, p0, LX/Ebt;->id_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2144826
    :cond_0
    iget v0, p0, LX/Ebt;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2144827
    iget-object v0, p0, LX/Ebt;->publicKey_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2144828
    :cond_1
    iget v0, p0, LX/Ebt;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2144829
    const/4 v0, 0x3

    iget-object v1, p0, LX/Ebt;->privateKey_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2144830
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2144831
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2144881
    iget-byte v1, p0, LX/Ebt;->memoizedIsInitialized:B

    .line 2144882
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2144883
    :goto_0
    return v0

    .line 2144884
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2144885
    :cond_1
    iput-byte v0, p0, LX/Ebt;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2144811
    iget v0, p0, LX/Ebt;->memoizedSerializedSize:I

    .line 2144812
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2144813
    :goto_0
    return v0

    .line 2144814
    :cond_0
    const/4 v0, 0x0

    .line 2144815
    iget v1, p0, LX/Ebt;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2144816
    iget v0, p0, LX/Ebt;->id_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2144817
    :cond_1
    iget v1, p0, LX/Ebt;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2144818
    iget-object v1, p0, LX/Ebt;->publicKey_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2144819
    :cond_2
    iget v1, p0, LX/Ebt;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 2144820
    const/4 v1, 0x3

    iget-object v2, p0, LX/Ebt;->privateKey_:LX/EWc;

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2144821
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2144822
    iput v0, p0, LX/Ebt;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2144810
    iget-object v0, p0, LX/Ebt;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2144809
    sget-object v0, LX/Eck;->p:LX/EYn;

    const-class v1, LX/Ebt;

    const-class v2, LX/Ebs;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ebt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2144808
    sget-object v0, LX/Ebt;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2144807
    invoke-static {p0}, LX/Ebt;->a(LX/Ebt;)LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2144806
    invoke-static {}, LX/Ebs;->u()LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2144805
    invoke-static {p0}, LX/Ebt;->a(LX/Ebt;)LX/Ebs;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2144804
    sget-object v0, LX/Ebt;->c:LX/Ebt;

    return-object v0
.end method
