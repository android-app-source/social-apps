.class public final LX/EyD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Eus;

.field public final synthetic b:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;LX/Eus;)V
    .locals 0

    .prologue
    .line 2185835
    iput-object p1, p0, LX/EyD;->b:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iput-object p2, p0, LX/EyD;->a:LX/Eus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x5a4c4f9e

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2185826
    iget-object v1, p0, LX/EyD;->a:LX/Eus;

    invoke-virtual {v1}, LX/Eus;->a()J

    move-result-wide v2

    .line 2185827
    iget-object v1, p0, LX/EyD;->b:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v1, v1, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->h:LX/2dj;

    sget-object v4, LX/2hC;->FRIENDS_CENTER:LX/2hC;

    invoke-virtual {v1, v2, v3, v4}, LX/2dj;->a(JLX/2hC;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2185828
    iget-object v1, p0, LX/EyD;->b:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v1, v1, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->i:LX/2do;

    new-instance v4, LX/2iB;

    invoke-direct {v4, v2, v3}, LX/2iB;-><init>(J)V

    invoke-virtual {v1, v4}, LX/0b4;->a(LX/0b7;)V

    .line 2185829
    iget-object v1, p0, LX/EyD;->b:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(J)I

    move-result v1

    .line 2185830
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2185831
    iget-object v2, p0, LX/EyD;->b:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v2, v2, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2185832
    iget-object v2, p0, LX/EyD;->b:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-virtual {v2, v1}, LX/1OM;->d(I)V

    .line 2185833
    iget-object v2, p0, LX/EyD;->b:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v3, p0, LX/EyD;->b:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    invoke-virtual {v2, v1, v3}, LX/1OM;->a(II)V

    .line 2185834
    :cond_0
    const v1, -0x231d93ac

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
