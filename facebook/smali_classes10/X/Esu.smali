.class public final LX/Esu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/1Cn;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

.field public final synthetic d:Lcom/facebook/content/SecureContextHelper;

.field public final synthetic e:LX/C51;

.field public final synthetic f:LX/03V;

.field public final synthetic g:Ljava/lang/String;

.field public final synthetic h:LX/1Nq;

.field public final synthetic i:LX/1Kf;

.field public final synthetic j:LX/1Ps;

.field public final synthetic k:Ljava/lang/String;

.field public final synthetic l:LX/Es5;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Cn;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Lcom/facebook/content/SecureContextHelper;LX/C51;LX/03V;Ljava/lang/String;LX/1Nq;LX/1Kf;LX/1Ps;Ljava/lang/String;LX/Es5;)V
    .locals 0

    .prologue
    .line 2176630
    iput-object p1, p0, LX/Esu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p2, p0, LX/Esu;->b:LX/1Cn;

    iput-object p3, p0, LX/Esu;->c:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object p4, p0, LX/Esu;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, LX/Esu;->e:LX/C51;

    iput-object p6, p0, LX/Esu;->f:LX/03V;

    iput-object p7, p0, LX/Esu;->g:Ljava/lang/String;

    iput-object p8, p0, LX/Esu;->h:LX/1Nq;

    iput-object p9, p0, LX/Esu;->i:LX/1Kf;

    iput-object p10, p0, LX/Esu;->j:LX/1Ps;

    iput-object p11, p0, LX/Esu;->k:Ljava/lang/String;

    iput-object p12, p0, LX/Esu;->l:LX/Es5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x1b3e0eda

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2176631
    iget-object v0, p0, LX/Esu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176632
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 2176633
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    .line 2176634
    iget-object v0, p0, LX/Esu;->b:LX/1Cn;

    iget-object v3, p0, LX/Esu;->c:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/CEz;->PROMOTION:LX/CEz;

    invoke-virtual {v0, v3, v2, v4}, LX/1Cn;->b(Ljava/lang/String;Ljava/lang/String;LX/CEz;)V

    .line 2176635
    const v0, 0x7f0d2f06

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2176636
    new-instance v3, LX/6WS;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2176637
    const v4, 0x7f0314b7

    invoke-virtual {v3, v4}, LX/5OM;->b(I)V

    .line 2176638
    new-instance v4, LX/Est;

    invoke-direct {v4, p0, p1, v2}, LX/Est;-><init>(LX/Esu;Landroid/view/View;Ljava/lang/String;)V

    .line 2176639
    iput-object v4, v3, LX/5OM;->p:LX/5OO;

    .line 2176640
    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v3, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2176641
    const v0, -0x505487ac

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    :cond_0
    move-object v0, p1

    .line 2176642
    goto :goto_0
.end method
