.class public LX/ClD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile r:LX/ClD;


# instance fields
.field public final a:LX/Cig;

.field private final b:LX/0So;

.field private final c:LX/03V;

.field public final d:LX/8bM;

.field public final e:LX/Ckt;

.field public final f:LX/0Xl;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/ClA;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:J

.field public j:J

.field public k:J

.field public l:J

.field public m:Landroid/content/Context;

.field public n:J

.field public o:LX/ClC;

.field public p:LX/0Yb;

.field public q:LX/0Yb;


# direct methods
.method public constructor <init>(LX/8bM;LX/0So;LX/03V;LX/Cig;LX/Ckt;LX/0Xl;)V
    .locals 4
    .param p6    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 1932079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932080
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/ClD;->g:Ljava/util/List;

    .line 1932081
    iput-wide v2, p0, LX/ClD;->k:J

    .line 1932082
    iput-wide v2, p0, LX/ClD;->l:J

    .line 1932083
    sget-object v0, LX/ClC;->IDLE:LX/ClC;

    iput-object v0, p0, LX/ClD;->o:LX/ClC;

    .line 1932084
    iput-object p4, p0, LX/ClD;->a:LX/Cig;

    .line 1932085
    iput-object p1, p0, LX/ClD;->d:LX/8bM;

    .line 1932086
    iput-object p2, p0, LX/ClD;->b:LX/0So;

    .line 1932087
    iput-object p3, p0, LX/ClD;->c:LX/03V;

    .line 1932088
    iput-object p5, p0, LX/ClD;->e:LX/Ckt;

    .line 1932089
    iput-object p6, p0, LX/ClD;->f:LX/0Xl;

    .line 1932090
    return-void
.end method

.method public static a(LX/0QB;)LX/ClD;
    .locals 10

    .prologue
    .line 1932177
    sget-object v0, LX/ClD;->r:LX/ClD;

    if-nez v0, :cond_1

    .line 1932178
    const-class v1, LX/ClD;

    monitor-enter v1

    .line 1932179
    :try_start_0
    sget-object v0, LX/ClD;->r:LX/ClD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1932180
    if-eqz v2, :cond_0

    .line 1932181
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1932182
    new-instance v3, LX/ClD;

    invoke-static {v0}, LX/8bM;->a(LX/0QB;)LX/8bM;

    move-result-object v4

    check-cast v4, LX/8bM;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/Cig;->a(LX/0QB;)LX/Cig;

    move-result-object v7

    check-cast v7, LX/Cig;

    invoke-static {v0}, LX/Ckt;->a(LX/0QB;)LX/Ckt;

    move-result-object v8

    check-cast v8, LX/Ckt;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v9

    check-cast v9, LX/0Xl;

    invoke-direct/range {v3 .. v9}, LX/ClD;-><init>(LX/8bM;LX/0So;LX/03V;LX/Cig;LX/Ckt;LX/0Xl;)V

    .line 1932183
    move-object v0, v3

    .line 1932184
    sput-object v0, LX/ClD;->r:LX/ClD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1932185
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1932186
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1932187
    :cond_1
    sget-object v0, LX/ClD;->r:LX/ClD;

    return-object v0

    .line 1932188
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1932189
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/ClD;Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1932159
    if-nez p1, :cond_0

    .line 1932160
    const/4 p2, 0x0

    .line 1932161
    :goto_0
    return-object p2

    .line 1932162
    :cond_0
    invoke-static {p0, p1}, LX/ClD;->e(LX/ClD;Landroid/content/Context;)LX/ClA;

    move-result-object v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1932163
    if-eqz v0, :cond_2

    .line 1932164
    invoke-static {p0, p1}, LX/ClD;->e(LX/ClD;Landroid/content/Context;)LX/ClA;

    move-result-object v0

    .line 1932165
    iget-object v1, v0, LX/ClA;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1932166
    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1932167
    invoke-virtual {v0, p1, p3}, LX/ClA;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 1932168
    :cond_1
    invoke-virtual {v0, p1}, LX/ClA;->b(Landroid/content/Context;)V

    .line 1932169
    :cond_2
    iget-object v0, p0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClA;

    .line 1932170
    iget-object v2, v0, LX/ClA;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1932171
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1932172
    invoke-virtual {v0, p1, p3}, LX/ClA;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 1932173
    :cond_4
    new-instance v0, LX/ClA;

    invoke-direct {v0}, LX/ClA;-><init>()V

    .line 1932174
    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, LX/ClA;->a(Landroid/content/Context;I)V

    .line 1932175
    iget-object v1, p0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1932176
    iget-object p2, v0, LX/ClA;->a:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/ClD;LX/ClB;)V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 1932150
    iget-object v0, p0, LX/ClD;->o:LX/ClC;

    sget-object v1, LX/ClC;->TRACKING:LX/ClC;

    if-eq v0, v1, :cond_0

    .line 1932151
    :goto_0
    return-void

    .line 1932152
    :cond_0
    sget-object v0, LX/ClC;->PAUSED:LX/ClC;

    iput-object v0, p0, LX/ClD;->o:LX/ClC;

    .line 1932153
    iget-wide v0, p0, LX/ClD;->k:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 1932154
    iget-object v0, p0, LX/ClD;->c:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v4, "pause() when resume() not called"

    invoke-static {v1, v4}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1932155
    :cond_1
    iget-object v0, p0, LX/ClD;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v4, p0, LX/ClD;->k:J

    sub-long v4, v0, v4

    sget-object v0, LX/ClB;->APP_BACKGROUNDED:LX/ClB;

    if-ne p1, v0, :cond_2

    const-wide/16 v0, 0x1388

    :goto_1
    sub-long v0, v4, v0

    long-to-double v0, v0

    .line 1932156
    iget-wide v6, p0, LX/ClD;->i:J

    long-to-double v6, v6

    add-double/2addr v6, v0

    double-to-long v6, v6

    iput-wide v6, p0, LX/ClD;->i:J

    .line 1932157
    iput-wide v2, p0, LX/ClD;->k:J

    goto :goto_0

    :cond_2
    move-wide v0, v2

    .line 1932158
    goto :goto_1
.end method

.method public static e(LX/ClD;Landroid/content/Context;)LX/ClA;
    .locals 3

    .prologue
    .line 1932147
    iget-object v0, p0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClA;

    .line 1932148
    invoke-static {v0, p1}, LX/ClA;->d(LX/ClA;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1932149
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/ClD;)V
    .locals 1

    .prologue
    .line 1932142
    iget-object v0, p0, LX/ClD;->p:LX/0Yb;

    if-eqz v0, :cond_0

    .line 1932143
    iget-object v0, p0, LX/ClD;->p:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1932144
    :cond_0
    iget-object v0, p0, LX/ClD;->q:LX/0Yb;

    if-eqz v0, :cond_1

    .line 1932145
    iget-object v0, p0, LX/ClD;->q:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1932146
    :cond_1
    return-void
.end method

.method public static i(LX/ClD;)V
    .locals 2

    .prologue
    .line 1932138
    iget-object v0, p0, LX/ClD;->o:LX/ClC;

    sget-object v1, LX/ClC;->TRACKING:LX/ClC;

    if-ne v0, v1, :cond_0

    .line 1932139
    :goto_0
    return-void

    .line 1932140
    :cond_0
    sget-object v0, LX/ClC;->TRACKING:LX/ClC;

    iput-object v0, p0, LX/ClD;->o:LX/ClC;

    .line 1932141
    iget-object v0, p0, LX/ClD;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/ClD;->k:J

    goto :goto_0
.end method

.method public static j(LX/ClD;)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    .line 1932133
    iget-wide v0, p0, LX/ClD;->l:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 1932134
    iget-object v0, p0, LX/ClD;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/ClD;->l:J

    sub-long/2addr v0, v2

    .line 1932135
    iget-wide v6, p0, LX/ClD;->j:J

    add-long/2addr v6, v0

    iput-wide v6, p0, LX/ClD;->j:J

    .line 1932136
    :cond_0
    iput-wide v4, p0, LX/ClD;->l:J

    .line 1932137
    return-void
.end method

.method public static k(LX/ClD;)V
    .locals 2

    .prologue
    .line 1932131
    iget-object v0, p0, LX/ClD;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/ClD;->l:J

    .line 1932132
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 1932123
    invoke-static {p0, p1}, LX/ClD;->e(LX/ClD;Landroid/content/Context;)LX/ClA;

    move-result-object v0

    .line 1932124
    if-eqz v0, :cond_1

    .line 1932125
    invoke-static {v0}, LX/ClA;->c(LX/ClA;)V

    .line 1932126
    iget-object v1, v0, LX/ClA;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1932127
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 1932128
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1932129
    :goto_0
    move v0, v1

    .line 1932130
    :goto_1
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1

    :cond_2
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public final a()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1932119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1932120
    iget-object v1, p0, LX/ClD;->h:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1932121
    const-string v1, "instant_articles_session_id"

    iget-object v2, p0, LX/ClD;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932122
    :cond_0
    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1932115
    invoke-static {p0, p1}, LX/ClD;->e(LX/ClD;Landroid/content/Context;)LX/ClA;

    move-result-object v0

    .line 1932116
    if-eqz v0, :cond_0

    .line 1932117
    iget-object p0, v0, LX/ClA;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1932118
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1932091
    iget-object v0, p0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClA;

    .line 1932092
    invoke-static {v0, p1}, LX/ClA;->d(LX/ClA;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1932093
    invoke-virtual {v0, p1}, LX/ClA;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 1932094
    :cond_1
    const/4 v2, 0x0

    iget-object v0, p0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    :goto_1
    if-ge v2, v1, :cond_2

    .line 1932095
    iget-object v0, p0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClA;

    .line 1932096
    invoke-static {v0}, LX/ClA;->c(LX/ClA;)V

    .line 1932097
    iget-object p1, v0, LX/ClA;->b:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    move v0, p1

    .line 1932098
    if-nez v0, :cond_5

    .line 1932099
    iget-object v0, p0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1932100
    add-int/lit8 v2, v2, -0x1

    .line 1932101
    add-int/lit8 v0, v1, -0x1

    move v1, v2

    .line 1932102
    :goto_2
    add-int/lit8 v2, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1932103
    :cond_2
    iget-object v0, p0, LX/ClD;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1932104
    sget-object v0, LX/ClB;->USER_EXITED_SESSION:LX/ClB;

    invoke-static {p0, v0}, LX/ClD;->a(LX/ClD;LX/ClB;)V

    .line 1932105
    invoke-static {p0}, LX/ClD;->j(LX/ClD;)V

    .line 1932106
    sget-object v0, LX/ClC;->IDLE:LX/ClC;

    iput-object v0, p0, LX/ClD;->o:LX/ClC;

    .line 1932107
    iget-object v0, p0, LX/ClD;->d:LX/8bM;

    .line 1932108
    if-eqz p0, :cond_3

    .line 1932109
    iget-object v1, v0, LX/8bM;->c:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1932110
    :cond_3
    iget-object v0, p0, LX/ClD;->a:LX/Cig;

    new-instance v1, LX/Ciq;

    invoke-direct {v1}, LX/Ciq;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1932111
    iget-object v0, p0, LX/ClD;->e:LX/Ckt;

    iget-object v1, p0, LX/ClD;->m:Landroid/content/Context;

    invoke-virtual {v0, p0, v1}, LX/Ckt;->a(LX/ClD;Landroid/content/Context;)V

    .line 1932112
    const/4 v0, 0x0

    iput-object v0, p0, LX/ClD;->m:Landroid/content/Context;

    .line 1932113
    invoke-static {p0}, LX/ClD;->h(LX/ClD;)V

    .line 1932114
    :cond_4
    return-void

    :cond_5
    move v0, v1

    move v1, v2

    goto :goto_2
.end method
