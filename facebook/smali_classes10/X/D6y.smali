.class public final LX/D6y;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ow;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;)V
    .locals 0

    .prologue
    .line 1966575
    iput-object p1, p0, LX/D6y;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1966576
    const-class v0, LX/2ow;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1966559
    check-cast p1, LX/2ow;

    .line 1966560
    sget-object v0, LX/D6w;->b:[I

    iget-object v1, p1, LX/2ow;->a:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1966561
    :goto_0
    return-void

    .line 1966562
    :pswitch_0
    iget-object v0, p0, LX/D6y;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    .line 1966563
    invoke-virtual {v0}, LX/3Ga;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1966564
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->t:Landroid/view/View;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1966565
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v1, :cond_0

    .line 1966566
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object p0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, p0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1966567
    iget-object v1, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1966568
    :cond_0
    goto :goto_0

    .line 1966569
    :pswitch_1
    iget-object v0, p0, LX/D6y;->a:Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;

    iget-object v1, p1, LX/2ow;->c:LX/7M6;

    iget-object v1, v1, LX/7M6;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1966570
    invoke-virtual {v0}, LX/3Ga;->g()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1966571
    iget-object p0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->t:Landroid/view/View;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1966572
    iget-object p0, v0, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->u:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz p0, :cond_1

    if-eqz v1, :cond_1

    .line 1966573
    invoke-static {v0, v1}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;->b(Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdFullScreenPlugin;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1966574
    :cond_1
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
