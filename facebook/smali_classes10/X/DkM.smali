.class public final LX/DkM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2034115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2034116
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2034117
    if-eqz p1, :cond_0

    .line 2034118
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034119
    if-eqz v0, :cond_0

    .line 2034120
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034121
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2034122
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034123
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;->j()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel$FutureConfirmedAppointmentsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2034124
    :cond_0
    const/4 v0, 0x0

    .line 2034125
    :goto_0
    return-object v0

    .line 2034126
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2034127
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2034128
    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel;->a()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel;->j()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel$FutureConfirmedAppointmentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$UserAppointmentsWithAPageQueryModel$ActorModel$FutureConfirmedAppointmentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2034129
    new-instance v0, LX/DkL;

    invoke-direct {v0, p0}, LX/DkL;-><init>(LX/DkM;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2034130
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
