.class public final enum LX/Dvw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dvw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dvw;

.field public static final enum LANDSCAPE:LX/Dvw;

.field public static final enum PORTRAIT:LX/Dvw;

.field public static final enum SQUARE:LX/Dvw;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2059416
    new-instance v0, LX/Dvw;

    const-string v1, "SQUARE"

    invoke-direct {v0, v1, v2}, LX/Dvw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dvw;->SQUARE:LX/Dvw;

    .line 2059417
    new-instance v0, LX/Dvw;

    const-string v1, "PORTRAIT"

    invoke-direct {v0, v1, v3}, LX/Dvw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dvw;->PORTRAIT:LX/Dvw;

    .line 2059418
    new-instance v0, LX/Dvw;

    const-string v1, "LANDSCAPE"

    invoke-direct {v0, v1, v4}, LX/Dvw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dvw;->LANDSCAPE:LX/Dvw;

    .line 2059419
    const/4 v0, 0x3

    new-array v0, v0, [LX/Dvw;

    sget-object v1, LX/Dvw;->SQUARE:LX/Dvw;

    aput-object v1, v0, v2

    sget-object v1, LX/Dvw;->PORTRAIT:LX/Dvw;

    aput-object v1, v0, v3

    sget-object v1, LX/Dvw;->LANDSCAPE:LX/Dvw;

    aput-object v1, v0, v4

    sput-object v0, LX/Dvw;->$VALUES:[LX/Dvw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2059420
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dvw;
    .locals 1

    .prologue
    .line 2059421
    const-class v0, LX/Dvw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dvw;

    return-object v0
.end method

.method public static values()[LX/Dvw;
    .locals 1

    .prologue
    .line 2059422
    sget-object v0, LX/Dvw;->$VALUES:[LX/Dvw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dvw;

    return-object v0
.end method
