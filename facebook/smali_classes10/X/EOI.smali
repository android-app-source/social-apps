.class public final LX/EOI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:LX/1Ps;

.field public final synthetic d:LX/CzL;

.field public final synthetic e:LX/EOJ;


# direct methods
.method public constructor <init>(LX/EOJ;ZLcom/facebook/graphql/model/GraphQLStory;LX/1Ps;LX/CzL;)V
    .locals 0

    .prologue
    .line 2114257
    iput-object p1, p0, LX/EOI;->e:LX/EOJ;

    iput-boolean p2, p0, LX/EOI;->a:Z

    iput-object p3, p0, LX/EOI;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p4, p0, LX/EOI;->c:LX/1Ps;

    iput-object p5, p0, LX/EOI;->d:LX/CzL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v0, 0x1

    const v1, 0x52836a1e

    invoke-static {v13, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2114258
    iget-boolean v0, p0, LX/EOI;->a:Z

    if-nez v0, :cond_0

    .line 2114259
    iget-object v0, p0, LX/EOI;->e:LX/EOJ;

    iget-object v0, v0, LX/EOJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/EOI;->e:LX/EOJ;

    iget-object v1, v1, LX/EOJ;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nB;

    iget-object v2, p0, LX/EOI;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v2}, LX/1nB;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2114260
    :cond_0
    iget-object v0, p0, LX/EOI;->e:LX/EOJ;

    iget-object v0, v0, LX/EOJ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/CvY;

    iget-object v0, p0, LX/EOI;->c:LX/1Ps;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    sget-object v10, LX/8ch;->CLICK:LX/8ch;

    iget-object v0, p0, LX/EOI;->c:LX/1Ps;

    check-cast v0, LX/CxP;

    iget-object v1, p0, LX/EOI;->d:LX/CzL;

    invoke-interface {v0, v1}, LX/CxP;->b(LX/CzL;)I

    move-result v11

    iget-object v12, p0, LX/EOI;->d:LX/CzL;

    iget-object v0, p0, LX/EOI;->e:LX/EOJ;

    iget-object v0, v0, LX/EOJ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/EOI;->c:LX/1Ps;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    iget-object v1, p0, LX/EOI;->d:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    iget-object v2, p0, LX/EOI;->c:LX/1Ps;

    check-cast v2, LX/CxV;

    invoke-interface {v2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v2

    .line 2114261
    iget-object v3, v2, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v2, v3

    .line 2114262
    iget-object v3, p0, LX/EOI;->d:LX/CzL;

    .line 2114263
    iget-object v4, v3, LX/CzL;->d:LX/0am;

    move-object v3, v4

    .line 2114264
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, LX/EOI;->c:LX/1Ps;

    check-cast v4, LX/CxP;

    iget-object v5, p0, LX/EOI;->d:LX/CzL;

    invoke-interface {v4, v5}, LX/CxP;->b(LX/CzL;)I

    move-result v4

    iget-object v5, p0, LX/EOI;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/EOI;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;ILjava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v0, v7

    move-object v1, v9

    move-object v2, v10

    move v3, v11

    move-object v4, v12

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2114265
    const v0, -0x8cda773

    invoke-static {v13, v13, v0, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
