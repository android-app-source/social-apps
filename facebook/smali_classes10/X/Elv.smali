.class public LX/Elv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Elu;


# instance fields
.field private final a:LX/0W3;

.field private final b:LX/Elm;


# direct methods
.method public constructor <init>(LX/0W3;LX/Elm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2164901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2164902
    iput-object p1, p0, LX/Elv;->a:LX/0W3;

    .line 2164903
    iput-object p2, p0, LX/Elv;->b:LX/Elm;

    .line 2164904
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 2164905
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 2164906
    iget-object v0, p0, LX/Elv;->a:LX/0W3;

    sget-wide v4, LX/0X5;->hV:J

    const-string v1, ""

    invoke-interface {v0, v4, v5, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2164907
    const/16 v1, 0x2c

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    .line 2164908
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2164909
    const/16 v1, 0x3a

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v5

    .line 2164910
    const/4 v0, 0x0

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2164911
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v6, :cond_3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2164912
    :goto_0
    invoke-static {v0, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2164913
    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2164914
    const-string v2, "m.facebook.com"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2164915
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    move-object v0, v0

    .line 2164916
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2164917
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 2164918
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2164919
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2164920
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2164921
    invoke-static {v0}, LX/Elm;->b(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 2164922
    :cond_2
    return-object v2

    :cond_3
    move-object v1, v2

    .line 2164923
    goto :goto_0
.end method
