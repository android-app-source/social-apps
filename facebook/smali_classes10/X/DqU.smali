.class public LX/DqU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/DqU;


# instance fields
.field public final a:LX/1s9;


# direct methods
.method public constructor <init>(LX/1s9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048368
    iput-object p1, p0, LX/DqU;->a:LX/1s9;

    .line 2048369
    return-void
.end method

.method public static a(LX/0QB;)LX/DqU;
    .locals 4

    .prologue
    .line 2048370
    sget-object v0, LX/DqU;->b:LX/DqU;

    if-nez v0, :cond_1

    .line 2048371
    const-class v1, LX/DqU;

    monitor-enter v1

    .line 2048372
    :try_start_0
    sget-object v0, LX/DqU;->b:LX/DqU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2048373
    if-eqz v2, :cond_0

    .line 2048374
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2048375
    new-instance p0, LX/DqU;

    invoke-static {v0}, LX/1s9;->a(LX/0QB;)LX/1s9;

    move-result-object v3

    check-cast v3, LX/1s9;

    invoke-direct {p0, v3}, LX/DqU;-><init>(LX/1s9;)V

    .line 2048376
    move-object v0, p0

    .line 2048377
    sput-object v0, LX/DqU;->b:LX/DqU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2048378
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2048379
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2048380
    :cond_1
    sget-object v0, LX/DqU;->b:LX/DqU;

    return-object v0

    .line 2048381
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2048382
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2048383
    invoke-static {p1}, LX/Cfu;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/0Px;

    move-result-object v2

    .line 2048384
    if-nez v2, :cond_0

    .line 2048385
    const-string v0, "ERROR_INVALID_COMPONENT"

    .line 2048386
    :goto_0
    return-object v0

    .line 2048387
    :cond_0
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2048388
    const-string v0, "EMPTY_SUB_COMPONENTS"

    goto :goto_0

    .line 2048389
    :cond_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2048390
    iget-object v4, p0, LX/DqU;->a:LX/1s9;

    invoke-virtual {v4, v0}, LX/1s9;->a(LX/9oK;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2048391
    const-string v0, "UNSUPPORTED_COMPONENT_STYLE"

    goto :goto_0

    .line 2048392
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2048393
    :cond_3
    const-string v0, "SUCCESS"

    goto :goto_0
.end method
