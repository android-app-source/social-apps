.class public final enum LX/EYN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EYN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EYN;

.field public static final enum BOOLEAN:LX/EYN;

.field public static final enum BYTE_STRING:LX/EYN;

.field public static final enum DOUBLE:LX/EYN;

.field public static final enum ENUM:LX/EYN;

.field public static final enum FLOAT:LX/EYN;

.field public static final enum INT:LX/EYN;

.field public static final enum LONG:LX/EYN;

.field public static final enum MESSAGE:LX/EYN;

.field public static final enum STRING:LX/EYN;


# instance fields
.field public final defaultDefault:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2137041
    new-instance v0, LX/EYN;

    const-string v1, "INT"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, LX/EYN;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EYN;->INT:LX/EYN;

    .line 2137042
    new-instance v0, LX/EYN;

    const-string v1, "LONG"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, LX/EYN;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EYN;->LONG:LX/EYN;

    .line 2137043
    new-instance v0, LX/EYN;

    const-string v1, "FLOAT"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LX/EYN;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EYN;->FLOAT:LX/EYN;

    .line 2137044
    new-instance v0, LX/EYN;

    const-string v1, "DOUBLE"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LX/EYN;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EYN;->DOUBLE:LX/EYN;

    .line 2137045
    new-instance v0, LX/EYN;

    const-string v1, "BOOLEAN"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LX/EYN;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EYN;->BOOLEAN:LX/EYN;

    .line 2137046
    new-instance v0, LX/EYN;

    const-string v1, "STRING"

    const/4 v2, 0x5

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, LX/EYN;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EYN;->STRING:LX/EYN;

    .line 2137047
    new-instance v0, LX/EYN;

    const-string v1, "BYTE_STRING"

    const/4 v2, 0x6

    sget-object v3, LX/EWc;->a:LX/EWc;

    invoke-direct {v0, v1, v2, v3}, LX/EYN;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EYN;->BYTE_STRING:LX/EYN;

    .line 2137048
    new-instance v0, LX/EYN;

    const-string v1, "ENUM"

    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/EYN;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EYN;->ENUM:LX/EYN;

    .line 2137049
    new-instance v0, LX/EYN;

    const-string v1, "MESSAGE"

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LX/EYN;-><init>(Ljava/lang/String;ILjava/lang/Object;)V

    sput-object v0, LX/EYN;->MESSAGE:LX/EYN;

    .line 2137050
    const/16 v0, 0x9

    new-array v0, v0, [LX/EYN;

    sget-object v1, LX/EYN;->INT:LX/EYN;

    aput-object v1, v0, v4

    sget-object v1, LX/EYN;->LONG:LX/EYN;

    aput-object v1, v0, v5

    sget-object v1, LX/EYN;->FLOAT:LX/EYN;

    aput-object v1, v0, v6

    sget-object v1, LX/EYN;->DOUBLE:LX/EYN;

    aput-object v1, v0, v7

    sget-object v1, LX/EYN;->BOOLEAN:LX/EYN;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/EYN;->STRING:LX/EYN;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/EYN;->BYTE_STRING:LX/EYN;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/EYN;->ENUM:LX/EYN;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/EYN;->MESSAGE:LX/EYN;

    aput-object v2, v0, v1

    sput-object v0, LX/EYN;->$VALUES:[LX/EYN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2137051
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2137052
    iput-object p3, p0, LX/EYN;->defaultDefault:Ljava/lang/Object;

    .line 2137053
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EYN;
    .locals 1

    .prologue
    .line 2137054
    const-class v0, LX/EYN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EYN;

    return-object v0
.end method

.method public static values()[LX/EYN;
    .locals 1

    .prologue
    .line 2137055
    sget-object v0, LX/EYN;->$VALUES:[LX/EYN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EYN;

    return-object v0
.end method
