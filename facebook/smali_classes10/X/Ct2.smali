.class public LX/Ct2;
.super LX/1OX;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Landroid/support/v7/widget/RecyclerView;

.field public final e:F

.field public final f:F

.field public final g:F

.field public final h:F

.field private final i:F

.field private j:I

.field public k:F

.field private l:J

.field private m:I


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3

    .prologue
    .line 1943770
    invoke-direct {p0}, LX/1OX;-><init>()V

    .line 1943771
    const/4 v0, 0x0

    iput v0, p0, LX/Ct2;->j:I

    .line 1943772
    iput-object p1, p0, LX/Ct2;->d:Landroid/support/v7/widget/RecyclerView;

    .line 1943773
    const-class v0, LX/Ct2;

    invoke-static {v0, p0}, LX/Ct2;->a(Ljava/lang/Class;LX/02k;)V

    .line 1943774
    invoke-virtual {p0}, LX/Ct2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1943775
    sget-object v1, LX/2fv;->k:LX/0Tn;

    sget v2, LX/CoL;->x:F

    invoke-direct {p0, v1, v2}, LX/Ct2;->a(LX/0Tn;F)F

    move-result v1

    iput v1, p0, LX/Ct2;->e:F

    .line 1943776
    sget-object v1, LX/2fv;->l:LX/0Tn;

    sget v2, LX/CoL;->y:F

    invoke-direct {p0, v1, v2}, LX/Ct2;->a(LX/0Tn;F)F

    move-result v1

    mul-float/2addr v1, v0

    iput v1, p0, LX/Ct2;->f:F

    .line 1943777
    sget-object v1, LX/2fv;->i:LX/0Tn;

    sget v2, LX/CoL;->z:F

    invoke-direct {p0, v1, v2}, LX/Ct2;->a(LX/0Tn;F)F

    move-result v1

    iput v1, p0, LX/Ct2;->g:F

    .line 1943778
    sget-object v1, LX/2fv;->j:LX/0Tn;

    sget v2, LX/CoL;->A:F

    invoke-direct {p0, v1, v2}, LX/Ct2;->a(LX/0Tn;F)F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, LX/Ct2;->h:F

    .line 1943779
    sget-object v0, LX/2fv;->h:LX/0Tn;

    sget v1, LX/CoL;->B:F

    invoke-direct {p0, v0, v1}, LX/Ct2;->a(LX/0Tn;F)F

    move-result v0

    iput v0, p0, LX/Ct2;->i:F

    .line 1943780
    return-void
.end method

.method private a(LX/0Tn;F)F
    .locals 2

    .prologue
    .line 1943781
    :try_start_0
    iget-object v0, p0, LX/Ct2;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 1943782
    :goto_0
    return p2

    :catch_0
    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Ct2;

    const/16 v2, 0xf9a

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2db

    invoke-static {v1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0x31e0

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, LX/Ct2;->a:LX/0Ot;

    iput-object v3, p1, LX/Ct2;->b:LX/0Ot;

    iput-object v1, p1, LX/Ct2;->c:LX/0Ot;

    return-void
.end method

.method private static a(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1943783
    if-eqz p0, :cond_0

    instance-of v1, p0, LX/Ctg;

    if-eqz v1, :cond_0

    .line 1943784
    check-cast p0, LX/Ctg;

    .line 1943785
    invoke-interface {p0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v1

    invoke-virtual {v1}, LX/CqX;->d()LX/Cqv;

    move-result-object v1

    sget-object v2, LX/Cqw;->b:LX/Cqw;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1943786
    :cond_0
    return v0
.end method

.method private b(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 1943787
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/Ctg;

    if-eqz v0, :cond_0

    .line 1943788
    iget-object v0, p0, LX/Ct2;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->c()V

    .line 1943789
    iget-object v0, p0, LX/Ct2;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiW;

    invoke-direct {v1}, LX/CiW;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1943790
    check-cast p1, LX/Ctg;

    .line 1943791
    sget-object v0, LX/Cqw;->b:LX/Cqw;

    invoke-interface {p1, v0}, LX/Ctg;->a(LX/Cqw;)V

    .line 1943792
    const/4 v0, 0x1

    .line 1943793
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 2

    .prologue
    .line 1943794
    iget-object v0, p0, LX/Ct2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/Ct2;->l:J

    .line 1943795
    iget v0, p0, LX/Ct2;->j:I

    .line 1943796
    iput p2, p0, LX/Ct2;->j:I

    .line 1943797
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    if-nez p2, :cond_0

    .line 1943798
    invoke-virtual {p0}, LX/Ct2;->a()Z

    .line 1943799
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    .line 1943800
    iget-object v0, p0, LX/Ct2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 1943801
    iget-wide v2, p0, LX/Ct2;->l:J

    sub-long v2, v0, v2

    .line 1943802
    iput-wide v0, p0, LX/Ct2;->l:J

    .line 1943803
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    .line 1943804
    :cond_0
    :goto_0
    return-void

    .line 1943805
    :cond_1
    const/high16 v0, 0x447a0000    # 1000.0f

    int-to-float v1, p3

    mul-float/2addr v0, v1

    long-to-float v1, v2

    div-float/2addr v0, v1

    .line 1943806
    iput v0, p0, LX/Ct2;->k:F

    .line 1943807
    iget v0, p0, LX/Ct2;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1943808
    invoke-virtual {p0}, LX/Ct2;->a()Z

    goto :goto_0
.end method

.method public final a()Z
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1943809
    iget-object v0, p0, LX/Ct2;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 1943810
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v1

    .line 1943811
    invoke-virtual {v0}, LX/1OR;->v()I

    move-result v4

    add-int/2addr v4, v1

    .line 1943812
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1943813
    :goto_0
    if-gt v1, v4, :cond_1

    .line 1943814
    invoke-virtual {v0, v1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v6

    .line 1943815
    invoke-static {v6}, LX/Ct2;->a(Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1943816
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1943817
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1943818
    :cond_1
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1943819
    iget v0, p0, LX/Ct2;->m:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1943820
    new-array v4, v8, [I

    iget v0, p0, LX/Ct2;->m:I

    add-int/lit8 v0, v0, 0x1

    aput v0, v4, v3

    iget v0, p0, LX/Ct2;->m:I

    add-int/lit8 v0, v0, -0x1

    aput v0, v4, v2

    move v1, v3

    .line 1943821
    :goto_1
    if-ge v1, v8, :cond_3

    aget v6, v4, v1

    .line 1943822
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, LX/Ct2;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1943823
    iput v6, p0, LX/Ct2;->m:I

    move v0, v2

    .line 1943824
    :goto_2
    return v0

    .line 1943825
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1943826
    :cond_3
    iget v0, p0, LX/Ct2;->k:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LX/Ct2;->i:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_7

    .line 1943827
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1943828
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1943829
    iget v5, p0, LX/Ct2;->k:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 1943830
    iget v6, p0, LX/Ct2;->g:F

    cmpl-float v6, v5, v6

    if-lez v6, :cond_9

    .line 1943831
    iget v5, p0, LX/Ct2;->h:F

    .line 1943832
    :goto_3
    move v5, v5

    .line 1943833
    iget-object v6, p0, LX/Ct2;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v6

    .line 1943834
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v7

    sub-int v7, v6, v7

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    add-float/2addr v5, v7

    .line 1943835
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    .line 1943836
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v8

    .line 1943837
    if-ltz v7, :cond_5

    int-to-float v7, v7

    cmpg-float v7, v7, v5

    if-gtz v7, :cond_5

    iget v7, p0, LX/Ct2;->k:F

    iget v9, p0, LX/Ct2;->e:F

    neg-float v9, v9

    cmpl-float v7, v7, v9

    if-gtz v7, :cond_6

    :cond_5
    int-to-float v7, v8

    int-to-float v9, v6

    sub-float v5, v9, v5

    cmpl-float v5, v7, v5

    if-ltz v5, :cond_8

    if-gt v8, v6, :cond_8

    iget v5, p0, LX/Ct2;->k:F

    iget v6, p0, LX/Ct2;->e:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_8

    .line 1943838
    :cond_6
    const/4 v5, 0x1

    .line 1943839
    :goto_4
    move v5, v5

    .line 1943840
    if-eqz v5, :cond_4

    invoke-direct {p0, v1}, LX/Ct2;->b(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1943841
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/Ct2;->m:I

    move v0, v2

    .line 1943842
    goto/16 :goto_2

    .line 1943843
    :cond_7
    const/4 v0, -0x1

    iput v0, p0, LX/Ct2;->m:I

    move v0, v3

    .line 1943844
    goto/16 :goto_2

    :cond_8
    const/4 v5, 0x0

    goto :goto_4

    .line 1943845
    :cond_9
    iget v6, p0, LX/Ct2;->e:F

    cmpg-float v6, v5, v6

    if-gez v6, :cond_a

    .line 1943846
    iget v5, p0, LX/Ct2;->f:F

    goto :goto_3

    .line 1943847
    :cond_a
    iget v6, p0, LX/Ct2;->h:F

    iget v7, p0, LX/Ct2;->f:F

    sub-float/2addr v6, v7

    iget v7, p0, LX/Ct2;->g:F

    iget v8, p0, LX/Ct2;->e:F

    sub-float/2addr v7, v8

    div-float/2addr v6, v7

    .line 1943848
    iget v7, p0, LX/Ct2;->f:F

    iget v8, p0, LX/Ct2;->e:F

    mul-float/2addr v8, v6

    sub-float/2addr v7, v8

    .line 1943849
    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    goto :goto_3
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1943850
    iget-object v0, p0, LX/Ct2;->d:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
