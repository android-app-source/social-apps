.class public final LX/Ejh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 2162320
    const/16 v17, 0x0

    .line 2162321
    const/16 v16, 0x0

    .line 2162322
    const/4 v15, 0x0

    .line 2162323
    const/4 v14, 0x0

    .line 2162324
    const/4 v13, 0x0

    .line 2162325
    const/4 v12, 0x0

    .line 2162326
    const/4 v11, 0x0

    .line 2162327
    const/4 v10, 0x0

    .line 2162328
    const/4 v9, 0x0

    .line 2162329
    const/4 v8, 0x0

    .line 2162330
    const/4 v7, 0x0

    .line 2162331
    const/4 v6, 0x0

    .line 2162332
    const/4 v5, 0x0

    .line 2162333
    const/4 v4, 0x0

    .line 2162334
    const/4 v3, 0x0

    .line 2162335
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 2162336
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2162337
    const/4 v3, 0x0

    .line 2162338
    :goto_0
    return v3

    .line 2162339
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2162340
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_9

    .line 2162341
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 2162342
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2162343
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 2162344
    const-string v19, "batch_size"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 2162345
    const/4 v9, 0x1

    .line 2162346
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto :goto_1

    .line 2162347
    :cond_2
    const-string v19, "field_setting"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 2162348
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    goto :goto_1

    .line 2162349
    :cond_3
    const-string v19, "max_concurrent_batches"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 2162350
    const/4 v8, 0x1

    .line 2162351
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto :goto_1

    .line 2162352
    :cond_4
    const-string v19, "max_num_contacts"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 2162353
    const/4 v7, 0x1

    .line 2162354
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto :goto_1

    .line 2162355
    :cond_5
    const-string v19, "max_num_emails_in_contact"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 2162356
    const/4 v6, 0x1

    .line 2162357
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto :goto_1

    .line 2162358
    :cond_6
    const-string v19, "max_num_phones_in_contact"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 2162359
    const/4 v5, 0x1

    .line 2162360
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto :goto_1

    .line 2162361
    :cond_7
    const-string v19, "max_num_retries"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 2162362
    const/4 v4, 0x1

    .line 2162363
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto/16 :goto_1

    .line 2162364
    :cond_8
    const-string v19, "upload_interval"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 2162365
    const/4 v3, 0x1

    .line 2162366
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto/16 :goto_1

    .line 2162367
    :cond_9
    const/16 v18, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2162368
    if-eqz v9, :cond_a

    .line 2162369
    const/4 v9, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v9, v1, v2}, LX/186;->a(III)V

    .line 2162370
    :cond_a
    const/4 v9, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 2162371
    if-eqz v8, :cond_b

    .line 2162372
    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15, v9}, LX/186;->a(III)V

    .line 2162373
    :cond_b
    if-eqz v7, :cond_c

    .line 2162374
    const/4 v7, 0x3

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14, v8}, LX/186;->a(III)V

    .line 2162375
    :cond_c
    if-eqz v6, :cond_d

    .line 2162376
    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13, v7}, LX/186;->a(III)V

    .line 2162377
    :cond_d
    if-eqz v5, :cond_e

    .line 2162378
    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12, v6}, LX/186;->a(III)V

    .line 2162379
    :cond_e
    if-eqz v4, :cond_f

    .line 2162380
    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v5}, LX/186;->a(III)V

    .line 2162381
    :cond_f
    if-eqz v3, :cond_10

    .line 2162382
    const/4 v3, 0x7

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v4}, LX/186;->a(III)V

    .line 2162383
    :cond_10
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
