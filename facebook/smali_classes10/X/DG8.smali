.class public final enum LX/DG8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DG8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DG8;

.field public static final enum NETEGO_FORSALE_PHOTO_STORY:LX/DG8;

.field public static final enum NETEGO_PHOTO_STORY:LX/DG8;

.field public static final enum NETEGO_VIDEO_STORY:LX/DG8;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1979254
    new-instance v0, LX/DG8;

    const-string v1, "NETEGO_FORSALE_PHOTO_STORY"

    invoke-direct {v0, v1, v2}, LX/DG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DG8;->NETEGO_FORSALE_PHOTO_STORY:LX/DG8;

    .line 1979255
    new-instance v0, LX/DG8;

    const-string v1, "NETEGO_PHOTO_STORY"

    invoke-direct {v0, v1, v3}, LX/DG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DG8;->NETEGO_PHOTO_STORY:LX/DG8;

    .line 1979256
    new-instance v0, LX/DG8;

    const-string v1, "NETEGO_VIDEO_STORY"

    invoke-direct {v0, v1, v4}, LX/DG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DG8;->NETEGO_VIDEO_STORY:LX/DG8;

    .line 1979257
    const/4 v0, 0x3

    new-array v0, v0, [LX/DG8;

    sget-object v1, LX/DG8;->NETEGO_FORSALE_PHOTO_STORY:LX/DG8;

    aput-object v1, v0, v2

    sget-object v1, LX/DG8;->NETEGO_PHOTO_STORY:LX/DG8;

    aput-object v1, v0, v3

    sget-object v1, LX/DG8;->NETEGO_VIDEO_STORY:LX/DG8;

    aput-object v1, v0, v4

    sput-object v0, LX/DG8;->$VALUES:[LX/DG8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1979258
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DG8;
    .locals 1

    .prologue
    .line 1979259
    const-class v0, LX/DG8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DG8;

    return-object v0
.end method

.method public static values()[LX/DG8;
    .locals 1

    .prologue
    .line 1979260
    sget-object v0, LX/DG8;->$VALUES:[LX/DG8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DG8;

    return-object v0
.end method
