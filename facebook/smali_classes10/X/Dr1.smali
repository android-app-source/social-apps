.class public final LX/Dr1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Dr5;

.field public final synthetic b:LX/Dr2;


# direct methods
.method public constructor <init>(LX/Dr2;LX/Dr5;)V
    .locals 0

    .prologue
    .line 2048845
    iput-object p1, p0, LX/Dr1;->b:LX/Dr2;

    iput-object p2, p0, LX/Dr1;->a:LX/Dr5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x10c741c9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2048846
    iget-object v0, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v0, v0, LX/Dr2;->l:LX/Dr4;

    iget-object v0, v0, LX/Dr4;->e:LX/Dq0;

    if-eqz v0, :cond_0

    .line 2048847
    iget-object v0, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v0, v0, LX/Dr2;->l:LX/Dr4;

    iget-object v0, v0, LX/Dr4;->e:LX/Dq0;

    .line 2048848
    iget-object v1, v0, LX/Dq0;->a:LX/3Af;

    invoke-virtual {v1}, LX/3Af;->dismiss()V

    .line 2048849
    :cond_0
    iget-object v0, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v0, v0, LX/Dr2;->l:LX/Dr4;

    iget-object v0, v0, LX/Dr4;->h:LX/2nq;

    if-eqz v0, :cond_2

    .line 2048850
    iget-object v0, p0, LX/Dr1;->a:LX/Dr5;

    .line 2048851
    iget-object v1, v0, LX/Dr5;->b:LX/BCO;

    move-object v0, v1

    .line 2048852
    invoke-interface {v0}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v1, v1, LX/Dr2;->l:LX/Dr4;

    iget-object v1, v1, LX/Dr4;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2048853
    iget-object v0, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v0, v0, LX/Dr2;->l:LX/Dr4;

    iget-object v0, v0, LX/Dr4;->g:LX/33a;

    sget-object v1, LX/8D0;->BOTTOM_SHEET:LX/8D0;

    iget-object v2, p0, LX/Dr1;->a:LX/Dr5;

    .line 2048854
    iget-object v4, v2, LX/Dr5;->b:LX/BCO;

    move-object v2, v4

    .line 2048855
    invoke-interface {v2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/33a;->a(LX/8D0;Ljava/lang/String;)V

    .line 2048856
    const v0, 0x52566a95

    invoke-static {v3, v3, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2048857
    :goto_0
    return-void

    .line 2048858
    :cond_1
    iget-object v0, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v0, v0, LX/Dr2;->l:LX/Dr4;

    iget-object v0, v0, LX/Dr4;->a:LX/33W;

    iget-object v1, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v1, v1, LX/Dr2;->l:LX/Dr4;

    iget-object v1, v1, LX/Dr4;->c:Landroid/content/Context;

    iget-object v2, p0, LX/Dr1;->a:LX/Dr5;

    .line 2048859
    iget-object v3, v2, LX/Dr5;->b:LX/BCO;

    move-object v2, v3

    .line 2048860
    sget-object v3, LX/8D0;->BOTTOM_SHEET:LX/8D0;

    iget-object v4, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v4, v4, LX/Dr2;->l:LX/Dr4;

    iget-object v4, v4, LX/Dr4;->h:LX/2nq;

    iget-object v5, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v5, v5, LX/Dr2;->l:LX/Dr4;

    iget-object v5, v5, LX/Dr4;->j:Ljava/lang/String;

    iget-object v6, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v6, v6, LX/Dr2;->l:LX/Dr4;

    iget-object v6, v6, LX/Dr4;->k:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, LX/33W;->a(Landroid/content/Context;LX/BCO;LX/8D0;LX/2nq;Ljava/lang/String;Ljava/lang/String;)V

    .line 2048861
    :goto_1
    const v0, -0x6cbf5ae2

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_0

    .line 2048862
    :cond_2
    iget-object v0, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v0, v0, LX/Dr2;->l:LX/Dr4;

    iget-object v0, v0, LX/Dr4;->a:LX/33W;

    iget-object v1, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v1, v1, LX/Dr2;->l:LX/Dr4;

    iget-object v1, v1, LX/Dr4;->c:Landroid/content/Context;

    iget-object v2, p0, LX/Dr1;->a:LX/Dr5;

    .line 2048863
    iget-object v3, v2, LX/Dr5;->b:LX/BCO;

    move-object v2, v3

    .line 2048864
    sget-object v3, LX/8D0;->BOTTOM_SHEET:LX/8D0;

    iget-object v4, p0, LX/Dr1;->b:LX/Dr2;

    iget-object v4, v4, LX/Dr2;->l:LX/Dr4;

    iget-object v4, v4, LX/Dr4;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/33W;->a(Landroid/content/Context;LX/BCO;LX/8D0;Ljava/lang/String;)V

    goto :goto_1
.end method
