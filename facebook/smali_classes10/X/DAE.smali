.class public LX/DAE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0lp;


# direct methods
.method public constructor <init>(LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1970898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970899
    iput-object p1, p0, LX/DAE;->a:LX/0lp;

    .line 1970900
    return-void
.end method

.method public static a(LX/DAE;LX/0Px;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/contactlogs/data/ContactLogMetadata;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1970901
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 1970902
    iget-object v0, p0, LX/DAE;->a:LX/0lp;

    invoke-virtual {v0, v2}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v3

    .line 1970903
    invoke-virtual {v3}, LX/0nX;->d()V

    .line 1970904
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contactlogs/data/ContactLogMetadata;

    .line 1970905
    iget-object v0, v0, Lcom/facebook/contactlogs/data/ContactLogMetadata;->b:LX/0m9;

    invoke-virtual {v3, v0}, LX/0nX;->a(LX/0lG;)V

    .line 1970906
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1970907
    :cond_0
    invoke-virtual {v3}, LX/0nX;->e()V

    .line 1970908
    invoke-virtual {v3}, LX/0nX;->flush()V

    .line 1970909
    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
