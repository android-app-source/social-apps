.class public LX/Ens;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/Ens;


# instance fields
.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2167507
    new-instance v0, LX/Ens;

    invoke-direct {v0}, LX/Ens;-><init>()V

    sput-object v0, LX/Ens;->a:LX/Ens;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2167508
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167509
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2167510
    iput-object v0, p0, LX/Ens;->b:LX/0Px;

    .line 2167511
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2167512
    iput-object v0, p0, LX/Ens;->c:LX/0P1;

    .line 2167513
    return-void
.end method

.method public constructor <init>(LX/0Px;LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TK;>;",
            "LX/0P1",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 2167514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2167515
    iput-object p1, p0, LX/Ens;->b:LX/0Px;

    .line 2167516
    iput-object p2, p0, LX/Ens;->c:LX/0P1;

    .line 2167517
    return-void
.end method

.method public static a()LX/Ens;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "LX/Ens",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 2167518
    sget-object v0, LX/Ens;->a:LX/Ens;

    return-object v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    .prologue
    .line 2167519
    iget-object v0, p0, LX/Ens;->c:LX/0P1;

    iget-object v1, p0, LX/Ens;->b:LX/0Px;

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 2167520
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2167521
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LX/Ens;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 2167522
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Ens;->c:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TK;"
        }
    .end annotation

    .prologue
    .line 2167523
    iget-object v0, p0, LX/Ens;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 2167524
    iget-object v0, p0, LX/Ens;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2167525
    iget-object v0, p0, LX/Ens;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
