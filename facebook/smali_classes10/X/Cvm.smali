.class public LX/Cvm;
.super LX/CvH;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Zb;

.field private final c:LX/0SG;

.field private final d:LX/2Sc;

.field public e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

.field public f:Ljava/lang/String;

.field public g:J

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1949295
    const-class v0, LX/Cvm;

    sput-object v0, LX/Cvm;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0SG;LX/2Sc;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1949288
    invoke-direct {p0}, LX/CvH;-><init>()V

    .line 1949289
    sget-object v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iput-object v0, p0, LX/Cvm;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 1949290
    const/4 v0, 0x0

    iput v0, p0, LX/Cvm;->h:I

    .line 1949291
    iput-object p1, p0, LX/Cvm;->b:LX/0Zb;

    .line 1949292
    iput-object p2, p0, LX/Cvm;->c:LX/0SG;

    .line 1949293
    iput-object p3, p0, LX/Cvm;->d:LX/2Sc;

    .line 1949294
    return-void
.end method

.method public static a(LX/Cvm;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1949278
    invoke-static {p0}, LX/Cvm;->d(LX/Cvm;)V

    .line 1949279
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "pps"

    .line 1949280
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1949281
    move-object v0, v0

    .line 1949282
    const-string v1, "session_id"

    iget-object v2, p0, LX/Cvm;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "back_presses"

    iget v2, p0, LX/Cvm;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "typeahead_sid"

    iget-object v2, p0, LX/Cvm;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v2, v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "candidate_result_sid"

    iget-object v2, p0, LX/Cvm;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v2, v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1949283
    return-object v0
.end method

.method public static c(LX/Cvm;)V
    .locals 2

    .prologue
    .line 1949284
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Cvm;->f:Ljava/lang/String;

    .line 1949285
    iget-object v0, p0, LX/Cvm;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Cvm;->g:J

    .line 1949286
    const/4 v0, 0x0

    iput v0, p0, LX/Cvm;->h:I

    .line 1949287
    return-void
.end method

.method public static d(LX/Cvm;)V
    .locals 4

    .prologue
    .line 1949275
    iget-object v0, p0, LX/Cvm;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/Cvm;->g:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1949276
    invoke-static {p0}, LX/Cvm;->c(LX/Cvm;)V

    .line 1949277
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0m9;Lcom/facebook/search/model/TypeaheadUnit;)V
    .locals 7

    .prologue
    .line 1949265
    move-object v0, p2

    check-cast v0, Lcom/facebook/search/model/SeeMoreResultPageUnit;

    .line 1949266
    iget-object v1, v0, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-object v0, v1

    .line 1949267
    if-eqz v0, :cond_0

    .line 1949268
    invoke-static {p1, v0}, LX/CvH;->a(LX/0m9;Lcom/facebook/search/model/EntityTypeaheadUnit;)V

    .line 1949269
    :goto_0
    return-void

    .line 1949270
    :cond_0
    iget-object v0, p0, LX/Cvm;->d:LX/2Sc;

    sget-object v1, LX/3Ql;->LOGGING_UNIMPLEMENTED_RESULT_ROW_TYPE:LX/3Ql;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Candidate result logging not implemented for TypeaheadUnit of type %s. Entity associated with this unit is null."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Boolean;LX/Cw8;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1949271
    iget-object v0, p0, LX/Cvm;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1949272
    :goto_0
    return-void

    .line 1949273
    :cond_0
    const-string v0, "see_more_impression"

    invoke-static {p0, v0}, LX/Cvm;->a(LX/Cvm;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "filter_type"

    invoke-virtual {p2}, LX/Cw8;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "is_empty"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1949274
    iget-object v1, p0, LX/Cvm;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
