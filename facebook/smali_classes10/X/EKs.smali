.class public LX/EKs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1vg;


# direct methods
.method public constructor <init>(LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2107383
    iput-object p1, p0, LX/EKs;->a:LX/1vg;

    .line 2107384
    return-void
.end method

.method public static a(LX/0QB;)LX/EKs;
    .locals 4

    .prologue
    .line 2107385
    const-class v1, LX/EKs;

    monitor-enter v1

    .line 2107386
    :try_start_0
    sget-object v0, LX/EKs;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107387
    sput-object v2, LX/EKs;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107388
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107389
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107390
    new-instance p0, LX/EKs;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-direct {p0, v3}, LX/EKs;-><init>(LX/1vg;)V

    .line 2107391
    move-object v0, p0

    .line 2107392
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107393
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107394
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107395
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
