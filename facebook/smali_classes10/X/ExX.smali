.class public final LX/ExX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/Euc;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0zS;

.field public final synthetic b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/0zS;)V
    .locals 0

    .prologue
    .line 2184843
    iput-object p1, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iput-object p2, p0, LX/ExX;->a:LX/0zS;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2184799
    iget-object v0, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2184800
    iget-object v0, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    sget-object v1, LX/Ey6;->LOADING_STATE_ERROR:LX/Ey6;

    invoke-static {v0, v1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/Ey6;)V

    .line 2184801
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 2184802
    check-cast p1, LX/Euc;

    const/4 v2, 0x0

    .line 2184803
    iget-object v0, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->v:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2184804
    iget-object v0, p1, LX/Euc;->a:LX/0Px;

    move-object v5, v0

    .line 2184805
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2184806
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->m()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2184807
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 2184808
    iget-object v1, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->s:Ljava/util/LinkedHashMap;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2184809
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    .line 2184810
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->n()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v10, v3, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2184811
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 2184812
    :goto_1
    if-eqz v10, :cond_2

    invoke-virtual {v7, v10, v2}, LX/15i;->j(II)I

    move-result v1

    .line 2184813
    :goto_2
    new-instance v7, LX/Euq;

    invoke-direct {v7}, LX/Euq;-><init>()V

    .line 2184814
    iput-wide v8, v7, LX/Euq;->a:J

    .line 2184815
    move-object v7, v7

    .line 2184816
    iput-object v3, v7, LX/Euq;->c:Ljava/lang/String;

    .line 2184817
    move-object v3, v7

    .line 2184818
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v7

    .line 2184819
    iput-object v7, v3, LX/Euq;->d:Ljava/lang/String;

    .line 2184820
    move-object v3, v3

    .line 2184821
    iput v1, v3, LX/Euq;->e:I

    .line 2184822
    move-object v1, v3

    .line 2184823
    sget-object v3, LX/2h7;->FRIENDS_CENTER_SUGGESTIONS:LX/2h7;

    .line 2184824
    iput-object v3, v1, LX/Euq;->f:LX/2h7;

    .line 2184825
    move-object v1, v1

    .line 2184826
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2184827
    iput-object v0, v1, LX/Euq;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2184828
    move-object v0, v1

    .line 2184829
    iget-object v1, p1, LX/Euc;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2184830
    iput-object v1, v0, LX/Euq;->g:Ljava/lang/String;

    .line 2184831
    move-object v0, v0

    .line 2184832
    invoke-virtual {v0}, LX/Euq;->b()LX/Eus;

    move-result-object v0

    .line 2184833
    iget-object v1, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->s:Ljava/util/LinkedHashMap;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2184834
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 2184835
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2184836
    :cond_1
    const/4 v1, 0x0

    move-object v3, v1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 2184837
    goto :goto_2

    .line 2184838
    :cond_3
    iget-object v0, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v1, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(Ljava/util/Collection;)V

    .line 2184839
    iget-object v0, p0, LX/ExX;->a:LX/0zS;

    sget-object v1, LX/0zS;->d:LX/0zS;

    if-ne v0, v1, :cond_4

    .line 2184840
    iget-object v0, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->u:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1OR;->e(I)V

    .line 2184841
    :cond_4
    iget-object v0, p0, LX/ExX;->b:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    sget-object v1, LX/Ey6;->LOADING_STATE_IDLE:LX/Ey6;

    invoke-static {v0, v1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/Ey6;)V

    .line 2184842
    return-void
.end method
