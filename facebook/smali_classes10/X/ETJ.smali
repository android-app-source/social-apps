.class public final LX/ETJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "LX/7Jh;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/ETO;

.field public final synthetic c:LX/ETN;


# direct methods
.method public constructor <init>(LX/ETN;ZLX/ETO;)V
    .locals 0

    .prologue
    .line 2124646
    iput-object p1, p0, LX/ETJ;->c:LX/ETN;

    iput-boolean p2, p0, LX/ETJ;->a:Z

    iput-object p3, p0, LX/ETJ;->b:LX/ETO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2124647
    iget-object v0, p0, LX/ETJ;->b:LX/ETO;

    invoke-interface {v0}, LX/ETO;->a()V

    .line 2124648
    iget-object v0, p0, LX/ETJ;->c:LX/ETN;

    const/4 v1, 0x0

    .line 2124649
    iput-boolean v1, v0, LX/ETN;->l:Z

    .line 2124650
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2124651
    check-cast p1, Ljava/util/List;

    .line 2124652
    iget-boolean v0, p0, LX/ETJ;->a:Z

    if-eqz v0, :cond_0

    .line 2124653
    iget-object v0, p0, LX/ETJ;->c:LX/ETN;

    .line 2124654
    iget-object v1, v0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    if-eqz v1, :cond_4

    .line 2124655
    iget-object v1, v0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    invoke-virtual {v1}, LX/ETQ;->clear()V

    .line 2124656
    :cond_0
    :goto_0
    iget-object v0, p0, LX/ETJ;->c:LX/ETN;

    .line 2124657
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2124658
    invoke-static {p1}, LX/15V;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 2124659
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2124660
    invoke-static {v1}, LX/ETN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/video/videohome/data/VideoHomeItem;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2124661
    :cond_1
    iget-object v1, v0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/ETQ;->addAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2124662
    iget-object v1, v0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    .line 2124663
    new-instance v2, LX/4a7;

    invoke-direct {v2}, LX/4a7;-><init>()V

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2124664
    iget-object v5, v0, LX/ETN;->b:LX/19w;

    invoke-virtual {v5, v4, v3}, LX/19w;->a(ZZ)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v1}, Lcom/facebook/video/videohome/data/VideoHomeItem;->q()LX/ETQ;

    move-result-object p1

    invoke-virtual {p1}, LX/ETQ;->size()I

    move-result p1

    if-le v5, p1, :cond_6

    :goto_2
    move v3, v3

    .line 2124665
    iput-boolean v3, v2, LX/4a7;->b:Z

    .line 2124666
    move-object v2, v2

    .line 2124667
    invoke-virtual {v2}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    .line 2124668
    iput-object v2, v1, Lcom/facebook/video/videohome/data/VideoHomeItem;->b:LX/0us;

    .line 2124669
    const/4 v1, 0x1

    .line 2124670
    :goto_3
    move v0, v1

    .line 2124671
    if-eqz v0, :cond_2

    .line 2124672
    iget-object v0, p0, LX/ETJ;->c:LX/ETN;

    iget-object v0, v0, LX/ETN;->e:LX/ETL;

    invoke-virtual {v0}, LX/ETL;->b()V

    .line 2124673
    :cond_2
    iget-object v0, p0, LX/ETJ;->b:LX/ETO;

    if-eqz v0, :cond_3

    .line 2124674
    iget-object v0, p0, LX/ETJ;->b:LX/ETO;

    invoke-interface {v0}, LX/ETO;->a()V

    .line 2124675
    :cond_3
    iget-object v0, p0, LX/ETJ;->c:LX/ETN;

    const/4 v1, 0x0

    .line 2124676
    iput-boolean v1, v0, LX/ETN;->l:Z

    .line 2124677
    return-void

    .line 2124678
    :cond_4
    new-instance v1, Lcom/facebook/video/videohome/data/VideoHomeItem;

    new-instance v2, LX/9vz;

    invoke-direct {v2}, LX/9vz;-><init>()V

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->PAGINATED_H_SCROLL_GENERIC_COMPONENTS_LIST:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2124679
    iput-object v3, v2, LX/9vz;->J:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    .line 2124680
    move-object v2, v2

    .line 2124681
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;->NONE:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 2124682
    iput-object v3, v2, LX/9vz;->cg:Lcom/facebook/graphql/enums/GraphQLVideoChannelFeedUnitPruneBehavior;

    .line 2124683
    move-object v2, v2

    .line 2124684
    invoke-virtual {v2}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v2

    const-string v3, "download-section-id"

    const-string v4, "download-type-token"

    invoke-direct {v1, v2, v3, v4}, Lcom/facebook/video/videohome/data/VideoHomeItem;-><init>(LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v0, LX/ETN;->k:Lcom/facebook/video/videohome/data/VideoHomeItem;

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    :cond_6
    move v3, v4

    goto :goto_2
.end method
