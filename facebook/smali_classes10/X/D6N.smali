.class public final enum LX/D6N;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D6N;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D6N;

.field public static final enum FIRST:LX/D6N;

.field public static final enum LAST:LX/D6N;

.field public static final enum NORMAL:LX/D6N;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1965537
    new-instance v0, LX/D6N;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v2}, LX/D6N;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D6N;->FIRST:LX/D6N;

    .line 1965538
    new-instance v0, LX/D6N;

    const-string v1, "LAST"

    invoke-direct {v0, v1, v3}, LX/D6N;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D6N;->LAST:LX/D6N;

    .line 1965539
    new-instance v0, LX/D6N;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, LX/D6N;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/D6N;->NORMAL:LX/D6N;

    .line 1965540
    const/4 v0, 0x3

    new-array v0, v0, [LX/D6N;

    sget-object v1, LX/D6N;->FIRST:LX/D6N;

    aput-object v1, v0, v2

    sget-object v1, LX/D6N;->LAST:LX/D6N;

    aput-object v1, v0, v3

    sget-object v1, LX/D6N;->NORMAL:LX/D6N;

    aput-object v1, v0, v4

    sput-object v0, LX/D6N;->$VALUES:[LX/D6N;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1965541
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D6N;
    .locals 1

    .prologue
    .line 1965542
    const-class v0, LX/D6N;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D6N;

    return-object v0
.end method

.method public static values()[LX/D6N;
    .locals 1

    .prologue
    .line 1965543
    sget-object v0, LX/D6N;->$VALUES:[LX/D6N;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D6N;

    return-object v0
.end method
