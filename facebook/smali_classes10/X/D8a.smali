.class public LX/D8a;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:F

.field public h:I

.field public i:I

.field public j:F

.field public k:I

.field public l:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1969020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1969021
    iput p1, p0, LX/D8a;->a:I

    .line 1969022
    iput p2, p0, LX/D8a;->b:I

    .line 1969023
    return-void
.end method


# virtual methods
.method public final a(FF)V
    .locals 2

    .prologue
    .line 1969024
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 1969025
    :goto_0
    const/4 v1, 0x0

    .line 1969026
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 p1, 0x3f000000    # 0.5f

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_1

    .line 1969027
    :goto_1
    return-void

    .line 1969028
    :cond_0
    iget v0, p0, LX/D8a;->h:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, LX/D8a;->k:I

    .line 1969029
    iget v0, p0, LX/D8a;->e:I

    iget v1, p0, LX/D8a;->k:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/D8a;->k:I

    .line 1969030
    iget v0, p0, LX/D8a;->c:I

    iget v1, p0, LX/D8a;->k:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/D8a;->k:I

    goto :goto_0

    .line 1969031
    :cond_1
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_3

    const/4 v0, 0x1

    .line 1969032
    :goto_2
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p1

    .line 1969033
    if-eqz v0, :cond_4

    .line 1969034
    iget v0, p0, LX/D8a;->g:F

    sub-float/2addr v0, p1

    iput v0, p0, LX/D8a;->j:F

    .line 1969035
    iget v0, p0, LX/D8a;->j:F

    iget v1, p0, LX/D8a;->a:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1969036
    iget v0, p0, LX/D8a;->i:I

    iget v1, p0, LX/D8a;->a:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/D8a;->j:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/D8a;->l:I

    .line 1969037
    iget v0, p0, LX/D8a;->a:I

    int-to-float v0, v0

    iput v0, p0, LX/D8a;->j:F

    .line 1969038
    :cond_2
    :goto_3
    iget v0, p0, LX/D8a;->b:I

    int-to-float v0, v0

    iget v1, p0, LX/D8a;->j:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LX/D8a;->j:F

    .line 1969039
    iget v0, p0, LX/D8a;->a:I

    int-to-float v0, v0

    iget v1, p0, LX/D8a;->j:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/D8a;->j:F

    .line 1969040
    iget v0, p0, LX/D8a;->f:I

    iget v1, p0, LX/D8a;->l:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/D8a;->l:I

    .line 1969041
    iget v0, p0, LX/D8a;->d:I

    iget v1, p0, LX/D8a;->l:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/D8a;->l:I

    goto :goto_1

    :cond_3
    move v0, v1

    .line 1969042
    goto :goto_2

    .line 1969043
    :cond_4
    invoke-virtual {p0}, LX/D8a;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1969044
    iget v0, p0, LX/D8a;->i:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, LX/D8a;->l:I

    .line 1969045
    iget v0, p0, LX/D8a;->l:I

    if-gez v0, :cond_2

    .line 1969046
    iget v0, p0, LX/D8a;->a:I

    iget p1, p0, LX/D8a;->l:I

    sub-int/2addr v0, p1

    int-to-float v0, v0

    iput v0, p0, LX/D8a;->j:F

    .line 1969047
    iput v1, p0, LX/D8a;->l:I

    goto :goto_3

    .line 1969048
    :cond_5
    invoke-virtual {p0}, LX/D8a;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1969049
    iget v0, p0, LX/D8a;->g:F

    add-float/2addr v0, p1

    iput v0, p0, LX/D8a;->j:F

    .line 1969050
    iput v1, p0, LX/D8a;->l:I

    goto :goto_3

    .line 1969051
    :cond_6
    iget v0, p0, LX/D8a;->b:I

    int-to-float v0, v0

    iput v0, p0, LX/D8a;->j:F

    .line 1969052
    iput v1, p0, LX/D8a;->l:I

    goto :goto_3
.end method

.method public final a(FII)V
    .locals 0

    .prologue
    .line 1969053
    iput p1, p0, LX/D8a;->j:F

    iput p1, p0, LX/D8a;->g:F

    .line 1969054
    iput p2, p0, LX/D8a;->k:I

    iput p2, p0, LX/D8a;->h:I

    .line 1969055
    iput p3, p0, LX/D8a;->l:I

    iput p3, p0, LX/D8a;->i:I

    .line 1969056
    return-void
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 1969057
    iput p1, p0, LX/D8a;->c:I

    .line 1969058
    iput p3, p0, LX/D8a;->d:I

    .line 1969059
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/D8a;->e:I

    .line 1969060
    invoke-static {p3, p4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/D8a;->f:I

    .line 1969061
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1969062
    iget v0, p0, LX/D8a;->g:F

    iget v1, p0, LX/D8a;->b:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p0, LX/D8a;->g:F

    iget v1, p0, LX/D8a;->a:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1969063
    iget v0, p0, LX/D8a;->g:F

    iget v1, p0, LX/D8a;->a:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
