.class public final LX/EHk;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public a:Landroid/widget/Scroller;

.field public final synthetic b:Lcom/facebook/rtc/views/RtcSpringDragView;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/views/RtcSpringDragView;)V
    .locals 2

    .prologue
    .line 2100231
    iput-object p1, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 2100232
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p1}, Lcom/facebook/rtc/views/RtcSpringDragView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/EHk;->a:Landroid/widget/Scroller;

    .line 2100233
    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 2100234
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 2100235
    iput v1, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->k:I

    .line 2100236
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 2100237
    iput v1, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->l:I

    .line 2100238
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v1, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v1, v1, Lcom/facebook/rtc/views/RtcSpringDragView;->k:I

    iget-object v2, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v2, v2, Lcom/facebook/rtc/views/RtcSpringDragView;->o:I

    sub-int/2addr v1, v2

    .line 2100239
    iput v1, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->m:I

    .line 2100240
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v1, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v1, v1, Lcom/facebook/rtc/views/RtcSpringDragView;->l:I

    iget-object v2, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v2, v2, Lcom/facebook/rtc/views/RtcSpringDragView;->p:I

    sub-int/2addr v1, v2

    .line 2100241
    iput v1, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->n:I

    .line 2100242
    const/4 v0, 0x1

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 2100209
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-static {v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getBounds(Lcom/facebook/rtc/views/RtcSpringDragView;)LX/EHi;

    move-result-object v9

    .line 2100210
    iget-object v0, p0, LX/EHk;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 2100211
    iget-object v0, p0, LX/EHk;->a:Landroid/widget/Scroller;

    iget-object v1, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v1, v1, Lcom/facebook/rtc/views/RtcSpringDragView;->k:I

    iget-object v2, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v2, v2, Lcom/facebook/rtc/views/RtcSpringDragView;->l:I

    float-to-int v3, p3

    float-to-int v4, p4

    iget v5, v9, LX/EHi;->a:I

    iget v6, v9, LX/EHi;->b:I

    iget v7, v9, LX/EHi;->c:I

    iget v8, v9, LX/EHi;->d:I

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 2100212
    iget v0, v9, LX/EHi;->a:I

    iget v1, v9, LX/EHi;->b:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v10

    .line 2100213
    iget v1, v9, LX/EHi;->c:I

    iget v2, v9, LX/EHi;->d:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    div-float v2, v1, v10

    .line 2100214
    iget-object v1, p0, LX/EHk;->a:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getFinalX()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    iget v0, v9, LX/EHi;->b:I

    move v1, v0

    .line 2100215
    :goto_0
    iget-object v0, p0, LX/EHk;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalY()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    iget v0, v9, LX/EHi;->d:I

    move v2, v0

    .line 2100216
    :goto_1
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    const/4 v3, 0x0

    .line 2100217
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/rtc/views/RtcSpringDragView;->a$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;IIZ)V

    .line 2100218
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v0, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->h:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2100219
    iget-object v3, v0, LX/EDx;->ck:LX/EDt;

    move-object v0, v3

    .line 2100220
    sget-object v3, LX/EDt;->TOP_LEFT:LX/EDt;

    if-ne v0, v3, :cond_0

    .line 2100221
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v0, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->h:I

    add-int/2addr v2, v0

    .line 2100222
    :cond_0
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->i:LX/0wd;

    iget-object v3, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v3, v3, Lcom/facebook/rtc/views/RtcSpringDragView;->o:I

    int-to-double v4, v3

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 2100223
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->i:LX/0wd;

    float-to-double v4, p3

    invoke-virtual {v0, v4, v5}, LX/0wd;->c(D)LX/0wd;

    .line 2100224
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->i:LX/0wd;

    int-to-double v4, v1

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 2100225
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->j:LX/0wd;

    iget-object v1, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v1, v1, Lcom/facebook/rtc/views/RtcSpringDragView;->p:I

    int-to-double v4, v1

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 2100226
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->j:LX/0wd;

    float-to-double v4, p4

    invoke-virtual {v0, v4, v5}, LX/0wd;->c(D)LX/0wd;

    .line 2100227
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->j:LX/0wd;

    int-to-double v2, v2

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2100228
    const/4 v0, 0x1

    return v0

    .line 2100229
    :cond_1
    iget v0, v9, LX/EHi;->a:I

    move v1, v0

    goto :goto_0

    .line 2100230
    :cond_2
    iget v0, v9, LX/EHi;->c:I

    move v2, v0

    goto :goto_1
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .prologue
    .line 2100199
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 2100200
    iput v1, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->k:I

    .line 2100201
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 2100202
    iput v1, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->l:I

    .line 2100203
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v1, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v1, v1, Lcom/facebook/rtc/views/RtcSpringDragView;->k:I

    iget-object v2, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v2, v2, Lcom/facebook/rtc/views/RtcSpringDragView;->m:I

    sub-int/2addr v1, v2

    .line 2100204
    iput v1, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->o:I

    .line 2100205
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v1, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v1, v1, Lcom/facebook/rtc/views/RtcSpringDragView;->l:I

    iget-object v2, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget v2, v2, Lcom/facebook/rtc/views/RtcSpringDragView;->n:I

    sub-int/2addr v1, v2

    .line 2100206
    iput v1, v0, Lcom/facebook/rtc/views/RtcSpringDragView;->p:I

    .line 2100207
    iget-object v0, p0, LX/EHk;->b:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-static {v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->f(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    .line 2100208
    const/4 v0, 0x0

    return v0
.end method
