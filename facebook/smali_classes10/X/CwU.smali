.class public final enum LX/CwU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CwU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CwU;

.field public static final enum NULL_STATE:LX/CwU;

.field public static final enum SINGLE_STATE:LX/CwU;

.field public static final enum TYPED:LX/CwU;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1950749
    new-instance v0, LX/CwU;

    const-string v1, "TYPED"

    invoke-direct {v0, v1, v2}, LX/CwU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwU;->TYPED:LX/CwU;

    .line 1950750
    new-instance v0, LX/CwU;

    const-string v1, "NULL_STATE"

    invoke-direct {v0, v1, v3}, LX/CwU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwU;->NULL_STATE:LX/CwU;

    .line 1950751
    new-instance v0, LX/CwU;

    const-string v1, "SINGLE_STATE"

    invoke-direct {v0, v1, v4}, LX/CwU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwU;->SINGLE_STATE:LX/CwU;

    .line 1950752
    const/4 v0, 0x3

    new-array v0, v0, [LX/CwU;

    sget-object v1, LX/CwU;->TYPED:LX/CwU;

    aput-object v1, v0, v2

    sget-object v1, LX/CwU;->NULL_STATE:LX/CwU;

    aput-object v1, v0, v3

    sget-object v1, LX/CwU;->SINGLE_STATE:LX/CwU;

    aput-object v1, v0, v4

    sput-object v0, LX/CwU;->$VALUES:[LX/CwU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1950753
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CwU;
    .locals 1

    .prologue
    .line 1950754
    const-class v0, LX/CwU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CwU;

    return-object v0
.end method

.method public static values()[LX/CwU;
    .locals 1

    .prologue
    .line 1950755
    sget-object v0, LX/CwU;->$VALUES:[LX/CwU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CwU;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950756
    invoke-virtual {p0}, LX/CwU;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
