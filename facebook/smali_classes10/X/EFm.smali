.class public LX/EFm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EG4;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/EDx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2096316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096317
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2096318
    iput-object v0, p0, LX/EFm;->b:LX/0Ot;

    .line 2096319
    return-void
.end method

.method public static a(LX/EFm;JLcom/facebook/widget/tiles/ThreadTileView;)V
    .locals 5

    .prologue
    .line 2096274
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, LX/EFm;->a(Ljava/lang/String;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2096275
    :goto_0
    return-void

    .line 2096276
    :cond_0
    iget-object v0, p0, LX/EFm;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EG4;

    iget-object v1, p0, LX/EFm;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {p1, p2, v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v0}, LX/EG4;->a()LX/8Vc;

    move-result-object v0

    .line 2096277
    invoke-virtual {p3, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2096278
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, LX/EFm;->b(Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2096279
    const v0, 0x7f0d002d

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 2096280
    if-nez p0, :cond_1

    .line 2096281
    if-nez v0, :cond_0

    move v0, v1

    .line 2096282
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 2096283
    goto :goto_0

    .line 2096284
    :cond_1
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/EFm;
    .locals 4

    .prologue
    .line 2096285
    new-instance v1, LX/EFm;

    invoke-direct {v1}, LX/EFm;-><init>()V

    .line 2096286
    const/16 v0, 0x15e8

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v0, 0x3261

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/EDx;->a(LX/0QB;)LX/EDx;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2096287
    iput-object v2, v1, LX/EFm;->a:LX/0Or;

    iput-object v3, v1, LX/EFm;->b:LX/0Ot;

    iput-object v0, v1, LX/EFm;->c:LX/EDx;

    .line 2096288
    return-object v1
.end method

.method private static b(Ljava/lang/String;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2096289
    const v0, 0x7f0d002d

    invoke-virtual {p1, v0, p0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2096290
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/user/tiles/UserTileView;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2096291
    iget-object v0, p0, LX/EFm;->c:LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2096292
    iget-object v0, p0, LX/EFm;->c:LX/EDx;

    .line 2096293
    iget-object v2, v0, LX/EDx;->af:LX/EGE;

    move-object v0, v2

    .line 2096294
    if-eqz v0, :cond_3

    .line 2096295
    iget-object v0, v0, LX/EGE;->b:Ljava/lang/String;

    .line 2096296
    :goto_0
    invoke-static {v0, p1}, LX/EFm;->a(Ljava/lang/String;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2096297
    :goto_1
    return-void

    .line 2096298
    :cond_0
    iget-object v0, p0, LX/EFm;->c:LX/EDx;

    .line 2096299
    iget-wide v4, v0, LX/EDx;->ak:J

    move-wide v2, v4

    .line 2096300
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2096301
    :cond_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2096302
    invoke-virtual {p1, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2096303
    :goto_2
    invoke-static {v0, p1}, LX/EFm;->b(Ljava/lang/String;Landroid/view/View;)V

    goto :goto_1

    .line 2096304
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-static {v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/widget/tiles/ThreadTileView;)V
    .locals 4

    .prologue
    .line 2096305
    iget-object v0, p0, LX/EFm;->c:LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2096306
    iget-object v0, p0, LX/EFm;->c:LX/EDx;

    .line 2096307
    iget-object v1, v0, LX/EDx;->al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v1

    .line 2096308
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2096309
    invoke-static {v1, p1}, LX/EFm;->a(Ljava/lang/String;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2096310
    :goto_0
    return-void

    .line 2096311
    :cond_0
    iget-object v0, p0, LX/EFm;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EG4;

    invoke-interface {v0}, LX/EG4;->a()LX/8Vc;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2096312
    invoke-static {v1, p1}, LX/EFm;->b(Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0

    .line 2096313
    :cond_1
    iget-object v0, p0, LX/EFm;->c:LX/EDx;

    .line 2096314
    iget-wide v2, v0, LX/EDx;->ak:J

    move-wide v0, v2

    .line 2096315
    invoke-static {p0, v0, v1, p1}, LX/EFm;->a(LX/EFm;JLcom/facebook/widget/tiles/ThreadTileView;)V

    goto :goto_0
.end method
