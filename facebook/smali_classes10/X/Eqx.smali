.class public LX/Eqx;
.super LX/A8X;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:Z

.field public final c:LX/ErX;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:I

.field public g:LX/EqG;

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0kL;

.field public j:LX/ErW;


# direct methods
.method public constructor <init>(LX/ErW;LX/0kL;Ljava/lang/String;ZILX/0Px;Ljava/util/Set;LX/EqG;LX/ErX;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/util/Set;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/EqG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/ErX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ErW;",
            "LX/0kL;",
            "Ljava/lang/String;",
            "ZI",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/events/invite/EventsExtendedInviteFriendSelectionChangedListener;",
            "LX/ErX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2172327
    invoke-direct {p0}, LX/A8X;-><init>()V

    .line 2172328
    const v0, 0x7fffffff

    iput v0, p0, LX/Eqx;->f:I

    .line 2172329
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2172330
    iput-object v0, p0, LX/Eqx;->h:LX/0Px;

    .line 2172331
    iput-object p1, p0, LX/Eqx;->j:LX/ErW;

    .line 2172332
    iput-object p2, p0, LX/Eqx;->i:LX/0kL;

    .line 2172333
    iput-object p3, p0, LX/Eqx;->a:Ljava/lang/String;

    .line 2172334
    iput-boolean p4, p0, LX/Eqx;->b:Z

    .line 2172335
    iput p5, p0, LX/Eqx;->f:I

    .line 2172336
    iput-object p6, p0, LX/Eqx;->h:LX/0Px;

    .line 2172337
    iput-object p7, p0, LX/Eqx;->d:Ljava/util/Set;

    .line 2172338
    iput-object p8, p0, LX/Eqx;->g:LX/EqG;

    .line 2172339
    iput-object p9, p0, LX/Eqx;->c:LX/ErX;

    .line 2172340
    return-void
.end method

.method public static d(LX/Eqx;)I
    .locals 1

    .prologue
    .line 2172326
    iget-object v0, p0, LX/Eqx;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2172323
    iput-object p1, p0, LX/Eqx;->h:LX/0Px;

    .line 2172324
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2172325
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2172311
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2172312
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2172313
    :pswitch_0
    check-cast p1, LX/BWk;

    .line 2172314
    iget-object v0, p0, LX/Eqx;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/BWk;->a(Ljava/lang/String;)V

    .line 2172315
    invoke-virtual {p1, v4}, LX/BWk;->setFocusable(Z)V

    .line 2172316
    :goto_0
    return-void

    .line 2172317
    :pswitch_1
    check-cast p1, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    .line 2172318
    iget-object v0, p0, LX/Eqx;->h:LX/0Px;

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2172319
    iget-object v2, p0, LX/Eqx;->d:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a(LX/8QL;Z)V

    .line 2172320
    invoke-virtual {p1, v1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->setAsHeaderItem(Z)V

    .line 2172321
    invoke-virtual {p1, v4}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->setFocusable(Z)V

    goto :goto_0

    .line 2172322
    :pswitch_2
    const v0, 0x7f0d0f32

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, LX/Eqx;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final d(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 2172341
    packed-switch p2, :pswitch_data_0

    .line 2172342
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2172343
    :pswitch_0
    new-instance v0, LX/BWk;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/BWk;-><init>(Landroid/content/Context;)V

    .line 2172344
    :goto_0
    return-object v0

    .line 2172345
    :pswitch_1
    new-instance v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2172346
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2172347
    const v1, 0x7f03057a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2172310
    iget-object v0, p0, LX/Eqx;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2172304
    if-nez p1, :cond_0

    .line 2172305
    iget-object v0, p0, LX/Eqx;->a:Ljava/lang/String;

    .line 2172306
    :goto_0
    return-object v0

    .line 2172307
    :cond_0
    invoke-static {p0}, LX/Eqx;->d(LX/Eqx;)I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2172308
    const/4 v0, 0x0

    goto :goto_0

    .line 2172309
    :cond_1
    iget-object v0, p0, LX/Eqx;->h:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2172298
    if-nez p1, :cond_0

    .line 2172299
    const/4 v0, 0x1

    .line 2172300
    :goto_0
    return v0

    .line 2172301
    :cond_0
    invoke-static {p0}, LX/Eqx;->d(LX/Eqx;)I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2172302
    const/4 v0, 0x2

    goto :goto_0

    .line 2172303
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2172297
    const/4 v0, 0x3

    return v0
.end method
