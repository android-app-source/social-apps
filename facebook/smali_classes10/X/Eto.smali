.class public LX/Eto;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Etn;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Etp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2178628
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Eto;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Etp;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2178629
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2178630
    iput-object p1, p0, LX/Eto;->b:LX/0Ot;

    .line 2178631
    return-void
.end method

.method public static a(LX/0QB;)LX/Eto;
    .locals 4

    .prologue
    .line 2178632
    const-class v1, LX/Eto;

    monitor-enter v1

    .line 2178633
    :try_start_0
    sget-object v0, LX/Eto;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2178634
    sput-object v2, LX/Eto;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2178635
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2178636
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2178637
    new-instance v3, LX/Eto;

    const/16 p0, 0x2225

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Eto;-><init>(LX/0Ot;)V

    .line 2178638
    move-object v0, v3

    .line 2178639
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2178640
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Eto;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2178641
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2178642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 2178643
    check-cast p1, LX/Etm;

    .line 2178644
    iget-object v0, p0, LX/Eto;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Etp;

    iget-object v1, p1, LX/Etm;->d:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    iget-object v2, p1, LX/Etm;->e:LX/2h7;

    .line 2178645
    iget-object v3, v0, LX/Etp;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2iT;

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v9

    move-object v8, v2

    invoke-virtual/range {v4 .. v9}, LX/2iT;->a(JLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2178646
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2178647
    check-cast p2, LX/Etm;

    .line 2178648
    iget-object v0, p0, LX/Eto;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Etp;

    iget-boolean v1, p2, LX/Etm;->a:Z

    iget v2, p2, LX/Etm;->b:I

    iget v3, p2, LX/Etm;->c:I

    .line 2178649
    iget-object v4, v0, LX/Etp;->a:LX/Eu4;

    invoke-virtual {v4, p1}, LX/Eu4;->c(LX/1De;)LX/Eu2;

    move-result-object v4

    const p0, 0x7f080f7b

    invoke-virtual {v4, p0}, LX/Eu2;->h(I)LX/Eu2;

    move-result-object v4

    const p0, 0x7f080f7c

    .line 2178650
    iget-object p2, v4, LX/Eu2;->a:LX/Eu3;

    invoke-virtual {v4, p0}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/Eu3;->b:Ljava/lang/CharSequence;

    .line 2178651
    move-object v4, v4

    .line 2178652
    invoke-virtual {v4, v2}, LX/Eu2;->l(I)LX/Eu2;

    move-result-object v4

    const p0, 0x7f080f7b

    invoke-virtual {v4, p0}, LX/Eu2;->j(I)LX/Eu2;

    move-result-object v4

    const p0, 0x7f0207a0

    .line 2178653
    iget-object p2, v4, LX/Eu2;->a:LX/Eu3;

    iput p0, p2, LX/Eu3;->f:I

    .line 2178654
    move-object v4, v4

    .line 2178655
    const p0, 0x7f0a09f6

    .line 2178656
    iget-object p2, v4, LX/Eu2;->a:LX/Eu3;

    iput p0, p2, LX/Eu3;->g:I

    .line 2178657
    move-object p0, v4

    .line 2178658
    if-eqz v1, :cond_0

    const v4, 0x7f020b78

    :goto_0
    invoke-virtual {p0, v4}, LX/Eu2;->k(I)LX/Eu2;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 2178659
    const p0, 0x1884322b

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2178660
    invoke-interface {v4, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    const/4 p0, 0x2

    invoke-interface {v4, p0, v3}, LX/1Di;->a(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x5

    invoke-interface {v4, p0, v3}, LX/1Di;->a(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2178661
    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2178662
    invoke-static {}, LX/1dS;->b()V

    .line 2178663
    iget v0, p1, LX/1dQ;->b:I

    .line 2178664
    packed-switch v0, :pswitch_data_0

    .line 2178665
    :goto_0
    return-object v1

    .line 2178666
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/Eto;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1884322b
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/Etn;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2178667
    new-instance v1, LX/Etm;

    invoke-direct {v1, p0}, LX/Etm;-><init>(LX/Eto;)V

    .line 2178668
    sget-object v2, LX/Eto;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Etn;

    .line 2178669
    if-nez v2, :cond_0

    .line 2178670
    new-instance v2, LX/Etn;

    invoke-direct {v2}, LX/Etn;-><init>()V

    .line 2178671
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Etn;->a$redex0(LX/Etn;LX/1De;IILX/Etm;)V

    .line 2178672
    move-object v1, v2

    .line 2178673
    move-object v0, v1

    .line 2178674
    return-object v0
.end method
