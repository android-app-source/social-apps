.class public final LX/DGW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AmK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AmK",
        "<",
        "Lcom/facebook/drawee/view/GenericDraweeView;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Ds;

.field public final synthetic b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;LX/3Ds;)V
    .locals 0

    .prologue
    .line 1980009
    iput-object p1, p0, LX/DGW;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    iput-object p2, p0, LX/DGW;->a:LX/3Ds;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1979991
    iget-object v0, p0, LX/DGW;->a:LX/3Ds;

    iget-object v0, v0, LX/3Ds;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1979992
    const/4 v0, 0x0

    .line 1979993
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/DGW;->a:LX/3Ds;

    iget-object v0, v0, LX/3Ds;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1bf;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1980002
    invoke-direct {p0}, LX/DGW;->b()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1980003
    iget-object v0, p0, LX/DGW;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->e:LX/03V;

    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Couldn\'t find an image in this link attachment"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1980004
    const/4 v0, 0x0

    .line 1980005
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, LX/DGW;->b()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    const/4 v1, 0x1

    .line 1980006
    iput-boolean v1, v0, LX/1bX;->g:Z

    .line 1980007
    move-object v0, v0

    .line 1980008
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;LX/1aZ;)V
    .locals 3
    .param p2    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1979994
    check-cast p1, Lcom/facebook/drawee/view/GenericDraweeView;

    .line 1979995
    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    .line 1979996
    iget-object v1, p0, LX/DGW;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;->d:Landroid/content/res/Resources;

    const v2, 0x7f0a045d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1979997
    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1979998
    invoke-virtual {p1, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1979999
    const v0, 0x3ff745d1

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1980000
    iget-object v0, p0, LX/DGW;->a:LX/3Ds;

    iget-object v0, v0, LX/3Ds;->b:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/GenericDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1980001
    return-void
.end method
