.class public final LX/D4D;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/D4G;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/D4G;)V
    .locals 1

    .prologue
    .line 1961904
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1961905
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/D4D;->a:Ljava/lang/ref/WeakReference;

    .line 1961906
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1961907
    iget-object v0, p0, LX/D4D;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D4G;

    .line 1961908
    if-nez v0, :cond_1

    .line 1961909
    :cond_0
    :goto_0
    return-void

    .line 1961910
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1961911
    :pswitch_0
    iget-object v1, v0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/D4G;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/D5z;

    .line 1961912
    :goto_1
    if-eqz v1, :cond_3

    .line 1961913
    iget-object v3, v1, LX/D5z;->r:Ljava/lang/String;

    move-object v1, v3

    .line 1961914
    :goto_2
    if-eqz v1, :cond_4

    iget-object v3, v0, LX/D4G;->i:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-virtual {v3, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g(Ljava/lang/String;)LX/2oN;

    move-result-object v1

    sget-object v3, LX/2oN;->NONE:LX/2oN;

    if-eq v1, v3, :cond_4

    move v1, v2

    .line 1961915
    :goto_3
    invoke-static {v0}, LX/D4G;->d$redex0(LX/D4G;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez v1, :cond_0

    .line 1961916
    invoke-virtual {v0, v2}, LX/D4G;->a(Z)V

    goto :goto_0

    :cond_2
    move-object v1, v3

    .line 1961917
    goto :goto_1

    :cond_3
    move-object v1, v3

    .line 1961918
    goto :goto_2

    .line 1961919
    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
