.class public LX/DJw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1986210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/DZC;LX/0W9;ZZZZ)Landroid/content/Intent;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1986205
    sget-object v0, LX/21D;->GROUP_FEED:LX/21D;

    const-string v1, "groupCommerceComposerHelper"

    invoke-static {p1, p2}, LX/DJw;->c(LX/DZC;LX/0W9;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, LX/89I;

    invoke-virtual {p1}, LX/DZC;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    sget-object v5, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v4, v6, v7, v5}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {p1}, LX/DZC;->b()Ljava/lang/String;

    move-result-object v5

    .line 1986206
    iput-object v5, v4, LX/89I;->c:Ljava/lang/String;

    .line 1986207
    move-object v4, v4

    .line 1986208
    invoke-virtual {v4}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    invoke-static {p1}, LX/DJw;->k(LX/DZC;)LX/0Px;

    move-result-object v5

    invoke-static {v0, v1, v2, v4, v5}, LX/1nC;->a(LX/21D;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_GROUP_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89K;

    invoke-direct {v1}, LX/89K;-><init>()V

    invoke-static {}, LX/88g;->c()LX/88g;

    move-result-object v1

    invoke-static {v1}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    .line 1986209
    invoke-static/range {v0 .. v8}, LX/DJw;->a(Landroid/content/Context;LX/DZC;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ZZZZZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;LX/DZC;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ZZZZZZ)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 1986187
    invoke-static {p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-virtual {p1}, LX/DZC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {p1}, LX/DZC;->b()Ljava/lang/String;

    move-result-object v2

    .line 1986188
    iput-object v2, v1, LX/89I;->c:Ljava/lang/String;

    .line 1986189
    move-object v1, v1

    .line 1986190
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 1986191
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->GROUP:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    .line 1986192
    iput-object v0, v1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1986193
    move-object v0, v1

    .line 1986194
    if-eqz p3, :cond_0

    .line 1986195
    invoke-virtual {v0}, LX/8AA;->b()LX/8AA;

    .line 1986196
    :cond_0
    if-eqz p4, :cond_1

    .line 1986197
    invoke-virtual {v0}, LX/8AA;->s()LX/8AA;

    .line 1986198
    :cond_1
    if-eqz p5, :cond_2

    .line 1986199
    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    .line 1986200
    :cond_2
    if-eqz p7, :cond_3

    .line 1986201
    invoke-virtual {v0}, LX/8AA;->g()LX/8AA;

    .line 1986202
    if-eqz p8, :cond_3

    .line 1986203
    invoke-virtual {v0}, LX/8AA;->h()LX/8AA;

    .line 1986204
    :cond_3
    invoke-static {p0, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/DZC;ZZZZZLcom/facebook/ipc/composer/intent/ComposerPageData;)Landroid/content/Intent;
    .locals 10
    .param p7    # Lcom/facebook/ipc/composer/intent/ComposerPageData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1986120
    sget-object v1, LX/21D;->GROUP_FEED:LX/21D;

    const-string v2, "groupCommerce"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const-string v2, "ANDROID_GROUP_COMPOSER"

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setShowPageVoiceSwitcher(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    .line 1986121
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move v7, p3

    move v8, p4

    move v9, p5

    invoke-static/range {v1 .. v9}, LX/DJw;->a(Landroid/content/Context;LX/DZC;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ZZZZZZ)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static a(LX/DZC;LX/03R;LX/0W9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 5

    .prologue
    .line 1986174
    invoke-static {p0}, LX/DJw;->a(LX/DZC;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    if-ne p1, v0, :cond_1

    .line 1986175
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1986176
    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    move v0, v0

    .line 1986177
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1986178
    if-eqz v0, :cond_0

    .line 1986179
    invoke-static {p0, p2}, LX/DJw;->b(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 1986180
    sget-object v1, LX/2rt;->STATUS:LX/2rt;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 1986181
    :goto_2
    return-object v0

    .line 1986182
    :cond_0
    sget-object v0, LX/21D;->GROUP_FEED:LX/21D;

    const-string v1, "groupCommerce"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-virtual {p0}, LX/DZC;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {p0}, LX/DZC;->b()Ljava/lang/String;

    move-result-object v2

    .line 1986183
    iput-object v2, v1, LX/89I;->c:Ljava/lang/String;

    .line 1986184
    move-object v1, v1

    .line 1986185
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 1986186
    const-string v1, "group_composer"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_GROUP_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->j()Z

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;
    .locals 3

    .prologue
    .line 1986150
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v0

    invoke-static {p0, p1}, LX/DJw;->c(LX/DZC;LX/0W9;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setCurrencyCode(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v0

    .line 1986151
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1986152
    if-nez v1, :cond_1

    .line 1986153
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1986154
    :goto_0
    move-object v1, v1

    .line 1986155
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setInterceptWords(LX/0Px;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v0

    .line 1986156
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1986157
    if-nez v1, :cond_2

    .line 1986158
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1986159
    :goto_1
    move-object v1, v1

    .line 1986160
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setInterceptWordsAfterNumber(LX/0Px;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v0

    invoke-static {p0}, LX/DJw;->l(LX/DZC;)Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setMarketplaceCrossPostSettingModel(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v0

    .line 1986161
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1986162
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->k()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$LocationPickerSettingModel;

    move-result-object v2

    if-nez v2, :cond_3

    .line 1986163
    :cond_0
    const/4 v1, 0x0

    .line 1986164
    :goto_2
    move-object v1, v1

    .line 1986165
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setProductItemLocationPickerSettings(Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v0

    .line 1986166
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1986167
    if-nez v1, :cond_4

    const/4 v1, 0x0

    :goto_3
    move v1, v1

    .line 1986168
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setIsCategoryOptional(Z)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v0

    .line 1986169
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1986170
    if-nez v1, :cond_5

    const/4 v1, 0x0

    :goto_4
    move-object v1, v1

    .line 1986171
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setPrefillCategoryId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->m()LX/0Px;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->n()LX/0Px;

    move-result-object v1

    goto :goto_1

    .line 1986172
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->k()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$LocationPickerSettingModel;

    move-result-object v1

    .line 1986173
    invoke-static {}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->newBuilder()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$LocationPickerSettingModel;->c()Z

    move-result p1

    invoke-virtual {v2, p1}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->setUseZipCode(Z)Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$LocationPickerSettingModel;->a()Z

    move-result p1

    invoke-virtual {v2, p1}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->setIsCompulsory(Z)Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$LocationPickerSettingModel;->b()Z

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->setUseNeighborhoodDataSource(Z)Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings$Builder;->a()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    move-result-object v1

    goto :goto_2

    :cond_4
    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->hx_()Z

    move-result v1

    goto :goto_3

    :cond_5
    invoke-virtual {v1}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->o()Ljava/lang/String;

    move-result-object v1

    goto :goto_4
.end method

.method public static a(LX/DZC;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1986143
    if-eqz p0, :cond_1

    invoke-virtual {p0}, LX/DZC;->c()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1986144
    invoke-virtual {p0}, LX/DZC;->c()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9TS;

    .line 1986145
    invoke-interface {v0}, LX/9TS;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->FOR_SALE:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    if-ne v0, v5, :cond_0

    .line 1986146
    const/4 v0, 0x1

    .line 1986147
    :goto_1
    return v0

    .line 1986148
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1986149
    goto :goto_1
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1986211
    if-eqz p0, :cond_2

    .line 1986212
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1986213
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 1986214
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->E()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1986215
    const-string v3, "for_sale"

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 1986216
    :cond_0
    return v1

    :cond_1
    move v0, v1

    .line 1986217
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 7

    .prologue
    .line 1986134
    sget-object v0, LX/21D;->GROUP_FEED:LX/21D;

    const-string v1, "groupCommerceComposerHelper"

    invoke-static {p0, p1}, LX/DJw;->c(LX/DZC;LX/0W9;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/89I;

    invoke-virtual {p0}, LX/DZC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v6, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v3, v4, v5, v6}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {p0}, LX/DZC;->b()Ljava/lang/String;

    move-result-object v4

    .line 1986135
    iput-object v4, v3, LX/89I;->c:Ljava/lang/String;

    .line 1986136
    move-object v3, v3

    .line 1986137
    invoke-virtual {v3}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    invoke-static {p0}, LX/DJw;->k(LX/DZC;)LX/0Px;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 1986138
    const-string v1, "group_composer"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {p0, p1}, LX/DJw;->a(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setCommerceInfo(Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 1986139
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1986140
    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move v2, v2

    .line 1986141
    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setShouldPostToMarketplaceByDefault(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const-string v2, "ANDROID_GROUP_COMPOSER"

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    new-instance v2, LX/89K;

    invoke-direct {v2}, LX/89K;-><init>()V

    invoke-static {}, LX/88g;->c()LX/88g;

    move-result-object v2

    invoke-static {v2}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1986142
    return-object v0

    :cond_0
    invoke-virtual {v2}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->q()Z

    move-result v2

    goto :goto_0
.end method

.method public static c(LX/DZC;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1986130
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1986131
    if-nez v0, :cond_0

    .line 1986132
    const/4 v0, 0x0

    .line 1986133
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(LX/DZC;LX/0W9;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1986122
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1986123
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1986124
    :cond_0
    :try_start_0
    invoke-virtual {p1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    .line 1986125
    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/util/Locale;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1986126
    :goto_0
    move-object v0, v0

    .line 1986127
    :goto_1
    return-object v0

    .line 1986128
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->d()LX/0Px;

    move-result-object v0

    .line 1986129
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1

    :catch_0
    const-string v0, "USD"

    goto :goto_0
.end method

.method public static d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1986111
    if-nez p0, :cond_0

    move-object v0, v1

    .line 1986112
    :goto_0
    return-object v0

    .line 1986113
    :cond_0
    iget-object v0, p0, LX/DZC;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->O()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v0

    move-object v0, v0

    .line 1986114
    if-eqz v0, :cond_2

    .line 1986115
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel;

    .line 1986116
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel;->a()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1986117
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel;->a()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v0

    goto :goto_0

    .line 1986118
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1986119
    goto :goto_0
.end method

.method public static k(LX/DZC;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/protocol/GroupInformationInterface;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1986093
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1986094
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->e()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    move-object v0, v1

    .line 1986095
    :cond_1
    :goto_0
    return-object v0

    .line 1986096
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->e()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel;

    move-result-object v0

    .line 1986097
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1986098
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel$ForSaleCategoriesEdgesModel;

    .line 1986099
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel$ForSaleCategoriesEdgesModel;->a()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel$ForSaleCategoriesEdgesModel$ForSaleCategoriesEdgesNodeModel;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 1986100
    new-instance v6, LX/88i;

    invoke-direct {v6}, LX/88i;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel$ForSaleCategoriesEdgesModel;->a()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel$ForSaleCategoriesEdgesModel$ForSaleCategoriesEdgesNodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel$ForSaleCategoriesEdgesModel$ForSaleCategoriesEdgesNodeModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 1986101
    iput-object v7, v6, LX/88i;->a:Ljava/lang/String;

    .line 1986102
    move-object v6, v6

    .line 1986103
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel$ForSaleCategoriesEdgesModel;->a()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel$ForSaleCategoriesEdgesModel$ForSaleCategoriesEdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$ForSaleCategoriesModel$ForSaleCategoriesEdgesModel$ForSaleCategoriesEdgesNodeModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1986104
    iput-object v0, v6, LX/88i;->b:Ljava/lang/String;

    .line 1986105
    move-object v0, v6

    .line 1986106
    new-instance v6, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;

    invoke-direct {v6, v0}, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;-><init>(LX/88i;)V

    move-object v0, v6

    .line 1986107
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1986108
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1986109
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1986110
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    goto :goto_0
.end method

.method private static l(LX/DZC;)Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;
    .locals 3

    .prologue
    .line 1986047
    invoke-static {p0}, LX/DJw;->d(LX/DZC;)Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1986048
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->l()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1986049
    :cond_0
    const/4 v0, 0x0

    .line 1986050
    :goto_0
    return-object v0

    .line 1986051
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel;->l()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;

    move-result-object v0

    .line 1986052
    new-instance v1, LX/5Rf;

    invoke-direct {v1}, LX/5Rf;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->e()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1986053
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    iput-boolean p0, v1, LX/5Rf;->a:Z

    .line 1986054
    move-object v1, v1

    .line 1986055
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->hy_()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1986056
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    iput-boolean p0, v1, LX/5Rf;->b:Z

    .line 1986057
    move-object v1, v1

    .line 1986058
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1986059
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    iput-boolean p0, v1, LX/5Rf;->c:Z

    .line 1986060
    move-object v1, v1

    .line 1986061
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->k()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1986062
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    iput-boolean p0, v1, LX/5Rf;->d:Z

    .line 1986063
    move-object v1, v1

    .line 1986064
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->d()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1986065
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    iput-boolean p0, v1, LX/5Rf;->e:Z

    .line 1986066
    move-object v1, v1

    .line 1986067
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1986068
    iput-object v2, v1, LX/5Rf;->g:Ljava/lang/String;

    .line 1986069
    move-object v1, v1

    .line 1986070
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->hz_()Ljava/lang/String;

    move-result-object v2

    .line 1986071
    iput-object v2, v1, LX/5Rf;->f:Ljava/lang/String;

    .line 1986072
    move-object v1, v1

    .line 1986073
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 1986074
    iput-object v2, v1, LX/5Rf;->h:Ljava/lang/String;

    .line 1986075
    move-object v1, v1

    .line 1986076
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 1986077
    iput-object v2, v1, LX/5Rf;->i:Ljava/lang/String;

    .line 1986078
    move-object v1, v1

    .line 1986079
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1986080
    iput-object v2, v1, LX/5Rf;->j:Ljava/lang/String;

    .line 1986081
    move-object v1, v1

    .line 1986082
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1986083
    iput-object v2, v1, LX/5Rf;->k:Ljava/lang/String;

    .line 1986084
    move-object v1, v1

    .line 1986085
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->n()Ljava/lang/String;

    move-result-object v2

    .line 1986086
    iput-object v2, v1, LX/5Rf;->l:Ljava/lang/String;

    .line 1986087
    move-object v1, v1

    .line 1986088
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel$EdgesModel$NodeModel$MarketplaceCrossPostSettingModel;->o()Ljava/lang/String;

    move-result-object v0

    .line 1986089
    iput-object v0, v1, LX/5Rf;->m:Ljava/lang/String;

    .line 1986090
    move-object v0, v1

    .line 1986091
    new-instance v1, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    invoke-direct {v1, v0}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;-><init>(LX/5Rf;)V

    move-object v0, v1

    .line 1986092
    goto/16 :goto_0
.end method
