.class public final enum LX/EiF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EiF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EiF;

.field public static final enum NORMAL:LX/EiF;

.field public static final enum PRIORITY_FB_TOKEN_1:LX/EiF;

.field public static final enum PRIORITY_FB_TOKEN_2:LX/EiF;

.field public static final enum PRIORITY_SENDER:LX/EiF;

.field public static final enum RETRY:LX/EiF;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2159218
    new-instance v0, LX/EiF;

    const-string v1, "PRIORITY_FB_TOKEN_1"

    invoke-direct {v0, v1, v2}, LX/EiF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiF;->PRIORITY_FB_TOKEN_1:LX/EiF;

    .line 2159219
    new-instance v0, LX/EiF;

    const-string v1, "PRIORITY_FB_TOKEN_2"

    invoke-direct {v0, v1, v3}, LX/EiF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiF;->PRIORITY_FB_TOKEN_2:LX/EiF;

    .line 2159220
    new-instance v0, LX/EiF;

    const-string v1, "PRIORITY_SENDER"

    invoke-direct {v0, v1, v4}, LX/EiF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiF;->PRIORITY_SENDER:LX/EiF;

    .line 2159221
    new-instance v0, LX/EiF;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v5}, LX/EiF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiF;->NORMAL:LX/EiF;

    .line 2159222
    new-instance v0, LX/EiF;

    const-string v1, "RETRY"

    invoke-direct {v0, v1, v6}, LX/EiF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EiF;->RETRY:LX/EiF;

    .line 2159223
    const/4 v0, 0x5

    new-array v0, v0, [LX/EiF;

    sget-object v1, LX/EiF;->PRIORITY_FB_TOKEN_1:LX/EiF;

    aput-object v1, v0, v2

    sget-object v1, LX/EiF;->PRIORITY_FB_TOKEN_2:LX/EiF;

    aput-object v1, v0, v3

    sget-object v1, LX/EiF;->PRIORITY_SENDER:LX/EiF;

    aput-object v1, v0, v4

    sget-object v1, LX/EiF;->NORMAL:LX/EiF;

    aput-object v1, v0, v5

    sget-object v1, LX/EiF;->RETRY:LX/EiF;

    aput-object v1, v0, v6

    sput-object v0, LX/EiF;->$VALUES:[LX/EiF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2159215
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EiF;
    .locals 1

    .prologue
    .line 2159217
    const-class v0, LX/EiF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EiF;

    return-object v0
.end method

.method public static values()[LX/EiF;
    .locals 1

    .prologue
    .line 2159216
    sget-object v0, LX/EiF;->$VALUES:[LX/EiF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EiF;

    return-object v0
.end method
