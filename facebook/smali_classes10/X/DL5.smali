.class public final enum LX/DL5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DL5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DL5;

.field public static final enum FILE_HANDLE_UPLOADING_FAILURE:LX/DL5;

.field public static final enum SEGMENT_UPLOADING_CANCELLATION:LX/DL5;

.field public static final enum SEGMENT_UPLOADING_FAILURE:LX/DL5;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1987796
    new-instance v0, LX/DL5;

    const-string v1, "SEGMENT_UPLOADING_FAILURE"

    invoke-direct {v0, v1, v2}, LX/DL5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DL5;->SEGMENT_UPLOADING_FAILURE:LX/DL5;

    .line 1987797
    new-instance v0, LX/DL5;

    const-string v1, "SEGMENT_UPLOADING_CANCELLATION"

    invoke-direct {v0, v1, v3}, LX/DL5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DL5;->SEGMENT_UPLOADING_CANCELLATION:LX/DL5;

    .line 1987798
    new-instance v0, LX/DL5;

    const-string v1, "FILE_HANDLE_UPLOADING_FAILURE"

    invoke-direct {v0, v1, v4}, LX/DL5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DL5;->FILE_HANDLE_UPLOADING_FAILURE:LX/DL5;

    .line 1987799
    const/4 v0, 0x3

    new-array v0, v0, [LX/DL5;

    sget-object v1, LX/DL5;->SEGMENT_UPLOADING_FAILURE:LX/DL5;

    aput-object v1, v0, v2

    sget-object v1, LX/DL5;->SEGMENT_UPLOADING_CANCELLATION:LX/DL5;

    aput-object v1, v0, v3

    sget-object v1, LX/DL5;->FILE_HANDLE_UPLOADING_FAILURE:LX/DL5;

    aput-object v1, v0, v4

    sput-object v0, LX/DL5;->$VALUES:[LX/DL5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1987795
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DL5;
    .locals 1

    .prologue
    .line 1987793
    const-class v0, LX/DL5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DL5;

    return-object v0
.end method

.method public static values()[LX/DL5;
    .locals 1

    .prologue
    .line 1987794
    sget-object v0, LX/DL5;->$VALUES:[LX/DL5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DL5;

    return-object v0
.end method
