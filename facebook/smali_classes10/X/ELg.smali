.class public final LX/ELg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/1Pn;

.field public final synthetic d:LX/7CM;

.field public final synthetic e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;LX/CzL;Ljava/lang/String;LX/1Pn;LX/7CM;)V
    .locals 0

    .prologue
    .line 2109423
    iput-object p1, p0, LX/ELg;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    iput-object p2, p0, LX/ELg;->a:LX/CzL;

    iput-object p3, p0, LX/ELg;->b:Ljava/lang/String;

    iput-object p4, p0, LX/ELg;->c:LX/1Pn;

    iput-object p5, p0, LX/ELg;->d:LX/7CM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 15

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x194a7e62

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2109424
    iget-object v0, p0, LX/ELg;->a:LX/CzL;

    invoke-virtual {v0}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ELg;->a:LX/CzL;

    invoke-virtual {v0}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8d0;

    invoke-interface {v0}, LX/8d0;->dW_()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2109425
    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x2

    const v2, 0x67b62656

    invoke-static {v0, v1, v2, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2109426
    :goto_0
    return-void

    .line 2109427
    :cond_1
    sget-object v1, LX/0ax;->aA:Ljava/lang/String;

    iget-object v0, p0, LX/ELg;->a:LX/CzL;

    invoke-virtual {v0}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8d0;

    invoke-interface {v0}, LX/8d0;->dW_()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/ELg;->b:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 2109428
    iget-object v0, p0, LX/ELg;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/CvY;

    iget-object v0, p0, LX/ELg;->c:LX/1Pn;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v10

    sget-object v11, LX/8ce;->NAVIGATION:LX/8ce;

    iget-object v12, p0, LX/ELg;->d:LX/7CM;

    iget-object v0, p0, LX/ELg;->c:LX/1Pn;

    check-cast v0, LX/CxP;

    iget-object v1, p0, LX/ELg;->a:LX/CzL;

    invoke-interface {v0, v1}, LX/CxP;->b(LX/CzL;)I

    move-result v13

    iget-object v0, p0, LX/ELg;->a:LX/CzL;

    invoke-virtual {v0}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-static {v0}, LX/CvY;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CvV;

    move-result-object v14

    iget-object v0, p0, LX/ELg;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/ELg;->c:LX/1Pn;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    sget-object v1, LX/8ce;->NAVIGATION:LX/8ce;

    iget-object v2, p0, LX/ELg;->d:LX/7CM;

    iget-object v3, p0, LX/ELg;->a:LX/CzL;

    invoke-virtual {v3}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8d0;

    invoke-interface {v3}, LX/8d0;->dW_()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, LX/ELg;->a:LX/CzL;

    invoke-virtual {v5}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8d0;

    invoke-interface {v5}, LX/8d0;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v0, v7

    move-object v1, v10

    move-object v2, v11

    move-object v3, v12

    move v4, v13

    move-object v5, v14

    invoke-virtual/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2109429
    iget-object v0, p0, LX/ELg;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/ELg;->c:LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v9}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2109430
    const v0, -0x419a1200

    invoke-static {v0, v8}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method
