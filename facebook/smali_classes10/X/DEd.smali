.class public LX/DEd;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public b:Landroid/widget/TextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field private final d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field private e:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1977196
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1977197
    const v0, 0x7f030f87

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1977198
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/DEd;->setOrientation(I)V

    .line 1977199
    const v0, 0x7f021486

    invoke-virtual {p0, v0}, LX/DEd;->setBackgroundResource(I)V

    .line 1977200
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1977201
    invoke-virtual {p0}, LX/DEd;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021484

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1977202
    const v0, 0x7f0d2575

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/DEd;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1977203
    const v0, 0x7f0d2576

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/DEd;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1977204
    const v0, 0x7f0d2577

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/DEd;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1977205
    const/4 v0, 0x0

    iput-object v0, p0, LX/DEd;->b:Landroid/widget/TextView;

    .line 1977206
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1977207
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DEd;->setFooterView(Landroid/view/View;)V

    .line 1977208
    return-void
.end method

.method public setFooterView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1977209
    iget-object v0, p0, LX/DEd;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1977210
    iget-object v0, p0, LX/DEd;->e:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/DEd;->removeView(Landroid/view/View;)V

    .line 1977211
    :cond_0
    iput-object p1, p0, LX/DEd;->e:Landroid/view/View;

    .line 1977212
    iget-object v0, p0, LX/DEd;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1977213
    invoke-virtual {p0, p1}, LX/DEd;->addView(Landroid/view/View;)V

    .line 1977214
    :cond_1
    return-void
.end method

.method public setIconImage(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1977215
    if-eqz p1, :cond_0

    .line 1977216
    iget-object v0, p0, LX/DEd;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1977217
    :goto_0
    return-void

    .line 1977218
    :cond_0
    iget-object v0, p0, LX/DEd;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const v1, 0x7f021494

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    goto :goto_0
.end method

.method public setPlaceTipOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1977219
    iget-object v0, p0, LX/DEd;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1977220
    return-void
.end method

.method public setSourceText(LX/2cx;)V
    .locals 3
    .param p1    # LX/2cx;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1977221
    if-nez p1, :cond_1

    .line 1977222
    iget-object v0, p0, LX/DEd;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1977223
    iget-object v0, p0, LX/DEd;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v1, p0, LX/DEd;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->removeView(Landroid/view/View;)V

    .line 1977224
    const/4 v0, 0x0

    iput-object v0, p0, LX/DEd;->b:Landroid/widget/TextView;

    .line 1977225
    :cond_0
    :goto_0
    return-void

    .line 1977226
    :cond_1
    iget-object v0, p0, LX/DEd;->b:Landroid/widget/TextView;

    if-nez v0, :cond_2

    .line 1977227
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, LX/DEd;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/DEd;->b:Landroid/widget/TextView;

    .line 1977228
    iget-object v0, p0, LX/DEd;->b:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1977229
    iget-object v0, p0, LX/DEd;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v1, p0, LX/DEd;->b:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;I)V

    .line 1977230
    :cond_2
    iget-object v0, p0, LX/DEd;->b:Landroid/widget/TextView;

    move-object v0, v0

    .line 1977231
    const-string v1, "[%s]"

    invoke-static {v1, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setSubText(LX/175;)V
    .locals 1
    .param p1    # LX/175;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1977232
    iget-object v0, p0, LX/DEd;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v0, p1}, LX/DEj;->a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;LX/175;)V

    .line 1977233
    return-void
.end method

.method public setTitle(LX/175;)V
    .locals 3

    .prologue
    .line 1977234
    :try_start_0
    iget-object v0, p0, LX/DEd;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextWithEntities(LX/175;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 1977235
    :goto_0
    return-void

    .line 1977236
    :catch_0
    move-exception v0

    .line 1977237
    const-string v1, "PlaceTipsFeedUnitView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
