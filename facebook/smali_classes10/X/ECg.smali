.class public LX/ECg;
.super LX/0hs;
.source ""


# instance fields
.field public a:LX/ECf;

.field public l:Landroid/view/View;

.field public m:Landroid/view/View;

.field public n:Lcom/facebook/user/tiles/UserTileView;

.field public o:Lcom/facebook/user/tiles/UserTileView;

.field public p:J

.field public q:J


# direct methods
.method public constructor <init>(Landroid/content/Context;IIJJ)V
    .locals 6

    .prologue
    .line 2090279
    invoke-direct {p0, p1, p2, p3}, LX/0hs;-><init>(Landroid/content/Context;II)V

    .line 2090280
    iput-wide p4, p0, LX/ECg;->p:J

    .line 2090281
    iput-wide p6, p0, LX/ECg;->q:J

    .line 2090282
    const-wide/16 v4, 0x0

    .line 2090283
    iget-object v0, p0, LX/0ht;->g:LX/5OY;

    const v1, 0x7f0d2af7

    invoke-virtual {v0, v1}, LX/5OY;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/ECg;->l:Landroid/view/View;

    .line 2090284
    iget-object v0, p0, LX/0ht;->g:LX/5OY;

    const v1, 0x7f0d2af8

    invoke-virtual {v0, v1}, LX/5OY;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/ECg;->m:Landroid/view/View;

    .line 2090285
    iget-object v0, p0, LX/0ht;->g:LX/5OY;

    const v1, 0x7f0d2af5

    invoke-virtual {v0, v1}, LX/5OY;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/ECg;->o:Lcom/facebook/user/tiles/UserTileView;

    .line 2090286
    iget-object v0, p0, LX/0ht;->g:LX/5OY;

    const v1, 0x7f0d2af6

    invoke-virtual {v0, v1}, LX/5OY;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/ECg;->n:Lcom/facebook/user/tiles/UserTileView;

    .line 2090287
    iget-wide v0, p0, LX/ECg;->p:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    iget-object v0, p0, LX/ECg;->o:Lcom/facebook/user/tiles/UserTileView;

    if-eqz v0, :cond_0

    .line 2090288
    iget-object v0, p0, LX/ECg;->o:Lcom/facebook/user/tiles/UserTileView;

    iget-wide v2, p0, LX/ECg;->p:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-static {v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2090289
    :cond_0
    iget-wide v0, p0, LX/ECg;->q:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    iget-object v0, p0, LX/ECg;->n:Lcom/facebook/user/tiles/UserTileView;

    if-eqz v0, :cond_1

    .line 2090290
    iget-object v0, p0, LX/ECg;->n:Lcom/facebook/user/tiles/UserTileView;

    iget-wide v2, p0, LX/ECg;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-static {v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2090291
    :cond_1
    iget-object v0, p0, LX/ECg;->l:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2090292
    iget-object v0, p0, LX/ECg;->l:Landroid/view/View;

    new-instance v1, LX/ECd;

    invoke-direct {v1, p0}, LX/ECd;-><init>(LX/ECg;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2090293
    :cond_2
    iget-object v0, p0, LX/ECg;->m:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 2090294
    iget-object v0, p0, LX/ECg;->m:Landroid/view/View;

    new-instance v1, LX/ECe;

    invoke-direct {v1, p0}, LX/ECe;-><init>(LX/ECg;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2090295
    :cond_3
    return-void
.end method
