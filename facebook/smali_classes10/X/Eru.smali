.class public LX/Eru;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:LX/1Fn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final c:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "LX/Erj;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "LX/Erj;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "LX/Erj;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "LX/Erj;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Ers;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Ert;",
            ">;"
        }
    .end annotation
.end field

.field public i:I

.field public final j:[I

.field public k:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2173797
    const-class v0, LX/Eru;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Eru;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2173788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2173789
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/Eru;->c:Ljava/util/ArrayDeque;

    .line 2173790
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/Eru;->d:Ljava/util/ArrayDeque;

    .line 2173791
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/Eru;->e:Ljava/util/ArrayDeque;

    .line 2173792
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/Eru;->f:Ljava/util/ArrayDeque;

    .line 2173793
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/Eru;->g:Ljava/util/Map;

    .line 2173794
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/Eru;->h:Ljava/util/Map;

    .line 2173795
    invoke-static {}, LX/3T5;->a()[Ljava/lang/Integer;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/Eru;->j:[I

    .line 2173796
    return-void
.end method

.method public static a(Ljava/util/ArrayDeque;)LX/Erj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayDeque",
            "<",
            "LX/Erj;",
            ">;)",
            "LX/Erj;"
        }
    .end annotation

    .prologue
    .line 2173781
    monitor-enter p0

    .line 2173782
    :try_start_0
    invoke-virtual {p0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173783
    const/4 v0, 0x0

    monitor-exit p0

    .line 2173784
    :goto_0
    return-object v0

    .line 2173785
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Erj;

    .line 2173786
    monitor-exit p0

    goto :goto_0

    .line 2173787
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(LX/Erj;)V
    .locals 2

    .prologue
    .line 2173778
    iget-object v1, p0, LX/Eru;->c:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 2173779
    :try_start_0
    iget-object v0, p0, LX/Eru;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 2173780
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(LX/Erj;)V
    .locals 2

    .prologue
    .line 2173775
    iget-object v1, p0, LX/Eru;->d:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 2173776
    :try_start_0
    iget-object v0, p0, LX/Eru;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 2173777
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private c(LX/Erj;)V
    .locals 2

    .prologue
    .line 2173772
    iget-object v1, p0, LX/Eru;->e:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 2173773
    :try_start_0
    iget-object v0, p0, LX/Eru;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 2173774
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private d(LX/Erj;)V
    .locals 2

    .prologue
    .line 2173769
    iget-object v1, p0, LX/Eru;->f:Ljava/util/ArrayDeque;

    monitor-enter v1

    .line 2173770
    :try_start_0
    iget-object v0, p0, LX/Eru;->f:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 2173771
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized j()I
    .locals 3

    .prologue
    .line 2173761
    monitor-enter p0

    const/4 v0, 0x0

    .line 2173762
    :try_start_0
    iget-object v1, p0, LX/Eru;->g:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ers;

    .line 2173763
    iget-object v0, v0, LX/Ers;->c:Ljava/lang/Integer;

    .line 2173764
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-gtz v0, :cond_0

    .line 2173765
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2173766
    goto :goto_0

    .line 2173767
    :cond_1
    monitor-exit p0

    return v1

    .line 2173768
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static k(LX/Eru;)J
    .locals 4

    .prologue
    .line 2173760
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/Eru;->a:LX/1Fn;

    invoke-virtual {v1}, LX/1Fn;->d()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(LX/Erj;Ljava/lang/Integer;)V
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "addToQueue"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    .line 2173798
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2173799
    :goto_0
    iget-object v0, p0, LX/Eru;->g:Ljava/util/Map;

    iget-object v1, p1, LX/Erj;->a:Ljava/lang/String;

    new-instance v2, LX/Ers;

    iget-object v3, p1, LX/Erj;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/Ers;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2173800
    return-void

    .line 2173801
    :pswitch_0
    invoke-direct {p0, p1}, LX/Eru;->a(LX/Erj;)V

    goto :goto_0

    .line 2173802
    :pswitch_1
    invoke-direct {p0, p1}, LX/Eru;->b(LX/Erj;)V

    goto :goto_0

    .line 2173803
    :pswitch_2
    invoke-direct {p0, p1}, LX/Eru;->c(LX/Erj;)V

    goto :goto_0

    .line 2173804
    :pswitch_3
    invoke-direct {p0, p1}, LX/Eru;->d(LX/Erj;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2173687
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Eru;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ert;

    .line 2173688
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Eru;->g:Ljava/util/Map;

    iget-object v2, v0, LX/Ert;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 2173689
    :cond_0
    const/4 v0, 0x0

    .line 2173690
    :goto_0
    monitor-exit p0

    return v0

    .line 2173691
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/Eru;->g:Ljava/util/Map;

    iget-object v2, v0, LX/Ert;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ers;

    .line 2173692
    iget-object v2, v1, LX/Ers;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, v1, LX/Ers;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gez v2, :cond_3

    .line 2173693
    :cond_2
    :goto_1
    iget-object v0, p0, LX/Eru;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2173694
    const/4 v0, 0x1

    goto :goto_0

    .line 2173695
    :cond_3
    iget-object v2, v1, LX/Ers;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2173696
    iput-object v2, v1, LX/Ers;->c:Ljava/lang/Integer;

    .line 2173697
    iget-object v2, p0, LX/Eru;->g:Ljava/util/Map;

    iget-object v0, v0, LX/Ert;->a:Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2173698
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Z
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "registerMediaForOfflineFeed"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 2173707
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Eru;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Eru;->h:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 2173708
    :cond_0
    const/4 v0, 0x0

    .line 2173709
    :goto_0
    monitor-exit p0

    return v0

    .line 2173710
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/Eru;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ers;

    .line 2173711
    iget-object v1, v0, LX/Ers;->c:Ljava/lang/Integer;

    if-nez v1, :cond_2

    .line 2173712
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2173713
    iput-object v1, v0, LX/Ers;->c:Ljava/lang/Integer;

    .line 2173714
    :goto_1
    iget-object v1, p0, LX/Eru;->g:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2173715
    iget-object v0, p0, LX/Eru;->h:Ljava/util/Map;

    new-instance v1, LX/Ert;

    iget-object v2, p0, LX/Eru;->k:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p0}, LX/Eru;->k(LX/Eru;)J

    move-result-wide v4

    add-long/2addr v2, v4

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, LX/Ert;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v7

    .line 2173716
    goto :goto_0

    .line 2173717
    :cond_2
    iget-object v1, v0, LX/Ers;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2173718
    iput-object v1, v0, LX/Ers;->c:Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173719
    goto :goto_1

    .line 2173720
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2173699
    iget v0, p0, LX/Eru;->i:I

    if-nez v0, :cond_0

    .line 2173700
    const/4 v0, 0x0

    .line 2173701
    :goto_0
    return v0

    .line 2173702
    :cond_0
    invoke-direct {p0}, LX/Eru;->j()I

    move-result v0

    .line 2173703
    iget v1, p0, LX/Eru;->i:I

    if-le v0, v1, :cond_1

    .line 2173704
    const/16 v0, 0x64

    goto :goto_0

    .line 2173705
    :cond_1
    mul-int/lit8 v0, v0, 0x64

    iget v1, p0, LX/Eru;->i:I

    div-int/2addr v0, v1

    iget v1, p0, LX/Eru;->l:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/Eru;->l:I

    .line 2173706
    iget v0, p0, LX/Eru;->l:I

    goto :goto_0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2173721
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Eru;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ert;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2173722
    if-nez v0, :cond_0

    .line 2173723
    const/4 v0, 0x0

    .line 2173724
    :goto_0
    monitor-exit p0

    return v0

    .line 2173725
    :cond_0
    :try_start_1
    iget-object v1, v0, LX/Ert;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, LX/Eru;->a(Ljava/lang/String;)Z

    .line 2173726
    iget-object v0, v0, LX/Ert;->d:Ljava/lang/Integer;

    .line 2173727
    iget-object v1, p0, LX/Eru;->j:[I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, LX/3CW;->a(I)I

    move-result v2

    aget p1, v1, v2

    add-int/lit8 p1, p1, 0x1

    aput p1, v1, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173728
    const/4 v0, 0x1

    goto :goto_0

    .line 2173729
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2173730
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Eru;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173731
    iget-object v0, p0, LX/Eru;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ers;

    iget-object v1, p0, LX/Eru;->k:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p0}, LX/Eru;->k(LX/Eru;)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 2173732
    iput-wide v2, v0, LX/Ers;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2173733
    :cond_0
    monitor-exit p0

    return-void

    .line 2173734
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2173735
    iget-object v0, p0, LX/Eru;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Eru;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Eru;->e:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Eru;->f:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized e()Z
    .locals 2

    .prologue
    .line 2173736
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Eru;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ers;

    .line 2173737
    iget-object v0, v0, LX/Ers;->c:Ljava/lang/Integer;

    .line 2173738
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    .line 2173739
    :cond_1
    const/4 v0, 0x1

    .line 2173740
    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2173741
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()LX/Erj;
    .locals 1

    .prologue
    .line 2173742
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Eru;->c:Ljava/util/ArrayDeque;

    invoke-static {v0}, LX/Eru;->a(Ljava/util/ArrayDeque;)LX/Erj;

    move-result-object v0

    .line 2173743
    if-nez v0, :cond_0

    .line 2173744
    iget-object v0, p0, LX/Eru;->d:Ljava/util/ArrayDeque;

    invoke-static {v0}, LX/Eru;->a(Ljava/util/ArrayDeque;)LX/Erj;

    move-result-object v0

    .line 2173745
    :cond_0
    if-nez v0, :cond_1

    .line 2173746
    iget-object v0, p0, LX/Eru;->e:Ljava/util/ArrayDeque;

    invoke-static {v0}, LX/Eru;->a(Ljava/util/ArrayDeque;)LX/Erj;

    move-result-object v0

    .line 2173747
    :cond_1
    if-nez v0, :cond_2

    .line 2173748
    iget-object v0, p0, LX/Eru;->f:Ljava/util/ArrayDeque;

    invoke-static {v0}, LX/Eru;->a(Ljava/util/ArrayDeque;)LX/Erj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2173749
    :cond_2
    monitor-exit p0

    return-object v0

    .line 2173750
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2173751
    monitor-enter p0

    .line 2173752
    :try_start_0
    iget-object v0, p0, LX/Eru;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ers;

    .line 2173753
    iget-object v3, v0, LX/Ers;->c:Ljava/lang/Integer;

    if-nez v3, :cond_1

    iget-wide v4, v0, LX/Ers;->b:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    iget-object v3, p0, LX/Eru;->k:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    iget-wide v6, v0, LX/Ers;->b:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 2173754
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2173755
    iput-object v3, v0, LX/Ers;->c:Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2173756
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 2173757
    goto :goto_0

    .line 2173758
    :cond_0
    monitor-exit p0

    return v1

    .line 2173759
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
