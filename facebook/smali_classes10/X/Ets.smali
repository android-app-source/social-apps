.class public LX/Ets;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Etq;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ett;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2178766
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ets;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ett;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2178763
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2178764
    iput-object p1, p0, LX/Ets;->b:LX/0Ot;

    .line 2178765
    return-void
.end method

.method public static a(LX/0QB;)LX/Ets;
    .locals 4

    .prologue
    .line 2178752
    const-class v1, LX/Ets;

    monitor-enter v1

    .line 2178753
    :try_start_0
    sget-object v0, LX/Ets;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2178754
    sput-object v2, LX/Ets;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2178755
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2178756
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2178757
    new-instance v3, LX/Ets;

    const/16 p0, 0x2227

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ets;-><init>(LX/0Ot;)V

    .line 2178758
    move-object v0, v3

    .line 2178759
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2178760
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ets;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2178761
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2178762
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 9

    .prologue
    .line 2178767
    check-cast p1, LX/Etr;

    .line 2178768
    iget-object v0, p0, LX/Ets;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ett;

    iget-object v1, p1, LX/Etr;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2178769
    iget-object v2, v0, LX/Ett;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2iT;

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v6

    sget-object v7, LX/2h7;->FRIENDS_CENTER_FRIENDS:LX/2h7;

    invoke-virtual {v1}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, LX/2iT;->a(JLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2178770
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2178733
    const v0, -0x28c63b4b

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2178747
    iget-object v0, p0, LX/Ets;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ett;

    .line 2178748
    iget-object v1, v0, LX/Ett;->a:LX/Eu4;

    invoke-virtual {v1, p1}, LX/Eu4;->c(LX/1De;)LX/Eu2;

    move-result-object v1

    const p0, 0x7f080017

    invoke-virtual {v1, p0}, LX/Eu2;->h(I)LX/Eu2;

    move-result-object v1

    const p0, 0x7f080f9e

    invoke-virtual {v1, p0}, LX/Eu2;->j(I)LX/Eu2;

    move-result-object v1

    const p0, 0x7f020b7a

    invoke-virtual {v1, p0}, LX/Eu2;->k(I)LX/Eu2;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const p0, 0x7f0b08a7

    invoke-interface {v1, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    .line 2178749
    const p0, -0x28c63b4b

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2178750
    invoke-interface {v1, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    const/4 p0, 0x2

    const p2, 0x7f0b08a6

    invoke-interface {v1, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2178751
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2178734
    invoke-static {}, LX/1dS;->b()V

    .line 2178735
    iget v0, p1, LX/1dQ;->b:I

    .line 2178736
    packed-switch v0, :pswitch_data_0

    .line 2178737
    :goto_0
    return-object v1

    .line 2178738
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/Ets;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x28c63b4b
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/Etq;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2178739
    new-instance v1, LX/Etr;

    invoke-direct {v1, p0}, LX/Etr;-><init>(LX/Ets;)V

    .line 2178740
    sget-object v2, LX/Ets;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Etq;

    .line 2178741
    if-nez v2, :cond_0

    .line 2178742
    new-instance v2, LX/Etq;

    invoke-direct {v2}, LX/Etq;-><init>()V

    .line 2178743
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Etq;->a$redex0(LX/Etq;LX/1De;IILX/Etr;)V

    .line 2178744
    move-object v1, v2

    .line 2178745
    move-object v0, v1

    .line 2178746
    return-object v0
.end method
