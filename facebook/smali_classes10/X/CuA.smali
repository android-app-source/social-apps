.class public LX/CuA;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "LX/Cqa;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:LX/0wT;


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:LX/0wd;

.field public e:Landroid/os/Bundle;

.field private f:I

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentLocationAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Landroid/view/View;

.field public i:Z

.field public j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

.field public k:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Lcom/facebook/maps/FbMapViewDelegate;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/Cqw;

.field public m:F

.field public n:LX/Cqa;

.field private final o:LX/Ci3;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1946190
    const-wide v0, 0x4071800000000000L    # 280.0

    const-wide/high16 v2, 0x4041000000000000L    # 34.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/CuA;->c:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/Ctg;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1946177
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1946178
    iput v4, p0, LX/CuA;->f:I

    .line 1946179
    iput-boolean v4, p0, LX/CuA;->i:Z

    .line 1946180
    const/4 v0, 0x0

    iput v0, p0, LX/CuA;->m:F

    .line 1946181
    new-instance v0, LX/Cu4;

    invoke-direct {v0, p0}, LX/Cu4;-><init>(LX/CuA;)V

    iput-object v0, p0, LX/CuA;->o:LX/Ci3;

    .line 1946182
    const-class v0, LX/CuA;

    invoke-static {v0, p0}, LX/CuA;->a(Ljava/lang/Class;LX/02k;)V

    .line 1946183
    iget-object v0, p0, LX/CuA;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/CuA;->c:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide v2, 0x4093880000000000L    # 1250.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    .line 1946184
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 1946185
    move-object v0, v0

    .line 1946186
    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/CuA;->d:LX/0wd;

    .line 1946187
    iget-object v0, p0, LX/CuA;->d:LX/0wd;

    new-instance v1, LX/Cu5;

    invoke-direct {v1, p0}, LX/Cu5;-><init>(LX/CuA;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1946188
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/CuA;->h:Landroid/view/View;

    .line 1946189
    return-void
.end method

.method public static a(Ljava/util/List;)LX/697;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentLocationAnnotation;",
            ">;)",
            "LX/697;"
        }
    .end annotation

    .prologue
    .line 1946172
    new-instance v1, LX/696;

    invoke-direct {v1}, LX/696;-><init>()V

    .line 1946173
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 1946174
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v0

    .line 1946175
    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    invoke-interface {v0}, LX/1k1;->a()D

    move-result-wide v4

    invoke-interface {v0}, LX/1k1;->b()D

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v3}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    goto :goto_0

    .line 1946176
    :cond_0
    invoke-virtual {v1}, LX/696;->a()LX/697;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CuA;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v1

    check-cast v1, LX/Chv;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object p0

    check-cast p0, LX/0wW;

    iput-object v1, p1, LX/CuA;->a:LX/Chv;

    iput-object p0, p1, LX/CuA;->b:LX/0wW;

    return-void
.end method

.method public static k(LX/CuA;)V
    .locals 2

    .prologue
    .line 1946164
    invoke-static {p0}, LX/CuA;->u(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1946165
    invoke-static {p0}, LX/CuA;->t(LX/CuA;)LX/6Zn;

    move-result-object v0

    .line 1946166
    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1946167
    invoke-virtual {v0}, LX/6Zn;->c()V

    .line 1946168
    invoke-virtual {v0}, LX/6Zn;->a()V

    .line 1946169
    iget-object v0, p0, LX/CuA;->k:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->clear()V

    .line 1946170
    const/4 v0, 0x0

    iput-object v0, p0, LX/CuA;->k:Ljava/lang/ref/SoftReference;

    .line 1946171
    :cond_0
    return-void
.end method

.method public static p(LX/CuA;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 1946140
    invoke-static {p0}, LX/CuA;->s(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1946141
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1946142
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v0, v4}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->setVisibility(I)V

    .line 1946143
    :cond_0
    :goto_0
    return-void

    .line 1946144
    :cond_1
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946145
    invoke-interface {v0}, LX/Ctg;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v1

    .line 1946146
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1946147
    const v2, 0x7f03120b

    invoke-virtual {v0, v2, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    iput-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    .line 1946148
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    iget-object v2, p0, LX/CuA;->h:Landroid/view/View;

    .line 1946149
    iput-object v2, v0, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->d:Landroid/view/View;

    .line 1946150
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v0, v4}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->setVisibility(I)V

    .line 1946151
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    new-instance v2, LX/Cu6;

    invoke-direct {v2, p0}, LX/Cu6;-><init>(LX/CuA;)V

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1946152
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v1, v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 1946153
    new-instance v1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v0, "rich_document_map_block"

    invoke-direct {v1, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    .line 1946154
    iget-object v0, p0, LX/CuA;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 1946155
    iget-object v0, p0, LX/CuA;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v0

    invoke-interface {v0}, LX/1k1;->a()D

    move-result-wide v2

    iget-object v0, p0, LX/CuA;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v0

    invoke-interface {v0}, LX/1k1;->b()D

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1946156
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1946157
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1946158
    iget-object v0, p0, LX/CuA;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 1946159
    new-instance v4, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v5

    invoke-interface {v5}, LX/1k1;->a()D

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v0

    invoke-interface {v0}, LX/1k1;->b()D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1946160
    :cond_2
    iget-object v0, p0, LX/CuA;->g:Ljava/util/List;

    invoke-static {v0}, LX/CuA;->a(Ljava/util/List;)LX/697;

    move-result-object v0

    .line 1946161
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, v0, LX/697;->c:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v3, Lcom/facebook/android/maps/model/LatLng;->b:D

    double-to-float v3, v4

    iget-object v4, v0, LX/697;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v4, Lcom/facebook/android/maps/model/LatLng;->a:D

    double-to-float v4, v4

    iget-object v5, v0, LX/697;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v6, v5, Lcom/facebook/android/maps/model/LatLng;->b:D

    double-to-float v5, v6

    iget-object v0, v0, LX/697;->c:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v6, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    double-to-float v0, v6

    invoke-direct {v2, v3, v4, v5, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v1, v2}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/graphics/RectF;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    goto :goto_1

    .line 1946162
    :cond_3
    const-string v0, "red"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Ljava/util/List;Ljava/lang/String;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1946163
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v0, v1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    goto/16 :goto_0
.end method

.method public static s(LX/CuA;)Z
    .locals 1

    .prologue
    .line 1946139
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(LX/CuA;)LX/6Zn;
    .locals 1

    .prologue
    .line 1946136
    iget-object v0, p0, LX/CuA;->k:Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CuA;->k:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1946137
    iget-object v0, p0, LX/CuA;->k:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Zn;

    .line 1946138
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u(LX/CuA;)Z
    .locals 1

    .prologue
    .line 1946135
    invoke-static {p0}, LX/CuA;->t(LX/CuA;)LX/6Zn;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v(LX/CuA;)Z
    .locals 1

    .prologue
    .line 1946191
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentLocationAnnotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1946132
    iput p1, p0, LX/CuA;->f:I

    .line 1946133
    iput-object p2, p0, LX/CuA;->g:Ljava/util/List;

    .line 1946134
    return-void
.end method

.method public final a(LX/Cqw;)V
    .locals 4

    .prologue
    .line 1946115
    invoke-static {p0}, LX/CuA;->v(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1946116
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    if-ne p1, v0, :cond_1

    .line 1946117
    sget-object v0, LX/Cqa;->HIDDEN:LX/Cqa;

    .line 1946118
    iget-object v1, p0, LX/Cts;->a:LX/Ctg;

    move-object v1, v1

    .line 1946119
    invoke-interface {v1}, LX/Cq9;->getAnnotationViews()LX/Cs7;

    move-result-object v1

    invoke-virtual {v1}, LX/Cs7;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CnQ;

    .line 1946120
    instance-of v3, v1, Lcom/facebook/richdocument/view/widget/LocationAnnotationView;

    if-eqz v3, :cond_0

    .line 1946121
    check-cast v1, Lcom/facebook/richdocument/view/widget/LocationAnnotationView;

    .line 1946122
    :goto_0
    move-object v2, v1

    .line 1946123
    if-eqz v2, :cond_1

    .line 1946124
    sget-object v1, LX/Cqa;->VISIBLE:LX/Cqa;

    if-ne v0, v1, :cond_2

    .line 1946125
    const v1, 0x7f081c58

    invoke-virtual {v2, v1}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setText(I)V

    .line 1946126
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f02166d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v3, 0x7f0d015d

    const p1, 0x7f0d015e

    invoke-virtual {v2, v1, v3, p1}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a(Landroid/graphics/drawable/Drawable;II)V

    .line 1946127
    :cond_1
    :goto_1
    return-void

    .line 1946128
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getAnnotation()LX/ClU;

    move-result-object v1

    check-cast v1, LX/ClY;

    .line 1946129
    iget-object v3, v1, LX/ClU;->b:Ljava/lang/String;

    move-object v1, v3

    .line 1946130
    invoke-virtual {v2, v1}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setText(Ljava/lang/String;)V

    .line 1946131
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f02166e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v3, 0x7f0d015f

    const p1, 0x7f0d0160

    invoke-virtual {v2, v1, v3, p1}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a(Landroid/graphics/drawable/Drawable;II)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(LX/CrS;)V
    .locals 6

    .prologue
    .line 1946094
    invoke-static {p0}, LX/CuA;->s(LX/CuA;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, LX/CuA;->u(LX/CuA;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1946095
    :cond_0
    return-void

    .line 1946096
    :cond_1
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v1, v0

    .line 1946097
    iget-object v0, p0, LX/CuA;->h:Landroid/view/View;

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 1946098
    iget-object v2, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v2, v2

    .line 1946099
    invoke-static {p0}, LX/CuA;->s(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1946100
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1946101
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1946102
    iget-object v3, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v3, v0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1946103
    iget-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-interface {v1, v0, v2}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1946104
    :cond_2
    invoke-static {p0}, LX/CuA;->u(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/CuA;->i:Z

    if-eqz v0, :cond_3

    .line 1946105
    invoke-static {p0}, LX/CuA;->t(LX/CuA;)LX/6Zn;

    move-result-object v3

    .line 1946106
    if-eqz v3, :cond_3

    invoke-virtual {v3}, LX/6Zn;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v3}, LX/6Zn;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1946107
    invoke-virtual {v3}, LX/6Zn;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1946108
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v4

    iput v4, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1946109
    invoke-virtual {v3, v0}, LX/6Zn;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1946110
    invoke-interface {v1, v3, v2}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1946111
    :cond_3
    invoke-interface {v1}, LX/Cq9;->getAnnotationViews()LX/Cs7;

    move-result-object v0

    invoke-virtual {v0}, LX/Cs7;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1946112
    invoke-interface {v0}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v4

    invoke-interface {v1, v4}, LX/Ctg;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    .line 1946113
    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    invoke-virtual {v2, v5, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    .line 1946114
    invoke-interface {v0, v4}, LX/CnQ;->setIsOverlay(Z)V

    goto :goto_0
.end method

.method public final a(LX/Crd;)Z
    .locals 1

    .prologue
    .line 1946091
    sget-object v0, LX/Crd;->CLICK_MAP:LX/Crd;

    if-ne p1, v0, :cond_0

    .line 1946092
    const/4 v0, 0x1

    .line 1946093
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/CrS;)V
    .locals 14

    .prologue
    .line 1946046
    invoke-interface {p1}, LX/CrS;->a()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    .line 1946047
    iget-object v1, v0, LX/Cqw;->e:LX/Cqu;

    sget-object p1, LX/Cqu;->COLLAPSED:LX/Cqu;

    if-ne v1, p1, :cond_3

    .line 1946048
    iget v1, v0, LX/Cqw;->g:F

    const/high16 p1, 0x3f800000    # 1.0f

    cmpg-float v1, v1, p1

    if-gez v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1946049
    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 1946050
    if-eqz v0, :cond_2

    invoke-static {p0}, LX/CuA;->v(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1946051
    iget v0, p0, LX/CuA;->m:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    sget-object v0, LX/Cqa;->VISIBLE:LX/Cqa;

    :goto_2
    move-object v0, v0

    .line 1946052
    sget-object v1, LX/Cqa;->VISIBLE:LX/Cqa;

    if-ne v0, v1, :cond_2

    .line 1946053
    iget-object v0, p0, LX/CuA;->n:LX/Cqa;

    move-object v0, v0

    .line 1946054
    sget-object v1, LX/Cqa;->HIDDEN:LX/Cqa;

    if-eq v0, v1, :cond_2

    .line 1946055
    sget-object v0, LX/Cqa;->HIDDEN:LX/Cqa;

    .line 1946056
    sget-object v2, LX/Cqa;->VISIBLE:LX/Cqa;

    if-ne v0, v2, :cond_1

    .line 1946057
    invoke-virtual {p0}, LX/Cts;->j()LX/Cqw;

    move-result-object v2

    .line 1946058
    iget-object v3, v2, LX/Cqw;->e:LX/Cqu;

    move-object v2, v3

    .line 1946059
    sget-object v3, LX/Cqu;->EXPANDED:LX/Cqu;

    if-ne v2, v3, :cond_6

    .line 1946060
    const/4 v9, 0x0

    .line 1946061
    iget-boolean v6, p0, LX/CuA;->i:Z

    if-nez v6, :cond_7

    .line 1946062
    :cond_0
    :goto_3
    iget-object v2, p0, LX/Cts;->a:LX/Ctg;

    move-object v2, v2

    .line 1946063
    invoke-interface {v2}, LX/Ctg;->getCurrentLayout()LX/CrS;

    move-result-object v2

    invoke-interface {v2}, LX/CrS;->a()LX/Cqv;

    move-result-object v2

    check-cast v2, LX/Cqw;

    .line 1946064
    invoke-virtual {v2}, LX/Cqw;->e()LX/Cqw;

    move-result-object v2

    iput-object v2, p0, LX/CuA;->l:LX/Cqw;

    .line 1946065
    :cond_1
    iput-object v0, p0, LX/CuA;->n:LX/Cqa;

    .line 1946066
    iget-object v2, p0, LX/CuA;->d:LX/0wd;

    iget v3, p0, LX/CuA;->m:F

    float-to-double v4, v3

    invoke-virtual {v2, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 1946067
    iget-object v2, p0, LX/CuA;->d:LX/0wd;

    iget-object v3, p0, LX/CuA;->n:LX/Cqa;

    invoke-virtual {v3}, LX/Cqa;->getVisibility()I

    move-result v3

    int-to-double v4, v3

    invoke-virtual {v2, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 1946068
    invoke-virtual {p0, v0}, LX/Cts;->a(Ljava/lang/Object;)V

    .line 1946069
    :cond_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    sget-object v0, LX/Cqa;->HIDDEN:LX/Cqa;

    goto :goto_2

    .line 1946070
    :cond_6
    invoke-static {p0}, LX/CuA;->p(LX/CuA;)V

    goto :goto_3

    .line 1946071
    :cond_7
    invoke-static {p0}, LX/CuA;->u(LX/CuA;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1946072
    invoke-static {p0}, LX/CuA;->t(LX/CuA;)LX/6Zn;

    move-result-object v6

    .line 1946073
    invoke-virtual {v6}, LX/6Zn;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_0

    .line 1946074
    invoke-virtual {v6, v9}, LX/6Zn;->setVisibility(I)V

    goto :goto_3

    .line 1946075
    :cond_8
    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v8

    .line 1946076
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 1946077
    const v7, 0x7f031207

    invoke-virtual {v6, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/maps/FbMapViewDelegate;

    .line 1946078
    invoke-virtual {v6, v9}, Lcom/facebook/maps/FbMapViewDelegate;->setVisibility(I)V

    .line 1946079
    iget-object v7, p0, LX/CuA;->e:Landroid/os/Bundle;

    invoke-virtual {v6, v7}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 1946080
    invoke-static {p0}, LX/CuA;->s(LX/CuA;)Z

    move-result v7

    if-eqz v7, :cond_9

    iget-object v7, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v8, v7}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v7

    .line 1946081
    :goto_4
    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v8, v6, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 1946082
    iget-object v7, p0, LX/CuA;->e:Landroid/os/Bundle;

    invoke-virtual {v7}, Landroid/os/Bundle;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1946083
    iget-object v7, p0, LX/CuA;->g:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_a

    .line 1946084
    new-instance v8, Lcom/facebook/android/maps/model/LatLng;

    iget-object v7, p0, LX/CuA;->g:Ljava/util/List;

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    invoke-virtual {v7}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v7

    invoke-interface {v7}, LX/1k1;->a()D

    move-result-wide v10

    iget-object v7, p0, LX/CuA;->g:Ljava/util/List;

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    invoke-virtual {v7}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;->a()LX/1k1;

    move-result-object v7

    invoke-interface {v7}, LX/1k1;->b()D

    move-result-wide v12

    invoke-direct {v8, v10, v11, v12, v13}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    const/high16 v7, 0x41000000    # 8.0f

    invoke-static {v8, v7}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v7

    .line 1946085
    :goto_5
    new-instance v8, Ljava/lang/ref/SoftReference;

    invoke-direct {v8, v6}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v8, p0, LX/CuA;->k:Ljava/lang/ref/SoftReference;

    .line 1946086
    new-instance v8, LX/Cu8;

    invoke-direct {v8, p0, v6, v7}, LX/Cu8;-><init>(LX/CuA;Lcom/facebook/maps/FbMapViewDelegate;LX/6aM;)V

    invoke-virtual {v6, v8}, LX/6Zn;->a(LX/6Zz;)V

    goto/16 :goto_3

    .line 1946087
    :cond_9
    const/4 v7, -0x1

    goto :goto_4

    .line 1946088
    :cond_a
    iget-object v7, p0, LX/CuA;->g:Ljava/util/List;

    invoke-static {v7}, LX/CuA;->a(Ljava/util/List;)LX/697;

    move-result-object v7

    .line 1946089
    invoke-static {v7, v9}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v7

    goto :goto_5

    .line 1946090
    :cond_b
    const/4 v7, 0x0

    goto :goto_5
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1946045
    iget-object v0, p0, LX/CuA;->g:Ljava/util/List;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1946035
    invoke-static {p0}, LX/CuA;->s(LX/CuA;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1946036
    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v2, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1946037
    const/4 v0, 0x0

    iput-object v0, p0, LX/CuA;->j:Lcom/facebook/richdocument/view/widget/MediaStaticMap;

    .line 1946038
    :cond_0
    invoke-static {p0}, LX/CuA;->k(LX/CuA;)V

    .line 1946039
    const/4 v0, 0x1

    iput v0, p0, LX/CuA;->f:I

    .line 1946040
    iput-object v1, p0, LX/CuA;->g:Ljava/util/List;

    .line 1946041
    iput-object v1, p0, LX/CuA;->l:LX/Cqw;

    .line 1946042
    const/4 v0, 0x0

    iput v0, p0, LX/CuA;->m:F

    .line 1946043
    sget-object v0, LX/Cqa;->HIDDEN:LX/Cqa;

    iput-object v0, p0, LX/CuA;->n:LX/Cqa;

    .line 1946044
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 1946015
    iget-object v0, p0, LX/CuA;->a:LX/Chv;

    iget-object v1, p0, LX/CuA;->o:LX/Ci3;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1946016
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 1946017
    invoke-interface {v0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    .line 1946018
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CqX;->a(LX/CrS;)V

    .line 1946019
    sget-object v1, LX/Cqw;->a:LX/Cqw;

    invoke-virtual {v0, v1}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v1

    .line 1946020
    if-eqz v1, :cond_0

    .line 1946021
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v2

    invoke-static {v1, v2}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v2

    .line 1946022
    new-instance v3, LX/CrR;

    invoke-direct {v3}, LX/CrR;-><init>()V

    .line 1946023
    invoke-virtual {v2}, LX/CrW;->c()LX/CqY;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/CrR;->a(LX/CqY;)V

    .line 1946024
    iget-object v2, p0, LX/CuA;->h:Landroid/view/View;

    invoke-interface {v1, v2, v3}, LX/CrS;->a(Landroid/view/View;LX/CrR;)V

    .line 1946025
    :cond_0
    sget-object v1, LX/Cqw;->b:LX/Cqw;

    invoke-virtual {v0, v1}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v1

    .line 1946026
    if-eqz v1, :cond_1

    .line 1946027
    new-instance v2, LX/CrR;

    invoke-direct {v2}, LX/CrR;-><init>()V

    .line 1946028
    new-instance v3, LX/CrW;

    .line 1946029
    iget-object v4, v0, LX/Cqi;->b:Landroid/graphics/Rect;

    move-object v0, v4

    .line 1946030
    invoke-direct {v3, v0}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v2, v3}, LX/CrR;->a(LX/CqY;)V

    .line 1946031
    iget-object v0, p0, LX/CuA;->h:Landroid/view/View;

    invoke-interface {v1, v0, v2}, LX/CrS;->a(Landroid/view/View;LX/CrR;)V

    .line 1946032
    :cond_1
    invoke-static {p0}, LX/CuA;->v(LX/CuA;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1946033
    invoke-static {p0}, LX/CuA;->p(LX/CuA;)V

    .line 1946034
    :cond_2
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1946013
    iget-object v0, p0, LX/CuA;->a:LX/Chv;

    iget-object v1, p0, LX/CuA;->o:LX/Ci3;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1946014
    return-void
.end method
