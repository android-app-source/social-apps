.class public LX/ElQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Ekx;

.field public final b:LX/0Zh;

.field public final c:LX/ElF;

.field public final d:LX/Elf;

.field public final e:Z


# direct methods
.method public constructor <init>(LX/ElP;)V
    .locals 6

    .prologue
    .line 2164331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2164332
    iget-object v1, p1, LX/ElP;->a:LX/Eku;

    .line 2164333
    iget-object v0, p1, LX/ElP;->b:LX/ElZ;

    if-eqz v0, :cond_0

    .line 2164334
    new-instance v0, LX/ElV;

    iget-object v2, p1, LX/ElP;->b:LX/ElZ;

    invoke-direct {v0, v2}, LX/ElV;-><init>(LX/ElZ;)V

    .line 2164335
    iget-object v2, v1, LX/Eku;->b:LX/Ekn;

    invoke-virtual {v2, v0}, LX/Ekn;->a(LX/Ekv;)LX/Ekn;

    .line 2164336
    :cond_0
    iget-object v0, p1, LX/ElP;->c:LX/ElF;

    .line 2164337
    if-eqz v0, :cond_1

    :goto_0
    move-object v0, v0

    .line 2164338
    iput-object v0, p0, LX/ElQ;->c:LX/ElF;

    .line 2164339
    iget-object v0, p1, LX/ElP;->d:LX/Elf;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Elf;

    iput-object v0, p0, LX/ElQ;->d:LX/Elf;

    .line 2164340
    new-instance v0, LX/ElH;

    invoke-direct {v0}, LX/ElH;-><init>()V

    .line 2164341
    iget-object v2, v1, LX/Eku;->d:LX/Eko;

    move-object v2, v2

    .line 2164342
    if-nez v0, :cond_2

    .line 2164343
    :goto_1
    move-object v0, v2

    .line 2164344
    iput-object v0, v1, LX/Eku;->d:LX/Eko;

    .line 2164345
    iget-object v0, p1, LX/ElP;->a:LX/Eku;

    invoke-virtual {v0}, LX/Eku;->b()LX/Ekx;

    move-result-object v0

    iput-object v0, p0, LX/ElQ;->a:LX/Ekx;

    .line 2164346
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    iput-object v0, p0, LX/ElQ;->b:LX/0Zh;

    .line 2164347
    iget-boolean v0, p1, LX/ElP;->e:Z

    iput-boolean v0, p0, LX/ElQ;->e:Z

    .line 2164348
    return-void

    :cond_1
    new-instance v0, LX/ElG;

    invoke-direct {v0}, LX/ElG;-><init>()V

    goto :goto_0

    .line 2164349
    :cond_2
    if-nez v2, :cond_3

    move-object v2, v0

    .line 2164350
    goto :goto_1

    .line 2164351
    :cond_3
    new-instance v3, LX/Ekp;

    const/4 v4, 0x2

    new-array v4, v4, [LX/Eko;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-direct {v3, v4}, LX/Ekp;-><init>([LX/Eko;)V

    move-object v2, v3

    goto :goto_1
.end method


# virtual methods
.method public final a(I)LX/ElO;
    .locals 9

    .prologue
    .line 2164352
    const-string v0, "get"

    .line 2164353
    iget-object v1, p0, LX/ElQ;->d:LX/Elf;

    invoke-virtual {v1, p1}, LX/Elf;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 2164354
    new-instance v2, LX/ElN;

    iget-object v3, p0, LX/ElQ;->b:LX/0Zh;

    iget-object v4, p0, LX/ElQ;->a:LX/Ekx;

    .line 2164355
    new-instance v6, LX/El4;

    invoke-direct {v6}, LX/El4;-><init>()V

    move-object v5, v6

    .line 2164356
    invoke-interface {v5, v1}, LX/Ekz;->a(Ljava/lang/String;)LX/Ekz;

    .line 2164357
    new-instance v6, LX/Ekt;

    iget-object v7, v4, LX/Ekx;->a:LX/Ekr;

    new-instance v8, LX/Eki;

    invoke-direct {v8, v5}, LX/Eki;-><init>(LX/Ekz;)V

    .line 2164358
    iput-object v1, v8, LX/Eki;->b:Ljava/lang/String;

    .line 2164359
    move-object v5, v8

    .line 2164360
    iget-object v8, v4, LX/Ekx;->e:LX/Eko;

    .line 2164361
    iput-object v8, v5, LX/Eki;->c:LX/Eko;

    .line 2164362
    move-object v5, v5

    .line 2164363
    iget-object v8, v4, LX/Ekx;->c:LX/Ekl;

    .line 2164364
    iput-object v8, v5, LX/Eki;->d:LX/Ekl;

    .line 2164365
    move-object v5, v5

    .line 2164366
    invoke-direct {v6, v7, v4, v5}, LX/Ekt;-><init>(LX/Ekr;LX/Ekx;LX/Eki;)V

    move-object v4, v6

    .line 2164367
    iget-object v5, p0, LX/ElQ;->c:LX/ElF;

    invoke-direct {v2, v3, v4, v5}, LX/ElN;-><init>(LX/0Zh;LX/Ekt;LX/ElF;)V

    .line 2164368
    iget-boolean v3, p0, LX/ElQ;->e:Z

    if-eqz v3, :cond_0

    .line 2164369
    const/4 v3, 0x1

    .line 2164370
    iput-boolean v3, v2, LX/ElN;->h:Z

    .line 2164371
    :cond_0
    move-object v1, v2

    .line 2164372
    iget-object v2, p0, LX/ElQ;->d:LX/Elf;

    invoke-virtual {v2, p1}, LX/Elf;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 2164373
    iput-object v0, v1, LX/ElN;->d:Ljava/lang/String;

    .line 2164374
    new-instance v3, LX/ElO;

    iget-object v4, p0, LX/ElQ;->b:LX/0Zh;

    invoke-direct {v3, v4, p1, v2, v1}, LX/ElO;-><init>(LX/0Zh;ILjava/lang/String;LX/ElN;)V

    move-object v0, v3

    .line 2164375
    return-object v0
.end method
