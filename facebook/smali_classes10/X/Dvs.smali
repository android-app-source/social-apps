.class public LX/Dvs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Dvs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2059400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059401
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPhoto;)D
    .locals 4

    .prologue
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 2059396
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2059397
    :cond_0
    :goto_0
    return-wide v0

    .line 2059398
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-static {v2}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-static {v3}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    add-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x2

    int-to-double v2, v2

    .line 2059399
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;)D
    .locals 4

    .prologue
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 2059380
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->E()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->E()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$TopLevelCommentsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->E()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2059381
    :cond_0
    :goto_0
    return-wide v0

    .line 2059382
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->E()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$TopLevelCommentsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$TopLevelCommentsModel;->a()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->E()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel$LikersModel;->a()I

    move-result v3

    add-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x2

    int-to-double v2, v2

    .line 2059383
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Dvs;
    .locals 3

    .prologue
    .line 2059384
    sget-object v0, LX/Dvs;->a:LX/Dvs;

    if-nez v0, :cond_1

    .line 2059385
    const-class v1, LX/Dvs;

    monitor-enter v1

    .line 2059386
    :try_start_0
    sget-object v0, LX/Dvs;->a:LX/Dvs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2059387
    if-eqz v2, :cond_0

    .line 2059388
    :try_start_1
    new-instance v0, LX/Dvs;

    invoke-direct {v0}, LX/Dvs;-><init>()V

    .line 2059389
    move-object v0, v0

    .line 2059390
    sput-object v0, LX/Dvs;->a:LX/Dvs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2059391
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2059392
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2059393
    :cond_1
    sget-object v0, LX/Dvs;->a:LX/Dvs;

    return-object v0

    .line 2059394
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2059395
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
