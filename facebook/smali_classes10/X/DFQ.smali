.class public LX/DFQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFR;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DFQ",
            "<TE;>.java/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DFR;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978132
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1978133
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DFQ;->b:LX/0Zi;

    .line 1978134
    iput-object p1, p0, LX/DFQ;->a:LX/0Ot;

    .line 1978135
    return-void
.end method

.method public static a(LX/0QB;)LX/DFQ;
    .locals 4

    .prologue
    .line 1978136
    const-class v1, LX/DFQ;

    monitor-enter v1

    .line 1978137
    :try_start_0
    sget-object v0, LX/DFQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978138
    sput-object v2, LX/DFQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978139
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978140
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978141
    new-instance v3, LX/DFQ;

    const/16 p0, 0x20e3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DFQ;-><init>(LX/0Ot;)V

    .line 1978142
    move-object v0, v3

    .line 1978143
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978144
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978145
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978146
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1978147
    const v0, 0x3391489d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1978148
    check-cast p2, LX/DFP;

    .line 1978149
    iget-object v0, p0, LX/DFQ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/DFP;->a:LX/2ep;

    .line 1978150
    iget-object v1, v0, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v1, p0, :cond_0

    .line 1978151
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    .line 1978152
    :goto_0
    move-object v0, v1

    .line 1978153
    return-object v0

    .line 1978154
    :cond_0
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const p0, 0x7f020afc

    invoke-virtual {v1, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/16 p0, 0x8

    const p2, 0x7f0b0a20

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const p0, 0x7f08186a

    invoke-interface {v1, p0}, LX/1Di;->A(I)LX/1Di;

    move-result-object v1

    const p0, 0x7f020afb

    invoke-interface {v1, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v1

    .line 1978155
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 1978156
    const p0, 0x3391489d

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1978157
    invoke-interface {v1, p0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1978158
    invoke-static {}, LX/1dS;->b()V

    .line 1978159
    iget v0, p1, LX/1dQ;->b:I

    .line 1978160
    packed-switch v0, :pswitch_data_0

    .line 1978161
    :goto_0
    return-object v1

    .line 1978162
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 1978163
    check-cast v0, LX/DFP;

    .line 1978164
    iget-object v2, p0, LX/DFQ;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/DFR;

    iget-object v3, v0, LX/DFP;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v0, LX/DFP;->c:LX/1Fa;

    iget-object p2, v0, LX/DFP;->d:LX/1Pq;

    .line 1978165
    iget-object v0, v2, LX/DFR;->a:LX/2ew;

    .line 1978166
    iget-object p0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1978167
    check-cast p0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v0, p0, p1, p2}, LX/2ew;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/1Pq;)V

    .line 1978168
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3391489d
        :pswitch_0
    .end packed-switch
.end method
