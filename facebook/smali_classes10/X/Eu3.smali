.class public final LX/Eu3;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Eu4;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:I

.field public g:I

.field public final synthetic h:LX/Eu4;


# direct methods
.method public constructor <init>(LX/Eu4;)V
    .locals 1

    .prologue
    .line 2178976
    iput-object p1, p0, LX/Eu3;->h:LX/Eu4;

    .line 2178977
    move-object v0, p1

    .line 2178978
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2178979
    sget v0, LX/Eu6;->c:I

    iput v0, p0, LX/Eu3;->e:I

    .line 2178980
    sget v0, LX/Eu6;->a:I

    iput v0, p0, LX/Eu3;->f:I

    .line 2178981
    sget v0, LX/Eu6;->b:I

    iput v0, p0, LX/Eu3;->g:I

    .line 2178982
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2179009
    const-string v0, "FriendListButton"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2178983
    if-ne p0, p1, :cond_1

    .line 2178984
    :cond_0
    :goto_0
    return v0

    .line 2178985
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2178986
    goto :goto_0

    .line 2178987
    :cond_3
    check-cast p1, LX/Eu3;

    .line 2178988
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2178989
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2178990
    if-eq v2, v3, :cond_0

    .line 2178991
    iget-object v2, p0, LX/Eu3;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Eu3;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Eu3;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2178992
    goto :goto_0

    .line 2178993
    :cond_5
    iget-object v2, p1, LX/Eu3;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 2178994
    :cond_6
    iget-object v2, p0, LX/Eu3;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Eu3;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Eu3;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2178995
    goto :goto_0

    .line 2178996
    :cond_8
    iget-object v2, p1, LX/Eu3;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 2178997
    :cond_9
    iget-object v2, p0, LX/Eu3;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Eu3;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Eu3;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2178998
    goto :goto_0

    .line 2178999
    :cond_b
    iget-object v2, p1, LX/Eu3;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_a

    .line 2179000
    :cond_c
    iget-object v2, p0, LX/Eu3;->d:LX/1dc;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Eu3;->d:LX/1dc;

    iget-object v3, p1, LX/Eu3;->d:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2179001
    goto :goto_0

    .line 2179002
    :cond_e
    iget-object v2, p1, LX/Eu3;->d:LX/1dc;

    if-nez v2, :cond_d

    .line 2179003
    :cond_f
    iget v2, p0, LX/Eu3;->e:I

    iget v3, p1, LX/Eu3;->e:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 2179004
    goto :goto_0

    .line 2179005
    :cond_10
    iget v2, p0, LX/Eu3;->f:I

    iget v3, p1, LX/Eu3;->f:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 2179006
    goto :goto_0

    .line 2179007
    :cond_11
    iget v2, p0, LX/Eu3;->g:I

    iget v3, p1, LX/Eu3;->g:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2179008
    goto :goto_0
.end method
