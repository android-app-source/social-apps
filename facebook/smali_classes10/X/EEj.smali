.class public final synthetic LX/EEj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2094159
    invoke-static {}, LX/EEm;->values()[LX/EEm;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/EEj;->b:[I

    :try_start_0
    sget-object v0, LX/EEj;->b:[I

    sget-object v1, LX/EEm;->Participant:LX/EEm;

    invoke-virtual {v1}, LX/EEm;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_0
    :try_start_1
    sget-object v0, LX/EEj;->b:[I

    sget-object v1, LX/EEm;->NotInCallHeader:LX/EEm;

    invoke-virtual {v1}, LX/EEm;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_1
    :try_start_2
    sget-object v0, LX/EEj;->b:[I

    sget-object v1, LX/EEm;->InCallHeader:LX/EEm;

    invoke-virtual {v1}, LX/EEm;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_2
    :try_start_3
    sget-object v0, LX/EEj;->b:[I

    sget-object v1, LX/EEm;->Add_People:LX/EEm;

    invoke-virtual {v1}, LX/EEm;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    .line 2094160
    :goto_3
    invoke-static {}, LX/EGD;->values()[LX/EGD;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/EEj;->a:[I

    :try_start_4
    sget-object v0, LX/EEj;->a:[I

    sget-object v1, LX/EGD;->CONNECTING:LX/EGD;

    invoke-virtual {v1}, LX/EGD;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_4
    :try_start_5
    sget-object v0, LX/EEj;->a:[I

    sget-object v1, LX/EGD;->CONTACTING:LX/EGD;

    invoke-virtual {v1}, LX/EGD;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_5
    :try_start_6
    sget-object v0, LX/EEj;->a:[I

    sget-object v1, LX/EGD;->CONNECTION_DROPPED:LX/EGD;

    invoke-virtual {v1}, LX/EGD;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_6
    :try_start_7
    sget-object v0, LX/EEj;->a:[I

    sget-object v1, LX/EGD;->RINGING:LX/EGD;

    invoke-virtual {v1}, LX/EGD;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    :goto_7
    :try_start_8
    sget-object v0, LX/EEj;->a:[I

    sget-object v1, LX/EGD;->UNREACHABLE:LX/EGD;

    invoke-virtual {v1}, LX/EGD;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_8
    :try_start_9
    sget-object v0, LX/EEj;->a:[I

    sget-object v1, LX/EGD;->NO_ANSWER:LX/EGD;

    invoke-virtual {v1}, LX/EGD;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_9
    :try_start_a
    sget-object v0, LX/EEj;->a:[I

    sget-object v1, LX/EGD;->REJECTED:LX/EGD;

    invoke-virtual {v1}, LX/EGD;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_a
    :try_start_b
    sget-object v0, LX/EEj;->a:[I

    sget-object v1, LX/EGD;->PARTICIPANT_LIMIT_REACHED:LX/EGD;

    invoke-virtual {v1}, LX/EGD;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_b
    :try_start_c
    sget-object v0, LX/EEj;->a:[I

    sget-object v1, LX/EGD;->IN_ANOTHER_CALL:LX/EGD;

    invoke-virtual {v1}, LX/EGD;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_c
    return-void

    :catch_0
    goto :goto_c

    :catch_1
    goto :goto_b

    :catch_2
    goto :goto_a

    :catch_3
    goto :goto_9

    :catch_4
    goto :goto_8

    :catch_5
    goto :goto_7

    :catch_6
    goto :goto_6

    :catch_7
    goto :goto_5

    :catch_8
    goto :goto_4

    :catch_9
    goto :goto_3

    :catch_a
    goto/16 :goto_2

    :catch_b
    goto/16 :goto_1

    :catch_c
    goto/16 :goto_0
.end method
