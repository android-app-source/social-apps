.class public final LX/DON;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Sp;


# instance fields
.field public final synthetic a:LX/DOb;


# direct methods
.method public constructor <init>(LX/DOb;)V
    .locals 0

    .prologue
    .line 1992304
    iput-object p1, p0, LX/DON;->a:LX/DOb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/0wD;
    .locals 1

    .prologue
    .line 1992305
    sget-object v0, LX/0wD;->GROUP:LX/0wD;

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Z
    .locals 2

    .prologue
    .line 1992306
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1992307
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1992308
    sget-object v0, LX/DOb;->c:LX/0Rf;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1992309
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/DOb;->d:LX/0Rf;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
