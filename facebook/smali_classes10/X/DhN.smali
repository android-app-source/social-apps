.class public final enum LX/DhN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DhN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DhN;

.field public static final enum PHOTO:LX/DhN;

.field public static final enum STICKER:LX/DhN;

.field public static final enum TEXT:LX/DhN;

.field public static final enum VIDEO:LX/DhN;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2030912
    new-instance v0, LX/DhN;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v2}, LX/DhN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhN;->PHOTO:LX/DhN;

    .line 2030913
    new-instance v0, LX/DhN;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/DhN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhN;->VIDEO:LX/DhN;

    .line 2030914
    new-instance v0, LX/DhN;

    const-string v1, "STICKER"

    invoke-direct {v0, v1, v4}, LX/DhN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhN;->STICKER:LX/DhN;

    .line 2030915
    new-instance v0, LX/DhN;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v5}, LX/DhN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DhN;->TEXT:LX/DhN;

    .line 2030916
    const/4 v0, 0x4

    new-array v0, v0, [LX/DhN;

    sget-object v1, LX/DhN;->PHOTO:LX/DhN;

    aput-object v1, v0, v2

    sget-object v1, LX/DhN;->VIDEO:LX/DhN;

    aput-object v1, v0, v3

    sget-object v1, LX/DhN;->STICKER:LX/DhN;

    aput-object v1, v0, v4

    sget-object v1, LX/DhN;->TEXT:LX/DhN;

    aput-object v1, v0, v5

    sput-object v0, LX/DhN;->$VALUES:[LX/DhN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2030911
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DhN;
    .locals 1

    .prologue
    .line 2030910
    const-class v0, LX/DhN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DhN;

    return-object v0
.end method

.method public static values()[LX/DhN;
    .locals 1

    .prologue
    .line 2030909
    sget-object v0, LX/DhN;->$VALUES:[LX/DhN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DhN;

    return-object v0
.end method
