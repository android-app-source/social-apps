.class public LX/DvV;
.super LX/Dcc;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/DvV;


# instance fields
.field private final a:LX/DvI;

.field private final b:LX/0tX;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/0sX;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/DvI;LX/0tX;LX/0sX;LX/0Ot;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/DvI;",
            "LX/0tX;",
            "LX/0sX;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2058648
    invoke-direct {p0}, LX/Dcc;-><init>()V

    .line 2058649
    iput-object p1, p0, LX/DvV;->c:Ljava/util/concurrent/ExecutorService;

    .line 2058650
    iput-object p2, p0, LX/DvV;->a:LX/DvI;

    .line 2058651
    iput-object p3, p0, LX/DvV;->b:LX/0tX;

    .line 2058652
    iput-object p4, p0, LX/DvV;->d:LX/0sX;

    .line 2058653
    iput-object p5, p0, LX/DvV;->e:LX/0Ot;

    .line 2058654
    return-void
.end method

.method public static a(LX/0QB;)LX/DvV;
    .locals 9

    .prologue
    .line 2058617
    sget-object v0, LX/DvV;->f:LX/DvV;

    if-nez v0, :cond_1

    .line 2058618
    const-class v1, LX/DvV;

    monitor-enter v1

    .line 2058619
    :try_start_0
    sget-object v0, LX/DvV;->f:LX/DvV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2058620
    if-eqz v2, :cond_0

    .line 2058621
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2058622
    new-instance v3, LX/DvV;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/DvI;->a(LX/0QB;)LX/DvI;

    move-result-object v5

    check-cast v5, LX/DvI;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v7

    check-cast v7, LX/0sX;

    const/16 v8, 0x1032

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/DvV;-><init>(Ljava/util/concurrent/ExecutorService;LX/DvI;LX/0tX;LX/0sX;LX/0Ot;)V

    .line 2058623
    move-object v0, v3

    .line 2058624
    sput-object v0, LX/DvV;->f:LX/DvV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2058625
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2058626
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2058627
    :cond_1
    sget-object v0, LX/DvV;->f:LX/DvV;

    return-object v0

    .line 2058628
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2058629
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;",
            "IZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2058630
    new-instance v0, LX/8IE;

    invoke-direct {v0}, LX/8IE;-><init>()V

    move-object v1, v0

    .line 2058631
    check-cast p3, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    .line 2058632
    const-string v0, "node_id"

    .line 2058633
    iget-object v2, p3, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2058634
    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058635
    const-string v0, "count"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058636
    const-string v0, "automatic_photo_captioning_enabled"

    iget-object v2, p0, LX/DvV;->d:LX/0sX;

    invoke-virtual {v2}, LX/0sX;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058637
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2058638
    const-string v0, "before"

    invoke-virtual {v1, v0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058639
    :cond_0
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2058640
    const-string v0, "after"

    invoke-virtual {v1, v0, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2058641
    :cond_1
    iget-object v0, p0, LX/DvV;->a:LX/DvI;

    invoke-virtual {v0, v1}, LX/DvI;->a(LX/0gW;)LX/0gW;

    .line 2058642
    iget-object v0, p0, LX/DvV;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/5lr;->d:S

    invoke-interface {v0, v2, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 2058643
    iget-object v0, p0, LX/DvV;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v3, LX/5lr;->e:I

    const v4, 0x15180

    invoke-interface {v0, v3, v4}, LX/0ad;->a(II)I

    move-result v3

    .line 2058644
    iget-object v4, p0, LX/DvV;->b:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    if-eqz v2, :cond_2

    sget-object v0, LX/0zS;->a:LX/0zS;

    :goto_0
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    int-to-long v2, v3

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    if-eqz p5, :cond_3

    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    :goto_1
    invoke-virtual {v1, v0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2058645
    new-instance v1, LX/DvU;

    invoke-direct {v1}, LX/DvU;-><init>()V

    iget-object v2, p0, LX/DvV;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2058646
    return-object v0

    .line 2058647
    :cond_2
    sget-object v0, LX/0zS;->c:LX/0zS;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    goto :goto_1
.end method
