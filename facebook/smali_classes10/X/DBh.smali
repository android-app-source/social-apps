.class public final LX/DBh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/13a;


# direct methods
.method public constructor <init>(LX/13a;)V
    .locals 0

    .prologue
    .line 1973136
    iput-object p1, p0, LX/DBh;->a:LX/13a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1973137
    iget-object v0, p0, LX/DBh;->a:LX/13a;

    iget-object v0, v0, LX/13a;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1973138
    iget-object v0, p0, LX/DBh;->a:LX/13a;

    iget-object v0, v0, LX/13a;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    if-le v0, v5, :cond_0

    .line 1973139
    iget-object v0, p0, LX/DBh;->a:LX/13a;

    iget-object v0, v0, LX/13a;->d:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1973140
    iget-object v0, p0, LX/DBh;->a:LX/13a;

    iget-object v0, v0, LX/13a;->d:Landroid/widget/TextView;

    iget-object v1, p0, LX/DBh;->a:LX/13a;

    iget-object v1, v1, LX/13a;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, LX/DBh;->a:LX/13a;

    iget-object v2, v2, LX/13a;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, v4, v1, v4, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1973141
    iget-object v0, p0, LX/DBh;->a:LX/13a;

    iget-object v1, v0, LX/13a;->d:Landroid/widget/TextView;

    iget-object v0, p0, LX/DBh;->a:LX/13a;

    iget-object v0, v0, LX/13a;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v2, 0x7f020bf8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1973142
    iget-object v0, p0, LX/DBh;->a:LX/13a;

    .line 1973143
    iput-boolean v5, v0, LX/13a;->r:Z

    .line 1973144
    :cond_0
    return-void
.end method
