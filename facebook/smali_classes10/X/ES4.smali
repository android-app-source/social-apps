.class public final enum LX/ES4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ES4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ES4;

.field public static final enum FIRST:LX/ES4;

.field public static final enum RETRY_SOFT_FAILURES_ONLY:LX/ES4;

.field public static final enum RETRY_WITH_HARD_FAILURES:LX/ES4;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2122135
    new-instance v0, LX/ES4;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v2}, LX/ES4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ES4;->FIRST:LX/ES4;

    new-instance v0, LX/ES4;

    const-string v1, "RETRY_SOFT_FAILURES_ONLY"

    invoke-direct {v0, v1, v3}, LX/ES4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ES4;->RETRY_SOFT_FAILURES_ONLY:LX/ES4;

    new-instance v0, LX/ES4;

    const-string v1, "RETRY_WITH_HARD_FAILURES"

    invoke-direct {v0, v1, v4}, LX/ES4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ES4;->RETRY_WITH_HARD_FAILURES:LX/ES4;

    .line 2122136
    const/4 v0, 0x3

    new-array v0, v0, [LX/ES4;

    sget-object v1, LX/ES4;->FIRST:LX/ES4;

    aput-object v1, v0, v2

    sget-object v1, LX/ES4;->RETRY_SOFT_FAILURES_ONLY:LX/ES4;

    aput-object v1, v0, v3

    sget-object v1, LX/ES4;->RETRY_WITH_HARD_FAILURES:LX/ES4;

    aput-object v1, v0, v4

    sput-object v0, LX/ES4;->$VALUES:[LX/ES4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2122137
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ES4;
    .locals 1

    .prologue
    .line 2122138
    const-class v0, LX/ES4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ES4;

    return-object v0
.end method

.method public static values()[LX/ES4;
    .locals 1

    .prologue
    .line 2122139
    sget-object v0, LX/ES4;->$VALUES:[LX/ES4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ES4;

    return-object v0
.end method
