.class public final LX/DJW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ARN;


# instance fields
.field public final synthetic c:LX/DJc;


# direct methods
.method public constructor <init>(LX/DJc;)V
    .locals 0

    .prologue
    .line 1985339
    iput-object p1, p0, LX/DJW;->c:LX/DJc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1985340
    iget-object v2, p0, LX/DJW;->c:LX/DJc;

    .line 1985341
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1985342
    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v2

    .line 1985343
    if-nez v2, :cond_1

    .line 1985344
    :cond_0
    :goto_0
    return v0

    .line 1985345
    :cond_1
    iget-object v3, p0, LX/DJW;->c:LX/DJc;

    .line 1985346
    invoke-virtual {v3}, LX/AQ9;->R()LX/B5j;

    move-result-object v4

    move-object v3, v4

    .line 1985347
    invoke-virtual {v3}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1985348
    iget-object v3, p0, LX/DJW;->c:LX/DJc;

    .line 1985349
    invoke-virtual {v3}, LX/AQ9;->R()LX/B5j;

    move-result-object v4

    move-object v3, v4

    .line 1985350
    invoke-virtual {v3}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v3

    .line 1985351
    if-eqz v3, :cond_0

    .line 1985352
    iget-object v4, v2, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    iget-object v5, v3, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    iget-object v5, v3, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    iget-object v5, v3, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->categoryID:Ljava/lang/String;

    iget-object v5, v3, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->categoryID:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->getLocationPageID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->getLocationPageID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1985353
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->a()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->b()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->c()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method
