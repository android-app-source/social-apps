.class public LX/Cn4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cms;


# instance fields
.field private final a:LX/Cju;


# direct methods
.method public constructor <init>(LX/Cju;)V
    .locals 0

    .prologue
    .line 1933721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1933722
    iput-object p1, p0, LX/Cn4;->a:LX/Cju;

    .line 1933723
    return-void
.end method

.method public static a(Landroid/view/View;Landroid/graphics/Rect;LX/Cmr;)V
    .locals 4

    .prologue
    .line 1933724
    iget v1, p1, Landroid/graphics/Rect;->left:I

    .line 1933725
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 1933726
    sget-object v2, LX/Cmr;->CENTER:LX/Cmr;

    if-ne p2, v2, :cond_0

    .line 1933727
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 1933728
    :cond_0
    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    invoke-static {p0, v1, v2, v0, v3}, LX/CoV;->a(Landroid/view/View;IIII)V

    .line 1933729
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/Cml;)V
    .locals 2

    .prologue
    .line 1933730
    invoke-interface {p2}, LX/Cml;->a()LX/Cmw;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1933731
    invoke-interface {p2}, LX/Cml;->a()LX/Cmw;

    move-result-object v0

    iget-object v1, p0, LX/Cn4;->a:LX/Cju;

    invoke-static {v0, v1}, LX/Cn1;->a(LX/Cmw;LX/Cju;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1933732
    invoke-interface {p2}, LX/Cml;->b()LX/Cmr;

    move-result-object v1

    .line 1933733
    invoke-static {p1, v0, v1}, LX/Cn4;->a(Landroid/view/View;Landroid/graphics/Rect;LX/Cmr;)V

    .line 1933734
    :cond_0
    return-void
.end method
