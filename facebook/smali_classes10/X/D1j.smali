.class public LX/D1j;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0ih;

.field private static volatile c:LX/D1j;


# instance fields
.field public b:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1957346
    sget-object v0, LX/0ig;->U:LX/0ih;

    sput-object v0, LX/D1j;->a:LX/0ih;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1957347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/D1j;
    .locals 4

    .prologue
    .line 1957348
    sget-object v0, LX/D1j;->c:LX/D1j;

    if-nez v0, :cond_1

    .line 1957349
    const-class v1, LX/D1j;

    monitor-enter v1

    .line 1957350
    :try_start_0
    sget-object v0, LX/D1j;->c:LX/D1j;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1957351
    if-eqz v2, :cond_0

    .line 1957352
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1957353
    new-instance p0, LX/D1j;

    invoke-direct {p0}, LX/D1j;-><init>()V

    .line 1957354
    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    .line 1957355
    iput-object v3, p0, LX/D1j;->b:LX/0if;

    .line 1957356
    move-object v0, p0

    .line 1957357
    sput-object v0, LX/D1j;->c:LX/D1j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1957358
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1957359
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1957360
    :cond_1
    sget-object v0, LX/D1j;->c:LX/D1j;

    return-object v0

    .line 1957361
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1957362
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
