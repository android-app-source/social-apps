.class public LX/DPo;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/DQW;

.field public final d:LX/17W;

.field private final e:LX/1g8;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/DLW;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DKH;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/DPA;

.field private l:Landroid/content/res/Resources;

.field public final m:LX/1Ck;

.field public final n:LX/3mF;

.field private final o:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1993876
    const-class v0, LX/DPo;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/DPo;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/DLW;Lcom/facebook/content/SecureContextHelper;LX/17W;LX/1g8;LX/DQW;LX/0Ot;LX/0Ot;LX/0Ot;LX/DPA;Landroid/content/res/Resources;LX/1Ck;LX/3mF;Landroid/content/Context;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/DLW;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17W;",
            "LX/1g8;",
            "LX/DQW;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DKH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "Lcom/facebook/groups/groupactions/GroupActionsHelper;",
            "Landroid/content/res/Resources;",
            "LX/1Ck;",
            "LX/3mF;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1993860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1993861
    iput-object p1, p0, LX/DPo;->b:LX/0Or;

    .line 1993862
    iput-object p3, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    .line 1993863
    iput-object p4, p0, LX/DPo;->d:LX/17W;

    .line 1993864
    iput-object p5, p0, LX/DPo;->e:LX/1g8;

    .line 1993865
    iput-object p6, p0, LX/DPo;->c:LX/DQW;

    .line 1993866
    iput-object p7, p0, LX/DPo;->g:LX/0Ot;

    .line 1993867
    iput-object p2, p0, LX/DPo;->h:LX/DLW;

    .line 1993868
    iput-object p8, p0, LX/DPo;->i:LX/0Ot;

    .line 1993869
    iput-object p9, p0, LX/DPo;->j:LX/0Ot;

    .line 1993870
    iput-object p10, p0, LX/DPo;->k:LX/DPA;

    .line 1993871
    iput-object p11, p0, LX/DPo;->l:Landroid/content/res/Resources;

    .line 1993872
    iput-object p12, p0, LX/DPo;->m:LX/1Ck;

    .line 1993873
    iput-object p13, p0, LX/DPo;->n:LX/3mF;

    .line 1993874
    iput-object p14, p0, LX/DPo;->o:Landroid/content/Context;

    .line 1993875
    return-void
.end method

.method public static a(LX/0QB;)LX/DPo;
    .locals 1

    .prologue
    .line 1993859
    invoke-static {p0}, LX/DPo;->b(LX/0QB;)LX/DPo;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Z)V
    .locals 4

    .prologue
    .line 1993857
    iget-object v0, p0, LX/DPo;->m:LX/1Ck;

    const-string v1, "GROUP_FOLLOW_UNFOLLOW"

    new-instance v2, LX/DPe;

    invoke-direct {v2, p0, p2, p1}, LX/DPe;-><init>(LX/DPo;ZLcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    new-instance v3, LX/DPf;

    invoke-direct {v3, p0}, LX/DPf;-><init>(LX/DPo;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1993858
    return-void
.end method

.method public static a$redex0(LX/DPo;I)V
    .locals 3

    .prologue
    .line 1993855
    iget-object v0, p0, LX/DPo;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DPo;->l:Landroid/content/res/Resources;

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1993856
    return-void
.end method

.method private static b(LX/0QB;)LX/DPo;
    .locals 15

    .prologue
    .line 1993850
    new-instance v0, LX/DPo;

    const/16 v1, 0xc

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    .line 1993851
    new-instance v2, LX/DLW;

    const/16 v3, 0xc

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v3}, LX/DLW;-><init>(LX/0Or;)V

    .line 1993852
    move-object v2, v2

    .line 1993853
    check-cast v2, LX/DLW;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {p0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v5

    check-cast v5, LX/1g8;

    invoke-static {p0}, LX/DQW;->b(LX/0QB;)LX/DQW;

    move-result-object v6

    check-cast v6, LX/DQW;

    const/16 v7, 0x259

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2390

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x12c4

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p0}, LX/DPB;->b(LX/0QB;)LX/DPB;

    move-result-object v10

    check-cast v10, LX/DPA;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v11

    check-cast v11, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v12

    check-cast v12, LX/1Ck;

    invoke-static {p0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v13

    check-cast v13, LX/3mF;

    const-class v14, Landroid/content/Context;

    invoke-interface {p0, v14}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/Context;

    invoke-direct/range {v0 .. v14}, LX/DPo;-><init>(LX/0Or;LX/DLW;Lcom/facebook/content/SecureContextHelper;LX/17W;LX/1g8;LX/DQW;LX/0Ot;LX/0Ot;LX/0Ot;LX/DPA;Landroid/content/res/Resources;LX/1Ck;LX/3mF;Landroid/content/Context;)V

    .line 1993854
    return-object v0
.end method

.method private static b(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1993844
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->l()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    .line 1993845
    :goto_0
    if-eqz v0, :cond_1

    .line 1993846
    iget-object v0, p0, LX/DPo;->k:LX/DPA;

    iget-object v2, p0, LX/DPo;->o:Landroid/content/Context;

    new-instance v3, LX/DPj;

    invoke-direct {v3, p0, p1}, LX/DPj;-><init>(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {v0, v2, v1, v3}, LX/DPA;->a(Landroid/content/Context;ZLandroid/content/DialogInterface$OnClickListener;)V

    .line 1993847
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1993848
    goto :goto_0

    .line 1993849
    :cond_1
    iget-object v0, p0, LX/DPo;->k:LX/DPA;

    iget-object v1, p0, LX/DPo;->o:Landroid/content/Context;

    new-instance v3, LX/DPm;

    invoke-direct {v3, p0, p1}, LX/DPm;-><init>(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {v0, v1, v2, v3}, LX/DPA;->a(Landroid/content/Context;ZLandroid/content/DialogInterface$OnClickListener;)V

    goto :goto_1
.end method

.method public static c(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V
    .locals 3

    .prologue
    .line 1993687
    iget-object v0, p0, LX/DPo;->o:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 1993688
    iget-object v0, p0, LX/DPo;->o:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 1993689
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1993690
    if-nez p1, :cond_2

    .line 1993691
    const/4 v0, 0x0

    .line 1993692
    :goto_0
    move-object v0, v0

    .line 1993693
    if-eqz v0, :cond_0

    .line 1993694
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DPo;->o:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1993695
    :cond_0
    :goto_1
    return-void

    .line 1993696
    :cond_1
    iget-object v0, p0, LX/DPo;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/DPo;->a:Ljava/lang/String;

    const-string v2, "goToGroupMall failed. Context is not an Activity."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1993697
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/DPo;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1993698
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->GROUPS_MALL_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993699
    const-string v1, "group_feed_id"

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993700
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1993705
    sget-object v0, LX/DPn;->a:[I

    invoke-virtual {p1}, LX/DQO;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1993706
    iget-object v0, p0, LX/DPo;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/DPo;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "received click for unsupported section :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993707
    :cond_0
    :goto_0
    return-void

    .line 1993708
    :pswitch_0
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    const-string v2, "group_info_page"

    invoke-virtual {v0, v1, v2}, LX/DQW;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1993709
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 1993710
    :pswitch_1
    iget-object v3, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v5

    if-eq v0, v5, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v5

    if-ne v0, v5, :cond_2

    :cond_1
    move v0, v2

    .line 1993711
    :goto_1
    invoke-static {v3}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v1

    .line 1993712
    const-string v2, "target_fragment"

    sget-object v5, LX/0cQ;->GROUP_PENDING_POSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v5}, LX/0cQ;->ordinal()I

    move-result v5

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993713
    const-string v2, "group_is_viewer_admin"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1993714
    const-string v2, "group_feed_id"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993715
    move-object v0, v1

    .line 1993716
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1993717
    goto :goto_1

    .line 1993718
    :pswitch_2
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    const-string v2, "group_info_page"

    const-string v3, "https://m.facebook.com/groups/%s/madminpanel/reported/"

    .line 1993719
    if-nez p2, :cond_c

    .line 1993720
    const/4 v4, 0x0

    .line 1993721
    :goto_2
    move v4, v4

    .line 1993722
    invoke-virtual {v0, v1, v2, v3, v4}, LX/DQW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1993723
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 1993724
    :pswitch_3
    iget-object v0, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v2

    .line 1993725
    invoke-static {v1}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v3

    .line 1993726
    const-string v4, "target_fragment"

    sget-object v5, LX/0cQ;->GROUP_EDIT_SETTINGS_FRAGMENT:LX/0cQ;

    invoke-virtual {v5}, LX/0cQ;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993727
    const-string v4, "group_feed_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993728
    move-object v1, v3

    .line 1993729
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993730
    :pswitch_4
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/DQW;->h(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1993731
    const-string v1, "group_admin_type"

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993732
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993733
    :pswitch_5
    iget-object v0, p0, LX/DPo;->h:LX/DLW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    .line 1993734
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    iget-object v2, v0, LX/DLW;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 1993735
    const-string v3, "group_feed_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993736
    const-string v3, "target_fragment"

    sget-object v4, LX/0cQ;->GROUP_FILES_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993737
    move-object v0, v2

    .line 1993738
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993739
    :pswitch_6
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v3

    .line 1993740
    invoke-static {v0}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v4

    .line 1993741
    const-string v5, "group_feed_id"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993742
    const-string v5, "target_fragment"

    sget-object v6, LX/0cQ;->GROUP_MEMBERSHIP_FRAGMENT:LX/0cQ;

    invoke-virtual {v6}, LX/0cQ;->ordinal()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993743
    move-object v0, v4

    .line 1993744
    const-string v3, "group_info_data_model"

    invoke-static {v0, v3, p2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1993745
    const-string v3, "is_archived"

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->l()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    :goto_3
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1993746
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 1993747
    goto :goto_3

    .line 1993748
    :pswitch_7
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    .line 1993749
    invoke-static {v0}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v2

    .line 1993750
    const-string v3, "group_feed_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993751
    const-string v3, "target_fragment"

    sget-object v4, LX/0cQ;->WORK_GROUPS_COMPANIES_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993752
    move-object v0, v2

    .line 1993753
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993754
    :pswitch_8
    instance-of v0, p3, Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_0

    .line 1993755
    check-cast p3, Lcom/facebook/widget/text/BetterTextView;

    .line 1993756
    invoke-virtual {p3}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v3, p0, LX/DPo;->l:Landroid/content/res/Resources;

    const v4, 0x7f08305a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1993757
    iget-object v0, p0, LX/DPo;->l:Landroid/content/res/Resources;

    const v1, 0x7f08305b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1993758
    invoke-static {p0, p2, v2}, LX/DPo;->a(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Z)V

    goto/16 :goto_0

    .line 1993759
    :cond_4
    iget-object v0, p0, LX/DPo;->l:Landroid/content/res/Resources;

    const v2, 0x7f08305a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1993760
    invoke-static {p0, p2, v1}, LX/DPo;->a(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Z)V

    goto/16 :goto_0

    .line 1993761
    :pswitch_9
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    .line 1993762
    const-string v2, "https://m.facebook.com/report/id/?id=GROUP_ID"

    const-string v3, "GROUP_ID"

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1993763
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1993764
    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1993765
    iget-object v3, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1993766
    goto/16 :goto_0

    .line 1993767
    :pswitch_a
    iget-object v0, p0, LX/DPo;->e:LX/1g8;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    const-string v2, "group_info"

    .line 1993768
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "group_side_conversation_displayed"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1993769
    iput-object v2, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1993770
    move-object v3, v3

    .line 1993771
    const-string v4, "group_id"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1993772
    iget-object v4, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1993773
    invoke-static {v0, v3}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1993774
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    .line 1993775
    invoke-static {v0}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v2

    .line 1993776
    const-string v3, "target_fragment"

    sget-object v4, LX/0cQ;->GROUP_CREATE_SIDE_CONVERSATION_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993777
    const-string v3, "group_feed_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993778
    move-object v0, v2

    .line 1993779
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993780
    :pswitch_b
    iget-object v0, p0, LX/DPo;->e:LX/1g8;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    .line 1993781
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "subgroup_creation_entered"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "group_creation"

    .line 1993782
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1993783
    move-object v2, v2

    .line 1993784
    const-string v3, "parent_group_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1993785
    iget-object v3, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1993786
    invoke-static {v0, v2}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1993787
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1993788
    const-string v1, "parent_group_or_page_name"

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993789
    const-string v1, "parent_group_id"

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993790
    iget-object v1, p0, LX/DPo;->d:LX/17W;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->N:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 1993791
    :pswitch_c
    iget-object v0, p0, LX/DPo;->e:LX/1g8;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    .line 1993792
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "browse_all_subgroups_entered"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "group_info"

    .line 1993793
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1993794
    move-object v2, v2

    .line 1993795
    const-string v3, "parent_group_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1993796
    iget-object v3, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1993797
    invoke-static {v0, v2}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1993798
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    .line 1993799
    invoke-static {v0}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v5

    .line 1993800
    const-string v4, "target_fragment"

    sget-object v6, LX/0cQ;->COMMUNITY_GROUPS_FRAGMENT:LX/0cQ;

    invoke-virtual {v6}, LX/0cQ;->ordinal()I

    move-result v6

    invoke-virtual {v5, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993801
    const-string v4, "group_feed_id"

    invoke-virtual {v5, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993802
    const-string v4, "group_name"

    invoke-virtual {v5, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993803
    const-string v4, "group_category"

    invoke-virtual {v5, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1993804
    const-string v6, "community_groups_list_type"

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INFORMAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v4, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    sget-object v4, LX/5QS;->ALL_SUB_GROUPS:LX/5QS;

    :goto_4
    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1993805
    move-object v0, v5

    .line 1993806
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993807
    :pswitch_d
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    .line 1993808
    invoke-static {v0}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v2

    .line 1993809
    const-string v3, "group_feed_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993810
    const-string v3, "target_fragment"

    sget-object v4, LX/0cQ;->GROUP_EVENTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993811
    move-object v0, v2

    .line 1993812
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993813
    :pswitch_e
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1993814
    invoke-static {v0}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v3

    .line 1993815
    const-string v4, "group_feed_id"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993816
    const-string v4, "target_fragment"

    sget-object v5, LX/0cQ;->GROUP_PHOTOS_FRAGMENT:LX/0cQ;

    invoke-virtual {v5}, LX/0cQ;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993817
    const-string v4, "group_name"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993818
    move-object v0, v3

    .line 1993819
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993820
    :pswitch_f
    const/4 v3, 0x0

    .line 1993821
    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 1993822
    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1993823
    const-class v5, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {v4, v0, v1, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    if-eqz v0, :cond_8

    .line 1993824
    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {v4, v0, v1, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1993825
    if-eqz v0, :cond_7

    move v0, v2

    :goto_6
    if-eqz v0, :cond_a

    .line 1993826
    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {v4, v0, v1, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1993827
    invoke-virtual {v4, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    :goto_7
    if-eqz v2, :cond_b

    .line 1993828
    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {v2, v0, v1, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1993829
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1993830
    :goto_8
    iget-object v1, p0, LX/DPo;->k:LX/DPA;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/DPA;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 1993831
    iget-object v0, p0, LX/DPo;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08065f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 1993832
    goto :goto_5

    :cond_6
    move v0, v1

    goto/16 :goto_5

    :cond_7
    move v0, v1

    goto :goto_6

    :cond_8
    move v0, v1

    goto :goto_6

    :cond_9
    move v2, v1

    goto :goto_7

    :cond_a
    move v2, v1

    goto :goto_7

    .line 1993833
    :pswitch_10
    iget-object v0, p0, LX/DPo;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DKH;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x7aa

    invoke-virtual {v0, v1, v2}, LX/DKH;->a(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 1993834
    :pswitch_11
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    .line 1993835
    invoke-static {v0}, LX/DQW;->a(LX/DQW;)Landroid/content/Intent;

    move-result-object v2

    .line 1993836
    const-string v3, "group_feed_id"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1993837
    const-string v3, "target_fragment"

    sget-object v4, LX/0cQ;->GROUPS_ADMIN_ACTIVITY_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1993838
    move-object v0, v2

    .line 1993839
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993840
    :pswitch_12
    iget-object v0, p0, LX/DPo;->c:LX/DQW;

    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/DQW;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1993841
    iget-object v1, p0, LX/DPo;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1993842
    :pswitch_13
    invoke-static {p0, p2}, LX/DPo;->b(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    goto/16 :goto_0

    :cond_b
    move-object v0, v3

    goto/16 :goto_8

    :cond_c
    invoke-virtual {p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v4

    const-string v5, "gk_groups_flagged_reported_post_queue"

    invoke-static {v4, v5}, LX/88m;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;Ljava/lang/String;)Z

    move-result v4

    goto/16 :goto_2

    .line 1993843
    :cond_d
    sget-object v4, LX/5QS;->ALL_CHILD_GROUPS:LX/5QS;

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public final a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/DPp;LX/0gc;Landroid/view/View;)V
    .locals 9

    .prologue
    .line 1993701
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 1993702
    new-instance v0, LX/DPD;

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/15i;->j(II)I

    move-result v3

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ab()Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v6

    const-string v7, "group_info_page"

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/DPD;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGroupVisibility;Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupCategory;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v0

    .line 1993703
    iget-object v0, v1, LX/DPo;->k:LX/DPA;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    new-instance v8, LX/DPg;

    invoke-direct {v8, v1, v2, v3}, LX/DPg;-><init>(LX/DPo;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/DPp;)V

    invoke-virtual {v0, v7, v4, v6, v8}, LX/DPA;->a(Landroid/content/Context;LX/0gc;LX/DPD;LX/DPC;)V

    .line 1993704
    return-void
.end method
