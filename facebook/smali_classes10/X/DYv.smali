.class public final LX/DYv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2012747
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 2012748
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2012749
    :goto_0
    return v1

    .line 2012750
    :cond_0
    const-string v11, "is_multi_company_group"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2012751
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    .line 2012752
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 2012753
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2012754
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2012755
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2012756
    const-string v11, "cover_photo"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2012757
    invoke-static {p0, p1}, LX/DYk;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2012758
    :cond_2
    const-string v11, "group_pending_members"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2012759
    invoke-static {p0, p1}, LX/DYu;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2012760
    :cond_3
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2012761
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2012762
    :cond_4
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2012763
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2012764
    :cond_5
    const-string v11, "viewer_admin_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2012765
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2012766
    :cond_6
    const-string v11, "visibility_sentence"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 2012767
    const/4 v10, 0x0

    .line 2012768
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v11, :cond_e

    .line 2012769
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2012770
    :goto_2
    move v3, v10

    .line 2012771
    goto :goto_1

    .line 2012772
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2012773
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2012774
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2012775
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 2012776
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2012777
    if-eqz v0, :cond_9

    .line 2012778
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 2012779
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2012780
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2012781
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2012782
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1

    .line 2012783
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2012784
    :cond_c
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_d

    .line 2012785
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2012786
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2012787
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_c

    if-eqz v11, :cond_c

    .line 2012788
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 2012789
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    .line 2012790
    :cond_d
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2012791
    invoke-virtual {p1, v10, v3}, LX/186;->b(II)V

    .line 2012792
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_2

    :cond_e
    move v3, v10

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x5

    .line 2012793
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012794
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2012795
    if-eqz v0, :cond_3

    .line 2012796
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012797
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012798
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2012799
    if-eqz v1, :cond_2

    .line 2012800
    const-string v3, "photo"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012801
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012802
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2012803
    if-eqz v3, :cond_1

    .line 2012804
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012805
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012806
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2012807
    if-eqz v0, :cond_0

    .line 2012808
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012809
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2012810
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012811
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012812
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012813
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2012814
    if-eqz v0, :cond_4

    .line 2012815
    const-string v1, "group_pending_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012816
    invoke-static {p0, v0, p2, p3}, LX/DYu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2012817
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2012818
    if-eqz v0, :cond_5

    .line 2012819
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012820
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2012821
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2012822
    if-eqz v0, :cond_6

    .line 2012823
    const-string v1, "is_multi_company_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012824
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2012825
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2012826
    if-eqz v0, :cond_7

    .line 2012827
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012828
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2012829
    :cond_7
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2012830
    if-eqz v0, :cond_8

    .line 2012831
    const-string v0, "viewer_admin_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012832
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2012833
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2012834
    if-eqz v0, :cond_a

    .line 2012835
    const-string v1, "visibility_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012836
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2012837
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2012838
    if-eqz v1, :cond_9

    .line 2012839
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2012840
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2012841
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012842
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2012843
    return-void
.end method
