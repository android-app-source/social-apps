.class public LX/DpI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final height:Ljava/lang/Integer;

.field public final width:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2044098
    new-instance v0, LX/1sv;

    const-string v1, "ImageMetadata"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpI;->b:LX/1sv;

    .line 2044099
    new-instance v0, LX/1sw;

    const-string v1, "width"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpI;->c:LX/1sw;

    .line 2044100
    new-instance v0, LX/1sw;

    const-string v1, "height"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpI;->d:LX/1sw;

    .line 2044101
    const/4 v0, 0x1

    sput-boolean v0, LX/DpI;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 2044094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2044095
    iput-object p1, p0, LX/DpI;->width:Ljava/lang/Integer;

    .line 2044096
    iput-object p2, p0, LX/DpI;->height:Ljava/lang/Integer;

    .line 2044097
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2044062
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2044063
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 2044064
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 2044065
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "ImageMetadata"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2044066
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044067
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044068
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044069
    const/4 v1, 0x1

    .line 2044070
    iget-object v5, p0, LX/DpI;->width:Ljava/lang/Integer;

    if-eqz v5, :cond_0

    .line 2044071
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044072
    const-string v1, "width"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044073
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044074
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044075
    iget-object v1, p0, LX/DpI;->width:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 2044076
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044077
    :goto_3
    const/4 v1, 0x0

    .line 2044078
    :cond_0
    iget-object v5, p0, LX/DpI;->height:Ljava/lang/Integer;

    if-eqz v5, :cond_2

    .line 2044079
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044080
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044081
    const-string v1, "height"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044082
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044083
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044084
    iget-object v0, p0, LX/DpI;->height:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 2044085
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044086
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044087
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2044088
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2044089
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2044090
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2044091
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 2044092
    :cond_6
    iget-object v1, p0, LX/DpI;->width:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2044093
    :cond_7
    iget-object v0, p0, LX/DpI;->height:Ljava/lang/Integer;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 1

    .prologue
    .line 2044050
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2044051
    iget-object v0, p0, LX/DpI;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2044052
    iget-object v0, p0, LX/DpI;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2044053
    sget-object v0, LX/DpI;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044054
    iget-object v0, p0, LX/DpI;->width:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2044055
    :cond_0
    iget-object v0, p0, LX/DpI;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2044056
    iget-object v0, p0, LX/DpI;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2044057
    sget-object v0, LX/DpI;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2044058
    iget-object v0, p0, LX/DpI;->height:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2044059
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2044060
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2044061
    return-void
.end method

.method public final a(LX/DpI;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2044025
    if-nez p1, :cond_1

    .line 2044026
    :cond_0
    :goto_0
    return v2

    .line 2044027
    :cond_1
    iget-object v0, p0, LX/DpI;->width:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    move v0, v1

    .line 2044028
    :goto_1
    iget-object v3, p1, LX/DpI;->width:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    move v3, v1

    .line 2044029
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    .line 2044030
    :cond_2
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2044031
    iget-object v0, p0, LX/DpI;->width:Ljava/lang/Integer;

    iget-object v3, p1, LX/DpI;->width:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2044032
    :cond_3
    iget-object v0, p0, LX/DpI;->height:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2044033
    :goto_3
    iget-object v3, p1, LX/DpI;->height:Ljava/lang/Integer;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2044034
    :goto_4
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2044035
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2044036
    iget-object v0, p0, LX/DpI;->height:Ljava/lang/Integer;

    iget-object v3, p1, LX/DpI;->height:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_5
    move v2, v1

    .line 2044037
    goto :goto_0

    :cond_6
    move v0, v2

    .line 2044038
    goto :goto_1

    :cond_7
    move v3, v2

    .line 2044039
    goto :goto_2

    :cond_8
    move v0, v2

    .line 2044040
    goto :goto_3

    :cond_9
    move v3, v2

    .line 2044041
    goto :goto_4
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2044046
    if-nez p1, :cond_1

    .line 2044047
    :cond_0
    :goto_0
    return v0

    .line 2044048
    :cond_1
    instance-of v1, p1, LX/DpI;

    if-eqz v1, :cond_0

    .line 2044049
    check-cast p1, LX/DpI;

    invoke-virtual {p0, p1}, LX/DpI;->a(LX/DpI;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2044045
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2044042
    sget-boolean v0, LX/DpI;->a:Z

    .line 2044043
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpI;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2044044
    return-object v0
.end method
