.class public LX/ElA;
.super LX/El5;
.source ""

# interfaces
.implements LX/El0;


# instance fields
.field public final a:LX/ElE;

.field public volatile b:Lorg/apache/http/HttpResponse;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2164091
    invoke-direct {p0}, LX/El5;-><init>()V

    .line 2164092
    new-instance v0, LX/ElE;

    invoke-direct {v0}, LX/ElE;-><init>()V

    iput-object v0, p0, LX/ElA;->a:LX/ElE;

    .line 2164093
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2164094
    iget-object v0, p0, LX/ElA;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    return v0
.end method

.method public final a(LX/1j2;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1j2",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2164095
    iget-object v0, p0, LX/ElA;->a:LX/ElE;

    .line 2164096
    iget-object v1, p1, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v1

    .line 2164097
    new-instance v2, LX/ElD;

    invoke-direct {v2, v0}, LX/ElD;-><init>(LX/ElE;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2164098
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2164099
    iget-object v0, p0, LX/ElA;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 2164100
    iget-object v0, p0, LX/ElA;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 2164101
    if-nez v0, :cond_0

    .line 2164102
    new-instance v0, Ljava/io/EOFException;

    const-string v1, "No body, the caller should consult response code!"

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2164103
    :cond_0
    new-instance v1, LX/El9;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    new-instance v2, LX/El8;

    invoke-direct {v2, p0}, LX/El8;-><init>(LX/ElA;)V

    invoke-direct {v1, v0, v2}, LX/El9;-><init>(Ljava/io/InputStream;LX/El7;)V

    return-object v1
.end method
