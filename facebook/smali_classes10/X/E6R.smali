.class public final LX/E6R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/os/Bundle;

.field public final synthetic b:Lcom/facebook/reaction/ui/ReactionHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/ReactionHeaderView;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2080123
    iput-object p1, p0, LX/E6R;->b:Lcom/facebook/reaction/ui/ReactionHeaderView;

    iput-object p2, p0, LX/E6R;->a:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x22a390fc

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2080124
    iget-object v2, p0, LX/E6R;->b:Lcom/facebook/reaction/ui/ReactionHeaderView;

    iget-object v3, p0, LX/E6R;->b:Lcom/facebook/reaction/ui/ReactionHeaderView;

    iget-boolean v3, v3, Lcom/facebook/reaction/ui/ReactionHeaderView;->e:Z

    if-nez v3, :cond_0

    .line 2080125
    :goto_0
    iput-boolean v0, v2, Lcom/facebook/reaction/ui/ReactionHeaderView;->e:Z

    .line 2080126
    iget-object v0, p0, LX/E6R;->b:Lcom/facebook/reaction/ui/ReactionHeaderView;

    iget-object v0, v0, Lcom/facebook/reaction/ui/ReactionHeaderView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vi;

    new-instance v2, LX/Cg5;

    iget-object v3, p0, LX/E6R;->a:Landroid/os/Bundle;

    const-string v4, "place_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/E6R;->b:Lcom/facebook/reaction/ui/ReactionHeaderView;

    iget-boolean v4, v4, Lcom/facebook/reaction/ui/ReactionHeaderView;->e:Z

    iget-object v5, p0, LX/E6R;->a:Landroid/os/Bundle;

    const-string v6, "reaction_session_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, LX/Cg5;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2080127
    iget-object v0, p0, LX/E6R;->b:Lcom/facebook/reaction/ui/ReactionHeaderView;

    invoke-static {v0}, Lcom/facebook/reaction/ui/ReactionHeaderView;->a(Lcom/facebook/reaction/ui/ReactionHeaderView;)V

    .line 2080128
    const v0, -0x20c45e58

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2080129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
