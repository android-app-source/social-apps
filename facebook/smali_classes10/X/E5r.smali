.class public final LX/E5r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1Pn;

.field public final synthetic b:LX/E67;

.field public final synthetic c:LX/E2g;

.field public final synthetic d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;LX/1Pn;LX/E67;LX/E2g;)V
    .locals 0

    .prologue
    .line 2079053
    iput-object p1, p0, LX/E5r;->d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;

    iput-object p2, p0, LX/E5r;->a:LX/1Pn;

    iput-object p3, p0, LX/E5r;->b:LX/E67;

    iput-object p4, p0, LX/E5r;->c:LX/E2g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x3712d199

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2079054
    const v0, 0x7f0d28b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2079055
    if-nez v4, :cond_0

    .line 2079056
    const v0, 0x1aa4f1cc

    invoke-static {v2, v2, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2079057
    :goto_0
    return-void

    .line 2079058
    :cond_0
    iget-object v0, p0, LX/E5r;->d:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;

    iget-object v1, p0, LX/E5r;->a:LX/1Pn;

    iget-object v2, p0, LX/E5r;->b:LX/E67;

    iget-object v2, v2, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    iget-object v3, p0, LX/E5r;->c:LX/E2g;

    iget-object v5, p0, LX/E5r;->b:LX/E67;

    iget-object v5, v5, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079059
    invoke-static/range {v0 .. v5}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;->a$redex0(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionActionListSaveActionPartDefinition;LX/1Pn;LX/5sc;LX/E2g;Landroid/widget/TextView;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2079060
    const v0, 0x53e50806

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto :goto_0
.end method
