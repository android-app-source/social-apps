.class public final LX/DeH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DeE;

.field public final synthetic b:Lcom/facebook/messaging/font/FontLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/font/FontLoader;LX/DeE;)V
    .locals 0

    .prologue
    .line 2021257
    iput-object p1, p0, LX/DeH;->b:Lcom/facebook/messaging/font/FontLoader;

    iput-object p2, p0, LX/DeH;->a:LX/DeE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2021258
    iget-object v0, p0, LX/DeH;->b:Lcom/facebook/messaging/font/FontLoader;

    iget-object v0, v0, Lcom/facebook/messaging/font/FontLoader;->b:Ljava/util/Map;

    iget-object v1, p0, LX/DeH;->a:LX/DeE;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021259
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2021260
    iget-object v0, p0, LX/DeH;->b:Lcom/facebook/messaging/font/FontLoader;

    iget-object v0, v0, Lcom/facebook/messaging/font/FontLoader;->b:Ljava/util/Map;

    iget-object v1, p0, LX/DeH;->a:LX/DeE;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2021261
    return-void
.end method
