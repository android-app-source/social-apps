.class public final enum LX/DBa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DBa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DBa;

.field public static final enum COMMENT_FETCH_FAILED:LX/DBa;

.field public static final enum COMMENT_POST_FAILED:LX/DBa;

.field public static final enum FAILURE_LOADING_EVENTS:LX/DBa;

.field public static final enum FEED_IS_UP_TO_DATE:LX/DBa;

.field public static final enum FETCH_EVENT_FAILED:LX/DBa;

.field public static final enum FETCH_PAGE_FAILED:LX/DBa;

.field public static final enum FETCH_STORY_FAILED:LX/DBa;

.field public static final enum FETCH_TIMELINE_FAILED:LX/DBa;

.field public static final enum NONE:LX/DBa;

.field public static final enum NO_CONNECTION:LX/DBa;

.field public static final enum YOU_CAN_STILL_POST:LX/DBa;


# instance fields
.field public final bannerMessageId:I

.field public final isTemporaryBanner:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1973028
    new-instance v0, LX/DBa;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5, v5, v5}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->NONE:LX/DBa;

    .line 1973029
    new-instance v0, LX/DBa;

    const-string v1, "NO_CONNECTION"

    const v2, 0x7f08003f

    invoke-direct {v0, v1, v4, v5, v2}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->NO_CONNECTION:LX/DBa;

    .line 1973030
    new-instance v0, LX/DBa;

    const-string v1, "YOU_CAN_STILL_POST"

    const v2, 0x7f080042

    invoke-direct {v0, v1, v6, v5, v2}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->YOU_CAN_STILL_POST:LX/DBa;

    .line 1973031
    new-instance v0, LX/DBa;

    const-string v1, "FEED_IS_UP_TO_DATE"

    const v2, 0x7f08110c

    invoke-direct {v0, v1, v7, v4, v2}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->FEED_IS_UP_TO_DATE:LX/DBa;

    .line 1973032
    new-instance v0, LX/DBa;

    const-string v1, "COMMENT_FETCH_FAILED"

    const v2, 0x7f0810eb

    invoke-direct {v0, v1, v8, v4, v2}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->COMMENT_FETCH_FAILED:LX/DBa;

    .line 1973033
    new-instance v0, LX/DBa;

    const-string v1, "COMMENT_POST_FAILED"

    const/4 v2, 0x5

    const v3, 0x7f0810ec

    invoke-direct {v0, v1, v2, v4, v3}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->COMMENT_POST_FAILED:LX/DBa;

    .line 1973034
    new-instance v0, LX/DBa;

    const-string v1, "FETCH_STORY_FAILED"

    const/4 v2, 0x6

    const v3, 0x7f0810ed

    invoke-direct {v0, v1, v2, v4, v3}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->FETCH_STORY_FAILED:LX/DBa;

    .line 1973035
    new-instance v0, LX/DBa;

    const-string v1, "FETCH_TIMELINE_FAILED"

    const/4 v2, 0x7

    const v3, 0x7f080061

    invoke-direct {v0, v1, v2, v4, v3}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->FETCH_TIMELINE_FAILED:LX/DBa;

    .line 1973036
    new-instance v0, LX/DBa;

    const-string v1, "FETCH_PAGE_FAILED"

    const/16 v2, 0x8

    const v3, 0x7f080063

    invoke-direct {v0, v1, v2, v4, v3}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->FETCH_PAGE_FAILED:LX/DBa;

    .line 1973037
    new-instance v0, LX/DBa;

    const-string v1, "FETCH_EVENT_FAILED"

    const/16 v2, 0x9

    const v3, 0x7f080065

    invoke-direct {v0, v1, v2, v4, v3}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->FETCH_EVENT_FAILED:LX/DBa;

    .line 1973038
    new-instance v0, LX/DBa;

    const-string v1, "FAILURE_LOADING_EVENTS"

    const/16 v2, 0xa

    const v3, 0x7f080066

    invoke-direct {v0, v1, v2, v4, v3}, LX/DBa;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, LX/DBa;->FAILURE_LOADING_EVENTS:LX/DBa;

    .line 1973039
    const/16 v0, 0xb

    new-array v0, v0, [LX/DBa;

    sget-object v1, LX/DBa;->NONE:LX/DBa;

    aput-object v1, v0, v5

    sget-object v1, LX/DBa;->NO_CONNECTION:LX/DBa;

    aput-object v1, v0, v4

    sget-object v1, LX/DBa;->YOU_CAN_STILL_POST:LX/DBa;

    aput-object v1, v0, v6

    sget-object v1, LX/DBa;->FEED_IS_UP_TO_DATE:LX/DBa;

    aput-object v1, v0, v7

    sget-object v1, LX/DBa;->COMMENT_FETCH_FAILED:LX/DBa;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/DBa;->COMMENT_POST_FAILED:LX/DBa;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DBa;->FETCH_STORY_FAILED:LX/DBa;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DBa;->FETCH_TIMELINE_FAILED:LX/DBa;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/DBa;->FETCH_PAGE_FAILED:LX/DBa;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/DBa;->FETCH_EVENT_FAILED:LX/DBa;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/DBa;->FAILURE_LOADING_EVENTS:LX/DBa;

    aput-object v2, v0, v1

    sput-object v0, LX/DBa;->$VALUES:[LX/DBa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI)V"
        }
    .end annotation

    .prologue
    .line 1973040
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1973041
    iput-boolean p3, p0, LX/DBa;->isTemporaryBanner:Z

    .line 1973042
    iput p4, p0, LX/DBa;->bannerMessageId:I

    .line 1973043
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DBa;
    .locals 1

    .prologue
    .line 1973044
    const-class v0, LX/DBa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DBa;

    return-object v0
.end method

.method public static values()[LX/DBa;
    .locals 1

    .prologue
    .line 1973045
    sget-object v0, LX/DBa;->$VALUES:[LX/DBa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DBa;

    return-object v0
.end method


# virtual methods
.method public final getBannerString(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1973046
    sget-object v0, LX/DBa;->NONE:LX/DBa;

    if-ne p0, v0, :cond_0

    .line 1973047
    const/4 v0, 0x0

    .line 1973048
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, LX/DBa;->bannerMessageId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
