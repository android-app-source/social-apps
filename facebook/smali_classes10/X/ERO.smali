.class public LX/ERO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/ERP;",
        "Lcom/facebook/vault/protocol/VaultAllImagesGetResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2120825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2120826
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2120814
    check-cast p1, LX/ERP;

    .line 2120815
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2120816
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "limit"

    .line 2120817
    iget v2, p1, LX/ERP;->b:I

    move v2, v2

    .line 2120818
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120819
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "after"

    .line 2120820
    iget-object v2, p1, LX/ERP;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2120821
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2120822
    new-instance v0, LX/14N;

    const-string v1, "vaultGetSyncedImageStatus"

    const-string v2, "GET"

    const-string v3, "me/vaultimages"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2120823
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2120824
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/vault/protocol/VaultAllImagesGetResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/protocol/VaultAllImagesGetResult;

    return-object v0
.end method
