.class public final LX/ErJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel;",
        "LX/7Hc",
        "<",
        "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7B6;

.field public final synthetic b:LX/ErK;


# direct methods
.method public constructor <init>(LX/ErK;LX/7B6;)V
    .locals 0

    .prologue
    .line 2172776
    iput-object p1, p0, LX/ErJ;->b:LX/ErK;

    iput-object p2, p0, LX/ErJ;->a:LX/7B6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2172777
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2172778
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 2172779
    iget-object v0, p0, LX/ErJ;->b:LX/ErK;

    iget-object v1, p0, LX/ErJ;->a:LX/7B6;

    new-instance v2, Ljava/lang/Throwable;

    const-string v3, "Server returned null"

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/7HT;->a(LX/7B6;Ljava/lang/Throwable;)V

    .line 2172780
    new-instance v0, LX/7Hc;

    .line 2172781
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2172782
    invoke-direct {v0, v1}, LX/7Hc;-><init>(LX/0Px;)V

    .line 2172783
    :goto_1
    return-object v0

    .line 2172784
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2172785
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventInvitableEntriesSearchQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2172786
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2172787
    const v3, 0x3d76496b

    invoke-static {v2, v0, v1, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_2
    invoke-static {v0}, LX/ErK;->b(LX/2uF;)LX/0Px;

    move-result-object v1

    .line 2172788
    new-instance v0, LX/7Hc;

    invoke-direct {v0, v1}, LX/7Hc;-><init>(LX/0Px;)V

    goto :goto_1

    .line 2172789
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2172790
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2
.end method
