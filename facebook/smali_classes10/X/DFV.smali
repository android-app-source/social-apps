.class public LX/DFV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/DFQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DFQ",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/DFc;

.field private final c:LX/DFg;

.field private final d:LX/DFY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DFY",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/2do;

.field private final f:LX/0ad;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/DFQ;LX/DFc;LX/DFg;LX/DFY;LX/2do;LX/0ad;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DFQ;",
            "LX/DFc;",
            "LX/DFg;",
            "LX/DFY;",
            "LX/2do;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1978358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1978359
    iput-object p1, p0, LX/DFV;->a:LX/DFQ;

    .line 1978360
    iput-object p2, p0, LX/DFV;->b:LX/DFc;

    .line 1978361
    iput-object p3, p0, LX/DFV;->c:LX/DFg;

    .line 1978362
    iput-object p4, p0, LX/DFV;->d:LX/DFY;

    .line 1978363
    iput-object p5, p0, LX/DFV;->e:LX/2do;

    .line 1978364
    iput-object p6, p0, LX/DFV;->f:LX/0ad;

    .line 1978365
    iput-object p7, p0, LX/DFV;->g:LX/0Ot;

    .line 1978366
    return-void
.end method

.method private static a(LX/DFV;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Fa;LX/2ep;LX/1Pc;LX/3mj;)LX/1Dh;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;",
            "LX/1Fa;",
            "LX/2ep;",
            "TE;",
            "LX/3mj;",
            ")",
            "LX/1Dh;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 1978367
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v7}, LX/DFV;->a(LX/DFV;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Fa;LX/2ep;LX/1Pc;LX/3mj;Z)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x6

    const v2, 0x7f0b0060

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    .line 1978368
    iget-object v0, p4, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_0

    .line 1978369
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v7}, LX/DFV;->a(LX/DFV;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Fa;LX/2ep;LX/1Pc;LX/3mj;Z)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x5

    const v2, 0x7f0b0060

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1978370
    :cond_0
    return-object v8
.end method

.method private static a(LX/DFV;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Fa;LX/2ep;LX/1Pc;LX/3mj;Z)LX/1Dh;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;",
            "LX/1Fa;",
            "LX/2ep;",
            "TE;",
            "LX/3mj;",
            "Z)",
            "LX/1Dh;"
        }
    .end annotation

    .prologue
    .line 1978299
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    iget-object v0, p0, LX/DFV;->d:LX/DFY;

    const/4 v2, 0x0

    .line 1978300
    new-instance v3, LX/DFX;

    invoke-direct {v3, v0}, LX/DFX;-><init>(LX/DFY;)V

    .line 1978301
    iget-object p0, v0, LX/DFY;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DFW;

    .line 1978302
    if-nez p0, :cond_0

    .line 1978303
    new-instance p0, LX/DFW;

    invoke-direct {p0, v0}, LX/DFW;-><init>(LX/DFY;)V

    .line 1978304
    :cond_0
    invoke-static {p0, p1, v2, v2, v3}, LX/DFW;->a$redex0(LX/DFW;LX/1De;IILX/DFX;)V

    .line 1978305
    move-object v3, p0

    .line 1978306
    move-object v2, v3

    .line 1978307
    move-object v2, v2

    .line 1978308
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1978309
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 1978310
    iget-object v3, v2, LX/DFW;->a:LX/DFX;

    iput-object v0, v3, LX/DFX;->c:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 1978311
    iget-object v3, v2, LX/DFW;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 1978312
    move-object v0, v2

    .line 1978313
    iget-object v2, v0, LX/DFW;->a:LX/DFX;

    iput-object p3, v2, LX/DFX;->d:LX/1Fa;

    .line 1978314
    iget-object v2, v0, LX/DFW;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978315
    move-object v0, v0

    .line 1978316
    iget-object v2, v0, LX/DFW;->a:LX/DFX;

    iput-object p6, v2, LX/DFX;->e:LX/3mj;

    .line 1978317
    iget-object v2, v0, LX/DFW;->e:Ljava/util/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978318
    move-object v0, v0

    .line 1978319
    iget-object v2, v0, LX/DFW;->a:LX/DFX;

    iput-object p4, v2, LX/DFX;->a:LX/2ep;

    .line 1978320
    iget-object v2, v0, LX/DFW;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978321
    move-object v0, v0

    .line 1978322
    iget-object v2, v0, LX/DFW;->a:LX/DFX;

    iput-object p5, v2, LX/DFX;->f:LX/1Pc;

    .line 1978323
    iget-object v2, v0, LX/DFW;->e:Ljava/util/BitSet;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978324
    move-object v0, v0

    .line 1978325
    iget-object v2, v0, LX/DFW;->a:LX/DFX;

    iput-boolean p7, v2, LX/DFX;->b:Z

    .line 1978326
    iget-object v2, v0, LX/DFW;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978327
    move-object v0, v0

    .line 1978328
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/DFV;LX/1De;Ljava/lang/String;Ljava/lang/String;LX/1Pc;)LX/1Dh;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "TE;)",
            "LX/1Dh;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1978273
    if-nez p2, :cond_0

    .line 1978274
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v0

    .line 1978275
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, LX/DFV;->b:LX/DFc;

    const/4 v2, 0x0

    .line 1978276
    new-instance v3, LX/DFb;

    invoke-direct {v3, v1}, LX/DFb;-><init>(LX/DFc;)V

    .line 1978277
    iget-object p0, v1, LX/DFc;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DFa;

    .line 1978278
    if-nez p0, :cond_1

    .line 1978279
    new-instance p0, LX/DFa;

    invoke-direct {p0, v1}, LX/DFa;-><init>(LX/DFc;)V

    .line 1978280
    :cond_1
    invoke-static {p0, p1, v2, v2, v3}, LX/DFa;->a$redex0(LX/DFa;LX/1De;IILX/DFb;)V

    .line 1978281
    move-object v3, p0

    .line 1978282
    move-object v2, v3

    .line 1978283
    move-object v1, v2

    .line 1978284
    iget-object v2, v1, LX/DFa;->a:LX/DFb;

    iput-object p2, v2, LX/DFb;->a:Ljava/lang/String;

    .line 1978285
    iget-object v2, v1, LX/DFa;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978286
    move-object v1, v1

    .line 1978287
    check-cast p4, LX/1Pp;

    .line 1978288
    iget-object v2, v1, LX/DFa;->a:LX/DFb;

    iput-object p4, v2, LX/DFb;->c:LX/1Pp;

    .line 1978289
    iget-object v2, v1, LX/DFa;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978290
    move-object v1, v1

    .line 1978291
    iget-object v2, v1, LX/DFa;->a:LX/DFb;

    iput-object p3, v2, LX/DFb;->b:Ljava/lang/String;

    .line 1978292
    iget-object v2, v1, LX/DFa;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978293
    move-object v1, v1

    .line 1978294
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/DFV;LX/1De;LX/1Fa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2ep;LX/162;)LX/1Di;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1978329
    iget-object v0, p0, LX/DFV;->c:LX/DFg;

    const/4 v1, 0x0

    .line 1978330
    new-instance v3, LX/DFf;

    invoke-direct {v3, v0}, LX/DFf;-><init>(LX/DFg;)V

    .line 1978331
    sget-object p0, LX/DFg;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DFe;

    .line 1978332
    if-nez p0, :cond_0

    .line 1978333
    new-instance p0, LX/DFe;

    invoke-direct {p0}, LX/DFe;-><init>()V

    .line 1978334
    :cond_0
    invoke-static {p0, p1, v1, v1, v3}, LX/DFe;->a$redex0(LX/DFe;LX/1De;IILX/DFf;)V

    .line 1978335
    move-object v3, p0

    .line 1978336
    move-object v1, v3

    .line 1978337
    move-object v0, v1

    .line 1978338
    iget-object v1, v0, LX/DFe;->a:LX/DFf;

    iput-object p3, v1, LX/DFf;->a:Ljava/lang/String;

    .line 1978339
    iget-object v1, v0, LX/DFe;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978340
    move-object v0, v0

    .line 1978341
    iget-object v1, v0, LX/DFe;->a:LX/DFf;

    iput-object p5, v1, LX/DFf;->b:Ljava/lang/String;

    .line 1978342
    iget-object v1, v0, LX/DFe;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978343
    move-object v0, v0

    .line 1978344
    iget-object v1, v0, LX/DFe;->a:LX/DFf;

    iput-object p6, v1, LX/DFf;->c:LX/2ep;

    .line 1978345
    iget-object v1, v0, LX/DFe;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978346
    move-object v0, v0

    .line 1978347
    invoke-interface {p2}, LX/1Fa;->k()Ljava/lang/String;

    move-result-object v1

    .line 1978348
    iget-object v3, v0, LX/DFe;->a:LX/DFf;

    iput-object v1, v3, LX/DFf;->d:Ljava/lang/String;

    .line 1978349
    iget-object v3, v0, LX/DFe;->d:Ljava/util/BitSet;

    const/4 p0, 0x3

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 1978350
    move-object v0, v0

    .line 1978351
    iget-object v1, v0, LX/DFe;->a:LX/DFf;

    iput-object p4, v1, LX/DFf;->e:Ljava/lang/String;

    .line 1978352
    iget-object v1, v0, LX/DFe;->d:Ljava/util/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978353
    move-object v0, v0

    .line 1978354
    iget-object v1, v0, LX/DFe;->a:LX/DFf;

    iput-object p7, v1, LX/DFf;->f:LX/162;

    .line 1978355
    iget-object v1, v0, LX/DFe;->d:Ljava/util/BitSet;

    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 1978356
    move-object v0, v0

    .line 1978357
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0f6a

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0f6b

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f020add

    invoke-interface {v0, v1}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Di;->c(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1, v2}, LX/1Di;->k(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v2, v2}, LX/1Di;->k(II)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Fa;LX/1Pc;)LX/2ep;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;",
            "LX/1Fa;",
            "TE;)",
            "LX/2ep;"
        }
    .end annotation

    .prologue
    .line 1978295
    new-instance v1, LX/2en;

    invoke-interface {p1}, LX/1Fa;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/2eo;->a(LX/1Fa;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/2en;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1978296
    check-cast p2, LX/1Pr;

    .line 1978297
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1978298
    check-cast v0, LX/0jW;

    invoke-interface {p2, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ep;

    return-object v0
.end method

.method public static a(LX/0QB;)LX/DFV;
    .locals 11

    .prologue
    .line 1978262
    const-class v1, LX/DFV;

    monitor-enter v1

    .line 1978263
    :try_start_0
    sget-object v0, LX/DFV;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978264
    sput-object v2, LX/DFV;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978265
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978266
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978267
    new-instance v3, LX/DFV;

    invoke-static {v0}, LX/DFQ;->a(LX/0QB;)LX/DFQ;

    move-result-object v4

    check-cast v4, LX/DFQ;

    invoke-static {v0}, LX/DFc;->a(LX/0QB;)LX/DFc;

    move-result-object v5

    check-cast v5, LX/DFc;

    invoke-static {v0}, LX/DFg;->a(LX/0QB;)LX/DFg;

    move-result-object v6

    check-cast v6, LX/DFg;

    invoke-static {v0}, LX/DFY;->a(LX/0QB;)LX/DFY;

    move-result-object v7

    check-cast v7, LX/DFY;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v8

    check-cast v8, LX/2do;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const/16 v10, 0xafc

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/DFV;-><init>(LX/DFQ;LX/DFc;LX/DFg;LX/DFY;LX/2do;LX/0ad;LX/0Ot;)V

    .line 1978268
    move-object v0, v3

    .line 1978269
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978270
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978271
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978272
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/DFV;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/1Pc;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            "LX/1Fa;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 1978259
    new-instance v0, LX/2f0;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1, v2, v3}, LX/2f0;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    .line 1978260
    iget-object v1, p0, LX/DFV;->e:LX/2do;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 1978261
    return-void
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;LX/1Pc;LX/3mj;)LX/1Dg;
    .locals 19
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pc;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/3mj;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;",
            "TE;",
            "LX/3mj;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1978250
    invoke-static/range {p3 .. p3}, LX/2es;->c(LX/1Fa;)Ljava/lang/String;

    move-result-object v12

    .line 1978251
    invoke-static/range {p3 .. p3}, LX/2es;->a(LX/1Fa;)Ljava/lang/String;

    move-result-object v13

    .line 1978252
    invoke-static/range {p3 .. p3}, LX/2es;->b(LX/1Fa;)Ljava/lang/String;

    move-result-object v14

    .line 1978253
    invoke-static/range {p2 .. p4}, LX/DFV;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Fa;LX/1Pc;)LX/2ep;

    move-result-object v15

    .line 1978254
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DFV;->f:LX/0ad;

    sget-short v5, LX/2ez;->l:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1978255
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DFV;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1My;

    sget-object v7, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v8, 0x0

    invoke-static/range {p3 .. p3}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v4

    invoke-static {v4}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v10

    move-object/from16 v6, p3

    invoke-virtual/range {v5 .. v10}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v6

    .line 1978256
    move-object/from16 v0, p0

    iget-object v4, v0, LX/DFV;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1My;

    new-instance v7, LX/DEy;

    move-object/from16 v5, p4

    check-cast v5, LX/1Pr;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v7, v0, v1, v5}, LX/DEy;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;LX/1Pr;)V

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersEdge;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v7, v5, v6}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1978257
    :goto_0
    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0f68

    invoke-interface {v4, v5}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b0f67

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3d

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    const/4 v6, 0x2

    invoke-interface {v4, v5, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v16

    invoke-static/range {p1 .. p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a00d5

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    invoke-static {v0, v1, v13, v12, v2}, LX/DFV;->a(LX/DFV;LX/1De;Ljava/lang/String;Ljava/lang/String;LX/1Pc;)LX/1Dh;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, LX/16h;

    move-object/from16 v0, p3

    invoke-static {v0, v4}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v11

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object v7, v12

    move-object v8, v13

    move-object v9, v14

    move-object v10, v15

    invoke-static/range {v4 .. v11}, LX/DFV;->a(LX/DFV;LX/1De;LX/1Fa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2ep;LX/162;)LX/1Di;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v11

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object v8, v15

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    invoke-static/range {v4 .. v10}, LX/DFV;->a(LX/DFV;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Fa;LX/2ep;LX/1Pc;LX/3mj;)LX/1Dh;

    move-result-object v4

    invoke-interface {v11, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v12}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    return-object v4

    .line 1978258
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-static {v0, v4, v1, v2}, LX/DFV;->a(LX/DFV;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;LX/1Fa;LX/1Pc;)V

    goto/16 :goto_0
.end method
