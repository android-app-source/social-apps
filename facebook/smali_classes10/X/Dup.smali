.class public final enum LX/Dup;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dup;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dup;

.field public static final enum USER:LX/Dup;

.field public static final enum VIDEO_LIST:LX/Dup;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2057616
    new-instance v0, LX/Dup;

    const-string v1, "USER"

    invoke-direct {v0, v1, v2}, LX/Dup;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dup;->USER:LX/Dup;

    .line 2057617
    new-instance v0, LX/Dup;

    const-string v1, "VIDEO_LIST"

    invoke-direct {v0, v1, v3}, LX/Dup;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dup;->VIDEO_LIST:LX/Dup;

    .line 2057618
    const/4 v0, 0x2

    new-array v0, v0, [LX/Dup;

    sget-object v1, LX/Dup;->USER:LX/Dup;

    aput-object v1, v0, v2

    sget-object v1, LX/Dup;->VIDEO_LIST:LX/Dup;

    aput-object v1, v0, v3

    sput-object v0, LX/Dup;->$VALUES:[LX/Dup;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2057619
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dup;
    .locals 1

    .prologue
    .line 2057615
    const-class v0, LX/Dup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dup;

    return-object v0
.end method

.method public static values()[LX/Dup;
    .locals 1

    .prologue
    .line 2057614
    sget-object v0, LX/Dup;->$VALUES:[LX/Dup;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dup;

    return-object v0
.end method
