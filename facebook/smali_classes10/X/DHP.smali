.class public LX/DHP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/361;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/361",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/2mn;

.field private final c:LX/2mq;

.field private final d:LX/DH6;

.field private final e:LX/04J;


# direct methods
.method public constructor <init>(LX/361;LX/2mn;LX/2mq;LX/DH6;LX/04J;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1981878
    iput-object p1, p0, LX/DHP;->a:LX/361;

    .line 1981879
    iput-object p2, p0, LX/DHP;->b:LX/2mn;

    .line 1981880
    iput-object p3, p0, LX/DHP;->c:LX/2mq;

    .line 1981881
    iput-object p4, p0, LX/DHP;->d:LX/DH6;

    .line 1981882
    iput-object p5, p0, LX/DHP;->e:LX/04J;

    .line 1981883
    return-void
.end method

.method public static a(LX/DHP;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/1Pe;)LX/1Di;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;ITE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 1981884
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1981885
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1981886
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1981887
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1981888
    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1981889
    iget-object v2, p0, LX/DHP;->c:LX/2mq;

    .line 1981890
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1981891
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v0}, LX/2mq;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)F

    move-result v0

    .line 1981892
    iget-object v2, p0, LX/DHP;->b:LX/2mn;

    invoke-virtual {v2, v1, v0}, LX/2mn;->c(Lcom/facebook/feed/rows/core/props/FeedProps;F)LX/3FO;

    move-result-object v3

    .line 1981893
    new-instance v4, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 1981894
    new-instance v0, LX/3EE;

    const/4 v2, -0x1

    new-instance v5, LX/CDf;

    iget v6, v3, LX/3FO;->a:I

    iget v3, v3, LX/3FO;->b:I

    const/4 v7, 0x0

    invoke-direct {v5, v6, v3, v7}, LX/CDf;-><init>(III)V

    invoke-static {v5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    sget-object v5, LX/04D;->SINGLE_CREATOR_SET_INLINE:LX/04D;

    invoke-direct/range {v0 .. v5}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;LX/04D;)V

    .line 1981895
    new-instance v2, LX/3FV;

    const/4 v5, 0x0

    iget-object v3, p0, LX/DHP;->d:LX/DH6;

    invoke-virtual {v3, p2}, LX/DH6;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/D4s;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, LX/04D;->SINGLE_CREATOR_SET_VIDEO:LX/04D;

    move-object v3, v1

    invoke-direct/range {v2 .. v10}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;LX/D6L;LX/0JG;Ljava/lang/String;LX/04D;)V

    .line 1981896
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1981897
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1981898
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 1981899
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    .line 1981900
    iget-object v4, p0, LX/DHP;->e:LX/04J;

    sget-object v1, LX/04I;->STORY_SET_ID:LX/04I;

    iget-object v5, v1, LX/04I;->value:Ljava/lang/String;

    .line 1981901
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1981902
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v3, v5, v1}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1981903
    iget-object v1, p0, LX/DHP;->e:LX/04J;

    sget-object v4, LX/04I;->STORY_SET_VIDEO_POSITION:LX/04I;

    iget-object v4, v4, LX/04I;->value:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5}, LX/04J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1981904
    :cond_0
    iget-object v1, p0, LX/DHP;->a:LX/361;

    invoke-virtual {v1, p1}, LX/361;->c(LX/1De;)LX/CDK;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/CDK;->a(LX/3EE;)LX/CDK;

    move-result-object v0

    .line 1981905
    iget-object v1, v0, LX/CDK;->a:LX/CDL;

    iput-object v2, v1, LX/CDL;->c:LX/3FV;

    .line 1981906
    move-object v0, v0

    .line 1981907
    invoke-virtual {v0, p4}, LX/CDK;->a(LX/1Pe;)LX/CDK;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/DHP;
    .locals 9

    .prologue
    .line 1981908
    const-class v1, LX/DHP;

    monitor-enter v1

    .line 1981909
    :try_start_0
    sget-object v0, LX/DHP;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981910
    sput-object v2, LX/DHP;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981911
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981912
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981913
    new-instance v3, LX/DHP;

    invoke-static {v0}, LX/361;->a(LX/0QB;)LX/361;

    move-result-object v4

    check-cast v4, LX/361;

    invoke-static {v0}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v5

    check-cast v5, LX/2mn;

    invoke-static {v0}, LX/2mq;->a(LX/0QB;)LX/2mq;

    move-result-object v6

    check-cast v6, LX/2mq;

    invoke-static {v0}, LX/DH6;->a(LX/0QB;)LX/DH6;

    move-result-object v7

    check-cast v7, LX/DH6;

    invoke-static {v0}, LX/04J;->a(LX/0QB;)LX/04J;

    move-result-object v8

    check-cast v8, LX/04J;

    invoke-direct/range {v3 .. v8}, LX/DHP;-><init>(LX/361;LX/2mn;LX/2mq;LX/DH6;LX/04J;)V

    .line 1981914
    move-object v0, v3

    .line 1981915
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981916
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DHP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981917
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981918
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
