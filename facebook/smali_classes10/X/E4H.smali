.class public final LX/E4H;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/E4I;


# direct methods
.method public constructor <init>(LX/E4I;)V
    .locals 0

    .prologue
    .line 2076449
    iput-object p1, p0, LX/E4H;->a:LX/E4I;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2076450
    iget-object v0, p0, LX/E4H;->a:LX/E4I;

    const/4 v1, 0x0

    .line 2076451
    iput-boolean v1, v0, LX/E4I;->f:Z

    .line 2076452
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2076453
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2076454
    iget-object v0, p0, LX/E4H;->a:LX/E4I;

    const/4 v1, 0x0

    .line 2076455
    iput-boolean v1, v0, LX/E4I;->f:Z

    .line 2076456
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2076457
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionMoreSubComponentsResultModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;

    move-result-object v0

    .line 2076458
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2076459
    :cond_0
    new-instance v0, LX/E4G;

    iget-object v1, p0, LX/E4H;->a:LX/E4I;

    invoke-direct {v0, v1}, LX/E4G;-><init>(LX/E4I;)V

    invoke-virtual {p0, v0}, LX/E4H;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2076460
    :goto_0
    return-void

    .line 2076461
    :cond_1
    iget-object v1, p0, LX/E4H;->a:LX/E4I;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->b()LX/0us;

    move-result-object v2

    invoke-static {v1, v2}, LX/E4I;->a$redex0(LX/E4I;LX/0us;)V

    .line 2076462
    iget-object v1, p0, LX/E4H;->a:LX/E4I;

    iget-object v1, v1, LX/E4I;->c:LX/E4E;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPaginatedSubComponentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/E4I;->b(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/E4E;->a(Ljava/util/List;)V

    goto :goto_0
.end method
