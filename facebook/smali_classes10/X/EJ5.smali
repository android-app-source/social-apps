.class public LX/EJ5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxV;",
        ":",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field private final b:LX/17W;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/1nD;

.field public final e:LX/11S;

.field private final f:LX/154;


# direct methods
.method public constructor <init>(LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/17W;LX/11R;LX/154;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2103156
    iput-object p1, p0, LX/EJ5;->d:LX/1nD;

    .line 2103157
    iput-object p2, p0, LX/EJ5;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2103158
    iput-object p3, p0, LX/EJ5;->b:LX/17W;

    .line 2103159
    iput-object p4, p0, LX/EJ5;->e:LX/11S;

    .line 2103160
    iput-object p6, p0, LX/EJ5;->a:LX/0ad;

    .line 2103161
    iput-object p5, p0, LX/EJ5;->f:LX/154;

    .line 2103162
    return-void
.end method

.method public static a(LX/0QB;)LX/EJ5;
    .locals 10

    .prologue
    .line 2103144
    const-class v1, LX/EJ5;

    monitor-enter v1

    .line 2103145
    :try_start_0
    sget-object v0, LX/EJ5;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103146
    sput-object v2, LX/EJ5;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103147
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103148
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103149
    new-instance v3, LX/EJ5;

    invoke-static {v0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v4

    check-cast v4, LX/1nD;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v7

    check-cast v7, LX/11R;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v8

    check-cast v8, LX/154;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, LX/EJ5;-><init>(LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/17W;LX/11R;LX/154;LX/0ad;)V

    .line 2103150
    move-object v0, v3

    .line 2103151
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103152
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EJ5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103153
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103154
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x4

    .line 2103139
    if-nez p0, :cond_1

    .line 2103140
    const/4 p0, 0x0

    .line 2103141
    :cond_0
    :goto_0
    return-object p0

    .line 2103142
    :cond_1
    const-string v0, "www."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 2103143
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/CharSequence;
    .locals 11

    .prologue
    .line 2103128
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2103129
    if-eqz p1, :cond_0

    .line 2103130
    invoke-virtual {v0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2103131
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x11

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2103132
    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v6, p2, v6

    if-eqz v6, :cond_3

    iget-object v6, p0, LX/EJ5;->e:LX/11S;

    sget-object v7, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, p2

    invoke-interface {v6, v7, v8, v9}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v6

    :goto_0
    move-object v1, v6

    .line 2103133
    if-eqz v1, :cond_2

    .line 2103134
    if-eqz p1, :cond_1

    .line 2103135
    const v2, 0x7f08114e

    invoke-virtual {p4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2103136
    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2103137
    :cond_1
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2103138
    :cond_2
    return-object v0

    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final a(JILandroid/content/Context;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2103124
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2103125
    iget-object v1, p0, LX/EJ5;->e:LX/11S;

    sget-object v2, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p1

    invoke-interface {v1, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 2103126
    const v2, 0x7f0f0052

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, LX/EJ5;->f:LX/154;

    invoke-virtual {v4, p3, v6}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, p3, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2103127
    const v3, 0x7f080d4c

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    aput-object v2, v4, v7

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CxV;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2103108
    iget-object v0, p0, LX/EJ5;->a:LX/0ad;

    sget-short v1, LX/100;->Q:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2103109
    if-eqz v0, :cond_0

    .line 2103110
    iget-object v0, p0, LX/EJ5;->d:LX/1nD;

    invoke-interface {p6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    .line 2103111
    iget-object v2, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v1, v2

    .line 2103112
    sget-object v2, LX/8ci;->h:LX/8ci;

    invoke-virtual {v0, v1, p1, p2, v2}, LX/1nD;->b(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;

    move-result-object v0

    .line 2103113
    iget-object v1, p0, LX/EJ5;->c:Lcom/facebook/content/SecureContextHelper;

    check-cast p6, LX/1Pn;

    invoke-interface {p6}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2103114
    :goto_0
    return-void

    .line 2103115
    :cond_0
    if-eqz p4, :cond_1

    .line 2103116
    if-eqz p5, :cond_2

    .line 2103117
    sget-object v0, LX/0ax;->hn:Ljava/lang/String;

    invoke-static {p5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p4, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 2103118
    :cond_1
    :goto_1
    iget-object v0, p0, LX/EJ5;->b:LX/17W;

    check-cast p6, LX/1Pn;

    invoke-interface {p6}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 2103119
    :cond_2
    sget-object v0, LX/0ax;->hm:Ljava/lang/String;

    invoke-static {v0, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2103120
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2103121
    iget-object v0, p0, LX/EJ5;->e:LX/11S;

    sget-object v2, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p2

    invoke-interface {v0, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2103122
    if-nez p1, :cond_0

    .line 2103123
    :goto_0
    return-object v0

    :cond_0
    const v2, 0x7f080d4c

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
