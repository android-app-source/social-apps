.class public final LX/Dx5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dx6;


# direct methods
.method public constructor <init>(LX/Dx6;)V
    .locals 0

    .prologue
    .line 2062370
    iput-object p1, p0, LX/Dx5;->a:LX/Dx6;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2062371
    iget-object v0, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v0, v0, LX/Dvb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchTaggedMediaSet"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2062372
    iget-object v0, p0, LX/Dx5;->a:LX/Dx6;

    const v1, -0x2b7bb912

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2062373
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2062374
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/16 v5, 0xc

    .line 2062375
    if-eqz p1, :cond_0

    .line 2062376
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2062377
    if-nez v0, :cond_1

    .line 2062378
    :cond_0
    :goto_0
    return-void

    .line 2062379
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;

    .line 2062380
    iget-object v1, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v1, v1, LX/Dvb;->q:LX/DwK;

    const-string v2, "LoadImageURLs"

    const-string v3, "ExtraLoadImageURLsSource"

    iget-object v4, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v4, v4, LX/Dvb;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, LX/DwK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2062381
    if-eqz v0, :cond_0

    .line 2062382
    iget-object v1, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v1, v1, LX/Dvb;->i:LX/Dvc;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/Dvc;->a(LX/0Px;)V

    .line 2062383
    iget-object v1, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v0, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    .line 2062384
    iget-boolean v2, v0, LX/Dv0;->c:Z

    move v0, v2

    .line 2062385
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 2062386
    :goto_1
    iput-boolean v0, v1, LX/Dx6;->m:Z

    .line 2062387
    iget-object v0, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    .line 2062388
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2062389
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    .line 2062390
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2062391
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v5, :cond_2

    iget-object v0, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    .line 2062392
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2062393
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt v0, v5, :cond_2

    .line 2062394
    iget-object v0, p0, LX/Dx5;->a:LX/Dx6;

    iget-object v0, v0, LX/Dvb;->q:LX/DwK;

    const-string v1, "LoadScreenImages"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2062395
    :cond_2
    iget-object v0, p0, LX/Dx5;->a:LX/Dx6;

    const v1, -0x72486794

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2062396
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
