.class public LX/ESo;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/os/Handler;


# instance fields
.field private final b:LX/0kb;

.field private final c:LX/0oz;

.field private final d:LX/2hU;

.field private final e:LX/0xX;

.field private final f:Landroid/content/Context;

.field private g:Ljava/lang/Runnable;

.field public h:I

.field public i:LX/4nS;

.field public j:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2123571
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, LX/ESo;->a:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(LX/0kb;LX/0oz;LX/2hU;LX/0xX;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2123564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2123565
    iput-object p1, p0, LX/ESo;->b:LX/0kb;

    .line 2123566
    iput-object p2, p0, LX/ESo;->c:LX/0oz;

    .line 2123567
    iput-object p3, p0, LX/ESo;->d:LX/2hU;

    .line 2123568
    iput-object p4, p0, LX/ESo;->e:LX/0xX;

    .line 2123569
    iput-object p5, p0, LX/ESo;->f:Landroid/content/Context;

    .line 2123570
    return-void
.end method

.method public static a(LX/0QB;)LX/ESo;
    .locals 7

    .prologue
    .line 2123561
    new-instance v1, LX/ESo;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-static {p0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v3

    check-cast v3, LX/0oz;

    invoke-static {p0}, LX/2hU;->b(LX/0QB;)LX/2hU;

    move-result-object v4

    check-cast v4, LX/2hU;

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v5

    check-cast v5, LX/0xX;

    const-class v6, Landroid/content/Context;

    invoke-interface {p0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct/range {v1 .. v6}, LX/ESo;-><init>(LX/0kb;LX/0oz;LX/2hU;LX/0xX;Landroid/content/Context;)V

    .line 2123562
    move-object v0, v1

    .line 2123563
    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2123532
    iget-object v0, p0, LX/ESo;->j:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2123533
    :goto_0
    return-void

    .line 2123534
    :cond_0
    new-instance v0, LX/EVc;

    iget-object v1, p0, LX/ESo;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/EVc;-><init>(Landroid/content/Context;)V

    .line 2123535
    iget-object v1, v0, LX/EVc;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2123536
    new-instance v1, LX/ESn;

    invoke-direct {v1, p0}, LX/ESn;-><init>(LX/ESo;)V

    .line 2123537
    iget-object v2, v0, LX/EVc;->b:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2123538
    iget-object v1, p0, LX/ESo;->d:LX/2hU;

    const/4 v2, -0x2

    invoke-virtual {v1, v0, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    iput-object v0, p0, LX/ESo;->i:LX/4nS;

    .line 2123539
    iget-object v0, p0, LX/ESo;->i:LX/4nS;

    iget-object v1, p0, LX/ESo;->j:Landroid/view/View;

    .line 2123540
    iput-object v1, v0, LX/4nS;->i:Landroid/view/View;

    .line 2123541
    iget-object v0, p0, LX/ESo;->i:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->a()V

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2123572
    iget-object v0, p0, LX/ESo;->i:LX/4nS;

    if-eqz v0, :cond_0

    .line 2123573
    iget-object v0, p0, LX/ESo;->i:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 2123574
    const/4 v0, 0x0

    iput-object v0, p0, LX/ESo;->i:LX/4nS;

    .line 2123575
    :cond_0
    return-void
.end method

.method public static g(LX/ESo;)I
    .locals 1

    .prologue
    .line 2123560
    iget-object v0, p0, LX/ESo;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ESo;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->i()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2123554
    iget-object v0, p0, LX/ESo;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, LX/ESo;->g(LX/ESo;)I

    move-result v0

    :goto_0
    iput v0, p0, LX/ESo;->h:I

    .line 2123555
    iget-object v0, p0, LX/ESo;->g:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 2123556
    new-instance v0, Lcom/facebook/video/videohome/connectionhelper/VideoHomeConnectionIndicatorHelper$1;

    invoke-direct {v0, p0}, Lcom/facebook/video/videohome/connectionhelper/VideoHomeConnectionIndicatorHelper$1;-><init>(LX/ESo;)V

    iput-object v0, p0, LX/ESo;->g:Ljava/lang/Runnable;

    .line 2123557
    :cond_0
    sget-object v0, LX/ESo;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/ESo;->g:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    const v4, 0x16c9ae4f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2123558
    return-void

    .line 2123559
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2123549
    invoke-direct {p0}, LX/ESo;->f()V

    .line 2123550
    iget-object v0, p0, LX/ESo;->g:Ljava/lang/Runnable;

    .line 2123551
    if-eqz v0, :cond_0

    .line 2123552
    sget-object p0, LX/ESo;->a:Landroid/os/Handler;

    invoke-static {p0, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2123553
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2123542
    iget-object v0, p0, LX/ESo;->c:LX/0oz;

    invoke-virtual {v0}, LX/0oz;->f()D

    move-result-wide v0

    .line 2123543
    iget-object v2, p0, LX/ESo;->b:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2123544
    iget-object v0, p0, LX/ESo;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082262

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/ESo;->a(Ljava/lang/String;)V

    .line 2123545
    :goto_0
    return-void

    .line 2123546
    :cond_0
    const-wide v2, 0x4072c00000000000L    # 300.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 2123547
    iget-object v0, p0, LX/ESo;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082261

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/ESo;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2123548
    :cond_1
    invoke-direct {p0}, LX/ESo;->f()V

    goto :goto_0
.end method
