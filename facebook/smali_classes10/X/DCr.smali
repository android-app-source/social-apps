.class public final LX/DCr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3n2;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

.field public final synthetic d:LX/1Po;

.field public final synthetic e:LX/3mE;


# direct methods
.method public constructor <init>(LX/3mE;LX/3n2;ZLcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;LX/1Po;)V
    .locals 0

    .prologue
    .line 1974625
    iput-object p1, p0, LX/DCr;->e:LX/3mE;

    iput-object p2, p0, LX/DCr;->a:LX/3n2;

    iput-boolean p3, p0, LX/DCr;->b:Z

    iput-object p4, p0, LX/DCr;->c:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    iput-object p5, p0, LX/DCr;->d:LX/1Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1974623
    iget-object v0, p0, LX/DCr;->e:LX/3mE;

    iget-object v0, v0, LX/3mE;->c:LX/3mG;

    invoke-virtual {v0, p1}, LX/3mG;->a(Ljava/lang/Throwable;)V

    .line 1974624
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1974616
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1974617
    iget-object v3, p0, LX/DCr;->a:LX/3n2;

    iget-boolean v0, p0, LX/DCr;->b:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 1974618
    :goto_0
    iput-boolean v0, v3, LX/3n2;->a:Z

    .line 1974619
    iget-object v0, p0, LX/DCr;->c:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    iget-object v3, p0, LX/DCr;->c:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    invoke-interface {v3}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-static {v0, v4, v5}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 1974620
    iget-object v0, p0, LX/DCr;->d:LX/1Po;

    check-cast v0, LX/1Pq;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, LX/DCr;->c:Lcom/facebook/graphql/model/GroupsYouShouldJoinFeedUnit;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 1974621
    return-void

    :cond_0
    move v0, v2

    .line 1974622
    goto :goto_0
.end method
