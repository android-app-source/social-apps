.class public LX/EPP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/23P;


# direct methods
.method public constructor <init>(LX/23P;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2116726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2116727
    iput-object p1, p0, LX/EPP;->a:LX/23P;

    .line 2116728
    return-void
.end method

.method public static a(LX/0QB;)LX/EPP;
    .locals 4

    .prologue
    .line 2116729
    const-class v1, LX/EPP;

    monitor-enter v1

    .line 2116730
    :try_start_0
    sget-object v0, LX/EPP;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116731
    sput-object v2, LX/EPP;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116732
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116733
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116734
    new-instance p0, LX/EPP;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-direct {p0, v3}, LX/EPP;-><init>(LX/23P;)V

    .line 2116735
    move-object v0, p0

    .line 2116736
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116737
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EPP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116738
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
