.class public LX/CyB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cxe;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:LX/CxV;

.field private final b:LX/CxG;

.field private final c:LX/CvY;

.field private final d:LX/CzB;


# direct methods
.method public constructor <init>(LX/CxV;LX/CxG;LX/CzB;LX/CvY;)V
    .locals 0
    .param p1    # LX/CxV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CxG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/CzB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952464
    iput-object p1, p0, LX/CyB;->a:LX/CxV;

    .line 1952465
    iput-object p2, p0, LX/CyB;->b:LX/CxG;

    .line 1952466
    iput-object p4, p0, LX/CyB;->c:LX/CvY;

    .line 1952467
    iput-object p3, p0, LX/CyB;->d:LX/CzB;

    .line 1952468
    return-void
.end method


# virtual methods
.method public final c(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 9

    .prologue
    .line 1952469
    iget-object v0, p0, LX/CyB;->c:LX/CvY;

    iget-object v1, p0, LX/CyB;->a:LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->CLICK:LX/8ch;

    iget-object v3, p0, LX/CyB;->b:LX/CxG;

    invoke-interface {v3, p1}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v3

    iget-object v4, p0, LX/CyB;->d:LX/CzB;

    invoke-virtual {v4, p1}, LX/CzB;->a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v4

    iget-object v5, p0, LX/CyB;->a:LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v5

    iget-object v6, p0, LX/CyB;->b:LX/CxG;

    invoke-interface {v6, p1}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v7

    .line 1952470
    sget-object v8, LX/0Rg;->a:LX/0Rg;

    move-object v8, v8

    .line 1952471
    invoke-static {v5, v6, v7, v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952472
    return-void
.end method
