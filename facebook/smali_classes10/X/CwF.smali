.class public final enum LX/CwF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CwF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CwF;

.field public static final enum am_football:LX/CwF;

.field public static final enum app:LX/CwF;

.field public static final enum celebrity:LX/CwF;

.field public static final enum company:LX/CwF;

.field public static final enum echo:LX/CwF;

.field public static final enum escape:LX/CwF;

.field public static final enum escape_pps_style:LX/CwF;

.field public static final enum event:LX/CwF;

.field public static final enum group:LX/CwF;

.field public static final enum happening_now:LX/CwF;

.field public static final enum hashtag:LX/CwF;

.field public static final enum keyword:LX/CwF;

.field public static final enum link:LX/CwF;

.field public static final enum local:LX/CwF;

.field public static final enum local_category:LX/CwF;

.field public static final enum movie:LX/CwF;

.field public static final enum page:LX/CwF;

.field public static final enum photos:LX/CwF;

.field public static final enum recent:LX/CwF;

.field public static final enum special_intent_gener:LX/CwF;

.field public static final enum trending:LX/CwF;

.field public static final enum user:LX/CwF;

.field public static final enum videos:LX/CwF;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1950170
    new-instance v0, LX/CwF;

    const-string v1, "keyword"

    invoke-direct {v0, v1, v3}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->keyword:LX/CwF;

    .line 1950171
    new-instance v0, LX/CwF;

    const-string v1, "trending"

    invoke-direct {v0, v1, v4}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->trending:LX/CwF;

    .line 1950172
    new-instance v0, LX/CwF;

    const-string v1, "celebrity"

    invoke-direct {v0, v1, v5}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->celebrity:LX/CwF;

    .line 1950173
    new-instance v0, LX/CwF;

    const-string v1, "escape"

    invoke-direct {v0, v1, v6}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->escape:LX/CwF;

    .line 1950174
    new-instance v0, LX/CwF;

    const-string v1, "echo"

    invoke-direct {v0, v1, v7}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->echo:LX/CwF;

    .line 1950175
    new-instance v0, LX/CwF;

    const-string v1, "recent"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->recent:LX/CwF;

    .line 1950176
    new-instance v0, LX/CwF;

    const-string v1, "am_football"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->am_football:LX/CwF;

    .line 1950177
    new-instance v0, LX/CwF;

    const-string v1, "photos"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->photos:LX/CwF;

    .line 1950178
    new-instance v0, LX/CwF;

    const-string v1, "videos"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->videos:LX/CwF;

    .line 1950179
    new-instance v0, LX/CwF;

    const-string v1, "hashtag"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->hashtag:LX/CwF;

    .line 1950180
    new-instance v0, LX/CwF;

    const-string v1, "local"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->local:LX/CwF;

    .line 1950181
    new-instance v0, LX/CwF;

    const-string v1, "company"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->company:LX/CwF;

    .line 1950182
    new-instance v0, LX/CwF;

    const-string v1, "movie"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->movie:LX/CwF;

    .line 1950183
    new-instance v0, LX/CwF;

    const-string v1, "happening_now"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->happening_now:LX/CwF;

    .line 1950184
    new-instance v0, LX/CwF;

    const-string v1, "link"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->link:LX/CwF;

    .line 1950185
    new-instance v0, LX/CwF;

    const-string v1, "special_intent_gener"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->special_intent_gener:LX/CwF;

    .line 1950186
    new-instance v0, LX/CwF;

    const-string v1, "local_category"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->local_category:LX/CwF;

    .line 1950187
    new-instance v0, LX/CwF;

    const-string v1, "escape_pps_style"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->escape_pps_style:LX/CwF;

    .line 1950188
    new-instance v0, LX/CwF;

    const-string v1, "user"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->user:LX/CwF;

    .line 1950189
    new-instance v0, LX/CwF;

    const-string v1, "page"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->page:LX/CwF;

    .line 1950190
    new-instance v0, LX/CwF;

    const-string v1, "event"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->event:LX/CwF;

    .line 1950191
    new-instance v0, LX/CwF;

    const-string v1, "app"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->app:LX/CwF;

    .line 1950192
    new-instance v0, LX/CwF;

    const-string v1, "group"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, LX/CwF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CwF;->group:LX/CwF;

    .line 1950193
    const/16 v0, 0x17

    new-array v0, v0, [LX/CwF;

    sget-object v1, LX/CwF;->keyword:LX/CwF;

    aput-object v1, v0, v3

    sget-object v1, LX/CwF;->trending:LX/CwF;

    aput-object v1, v0, v4

    sget-object v1, LX/CwF;->celebrity:LX/CwF;

    aput-object v1, v0, v5

    sget-object v1, LX/CwF;->escape:LX/CwF;

    aput-object v1, v0, v6

    sget-object v1, LX/CwF;->echo:LX/CwF;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CwF;->recent:LX/CwF;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CwF;->am_football:LX/CwF;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CwF;->photos:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/CwF;->videos:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/CwF;->hashtag:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/CwF;->local:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/CwF;->company:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/CwF;->movie:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/CwF;->happening_now:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/CwF;->link:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/CwF;->special_intent_gener:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/CwF;->local_category:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/CwF;->escape_pps_style:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/CwF;->user:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/CwF;->page:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/CwF;->event:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/CwF;->app:LX/CwF;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/CwF;->group:LX/CwF;

    aput-object v2, v0, v1

    sput-object v0, LX/CwF;->$VALUES:[LX/CwF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1950196
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CwF;
    .locals 1

    .prologue
    .line 1950195
    const-class v0, LX/CwF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CwF;

    return-object v0
.end method

.method public static values()[LX/CwF;
    .locals 1

    .prologue
    .line 1950194
    sget-object v0, LX/CwF;->$VALUES:[LX/CwF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CwF;

    return-object v0
.end method
