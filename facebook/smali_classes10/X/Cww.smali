.class public LX/Cww;
.super LX/Cwh;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cwh",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Cww;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951369
    invoke-direct {p0}, LX/Cwh;-><init>()V

    .line 1951370
    return-void
.end method

.method public static a(LX/0QB;)LX/Cww;
    .locals 3

    .prologue
    .line 1951371
    sget-object v0, LX/Cww;->a:LX/Cww;

    if-nez v0, :cond_1

    .line 1951372
    const-class v1, LX/Cww;

    monitor-enter v1

    .line 1951373
    :try_start_0
    sget-object v0, LX/Cww;->a:LX/Cww;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1951374
    if-eqz v2, :cond_0

    .line 1951375
    :try_start_1
    new-instance v0, LX/Cww;

    invoke-direct {v0}, LX/Cww;-><init>()V

    .line 1951376
    move-object v0, v0

    .line 1951377
    sput-object v0, LX/Cww;->a:LX/Cww;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1951378
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1951379
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1951380
    :cond_1
    sget-object v0, LX/Cww;->a:LX/Cww;

    return-object v0

    .line 1951381
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1951382
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951367
    iget-object v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1951368
    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951366
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateModuleSuggestionUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951364
    iget-object v0, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1951365
    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1951383
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[See More] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v1

    invoke-virtual {v1}, LX/Cwb;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951362
    iget-object v0, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1951363
    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951360
    iget-object v0, p1, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->a:Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-object v0, v0

    .line 1951361
    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951356
    iget-object v0, p1, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 1951357
    iget-object p1, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v0, p1

    .line 1951358
    move-object v0, v0

    .line 1951359
    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreTypeaheadUnit;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1951355
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[See More] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951351
    iget-object v0, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1951352
    return-object v0
.end method

.method public final a(Lcom/facebook/search/model/TrendingTypeaheadUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1951353
    iget-object v0, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1951354
    return-object v0
.end method
