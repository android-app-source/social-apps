.class public final LX/EUX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

.field public b:I


# direct methods
.method public constructor <init>(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;)V
    .locals 1

    .prologue
    .line 2126142
    iput-object p1, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2126143
    const/4 v0, 0x0

    iput v0, p0, LX/EUX;->b:I

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 3

    .prologue
    .line 2126144
    if-nez p2, :cond_1

    .line 2126145
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k:LX/1k3;

    invoke-virtual {v0, p1}, LX/1Kt;->b(LX/0g8;)V

    .line 2126146
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->B:LX/ESm;

    .line 2126147
    iget-object v1, v0, LX/ESm;->b:LX/1Aa;

    move-object v0, v1

    .line 2126148
    invoke-virtual {v0}, LX/1Aa;->f()V

    .line 2126149
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->t:LX/D7a;

    invoke-virtual {v0, p1, p2}, LX/D7a;->a(LX/0g8;I)V

    .line 2126150
    iput p2, p0, LX/EUX;->b:I

    .line 2126151
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->w:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2126152
    :goto_1
    return-void

    .line 2126153
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 2126154
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->B:LX/ESm;

    .line 2126155
    iget-object v1, v0, LX/ESm;->b:LX/1Aa;

    move-object v0, v1

    .line 2126156
    invoke-virtual {v0}, LX/1Aa;->f()V

    goto :goto_0

    .line 2126157
    :cond_2
    packed-switch p2, :pswitch_data_0

    goto :goto_1

    .line 2126158
    :pswitch_0
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Mp;

    .line 2126159
    iget-boolean v1, v0, LX/5Mp;->f:Z

    if-nez v1, :cond_3

    .line 2126160
    :goto_2
    goto :goto_1

    .line 2126161
    :pswitch_1
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Mp;

    .line 2126162
    iget-boolean v1, v0, LX/5Mp;->d:Z

    const-string v2, "FrameRateInternalDebugger should only be used for employee debugging, but current logged-in user is not employee."

    invoke-static {v1, v2}, LX/0Tp;->a(ZLjava/lang/String;)V

    .line 2126163
    iget-boolean v1, v0, LX/5Mp;->f:Z

    if-eqz v1, :cond_5

    .line 2126164
    :goto_3
    goto :goto_1

    .line 2126165
    :cond_3
    iget-object v1, v0, LX/5Mp;->a:LX/19D;

    invoke-virtual {v1}, LX/19D;->b()V

    .line 2126166
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/5Mp;->f:Z

    .line 2126167
    iget-object v1, v0, LX/5Mp;->e:LX/0Jb;

    if-eqz v1, :cond_4

    .line 2126168
    iget-object v1, v0, LX/5Mp;->e:LX/0Jb;

    iget-object v2, v0, LX/5Mp;->b:LX/5Mo;

    iget v2, v2, LX/5Mo;->a:F

    iget-object p1, v0, LX/5Mp;->b:LX/5Mo;

    iget p1, p1, LX/5Mo;->b:F

    iget-object p0, v0, LX/5Mp;->b:LX/5Mo;

    iget p0, p0, LX/5Mo;->c:F

    .line 2126169
    iget p2, v1, LX/0Jb;->b:F

    iput v2, v1, LX/0Jb;->a:F

    add-float/2addr p2, v2

    iput p2, v1, LX/0Jb;->b:F

    .line 2126170
    iget p2, v1, LX/0Jb;->d:F

    iput p1, v1, LX/0Jb;->c:F

    add-float/2addr p2, p1

    iput p2, v1, LX/0Jb;->d:F

    .line 2126171
    iget p2, v1, LX/0Jb;->f:F

    iput p0, v1, LX/0Jb;->e:F

    add-float/2addr p2, p0

    iput p2, v1, LX/0Jb;->f:F

    .line 2126172
    iget-object p2, v1, LX/0Jb;->g:LX/0Ja;

    if-eqz p2, :cond_4

    .line 2126173
    iget-object p2, v1, LX/0Jb;->g:LX/0Ja;

    invoke-interface {p2, v1}, LX/0Ja;->a(LX/0Jb;)V

    .line 2126174
    :cond_4
    iget-object v1, v0, LX/5Mp;->b:LX/5Mo;

    const/4 v2, 0x0

    .line 2126175
    iput v2, v1, LX/5Mo;->a:F

    .line 2126176
    iput v2, v1, LX/5Mo;->b:F

    .line 2126177
    iput v2, v1, LX/5Mo;->c:F

    .line 2126178
    goto :goto_2

    .line 2126179
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/5Mp;->f:Z

    .line 2126180
    iget-object v1, v0, LX/5Mp;->a:LX/19D;

    invoke-virtual {v1}, LX/19D;->a()V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/0g8;III)V
    .locals 5

    .prologue
    .line 2126181
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    const/4 v1, 0x0

    .line 2126182
    iget-object v2, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ar:LX/1P0;

    invoke-virtual {v2, v1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    .line 2126183
    iget-object v3, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ao:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v3, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 2126184
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    const/4 v2, 0x1

    .line 2126185
    iget-object v1, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    if-nez v1, :cond_3

    .line 2126186
    :cond_1
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->k()V

    .line 2126187
    iget v0, p0, LX/EUX;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 2126188
    invoke-interface {p1}, LX/0g8;->y()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 2126189
    const/16 v1, 0x12c

    if-le v0, v1, :cond_8

    .line 2126190
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->B:LX/ESm;

    .line 2126191
    iget-object v1, v0, LX/ESm;->b:LX/1Aa;

    move-object v0, v1

    .line 2126192
    invoke-virtual {v0}, LX/1Aa;->d()V

    .line 2126193
    :cond_2
    :goto_0
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->t:LX/D7a;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/D7a;->a(LX/0g8;III)V

    .line 2126194
    return-void

    .line 2126195
    :cond_3
    iget-object v1, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->ar:LX/1P0;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v3

    .line 2126196
    iget v1, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->an:I

    if-eq v3, v1, :cond_1

    .line 2126197
    iget v1, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->an:I

    if-le v1, v3, :cond_5

    move v1, v2

    .line 2126198
    :goto_1
    iput v3, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->an:I

    .line 2126199
    if-eqz v1, :cond_6

    .line 2126200
    :goto_2
    sub-int v1, v3, v2

    if-ltz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    invoke-virtual {v1}, LX/0xX;->k()I

    move-result v1

    if-gt v2, v1, :cond_1

    .line 2126201
    sub-int v1, v3, v2

    invoke-static {v0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2126202
    if-eqz v1, :cond_4

    .line 2126203
    invoke-static {v0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2126204
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 2126205
    goto :goto_2

    .line 2126206
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 2126207
    :cond_6
    :goto_3
    add-int v1, v3, v2

    iget-object v4, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->aj:LX/1Rq;

    invoke-interface {v4}, LX/1OP;->ij_()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v1, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->A:LX/0xX;

    invoke-virtual {v1}, LX/0xX;->k()I

    move-result v1

    if-gt v2, v1, :cond_1

    .line 2126208
    add-int v1, v3, v2

    invoke-static {v0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2126209
    if-eqz v1, :cond_7

    .line 2126210
    invoke-static {v0, v1}, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->a(Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2126211
    :cond_7
    add-int/lit8 v2, v2, 0x1

    .line 2126212
    goto :goto_3

    .line 2126213
    :cond_8
    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 2126214
    iget-object v0, p0, LX/EUX;->a:Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    iget-object v0, v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;->B:LX/ESm;

    .line 2126215
    iget-object v1, v0, LX/ESm;->b:LX/1Aa;

    move-object v0, v1

    .line 2126216
    invoke-virtual {v0}, LX/1Aa;->f()V

    goto :goto_0
.end method
