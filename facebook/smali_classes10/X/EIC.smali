.class public LX/EIC;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final d:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

.field public e:Landroid/view/LayoutInflater;

.field public f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Landroid/view/View;

.field public i:Landroid/view/View;

.field public j:Lcom/facebook/user/tiles/UserTileView;

.field public k:Lcom/facebook/rtc/views/RtcLevelTileView;

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Lcom/facebook/user/tiles/UserTileView;

.field public n:LX/EGE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public p:LX/03R;

.field public q:J

.field public r:LX/EFy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z

.field private t:LX/EIB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2101215
    const-class v0, LX/EIC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EIC;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2101199
    invoke-direct {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2101200
    sget-object v0, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v0, v0

    .line 2101201
    iput-object v0, p0, LX/EIC;->d:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    .line 2101202
    const-class v0, LX/EIC;

    invoke-static {v0, p0}, LX/EIC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2101203
    invoke-virtual {p0}, LX/EIC;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/EIC;->e:Landroid/view/LayoutInflater;

    .line 2101204
    iget-object v0, p0, LX/EIC;->e:Landroid/view/LayoutInflater;

    const p1, 0x7f031246

    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2101205
    const v0, 0x7f0d1c88

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EIC;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2101206
    const v0, 0x7f0d2ab6

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EIC;->h:Landroid/view/View;

    .line 2101207
    const v0, 0x7f0d2af9

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    iput-object v0, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    .line 2101208
    const v0, 0x7f0d2afa

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EIC;->i:Landroid/view/View;

    .line 2101209
    const v0, 0x7f0d2afc

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/EIC;->j:Lcom/facebook/user/tiles/UserTileView;

    .line 2101210
    const v0, 0x7f0d2afb

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcLevelTileView;

    iput-object v0, p0, LX/EIC;->k:Lcom/facebook/rtc/views/RtcLevelTileView;

    .line 2101211
    const v0, 0x7f0d2afd

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EIC;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2101212
    const v0, 0x7f0d2afe

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/EIC;->m:Lcom/facebook/user/tiles/UserTileView;

    .line 2101213
    iget-object v0, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setScaleType(I)V

    .line 2101214
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/EIC;

    invoke-static {p0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    iput-object v1, p1, LX/EIC;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p0, p1, LX/EIC;->c:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private b()V
    .locals 11

    .prologue
    .line 2101190
    iget-object v0, p0, LX/EIC;->r:LX/EFy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    if-nez v0, :cond_1

    .line 2101191
    :cond_0
    :goto_0
    return-void

    .line 2101192
    :cond_1
    iget-object v0, p0, LX/EIC;->r:LX/EFy;

    iget-object v1, p0, LX/EIC;->n:LX/EGE;

    iget-wide v2, v1, LX/EGE;->h:J

    iget-object v1, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v0, v2, v3, v1}, LX/EFy;->b(JLandroid/view/View;)V

    .line 2101193
    iget-object v0, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setVisibility(I)V

    .line 2101194
    invoke-static {p0}, LX/EIC;->e$redex0(LX/EIC;)V

    .line 2101195
    iget-object v4, p0, LX/EIC;->b:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/facebook/rtc/views/RtcVideoParticipantView$1;

    invoke-direct {v5, p0}, Lcom/facebook/rtc/views/RtcVideoParticipantView$1;-><init>(LX/EIC;)V

    const-wide/16 v6, 0x3e8

    const-wide/16 v8, 0x320

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v4 .. v10}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v4

    iput-object v4, p0, LX/EIC;->o:Ljava/util/concurrent/ScheduledFuture;

    .line 2101196
    sget-object v0, LX/03R;->YES:LX/03R;

    iput-object v0, p0, LX/EIC;->p:LX/03R;

    .line 2101197
    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    iget-wide v0, v0, LX/EGE;->h:J

    iput-wide v0, p0, LX/EIC;->q:J

    .line 2101198
    invoke-static {p0}, LX/EIC;->g(LX/EIC;)V

    goto :goto_0
.end method

.method public static e$redex0(LX/EIC;)V
    .locals 2

    .prologue
    .line 2101186
    iget-object v0, p0, LX/EIC;->o:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2101187
    iget-object v0, p0, LX/EIC;->o:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2101188
    const/4 v0, 0x0

    iput-object v0, p0, LX/EIC;->o:Ljava/util/concurrent/ScheduledFuture;

    .line 2101189
    :cond_0
    return-void
.end method

.method public static f(LX/EIC;)V
    .locals 6

    .prologue
    .line 2101105
    sget-object v0, LX/EIA;->a:[I

    iget-object v1, p0, LX/EIC;->t:LX/EIB;

    invoke-virtual {v1}, LX/EIB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2101106
    :goto_0
    return-void

    .line 2101107
    :pswitch_0
    invoke-direct {p0}, LX/EIC;->i()V

    .line 2101108
    iget-object v0, p0, LX/EIC;->m:Lcom/facebook/user/tiles/UserTileView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2101109
    goto :goto_0

    .line 2101110
    :pswitch_1
    invoke-direct {p0}, LX/EIC;->k()V

    .line 2101111
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 2101112
    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    if-nez v0, :cond_0

    .line 2101113
    :goto_1
    goto :goto_0

    .line 2101114
    :cond_0
    iget-object v0, p0, LX/EIC;->p:LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, LX/EIC;->s:Z

    if-eqz v0, :cond_2

    .line 2101115
    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    iget-object v0, v0, LX/EGE;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2101116
    iget-object v0, p0, LX/EIC;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/EIC;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080703

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2101117
    :goto_2
    iget-object v0, p0, LX/EIC;->h:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2101118
    iget-object v0, p0, LX/EIC;->i:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2101119
    iget-object v0, p0, LX/EIC;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    .line 2101120
    :cond_1
    iget-object v0, p0, LX/EIC;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/EIC;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080702

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/EIC;->n:LX/EGE;

    iget-object v4, v4, LX/EGE;->d:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2101121
    :cond_2
    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    iget-object v0, v0, LX/EGE;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2101122
    iget-object v0, p0, LX/EIC;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/EIC;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080701

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2101123
    :cond_3
    iget-object v0, p0, LX/EIC;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/EIC;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080700

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/EIC;->n:LX/EGE;

    iget-object v4, v4, LX/EGE;->d:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static g(LX/EIC;)V
    .locals 0

    .prologue
    .line 2101183
    invoke-direct {p0}, LX/EIC;->k()V

    .line 2101184
    invoke-direct {p0}, LX/EIC;->i()V

    .line 2101185
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2101179
    iget-object v0, p0, LX/EIC;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2101180
    iget-object v0, p0, LX/EIC;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2101181
    iget-object v0, p0, LX/EIC;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2101182
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 2101177
    iget-object v0, p0, LX/EIC;->m:Lcom/facebook/user/tiles/UserTileView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2101178
    return-void
.end method


# virtual methods
.method public final a(LX/EFy;)V
    .locals 5
    .param p1    # LX/EFy;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2101172
    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    iget-wide v0, v0, LX/EGE;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    if-eqz p1, :cond_0

    .line 2101173
    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    iget-wide v0, v0, LX/EGE;->h:J

    invoke-virtual {p1, v0, v1, v4}, LX/EFy;->b(JLandroid/view/View;)V

    .line 2101174
    :cond_0
    iget-object v0, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    .line 2101175
    iput-object v4, v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->p:LX/7fE;

    .line 2101176
    return-void
.end method

.method public final a(LX/EGE;LX/EFy;LX/EIB;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2101143
    iget-object v0, p0, LX/EIC;->t:LX/EIB;

    if-eq v0, p3, :cond_2

    move v0, v1

    .line 2101144
    :goto_0
    iget-object v3, p0, LX/EIC;->n:LX/EGE;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/EIC;->n:LX/EGE;

    iget-object v3, v3, LX/EGE;->b:Ljava/lang/String;

    iget-object v4, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_0
    move v3, v1

    .line 2101145
    :goto_1
    iput-object p3, p0, LX/EIC;->t:LX/EIB;

    .line 2101146
    iput-object p2, p0, LX/EIC;->r:LX/EFy;

    .line 2101147
    iput-object p1, p0, LX/EIC;->n:LX/EGE;

    .line 2101148
    if-eqz p1, :cond_1

    if-nez p2, :cond_4

    .line 2101149
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 2101150
    goto :goto_0

    :cond_3
    move v3, v2

    .line 2101151
    goto :goto_1

    .line 2101152
    :cond_4
    iget-object v4, p0, LX/EIC;->g:Lcom/facebook/resources/ui/FbTextView;

    iget-object v5, p1, LX/EGE;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2101153
    if-eqz v3, :cond_5

    .line 2101154
    iget-object v3, p0, LX/EIC;->j:Lcom/facebook/user/tiles/UserTileView;

    new-instance v4, Lcom/facebook/user/model/UserKey;

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v6, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-static {v4}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2101155
    iget-object v3, p0, LX/EIC;->m:Lcom/facebook/user/tiles/UserTileView;

    new-instance v4, Lcom/facebook/user/model/UserKey;

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v6, p1, LX/EGE;->b:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-static {v4}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2101156
    :cond_5
    iget-object v3, p0, LX/EIC;->p:LX/03R;

    iget-object v4, p0, LX/EIC;->n:LX/EGE;

    iget-boolean v4, v4, LX/EGE;->i:Z

    invoke-static {v4}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v4

    if-ne v3, v4, :cond_6

    iget-wide v4, p0, LX/EIC;->q:J

    iget-object v3, p0, LX/EIC;->n:LX/EGE;

    iget-wide v6, v3, LX/EGE;->h:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_6

    if-nez v0, :cond_6

    iget-object v3, p0, LX/EIC;->r:LX/EFy;

    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    iget-wide v4, v0, LX/EGE;->h:J

    iget-object v0, p0, LX/EIC;->n:LX/EGE;

    iget-boolean v0, v0, LX/EGE;->i:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    :goto_3
    invoke-interface {v3, v4, v5, v0}, LX/EFv;->a(JLandroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    move v2, v1

    .line 2101157
    :cond_7
    if-eqz v2, :cond_1

    iget-object v0, p0, LX/EIC;->r:LX/EFy;

    .line 2101158
    iget-object v1, v0, LX/EFy;->a:LX/EFw;

    iget-object v2, v0, LX/EFy;->b:LX/EG5;

    const-string v3, "sourceAllowed"

    invoke-virtual {v1, v2, v3}, LX/EFw;->a(LX/EG5;Ljava/lang/String;)Z

    move-result v1

    move v0, v1

    .line 2101159
    if-eqz v0, :cond_1

    .line 2101160
    iget-boolean v0, p1, LX/EGE;->i:Z

    if-eqz v0, :cond_9

    .line 2101161
    invoke-direct {p0}, LX/EIC;->b()V

    goto :goto_2

    .line 2101162
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 2101163
    :cond_9
    iget-object v8, p0, LX/EIC;->r:LX/EFy;

    if-eqz v8, :cond_a

    iget-object v8, p0, LX/EIC;->n:LX/EGE;

    if-nez v8, :cond_b

    .line 2101164
    :cond_a
    :goto_4
    goto/16 :goto_2

    .line 2101165
    :cond_b
    iget-object v8, p0, LX/EIC;->r:LX/EFy;

    iget-object v9, p0, LX/EIC;->n:LX/EGE;

    iget-wide v10, v9, LX/EGE;->h:J

    const/4 v9, 0x0

    invoke-virtual {v8, v10, v11, v9}, LX/EFy;->b(JLandroid/view/View;)V

    .line 2101166
    iget-object v8, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setVisibility(I)V

    .line 2101167
    invoke-static {p0}, LX/EIC;->e$redex0(LX/EIC;)V

    .line 2101168
    sget-object v8, LX/03R;->NO:LX/03R;

    iput-object v8, p0, LX/EIC;->p:LX/03R;

    .line 2101169
    const/4 v8, 0x0

    iput-boolean v8, p0, LX/EIC;->s:Z

    .line 2101170
    const-wide/16 v8, 0x0

    iput-wide v8, p0, LX/EIC;->q:J

    .line 2101171
    invoke-static {p0}, LX/EIC;->f(LX/EIC;)V

    goto :goto_4
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x162a2d90

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2101137
    invoke-super {p0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onAttachedToWindow()V

    .line 2101138
    iget-object v1, p0, LX/EIC;->r:LX/EFy;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EIC;->n:LX/EGE;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    if-nez v1, :cond_1

    .line 2101139
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x2c97f1ad

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2101140
    :goto_0
    return-void

    .line 2101141
    :cond_1
    iget-object v1, p0, LX/EIC;->r:LX/EFy;

    iget-object v2, p0, LX/EIC;->n:LX/EGE;

    iget-wide v2, v2, LX/EGE;->h:J

    iget-object v4, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v1, v2, v3, v4}, LX/EFy;->b(JLandroid/view/View;)V

    .line 2101142
    const v1, 0x39b7fc30

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x45d2ce9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2101126
    invoke-super {p0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onDetachedFromWindow()V

    .line 2101127
    iget-object v1, p0, LX/EIC;->r:LX/EFy;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/EIC;->n:LX/EGE;

    if-nez v1, :cond_1

    .line 2101128
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x6e1d0f0a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2101129
    :goto_0
    return-void

    .line 2101130
    :cond_1
    iget-object v1, p0, LX/EIC;->r:LX/EFy;

    iget-object v2, p0, LX/EIC;->n:LX/EGE;

    iget-wide v2, v2, LX/EGE;->h:J

    .line 2101131
    iget-object v4, v1, LX/EFy;->a:LX/EFw;

    iget-object v5, v1, LX/EFy;->b:LX/EG5;

    .line 2101132
    iget-boolean v6, v4, LX/EFw;->d:Z

    if-eqz v6, :cond_3

    .line 2101133
    sget-object v6, LX/EFw;->a:Ljava/lang/String;

    const-string v7, "Unable to clear Renderer window because conference call has ended"

    invoke-static {v6, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2101134
    :cond_2
    :goto_1
    const v1, -0x5026c567

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0

    .line 2101135
    :cond_3
    const-string v6, "clearRendererWindow"

    invoke-virtual {v4, v5, v6}, LX/EFw;->a(LX/EG5;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2101136
    iget-object v6, v4, LX/EFw;->j:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 p0, 0x0

    invoke-interface {v6, v7, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public setZOrderOnTop(Z)V
    .locals 1

    .prologue
    .line 2101124
    iget-object v0, p0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v0, p1}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 2101125
    return-void
.end method
