.class public LX/Ctq;
.super LX/2oy;
.source ""


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/resources/ui/FbImageButton;

.field private final c:LX/Cto;

.field public d:Z

.field public e:Z

.field public f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/richdocument/view/widget/media/plugins/AudioPlugin$AudioPluginClickListener;",
            ">;"
        }
    .end annotation
.end field

.field public n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1945727
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Ctq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1945728
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1945709
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Ctq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945710
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1945711
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945712
    new-instance v0, LX/Cto;

    invoke-direct {v0, p0}, LX/Cto;-><init>(LX/Ctq;)V

    iput-object v0, p0, LX/Ctq;->c:LX/Cto;

    .line 1945713
    const-class v0, LX/Ctq;

    invoke-static {v0, p0}, LX/Ctq;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1945714
    const v0, 0x7f0311fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1945715
    const v0, 0x7f0d2a33

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbImageButton;

    iput-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    .line 1945716
    iget-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    const v1, 0x7f0200e5

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setImageResource(I)V

    .line 1945717
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ctq;->n:Z

    .line 1945718
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Ctp;

    invoke-direct {v1, p0}, LX/Ctp;-><init>(LX/Ctq;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1945719
    iget-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1945720
    iget-object v1, p0, LX/Ctq;->a:LX/Cju;

    const v2, 0x7f0d0169

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1945721
    iget-object v1, p0, LX/Ctq;->a:LX/Cju;

    const v2, 0x7f0d016a

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1945722
    iget-object v1, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1945723
    iget-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    iget-object v1, p0, LX/Ctq;->c:LX/Cto;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1945724
    iget-object v0, p0, LX/Ctq;->a:LX/Cju;

    const v1, 0x7f0d016c

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    .line 1945725
    iget-object v1, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1945726
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ctq;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object p0

    check-cast p0, LX/Cju;

    iput-object p0, p1, LX/Ctq;->a:LX/Cju;

    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1945729
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1945730
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1945731
    iget-object p1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, p1

    .line 1945732
    invoke-virtual {p0, v0}, LX/Ctq;->a(LX/2qV;)V

    .line 1945733
    return-void
.end method

.method public final a(LX/2qV;)V
    .locals 1

    .prologue
    .line 1945686
    sget-object v0, LX/2qV;->PLAYING:LX/2qV;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, LX/Ctq;->d:Z

    if-nez v0, :cond_0

    .line 1945687
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Ctq;->a(Z)V

    .line 1945688
    :goto_0
    return-void

    .line 1945689
    :cond_0
    iget-boolean v0, p0, LX/Ctq;->d:Z

    if-nez v0, :cond_1

    sget-object v0, LX/2qV;->PAUSED:LX/2qV;

    if-ne p1, v0, :cond_1

    .line 1945690
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Ctq;->a(Z)V

    goto :goto_0

    .line 1945691
    :cond_1
    invoke-virtual {p0}, LX/Ctq;->h()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1945692
    iget-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    if-eqz v0, :cond_0

    .line 1945693
    iget-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setVisibility(I)V

    .line 1945694
    if-eqz p1, :cond_0

    .line 1945695
    invoke-virtual {p0}, LX/Ctq;->g()V

    .line 1945696
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1945697
    iget-boolean v0, p0, LX/Ctq;->e:Z

    if-eqz v0, :cond_0

    .line 1945698
    :goto_0
    return-void

    .line 1945699
    :cond_0
    iget-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 1945700
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ctq;->e:Z

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1945701
    iget-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    if-eqz v0, :cond_0

    .line 1945702
    iget-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setVisibility(I)V

    .line 1945703
    iget-boolean v0, p0, LX/Ctq;->e:Z

    if-nez v0, :cond_1

    .line 1945704
    :cond_0
    :goto_0
    return-void

    .line 1945705
    :cond_1
    iget-object v0, p0, LX/Ctq;->b:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1945706
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_0

    .line 1945707
    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    .line 1945708
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ctq;->e:Z

    goto :goto_0
.end method
