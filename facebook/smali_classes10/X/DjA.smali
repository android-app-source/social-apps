.class public final LX/DjA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dj9;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;)V
    .locals 0

    .prologue
    .line 2032897
    iput-object p1, p0, LX/DjA;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 5

    .prologue
    .line 2032898
    iget-object v0, p0, LX/DjA;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, p0, LX/DjA;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-static {v1}, LX/Djn;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)LX/Djn;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->q()Ljava/lang/String;

    move-result-object v2

    .line 2032899
    iput-object v2, v1, LX/Djn;->j:Ljava/lang/String;

    .line 2032900
    move-object v1, v1

    .line 2032901
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->v()Ljava/lang/String;

    move-result-object v2

    .line 2032902
    iput-object v2, v1, LX/Djn;->i:Ljava/lang/String;

    .line 2032903
    move-object v1, v1

    .line 2032904
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->o()J

    move-result-wide v2

    .line 2032905
    iput-wide v2, v1, LX/Djn;->h:J

    .line 2032906
    move-object v1, v1

    .line 2032907
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->p()Ljava/lang/String;

    move-result-object v2

    .line 2032908
    iput-object v2, v1, LX/Djn;->f:Ljava/lang/String;

    .line 2032909
    move-object v1, v1

    .line 2032910
    invoke-virtual {v1}, LX/Djn;->a()Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    move-result-object v1

    .line 2032911
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    .line 2032912
    iget-object v0, p0, LX/DjA;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    iget-object v1, p0, LX/DjA;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->p:Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;->a(Lcom/facebook/messaging/professionalservices/booking/model/CreateBookingAppointmentModel;)V

    .line 2032913
    iget-object v0, p0, LX/DjA;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/DjA;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/CreateBookingAppointmentFragment;->a:Lcom/facebook/messaging/professionalservices/booking/ui/CreateAppointmentAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2032914
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2032915
    return-void
.end method
