.class public final enum LX/Crd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Crd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Crd;

.field public static final enum AUTOPLAY:LX/Crd;

.field public static final enum BACK:LX/Crd;

.field public static final enum CLICK_MAP:LX/Crd;

.field public static final enum CLICK_MEDIA:LX/Crd;

.field public static final enum CONTROLLER_PAUSE:LX/Crd;

.field public static final enum SCROLL_FINISHED:LX/Crd;

.field public static final enum UNFOCUSED:LX/Crd;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1941276
    new-instance v0, LX/Crd;

    const-string v1, "CLICK_MEDIA"

    invoke-direct {v0, v1, v3}, LX/Crd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Crd;->CLICK_MEDIA:LX/Crd;

    .line 1941277
    new-instance v0, LX/Crd;

    const-string v1, "CLICK_MAP"

    invoke-direct {v0, v1, v4}, LX/Crd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Crd;->CLICK_MAP:LX/Crd;

    .line 1941278
    new-instance v0, LX/Crd;

    const-string v1, "SCROLL_FINISHED"

    invoke-direct {v0, v1, v5}, LX/Crd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Crd;->SCROLL_FINISHED:LX/Crd;

    .line 1941279
    new-instance v0, LX/Crd;

    const-string v1, "BACK"

    invoke-direct {v0, v1, v6}, LX/Crd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Crd;->BACK:LX/Crd;

    .line 1941280
    new-instance v0, LX/Crd;

    const-string v1, "UNFOCUSED"

    invoke-direct {v0, v1, v7}, LX/Crd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Crd;->UNFOCUSED:LX/Crd;

    .line 1941281
    new-instance v0, LX/Crd;

    const-string v1, "CONTROLLER_PAUSE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Crd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Crd;->CONTROLLER_PAUSE:LX/Crd;

    .line 1941282
    new-instance v0, LX/Crd;

    const-string v1, "AUTOPLAY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Crd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Crd;->AUTOPLAY:LX/Crd;

    .line 1941283
    const/4 v0, 0x7

    new-array v0, v0, [LX/Crd;

    sget-object v1, LX/Crd;->CLICK_MEDIA:LX/Crd;

    aput-object v1, v0, v3

    sget-object v1, LX/Crd;->CLICK_MAP:LX/Crd;

    aput-object v1, v0, v4

    sget-object v1, LX/Crd;->SCROLL_FINISHED:LX/Crd;

    aput-object v1, v0, v5

    sget-object v1, LX/Crd;->BACK:LX/Crd;

    aput-object v1, v0, v6

    sget-object v1, LX/Crd;->UNFOCUSED:LX/Crd;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Crd;->CONTROLLER_PAUSE:LX/Crd;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Crd;->AUTOPLAY:LX/Crd;

    aput-object v2, v0, v1

    sput-object v0, LX/Crd;->$VALUES:[LX/Crd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1941273
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Crd;
    .locals 1

    .prologue
    .line 1941275
    const-class v0, LX/Crd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Crd;

    return-object v0
.end method

.method public static values()[LX/Crd;
    .locals 1

    .prologue
    .line 1941274
    sget-object v0, LX/Crd;->$VALUES:[LX/Crd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Crd;

    return-object v0
.end method
