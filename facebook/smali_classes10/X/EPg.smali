.class public final LX/EPg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D6L;


# instance fields
.field public final synthetic a:LX/1Pn;

.field public final synthetic b:LX/CzL;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLActor;

.field public final synthetic d:Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;LX/1Pn;LX/CzL;Lcom/facebook/graphql/model/GraphQLActor;)V
    .locals 0

    .prologue
    .line 2117368
    iput-object p1, p0, LX/EPg;->d:Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;

    iput-object p2, p0, LX/EPg;->a:LX/1Pn;

    iput-object p3, p0, LX/EPg;->b:LX/CzL;

    iput-object p4, p0, LX/EPg;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    .line 2117369
    iget-object v0, p0, LX/EPg;->d:Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/CvY;

    iget-object v0, p0, LX/EPg;->a:LX/1Pn;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    sget-object v9, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v10, LX/7CM;->OPEN_CHANNEL_FEED:LX/7CM;

    iget-object v0, p0, LX/EPg;->a:LX/1Pn;

    check-cast v0, LX/CxP;

    iget-object v1, p0, LX/EPg;->b:LX/CzL;

    .line 2117370
    iget-object v2, v1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v2

    .line 2117371
    invoke-interface {v0, v1}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v11

    iget-object v12, p0, LX/EPg;->b:LX/CzL;

    iget-object v0, p0, LX/EPg;->d:Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsLiveVideoPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/EPg;->a:LX/1Pn;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    sget-object v1, LX/8ce;->NAVIGATION:LX/8ce;

    sget-object v2, LX/7CM;->OPEN_CHANNEL_FEED:LX/7CM;

    iget-object v3, p0, LX/EPg;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/EPg;->b:LX/CzL;

    invoke-virtual {v4}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    iget-object v5, p0, LX/EPg;->c:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v0, v7

    move-object v1, v8

    move-object v2, v9

    move-object v3, v10

    move v4, v11

    move-object v5, v12

    invoke-virtual/range {v0 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ce;LX/7CM;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2117372
    return-void
.end method
