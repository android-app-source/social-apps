.class public abstract LX/ENE;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public k:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2112361
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2112362
    invoke-virtual {p0, p4}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2112363
    const p1, 0x7f0d0c4e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, LX/ENE;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2112364
    const p1, 0x7f0d0c4f

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/ENE;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2112365
    return-void
.end method


# virtual methods
.method public setActorClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2112366
    iget-object v0, p0, LX/ENE;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2112367
    return-void
.end method

.method public setBodyClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2112358
    invoke-virtual {p0, p1}, LX/ENE;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2112359
    iget-object v0, p0, LX/ENE;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2112360
    return-void
.end method

.method public setPostBodyText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2112356
    iget-object v0, p0, LX/ENE;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2112357
    return-void
.end method
