.class public final LX/Eu1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Eu0;


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

.field public final synthetic b:Lcom/facebook/friending/center/components/FriendItemComponentSpec;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/components/FriendItemComponentSpec;Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)V
    .locals 0

    .prologue
    .line 2178916
    iput-object p1, p0, LX/Eu1;->b:Lcom/facebook/friending/center/components/FriendItemComponentSpec;

    iput-object p2, p0, LX/Eu1;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 2178917
    iget-object v0, p0, LX/Eu1;->b:Lcom/facebook/friending/center/components/FriendItemComponentSpec;

    iget-object v0, v0, Lcom/facebook/friending/center/components/FriendItemComponentSpec;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iT;

    iget-object v0, p0, LX/Eu1;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v0, p0, LX/Eu1;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->o()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/2h7;->FRIENDS_CENTER_FRIENDS:LX/2h7;

    iget-object v0, p0, LX/Eu1;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/2iT;->a(JLjava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2178918
    return-void
.end method
