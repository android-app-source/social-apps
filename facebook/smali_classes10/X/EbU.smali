.class public final LX/EbU;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EbS;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EbU;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EbU;


# instance fields
.field public bitField0_:I

.field public ciphertext_:LX/EWc;

.field public counter_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public previousCounter_:I

.field public ratchetKey_:LX/EWc;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2144026
    new-instance v0, LX/EbR;

    invoke-direct {v0}, LX/EbR;-><init>()V

    sput-object v0, LX/EbU;->a:LX/EWZ;

    .line 2144027
    new-instance v0, LX/EbU;

    invoke-direct {v0}, LX/EbU;-><init>()V

    .line 2144028
    sput-object v0, LX/EbU;->c:LX/EbU;

    invoke-direct {v0}, LX/EbU;->y()V

    .line 2144029
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2144021
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2144022
    iput-byte v0, p0, LX/EbU;->memoizedIsInitialized:B

    .line 2144023
    iput v0, p0, LX/EbU;->memoizedSerializedSize:I

    .line 2144024
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2144025
    iput-object v0, p0, LX/EbU;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2143987
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2143988
    iput-byte v0, p0, LX/EbU;->memoizedIsInitialized:B

    .line 2143989
    iput v0, p0, LX/EbU;->memoizedSerializedSize:I

    .line 2143990
    invoke-direct {p0}, LX/EbU;->y()V

    .line 2143991
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2143992
    const/4 v0, 0x0

    .line 2143993
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2143994
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2143995
    sparse-switch v3, :sswitch_data_0

    .line 2143996
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2143997
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2143998
    goto :goto_0

    .line 2143999
    :sswitch_1
    iget v3, p0, LX/EbU;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EbU;->bitField0_:I

    .line 2144000
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EbU;->ratchetKey_:LX/EWc;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2144001
    :catch_0
    move-exception v0

    .line 2144002
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2144003
    move-object v0, v0

    .line 2144004
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2144005
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EbU;->unknownFields:LX/EZQ;

    .line 2144006
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2144007
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EbU;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EbU;->bitField0_:I

    .line 2144008
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EbU;->counter_:I
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2144009
    :catch_1
    move-exception v0

    .line 2144010
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2144011
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2144012
    move-object v0, v1

    .line 2144013
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2144014
    :sswitch_3
    :try_start_4
    iget v3, p0, LX/EbU;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/EbU;->bitField0_:I

    .line 2144015
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EbU;->previousCounter_:I

    goto :goto_0

    .line 2144016
    :sswitch_4
    iget v3, p0, LX/EbU;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, LX/EbU;->bitField0_:I

    .line 2144017
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EbU;->ciphertext_:LX/EWc;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2144018
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EbU;->unknownFields:LX/EZQ;

    .line 2144019
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2144020
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2143982
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2143983
    iput-byte v1, p0, LX/EbU;->memoizedIsInitialized:B

    .line 2143984
    iput v1, p0, LX/EbU;->memoizedSerializedSize:I

    .line 2143985
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EbU;->unknownFields:LX/EZQ;

    .line 2143986
    return-void
.end method

.method private static a(LX/EbU;)LX/EbT;
    .locals 1

    .prologue
    .line 2143981
    invoke-static {}, LX/EbT;->u()LX/EbT;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EbT;->a(LX/EbU;)LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method private y()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2143976
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbU;->ratchetKey_:LX/EWc;

    .line 2143977
    iput v1, p0, LX/EbU;->counter_:I

    .line 2143978
    iput v1, p0, LX/EbU;->previousCounter_:I

    .line 2143979
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EbU;->ciphertext_:LX/EWc;

    .line 2143980
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2143974
    new-instance v0, LX/EbT;

    invoke-direct {v0, p1}, LX/EbT;-><init>(LX/EYd;)V

    .line 2143975
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2143963
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2143964
    iget v0, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2143965
    iget-object v0, p0, LX/EbU;->ratchetKey_:LX/EWc;

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2143966
    :cond_0
    iget v0, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2143967
    iget v0, p0, LX/EbU;->counter_:I

    invoke-virtual {p1, v2, v0}, LX/EWf;->c(II)V

    .line 2143968
    :cond_1
    iget v0, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2143969
    const/4 v0, 0x3

    iget v1, p0, LX/EbU;->previousCounter_:I

    invoke-virtual {p1, v0, v1}, LX/EWf;->c(II)V

    .line 2143970
    :cond_2
    iget v0, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2143971
    iget-object v0, p0, LX/EbU;->ciphertext_:LX/EWc;

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2143972
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2143973
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2143958
    iget-byte v1, p0, LX/EbU;->memoizedIsInitialized:B

    .line 2143959
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2143960
    :goto_0
    return v0

    .line 2143961
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2143962
    :cond_1
    iput-byte v0, p0, LX/EbU;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2143944
    iget v0, p0, LX/EbU;->memoizedSerializedSize:I

    .line 2143945
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2143946
    :goto_0
    return v0

    .line 2143947
    :cond_0
    const/4 v0, 0x0

    .line 2143948
    iget v1, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2143949
    iget-object v0, p0, LX/EbU;->ratchetKey_:LX/EWc;

    invoke-static {v2, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2143950
    :cond_1
    iget v1, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2143951
    iget v1, p0, LX/EbU;->counter_:I

    invoke-static {v3, v1}, LX/EWf;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143952
    :cond_2
    iget v1, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2143953
    const/4 v1, 0x3

    iget v2, p0, LX/EbU;->previousCounter_:I

    invoke-static {v1, v2}, LX/EWf;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143954
    :cond_3
    iget v1, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2143955
    iget-object v1, p0, LX/EbU;->ciphertext_:LX/EWc;

    invoke-static {v4, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2143956
    :cond_4
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2143957
    iput v0, p0, LX/EbU;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2143943
    iget-object v0, p0, LX/EbU;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2143934
    sget-object v0, LX/EbV;->b:LX/EYn;

    const-class v1, LX/EbU;

    const-class v2, LX/EbT;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EbU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2143935
    sget-object v0, LX/EbU;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2143936
    iget v1, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2143937
    iget v0, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 2143938
    iget v0, p0, LX/EbU;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2143939
    invoke-static {p0}, LX/EbU;->a(LX/EbU;)LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2143940
    invoke-static {}, LX/EbT;->u()LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2143941
    invoke-static {p0}, LX/EbU;->a(LX/EbU;)LX/EbT;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2143942
    sget-object v0, LX/EbU;->c:LX/EbU;

    return-object v0
.end method
