.class public final LX/DGP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DGQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/DGK;

.field public b:LX/DG8;

.field public c:LX/1Pd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/DGQ;


# direct methods
.method public constructor <init>(LX/DGQ;)V
    .locals 1

    .prologue
    .line 1979737
    iput-object p1, p0, LX/DGP;->d:LX/DGQ;

    .line 1979738
    move-object v0, p1

    .line 1979739
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1979740
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1979741
    const-string v0, "NetEgoStorySetSinglePageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1979742
    if-ne p0, p1, :cond_1

    .line 1979743
    :cond_0
    :goto_0
    return v0

    .line 1979744
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1979745
    goto :goto_0

    .line 1979746
    :cond_3
    check-cast p1, LX/DGP;

    .line 1979747
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1979748
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1979749
    if-eq v2, v3, :cond_0

    .line 1979750
    iget-object v2, p0, LX/DGP;->a:LX/DGK;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DGP;->a:LX/DGK;

    iget-object v3, p1, LX/DGP;->a:LX/DGK;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1979751
    goto :goto_0

    .line 1979752
    :cond_5
    iget-object v2, p1, LX/DGP;->a:LX/DGK;

    if-nez v2, :cond_4

    .line 1979753
    :cond_6
    iget-object v2, p0, LX/DGP;->b:LX/DG8;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DGP;->b:LX/DG8;

    iget-object v3, p1, LX/DGP;->b:LX/DG8;

    invoke-virtual {v2, v3}, LX/DG8;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1979754
    goto :goto_0

    .line 1979755
    :cond_8
    iget-object v2, p1, LX/DGP;->b:LX/DG8;

    if-nez v2, :cond_7

    .line 1979756
    :cond_9
    iget-object v2, p0, LX/DGP;->c:LX/1Pd;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/DGP;->c:LX/1Pd;

    iget-object v3, p1, LX/DGP;->c:LX/1Pd;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1979757
    goto :goto_0

    .line 1979758
    :cond_a
    iget-object v2, p1, LX/DGP;->c:LX/1Pd;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
