.class public final LX/DBv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/87n;


# instance fields
.field public final synthetic a:LX/DBx;


# direct methods
.method public constructor <init>(LX/DBx;)V
    .locals 0

    .prologue
    .line 1973513
    iput-object p1, p0, LX/DBv;->a:LX/DBx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;ILX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;I",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodfriends/data/FriendData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1973514
    iget-object v0, p0, LX/DBv;->a:LX/DBx;

    iget-boolean v0, v0, LX/DBx;->n:Z

    if-eqz v0, :cond_0

    .line 1973515
    :goto_0
    return-void

    .line 1973516
    :cond_0
    add-int/lit8 v0, p2, -0x4

    add-int/lit8 v0, v0, 0x1

    .line 1973517
    iget-object v1, p0, LX/DBv;->a:LX/DBx;

    iget-object v2, p0, LX/DBv;->a:LX/DBx;

    iget-object v2, v2, LX/DBx;->l:LX/0Or;

    .line 1973518
    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 1973519
    invoke-virtual {v3}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    const/4 v4, 0x0

    .line 1973520
    :goto_1
    new-instance p2, Lcom/facebook/goodfriends/data/FriendData;

    .line 1973521
    iget-object p3, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p3, p3

    .line 1973522
    invoke-virtual {v3}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p2, p3, v3, v4}, Lcom/facebook/goodfriends/data/FriendData;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object v2, p2

    .line 1973523
    iput-object v2, v1, LX/DBx;->m:Lcom/facebook/goodfriends/data/FriendData;

    .line 1973524
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1973525
    iget-object v2, p0, LX/DBv;->a:LX/DBx;

    iget-object v2, v2, LX/DBx;->m:Lcom/facebook/goodfriends/data/FriendData;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1973526
    invoke-virtual {v1, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1973527
    iget-object v2, p0, LX/DBv;->a:LX/DBx;

    new-instance v3, LX/DBw;

    iget-object v4, p0, LX/DBv;->a:LX/DBx;

    iget-object v4, v4, LX/DBx;->m:Lcom/facebook/goodfriends/data/FriendData;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v3, v4, v1, v0}, LX/DBw;-><init>(Lcom/facebook/goodfriends/data/FriendData;LX/0Px;I)V

    .line 1973528
    iput-object v3, v2, LX/DBx;->g:LX/DBw;

    .line 1973529
    iget-object v0, p0, LX/DBv;->a:LX/DBx;

    const v1, -0x3c9ccc51

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 1973530
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1973531
    sget-object v0, LX/DBx;->a:Ljava/lang/String;

    const-string v1, "Fail to retrieve good friends"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1973532
    iget-object v0, p0, LX/DBv;->a:LX/DBx;

    iget-object v0, v0, LX/DBx;->c:LX/03V;

    const-string v1, "FailToRetrieveGoodFriends"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1973533
    return-void
.end method
