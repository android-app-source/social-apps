.class public final LX/Efk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AKt;


# instance fields
.field public final synthetic a:LX/Efo;


# direct methods
.method public constructor <init>(LX/Efo;)V
    .locals 0

    .prologue
    .line 2155267
    iput-object p1, p0, LX/Efk;->a:LX/Efo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2155268
    iget-object v0, p0, LX/Efk;->a:LX/Efo;

    .line 2155269
    invoke-virtual {v0}, LX/Efo;->f()V

    .line 2155270
    iget-object v0, p0, LX/Efk;->a:LX/Efo;

    iget-object v0, v0, LX/Efo;->m:LX/Efs;

    if-eqz v0, :cond_0

    .line 2155271
    iget-object v0, p0, LX/Efk;->a:LX/Efo;

    iget-object v0, v0, LX/Efo;->m:LX/Efs;

    invoke-virtual {v0}, LX/Efs;->a()V

    .line 2155272
    iget-object v0, p0, LX/Efk;->a:LX/Efo;

    iget-object v0, v0, LX/Efo;->m:LX/Efs;

    const/4 v1, 0x0

    const v2, 0x7f04002f

    .line 2155273
    iget-object p0, v0, LX/Efs;->a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->overridePendingTransition(II)V

    .line 2155274
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 2155275
    iget-object v0, p0, LX/Efk;->a:LX/Efo;

    invoke-virtual {v0}, LX/Efo;->c()V

    .line 2155276
    iget-object v1, p0, LX/Efk;->a:LX/Efo;

    iget-object v1, v1, LX/Efo;->l:Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;

    invoke-virtual {v1}, Lcom/facebook/audience/snacks/view/bucket/BucketOverlayGestureView;->getWidth()I

    move-result v1

    .line 2155277
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    .line 2155278
    div-int/lit8 v0, v1, 0x3

    if-ge v2, v0, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2155279
    move v0, v2

    .line 2155280
    if-eqz v0, :cond_1

    .line 2155281
    iget-object v0, p0, LX/Efk;->a:LX/Efo;

    iget-object v0, v0, LX/Efo;->h:LX/Efj;

    .line 2155282
    iget v1, v0, LX/Efj;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/Efj;->b:I

    .line 2155283
    iget v1, v0, LX/Efj;->b:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    .line 2155284
    iget v1, v0, LX/Efj;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/Efj;->b:I

    .line 2155285
    iget-object v1, v0, LX/Efj;->c:LX/Efl;

    .line 2155286
    iget-object v2, v1, LX/Efl;->a:LX/Efo;

    iget-object v2, v2, LX/Efo;->m:LX/Efs;

    .line 2155287
    iget-object v0, v2, LX/Efs;->a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    iget-object v0, v0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2155288
    if-ltz v0, :cond_0

    .line 2155289
    iget-object v1, v2, LX/Efs;->a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    iget-object v1, v1, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2155290
    :cond_0
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 2155291
    :cond_1
    iget-object v0, p0, LX/Efk;->a:LX/Efo;

    iget-object v0, v0, LX/Efo;->h:LX/Efj;

    .line 2155292
    iget v1, v0, LX/Efj;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/Efj;->b:I

    .line 2155293
    iget v1, v0, LX/Efj;->b:I

    iget v2, v0, LX/Efj;->a:I

    if-ne v1, v2, :cond_5

    .line 2155294
    iget v1, v0, LX/Efj;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, LX/Efj;->b:I

    .line 2155295
    iget-object v1, v0, LX/Efj;->c:LX/Efl;

    .line 2155296
    iget-object v2, v1, LX/Efl;->a:LX/Efo;

    iget-object v2, v2, LX/Efo;->m:LX/Efs;

    .line 2155297
    iget-object v0, v2, LX/Efs;->a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    iget-object v0, v0, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2155298
    iget-object v1, v2, LX/Efs;->a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    iget-object v1, v1, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->g:LX/Efq;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2155299
    iget-object v1, v2, LX/Efs;->a:Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;

    iget-object v1, v1, Lcom/facebook/audience/snacks/view/bucket/SnacksBucketFragment;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2155300
    :cond_2
    :goto_2
    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 2155301
    :cond_4
    iget-object v1, v0, LX/Efj;->c:LX/Efl;

    iget v2, v0, LX/Efj;->b:I

    invoke-virtual {v1, v2}, LX/Efl;->a(I)V

    goto :goto_1

    .line 2155302
    :cond_5
    iget-object v1, v0, LX/Efj;->c:LX/Efl;

    iget v2, v0, LX/Efj;->b:I

    invoke-virtual {v1, v2}, LX/Efl;->a(I)V

    goto :goto_2
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 2155303
    iget-object v0, p0, LX/Efk;->a:LX/Efo;

    invoke-virtual {v0, p1}, LX/Efo;->a(Landroid/view/MotionEvent;)V

    .line 2155304
    return-void
.end method
