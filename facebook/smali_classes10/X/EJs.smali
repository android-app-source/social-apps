.class public final LX/EJs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

.field public final synthetic b:LX/1Ps;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Ps;)V
    .locals 0

    .prologue
    .line 2105546
    iput-object p1, p0, LX/EJs;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    iput-object p2, p0, LX/EJs;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    iput-object p3, p0, LX/EJs;->b:LX/1Ps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x55c92af1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2105547
    iget-object v0, p0, LX/EJs;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105548
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v2

    .line 2105549
    iget-object v2, p0, LX/EJs;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105550
    iget-object v3, v2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v2, v3

    .line 2105551
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v2, v3, :cond_1

    .line 2105552
    iget-object v2, p0, LX/EJs;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->d:LX/7j6;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    sget-object v3, LX/7iP;->GLOBAL_SEARCH:LX/7iP;

    invoke-virtual {v2, v0, v3}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2105553
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EJs;->b:LX/1Ps;

    check-cast v0, LX/Cxh;

    iget-object v2, p0, LX/EJs;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-interface {v0, v2}, LX/Cxh;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)V

    .line 2105554
    const v0, -0x2bbc7d46

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2105555
    :cond_1
    iget-object v2, p0, LX/EJs;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105556
    iget-object v3, v2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v2, v3

    .line 2105557
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v2, v3, :cond_0

    .line 2105558
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gh()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2105559
    new-instance v2, LX/89k;

    invoke-direct {v2}, LX/89k;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    .line 2105560
    iput-object v3, v2, LX/89k;->b:Ljava/lang/String;

    .line 2105561
    move-object v2, v2

    .line 2105562
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 2105563
    iput-object v0, v2, LX/89k;->c:Ljava/lang/String;

    .line 2105564
    move-object v0, v2

    .line 2105565
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 2105566
    iget-object v2, p0, LX/EJs;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->e:LX/0hy;

    invoke-interface {v2, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v2

    .line 2105567
    if-eqz v2, :cond_0

    .line 2105568
    iget-object v0, p0, LX/EJs;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    iget-object v3, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/EJs;->b:LX/1Ps;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
