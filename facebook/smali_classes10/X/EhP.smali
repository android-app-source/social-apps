.class public final LX/EhP;
.super LX/2lH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2lH",
        "<",
        "LX/EhU;",
        "Lcom/facebook/bookmark/model/BookmarksGroup;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Lcom/facebook/bookmark/model/BookmarksGroup;Z)V
    .locals 2
    .param p3    # Lcom/facebook/bookmark/model/BookmarksGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2158396
    iput-object p1, p0, LX/EhP;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    .line 2158397
    const v0, 0x7f03024c

    iget-object v1, p1, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b:Landroid/view/LayoutInflater;

    invoke-direct {p0, p2, v0, p3, v1}, LX/2lH;-><init>(LX/2lb;ILjava/lang/Object;Landroid/view/LayoutInflater;)V

    .line 2158398
    iput-boolean p4, p0, LX/EhP;->f:Z

    .line 2158399
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2158400
    invoke-super {p0, p1, p2, p3}, LX/2lH;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;

    .line 2158401
    iget-boolean v1, p0, LX/EhP;->f:Z

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->setExtraPaddingEnabled(Z)V

    .line 2158402
    return-object v0
.end method

.method public final a(Landroid/view/View;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2158403
    new-instance v0, LX/EhU;

    invoke-direct {v0, p1}, LX/EhU;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2158404
    check-cast p1, LX/EhU;

    .line 2158405
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 2158406
    iget-object v1, p1, LX/EhU;->a:Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v0, v0, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->setText(Ljava/lang/CharSequence;)V

    .line 2158407
    :goto_0
    return-void

    .line 2158408
    :cond_0
    iget-object v0, p1, LX/EhU;->a:Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/bookmarks/BookmarkDividerView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
