.class public final LX/D44;
.super LX/7Ji;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;


# direct methods
.method public constructor <init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V
    .locals 0

    .prologue
    .line 1961532
    iput-object p1, p0, LX/D44;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-direct {p0}, LX/7Ji;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;B)V
    .locals 0

    .prologue
    .line 1961533
    invoke-direct {p0, p1}, LX/D44;-><init>(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1961534
    iget-object v0, p0, LX/D44;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->u:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1961535
    return-void
.end method

.method public final a(LX/2qD;)V
    .locals 2

    .prologue
    .line 1961536
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSetPlayerCurrentState__"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1961537
    iget-object v0, p0, LX/D44;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->z:LX/2qD;

    if-ne v0, p1, :cond_0

    .line 1961538
    :goto_0
    return-void

    .line 1961539
    :cond_0
    iget-object v0, p0, LX/D44;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    .line 1961540
    iput-object p1, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->z:LX/2qD;

    .line 1961541
    sget-object v0, LX/D43;->a:[I

    invoke-virtual {p1}, LX/2qD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1961542
    :goto_1
    iget-object v0, p0, LX/D44;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    invoke-static {v0}, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->f(Lcom/facebook/video/backgroundplay/control/ControlNotificationService;)V

    goto :goto_0

    .line 1961543
    :pswitch_0
    iget-object v0, p0, LX/D44;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->u:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 1961544
    :pswitch_1
    iget-object v0, p0, LX/D44;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->u:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 1961545
    :pswitch_2
    iget-object v0, p0, LX/D44;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->u:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1961546
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1961547
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1961548
    iget-object v0, p0, LX/D44;->a:Lcom/facebook/video/backgroundplay/control/ControlNotificationService;

    iget-object v0, v0, Lcom/facebook/video/backgroundplay/control/ControlNotificationService;->u:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1961549
    return-void
.end method
