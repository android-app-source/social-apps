.class public final LX/EJq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2105402
    iput-object p1, p0, LX/EJq;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    iput-object p2, p0, LX/EJq;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    iput-object p3, p0, LX/EJq;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x1b188756

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2105403
    iget-object v0, p0, LX/EJq;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105404
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v2

    .line 2105405
    iget-object v2, p0, LX/EJq;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105406
    iget-object v3, v2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v2, v3

    .line 2105407
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v2, v3, :cond_1

    .line 2105408
    iget-object v2, p0, LX/EJq;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->b:LX/7j6;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    sget-object v3, LX/7iP;->GLOBAL_SEARCH:LX/7iP;

    invoke-virtual {v2, v0, v3}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2105409
    :cond_0
    :goto_0
    iget-object v0, p0, LX/EJq;->b:LX/1Pn;

    check-cast v0, LX/Cxh;

    iget-object v2, p0, LX/EJq;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-interface {v0, v2}, LX/Cxh;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)V

    .line 2105410
    const v0, -0x21e63cd2

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2105411
    :cond_1
    iget-object v2, p0, LX/EJq;->a:Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105412
    iget-object v3, v2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v2, v3

    .line 2105413
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v2, v3, :cond_0

    .line 2105414
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gh()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2105415
    new-instance v2, LX/89k;

    invoke-direct {v2}, LX/89k;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    .line 2105416
    iput-object v3, v2, LX/89k;->b:Ljava/lang/String;

    .line 2105417
    move-object v2, v2

    .line 2105418
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 2105419
    iput-object v0, v2, LX/89k;->c:Ljava/lang/String;

    .line 2105420
    move-object v0, v2

    .line 2105421
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 2105422
    iget-object v2, p0, LX/EJq;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->c:LX/0hy;

    invoke-interface {v2, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2105423
    if-eqz v0, :cond_0

    .line 2105424
    iget-object v2, p0, LX/EJq;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/EJq;->b:LX/1Pn;

    invoke-interface {v3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
