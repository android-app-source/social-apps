.class public final LX/DQ2;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/Db3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

.field public final synthetic b:Lcom/facebook/groups/info/GroupInfoAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V
    .locals 0

    .prologue
    .line 1994023
    iput-object p1, p0, LX/DQ2;->b:Lcom/facebook/groups/info/GroupInfoAdapter;

    iput-object p3, p0, LX/DQ2;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1994024
    check-cast p1, LX/Db3;

    .line 1994025
    iget-object v0, p0, LX/DQ2;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1994026
    invoke-interface {v0}, LX/DQn;->kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    if-ne v1, v2, :cond_0

    .line 1994027
    sget-object v1, LX/Db2;->a:[I

    invoke-interface {v0}, LX/DQn;->j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1994028
    iget-object v1, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/Db3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082fe2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1994029
    :goto_0
    iget-object v1, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/Db3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a05ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1994030
    :goto_1
    iget-object v0, p0, LX/DQ2;->b:Lcom/facebook/groups/info/GroupInfoAdapter;

    sget-object v1, LX/DQO;->EDIT_NOTIFICATION_SETTINGS:LX/DQO;

    iget-object v2, p0, LX/DQ2;->a:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v0, v1, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a$redex0(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/Db3;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1994031
    return-void

    .line 1994032
    :pswitch_0
    iget-object v1, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/Db3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082fe2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1994033
    :pswitch_1
    iget-object v1, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/Db3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082fe0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1994034
    :pswitch_2
    iget-object v1, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/Db3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082fdf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1994035
    :pswitch_3
    iget-object v1, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/Db3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082fde

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1994036
    :cond_0
    iget-object v1, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/Db3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082fe1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1994037
    iget-object v1, p1, LX/Db3;->a:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/Db3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a05ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
