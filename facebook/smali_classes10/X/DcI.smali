.class public LX/DcI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dc9;


# static fields
.field public static final a:I


# instance fields
.field private final b:LX/Dc5;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/17Y;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2018144
    sget-object v0, LX/DcG;->PHOTO_MENU:LX/DcG;

    invoke-virtual {v0}, LX/DcG;->ordinal()I

    move-result v0

    sput v0, LX/DcI;->a:I

    return-void
.end method

.method public constructor <init>(LX/Dc5;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018178
    iput-object p1, p0, LX/DcI;->b:LX/Dc5;

    .line 2018179
    iput-object p2, p0, LX/DcI;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2018180
    iput-object p3, p0, LX/DcI;->d:LX/17Y;

    .line 2018181
    return-void
.end method


# virtual methods
.method public final a()LX/DcG;
    .locals 1

    .prologue
    .line 2018176
    sget-object v0, LX/DcG;->PHOTO_MENU:LX/DcG;

    return-object v0
.end method

.method public final a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V
    .locals 5
    .param p4    # Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2018151
    if-eqz p4, :cond_3

    .line 2018152
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2018153
    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 2018154
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2018155
    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    move v3, v0

    .line 2018156
    :goto_1
    if-eqz p4, :cond_6

    .line 2018157
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2018158
    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 2018159
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2018160
    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    if-eqz v0, :cond_0

    .line 2018161
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2018162
    invoke-virtual {v4, v0, v1}, LX/15i;->h(II)Z

    move-result v2

    .line 2018163
    :cond_0
    if-nez v3, :cond_1

    if-eqz v2, :cond_9

    .line 2018164
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2018165
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2018166
    const-string v1, "extra_menu_type"

    sget-object v2, LX/Dc2;->PHOTO_MENU:LX/Dc2;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2018167
    iget-object v1, p0, LX/DcI;->c:Lcom/facebook/content/SecureContextHelper;

    sget v2, LX/DcI;->a:I

    invoke-interface {v1, v0, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2018168
    :goto_4
    return-void

    :cond_2
    move v0, v2

    .line 2018169
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v3, v2

    goto :goto_1

    :cond_5
    move v0, v2

    .line 2018170
    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_3

    .line 2018171
    :cond_9
    sget-object v0, LX/0ax;->eA:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2018172
    iget-object v1, p0, LX/DcI;->d:LX/17Y;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2018173
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2018174
    iget-object v1, p0, LX/DcI;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2018175
    goto :goto_4
.end method

.method public final a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 2018148
    sget v0, LX/DcI;->a:I

    if-ne p3, v0, :cond_0

    const/4 v0, -0x1

    if-ne p4, v0, :cond_0

    .line 2018149
    iget-object v0, p0, LX/DcI;->b:LX/Dc5;

    const-string v1, "photo"

    invoke-virtual {v0, p1, p2, v1}, LX/Dc5;->a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2018150
    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 2018147
    sget v0, LX/DcI;->a:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/15i;I)Z
    .locals 2
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "isVisibleMenu"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2018146
    if-eqz p2, :cond_0

    invoke-virtual {p1, p2, v0}, LX/15i;->h(II)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Z
    .locals 1
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "shouldShowInManagementScreen"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2018145
    const/4 v0, 0x1

    return v0
.end method
