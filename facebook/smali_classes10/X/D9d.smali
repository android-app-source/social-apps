.class public final LX/D9d;
.super Landroid/view/View;
.source ""


# instance fields
.field public final synthetic a:LX/D9e;

.field private final b:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(LX/D9e;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1970351
    iput-object p1, p0, LX/D9d;->a:LX/D9e;

    .line 1970352
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1970353
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/D9d;->b:Landroid/graphics/Rect;

    .line 1970354
    return-void
.end method


# virtual methods
.method public final fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 1970355
    iget-object v0, p0, LX/D9d;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1970356
    iget-object v0, p0, LX/D9d;->a:LX/D9e;

    iget-object v1, p0, LX/D9d;->b:Landroid/graphics/Rect;

    .line 1970357
    iget-object p0, v0, LX/D9e;->a:Ljava/util/Set;

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/D9b;

    .line 1970358
    iget-object v0, p0, LX/D9b;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1970359
    iget-object v0, p0, LX/D9b;->b:LX/D9a;

    if-eqz v0, :cond_0

    .line 1970360
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 1970361
    :cond_0
    goto :goto_0

    .line 1970362
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
