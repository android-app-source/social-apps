.class public LX/EPX;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPa;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EPX",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EPa;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117050
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2117051
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/EPX;->b:LX/0Zi;

    .line 2117052
    iput-object p1, p0, LX/EPX;->a:LX/0Ot;

    .line 2117053
    return-void
.end method

.method public static a(LX/0QB;)LX/EPX;
    .locals 4

    .prologue
    .line 2117054
    const-class v1, LX/EPX;

    monitor-enter v1

    .line 2117055
    :try_start_0
    sget-object v0, LX/EPX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117056
    sput-object v2, LX/EPX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117057
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117058
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117059
    new-instance v3, LX/EPX;

    const/16 p0, 0x3499

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EPX;-><init>(LX/0Ot;)V

    .line 2117060
    move-object v0, v3

    .line 2117061
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117062
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EPX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117063
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117064
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2117039
    check-cast p2, LX/EPW;

    .line 2117040
    iget-object v0, p0, LX/EPX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPa;

    iget-object v1, p2, LX/EPW;->a:LX/CzL;

    iget-object v2, p2, LX/EPW;->b:LX/CxV;

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2117041
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x7

    const v5, 0x7f0b0094

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x6

    const v5, 0x7f0b0060

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a009a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-interface {v3, v4}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0e094b

    invoke-static {p1, v6, v4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2117042
    const v9, 0x7f0822a7

    const/4 v8, 0x1

    new-array v10, v8, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-interface {v2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v8

    invoke-interface {v8}, LX/8ef;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v8

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->SHOWING:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-ne v8, p2, :cond_0

    .line 2117043
    iget-object v8, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v8, v8

    .line 2117044
    check-cast v8, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-static {v8}, LX/EPa;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Ljava/lang/String;

    move-result-object v8

    :goto_0
    aput-object v8, v10, p0

    invoke-virtual {v5, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    move-object v5, v8

    .line 2117045
    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0e012d

    invoke-static {p1, v6, v4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v4

    invoke-static {v0, p1, v1, v2}, LX/EPa;->b(LX/EPa;LX/1De;LX/CzL;LX/CxV;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a009c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 2117046
    iget-object v6, v4, LX/1ne;->a:LX/1nb;

    iput v5, v6, LX/1nb;->z:I

    .line 2117047
    move-object v4, v4

    .line 2117048
    invoke-virtual {v4, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2117049
    return-object v0

    :cond_0
    invoke-interface {v2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2117037
    invoke-static {}, LX/1dS;->b()V

    .line 2117038
    const/4 v0, 0x0

    return-object v0
.end method
