.class public final LX/Dc7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/LinkMenuMutationsModels$PageLinkMenuCreateMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;LX/4BY;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2017908
    iput-object p1, p0, LX/Dc7;->c:Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;

    iput-object p2, p0, LX/Dc7;->a:LX/4BY;

    iput-object p3, p0, LX/Dc7;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2017909
    iget-object v0, p0, LX/Dc7;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2017910
    iget-object v0, p0, LX/Dc7;->c:Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;

    iget-object v1, p0, LX/Dc7;->b:Ljava/lang/String;

    const/4 v2, 0x0

    .line 2017911
    instance-of v3, p1, LX/4Ua;

    if-eqz v3, :cond_0

    .line 2017912
    check-cast p1, LX/4Ua;

    .line 2017913
    iget-object v3, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v3, v3

    .line 2017914
    if-nez v3, :cond_2

    .line 2017915
    :cond_0
    :goto_0
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2017916
    const v2, 0x7f0828fc

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2017917
    :cond_1
    iget-object v3, v0, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;->e:LX/0kL;

    new-instance p0, LX/27k;

    invoke-direct {p0, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, p0}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2017918
    iget-object v2, v0, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;->c:LX/DbM;

    .line 2017919
    iget-object v3, v2, LX/DbM;->a:LX/0Zb;

    const-string p0, "page_menu_management"

    const-string v0, "menu_management_save_link_menu_failure"

    invoke-static {p0, v0, v1}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v3, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017920
    return-void

    .line 2017921
    :cond_2
    iget-object v2, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v2, v2

    .line 2017922
    invoke-virtual {v2}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2017923
    iget-object v0, p0, LX/Dc7;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2017924
    iget-object v0, p0, LX/Dc7;->c:Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;

    iget-object v1, p0, LX/Dc7;->b:Ljava/lang/String;

    .line 2017925
    iget-object v2, v0, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;->c:LX/DbM;

    .line 2017926
    iget-object v3, v2, LX/DbM;->a:LX/0Zb;

    const-string p0, "page_menu_management"

    const-string p1, "menu_management_save_link_menu_successful"

    invoke-static {p0, p1, v1}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v3, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017927
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    .line 2017928
    if-eqz v2, :cond_1

    .line 2017929
    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    .line 2017930
    if-eqz v3, :cond_0

    .line 2017931
    iget-object p0, v0, Lcom/facebook/localcontent/menus/admin/manager/PageLinkMenuFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 p1, 0x0

    invoke-virtual {p0, v3, p1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2017932
    :cond_0
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/app/Activity;->setResult(I)V

    .line 2017933
    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 2017934
    :cond_1
    return-void
.end method
