.class public final LX/Cmh;
.super LX/Cm8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cm8",
        "<",
        "Lcom/facebook/richdocument/model/data/WebViewBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:I

.field public f:LX/8Yr;

.field public final g:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

.field public final h:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

.field public i:Z


# direct methods
.method public constructor <init>(ILcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V
    .locals 0

    .prologue
    .line 1933558
    invoke-direct {p0, p1}, LX/Cm8;-><init>(I)V

    .line 1933559
    iput-object p2, p0, LX/Cmh;->g:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    .line 1933560
    iput-object p3, p0, LX/Cmh;->h:Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    .line 1933561
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V
    .locals 1

    .prologue
    .line 1933553
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1, p2}, LX/Cmh;-><init>(ILcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;)V

    .line 1933554
    return-void
.end method


# virtual methods
.method public final b()LX/Clr;
    .locals 2

    .prologue
    .line 1933555
    iget-object v0, p0, LX/Cmh;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cmh;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1933556
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "WebViewBlock must have either a non-null source or url"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1933557
    :cond_0
    new-instance v0, LX/Cmi;

    invoke-direct {v0, p0}, LX/Cmi;-><init>(LX/Cmh;)V

    return-object v0
.end method
