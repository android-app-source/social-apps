.class public LX/Eok;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Eoj;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2168540
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2168541
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;LX/Emj;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/2h7;LX/5P2;)LX/Eoj;
    .locals 19

    .prologue
    .line 2168542
    new-instance v1, LX/Eoj;

    invoke-static/range {p0 .. p0}, LX/BS5;->a(LX/0QB;)LX/BS5;

    move-result-object v10

    check-cast v10, LX/BS5;

    invoke-static/range {p0 .. p0}, LX/EpK;->a(LX/0QB;)LX/EpK;

    move-result-object v11

    check-cast v11, LX/EpK;

    const-class v2, LX/EpO;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/EpO;

    const-class v2, LX/Epi;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/Epi;

    const-class v2, LX/Epu;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/Epu;

    const/16 v2, 0x1ad9

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v16

    check-cast v16, LX/2do;

    const/16 v2, 0xafc

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v18

    check-cast v18, LX/0ad;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v18}, LX/Eoj;-><init>(Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;LX/Emj;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/2h7;LX/5P2;LX/BS5;LX/EpK;LX/EpO;LX/Epi;LX/Epu;LX/0Or;LX/2do;LX/0Ot;LX/0ad;)V

    .line 2168543
    return-object v1
.end method
