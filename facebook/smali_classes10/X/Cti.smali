.class public abstract LX/Cti;
.super LX/Cte;
.source ""

# interfaces
.implements LX/Ctg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/Ct1;",
        ">",
        "LX/Cte;",
        "LX/Ctg",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public d:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CsO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/richdocument/view/widget/media/MediaFrameBody",
            "<TV;>;"
        }
    .end annotation
.end field

.field private g:LX/Cqj;

.field public h:LX/CrS;

.field private final i:Landroid/view/GestureDetector;

.field private j:LX/Cre;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1945380
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Cti;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1945381
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1945382
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Cti;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945383
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1945384
    invoke-direct {p0, p1, p2, p3}, LX/Cte;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945385
    const-class v0, LX/Cti;

    invoke-static {v0, p0}, LX/Cti;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1945386
    new-instance v0, LX/Ctk;

    iget-object v1, p0, LX/Cti;->e:LX/CsO;

    invoke-direct {v0, p0, v1}, LX/Ctk;-><init>(LX/Ctg;LX/CsO;)V

    iput-object v0, p0, LX/Cti;->j:LX/Cre;

    .line 1945387
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, LX/Cti;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/Cth;

    invoke-direct {v2, p0}, LX/Cth;-><init>(LX/Cti;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/Cti;->i:Landroid/view/GestureDetector;

    .line 1945388
    iget-object v0, p0, LX/Cti;->i:Landroid/view/GestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 1945389
    invoke-virtual {p0, v3}, LX/Cti;->setClipChildren(Z)V

    .line 1945390
    invoke-virtual {p0, v3}, LX/Cti;->setClipToPadding(Z)V

    .line 1945391
    return-void
.end method

.method private static a(LX/CrS;Landroid/view/View;)Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1945392
    sget-object v0, LX/CrQ;->RECT:LX/CrQ;

    const-class v1, LX/CrW;

    invoke-interface {p0, p1, v0, v1}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v0

    check-cast v0, LX/CrW;

    .line 1945393
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1945394
    :cond_0
    iget-object v1, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v0, v1

    .line 1945395
    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cti;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v1

    check-cast v1, LX/Chv;

    invoke-static {p0}, LX/CsO;->a(LX/0QB;)LX/CsO;

    move-result-object p0

    check-cast p0, LX/CsO;

    iput-object v1, p1, LX/Cti;->d:LX/Chv;

    iput-object p0, p1, LX/Cti;->e:LX/CsO;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1945396
    invoke-virtual {p0}, LX/Cti;->getCurrentLayout()LX/CrS;

    move-result-object v0

    invoke-static {v0, p1}, LX/Cti;->a(LX/CrS;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public final a()Landroid/view/ViewGroup;
    .locals 0

    .prologue
    .line 1945397
    return-object p0
.end method

.method public final a(LX/CnQ;)V
    .locals 3

    .prologue
    .line 1945436
    invoke-virtual {p0}, LX/Cti;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    .line 1945437
    invoke-virtual {v0}, LX/CqX;->d()LX/Cqv;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CqX;->d(LX/Cqv;)LX/Cqf;

    move-result-object v1

    check-cast v1, LX/Cqf;

    .line 1945438
    iget-object v0, v1, LX/Cqf;->j:LX/Cqc;

    move-object v1, v0

    .line 1945439
    move-object v0, v1

    .line 1945440
    invoke-interface {p1}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v1

    .line 1945441
    iget-object v2, v1, LX/ClU;->e:LX/ClR;

    move-object v1, v2

    .line 1945442
    sget-object v2, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    if-ne v0, v2, :cond_0

    sget-object v0, LX/ClR;->ABOVE:LX/ClR;

    if-eq v1, v0, :cond_2

    sget-object v0, LX/ClR;->BELOW:LX/ClR;

    if-eq v1, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1945443
    if-eqz v0, :cond_1

    .line 1945444
    invoke-virtual {p0}, LX/Cti;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Cte;->a(LX/CnQ;)V

    .line 1945445
    :goto_1
    return-void

    .line 1945446
    :cond_1
    invoke-super {p0, p1}, LX/Cte;->a(LX/CnQ;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(LX/Cqw;)V
    .locals 2

    .prologue
    .line 1945398
    invoke-virtual {p0, p1}, LX/Cti;->b(LX/Cqw;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1945399
    invoke-virtual {p0}, LX/Cti;->getCurrentLayout()LX/CrS;

    move-result-object v0

    .line 1945400
    invoke-virtual {p0}, LX/Cti;->getTransitionStrategy()LX/Cqj;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v1

    .line 1945401
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1945402
    invoke-virtual {p0}, LX/Cti;->getTransitionStrategy()LX/Cqj;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/CqX;->f(LX/Cqv;)V

    .line 1945403
    invoke-interface {v0}, LX/CrS;->a()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    invoke-virtual {v0}, LX/Cqw;->e()LX/Cqw;

    move-result-object v0

    .line 1945404
    invoke-virtual {p0}, LX/Cti;->getTransitionStrategy()LX/Cqj;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, LX/Cqj;->a(LX/Cqw;LX/Cqw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1945405
    invoke-virtual {p0}, LX/Cti;->c()V

    .line 1945406
    :cond_0
    invoke-virtual {p0, p1}, LX/Cti;->c(LX/Cqw;)V

    .line 1945407
    :cond_1
    return-void
.end method

.method public a(LX/CrS;)V
    .locals 6

    .prologue
    .line 1945408
    iget-object v1, p0, LX/Cti;->h:LX/CrS;

    .line 1945409
    if-nez v1, :cond_2

    .line 1945410
    invoke-virtual {p0}, LX/Cti;->requestLayout()V

    .line 1945411
    :cond_0
    iput-object p1, p0, LX/Cti;->h:LX/CrS;

    .line 1945412
    invoke-static {p1, p0}, LX/Cti;->a(LX/CrS;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1945413
    if-eqz v0, :cond_1

    .line 1945414
    invoke-virtual {p0, v0}, LX/Cti;->a(Landroid/graphics/Rect;)V

    .line 1945415
    :cond_1
    return-void

    .line 1945416
    :cond_2
    invoke-interface {p1}, LX/CrS;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1945417
    const/4 v3, 0x1

    .line 1945418
    invoke-interface {v1, v0}, LX/CrS;->a(Landroid/view/View;)LX/CrR;

    move-result-object v4

    .line 1945419
    invoke-interface {p1, v0}, LX/CrS;->a(Landroid/view/View;)LX/CrR;

    move-result-object v5

    .line 1945420
    if-eqz v4, :cond_4

    if-nez v5, :cond_5

    .line 1945421
    :cond_4
    :goto_1
    move v3, v3

    .line 1945422
    if-eqz v3, :cond_3

    .line 1945423
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_0

    :cond_5
    invoke-virtual {v4, v5}, LX/CrR;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1945424
    iget-object v0, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->getRotation()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 1945425
    invoke-super {p0, p1}, LX/Cte;->a(Landroid/graphics/Canvas;)V

    .line 1945426
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 1945427
    if-eqz p1, :cond_0

    .line 1945428
    iget-object v0, p0, LX/Cti;->d:LX/Chv;

    new-instance v1, LX/CiN;

    sget-object v2, LX/CiM;->SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiM;

    invoke-direct {v1, v2, p0, p1}, LX/CiN;-><init>(LX/CiM;Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1945429
    :cond_0
    return-void
.end method

.method public a(LX/Crd;)Z
    .locals 1

    .prologue
    .line 1945430
    iget-object v0, p0, LX/Cti;->j:LX/Cre;

    if-eqz v0, :cond_0

    .line 1945431
    iget-object v0, p0, LX/Cti;->j:LX/Cre;

    invoke-interface {v0, p1}, LX/Cre;->a(LX/Crd;)Z

    move-result v0

    .line 1945432
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1945433
    invoke-virtual {p0}, LX/Cti;->getCurrentLayout()LX/CrS;

    move-result-object v0

    sget-object v1, LX/CrQ;->OPACITY:LX/CrQ;

    const-class v2, LX/CrV;

    invoke-interface {v0, p1, v1, v2}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v0

    check-cast v0, LX/CrV;

    .line 1945434
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/CrV;->b()Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 1945376
    invoke-super {p0}, LX/Cte;->b()V

    .line 1945377
    invoke-virtual {p0}, LX/Cti;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0}, LX/Cte;->b()V

    .line 1945378
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cti;->h:LX/CrS;

    .line 1945379
    return-void
.end method

.method public final b(LX/Cqw;)Z
    .locals 1

    .prologue
    .line 1945435
    invoke-virtual {p0}, LX/Cti;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 4

    .prologue
    .line 1945325
    iget-object v0, p0, LX/Cti;->d:LX/Chv;

    new-instance v1, LX/CiN;

    sget-object v2, LX/CiM;->SET_FOCUSED_VIEW:LX/CiM;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, v3}, LX/CiN;-><init>(LX/CiM;Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1945326
    return-void
.end method

.method public c(LX/Cqw;)V
    .locals 2

    .prologue
    .line 1945327
    invoke-virtual {p0}, LX/Cti;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0}, LX/CqX;->d()LX/Cqv;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 1945328
    iget-object v0, p0, LX/Cti;->e:LX/CsO;

    invoke-virtual {v0, p0}, LX/CsO;->a(Landroid/view/View;)V

    .line 1945329
    :goto_0
    return-void

    .line 1945330
    :cond_0
    iget-object v0, p0, LX/Cti;->e:LX/CsO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CsO;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 1945331
    return-void
.end method

.method public final drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    .prologue
    .line 1945332
    iget-object v0, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    if-ne p2, v0, :cond_0

    .line 1945333
    invoke-virtual {p0}, LX/Cte;->getOverlayView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Cti;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1945334
    if-eqz v0, :cond_0

    .line 1945335
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->save(I)I

    move-result v1

    .line 1945336
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 1945337
    invoke-super {p0, p1, p2, p3, p4}, LX/Cte;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 1945338
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1945339
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, LX/Cte;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 1945340
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1945341
    iget-object v1, p0, LX/Cti;->g:LX/Cqj;

    invoke-virtual {p0}, LX/Cti;->getCurrentLayout()LX/CrS;

    move-result-object v0

    invoke-interface {v0}, LX/CrS;->a()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    .line 1945342
    iget-object p0, v1, LX/CqX;->c:Ljava/util/Map;

    invoke-interface {p0, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CrS;

    .line 1945343
    return-void
.end method

.method public getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/richdocument/view/widget/media/MediaFrameBody",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1945344
    iget-object v0, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    return-object v0
.end method

.method public getCurrentLayout()LX/CrS;
    .locals 1

    .prologue
    .line 1945345
    iget-object v0, p0, LX/Cti;->g:LX/Cqj;

    invoke-virtual {v0}, LX/CqX;->g()LX/CrS;

    move-result-object v0

    return-object v0
.end method

.method public getMediaView()LX/Ct1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 1945346
    iget-object v0, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    .line 1945347
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    move-object v0, p0

    .line 1945348
    goto :goto_0
.end method

.method public getOverlayBounds()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1945349
    iget-object v0, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    invoke-virtual {p0, v0}, LX/Cti;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getTransitionStrategy()LX/Cqj;
    .locals 1

    .prologue
    .line 1945350
    iget-object v0, p0, LX/Cti;->g:LX/Cqj;

    return-object v0
.end method

.method public onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1945351
    invoke-super/range {p0 .. p5}, LX/Cte;->onLayout(ZIIII)V

    .line 1945352
    iget-object v0, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    if-eqz v0, :cond_0

    .line 1945353
    iget-object v0, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    invoke-virtual {p0, v0}, LX/Cti;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1945354
    iget-object v1, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    invoke-virtual {p0, v1, v0}, LX/Cte;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1945355
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x2

    const v1, -0x435e1d2c

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1945356
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1945357
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 1945358
    invoke-virtual {p0}, LX/Cti;->getOverlayBounds()Landroid/graphics/Rect;

    move-result-object v4

    .line 1945359
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1945360
    iget-object v2, p0, LX/Cti;->i:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1945361
    const v2, 0x203cea3d

    invoke-static {v5, v5, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1945362
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/Cte;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x3ec19da1

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setBody(Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/view/widget/media/MediaFrameBody",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1945363
    iput-object p1, p0, LX/Cti;->f:Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    .line 1945364
    return-void
.end method

.method public setOverlayBackgroundColor(I)V
    .locals 1

    .prologue
    .line 1945365
    invoke-super {p0, p1}, LX/Cte;->setOverlayBackgroundColor(I)V

    .line 1945366
    invoke-virtual {p0}, LX/Cti;->getBody()Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Cte;->setOverlayBackgroundColor(I)V

    .line 1945367
    return-void
.end method

.method public setTransitionStrategy(LX/Cqj;)V
    .locals 2

    .prologue
    .line 1945368
    iget-object v0, p0, LX/Cti;->g:LX/Cqj;

    if-eqz v0, :cond_0

    .line 1945369
    iget-object v0, p0, LX/Cti;->g:LX/Cqj;

    .line 1945370
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CqX;->a(LX/CrK;)V

    .line 1945371
    iget-object v1, v0, LX/CqX;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1945372
    :cond_0
    iput-object p1, p0, LX/Cti;->g:LX/Cqj;

    .line 1945373
    iget-object v0, p0, LX/Cti;->g:LX/Cqj;

    .line 1945374
    iget-object v1, v0, LX/CqX;->i:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1945375
    return-void
.end method
