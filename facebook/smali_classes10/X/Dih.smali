.class public LX/Dih;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method private constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2032464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2032465
    iput-object p1, p0, LX/Dih;->a:LX/0Zb;

    .line 2032466
    return-void
.end method

.method public static b(LX/0QB;)LX/Dih;
    .locals 2

    .prologue
    .line 2032471
    new-instance v1, LX/Dih;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/Dih;-><init>(LX/0Zb;)V

    .line 2032472
    return-object v1
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2032467
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "pages_public_view"

    .line 2032468
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2032469
    move-object v0, v0

    .line 2032470
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2032460
    iget-object v0, p0, LX/Dih;->a:LX/0Zb;

    const-string v1, "profservices_booking_admin_click_request_list"

    invoke-static {v1, p1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032461
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2032462
    iget-object v0, p0, LX/Dih;->a:LX/0Zb;

    const-string v1, "profservices_booking_error"

    invoke-static {v1, p3}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "error_category"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "error_detail"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "request_id"

    invoke-virtual {v1, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032463
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2032458
    iget-object v0, p0, LX/Dih;->a:LX/0Zb;

    const-string v1, "profservices_booking_admin_click_sent_list"

    invoke-static {v1, p1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032459
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2032456
    iget-object v0, p0, LX/Dih;->a:LX/0Zb;

    const-string v1, "profservices_booking_consumer_accept_appointment"

    invoke-static {v1, p1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "request_id"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032457
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)V
    .locals 3

    .prologue
    .line 2032454
    iget-object v0, p0, LX/Dih;->a:LX/0Zb;

    const-string v1, "profservices_booking_consumer_tapped_confirmed_banner"

    invoke-static {v1, p1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "request_id"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "booking_status"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032455
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2032452
    iget-object v0, p0, LX/Dih;->a:LX/0Zb;

    const-string v1, "profservices_booking_consumer_click_upcoming_list"

    invoke-static {v1, p1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "referrer"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032453
    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;)V
    .locals 3

    .prologue
    .line 2032450
    iget-object v0, p0, LX/Dih;->a:LX/0Zb;

    const-string v1, "profservices_booking_admin_banner_tapped_schedule"

    invoke-static {v1, p1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "request_id"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "booking_status"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032451
    return-void
.end method
