.class public LX/Edg;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[B

.field public static final b:[B

.field public static final c:[B


# instance fields
.field public d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/net/Uri;

.field public f:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2150695
    const-string v0, "from-data"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, LX/Edg;->a:[B

    .line 2150696
    const-string v0, "attachment"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, LX/Edg;->b:[B

    .line 2150697
    const-string v0, "inline"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, LX/Edg;->c:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2150698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2150699
    iput-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    .line 2150700
    iput-object v0, p0, LX/Edg;->e:Landroid/net/Uri;

    .line 2150701
    iput-object v0, p0, LX/Edg;->f:[B

    .line 2150702
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    .line 2150703
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 2150685
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x81

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150686
    return-void
.end method

.method public final a()[B
    .locals 1

    .prologue
    .line 2150704
    iget-object v0, p0, LX/Edg;->f:[B

    return-object v0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 2150720
    iget-object v0, p0, LX/Edg;->e:Landroid/net/Uri;

    return-object v0
.end method

.method public final b([B)V
    .locals 6

    .prologue
    const/16 v5, 0xc0

    const/16 v4, 0x3e

    const/16 v1, 0x3c

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2150705
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 2150706
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Content-Id may not be null or empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150707
    :cond_1
    array-length v0, p1

    if-le v0, v3, :cond_2

    aget-byte v0, p1, v2

    int-to-char v0, v0

    if-ne v0, v1, :cond_2

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte v0, p1, v0

    int-to-char v0, v0

    if-ne v0, v4, :cond_2

    .line 2150708
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v5, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150709
    :goto_0
    return-void

    .line 2150710
    :cond_2
    array-length v0, p1

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    .line 2150711
    aput-byte v1, v0, v2

    .line 2150712
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-byte v4, v0, v1

    .line 2150713
    array-length v1, p1

    invoke-static {p1, v2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2150714
    iget-object v1, p0, LX/Edg;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final c([B)V
    .locals 2

    .prologue
    .line 2150715
    if-nez p1, :cond_0

    .line 2150716
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null content-location"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150717
    :cond_0
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x8e

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150718
    return-void
.end method

.method public final c()[B
    .locals 2

    .prologue
    .line 2150719
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0xc0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2150687
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x81

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2150688
    if-nez v0, :cond_0

    .line 2150689
    const/4 v0, 0x0

    .line 2150690
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final d([B)V
    .locals 2

    .prologue
    .line 2150691
    if-nez p1, :cond_0

    .line 2150692
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null content-disposition"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150693
    :cond_0
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0xc5

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150694
    return-void
.end method

.method public final e([B)V
    .locals 2

    .prologue
    .line 2150658
    if-nez p1, :cond_0

    .line 2150659
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null content-type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150660
    :cond_0
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x91

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150661
    return-void
.end method

.method public final e()[B
    .locals 2

    .prologue
    .line 2150662
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x8e

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public final f([B)V
    .locals 2

    .prologue
    .line 2150663
    if-nez p1, :cond_0

    .line 2150664
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null content-transfer-encoding"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150665
    :cond_0
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150666
    return-void
.end method

.method public final g([B)V
    .locals 2

    .prologue
    .line 2150667
    if-nez p1, :cond_0

    .line 2150668
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null content-id"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150669
    :cond_0
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x97

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150670
    return-void
.end method

.method public final g()[B
    .locals 2

    .prologue
    .line 2150671
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x91

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public final h([B)V
    .locals 2

    .prologue
    .line 2150672
    if-nez p1, :cond_0

    .line 2150673
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null content-id"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2150674
    :cond_0
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x98

    invoke-virtual {v0, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2150675
    return-void
.end method

.method public final i()[B
    .locals 2

    .prologue
    .line 2150676
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x97

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public final j()[B
    .locals 2

    .prologue
    .line 2150677
    iget-object v0, p0, LX/Edg;->d:Landroid/util/SparseArray;

    const/16 v1, 0x98

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 2150678
    iget-object v0, p0, LX/Edg;->f:[B

    if-eqz v0, :cond_0

    .line 2150679
    iget-object v0, p0, LX/Edg;->f:[B

    array-length v0, v0

    .line 2150680
    :goto_0
    return v0

    .line 2150681
    :cond_0
    iget-object v0, p0, LX/Edg;->e:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 2150682
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/Edg;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2150683
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0

    .line 2150684
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
