.class public final enum LX/DAT;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/3OI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DAT;",
        ">;",
        "LX/3OI;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DAT;

.field public static final enum AGGREGATE_CALL_DETAILS:LX/DAT;

.field public static final enum AUTO_COMPLETE:LX/DAT;

.field public static final enum CALL_LOG:LX/DAT;

.field public static final enum NULL_STATE_TOP_GROUP:LX/DAT;

.field public static final enum ONGOING_GROUP_CALL:LX/DAT;

.field public static final enum SEARCH_RESULT:LX/DAT;

.field public static final enum SUGGESTIONS:LX/DAT;

.field public static final enum UNKNOWN:LX/DAT;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1971270
    new-instance v0, LX/DAT;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/DAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAT;->UNKNOWN:LX/DAT;

    .line 1971271
    new-instance v0, LX/DAT;

    const-string v1, "SEARCH_RESULT"

    invoke-direct {v0, v1, v4}, LX/DAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAT;->SEARCH_RESULT:LX/DAT;

    .line 1971272
    new-instance v0, LX/DAT;

    const-string v1, "AUTO_COMPLETE"

    invoke-direct {v0, v1, v5}, LX/DAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAT;->AUTO_COMPLETE:LX/DAT;

    .line 1971273
    new-instance v0, LX/DAT;

    const-string v1, "SUGGESTIONS"

    invoke-direct {v0, v1, v6}, LX/DAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAT;->SUGGESTIONS:LX/DAT;

    .line 1971274
    new-instance v0, LX/DAT;

    const-string v1, "CALL_LOG"

    invoke-direct {v0, v1, v7}, LX/DAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAT;->CALL_LOG:LX/DAT;

    .line 1971275
    new-instance v0, LX/DAT;

    const-string v1, "AGGREGATE_CALL_DETAILS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/DAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAT;->AGGREGATE_CALL_DETAILS:LX/DAT;

    .line 1971276
    new-instance v0, LX/DAT;

    const-string v1, "ONGOING_GROUP_CALL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/DAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAT;->ONGOING_GROUP_CALL:LX/DAT;

    .line 1971277
    new-instance v0, LX/DAT;

    const-string v1, "NULL_STATE_TOP_GROUP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/DAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DAT;->NULL_STATE_TOP_GROUP:LX/DAT;

    .line 1971278
    const/16 v0, 0x8

    new-array v0, v0, [LX/DAT;

    sget-object v1, LX/DAT;->UNKNOWN:LX/DAT;

    aput-object v1, v0, v3

    sget-object v1, LX/DAT;->SEARCH_RESULT:LX/DAT;

    aput-object v1, v0, v4

    sget-object v1, LX/DAT;->AUTO_COMPLETE:LX/DAT;

    aput-object v1, v0, v5

    sget-object v1, LX/DAT;->SUGGESTIONS:LX/DAT;

    aput-object v1, v0, v6

    sget-object v1, LX/DAT;->CALL_LOG:LX/DAT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/DAT;->AGGREGATE_CALL_DETAILS:LX/DAT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/DAT;->ONGOING_GROUP_CALL:LX/DAT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/DAT;->NULL_STATE_TOP_GROUP:LX/DAT;

    aput-object v2, v0, v1

    sput-object v0, LX/DAT;->$VALUES:[LX/DAT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1971267
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DAT;
    .locals 1

    .prologue
    .line 1971268
    const-class v0, LX/DAT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DAT;

    return-object v0
.end method

.method public static values()[LX/DAT;
    .locals 1

    .prologue
    .line 1971269
    sget-object v0, LX/DAT;->$VALUES:[LX/DAT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DAT;

    return-object v0
.end method
