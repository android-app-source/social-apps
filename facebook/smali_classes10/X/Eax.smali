.class public LX/Eax;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2PE;


# direct methods
.method public constructor <init>(LX/2PE;)V
    .locals 0

    .prologue
    .line 2142785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142786
    iput-object p1, p0, LX/Eax;->a:LX/2PE;

    .line 2142787
    return-void
.end method


# virtual methods
.method public final a(LX/Eay;)LX/EbB;
    .locals 8

    .prologue
    .line 2142788
    sget-object v1, LX/Eaw;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2142789
    :try_start_0
    iget-object v0, p0, LX/Eax;->a:LX/2PE;

    invoke-virtual {v0, p1}, LX/2PE;->a(LX/Eay;)LX/Eb1;

    move-result-object v0

    .line 2142790
    invoke-virtual {v0}, LX/Eb1;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2142791
    invoke-static {}, LX/Ecq;->e()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {}, LX/Ecq;->d()[B

    move-result-object v4

    invoke-static {}, LX/Ear;->a()LX/Eau;

    move-result-object v5

    .line 2142792
    iget-object v6, v0, LX/Eb1;->a:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->clear()V

    .line 2142793
    iget-object v6, v0, LX/Eb1;->a:Ljava/util/LinkedList;

    new-instance v7, LX/Eb2;

    invoke-direct {v7, v2, v3, v4, v5}, LX/Eb2;-><init>(II[BLX/Eau;)V

    invoke-virtual {v6, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2142794
    iget-object v2, p0, LX/Eax;->a:LX/2PE;

    invoke-virtual {v2, p1, v0}, LX/2PE;->a(LX/Eay;LX/Eb1;)V

    .line 2142795
    :cond_0
    invoke-virtual {v0}, LX/Eb1;->b()LX/Eb2;

    move-result-object v0

    .line 2142796
    new-instance v2, LX/EbB;

    invoke-virtual {v0}, LX/Eb2;->a()I

    move-result v3

    invoke-virtual {v0}, LX/Eb2;->b()LX/Eaz;

    move-result-object v4

    .line 2142797
    iget v5, v4, LX/Eaz;->c:I

    move v4, v5

    .line 2142798
    invoke-virtual {v0}, LX/Eb2;->b()LX/Eaz;

    move-result-object v5

    .line 2142799
    iget-object v6, v5, LX/Eaz;->d:[B

    move-object v5, v6

    .line 2142800
    invoke-virtual {v0}, LX/Eb2;->c()LX/Eat;

    move-result-object v0

    invoke-direct {v2, v3, v4, v5, v0}, LX/EbB;-><init>(II[BLX/Eat;)V
    :try_end_0
    .catch LX/Eah; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit v1

    return-object v2

    .line 2142801
    :catch_0
    move-exception v0

    .line 2142802
    :goto_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 2142803
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2142804
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final a(LX/Eay;LX/EbB;)V
    .locals 7

    .prologue
    .line 2142805
    sget-object v1, LX/Eaw;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2142806
    :try_start_0
    iget-object v0, p0, LX/Eax;->a:LX/2PE;

    invoke-virtual {v0, p1}, LX/2PE;->a(LX/Eay;)LX/Eb1;

    move-result-object v0

    .line 2142807
    iget v2, p2, LX/EbB;->a:I

    move v2, v2

    .line 2142808
    iget v3, p2, LX/EbB;->b:I

    move v3, v3

    .line 2142809
    iget-object v4, p2, LX/EbB;->c:[B

    move-object v4, v4

    .line 2142810
    iget-object v5, p2, LX/EbB;->d:LX/Eat;

    move-object v5, v5

    .line 2142811
    iget-object v6, v0, LX/Eb1;->a:Ljava/util/LinkedList;

    new-instance p2, LX/Eb2;

    invoke-direct {p2, v2, v3, v4, v5}, LX/Eb2;-><init>(II[BLX/Eat;)V

    invoke-virtual {v6, p2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 2142812
    iget-object v6, v0, LX/Eb1;->a:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    const/4 p2, 0x5

    if-le v6, p2, :cond_0

    .line 2142813
    iget-object v6, v0, LX/Eb1;->a:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    .line 2142814
    :cond_0
    iget-object v2, p0, LX/Eax;->a:LX/2PE;

    invoke-virtual {v2, p1, v0}, LX/2PE;->a(LX/Eay;LX/Eb1;)V

    .line 2142815
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
