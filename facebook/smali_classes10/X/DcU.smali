.class public final LX/DcU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

.field public final synthetic b:Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

.field public final synthetic c:LX/DcX;

.field public final synthetic d:LX/DcW;


# direct methods
.method public constructor <init>(LX/DcW;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;LX/DcX;)V
    .locals 0

    .prologue
    .line 2018461
    iput-object p1, p0, LX/DcU;->d:LX/DcW;

    iput-object p2, p0, LX/DcU;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iput-object p3, p0, LX/DcU;->b:Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    iput-object p4, p0, LX/DcU;->c:LX/DcX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x4557e4ff

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018462
    iget-object v1, p0, LX/DcU;->d:LX/DcW;

    iget-object v2, p0, LX/DcU;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iget-object v3, p0, LX/DcU;->b:Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    iget-object v4, p0, LX/DcU;->c:LX/DcX;

    .line 2018463
    invoke-static {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->a(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    move-result-object v6

    .line 2018464
    if-nez v6, :cond_0

    .line 2018465
    iget-object v6, v1, LX/DcW;->e:LX/03V;

    sget-object v7, LX/DcW;->a:Ljava/lang/String;

    const-string v8, "Trying to like null product"

    invoke-virtual {v6, v7, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018466
    :goto_0
    const v1, 0x27676a6d

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2018467
    :cond_0
    new-instance v7, LX/8AY;

    invoke-direct {v7}, LX/8AY;-><init>()V

    invoke-static {v6}, LX/8AY;->a(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)LX/8AY;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->c()Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v6, 0x1

    .line 2018468
    :goto_1
    iput-boolean v6, v7, LX/8AY;->b:Z

    .line 2018469
    move-object v6, v7

    .line 2018470
    invoke-virtual {v6}, LX/8AY;->a()Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;

    move-result-object v6

    .line 2018471
    invoke-virtual {v4, v6}, LX/DcX;->a(Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;)V

    .line 2018472
    invoke-static {v1, v2, v6, v4}, LX/DcW;->a(LX/DcW;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;LX/DcX;)V

    .line 2018473
    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->d()Ljava/lang/String;

    move-result-object v7

    .line 2018474
    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;->c()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2018475
    new-instance v6, LX/4Hy;

    invoke-direct {v6}, LX/4Hy;-><init>()V

    .line 2018476
    const-string v8, "page_product_id"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018477
    move-object v6, v6

    .line 2018478
    const-string v8, "after_party_popular_at_android"

    .line 2018479
    const-string p0, "context"

    invoke-virtual {v6, p0, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018480
    move-object v6, v6

    .line 2018481
    new-instance v8, LX/8BG;

    invoke-direct {v8}, LX/8BG;-><init>()V

    move-object v8, v8

    .line 2018482
    const-string p0, "input"

    invoke-virtual {v8, p0, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2018483
    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    move-object v6, v6

    .line 2018484
    :goto_2
    iget-object v8, v1, LX/DcW;->h:LX/1Ck;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "task_key_mutate_like"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object p0, v1, LX/DcW;->f:LX/0tX;

    sget-object p1, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {p0, v6, p1}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance p0, LX/DcV;

    invoke-direct {p0, v1, v2, v3, v4}, LX/DcV;-><init>(LX/DcW;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;LX/DcX;)V

    invoke-virtual {v8, v7, v6, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2018485
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 2018486
    :cond_2
    new-instance v6, LX/4Hx;

    invoke-direct {v6}, LX/4Hx;-><init>()V

    .line 2018487
    const-string v8, "page_product_id"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018488
    move-object v6, v6

    .line 2018489
    const-string v8, "after_party_popular_at_android"

    .line 2018490
    const-string p0, "context"

    invoke-virtual {v6, p0, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018491
    move-object v6, v6

    .line 2018492
    new-instance v8, LX/8BF;

    invoke-direct {v8}, LX/8BF;-><init>()V

    move-object v8, v8

    .line 2018493
    const-string p0, "input"

    invoke-virtual {v8, p0, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2018494
    invoke-static {v8}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    move-object v6, v6

    .line 2018495
    goto :goto_2
.end method
