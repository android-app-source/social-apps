.class public final enum LX/EjN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EjN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EjN;

.field public static final enum ADD:LX/EjN;

.field public static final enum REMOVE:LX/EjN;

.field public static final enum UPDATE:LX/EjN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2161300
    new-instance v0, LX/EjN;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v2}, LX/EjN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EjN;->ADD:LX/EjN;

    .line 2161301
    new-instance v0, LX/EjN;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v3}, LX/EjN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EjN;->REMOVE:LX/EjN;

    .line 2161302
    new-instance v0, LX/EjN;

    const-string v1, "UPDATE"

    invoke-direct {v0, v1, v4}, LX/EjN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EjN;->UPDATE:LX/EjN;

    .line 2161303
    const/4 v0, 0x3

    new-array v0, v0, [LX/EjN;

    sget-object v1, LX/EjN;->ADD:LX/EjN;

    aput-object v1, v0, v2

    sget-object v1, LX/EjN;->REMOVE:LX/EjN;

    aput-object v1, v0, v3

    sget-object v1, LX/EjN;->UPDATE:LX/EjN;

    aput-object v1, v0, v4

    sput-object v0, LX/EjN;->$VALUES:[LX/EjN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2161297
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EjN;
    .locals 1

    .prologue
    .line 2161299
    const-class v0, LX/EjN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EjN;

    return-object v0
.end method

.method public static values()[LX/EjN;
    .locals 1

    .prologue
    .line 2161298
    sget-object v0, LX/EjN;->$VALUES:[LX/EjN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EjN;

    return-object v0
.end method
