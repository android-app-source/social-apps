.class public final LX/EAE;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EA8;

.field public final synthetic b:LX/EAH;


# direct methods
.method public constructor <init>(LX/EAH;LX/EA8;)V
    .locals 0

    .prologue
    .line 2085363
    iput-object p1, p0, LX/EAE;->b:LX/EAH;

    iput-object p2, p0, LX/EAE;->a:LX/EA8;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2085364
    iget-object v0, p0, LX/EAE;->a:LX/EA8;

    .line 2085365
    iget-object v1, v0, LX/EA8;->c:LX/EA9;

    iget-object v2, v0, LX/EA8;->a:Ljava/lang/String;

    iget-object p0, v0, LX/EA8;->b:Lcom/facebook/fbservice/service/OperationResult;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p1

    invoke-static {v1, v2, p0, p1}, LX/EA9;->a$redex0(LX/EA9;Ljava/lang/String;Lcom/facebook/fbservice/service/OperationResult;LX/0am;)V

    .line 2085366
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085367
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2085368
    if-eqz p1, :cond_0

    .line 2085369
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2085370
    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    .line 2085371
    :goto_0
    if-eqz p1, :cond_1

    .line 2085372
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2085373
    if-eqz v1, :cond_1

    .line 2085374
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2085375
    check-cast v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->j()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    move-result-object v1

    if-nez v1, :cond_4

    .line 2085376
    :cond_1
    const/4 v1, 0x0

    .line 2085377
    :goto_1
    move-object v1, v1

    .line 2085378
    iget-object v2, p0, LX/EAE;->a:LX/EA8;

    .line 2085379
    if-eqz v0, :cond_2

    .line 2085380
    iget-object v3, v2, LX/EA8;->c:LX/EA9;

    iget-object v3, v3, LX/EA9;->e:LX/Ch5;

    new-instance v4, LX/Ch7;

    iget-object p0, v2, LX/EA8;->a:Ljava/lang/String;

    invoke-direct {v4, v0, p0}, LX/Ch7;-><init>(Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 2085381
    :cond_2
    iget-object v3, v2, LX/EA8;->c:LX/EA9;

    iget-object v4, v2, LX/EA8;->a:Ljava/lang/String;

    iget-object p0, v2, LX/EA8;->b:Lcom/facebook/fbservice/service/OperationResult;

    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    invoke-static {v3, v4, p0, p1}, LX/EA9;->a$redex0(LX/EA9;Ljava/lang/String;Lcom/facebook/fbservice/service/OperationResult;LX/0am;)V

    .line 2085382
    return-void

    .line 2085383
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2085384
    check-cast v0, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    goto :goto_0

    .line 2085385
    :cond_4
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2085386
    check-cast v1, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel;->j()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$OverallRatingAndViewerReviewStoryModel$ViewerRecommendationModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    goto :goto_1
.end method
