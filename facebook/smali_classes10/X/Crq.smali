.class public final LX/Crq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Crn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1941416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1941417
    instance-of v0, p1, LX/Clq;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1941418
    check-cast v0, LX/Clq;

    .line 1941419
    invoke-interface {v0}, LX/Clq;->j()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, LX/Clq;->k()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1941420
    const/4 v0, 0x1

    move v1, v0

    .line 1941421
    :goto_0
    instance-of v0, p1, LX/Cm3;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/Cm3;

    invoke-interface {v0}, LX/Cm3;->a()LX/Clo;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/Cm3;

    invoke-interface {v0}, LX/Cm3;->a()LX/Clo;

    move-result-object v0

    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v0

    if-lez v0, :cond_1

    .line 1941422
    check-cast p1, LX/Cm3;

    .line 1941423
    invoke-interface {p1}, LX/Cm3;->a()LX/Clo;

    move-result-object v0

    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v4

    move v3, v2

    move v2, v1

    .line 1941424
    :goto_1
    if-ge v3, v4, :cond_0

    .line 1941425
    invoke-interface {p1}, LX/Cm3;->a()LX/Clo;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/Clo;->a(I)LX/Clr;

    move-result-object v1

    .line 1941426
    instance-of v0, v1, LX/Clq;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, LX/Clq;

    invoke-interface {v0}, LX/Clq;->j()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v1, LX/Clq;

    invoke-interface {v1}, LX/Clq;->k()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1941427
    add-int/lit8 v0, v2, 0x1

    .line 1941428
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    :cond_0
    move v1, v2

    .line 1941429
    :cond_1
    return v1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_0
.end method
