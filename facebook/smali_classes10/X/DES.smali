.class public LX/DES;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1976981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1976982
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;LX/162;LX/1Qt;Z)Landroid/os/Bundle;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 1976983
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1976984
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1976985
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1976986
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    :goto_0
    if-ge v1, v7, :cond_1

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 1976987
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1976988
    if-eqz p4, :cond_0

    .line 1976989
    invoke-static {v0}, LX/DES;->a(Lcom/facebook/graphql/model/GraphQLUser;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1976990
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1976991
    :cond_1
    const-string v0, ","

    new-array v1, v9, [Ljava/lang/Object;

    aput-object v4, v1, v2

    invoke-static {v0, v1}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1976992
    const-string v1, "group_name"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976993
    const-string v1, "group_members"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976994
    const-string v0, "group_visibility"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976995
    const-string v1, "ref"

    sget-object v0, LX/1Qt;->GROUPS:LX/1Qt;

    if-ne p3, v0, :cond_5

    const-string v0, "GROUPS_YOU_SHOULD_CREATE_GROUPS_MALL"

    :goto_1
    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976996
    const-string v0, "suggestion_category"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976997
    const-string v0, "suggestion_identifier"

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976998
    const-string v0, "cache_id"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976999
    if-eqz p4, :cond_2

    .line 1977000
    const-string v0, "group_members_tokens"

    invoke-virtual {v3, v0, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1977001
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v0

    .line 1977002
    if-eqz v0, :cond_2

    .line 1977003
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->p()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->l()Lcom/facebook/graphql/model/GraphQLGroupPurpose;

    move-result-object v1

    .line 1977004
    const-string v4, "group_purpose_extra_key"

    new-instance v5, Lcom/facebook/work/groups/create/api/GroupPurpose;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupPurpose;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v8

    invoke-direct {v5, v6, v7, v1, v8}, Lcom/facebook/work/groups/create/api/GroupPurpose;-><init>(Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1977005
    const-string v1, "group_extra_settings"

    new-instance v4, Lcom/facebook/work/groups/create/api/GroupExtraSettings;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->a()Z

    move-result v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->j()Z

    move-result v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionExtraSetting;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v6, v0}, Lcom/facebook/work/groups/create/api/GroupExtraSettings;-><init>(ZZLjava/lang/String;)V

    invoke-virtual {v3, v1, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1977006
    :cond_2
    invoke-virtual {p2, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1977007
    const-string v0, "trackingcode_item"

    invoke-virtual {p2, v2}, LX/0lF;->a(I)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1977008
    :cond_3
    invoke-virtual {p2, v9}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1977009
    const-string v0, "trackingcode_unit"

    invoke-virtual {p2, v9}, LX/0lF;->a(I)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1977010
    :cond_4
    return-object v3

    .line 1977011
    :cond_5
    const-string v0, "GROUPS_YOU_SHOULD_CREATE_NEWS_FEED"

    goto/16 :goto_1
.end method

.method public static a(LX/1Qt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;LX/0tX;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/util/concurrent/ExecutorService;)Landroid/view/View$OnClickListener;
    .locals 8

    .prologue
    .line 1977012
    new-instance v0, LX/DEN;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, LX/DEN;-><init>(LX/1Qt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;LX/0tX;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/util/concurrent/ExecutorService;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;LX/17W;Landroid/content/Context;LX/17Q;LX/0Zb;LX/1Qt;Z)Landroid/view/View$OnClickListener;
    .locals 9

    .prologue
    .line 1976945
    new-instance v0, LX/DEM;

    move-object v1, p1

    move-object v2, p0

    move-object v3, p6

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, LX/DEM;-><init>(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;LX/1Qt;LX/17W;Landroid/content/Context;LX/17Q;LX/0Zb;Z)V

    return-object v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLUser;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1976980
    new-instance v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    new-instance v2, Lcom/facebook/user/model/Name;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->R()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v4, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-direct {v1, v2, v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    return-object v1
.end method

.method public static a(ILjava/lang/String;Landroid/content/res/Resources;)Ljava/lang/StringBuffer;
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 1976976
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1976977
    if-le p0, v4, :cond_0

    .line 1976978
    const-string v1, " + "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const v2, 0x7f0f00ba

    add-int/lit8 v3, p0, -0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    add-int/lit8 v6, p0, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p2, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1976979
    :cond_0
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;LX/1Qt;LX/17W;Landroid/content/Context;LX/17Q;LX/0Zb;Z)V
    .locals 2

    .prologue
    .line 1976963
    invoke-static {p0, p1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v0

    invoke-static {p0, p1, v0, p2, p7}, LX/DES;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;LX/162;LX/1Qt;Z)Landroid/os/Bundle;

    move-result-object v0

    .line 1976964
    sget-object v1, LX/0ax;->N:Ljava/lang/String;

    invoke-virtual {p3, p4, v1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 1976965
    invoke-static {p0, p1}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->name()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->n()Ljava/lang/String;

    move-result-object p4

    sget-object v0, LX/1Qt;->GROUPS:LX/1Qt;

    if-ne p2, v0, :cond_0

    const-string v0, "GROUPS_YOU_SHOULD_CREATE_GROUPS_MALL"

    .line 1976966
    :goto_0
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1976967
    const/4 p1, 0x0

    .line 1976968
    :goto_1
    move-object v0, p1

    .line 1976969
    invoke-interface {p6, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1976970
    return-void

    .line 1976971
    :cond_0
    const-string v0, "GROUPS_YOU_SHOULD_CREATE_NEWS_FEED"

    goto :goto_0

    .line 1976972
    :cond_1
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "gysc_click"

    invoke-direct {p1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "tracking"

    invoke-virtual {p1, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p0, "suggestion_type"

    invoke-virtual {p1, p0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p0, "suggestion_id"

    invoke-virtual {p1, p0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p0, "ref"

    invoke-virtual {p1, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p0, "native_newsfeed"

    .line 1976973
    iput-object p0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1976974
    move-object p1, p1

    .line 1976975
    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;LX/0tX;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 1976946
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p5, v0, v1}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976947
    new-instance v0, LX/4GK;

    invoke-direct {v0}, LX/4GK;-><init>()V

    .line 1976948
    const-string v1, "actor_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976949
    move-object v0, v0

    .line 1976950
    const-string v1, "ref"

    invoke-virtual {v0, v1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976951
    move-object v0, v0

    .line 1976952
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->n()Ljava/lang/String;

    move-result-object v1

    .line 1976953
    const-string p2, "suggestion_identifier"

    invoke-virtual {v0, p2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976954
    move-object v0, v0

    .line 1976955
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->o()Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupCreationSuggestionType;->name()Ljava/lang/String;

    move-result-object v1

    .line 1976956
    const-string p2, "suggestion_type"

    invoke-virtual {v0, p2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1976957
    move-object v0, v0

    .line 1976958
    new-instance v1, LX/DET;

    invoke-direct {v1}, LX/DET;-><init>()V

    move-object v1, v1

    .line 1976959
    const-string p2, "input"

    invoke-virtual {v1, p2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/DET;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1976960
    invoke-virtual {p4, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1976961
    new-instance v1, LX/DEO;

    invoke-direct {v1}, LX/DEO;-><init>()V

    invoke-static {v0, v1, p6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1976962
    return-void
.end method
