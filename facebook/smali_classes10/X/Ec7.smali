.class public final LX/Ec7;
.super LX/EWj;
.source ""

# interfaces
.implements LX/Ec6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/Ec7;",
        ">;",
        "LX/Ec6;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2145521
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2145522
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ec7;->c:LX/EWc;

    .line 2145523
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2145524
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2145525
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/Ec7;->c:LX/EWc;

    .line 2145526
    return-void
.end method

.method private d(LX/EWY;)LX/Ec7;
    .locals 1

    .prologue
    .line 2145527
    instance-of v0, p1, LX/Ec8;

    if-eqz v0, :cond_0

    .line 2145528
    check-cast p1, LX/Ec8;

    invoke-virtual {p0, p1}, LX/Ec7;->a(LX/Ec8;)LX/Ec7;

    move-result-object p0

    .line 2145529
    :goto_0
    return-object p0

    .line 2145530
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/Ec7;
    .locals 4

    .prologue
    .line 2145531
    const/4 v2, 0x0

    .line 2145532
    :try_start_0
    sget-object v0, LX/Ec8;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ec8;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2145533
    if-eqz v0, :cond_0

    .line 2145534
    invoke-virtual {p0, v0}, LX/Ec7;->a(LX/Ec8;)LX/Ec7;

    .line 2145535
    :cond_0
    return-object p0

    .line 2145536
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2145537
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2145538
    check-cast v0, LX/Ec8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2145539
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2145540
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2145541
    invoke-virtual {p0, v1}, LX/Ec7;->a(LX/Ec8;)LX/Ec7;

    :cond_1
    throw v0

    .line 2145542
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method public static w()LX/Ec7;
    .locals 1

    .prologue
    .line 2145543
    new-instance v0, LX/Ec7;

    invoke-direct {v0}, LX/Ec7;-><init>()V

    return-object v0
.end method

.method private x()LX/Ec7;
    .locals 2

    .prologue
    .line 2145544
    invoke-static {}, LX/Ec7;->w()LX/Ec7;

    move-result-object v0

    invoke-virtual {p0}, LX/Ec7;->m()LX/Ec8;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ec7;->a(LX/Ec8;)LX/Ec7;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2145545
    invoke-direct {p0, p1}, LX/Ec7;->d(LX/EWY;)LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2145546
    invoke-direct {p0, p1, p2}, LX/Ec7;->d(LX/EWd;LX/EYZ;)LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/Ec7;
    .locals 1

    .prologue
    .line 2145547
    iget v0, p0, LX/Ec7;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ec7;->a:I

    .line 2145548
    iput p1, p0, LX/Ec7;->b:I

    .line 2145549
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145550
    return-object p0
.end method

.method public final a(LX/EWc;)LX/Ec7;
    .locals 1

    .prologue
    .line 2145486
    if-nez p1, :cond_0

    .line 2145487
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2145488
    :cond_0
    iget v0, p0, LX/Ec7;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/Ec7;->a:I

    .line 2145489
    iput-object p1, p0, LX/Ec7;->c:LX/EWc;

    .line 2145490
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2145491
    return-object p0
.end method

.method public final a(LX/Ec8;)LX/Ec7;
    .locals 2

    .prologue
    .line 2145551
    sget-object v0, LX/Ec8;->c:LX/Ec8;

    move-object v0, v0

    .line 2145552
    if-ne p1, v0, :cond_0

    .line 2145553
    :goto_0
    return-object p0

    .line 2145554
    :cond_0
    const/4 v0, 0x1

    .line 2145555
    iget v1, p1, LX/Ec8;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_3

    :goto_1
    move v0, v0

    .line 2145556
    if-eqz v0, :cond_1

    .line 2145557
    iget v0, p1, LX/Ec8;->iteration_:I

    move v0, v0

    .line 2145558
    invoke-virtual {p0, v0}, LX/Ec7;->a(I)LX/Ec7;

    .line 2145559
    :cond_1
    iget v0, p1, LX/Ec8;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2145560
    if-eqz v0, :cond_2

    .line 2145561
    iget-object v0, p1, LX/Ec8;->seed_:LX/EWc;

    move-object v0, v0

    .line 2145562
    invoke-virtual {p0, v0}, LX/Ec7;->a(LX/EWc;)LX/Ec7;

    .line 2145563
    :cond_2
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2145564
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2145565
    invoke-direct {p0, p1, p2}, LX/Ec7;->d(LX/EWd;LX/EYZ;)LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2145520
    invoke-direct {p0}, LX/Ec7;->x()LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2145566
    invoke-direct {p0, p1, p2}, LX/Ec7;->d(LX/EWd;LX/EYZ;)LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2145519
    invoke-direct {p0}, LX/Ec7;->x()LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2145518
    invoke-direct {p0, p1}, LX/Ec7;->d(LX/EWY;)LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2145517
    invoke-direct {p0}, LX/Ec7;->x()LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2145516
    sget-object v0, LX/Eck;->x:LX/EYn;

    const-class v1, LX/Ec8;

    const-class v2, LX/Ec7;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2145515
    sget-object v0, LX/Eck;->w:LX/EYF;

    return-object v0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2145514
    invoke-direct {p0}, LX/Ec7;->x()LX/Ec7;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2145513
    invoke-virtual {p0}, LX/Ec7;->m()LX/Ec8;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2145512
    invoke-virtual {p0}, LX/Ec7;->l()LX/Ec8;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2145511
    invoke-virtual {p0}, LX/Ec7;->m()LX/Ec8;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2145510
    invoke-virtual {p0}, LX/Ec7;->l()LX/Ec8;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Ec8;
    .locals 2

    .prologue
    .line 2145506
    invoke-virtual {p0}, LX/Ec7;->m()LX/Ec8;

    move-result-object v0

    .line 2145507
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2145508
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2145509
    :cond_0
    return-object v0
.end method

.method public final m()LX/Ec8;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2145494
    new-instance v2, LX/Ec8;

    invoke-direct {v2, p0}, LX/Ec8;-><init>(LX/EWj;)V

    .line 2145495
    iget v3, p0, LX/Ec7;->a:I

    .line 2145496
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_1

    .line 2145497
    :goto_0
    iget v1, p0, LX/Ec7;->b:I

    .line 2145498
    iput v1, v2, LX/Ec8;->iteration_:I

    .line 2145499
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 2145500
    or-int/lit8 v0, v0, 0x2

    .line 2145501
    :cond_0
    iget-object v1, p0, LX/Ec7;->c:LX/EWc;

    .line 2145502
    iput-object v1, v2, LX/Ec8;->seed_:LX/EWc;

    .line 2145503
    iput v0, v2, LX/Ec8;->bitField0_:I

    .line 2145504
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2145505
    return-object v2

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145492
    sget-object v0, LX/Ec8;->c:LX/Ec8;

    move-object v0, v0

    .line 2145493
    return-object v0
.end method
