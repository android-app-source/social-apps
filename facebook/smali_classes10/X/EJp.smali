.class public final LX/EJp;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

.field public final synthetic b:LX/Cxo;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/Cxo;)V
    .locals 1

    .prologue
    .line 2105328
    iput-object p1, p0, LX/EJp;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    iput-object p2, p0, LX/EJp;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    iput-object p3, p0, LX/EJp;->b:LX/Cxo;

    invoke-direct {p0}, LX/2eI;-><init>()V

    .line 2105329
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EJp;->d:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2105312
    iget-object v1, p0, LX/EJp;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105313
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2105314
    add-int/lit8 v0, v1, 0x1

    .line 2105315
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2105316
    :cond_0
    add-int/lit8 v0, v1, 0x1

    .line 2105317
    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final a(LX/2eI;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2105322
    iget-object v0, p0, LX/EJp;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105323
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2105324
    iget-object v4, p0, LX/EJp;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    iget-object v4, v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;

    invoke-virtual {p1, v4, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2105325
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2105326
    :cond_1
    iget-object v0, p0, LX/EJp;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->g:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;

    iget-object v1, p0, LX/EJp;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2105327
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2105318
    if-ltz p1, :cond_0

    iget-boolean v0, p0, LX/EJp;->d:Z

    if-nez v0, :cond_0

    .line 2105319
    iget-object v0, p0, LX/EJp;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->h:LX/CvY;

    iget-object v1, p0, LX/EJp;->b:LX/Cxo;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CvY;->d(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2105320
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EJp;->d:Z

    .line 2105321
    :cond_0
    return-void
.end method
