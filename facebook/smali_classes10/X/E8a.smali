.class public final LX/E8a;
.super LX/5Mq;
.source ""


# instance fields
.field public final synthetic a:F

.field public final synthetic b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;LX/195;F)V
    .locals 0

    .prologue
    .line 2082816
    iput-object p1, p0, LX/E8a;->b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iput p3, p0, LX/E8a;->a:F

    invoke-direct {p0, p2}, LX/5Mq;-><init>(LX/195;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 8

    .prologue
    .line 2082817
    iget-object v0, p0, LX/E8a;->b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-object v0, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->n:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 2082818
    if-lez p3, :cond_0

    iget-object v2, p0, LX/E8a;->b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-object v2, v2, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    .line 2082819
    iget-boolean v3, v2, LX/2ja;->m:Z

    move v2, v3

    .line 2082820
    if-eqz v2, :cond_0

    .line 2082821
    iget-object v2, p0, LX/E8a;->b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-object v2, v2, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    iget-object v3, p0, LX/E8a;->b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-wide v4, v3, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->I:J

    sub-long v4, v0, v4

    iget-object v3, p0, LX/E8a;->b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-wide v6, v3, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->O:J

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, LX/2ja;->c(J)V

    .line 2082822
    :cond_0
    iget-object v2, p0, LX/E8a;->b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget-object v2, v2, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->T:LX/2ja;

    .line 2082823
    invoke-static {v2, v0, v1}, LX/2ja;->g(LX/2ja;J)V

    .line 2082824
    iget-object v0, p0, LX/E8a;->b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    iget v1, p0, LX/E8a;->a:F

    .line 2082825
    iget-object v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    if-eqz v2, :cond_3

    .line 2082826
    const/4 v2, 0x0

    .line 2082827
    iget-object v3, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Y:LX/1P0;

    invoke-virtual {v3}, LX/1P1;->l()I

    move-result v3

    .line 2082828
    iget-object v4, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v4}, LX/E8m;->i()I

    move-result v4

    .line 2082829
    iget-object v5, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Y:LX/1P0;

    invoke-virtual {v5, v2}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v5

    .line 2082830
    if-ge v3, v4, :cond_1

    const/4 v2, 0x1

    .line 2082831
    :cond_1
    if-eqz v2, :cond_4

    if-eqz v5, :cond_4

    .line 2082832
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v0, v3}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->b(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;F)V

    .line 2082833
    iget-object v3, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->ab:Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3, v5}, Lcom/facebook/reaction/ui/welcomeheader/ReactionWelcomeHeaderView;->a(F)V

    .line 2082834
    :goto_0
    invoke-static {v0, v4, v2, v1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->a(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;IZF)V

    .line 2082835
    :goto_1
    iget-object v0, p0, LX/E8a;->b:Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;

    .line 2082836
    iget-object v1, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Y:LX/1P0;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v1

    .line 2082837
    iget-object v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v2}, LX/E8m;->f()I

    move-result v2

    .line 2082838
    iget-object v3, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    sub-int/2addr v1, v2

    invoke-virtual {v3, v1}, LX/E8m;->e(I)I

    move-result v1

    .line 2082839
    iget-object v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    iget-object v3, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v3}, LX/E8m;->d()I

    move-result v3

    invoke-virtual {v2, v3}, LX/E8m;->e(I)I

    move-result v2

    .line 2082840
    add-int/lit8 v2, v2, -0x5

    if-lt v1, v2, :cond_2

    .line 2082841
    iget-object v1, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->h()V

    .line 2082842
    :cond_2
    return-void

    .line 2082843
    :cond_3
    const/4 v3, 0x0

    .line 2082844
    iget-object v2, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->X:LX/E8m;

    iget-object v4, v0, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->Y:LX/1P0;

    invoke-virtual {v4}, LX/1P1;->l()I

    move-result v4

    invoke-virtual {v2, v4}, LX/E8m;->e(I)I

    move-result v2

    .line 2082845
    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_2
    invoke-static {v0, v3, v2, v1}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->a(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;IZF)V

    .line 2082846
    goto :goto_1

    .line 2082847
    :cond_4
    const/4 v3, 0x0

    invoke-static {v0, v3}, Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;->b(Lcom/facebook/reaction/ui/fragment/ReactionDialogFragment;F)V

    goto :goto_0

    :cond_5
    move v2, v3

    .line 2082848
    goto :goto_2
.end method
