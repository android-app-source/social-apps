.class public final LX/EMQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/CxA;

.field public final synthetic b:LX/CzL;

.field public final synthetic c:LX/CzL;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;LX/CxA;LX/CzL;LX/CzL;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2110613
    iput-object p1, p0, LX/EMQ;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iput-object p2, p0, LX/EMQ;->a:LX/CxA;

    iput-object p3, p0, LX/EMQ;->b:LX/CzL;

    iput-object p4, p0, LX/EMQ;->c:LX/CzL;

    iput-object p5, p0, LX/EMQ;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2110614
    iget-object v0, p0, LX/EMQ;->a:LX/CxA;

    iget-object v1, p0, LX/EMQ;->b:LX/CzL;

    invoke-interface {v0, v1}, LX/CxA;->a(LX/CzL;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110615
    iget-object v0, p0, LX/EMQ;->a:LX/CxA;

    iget-object v1, p0, LX/EMQ;->b:LX/CzL;

    iget-object v2, p0, LX/EMQ;->c:LX/CzL;

    invoke-interface {v0, v1, v2}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2110616
    iget-object v0, p0, LX/EMQ;->a:LX/CxA;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2110617
    :cond_0
    iget-object v0, p0, LX/EMQ;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "warn_override_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/EMQ;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/EMO;

    invoke-direct {v2, p0}, LX/EMO;-><init>(LX/EMQ;)V

    new-instance v3, LX/EMP;

    invoke-direct {v3, p0}, LX/EMP;-><init>(LX/EMQ;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2110618
    return-void
.end method
