.class public LX/Dw4;
.super Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;
.source ""


# instance fields
.field public k:D

.field public l:D

.field public m:D

.field public n:D

.field private o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 2059945
    invoke-direct {p0, p1}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;-><init>(Landroid/content/Context;)V

    .line 2059946
    iput-wide v0, p0, LX/Dw4;->k:D

    .line 2059947
    iput-wide v0, p0, LX/Dw4;->l:D

    .line 2059948
    iput-wide v0, p0, LX/Dw4;->m:D

    .line 2059949
    iput-wide v0, p0, LX/Dw4;->n:D

    .line 2059950
    invoke-virtual {p0}, LX/Dw4;->a()V

    .line 2059951
    return-void
.end method

.method private static a(LX/Dw4;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;I)V
    .locals 13

    .prologue
    .line 2059952
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->K()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2059953
    :cond_0
    :goto_0
    return-void

    .line 2059954
    :cond_1
    const/4 v12, 0x0

    .line 2059955
    if-nez p2, :cond_2

    .line 2059956
    new-instance v6, Landroid/graphics/Rect;

    iget-wide v8, p0, LX/Dw4;->n:D

    double-to-int v7, v8

    iget-wide v8, p0, LX/Dw4;->m:D

    double-to-int v8, v8

    invoke-direct {v6, v12, v12, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2059957
    :goto_1
    move-object v1, v6

    .line 2059958
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->K()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2059959
    iget-object v3, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {p0}, LX/Dw4;->getNumOfItems()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    const-string v5, "LoadPortraitImageThumbnail"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Landroid/graphics/Rect;Landroid/net/Uri;Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v6, Landroid/graphics/Rect;

    iget-wide v8, p0, LX/Dw4;->k:D

    iget-wide v10, p0, LX/Dw4;->l:D

    add-double/2addr v8, v10

    double-to-int v7, v8

    iget-wide v8, p0, LX/Dw4;->k:D

    iget-wide v10, p0, LX/Dw4;->l:D

    add-double/2addr v8, v10

    iget-wide v10, p0, LX/Dw4;->n:D

    add-double/2addr v8, v10

    double-to-int v8, v8

    iget-wide v10, p0, LX/Dw4;->m:D

    double-to-int v9, v10

    invoke-direct {v6, v7, v12, v8, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_1
.end method

.method public static b(LX/Dw4;II)Landroid/graphics/Rect;
    .locals 10

    .prologue
    .line 2059903
    const/4 v0, 0x0

    .line 2059904
    if-eqz p1, :cond_0

    .line 2059905
    int-to-double v0, p1

    iget-wide v2, p0, LX/Dw4;->k:D

    iget-wide v4, p0, LX/Dw4;->l:D

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 2059906
    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    int-to-double v2, p2

    iget-wide v4, p0, LX/Dw4;->l:D

    iget-wide v6, p0, LX/Dw4;->k:D

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-int v2, v2

    int-to-double v4, v0

    iget-wide v6, p0, LX/Dw4;->l:D

    add-double/2addr v4, v6

    double-to-int v3, v4

    int-to-double v4, p2

    iget-wide v6, p0, LX/Dw4;->l:D

    iget-wide v8, p0, LX/Dw4;->k:D

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    iget-wide v6, p0, LX/Dw4;->l:D

    add-double/2addr v4, v6

    double-to-int v4, v4

    invoke-direct {v1, v0, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method

.method private b(I)V
    .locals 13

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 2059932
    move v1, v2

    move v3, v2

    .line 2059933
    :goto_0
    iget-object v0, p0, LX/Dw4;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2059934
    if-eq p1, v1, :cond_2

    .line 2059935
    if-ge p1, v5, :cond_1

    move v4, v5

    :goto_1
    add-int/lit8 v6, v3, 0x1

    iget-object v0, p0, LX/Dw4;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2059936
    if-eqz v0, :cond_0

    iget-object v7, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    if-eqz v7, :cond_0

    iget-object v7, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v7}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    if-nez v7, :cond_5

    .line 2059937
    :cond_0
    :goto_2
    move v3, v6

    .line 2059938
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v4, v2

    .line 2059939
    goto :goto_1

    .line 2059940
    :cond_2
    iget-object v0, p0, LX/Dw4;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    if-ge p1, v5, :cond_3

    move v4, v2

    :goto_4
    invoke-static {p0, v0, v4}, LX/Dw4;->a(LX/Dw4;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;I)V

    goto :goto_3

    :cond_3
    const/4 v4, 0x1

    goto :goto_4

    .line 2059941
    :cond_4
    return-void

    .line 2059942
    :cond_5
    invoke-static {p0, v4, v3}, LX/Dw4;->b(LX/Dw4;II)Landroid/graphics/Rect;

    move-result-object v8

    .line 2059943
    iget-object v7, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v7}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->J()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 2059944
    iget-object v10, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    const-string v12, "LoadSmallImageThumbnail"

    move-object v7, p0

    move v11, v3

    invoke-virtual/range {v7 .. v12}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Landroid/graphics/Rect;Landroid/net/Uri;Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;ILjava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 2059960
    invoke-super {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a()V

    .line 2059961
    invoke-virtual {p0}, LX/Dw4;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2059962
    invoke-virtual {p0}, LX/Dw4;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b48

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-double v2, v1

    iput-wide v2, p0, LX/Dw4;->k:D

    .line 2059963
    int-to-double v0, v0

    iget-wide v2, p0, LX/Dw4;->k:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    div-double/2addr v0, v6

    iput-wide v0, p0, LX/Dw4;->l:D

    .line 2059964
    iget-wide v0, p0, LX/Dw4;->l:D

    mul-double/2addr v0, v6

    iget-wide v2, p0, LX/Dw4;->k:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/Dw4;->m:D

    .line 2059965
    iget-wide v0, p0, LX/Dw4;->l:D

    mul-double/2addr v0, v4

    iget-wide v2, p0, LX/Dw4;->k:D

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/Dw4;->n:D

    .line 2059966
    return-void
.end method

.method public final a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V
    .locals 2
    .param p8    # LX/Dw7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2059911
    invoke-super/range {p0 .. p8}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V

    .line 2059912
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2059913
    :cond_0
    :goto_0
    return-void

    .line 2059914
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c()V

    .line 2059915
    iput-object p1, p0, LX/Dw4;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    .line 2059916
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2059917
    if-eqz v0, :cond_0

    .line 2059918
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 2059919
    iget-object p2, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p2

    const/4 p3, 0x1

    if-ne p2, p3, :cond_2

    move v0, v1

    .line 2059920
    :goto_1
    move v0, v0

    .line 2059921
    invoke-virtual {p0}, LX/Dw4;->forceLayout()V

    .line 2059922
    invoke-direct {p0, v0}, LX/Dw4;->b(I)V

    .line 2059923
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->b()V

    goto :goto_0

    .line 2059924
    :cond_2
    iget-object p4, p1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {p4}, LX/0Px;->size()I

    move-result p5

    move p3, v0

    move p2, v0

    .line 2059925
    :goto_2
    if-ge p3, p5, :cond_4

    .line 2059926
    invoke-virtual {p4, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2059927
    iget-boolean v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    if-eqz v0, :cond_3

    move v0, p2

    .line 2059928
    goto :goto_1

    .line 2059929
    :cond_3
    add-int/lit8 p2, p2, 0x1

    .line 2059930
    add-int/lit8 v0, p3, 0x1

    move p3, v0

    goto :goto_2

    :cond_4
    move v0, v1

    .line 2059931
    goto :goto_1
.end method

.method public final getNumOfItems()I
    .locals 1

    .prologue
    .line 2059910
    const/4 v0, 0x4

    return v0
.end method

.method public getRowHeight()I
    .locals 2

    .prologue
    .line 2059907
    iget-object v0, p0, LX/Dw4;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dw4;->o:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    if-nez v0, :cond_1

    .line 2059908
    :cond_0
    const/4 v0, 0x0

    .line 2059909
    :goto_0
    return v0

    :cond_1
    iget-wide v0, p0, LX/Dw4;->m:D

    double-to-int v0, v0

    goto :goto_0
.end method
