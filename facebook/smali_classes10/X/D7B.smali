.class public final LX/D7B;
.super Landroid/os/CountDownTimer;
.source ""


# instance fields
.field public final synthetic a:LX/D7C;

.field private final b:J


# direct methods
.method private constructor <init>(LX/D7C;JJ)V
    .locals 2

    .prologue
    .line 1966818
    iput-object p1, p0, LX/D7B;->a:LX/D7C;

    .line 1966819
    const-wide/16 v0, 0x2a

    invoke-direct {p0, p4, p5, v0, v1}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 1966820
    iput-wide p2, p0, LX/D7B;->b:J

    .line 1966821
    return-void
.end method

.method public synthetic constructor <init>(LX/D7C;JJB)V
    .locals 0

    .prologue
    .line 1966822
    invoke-direct/range {p0 .. p5}, LX/D7B;-><init>(LX/D7C;JJ)V

    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 0

    .prologue
    .line 1966823
    return-void
.end method

.method public final onTick(J)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1966824
    iget-wide v0, p0, LX/D7B;->b:J

    sub-long/2addr v0, p1

    long-to-int v0, v0

    .line 1966825
    iget-object v1, p0, LX/D7B;->a:LX/D7C;

    iget-object v1, v1, LX/D7C;->b:Landroid/widget/ProgressBar;

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-wide v2, p0, LX/D7B;->b:J

    long-to-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1966826
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    long-to-int v0, v0

    .line 1966827
    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1966828
    :goto_0
    iget-object v1, p0, LX/D7B;->a:LX/D7C;

    invoke-virtual {v1}, LX/D7C;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080de0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1966829
    iget-object v1, p0, LX/D7B;->a:LX/D7C;

    iget-object v1, v1, LX/D7C;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1966830
    iget-object v1, p0, LX/D7B;->a:LX/D7C;

    iget-object v1, v1, LX/D7C;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1966831
    :cond_0
    return-void

    .line 1966832
    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
