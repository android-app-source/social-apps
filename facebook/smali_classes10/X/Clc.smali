.class public final LX/Clc;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "LX/Clb;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1932576
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 1932577
    sget-object v0, LX/Clb;->KICKER:LX/Clb;

    const v1, 0x7f0e078a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932578
    sget-object v0, LX/Clb;->TITLE:LX/Clb;

    const v1, 0x7f0e078b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932579
    sget-object v0, LX/Clb;->SUBTITLE:LX/Clb;

    const v1, 0x7f0e078c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932580
    sget-object v0, LX/Clb;->HEADER_ONE:LX/Clb;

    const v1, 0x7f0e078d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932581
    sget-object v0, LX/Clb;->HEADER_TWO:LX/Clb;

    const v1, 0x7f0e078e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932582
    sget-object v0, LX/Clb;->BODY:LX/Clb;

    const v1, 0x7f0e0789

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932583
    sget-object v0, LX/Clb;->PULL_QUOTE:LX/Clb;

    const v1, 0x7f0e0790

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932584
    sget-object v0, LX/Clb;->PULL_QUOTE_ATTRIBUTION:LX/Clb;

    const v1, 0x7f0e0791

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932585
    sget-object v0, LX/Clb;->BLOCK_QUOTE:LX/Clb;

    const v1, 0x7f0e078f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932586
    sget-object v0, LX/Clb;->CODE:LX/Clb;

    const v1, 0x7f0e0792

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932587
    sget-object v0, LX/Clb;->RELATED_ARTICLES:LX/Clb;

    const v1, 0x7f0e079b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932588
    sget-object v0, LX/Clb;->RELATED_ARTICLES_HEADER:LX/Clb;

    const v1, 0x7f0e07af

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932589
    sget-object v0, LX/Clb;->INLINE_RELATED_ARTICLES_HEADER:LX/Clb;

    const v1, 0x7f0e07af

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932590
    sget-object v0, LX/Clb;->BYLINE:LX/Clb;

    const v1, 0x7f0e0793

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932591
    sget-object v0, LX/Clb;->CREDITS:LX/Clb;

    const v1, 0x7f0e0799

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932592
    sget-object v0, LX/Clb;->AUTHORS_CONTRIBUTORS_HEADER:LX/Clb;

    const v1, 0x7f0e07ae

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932593
    sget-object v0, LX/Clb;->COPYRIGHT:LX/Clb;

    const v1, 0x7f0e0799

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Clc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932594
    return-void
.end method
