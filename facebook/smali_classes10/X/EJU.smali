.class public final LX/EJU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Aj7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Aj7",
        "<",
        "LX/ByV;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

.field public final synthetic b:LX/1Ps;

.field public final synthetic c:Ljava/util/HashMap;

.field public final synthetic d:LX/0Px;

.field public final synthetic e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/1Ps;Ljava/util/HashMap;LX/0Px;)V
    .locals 0

    .prologue
    .line 2104168
    iput-object p1, p0, LX/EJU;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    iput-object p2, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    iput-object p3, p0, LX/EJU;->b:LX/1Ps;

    iput-object p4, p0, LX/EJU;->c:Ljava/util/HashMap;

    iput-object p5, p0, LX/EJU;->d:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/26N;I)V
    .locals 12

    .prologue
    .line 2104169
    check-cast p2, LX/ByV;

    .line 2104170
    iget-object v0, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->b(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 2104171
    iget-object v0, p0, LX/EJU;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    iget-object v8, v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->o:LX/CvY;

    iget-object v0, p0, LX/EJU;->b:LX/1Ps;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    iget-object v0, p0, LX/EJU;->b:LX/1Ps;

    check-cast v0, LX/CxG;

    iget-object v1, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-interface {v0, v1}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v10

    iget-object v11, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2104172
    iget-object v0, p0, LX/EJU;->b:LX/1Ps;

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    iget-object v1, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    iget-object v2, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    iget-object v3, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2104173
    iget-object v5, v4, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v4, v5

    .line 2104174
    invoke-virtual {v4}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    iget-object v5, p0, LX/EJU;->b:LX/1Ps;

    check-cast v5, LX/CxG;

    iget-object v6, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-interface {v5, v6}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v5

    iget-object v6, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v6}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    const/4 v7, 0x3

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    iget-object v7, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v7}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->u()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static/range {v0 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;IILcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v8, v9, v10, v11, v0}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2104175
    iget-object v0, p0, LX/EJU;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->m:LX/1nD;

    iget-object v1, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    iget-object v2, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->k()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, LX/EJU;->a:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->n()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, LX/8ci;->u:LX/8ci;

    iget-object v6, p0, LX/EJU;->b:LX/1Ps;

    check-cast v6, LX/CxV;

    invoke-interface {v6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    .line 2104176
    iget-object v7, v6, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v6, v7

    .line 2104177
    invoke-virtual/range {v0 .. v6}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v1

    .line 2104178
    iget-object v0, p0, LX/EJU;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    iget-object v2, v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/EJU;->b:LX/1Ps;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2104179
    :goto_0
    return-void

    .line 2104180
    :cond_0
    invoke-virtual {p2}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-ne v0, v1, :cond_1

    .line 2104181
    iget-object v0, p0, LX/EJU;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    iget-object v1, v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->p:LX/D3w;

    invoke-virtual {p2}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    iget-object v0, p0, LX/EJU;->b:LX/1Ps;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v3, LX/04D;->RESULTS_PAGE_MIXED_MEDIA:LX/04D;

    invoke-virtual {v1, v2, v0, v3}, LX/D3w;->a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;)V

    .line 2104182
    :goto_1
    iget-object v0, p0, LX/EJU;->b:LX/1Ps;

    check-cast v0, LX/Cxe;

    iget-object v2, p0, LX/EJU;->c:Ljava/util/HashMap;

    iget-object v1, p0, LX/EJU;->d:LX/0Px;

    invoke-virtual {v1, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104183
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 2104184
    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v0, v1}, LX/Cxe;->c(Lcom/facebook/graphql/model/GraphQLNode;)V

    goto :goto_0

    .line 2104185
    :cond_1
    invoke-virtual {p2}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x1eaef984

    if-ne v0, v1, :cond_2

    .line 2104186
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2104187
    iget-object v2, p0, LX/EJU;->c:Ljava/util/HashMap;

    iget-object v0, p0, LX/EJU;->d:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104188
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2104189
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->cV()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2104190
    iget-object v0, p0, LX/EJU;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    iget-object v2, v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/EJU;->b:LX/1Ps;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 2104191
    :cond_2
    iget-object v0, p0, LX/EJU;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->h:LX/1qa;

    invoke-virtual {p2}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    sget-object v2, LX/26P;->Photo:LX/26P;

    invoke-virtual {v0, v1, v2}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v2

    .line 2104192
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2104193
    iget-object v0, p0, LX/EJU;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_4

    iget-object v0, p0, LX/EJU;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104194
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 2104195
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2104196
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, 0x4984e12

    if-ne v5, v6, :cond_3

    .line 2104197
    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/5kD;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2104198
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2104199
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/9hF;->e(LX/0Px;)LX/9hE;

    move-result-object v1

    iget-object v0, p0, LX/EJU;->d:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104200
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2104201
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    sget-object v1, LX/74S;->SEARCH_PHOTOS_GRID_MODULE:LX/74S;

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    const/4 v1, 0x1

    .line 2104202
    iput-boolean v1, v0, LX/9hD;->C:Z

    .line 2104203
    move-object v0, v0

    .line 2104204
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v0

    .line 2104205
    iget-object v1, p0, LX/EJU;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->i:LX/23R;

    invoke-virtual {p1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p1, v2, p3}, LX/BrA;->a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/1bf;I)LX/9hN;

    move-result-object v2

    invoke-interface {v1, v3, v0, v2}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    goto/16 :goto_1
.end method
