.class public final LX/E4i;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E4j;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

.field public e:Ljava/lang/String;

.field public f:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic h:LX/E4j;


# direct methods
.method public constructor <init>(LX/E4j;)V
    .locals 1

    .prologue
    .line 2077095
    iput-object p1, p0, LX/E4i;->h:LX/E4j;

    .line 2077096
    move-object v0, p1

    .line 2077097
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2077098
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2077099
    const-string v0, "ReactionAdinterfacesObjectiveBlockComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2077100
    if-ne p0, p1, :cond_1

    .line 2077101
    :cond_0
    :goto_0
    return v0

    .line 2077102
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2077103
    goto :goto_0

    .line 2077104
    :cond_3
    check-cast p1, LX/E4i;

    .line 2077105
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2077106
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2077107
    if-eq v2, v3, :cond_0

    .line 2077108
    iget-object v2, p0, LX/E4i;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E4i;->a:Ljava/lang/String;

    iget-object v3, p1, LX/E4i;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2077109
    goto :goto_0

    .line 2077110
    :cond_5
    iget-object v2, p1, LX/E4i;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2077111
    :cond_6
    iget-object v2, p0, LX/E4i;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E4i;->b:Ljava/lang/String;

    iget-object v3, p1, LX/E4i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2077112
    goto :goto_0

    .line 2077113
    :cond_8
    iget-object v2, p1, LX/E4i;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2077114
    :cond_9
    iget-object v2, p0, LX/E4i;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E4i;->c:Ljava/lang/String;

    iget-object v3, p1, LX/E4i;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2077115
    goto :goto_0

    .line 2077116
    :cond_b
    iget-object v2, p1, LX/E4i;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2077117
    :cond_c
    iget-object v2, p0, LX/E4i;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E4i;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p1, LX/E4i;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2077118
    goto :goto_0

    .line 2077119
    :cond_e
    iget-object v2, p1, LX/E4i;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-nez v2, :cond_d

    .line 2077120
    :cond_f
    iget-object v2, p0, LX/E4i;->e:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/E4i;->e:Ljava/lang/String;

    iget-object v3, p1, LX/E4i;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 2077121
    goto :goto_0

    .line 2077122
    :cond_11
    iget-object v2, p1, LX/E4i;->e:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 2077123
    :cond_12
    iget-object v2, p0, LX/E4i;->f:LX/2km;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/E4i;->f:LX/2km;

    iget-object v3, p1, LX/E4i;->f:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 2077124
    goto/16 :goto_0

    .line 2077125
    :cond_14
    iget-object v2, p1, LX/E4i;->f:LX/2km;

    if-nez v2, :cond_13

    .line 2077126
    :cond_15
    iget-object v2, p0, LX/E4i;->g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/E4i;->g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/E4i;->g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2077127
    goto/16 :goto_0

    .line 2077128
    :cond_16
    iget-object v2, p1, LX/E4i;->g:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
