.class public final LX/DTd;
.super LX/DRr;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V
    .locals 0

    .prologue
    .line 2000652
    iput-object p1, p0, LX/DTd;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    invoke-direct {p0}, LX/DRr;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2000653
    check-cast p1, LX/DTi;

    .line 2000654
    iget-object v0, p1, LX/DTi;->a:Ljava/lang/String;

    iget-object v1, p0, LX/DTd;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2000655
    :cond_0
    :goto_0
    return-void

    .line 2000656
    :cond_1
    iget-object v0, p1, LX/DTi;->b:Ljava/lang/String;

    iget-object v1, p0, LX/DTd;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2000657
    iget-object v0, p0, LX/DTd;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->n:LX/DTb;

    iget-object v1, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2000658
    iget-object v2, v0, LX/DTb;->b:Landroid/os/Bundle;

    const-string v3, "group_admin_type"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000659
    iget-object v0, p0, LX/DTd;->a:Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    .line 2000660
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2000661
    if-eqz v0, :cond_0

    .line 2000662
    const-string v1, "group_admin_type"

    iget-object v2, p1, LX/DTi;->c:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
