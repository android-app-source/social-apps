.class public final LX/DHX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/25K;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStorySet;

.field public final synthetic b:LX/DHY;


# direct methods
.method public constructor <init>(LX/DHY;Lcom/facebook/graphql/model/GraphQLStorySet;)V
    .locals 0

    .prologue
    .line 1982057
    iput-object p1, p0, LX/DHX;->b:LX/DHY;

    iput-object p2, p0, LX/DHX;->a:Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILX/0Px;)V
    .locals 6

    .prologue
    .line 1982058
    iget-object v0, p0, LX/DHX;->a:Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0, p2, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/util/List;I)V

    .line 1982059
    iget-object v0, p0, LX/DHX;->b:LX/DHY;

    iget-object v0, v0, LX/DHY;->c:LX/1LV;

    iget-object v1, p0, LX/DHX;->a:Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v0, v1, p1}, LX/1LV;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 1982060
    iget-object v0, p0, LX/DHX;->a:Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_0

    .line 1982061
    iget-object v0, p0, LX/DHX;->b:LX/DHY;

    iget-object v0, v0, LX/DHY;->e:LX/2yK;

    sget-object v1, LX/0ig;->z:LX/0ih;

    iget-object v2, p0, LX/DHX;->a:Lcom/facebook/graphql/model/GraphQLStorySet;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "position:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/DHX;->a:Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;ILjava/lang/String;)V

    .line 1982062
    :cond_0
    return-void
.end method
