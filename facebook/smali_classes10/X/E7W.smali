.class public final LX/E7W;
.super LX/E7V;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/E7V",
        "<",
        "LX/1U8;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;

.field private m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 0

    .prologue
    .line 2081738
    iput-object p1, p0, LX/E7W;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;

    .line 2081739
    invoke-direct {p0, p2}, LX/E7V;-><init>(Landroid/view/View;)V

    .line 2081740
    iput-object p2, p0, LX/E7W;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2081741
    return-void
.end method


# virtual methods
.method public final a(LX/1U8;)V
    .locals 4

    .prologue
    .line 2081742
    invoke-interface {p1}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2081743
    iget-object v1, p0, LX/E7W;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2081744
    iget-object v0, p0, LX/E7W;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/E7W;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;

    iget-object v1, v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->b:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;

    iget-object v2, p0, LX/E7W;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;

    iget-object v2, v2, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->c:Ljava/lang/String;

    iget-object v3, p0, LX/E7W;->l:Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;

    iget-object v3, v3, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionDefaultPhotosRecyclerAdapter;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, p1}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->a(Ljava/lang/String;Ljava/lang/String;LX/1U8;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2081745
    return-void
.end method
