.class public final LX/DaA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DaD;


# direct methods
.method public constructor <init>(LX/DaD;)V
    .locals 0

    .prologue
    .line 2014623
    iput-object p1, p0, LX/DaA;->a:LX/DaD;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2014609
    iget-object v0, p0, LX/DaA;->a:LX/DaD;

    iget-object v0, v0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    invoke-static {v0}, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->k(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V

    .line 2014610
    iget-object v0, p0, LX/DaA;->a:LX/DaD;

    iget-object v0, v0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v0, v0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->l:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081b61

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2014611
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2014612
    check-cast p1, Ljava/lang/String;

    .line 2014613
    iget-object v0, p0, LX/DaA;->a:LX/DaD;

    iget-object v0, v0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    invoke-static {v0}, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->k(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V

    .line 2014614
    iget-object v0, p0, LX/DaA;->a:LX/DaD;

    iget-object v0, v0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    .line 2014615
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2014616
    if-eqz v0, :cond_0

    .line 2014617
    iget-object v0, p0, LX/DaA;->a:LX/DaD;

    iget-object v0, v0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    invoke-static {v0}, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->c(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V

    .line 2014618
    :cond_0
    iget-object v0, p0, LX/DaA;->a:LX/DaD;

    iget-object v0, v0, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v0, v0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->i:LX/Da0;

    iget-object v1, p0, LX/DaA;->a:LX/DaD;

    iget-object v1, v1, LX/DaD;->a:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2014619
    sget-object v2, LX/0ax;->ao:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2014620
    iget-object v2, v0, LX/Da0;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/17Y;

    invoke-interface {v2, v1, p0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 2014621
    iget-object v2, v0, LX/Da0;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, p0, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2014622
    return-void
.end method
