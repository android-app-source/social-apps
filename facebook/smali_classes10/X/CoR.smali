.class public LX/CoR;
.super LX/Ci8;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CoN;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CoN;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CoN;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/Chv;

.field public e:I


# direct methods
.method public constructor <init>(LX/Chv;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1935614
    invoke-direct {p0}, LX/Ci8;-><init>()V

    .line 1935615
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CoR;->a:Ljava/util/List;

    .line 1935616
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CoR;->b:Ljava/util/List;

    .line 1935617
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CoR;->c:Ljava/util/List;

    .line 1935618
    iput-object p1, p0, LX/CoR;->d:LX/Chv;

    .line 1935619
    iget-object v0, p0, LX/CoR;->d:LX/Chv;

    invoke-virtual {v0, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 1935620
    return-void
.end method

.method public static a(LX/0QB;)LX/CoR;
    .locals 4

    .prologue
    .line 1935621
    const-class v1, LX/CoR;

    monitor-enter v1

    .line 1935622
    :try_start_0
    sget-object v0, LX/CoR;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1935623
    sput-object v2, LX/CoR;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1935624
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1935625
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1935626
    new-instance p0, LX/CoR;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v3

    check-cast v3, LX/Chv;

    invoke-direct {p0, v3}, LX/CoR;-><init>(LX/Chv;)V

    .line 1935627
    move-object v0, p0

    .line 1935628
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1935629
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CoR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1935630
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1935631
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Ljava/util/List;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/CoN;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1935632
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1935633
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CoN;

    .line 1935634
    iget-object v0, v0, LX/CoN;->a:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 1935635
    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1935636
    add-int/lit8 v1, v1, -0x1

    .line 1935637
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1935638
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1935639
    iget-object v0, p0, LX/CoR;->a:Ljava/util/List;

    invoke-static {v0, p1}, LX/CoR;->a(Ljava/util/List;Landroid/view/View;)V

    .line 1935640
    iget-object v0, p0, LX/CoR;->b:Ljava/util/List;

    invoke-static {v0, p1}, LX/CoR;->a(Ljava/util/List;Landroid/view/View;)V

    .line 1935641
    iget-object v0, p0, LX/CoR;->c:Ljava/util/List;

    invoke-static {v0, p1}, LX/CoR;->a(Ljava/util/List;Landroid/view/View;)V

    .line 1935642
    return-void
.end method

.method public final a(Landroid/view/View;LX/CoQ;LX/CoO;)V
    .locals 2

    .prologue
    .line 1935643
    new-instance v0, LX/CoN;

    invoke-direct {v0, p1, p3, p2}, LX/CoN;-><init>(Landroid/view/View;LX/CoO;LX/CoQ;)V

    .line 1935644
    iget-object v1, p0, LX/CoR;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1935645
    iget-object v1, p0, LX/CoR;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1935646
    :cond_0
    return-void
.end method

.method public final b(LX/0b7;)V
    .locals 11

    .prologue
    .line 1935647
    check-cast p1, LX/Cic;

    const/4 v2, 0x0

    .line 1935648
    iget v0, p0, LX/CoR;->e:I

    .line 1935649
    iget v1, p1, LX/Cic;->b:I

    move v1, v1

    .line 1935650
    add-int/2addr v0, v1

    iput v0, p0, LX/CoR;->e:I

    move v1, v2

    .line 1935651
    :goto_0
    iget-object v0, p0, LX/CoR;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1935652
    iget-object v0, p0, LX/CoR;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CoN;

    .line 1935653
    invoke-virtual {v0}, LX/CoN;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1935654
    iget v3, p0, LX/CoR;->e:I

    invoke-virtual {v0, v3}, LX/CoN;->a(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1935655
    invoke-virtual {v0}, LX/CoN;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1935656
    iget-object v3, v0, LX/CoN;->b:LX/CoO;

    invoke-interface {v3}, LX/CoO;->b()V

    .line 1935657
    :cond_0
    iget-object v3, p0, LX/CoR;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1935658
    iget-object v0, p0, LX/CoR;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1935659
    add-int/lit8 v1, v1, -0x1

    .line 1935660
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1935661
    :cond_2
    iget-object v0, p0, LX/CoR;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1935662
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1935663
    :cond_3
    :goto_2
    iget-object v0, p0, LX/CoR;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 1935664
    iget-object v0, p0, LX/CoR;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CoN;

    .line 1935665
    invoke-virtual {v0}, LX/CoN;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1935666
    iget v1, p0, LX/CoR;->e:I

    invoke-virtual {v0, v1}, LX/CoN;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1935667
    invoke-virtual {v0}, LX/CoN;->b()V

    .line 1935668
    iget-object v1, p0, LX/CoR;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1935669
    iget-object v0, p0, LX/CoR;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1935670
    add-int/lit8 v2, v2, -0x1

    .line 1935671
    :cond_4
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1935672
    :cond_5
    iget-object v0, p0, LX/CoR;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1935673
    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    .line 1935674
    :cond_6
    iget-object v0, p1, LX/Cic;->c:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v0

    .line 1935675
    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 1935676
    const/4 v1, 0x2

    new-array v4, v1, [I

    move v2, v3

    .line 1935677
    :goto_4
    iget-object v1, p0, LX/CoR;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_b

    .line 1935678
    iget-object v1, p0, LX/CoR;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CoN;

    .line 1935679
    iget-object v5, v1, LX/CoN;->a:Landroid/view/View;

    if-eqz v5, :cond_a

    iget-object v5, v1, LX/CoN;->b:LX/CoO;

    if-eqz v5, :cond_a

    .line 1935680
    iget-object v5, v1, LX/CoN;->a:Landroid/view/View;

    .line 1935681
    invoke-virtual {v5, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1935682
    aget v6, v4, v3

    if-gez v6, :cond_7

    aget v6, v4, v10

    if-ltz v6, :cond_8

    .line 1935683
    :cond_7
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v6

    .line 1935684
    aget v7, v4, v10

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    .line 1935685
    iget-object v8, v1, LX/CoN;->c:LX/CoQ;

    .line 1935686
    iget-object v9, v8, LX/CoQ;->a:LX/CoP;

    sget-object p1, LX/CoP;->PIXEL:LX/CoP;

    if-ne v9, p1, :cond_c

    .line 1935687
    iget v9, v8, LX/CoQ;->b:I

    .line 1935688
    :goto_5
    move v8, v9

    .line 1935689
    iget v9, p0, LX/CoR;->e:I

    add-int/2addr v9, v7

    sub-int/2addr v6, v8

    sub-int v6, v9, v6

    .line 1935690
    iget v9, p0, LX/CoR;->e:I

    add-int/2addr v7, v9

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v5, v7

    sub-int/2addr v5, v8

    .line 1935691
    iput v6, v1, LX/CoN;->d:I

    .line 1935692
    iput v5, v1, LX/CoN;->e:I

    .line 1935693
    iget v5, p0, LX/CoR;->e:I

    invoke-virtual {v1, v5}, LX/CoN;->a(I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1935694
    iget-object v5, p0, LX/CoR;->b:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1935695
    invoke-virtual {v1}, LX/CoN;->b()V

    .line 1935696
    :goto_6
    iget-object v1, p0, LX/CoR;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1935697
    add-int/lit8 v2, v2, -0x1

    .line 1935698
    :cond_8
    :goto_7
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 1935699
    :cond_9
    iget-object v5, p0, LX/CoR;->c:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1935700
    :cond_a
    iget-object v1, p0, LX/CoR;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1935701
    add-int/lit8 v2, v2, -0x1

    goto :goto_7

    .line 1935702
    :cond_b
    return-void

    :cond_c
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v9

    iget p1, v8, LX/CoQ;->b:I

    mul-int/2addr v9, p1

    div-int/lit8 v9, v9, 0x64

    goto :goto_5
.end method
