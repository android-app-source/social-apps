.class public final LX/EHX;
.super LX/6Il;
.source ""


# instance fields
.field public final synthetic a:LX/EHZ;


# direct methods
.method public constructor <init>(LX/EHZ;)V
    .locals 0

    .prologue
    .line 2099671
    iput-object p1, p0, LX/EHX;->a:LX/EHZ;

    invoke-direct {p0}, LX/6Il;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2099662
    iget-object v0, p0, LX/EHX;->a:LX/EHZ;

    sget-object v1, LX/6JE;->a:LX/6JE;

    invoke-static {v0, v1}, LX/EHZ;->a$redex0(LX/EHZ;LX/6Ik;)V

    .line 2099663
    sget-object v0, LX/EHZ;->c:Ljava/lang/Class;

    const-string v1, "msqrd_camera_start_preview"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2099664
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2099665
    iget-object v0, p0, LX/EHX;->a:LX/EHZ;

    .line 2099666
    iget-object v1, v0, LX/EHZ;->q:LX/EDx;

    if-eqz v1, :cond_0

    .line 2099667
    iget-object v1, v0, LX/EHZ;->q:LX/EDx;

    iget-object v2, v0, LX/EHZ;->m:LX/6JR;

    iget v2, v2, LX/6JR;->a:I

    iget-object v3, v0, LX/EHZ;->m:LX/6JR;

    iget v3, v3, LX/6JR;->b:I

    invoke-virtual {v1, v2, v3}, LX/EDx;->a(II)V

    .line 2099668
    :cond_0
    iget v1, v0, LX/EHZ;->r:I

    if-lez v1, :cond_1

    iget-wide v1, v0, LX/EHZ;->u:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    .line 2099669
    iget-object v1, v0, LX/EHZ;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    iput-wide v1, v0, LX/EHZ;->u:J

    .line 2099670
    :cond_1
    return-void
.end method
