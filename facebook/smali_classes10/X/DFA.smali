.class public LX/DFA;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFB;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DFA",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/DFB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977751
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1977752
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DFA;->b:LX/0Zi;

    .line 1977753
    iput-object p1, p0, LX/DFA;->a:LX/0Ot;

    .line 1977754
    return-void
.end method

.method public static a(LX/0QB;)LX/DFA;
    .locals 4

    .prologue
    .line 1977755
    const-class v1, LX/DFA;

    monitor-enter v1

    .line 1977756
    :try_start_0
    sget-object v0, LX/DFA;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1977757
    sput-object v2, LX/DFA;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1977758
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977759
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1977760
    new-instance v3, LX/DFA;

    const/16 p0, 0x20de

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DFA;-><init>(LX/0Ot;)V

    .line 1977761
    move-object v0, v3

    .line 1977762
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1977763
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DFA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977764
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1977765
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1977766
    check-cast p2, LX/DF9;

    .line 1977767
    iget-object v0, p0, LX/DFA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DFB;

    iget-object v1, p2, LX/DF9;->a:LX/1Pc;

    iget-object v2, p2, LX/DF9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1977768
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    .line 1977769
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f080f77

    invoke-virtual {v4, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a015d

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0050

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    sget-object v5, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v4, v5}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x6

    const p0, 0x7f0b0917

    invoke-interface {v4, v5, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x7

    const p0, 0x7f0b08fe

    invoke-interface {v4, v5, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 1977770
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 1977771
    iget-object v4, v0, LX/DFB;->a:LX/DFL;

    const/4 v5, 0x0

    .line 1977772
    new-instance p0, LX/DFK;

    invoke-direct {p0, v4}, LX/DFK;-><init>(LX/DFL;)V

    .line 1977773
    iget-object p2, v4, LX/DFL;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/DFJ;

    .line 1977774
    if-nez p2, :cond_0

    .line 1977775
    new-instance p2, LX/DFJ;

    invoke-direct {p2, v4}, LX/DFJ;-><init>(LX/DFL;)V

    .line 1977776
    :cond_0
    invoke-static {p2, p1, v5, v5, p0}, LX/DFJ;->a$redex0(LX/DFJ;LX/1De;IILX/DFK;)V

    .line 1977777
    move-object p0, p2

    .line 1977778
    move-object v5, p0

    .line 1977779
    move-object v4, v5

    .line 1977780
    iget-object v5, v4, LX/DFJ;->a:LX/DFK;

    iput-object v1, v5, LX/DFK;->a:LX/1Pc;

    .line 1977781
    iget-object v5, v4, LX/DFJ;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1977782
    move-object v4, v4

    .line 1977783
    new-instance v5, LX/2dy;

    new-instance p0, LX/2dx;

    invoke-direct {p0}, LX/2dx;-><init>()V

    invoke-direct {v5, v2, p0}, LX/2dy;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2dx;)V

    .line 1977784
    iget-object p0, v4, LX/DFJ;->a:LX/DFK;

    iput-object v5, p0, LX/DFK;->b:LX/2dy;

    .line 1977785
    iget-object p0, v4, LX/DFJ;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1977786
    move-object v4, v4

    .line 1977787
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/DFB;->b:LX/2e9;

    invoke-virtual {v4, p1}, LX/2e9;->c(LX/1De;)LX/DFF;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1977788
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1977789
    invoke-static {}, LX/1dS;->b()V

    .line 1977790
    const/4 v0, 0x0

    return-object v0
.end method
