.class public LX/Cwi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Cwi;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1950952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950953
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Cwi;->a:Ljava/util/Set;

    .line 1950954
    return-void
.end method

.method public static a(LX/0QB;)LX/Cwi;
    .locals 3

    .prologue
    .line 1950955
    sget-object v0, LX/Cwi;->b:LX/Cwi;

    if-nez v0, :cond_1

    .line 1950956
    const-class v1, LX/Cwi;

    monitor-enter v1

    .line 1950957
    :try_start_0
    sget-object v0, LX/Cwi;->b:LX/Cwi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1950958
    if-eqz v2, :cond_0

    .line 1950959
    :try_start_1
    new-instance v0, LX/Cwi;

    invoke-direct {v0}, LX/Cwi;-><init>()V

    .line 1950960
    move-object v0, v0

    .line 1950961
    sput-object v0, LX/Cwi;->b:LX/Cwi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1950962
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1950963
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1950964
    :cond_1
    sget-object v0, LX/Cwi;->b:LX/Cwi;

    return-object v0

    .line 1950965
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1950966
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/model/TypeaheadUnit;)Z
    .locals 1

    .prologue
    .line 1950967
    iget-object v0, p0, LX/Cwi;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
