.class public LX/Dz6;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/9jT;

.field public final c:LX/0y3;

.field public final d:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

.field public e:Lcom/facebook/places/checkin/PlacePickerFragment;

.field public f:Z

.field public g:Z

.field public h:Landroid/location/Location;

.field public final i:LX/Dz4;

.field private final j:Lcom/facebook/common/perftest/PerfTestConfig;

.field public final k:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/location/ImmutableLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2066458
    const-class v0, LX/Dz6;

    sput-object v0, LX/Dz6;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/9jT;LX/0y3;LX/Dz4;Lcom/facebook/common/perftest/PerfTestConfig;Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2066450
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dz6;->g:Z

    .line 2066451
    new-instance v0, LX/Dz5;

    invoke-direct {v0, p0}, LX/Dz5;-><init>(LX/Dz6;)V

    iput-object v0, p0, LX/Dz6;->k:LX/0TF;

    .line 2066452
    iput-object p1, p0, LX/Dz6;->b:LX/9jT;

    .line 2066453
    iput-object p2, p0, LX/Dz6;->c:LX/0y3;

    .line 2066454
    iput-object p3, p0, LX/Dz6;->i:LX/Dz4;

    .line 2066455
    iput-object p4, p0, LX/Dz6;->j:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 2066456
    iput-object p5, p0, LX/Dz6;->d:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    .line 2066457
    return-void
.end method

.method public static a(LX/0QB;)LX/Dz6;
    .locals 8

    .prologue
    .line 2066443
    new-instance v1, LX/Dz6;

    invoke-static {p0}, LX/9jT;->a(LX/0QB;)LX/9jT;

    move-result-object v2

    check-cast v2, LX/9jT;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v3

    check-cast v3, LX/0y3;

    .line 2066444
    new-instance v7, LX/Dz4;

    invoke-static {p0}, LX/9jT;->a(LX/0QB;)LX/9jT;

    move-result-object v4

    check-cast v4, LX/9jT;

    invoke-static {p0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v5

    check-cast v5, LX/0y2;

    invoke-static {p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v6

    check-cast v6, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-direct {v7, v4, v5, v6}, LX/Dz4;-><init>(LX/9jT;LX/0y2;Lcom/facebook/common/perftest/PerfTestConfig;)V

    .line 2066445
    move-object v4, v7

    .line 2066446
    check-cast v4, LX/Dz4;

    invoke-static {p0}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v5

    check-cast v5, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {p0}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->b(LX/0QB;)Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    move-result-object v6

    check-cast v6, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    invoke-direct/range {v1 .. v6}, LX/Dz6;-><init>(LX/9jT;LX/0y3;LX/Dz4;Lcom/facebook/common/perftest/PerfTestConfig;Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;)V

    .line 2066447
    move-object v0, v1

    .line 2066448
    return-object v0
.end method

.method public static c(LX/Dz6;Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 2066437
    iget-object v0, p0, LX/Dz6;->b:LX/9jT;

    .line 2066438
    iput-object p1, v0, LX/9jT;->b:Landroid/location/Location;

    .line 2066439
    iput-object p1, p0, LX/Dz6;->h:Landroid/location/Location;

    .line 2066440
    invoke-static {p0, p1}, LX/Dz6;->d(LX/Dz6;Landroid/location/Location;)V

    .line 2066441
    invoke-static {p0}, LX/Dz6;->n(LX/Dz6;)V

    .line 2066442
    return-void
.end method

.method public static d(LX/Dz6;Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 2066418
    iget-boolean v0, p0, LX/Dz6;->g:Z

    move v0, v0

    .line 2066419
    if-nez v0, :cond_0

    .line 2066420
    iget-object v0, p0, LX/Dz6;->e:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2066421
    invoke-virtual {v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    .line 2066422
    iget-object v0, p0, LX/Dz6;->e:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(Landroid/location/Location;)V

    .line 2066423
    iget-object v0, p0, LX/Dz6;->e:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2066424
    invoke-virtual {v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    .line 2066425
    :goto_0
    return-void

    .line 2066426
    :cond_0
    iget-object v0, p0, LX/Dz6;->e:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(Landroid/location/Location;)V

    goto :goto_0
.end method

.method public static n(LX/Dz6;)V
    .locals 3

    .prologue
    .line 2066430
    iget-boolean v0, p0, LX/Dz6;->g:Z

    if-eqz v0, :cond_0

    .line 2066431
    iget-object v0, p0, LX/Dz6;->d:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    .line 2066432
    iget-object v1, v0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->g:LX/9kB;

    sget-object v2, LX/9jc;->NEARBY_LOCATION:LX/9jc;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2066433
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dz6;->g:Z

    .line 2066434
    iget-object v0, p0, LX/Dz6;->e:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2066435
    invoke-virtual {v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    .line 2066436
    :cond_0
    return-void
.end method


# virtual methods
.method public final h()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2066427
    sget-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->i:Z

    move v1, v1

    .line 2066428
    if-eqz v1, :cond_1

    .line 2066429
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/Dz6;->c:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->a()LX/0yG;

    move-result-object v1

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
