.class public abstract LX/DPA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Sh;

.field public final b:LX/3mF;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/settings/GroupSubscriptionController;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/DPS;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DPK;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/3my;


# direct methods
.method public constructor <init>(LX/0Sh;LX/3mF;LX/0Ot;LX/DPS;LX/0Ot;LX/3my;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/3mF;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/settings/GroupSubscriptionController;",
            ">;",
            "LX/DPS;",
            "LX/0Ot",
            "<",
            "LX/DPK;",
            ">;",
            "LX/3my;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1993083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1993084
    iput-object p1, p0, LX/DPA;->a:LX/0Sh;

    .line 1993085
    iput-object p2, p0, LX/DPA;->b:LX/3mF;

    .line 1993086
    iput-object p3, p0, LX/DPA;->c:LX/0Ot;

    .line 1993087
    iput-object p4, p0, LX/DPA;->d:LX/DPS;

    .line 1993088
    iput-object p5, p0, LX/DPA;->e:LX/0Ot;

    .line 1993089
    iput-object p6, p0, LX/DPA;->f:LX/3my;

    .line 1993090
    return-void
.end method

.method public static synthetic a(LX/DPA;LX/0gc;LX/DPD;Landroid/content/Context;LX/DPC;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 1993091
    iget-object v0, p0, LX/DPA;->f:LX/3my;

    .line 1993092
    iget-object v1, p2, LX/DPD;->h:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-object v1, v1

    .line 1993093
    invoke-virtual {v0, v1}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f08302e

    .line 1993094
    :goto_0
    invoke-static {p1, v0}, LX/DPS;->a(LX/0gc;I)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 1993095
    iget-object v1, p0, LX/DPA;->b:LX/3mF;

    .line 1993096
    iget-object v2, p2, LX/DPD;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1993097
    iget-object v3, p2, LX/DPD;->g:Ljava/lang/String;

    move-object v3, v3

    .line 1993098
    invoke-virtual {v1, v2, v3, p5}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1993099
    iget-object v2, p0, LX/DPA;->a:LX/0Sh;

    iget-object v3, p0, LX/DPA;->d:LX/DPS;

    invoke-virtual {v3, p3}, LX/DPS;->a(Landroid/content/Context;)LX/0ju;

    move-result-object v3

    .line 1993100
    new-instance v4, LX/DP9;

    move-object v5, p0

    move-object v6, p2

    move-object v7, v0

    move-object v8, p4

    move-object v9, v3

    invoke-direct/range {v4 .. v9}, LX/DP9;-><init>(LX/DPA;LX/DPD;Landroid/support/v4/app/DialogFragment;LX/DPC;LX/0ju;)V

    move-object v0, v4

    .line 1993101
    invoke-virtual {v2, v1, v0}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1993102
    return-void

    .line 1993103
    :cond_0
    const v0, 0x7f08302d

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/0gc;LX/DPD;LX/DPC;)V
    .locals 10
    .param p4    # LX/DPC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1993104
    iget-object v0, p3, LX/DPD;->e:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-object v7, v0

    .line 1993105
    new-instance v0, LX/DP6;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/DP6;-><init>(LX/DPA;LX/0gc;LX/DPD;Landroid/content/Context;LX/DPC;)V

    .line 1993106
    new-instance v1, LX/DP7;

    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p1

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, LX/DP7;-><init>(LX/DPA;LX/0gc;LX/DPD;Landroid/content/Context;LX/DPC;)V

    .line 1993107
    new-instance v6, LX/DP8;

    invoke-direct {v6, p0, p3}, LX/DP8;-><init>(LX/DPA;LX/DPD;)V

    .line 1993108
    iget-object v2, p0, LX/DPA;->d:LX/DPS;

    .line 1993109
    iget-object v3, p3, LX/DPD;->b:Ljava/lang/String;

    move-object v8, v3

    .line 1993110
    iget-object v3, p0, LX/DPA;->f:LX/3my;

    .line 1993111
    iget-object v4, p3, LX/DPD;->h:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-object v4, v4

    .line 1993112
    invoke-virtual {v3, v4}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v9

    move-object v3, p1

    move-object v4, v1

    move-object v5, v0

    invoke-virtual/range {v2 .. v9}, LX/DPS;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;Ljava/lang/String;Z)V

    .line 1993113
    return-void
.end method

.method public final a(Landroid/content/Context;ZLandroid/content/DialogInterface$OnClickListener;)V
    .locals 4

    .prologue
    .line 1993114
    iget-object v0, p0, LX/DPA;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DPK;

    .line 1993115
    new-instance v3, LX/0ju;

    invoke-direct {v3, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1993116
    if-eqz p2, :cond_0

    const v1, 0x7f083042

    move v2, v1

    .line 1993117
    :goto_0
    if-eqz p2, :cond_1

    const v1, 0x7f083044

    .line 1993118
    :goto_1
    iget-object p0, v0, LX/DPK;->a:Landroid/content/res/Resources;

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993119
    iget-object v2, v0, LX/DPK;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1993120
    iget-object v1, v0, LX/DPK;->a:Landroid/content/res/Resources;

    const v2, 0x7f083046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, p3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993121
    iget-object v1, v0, LX/DPK;->a:Landroid/content/res/Resources;

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/DPJ;

    invoke-direct {v2, v0}, LX/DPJ;-><init>(LX/DPK;)V

    invoke-virtual {v3, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1993122
    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 1993123
    return-void

    .line 1993124
    :cond_0
    const v1, 0x7f083043

    move v2, v1

    goto :goto_0

    .line 1993125
    :cond_1
    const v1, 0x7f083045

    goto :goto_1
.end method

.method public abstract a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
.end method
