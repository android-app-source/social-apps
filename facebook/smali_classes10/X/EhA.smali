.class public LX/EhA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public a:LX/EtW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hx;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2lP;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/10M;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0jh;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6G2;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2lM;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/katana/urimap/IntentHandlerUtil;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/bookmark/ui/event/OnBookmarkSelectedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2158156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2158157
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2158158
    iput-object v0, p0, LX/EhA;->b:LX/0Ot;

    .line 2158159
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2158160
    iput-object v0, p0, LX/EhA;->c:LX/0Ot;

    .line 2158161
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2158162
    iput-object v0, p0, LX/EhA;->d:LX/0Ot;

    .line 2158163
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2158164
    iput-object v0, p0, LX/EhA;->f:LX/0Ot;

    .line 2158165
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2158166
    iput-object v0, p0, LX/EhA;->g:LX/0Ot;

    .line 2158167
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2158168
    iput-object v0, p0, LX/EhA;->h:LX/0Ot;

    .line 2158169
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2158170
    iput-object v0, p0, LX/EhA;->i:LX/0Ot;

    .line 2158171
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2158172
    iput-object v0, p0, LX/EhA;->j:LX/0Ot;

    .line 2158173
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2158174
    iput-object v0, p0, LX/EhA;->k:LX/0Ot;

    .line 2158175
    return-void
.end method

.method public static a(LX/0QB;)LX/EhA;
    .locals 14

    .prologue
    .line 2158143
    const-class v1, LX/EhA;

    monitor-enter v1

    .line 2158144
    :try_start_0
    sget-object v0, LX/EhA;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2158145
    sput-object v2, LX/EhA;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2158146
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2158147
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2158148
    new-instance v3, LX/EhA;

    invoke-direct {v3}, LX/EhA;-><init>()V

    .line 2158149
    invoke-static {v0}, LX/EtW;->a(LX/0QB;)LX/EtW;

    move-result-object v4

    check-cast v4, LX/EtW;

    const/16 v5, 0x91

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xc59

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x4d6

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x238

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x186a

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xeae

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xc4b

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x97

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 p0, 0xc40

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2158150
    iput-object v4, v3, LX/EhA;->a:LX/EtW;

    iput-object v5, v3, LX/EhA;->b:LX/0Ot;

    iput-object v6, v3, LX/EhA;->c:LX/0Ot;

    iput-object v7, v3, LX/EhA;->d:LX/0Ot;

    iput-object v8, v3, LX/EhA;->e:LX/0Or;

    iput-object v9, v3, LX/EhA;->f:LX/0Ot;

    iput-object v10, v3, LX/EhA;->g:LX/0Ot;

    iput-object v11, v3, LX/EhA;->h:LX/0Ot;

    iput-object v12, v3, LX/EhA;->i:LX/0Ot;

    iput-object v13, v3, LX/EhA;->j:LX/0Ot;

    iput-object p0, v3, LX/EhA;->k:LX/0Ot;

    .line 2158151
    move-object v0, v3

    .line 2158152
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2158153
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EhA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2158154
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2158155
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
