.class public final LX/D6W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3IA;
.implements LX/3IB;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1965882
    iput-object p1, p0, LX/D6W;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1965883
    iput-boolean v0, p0, LX/D6W;->b:Z

    .line 1965884
    iput-boolean v0, p0, LX/D6W;->c:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1965881
    return-void
.end method

.method public final a(FF)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1965876
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-boolean v0, p0, LX/D6W;->b:Z

    if-nez v0, :cond_0

    .line 1965877
    iget-object v0, p0, LX/D6W;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1965878
    iput-boolean v2, p0, LX/D6W;->b:Z

    .line 1965879
    :cond_0
    iget-object v0, p0, LX/D6W;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lr;

    invoke-direct {v1, v2, p1, p2}, LX/7Lr;-><init>(IFF)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1965880
    return-void
.end method

.method public final a(F)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1965873
    iget-object v0, p0, LX/D6W;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LX/7Lq;-><init>(IF)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1965874
    iput-boolean v3, p0, LX/D6W;->c:Z

    .line 1965875
    return v3
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1965871
    iget-object v0, p0, LX/D6W;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lr;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LX/7Lr;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1965872
    return-void
.end method

.method public final b(FF)V
    .locals 3

    .prologue
    .line 1965863
    iget-object v0, p0, LX/D6W;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lr;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1, p2}, LX/7Lr;-><init>(IFF)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1965864
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D6W;->b:Z

    .line 1965865
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1965869
    iget-object v0, p0, LX/D6W;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    invoke-direct {v1, v2}, LX/7Lq;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1965870
    return v2
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1965866
    iget-object v0, p0, LX/D6W;->a:Lcom/facebook/video/channelfeed/plugins/ChannelFeed360TouchControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, LX/7Lq;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1965867
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D6W;->c:Z

    .line 1965868
    return-void
.end method
