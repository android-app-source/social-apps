.class public final LX/DL1;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Ljava/io/File;

.field public final synthetic c:LX/DL8;


# direct methods
.method public constructor <init>(LX/DL8;Lcom/google/common/util/concurrent/SettableFuture;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1987763
    iput-object p1, p0, LX/DL1;->c:LX/DL8;

    iput-object p2, p0, LX/DL1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/DL1;->b:Ljava/io/File;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1987764
    iget-object v0, p0, LX/DL1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, LX/DL2;

    sget-object v2, LX/DL5;->FILE_HANDLE_UPLOADING_FAILURE:LX/DL5;

    iget-object v3, p0, LX/DL1;->b:Ljava/io/File;

    invoke-direct {v1, v2, v3}, LX/DL2;-><init>(LX/DL5;Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1987765
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1987766
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1987767
    if-eqz p1, :cond_0

    .line 1987768
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1987769
    if-eqz v0, :cond_0

    .line 1987770
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1987771
    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel;->a()Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1987772
    iget-object v1, p0, LX/DL1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v2, p0, LX/DL1;->b:Ljava/io/File;

    .line 1987773
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1987774
    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel;->a()Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;

    move-result-object v0

    invoke-static {v2, v0}, LX/DL8;->b(Ljava/io/File;Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel;)LX/DL7;

    move-result-object v0

    const v2, -0x2669eb1d

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1987775
    :goto_0
    return-void

    .line 1987776
    :cond_0
    iget-object v0, p0, LX/DL1;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, LX/DL2;

    sget-object v2, LX/DL5;->FILE_HANDLE_UPLOADING_FAILURE:LX/DL5;

    iget-object v3, p0, LX/DL1;->b:Ljava/io/File;

    invoke-direct {v1, v2, v3}, LX/DL2;-><init>(LX/DL5;Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
