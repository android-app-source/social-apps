.class public LX/Egq;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ego;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Egr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2157314
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Egq;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Egr;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2157315
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2157316
    iput-object p1, p0, LX/Egq;->b:LX/0Ot;

    .line 2157317
    return-void
.end method

.method public static a(LX/0QB;)LX/Egq;
    .locals 4

    .prologue
    .line 2157318
    const-class v1, LX/Egq;

    monitor-enter v1

    .line 2157319
    :try_start_0
    sget-object v0, LX/Egq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2157320
    sput-object v2, LX/Egq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2157321
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2157322
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2157323
    new-instance v3, LX/Egq;

    const/16 p0, 0x180f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Egq;-><init>(LX/0Ot;)V

    .line 2157324
    move-object v0, v3

    .line 2157325
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2157326
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Egq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2157327
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2157328
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2157329
    invoke-static {}, LX/1dS;->b()V

    .line 2157330
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 6

    .prologue
    .line 2157331
    check-cast p3, LX/Egp;

    .line 2157332
    iget-object v0, p0, LX/Egq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Egr;

    iget-object v1, p3, LX/Egp;->b:Ljava/lang/String;

    iget-object v2, p3, LX/Egp;->c:Ljava/util/List;

    .line 2157333
    invoke-static {p1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v3

    invoke-static {p1}, LX/Egu;->c(LX/1De;)LX/Egt;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/Egt;->b(Ljava/lang/String;)LX/Egt;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object v3

    invoke-virtual {v3}, LX/Bcc;->b()LX/BcO;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2157334
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FBn;

    .line 2157335
    invoke-static {p1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v5

    iget-object p0, v0, LX/Egr;->a:LX/Eh9;

    const/4 p3, 0x0

    .line 2157336
    new-instance v1, LX/Eh8;

    invoke-direct {v1, p0}, LX/Eh8;-><init>(LX/Eh9;)V

    .line 2157337
    sget-object v2, LX/Eh9;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Eh7;

    .line 2157338
    if-nez v2, :cond_0

    .line 2157339
    new-instance v2, LX/Eh7;

    invoke-direct {v2}, LX/Eh7;-><init>()V

    .line 2157340
    :cond_0
    invoke-static {v2, p1, p3, p3, v1}, LX/Eh7;->a$redex0(LX/Eh7;LX/1De;IILX/Eh8;)V

    .line 2157341
    move-object v1, v2

    .line 2157342
    move-object p3, v1

    .line 2157343
    move-object p0, p3

    .line 2157344
    iget-object p3, p0, LX/Eh7;->a:LX/Eh8;

    iput-object v3, p3, LX/Eh8;->a:LX/FBn;

    .line 2157345
    iget-object p3, p0, LX/Eh7;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {p3, v1}, Ljava/util/BitSet;->set(I)V

    .line 2157346
    move-object p0, p0

    .line 2157347
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object v5

    iget-object v3, v3, LX/FBn;->d:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/Bcc;->b(Ljava/lang/String;)LX/Bcc;

    move-result-object v3

    invoke-virtual {v3}, LX/Bcc;->b()LX/BcO;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2157348
    :cond_1
    const/4 v3, 0x0

    sget-object v4, LX/BcL;->SUCCEEDED:LX/BcL;

    const/4 v5, 0x0

    invoke-static {p1, v3, v4, v5}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 2157349
    return-void
.end method

.method public final e(LX/BcP;LX/BcO;)V
    .locals 1

    .prologue
    .line 2157350
    iget-object v0, p0, LX/Egq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2157351
    const/4 v0, 0x0

    sget-object p0, LX/BcL;->SUCCEEDED:LX/BcL;

    const/4 p2, 0x0

    invoke-static {p1, v0, p0, p2}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 2157352
    return-void
.end method
