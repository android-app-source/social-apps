.class public final LX/EJX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Aj7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Aj7",
        "<",
        "LX/ByV;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CzL;

.field public final synthetic b:LX/1Ps;

.field public final synthetic c:LX/0Px;

.field public final synthetic d:LX/0Px;

.field public final synthetic e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;LX/CzL;LX/1Ps;LX/0Px;LX/0Px;)V
    .locals 0

    .prologue
    .line 2104556
    iput-object p1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    iput-object p2, p0, LX/EJX;->a:LX/CzL;

    iput-object p3, p0, LX/EJX;->b:LX/1Ps;

    iput-object p4, p0, LX/EJX;->c:LX/0Px;

    iput-object p5, p0, LX/EJX;->d:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/ByV;I)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/ByV;",
            ">;",
            "LX/ByV;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2104540
    iget-object v1, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    move/from16 v0, p3

    if-ne v0, v1, :cond_0

    .line 2104541
    iget-object v1, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2104542
    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v9

    .line 2104543
    iget-object v1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    iget-object v10, v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->o:LX/CvY;

    iget-object v1, p0, LX/EJX;->b:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v11

    iget-object v1, p0, LX/EJX;->b:LX/1Ps;

    check-cast v1, LX/CxP;

    iget-object v2, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v2}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v2

    invoke-interface {v1, v2}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v12

    iget-object v13, p0, LX/EJX;->a:LX/CzL;

    iget-object v1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;)LX/CvY;

    iget-object v1, p0, LX/EJX;->b:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    iget-object v2, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v2}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v2

    invoke-static {v2}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    iget-object v3, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v3}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v3

    invoke-static {v3}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v9}, LX/8dH;->fn_()LX/0Px;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    iget-object v6, p0, LX/EJX;->b:LX/1Ps;

    check-cast v6, LX/CxP;

    iget-object v7, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v7}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v7

    invoke-interface {v6, v7}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v6

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->Q()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    const/4 v14, 0x3

    invoke-static {v7, v14}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-static/range {v1 .. v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;IILcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-virtual {v10, v11, v12, v13, v1}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2104544
    iget-object v1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->m:LX/1nD;

    invoke-interface {v9}, LX/8dH;->fn_()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-interface {v9}, LX/8dH;->j()LX/8dG;

    move-result-object v3

    invoke-interface {v3}, LX/8dG;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/EJX;->b:LX/1Ps;

    check-cast v5, LX/CxV;

    invoke-interface {v5}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v5

    invoke-interface {v5}, LX/8ef;->m()Ljava/lang/String;

    move-result-object v5

    sget-object v6, LX/8ci;->u:LX/8ci;

    iget-object v7, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v7}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v7

    invoke-static {v7}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v7

    iget-object v8, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v8}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v8

    invoke-static {v8}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    iget-object v9, p0, LX/EJX;->b:LX/1Ps;

    check-cast v9, LX/CxV;

    invoke-interface {v9}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v9

    invoke-virtual/range {v1 .. v9}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v2

    .line 2104545
    iget-object v1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    iget-object v3, v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/EJX;->b:LX/1Ps;

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2104546
    :goto_0
    return-void

    .line 2104547
    :cond_0
    invoke-virtual/range {p2 .. p2}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4ed245b

    if-ne v1, v2, :cond_1

    .line 2104548
    iget-object v1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    iget-object v2, v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->p:LX/D3w;

    invoke-virtual/range {p2 .. p2}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v3

    iget-object v1, p0, LX/EJX;->b:LX/1Ps;

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v4, LX/04D;->RESULTS_PAGE_MIXED_MEDIA:LX/04D;

    invoke-virtual {v2, v3, v1, v4}, LX/D3w;->a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;)V

    goto :goto_0

    .line 2104549
    :cond_1
    invoke-virtual/range {p2 .. p2}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x1eaef984

    if-ne v1, v2, :cond_2

    .line 2104550
    new-instance v2, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2104551
    iget-object v1, p0, LX/EJX;->c:LX/0Px;

    move/from16 v0, p3

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2104552
    iget-object v1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    iget-object v3, v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/EJX;->b:LX/1Ps;

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2104553
    :cond_2
    iget-object v1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->g:LX/1qa;

    iget-object v5, p0, LX/EJX;->d:LX/0Px;

    iget-object v2, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    iget-object v6, v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->h:LX/23R;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    invoke-static/range {v1 .. v6}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/1qa;Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/ByV;ILX/0Px;LX/23R;)V

    .line 2104554
    iget-object v1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    iget-object v11, v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->o:LX/CvY;

    iget-object v1, p0, LX/EJX;->b:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v12

    sget-object v13, LX/8ch;->CLICK:LX/8ch;

    iget-object v14, p0, LX/EJX;->a:LX/CzL;

    iget-object v1, p0, LX/EJX;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;)LX/CvY;

    iget-object v1, p0, LX/EJX;->b:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    iget-object v2, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v2}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v2

    invoke-static {v2}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, LX/EJX;->b:LX/1Ps;

    check-cast v4, LX/CxP;

    iget-object v5, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v5}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v5

    invoke-interface {v4, v5}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v4

    iget-object v5, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v5}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->Q()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    iget-object v5, p0, LX/EJX;->a:LX/CzL;

    invoke-virtual {v5}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v8

    iget-object v5, p0, LX/EJX;->c:LX/0Px;

    move/from16 v0, p3

    invoke-virtual {v5, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v10

    move/from16 v5, p3

    invoke-static/range {v1 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v1, v11

    move-object v2, v12

    move-object v3, v13

    move/from16 v4, p3

    move-object v5, v14

    invoke-virtual/range {v1 .. v6}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/26N;I)V
    .locals 0

    .prologue
    .line 2104555
    check-cast p2, LX/ByV;

    invoke-direct {p0, p1, p2, p3}, LX/EJX;->a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/ByV;I)V

    return-void
.end method
