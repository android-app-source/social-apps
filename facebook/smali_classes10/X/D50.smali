.class public final LX/D50;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1xz;


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

.field private final b:LX/1SX;

.field private final c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final d:LX/1nq;

.field private final e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;Landroid/content/Context;LX/1nq;LX/D52;LX/1SX;)V
    .locals 2

    .prologue
    .line 1963182
    iput-object p1, p0, LX/D50;->a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1963183
    iput-object p3, p0, LX/D50;->d:LX/1nq;

    .line 1963184
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1963185
    const v0, 0x7f01029a

    invoke-static {p2, v0}, LX/0WH;->d(Landroid/content/Context;I)LX/0am;

    move-result-object v0

    .line 1963186
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1963187
    iget-object v1, p0, LX/D50;->d:LX/1nq;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, LX/1nq;->c(I)LX/1nq;

    .line 1963188
    :cond_0
    iget-object v0, p4, LX/D52;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, LX/D50;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963189
    iget-object v0, p0, LX/D50;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963190
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1963191
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aT()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/D50;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1963192
    iput-object p5, p0, LX/D50;->b:LX/1SX;

    .line 1963193
    new-instance v0, LX/D51;

    const-class v1, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, p4, v1}, LX/D51;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;LX/D52;Ljava/lang/String;)V

    iput-object v0, p0, LX/D50;->f:LX/1KL;

    .line 1963194
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 1

    .prologue
    .line 1963181
    const/4 v0, 0x0

    return v0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1963180
    iget-object v0, p0, LX/D50;->f:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1963164
    iget-object v0, p0, LX/D50;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 1963177
    iget-object v0, p0, LX/D50;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1963178
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1963179
    check-cast v0, LX/0jW;

    return-object v0
.end method

.method public final d()LX/1nq;
    .locals 1

    .prologue
    .line 1963176
    iget-object v0, p0, LX/D50;->d:LX/1nq;

    return-object v0
.end method

.method public final e()I
    .locals 8

    .prologue
    .line 1963165
    iget-object v0, p0, LX/D50;->a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->c:LX/D4z;

    iget-object v1, p0, LX/D50;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/D50;->b:LX/1SX;

    iget-object v3, p0, LX/D50;->a:Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;

    iget-object v3, v3, Lcom/facebook/video/channelfeed/ChannelFeedHeaderTitlePartDefinition;->d:LX/1DR;

    invoke-virtual {v3}, LX/1DR;->a()I

    move-result v3

    const/4 v6, 0x0

    .line 1963166
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1963167
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1963168
    iget-object v5, v0, LX/D4z;->a:LX/1V8;

    sget-object v7, LX/1Ua;->h:LX/1Ua;

    iget p0, v0, LX/D4z;->c:F

    invoke-virtual {v5, v7, v1, p0}, LX/1V8;->a(LX/1Ua;Lcom/facebook/feed/rows/core/props/FeedProps;F)I

    move-result v7

    .line 1963169
    invoke-static {v4}, LX/1wE;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;

    move-result-object v5

    invoke-static {v5}, LX/1VF;->a(Lcom/facebook/feed/util/story/FeedStoryUtilGraphQLModels$ShouldDisplayProfilePictureGraphQLModel;)Z

    move-result v5

    .line 1963170
    if-eqz v5, :cond_1

    iget v5, v0, LX/D4z;->d:I

    .line 1963171
    :goto_0
    invoke-virtual {v2, v4}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1wH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    .line 1963172
    if-eqz v4, :cond_0

    iget v6, v0, LX/D4z;->f:I

    .line 1963173
    :cond_0
    mul-int/lit8 v4, v7, 0x2

    sub-int v4, v3, v4

    sub-int/2addr v4, v5

    sub-int/2addr v4, v6

    iget v5, v0, LX/D4z;->e:I

    sub-int/2addr v4, v5

    move v0, v4

    .line 1963174
    return v0

    :cond_1
    move v5, v6

    .line 1963175
    goto :goto_0
.end method
