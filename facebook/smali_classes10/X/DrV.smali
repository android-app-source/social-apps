.class public final LX/DrV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DrP;

.field public final synthetic b:LX/1Pn;

.field public final synthetic c:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;LX/DrP;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2049497
    iput-object p1, p0, LX/DrV;->c:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;

    iput-object p2, p0, LX/DrV;->a:LX/DrP;

    iput-object p3, p0, LX/DrV;->b:LX/1Pn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x2

    const v0, 0x5d1708a7

    invoke-static {v6, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2049498
    iget-object v0, p0, LX/DrV;->a:LX/DrP;

    .line 2049499
    iget-object v2, v0, LX/DrP;->b:LX/Drh;

    move-object v0, v2

    .line 2049500
    iget-object v2, p0, LX/DrV;->a:LX/DrP;

    iget-object v2, v2, LX/DrP;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 2049501
    iget-object v4, v2, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v2, v4

    .line 2049502
    invoke-interface {v2}, LX/BCO;->c()Ljava/lang/String;

    move-result-object v2

    .line 2049503
    iget-object v4, v0, LX/Drh;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2049504
    iget-object v0, p0, LX/DrV;->b:LX/1Pn;

    check-cast v0, LX/1Pq;

    new-array v2, v3, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    iget-object v4, p0, LX/DrV;->a:LX/DrP;

    iget-object v4, v4, LX/DrP;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    invoke-static {v4}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2049505
    iget-object v0, p0, LX/DrV;->c:Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->c:LX/33W;

    iget-object v2, p0, LX/DrV;->b:LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/DrV;->a:LX/DrP;

    iget-object v3, v3, LX/DrP;->a:Lcom/facebook/notifications/settings/data/NotifOptionNode;

    .line 2049506
    iget-object v4, v3, Lcom/facebook/notifications/settings/data/NotifOptionNode;->a:LX/BCO;

    move-object v3, v4

    .line 2049507
    sget-object v4, LX/8D0;->ENTITY_ROW:LX/8D0;

    new-instance v5, LX/DrU;

    invoke-direct {v5, p0}, LX/DrU;-><init>(LX/DrV;)V

    invoke-virtual {v0, v2, v3, v4, v5}, LX/33W;->a(Landroid/content/Context;LX/BCO;LX/8D0;LX/Dq2;)V

    .line 2049508
    const v0, -0x98f8451

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
