.class public final LX/EKu;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EKv;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionInterfaces$SearchResultsPartyInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/EKv;


# direct methods
.method public constructor <init>(LX/EKv;)V
    .locals 1

    .prologue
    .line 2107418
    iput-object p1, p0, LX/EKu;->b:LX/EKv;

    .line 2107419
    move-object v0, p1

    .line 2107420
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2107421
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2107422
    const-string v0, "SearchResultsPartyInfoListComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2107423
    if-ne p0, p1, :cond_1

    .line 2107424
    :cond_0
    :goto_0
    return v0

    .line 2107425
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2107426
    goto :goto_0

    .line 2107427
    :cond_3
    check-cast p1, LX/EKu;

    .line 2107428
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2107429
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2107430
    if-eq v2, v3, :cond_0

    .line 2107431
    iget-object v2, p0, LX/EKu;->a:LX/0Px;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/EKu;->a:LX/0Px;

    iget-object v3, p1, LX/EKu;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2107432
    goto :goto_0

    .line 2107433
    :cond_4
    iget-object v2, p1, LX/EKu;->a:LX/0Px;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
