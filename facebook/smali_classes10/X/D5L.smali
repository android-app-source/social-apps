.class public final LX/D5L;
.super Landroid/view/OrientationEventListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1963611
    iput-object p1, p0, LX/D5L;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final onOrientationChanged(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1963612
    iget-object v0, p0, LX/D5L;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1963613
    iget-object v1, p0, LX/D5L;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->D:LX/0sV;

    invoke-virtual {v1}, LX/0sV;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 1963614
    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1963615
    iget-object v0, p0, LX/D5L;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ai:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 1963616
    :cond_0
    return-void
.end method
