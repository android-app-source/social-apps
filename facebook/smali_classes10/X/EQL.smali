.class public LX/EQL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/EQL;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public final b:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2118473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2118474
    iput-object p1, p0, LX/EQL;->a:Landroid/content/res/Resources;

    .line 2118475
    iput-object p2, p0, LX/EQL;->b:LX/0ad;

    .line 2118476
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 2118477
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 2118478
    iget-object v1, p0, LX/EQL;->b:LX/0ad;

    sget-short v2, LX/0wf;->v:S

    invoke-interface {v1, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2118479
    iget-object v2, p0, LX/EQL;->b:LX/0ad;

    sget-short v3, LX/0wf;->w:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 2118480
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    .line 2118481
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v5

    .line 2118482
    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v6

    .line 2118483
    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    .line 2118484
    const-wide v9, 0x3fd3333333333333L    # 0.3

    int-to-double v11, v5

    mul-double/2addr v9, v11

    const-wide v11, 0x3fe2e147ae147ae1L    # 0.59

    int-to-double v5, v6

    mul-double/2addr v5, v11

    add-double/2addr v5, v9

    const-wide v9, 0x3fbc28f5c28f5c29L    # 0.11

    int-to-double v7, v7

    mul-double/2addr v7, v9

    add-double/2addr v5, v7

    .line 2118485
    iget-object v7, p0, LX/EQL;->b:LX/0ad;

    sget v8, LX/0wf;->u:I

    const/16 v9, 0x64

    invoke-interface {v7, v8, v9}, LX/0ad;->a(II)I

    move-result v7

    .line 2118486
    int-to-double v7, v7

    cmpg-double v5, v5, v7

    if-gtz v5, :cond_2

    const/4 v5, 0x1

    :goto_0
    move v1, v5

    .line 2118487
    if-eqz v1, :cond_1

    .line 2118488
    :cond_0
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    .line 2118489
    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 2118490
    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 2118491
    iget-object v4, p0, LX/EQL;->b:LX/0ad;

    sget v5, LX/0wf;->x:F

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-interface {v4, v5, v6}, LX/0ad;->a(FF)F

    move-result v4

    .line 2118492
    int-to-float v5, v1

    rsub-int v1, v1, 0xff

    int-to-float v1, v1

    mul-float/2addr v1, v4

    add-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v2

    rsub-int v2, v2, 0xff

    int-to-float v2, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v5

    float-to-int v2, v2

    int-to-float v5, v3

    rsub-int v3, v3, 0xff

    int-to-float v3, v3

    mul-float/2addr v3, v4

    add-float/2addr v3, v5

    float-to-int v3, v3

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    move v0, v1

    .line 2118493
    :cond_1
    return v0

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/EQL;
    .locals 5

    .prologue
    .line 2118494
    sget-object v0, LX/EQL;->c:LX/EQL;

    if-nez v0, :cond_1

    .line 2118495
    const-class v1, LX/EQL;

    monitor-enter v1

    .line 2118496
    :try_start_0
    sget-object v0, LX/EQL;->c:LX/EQL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2118497
    if-eqz v2, :cond_0

    .line 2118498
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2118499
    new-instance p0, LX/EQL;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/EQL;-><init>(Landroid/content/res/Resources;LX/0ad;)V

    .line 2118500
    move-object v0, p0

    .line 2118501
    sput-object v0, LX/EQL;->c:LX/EQL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2118502
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2118503
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2118504
    :cond_1
    sget-object v0, LX/EQL;->c:LX/EQL;

    return-object v0

    .line 2118505
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2118506
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2118507
    if-nez p1, :cond_0

    .line 2118508
    iget-object v0, p0, LX/EQL;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2118509
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p0, p1}, LX/EQL;->a(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0
.end method
