.class public LX/Cuw;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;


# instance fields
.field public b:LX/Cum;

.field public c:Lcom/facebook/video/player/RichVideoPlayer;

.field public d:Landroid/os/Bundle;

.field public e:LX/Cuv;

.field public f:LX/Cua;

.field public g:Z

.field public h:Z

.field public i:LX/Cso;

.field public j:Z

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1947520
    const-class v0, LX/Cuw;

    sput-object v0, LX/Cuw;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1947518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1947519
    return-void
.end method

.method public static a(LX/0QB;)LX/Cuw;
    .locals 1

    .prologue
    .line 1947515
    new-instance v0, LX/Cuw;

    invoke-direct {v0}, LX/Cuw;-><init>()V

    .line 1947516
    move-object v0, v0

    .line 1947517
    return-object v0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 1947514
    iget-object v0, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cuw;->b:LX/Cum;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1947508
    iput-object v0, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1947509
    iput-object v0, p0, LX/Cuw;->b:LX/Cum;

    .line 1947510
    iput-object v0, p0, LX/Cuw;->d:Landroid/os/Bundle;

    .line 1947511
    iput-object v0, p0, LX/Cuw;->e:LX/Cuv;

    .line 1947512
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Cuw;->k:Z

    .line 1947513
    return-void
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;LX/Cun;ZZZZZZLX/Cso;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1947386
    invoke-direct {p0}, LX/Cuw;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1947387
    invoke-virtual {p0}, LX/Cuw;->a()V

    .line 1947388
    :cond_0
    iput-object p1, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1947389
    new-instance v3, LX/Cum;

    invoke-direct {v3}, LX/Cum;-><init>()V

    .line 1947390
    if-nez p6, :cond_3

    if-nez p3, :cond_3

    if-eqz p4, :cond_3

    .line 1947391
    sget-object v0, LX/Cun;->a:LX/Cuk;

    .line 1947392
    iput-object v0, v3, LX/Cum;->b:LX/Cuk;

    .line 1947393
    sget-object v2, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    sget-object v4, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947394
    sget-object v2, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object v4, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947395
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->d:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947396
    if-eqz p5, :cond_2

    .line 1947397
    sget-object v2, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object v4, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947398
    sget-object v2, LX/Cun;->c:LX/Cuk;

    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object p1, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947399
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    sget-object p1, LX/Cun;->a:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947400
    sget-object v2, LX/Cun;->d:LX/Cuk;

    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    sget-object p1, LX/Cun;->a:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947401
    sget-object v2, LX/Cun;->a:LX/Cuk;

    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object p1, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947402
    :goto_0
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947403
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947404
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->d:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947405
    sget-object v2, LX/Cun;->c:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->d:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947406
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947407
    sget-object v0, LX/Cun;->d:LX/Cuk;

    sget-object v2, LX/Cuj;->USER_CONTROLLER_PAUSED:LX/Cuj;

    sget-object v4, LX/Cun;->a:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947408
    sget-object v0, LX/Cun;->d:LX/Cuk;

    sget-object v2, LX/Cuj;->SYSTEM_AUTOHIDE_CONTROLS:LX/Cuj;

    sget-object v4, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947409
    :goto_1
    move-object v0, v3

    .line 1947410
    iput-object v0, p0, LX/Cuw;->b:LX/Cum;

    .line 1947411
    new-instance v0, LX/Cuv;

    invoke-direct {v0, p0}, LX/Cuv;-><init>(LX/Cuw;)V

    iput-object v0, p0, LX/Cuw;->e:LX/Cuv;

    .line 1947412
    if-eqz p7, :cond_1

    if-eqz p5, :cond_1

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, LX/Cuw;->g:Z

    .line 1947413
    iput-object p9, p0, LX/Cuw;->i:LX/Cso;

    .line 1947414
    iput-boolean v1, p0, LX/Cuw;->k:Z

    .line 1947415
    return-void

    :cond_1
    move v0, v1

    .line 1947416
    goto :goto_2

    .line 1947417
    :cond_2
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    sget-object p1, LX/Cun;->a:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    goto :goto_0

    .line 1947418
    :cond_3
    if-eqz p3, :cond_7

    .line 1947419
    if-eqz p4, :cond_5

    .line 1947420
    sget-object v0, LX/Cun;->e:LX/Cuk;

    .line 1947421
    iput-object v0, v3, LX/Cum;->b:LX/Cuk;

    .line 1947422
    sget-object v2, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    sget-object v4, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947423
    sget-object v2, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object v4, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947424
    if-eqz p5, :cond_4

    .line 1947425
    sget-object v2, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object v4, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947426
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947427
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947428
    :cond_4
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947429
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947430
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947431
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947432
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CONTROLLER_PAUSED:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947433
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947434
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947435
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_AUTOHIDE_CONTROLS:LX/Cuj;

    sget-object p1, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947436
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947437
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    goto/16 :goto_1

    .line 1947438
    :cond_5
    sget-object v0, LX/Cun;->e:LX/Cuk;

    .line 1947439
    iput-object v0, v3, LX/Cum;->b:LX/Cuk;

    .line 1947440
    sget-object v2, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    sget-object v4, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947441
    if-eqz p5, :cond_6

    .line 1947442
    sget-object v2, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object v4, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v0, v2, v4}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947443
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947444
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947445
    :cond_6
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947446
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947447
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947448
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947449
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947450
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947451
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947452
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_PRESSED_BACK:LX/Cuj;

    sget-object p1, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947453
    sget-object v2, LX/Cun;->b:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    goto/16 :goto_1

    .line 1947454
    :cond_7
    if-eqz p4, :cond_b

    .line 1947455
    sget-object v2, LX/Cun;->a:LX/Cuk;

    .line 1947456
    if-eqz p5, :cond_9

    sget-object v0, LX/Cun;->b:LX/Cuk;

    .line 1947457
    :goto_3
    iput-object v2, v3, LX/Cum;->b:LX/Cuk;

    .line 1947458
    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    sget-object p1, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947459
    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947460
    if-eqz p5, :cond_8

    .line 1947461
    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object p1, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947462
    sget-object v4, LX/Cun;->c:LX/Cuk;

    sget-object p1, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object p2, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947463
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    sget-object p2, LX/Cun;->a:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947464
    sget-object v4, LX/Cun;->a:LX/Cuk;

    sget-object p1, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object p2, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947465
    :cond_8
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947466
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947467
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p2, LX/Cun;->g:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947468
    sget-object v4, LX/Cun;->c:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p2, LX/Cun;->g:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947469
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947470
    sget-object v4, LX/Cun;->f:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947471
    sget-object v4, LX/Cun;->f:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p2, LX/Cun;->g:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947472
    sget-object v4, LX/Cun;->f:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_SCROLL_FINISHED:LX/Cuj;

    invoke-virtual {v3, v4, p1, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947473
    sget-object v4, LX/Cun;->f:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_PRESSED_BACK:LX/Cuj;

    invoke-virtual {v3, v4, p1, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947474
    if-eqz p8, :cond_a

    .line 1947475
    sget-object v4, LX/Cun;->g:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_CONTROLLER_PAUSED:LX/Cuj;

    sget-object p2, LX/Cun;->e:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947476
    sget-object v4, LX/Cun;->e:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p2, LX/Cun;->g:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947477
    :goto_4
    sget-object v4, LX/Cun;->g:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947478
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947479
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->SYSTEM_AUTOHIDE_CONTROLS:LX/Cuj;

    sget-object p1, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947480
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_SCROLL_FINISHED:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947481
    sget-object v2, LX/Cun;->g:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_PRESSED_BACK:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    goto/16 :goto_1

    :cond_9
    move-object v0, v2

    .line 1947482
    goto/16 :goto_3

    .line 1947483
    :cond_a
    sget-object v4, LX/Cun;->g:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_CONTROLLER_PAUSED:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947484
    sget-object v4, LX/Cun;->f:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947485
    sget-object v4, LX/Cun;->g:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    goto :goto_4

    .line 1947486
    :cond_b
    sget-object v2, LX/Cun;->a:LX/Cuk;

    .line 1947487
    if-eqz p5, :cond_d

    sget-object v0, LX/Cun;->b:LX/Cuk;

    .line 1947488
    :goto_5
    iput-object v2, v3, LX/Cum;->b:LX/Cuk;

    .line 1947489
    sget-object v4, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    sget-object p1, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947490
    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p1, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947491
    if-eqz p5, :cond_c

    .line 1947492
    sget-object v4, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object p1, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v2, v4, p1}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947493
    sget-object v4, LX/Cun;->c:LX/Cuk;

    sget-object p1, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object p2, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947494
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947495
    sget-object v4, LX/Cun;->a:LX/Cuk;

    sget-object p1, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    sget-object p2, LX/Cun;->b:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947496
    :cond_c
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947497
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947498
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p2, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947499
    sget-object v4, LX/Cun;->c:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p2, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947500
    sget-object v4, LX/Cun;->a:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    sget-object p2, LX/Cun;->f:LX/Cuk;

    invoke-virtual {v3, v4, p1, p2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947501
    sget-object v4, LX/Cun;->b:LX/Cuk;

    sget-object p1, LX/Cuj;->USER_UNFOCUSED_MEDIA:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947502
    sget-object v4, LX/Cun;->f:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947503
    sget-object v4, LX/Cun;->f:LX/Cuk;

    sget-object p1, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    invoke-virtual {v3, v4, p1, v2}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947504
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947505
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_SCROLL_FINISHED:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    .line 1947506
    sget-object v2, LX/Cun;->f:LX/Cuk;

    sget-object v4, LX/Cuj;->USER_PRESSED_BACK:LX/Cuj;

    invoke-virtual {v3, v2, v4, v0}, LX/Cum;->a(LX/Cuk;LX/Cuj;LX/Cuk;)V

    goto/16 :goto_1

    :cond_d
    move-object v0, v2

    .line 1947507
    goto/16 :goto_5
.end method

.method public final a(LX/Cuj;)Z
    .locals 13

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1947296
    new-array v0, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, LX/Cuj;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    .line 1947297
    invoke-direct {p0}, LX/Cuw;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1947298
    :cond_0
    :goto_0
    return v2

    .line 1947299
    :cond_1
    const/4 v0, 0x0

    .line 1947300
    iget-boolean v1, p0, LX/Cuw;->g:Z

    if-nez v1, :cond_13

    .line 1947301
    :cond_2
    :goto_1
    move v0, v0

    .line 1947302
    if-nez v0, :cond_0

    .line 1947303
    sget-object v0, LX/Cuj;->ATTEMPT_TO_PLAY:LX/Cuj;

    if-ne p1, v0, :cond_4

    .line 1947304
    iget-object v0, p0, LX/Cuw;->f:LX/Cua;

    if-eqz v0, :cond_3

    .line 1947305
    iget-object v0, p0, LX/Cuw;->f:LX/Cua;

    .line 1947306
    iget-object v8, v0, LX/Cua;->a:LX/Cub;

    iget-object v8, v8, LX/Cub;->o:Landroid/os/Handler;

    iget-object v9, v0, LX/Cua;->a:LX/Cub;

    iget-object v9, v9, LX/Cub;->q:Ljava/lang/Runnable;

    invoke-static {v8, v9}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1947307
    iget-object v8, v0, LX/Cua;->a:LX/Cub;

    iget-object v8, v8, LX/Cub;->o:Landroid/os/Handler;

    iget-object v9, v0, LX/Cua;->a:LX/Cub;

    iget-object v9, v9, LX/Cub;->q:Ljava/lang/Runnable;

    const-wide/16 v10, 0x3e8

    const v12, 0x5a16ec2f

    invoke-static {v8, v9, v10, v11, v12}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1947308
    :cond_3
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    .line 1947309
    iput-boolean v6, v0, LX/Cuv;->f:Z

    .line 1947310
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    invoke-virtual {v0, v2}, LX/Cuv;->b(Z)V

    .line 1947311
    :cond_4
    sget-object v0, LX/Cuj;->SYSTEM_LOADING:LX/Cuj;

    if-ne p1, v0, :cond_d

    .line 1947312
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    invoke-virtual {v0, v6}, LX/Cuv;->b(Z)V

    .line 1947313
    :cond_5
    :goto_2
    iget-object v1, p0, LX/Cuw;->e:LX/Cuv;

    iget-object v0, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v0

    sget-object v3, LX/2qV;->ERROR:LX/2qV;

    if-ne v0, v3, :cond_10

    move v0, v6

    .line 1947314
    :goto_3
    iget-boolean v3, v1, LX/Cuv;->d:Z

    iput-boolean v3, v1, LX/Cuv;->e:Z

    .line 1947315
    iput-boolean v0, v1, LX/Cuv;->d:Z

    .line 1947316
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    invoke-virtual {v0}, LX/Cuv;->b()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    invoke-virtual {v0}, LX/Cuv;->a()Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_6
    move v0, v6

    .line 1947317
    :goto_4
    if-eqz v0, :cond_7

    .line 1947318
    new-array v1, v4, [Ljava/lang/Object;

    iget-object v3, p0, LX/Cuw;->e:LX/Cuv;

    invoke-virtual {v3}, LX/Cuv;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iget-object v3, p0, LX/Cuw;->e:LX/Cuv;

    invoke-virtual {v3}, LX/Cuv;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v6

    .line 1947319
    :cond_7
    sget-object v1, LX/Cuj;->ATTEMPT_TO_PLAY:LX/Cuj;

    if-ne p1, v1, :cond_12

    .line 1947320
    sget-object v1, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    .line 1947321
    :goto_5
    iget-object v3, p0, LX/Cuw;->b:LX/Cum;

    .line 1947322
    iget-object v5, v3, LX/Cum;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_16

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Cul;

    .line 1947323
    iget-object v8, v5, LX/Cul;->a:LX/Cuk;

    move-object v8, v8

    .line 1947324
    iget-object v9, v3, LX/Cum;->b:LX/Cuk;

    invoke-virtual {v8, v9}, LX/Cuk;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1947325
    iget-object v8, v5, LX/Cul;->b:LX/Cuj;

    move-object v8, v8

    .line 1947326
    invoke-virtual {v8, v1}, LX/Cuj;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1947327
    iget-object v7, v5, LX/Cul;->c:LX/Cuk;

    move-object v5, v7

    .line 1947328
    iput-object v5, v3, LX/Cum;->b:LX/Cuk;

    .line 1947329
    const/4 v5, 0x1

    .line 1947330
    :goto_6
    move v7, v5

    .line 1947331
    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v6

    .line 1947332
    if-nez v7, :cond_9

    if-eqz v0, :cond_b

    .line 1947333
    :cond_9
    iget-object v0, p0, LX/Cuw;->b:LX/Cum;

    .line 1947334
    iget-object v2, v0, LX/Cum;->b:LX/Cuk;

    move-object v0, v2

    .line 1947335
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1947336
    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, LX/Cuk;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    .line 1947337
    iget-object v4, p0, LX/Cuw;->e:LX/Cuv;

    iget-boolean v4, v4, LX/Cuv;->d:Z

    if-nez v4, :cond_a

    iget-object v4, p0, LX/Cuw;->e:LX/Cuv;

    .line 1947338
    iget-boolean v5, v4, LX/Cuv;->b:Z

    move v4, v5

    .line 1947339
    if-eqz v4, :cond_17

    .line 1947340
    :cond_a
    :goto_7
    iget-object v0, p0, LX/Cuw;->f:LX/Cua;

    if-eqz v0, :cond_b

    .line 1947341
    iget-object v0, p0, LX/Cuw;->f:LX/Cua;

    iget-object v2, p0, LX/Cuw;->b:LX/Cum;

    .line 1947342
    iget-object v3, v2, LX/Cum;->b:LX/Cuk;

    move-object v2, v3

    .line 1947343
    iget-object v3, p0, LX/Cuw;->e:LX/Cuv;

    .line 1947344
    iget-boolean v4, v3, LX/Cuv;->b:Z

    move v3, v4

    .line 1947345
    iget-object v4, p0, LX/Cuw;->e:LX/Cuv;

    iget-boolean v4, v4, LX/Cuv;->d:Z

    iget-boolean v5, p0, LX/Cuw;->j:Z

    invoke-virtual/range {v0 .. v5}, LX/Cua;->a(LX/Cuj;LX/Cuk;ZZZ)V

    .line 1947346
    :cond_b
    if-eqz v7, :cond_c

    iget-boolean v0, p0, LX/Cuw;->g:Z

    if-eqz v0, :cond_c

    sget-object v0, LX/Cuj;->USER_CONTROLLER_PAUSED:LX/Cuj;

    if-ne v1, v0, :cond_c

    .line 1947347
    iput-boolean v6, p0, LX/Cuw;->h:Z

    :cond_c
    move v2, v7

    .line 1947348
    goto/16 :goto_0

    .line 1947349
    :cond_d
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    .line 1947350
    iget-boolean v1, v0, LX/Cuv;->b:Z

    move v0, v1

    .line 1947351
    if-nez v0, :cond_e

    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    .line 1947352
    iget-boolean v1, v0, LX/Cuv;->f:Z

    move v0, v1

    .line 1947353
    if-eqz v0, :cond_5

    .line 1947354
    :cond_e
    sget-object v0, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    if-eq p1, v0, :cond_f

    sget-object v0, LX/Cuj;->SYSTEM_VIDEO_PAUSE:LX/Cuj;

    if-eq p1, v0, :cond_f

    sget-object v0, LX/Cuj;->SYSTEM_VIDEO_FINISHED:LX/Cuj;

    if-ne p1, v0, :cond_1f

    :cond_f
    const/4 v0, 0x1

    :goto_8
    move v0, v0

    .line 1947355
    if-eqz v0, :cond_5

    .line 1947356
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    invoke-virtual {v0, v2}, LX/Cuv;->b(Z)V

    .line 1947357
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    .line 1947358
    iput-boolean v2, v0, LX/Cuv;->f:Z

    .line 1947359
    goto/16 :goto_2

    :cond_10
    move v0, v2

    .line 1947360
    goto/16 :goto_3

    :cond_11
    move v0, v2

    .line 1947361
    goto/16 :goto_4

    :cond_12
    move-object v1, p1

    goto/16 :goto_5

    .line 1947362
    :cond_13
    iget-boolean v1, p0, LX/Cuw;->h:Z

    if-eqz v1, :cond_14

    sget-object v1, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    if-ne p1, v1, :cond_14

    .line 1947363
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 1947364
    :cond_14
    iget-boolean v1, p0, LX/Cuw;->h:Z

    if-eqz v1, :cond_2

    sget-object v1, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    if-eq p1, v1, :cond_15

    sget-object v1, LX/Cuj;->ATTEMPT_TO_PLAY:LX/Cuj;

    if-eq p1, v1, :cond_15

    sget-object v1, LX/Cuj;->SYSTEM_VIDEO_PLAY:LX/Cuj;

    if-eq p1, v1, :cond_15

    sget-object v1, LX/Cuj;->USER_CLICK_MEDIA:LX/Cuj;

    if-ne p1, v1, :cond_2

    .line 1947365
    :cond_15
    iput-boolean v0, p0, LX/Cuw;->h:Z

    goto/16 :goto_1

    :cond_16
    const/4 v5, 0x0

    goto/16 :goto_6

    .line 1947366
    :cond_17
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 1947367
    iget-object v5, p0, LX/Cuw;->i:LX/Cso;

    if-nez v5, :cond_1c

    .line 1947368
    :cond_18
    :goto_9
    iget-boolean v4, v0, LX/Cuk;->b:Z

    move v4, v4

    .line 1947369
    if-eqz v4, :cond_1a

    .line 1947370
    iget-object v4, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v4

    if-nez v4, :cond_19

    .line 1947371
    iget-object v4, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v5, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v4, v5}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1947372
    :cond_19
    :goto_a
    iget-object v4, v0, LX/Cuk;->a:LX/Cqu;

    move-object v4, v4

    .line 1947373
    if-eqz v4, :cond_1b

    .line 1947374
    iget-object v4, v0, LX/Cuk;->a:LX/Cqu;

    move-object v4, v4

    .line 1947375
    sget-object v5, LX/Cqu;->EXPANDED:LX/Cqu;

    if-ne v4, v5, :cond_1b

    :goto_b
    iput-boolean v2, p0, LX/Cuw;->k:Z

    goto/16 :goto_7

    .line 1947376
    :cond_1a
    iget-object v4, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v4

    if-eqz v4, :cond_19

    .line 1947377
    iget-object v4, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v5, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v4, v5}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_a

    :cond_1b
    move v2, v3

    .line 1947378
    goto :goto_b

    .line 1947379
    :cond_1c
    iget-object v5, v0, LX/Cuk;->a:LX/Cqu;

    move-object v5, v5

    .line 1947380
    sget-object v9, LX/Cqu;->COLLAPSED:LX/Cqu;

    if-ne v5, v9, :cond_1e

    move v5, v4

    .line 1947381
    :goto_c
    if-nez v5, :cond_1d

    iget-boolean v9, p0, LX/Cuw;->k:Z

    if-nez v9, :cond_18

    .line 1947382
    :cond_1d
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v8

    iget-boolean v8, p0, LX/Cuw;->j:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v9, v4

    .line 1947383
    iget-object v4, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    instance-of v4, v4, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    if-eqz v4, :cond_18

    .line 1947384
    iget-object v4, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    check-cast v4, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    iget-object v8, p0, LX/Cuw;->i:LX/Cso;

    iget-boolean v9, p0, LX/Cuw;->j:Z

    invoke-virtual {v8, v5, v9}, LX/Cso;->a(ZZ)LX/Cuo;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->a(LX/Cuo;)V

    goto :goto_9

    :cond_1e
    move v5, v8

    .line 1947385
    goto :goto_c

    :cond_1f
    const/4 v0, 0x0

    goto/16 :goto_8
.end method

.method public final a(LX/Cuk;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1947279
    iget-object v0, p0, LX/Cuw;->b:LX/Cum;

    .line 1947280
    iget-object p0, v0, LX/Cum;->b:LX/Cuk;

    move-object v0, p0

    .line 1947281
    invoke-virtual {v0, p1}, LX/Cuk;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1947291
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    if-nez v0, :cond_0

    .line 1947292
    const/4 v0, 0x0

    .line 1947293
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    .line 1947294
    iget-boolean p0, v0, LX/Cuv;->b:Z

    move v0, p0

    .line 1947295
    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1947288
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    if-nez v0, :cond_0

    .line 1947289
    const/4 v0, 0x0

    .line 1947290
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Cuw;->e:LX/Cuv;

    iget-boolean v0, v0, LX/Cuv;->d:Z

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1947285
    iget-object v0, p0, LX/Cuw;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_1

    .line 1947286
    :cond_0
    :goto_0
    return-void

    .line 1947287
    :cond_1
    iget-object v0, p0, LX/Cuw;->d:Landroid/os/Bundle;

    const-string v1, "player_current_position"

    iget-object v2, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1947282
    iget-object v0, p0, LX/Cuw;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_1

    .line 1947283
    :cond_0
    :goto_0
    return-void

    .line 1947284
    :cond_1
    iget-object v0, p0, LX/Cuw;->c:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/Cuw;->d:Landroid/os/Bundle;

    const-string v2, "player_current_position"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    goto :goto_0
.end method
