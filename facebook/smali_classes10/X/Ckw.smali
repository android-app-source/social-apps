.class public LX/Ckw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/ClD;

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/Chi;

.field public final e:LX/ClO;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/8bZ;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;LX/ClD;Landroid/content/Context;LX/Chi;LX/ClO;LX/8bZ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1931788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931789
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Ckw;->f:Ljava/util/Set;

    .line 1931790
    iput-object p1, p0, LX/Ckw;->a:LX/0Zb;

    .line 1931791
    iput-object p2, p0, LX/Ckw;->b:LX/ClD;

    .line 1931792
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Ckw;->c:Ljava/lang/ref/WeakReference;

    .line 1931793
    iput-object p4, p0, LX/Ckw;->d:LX/Chi;

    .line 1931794
    iput-object p5, p0, LX/Ckw;->e:LX/ClO;

    .line 1931795
    iput-object p6, p0, LX/Ckw;->g:LX/8bZ;

    .line 1931796
    return-void
.end method

.method public static a(LX/0QB;)LX/Ckw;
    .locals 10

    .prologue
    .line 1931777
    const-class v1, LX/Ckw;

    monitor-enter v1

    .line 1931778
    :try_start_0
    sget-object v0, LX/Ckw;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1931779
    sput-object v2, LX/Ckw;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1931780
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931781
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1931782
    new-instance v3, LX/Ckw;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v5

    check-cast v5, LX/ClD;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v7

    check-cast v7, LX/Chi;

    invoke-static {v0}, LX/ClO;->a(LX/0QB;)LX/ClO;

    move-result-object v8

    check-cast v8, LX/ClO;

    invoke-static {v0}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object v9

    check-cast v9, LX/8bZ;

    invoke-direct/range {v3 .. v9}, LX/Ckw;-><init>(LX/0Zb;LX/ClD;Landroid/content/Context;LX/Chi;LX/ClO;LX/8bZ;)V

    .line 1931783
    move-object v0, v3

    .line 1931784
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1931785
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ckw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1931786
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1931787
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/Ckw;LX/0oG;Ljava/util/Map;)V
    .locals 3
    .param p0    # LX/Ckw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/0oG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0oG;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1931752
    if-nez p1, :cond_1

    .line 1931753
    :cond_0
    :goto_0
    return-void

    .line 1931754
    :cond_1
    invoke-virtual {p1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931755
    if-nez p2, :cond_2

    .line 1931756
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 1931757
    :cond_2
    iget-object v0, p0, LX/Ckw;->b:LX/ClD;

    .line 1931758
    iget-object v1, v0, LX/ClD;->h:Ljava/lang/String;

    move-object v0, v1

    .line 1931759
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1931760
    const-string v0, "instant_articles_session_id"

    iget-object v1, p0, LX/Ckw;->b:LX/ClD;

    .line 1931761
    iget-object v2, v1, LX/ClD;->h:Ljava/lang/String;

    move-object v1, v2

    .line 1931762
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1931763
    :cond_3
    iget-object v0, p0, LX/Ckw;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1931764
    const-string v1, "article_depth_level"

    iget-object v2, p0, LX/Ckw;->b:LX/ClD;

    iget-object v0, p0, LX/Ckw;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v2, v0}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p1, v1, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1931765
    iget-object v1, p0, LX/Ckw;->b:LX/ClD;

    iget-object v0, p0, LX/Ckw;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, LX/ClD;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1931766
    const-string v1, "article_chaining_ID"

    iget-object v2, p0, LX/Ckw;->b:LX/ClD;

    iget-object v0, p0, LX/Ckw;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v2, v0}, LX/ClD;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1931767
    :cond_4
    const-string v0, "article_ID"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1931768
    const-string v0, "article_ID"

    iget-object v1, p0, LX/Ckw;->h:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931769
    const-string v0, "click_source"

    iget-object v1, p0, LX/Ckw;->i:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931770
    :cond_5
    const-string v0, "open_action"

    iget-object v1, p0, LX/Ckw;->j:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931771
    iget-object v0, p0, LX/Ckw;->g:LX/8bZ;

    invoke-virtual {v0}, LX/8bZ;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "tablet"

    .line 1931772
    :goto_1
    const-string v1, "device_type"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931773
    invoke-virtual {p1, p2}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 1931774
    const-string v0, "native_article_story"

    invoke-virtual {p1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1931775
    invoke-virtual {p1}, LX/0oG;->d()V

    goto/16 :goto_0

    .line 1931776
    :cond_6
    const-string v0, "phone"

    goto :goto_1
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1931745
    if-nez p3, :cond_0

    .line 1931746
    :goto_0
    return-void

    .line 1931747
    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    .line 1931748
    const-string v0, "share_type"

    const-string v1, "edit_and_share_successful"

    invoke-interface {p3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931749
    :cond_1
    :goto_1
    invoke-virtual {p0, p2, p3}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 1931750
    :cond_2
    if-nez p1, :cond_1

    .line 1931751
    const-string v0, "share_type"

    const-string v1, "edit_and_share_abandoned"

    invoke-interface {p3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1931740
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1931741
    const-string v1, "interaction"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931742
    const-string v1, "ia_source"

    const-string v2, "native_article_text_block"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931743
    const-string v1, "android_native_article_block_interaction"

    invoke-virtual {p0, v1, v0}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 1931744
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 1931731
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1931732
    const-string v1, "interaction"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931733
    const-string v1, "block_media_type"

    const-string v2, "slideshow"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931734
    const-string v1, "current_slide"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931735
    const-string v1, "total_slides"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931736
    if-eqz p3, :cond_0

    .line 1931737
    const-string v1, "swipe_percent"

    mul-int/lit8 v2, p2, 0x64

    div-int/2addr v2, p3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931738
    :cond_0
    const-string v1, "android_native_article_block_interaction"

    invoke-virtual {p0, v1, v0}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 1931739
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1931723
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1931724
    const-string v1, "interaction"

    const-string v2, "link_tap"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931725
    const-string v1, "webview_URL"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931726
    const-string v1, "ia_source"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931727
    invoke-static {p3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1931728
    const-string v1, "block_id"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931729
    :cond_0
    const-string v1, "android_native_article_block_interaction"

    invoke-virtual {p0, v1, v0}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 1931730
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1931710
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Ckw;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1931711
    :cond_0
    :goto_0
    return-void

    .line 1931712
    :cond_1
    if-nez p2, :cond_2

    .line 1931713
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 1931714
    :cond_2
    iget-object v0, p0, LX/Ckw;->d:LX/Chi;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Ckw;->d:LX/Chi;

    .line 1931715
    iget-object v1, v0, LX/Chi;->g:LX/0lF;

    move-object v0, v1

    .line 1931716
    if-eqz v0, :cond_3

    .line 1931717
    const-string v0, "instant_article_tracking_codes"

    iget-object v1, p0, LX/Ckw;->d:LX/Chi;

    .line 1931718
    iget-object v2, v1, LX/Chi;->g:LX/0lF;

    move-object v1, v2

    .line 1931719
    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931720
    :cond_3
    const-string v0, "instant_article_element_id"

    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931721
    const-string v0, "instant_article_vpv"

    invoke-virtual {p0, v0, p2}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 1931722
    iget-object v0, p0, LX/Ckw;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1931685
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 1931686
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1931705
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1931706
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1931707
    const-string v1, "instant_article_media_id"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931708
    :cond_0
    invoke-virtual {p0, p1, v0}, LX/Ckw;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1931709
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1931693
    iget-object v0, p0, LX/Ckw;->a:LX/0Zb;

    const-string v1, "open_link"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1931694
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1931695
    :goto_0
    return-void

    .line 1931696
    :cond_0
    const-string v1, "url"

    invoke-virtual {v0, v1}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 1931697
    iget-object v1, p0, LX/Ckw;->e:LX/ClO;

    .line 1931698
    iget-boolean v2, v1, LX/ClO;->b:Z

    move v1, v2

    .line 1931699
    if-eqz v1, :cond_2

    .line 1931700
    new-instance v1, LX/ClL;

    const-string v2, "open_link"

    invoke-direct {v1, v2}, LX/ClL;-><init>(Ljava/lang/String;)V

    .line 1931701
    if-eqz p2, :cond_1

    const-string v2, "click_source"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1931702
    const-string v2, "click_source"

    const-string v3, "click_source"

    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/ClL;->a(Ljava/lang/String;Ljava/lang/Object;)LX/ClL;

    .line 1931703
    :cond_1
    iget-object v2, p0, LX/Ckw;->e:LX/ClO;

    invoke-virtual {v1}, LX/ClL;->a()LX/ClM;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/ClO;->a(LX/ClM;)V

    .line 1931704
    :cond_2
    invoke-static {p0, v0, p2}, LX/Ckw;->a(LX/Ckw;LX/0oG;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1931691
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/Ckw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1931692
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1931687
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931688
    :goto_0
    return-void

    .line 1931689
    :cond_0
    iget-object v0, p0, LX/Ckw;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1931690
    invoke-static {p0, v0, p2}, LX/Ckw;->a(LX/Ckw;LX/0oG;Ljava/util/Map;)V

    goto :goto_0
.end method
