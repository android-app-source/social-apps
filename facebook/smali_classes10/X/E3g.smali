.class public final LX/E3g;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic c:LX/1Pn;

.field public final synthetic d:LX/E2e;

.field public final synthetic e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;LX/0Px;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;LX/E2e;)V
    .locals 0

    .prologue
    .line 2074991
    iput-object p1, p0, LX/E3g;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    iput-object p2, p0, LX/E3g;->a:LX/0Px;

    iput-object p3, p0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p4, p0, LX/E3g;->c:LX/1Pn;

    iput-object p5, p0, LX/E3g;->d:LX/E2e;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2074992
    iget-object v0, p0, LX/E3g;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    iget v2, v0, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->g:I

    .line 2074993
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/E3g;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2074994
    iget-object v0, p0, LX/E3g;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2074995
    new-instance v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v4, p0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074996
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v4, v5

    .line 2074997
    iget-object v5, p0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2074998
    iget-object v6, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v6

    .line 2074999
    iget-object v6, p0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075000
    iget-object v7, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v7

    .line 2075001
    invoke-direct {v3, v4, v0, v5, v6}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2075002
    new-instance v4, LX/E3f;

    invoke-direct {v4, p0, v2}, LX/E3f;-><init>(LX/E3g;I)V

    .line 2075003
    iget-object v5, p0, LX/E3g;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    iget-object v5, v5, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->c:LX/1vo;

    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/1vo;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 2075004
    instance-of v5, v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    if-eqz v5, :cond_1

    .line 2075005
    check-cast v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v5, p0, LX/E3g;->c:LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, LX/E3g;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    iget-object v6, v6, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->e:LX/1Qx;

    invoke-static {v0, v4, v5, v6}, LX/6Vo;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v0

    invoke-virtual {p1, v0, v3}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2075006
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2075007
    :cond_1
    instance-of v5, v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v5, :cond_0

    .line 2075008
    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    iget-object v5, p0, LX/E3g;->c:LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, LX/E3g;->e:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;

    iget-object v6, v6, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionHScrollXOutUnitComponentPartDefinition;->e:LX/1Qx;

    invoke-static {v0, v4, v5, v6}, LX/6Vo;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v0

    invoke-virtual {p1, v0, v3}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_1

    .line 2075009
    :cond_2
    return-void
.end method

.method public final c(I)V
    .locals 4

    .prologue
    .line 2075010
    iget-object v0, p0, LX/E3g;->c:LX/1Pn;

    check-cast v0, LX/1Pr;

    iget-object v1, p0, LX/E3g;->d:LX/E2e;

    iget-object v2, p0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2f;

    .line 2075011
    iput p1, v0, LX/E2f;->d:I

    .line 2075012
    iget-object v0, p0, LX/E3g;->c:LX/1Pn;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v1, p0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075013
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2075014
    iget-object v2, p0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075015
    iget-object v3, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2075016
    invoke-virtual {v0, v1, v2}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2075017
    iget-object v0, p0, LX/E3g;->c:LX/1Pn;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v1

    iget-object v0, p0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075018
    iget-object v2, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2075019
    iget-object v0, p0, LX/E3g;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2075020
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2075021
    iget-object v0, p0, LX/E3g;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    invoke-interface {v0}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, p1, v0}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 2075022
    return-void
.end method
