.class public final LX/Csh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;)V
    .locals 0

    .prologue
    .line 1943036
    iput-object p1, p0, LX/Csh;->a:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 1943037
    iget-object v1, p0, LX/Csh;->a:Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1943038
    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesDocumentLoadingProgressIndicator;->setAlpha(F)V

    .line 1943039
    return-void
.end method
