.class public final LX/Eug;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Eui;


# direct methods
.method public constructor <init>(LX/Eui;)V
    .locals 0

    .prologue
    .line 2179825
    iput-object p1, p0, LX/Eug;->a:LX/Eui;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2179826
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2179827
    if-eqz p1, :cond_0

    .line 2179828
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179829
    if-eqz v0, :cond_0

    .line 2179830
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179831
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$ContextualPymkModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2179832
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179833
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$ContextualPymkModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$ContextualPymkModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2179834
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2179835
    :goto_0
    return-object v0

    .line 2179836
    :cond_1
    iget-object v1, p0, LX/Eug;->a:LX/Eui;

    .line 2179837
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179838
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$ContextualPymkModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$ContextualPymkModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/Eui;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2179839
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2179840
    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel;->a()Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$ContextualPymkModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterFetchContextualPYMKSuggestionsModels$FriendsCenterFetchContextualPYMKSuggestionsQueryModel$ContextualPymkModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
