.class public final LX/Em9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/2KK;


# direct methods
.method public constructor <init>(LX/2KK;Z)V
    .locals 0

    .prologue
    .line 2165115
    iput-object p1, p0, LX/Em9;->b:LX/2KK;

    iput-boolean p2, p0, LX/Em9;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2165116
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2165117
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2165118
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2165119
    check-cast v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;

    invoke-virtual {v0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2165120
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2165121
    check-cast v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;

    invoke-virtual {v0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->a()LX/2uF;

    move-result-object v0

    .line 2165122
    iget-object v2, p0, LX/Em9;->b:LX/2KK;

    const-string v3, "query_result"

    invoke-interface {v0}, LX/3Sb;->c()I

    move-result v4

    .line 2165123
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "ott_wilde_tcp_device_discovery"

    invoke-direct {v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2165124
    const-string p1, "ott_device_discovery"

    .line 2165125
    iput-object p1, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2165126
    const-string p1, "event_type"

    invoke-virtual {v5, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2165127
    const-string p1, "count"

    invoke-virtual {v5, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2165128
    iget-object p1, v2, LX/2KK;->c:LX/0Zb;

    invoke-interface {p1, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2165129
    invoke-interface {v0}, LX/3Sb;->b()LX/2sN;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2165130
    iget-object v4, p0, LX/Em9;->b:LX/2KK;

    .line 2165131
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const-string v6, "tcp://"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 2165132
    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    .line 2165133
    const/4 v5, 0x1

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const-string v6, "tcp://"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 2165134
    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 2165135
    const/4 v6, 0x0

    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v7

    .line 2165136
    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v8

    .line 2165137
    const/16 v6, 0x20

    .line 2165138
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2165139
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2165140
    const-string v5, "bytes_to_read"

    invoke-virtual {v9, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 2165141
    :goto_1
    :try_start_2
    new-instance v6, LX/EmC;

    const/16 v9, 0xf

    invoke-direct {v6, v9, v7, v8, v5}, LX/EmC;-><init>(ILjava/net/InetAddress;II)V

    .line 2165142
    invoke-static {v4, v6}, LX/2KK;->a(LX/2KK;LX/EmC;)V
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2165143
    :goto_2
    goto :goto_0

    .line 2165144
    :cond_0
    iget-boolean v0, p0, LX/Em9;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Em9;->b:LX/2KK;

    iget-object v0, v0, LX/2KK;->u:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2165145
    iget-object v0, p0, LX/Em9;->b:LX/2KK;

    .line 2165146
    iget-object v2, v0, LX/2KK;->g:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->o()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 2165147
    if-eqz v2, :cond_1

    .line 2165148
    invoke-static {v0}, LX/2KK;->f(LX/2KK;)V

    .line 2165149
    new-instance v2, LX/4E6;

    invoke-direct {v2}, LX/4E6;-><init>()V

    .line 2165150
    const-string v3, "topic_tag"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2165151
    move-object v2, v2

    .line 2165152
    new-instance v3, LX/EmE;

    invoke-direct {v3}, LX/EmE;-><init>()V

    .line 2165153
    const-string v4, "0"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2165154
    :try_start_3
    new-instance v2, LX/EmA;

    invoke-direct {v2, v0}, LX/EmA;-><init>(LX/2KK;)V

    move-object v2, v2

    .line 2165155
    iget-object v4, v0, LX/2KK;->j:LX/0gX;

    invoke-virtual {v4, v3, v2}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v2

    iput-object v2, v0, LX/2KK;->p:LX/0gM;

    .line 2165156
    iput-object v1, v0, LX/2KK;->u:Ljava/lang/String;
    :try_end_3
    .catch LX/31B; {:try_start_3 .. :try_end_3} :catch_2

    .line 2165157
    :cond_1
    :goto_3
    return-void

    .line 2165158
    :catch_0
    :try_start_4
    move-exception v5

    .line 2165159
    invoke-virtual {v5}, Lorg/json/JSONException;->printStackTrace()V

    .line 2165160
    sget-object v9, LX/2KK;->b:Ljava/lang/Class;

    const-string p1, "Failed to read json command, will use default bytesToRead"

    invoke-static {v9, p1, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_1

    move v5, v6

    goto :goto_1

    .line 2165161
    :catch_1
    move-exception v5

    .line 2165162
    sget-object v6, LX/2KK;->b:Ljava/lang/Class;

    const-string v7, "Could not connect to socket endpoint"

    invoke-static {v6, v7, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_2
    goto :goto_3
.end method
