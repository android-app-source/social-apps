.class public final LX/Cy5;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic c:LX/Cy9;


# direct methods
.method public constructor <init>(LX/Cy9;Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 0

    .prologue
    .line 1952393
    iput-object p1, p0, LX/Cy5;->c:LX/Cy9;

    iput-object p2, p0, LX/Cy5;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object p3, p0, LX/Cy5;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1952394
    iget-object v0, p0, LX/Cy5;->c:LX/Cy9;

    iget-object v1, p0, LX/Cy5;->b:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v2, p0, LX/Cy5;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1952395
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x285feb

    if-ne v3, v4, :cond_0

    .line 1952396
    iget-object v3, v0, LX/Cy9;->i:LX/2hZ;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    .line 1952397
    new-instance p0, LX/Cy8;

    invoke-direct {p0, v0, v1, v2, v4}, LX/Cy8;-><init>(LX/Cy9;Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)V

    move-object v4, p0

    .line 1952398
    invoke-virtual {v3, p1, v4}, LX/2hZ;->a(Ljava/lang/Throwable;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1952399
    :cond_0
    iget-object v3, v0, LX/Cy9;->m:LX/2Sc;

    sget-object v4, LX/3Ql;->FAILED_MUTATION:LX/3Ql;

    invoke-virtual {v3, v4, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 1952400
    iget-object v3, v0, LX/Cy9;->a:LX/Cx5;

    invoke-interface {v3, v2, v1}, LX/Cx5;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1952401
    iget-object v3, v0, LX/Cy9;->b:LX/1Pq;

    invoke-interface {v3}, LX/1Pq;->iN_()V

    .line 1952402
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1952403
    iget-object v0, p0, LX/Cy5;->c:LX/Cy9;

    iget-object v0, v0, LX/Cy9;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/search/results/environment/entity/OldCanApplyEntityInlineActionImpl$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/search/results/environment/entity/OldCanApplyEntityInlineActionImpl$2$1;-><init>(LX/Cy5;)V

    const v2, -0x67692aa5

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1952404
    iget-object v0, p0, LX/Cy5;->c:LX/Cy9;

    iget-object v0, v0, LX/Cy9;->f:LX/CvY;

    iget-object v1, p0, LX/Cy5;->c:LX/Cy9;

    iget-object v1, v1, LX/Cy9;->c:LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    iget-object v2, p0, LX/Cy5;->c:LX/Cy9;

    iget-object v2, v2, LX/Cy9;->p:LX/Cz2;

    iget-object v3, p0, LX/Cy5;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v2, v3}, LX/Cz2;->b(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v2

    iget-object v3, p0, LX/Cy5;->c:LX/Cy9;

    iget-object v3, v3, LX/Cy9;->p:LX/Cz2;

    iget-object v4, p0, LX/Cy5;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v3, v4}, LX/Cz2;->a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v3

    iget-object v4, p0, LX/Cy5;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v6, p0, LX/Cy5;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v7, p0, LX/Cy5;->c:LX/Cy9;

    iget-object v7, v7, LX/Cy9;->c:LX/CxV;

    invoke-interface {v7}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v7

    iget-object v8, p0, LX/Cy5;->c:LX/Cy9;

    iget-object v8, v8, LX/Cy9;->d:LX/Cxc;

    iget-object v9, p0, LX/Cy5;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v8, v9}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v8

    iget-object v9, p0, LX/Cy5;->c:LX/Cy9;

    iget-object v9, v9, LX/Cy9;->d:LX/Cxc;

    iget-object v10, p0, LX/Cy5;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v9, v10}, LX/Cxc;->e(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v7, v8, v9}, LX/CvY;->a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1952405
    return-void
.end method
