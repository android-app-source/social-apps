.class public LX/CuU;
.super LX/Cts;
.source ""

# interfaces
.implements LX/Ctn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "LX/Ctn;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/richdocument/view/widget/SlideshowView;

.field private final b:I

.field private c:LX/CuT;

.field private d:F


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 1

    .prologue
    .line 1946712
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1946713
    sget-object v0, LX/CuT;->WAITING_FOR_DOWN:LX/CuT;

    iput-object v0, p0, LX/CuU;->c:LX/CuT;

    .line 1946714
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    iput-object v0, p0, LX/CuU;->a:Lcom/facebook/richdocument/view/widget/SlideshowView;

    .line 1946715
    invoke-interface {p1}, LX/Ctg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, LX/CuU;->b:I

    .line 1946716
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1946700
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 1946701
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, LX/CuU;->d:F

    .line 1946702
    sget-object v1, LX/CuT;->WAITING_TO_STEAL_GESTURE:LX/CuT;

    iput-object v1, p0, LX/CuU;->c:LX/CuT;

    .line 1946703
    :cond_0
    :goto_0
    return v0

    .line 1946704
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    iget-object v2, p0, LX/CuU;->c:LX/CuT;

    sget-object v3, LX/CuT;->WAITING_TO_STEAL_GESTURE:LX/CuT;

    if-ne v2, v3, :cond_2

    .line 1946705
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, LX/CuU;->d:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 1946706
    iget v3, p0, LX/CuU;->b:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 1946707
    iget-object v0, p0, LX/CuU;->a:Lcom/facebook/richdocument/view/widget/SlideshowView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/SlideshowView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1946708
    sget-object v0, LX/CuT;->SCROLLING_SLIDESHOW:LX/CuT;

    iput-object v0, p0, LX/CuU;->c:LX/CuT;

    move v0, v1

    .line 1946709
    goto :goto_0

    .line 1946710
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 1946711
    sget-object v1, LX/CuT;->WAITING_FOR_DOWN:LX/CuT;

    iput-object v1, p0, LX/CuU;->c:LX/CuT;

    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1946717
    const/4 v0, 0x0

    return v0
.end method
