.class public LX/Dvl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Dvl;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvs;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DwH;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dvs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DwH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2059123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059124
    iput-object p1, p0, LX/Dvl;->a:LX/0Ot;

    .line 2059125
    iput-object p2, p0, LX/Dvl;->b:LX/0Ot;

    .line 2059126
    return-void
.end method

.method public static a(LX/0QB;)LX/Dvl;
    .locals 5

    .prologue
    .line 2059127
    sget-object v0, LX/Dvl;->c:LX/Dvl;

    if-nez v0, :cond_1

    .line 2059128
    const-class v1, LX/Dvl;

    monitor-enter v1

    .line 2059129
    :try_start_0
    sget-object v0, LX/Dvl;->c:LX/Dvl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2059130
    if-eqz v2, :cond_0

    .line 2059131
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2059132
    new-instance v3, LX/Dvl;

    const/16 v4, 0x2e9a

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2e9c

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/Dvl;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2059133
    move-object v0, v3

    .line 2059134
    sput-object v0, LX/Dvl;->c:LX/Dvl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2059135
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2059136
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2059137
    :cond_1
    sget-object v0, LX/Dvl;->c:LX/Dvl;

    return-object v0

    .line 2059138
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2059139
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/pandora/common/data/model/PandoraMultiPhotoStoryModel;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraMultiPhotoStoryModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x3

    const/4 v2, 0x0

    .line 2059140
    iget-object v0, p0, LX/Dvl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DwH;

    iget-object v1, p1, Lcom/facebook/photos/pandora/common/data/model/PandoraMultiPhotoStoryModel;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1}, LX/DwH;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v3

    .line 2059141
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2059142
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    :goto_0
    if-ge v1, v7, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2059143
    iget-object v8, p0, LX/Dvl;->a:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/Dvs;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)D

    move-result-wide v8

    .line 2059144
    new-instance v10, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;

    invoke-direct {v10, p1, v0, v8, v9}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;-><init>(Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;Lcom/facebook/graphql/model/GraphQLPhoto;D)V

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2059145
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2059146
    :cond_0
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    move-object v3, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;

    .line 2059147
    if-eqz v3, :cond_1

    iget-wide v8, v3, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->c:D

    iget-wide v10, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->c:D

    cmpg-double v8, v8, v10

    if-gez v8, :cond_2

    :cond_1
    iget-wide v8, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->c:D

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpl-double v8, v8, v10

    if-lez v8, :cond_2

    move-object v3, v0

    .line 2059148
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 2059149
    const/4 v8, 0x6

    if-eq v1, v8, :cond_3

    invoke-interface {v6, v0}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne v0, v8, :cond_b

    .line 2059150
    :cond_3
    if-eqz v3, :cond_a

    .line 2059151
    const/4 v0, 0x1

    iput-boolean v0, v3, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->d:Z

    move v0, v2

    move-object v1, v4

    :goto_2
    move-object v3, v1

    move v1, v0

    .line 2059152
    goto :goto_1

    .line 2059153
    :cond_4
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2059154
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2059155
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v5

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;

    .line 2059156
    iget-boolean v6, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->d:Z

    if-eqz v6, :cond_5

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->a()LX/Dvz;

    move-result-object v6

    sget-object v7, LX/Dvz;->LANDSCAPE:LX/Dvz;

    if-ne v6, v7, :cond_5

    .line 2059157
    new-instance v6, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;-><init>(LX/0Px;)V

    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2059158
    :cond_5
    iget-boolean v6, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->d:Z

    if-eqz v6, :cond_6

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->a()LX/Dvz;

    move-result-object v6

    sget-object v7, LX/Dvz;->PORTRAIT:LX/Dvz;

    if-ne v6, v7, :cond_6

    .line 2059159
    const/4 v1, 0x4

    .line 2059160
    :cond_6
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2059161
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_9

    .line 2059162
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;-><init>(LX/0Px;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2059163
    invoke-interface {v3}, Ljava/util/List;->clear()V

    move v0, v5

    :goto_4
    move v1, v0

    .line 2059164
    goto :goto_3

    .line 2059165
    :cond_7
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2059166
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;-><init>(LX/0Px;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2059167
    :cond_8
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_9
    move v0, v1

    goto :goto_4

    :cond_a
    move v0, v2

    move-object v1, v3

    goto :goto_2

    :cond_b
    move v0, v1

    move-object v1, v3

    goto :goto_2
.end method
