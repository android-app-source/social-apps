.class public final LX/EGV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EFr;


# instance fields
.field public final synthetic a:LX/EGe;


# direct methods
.method public constructor <init>(LX/EGe;)V
    .locals 0

    .prologue
    .line 2097367
    iput-object p1, p0, LX/EGV;->a:LX/EGe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/WindowManager$LayoutParams;
    .locals 6

    .prologue
    const/4 v1, -0x2

    .line 2097368
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d7

    const v4, 0x1400228

    move v2, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 2097369
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2097370
    iget-object v1, p0, LX/EGV;->a:LX/EGe;

    iget-object v1, v1, LX/EGe;->ag:LX/EFp;

    .line 2097371
    iget v2, v1, LX/EFp;->h:I

    move v1, v2

    .line 2097372
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2097373
    iget-object v1, p0, LX/EGV;->a:LX/EGe;

    iget-object v1, v1, LX/EGe;->ag:LX/EFp;

    .line 2097374
    iget v2, v1, LX/EFp;->i:I

    move v1, v2

    .line 2097375
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2097376
    return-object v0
.end method

.method public final b()Landroid/widget/FrameLayout$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 2097377
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x33

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 2097378
    iget-object v1, p0, LX/EGV;->a:LX/EGe;

    iget-object v1, v1, LX/EGe;->ag:LX/EFp;

    .line 2097379
    iget v2, v1, LX/EFp;->h:I

    move v1, v2

    .line 2097380
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2097381
    iget-object v1, p0, LX/EGV;->a:LX/EGe;

    iget-object v1, v1, LX/EGe;->ag:LX/EFp;

    .line 2097382
    iget v2, v1, LX/EFp;->i:I

    move v1, v2

    .line 2097383
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 2097384
    return-object v0
.end method
