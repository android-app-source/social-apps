.class public final LX/Eyc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Eyd;


# direct methods
.method public constructor <init>(LX/Eyd;)V
    .locals 0

    .prologue
    .line 2186401
    iput-object p1, p0, LX/Eyc;->a:LX/Eyd;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2186402
    iget-object v0, p0, LX/Eyc;->a:LX/Eyd;

    iget-object v0, v0, LX/Eyd;->b:LX/2iU;

    iget-object v1, p0, LX/Eyc;->a:LX/Eyd;

    iget-object v1, v1, LX/Eyd;->a:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v1}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/2iU;->a$redex0(LX/2iU;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2186403
    iget-object v0, p0, LX/Eyc;->a:LX/Eyd;

    iget-object v0, v0, LX/Eyd;->b:LX/2iU;

    iget-object v0, v0, LX/2iU;->c:LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    .line 2186404
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2186405
    iget-object v0, p0, LX/Eyc;->a:LX/Eyd;

    iget-object v0, v0, LX/Eyd;->b:LX/2iU;

    iget-object v1, p0, LX/Eyc;->a:LX/Eyd;

    iget-object v1, v1, LX/Eyd;->a:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v1}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/2iU;->a$redex0(LX/2iU;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2186406
    return-void
.end method
