.class public LX/Dqr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Dqr;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;",
            "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2048702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2048703
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Dqr;->a:Ljava/util/Map;

    .line 2048704
    iget-object v0, p0, LX/Dqr;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->BASIC_MENU:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048705
    iget-object v0, p0, LX/Dqr;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->TEXT_WITH_BUTTON:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048706
    iget-object v0, p0, LX/Dqr;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->PROFILE_IMAGE_OPTION:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048707
    iget-object v0, p0, LX/Dqr;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;->WASH_TEXTS:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048708
    return-void
.end method

.method public static a(LX/0QB;)LX/Dqr;
    .locals 6

    .prologue
    .line 2048709
    sget-object v0, LX/Dqr;->b:LX/Dqr;

    if-nez v0, :cond_1

    .line 2048710
    const-class v1, LX/Dqr;

    monitor-enter v1

    .line 2048711
    :try_start_0
    sget-object v0, LX/Dqr;->b:LX/Dqr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2048712
    if-eqz v2, :cond_0

    .line 2048713
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2048714
    new-instance p0, LX/Dqr;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;->a(LX/0QB;)Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, LX/Dqr;-><init>(Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsTextWithButtonPartDefinition;Lcom/facebook/notifications/settings/partdefinitions/NotificationSettingsWashTextPartDefinition;)V

    .line 2048715
    move-object v0, p0

    .line 2048716
    sput-object v0, LX/Dqr;->b:LX/Dqr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2048717
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2048718
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2048719
    :cond_1
    sget-object v0, LX/Dqr;->b:LX/Dqr;

    return-object v0

    .line 2048720
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2048721
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;)Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2048722
    iget-object v0, p0, LX/Dqr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLNotifOptionRowDisplayStyle;)Z
    .locals 1

    .prologue
    .line 2048723
    iget-object v0, p0, LX/Dqr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
