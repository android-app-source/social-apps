.class public LX/Cq3;
.super Landroid/webkit/WebViewClient;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:J

.field public final synthetic c:LX/CqA;


# direct methods
.method public constructor <init>(LX/CqA;)V
    .locals 0

    .prologue
    .line 1938976
    iput-object p1, p0, LX/Cq3;->c:LX/CqA;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1938977
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1938978
    iget-object v0, p0, LX/Cq3;->c:LX/CqA;

    iget-object v0, v0, LX/CqA;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/Cq3;->a:J

    .line 1938979
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1938980
    if-nez p2, :cond_1

    .line 1938981
    :cond_0
    :goto_0
    return v0

    .line 1938982
    :cond_1
    iget-object v1, p0, LX/Cq3;->c:LX/CqA;

    iget-object v1, v1, LX/CqA;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/Cq3;->a:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0xfa

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 1938983
    iget-object v0, p0, LX/Cq3;->c:LX/CqA;

    invoke-static {v0, p2}, LX/CqA;->a$redex0(LX/CqA;Ljava/lang/String;)V

    .line 1938984
    const/4 v0, 0x1

    goto :goto_0
.end method
