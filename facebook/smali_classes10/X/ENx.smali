.class public LX/ENx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/EO7;

.field private final c:LX/EOH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EOH",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:LX/EPO;

.field private final e:LX/EPK;


# direct methods
.method public constructor <init>(LX/0Ot;LX/EO7;LX/EOH;LX/EPO;LX/EPK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/EO7;",
            "LX/EOH;",
            "LX/EPO;",
            "LX/EPK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2113763
    iput-object p1, p0, LX/ENx;->a:LX/0Ot;

    .line 2113764
    iput-object p2, p0, LX/ENx;->b:LX/EO7;

    .line 2113765
    iput-object p3, p0, LX/ENx;->c:LX/EOH;

    .line 2113766
    iput-object p4, p0, LX/ENx;->d:LX/EPO;

    .line 2113767
    iput-object p5, p0, LX/ENx;->e:LX/EPK;

    .line 2113768
    return-void
.end method

.method public static a(LX/0QB;)LX/ENx;
    .locals 9

    .prologue
    .line 2113698
    const-class v1, LX/ENx;

    monitor-enter v1

    .line 2113699
    :try_start_0
    sget-object v0, LX/ENx;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113700
    sput-object v2, LX/ENx;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113701
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113702
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113703
    new-instance v3, LX/ENx;

    const/16 v4, 0x32d4

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/EO7;->a(LX/0QB;)LX/EO7;

    move-result-object v5

    check-cast v5, LX/EO7;

    invoke-static {v0}, LX/EOH;->a(LX/0QB;)LX/EOH;

    move-result-object v6

    check-cast v6, LX/EOH;

    invoke-static {v0}, LX/EPO;->a(LX/0QB;)LX/EPO;

    move-result-object v7

    check-cast v7, LX/EPO;

    invoke-static {v0}, LX/EPK;->b(LX/0QB;)LX/EPK;

    move-result-object v8

    check-cast v8, LX/EPK;

    invoke-direct/range {v3 .. v8}, LX/ENx;-><init>(LX/0Ot;LX/EO7;LX/EOH;LX/EPO;LX/EPK;)V

    .line 2113704
    move-object v0, v3

    .line 2113705
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113706
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ENx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113707
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113708
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/CzL;LX/1Ps;ZI)LX/1Dg;
    .locals 9
    .param p2    # LX/CzL;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Ps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleInterfaces$SearchResultsOpinionModule;",
            ">;TE;ZI)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 2113709
    iget-object v0, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2113710
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v3

    .line 2113711
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2113712
    invoke-virtual {v3, p5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bb()Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel;->a()LX/0Px;

    move-result-object v2

    move v1, v6

    .line 2113713
    :goto_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2113714
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleModels$SearchResultsOpinionDataResultsModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2113715
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2113716
    new-instance v4, LX/C33;

    sget-object v5, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    invoke-direct {v4, v8, v5, v0, v8}, LX/C33;-><init>(Landroid/view/View$OnClickListener;LX/C34;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2113717
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2113718
    :cond_0
    if-eqz p4, :cond_1

    .line 2113719
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, LX/ENx;->c:LX/EOH;

    invoke-virtual {v1, p1}, LX/EOH;->c(LX/1De;)LX/EOF;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/EOF;->a(LX/CzL;)LX/EOF;

    move-result-object v1

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EOF;->a(LX/0Px;)LX/EOF;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/EOF;->a(LX/1Ps;)LX/EOF;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v1

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, LX/25Q;->i(I)LX/25Q;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b0033

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, LX/ENx;->d:LX/EPO;

    invoke-virtual {v1, p1}, LX/EPO;->c(LX/1De;)LX/EPM;

    move-result-object v1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0822c8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EPM;->a(Ljava/lang/CharSequence;)LX/EPM;

    move-result-object v1

    iget-object v2, p0, LX/ENx;->e:LX/EPK;

    check-cast p3, LX/Cxi;

    invoke-virtual {v2, p2, p3}, LX/EPK;->a(LX/CzL;LX/Cxi;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EPM;->a(Landroid/view/View$OnClickListener;)LX/EPM;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x7

    const v3, 0x7f0b0060

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    .line 2113720
    :goto_1
    return-object v0

    .line 2113721
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2113722
    invoke-virtual {v3, p5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move v1, v6

    .line 2113723
    :goto_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2113724
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2113725
    invoke-interface {v0}, LX/8d7;->aP()LX/8dH;

    move-result-object v0

    invoke-interface {v0}, LX/8dH;->j()LX/8dG;

    move-result-object v0

    invoke-interface {v0}, LX/8dG;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2113726
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2113727
    :cond_2
    new-instance v0, LX/ENw;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/ENw;-><init>(LX/ENx;LX/1De;LX/0Px;LX/1Ps;LX/CzL;)V

    .line 2113728
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    .line 2113729
    new-instance v3, LX/5Jh;

    invoke-direct {v3}, LX/5Jh;-><init>()V

    .line 2113730
    sget-object v4, LX/5Ji;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5Jg;

    .line 2113731
    if-nez v4, :cond_3

    .line 2113732
    new-instance v4, LX/5Jg;

    invoke-direct {v4}, LX/5Jg;-><init>()V

    .line 2113733
    :cond_3
    invoke-static {v4, p1, v2, v2, v3}, LX/5Jg;->a$redex0(LX/5Jg;LX/1De;IILX/5Jh;)V

    .line 2113734
    move-object v3, v4

    .line 2113735
    move-object v2, v3

    .line 2113736
    move-object v2, v2

    .line 2113737
    const/4 v3, 0x0

    .line 2113738
    new-instance v4, LX/EO3;

    invoke-direct {v4}, LX/EO3;-><init>()V

    .line 2113739
    sget-object v5, LX/EO4;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/EO2;

    .line 2113740
    if-nez v5, :cond_4

    .line 2113741
    new-instance v5, LX/EO2;

    invoke-direct {v5}, LX/EO2;-><init>()V

    .line 2113742
    :cond_4
    invoke-static {v5, p1, v3, v3, v4}, LX/EO2;->a$redex0(LX/EO2;LX/1De;IILX/EO3;)V

    .line 2113743
    move-object v4, v5

    .line 2113744
    move-object v3, v4

    .line 2113745
    move-object v3, v3

    .line 2113746
    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    .line 2113747
    iget-object v5, v3, LX/EO2;->a:LX/EO3;

    iput-object v4, v5, LX/EO3;->b:LX/0Px;

    .line 2113748
    iget-object v5, v3, LX/EO2;->d:Ljava/util/BitSet;

    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Ljava/util/BitSet;->set(I)V

    .line 2113749
    move-object v3, v3

    .line 2113750
    iget-object v4, v3, LX/EO2;->a:LX/EO3;

    iput p5, v4, LX/EO3;->a:I

    .line 2113751
    iget-object v4, v3, LX/EO2;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2113752
    move-object v3, v3

    .line 2113753
    iget-object v4, v3, LX/EO2;->a:LX/EO3;

    iput-object v0, v4, LX/EO3;->c:LX/ENw;

    .line 2113754
    iget-object v4, v3, LX/EO2;->d:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2113755
    move-object v0, v3

    .line 2113756
    iget-object v3, v2, LX/5Jg;->a:LX/5Jh;

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    iput-object v4, v3, LX/5Jh;->a:LX/1X1;

    .line 2113757
    iget-object v3, v2, LX/5Jg;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2113758
    move-object v0, v2

    .line 2113759
    iget-object v2, v0, LX/5Jg;->a:LX/5Jh;

    iput-boolean v6, v2, LX/5Jh;->g:Z

    .line 2113760
    move-object v0, v0

    .line 2113761
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v1

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, LX/25Q;->i(I)LX/25Q;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b0033

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, LX/ENx;->c:LX/EOH;

    invoke-virtual {v1, p1}, LX/EOH;->c(LX/1De;)LX/EOF;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/EOF;->a(LX/CzL;)LX/EOF;

    move-result-object v1

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EOF;->a(LX/0Px;)LX/EOF;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/EOF;->a(LX/1Ps;)LX/EOF;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v1

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, LX/25Q;->i(I)LX/25Q;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b0033

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_1
.end method
