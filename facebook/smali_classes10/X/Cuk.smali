.class public final LX/Cuk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Cqu;

.field public final b:Z

.field public final c:Z


# direct methods
.method public constructor <init>(LX/Cqu;ZZ)V
    .locals 0

    .prologue
    .line 1947123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1947124
    iput-object p1, p0, LX/Cuk;->a:LX/Cqu;

    .line 1947125
    iput-boolean p2, p0, LX/Cuk;->b:Z

    .line 1947126
    iput-boolean p3, p0, LX/Cuk;->c:Z

    .line 1947127
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1947129
    if-ne p0, p1, :cond_1

    .line 1947130
    :cond_0
    :goto_0
    return v0

    .line 1947131
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1947132
    goto :goto_0

    .line 1947133
    :cond_3
    check-cast p1, LX/Cuk;

    .line 1947134
    iget-object v2, p0, LX/Cuk;->a:LX/Cqu;

    iget-object v3, p1, LX/Cuk;->a:LX/Cqu;

    invoke-virtual {v2, v3}, LX/Cqu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, LX/Cuk;->b:Z

    iget-boolean v3, p1, LX/Cuk;->b:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, LX/Cuk;->c:Z

    iget-boolean v3, p1, LX/Cuk;->c:Z

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1947135
    iget-object v0, p0, LX/Cuk;->a:LX/Cqu;

    iget-boolean v1, p0, LX/Cuk;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p0, LX/Cuk;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1bi;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1947128
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "{mode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/Cuk;->a:LX/Cqu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playing: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/Cuk;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", controls: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/Cuk;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
