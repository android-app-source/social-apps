.class public final LX/DSI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;)V
    .locals 0

    .prologue
    .line 1999340
    iput-object p1, p0, LX/DSI;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1999332
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 1999333
    iget-object v0, p0, LX/DSI;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->u:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1999334
    :goto_0
    iget-object v0, p0, LX/DSI;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1999335
    iput-object v1, v0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->w:Ljava/lang/String;

    .line 1999336
    iget-object v0, p0, LX/DSI;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Landroid/text/Editable;)V

    .line 1999337
    iget-object v0, p0, LX/DSI;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Z)V

    .line 1999338
    return-void

    .line 1999339
    :cond_0
    iget-object v0, p0, LX/DSI;->a:Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1999341
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1999331
    return-void
.end method
