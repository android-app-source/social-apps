.class public LX/Erv;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Erv;


# direct methods
.method public constructor <init>(LX/1Fn;)V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2173805
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2173806
    invoke-virtual {p1}, LX/1Fn;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173807
    sget-object v0, LX/0ax;->eT:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->OFFLINE_FEED_SETTINGS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2173808
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/Erv;
    .locals 4

    .prologue
    .line 2173809
    sget-object v0, LX/Erv;->a:LX/Erv;

    if-nez v0, :cond_1

    .line 2173810
    const-class v1, LX/Erv;

    monitor-enter v1

    .line 2173811
    :try_start_0
    sget-object v0, LX/Erv;->a:LX/Erv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2173812
    if-eqz v2, :cond_0

    .line 2173813
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2173814
    new-instance p0, LX/Erv;

    invoke-static {v0}, LX/1Fn;->b(LX/0QB;)LX/1Fn;

    move-result-object v3

    check-cast v3, LX/1Fn;

    invoke-direct {p0, v3}, LX/Erv;-><init>(LX/1Fn;)V

    .line 2173815
    move-object v0, p0

    .line 2173816
    sput-object v0, LX/Erv;->a:LX/Erv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173817
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2173818
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2173819
    :cond_1
    sget-object v0, LX/Erv;->a:LX/Erv;

    return-object v0

    .line 2173820
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2173821
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
