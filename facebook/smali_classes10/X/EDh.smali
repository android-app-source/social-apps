.class public final LX/EDh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field public final synthetic a:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;)V
    .locals 0

    .prologue
    .line 2091783
    iput-object p1, p0, LX/EDh;->a:LX/EDx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2091784
    return-void
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 2091752
    return-void
.end method

.method public final onActivityPaused(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2091753
    iget-object v0, p0, LX/EDh;->a:LX/EDx;

    .line 2091754
    iput-boolean v1, v0, LX/EDx;->cg:Z

    .line 2091755
    instance-of v0, p1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    if-eqz v0, :cond_0

    .line 2091756
    iget-object v0, p0, LX/EDh;->a:LX/EDx;

    .line 2091757
    iput-boolean v1, v0, LX/EDx;->ch:Z

    .line 2091758
    :cond_0
    iget-object v0, p0, LX/EDh;->a:LX/EDx;

    iget-boolean v0, v0, LX/EDx;->bp:Z

    if-eqz v0, :cond_1

    .line 2091759
    iget-object v0, p0, LX/EDh;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->bq:LX/EGe;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 2091760
    iget-object v2, v0, LX/EGe;->s:LX/EFs;

    if-eqz v2, :cond_1

    .line 2091761
    iget-object v2, v0, LX/EGe;->s:LX/EFs;

    .line 2091762
    iget-boolean v0, v2, LX/EFs;->d:Z

    move v0, v0

    .line 2091763
    if-nez v0, :cond_1

    if-nez v1, :cond_3

    .line 2091764
    :cond_1
    :goto_0
    iget-object v0, p0, LX/EDh;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->bD:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/EDh;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->bD:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 2091765
    iget-object v0, p0, LX/EDh;->a:LX/EDx;

    const/4 v1, 0x0

    .line 2091766
    iput-object v1, v0, LX/EDx;->bD:Ljava/lang/ref/WeakReference;

    .line 2091767
    :cond_2
    return-void

    .line 2091768
    :cond_3
    iget-object v0, v2, LX/EFs;->f:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    .line 2091769
    :goto_1
    if-ne v0, v1, :cond_1

    .line 2091770
    const/4 v0, 0x0

    iput-boolean v0, v2, LX/EFs;->c:Z

    .line 2091771
    iget-boolean v0, v2, LX/EFs;->b:Z

    if-eqz v0, :cond_1

    .line 2091772
    iget-object v0, v2, LX/EFs;->g:LX/EGW;

    if-eqz v0, :cond_1

    .line 2091773
    iget-object v0, v2, LX/EFs;->g:LX/EGW;

    .line 2091774
    iget-object v2, v0, LX/EGW;->a:LX/EGe;

    iget-object v2, v2, LX/EGe;->y:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->K()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2091775
    iget-object v2, v0, LX/EGW;->a:LX/EGe;

    iget-object v2, v2, LX/EGe;->y:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->X()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, LX/EGW;->a:LX/EGe;

    iget-object v2, v2, LX/EGe;->y:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->V()LX/EHZ;

    move-result-object v2

    .line 2091776
    iget-object v1, v2, LX/EHZ;->d:Landroid/view/TextureView;

    move-object v2, v1

    .line 2091777
    iget-object v1, v0, LX/EGW;->a:LX/EGe;

    iget-object v1, v1, LX/EGe;->D:LX/EI9;

    invoke-virtual {v1}, LX/EI9;->getSelfTextureView()Landroid/view/TextureView;

    move-result-object v1

    if-ne v2, v1, :cond_4

    .line 2091778
    iget-object v2, v0, LX/EGW;->a:LX/EGe;

    iget-object v2, v2, LX/EGe;->y:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->V()LX/EHZ;

    move-result-object v2

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, LX/EHZ;->a(Landroid/view/TextureView;)V

    .line 2091779
    :cond_4
    iget-object v2, v0, LX/EGW;->a:LX/EGe;

    iget-object v2, v2, LX/EGe;->y:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    sget-object v1, LX/EDv;->PAUSED:LX/EDv;

    invoke-virtual {v2, v1}, LX/EDx;->a(LX/EDv;)V

    .line 2091780
    :cond_5
    iget-object v2, v0, LX/EGW;->a:LX/EGe;

    invoke-static {v2}, LX/EGe;->aD(LX/EGe;)V

    .line 2091781
    goto/16 :goto_0

    .line 2091782
    :cond_6
    iget-object v0, v2, LX/EFs;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Window;

    goto :goto_1
.end method

.method public final onActivityResumed(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2091741
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 2091742
    iget-object v1, p0, LX/EDh;->a:LX/EDx;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2091743
    iput-object v2, v1, LX/EDx;->bD:Ljava/lang/ref/WeakReference;

    .line 2091744
    iget-object v1, p0, LX/EDh;->a:LX/EDx;

    iget-boolean v1, v1, LX/EDx;->bp:Z

    if-eqz v1, :cond_0

    .line 2091745
    iget-object v1, p0, LX/EDh;->a:LX/EDx;

    iget-object v1, v1, LX/EDx;->bq:LX/EGe;

    invoke-virtual {v1, v0}, LX/EGe;->a(Landroid/view/Window;)V

    .line 2091746
    :cond_0
    iget-object v0, p0, LX/EDh;->a:LX/EDx;

    .line 2091747
    iput-boolean v3, v0, LX/EDx;->cg:Z

    .line 2091748
    instance-of v0, p1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    if-eqz v0, :cond_1

    .line 2091749
    iget-object v0, p0, LX/EDh;->a:LX/EDx;

    .line 2091750
    iput-boolean v3, v0, LX/EDx;->ch:Z

    .line 2091751
    :cond_1
    return-void
.end method

.method public final onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2091740
    return-void
.end method

.method public final onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 2091739
    return-void
.end method

.method public final onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 2091738
    return-void
.end method
