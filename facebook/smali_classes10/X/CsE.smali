.class public final LX/CsE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CsG;


# direct methods
.method public constructor <init>(LX/CsG;)V
    .locals 0

    .prologue
    .line 1942317
    iput-object p1, p0, LX/CsE;->a:LX/CsG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, 0xfcbb107

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1942318
    iget-object v1, p0, LX/CsE;->a:LX/CsG;

    .line 1942319
    iput-boolean v2, v1, LX/CsG;->q:Z

    .line 1942320
    iget-object v1, p0, LX/CsE;->a:LX/CsG;

    iget-boolean v1, v1, LX/CsG;->n:Z

    if-eqz v1, :cond_0

    .line 1942321
    iget-object v1, p0, LX/CsE;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->d:LX/CoV;

    iget-object v2, p0, LX/CsE;->a:LX/CsG;

    iget-object v2, v2, LX/CsG;->f:Landroid/content/Context;

    iget-object v3, p0, LX/CsE;->a:LX/CsG;

    iget-object v3, v3, LX/CsG;->o:Ljava/lang/String;

    .line 1942322
    const/4 v4, 0x0

    const/16 p1, 0x3ea

    invoke-virtual {v1, v2, v4, v3, p1}, LX/CoV;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1942323
    iget-object v1, p0, LX/CsE;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->c:LX/Ckw;

    const-string v2, "shared_link"

    invoke-virtual {v1, v2}, LX/Ckw;->a(Ljava/lang/String;)V

    .line 1942324
    :goto_0
    iget-object v1, p0, LX/CsE;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->k:LX/Csm;

    invoke-virtual {v1}, LX/0ht;->l()V

    .line 1942325
    const v1, 0x70ee47c8

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1942326
    :cond_0
    iget-object v1, p0, LX/CsE;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->d:LX/CoV;

    iget-object v2, p0, LX/CsE;->a:LX/CsG;

    iget-object v2, v2, LX/CsG;->f:Landroid/content/Context;

    iget-object v3, p0, LX/CsE;->a:LX/CsG;

    iget-object v3, v3, LX/CsG;->a:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/CsE;->a:LX/CsG;

    iget-object v4, v4, LX/CsG;->b:LX/Chi;

    .line 1942327
    iget-object p1, v4, LX/Chi;->d:Ljava/lang/String;

    move-object v4, p1

    .line 1942328
    iget-object v5, v1, LX/CoV;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1942329
    iget-object v5, v1, LX/CoV;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/K20;

    .line 1942330
    sget-object v7, LX/21D;->INSTANT_ARTICLE:LX/21D;

    const-string p1, "shareQuote"

    invoke-static {v4}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v1

    iget-object v6, v5, LX/K20;->a:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Chi;

    invoke-virtual {v6}, LX/Chi;->c()Ljava/lang/String;

    move-result-object v6

    .line 1942331
    iput-object v6, v1, LX/89G;->d:Ljava/lang/String;

    .line 1942332
    move-object v6, v1

    .line 1942333
    iput-object v3, v6, LX/89G;->e:Ljava/lang/String;

    .line 1942334
    move-object v6, v6

    .line 1942335
    invoke-virtual {v6}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v6

    invoke-static {v7, p1, v6}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    .line 1942336
    sget-object v7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    .line 1942337
    const/16 v7, 0x3e9

    const/4 p1, 0x0

    invoke-static {v5, v7, p1, v6, v2}, LX/K20;->a(LX/K20;ILjava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1942338
    :cond_1
    iget-object v1, p0, LX/CsE;->a:LX/CsG;

    iget-object v1, v1, LX/CsG;->c:LX/Ckw;

    const-string v2, "shared_text"

    invoke-virtual {v1, v2}, LX/Ckw;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
