.class public final LX/DJ5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/DJ6;


# direct methods
.method public constructor <init>(LX/DJ6;)V
    .locals 0

    .prologue
    .line 1984848
    iput-object p1, p0, LX/DJ5;->a:LX/DJ6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/16 v0, 0x26

    const v1, -0x3f3b57bf

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1984849
    const-string v0, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DJ5;->a:LX/DJ6;

    iget-object v0, v0, LX/DJ6;->e:Ljava/lang/String;

    const-string v1, "extra_request_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1984850
    :cond_0
    const/16 v0, 0x27

    const v1, 0x3f675c04

    invoke-static {v2, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1984851
    :goto_0
    return-void

    .line 1984852
    :cond_1
    const-string v0, "graphql_story"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1984853
    const-string v0, "extra_result"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7m7;->valueOf(Ljava/lang/String;)LX/7m7;

    move-result-object v0

    .line 1984854
    sget-object v1, LX/7m7;->SUCCESS:LX/7m7;

    if-ne v0, v1, :cond_2

    if-eqz v2, :cond_2

    .line 1984855
    iget-object v0, p0, LX/DJ5;->a:LX/DJ6;

    iget-object v0, v0, LX/DJ6;->a:LX/DJl;

    iget-object v1, p0, LX/DJ5;->a:LX/DJ6;

    iget-object v1, v1, LX/DJ6;->g:LX/21D;

    iget-object v3, p0, LX/DJ5;->a:LX/DJ6;

    iget-object v3, v3, LX/DJ6;->f:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/DJ5;->a:LX/DJ6;

    iget-object v3, v3, LX/DJ6;->f:Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    :goto_1
    iget-object v4, p0, LX/DJ5;->a:LX/DJ6;

    iget-object v4, v4, LX/DJ6;->i:LX/DJr;

    iget-object v5, p0, LX/DJ5;->a:LX/DJ6;

    iget-object v5, v5, LX/DJ6;->h:Landroid/content/Context;

    invoke-virtual/range {v0 .. v5}, LX/DJl;->a(LX/21D;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;LX/DJr;Landroid/content/Context;)V

    .line 1984856
    :cond_2
    iget-object v0, p0, LX/DJ5;->a:LX/DJ6;

    invoke-virtual {v0}, LX/DJ6;->a()V

    .line 1984857
    const v0, -0x2e85aaee

    invoke-static {v0, v6}, LX/02F;->e(II)V

    goto :goto_0

    .line 1984858
    :cond_3
    iget-object v3, p0, LX/DJ5;->a:LX/DJ6;

    .line 1984859
    :try_start_0
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v4

    iget-object v5, v3, LX/DJ6;->b:LX/0W9;

    invoke-virtual {v5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Currency;->getInstance(Ljava/util/Locale;)Ljava/util/Currency;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->setCurrencyCode(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1984860
    :goto_2
    move-object v3, v4

    .line 1984861
    goto :goto_1

    :catch_0
    const/4 v4, 0x0

    goto :goto_2
.end method
