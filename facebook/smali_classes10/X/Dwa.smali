.class public LX/Dwa;
.super LX/Dvb;
.source ""


# instance fields
.field private A:LX/8A4;

.field private B:Z

.field private C:J

.field private D:Z

.field public final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DvH;",
            ">;"
        }
    .end annotation
.end field

.field private final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvg;",
            ">;"
        }
    .end annotation
.end field

.field public final v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dy7;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/Dvf;

.field public x:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public y:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public z:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/auth/viewercontext/ViewerContext;LX/DwK;LX/Dw8;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dux;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DvH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dy7;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/DwK;",
            "LX/Dw8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2060825
    invoke-interface {p5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Dvf;

    invoke-virtual/range {p8 .. p8}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v5, p7

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v0 .. v8}, LX/Dvb;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/Dvf;LX/0Ot;Ljava/lang/String;LX/DwK;LX/Dw8;)V

    .line 2060826
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/Dwa;->C:J

    .line 2060827
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dwa;->D:Z

    .line 2060828
    iput-object p2, p0, LX/Dwa;->t:LX/0Ot;

    .line 2060829
    iput-object p5, p0, LX/Dwa;->u:LX/0Ot;

    .line 2060830
    iget-object v0, p0, LX/Dwa;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dvf;

    iput-object v0, p0, LX/Dwa;->w:LX/Dvf;

    .line 2060831
    iput-object p6, p0, LX/Dwa;->v:LX/0Ot;

    .line 2060832
    return-void
.end method

.method public static q(LX/Dwa;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2060811
    iget-boolean v2, p0, LX/Dwa;->B:Z

    if-eqz v2, :cond_1

    .line 2060812
    :cond_0
    :goto_0
    return v0

    .line 2060813
    :cond_1
    iget-object v2, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2060814
    iget-object v2, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    .line 2060815
    iget-object v2, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v4, p0, LX/Dvb;->k:Ljava/lang/String;

    invoke-static {v2, v4}, LX/Dxo;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v4, p0, LX/Dvb;->k:Ljava/lang/String;

    invoke-static {v2, v4}, LX/Dxo;->b(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v2, v1

    .line 2060816
    :goto_1
    sparse-switch v3, :sswitch_data_0

    move v0, v2

    .line 2060817
    goto :goto_0

    :cond_3
    move v2, v0

    .line 2060818
    goto :goto_1

    .line 2060819
    :sswitch_0
    iget-object v0, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->o()Z

    move-result v0

    goto :goto_0

    .line 2060820
    :sswitch_1
    iget-object v3, p0, LX/Dwa;->A:LX/8A4;

    if-eqz v3, :cond_4

    .line 2060821
    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Dwa;->A:LX/8A4;

    sget-object v3, LX/8A3;->EDIT_PROFILE:LX/8A3;

    invoke-virtual {v2, v3}, LX/8A4;->a(LX/8A3;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 2060822
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x403827a -> :sswitch_0
        0x41e065f -> :sswitch_0
    .end sparse-switch
.end method

.method public static s(LX/Dwa;)LX/Dv0;
    .locals 2

    .prologue
    .line 2060823
    iget-object v0, p0, LX/Dvb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dux;

    invoke-virtual {p0}, LX/Dvb;->i()Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Dux;->a(Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;)LX/Dv0;

    move-result-object v0

    iput-object v0, p0, LX/Dwa;->h:LX/Dv0;

    .line 2060824
    iget-object v0, p0, LX/Dvb;->h:LX/Dv0;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/view/View;)Landroid/view/View;
    .locals 12

    .prologue
    .line 2060833
    if-eqz p1, :cond_0

    .line 2060834
    check-cast p1, LX/DxP;

    move-object v1, p1

    .line 2060835
    :goto_0
    iget-object v2, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-boolean v3, p0, LX/Dwa;->z:Z

    iget-wide v4, p0, LX/Dwa;->C:J

    iget-boolean v6, p0, LX/Dwa;->D:Z

    .line 2060836
    iget-object v8, p0, LX/Dwa;->y:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v8, :cond_1

    .line 2060837
    iget-object v8, p0, LX/Dwa;->y:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2060838
    :goto_1
    move-object v7, v8

    .line 2060839
    invoke-virtual/range {v1 .. v7}, LX/DxP;->a(Lcom/facebook/graphql/model/GraphQLAlbum;ZJZLcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 2060840
    return-object v1

    .line 2060841
    :cond_0
    new-instance v1, LX/DxP;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/DxP;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2060842
    :cond_1
    iget-object v8, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v8

    if-eqz v8, :cond_2

    iget-object v8, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 2060843
    iget-object v8, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v8

    .line 2060844
    const v9, 0x41e065f

    if-ne v8, v9, :cond_3

    .line 2060845
    new-instance v8, LX/89I;

    iget-object v9, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    sget-object v9, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v8, v10, v11, v9}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v9, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v9

    .line 2060846
    iput-object v9, v8, LX/89I;->c:Ljava/lang/String;

    .line 2060847
    move-object v8, v8

    .line 2060848
    invoke-virtual {v8}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v8

    iput-object v8, p0, LX/Dwa;->y:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2060849
    :cond_2
    :goto_2
    iget-object v8, p0, LX/Dwa;->y:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    goto :goto_1

    .line 2060850
    :cond_3
    const v9, 0x403827a

    if-ne v8, v9, :cond_2

    .line 2060851
    new-instance v8, LX/89I;

    iget-object v9, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    sget-object v9, LX/2rw;->EVENT:LX/2rw;

    invoke-direct {v8, v10, v11, v9}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v9, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v9

    .line 2060852
    iput-object v9, v8, LX/89I;->c:Ljava/lang/String;

    .line 2060853
    move-object v8, v8

    .line 2060854
    invoke-virtual {v8}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v8

    iput-object v8, p0, LX/Dwa;->y:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    goto :goto_2
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/util/ArrayList;ZJZ)V
    .locals 7
    .param p2    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;ZJZ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2060798
    iput-object p1, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2060799
    iput-object p2, p0, LX/Dwa;->y:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2060800
    if-eqz p3, :cond_0

    new-instance v0, LX/8A4;

    invoke-direct {v0, p3}, LX/8A4;-><init>(Ljava/util/List;)V

    :goto_0
    iput-object v0, p0, LX/Dwa;->A:LX/8A4;

    .line 2060801
    iput-boolean p4, p0, LX/Dwa;->B:Z

    .line 2060802
    invoke-static {p0}, LX/Dwa;->q(LX/Dwa;)Z

    move-result v0

    iput-boolean v0, p0, LX/Dwa;->z:Z

    .line 2060803
    iput-wide p5, p0, LX/Dwa;->C:J

    .line 2060804
    iput-boolean p7, p0, LX/Dwa;->D:Z

    .line 2060805
    iget-object v0, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    .line 2060806
    iget-object v1, p0, LX/Dvb;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    const-string v2, "refetchAlbumDetails_%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, LX/DwY;

    invoke-direct {v4, p0, v0}, LX/DwY;-><init>(LX/Dwa;Ljava/lang/String;)V

    new-instance v5, LX/DwZ;

    invoke-direct {v5, p0}, LX/DwZ;-><init>(LX/Dwa;)V

    invoke-virtual {v1, v2, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2060807
    iget-object v0, p0, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LoadScreenImagesAlbum"

    const/4 v5, 0x0

    move-object v0, p0

    move v4, v3

    invoke-super/range {v0 .. v5}, LX/Dvb;->a(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 2060808
    return-void

    .line 2060809
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 2

    .prologue
    .line 2060810
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "should use init(GraphQLAlbum album) instead. "

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 2060791
    iget-boolean v0, p0, LX/Dvb;->m:Z

    if-nez v0, :cond_0

    .line 2060792
    :goto_0
    return-void

    .line 2060793
    :cond_0
    iget-object v0, p0, LX/Dvb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {p0}, LX/Dvb;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/DwW;

    invoke-direct {v2, p0}, LX/DwW;-><init>(LX/Dwa;)V

    new-instance v3, LX/DwX;

    invoke-direct {v3, p0}, LX/DwX;-><init>(LX/Dwa;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2060797
    const-string v0, "fetchAlbumMediaSet_%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()LX/DvW;
    .locals 1

    .prologue
    .line 2060794
    sget-object v0, LX/DvW;->ALBUM_MEDIA_SET:LX/DvW;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2060795
    const/4 v0, 0x1

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 2060796
    iget-boolean v0, p0, LX/Dvb;->m:Z

    return v0
.end method
