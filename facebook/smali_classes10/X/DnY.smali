.class public LX/DnY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6lX;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2040460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2040461
    iput-object p1, p0, LX/DnY;->a:Landroid/content/Context;

    .line 2040462
    return-void
.end method


# virtual methods
.method public final a(LX/6lg;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2040463
    iget-object v0, p1, LX/6lg;->b:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bH()Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    move-result-object v0

    .line 2040464
    sget-object v1, LX/DnX;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2040465
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2040466
    :pswitch_0
    invoke-virtual {p1}, LX/6lg;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2040467
    iget-object v0, p0, LX/DnY;->a:Landroid/content/Context;

    const v1, 0x7f082bd5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2040468
    :cond_0
    iget-object v0, p0, LX/DnY;->a:Landroid/content/Context;

    const v1, 0x7f082bda

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, LX/6lg;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2040469
    :pswitch_1
    invoke-virtual {p1}, LX/6lg;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2040470
    iget-object v0, p0, LX/DnY;->a:Landroid/content/Context;

    const v1, 0x7f082bdb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2040471
    :cond_1
    iget-object v0, p0, LX/DnY;->a:Landroid/content/Context;

    const v1, 0x7f082bdc

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, LX/6lg;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2040472
    :pswitch_2
    invoke-virtual {p1}, LX/6lg;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2040473
    iget-object v0, p0, LX/DnY;->a:Landroid/content/Context;

    const v1, 0x7f082bdf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2040474
    :cond_2
    iget-object v0, p0, LX/DnY;->a:Landroid/content/Context;

    const v1, 0x7f082be0

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, LX/6lg;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2040475
    :pswitch_3
    invoke-virtual {p1}, LX/6lg;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2040476
    iget-object v0, p0, LX/DnY;->a:Landroid/content/Context;

    const v1, 0x7f082bdd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2040477
    :cond_3
    iget-object v0, p0, LX/DnY;->a:Landroid/content/Context;

    const v1, 0x7f082bde

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, LX/6lg;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
