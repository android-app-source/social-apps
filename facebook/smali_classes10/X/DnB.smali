.class public final LX/DnB;
.super LX/DnA;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2040066
    iput-object p1, p0, LX/DnB;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-direct {p0, p2}, LX/DnA;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2040067
    invoke-static {p1}, LX/DnS;->d(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;)Ljava/lang/String;

    move-result-object v0

    .line 2040068
    iget-object v1, p0, LX/DnA;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v2, v2, v2, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 2040069
    iget-object v1, p0, LX/DnA;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    sget-object v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2040070
    invoke-virtual {p1}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel;->m()Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$ProductItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentDetailQueryModel$ProductItemModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 2040071
    iget-object v1, p0, LX/DnA;->n:Landroid/widget/TextView;

    iget-object v2, p0, LX/DnB;->l:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082ba2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040072
    iget-object v1, p0, LX/DnA;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2040073
    return-void

    .line 2040074
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
