.class public LX/EtU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/2g9;

.field private final b:LX/AqM;

.field private final c:LX/AqB;

.field private final d:LX/Aq2;

.field private final e:LX/1vg;


# direct methods
.method public constructor <init>(LX/2g9;LX/AqM;LX/AqB;LX/Aq2;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2178084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2178085
    iput-object p1, p0, LX/EtU;->a:LX/2g9;

    .line 2178086
    iput-object p2, p0, LX/EtU;->b:LX/AqM;

    .line 2178087
    iput-object p3, p0, LX/EtU;->c:LX/AqB;

    .line 2178088
    iput-object p4, p0, LX/EtU;->d:LX/Aq2;

    .line 2178089
    iput-object p5, p0, LX/EtU;->e:LX/1vg;

    .line 2178090
    return-void
.end method

.method private static a(I)I
    .locals 3
    .param p0    # I
        .annotation build Lcom/facebook/fig/listitem/annotations/ActionType;
        .end annotation
    .end param

    .prologue
    .line 2178091
    packed-switch p0, :pswitch_data_0

    .line 2178092
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported button type:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2178093
    :pswitch_0
    const/16 v0, 0x24

    .line 2178094
    :goto_0
    return v0

    .line 2178095
    :pswitch_1
    const/16 v0, 0x12

    goto :goto_0

    .line 2178096
    :pswitch_2
    const/16 v0, 0x82

    goto :goto_0

    .line 2178097
    :pswitch_3
    const/16 v0, 0x102

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/EtU;
    .locals 9

    .prologue
    .line 2178098
    const-class v1, LX/EtU;

    monitor-enter v1

    .line 2178099
    :try_start_0
    sget-object v0, LX/EtU;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2178100
    sput-object v2, LX/EtU;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2178101
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2178102
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2178103
    new-instance v3, LX/EtU;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v4

    check-cast v4, LX/2g9;

    invoke-static {v0}, LX/AqM;->a(LX/0QB;)LX/AqM;

    move-result-object v5

    check-cast v5, LX/AqM;

    invoke-static {v0}, LX/AqB;->a(LX/0QB;)LX/AqB;

    move-result-object v6

    check-cast v6, LX/AqB;

    invoke-static {v0}, LX/Aq2;->a(LX/0QB;)LX/Aq2;

    move-result-object v7

    check-cast v7, LX/Aq2;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v8

    check-cast v8, LX/1vg;

    invoke-direct/range {v3 .. v8}, LX/EtU;-><init>(LX/2g9;LX/AqM;LX/AqB;LX/Aq2;LX/1vg;)V

    .line 2178104
    move-object v0, v3

    .line 2178105
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2178106
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EtU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2178107
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2178108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;ILjava/lang/CharSequence;IZLX/1dQ;LX/1dQ;)LX/1Dg;
    .locals 4
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/listitem/annotations/ActionType;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Ljava/lang/CharSequence;",
            "IZ",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/Apr;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 2178109
    packed-switch p2, :pswitch_data_0

    .line 2178110
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2178111
    :pswitch_0
    const/4 v0, 0x0

    .line 2178112
    :goto_0
    return-object v0

    .line 2178113
    :pswitch_1
    iget-object v0, p0, LX/EtU;->d:LX/Aq2;

    const/4 v1, 0x0

    .line 2178114
    new-instance v2, LX/Aq1;

    invoke-direct {v2, v0}, LX/Aq1;-><init>(LX/Aq2;)V

    .line 2178115
    sget-object p0, LX/Aq2;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Aq0;

    .line 2178116
    if-nez p0, :cond_0

    .line 2178117
    new-instance p0, LX/Aq0;

    invoke-direct {p0}, LX/Aq0;-><init>()V

    .line 2178118
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Aq0;->a$redex0(LX/Aq0;LX/1De;IILX/Aq1;)V

    .line 2178119
    move-object v2, p0

    .line 2178120
    move-object v1, v2

    .line 2178121
    move-object v0, v1

    .line 2178122
    iget-object v1, v0, LX/Aq0;->a:LX/Aq1;

    iput-boolean p5, v1, LX/Aq1;->a:Z

    .line 2178123
    move-object v0, v0

    .line 2178124
    iget-object v1, v0, LX/Aq0;->a:LX/Aq1;

    iput-object p7, v1, LX/Aq1;->b:LX/1dQ;

    .line 2178125
    move-object v0, v0

    .line 2178126
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0

    .line 2178127
    :pswitch_2
    invoke-static {p1}, LX/AqH;->c(LX/1De;)LX/AqF;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/AqF;->a(Z)LX/AqF;

    move-result-object v0

    invoke-virtual {v0, p7}, LX/AqF;->a(LX/1dQ;)LX/AqF;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0

    .line 2178128
    :pswitch_3
    iget-object v0, p0, LX/EtU;->c:LX/AqB;

    const/4 v1, 0x0

    .line 2178129
    new-instance v2, LX/AqA;

    invoke-direct {v2, v0}, LX/AqA;-><init>(LX/AqB;)V

    .line 2178130
    sget-object p0, LX/AqB;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Aq9;

    .line 2178131
    if-nez p0, :cond_1

    .line 2178132
    new-instance p0, LX/Aq9;

    invoke-direct {p0}, LX/Aq9;-><init>()V

    .line 2178133
    :cond_1
    invoke-static {p0, p1, v1, v1, v2}, LX/Aq9;->a$redex0(LX/Aq9;LX/1De;IILX/AqA;)V

    .line 2178134
    move-object v2, p0

    .line 2178135
    move-object v1, v2

    .line 2178136
    move-object v0, v1

    .line 2178137
    iget-object v1, v0, LX/Aq9;->a:LX/AqA;

    iput-boolean p5, v1, LX/AqA;->a:Z

    .line 2178138
    move-object v0, v0

    .line 2178139
    iget-object v1, v0, LX/Aq9;->a:LX/AqA;

    iput-object p7, v1, LX/AqA;->b:LX/1dQ;

    .line 2178140
    move-object v0, v0

    .line 2178141
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    .line 2178142
    :pswitch_4
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    const v0, 0x7f0e0125

    .line 2178143
    :goto_1
    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    .line 2178144
    :cond_2
    const v0, 0x7f0e0123

    goto :goto_1

    .line 2178145
    :pswitch_5
    iget-object v0, p0, LX/EtU;->a:LX/2g9;

    invoke-virtual {v0, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v0

    invoke-static {p2}, LX/EtU;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/2gA;->h(I)LX/2gA;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    .line 2178146
    :pswitch_6
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    iget-object v1, p0, LX/EtU;->e:LX/1vg;

    invoke-virtual {v1, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    const v2, 0x7f0a00a6

    invoke-virtual {v1, v2}, LX/2xv;->j(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    .line 2178147
    :pswitch_7
    iget-object v0, p0, LX/EtU;->b:LX/AqM;

    const/4 v1, 0x0

    .line 2178148
    new-instance v2, LX/AqL;

    invoke-direct {v2, v0}, LX/AqL;-><init>(LX/AqM;)V

    .line 2178149
    sget-object p0, LX/AqM;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/AqK;

    .line 2178150
    if-nez p0, :cond_3

    .line 2178151
    new-instance p0, LX/AqK;

    invoke-direct {p0}, LX/AqK;-><init>()V

    .line 2178152
    :cond_3
    invoke-static {p0, p1, v1, v1, v2}, LX/AqK;->a$redex0(LX/AqK;LX/1De;IILX/AqL;)V

    .line 2178153
    move-object v2, p0

    .line 2178154
    move-object v1, v2

    .line 2178155
    move-object v0, v1

    .line 2178156
    const/16 v1, 0x14

    .line 2178157
    iget-object v2, v0, LX/AqK;->a:LX/AqL;

    iput v1, v2, LX/AqL;->a:I

    .line 2178158
    iget-object v2, v0, LX/AqK;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2178159
    move-object v0, v0

    .line 2178160
    iget-object v1, v0, LX/AqK;->a:LX/AqL;

    iput p4, v1, LX/AqL;->b:I

    .line 2178161
    iget-object v1, v0, LX/AqK;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2178162
    move-object v0, v0

    .line 2178163
    iget-object v1, v0, LX/AqK;->a:LX/AqL;

    iput-boolean p5, v1, LX/AqL;->d:Z

    .line 2178164
    move-object v0, v0

    .line 2178165
    iget-object v1, v0, LX/AqK;->a:LX/AqL;

    iput-object p7, v1, LX/AqL;->e:LX/1dQ;

    .line 2178166
    move-object v0, v0

    .line 2178167
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
