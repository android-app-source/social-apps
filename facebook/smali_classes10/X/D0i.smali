.class public LX/D0i;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/D0g;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1955828
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/D0i;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/D0j;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1955854
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1955855
    iput-object p1, p0, LX/D0i;->b:LX/0Ot;

    .line 1955856
    return-void
.end method

.method public static a(LX/0QB;)LX/D0i;
    .locals 4

    .prologue
    .line 1955839
    const-class v1, LX/D0i;

    monitor-enter v1

    .line 1955840
    :try_start_0
    sget-object v0, LX/D0i;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1955841
    sput-object v2, LX/D0i;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1955842
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1955843
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1955844
    new-instance v3, LX/D0i;

    const/16 p0, 0x356b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/D0i;-><init>(LX/0Ot;)V

    .line 1955845
    move-object v0, v3

    .line 1955846
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1955847
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D0i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1955848
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1955849
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1955850
    check-cast p2, LX/D0h;

    .line 1955851
    iget-object v0, p0, LX/D0i;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0j;

    iget v2, p2, LX/D0h;->a:I

    iget-object v3, p2, LX/D0h;->b:LX/1dQ;

    iget v4, p2, LX/D0h;->c:I

    iget-object v5, p2, LX/D0h;->d:LX/1dQ;

    move-object v1, p1

    const/16 p2, 0x24

    const/4 p1, 0x6

    const/4 v7, 0x2

    const/high16 p0, 0x3f800000    # 1.0f

    const/4 v9, 0x7

    .line 1955852
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, LX/D0j;->a:LX/2g9;

    invoke-virtual {v7, v1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v7

    invoke-virtual {v7, p2}, LX/2gA;->h(I)LX/2gA;

    move-result-object v7

    invoke-virtual {v7, v2}, LX/2gA;->i(I)LX/2gA;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v7, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b00d6

    invoke-interface {v7, p1, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b00d5

    invoke-interface {v7, v9, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v7

    const v8, 0x7f0a009f

    invoke-virtual {v7, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v8, 0x7f0b0043

    invoke-interface {v7, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v7

    const/4 v8, 0x4

    invoke-interface {v7, v8}, LX/1Di;->b(I)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b00d5

    invoke-interface {v7, v9, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, LX/D0j;->a:LX/2g9;

    invoke-virtual {v7, v1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v7

    invoke-virtual {v7, p2}, LX/2gA;->h(I)LX/2gA;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/2gA;->i(I)LX/2gA;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v7, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b00d6

    invoke-interface {v7, p1, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    const v8, 0x7f0b00d5

    invoke-interface {v7, v9, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 1955853
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1955837
    invoke-static {}, LX/1dS;->b()V

    .line 1955838
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/D0g;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1955829
    new-instance v1, LX/D0h;

    invoke-direct {v1, p0}, LX/D0h;-><init>(LX/D0i;)V

    .line 1955830
    sget-object v2, LX/D0i;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/D0g;

    .line 1955831
    if-nez v2, :cond_0

    .line 1955832
    new-instance v2, LX/D0g;

    invoke-direct {v2}, LX/D0g;-><init>()V

    .line 1955833
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/D0g;->a$redex0(LX/D0g;LX/1De;IILX/D0h;)V

    .line 1955834
    move-object v1, v2

    .line 1955835
    move-object v0, v1

    .line 1955836
    return-object v0
.end method
