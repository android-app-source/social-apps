.class public LX/CoJ;
.super LX/1OM;
.source ""

# interfaces
.implements LX/02k;
.implements LX/1UG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/02k;",
        "LX/1UG;"
    }
.end annotation


# instance fields
.field private A:Z

.field public B:Z

.field private C:LX/CqF;

.field private final D:LX/Chz;

.field private final E:LX/Ci0;

.field private final F:LX/ChN;

.field private final G:LX/Ci6;

.field private final H:LX/Chy;

.field public a:LX/CqG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/11i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/CqK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/CqM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11i;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:Landroid/content/Context;

.field public final k:LX/Clo;

.field private final l:LX/CjK;

.field public final m:LX/1P1;

.field public final n:Landroid/support/v7/widget/RecyclerView;

.field public final o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/Cm0;",
            ">;"
        }
    .end annotation
.end field

.field public final p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/Cm0;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/Cm0;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Cm0;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Z

.field public final t:Z

.field public final u:LX/0Pq;

.field private v:I

.field private w:I

.field private x:I

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Clo;LX/CjK;LX/1P1;Landroid/support/v7/widget/RecyclerView;LX/0Pq;)V
    .locals 9

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 1935468
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1935469
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CoJ;->o:Ljava/util/Map;

    .line 1935470
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CoJ;->p:Ljava/util/Map;

    .line 1935471
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CoJ;->q:Ljava/util/Map;

    .line 1935472
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CoJ;->r:Ljava/util/Set;

    .line 1935473
    iput v1, p0, LX/CoJ;->v:I

    .line 1935474
    iput v1, p0, LX/CoJ;->w:I

    .line 1935475
    iput v1, p0, LX/CoJ;->x:I

    .line 1935476
    iput-boolean v3, p0, LX/CoJ;->y:Z

    .line 1935477
    iput-boolean v3, p0, LX/CoJ;->z:Z

    .line 1935478
    iput-boolean v3, p0, LX/CoJ;->A:Z

    .line 1935479
    iput-boolean v3, p0, LX/CoJ;->B:Z

    .line 1935480
    new-instance v0, LX/CoD;

    invoke-direct {v0, p0}, LX/CoD;-><init>(LX/CoJ;)V

    iput-object v0, p0, LX/CoJ;->D:LX/Chz;

    .line 1935481
    new-instance v0, LX/CoE;

    invoke-direct {v0, p0}, LX/CoE;-><init>(LX/CoJ;)V

    iput-object v0, p0, LX/CoJ;->E:LX/Ci0;

    .line 1935482
    new-instance v0, LX/CoF;

    invoke-direct {v0, p0}, LX/CoF;-><init>(LX/CoJ;)V

    iput-object v0, p0, LX/CoJ;->F:LX/ChN;

    .line 1935483
    new-instance v0, LX/CoG;

    invoke-direct {v0, p0}, LX/CoG;-><init>(LX/CoJ;)V

    iput-object v0, p0, LX/CoJ;->G:LX/Ci6;

    .line 1935484
    new-instance v0, LX/CoH;

    invoke-direct {v0, p0}, LX/CoH;-><init>(LX/CoJ;)V

    iput-object v0, p0, LX/CoJ;->H:LX/Chy;

    .line 1935485
    iput-object p1, p0, LX/CoJ;->j:Landroid/content/Context;

    .line 1935486
    iput-object p2, p0, LX/CoJ;->k:LX/Clo;

    .line 1935487
    iput-object p3, p0, LX/CoJ;->l:LX/CjK;

    .line 1935488
    iput-object p4, p0, LX/CoJ;->m:LX/1P1;

    .line 1935489
    iput-object p5, p0, LX/CoJ;->n:Landroid/support/v7/widget/RecyclerView;

    .line 1935490
    iput-object p6, p0, LX/CoJ;->u:LX/0Pq;

    .line 1935491
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p6

    move-object v2, p0

    check-cast v2, LX/CoJ;

    const-class v4, LX/CqG;

    invoke-interface {p6, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/CqG;

    invoke-static {p6}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {p6}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v6

    check-cast v6, LX/Chv;

    invoke-static {p6}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v7

    check-cast v7, LX/11i;

    invoke-static {p6}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {p6}, LX/CqK;->a(LX/0QB;)LX/CqK;

    move-result-object p1

    check-cast p1, LX/CqK;

    invoke-static {p6}, LX/CqM;->a(LX/0QB;)LX/CqM;

    move-result-object p3

    check-cast p3, LX/CqM;

    const/16 p4, 0x11e9

    invoke-static {p6, p4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p4

    const/16 v0, 0x259

    invoke-static {p6, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p6

    iput-object v4, v2, LX/CoJ;->a:LX/CqG;

    iput-object v5, v2, LX/CoJ;->b:LX/0Uh;

    iput-object v6, v2, LX/CoJ;->c:LX/Chv;

    iput-object v7, v2, LX/CoJ;->d:LX/11i;

    iput-object v8, v2, LX/CoJ;->e:LX/0ad;

    iput-object p1, v2, LX/CoJ;->f:LX/CqK;

    iput-object p3, v2, LX/CoJ;->g:LX/CqM;

    iput-object p4, v2, LX/CoJ;->h:LX/0Ot;

    iput-object p6, v2, LX/CoJ;->i:LX/0Ot;

    .line 1935492
    new-instance v0, LX/CoI;

    invoke-direct {v0, p0}, LX/CoI;-><init>(LX/CoJ;)V

    .line 1935493
    iget-object v1, p0, LX/CoJ;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1935494
    iget-object v0, p0, LX/CoJ;->b:LX/0Uh;

    const/16 v1, 0xb7

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CoJ;->A:Z

    .line 1935495
    iget-object v0, p0, LX/CoJ;->b:LX/0Uh;

    const/16 v1, 0x3ed

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CoJ;->s:Z

    .line 1935496
    iget-object v0, p0, LX/CoJ;->b:LX/0Uh;

    const/16 v1, 0x3eb

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/CoJ;->t:Z

    .line 1935497
    iget-object v0, p0, LX/CoJ;->b:LX/0Uh;

    const/16 v1, 0x3ec

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 1935498
    iget-boolean v1, p0, LX/CoJ;->A:Z

    if-eqz v1, :cond_0

    .line 1935499
    iget-object v1, p0, LX/CoJ;->a:LX/CqG;

    sget v2, LX/CoL;->Q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1935500
    new-instance v5, LX/CqF;

    invoke-static {v1}, LX/0kl;->b(LX/0QB;)LX/0Wd;

    move-result-object v4

    check-cast v4, LX/0Wd;

    invoke-direct {v5, p0, p5, v2, v4}, LX/CqF;-><init>(LX/1UG;Landroid/view/ViewGroup;Ljava/lang/Integer;LX/0Wd;)V

    .line 1935501
    move-object v1, v5

    .line 1935502
    iput-object v1, p0, LX/CoJ;->C:LX/CqF;

    .line 1935503
    iget-object v1, p0, LX/CoJ;->f:LX/CqK;

    iget-object v2, p0, LX/CoJ;->C:LX/CqF;

    invoke-virtual {v1, v2}, LX/CqK;->a(LX/CqE;)V

    .line 1935504
    :cond_0
    if-eqz v0, :cond_1

    .line 1935505
    iget-object v0, p0, LX/CoJ;->g:LX/CqM;

    invoke-virtual {v0, p2}, LX/CqM;->a(LX/Clo;)V

    .line 1935506
    iget-object v0, p0, LX/CoJ;->f:LX/CqK;

    iget-object v1, p0, LX/CoJ;->g:LX/CqM;

    invoke-virtual {v0, v1}, LX/CqK;->a(LX/CqE;)V

    .line 1935507
    :cond_1
    iget-object v0, p0, LX/CoJ;->c:LX/Chv;

    iget-object v1, p0, LX/CoJ;->D:LX/Chz;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1935508
    iget-object v0, p0, LX/CoJ;->c:LX/Chv;

    iget-object v1, p0, LX/CoJ;->E:LX/Ci0;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1935509
    iget-object v0, p0, LX/CoJ;->c:LX/Chv;

    iget-object v1, p0, LX/CoJ;->F:LX/ChN;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1935510
    iget-object v0, p0, LX/CoJ;->c:LX/Chv;

    iget-object v1, p0, LX/CoJ;->G:LX/Ci6;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1935511
    iget-object v0, p0, LX/CoJ;->c:LX/Chv;

    iget-object v1, p0, LX/CoJ;->H:LX/Chy;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1935512
    invoke-static {p0, v3}, LX/CoJ;->g(LX/CoJ;I)V

    .line 1935513
    return-void
.end method

.method public static a$redex0(LX/CoJ;LX/Cm0;IZ)V
    .locals 2

    .prologue
    .line 1935462
    iget-object v0, p0, LX/CoJ;->r:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1935463
    if-eqz p3, :cond_0

    .line 1935464
    iget-object v0, p0, LX/CoJ;->q:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1935465
    :cond_0
    iget-object v0, p0, LX/CoJ;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1935466
    iget-object v0, p0, LX/CoJ;->n:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/facebook/richdocument/view/RichDocumentAdapter$9;

    invoke-direct {v1, p0}, Lcom/facebook/richdocument/view/RichDocumentAdapter$9;-><init>(LX/CoJ;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    .line 1935467
    :cond_1
    return-void
.end method

.method private static e(LX/1a1;)LX/CnT;
    .locals 1

    .prologue
    .line 1935459
    if-eqz p0, :cond_0

    .line 1935460
    check-cast p0, LX/Cs4;

    invoke-virtual {p0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 1935461
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/CoJ;)V
    .locals 8

    .prologue
    .line 1935428
    iget-boolean v0, p0, LX/CoJ;->z:Z

    if-nez v0, :cond_1

    .line 1935429
    :cond_0
    return-void

    .line 1935430
    :cond_1
    iget-boolean v0, p0, LX/CoJ;->y:Z

    if-nez v0, :cond_0

    .line 1935431
    iget v0, p0, LX/CoJ;->v:I

    iget-object v1, p0, LX/CoJ;->m:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1935432
    iget v0, p0, LX/CoJ;->v:I

    iget-object v1, p0, LX/CoJ;->m:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1935433
    iget v0, p0, LX/CoJ;->w:I

    if-ne v0, v2, :cond_2

    iget v0, p0, LX/CoJ;->x:I

    if-eq v0, v3, :cond_0

    .line 1935434
    :cond_2
    iput v2, p0, LX/CoJ;->w:I

    .line 1935435
    iput v3, p0, LX/CoJ;->x:I

    .line 1935436
    iget-object v0, p0, LX/CoJ;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1935437
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cm0;

    .line 1935438
    invoke-interface {v1}, LX/Cm0;->ja_()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1935439
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1935440
    invoke-interface {v1}, LX/Cm0;->jb_()I

    move-result v5

    sub-int v5, v2, v5

    .line 1935441
    invoke-interface {v1}, LX/Cm0;->jb_()I

    move-result v6

    add-int/2addr v6, v3

    .line 1935442
    if-lt v0, v5, :cond_5

    if-gt v0, v6, :cond_5

    const/4 v7, 0x1

    :goto_1
    move v5, v7

    .line 1935443
    if-eqz v5, :cond_3

    .line 1935444
    const/4 v5, 0x1

    iput-boolean v5, p0, LX/CoJ;->y:Z

    .line 1935445
    const/4 v7, 0x1

    .line 1935446
    iget-object v5, p0, LX/CoJ;->j:Landroid/content/Context;

    invoke-interface {v1, v5}, LX/Cm0;->a(Landroid/content/Context;)V

    .line 1935447
    iget-object v5, p0, LX/CoJ;->r:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1935448
    iget-object v5, p0, LX/CoJ;->m:LX/1P1;

    instance-of v5, v5, LX/CqN;

    if-eqz v5, :cond_6

    iget-object v5, p0, LX/CoJ;->m:LX/1P1;

    check-cast v5, LX/CqN;

    .line 1935449
    :goto_2
    instance-of v6, v1, LX/Cm1;

    if-eqz v6, :cond_4

    if-nez v5, :cond_7

    .line 1935450
    :cond_4
    invoke-static {p0, v1, v0, v7}, LX/CoJ;->a$redex0(LX/CoJ;LX/Cm0;IZ)V

    .line 1935451
    :goto_3
    goto :goto_0

    :cond_5
    const/4 v7, 0x0

    goto :goto_1

    .line 1935452
    :cond_6
    const/4 v5, 0x0

    goto :goto_2

    .line 1935453
    :cond_7
    instance-of v6, v1, LX/Cm4;

    if-eqz v6, :cond_8

    iget-boolean v6, p0, LX/CoJ;->t:Z

    if-nez v6, :cond_8

    .line 1935454
    invoke-static {p0, v1, v0, v7}, LX/CoJ;->a$redex0(LX/CoJ;LX/Cm0;IZ)V

    goto :goto_3

    .line 1935455
    :cond_8
    iget-boolean v6, p0, LX/CoJ;->s:Z

    if-eqz v6, :cond_9

    .line 1935456
    new-instance v6, Lcom/facebook/richdocument/view/RichDocumentAdapter$8;

    invoke-direct {v6, p0, v5, v0, v1}, Lcom/facebook/richdocument/view/RichDocumentAdapter$8;-><init>(LX/CoJ;LX/CqN;ILX/Cm0;)V

    .line 1935457
    iget-object v5, p0, LX/CoJ;->f:LX/CqK;

    new-instance v7, LX/CqL;

    invoke-direct {v7, v6}, LX/CqL;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v5, v7}, LX/CqK;->a(LX/CqE;)V

    goto :goto_3

    .line 1935458
    :cond_9
    invoke-virtual {p0, v0}, LX/1OM;->getItemViewType(I)I

    move-result v6

    invoke-interface {v5, v0, v6}, LX/CqN;->c_(II)Z

    move-result v5

    invoke-static {p0, v1, v0, v5}, LX/CoJ;->a$redex0(LX/CoJ;LX/Cm0;IZ)V

    goto :goto_3
.end method

.method public static g(LX/CoJ;I)V
    .locals 3

    .prologue
    .line 1935420
    iget-object v0, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1935421
    :cond_0
    :goto_0
    return-void

    .line 1935422
    :cond_1
    :goto_1
    iget-object v0, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v0

    if-ge p1, v0, :cond_3

    .line 1935423
    iget-object v0, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {v0, p1}, LX/Clo;->a(I)LX/Clr;

    move-result-object v0

    .line 1935424
    instance-of v1, v0, LX/Cm0;

    if-eqz v1, :cond_2

    .line 1935425
    iget-object v1, p0, LX/CoJ;->p:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    check-cast v0, LX/Cm0;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1935426
    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 1935427
    :cond_3
    invoke-static {p0}, LX/CoJ;->e(LX/CoJ;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 1935412
    iget-boolean v0, p0, LX/CoJ;->A:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/CoJ;->C:LX/CqF;

    if-eqz v0, :cond_1

    .line 1935413
    iget-object v0, p0, LX/CoJ;->C:LX/CqF;

    invoke-virtual {v0, p2}, LX/1UI;->a(I)LX/1a1;

    move-result-object v0

    .line 1935414
    :cond_0
    :goto_0
    return-object v0

    .line 1935415
    :cond_1
    if-ne p2, v2, :cond_2

    .line 1935416
    const-string v0, "RicDocumentAdapter.onCreateViewHolder#forPhoto"

    const v1, 0x29aded1c

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1935417
    :cond_2
    iget-object v0, p0, LX/CoJ;->l:LX/CjK;

    invoke-virtual {v0, p2, p1}, LX/CjK;->a(ILandroid/view/ViewGroup;)LX/Cs4;

    move-result-object v0

    .line 1935418
    if-ne p2, v2, :cond_0

    .line 1935419
    const v1, -0x3f722afc

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0
.end method

.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 1935399
    invoke-super {p0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 1935400
    if-eqz p1, :cond_1

    .line 1935401
    check-cast p1, LX/Cs4;

    .line 1935402
    invoke-virtual {p1}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    instance-of v0, v0, LX/Cnu;

    if-eqz v0, :cond_1

    .line 1935403
    invoke-virtual {p1}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    check-cast v0, LX/Cnu;

    .line 1935404
    :goto_0
    move-object v0, v0

    .line 1935405
    if-eqz v0, :cond_0

    .line 1935406
    iget-object p1, v0, LX/CnT;->d:LX/CnG;

    move-object p1, p1

    .line 1935407
    check-cast p1, LX/CpI;

    .line 1935408
    iget-object v0, p1, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 1935409
    invoke-interface {v0}, LX/Ctg;->b()V

    .line 1935410
    :cond_0
    invoke-static {p0}, LX/CoJ;->e(LX/CoJ;)V

    .line 1935411
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1935379
    iput p2, p0, LX/CoJ;->v:I

    move-object v0, p1

    .line 1935380
    check-cast v0, LX/Cs4;

    .line 1935381
    iget-object v1, p0, LX/CoJ;->u:LX/0Pq;

    if-eqz v1, :cond_4

    .line 1935382
    iget-object v1, p0, LX/CoJ;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11i;

    iget-object v2, p0, LX/CoJ;->u:LX/0Pq;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    move-object v2, v1

    .line 1935383
    :goto_0
    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    .line 1935384
    const-string v1, "rich_document_block_type"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    .line 1935385
    const-string v4, "rich_document_block_bind"

    const v5, 0x2fdc9ac9

    invoke-static {v2, v4, v3, v1, v5}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 1935386
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {v1, p2}, LX/Clo;->a(I)LX/Clr;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Cs4;->a(LX/Clr;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1935387
    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    .line 1935388
    const-string v0, "rich_document_block_bind"

    const v1, 0x7f139a04

    invoke-static {v2, v0, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1935389
    :cond_2
    invoke-static {p0}, LX/CoJ;->e(LX/CoJ;)V

    .line 1935390
    return-void

    .line 1935391
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1935392
    iget-object v0, p0, LX/CoJ;->b:LX/0Uh;

    const/16 v3, 0xaa

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1935393
    iget-object v0, p0, LX/CoJ;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 1935394
    if-eqz v0, :cond_1

    .line 1935395
    const-string v3, "instant_articles"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error on block index = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1935396
    iget v5, p1, LX/1a1;->e:I

    move v5, v5

    .line 1935397
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1935398
    :cond_3
    throw v1

    :cond_4
    move-object v2, v3

    goto :goto_0
.end method

.method public final a_(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 1

    .prologue
    .line 1935353
    iget-object v0, p0, LX/CoJ;->l:LX/CjK;

    invoke-virtual {v0, p2, p1}, LX/CjK;->a(ILandroid/view/ViewGroup;)LX/Cs4;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1935378
    iget v0, p0, LX/CoJ;->v:I

    return v0
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 1935373
    invoke-super {p0, p1}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 1935374
    iget-object v0, p0, LX/CoJ;->C:LX/CqF;

    if-eqz v0, :cond_0

    .line 1935375
    iget-object v0, p0, LX/CoJ;->C:LX/CqF;

    invoke-virtual {v0}, LX/1UI;->d()V

    .line 1935376
    const/4 v0, 0x0

    iput-object v0, p0, LX/CoJ;->C:LX/CqF;

    .line 1935377
    :cond_0
    return-void
.end method

.method public final c(LX/1a1;)V
    .locals 3
    .param p1    # LX/1a1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1935365
    iget-boolean v0, p0, LX/CoJ;->B:Z

    if-eqz v0, :cond_0

    .line 1935366
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CoJ;->B:Z

    .line 1935367
    :goto_0
    return-void

    .line 1935368
    :cond_0
    invoke-super {p0, p1}, LX/1OM;->c(LX/1a1;)V

    .line 1935369
    invoke-static {p1}, LX/CoJ;->e(LX/1a1;)LX/CnT;

    move-result-object v0

    .line 1935370
    if-eqz v0, :cond_1

    .line 1935371
    iget-object v1, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {p1}, LX/1a1;->d()I

    move-result v2

    invoke-virtual {v1, v2}, LX/Clo;->a(I)LX/Clr;

    move-result-object v1

    invoke-interface {v1}, LX/Clr;->o()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 1935372
    :cond_1
    invoke-static {p0}, LX/CoJ;->e(LX/CoJ;)V

    goto :goto_0
.end method

.method public final d(LX/1a1;)V
    .locals 4

    .prologue
    .line 1935359
    invoke-super {p0, p1}, LX/1OM;->d(LX/1a1;)V

    .line 1935360
    invoke-static {p1}, LX/CoJ;->e(LX/1a1;)LX/CnT;

    move-result-object v0

    .line 1935361
    if-eqz v0, :cond_0

    .line 1935362
    iget-object v1, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {p1}, LX/1a1;->d()I

    move-result v2

    invoke-virtual {v1, v2}, LX/Clo;->a(I)LX/Clr;

    move-result-object v1

    invoke-interface {v1}, LX/Clr;->o()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 1935363
    :cond_0
    iget-object v0, p0, LX/CoJ;->n:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/facebook/richdocument/view/RichDocumentAdapter$7;

    invoke-direct {v1, p0}, Lcom/facebook/richdocument/view/RichDocumentAdapter$7;-><init>(LX/CoJ;)V

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/RecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1935364
    return-void
.end method

.method public final e(I)LX/Clr;
    .locals 1

    .prologue
    .line 1935358
    iget-object v0, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {v0, p1}, LX/Clo;->a(I)LX/Clr;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1935357
    iget-object v0, p0, LX/CoJ;->j:Landroid/content/Context;

    return-object v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1935355
    iget-object v0, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {v0, p1}, LX/Clo;->a(I)LX/Clr;

    move-result-object v0

    invoke-interface {v0}, LX/Clr;->lx_()I

    move-result v0

    .line 1935356
    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1935354
    iget-object v0, p0, LX/CoJ;->k:LX/Clo;

    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v0

    return v0
.end method
