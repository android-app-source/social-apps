.class public final LX/EMP;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EMQ;


# direct methods
.method public constructor <init>(LX/EMQ;)V
    .locals 0

    .prologue
    .line 2110606
    iput-object p1, p0, LX/EMP;->a:LX/EMQ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2110607
    iget-object v0, p0, LX/EMP;->a:LX/EMQ;

    iget-object v0, v0, LX/EMQ;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    .line 2110608
    iget-object v0, p0, LX/EMP;->a:LX/EMQ;

    iget-object v0, v0, LX/EMQ;->a:LX/CxA;

    iget-object v1, p0, LX/EMP;->a:LX/EMQ;

    iget-object v1, v1, LX/EMQ;->c:LX/CzL;

    invoke-interface {v0, v1}, LX/CxA;->a(LX/CzL;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110609
    iget-object v0, p0, LX/EMP;->a:LX/EMQ;

    iget-object v0, v0, LX/EMQ;->a:LX/CxA;

    iget-object v1, p0, LX/EMP;->a:LX/EMQ;

    iget-object v1, v1, LX/EMQ;->c:LX/CzL;

    iget-object v2, p0, LX/EMP;->a:LX/EMQ;

    iget-object v2, v2, LX/EMQ;->b:LX/CzL;

    invoke-interface {v0, v1, v2}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2110610
    iget-object v0, p0, LX/EMP;->a:LX/EMQ;

    iget-object v0, v0, LX/EMQ;->a:LX/CxA;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2110611
    :cond_0
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2110612
    return-void
.end method
