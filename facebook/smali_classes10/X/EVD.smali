.class public final LX/EVD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final b:Z

.field public final c:Z

.field public final d:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field public final e:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field public final f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;


# direct methods
.method private constructor <init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;ZZIILcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 0

    .prologue
    .line 2127674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2127675
    iput-object p1, p0, LX/EVD;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2127676
    iput-boolean p2, p0, LX/EVD;->b:Z

    .line 2127677
    iput-boolean p3, p0, LX/EVD;->c:Z

    .line 2127678
    iput p4, p0, LX/EVD;->d:I

    .line 2127679
    iput p5, p0, LX/EVD;->e:I

    .line 2127680
    iput-object p6, p0, LX/EVD;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2127681
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;ZZIILcom/facebook/graphql/model/GraphQLTextWithEntities;B)V
    .locals 0

    .prologue
    .line 2127682
    invoke-direct/range {p0 .. p6}, LX/EVD;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;ZZIILcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    return-void
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/EVC;
    .locals 2

    .prologue
    .line 2127683
    new-instance v0, LX/EVC;

    invoke-direct {v0, p0}, LX/EVC;-><init>(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    return-object v0
.end method
