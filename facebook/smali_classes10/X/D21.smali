.class public final LX/D21;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;)V
    .locals 0

    .prologue
    .line 1957819
    iput-object p1, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 5

    .prologue
    .line 1957792
    iget-object v0, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    .line 1957793
    const-string v1, "android_profile_video_accepted"

    invoke-static {v0, v1}, LX/BQj;->a(LX/BQj;Ljava/lang/String;)V

    .line 1957794
    iget-object v0, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->b()V

    .line 1957795
    iget-object v0, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->z:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    .line 1957796
    iget-object v1, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->p:LX/1FJ;

    move-object v1, v1

    .line 1957797
    if-nez v1, :cond_0

    .line 1957798
    :goto_0
    return-void

    .line 1957799
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 1957800
    iget-object v0, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D2h;

    iget-object v2, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    invoke-virtual {v2}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v2

    iget-object v3, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v3, v3, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    .line 1957801
    iget-object v4, v0, LX/D2h;->d:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v4, :cond_1

    iget-object v4, v0, LX/D2h;->d:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1957802
    iget-boolean p1, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v4, p1

    .line 1957803
    if-eqz v4, :cond_2

    .line 1957804
    :cond_1
    iget-object v4, v0, LX/D2h;->e:LX/03V;

    const-string p1, "not_valid_vc"

    const-string p2, "User VC must be set"

    invoke-virtual {v4, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957805
    :goto_1
    iget-object v0, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->s:LX/D2D;

    iget-object v1, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v1, v1, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    iget-object v2, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    invoke-virtual {v2}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v2

    .line 1957806
    iget v3, v2, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    move v2, v3

    .line 1957807
    int-to-long v2, v2

    .line 1957808
    sget-object v4, LX/D2C;->UPLOAD_STARTED:LX/D2C;

    invoke-virtual {v4}, LX/D2C;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, LX/D2D;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1957809
    const-string p1, "thumbnail_ms"

    invoke-virtual {v4, p1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1957810
    invoke-static {v0, v4}, LX/D2D;->a(LX/D2D;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1957811
    iget-object v0, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->setResult(I)V

    .line 1957812
    iget-object v0, p0, LX/D21;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->finish()V

    goto :goto_0

    .line 1957813
    :cond_2
    iget-object v4, v0, LX/D2h;->i:LX/0TD;

    new-instance p1, LX/D2g;

    invoke-direct {p1, v0, v2}, LX/D2g;-><init>(LX/D2h;Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;)V

    invoke-interface {v4, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v4, v4

    .line 1957814
    invoke-virtual {v1}, LX/1FJ;->b()LX/1FJ;

    move-result-object p1

    .line 1957815
    new-instance p2, LX/D2e;

    invoke-direct {p2, v0, v2, p1, v3}, LX/D2e;-><init>(LX/D2h;Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;LX/1FJ;Ljava/lang/String;)V

    move-object p1, p2

    .line 1957816
    invoke-static {v4, p1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1957817
    new-instance p1, LX/D2f;

    invoke-direct {p1, v0, v3}, LX/D2f;-><init>(LX/D2h;Ljava/lang/String;)V

    iget-object p2, v0, LX/D2h;->j:LX/0TD;

    invoke-static {v4, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1957818
    goto :goto_1
.end method
