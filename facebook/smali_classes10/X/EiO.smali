.class public final LX/EiO;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V
    .locals 0

    .prologue
    .line 2159354
    iput-object p1, p0, LX/EiO;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2159355
    iget-object v0, p0, LX/EiO;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2159356
    iget-object v0, p0, LX/EiO;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->s:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2159357
    iget-object v0, p0, LX/EiO;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->w:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 2159358
    return-void

    .line 2159359
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
