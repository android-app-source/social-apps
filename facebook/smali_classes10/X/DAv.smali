.class public final LX/DAv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# instance fields
.field public final synthetic a:LX/1Zm;


# direct methods
.method public constructor <init>(LX/1Zm;)V
    .locals 0

    .prologue
    .line 1971585
    iput-object p1, p0, LX/DAv;->a:LX/1Zm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 1971586
    iget-object v0, p0, LX/DAv;->a:LX/1Zm;

    iget-object v0, v0, LX/1Zm;->k:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1971587
    :goto_0
    return-void

    .line 1971588
    :cond_0
    iget-object v0, p0, LX/DAv;->a:LX/1Zm;

    iget-object v0, v0, LX/1Zm;->k:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1971589
    iget-object v0, p0, LX/DAv;->a:LX/1Zm;

    iget-object v0, v0, LX/1Zm;->f:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, LX/DAu;

    invoke-direct {v2, p0, p1}, LX/DAu;-><init>(LX/DAv;Landroid/bluetooth/BluetoothDevice;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/bluetooth/BluetoothDevice;->connectGatt(Landroid/content/Context;ZLandroid/bluetooth/BluetoothGattCallback;)Landroid/bluetooth/BluetoothGatt;

    goto :goto_0
.end method
