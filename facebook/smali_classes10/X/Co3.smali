.class public LX/Co3;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/richdocument/view/block/RelatedArticleCompressedSocialBlockView;",
        "Lcom/facebook/richdocument/model/data/RelatedArticleBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;)V
    .locals 1

    .prologue
    .line 1934790
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 1934791
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Co3;

    invoke-static {v0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v0

    check-cast v0, LX/Ck0;

    iput-object v0, p0, LX/Co3;->d:LX/Ck0;

    .line 1934792
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 12

    .prologue
    .line 1934793
    check-cast p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    .line 1934794
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934795
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    .line 1934796
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 1934797
    iget-object v1, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1934798
    iget-object v2, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1934799
    iget-object v3, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1934800
    iget-object v4, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->i:Ljava/lang/String;

    move-object v4, v4

    .line 1934801
    iget-boolean v5, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->k:Z

    move v5, v5

    .line 1934802
    iget-object v6, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->m:Ljava/lang/String;

    move-object v6, v6

    .line 1934803
    iget-object v7, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v7, v7

    .line 1934804
    iget-object v8, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->p:Ljava/lang/String;

    move-object v8, v8

    .line 1934805
    iget-object v9, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->j:Ljava/lang/String;

    move-object v9, v9

    .line 1934806
    iget-object v10, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->r:Ljava/util/List;

    move-object v10, v10

    .line 1934807
    iget-object v11, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->s:Ljava/lang/String;

    move-object v11, v11

    .line 1934808
    invoke-virtual/range {v0 .. v11}, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 1934809
    iget-object v0, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->n:LX/Cm2;

    move-object v1, v0

    .line 1934810
    sget-object v0, LX/Cm2;->BOTTOM:LX/Cm2;

    if-ne v1, v0, :cond_2

    .line 1934811
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934812
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    const-string v2, "footer_related_article"

    .line 1934813
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->y:Ljava/lang/String;

    .line 1934814
    :cond_0
    :goto_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934815
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    invoke-interface {v0}, LX/CnG;->c()Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, LX/Co3;->d:LX/Ck0;

    invoke-static {v0, v1, v2}, LX/Crt;->a(Landroid/view/View;LX/Cm2;LX/Ck0;)V

    .line 1934816
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934817
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0622

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1934818
    iput v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->v:I

    .line 1934819
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934820
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    .line 1934821
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->z:Ljava/lang/String;

    .line 1934822
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934823
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->n()Ljava/lang/String;

    move-result-object v2

    .line 1934824
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->B:Ljava/lang/String;

    .line 1934825
    iget v0, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->l:I

    move v2, v0

    .line 1934826
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934827
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    .line 1934828
    iput v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->w:I

    .line 1934829
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934830
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    .line 1934831
    iget v3, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->q:I

    move v3, v3

    .line 1934832
    iput v3, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->x:I

    .line 1934833
    sget-object v0, LX/Cm2;->BOTTOM:LX/Cm2;

    if-ne v1, v0, :cond_1

    const/4 v0, 0x1

    if-ne v2, v0, :cond_1

    .line 1934834
    iget-object v0, p0, LX/CnT;->a:LX/Chv;

    new-instance v1, LX/CiE;

    invoke-direct {v1}, LX/CiE;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1934835
    :cond_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934836
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    invoke-interface {p1}, LX/Clr;->lw_()LX/Cml;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CnG;->a(LX/Cml;)V

    .line 1934837
    return-void

    .line 1934838
    :cond_2
    sget-object v0, LX/Cm2;->INLINE:LX/Cm2;

    if-ne v1, v0, :cond_0

    .line 1934839
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 1934840
    check-cast v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    const-string v2, "inline_related_article"

    .line 1934841
    iput-object v2, v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->y:Ljava/lang/String;

    .line 1934842
    goto :goto_0
.end method
