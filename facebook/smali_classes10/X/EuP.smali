.class public LX/EuP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile h:LX/EuP;


# instance fields
.field public final b:LX/1rq;

.field public final c:LX/EuQ;

.field private final d:LX/0Sh;

.field private final e:LX/95S;

.field private final f:LX/95S;

.field private final g:LX/Evy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2179585
    new-instance v0, LX/EuO;

    invoke-direct {v0}, LX/EuO;-><init>()V

    sput-object v0, LX/EuP;->a:LX/0QK;

    return-void
.end method

.method public constructor <init>(LX/1rq;LX/EuQ;LX/0Sh;LX/Evy;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2179555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2179556
    iput-object p1, p0, LX/EuP;->b:LX/1rq;

    .line 2179557
    iput-object p2, p0, LX/EuP;->c:LX/EuQ;

    .line 2179558
    iput-object p3, p0, LX/EuP;->d:LX/0Sh;

    .line 2179559
    iput-object p4, p0, LX/EuP;->g:LX/Evy;

    .line 2179560
    new-instance v0, LX/95S;

    .line 2179561
    iget-object v2, p0, LX/EuP;->b:LX/1rq;

    const-string v3, "FriendsCenterSuggestions"

    new-instance v4, LX/EuS;

    invoke-direct {v4}, LX/EuS;-><init>()V

    invoke-virtual {v2, v3, v4}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v2

    const-wide/16 v4, 0x258

    .line 2179562
    iput-wide v4, v2, LX/2jj;->j:J

    .line 2179563
    move-object v2, v2

    .line 2179564
    const/4 v3, 0x1

    .line 2179565
    iput-boolean v3, v2, LX/2jj;->k:Z

    .line 2179566
    move-object v2, v2

    .line 2179567
    iget-object v3, p0, LX/EuP;->c:LX/EuQ;

    invoke-virtual {v3}, LX/EuQ;->b()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2179568
    const/16 v3, 0x14

    .line 2179569
    iput v3, v2, LX/2jj;->o:I

    .line 2179570
    :cond_0
    move-object v1, v2

    .line 2179571
    invoke-direct {v0, v1}, LX/95S;-><init>(LX/2jj;)V

    iput-object v0, p0, LX/EuP;->e:LX/95S;

    .line 2179572
    new-instance v0, LX/95S;

    .line 2179573
    iget-object v2, p0, LX/EuP;->b:LX/1rq;

    const-string v3, "FriendsCenter"

    new-instance v4, LX/EuR;

    invoke-direct {v4}, LX/EuR;-><init>()V

    invoke-virtual {v2, v3, v4}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v2

    const-wide/16 v4, 0x258

    .line 2179574
    iput-wide v4, v2, LX/2jj;->j:J

    .line 2179575
    move-object v2, v2

    .line 2179576
    const/4 v3, 0x1

    .line 2179577
    iput-boolean v3, v2, LX/2jj;->k:Z

    .line 2179578
    move-object v2, v2

    .line 2179579
    const/16 v3, 0x14

    .line 2179580
    iput v3, v2, LX/2jj;->o:I

    .line 2179581
    move-object v2, v2

    .line 2179582
    move-object v1, v2

    .line 2179583
    invoke-direct {v0, v1}, LX/95S;-><init>(LX/2jj;)V

    iput-object v0, p0, LX/EuP;->f:LX/95S;

    .line 2179584
    return-void
.end method

.method public static a(LX/0QB;)LX/EuP;
    .locals 7

    .prologue
    .line 2179539
    sget-object v0, LX/EuP;->h:LX/EuP;

    if-nez v0, :cond_1

    .line 2179540
    const-class v1, LX/EuP;

    monitor-enter v1

    .line 2179541
    :try_start_0
    sget-object v0, LX/EuP;->h:LX/EuP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2179542
    if-eqz v2, :cond_0

    .line 2179543
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2179544
    new-instance p0, LX/EuP;

    const-class v3, LX/1rq;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1rq;

    .line 2179545
    new-instance v5, LX/EuQ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, v4}, LX/EuQ;-><init>(LX/0ad;)V

    .line 2179546
    move-object v4, v5

    .line 2179547
    check-cast v4, LX/EuQ;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {v0}, LX/Evy;->a(LX/0QB;)LX/Evy;

    move-result-object v6

    check-cast v6, LX/Evy;

    invoke-direct {p0, v3, v4, v5, v6}, LX/EuP;-><init>(LX/1rq;LX/EuQ;LX/0Sh;LX/Evy;)V

    .line 2179548
    move-object v0, p0

    .line 2179549
    sput-object v0, LX/EuP;->h:LX/EuP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2179550
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2179551
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2179552
    :cond_1
    sget-object v0, LX/EuP;->h:LX/EuP;

    return-object v0

    .line 2179553
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2179554
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/95R;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/95R",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179526
    iget-object v0, p0, LX/EuP;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2179527
    iget-object v0, p0, LX/EuP;->c:LX/EuQ;

    invoke-virtual {v0}, LX/EuQ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/EuP;->c:LX/EuQ;

    invoke-virtual {v0}, LX/EuQ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2179528
    const/4 v0, 0x0

    .line 2179529
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/EuP;->e:LX/95S;

    invoke-virtual {v0}, LX/95S;->a()LX/95R;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2179535
    iget-object v0, p0, LX/EuP;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2179536
    iget-object v0, p0, LX/EuP;->c:LX/EuQ;

    invoke-virtual {v0}, LX/EuQ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2179537
    :goto_0
    return-void

    .line 2179538
    :cond_0
    iget-object v0, p0, LX/EuP;->e:LX/95S;

    sget-object v1, LX/EuP;->a:LX/0QK;

    invoke-virtual {v0, v1}, LX/95S;->a(LX/0QK;)V

    goto :goto_0
.end method

.method public final c()LX/95R;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/95R",
            "<",
            "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2179533
    iget-object v0, p0, LX/EuP;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2179534
    iget-object v0, p0, LX/EuP;->f:LX/95S;

    invoke-virtual {v0}, LX/95S;->a()LX/95R;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2179530
    iget-object v0, p0, LX/EuP;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2179531
    iget-object v0, p0, LX/EuP;->f:LX/95S;

    sget-object v1, LX/EuP;->a:LX/0QK;

    invoke-virtual {v0, v1}, LX/95S;->a(LX/0QK;)V

    .line 2179532
    return-void
.end method
