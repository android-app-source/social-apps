.class public final LX/EtQ;
.super LX/0vn;
.source ""


# instance fields
.field public final synthetic b:LX/EtR;


# direct methods
.method public constructor <init>(LX/EtR;)V
    .locals 0

    .prologue
    .line 2177918
    iput-object p1, p0, LX/EtQ;->b:LX/EtR;

    invoke-direct {p0}, LX/0vn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2177888
    invoke-super {p0, p1, p2}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 2177889
    iget-object v0, p0, LX/EtQ;->b:LX/EtR;

    invoke-static {v0}, LX/EtR;->h(LX/EtR;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2177890
    :goto_0
    return-void

    .line 2177891
    :cond_0
    invoke-virtual {p2, v1}, LX/3sp;->h(Z)V

    .line 2177892
    check-cast p1, LX/EtO;

    .line 2177893
    iget-object v0, p0, LX/EtQ;->b:LX/EtR;

    invoke-static {v0, p1, v1}, LX/EtR;->a$redex0(LX/EtR;LX/EtO;Z)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2177894
    const/16 v0, 0x10

    if-ne p2, v0, :cond_0

    iget-object v0, p0, LX/EtQ;->b:LX/EtR;

    invoke-static {v0}, LX/EtR;->h(LX/EtR;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2177895
    :cond_0
    invoke-super {p0, p1, p2, p3}, LX/0vn;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v2

    .line 2177896
    :cond_1
    :goto_0
    return v2

    .line 2177897
    :cond_2
    check-cast p1, LX/EtO;

    .line 2177898
    invoke-virtual {p1}, LX/EtO;->getItemData()LX/3v3;

    move-result-object v3

    .line 2177899
    if-eqz v3, :cond_4

    invoke-virtual {v3}, LX/3v3;->isCheckable()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, LX/3v3;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 2177900
    :goto_1
    const-string v4, "CHECKED_BEFORE_CLICK"

    invoke-virtual {p3, v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 2177901
    if-eqz v3, :cond_5

    invoke-virtual {v3}, LX/3v3;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2177902
    :goto_2
    const-string v3, "ENABLED_BEFORE_CLICK"

    invoke-virtual {p3, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 2177903
    if-ne v1, v3, :cond_3

    if-eq v0, v4, :cond_1

    .line 2177904
    :cond_3
    invoke-virtual {p1}, LX/EtO;->c()Ljava/lang/CharSequence;

    move-result-object v4

    .line 2177905
    const-string v5, "TEXT_BEFORE_CLICK"

    invoke-virtual {p3, v5, v4}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 2177906
    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2177907
    if-eq v1, v3, :cond_7

    .line 2177908
    if-eqz v1, :cond_6

    const v0, 0x7f08337a

    .line 2177909
    :goto_3
    iget-object v1, p0, LX/EtQ;->b:LX/EtR;

    iget-object v1, v1, LX/EtR;->g:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2177910
    :goto_4
    iget-object v1, p0, LX/EtQ;->b:LX/EtR;

    iget-object v1, v1, LX/EtR;->o:LX/0wL;

    .line 2177911
    invoke-static {v1, p1, v0}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 2177912
    goto :goto_0

    :cond_4
    move v0, v2

    .line 2177913
    goto :goto_1

    :cond_5
    move v1, v2

    .line 2177914
    goto :goto_2

    .line 2177915
    :cond_6
    const v0, 0x7f08337b

    goto :goto_3

    .line 2177916
    :cond_7
    if-eqz v0, :cond_8

    const v0, 0x7f083378

    goto :goto_3

    :cond_8
    const v0, 0x7f083379

    goto :goto_3

    .line 2177917
    :cond_9
    iget-object v0, p0, LX/EtQ;->b:LX/EtR;

    invoke-static {v0, p1, v2}, LX/EtR;->a$redex0(LX/EtR;LX/EtO;Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_4
.end method
