.class public LX/DcT;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DcT;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018446
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2018447
    sget-object v0, LX/0ax;->eF:Ljava/lang/String;

    const-string v1, "{com.facebook.katana.profile.id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->STRUCTURED_MENU_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2018448
    return-void
.end method

.method public static a(LX/0QB;)LX/DcT;
    .locals 3

    .prologue
    .line 2018449
    sget-object v0, LX/DcT;->a:LX/DcT;

    if-nez v0, :cond_1

    .line 2018450
    const-class v1, LX/DcT;

    monitor-enter v1

    .line 2018451
    :try_start_0
    sget-object v0, LX/DcT;->a:LX/DcT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2018452
    if-eqz v2, :cond_0

    .line 2018453
    :try_start_1
    new-instance v0, LX/DcT;

    invoke-direct {v0}, LX/DcT;-><init>()V

    .line 2018454
    move-object v0, v0

    .line 2018455
    sput-object v0, LX/DcT;->a:LX/DcT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2018456
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2018457
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2018458
    :cond_1
    sget-object v0, LX/DcT;->a:LX/DcT;

    return-object v0

    .line 2018459
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2018460
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
