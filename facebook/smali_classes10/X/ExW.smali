.class public final LX/ExW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;)V
    .locals 0

    .prologue
    .line 2184781
    iput-object p1, p0, LX/ExW;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2184782
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2184783
    check-cast p1, LX/0Px;

    const/4 v0, 0x0

    .line 2184784
    if-nez p1, :cond_0

    .line 2184785
    :goto_0
    return-void

    .line 2184786
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v3, v0

    move v2, v0

    :goto_1
    if-ge v3, v4, :cond_3

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2184787
    iget-object v1, p0, LX/ExW;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Eus;

    .line 2184788
    if-eqz v1, :cond_2

    .line 2184789
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->m()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2184790
    iget-object v0, p0, LX/ExW;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, LX/Eus;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2184791
    const/4 v0, 0x1

    .line 2184792
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_1

    .line 2184793
    :cond_1
    invoke-virtual {v1}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    if-eq v5, v6, :cond_2

    .line 2184794
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Eus;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    :cond_2
    move v0, v2

    goto :goto_2

    .line 2184795
    :cond_3
    if-eqz v2, :cond_4

    .line 2184796
    iget-object v0, p0, LX/ExW;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    iget-object v1, p0, LX/ExW;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->s:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;->a(Ljava/util/Collection;)V

    .line 2184797
    :goto_3
    iget-object v0, p0, LX/ExW;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    sget-object v1, LX/Ey6;->LOADING_STATE_IDLE:LX/Ey6;

    invoke-static {v0, v1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;LX/Ey6;)V

    goto :goto_0

    .line 2184798
    :cond_4
    iget-object v0, p0, LX/ExW;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsRedesignFragment;->k:Lcom/facebook/friending/center/ui/FriendsCenterSuggestionsRedesignAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_3
.end method
