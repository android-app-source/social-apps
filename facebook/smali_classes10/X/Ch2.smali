.class public LX/Ch2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j2;",
        "PluginSession::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TPluginSession;>;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/res/Resources;

.field private final c:LX/79D;

.field public d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field public h:I


# direct methods
.method public constructor <init>(LX/0il;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;LX/79D;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPluginSession;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/content/res/Resources;",
            "LX/79D;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1927754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1927755
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Ch2;->d:LX/0am;

    .line 1927756
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Ch2;->a:Ljava/lang/ref/WeakReference;

    .line 1927757
    iput-object p2, p0, LX/Ch2;->f:Ljava/lang/String;

    .line 1927758
    iput-object p3, p0, LX/Ch2;->g:Ljava/lang/String;

    .line 1927759
    iput-object p4, p0, LX/Ch2;->b:Landroid/content/res/Resources;

    .line 1927760
    iput-object p5, p0, LX/Ch2;->c:LX/79D;

    .line 1927761
    invoke-static {p0}, LX/Ch2;->f(LX/Ch2;)V

    .line 1927762
    return-void
.end method

.method public static a(LX/Ch2;I)Z
    .locals 1

    .prologue
    .line 1927751
    if-lez p1, :cond_0

    .line 1927752
    iget v0, p0, LX/Ch2;->e:I

    move v0, v0

    .line 1927753
    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)V
    .locals 7

    .prologue
    .line 1927742
    invoke-static {p0, p1}, LX/Ch2;->d(LX/Ch2;I)LX/Ch1;

    move-result-object v0

    .line 1927743
    invoke-static {p0}, LX/Ch2;->g(LX/Ch2;)I

    move-result v4

    .line 1927744
    invoke-static {p0, v4}, LX/Ch2;->d(LX/Ch2;I)LX/Ch1;

    move-result-object v1

    .line 1927745
    if-eq v1, v0, :cond_0

    .line 1927746
    iget-object v0, p0, LX/Ch2;->c:LX/79D;

    iget-object v1, p0, LX/Ch2;->f:Ljava/lang/String;

    iget-object v2, p0, LX/Ch2;->g:Ljava/lang/String;

    iget v5, p0, LX/Ch2;->e:I

    move v3, p1

    .line 1927747
    const-string v6, "composer_review_length_category_changed"

    invoke-static {v6, v1, v2}, LX/79D;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 1927748
    const-string p0, "old_review_length"

    invoke-virtual {v6, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "new_review_length"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "review_length_threshold"

    invoke-virtual {p0, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1927749
    iget-object p0, v0, LX/79D;->a:LX/0Zb;

    invoke-interface {p0, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1927750
    :cond_0
    return-void
.end method

.method private static d(LX/Ch2;I)LX/Ch1;
    .locals 1

    .prologue
    .line 1927739
    if-nez p1, :cond_0

    .line 1927740
    sget-object v0, LX/Ch1;->EMPTY_REVIEW:LX/Ch1;

    .line 1927741
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, LX/Ch2;->a(LX/Ch2;I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/Ch1;->NON_EMPTY_BELOW_THRESHOLD:LX/Ch1;

    goto :goto_0

    :cond_1
    sget-object v0, LX/Ch1;->ABOVE_THRESHOLD:LX/Ch1;

    goto :goto_0
.end method

.method private d()V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1927719
    iget-object v0, p0, LX/Ch2;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1927720
    :goto_0
    return-void

    .line 1927721
    :cond_0
    invoke-static {p0}, LX/Ch2;->g(LX/Ch2;)I

    move-result v1

    .line 1927722
    invoke-static {p0, v1}, LX/Ch2;->a(LX/Ch2;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1927723
    iget-object v0, p0, LX/Ch2;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1927724
    iget-object v2, p0, LX/Ch2;->b:Landroid/content/res/Resources;

    const v3, 0x7f081506

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "[[REVIEW_LENGTH]]"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, LX/Ch2;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1927725
    new-instance v3, LX/47x;

    iget-object v4, p0, LX/Ch2;->b:Landroid/content/res/Resources;

    invoke-direct {v3, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v3, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v2

    const-string v3, "[[REVIEW_LENGTH]]"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    const/high16 v6, -0x10000

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v6, 0x21

    invoke-virtual {v2, v3, v4, v5, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v2

    .line 1927726
    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v2

    move-object v1, v2

    .line 1927727
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1927728
    iget-object v0, p0, LX/Ch2;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1927729
    :cond_1
    iget-object v0, p0, LX/Ch2;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static f(LX/Ch2;)V
    .locals 2

    .prologue
    .line 1927737
    iget-object v0, p0, LX/Ch2;->b:Landroid/content/res/Resources;

    const v1, 0x7f0c002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, LX/Ch2;->e:I

    .line 1927738
    return-void
.end method

.method public static g(LX/Ch2;)I
    .locals 1

    .prologue
    .line 1927735
    iget-object v0, p0, LX/Ch2;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j2;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1927736
    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 1927730
    iget v0, p0, LX/Ch2;->e:I

    if-lez v0, :cond_0

    .line 1927731
    invoke-direct {p0}, LX/Ch2;->d()V

    .line 1927732
    iget v0, p0, LX/Ch2;->h:I

    invoke-direct {p0, v0}, LX/Ch2;->c(I)V

    .line 1927733
    invoke-static {p0}, LX/Ch2;->g(LX/Ch2;)I

    move-result v0

    iput v0, p0, LX/Ch2;->h:I

    .line 1927734
    :cond_0
    return-void
.end method
