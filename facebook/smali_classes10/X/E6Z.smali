.class public final LX/E6Z;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:LX/E6d;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:LX/E6b;


# direct methods
.method public constructor <init>(LX/E6d;Ljava/lang/String;Ljava/lang/String;LX/E6b;)V
    .locals 0

    .prologue
    .line 2080303
    iput-object p1, p0, LX/E6Z;->a:LX/E6d;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    .line 2080304
    iput-object p4, p0, LX/E6Z;->d:LX/E6b;

    .line 2080305
    iput-object p2, p0, LX/E6Z;->b:Ljava/lang/String;

    .line 2080306
    iput-object p3, p0, LX/E6Z;->c:Ljava/lang/String;

    .line 2080307
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 9

    .prologue
    .line 2080308
    iget-object v0, p0, LX/E6Z;->d:LX/E6b;

    invoke-virtual {v0}, LX/E6b;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2080309
    iget-object v0, p0, LX/E6Z;->d:LX/E6b;

    .line 2080310
    iget-boolean v1, v0, LX/E6b;->d:Z

    if-eqz v1, :cond_0

    .line 2080311
    iget-object v1, v0, LX/E6b;->a:LX/E6d;

    iget-object v1, v1, LX/E6d;->e:Lcom/facebook/reaction/ReactionUtil;

    iget-object v2, v0, LX/E6b;->f:Ljava/lang/String;

    new-instance v3, LX/E6a;

    invoke-direct {v3, v0}, LX/E6a;-><init>(LX/E6b;)V

    const/4 v4, 0x5

    iget-object v5, v0, LX/E6b;->g:Ljava/lang/String;

    .line 2080312
    invoke-static {}, LX/9qR;->c()LX/9qN;

    move-result-object v6

    .line 2080313
    const-string v7, "reaction_after_cursor"

    invoke-virtual {v6, v7, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "reaction_result_count"

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v8, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "reaction_story_id"

    invoke-virtual {v7, v8, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2080314
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    .line 2080315
    iget-object v7, v1, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 2080316
    iget-object v7, v1, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    invoke-virtual {v7, v5, v6, v3}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2080317
    :cond_0
    if-lez p1, :cond_1

    .line 2080318
    iget-object v0, p0, LX/E6Z;->a:LX/E6d;

    iget-object v1, p0, LX/E6Z;->b:Ljava/lang/String;

    iget-object v2, p0, LX/E6Z;->c:Ljava/lang/String;

    .line 2080319
    invoke-virtual {v0, v1, v2}, LX/Cfk;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2080320
    :cond_1
    iget-object v0, p0, LX/E6Z;->a:LX/E6d;

    .line 2080321
    iput p1, v0, LX/E6d;->c:I

    .line 2080322
    return-void
.end method
