.class public final LX/EJV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic b:LX/Cxe;

.field public final synthetic c:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/Cxe;)V
    .locals 0

    .prologue
    .line 2104237
    iput-object p1, p0, LX/EJV;->c:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    iput-object p2, p0, LX/EJV;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object p3, p0, LX/EJV;->b:LX/Cxe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x7d03d663

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2104206
    iget-object v1, p0, LX/EJV;->c:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->a:LX/1nG;

    iget-object v2, p0, LX/EJV;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    iget-object v3, p0, LX/EJV;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2104207
    if-eqz v1, :cond_0

    .line 2104208
    iget-object v2, p0, LX/EJV;->c:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->b:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2104209
    :cond_0
    iget-object v1, p0, LX/EJV;->b:LX/Cxe;

    iget-object v2, p0, LX/EJV;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v1, v2}, LX/Cxe;->c(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 2104210
    iget-object v1, p0, LX/EJV;->c:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    iget-object v2, p0, LX/EJV;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v3, p0, LX/EJV;->b:LX/Cxe;

    const/4 v8, 0x0

    const/4 p1, 0x0

    .line 2104211
    iget-object v5, v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->f:LX/0ad;

    sget-short v6, LX/100;->bw:S

    invoke-interface {v5, v6, p1}, LX/0ad;->a(SZ)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2104212
    :goto_0
    const v1, 0x35b1023a

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2104213
    :cond_1
    new-instance v5, LX/CwR;

    invoke-direct {v5}, LX/CwR;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v6

    .line 2104214
    iput-object v6, v5, LX/CwR;->b:Ljava/lang/String;

    .line 2104215
    move-object v5, v5

    .line 2104216
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 2104217
    iput-object v6, v5, LX/CwR;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2104218
    move-object v5, v5

    .line 2104219
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v6

    .line 2104220
    iput-object v6, v5, LX/CwR;->a:Ljava/lang/String;

    .line 2104221
    move-object v5, v5

    .line 2104222
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    :cond_2
    const/4 v6, 0x1

    .line 2104223
    :goto_1
    if-eqz v6, :cond_5

    const-string v6, ""

    :goto_2
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object v6, v6

    .line 2104224
    iput-object v6, v5, LX/CwR;->e:Landroid/net/Uri;

    .line 2104225
    move-object v5, v5

    .line 2104226
    const/4 v6, 0x1

    .line 2104227
    iput-boolean v6, v5, LX/CwR;->d:Z

    .line 2104228
    move-object v5, v5

    .line 2104229
    invoke-virtual {v5}, LX/CwR;->j()Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v9

    .line 2104230
    check-cast v3, LX/CxV;

    invoke-interface {v3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v7

    .line 2104231
    iget-object v5, v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2SY;

    iget-object v6, v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->f:LX/0ad;

    sget-short p0, LX/100;->cb:S

    invoke-interface {v6, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v6, v7

    .line 2104232
    :goto_3
    invoke-static {v5, v9, v6}, LX/2SY;->b(LX/2SY;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;Ljava/lang/String;)V

    .line 2104233
    iget-object v5, v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/EQ6;

    invoke-virtual {v5, v8, v9, v7}, LX/EQ6;->a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v6, v8

    .line 2104234
    goto :goto_3

    .line 2104235
    :cond_4
    const/4 v6, 0x0

    goto :goto_1

    .line 2104236
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method
