.class public LX/DXK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/DXd;

.field public final b:LX/DXU;

.field public final c:LX/DXY;


# direct methods
.method public constructor <init>(LX/DXd;LX/DXU;LX/DXY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2010102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2010103
    iput-object p1, p0, LX/DXK;->a:LX/DXd;

    .line 2010104
    iput-object p2, p0, LX/DXK;->b:LX/DXU;

    .line 2010105
    iput-object p3, p0, LX/DXK;->c:LX/DXY;

    .line 2010106
    return-void
.end method

.method public static a(LX/0QB;)LX/DXK;
    .locals 6

    .prologue
    .line 2010091
    const-class v1, LX/DXK;

    monitor-enter v1

    .line 2010092
    :try_start_0
    sget-object v0, LX/DXK;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2010093
    sput-object v2, LX/DXK;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2010094
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2010095
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2010096
    new-instance p0, LX/DXK;

    invoke-static {v0}, LX/DXd;->a(LX/0QB;)LX/DXd;

    move-result-object v3

    check-cast v3, LX/DXd;

    invoke-static {v0}, LX/DXU;->a(LX/0QB;)LX/DXU;

    move-result-object v4

    check-cast v4, LX/DXU;

    invoke-static {v0}, LX/DXY;->a(LX/0QB;)LX/DXY;

    move-result-object v5

    check-cast v5, LX/DXY;

    invoke-direct {p0, v3, v4, v5}, LX/DXK;-><init>(LX/DXd;LX/DXU;LX/DXY;)V

    .line 2010097
    move-object v0, p0

    .line 2010098
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2010099
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DXK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2010100
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2010101
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
