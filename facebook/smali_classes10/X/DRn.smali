.class public final LX/DRn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/GroupAdminListFragment;)V
    .locals 0

    .prologue
    .line 1998541
    iput-object p1, p0, LX/DRn;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x386c6ca9

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1998542
    iget-object v1, p0, LX/DRn;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    iget-object v1, v1, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DRn;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    iget-object v2, v2, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->d:LX/DTp;

    iget-object v3, p0, LX/DRn;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    .line 1998543
    iget-object v4, v3, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1998544
    iget-object v4, p0, LX/DRn;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    .line 1998545
    iget-object p1, v4, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object v4, p1

    .line 1998546
    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1998547
    invoke-static {v2}, LX/DTp;->a(LX/DTp;)Landroid/content/Intent;

    move-result-object v6

    .line 1998548
    const-string v7, "group_feed_id"

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1998549
    const-string v7, "group_admin_type"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1998550
    const-string v7, "target_fragment"

    sget-object p1, LX/0cQ;->GROUP_SUGGEST_ADMIN_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {v6, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1998551
    move-object v2, v6

    .line 1998552
    iget-object v3, p0, LX/DRn;->a:Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1998553
    const v1, 0x785fd617

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
