.class public final LX/Egj;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Egl;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FBn;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/Egl;


# direct methods
.method public constructor <init>(LX/Egl;)V
    .locals 1

    .prologue
    .line 2157172
    iput-object p1, p0, LX/Egj;->d:LX/Egl;

    .line 2157173
    move-object v0, p1

    .line 2157174
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2157175
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2157176
    if-ne p0, p1, :cond_1

    .line 2157177
    :cond_0
    :goto_0
    return v0

    .line 2157178
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2157179
    goto :goto_0

    .line 2157180
    :cond_3
    check-cast p1, LX/Egj;

    .line 2157181
    iget-object v2, p0, LX/Egj;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Egj;->b:Ljava/lang/String;

    iget-object v3, p1, LX/Egj;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2157182
    goto :goto_0

    .line 2157183
    :cond_5
    iget-object v2, p1, LX/Egj;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2157184
    :cond_6
    iget-object v2, p0, LX/Egj;->c:Ljava/util/List;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Egj;->c:Ljava/util/List;

    iget-object v3, p1, LX/Egj;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2157185
    goto :goto_0

    .line 2157186
    :cond_7
    iget-object v2, p1, LX/Egj;->c:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
