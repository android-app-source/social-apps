.class public LX/EMp;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EMn;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EMq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2111402
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EMp;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EMq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111403
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2111404
    iput-object p1, p0, LX/EMp;->b:LX/0Ot;

    .line 2111405
    return-void
.end method

.method public static a(LX/0QB;)LX/EMp;
    .locals 4

    .prologue
    .line 2111406
    const-class v1, LX/EMp;

    monitor-enter v1

    .line 2111407
    :try_start_0
    sget-object v0, LX/EMp;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111408
    sput-object v2, LX/EMp;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111409
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111410
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111411
    new-instance v3, LX/EMp;

    const/16 p0, 0x3426

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EMp;-><init>(LX/0Ot;)V

    .line 2111412
    move-object v0, v3

    .line 2111413
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111414
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EMp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111415
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111416
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2111417
    check-cast p2, LX/EMo;

    .line 2111418
    iget-object v0, p0, LX/EMp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/EMo;->a:Ljava/lang/CharSequence;

    const/4 p2, 0x0

    .line 2111419
    const v1, 0x7f0e0123

    invoke-static {p1, p2, v1}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    sget-object p0, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, p0}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 p0, 0x1

    invoke-interface {v1, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    const p0, 0x7f0b0060

    invoke-interface {v1, p2, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/4 p0, 0x2

    const p2, 0x7f0b0060

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/16 p0, 0x20

    invoke-interface {v1, p0}, LX/1Di;->r(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2111420
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2111421
    invoke-static {}, LX/1dS;->b()V

    .line 2111422
    const/4 v0, 0x0

    return-object v0
.end method
