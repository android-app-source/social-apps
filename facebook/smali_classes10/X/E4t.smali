.class public final LX/E4t;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E4u;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public c:LX/2ja;

.field public d:LX/1Pq;

.field public e:LX/E2a;

.field public final synthetic f:LX/E4u;


# direct methods
.method public constructor <init>(LX/E4u;)V
    .locals 1

    .prologue
    .line 2077427
    iput-object p1, p0, LX/E4t;->f:LX/E4u;

    .line 2077428
    move-object v0, p1

    .line 2077429
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2077430
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2077431
    const-string v0, "ReactionExpandableActionDelegateComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2077432
    if-ne p0, p1, :cond_1

    .line 2077433
    :cond_0
    :goto_0
    return v0

    .line 2077434
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2077435
    goto :goto_0

    .line 2077436
    :cond_3
    check-cast p1, LX/E4t;

    .line 2077437
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2077438
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2077439
    if-eq v2, v3, :cond_0

    .line 2077440
    iget-object v2, p0, LX/E4t;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E4t;->a:LX/1X1;

    iget-object v3, p1, LX/E4t;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2077441
    goto :goto_0

    .line 2077442
    :cond_5
    iget-object v2, p1, LX/E4t;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 2077443
    :cond_6
    iget-object v2, p0, LX/E4t;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/E4t;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/E4t;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2077444
    goto :goto_0

    .line 2077445
    :cond_8
    iget-object v2, p1, LX/E4t;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_7

    .line 2077446
    :cond_9
    iget-object v2, p0, LX/E4t;->c:LX/2ja;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/E4t;->c:LX/2ja;

    iget-object v3, p1, LX/E4t;->c:LX/2ja;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2077447
    goto :goto_0

    .line 2077448
    :cond_b
    iget-object v2, p1, LX/E4t;->c:LX/2ja;

    if-nez v2, :cond_a

    .line 2077449
    :cond_c
    iget-object v2, p0, LX/E4t;->d:LX/1Pq;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/E4t;->d:LX/1Pq;

    iget-object v3, p1, LX/E4t;->d:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2077450
    goto :goto_0

    .line 2077451
    :cond_e
    iget-object v2, p1, LX/E4t;->d:LX/1Pq;

    if-nez v2, :cond_d

    .line 2077452
    :cond_f
    iget-object v2, p0, LX/E4t;->e:LX/E2a;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/E4t;->e:LX/E2a;

    iget-object v3, p1, LX/E4t;->e:LX/E2a;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2077453
    goto :goto_0

    .line 2077454
    :cond_10
    iget-object v2, p1, LX/E4t;->e:LX/E2a;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2077455
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/E4t;

    .line 2077456
    iget-object v1, v0, LX/E4t;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/E4t;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/E4t;->a:LX/1X1;

    .line 2077457
    return-object v0

    .line 2077458
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
