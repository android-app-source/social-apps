.class public LX/ExM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2184668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2184669
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2184670
    iput-object v0, p0, LX/ExM;->a:LX/0Ot;

    .line 2184671
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2184672
    iput-object v0, p0, LX/ExM;->b:LX/0Ot;

    .line 2184673
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2184674
    iput-object v0, p0, LX/ExM;->c:LX/0Ot;

    .line 2184675
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/83X;)V
    .locals 7

    .prologue
    .line 2184676
    instance-of v0, p2, LX/Eus;

    if-eqz v0, :cond_0

    .line 2184677
    iget-object v0, p0, LX/ExM;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2hd;

    move-object v0, p2

    check-cast v0, LX/Eus;

    .line 2184678
    iget-object v2, v0, LX/Eus;->h:Ljava/lang/String;

    move-object v3, v2

    .line 2184679
    invoke-interface {p2}, LX/2lr;->a()J

    move-result-wide v4

    invoke-interface {p2}, LX/83W;->g()LX/2h7;

    move-result-object v0

    iget-object v6, v0, LX/2h7;->peopleYouMayKnowLocation:LX/2hC;

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, LX/2hd;->d(Ljava/lang/String;Ljava/lang/String;JLX/2hC;)V

    .line 2184680
    :cond_0
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    invoke-interface {p2}, LX/2lr;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2184681
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2184682
    const-string v0, "timeline_friend_request_ref"

    sget-object v1, LX/5P2;->FRIENDS_CENTER:LX/5P2;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2184683
    new-instance v0, LX/5vg;

    invoke-direct {v0}, LX/5vg;-><init>()V

    invoke-interface {p2}, LX/2lr;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2184684
    iput-object v1, v0, LX/5vg;->d:Ljava/lang/String;

    .line 2184685
    move-object v0, v0

    .line 2184686
    new-instance v1, LX/4aM;

    invoke-direct {v1}, LX/4aM;-><init>()V

    invoke-interface {p2}, LX/2lq;->d()Ljava/lang/String;

    move-result-object v4

    .line 2184687
    iput-object v4, v1, LX/4aM;->b:Ljava/lang/String;

    .line 2184688
    move-object v1, v1

    .line 2184689
    invoke-virtual {v1}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    .line 2184690
    iput-object v1, v0, LX/5vg;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2184691
    move-object v0, v0

    .line 2184692
    invoke-interface {p2}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v1

    .line 2184693
    iput-object v1, v0, LX/5vg;->e:Ljava/lang/String;

    .line 2184694
    move-object v0, v0

    .line 2184695
    invoke-interface {p2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2184696
    iput-object v1, v0, LX/5vg;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2184697
    move-object v0, v0

    .line 2184698
    invoke-virtual {v0}, LX/5vg;->a()Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;

    move-result-object v0

    invoke-static {v3, v0}, LX/5ve;->a(Landroid/os/Bundle;LX/36O;)V

    .line 2184699
    iget-object v0, p0, LX/ExM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/ExM;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v0, v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2184700
    return-void
.end method
