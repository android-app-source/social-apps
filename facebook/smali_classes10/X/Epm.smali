.class public LX/Epm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Epm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G4V;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2170644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170645
    return-void
.end method

.method public static a(LX/0QB;)LX/Epm;
    .locals 5

    .prologue
    .line 2170646
    sget-object v0, LX/Epm;->c:LX/Epm;

    if-nez v0, :cond_1

    .line 2170647
    const-class v1, LX/Epm;

    monitor-enter v1

    .line 2170648
    :try_start_0
    sget-object v0, LX/Epm;->c:LX/Epm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2170649
    if-eqz v2, :cond_0

    .line 2170650
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2170651
    new-instance v3, LX/Epm;

    invoke-direct {v3}, LX/Epm;-><init>()V

    .line 2170652
    const/16 v4, 0x2eb

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x36ef

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2170653
    iput-object v4, v3, LX/Epm;->a:LX/0Or;

    iput-object p0, v3, LX/Epm;->b:LX/0Or;

    .line 2170654
    move-object v0, v3

    .line 2170655
    sput-object v0, LX/Epm;->c:LX/Epm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2170656
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2170657
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2170658
    :cond_1
    sget-object v0, LX/Epm;->c:LX/Epm;

    return-object v0

    .line 2170659
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2170660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/5vW;LX/Eoo;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 2170580
    invoke-interface {p2}, LX/5vW;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 2170581
    invoke-interface {p2}, LX/5vW;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, LX/5vW;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 2170582
    :cond_0
    invoke-interface {p2}, LX/5vW;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 2170583
    invoke-interface {p2}, LX/5vW;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->e()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2170584
    invoke-interface {p2}, LX/5vW;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->e()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 2170585
    :goto_0
    invoke-interface {p2}, LX/5vW;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->c()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 2170586
    invoke-interface {p2}, LX/5vW;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->c()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$TimelineContextListItemPhotoNodeFieldsModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v3

    move v5, v0

    move-object v8, v1

    move-object v1, v3

    move-object v3, v8

    .line 2170587
    :goto_1
    invoke-interface {p3}, LX/Eoo;->u()LX/Eoq;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2170588
    invoke-interface {p3}, LX/Eoo;->u()LX/Eoq;

    move-result-object v0

    invoke-interface {v0}, LX/Eoq;->b()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 2170589
    :goto_2
    iget-object v0, p0, LX/Epm;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4V;

    .line 2170590
    new-instance v7, LX/G4Y;

    invoke-direct {v7}, LX/G4Y;-><init>()V

    .line 2170591
    iput v5, v7, LX/G4Y;->a:I

    .line 2170592
    move-object v5, v7

    .line 2170593
    iput-object v4, v5, LX/G4Y;->b:Ljava/lang/String;

    .line 2170594
    move-object v4, v5

    .line 2170595
    iput-object v3, v4, LX/G4Y;->c:Ljava/lang/String;

    .line 2170596
    move-object v3, v4

    .line 2170597
    iput-object v1, v3, LX/G4Y;->d:Ljava/lang/String;

    .line 2170598
    move-object v1, v3

    .line 2170599
    sget-object v3, LX/74S;->PERSON_CARD_CONTEXT_ITEM:LX/74S;

    .line 2170600
    iput-object v3, v1, LX/G4Y;->e:LX/74S;

    .line 2170601
    move-object v1, v1

    .line 2170602
    invoke-interface {p2}, LX/5vW;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v3

    .line 2170603
    iput-object v3, v1, LX/G4Y;->f:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    .line 2170604
    move-object v1, v1

    .line 2170605
    invoke-interface {p3}, LX/Eoo;->c()Ljava/lang/String;

    move-result-object v3

    .line 2170606
    iput-object v3, v1, LX/G4Y;->g:Ljava/lang/String;

    .line 2170607
    move-object v1, v1

    .line 2170608
    invoke-interface {p3}, LX/Eoo;->ce_()Ljava/lang/String;

    move-result-object v3

    .line 2170609
    iput-object v3, v1, LX/G4Y;->h:Ljava/lang/String;

    .line 2170610
    move-object v1, v1

    .line 2170611
    iput-object v6, v1, LX/G4Y;->i:Ljava/lang/String;

    .line 2170612
    move-object v1, v1

    .line 2170613
    invoke-interface {p3}, LX/Eoo;->s()LX/2rX;

    move-result-object v3

    .line 2170614
    iput-object v3, v1, LX/G4Y;->j:LX/2rX;

    .line 2170615
    move-object v1, v1

    .line 2170616
    const-string v3, "person_card_context_item"

    .line 2170617
    iput-object v3, v1, LX/G4Y;->k:Ljava/lang/String;

    .line 2170618
    move-object v1, v1

    .line 2170619
    invoke-interface {p2}, LX/5vW;->e()Ljava/lang/String;

    move-result-object v3

    .line 2170620
    iput-object v3, v1, LX/G4Y;->l:Ljava/lang/String;

    .line 2170621
    move-object v1, v1

    .line 2170622
    invoke-interface {p3}, LX/Eoo;->v()LX/1k2;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 2170623
    if-eqz v3, :cond_1

    invoke-interface {v3}, LX/1k2;->a()LX/0Px;

    move-result-object v4

    if-nez v4, :cond_8

    :cond_1
    move-object v4, v5

    .line 2170624
    :goto_3
    move-object v3, v4

    .line 2170625
    iput-object v3, v1, LX/G4Y;->o:Ljava/lang/String;

    .line 2170626
    move-object v3, v1

    .line 2170627
    invoke-interface {p3}, LX/Eoo;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {p3}, LX/Eoo;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2170628
    :goto_4
    iput-object v1, v3, LX/G4Y;->m:Ljava/lang/String;

    .line 2170629
    move-object v1, v3

    .line 2170630
    invoke-interface {p3}, LX/Eoo;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {p3}, LX/Eoo;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2170631
    :cond_2
    iput-object v2, v1, LX/G4Y;->n:Ljava/lang/String;

    .line 2170632
    move-object v1, v1

    .line 2170633
    invoke-virtual {v1}, LX/G4Y;->a()LX/G4Z;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/G4V;->a(Landroid/content/Context;LX/G4Z;)V

    .line 2170634
    return-void

    :cond_3
    move-object v1, v2

    .line 2170635
    goto :goto_4

    :cond_4
    move-object v6, v2

    goto/16 :goto_2

    :cond_5
    move-object v3, v1

    move v5, v0

    move-object v1, v2

    goto/16 :goto_1

    :cond_6
    move-object v1, v2

    goto/16 :goto_0

    :cond_7
    move-object v1, v2

    move-object v3, v2

    move-object v4, v2

    move v5, v0

    goto/16 :goto_1

    .line 2170636
    :cond_8
    invoke-interface {v3}, LX/1k2;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v7

    :goto_5
    if-ge v6, v9, :cond_a

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2qo;

    .line 2170637
    invoke-interface {v4}, LX/2qo;->c()Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object p0

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->FIRST:Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    invoke-virtual {p0, p2}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2170638
    invoke-interface {v3}, LX/1k2;->m_()Ljava/lang/String;

    move-result-object v5

    .line 2170639
    invoke-interface {v4}, LX/2qo;->n_()I

    move-result v6

    invoke-virtual {v5, v7, v6}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v6

    .line 2170640
    invoke-interface {v4}, LX/2qo;->a()I

    move-result v4

    invoke-virtual {v5, v6, v4}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v4

    .line 2170641
    invoke-virtual {v5, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 2170642
    :cond_9
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_5

    :cond_a
    move-object v4, v5

    .line 2170643
    goto :goto_3
.end method
