.class public LX/CyD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3mF;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7vW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7vZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1952524
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1952525
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952526
    iput-object v0, p0, LX/CyD;->a:LX/0Ot;

    .line 1952527
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952528
    iput-object v0, p0, LX/CyD;->b:LX/0Ot;

    .line 1952529
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952530
    iput-object v0, p0, LX/CyD;->c:LX/0Ot;

    .line 1952531
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952532
    iput-object v0, p0, LX/CyD;->d:LX/0Ot;

    .line 1952533
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1952534
    iput-object v0, p0, LX/CyD;->e:LX/0Ot;

    .line 1952535
    return-void
.end method

.method public static a(LX/0QB;)LX/CyD;
    .locals 8

    .prologue
    .line 1952511
    const-class v1, LX/CyD;

    monitor-enter v1

    .line 1952512
    :try_start_0
    sget-object v0, LX/CyD;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1952513
    sput-object v2, LX/CyD;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1952514
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1952515
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1952516
    new-instance v3, LX/CyD;

    invoke-direct {v3}, LX/CyD;-><init>()V

    .line 1952517
    const/16 v4, 0xa71

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x474

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xb39

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1b5d

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x1b5e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1952518
    iput-object v4, v3, LX/CyD;->a:LX/0Ot;

    iput-object v5, v3, LX/CyD;->b:LX/0Ot;

    iput-object v6, v3, LX/CyD;->c:LX/0Ot;

    iput-object v7, v3, LX/CyD;->d:LX/0Ot;

    iput-object p0, v3, LX/CyD;->e:LX/0Ot;

    .line 1952519
    move-object v0, v3

    .line 1952520
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1952521
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CyD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1952522
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1952523
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/2dj;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2dj;",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1952477
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p1, v0, :cond_0

    .line 1952478
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2h8;->SEARCH:LX/2h8;

    move-object v1, p0

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1952479
    :goto_0
    return-object v0

    .line 1952480
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p1, v0, :cond_1

    .line 1952481
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    sget-object v2, LX/2h9;->SEARCH:LX/2h9;

    invoke-virtual {p0, v0, v1, v2}, LX/2dj;->a(JLX/2h9;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1952482
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p1, v0, :cond_2

    .line 1952483
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    sget-object v2, LX/2na;->CONFIRM:LX/2na;

    sget-object v3, LX/2hA;->SEARCH:LX/2hA;

    invoke-virtual {p0, v0, v1, v2, v3}, LX/2dj;->a(JLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1952484
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This type of friend event is not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/3iV;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 1952536
    new-instance v0, LX/21A;

    invoke-direct {v0}, LX/21A;-><init>()V

    const-string v1, "keyword_search"

    .line 1952537
    iput-object v1, v0, LX/21A;->c:Ljava/lang/String;

    .line 1952538
    move-object v0, v0

    .line 1952539
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    .line 1952540
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a()LX/5H8;

    move-result-object v0

    .line 1952541
    iput-object p1, v0, LX/5H8;->a:Ljava/lang/String;

    .line 1952542
    move-object v2, v0

    .line 1952543
    if-nez p2, :cond_0

    const/4 v0, 0x1

    .line 1952544
    :goto_0
    iput-boolean v0, v2, LX/5H8;->b:Z

    .line 1952545
    move-object v0, v2

    .line 1952546
    iput-object v1, v0, LX/5H8;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1952547
    move-object v0, v0

    .line 1952548
    invoke-virtual {v0}, LX/5H8;->a()Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    move-result-object v0

    .line 1952549
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/3iV;->a(Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1952550
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 1952499
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 1952500
    :goto_0
    sparse-switch v0, :sswitch_data_0

    .line 1952501
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported node type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1952502
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1952503
    :sswitch_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 1952504
    iget-object v0, p0, LX/CyD;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/CyD;->a(LX/2dj;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 1952505
    :goto_1
    return-object v0

    .line 1952506
    :sswitch_1
    iget-object v0, p0, LX/CyD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iV;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->cc()Z

    move-result v2

    invoke-static {v0, v1, v2}, LX/CyD;->a(LX/3iV;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 1952507
    goto :goto_1

    .line 1952508
    :sswitch_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/CyD;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 1952509
    goto :goto_1

    .line 1952510
    :sswitch_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->bx()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->kK()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->kW()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, LX/CyD;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 1952498
    iget-object v0, p0, LX/CyD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3mF;

    const-string v1, "search"

    invoke-virtual {v0, p1, v1}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 1952485
    invoke-static {p2}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v0

    .line 1952486
    if-eqz v0, :cond_1

    .line 1952487
    iget-object v0, p0, LX/CyD;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7vW;

    .line 1952488
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq p3, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p3, v1, :cond_3

    .line 1952489
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1952490
    :goto_0
    move-object v2, v1

    .line 1952491
    const-string v3, "unknown"

    const-string v4, "graph_search_results_page"

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->SEARCH_RESULT_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1952492
    :goto_1
    return-object v0

    .line 1952493
    :cond_1
    iget-object v0, p0, LX/CyD;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7vZ;

    .line 1952494
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq p4, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p4, v1, :cond_4

    .line 1952495
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1952496
    :goto_2
    move-object v2, v1

    .line 1952497
    const-string v3, "unknown"

    const-string v4, "graph_search_results_page"

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->SEARCH_RESULT_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_2
.end method
