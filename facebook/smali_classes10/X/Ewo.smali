.class public LX/Ewo;
.super LX/Eus;
.source ""

# interfaces
.implements LX/Ewe;


# direct methods
.method public constructor <init>(LX/Ewn;)V
    .locals 0

    .prologue
    .line 2183784
    invoke-direct {p0, p1}, LX/Eus;-><init>(LX/Euq;)V

    .line 2183785
    return-void
.end method


# virtual methods
.method public final lg_()LX/Ewp;
    .locals 2

    .prologue
    .line 2183786
    invoke-virtual {p0}, LX/Eus;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2183787
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2183788
    :cond_0
    sget-object v0, LX/Ewp;->PERSON_YOU_MAY_KNOW:LX/Ewp;

    .line 2183789
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/Ewp;->RESPONDED_PERSON_YOU_MAY_KNOW:LX/Ewp;

    goto :goto_0
.end method

.method public final lh_()Z
    .locals 1

    .prologue
    .line 2183790
    const/4 v0, 0x1

    return v0
.end method
