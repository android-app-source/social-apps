.class public LX/EIs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2102496
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 2102497
    check-cast p1, Ljava/lang/String;

    .line 2102498
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2102499
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "custom_voicemail_delete"

    .line 2102500
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2102501
    move-object v1, v1

    .line 2102502
    const-string v2, "DELETE"

    .line 2102503
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2102504
    move-object v1, v1

    .line 2102505
    iput-object p1, v1, LX/14O;->d:Ljava/lang/String;

    .line 2102506
    move-object v1, v1

    .line 2102507
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2102508
    move-object v0, v1

    .line 2102509
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2102510
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2102511
    move-object v0, v0

    .line 2102512
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2102513
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
