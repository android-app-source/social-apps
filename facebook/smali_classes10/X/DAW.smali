.class public LX/DAW;
.super LX/3OP;
.source ""


# instance fields
.field private a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z


# virtual methods
.method public final a(LX/3L9;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "ARG:",
            "Ljava/lang/Object;",
            ">(",
            "LX/3L9",
            "<TT;TARG;>;TARG;)TT;"
        }
    .end annotation

    .prologue
    .line 1971352
    invoke-interface {p1, p0, p2}, LX/3L9;->a(LX/DAW;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1971350
    iput-boolean p1, p0, LX/DAW;->a:Z

    .line 1971351
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1971349
    iget-boolean v0, p0, LX/DAW;->a:Z

    return v0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1971353
    iput-boolean p1, p0, LX/DAW;->b:Z

    .line 1971354
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1971333
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 1971334
    :cond_0
    :goto_0
    return v0

    .line 1971335
    :cond_1
    check-cast p1, LX/DAW;

    .line 1971336
    invoke-virtual {p1}, LX/3OP;->a()Z

    move-result v1

    invoke-virtual {p0}, LX/3OP;->a()Z

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1971337
    iget-boolean v1, p1, LX/DAW;->b:Z

    move v1, v1

    .line 1971338
    iget-boolean v2, p0, LX/DAW;->b:Z

    move v2, v2

    .line 1971339
    if-ne v1, v2, :cond_0

    .line 1971340
    iget-boolean v1, p1, LX/DAW;->c:Z

    move v1, v1

    .line 1971341
    iget-boolean v2, p0, LX/DAW;->c:Z

    move v2, v2

    .line 1971342
    if-ne v1, v2, :cond_0

    .line 1971343
    iget-boolean v1, p1, LX/DAW;->d:Z

    move v1, v1

    .line 1971344
    iget-boolean v2, p0, LX/DAW;->d:Z

    move v2, v2

    .line 1971345
    if-ne v1, v2, :cond_0

    .line 1971346
    iget-boolean v1, p1, LX/DAW;->e:Z

    move v1, v1

    .line 1971347
    iget-boolean v2, p0, LX/DAW;->e:Z

    move v2, v2

    .line 1971348
    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1971326
    iget-boolean v0, p0, LX/DAW;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    .line 1971327
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, LX/DAW;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1971328
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, LX/DAW;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1971329
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, LX/DAW;->d:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1971330
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, LX/DAW;->e:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1971331
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1971332
    const-string v0, "My Montage"

    return-object v0
.end method
