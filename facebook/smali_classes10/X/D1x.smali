.class public final LX/D1x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)V
    .locals 0

    .prologue
    .line 1957669
    iput-object p1, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4

    .prologue
    .line 1957633
    iget-object v0, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-static {v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->e(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v0

    .line 1957634
    if-nez v0, :cond_0

    .line 1957635
    :goto_0
    return-void

    .line 1957636
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->j()I

    move-result v1

    .line 1957637
    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->k()I

    move-result v0

    .line 1957638
    sub-int/2addr v0, v1

    mul-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x64

    add-int/2addr v0, v1

    .line 1957639
    iget-object v1, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v1, v1, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b:LX/D2B;

    iget-object v2, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v2, v2, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->s:LX/D1v;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v0, v2, v3}, LX/D2B;->a(ILX/D1v;F)V

    goto :goto_0
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 6

    .prologue
    .line 1957661
    iget-object v0, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->e:LX/D2o;

    if-eqz v0, :cond_0

    .line 1957662
    iget-object v0, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->e:LX/D2o;

    .line 1957663
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/D2o;->n:Z

    .line 1957664
    iget-object v2, v0, LX/D2o;->b:Landroid/view/View;

    iget-object v3, v0, LX/D2o;->o:Ljava/lang/Runnable;

    const-wide/16 v4, 0x96

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1957665
    iget-object v0, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    const/4 v1, 0x1

    .line 1957666
    iput-boolean v1, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->k:Z

    .line 1957667
    iget-object v0, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-static {v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->k(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)V

    .line 1957668
    :cond_0
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 10

    .prologue
    .line 1957640
    iget-object v0, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->e:LX/D2o;

    if-eqz v0, :cond_1

    .line 1957641
    iget-object v0, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->e:LX/D2o;

    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1957642
    iget-boolean v2, v0, LX/D2o;->n:Z

    if-eqz v2, :cond_2

    .line 1957643
    iget-object v2, v0, LX/D2o;->b:Landroid/view/View;

    iget-object v3, v0, LX/D2o;->o:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1957644
    :cond_0
    :goto_0
    iget-object v0, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    const/4 v1, 0x0

    .line 1957645
    iput-boolean v1, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->k:Z

    .line 1957646
    iget-object v0, p0, LX/D1x;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-static {v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->k(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)V

    .line 1957647
    :cond_1
    return-void

    .line 1957648
    :cond_2
    iget-object v2, v0, LX/D2o;->f:Landroid/animation/Animator;

    if-eqz v2, :cond_3

    .line 1957649
    iget-object v2, v0, LX/D2o;->f:Landroid/animation/Animator;

    invoke-virtual {v2}, Landroid/animation/Animator;->cancel()V

    .line 1957650
    :cond_3
    iget-object v2, v0, LX/D2o;->e:LX/D2l;

    .line 1957651
    iget-object v3, v2, LX/D2l;->a:Landroid/widget/ImageView;

    move-object v2, v3

    .line 1957652
    if-eqz v2, :cond_0

    .line 1957653
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1957654
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1957655
    iget-object v3, v0, LX/D2o;->d:Landroid/widget/ImageView;

    sget-object v4, Landroid/view/View;->X:Landroid/util/Property;

    new-array v5, v7, [F

    iget-object v6, v0, LX/D2o;->h:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    aput v6, v5, v8

    iget-object v6, v0, LX/D2o;->g:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    aput v6, v5, v9

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    iget-object v4, v0, LX/D2o;->d:Landroid/widget/ImageView;

    sget-object v5, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v6, v7, [F

    iget-object v7, v0, LX/D2o;->h:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    aput v7, v6, v8

    iget-object v7, v0, LX/D2o;->g:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    aput v7, v6, v9

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    iget-object v4, v0, LX/D2o;->d:Landroid/widget/ImageView;

    sget-object v5, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v6, v9, [F

    iget v7, v0, LX/D2o;->k:F

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    iget-object v4, v0, LX/D2o;->d:Landroid/widget/ImageView;

    sget-object v5, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v6, v9, [F

    iget v7, v0, LX/D2o;->k:F

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1957656
    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1957657
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1957658
    iget-object v3, v0, LX/D2o;->m:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1957659
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 1957660
    iput-object v2, v0, LX/D2o;->f:Landroid/animation/Animator;

    goto/16 :goto_0
.end method
