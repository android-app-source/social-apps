.class public final enum LX/Eno;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Eno;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Eno;

.field public static final enum INITIAL_ENTITIES_LOADED:LX/Eno;

.field public static final enum PRELIMINARY_INITIALIZED:LX/Eno;

.field public static final enum UNINITIALIZED:LX/Eno;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2167471
    new-instance v0, LX/Eno;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v2}, LX/Eno;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Eno;->UNINITIALIZED:LX/Eno;

    .line 2167472
    new-instance v0, LX/Eno;

    const-string v1, "PRELIMINARY_INITIALIZED"

    invoke-direct {v0, v1, v3}, LX/Eno;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Eno;->PRELIMINARY_INITIALIZED:LX/Eno;

    .line 2167473
    new-instance v0, LX/Eno;

    const-string v1, "INITIAL_ENTITIES_LOADED"

    invoke-direct {v0, v1, v4}, LX/Eno;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Eno;->INITIAL_ENTITIES_LOADED:LX/Eno;

    .line 2167474
    const/4 v0, 0x3

    new-array v0, v0, [LX/Eno;

    sget-object v1, LX/Eno;->UNINITIALIZED:LX/Eno;

    aput-object v1, v0, v2

    sget-object v1, LX/Eno;->PRELIMINARY_INITIALIZED:LX/Eno;

    aput-object v1, v0, v3

    sget-object v1, LX/Eno;->INITIAL_ENTITIES_LOADED:LX/Eno;

    aput-object v1, v0, v4

    sput-object v0, LX/Eno;->$VALUES:[LX/Eno;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2167476
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Eno;
    .locals 1

    .prologue
    .line 2167477
    const-class v0, LX/Eno;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Eno;

    return-object v0
.end method

.method public static values()[LX/Eno;
    .locals 1

    .prologue
    .line 2167475
    sget-object v0, LX/Eno;->$VALUES:[LX/Eno;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Eno;

    return-object v0
.end method
