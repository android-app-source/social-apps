.class public final LX/EpM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EpL;


# instance fields
.field public final synthetic a:LX/EpN;


# direct methods
.method public constructor <init>(LX/EpN;)V
    .locals 0

    .prologue
    .line 2170211
    iput-object p1, p0, LX/EpM;->a:LX/EpN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 7

    .prologue
    .line 2170212
    iget-object v0, p0, LX/EpM;->a:LX/EpN;

    iget-object v0, v0, LX/EpN;->e:LX/2do;

    new-instance v1, LX/2iE;

    invoke-direct {v1, p1, p2}, LX/2iE;-><init>(J)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2170213
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->REGULAR_FOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-object v1, p0

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, LX/EpM;->a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2170214
    iget-object v0, p0, LX/EpM;->a:LX/EpN;

    iget-object v0, v0, LX/EpN;->k:LX/EnL;

    sget-object v1, LX/Emm;->USER_BLOCKED:LX/Emm;

    invoke-virtual {v0, v1}, LX/EnL;->a(LX/Emm;)V

    .line 2170215
    return-void
.end method

.method public final a(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2170216
    iget-object v0, p0, LX/EpM;->a:LX/EpN;

    iget-object v0, v0, LX/EpN;->e:LX/2do;

    new-instance v1, LX/2f2;

    invoke-direct {v1, p1, p2, p3, v6}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2170217
    iget-object v0, p0, LX/EpM;->a:LX/EpN;

    iget-object v0, v0, LX/EpN;->e:LX/2do;

    new-instance v1, LX/84Z;

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v1 .. v6}, LX/84Z;-><init>(JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2170218
    return-void
.end method
