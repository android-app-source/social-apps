.class public LX/EbA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Eb9;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:LX/Ecs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ecs",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public final e:LX/Eat;

.field public final f:LX/Eae;

.field public final g:LX/EbD;

.field private final h:[B


# direct methods
.method public constructor <init>(IILX/Ecs;ILX/Eat;LX/Eae;LX/EbD;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "LX/Ecs",
            "<",
            "Ljava/lang/Integer;",
            ">;I",
            "Lorg/whispersystems/libsignal/ecc/ECPublicKey;",
            "LX/Eae;",
            "LX/EbD;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2142975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142976
    iput p1, p0, LX/EbA;->a:I

    .line 2142977
    iput p2, p0, LX/EbA;->b:I

    .line 2142978
    iput-object p3, p0, LX/EbA;->c:LX/Ecs;

    .line 2142979
    iput p4, p0, LX/EbA;->d:I

    .line 2142980
    iput-object p5, p0, LX/EbA;->e:LX/Eat;

    .line 2142981
    iput-object p6, p0, LX/EbA;->f:LX/Eae;

    .line 2142982
    iput-object p7, p0, LX/EbA;->g:LX/EbD;

    .line 2142983
    invoke-static {}, LX/EbH;->u()LX/EbH;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/EbH;->c(I)LX/EbH;

    move-result-object v0

    invoke-virtual {p5}, LX/Eat;->a()[B

    move-result-object v1

    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EbH;->a(LX/EWc;)LX/EbH;

    move-result-object v0

    invoke-virtual {p6}, LX/Eae;->b()[B

    move-result-object v1

    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EbH;->b(LX/EWc;)LX/EbH;

    move-result-object v0

    invoke-virtual {p7}, LX/EbD;->a()[B

    move-result-object v1

    invoke-static {v1}, LX/EWc;->a([B)LX/EWc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EbH;->c(LX/EWc;)LX/EbH;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/EbH;->a(I)LX/EbH;

    move-result-object v1

    .line 2142984
    invoke-virtual {p3}, LX/Ecs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2142985
    invoke-virtual {p3}, LX/Ecs;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, LX/EbH;->b(I)LX/EbH;

    .line 2142986
    :cond_0
    new-array v0, v5, [B

    iget v2, p0, LX/EbA;->a:I

    const/4 v3, 0x3

    invoke-static {v2, v3}, LX/Eco;->a(II)B

    move-result v2

    aput-byte v2, v0, v4

    .line 2142987
    invoke-virtual {v1}, LX/EbH;->l()LX/EbI;

    move-result-object v1

    invoke-virtual {v1}, LX/EWX;->jZ_()[B

    move-result-object v1

    .line 2142988
    const/4 v2, 0x2

    new-array v2, v2, [[B

    aput-object v0, v2, v4

    aput-object v1, v2, v5

    invoke-static {v2}, LX/Eco;->a([[B)[B

    move-result-object v0

    iput-object v0, p0, LX/EbA;->h:[B

    .line 2142989
    return-void
.end method

.method public constructor <init>([B)V
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 2142990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2142991
    const/4 v0, 0x0

    :try_start_0
    aget-byte v0, p1, v0

    invoke-static {v0}, LX/Eco;->a(B)I

    move-result v0

    iput v0, p0, LX/EbA;->a:I

    .line 2142992
    iget v0, p0, LX/EbA;->a:I

    if-le v0, v1, :cond_0

    .line 2142993
    new-instance v0, LX/Eaj;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/EbA;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eaj;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/Eag; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/Eak; {:try_start_0 .. :try_end_0} :catch_2

    .line 2142994
    :catch_0
    move-exception v0

    .line 2142995
    :goto_0
    new-instance v1, LX/Eai;

    invoke-direct {v1, v0}, LX/Eai;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2142996
    :cond_0
    :try_start_1
    iget v0, p0, LX/EbA;->a:I

    if-ge v0, v1, :cond_1

    .line 2142997
    new-instance v0, LX/Eak;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Legacy version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/EbA;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eak;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2142998
    :catch_1
    move-exception v0

    goto :goto_0

    .line 2142999
    :cond_1
    const/4 v0, 0x1

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v0, v1}, LX/EWc;->a([BII)LX/EWc;

    move-result-object v0

    .line 2143000
    sget-object v1, LX/EbI;->a:LX/EWZ;

    .line 2143001
    sget-object v2, LX/EWZ;->a:LX/EYZ;

    invoke-static {v1, v0, v2}, LX/EWZ;->d(LX/EWZ;LX/EWc;LX/EYZ;)LX/EWW;

    move-result-object v2

    move-object v1, v2

    .line 2143002
    check-cast v1, LX/EbI;

    move-object v1, v1

    .line 2143003
    invoke-virtual {v1}, LX/EbI;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, LX/EbI;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, LX/EbI;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, LX/EbI;->y()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2143004
    :cond_2
    new-instance v0, LX/Eai;

    const-string v1, "Incomplete message."

    invoke-direct {v0, v1}, LX/Eai;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2143005
    :catch_2
    move-exception v0

    goto :goto_0

    .line 2143006
    :cond_3
    iput-object p1, p0, LX/EbA;->h:[B

    .line 2143007
    iget v0, v1, LX/EbI;->registrationId_:I

    move v0, v0

    .line 2143008
    iput v0, p0, LX/EbA;->b:I

    .line 2143009
    invoke-virtual {v1}, LX/EbI;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2143010
    iget v0, v1, LX/EbI;->preKeyId_:I

    move v0, v0

    .line 2143011
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/Ecs;->a(Ljava/lang/Object;)LX/Ecs;

    move-result-object v0

    :goto_1
    iput-object v0, p0, LX/EbA;->c:LX/Ecs;

    .line 2143012
    invoke-virtual {v1}, LX/EbI;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2143013
    iget v0, v1, LX/EbI;->signedPreKeyId_:I

    move v0, v0

    .line 2143014
    :goto_2
    iput v0, p0, LX/EbA;->d:I

    .line 2143015
    iget-object v0, v1, LX/EbI;->baseKey_:LX/EWc;

    move-object v0, v0

    .line 2143016
    invoke-virtual {v0}, LX/EWc;->d()[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v0

    iput-object v0, p0, LX/EbA;->e:LX/Eat;

    .line 2143017
    new-instance v0, LX/Eae;

    .line 2143018
    iget-object v2, v1, LX/EbI;->identityKey_:LX/EWc;

    move-object v2, v2

    .line 2143019
    invoke-virtual {v2}, LX/EWc;->d()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/Ear;->a([BI)LX/Eat;

    move-result-object v2

    invoke-direct {v0, v2}, LX/Eae;-><init>(LX/Eat;)V

    iput-object v0, p0, LX/EbA;->f:LX/Eae;

    .line 2143020
    new-instance v0, LX/EbD;

    .line 2143021
    iget-object v2, v1, LX/EbI;->message_:LX/EWc;

    move-object v1, v2

    .line 2143022
    invoke-virtual {v1}, LX/EWc;->d()[B

    move-result-object v1

    invoke-direct {v0, v1}, LX/EbD;-><init>([B)V

    iput-object v0, p0, LX/EbA;->g:LX/EbD;

    .line 2143023
    return-void

    .line 2143024
    :cond_4
    sget-object v0, LX/Ect;->a:LX/Ect;

    move-object v0, v0
    :try_end_1
    .catch LX/EYr; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/Eag; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/Eak; {:try_start_1 .. :try_end_1} :catch_2

    .line 2143025
    goto :goto_1

    .line 2143026
    :cond_5
    const/4 v0, -0x1

    goto :goto_2
.end method


# virtual methods
.method public final a()[B
    .locals 1

    .prologue
    .line 2143027
    iget-object v0, p0, LX/EbA;->h:[B

    return-object v0
.end method
