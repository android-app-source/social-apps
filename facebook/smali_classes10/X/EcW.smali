.class public final LX/EcW;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EcM;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcW;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EcW;


# instance fields
.field public bitField0_:I

.field public chainKey_:LX/EcR;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public messageKeys_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EcV;",
            ">;"
        }
    .end annotation
.end field

.field public senderRatchetKeyPrivate_:LX/EWc;

.field public senderRatchetKey_:LX/EWc;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2147076
    new-instance v0, LX/EcL;

    invoke-direct {v0}, LX/EcL;-><init>()V

    sput-object v0, LX/EcW;->a:LX/EWZ;

    .line 2147077
    new-instance v0, LX/EcW;

    invoke-direct {v0}, LX/EcW;-><init>()V

    .line 2147078
    sput-object v0, LX/EcW;->c:LX/EcW;

    invoke-direct {v0}, LX/EcW;->z()V

    .line 2147079
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2147071
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147072
    iput-byte v0, p0, LX/EcW;->memoizedIsInitialized:B

    .line 2147073
    iput v0, p0, LX/EcW;->memoizedSerializedSize:I

    .line 2147074
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2147075
    iput-object v0, p0, LX/EcW;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/16 v7, 0x8

    .line 2147022
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2147023
    iput-byte v1, p0, LX/EcW;->memoizedIsInitialized:B

    .line 2147024
    iput v1, p0, LX/EcW;->memoizedSerializedSize:I

    .line 2147025
    invoke-direct {p0}, LX/EcW;->z()V

    .line 2147026
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 2147027
    :goto_0
    if-nez v3, :cond_2

    .line 2147028
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2147029
    sparse-switch v0, :sswitch_data_0

    .line 2147030
    invoke-virtual {p0, p1, v5, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_6

    move v3, v4

    .line 2147031
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 2147032
    goto :goto_0

    .line 2147033
    :sswitch_1
    iget v0, p0, LX/EcW;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EcW;->bitField0_:I

    .line 2147034
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EcW;->senderRatchetKey_:LX/EWc;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2147035
    :catch_0
    move-exception v0

    .line 2147036
    :goto_1
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147037
    move-object v0, v0

    .line 2147038
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2147039
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v7, :cond_0

    .line 2147040
    iget-object v1, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    .line 2147041
    :cond_0
    invoke-virtual {v5}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EcW;->unknownFields:LX/EZQ;

    .line 2147042
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2147043
    :sswitch_2
    :try_start_2
    iget v0, p0, LX/EcW;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EcW;->bitField0_:I

    .line 2147044
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EcW;->senderRatchetKeyPrivate_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2147045
    :catch_1
    move-exception v0

    .line 2147046
    :goto_3
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2147047
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2147048
    move-object v0, v2

    .line 2147049
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2147050
    :sswitch_3
    const/4 v0, 0x0

    .line 2147051
    :try_start_4
    iget v2, p0, LX/EcW;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v6, 0x4

    if-ne v2, v6, :cond_5

    .line 2147052
    iget-object v0, p0, LX/EcW;->chainKey_:LX/EcR;

    invoke-virtual {v0}, LX/EcR;->o()LX/EcQ;

    move-result-object v0

    move-object v2, v0

    .line 2147053
    :goto_4
    sget-object v0, LX/EcR;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EcR;

    iput-object v0, p0, LX/EcW;->chainKey_:LX/EcR;

    .line 2147054
    if-eqz v2, :cond_1

    .line 2147055
    iget-object v0, p0, LX/EcW;->chainKey_:LX/EcR;

    invoke-virtual {v2, v0}, LX/EcQ;->a(LX/EcR;)LX/EcQ;

    .line 2147056
    invoke-virtual {v2}, LX/EcQ;->m()LX/EcR;

    move-result-object v0

    iput-object v0, p0, LX/EcW;->chainKey_:LX/EcR;

    .line 2147057
    :cond_1
    iget v0, p0, LX/EcW;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EcW;->bitField0_:I

    goto/16 :goto_0

    .line 2147058
    :sswitch_4
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v7, :cond_4

    .line 2147059
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EcW;->messageKeys_:Ljava/util/List;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2147060
    or-int/lit8 v0, v1, 0x8

    .line 2147061
    :goto_5
    :try_start_5
    iget-object v1, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    sget-object v2, LX/EcV;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch LX/EYr; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_6
    move v1, v0

    .line 2147062
    goto/16 :goto_0

    .line 2147063
    :cond_2
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v7, :cond_3

    .line 2147064
    iget-object v0, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    .line 2147065
    :cond_3
    invoke-virtual {v5}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcW;->unknownFields:LX/EZQ;

    .line 2147066
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2147067
    return-void

    .line 2147068
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_2

    .line 2147069
    :catch_2
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_3

    .line 2147070
    :catch_3
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto :goto_5

    :cond_5
    move-object v2, v0

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2147017
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2147018
    iput-byte v1, p0, LX/EcW;->memoizedIsInitialized:B

    .line 2147019
    iput v1, p0, LX/EcW;->memoizedSerializedSize:I

    .line 2147020
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcW;->unknownFields:LX/EZQ;

    .line 2147021
    return-void
.end method

.method public static a(LX/EcW;)LX/EcN;
    .locals 1

    .prologue
    .line 2147016
    invoke-static {}, LX/EcN;->y()LX/EcN;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EcN;->a(LX/EcW;)LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method private z()V
    .locals 1

    .prologue
    .line 2147010
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcW;->senderRatchetKey_:LX/EWc;

    .line 2147011
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcW;->senderRatchetKeyPrivate_:LX/EWc;

    .line 2147012
    sget-object v0, LX/EcR;->c:LX/EcR;

    move-object v0, v0

    .line 2147013
    iput-object v0, p0, LX/EcW;->chainKey_:LX/EcR;

    .line 2147014
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    .line 2147015
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2147008
    new-instance v0, LX/EcN;

    invoke-direct {v0, p1}, LX/EcN;-><init>(LX/EYd;)V

    .line 2147009
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2146996
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2146997
    iget v0, p0, LX/EcW;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2146998
    iget-object v0, p0, LX/EcW;->senderRatchetKey_:LX/EWc;

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2146999
    :cond_0
    iget v0, p0, LX/EcW;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2147000
    iget-object v0, p0, LX/EcW;->senderRatchetKeyPrivate_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2147001
    :cond_1
    iget v0, p0, LX/EcW;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2147002
    const/4 v0, 0x3

    iget-object v1, p0, LX/EcW;->chainKey_:LX/EcR;

    invoke-virtual {p1, v0, v1}, LX/EWf;->b(ILX/EWW;)V

    .line 2147003
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2147004
    iget-object v0, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2147005
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2147006
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2147007
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2147080
    iget-byte v1, p0, LX/EcW;->memoizedIsInitialized:B

    .line 2147081
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2147082
    :goto_0
    return v0

    .line 2147083
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2147084
    :cond_1
    iput-byte v0, p0, LX/EcW;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2146974
    iget v0, p0, LX/EcW;->memoizedSerializedSize:I

    .line 2146975
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2146976
    :goto_0
    return v0

    .line 2146977
    :cond_0
    iget v0, p0, LX/EcW;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 2146978
    iget-object v0, p0, LX/EcW;->senderRatchetKey_:LX/EWc;

    invoke-static {v3, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2146979
    :goto_1
    iget v2, p0, LX/EcW;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2146980
    iget-object v2, p0, LX/EcW;->senderRatchetKeyPrivate_:LX/EWc;

    invoke-static {v4, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2146981
    :cond_1
    iget v2, p0, LX/EcW;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_2

    .line 2146982
    const/4 v2, 0x3

    iget-object v3, p0, LX/EcW;->chainKey_:LX/EcR;

    invoke-static {v2, v3}, LX/EWf;->e(ILX/EWW;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    move v2, v0

    .line 2146983
    :goto_2
    iget-object v0, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2146984
    iget-object v0, p0, LX/EcW;->messageKeys_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v5, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2146985
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2146986
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2146987
    iput v0, p0, LX/EcW;->memoizedSerializedSize:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2146989
    iget-object v0, p0, LX/EcW;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2146990
    sget-object v0, LX/Eck;->d:LX/EYn;

    const-class v1, LX/EcW;

    const-class v2, LX/EcN;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2146991
    sget-object v0, LX/EcW;->a:LX/EWZ;

    return-object v0
.end method

.method public final r()LX/EcN;
    .locals 1

    .prologue
    .line 2146992
    invoke-static {p0}, LX/EcW;->a(LX/EcW;)LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2146988
    invoke-virtual {p0}, LX/EcW;->r()LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2146993
    invoke-static {}, LX/EcN;->y()LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2146994
    invoke-virtual {p0}, LX/EcW;->r()LX/EcN;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2146995
    sget-object v0, LX/EcW;->c:LX/EcW;

    return-object v0
.end method
