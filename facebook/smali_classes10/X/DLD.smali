.class public final LX/DLD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DLJ;


# direct methods
.method public constructor <init>(LX/DLJ;)V
    .locals 0

    .prologue
    .line 1987983
    iput-object p1, p0, LX/DLD;->a:LX/DLJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x29067d81

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1987984
    iget-object v1, p0, LX/DLD;->a:LX/DLJ;

    iget-object v1, v1, LX/DLJ;->w:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    if-eqz v1, :cond_1

    .line 1987985
    sget-object v1, LX/DLG;->a:[I

    iget-object v2, p0, LX/DLD;->a:LX/DLJ;

    iget-object v2, v2, LX/DLJ;->t:LX/DLI;

    invoke-virtual {v2}, LX/DLI;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1987986
    iget-object v1, p0, LX/DLD;->a:LX/DLJ;

    iget-object v1, v1, LX/DLJ;->w:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object v2, p0, LX/DLD;->a:LX/DLJ;

    iget-object v2, v2, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    .line 1987987
    iget-object v3, v1, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->r:LX/DL8;

    .line 1987988
    iget-object v5, v3, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/DL6;

    .line 1987989
    if-eqz v5, :cond_0

    iget-object p0, v5, LX/DL6;->d:LX/7z0;

    if-nez p0, :cond_3

    iget-object p0, v5, LX/DL6;->b:LX/DL5;

    if-nez p0, :cond_3

    .line 1987990
    :cond_0
    :goto_0
    iget-object v3, v1, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    invoke-virtual {v3, v2, v4}, LX/DKt;->a(Ljava/lang/String;Z)V

    .line 1987991
    invoke-static {v1}, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->d(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1987992
    invoke-static {v1, v4}, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->a(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;Z)V

    .line 1987993
    :cond_1
    :goto_1
    const v1, 0x64070adb

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1987994
    :pswitch_0
    iget-object v1, p0, LX/DLD;->a:LX/DLJ;

    iget-object v1, v1, LX/DLJ;->w:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object v2, p0, LX/DLD;->a:LX/DLJ;

    iget-object v2, v2, LX/DLJ;->C:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/DLD;->a:LX/DLJ;

    iget v3, v3, LX/DLJ;->u:I

    .line 1987995
    iget-object v4, v1, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    sget-object v5, LX/DLI;->NORMAL:LX/DLI;

    invoke-virtual {v4, v3, v5}, LX/DKt;->a(ILX/DLI;)V

    .line 1987996
    iget-object v4, v1, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->q:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    if-eqz v4, :cond_2

    .line 1987997
    iget-object v4, v1, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->q:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    .line 1987998
    iget-object v5, v4, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->h:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "DOWNLOAD_GROUP_FILES_"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1987999
    :cond_2
    goto :goto_1

    .line 1988000
    :cond_3
    iget-object p0, v5, LX/DL6;->d:LX/7z0;

    if-eqz p0, :cond_4

    .line 1988001
    iget-object p0, v3, LX/DL8;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/7zS;

    .line 1988002
    iget-object p1, p0, LX/7zS;->a:LX/7z2;

    move-object p0, p1

    .line 1988003
    iget-object v5, v5, LX/DL6;->d:LX/7z0;

    invoke-virtual {p0, v5}, LX/7z2;->b(LX/7z0;)V

    .line 1988004
    :cond_4
    iget-object v5, v3, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
