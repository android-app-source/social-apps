.class public LX/EGA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/EGA;


# instance fields
.field private final a:LX/3Lf;


# direct methods
.method public constructor <init>(LX/3Lf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2096714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096715
    iput-object p1, p0, LX/EGA;->a:LX/3Lf;

    .line 2096716
    return-void
.end method

.method public static a(LX/0QB;)LX/EGA;
    .locals 4

    .prologue
    .line 2096717
    sget-object v0, LX/EGA;->b:LX/EGA;

    if-nez v0, :cond_1

    .line 2096718
    const-class v1, LX/EGA;

    monitor-enter v1

    .line 2096719
    :try_start_0
    sget-object v0, LX/EGA;->b:LX/EGA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2096720
    if-eqz v2, :cond_0

    .line 2096721
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2096722
    new-instance p0, LX/EGA;

    invoke-static {v0}, LX/3Lf;->a(LX/0QB;)LX/3Lf;

    move-result-object v3

    check-cast v3, LX/3Lf;

    invoke-direct {p0, v3}, LX/EGA;-><init>(LX/3Lf;)V

    .line 2096723
    move-object v0, p0

    .line 2096724
    sput-object v0, LX/EGA;->b:LX/EGA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2096725
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2096726
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2096727
    :cond_1
    sget-object v0, LX/EGA;->b:LX/EGA;

    return-object v0

    .line 2096728
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2096729
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2096730
    iget-object v0, p0, LX/EGA;->a:LX/3Lf;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 2096731
    return-void
.end method
