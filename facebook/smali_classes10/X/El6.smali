.class public LX/El6;
.super LX/El5;
.source ""

# interfaces
.implements LX/El0;


# instance fields
.field public final a:Lorg/apache/http/client/methods/HttpUriRequest;

.field public final b:LX/El1;

.field private final c:LX/ElC;

.field public final d:LX/ElB;

.field public final e:LX/15D;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/15D",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/El4;)V
    .locals 3

    .prologue
    .line 2164026
    invoke-direct {p0}, LX/El5;-><init>()V

    .line 2164027
    iget-object v0, p1, LX/El4;->b:Lorg/apache/http/message/HeaderGroup;

    invoke-virtual {v0}, Lorg/apache/http/message/HeaderGroup;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    .line 2164028
    invoke-static {p1}, LX/El6;->a(LX/El4;)Lorg/apache/http/client/methods/HttpRequestBase;

    move-result-object v1

    .line 2164029
    iget-object v2, p1, LX/El4;->e:Ljava/lang/String;

    invoke-static {v2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 2164030
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeaders([Lorg/apache/http/Header;)V

    .line 2164031
    invoke-virtual {p0, v1}, LX/El5;->a(Lorg/apache/http/HttpMessage;)V

    .line 2164032
    iput-object v1, p0, LX/El6;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2164033
    iget-object v0, p1, LX/El4;->d:LX/El1;

    iput-object v0, p0, LX/El6;->b:LX/El1;

    .line 2164034
    new-instance v0, LX/ElC;

    invoke-direct {v0}, LX/ElC;-><init>()V

    iput-object v0, p0, LX/El6;->c:LX/ElC;

    .line 2164035
    new-instance v0, LX/ElB;

    invoke-direct {v0}, LX/ElB;-><init>()V

    iput-object v0, p0, LX/El6;->d:LX/ElB;

    .line 2164036
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v0

    iget-object v1, p1, LX/El4;->a:Ljava/lang/String;

    .line 2164037
    iput-object v1, v0, LX/15E;->c:Ljava/lang/String;

    .line 2164038
    move-object v0, v0

    .line 2164039
    iget-object v1, p0, LX/El6;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2164040
    iput-object v1, v0, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2164041
    move-object v0, v0

    .line 2164042
    iget-object v1, p0, LX/El6;->d:LX/ElB;

    .line 2164043
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2164044
    move-object v0, v0

    .line 2164045
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    iput-object v0, p0, LX/El6;->e:LX/15D;

    .line 2164046
    return-void
.end method

.method private static a(LX/El4;)Lorg/apache/http/client/methods/HttpRequestBase;
    .locals 5

    .prologue
    .line 2164047
    iget-object v0, p0, LX/El4;->c:Ljava/lang/String;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2164048
    iget-object v0, p0, LX/El4;->d:LX/El1;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0nE;->a(Z)V

    .line 2164049
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 2164050
    :goto_1
    return-object v0

    .line 2164051
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2164052
    :cond_1
    iget-object v0, p0, LX/El4;->c:Ljava/lang/String;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2164053
    iget-object v0, p0, LX/El4;->d:LX/El1;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2164054
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0}, Lorg/apache/http/client/methods/HttpPost;-><init>()V

    .line 2164055
    new-instance v1, LX/El2;

    iget-object v2, p0, LX/El4;->d:LX/El1;

    iget-object v3, p0, LX/El4;->b:Lorg/apache/http/message/HeaderGroup;

    const-string v4, "Content-Encoding"

    invoke-virtual {v3, v4}, Lorg/apache/http/message/HeaderGroup;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/El2;-><init>(LX/El1;Lorg/apache/http/Header;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto :goto_1

    .line 2164056
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "method="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/El4;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
