.class public final LX/EAl;
.super LX/ChG;
.source ""


# instance fields
.field public final synthetic a:LX/E9Y;

.field public final synthetic b:LX/E9T;

.field public final synthetic c:LX/EAs;


# direct methods
.method public constructor <init>(LX/EAs;LX/E9Y;LX/E9T;)V
    .locals 0

    .prologue
    .line 2085879
    iput-object p1, p0, LX/EAl;->c:LX/EAs;

    iput-object p2, p0, LX/EAl;->a:LX/E9Y;

    iput-object p3, p0, LX/EAl;->b:LX/E9T;

    invoke-direct {p0}, LX/ChG;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/5tj;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 3
    .param p2    # LX/5tj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085880
    if-nez p2, :cond_0

    .line 2085881
    iget-object v0, p0, LX/EAl;->a:LX/E9Y;

    .line 2085882
    iget-object v1, v0, LX/E9Y;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2085883
    iget-object v1, v0, LX/E9Y;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    .line 2085884
    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v2

    .line 2085885
    iget-object p2, v0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p2

    .line 2085886
    iget-object p3, v0, LX/E9Y;->c:Ljava/util/List;

    invoke-interface {p3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2085887
    iget-object v1, v0, LX/E9Y;->g:Ljava/util/Map;

    invoke-static {v2}, LX/BNJ;->b(LX/5tj;)Ljava/lang/String;

    move-result-object p3

    invoke-interface {v1, p3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2085888
    iget-object v1, v0, LX/E9Y;->e:Ljava/util/Map;

    invoke-static {v2}, LX/BNJ;->c(LX/5tj;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2085889
    invoke-static {v0, p2}, LX/E9Y;->f(LX/E9Y;I)V

    .line 2085890
    :cond_0
    iget-object v0, p0, LX/EAl;->b:LX/E9T;

    invoke-interface {v0}, LX/E9T;->f()V

    .line 2085891
    return-void
.end method
