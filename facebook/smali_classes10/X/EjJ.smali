.class public LX/EjJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final h:Ljava/lang/Object;


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/30Z;

.field private final d:LX/1wU;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/1Ml;

.field private g:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2161176
    const-class v0, LX/EjJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/EjJ;->a:Ljava/lang/String;

    .line 2161177
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/EjJ;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/30Z;LX/1wU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Ml;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2161107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2161108
    const/4 v0, 0x0

    iput v0, p0, LX/EjJ;->g:I

    .line 2161109
    iput-object p1, p0, LX/EjJ;->b:LX/0SG;

    .line 2161110
    iput-object p2, p0, LX/EjJ;->c:LX/30Z;

    .line 2161111
    iput-object p3, p0, LX/EjJ;->d:LX/1wU;

    .line 2161112
    iput-object p4, p0, LX/EjJ;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2161113
    iput-object p5, p0, LX/EjJ;->f:LX/1Ml;

    .line 2161114
    return-void
.end method

.method public static a(LX/0QB;)LX/EjJ;
    .locals 13

    .prologue
    .line 2161147
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2161148
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2161149
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2161150
    if-nez v1, :cond_0

    .line 2161151
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2161152
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2161153
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2161154
    sget-object v1, LX/EjJ;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2161155
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2161156
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2161157
    :cond_1
    if-nez v1, :cond_4

    .line 2161158
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2161159
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2161160
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2161161
    new-instance v7, LX/EjJ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/30Z;->a(LX/0QB;)LX/30Z;

    move-result-object v9

    check-cast v9, LX/30Z;

    invoke-static {v0}, LX/1wU;->b(LX/0QB;)LX/1wU;

    move-result-object v10

    check-cast v10, LX/1wU;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v12

    check-cast v12, LX/1Ml;

    invoke-direct/range {v7 .. v12}, LX/EjJ;-><init>(LX/0SG;LX/30Z;LX/1wU;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1Ml;)V

    .line 2161162
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2161163
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2161164
    if-nez v1, :cond_2

    .line 2161165
    sget-object v0, LX/EjJ;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjJ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2161166
    :goto_1
    if-eqz v0, :cond_3

    .line 2161167
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2161168
    :goto_3
    check-cast v0, LX/EjJ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2161169
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2161170
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2161171
    :catchall_1
    move-exception v0

    .line 2161172
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2161173
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2161174
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2161175
    :cond_2
    :try_start_8
    sget-object v0, LX/EjJ;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EjJ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2161115
    invoke-virtual {p1}, LX/2Jy;->a()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 2161116
    :cond_0
    :goto_0
    return v0

    .line 2161117
    :cond_1
    iget-object v0, p0, LX/EjJ;->f:LX/1Ml;

    const-string v1, "android.permission.READ_CONTACTS"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 2161118
    goto :goto_0

    .line 2161119
    :cond_2
    iget-object v0, p0, LX/EjJ;->d:LX/1wU;

    invoke-virtual {v0}, LX/1wU;->a()Z

    move-result v1

    .line 2161120
    if-eqz v1, :cond_3

    .line 2161121
    iget-object v0, p0, LX/EjJ;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v4, LX/2vf;->e:LX/0Tn;

    invoke-interface {v0, v4}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2161122
    :cond_3
    iget-object v0, p0, LX/EjJ;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/2vf;->e:LX/0Tn;

    invoke-interface {v0, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2161123
    iget-object v4, p0, LX/EjJ;->d:LX/1wU;

    invoke-virtual {v4}, LX/1wU;->b()LX/03R;

    move-result-object v4

    .line 2161124
    sget-object v5, LX/03R;->UNSET:LX/03R;

    if-ne v4, v5, :cond_4

    .line 2161125
    if-nez v0, :cond_0

    .line 2161126
    iget-object v1, p0, LX/EjJ;->c:LX/30Z;

    const-string v2, "default"

    const-string v3, "ccu_background_ping"

    invoke-virtual {v1, v2, v3}, LX/30Z;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2161127
    :cond_4
    sget-object v5, LX/03R;->NO:LX/03R;

    if-ne v4, v5, :cond_6

    .line 2161128
    if-nez v1, :cond_5

    if-nez v0, :cond_0

    .line 2161129
    :cond_5
    iget-object v2, p0, LX/EjJ;->c:LX/30Z;

    const-string v3, "off"

    const-string v4, "ccu_background_ping"

    invoke-virtual {v2, v3, v4, v1}, LX/30Z;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2161130
    :cond_6
    if-nez v1, :cond_7

    if-nez v0, :cond_8

    .line 2161131
    :cond_7
    iget-object v0, p0, LX/EjJ;->c:LX/30Z;

    const-string v4, "on"

    const-string v5, "ccu_background_ping"

    invoke-virtual {v0, v4, v5, v1}, LX/30Z;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2161132
    :cond_8
    iget-object v0, p0, LX/EjJ;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0uQ;->d:LX/0Tn;

    invoke-interface {v0, v1, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 2161133
    cmp-long v4, v0, v6

    if-nez v4, :cond_9

    iget-object v0, p0, LX/EjJ;->c:LX/30Z;

    .line 2161134
    iget-object v1, v0, LX/30Z;->t:LX/35c;

    move-object v0, v1

    .line 2161135
    iget-wide v0, v0, LX/35c;->i:J

    .line 2161136
    :cond_9
    iget-object v4, p0, LX/EjJ;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2vf;->a:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 2161137
    iget-object v6, p0, LX/EjJ;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    cmp-long v0, v4, v0

    if-gez v0, :cond_a

    .line 2161138
    iput v2, p0, LX/EjJ;->g:I

    move v0, v3

    .line 2161139
    goto/16 :goto_0

    .line 2161140
    :cond_a
    iget-object v0, p0, LX/EjJ;->c:LX/30Z;

    const-string v1, "FB_CCU_BACKGROUND_PING"

    iget v4, p0, LX/EjJ;->g:I

    invoke-virtual {v0, v1, v2, v4}, LX/30Z;->a(Ljava/lang/String;ZI)Z

    .line 2161141
    iget v0, p0, LX/EjJ;->g:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/EjJ;->g:I

    iget-object v1, p0, LX/EjJ;->c:LX/30Z;

    .line 2161142
    iget-object v4, v1, LX/30Z;->t:LX/35c;

    move-object v1, v4

    .line 2161143
    iget v1, v1, LX/35c;->f:I

    if-lt v0, v1, :cond_b

    move v0, v3

    .line 2161144
    :goto_1
    if-eqz v0, :cond_0

    .line 2161145
    iput v2, p0, LX/EjJ;->g:I

    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 2161146
    goto :goto_1
.end method
