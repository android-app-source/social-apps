.class public final LX/ENK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:LX/1Pn;

.field public final synthetic b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

.field public final synthetic c:Ljava/util/List;

.field public final synthetic d:LX/697;

.field public final synthetic e:Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;LX/1Pn;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Ljava/util/List;LX/697;)V
    .locals 0

    .prologue
    .line 2112432
    iput-object p1, p0, LX/ENK;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    iput-object p2, p0, LX/ENK;->a:LX/1Pn;

    iput-object p3, p0, LX/ENK;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    iput-object p4, p0, LX/ENK;->c:Ljava/util/List;

    iput-object p5, p0, LX/ENK;->d:LX/697;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    .line 2112433
    const v0, 0x7f020e90

    invoke-static {v0}, LX/690;->a(I)LX/68w;

    move-result-object v3

    .line 2112434
    invoke-virtual {p1}, LX/6al;->a()V

    .line 2112435
    iget-object v0, p0, LX/ENK;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v4, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, LX/6al;->a(Z)V

    .line 2112436
    new-instance v0, LX/ENI;

    invoke-direct {v0, p0}, LX/ENI;-><init>(LX/ENK;)V

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6ak;)V

    .line 2112437
    new-instance v0, LX/ENJ;

    invoke-direct {v0, p0}, LX/ENJ;-><init>(LX/ENK;)V

    invoke-virtual {p1, v0}, LX/6al;->a(LX/6Zu;)V

    .line 2112438
    invoke-virtual {p1}, LX/6al;->c()LX/6az;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/6az;->c(Z)V

    .line 2112439
    invoke-virtual {p1}, LX/6al;->c()LX/6az;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/6az;->b(Z)V

    .line 2112440
    invoke-virtual {p1}, LX/6al;->c()LX/6az;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/6az;->a(Z)V

    .line 2112441
    iget-object v0, p0, LX/ENK;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    .line 2112442
    new-instance v5, LX/699;

    invoke-direct {v5}, LX/699;-><init>()V

    .line 2112443
    iput-object v0, v5, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2112444
    move-object v0, v5

    .line 2112445
    iput-object v3, v0, LX/699;->c:LX/68w;

    .line 2112446
    move-object v0, v0

    .line 2112447
    invoke-virtual {v0, v6, v6}, LX/699;->a(FF)LX/699;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/6al;->a(LX/699;)LX/6ax;

    goto :goto_1

    :cond_0
    move v0, v2

    .line 2112448
    goto :goto_0

    .line 2112449
    :cond_1
    iget-object v0, p0, LX/ENK;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 2112450
    iget-object v0, p0, LX/ENK;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    const/high16 v1, 0x41300000    # 11.0f

    invoke-static {v0, v1}, LX/692;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/692;

    move-result-object v0

    invoke-static {v0}, LX/6aN;->a(LX/692;)LX/6aM;

    move-result-object v0

    .line 2112451
    :goto_2
    invoke-virtual {p1, v0}, LX/6al;->a(LX/6aM;)V

    .line 2112452
    return-void

    .line 2112453
    :cond_2
    iget-object v0, p0, LX/ENK;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f0b1718

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget-object v0, p0, LX/ENK;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v2, 0x7f0b1717

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 2112454
    iget-object v1, p0, LX/ENK;->d:LX/697;

    invoke-static {v1, v0}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v0

    goto :goto_2
.end method
