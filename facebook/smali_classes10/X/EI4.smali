.class public final enum LX/EI4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EI4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EI4;

.field public static final enum BLUE:LX/EI4;

.field public static final enum DARK:LX/EI4;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2100551
    new-instance v0, LX/EI4;

    const-string v1, "DARK"

    invoke-direct {v0, v1, v2}, LX/EI4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI4;->DARK:LX/EI4;

    .line 2100552
    new-instance v0, LX/EI4;

    const-string v1, "BLUE"

    invoke-direct {v0, v1, v3}, LX/EI4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EI4;->BLUE:LX/EI4;

    .line 2100553
    const/4 v0, 0x2

    new-array v0, v0, [LX/EI4;

    sget-object v1, LX/EI4;->DARK:LX/EI4;

    aput-object v1, v0, v2

    sget-object v1, LX/EI4;->BLUE:LX/EI4;

    aput-object v1, v0, v3

    sput-object v0, LX/EI4;->$VALUES:[LX/EI4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2100548
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EI4;
    .locals 1

    .prologue
    .line 2100550
    const-class v0, LX/EI4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EI4;

    return-object v0
.end method

.method public static values()[LX/EI4;
    .locals 1

    .prologue
    .line 2100549
    sget-object v0, LX/EI4;->$VALUES:[LX/EI4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EI4;

    return-object v0
.end method
