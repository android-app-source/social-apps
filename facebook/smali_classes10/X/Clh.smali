.class public LX/Clh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Chi;

.field private final b:LX/Crz;


# direct methods
.method public constructor <init>(LX/Chi;LX/Crz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1932729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932730
    iput-object p1, p0, LX/Clh;->a:LX/Chi;

    .line 1932731
    iput-object p2, p0, LX/Clh;->b:LX/Crz;

    .line 1932732
    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1932898
    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1932899
    const-string v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1932900
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1932901
    :cond_0
    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 1932902
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Clh;
    .locals 1

    .prologue
    .line 1932897
    invoke-static {p0}, LX/Clh;->b(LX/0QB;)LX/Clh;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;Landroid/content/Context;)LX/Cmn;
    .locals 3

    .prologue
    .line 1932894
    if-nez p0, :cond_0

    .line 1932895
    sget-object v0, LX/Cmn;->a:LX/Cmn;

    .line 1932896
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Cmn;

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;->b()I

    move-result v1

    int-to-float v1, v1

    invoke-static {p1, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/Clh;->a(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/Cmn;-><init>(II)V

    goto :goto_0
.end method

.method public static final a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Landroid/content/Context;)LX/Cmo;
    .locals 5

    .prologue
    .line 1932885
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel$BorderModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1932886
    :cond_0
    const/4 v0, 0x0

    .line 1932887
    :goto_0
    return-object v0

    .line 1932888
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel$BorderModel;

    move-result-object v0

    .line 1932889
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel$BorderModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;

    move-result-object v1

    invoke-static {v1, p1}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;Landroid/content/Context;)LX/Cmn;

    move-result-object v1

    .line 1932890
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel$BorderModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;

    move-result-object v2

    invoke-static {v2, p1}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;Landroid/content/Context;)LX/Cmn;

    move-result-object v2

    .line 1932891
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel$BorderModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;

    move-result-object v3

    invoke-static {v3, p1}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;Landroid/content/Context;)LX/Cmn;

    move-result-object v3

    .line 1932892
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel$BorderModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;

    move-result-object v0

    invoke-static {v0, p1}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextBorderModel;Landroid/content/Context;)LX/Cmn;

    move-result-object v4

    .line 1932893
    new-instance v0, LX/Cmo;

    invoke-direct {v0, v1, v2, v3, v4}, LX/Cmo;-><init>(LX/Cmn;LX/Cmn;LX/Cmn;LX/Cmn;)V

    goto :goto_0
.end method

.method public static a(LX/Clh;)LX/Cmr;
    .locals 1

    .prologue
    .line 1932884
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Clh;->b:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/Cmr;->RIGHT:LX/Cmr;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Cmr;->LEFT:LX/Cmr;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;)LX/Cmv;
    .locals 4

    .prologue
    .line 1932881
    if-nez p0, :cond_0

    .line 1932882
    sget-object v0, LX/Cmv;->b:LX/Cmv;

    .line 1932883
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;->a()Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;->b()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-static {v0, v1}, LX/Cmv;->a(Lcom/facebook/graphql/enums/GraphQLTextBlockMarginUnit;F)LX/Cmv;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;)LX/Cmw;
    .locals 5

    .prologue
    .line 1932876
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;

    move-result-object v0

    invoke-static {v0}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;)LX/Cmv;

    move-result-object v0

    .line 1932877
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;

    move-result-object v1

    invoke-static {v1}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;)LX/Cmv;

    move-result-object v1

    .line 1932878
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;

    move-result-object v2

    invoke-static {v2}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;)LX/Cmv;

    move-result-object v2

    .line 1932879
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;

    move-result-object v3

    invoke-static {v3}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSideSpacingModel;)LX/Cmv;

    move-result-object v3

    .line 1932880
    new-instance v4, LX/Cmw;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    return-object v4
.end method

.method public static a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;LX/ClT;LX/ClS;)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;
    .locals 3

    .prologue
    .line 1932855
    const/4 v0, 0x0

    .line 1932856
    if-eqz p0, :cond_0

    .line 1932857
    sget-object v1, LX/Clg;->b:[I

    invoke-virtual {p1}, LX/ClT;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1932858
    :cond_0
    :goto_0
    return-object v0

    .line 1932859
    :pswitch_0
    sget-object v1, LX/ClS;->REGULAR:LX/ClS;

    if-ne p2, v1, :cond_1

    .line 1932860
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932861
    :cond_1
    sget-object v1, LX/ClS;->MEDIUM:LX/ClS;

    if-ne p2, v1, :cond_2

    .line 1932862
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932863
    :cond_2
    sget-object v1, LX/ClS;->LARGE:LX/ClS;

    if-ne p2, v1, :cond_3

    .line 1932864
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932865
    :cond_3
    sget-object v1, LX/ClS;->EXTRA_LARGE:LX/ClS;

    if-ne p2, v1, :cond_0

    .line 1932866
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932867
    :pswitch_1
    sget-object v1, LX/ClS;->REGULAR:LX/ClS;

    if-ne p2, v1, :cond_4

    .line 1932868
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932869
    :cond_4
    sget-object v1, LX/ClS;->MEDIUM:LX/ClS;

    if-ne p2, v1, :cond_5

    .line 1932870
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932871
    :cond_5
    sget-object v1, LX/ClS;->LARGE:LX/ClS;

    if-ne p2, v1, :cond_6

    .line 1932872
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->ep_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932873
    :cond_6
    sget-object v1, LX/ClS;->EXTRA_LARGE:LX/ClS;

    if-ne p2, v1, :cond_0

    .line 1932874
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->eq_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932875
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/graphics/Canvas;Landroid/view/View;LX/Cmo;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1932845
    if-eqz p0, :cond_0

    if-nez p2, :cond_1

    .line 1932846
    :cond_0
    :goto_0
    return-void

    .line 1932847
    :cond_1
    iget-object v0, p2, LX/Cmo;->a:LX/Cmn;

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    .line 1932848
    iget-object v0, p2, LX/Cmo;->a:LX/Cmn;

    iget v0, v0, LX/Cmn;->b:I

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p0

    move v2, v1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1932849
    :cond_2
    iget-object v0, p2, LX/Cmo;->c:LX/Cmn;

    if-eqz v0, :cond_3

    if-eqz p5, :cond_3

    .line 1932850
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v2, p2, LX/Cmo;->c:LX/Cmn;

    iget v2, v2, LX/Cmn;->b:I

    sub-int/2addr v0, v2

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v5, v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v6, v0

    move-object v2, p0

    move v4, v1

    move-object v7, p5

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1932851
    :cond_3
    iget-object v0, p2, LX/Cmo;->b:LX/Cmn;

    if-eqz v0, :cond_4

    if-eqz p4, :cond_4

    .line 1932852
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v0, p2, LX/Cmo;->b:LX/Cmn;

    iget v0, v0, LX/Cmn;->b:I

    int-to-float v4, v0

    move-object v0, p0

    move v2, v1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1932853
    :cond_4
    iget-object v0, p2, LX/Cmo;->d:LX/Cmn;

    if-eqz v0, :cond_0

    if-eqz p6, :cond_0

    .line 1932854
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v2, p2, LX/Cmo;->d:LX/Cmn;

    iget v2, v2, LX/Cmn;->b:I

    sub-int/2addr v0, v2

    int-to-float v2, v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, p0

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Clh;
    .locals 3

    .prologue
    .line 1932843
    new-instance v2, LX/Clh;

    invoke-static {p0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v0

    check-cast v0, LX/Chi;

    invoke-static {p0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v1

    check-cast v1, LX/Crz;

    invoke-direct {v2, v0, v1}, LX/Clh;-><init>(LX/Chi;LX/Crz;)V

    .line 1932844
    return-object v2
.end method

.method public static c(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)LX/Cmp;
    .locals 2

    .prologue
    .line 1932836
    if-nez p0, :cond_0

    .line 1932837
    sget-object v0, LX/Cmp;->BLOCK:LX/Cmp;

    .line 1932838
    :goto_0
    return-object v0

    .line 1932839
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->d()Lcom/facebook/graphql/enums/GraphQLDisplayStyle;

    move-result-object v0

    .line 1932840
    sget-object v1, LX/Clg;->d:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDisplayStyle;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1932841
    sget-object v0, LX/Cmp;->BLOCK:LX/Cmp;

    goto :goto_0

    .line 1932842
    :pswitch_0
    sget-object v0, LX/Cmp;->INLINE:LX/Cmp;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)LX/Cmw;
    .locals 1

    .prologue
    .line 1932833
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1932834
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;

    move-result-object v0

    invoke-static {v0}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;)LX/Cmw;

    move-result-object v0

    .line 1932835
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/Cmw;->b:LX/Cmw;

    goto :goto_0
.end method

.method public final a(LX/Cjt;)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1932777
    iget-object v1, p0, LX/Clh;->a:LX/Chi;

    .line 1932778
    iget-object v2, v1, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v1, v2

    .line 1932779
    if-nez v1, :cond_0

    .line 1932780
    :goto_0
    return-object v0

    .line 1932781
    :cond_0
    sget-object v1, LX/Clg;->a:[I

    invoke-virtual {p1}, LX/Cjt;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1932782
    :pswitch_0
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932783
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932784
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932785
    :pswitch_1
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932786
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932787
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->C()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932788
    :pswitch_2
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932789
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932790
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->B()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932791
    :pswitch_3
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932792
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932793
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932794
    :pswitch_4
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932795
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932796
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932797
    :pswitch_5
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932798
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932799
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932800
    :pswitch_6
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932801
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932802
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->z()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932803
    :pswitch_7
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932804
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932805
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->y()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932806
    :pswitch_8
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932807
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932808
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932809
    :pswitch_9
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932810
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932811
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932812
    :pswitch_a
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932813
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932814
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->A()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto :goto_0

    .line 1932815
    :pswitch_b
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932816
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932817
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto/16 :goto_0

    .line 1932818
    :pswitch_c
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932819
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932820
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto/16 :goto_0

    .line 1932821
    :pswitch_d
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932822
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932823
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto/16 :goto_0

    .line 1932824
    :pswitch_e
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932825
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932826
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto/16 :goto_0

    .line 1932827
    :pswitch_f
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932828
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932829
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto/16 :goto_0

    .line 1932830
    :pswitch_10
    iget-object v0, p0, LX/Clh;->a:LX/Chi;

    .line 1932831
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 1932832
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public final a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;LX/Cm7;Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 1932736
    invoke-static {p1, p3}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Landroid/content/Context;)LX/Cmo;

    move-result-object v5

    .line 1932737
    invoke-virtual {p0, p1}, LX/Clh;->b(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)LX/Cmw;

    move-result-object v6

    .line 1932738
    if-nez p1, :cond_2

    .line 1932739
    invoke-static {p0}, LX/Clh;->a(LX/Clh;)LX/Cmr;

    move-result-object v0

    .line 1932740
    :goto_0
    move-object v2, v0

    .line 1932741
    invoke-static {p1}, LX/Clh;->c(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)LX/Cmp;

    move-result-object v3

    .line 1932742
    invoke-virtual {p0, p1}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)LX/Cmw;

    move-result-object v1

    .line 1932743
    iget-object v0, v1, LX/Cmw;->d:LX/Cmv;

    move-object v0, v0

    .line 1932744
    invoke-virtual {v0}, LX/Cmv;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1932745
    iget-object v0, v1, LX/Cmw;->d:LX/Cmv;

    move-object v0, v0

    .line 1932746
    iput-object v0, p2, LX/Cm7;->c:LX/Cmv;

    .line 1932747
    new-instance v0, LX/Cmw;

    .line 1932748
    iget-object v4, v1, LX/Cmw;->c:LX/Cmv;

    move-object v4, v4

    .line 1932749
    sget-object v7, LX/Cmv;->b:LX/Cmv;

    .line 1932750
    iget-object v8, v1, LX/Cmw;->e:LX/Cmv;

    move-object v8, v8

    .line 1932751
    iget-object p0, v1, LX/Cmw;->f:LX/Cmv;

    move-object v1, p0

    .line 1932752
    invoke-direct {v0, v4, v7, v8, v1}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    .line 1932753
    :goto_1
    iget-object v1, v0, LX/Cmw;->f:LX/Cmv;

    move-object v1, v1

    .line 1932754
    invoke-virtual {v1}, LX/Cmv;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1932755
    iget-object v1, v0, LX/Cmw;->f:LX/Cmv;

    move-object v1, v1

    .line 1932756
    iput-object v1, p2, LX/Cm7;->d:LX/Cmv;

    .line 1932757
    new-instance v1, LX/Cmw;

    .line 1932758
    iget-object v4, v0, LX/Cmw;->c:LX/Cmv;

    move-object v4, v4

    .line 1932759
    iget-object v7, v0, LX/Cmw;->d:LX/Cmv;

    move-object v7, v7

    .line 1932760
    iget-object v8, v0, LX/Cmw;->e:LX/Cmv;

    move-object v0, v8

    .line 1932761
    sget-object v8, LX/Cmv;->b:LX/Cmv;

    invoke-direct {v1, v4, v7, v0, v8}, LX/Cmw;-><init>(LX/Cmv;LX/Cmv;LX/Cmv;LX/Cmv;)V

    .line 1932762
    :goto_2
    new-instance v0, LX/Cn6;

    .line 1932763
    if-eqz p1, :cond_3

    .line 1932764
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->k()Ljava/lang/String;

    move-result-object v4

    .line 1932765
    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 1932766
    invoke-static {v4}, LX/Clh;->a(Ljava/lang/String;)I

    move-result v4

    .line 1932767
    :goto_3
    move v4, v4

    .line 1932768
    invoke-direct/range {v0 .. v6}, LX/Cn6;-><init>(LX/Cmw;LX/Cmr;LX/Cmp;ILX/Cmo;LX/Cmw;)V

    .line 1932769
    iput-object v0, p2, LX/Cm7;->e:LX/Cml;

    .line 1932770
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_2

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 1932771
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->j()Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;

    move-result-object v0

    .line 1932772
    sget-object v1, LX/Clg;->c:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAlignmentStyle;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1932773
    invoke-static {p0}, LX/Clh;->a(LX/Clh;)LX/Cmr;

    move-result-object v0

    goto :goto_0

    .line 1932774
    :pswitch_0
    sget-object v0, LX/Cmr;->LEFT:LX/Cmr;

    goto :goto_0

    .line 1932775
    :pswitch_1
    sget-object v0, LX/Cmr;->CENTER:LX/Cmr;

    goto :goto_0

    .line 1932776
    :pswitch_2
    sget-object v0, LX/Cmr;->RIGHT:LX/Cmr;

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)LX/Cmw;
    .locals 1

    .prologue
    .line 1932733
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->el_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1932734
    :cond_0
    sget-object v0, LX/Cmw;->a:LX/Cmw;

    .line 1932735
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->el_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;

    move-result-object v0

    invoke-static {v0}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichTextSpacingModel;)LX/Cmw;

    move-result-object v0

    goto :goto_0
.end method
