.class public LX/D46;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Landroid/widget/RemoteViews;

.field public final c:Landroid/widget/RemoteViews;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1961786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961787
    iput-object p1, p0, LX/D46;->a:Landroid/content/Context;

    .line 1961788
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030391

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, LX/D46;->b:Landroid/widget/RemoteViews;

    .line 1961789
    iget-object v0, p0, LX/D46;->b:Landroid/widget/RemoteViews;

    invoke-direct {p0, v0}, LX/D46;->a(Landroid/widget/RemoteViews;)V

    .line 1961790
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1961791
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03038d

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, LX/D46;->c:Landroid/widget/RemoteViews;

    .line 1961792
    iget-object v0, p0, LX/D46;->c:Landroid/widget/RemoteViews;

    invoke-direct {p0, v0}, LX/D46;->a(Landroid/widget/RemoteViews;)V

    .line 1961793
    :goto_0
    return-void

    .line 1961794
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/D46;->c:Landroid/widget/RemoteViews;

    goto :goto_0
.end method

.method public static a(LX/D46;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 1961822
    iget-object v0, p0, LX/D46;->a:Landroid/content/Context;

    invoke-static {v0, p1}, LX/D3x;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/D46;IILandroid/app/PendingIntent;)V
    .locals 1
    .param p0    # LX/D46;
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1961818
    iget-object v0, p0, LX/D46;->b:Landroid/widget/RemoteViews;

    invoke-static {v0, p1, p2, p3}, LX/D46;->a(Landroid/widget/RemoteViews;IILandroid/app/PendingIntent;)V

    .line 1961819
    iget-object v0, p0, LX/D46;->c:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    .line 1961820
    iget-object v0, p0, LX/D46;->c:Landroid/widget/RemoteViews;

    invoke-static {v0, p1, p2, p3}, LX/D46;->a(Landroid/widget/RemoteViews;IILandroid/app/PendingIntent;)V

    .line 1961821
    :cond_0
    return-void
.end method

.method public static a(LX/D46;ILjava/lang/CharSequence;)V
    .locals 1
    .param p0    # LX/D46;
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    .line 1961814
    iget-object v0, p0, LX/D46;->b:Landroid/widget/RemoteViews;

    invoke-virtual {v0, p1, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1961815
    iget-object v0, p0, LX/D46;->c:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    .line 1961816
    iget-object v0, p0, LX/D46;->c:Landroid/widget/RemoteViews;

    invoke-virtual {v0, p1, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1961817
    :cond_0
    return-void
.end method

.method public static a(LX/D46;IZ)V
    .locals 4
    .param p0    # LX/D46;
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1961808
    iget-object v3, p0, LX/D46;->b:Landroid/widget/RemoteViews;

    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, p1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1961809
    iget-object v0, p0, LX/D46;->c:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    .line 1961810
    iget-object v0, p0, LX/D46;->c:Landroid/widget/RemoteViews;

    if-eqz p2, :cond_2

    :goto_1
    invoke-virtual {v0, p1, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1961811
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1961812
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1961813
    goto :goto_1
.end method

.method private a(Landroid/widget/RemoteViews;)V
    .locals 2

    .prologue
    .line 1961803
    const v0, 0x7f0d0b6a

    const v1, 0x7f021b28

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1961804
    const v0, 0x7f0d0b6a

    const-string v1, "video.playback.control.action.close"

    invoke-static {p0, v1}, LX/D46;->a(LX/D46;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1961805
    const v0, 0x7f0d0b68

    const v1, 0x7f021b29

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1961806
    const v0, 0x7f0d0b68

    iget-object v1, p0, LX/D46;->a:Landroid/content/Context;

    invoke-static {v1}, LX/D3x;->b(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1961807
    return-void
.end method

.method private static a(Landroid/widget/RemoteViews;IILandroid/app/PendingIntent;)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3    # Landroid/app/PendingIntent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1961799
    invoke-virtual {p0, p1, p2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1961800
    if-eqz p3, :cond_0

    .line 1961801
    invoke-virtual {p0, p1, p3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1961802
    :cond_0
    return-void
.end method

.method public static a(Landroid/widget/RemoteViews;ILandroid/graphics/Bitmap;Landroid/app/PendingIntent;)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/IdRes;
        .end annotation
    .end param
    .param p3    # Landroid/app/PendingIntent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1961795
    invoke-virtual {p0, p1, p2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 1961796
    if-eqz p3, :cond_0

    .line 1961797
    invoke-virtual {p0, p1, p3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1961798
    :cond_0
    return-void
.end method
