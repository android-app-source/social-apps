.class public final LX/Eq2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V
    .locals 0

    .prologue
    .line 2170915
    iput-object p1, p0, LX/Eq2;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2170895
    iget-object v0, p0, LX/Eq2;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    iget-object v0, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->x:LX/03V;

    sget-object v1, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->G:Ljava/lang/String;

    const-string v2, "Failed to fetch suggested facebook friends"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2170896
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2170897
    move-object v1, v1

    .line 2170898
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2170899
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2170900
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel;

    .line 2170901
    iget-object v0, p0, LX/Eq2;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170902
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->n()Z

    move-result v1

    move v0, v1

    .line 2170903
    if-eqz v0, :cond_1

    .line 2170904
    :cond_0
    :goto_0
    return-void

    .line 2170905
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2170906
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2170907
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2170908
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;

    .line 2170909
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2170910
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventsSuggestedInviteCandidatesQueryModel$EventInviteeCandidatesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2170911
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2170912
    :cond_3
    iget-object v0, p0, LX/Eq2;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    .line 2170913
    iput-object v2, v0, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->L:Ljava/util/List;

    .line 2170914
    iget-object v0, p0, LX/Eq2;->a:Lcom/facebook/events/invite/CaspianFriendSelectorFragment;

    invoke-static {v0}, Lcom/facebook/events/invite/CaspianFriendSelectorFragment;->R(Lcom/facebook/events/invite/CaspianFriendSelectorFragment;)V

    goto :goto_0
.end method
