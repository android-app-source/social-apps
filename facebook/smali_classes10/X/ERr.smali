.class public LX/ERr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/ERr;


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private b:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:LX/ERM;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/ERM;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2121763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2121764
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/ERr;->a:Ljava/util/Map;

    .line 2121765
    iput v1, p0, LX/ERr;->b:I

    .line 2121766
    iput-object p1, p0, LX/ERr;->c:Landroid/content/Context;

    .line 2121767
    iput-object p2, p0, LX/ERr;->d:LX/ERM;

    .line 2121768
    iput v1, p0, LX/ERr;->b:I

    .line 2121769
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/ERr;->a:Ljava/util/Map;

    .line 2121770
    const-string v0, "end_vault_upload"

    invoke-static {p0, v0}, LX/ERr;->b(LX/ERr;Ljava/lang/String;)V

    .line 2121771
    return-void
.end method

.method public static a(LX/0QB;)LX/ERr;
    .locals 5

    .prologue
    .line 2121750
    sget-object v0, LX/ERr;->e:LX/ERr;

    if-nez v0, :cond_1

    .line 2121751
    const-class v1, LX/ERr;

    monitor-enter v1

    .line 2121752
    :try_start_0
    sget-object v0, LX/ERr;->e:LX/ERr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2121753
    if-eqz v2, :cond_0

    .line 2121754
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2121755
    new-instance p0, LX/ERr;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/ERM;->a(LX/0QB;)LX/ERM;

    move-result-object v4

    check-cast v4, LX/ERM;

    invoke-direct {p0, v3, v4}, LX/ERr;-><init>(Landroid/content/Context;LX/ERM;)V

    .line 2121756
    move-object v0, p0

    .line 2121757
    sput-object v0, LX/ERr;->e:LX/ERr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2121758
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2121759
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2121760
    :cond_1
    sget-object v0, LX/ERr;->e:LX/ERr;

    return-object v0

    .line 2121761
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2121762
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/content/Intent;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2121747
    const-string v0, "vault.row_upload_key"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2121748
    const-string v0, "vault.upload_percentage"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2121749
    return-object p0
.end method

.method public static a(LX/ERr;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2121744
    const-string v0, "vault.intent.action.SyncStatus"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2121745
    iget-object v0, p0, LX/ERr;->c:Landroid/content/Context;

    invoke-static {v0}, LX/0Xp;->a(Landroid/content/Context;)LX/0Xp;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 2121746
    return-void
.end method

.method private static b(LX/ERr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2121739
    iget-object v0, p0, LX/ERr;->d:LX/ERM;

    .line 2121740
    iget-object v1, v0, LX/ERM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2121741
    sget-object p0, LX/2TR;->c:LX/0Tn;

    invoke-interface {v1, p0, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2121742
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2121743
    return-void
.end method

.method private static c(Lcom/facebook/vault/provider/VaultImageProviderRow;)Z
    .locals 4

    .prologue
    .line 2121738
    iget-wide v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(LX/ERr;)Z
    .locals 2

    .prologue
    .line 2121737
    iget v0, p0, LX/ERr;->b:I

    iget-object v1, p0, LX/ERr;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2121731
    monitor-enter p0

    .line 2121732
    :try_start_0
    const-string v0, "end_vault_upload"

    invoke-static {p0, v0}, LX/ERr;->b(LX/ERr;Ljava/lang/String;)V

    .line 2121733
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2121734
    const-string v1, "vault.sync_end"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2121735
    invoke-static {p0, v0}, LX/ERr;->a(LX/ERr;Landroid/content/Intent;)V

    .line 2121736
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 2121726
    monitor-enter p0

    .line 2121727
    :try_start_0
    invoke-static {p0}, LX/ERr;->i(LX/ERr;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2121728
    iget-object v0, p0, LX/ERr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2121729
    const/4 v0, 0x0

    iput v0, p0, LX/ERr;->b:I

    .line 2121730
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static i(LX/ERr;)Z
    .locals 2

    .prologue
    .line 2121725
    invoke-virtual {p0}, LX/ERr;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "end_vault_upload"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/vault/provider/VaultImageProviderRow;)V
    .locals 3

    .prologue
    .line 2121717
    invoke-static {p1}, LX/ERr;->c(Lcom/facebook/vault/provider/VaultImageProviderRow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2121718
    :goto_0
    return-void

    .line 2121719
    :cond_0
    monitor-enter p0

    .line 2121720
    :try_start_0
    iget-object v0, p0, LX/ERr;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2121721
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2121722
    const-string v0, "vault_upload_start"

    invoke-static {p0, v0}, LX/ERr;->b(LX/ERr;Ljava/lang/String;)V

    .line 2121723
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/ERr;->a(Landroid/content/Intent;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, LX/ERr;->a(LX/ERr;Landroid/content/Intent;)V

    goto :goto_0

    .line 2121724
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/vault/provider/VaultImageProviderRow;I)V
    .locals 2

    .prologue
    .line 2121671
    invoke-static {p1}, LX/ERr;->c(Lcom/facebook/vault/provider/VaultImageProviderRow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2121672
    :goto_0
    return-void

    .line 2121673
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-static {v0, v1, p2}, LX/ERr;->a(Landroid/content/Intent;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, LX/ERr;->a(LX/ERr;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2121712
    monitor-enter p0

    .line 2121713
    :try_start_0
    iget-object v0, p0, LX/ERr;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2121714
    invoke-static {p0}, LX/ERr;->f(LX/ERr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2121715
    invoke-virtual {p0}, LX/ERr;->c()V

    .line 2121716
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2121701
    invoke-static {p0}, LX/ERr;->i(LX/ERr;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2121702
    const-string v0, "begin_vault_upload"

    invoke-static {p0, v0}, LX/ERr;->b(LX/ERr;Ljava/lang/String;)V

    .line 2121703
    :cond_0
    monitor-enter p0

    .line 2121704
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2121705
    invoke-static {v0}, LX/ERr;->c(Lcom/facebook/vault/provider/VaultImageProviderRow;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2121706
    iget-object v2, p0, LX/ERr;->a:Ljava/util/Map;

    iget-object v3, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2121707
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2121708
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2121709
    const-string v1, "vault.sync_start"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2121710
    invoke-static {p0, v0}, LX/ERr;->a(LX/ERr;Landroid/content/Intent;)V

    .line 2121711
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2121697
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2121698
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "vault.table_refreshed_key"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 2121699
    invoke-static {p0, v0}, LX/ERr;->a(LX/ERr;Landroid/content/Intent;)V

    .line 2121700
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2121694
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "vault.status_change_key"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 2121695
    invoke-static {p0, v0}, LX/ERr;->a(LX/ERr;Landroid/content/Intent;)V

    .line 2121696
    return-void
.end method

.method public final b(Lcom/facebook/vault/provider/VaultImageProviderRow;)V
    .locals 3

    .prologue
    .line 2121684
    monitor-enter p0

    .line 2121685
    :try_start_0
    iget-object v0, p0, LX/ERr;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2121686
    iget-object v0, p0, LX/ERr;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2121687
    iget v0, p0, LX/ERr;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/ERr;->b:I

    .line 2121688
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, LX/ERr;->a(Landroid/content/Intent;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2121689
    const-string v1, "vault.upload_completed"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2121690
    invoke-static {p0, v0}, LX/ERr;->a(LX/ERr;Landroid/content/Intent;)V

    .line 2121691
    invoke-static {p0}, LX/ERr;->f(LX/ERr;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2121692
    invoke-virtual {p0}, LX/ERr;->c()V

    .line 2121693
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2121681
    invoke-direct {p0}, LX/ERr;->g()V

    .line 2121682
    invoke-direct {p0}, LX/ERr;->h()V

    .line 2121683
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2121677
    monitor-enter p0

    .line 2121678
    :try_start_0
    invoke-static {p0}, LX/ERr;->f(LX/ERr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2121679
    invoke-direct {p0}, LX/ERr;->g()V

    .line 2121680
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2121674
    iget-object v0, p0, LX/ERr;->d:LX/ERM;

    .line 2121675
    iget-object v1, v0, LX/ERM;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2TR;->c:LX/0Tn;

    const-string p0, "end_vault_upload"

    invoke-interface {v1, v2, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2121676
    return-object v0
.end method
