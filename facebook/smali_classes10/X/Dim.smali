.class public final LX/Dim;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;)V
    .locals 0

    .prologue
    .line 2032545
    iput-object p1, p0, LX/Dim;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 2

    .prologue
    .line 2032546
    packed-switch p1, :pswitch_data_0

    .line 2032547
    :goto_0
    return-void

    .line 2032548
    :pswitch_0
    iget-object v0, p0, LX/Dim;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->a:LX/Dih;

    iget-object v1, p0, LX/Dim;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->b:Ljava/lang/String;

    .line 2032549
    iget-object p0, v0, LX/Dih;->a:LX/0Zb;

    const-string p1, "appointment_calendar_tap_upcoming"

    invoke-static {p1, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032550
    goto :goto_0

    .line 2032551
    :pswitch_1
    iget-object v0, p0, LX/Dim;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->a:LX/Dih;

    iget-object v1, p0, LX/Dim;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentCalendarFragment;->b:Ljava/lang/String;

    .line 2032552
    iget-object p0, v0, LX/Dih;->a:LX/0Zb;

    const-string p1, "appointment_calendar_tap_past"

    invoke-static {p1, v1}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032553
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2032554
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2032555
    return-void
.end method
