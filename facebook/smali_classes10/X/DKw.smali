.class public final LX/DKw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/DKy;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;)V
    .locals 0

    .prologue
    .line 1987639
    iput-object p1, p0, LX/DKw;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1987640
    iget-object v0, p0, LX/DKw;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iget-object v0, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v2, LX/27k;

    iget-object v1, p0, LX/DKw;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iget-object v1, v1, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v3, 0x7f0830a1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1987641
    iget-object v0, p0, LX/DKw;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iget-object v0, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1987642
    instance-of v0, p1, LX/DKx;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DKw;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iget-object v0, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->l:LX/DLT;

    if-eqz v0, :cond_0

    .line 1987643
    iget-object v0, p0, LX/DKw;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iget-object v0, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->l:LX/DLT;

    check-cast p1, LX/DKx;

    iget v1, p1, LX/DKx;->position:I

    .line 1987644
    iget-object v2, v0, LX/DLT;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object v2, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    sget-object v3, LX/DLI;->NORMAL:LX/DLI;

    invoke-virtual {v2, v1, v3}, LX/DKt;->a(ILX/DLI;)V

    .line 1987645
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1987646
    check-cast p1, LX/DKy;

    .line 1987647
    iget-object v0, p0, LX/DKw;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iget-object v0, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->l:LX/DLT;

    if-eqz v0, :cond_0

    .line 1987648
    iget-object v0, p0, LX/DKw;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iget-object v0, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->l:LX/DLT;

    iget v1, p1, LX/DKy;->a:I

    .line 1987649
    iget-object v2, v0, LX/DLT;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object v2, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    sget-object v3, LX/DLI;->NORMAL:LX/DLI;

    invoke-virtual {v2, v1, v3}, LX/DKt;->a(ILX/DLI;)V

    .line 1987650
    :cond_0
    iget-object v0, p0, LX/DKw;->a:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iget-object v1, p1, LX/DKy;->b:Ljava/io/File;

    .line 1987651
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1987652
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 1987653
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v4

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1987654
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1987655
    iget-object v3, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1987656
    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1987657
    :cond_1
    move-object v2, v2

    .line 1987658
    iget-object v3, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->k:Landroid/content/Context;

    const/16 v4, 0x26d

    const/high16 p0, 0x8000000

    invoke-static {v3, v4, v2, p0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 1987659
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1987660
    new-instance p0, LX/2HB;

    iget-object v2, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->k:Landroid/content/Context;

    invoke-direct {p0, v2}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v2, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->i:LX/DM4;

    .line 1987661
    iget-object p1, v2, LX/DM4;->a:Landroid/content/res/Resources;

    const v1, 0x7f020701

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    move-object v2, p1

    .line 1987662
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1987663
    iput-object v2, p0, LX/2HB;->g:Landroid/graphics/Bitmap;

    .line 1987664
    move-object v2, p0

    .line 1987665
    const p0, 0x7f020841

    invoke-virtual {v2, p0}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object p0

    iget-object v2, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const p1, 0x7f0830b8

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v2

    .line 1987666
    iput-object v3, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1987667
    move-object v2, v2

    .line 1987668
    iget-object v3, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->j:Landroid/app/NotificationManager;

    const/16 p0, 0x10

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v3, v4, p0, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1987669
    return-void
.end method
