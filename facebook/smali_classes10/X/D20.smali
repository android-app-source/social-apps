.class public final LX/D20;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ATX;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;)V
    .locals 0

    .prologue
    .line 1957777
    iput-object p1, p0, LX/D20;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1957778
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1957779
    iget-object v0, p0, LX/D20;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->onBackPressed()V

    .line 1957780
    return-void
.end method

.method public final a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1957781
    iget-object v0, p0, LX/D20;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v0, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    if-nez v0, :cond_0

    .line 1957782
    :goto_0
    return-void

    .line 1957783
    :cond_0
    iget-object v0, p0, LX/D20;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v1, p0, LX/D20;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    iget-object v1, v1, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->o()LX/D2G;

    move-result-object v1

    .line 1957784
    iput-object p1, v1, LX/D2G;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1957785
    move-object v1, v1

    .line 1957786
    invoke-virtual {v1}, LX/D2G;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v1

    .line 1957787
    iput-object v1, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957788
    iget-object v0, p0, LX/D20;->a:Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->b(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;Z)V

    goto :goto_0
.end method
