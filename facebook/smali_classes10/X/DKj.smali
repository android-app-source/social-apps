.class public final LX/DKj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TResultType;>;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DKo;


# direct methods
.method public constructor <init>(LX/DKo;)V
    .locals 0

    .prologue
    .line 1987463
    iput-object p1, p0, LX/DKj;->a:LX/DKo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1987464
    iget-object v0, p0, LX/DKj;->a:LX/DKo;

    iget-object v0, v0, LX/DKo;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DKj;->a:LX/DKo;

    iget-boolean v0, v0, LX/DKo;->i:Z

    if-nez v0, :cond_1

    .line 1987465
    :cond_0
    const/4 v0, 0x0

    .line 1987466
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/DKj;->a:LX/DKo;

    iget-object v0, v0, LX/DKo;->e:LX/DKm;

    iget-object v1, p0, LX/DKj;->a:LX/DKo;

    iget-object v1, v1, LX/DKo;->g:Ljava/lang/String;

    sget-object v2, LX/0zS;->a:LX/0zS;

    invoke-interface {v0, v1, v2}, LX/DKm;->a(Ljava/lang/String;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
