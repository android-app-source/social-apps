.class public LX/E6i;
.super LX/Cfk;
.source ""


# instance fields
.field public final a:LX/E1i;


# direct methods
.method public constructor <init>(LX/3Tx;LX/E1i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2080500
    invoke-direct {p0, p1}, LX/Cfk;-><init>(LX/3Tx;)V

    .line 2080501
    iput-object p2, p0, LX/E6i;->a:LX/E1i;

    .line 2080502
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;Landroid/view/View;)LX/Cfl;
    .locals 1

    .prologue
    .line 2080510
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2080504
    const v0, 0x7f031110

    invoke-virtual {p0, v0}, LX/Cfk;->a(I)Landroid/view/View;

    move-result-object v1

    .line 2080505
    const v0, 0x7f0d22d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2080506
    const v2, 0x7f0d2878

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2080507
    const v3, 0x7f0821fc

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2080508
    new-instance v0, LX/E6h;

    invoke-direct {v0, p0}, LX/E6h;-><init>(LX/E6i;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2080509
    return-object v1
.end method

.method public final b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)Z
    .locals 1

    .prologue
    .line 2080503
    const/4 v0, 0x1

    return v0
.end method
