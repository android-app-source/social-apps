.class public abstract LX/Cqj;
.super LX/Cqi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cqi",
        "<",
        "LX/Ctg;",
        "LX/Cqw;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1940390
    invoke-direct {p0, p1, p2}, LX/Cqi;-><init>(Ljava/lang/Object;LX/CrK;)V

    .line 1940391
    invoke-virtual {p0}, LX/Cqj;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1940392
    new-instance v1, LX/Cr8;

    sget-object v2, LX/Cqw;->c:LX/Cqw;

    .line 1940393
    iget-object v0, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1940394
    check-cast v0, LX/Ctg;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    sget-object v4, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    invoke-direct {v1, v2, v0, v3, v4}, LX/Cr8;-><init>(LX/Cqw;LX/Ctg;Ljava/lang/Float;LX/Cqt;)V

    invoke-virtual {p0, v1}, LX/CqX;->a(LX/Cqf;)V

    .line 1940395
    new-instance v1, LX/Cr8;

    sget-object v2, LX/Cqw;->d:LX/Cqw;

    .line 1940396
    iget-object v0, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1940397
    check-cast v0, LX/Ctg;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    sget-object v4, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    invoke-direct {v1, v2, v0, v3, v4}, LX/Cr8;-><init>(LX/Cqw;LX/Ctg;Ljava/lang/Float;LX/Cqt;)V

    invoke-virtual {p0, v1}, LX/CqX;->a(LX/Cqf;)V

    .line 1940398
    :cond_0
    return-void
.end method

.method public static a(LX/CrS;Landroid/view/View;)LX/CrW;
    .locals 2

    .prologue
    .line 1940389
    sget-object v0, LX/CrQ;->RECT:LX/CrQ;

    const-class v1, LX/CrW;

    invoke-interface {p0, p1, v0, v1}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v0

    check-cast v0, LX/CrW;

    return-object v0
.end method


# virtual methods
.method public a(LX/Cqw;LX/Cqw;)Z
    .locals 1

    .prologue
    .line 1940388
    const/4 v0, 0x1

    return v0
.end method

.method public final g()LX/CrS;
    .locals 4

    .prologue
    .line 1940362
    invoke-super {p0}, LX/Cqi;->g()LX/CrS;

    move-result-object v0

    .line 1940363
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v1

    .line 1940364
    invoke-static {v0, v1}, LX/Cqj;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v2

    .line 1940365
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v2, v3}, LX/CrW;->b(I)V

    .line 1940366
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v2, v1}, LX/CrW;->a(I)V

    .line 1940367
    return-object v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 1940377
    invoke-virtual {p0}, LX/Cqj;->l()V

    .line 1940378
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    .line 1940379
    iget-object v1, p0, LX/Cqi;->a:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1940380
    invoke-virtual {p0, v0, v1}, LX/Cqi;->a(LX/Cqv;Landroid/graphics/Rect;)V

    .line 1940381
    sget-object v0, LX/Cqw;->b:LX/Cqw;

    .line 1940382
    iget-object v1, p0, LX/Cqi;->b:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1940383
    invoke-virtual {p0, v0, v1}, LX/Cqi;->a(LX/Cqv;Landroid/graphics/Rect;)V

    .line 1940384
    invoke-virtual {p0}, LX/Cqj;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1940385
    sget-object v0, LX/Cqw;->c:LX/Cqw;

    invoke-virtual {p0}, LX/Cqi;->r()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Cqi;->a(LX/Cqv;Landroid/graphics/Rect;)V

    .line 1940386
    sget-object v0, LX/Cqw;->d:LX/Cqw;

    invoke-virtual {p0}, LX/Cqi;->r()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Cqi;->a(LX/Cqv;Landroid/graphics/Rect;)V

    .line 1940387
    :cond_0
    return-void
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 1940376
    const/4 v0, 0x1

    return v0
.end method

.method public l()V
    .locals 3

    .prologue
    .line 1940372
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1940373
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, LX/Cqi;->a(II)V

    .line 1940374
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LX/Cqi;->b(II)V

    .line 1940375
    return-void
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1940370
    invoke-super {p0}, LX/Cqi;->g()LX/CrS;

    move-result-object v0

    .line 1940371
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Landroid/view/View;
    .locals 1

    .prologue
    .line 1940368
    iget-object v0, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1940369
    check-cast v0, LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method
