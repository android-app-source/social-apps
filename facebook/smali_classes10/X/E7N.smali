.class public final LX/E7N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E6y;

.field public final b:LX/5sc;

.field public c:Z


# direct methods
.method public constructor <init>(LX/E6y;LX/5sc;)V
    .locals 1

    .prologue
    .line 2081333
    iput-object p1, p0, LX/E7N;->a:LX/E6y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2081334
    iput-object p2, p0, LX/E7N;->b:LX/5sc;

    .line 2081335
    iget-object v0, p0, LX/E7N;->b:LX/5sc;

    invoke-interface {v0}, LX/5sc;->as_()Z

    move-result v0

    iput-boolean v0, p0, LX/E7N;->c:Z

    .line 2081336
    return-void
.end method

.method public static a$redex0(LX/E7N;LX/6WS;)V
    .locals 2

    .prologue
    .line 2081330
    invoke-virtual {p1}, LX/5OM;->c()LX/5OG;

    move-result-object v0

    const v1, 0x7f0d3242

    invoke-virtual {v0, v1}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, LX/E7N;->b:LX/5sc;

    invoke-interface {v1}, LX/5sc;->c()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, LX/E7N;->c:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v1

    iget-boolean v0, p0, LX/E7N;->c:Z

    if-eqz v0, :cond_0

    const v0, 0x7f081782

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 2081331
    return-void

    .line 2081332
    :cond_0
    const v0, 0x7f081780

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, -0xf67591e

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2081320
    new-instance v1, LX/6WS;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2081321
    const v2, 0x7f110022

    invoke-virtual {v1, v2}, LX/5OM;->b(I)V

    .line 2081322
    new-instance v2, LX/E7M;

    invoke-direct {v2, p0, v1, p1}, LX/E7M;-><init>(LX/E7N;LX/6WS;Landroid/view/View;)V

    .line 2081323
    iput-object v2, v1, LX/5OM;->p:LX/5OO;

    .line 2081324
    invoke-static {p0, v1}, LX/E7N;->a$redex0(LX/E7N;LX/6WS;)V

    .line 2081325
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    const v3, 0x7f0d3244

    invoke-virtual {v2, v3}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget-object v3, p0, LX/E7N;->b:LX/5sc;

    invoke-interface {v3}, LX/5sc;->d()Z

    move-result v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2081326
    invoke-virtual {v1, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2081327
    invoke-virtual {v1, v5}, LX/0ht;->c(Z)V

    .line 2081328
    invoke-virtual {v1}, LX/0ht;->d()V

    .line 2081329
    const v1, -0xc4d435c

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
