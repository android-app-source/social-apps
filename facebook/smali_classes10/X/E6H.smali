.class public LX/E6H;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2079954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2079955
    return-void
.end method

.method public static a(LX/0Px;Ljava/lang/String;LX/0SG;)LX/Dt2;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLInterfaces$DefaultTimeRangeFields;",
            ">;",
            "Ljava/lang/String;",
            "LX/0SG;",
            ")",
            "LX/Dt2;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2079846
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2079847
    :cond_0
    const/4 v0, 0x0

    .line 2079848
    :goto_0
    return-object v0

    .line 2079849
    :cond_1
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 2079850
    new-instance v1, LX/Dt3;

    invoke-direct {v1, p0, v0, p2}, LX/Dt3;-><init>(Ljava/util/List;Ljava/util/TimeZone;LX/0SG;)V

    .line 2079851
    invoke-virtual {v1}, LX/Dt3;->a()LX/Dt2;

    move-result-object v0

    goto :goto_0
.end method

.method public static varargs a(Ljava/lang/String;Landroid/content/res/Resources;[LX/E6G;)Landroid/text/SpannableString;
    .locals 7

    .prologue
    .line 2079852
    new-instance v0, LX/47x;

    invoke-direct {v0, p1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, p0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v1

    .line 2079853
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p2, v0

    .line 2079854
    iget-object v4, v3, LX/E6G;->c:Landroid/text/style/CharacterStyle;

    if-nez v4, :cond_0

    .line 2079855
    iget-object v4, v3, LX/E6G;->a:Ljava/lang/String;

    iget-object v3, v3, LX/E6G;->b:Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    .line 2079856
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2079857
    :cond_0
    iget-object v4, v3, LX/E6G;->a:Ljava/lang/String;

    iget-object v5, v3, LX/E6G;->b:Ljava/lang/String;

    iget-object v3, v3, LX/E6G;->c:Landroid/text/style/CharacterStyle;

    const/16 v6, 0x21

    invoke-virtual {v1, v4, v5, v3, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    goto :goto_1

    .line 2079858
    :cond_1
    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0SF;Ljava/util/Locale;LX/0Px;Ljava/lang/String;DDLjava/lang/String;Landroid/content/res/Resources;)Landroid/text/SpannableStringBuilder;
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SF;",
            "Ljava/util/Locale;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLInterfaces$DefaultTimeRangeFields;",
            ">;",
            "Ljava/lang/String;",
            "DD",
            "Ljava/lang/String;",
            "Landroid/content/res/Resources;",
            ")",
            "Landroid/text/SpannableStringBuilder;"
        }
    .end annotation

    .prologue
    .line 2079859
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2079860
    invoke-static {p1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 2079861
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 2079862
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 2079863
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumIntegerDigits(I)V

    .line 2079864
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumIntegerDigits(I)V

    .line 2079865
    const-wide/16 v4, 0x0

    cmpl-double v1, p4, v4

    if-lez v1, :cond_0

    .line 2079866
    const v1, 0x7f0a00d1

    invoke-virtual {p9, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2079867
    invoke-virtual {v0, p4, p5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v3, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v2, v0, v3}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    .line 2079868
    const v0, 0x7f0b004e

    invoke-virtual {p9, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0, v2, p9}, LX/E6H;->a(IILandroid/text/SpannableStringBuilder;Landroid/content/res/Resources;)V

    .line 2079869
    const-string v0, " \u00b7 "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079870
    :cond_0
    const-wide/16 v0, 0x0

    cmpl-double v0, p6, v0

    if-lez v0, :cond_1

    .line 2079871
    const v0, 0x7f0f0107

    double-to-int v1, p6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    double-to-int v5, p6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p9, v0, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079872
    const-string v0, " \u00b7 "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079873
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2079874
    invoke-virtual {v2, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079875
    const-string v0, " \u00b7 "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079876
    :cond_2
    invoke-static {p2, p8, p0}, LX/E6H;->a(LX/0Px;Ljava/lang/String;LX/0SG;)LX/Dt2;

    move-result-object v1

    .line 2079877
    if-eqz v1, :cond_3

    .line 2079878
    sget-object v0, LX/Dt2;->OPEN:LX/Dt2;

    if-ne v1, v0, :cond_4

    const v0, 0x7f08211e

    invoke-virtual {p9, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    sget-object v4, LX/Dt2;->OPEN:LX/Dt2;

    if-ne v1, v4, :cond_5

    const v1, 0x7f0a00d4

    invoke-virtual {p9, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_1
    invoke-direct {v3, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v2, v0, v3}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    .line 2079879
    const-string v0, " \u00b7 "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079880
    :cond_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v2

    :goto_2
    return-object v0

    .line 2079881
    :cond_4
    const v0, 0x7f08211f

    invoke-virtual {p9, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const v1, 0x7f0a00d3

    invoke-virtual {p9, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1

    .line 2079882
    :cond_6
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_2
.end method

.method public static a(LX/0SG;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;)Landroid/text/SpannableStringBuilder;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2079883
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2079884
    const v0, 0x7f0a010e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 2079885
    const v0, 0x7f0b004b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2079886
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2079887
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2079888
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {p1}, LX/E6H;->a(Landroid/content/Context;)Landroid/text/style/TypefaceSpan;

    move-result-object v5

    invoke-static {v3, p2, v4, v5}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;Landroid/text/style/CharacterStyle;)V

    .line 2079889
    const-string v4, " \u00b7 "

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079890
    :cond_0
    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;->ap_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2079891
    const v4, 0x7f0a00d1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 2079892
    new-instance v5, Ljava/text/DecimalFormat;

    const-string v6, "0.#"

    invoke-direct {v5, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;->ap_()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$OverallStarRatingModel;->a()D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {p1}, LX/E6H;->a(Landroid/content/Context;)Landroid/text/style/TypefaceSpan;

    move-result-object v7

    invoke-static {v3, v5, v6, v7}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;Landroid/text/style/CharacterStyle;)V

    .line 2079893
    invoke-static {v4, v0, v3, v1}, LX/E6H;->a(IILandroid/text/SpannableStringBuilder;Landroid/content/res/Resources;)V

    .line 2079894
    const-string v0, " \u00b7 "

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079895
    :cond_1
    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;->d()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;

    move-result-object v0

    if-nez v0, :cond_6

    .line 2079896
    const/4 v0, 0x0

    .line 2079897
    :goto_0
    move-object v4, v0

    .line 2079898
    if-eqz v4, :cond_2

    .line 2079899
    sget-object v0, LX/Dt2;->OPEN:LX/Dt2;

    if-ne v4, v0, :cond_4

    const v0, 0x7f08211e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    sget-object v6, LX/Dt2;->OPEN:LX/Dt2;

    if-ne v4, v6, :cond_5

    const v4, 0x7f0a00d4

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_2
    invoke-direct {v5, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {p1}, LX/E6H;->a(Landroid/content/Context;)Landroid/text/style/TypefaceSpan;

    move-result-object v1

    invoke-static {v3, v0, v5, v1}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;Landroid/text/style/CharacterStyle;)V

    .line 2079900
    const-string v0, " \u00b7 "

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079901
    :cond_2
    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;->ao_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2079902
    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;->ao_()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {p1}, LX/E6H;->a(Landroid/content/Context;)Landroid/text/style/TypefaceSpan;

    move-result-object v4

    invoke-static {v3, v0, v1, v4}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;Landroid/text/style/CharacterStyle;)V

    .line 2079903
    const-string v0, " \u00b7 "

    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079904
    :cond_3
    const-string v0, "\n"

    const-string v1, " "

    invoke-virtual {p3, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v3, v0, v1}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    .line 2079905
    return-object v3

    .line 2079906
    :cond_4
    const v0, 0x7f08211f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const v4, 0x7f0a00d3

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_2

    :cond_6
    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel;->d()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$PlaceInfoBlurbFieldsModel$LocationModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, p0}, LX/E6H;->a(LX/0Px;Ljava/lang/String;LX/0SG;)LX/Dt2;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Landroid/text/SpannableStringBuilder;ZLX/174;III)Landroid/text/SpannableStringBuilder;
    .locals 7
    .param p3    # LX/174;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2079907
    if-nez p3, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v6}, LX/E6H;->a(Landroid/content/res/Resources;Landroid/text/SpannableStringBuilder;ZLjava/lang/String;III)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-interface {p3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private static a(Landroid/content/res/Resources;Landroid/text/SpannableStringBuilder;ZLjava/lang/String;III)Landroid/text/SpannableStringBuilder;
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2079908
    const v0, 0x7f0a00d1

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2079909
    const v1, 0x7f0b163a

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    .line 2079910
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int v1, v2, v1

    mul-int/2addr v1, p6

    .line 2079911
    new-instance v2, Landroid/text/SpannableStringBuilder;

    const-string v3, " \u2026 "

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2079912
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2079913
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 2079914
    invoke-virtual {v2, p3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079915
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const/16 v5, 0x21

    invoke-virtual {v2, v4, v3, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2079916
    :cond_0
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v7}, Landroid/text/TextPaint;-><init>(I)V

    .line 2079917
    invoke-virtual {v0, p4}, Landroid/text/TextPaint;->setColor(I)V

    .line 2079918
    int-to-float v3, p5

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2079919
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, p1, v6, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v3

    .line 2079920
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v0, v2, v6, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    .line 2079921
    int-to-float v4, v1

    add-float/2addr v4, v0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_1

    .line 2079922
    int-to-float v1, v1

    sub-float v1, v3, v1

    add-float/2addr v0, v1

    .line 2079923
    div-float/2addr v0, v3

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 2079924
    new-array v1, v8, [Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    sub-int v0, v3, v0

    invoke-virtual {p1, v6, v0}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v1, v6

    aput-object v2, v1, v7

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 2079925
    :goto_0
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    return-object v1

    .line 2079926
    :cond_1
    if-eqz p2, :cond_2

    .line 2079927
    new-array v0, v8, [Ljava/lang/CharSequence;

    aput-object p1, v0, v6

    aput-object v2, v0, v7

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2079928
    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Landroid/text/style/TypefaceSpan;
    .locals 5

    .prologue
    .line 2079929
    new-instance v0, LX/7Gs;

    const-string v1, "roboto"

    sget-object v2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v3, LX/0xr;->MEDIUM:LX/0xr;

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v4}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7Gs;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    return-object v0
.end method

.method private static a(IILandroid/text/SpannableStringBuilder;Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2079930
    const-string v0, " "

    invoke-virtual {p2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079931
    int-to-float v0, p1

    invoke-static {p3, v0}, LX/0tP;->b(Landroid/content/res/Resources;F)I

    move-result v0

    .line 2079932
    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    .line 2079933
    const v1, 0x7f020b11

    .line 2079934
    :goto_0
    move v0, v1

    .line 2079935
    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2079936
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2079937
    invoke-virtual {v0, v2, v2, p1, p1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2079938
    const-string v1, " "

    new-instance v2, LX/E6F;

    invoke-direct {v2, v0, p1}, LX/E6F;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-static {p2, v1, v2}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    .line 2079939
    return-void

    .line 2079940
    :cond_0
    const/16 v1, 0xc

    if-gt v0, v1, :cond_1

    .line 2079941
    const v1, 0x7f020b0e

    goto :goto_0

    .line 2079942
    :cond_1
    const/16 v1, 0x10

    if-gt v0, v1, :cond_2

    .line 2079943
    const v1, 0x7f020b0f

    goto :goto_0

    .line 2079944
    :cond_2
    const v1, 0x7f020b10

    goto :goto_0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V
    .locals 3
    .param p2    # Landroid/text/style/CharacterStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2079945
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 2079946
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2079947
    if-eqz p2, :cond_0

    .line 2079948
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    const/16 v2, 0x21

    invoke-virtual {p0, p2, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2079949
    :cond_0
    return-void
.end method

.method private static a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;Landroid/text/style/CharacterStyle;)V
    .locals 3

    .prologue
    .line 2079950
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 2079951
    invoke-static {p0, p1, p2}, LX/E6H;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/String;Landroid/text/style/CharacterStyle;)V

    .line 2079952
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v0

    const/16 v2, 0x21

    invoke-virtual {p0, p3, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2079953
    return-void
.end method
