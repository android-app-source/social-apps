.class public LX/DHl;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2fC;
.implements LX/2eZ;


# instance fields
.field public a:Z

.field private final b:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1982346
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 1982347
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1982348
    const v0, 0x7f0d1507

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/DHl;->b:Landroid/widget/LinearLayout;

    .line 1982349
    return-void
.end method


# virtual methods
.method public final a(IZZ)V
    .locals 0

    .prologue
    .line 1982361
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1982360
    iget-boolean v0, p0, LX/DHl;->a:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2b6614ad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1982356
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1982357
    const/4 v1, 0x1

    .line 1982358
    iput-boolean v1, p0, LX/DHl;->a:Z

    .line 1982359
    const/16 v1, 0x2d

    const v2, -0x5fde6408

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5cc8c26b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1982352
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1982353
    const/4 v1, 0x0

    .line 1982354
    iput-boolean v1, p0, LX/DHl;->a:Z

    .line 1982355
    const/16 v1, 0x2d

    const v2, -0x5799b497

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setWidth(I)V
    .locals 2

    .prologue
    .line 1982350
    iget-object v0, p0, LX/DHl;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0213f4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 1982351
    return-void
.end method
