.class public final enum LX/EDB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDB;

.field public static final enum INSTANT_VIDEO_ENDED:LX/EDB;

.field public static final enum INSTANT_VIDEO_ENDED_WITH_ERROR:LX/EDB;

.field public static final enum INSTANT_VIDEO_RECIPROCATED:LX/EDB;

.field public static final enum INSTANT_VIDEO_STARTED:LX/EDB;


# instance fields
.field private final mType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2091013
    new-instance v0, LX/EDB;

    const-string v1, "INSTANT_VIDEO_STARTED"

    invoke-direct {v0, v1, v5, v2}, LX/EDB;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EDB;->INSTANT_VIDEO_STARTED:LX/EDB;

    .line 2091014
    new-instance v0, LX/EDB;

    const-string v1, "INSTANT_VIDEO_RECIPROCATED"

    invoke-direct {v0, v1, v2, v3}, LX/EDB;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EDB;->INSTANT_VIDEO_RECIPROCATED:LX/EDB;

    .line 2091015
    new-instance v0, LX/EDB;

    const-string v1, "INSTANT_VIDEO_ENDED"

    invoke-direct {v0, v1, v3, v4}, LX/EDB;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EDB;->INSTANT_VIDEO_ENDED:LX/EDB;

    .line 2091016
    new-instance v0, LX/EDB;

    const-string v1, "INSTANT_VIDEO_ENDED_WITH_ERROR"

    invoke-direct {v0, v1, v4, v6}, LX/EDB;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/EDB;->INSTANT_VIDEO_ENDED_WITH_ERROR:LX/EDB;

    .line 2091017
    new-array v0, v6, [LX/EDB;

    sget-object v1, LX/EDB;->INSTANT_VIDEO_STARTED:LX/EDB;

    aput-object v1, v0, v5

    sget-object v1, LX/EDB;->INSTANT_VIDEO_RECIPROCATED:LX/EDB;

    aput-object v1, v0, v2

    sget-object v1, LX/EDB;->INSTANT_VIDEO_ENDED:LX/EDB;

    aput-object v1, v0, v3

    sget-object v1, LX/EDB;->INSTANT_VIDEO_ENDED_WITH_ERROR:LX/EDB;

    aput-object v1, v0, v4

    sput-object v0, LX/EDB;->$VALUES:[LX/EDB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2091010
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2091011
    iput p3, p0, LX/EDB;->mType:I

    .line 2091012
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDB;
    .locals 1

    .prologue
    .line 2091024
    const-class v0, LX/EDB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDB;

    return-object v0
.end method

.method public static values()[LX/EDB;
    .locals 1

    .prologue
    .line 2091025
    sget-object v0, LX/EDB;->$VALUES:[LX/EDB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDB;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 2091023
    iget v0, p0, LX/EDB;->mType:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2091018
    iget v0, p0, LX/EDB;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 2091019
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2091020
    :pswitch_0
    const-string v0, "video_started"

    goto :goto_0

    .line 2091021
    :pswitch_1
    const-string v0, "video_reciprocated"

    goto :goto_0

    .line 2091022
    :pswitch_2
    const-string v0, "video_ended"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
