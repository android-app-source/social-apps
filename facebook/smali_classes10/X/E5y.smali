.class public final LX/E5y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/E67;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/1Pq;

.field public final synthetic g:LX/Cfc;

.field public final synthetic h:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1Pq;LX/Cfc;)V
    .locals 0

    .prologue
    .line 2079270
    iput-object p1, p0, LX/E5y;->h:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    iput-object p2, p0, LX/E5y;->a:LX/E67;

    iput-object p3, p0, LX/E5y;->b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object p4, p0, LX/E5y;->c:Ljava/lang/String;

    iput-object p5, p0, LX/E5y;->d:Ljava/lang/String;

    iput-object p6, p0, LX/E5y;->e:Ljava/lang/String;

    iput-object p7, p0, LX/E5y;->f:LX/1Pq;

    iput-object p8, p0, LX/E5y;->g:LX/Cfc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x4d1b718f    # 1.62994416E8f

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2079271
    iget-object v0, p0, LX/E5y;->h:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->f:LX/7vZ;

    iget-object v1, p0, LX/E5y;->a:LX/E67;

    iget-object v1, v1, LX/E67;->a:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/E5y;->b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iget-object v3, p0, LX/E5y;->c:Ljava/lang/String;

    iget-object v4, p0, LX/E5y;->d:Ljava/lang/String;

    iget-object v5, p0, LX/E5y;->e:Ljava/lang/String;

    iget-object v6, p0, LX/E5y;->a:LX/E67;

    iget-object v6, v6, LX/E67;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2079272
    iget-object p1, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, p1

    .line 2079273
    invoke-virtual/range {v0 .. v6}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2079274
    iget-object v0, p0, LX/E5y;->h:Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;

    iget-object v1, p0, LX/E5y;->a:LX/E67;

    iget-object v2, p0, LX/E5y;->f:LX/1Pq;

    iget-object v3, p0, LX/E5y;->g:LX/Cfc;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;->a$redex0(Lcom/facebook/reaction/feed/unitcomponents/subpart/ReactionEventRSVPActionPartDefinition;LX/E67;LX/1Pq;LX/Cfc;)V

    .line 2079275
    const v0, 0x732e0c91

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
