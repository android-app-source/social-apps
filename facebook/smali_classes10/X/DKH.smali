.class public LX/DKH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Sh;

.field public final b:LX/DKL;

.field private final c:LX/DKa;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/0kL;


# direct methods
.method public constructor <init>(LX/0Sh;LX/DKL;LX/DKa;Lcom/facebook/content/SecureContextHelper;LX/0kL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1986839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1986840
    iput-object p1, p0, LX/DKH;->a:LX/0Sh;

    .line 1986841
    iput-object p2, p0, LX/DKH;->b:LX/DKL;

    .line 1986842
    iput-object p3, p0, LX/DKH;->c:LX/DKa;

    .line 1986843
    iput-object p4, p0, LX/DKH;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1986844
    iput-object p5, p0, LX/DKH;->e:LX/0kL;

    .line 1986845
    return-void
.end method

.method public static a(LX/0QB;)LX/DKH;
    .locals 1

    .prologue
    .line 1986824
    invoke-static {p0}, LX/DKH;->b(LX/0QB;)LX/DKH;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DKH;
    .locals 6

    .prologue
    .line 1986825
    new-instance v0, LX/DKH;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/DKL;->a(LX/0QB;)LX/DKL;

    move-result-object v2

    check-cast v2, LX/DKL;

    invoke-static {p0}, LX/DKa;->b(LX/0QB;)LX/DKa;

    move-result-object v3

    check-cast v3, LX/DKa;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-direct/range {v0 .. v5}, LX/DKH;-><init>(LX/0Sh;LX/DKL;LX/DKa;Lcom/facebook/content/SecureContextHelper;LX/0kL;)V

    .line 1986826
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 1986827
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->GROUP:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    sget-object v1, LX/8A9;->LAUNCH_COVER_PIC_CROPPER:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 1986828
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1986829
    const-string v2, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1986830
    iget-object v2, p0, LX/DKH;->d:Lcom/facebook/content/SecureContextHelper;

    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, p2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1986831
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 1986832
    new-instance v0, LX/DKG;

    invoke-direct {v0, p0, p2}, LX/DKG;-><init>(LX/DKH;Landroid/net/Uri;)V

    invoke-virtual {p0, p1, p2, v0}, LX/DKH;->a(Ljava/lang/String;Landroid/net/Uri;LX/0TF;)V

    .line 1986833
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;LX/0TF;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "LX/0TF",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1986834
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 1986835
    iget-object v0, p0, LX/DKH;->c:LX/DKa;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/DKa;->a(Ljava/lang/String;Landroid/net/Uri;Lcom/google/common/util/concurrent/SettableFuture;LX/F4e;Z)V

    .line 1986836
    iget-object v0, p0, LX/DKH;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082f54

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1986837
    iget-object v0, p0, LX/DKH;->a:LX/0Sh;

    invoke-virtual {v0, v3, p3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1986838
    return-void
.end method
