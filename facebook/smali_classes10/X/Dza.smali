.class public final LX/Dza;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 0

    .prologue
    .line 2067097
    iput-object p1, p0, LX/Dza;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 2067091
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/Dza;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->t:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067092
    iget-object v1, p0, LX/Dza;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, p0, LX/Dza;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->t:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {v1, v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->a$redex0(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2067093
    :cond_0
    iget-object v0, p0, LX/Dza;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v1, p0, LX/Dza;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v1, v1, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 2067094
    iget-object p1, v0, LX/96B;->a:LX/0Zb;

    const-string p2, "current_location_toggled"

    invoke-static {v0, v1, p2}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-interface {p1, p2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2067095
    iget-object v0, p0, LX/Dza;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-static {v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067096
    return-void
.end method
