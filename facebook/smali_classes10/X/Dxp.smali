.class public final LX/Dxp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

.field public final synthetic c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;LX/4BY;Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;)V
    .locals 0

    .prologue
    .line 2063331
    iput-object p1, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iput-object p2, p0, LX/Dxp;->a:LX/4BY;

    iput-object p3, p0, LX/Dxp;->b:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 2063333
    iget-object v0, p0, LX/Dxp;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2063334
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v3, v0

    .line 2063335
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/173;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/173;

    move-result-object v0

    iget-object v1, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2063336
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 2063337
    move-object v0, v0

    .line 2063338
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 2063339
    new-instance v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;-><init>()V

    invoke-static {v0}, LX/173;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/173;

    move-result-object v0

    iget-object v1, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->x:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2063340
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 2063341
    move-object v0, v0

    .line 2063342
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 2063343
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_6

    .line 2063344
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    .line 2063345
    new-instance v1, LX/4YJ;

    invoke-direct {v1}, LX/4YJ;-><init>()V

    .line 2063346
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2063347
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->a()Z

    move-result v6

    iput-boolean v6, v1, LX/4YJ;->b:Z

    .line 2063348
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v6

    iput-object v6, v1, LX/4YJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063349
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->k()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    move-result-object v6

    iput-object v6, v1, LX/4YJ;->d:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionInfoType;

    .line 2063350
    invoke-static {v1, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2063351
    move-object v0, v1

    .line 2063352
    iget-object v1, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063353
    iput-object v1, v0, LX/4YJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063354
    move-object v0, v0

    .line 2063355
    const/4 v1, 0x1

    .line 2063356
    iput-boolean v1, v0, LX/4YJ;->b:Z

    .line 2063357
    move-object v0, v0

    .line 2063358
    invoke-virtual {v0}, LX/4YJ;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    move-result-object v0

    .line 2063359
    iget-object v1, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v1

    .line 2063360
    new-instance v6, LX/4YI;

    invoke-direct {v6}, LX/4YI;-><init>()V

    .line 2063361
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2063362
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v7

    iput-object v7, v6, LX/4YI;->b:LX/0Px;

    .line 2063363
    invoke-static {v6, v1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2063364
    move-object v1, v6

    .line 2063365
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2063366
    iput-object v0, v1, LX/4YI;->b:LX/0Px;

    .line 2063367
    move-object v0, v1

    .line 2063368
    invoke-virtual {v0}, LX/4YI;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    .line 2063369
    iget-object v1, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 2063370
    new-instance v6, LX/4YL;

    invoke-direct {v6}, LX/4YL;-><init>()V

    .line 2063371
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2063372
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->a()Z

    move-result v7

    iput-boolean v7, v6, LX/4YL;->b:Z

    .line 2063373
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->c:Ljava/lang/String;

    .line 2063374
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->k()Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->d:Lcom/facebook/graphql/model/GraphQLPrivacyEducationInfo;

    .line 2063375
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->l()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->e:Ljava/lang/String;

    .line 2063376
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->m()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->f:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 2063377
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2063378
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->h:Ljava/lang/String;

    .line 2063379
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->p()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->i:Ljava/lang/String;

    .line 2063380
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 2063381
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->r()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 2063382
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->s()Z

    move-result v7

    iput-boolean v7, v6, LX/4YL;->l:Z

    .line 2063383
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4YL;->m:Ljava/lang/String;

    .line 2063384
    invoke-static {v6, v1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2063385
    move-object v1, v6

    .line 2063386
    iget-object v6, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v6, v6, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->y_()Ljava/lang/String;

    move-result-object v6

    .line 2063387
    iput-object v6, v1, LX/4YL;->c:Ljava/lang/String;

    .line 2063388
    move-object v1, v1

    .line 2063389
    iget-object v6, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v6, v6, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v6

    .line 2063390
    iput-object v6, v1, LX/4YL;->h:Ljava/lang/String;

    .line 2063391
    move-object v1, v1

    .line 2063392
    iget-object v6, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v6, v6, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 2063393
    iput-object v6, v1, LX/4YL;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2063394
    move-object v1, v1

    .line 2063395
    iput-object v0, v1, LX/4YL;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 2063396
    move-object v0, v1

    .line 2063397
    invoke-virtual {v0}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    move-object v1, v0

    .line 2063398
    :goto_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2063399
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-static {v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->n(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2063400
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->U:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2063401
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2063402
    iget-object v8, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v8, v8, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v8

    invoke-static {v8}, LX/3dL;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/3dL;

    move-result-object v8

    iget-wide v10, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    .line 2063403
    iput-object v9, v8, LX/3dL;->E:Ljava/lang/String;

    .line 2063404
    move-object v8, v8

    .line 2063405
    iget-object v9, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 2063406
    iput-object v9, v8, LX/3dL;->ag:Ljava/lang/String;

    .line 2063407
    move-object v8, v8

    .line 2063408
    new-instance v9, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-direct {v9}, Lcom/facebook/graphql/model/GraphQLImage;-><init>()V

    invoke-static {v9}, LX/2dc;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/2dc;

    move-result-object v9

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 2063409
    iput-object v0, v9, LX/2dc;->h:Ljava/lang/String;

    .line 2063410
    move-object v0, v9

    .line 2063411
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2063412
    iput-object v0, v8, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2063413
    move-object v0, v8

    .line 2063414
    invoke-virtual {v0}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 2063415
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2063416
    :cond_0
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->V:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v0, :cond_3

    .line 2063417
    new-instance v0, Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-direct {v0}, Lcom/facebook/graphql/model/GraphQLPlace;-><init>()V

    invoke-static {v0}, LX/4Y6;->a(Lcom/facebook/graphql/model/GraphQLPlace;)LX/4Y6;

    move-result-object v0

    iget-object v2, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v2, v2, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->V:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2063418
    iput-object v2, v0, LX/4Y6;->r:Ljava/lang/String;

    .line 2063419
    move-object v0, v0

    .line 2063420
    invoke-virtual {v0}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    .line 2063421
    :cond_1
    :goto_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2063422
    iget-object v7, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v7, v7, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v7}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2063423
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->U:Ljava/util/List;

    if-nez v0, :cond_4

    .line 2063424
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v0

    .line 2063425
    :cond_2
    :goto_3
    iget-object v6, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v6, v6, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v6}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/4Vp;

    move-result-object v6

    .line 2063426
    iput-object v3, v6, LX/4Vp;->m:Ljava/lang/String;

    .line 2063427
    move-object v3, v6

    .line 2063428
    iput-object v4, v3, LX/4Vp;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2063429
    move-object v3, v3

    .line 2063430
    iput-object v5, v3, LX/4Vp;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2063431
    move-object v3, v3

    .line 2063432
    iget-object v4, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v4, v4, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    .line 2063433
    iput-boolean v4, v3, LX/4Vp;->d:Z

    .line 2063434
    move-object v3, v3

    .line 2063435
    if-eqz v1, :cond_5

    .line 2063436
    :goto_4
    iput-object v1, v3, LX/4Vp;->v:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 2063437
    move-object v1, v3

    .line 2063438
    iget-object v3, p0, LX/Dxp;->b:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 2063439
    iput-object v3, v1, LX/4Vp;->c:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 2063440
    move-object v1, v1

    .line 2063441
    iput-object v0, v1, LX/4Vp;->i:LX/0Px;

    .line 2063442
    move-object v0, v1

    .line 2063443
    iput-object v2, v0, LX/4Vp;->k:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 2063444
    move-object v0, v0

    .line 2063445
    iget-object v1, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 2063446
    iput-boolean v1, v0, LX/4Vp;->d:Z

    .line 2063447
    move-object v0, v0

    .line 2063448
    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 2063449
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2063450
    const-string v2, "Updated_ALBUM"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2063451
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->setResult(ILandroid/content/Intent;)V

    .line 2063452
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-virtual {v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->finish()V

    .line 2063453
    return-void

    .line 2063454
    :cond_3
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-boolean v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->W:Z

    if-nez v0, :cond_1

    .line 2063455
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    goto :goto_2

    .line 2063456
    :cond_4
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_3

    .line 2063457
    :cond_5
    iget-object v1, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    goto :goto_4

    :cond_6
    move-object v1, v2

    goto/16 :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2063458
    iget-object v0, p0, LX/Dxp;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2063459
    iget-object v0, p0, LX/Dxp;->c:Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2063460
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2063332
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/Dxp;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
