.class public final LX/CmR;
.super LX/Cm8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cm8",
        "<",
        "Lcom/facebook/richdocument/model/data/MapBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentLocationAnnotation;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;


# direct methods
.method public constructor <init>(LX/0Px;Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;ILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentLocationAnnotation;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;",
            "I",
            "Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1933375
    const/4 v5, 0x7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LX/CmR;-><init>(LX/0Px;Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;ILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;I)V

    .line 1933376
    return-void
.end method

.method private constructor <init>(LX/0Px;Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;ILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentLocationAnnotation;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;",
            "I",
            "Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1933368
    invoke-direct {p0, p5}, LX/Cm8;-><init>(I)V

    .line 1933369
    iput p3, p0, LX/CmR;->a:I

    .line 1933370
    iput-object p1, p0, LX/CmR;->c:LX/0Px;

    .line 1933371
    iput-object p2, p0, LX/CmR;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    .line 1933372
    iput-object p4, p0, LX/CmR;->d:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1933373
    return-void
.end method


# virtual methods
.method public final b()LX/Clr;
    .locals 2

    .prologue
    .line 1933374
    new-instance v0, LX/CmS;

    invoke-direct {v0, p0}, LX/CmS;-><init>(LX/CmR;)V

    return-object v0
.end method
