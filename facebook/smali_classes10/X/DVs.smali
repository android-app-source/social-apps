.class public final LX/DVs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;)V
    .locals 0

    .prologue
    .line 2005848
    iput-object p1, p0, LX/DVs;->a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2005872
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2005873
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 2005849
    iget-object v0, p0, LX/DVs;->a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;

    const-class v1, LX/1ZF;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2005850
    if-eqz v0, :cond_0

    .line 2005851
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 2005852
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    iget-object v2, p0, LX/DVs;->a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08308b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2005853
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2005854
    move-object v1, v1

    .line 2005855
    iput v4, v1, LX/108;->h:I

    .line 2005856
    move-object v1, v1

    .line 2005857
    const/4 v2, 0x1

    .line 2005858
    iput-boolean v2, v1, LX/108;->d:Z

    .line 2005859
    move-object v1, v1

    .line 2005860
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2005861
    :goto_0
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2005862
    :cond_0
    return-void

    .line 2005863
    :cond_1
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    iget-object v2, p0, LX/DVs;->a:Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08308b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2005864
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2005865
    move-object v1, v1

    .line 2005866
    iput v4, v1, LX/108;->h:I

    .line 2005867
    move-object v1, v1

    .line 2005868
    const/4 v2, 0x0

    .line 2005869
    iput-boolean v2, v1, LX/108;->d:Z

    .line 2005870
    move-object v1, v1

    .line 2005871
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    goto :goto_0
.end method
