.class public LX/E33;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field public static final a:LX/1Cz;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2073571
    new-instance v0, LX/E32;

    invoke-direct {v0}, LX/E32;-><init>()V

    sput-object v0, LX/E33;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2073566
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2073567
    const v0, 0x7f031138

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2073568
    const v0, 0x7f0d28c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/E33;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2073569
    const v0, 0x7f0d28c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/E33;->c:Landroid/widget/TextView;

    .line 2073570
    return-void
.end method


# virtual methods
.method public final a(FLX/1aZ;Landroid/graphics/PointF;Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p2    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2073559
    iget-object v0, p0, LX/E33;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2073560
    iget-object v0, p0, LX/E33;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2073561
    iget-object v0, p0, LX/E33;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2073562
    if-eqz p3, :cond_0

    .line 2073563
    iget-object v0, p0, LX/E33;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p3}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 2073564
    :cond_0
    return-void
.end method

.method public getDraweeView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1
    .annotation build Lcom/facebook/common/internal/VisibleForTesting;
    .end annotation

    .prologue
    .line 2073565
    iget-object v0, p0, LX/E33;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method
