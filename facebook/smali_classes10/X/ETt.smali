.class public LX/ETt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ETi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/ETi",
        "<",
        "Lcom/facebook/video/videohome/data/VideoHomeItem;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/EVb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/ETQ;


# direct methods
.method public constructor <init>(LX/ETQ;)V
    .locals 0
    .param p1    # LX/ETQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2125542
    iput-object p1, p0, LX/ETt;->b:LX/ETQ;

    .line 2125543
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 1

    .prologue
    .line 2125544
    iget-object v0, p0, LX/ETt;->b:LX/ETQ;

    invoke-static {v0, p1, p2}, LX/ETR;->a(LX/ETQ;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v0

    return v0
.end method

.method public final b_(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2125545
    iget-object v0, p0, LX/ETt;->b:LX/ETQ;

    invoke-static {v0, p1}, LX/ETR;->a(LX/ETQ;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
