.class public final LX/EFl;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3E7;


# direct methods
.method public constructor <init>(LX/3E7;)V
    .locals 0

    .prologue
    .line 2096273
    iput-object p1, p0, LX/EFl;->a:LX/3E7;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2096259
    iget-object v0, p0, LX/EFl;->a:LX/3E7;

    iget-object v0, v0, LX/3E7;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 2096260
    :try_start_0
    invoke-static {v1}, LX/2gV;->h(LX/2gV;)LX/1tH;

    move-result-object v0

    .line 2096261
    invoke-interface {v0}, LX/1tH;->a()Z

    move-result v0

    move v0, v0

    .line 2096262
    if-nez v0, :cond_0

    .line 2096263
    const-string v0, "RtcSignalingHandler"

    const-string v2, "mqtt is not connected"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096264
    iget-object v0, p0, LX/EFl;->a:LX/3E7;

    .line 2096265
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2096266
    const-string v2, "com.facebook.rti.mqtt.intent.ACTION_WAKEUP"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2096267
    iget-object v2, v0, LX/3E7;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Xl;

    invoke-interface {v2, v3}, LX/0Xl;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2096268
    :cond_0
    invoke-virtual {v1}, LX/2gV;->f()V

    .line 2096269
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2096270
    :catch_0
    move-exception v0

    .line 2096271
    :try_start_1
    const-string v2, "RtcSignalingHandler"

    const-string v3, "Could not connect to mqtt"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2096272
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0
.end method
