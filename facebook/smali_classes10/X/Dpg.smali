.class public LX/Dpg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;


# instance fields
.field public final fbid:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2046645
    new-instance v0, LX/1sv;

    const-string v1, "StickerInfo"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dpg;->b:LX/1sv;

    .line 2046646
    new-instance v0, LX/1sw;

    const-string v1, "fbid"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpg;->c:LX/1sw;

    .line 2046647
    const/4 v0, 0x1

    sput-boolean v0, LX/Dpg;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2046648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2046649
    iput-object p1, p0, LX/Dpg;->fbid:Ljava/lang/Long;

    .line 2046650
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2046651
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2046652
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2046653
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2046654
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "StickerInfo"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2046655
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046656
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046657
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046658
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046659
    const-string v4, "fbid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046660
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046661
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046662
    iget-object v0, p0, LX/Dpg;->fbid:Ljava/lang/Long;

    if-nez v0, :cond_3

    .line 2046663
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046664
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046665
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046666
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2046667
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    .line 2046668
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    .line 2046669
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 2046670
    :cond_3
    iget-object v0, p0, LX/Dpg;->fbid:Ljava/lang/Long;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2046671
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2046672
    iget-object v0, p0, LX/Dpg;->fbid:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2046673
    sget-object v0, LX/Dpg;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2046674
    iget-object v0, p0, LX/Dpg;->fbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2046675
    :cond_0
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2046676
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2046677
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2046678
    if-nez p1, :cond_1

    .line 2046679
    :cond_0
    :goto_0
    return v0

    .line 2046680
    :cond_1
    instance-of v1, p1, LX/Dpg;

    if-eqz v1, :cond_0

    .line 2046681
    check-cast p1, LX/Dpg;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2046682
    if-nez p1, :cond_3

    .line 2046683
    :cond_2
    :goto_1
    move v0, v2

    .line 2046684
    goto :goto_0

    .line 2046685
    :cond_3
    iget-object v0, p0, LX/Dpg;->fbid:Ljava/lang/Long;

    if-eqz v0, :cond_6

    move v0, v1

    .line 2046686
    :goto_2
    iget-object v3, p1, LX/Dpg;->fbid:Ljava/lang/Long;

    if-eqz v3, :cond_7

    move v3, v1

    .line 2046687
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2046688
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2046689
    iget-object v0, p0, LX/Dpg;->fbid:Ljava/lang/Long;

    iget-object v3, p1, LX/Dpg;->fbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_5
    move v2, v1

    .line 2046690
    goto :goto_1

    :cond_6
    move v0, v2

    .line 2046691
    goto :goto_2

    :cond_7
    move v3, v2

    .line 2046692
    goto :goto_3
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2046693
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2046694
    sget-boolean v0, LX/Dpg;->a:Z

    .line 2046695
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Dpg;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2046696
    return-object v0
.end method
