.class public final LX/Exy;
.super LX/3x6;
.source ""


# instance fields
.field public final synthetic a:LX/Exj;

.field public final synthetic b:LX/Exz;


# direct methods
.method public constructor <init>(LX/Exz;LX/Exj;)V
    .locals 0

    .prologue
    .line 2185675
    iput-object p1, p0, LX/Exy;->b:LX/Exz;

    iput-object p2, p0, LX/Exy;->a:LX/Exj;

    invoke-direct {p0}, LX/3x6;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2185654
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v3

    .line 2185655
    iget-object v0, p0, LX/Exy;->a:LX/Exj;

    invoke-virtual {v0, v3}, LX/Exj;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2185656
    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 2185657
    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 2185658
    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 2185659
    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 2185660
    :goto_0
    return-void

    .line 2185661
    :cond_0
    iget-object v0, p0, LX/Exy;->b:LX/Exz;

    iget v0, v0, LX/Exz;->i:I

    div-int/lit8 v2, v0, 0x2

    .line 2185662
    iget-object v0, p0, LX/Exy;->b:LX/Exz;

    iget-object v0, v0, LX/Exz;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v4

    .line 2185663
    const/4 v0, 0x2

    if-ge v3, v0, :cond_1

    move v0, v1

    :goto_1
    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 2185664
    iput v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 2185665
    rem-int/lit8 v0, v3, 0x2

    if-nez v0, :cond_4

    .line 2185666
    if-eqz v4, :cond_2

    move v0, v2

    :goto_2
    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 2185667
    if-eqz v4, :cond_3

    :goto_3
    iput v1, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0

    .line 2185668
    :cond_1
    iget-object v0, p0, LX/Exy;->b:LX/Exz;

    iget v0, v0, LX/Exz;->i:I

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2185669
    goto :goto_2

    :cond_3
    move v1, v2

    .line 2185670
    goto :goto_3

    .line 2185671
    :cond_4
    if-eqz v4, :cond_5

    move v0, v1

    :goto_4
    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 2185672
    if-eqz v4, :cond_6

    :goto_5
    iput v2, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0

    :cond_5
    move v0, v2

    .line 2185673
    goto :goto_4

    :cond_6
    move v2, v1

    .line 2185674
    goto :goto_5
.end method
