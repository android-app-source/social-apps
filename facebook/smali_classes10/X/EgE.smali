.class public final LX/EgE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AL3;


# instance fields
.field public final synthetic a:LX/EgF;


# direct methods
.method public constructor <init>(LX/EgF;)V
    .locals 0

    .prologue
    .line 2155953
    iput-object p1, p0, LX/EgE;->a:LX/EgF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2155954
    iget-object v0, p0, LX/EgE;->a:LX/EgF;

    invoke-virtual {v0}, LX/EgF;->dismiss()V

    .line 2155955
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2155956
    iget-object v0, p0, LX/EgE;->a:LX/EgF;

    iget-object v0, v0, LX/EgF;->d:LX/Efv;

    .line 2155957
    iget-object v1, v0, LX/Efv;->a:LX/Eg1;

    iget-object v1, v1, LX/Efo;->h:LX/Efj;

    .line 2155958
    iget v2, v1, LX/Efj;->b:I

    move v4, v2

    .line 2155959
    iget-object v1, v0, LX/Efv;->a:LX/Eg1;

    iget-object v1, v1, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155960
    iget-object v2, v1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v1, v2

    .line 2155961
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 2155962
    iget-object v1, v0, LX/Efv;->a:LX/Eg1;

    iget-object v1, v1, LX/Eg1;->l:LX/ALw;

    iget-object v2, v0, LX/Efv;->a:LX/Eg1;

    iget-object v2, v2, LX/Eg1;->q:LX/Efu;

    .line 2155963
    iget-object v5, v1, LX/ALw;->b:LX/1Fm;

    .line 2155964
    iget-object v6, v3, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2155965
    new-instance v7, LX/ALv;

    invoke-direct {v7, v1, v2}, LX/ALv;-><init>(LX/ALw;LX/Efu;)V

    invoke-virtual {v5, p1, v6, v7}, LX/1Fm;->a(Ljava/lang/String;Ljava/lang/String;LX/AI9;)V

    .line 2155966
    iget-object v1, v0, LX/Efv;->a:LX/Eg1;

    iget-object v1, v1, LX/Eg1;->o:LX/7ga;

    iget-object v2, v0, LX/Efv;->a:LX/Eg1;

    iget-object v2, v2, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155967
    iget-object v5, v2, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v2, v5

    .line 2155968
    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v2

    .line 2155969
    iget-object v5, v3, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    move-object v3, v5

    .line 2155970
    iget-object v5, v0, LX/Efv;->a:LX/Eg1;

    invoke-virtual {v5}, LX/Eg1;->getCurrentThreadMediaType()LX/7gk;

    move-result-object v5

    sget-object v6, LX/7gZ;->FRIEND:LX/7gZ;

    .line 2155971
    invoke-static/range {v1 .. v6}, LX/7ga;->b(LX/7ga;Ljava/lang/String;Ljava/lang/String;ILX/7gk;LX/7gZ;)Landroid/os/Bundle;

    move-result-object v7

    .line 2155972
    sget-object v8, LX/7gX;->ATTEMPT_TO_SEND_REPLY:LX/7gX;

    invoke-static {v1, v8, v7}, LX/7ga;->a(LX/7ga;LX/7gX;Landroid/os/Bundle;)V

    .line 2155973
    iget-object v0, p0, LX/EgE;->a:LX/EgF;

    invoke-virtual {v0}, LX/EgF;->dismiss()V

    .line 2155974
    return-void
.end method
