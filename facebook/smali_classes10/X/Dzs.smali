.class public final LX/Dzs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/E02;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dzl;

.field public final synthetic b:LX/DzU;

.field public final synthetic c:LX/Dzt;


# direct methods
.method public constructor <init>(LX/Dzt;LX/Dzl;LX/DzU;)V
    .locals 0

    .prologue
    .line 2067503
    iput-object p1, p0, LX/Dzs;->c:LX/Dzt;

    iput-object p2, p0, LX/Dzs;->a:LX/Dzl;

    iput-object p3, p0, LX/Dzs;->b:LX/DzU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2067534
    iget-object v0, p0, LX/Dzs;->c:LX/Dzt;

    iget-object v0, v0, LX/Dzt;->b:LX/DzP;

    .line 2067535
    iget-object v1, v0, LX/DzP;->b:LX/0Zb;

    const-string p1, "bellerophon_error"

    invoke-static {v0, p1}, LX/DzP;->b(LX/DzP;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v1, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2067536
    iget-object v0, p0, LX/Dzs;->a:LX/Dzl;

    invoke-virtual {v0}, LX/Dzl;->b()V

    .line 2067537
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2067504
    check-cast p1, LX/E02;

    .line 2067505
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/E02;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2067506
    :cond_0
    iget-object v0, p0, LX/Dzs;->c:LX/Dzt;

    iget-object v0, v0, LX/Dzt;->b:LX/DzP;

    .line 2067507
    iget-object v1, v0, LX/DzP;->b:LX/0Zb;

    const-string v2, "bellerophon_empty_result"

    invoke-static {v0, v2}, LX/DzP;->b(LX/DzP;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2067508
    iget-object v0, p0, LX/Dzs;->a:LX/Dzl;

    invoke-virtual {v0}, LX/Dzl;->b()V

    .line 2067509
    :goto_0
    return-void

    .line 2067510
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2067511
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2067512
    invoke-virtual {p1}, LX/E02;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067513
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2067514
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2067515
    :cond_2
    iget-object v0, p0, LX/Dzs;->c:LX/Dzt;

    iget-object v0, v0, LX/Dzt;->b:LX/DzP;

    .line 2067516
    iget-object v3, v0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    move-object v0, v3

    .line 2067517
    iget-object v3, p1, LX/E02;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2067518
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2067519
    iput-object v3, v0, Lcom/facebook/places/create/BellerophonLoggerData;->c:Ljava/lang/String;

    .line 2067520
    iput-object v1, v0, Lcom/facebook/places/create/BellerophonLoggerData;->d:Ljava/util/List;

    .line 2067521
    iget-object v0, p0, LX/Dzs;->a:LX/Dzl;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2067522
    iget-object v2, v0, LX/Dzl;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v3, v0, LX/Dzl;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v3, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v4, LX/96A;->DEDUPER:LX/96A;

    invoke-virtual {v2, v3, v4}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    .line 2067523
    iget-object v2, v0, LX/Dzl;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v2, v2, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->l:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2067524
    invoke-virtual {p1}, LX/E02;->b()Ljava/util/List;

    move-result-object v0

    .line 2067525
    iget-object v1, p0, LX/Dzs;->c:LX/Dzt;

    iget-object v2, p0, LX/Dzs;->b:LX/DzU;

    .line 2067526
    new-instance v3, Landroid/content/Intent;

    iget-object v4, v1, LX/Dzt;->a:Landroid/content/Context;

    const-class v5, Lcom/facebook/places/create/PlaceCreationDupActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2067527
    const-string v4, "possible_dup_places"

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 2067528
    const-string v4, "bellerophon_logger_data"

    iget-object v5, v1, LX/Dzt;->b:LX/DzP;

    .line 2067529
    iget-object v1, v5, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    move-object v5, v1

    .line 2067530
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2067531
    iget-object v4, v2, LX/DzU;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-static {v4}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->A(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067532
    iget-object v4, v2, LX/DzU;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    iget-object v4, v4, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->b:Lcom/facebook/content/SecureContextHelper;

    const/4 v5, 0x1

    iget-object v1, v2, LX/DzU;->a:Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-interface {v4, v3, v5, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2067533
    goto/16 :goto_0
.end method
