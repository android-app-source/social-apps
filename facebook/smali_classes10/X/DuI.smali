.class public final LX/DuI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2056398
    const/4 v10, 0x0

    .line 2056399
    const/4 v9, 0x0

    .line 2056400
    const/4 v8, 0x0

    .line 2056401
    const/4 v7, 0x0

    .line 2056402
    const/4 v6, 0x0

    .line 2056403
    const/4 v5, 0x0

    .line 2056404
    const/4 v4, 0x0

    .line 2056405
    const/4 v3, 0x0

    .line 2056406
    const/4 v2, 0x0

    .line 2056407
    const/4 v1, 0x0

    .line 2056408
    const/4 v0, 0x0

    .line 2056409
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 2056410
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2056411
    const/4 v0, 0x0

    .line 2056412
    :goto_0
    return v0

    .line 2056413
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2056414
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_8

    .line 2056415
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2056416
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2056417
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2056418
    const-string v12, "currency"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2056419
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 2056420
    :cond_2
    const-string v12, "option_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2056421
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2056422
    :cond_3
    const-string v12, "shipping_price"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2056423
    const/4 v3, 0x1

    .line 2056424
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 2056425
    :cond_4
    const-string v12, "subtotal"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2056426
    const/4 v2, 0x1

    .line 2056427
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 2056428
    :cond_5
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2056429
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2056430
    :cond_6
    const-string v12, "total"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2056431
    const/4 v1, 0x1

    .line 2056432
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 2056433
    :cond_7
    const-string v12, "total_tax"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2056434
    const/4 v0, 0x1

    .line 2056435
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    goto/16 :goto_1

    .line 2056436
    :cond_8
    const/4 v11, 0x7

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2056437
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 2056438
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2056439
    if-eqz v3, :cond_9

    .line 2056440
    const/4 v3, 0x2

    const/4 v9, 0x0

    invoke-virtual {p1, v3, v8, v9}, LX/186;->a(III)V

    .line 2056441
    :cond_9
    if-eqz v2, :cond_a

    .line 2056442
    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v7, v3}, LX/186;->a(III)V

    .line 2056443
    :cond_a
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 2056444
    if-eqz v1, :cond_b

    .line 2056445
    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v5, v2}, LX/186;->a(III)V

    .line 2056446
    :cond_b
    if-eqz v0, :cond_c

    .line 2056447
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 2056448
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2056449
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2056450
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056451
    if-eqz v0, :cond_0

    .line 2056452
    const-string v1, "currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056453
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056454
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056455
    if-eqz v0, :cond_1

    .line 2056456
    const-string v1, "option_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056457
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056458
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2056459
    if-eqz v0, :cond_2

    .line 2056460
    const-string v1, "shipping_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056461
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2056462
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2056463
    if-eqz v0, :cond_3

    .line 2056464
    const-string v1, "subtotal"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056465
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2056466
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2056467
    if-eqz v0, :cond_4

    .line 2056468
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056469
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2056470
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2056471
    if-eqz v0, :cond_5

    .line 2056472
    const-string v1, "total"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056473
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2056474
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2056475
    if-eqz v0, :cond_6

    .line 2056476
    const-string v1, "total_tax"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2056477
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2056478
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2056479
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2056480
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2056481
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2056482
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2056483
    invoke-static {p0, p1}, LX/DuI;->a(LX/15w;LX/186;)I

    move-result v1

    .line 2056484
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2056485
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method
