.class public final enum LX/EFu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EFu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EFu;

.field public static final enum HIGH:LX/EFu;

.field public static final enum LOW:LX/EFu;

.field public static final enum MEDIUM:LX/EFu;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2096595
    new-instance v0, LX/EFu;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v2}, LX/EFu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFu;->LOW:LX/EFu;

    .line 2096596
    new-instance v0, LX/EFu;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, LX/EFu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFu;->MEDIUM:LX/EFu;

    .line 2096597
    new-instance v0, LX/EFu;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v4}, LX/EFu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EFu;->HIGH:LX/EFu;

    .line 2096598
    const/4 v0, 0x3

    new-array v0, v0, [LX/EFu;

    sget-object v1, LX/EFu;->LOW:LX/EFu;

    aput-object v1, v0, v2

    sget-object v1, LX/EFu;->MEDIUM:LX/EFu;

    aput-object v1, v0, v3

    sget-object v1, LX/EFu;->HIGH:LX/EFu;

    aput-object v1, v0, v4

    sput-object v0, LX/EFu;->$VALUES:[LX/EFu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2096594
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EFu;
    .locals 1

    .prologue
    .line 2096600
    const-class v0, LX/EFu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EFu;

    return-object v0
.end method

.method public static values()[LX/EFu;
    .locals 1

    .prologue
    .line 2096599
    sget-object v0, LX/EFu;->$VALUES:[LX/EFu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EFu;

    return-object v0
.end method
