.class public final LX/E9O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/E9P;


# direct methods
.method public constructor <init>(LX/E9P;)V
    .locals 0

    .prologue
    .line 2084341
    iput-object p1, p0, LX/E9O;->a:LX/E9P;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2084342
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v2, 0x0

    .line 2084343
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/E9O;->a:LX/E9P;

    iget-object v0, v0, LX/E9P;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2084344
    :cond_0
    :goto_0
    return-object v2

    .line 2084345
    :cond_1
    iget-object v0, p0, LX/E9O;->a:LX/E9P;

    iget-object v0, v0, LX/E9P;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5tj;

    invoke-static {v0, p1}, LX/BNG;->a(LX/5tj;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/5tj;

    move-result-object v0

    .line 2084346
    iget-object v1, p0, LX/E9O;->a:LX/E9P;

    invoke-virtual {v1, v0}, LX/E9P;->a(LX/5tj;)V

    .line 2084347
    iget-object v0, p0, LX/E9O;->a:LX/E9P;

    iget-object v0, v0, LX/E9P;->d:LX/E9T;

    invoke-interface {v0}, LX/E9T;->f()V

    goto :goto_0
.end method
