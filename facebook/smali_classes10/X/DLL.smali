.class public final LX/DLL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V
    .locals 0

    .prologue
    .line 1988123
    iput-object p1, p0, LX/DLL;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1988124
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1988125
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;

    .line 1988126
    new-instance v4, LX/DLH;

    sget-object v5, LX/DLI;->NORMAL:LX/DLI;

    invoke-direct {v4, v0, v5}, LX/DLH;-><init>(Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel$NodesModel;LX/DLI;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1988127
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1988128
    :cond_0
    iget-object v0, p0, LX/DLL;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object v0, v0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    .line 1988129
    if-nez v2, :cond_1

    .line 1988130
    :goto_1
    return-void

    .line 1988131
    :cond_1
    iput-object v2, v0, LX/DKt;->a:Ljava/util/List;

    .line 1988132
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, v0, LX/DKt;->e:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1988133
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1988134
    iget-object v3, v0, LX/DKt;->a:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4, v1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 1988135
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1988136
    iget-object v0, p0, LX/DLL;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    iget-object v0, v0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    .line 1988137
    iget-boolean p0, v0, LX/DKt;->b:Z

    if-eq p1, p0, :cond_0

    .line 1988138
    iput-boolean p1, v0, LX/DKt;->b:Z

    .line 1988139
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1988140
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1988141
    iget-object v0, p0, LX/DLL;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    .line 1988142
    iput-boolean p1, v0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->A:Z

    .line 1988143
    iget-object v0, p0, LX/DLL;->a:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    invoke-static {v0, p1}, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->a(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;Z)V

    .line 1988144
    return-void
.end method
