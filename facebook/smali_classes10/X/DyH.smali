.class public final LX/DyH;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V
    .locals 0

    .prologue
    .line 2064077
    iput-object p1, p0, LX/DyH;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2064095
    iget-object v0, p0, LX/DyH;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchMoreAlbumsList"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2064096
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2064078
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v2, 0x0

    .line 2064079
    if-eqz p1, :cond_0

    .line 2064080
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2064081
    if-nez v0, :cond_1

    .line 2064082
    :cond_0
    :goto_0
    return-void

    .line 2064083
    :cond_1
    iget-object v0, p0, LX/DyH;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    .line 2064084
    iput-boolean v2, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->q:Z

    .line 2064085
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 2064086
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2064087
    iget-object v1, p0, LX/DyH;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 2064088
    iput-object v2, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->o:Ljava/lang/String;

    .line 2064089
    iget-object v1, p0, LX/DyH;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v2

    .line 2064090
    iput-boolean v2, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->p:Z

    .line 2064091
    :goto_1
    iget-object v1, p0, LX/DyH;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    iget-object v2, p0, LX/DyH;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-boolean v2, v2, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->p:Z

    invoke-virtual {v1, v0, v2}, LX/9bF;->b(Lcom/facebook/graphql/model/GraphQLAlbumsConnection;Z)V

    goto :goto_0

    .line 2064092
    :cond_2
    iget-object v1, p0, LX/DyH;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    .line 2064093
    iput-boolean v2, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->p:Z

    .line 2064094
    goto :goto_1
.end method
