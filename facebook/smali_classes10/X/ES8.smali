.class public LX/ES8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile k:LX/ES8;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2TN;

.field private final d:LX/ERw;

.field private final e:LX/ERj;

.field public final f:Landroid/content/ContentResolver;

.field private final g:LX/2TL;

.field private final h:LX/ERL;

.field private final i:LX/ERH;

.field private final j:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2122520
    const-class v0, LX/ES8;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ES8;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;LX/2TN;LX/ERw;LX/ERj;LX/2TL;LX/ERL;LX/ERH;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2122509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2122510
    iput-object p1, p0, LX/ES8;->b:Landroid/content/Context;

    .line 2122511
    iput-object p2, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    .line 2122512
    iput-object p3, p0, LX/ES8;->c:LX/2TN;

    .line 2122513
    iput-object p4, p0, LX/ES8;->d:LX/ERw;

    .line 2122514
    iput-object p5, p0, LX/ES8;->e:LX/ERj;

    .line 2122515
    iput-object p6, p0, LX/ES8;->g:LX/2TL;

    .line 2122516
    iput-object p7, p0, LX/ES8;->h:LX/ERL;

    .line 2122517
    iput-object p8, p0, LX/ES8;->i:LX/ERH;

    .line 2122518
    iput-object p9, p0, LX/ES8;->j:LX/03V;

    .line 2122519
    return-void
.end method

.method public static a(LX/0QB;)LX/ES8;
    .locals 13

    .prologue
    .line 2122487
    sget-object v0, LX/ES8;->k:LX/ES8;

    if-nez v0, :cond_1

    .line 2122488
    const-class v1, LX/ES8;

    monitor-enter v1

    .line 2122489
    :try_start_0
    sget-object v0, LX/ES8;->k:LX/ES8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2122490
    if-eqz v2, :cond_0

    .line 2122491
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2122492
    new-instance v3, LX/ES8;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v5

    check-cast v5, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/2TN;->c(LX/0QB;)LX/2TN;

    move-result-object v6

    check-cast v6, LX/2TN;

    .line 2122493
    new-instance v9, LX/ERw;

    invoke-static {v0}, LX/2TN;->c(LX/0QB;)LX/2TN;

    move-result-object v7

    check-cast v7, LX/2TN;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct {v9, v7, v8}, LX/ERw;-><init>(LX/2TN;LX/03V;)V

    .line 2122494
    move-object v7, v9

    .line 2122495
    check-cast v7, LX/ERw;

    .line 2122496
    new-instance v11, LX/ERj;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v8

    check-cast v8, LX/11H;

    .line 2122497
    new-instance v10, LX/ERW;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v9

    check-cast v9, LX/0lC;

    invoke-direct {v10, v9}, LX/ERW;-><init>(LX/0lC;)V

    .line 2122498
    move-object v9, v10

    .line 2122499
    check-cast v9, LX/ERW;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-direct {v11, v8, v9, v10}, LX/ERj;-><init>(LX/11H;LX/ERW;LX/03V;)V

    .line 2122500
    move-object v8, v11

    .line 2122501
    check-cast v8, LX/ERj;

    invoke-static {v0}, LX/2TL;->a(LX/0QB;)LX/2TL;

    move-result-object v9

    check-cast v9, LX/2TL;

    invoke-static {v0}, LX/ERL;->a(LX/0QB;)LX/ERL;

    move-result-object v10

    check-cast v10, LX/ERL;

    invoke-static {v0}, LX/ERH;->a(LX/0QB;)LX/ERH;

    move-result-object v11

    check-cast v11, LX/ERH;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-direct/range {v3 .. v12}, LX/ES8;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;LX/2TN;LX/ERw;LX/ERj;LX/2TL;LX/ERL;LX/ERH;LX/03V;)V

    .line 2122502
    move-object v0, v3

    .line 2122503
    sput-object v0, LX/ES8;->k:LX/ES8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2122504
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2122505
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2122506
    :cond_1
    sget-object v0, LX/ES8;->k:LX/ES8;

    return-object v0

    .line 2122507
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2122508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/util/Set;Ljava/util/List;)Ljava/lang/StringBuffer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/StringBuffer;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2122477
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 2122478
    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2122479
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2122480
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 2122481
    const-string v3, " or "

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2122482
    :cond_0
    const-string v3, "%s = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v5, LX/DbD;->a:LX/0U1;

    .line 2122483
    iget-object p0, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p0

    .line 2122484
    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2122485
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2122486
    :cond_1
    return-object v1
.end method

.method public static a(LX/ES8;LX/ES7;ILX/ES4;LX/ES6;J)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ES7;",
            "I",
            "LX/ES4;",
            "LX/ES6;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2122424
    if-nez p2, :cond_0

    .line 2122425
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2122426
    :goto_0
    return-object v0

    .line 2122427
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2122428
    const-string v0, "%s = ?"

    new-array v1, v6, [Ljava/lang/Object;

    sget-object v2, LX/DbD;->f:LX/0U1;

    .line 2122429
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2122430
    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2122431
    sget-object v1, LX/ES7;->UPGRADE_ONLY:LX/ES7;

    if-eq p1, v1, :cond_4

    .line 2122432
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122433
    sget-object v1, LX/ES7;->ALL:LX/ES7;

    if-ne p1, v1, :cond_1

    .line 2122434
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " or %s = ?"

    new-array v2, v6, [Ljava/lang/Object;

    sget-object v3, LX/DbD;->f:LX/0U1;

    .line 2122435
    iget-object v8, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v8

    .line 2122436
    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2122437
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122438
    :cond_1
    :goto_1
    sget-object v1, LX/ES4;->RETRY_WITH_HARD_FAILURES:LX/ES4;

    if-ne p3, v1, :cond_2

    .line 2122439
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " or %s = ?"

    new-array v2, v6, [Ljava/lang/Object;

    sget-object v3, LX/DbD;->f:LX/0U1;

    .line 2122440
    iget-object v8, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v8

    .line 2122441
    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2122442
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122443
    :cond_2
    const-string v1, "(%s) and (%s = ?)"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v5

    sget-object v0, LX/DbD;->h:LX/0U1;

    .line 2122444
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 2122445
    aput-object v0, v2, v6

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2122446
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122447
    sget-object v0, LX/ES4;->RETRY_SOFT_FAILURES_ONLY:LX/ES4;

    if-ne p3, v0, :cond_5

    .line 2122448
    const-string v0, "%s > 0 and %s < %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, LX/DbD;->e:LX/0U1;

    .line 2122449
    iget-object v8, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v8

    .line 2122450
    aput-object v3, v2, v5

    sget-object v3, LX/DbD;->e:LX/0U1;

    .line 2122451
    iget-object v8, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v8

    .line 2122452
    aput-object v3, v2, v6

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2122453
    :goto_2
    const-string v2, "(%s) and (%s)"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v5

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2122454
    sget-object v0, LX/ES6;->OLD_PHOTOS:LX/ES6;

    if-ne p4, v0, :cond_7

    .line 2122455
    const-string v0, "(%s) and (%s < ?)"

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v3, v1, v5

    sget-object v2, LX/DbD;->c:LX/0U1;

    .line 2122456
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2122457
    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2122458
    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122459
    :cond_3
    :goto_3
    const-string v0, "%s DESC LIMIT %d"

    new-array v1, v7, [Ljava/lang/Object;

    sget-object v2, LX/DbD;->c:LX/0U1;

    .line 2122460
    iget-object v7, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v7

    .line 2122461
    aput-object v2, v1, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2122462
    iget-object v0, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/DbE;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2122463
    invoke-static {v0}, LX/ES8;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 2122464
    :cond_4
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2122465
    :cond_5
    sget-object v0, LX/ES4;->RETRY_WITH_HARD_FAILURES:LX/ES4;

    if-ne p3, v0, :cond_6

    .line 2122466
    const-string v0, "%s > 0"

    new-array v2, v6, [Ljava/lang/Object;

    sget-object v3, LX/DbD;->e:LX/0U1;

    .line 2122467
    iget-object v8, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v8

    .line 2122468
    aput-object v3, v2, v5

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2122469
    :cond_6
    const-string v0, "%s = 0"

    new-array v2, v6, [Ljava/lang/Object;

    sget-object v3, LX/DbD;->e:LX/0U1;

    .line 2122470
    iget-object v8, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v8

    .line 2122471
    aput-object v3, v2, v5

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 2122472
    :cond_7
    sget-object v0, LX/ES6;->NEW_PHOTOS:LX/ES6;

    if-ne p4, v0, :cond_3

    .line 2122473
    const-string v0, "(%s) and (%s >= ?)"

    new-array v1, v7, [Ljava/lang/Object;

    aput-object v3, v1, v5

    sget-object v2, LX/DbD;->c:LX/0U1;

    .line 2122474
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2122475
    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2122476
    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method private static a(Landroid/database/Cursor;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2122414
    if-eqz p0, :cond_2

    .line 2122415
    :try_start_0
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 2122416
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2122417
    new-instance v1, Lcom/facebook/vault/provider/VaultImageProviderRow;

    invoke-direct {v1, p0}, Lcom/facebook/vault/provider/VaultImageProviderRow;-><init>(Landroid/database/Cursor;)V

    .line 2122418
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2122419
    :catchall_0
    move-exception v0

    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 2122420
    :goto_1
    const-string v1, "vault"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2122421
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getResultsFromCursor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2122422
    :cond_1
    return-object v0

    .line 2122423
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(LX/ES8;JILX/ERl;I)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "LX/ERl;",
            "I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2122391
    new-array v2, v6, [Ljava/lang/String;

    sget-object v0, LX/DbD;->a:LX/0U1;

    .line 2122392
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2122393
    aput-object v0, v2, v5

    .line 2122394
    sget-object v0, LX/ERl;->BEFORE:LX/ERl;

    if-ne p4, v0, :cond_1

    .line 2122395
    const-string v0, "%s <= %d"

    .line 2122396
    :goto_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v3, LX/DbD;->c:LX/0U1;

    .line 2122397
    iget-object v7, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v7

    .line 2122398
    aput-object v3, v1, v5

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v6

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2122399
    iget-object v0, p0, LX/ES8;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/DbE;->a:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2122400
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 2122401
    if-eqz v1, :cond_3

    .line 2122402
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2122403
    if-eqz v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2122404
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2122405
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2122406
    :cond_1
    const-string v0, "%s >= %d"

    goto :goto_0

    .line 2122407
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2122408
    :cond_3
    iget-object v1, p0, LX/ES8;->c:LX/2TN;

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, LX/2TN;->a(JILX/ERl;I)Ljava/util/Map;

    move-result-object v1

    .line 2122409
    invoke-interface {v1}, Ljava/util/Map;->size()I

    .line 2122410
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2122411
    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2122412
    :cond_4
    invoke-interface {v7}, Ljava/util/Set;->size()I

    invoke-interface {v1}, Ljava/util/Map;->size()I

    .line 2122413
    return-object v1
.end method

.method public static a(LX/ES8;Ljava/lang/String;Ljava/util/List;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2122156
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    sget-object v0, LX/DbD;->a:LX/0U1;

    .line 2122157
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 2122158
    aput-object v0, v2, v1

    sget-object v0, LX/DbD;->b:LX/0U1;

    .line 2122159
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2122160
    aput-object v0, v2, v3

    .line 2122161
    iget-object v0, p0, LX/ES8;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, LX/DbE;->a:Landroid/net/Uri;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {p2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    move-object v4, v3

    check-cast v4, [Ljava/lang/String;

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2122162
    if-nez v1, :cond_0

    .line 2122163
    iget-object v0, p0, LX/ES8;->j:LX/03V;

    const-string v1, "get_fbids_for_hashes_no_cursor"

    const-string v2, "no vault cursor"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2122164
    :goto_0
    return-object v5

    .line 2122165
    :cond_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 2122166
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2122167
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2122168
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private a(Ljava/util/Map;)Ljava/util/Set;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2122362
    iget-object v0, p0, LX/ES8;->e:LX/ERj;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, LX/ES8;->g:LX/2TL;

    invoke-virtual {v2}, LX/2TL;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/ERj;->a(Ljava/util/Set;J)Ljava/util/Map;

    move-result-object v0

    .line 2122363
    iget-object v1, p0, LX/ES8;->d:LX/ERw;

    const/16 v13, 0x800

    const/4 v12, 0x1

    .line 2122364
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 2122365
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2122366
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2122367
    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;

    .line 2122368
    if-eqz v8, :cond_0

    .line 2122369
    iget-boolean v10, v8, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mDeleted:Z

    if-eqz v10, :cond_2

    .line 2122370
    const/4 v7, 0x7

    iput v7, v6, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    .line 2122371
    :cond_1
    :goto_1
    iget-wide v10, v8, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mFbid:J

    iput-wide v10, v6, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    goto :goto_0

    .line 2122372
    :cond_2
    iget v10, v8, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mHeight:I

    if-ge v10, v13, :cond_3

    iget v10, v8, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mWidth:I

    if-lt v10, v13, :cond_4

    .line 2122373
    :cond_3
    iput v12, v6, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    goto :goto_1

    .line 2122374
    :cond_4
    iget v10, v8, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mHeight:I

    if-lez v10, :cond_6

    iget v10, v8, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mWidth:I

    if-lez v10, :cond_6

    .line 2122375
    iget-object v10, v1, LX/ERw;->b:LX/2TN;

    invoke-virtual {v10, v7}, LX/2TN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2122376
    if-eqz v7, :cond_1

    .line 2122377
    invoke-static {v7}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v7

    .line 2122378
    iget v10, v8, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mHeight:I

    iget v11, v7, LX/434;->a:I

    if-ne v10, v11, :cond_5

    iget v10, v8, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mWidth:I

    iget v7, v7, LX/434;->b:I

    if-ne v10, v7, :cond_5

    .line 2122379
    iput v12, v6, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    goto :goto_1

    .line 2122380
    :cond_5
    const/4 v7, 0x0

    iput v7, v6, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    goto :goto_1

    .line 2122381
    :cond_6
    iget-object v7, v1, LX/ERw;->c:LX/03V;

    const-string v10, "Unexpected server image state"

    const-string v11, "Image found on server but has 0px width or 0px height"

    invoke-virtual {v7, v10, v11}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2122382
    const/4 v7, 0x4

    iput v7, v6, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    goto :goto_1

    .line 2122383
    :cond_7
    move-object v3, p1

    .line 2122384
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    new-array v4, v0, [Landroid/content/ContentValues;

    .line 2122385
    const/4 v0, 0x0

    .line 2122386
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2122387
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Lcom/facebook/vault/provider/VaultImageProviderRow;->b()Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v4, v1

    move v1, v2

    .line 2122388
    goto :goto_2

    .line 2122389
    :cond_8
    iget-object v0, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/DbE;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    .line 2122390
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2122354
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2122355
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2122356
    const-string v0, "%s IN (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, LX/DbD;->a:LX/0U1;

    .line 2122357
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2122358
    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2122359
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 2122360
    iget-object v2, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v3, LX/DbE;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2122361
    return-void
.end method

.method private b(I)J
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2122341
    if-ne p1, v6, :cond_0

    const-string v0, "DESC"

    .line 2122342
    :goto_0
    const-string v1, "%s %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, LX/DbD;->c:LX/0U1;

    .line 2122343
    iget-object p1, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p1

    .line 2122344
    aput-object v5, v3, v4

    aput-object v0, v3, v6

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2122345
    iget-object v0, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/DbE;->a:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2122346
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2122347
    sget-object v0, LX/DbD;->c:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2122348
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2122349
    :goto_1
    return-wide v0

    .line 2122350
    :cond_0
    const-string v0, "ASC"

    goto :goto_0

    .line 2122351
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2122352
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 2122353
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static d(LX/ES8;)J
    .locals 8

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 2122323
    const-string v0, "%s DESC LIMIT 1"

    new-array v1, v4, [Ljava/lang/Object;

    sget-object v2, LX/DbD;->c:LX/0U1;

    .line 2122324
    iget-object v5, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v5

    .line 2122325
    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2122326
    const-string v0, "%s != %d AND %s = %d"

    new-array v1, v3, [Ljava/lang/Object;

    sget-object v2, LX/DbD;->f:LX/0U1;

    .line 2122327
    iget-object v7, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v7

    .line 2122328
    aput-object v2, v1, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x2

    sget-object v3, LX/DbD;->h:LX/0U1;

    .line 2122329
    iget-object v7, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v7

    .line 2122330
    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2122331
    iget-object v0, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/DbE;->a:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    sget-object v4, LX/DbD;->c:LX/0U1;

    .line 2122332
    iget-object v7, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v7

    .line 2122333
    aput-object v4, v2, v6

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2122334
    const-wide/16 v0, 0x0

    .line 2122335
    if-eqz v2, :cond_1

    .line 2122336
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2122337
    sget-object v0, LX/DbD;->c:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2122338
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2122339
    :cond_1
    return-wide v0

    .line 2122340
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static e()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2122320
    const-string v0, "%s IN (%d, %d, %d, %d, %d)"

    new-array v1, v7, [Ljava/lang/Object;

    sget-object v2, LX/DbD;->f:LX/0U1;

    .line 2122321
    iget-object v8, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v8

    .line 2122322
    aput-object v2, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()J
    .locals 8

    .prologue
    .line 2122316
    const/4 v0, 0x2

    invoke-direct {p0, v0}, LX/ES8;->b(I)J

    move-result-wide v0

    .line 2122317
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/ES8;->i:LX/ERH;

    .line 2122318
    iget-object v4, v0, LX/ERH;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/2TR;->g:LX/0Tn;

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    move-wide v0, v4

    .line 2122319
    goto :goto_0
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2122308
    const-string v0, "%s DESC"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, LX/DbD;->c:LX/0U1;

    .line 2122309
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2122310
    aput-object v4, v1, v3

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2122311
    iget-object v0, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/DbE;->a:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2122312
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2122313
    new-instance v0, Lcom/facebook/vault/provider/VaultImageProviderRow;

    invoke-direct {v0, v1}, Lcom/facebook/vault/provider/VaultImageProviderRow;-><init>(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2122314
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2122315
    return-void
.end method


# virtual methods
.method public final a(J)I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2122292
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2122293
    sget-object v2, LX/DbD;->h:LX/0U1;

    .line 2122294
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2122295
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2122296
    const-string v2, "%s < ? AND %s = ?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v4, LX/DbD;->i:LX/0U1;

    .line 2122297
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2122298
    aput-object v4, v3, v0

    sget-object v4, LX/DbD;->h:LX/0U1;

    .line 2122299
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2122300
    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2122301
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2122302
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122303
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122304
    iget-object v4, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v5, LX/DbE;->a:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2122305
    iget-object v4, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v5, LX/DbE;->a:Landroid/net/Uri;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v4, v5, v1, v2, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2122306
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "reseting queue flags for # rows: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2122307
    return v0
.end method

.method public final a(IZ)LX/ES5;
    .locals 14

    .prologue
    .line 2122278
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v9

    .line 2122279
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v10

    .line 2122280
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 2122281
    invoke-static {p0}, LX/ES8;->d(LX/ES8;)J

    move-result-wide v12

    .line 2122282
    sget-object v2, LX/ES7;->NO_UPGRADES:LX/ES7;

    sget-object v4, LX/ES4;->FIRST:LX/ES4;

    sget-object v5, LX/ES6;->NONE:LX/ES6;

    const-wide/16 v6, 0x0

    move-object v1, p0

    move v3, p1

    invoke-static/range {v1 .. v7}, LX/ES8;->a(LX/ES8;LX/ES7;ILX/ES4;LX/ES6;J)Ljava/util/List;

    move-result-object v1

    .line 2122283
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2122284
    iget-wide v4, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->c:J

    cmp-long v3, v4, v12

    if-ltz v3, :cond_0

    .line 2122285
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2122286
    :cond_0
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2122287
    :cond_1
    if-eqz p2, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, p1, :cond_2

    .line 2122288
    sget-object v2, LX/ES7;->UPGRADE_ONLY:LX/ES7;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    sub-int v3, p1, v0

    sget-object v4, LX/ES4;->FIRST:LX/ES4;

    sget-object v5, LX/ES6;->NONE:LX/ES6;

    const-wide/16 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v7}, LX/ES8;->a(LX/ES8;LX/ES7;ILX/ES4;LX/ES6;J)Ljava/util/List;

    move-result-object v0

    .line 2122289
    :goto_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    invoke-interface {v10}, Ljava/util/List;->size()I

    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 2122290
    invoke-interface {v10, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2122291
    new-instance v0, LX/ES5;

    invoke-direct {v0, p0, v9, v10}, LX/ES5;-><init>(LX/ES8;Ljava/util/List;Ljava/util/List;)V

    return-object v0

    :cond_2
    move-object v0, v8

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/vault/provider/VaultImageProviderRow;
    .locals 2

    .prologue
    .line 2122272
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2122273
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2122274
    invoke-virtual {p0, v0}, LX/ES8;->a(Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    .line 2122275
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2122276
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2122277
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JIZ)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIZ)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2122240
    const-string v0, "%s DESC"

    new-array v1, v7, [Ljava/lang/Object;

    sget-object v2, LX/DbD;->c:LX/0U1;

    .line 2122241
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2122242
    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2122243
    const-string v0, "%s >= ?"

    sget-object v1, LX/DbD;->c:LX/0U1;

    .line 2122244
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2122245
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2122246
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2122247
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122248
    invoke-static {}, LX/ES8;->e()Ljava/lang/String;

    move-result-object v1

    .line 2122249
    const-string v2, "(%s) and (%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2122250
    iget-object v0, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/DbE;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-interface {v4, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    check-cast v4, [Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2122251
    invoke-static {v0}, LX/ES8;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v2

    .line 2122252
    if-eqz p4, :cond_3

    .line 2122253
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 2122254
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v6

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2122255
    iget-object v0, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2122256
    add-int/lit8 v0, v1, 0x1

    if-ge v0, p3, :cond_0

    move v1, v0

    .line 2122257
    goto :goto_0

    .line 2122258
    :cond_0
    iget-object v0, p0, LX/ES8;->e:LX/ERj;

    iget-object v1, p0, LX/ES8;->g:LX/2TL;

    invoke-virtual {v1}, LX/2TL;->a()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, LX/ERj;->a(Ljava/util/Set;J)Ljava/util/Map;

    move-result-object v0

    .line 2122259
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2122260
    const/4 v1, 0x0

    move v4, v1

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_2

    .line 2122261
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2122262
    iget-object v3, v1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;

    .line 2122263
    if-eqz v3, :cond_1

    .line 2122264
    iget-boolean v3, v3, Lcom/facebook/vault/protocol/VaultGetSyncedImageStatus;->mDeleted:Z

    if-eqz v3, :cond_1

    .line 2122265
    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2122266
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122267
    :cond_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 2122268
    :cond_2
    move-object v0, v5

    .line 2122269
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 2122270
    invoke-virtual {p0, v0}, LX/ES8;->a(Ljava/util/List;)V

    .line 2122271
    :cond_3
    return-object v2
.end method

.method public final a(Ljava/util/Set;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2122231
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2122232
    invoke-static {p1, v4}, LX/ES8;->a(Ljava/util/Set;Ljava/util/List;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 2122233
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 2122234
    const-string v1, "%s DESC"

    new-array v3, v7, [Ljava/lang/Object;

    sget-object v5, LX/DbD;->c:LX/0U1;

    .line 2122235
    iget-object p1, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, p1

    .line 2122236
    aput-object v5, v3, v6

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2122237
    const-string v1, "%s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2122238
    iget-object v0, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v1, LX/DbE;->a:Landroid/net/Uri;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2122239
    :cond_0
    invoke-static {v2}, LX/ES8;->a(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2122224
    iget-object v0, p0, LX/ES8;->h:LX/ERL;

    invoke-virtual {v0}, LX/ERL;->a()J

    move-result-wide v2

    .line 2122225
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 2122226
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2122227
    :cond_0
    sget-object v5, LX/ERl;->BEFORE:LX/ERl;

    const/4 v6, 0x4

    move-object v1, p0

    move v4, p1

    invoke-static/range {v1 .. v6}, LX/ES8;->a(LX/ES8;JILX/ERl;I)Ljava/util/Map;

    move-result-object v0

    .line 2122228
    invoke-direct {p0, v0}, LX/ES8;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 2122229
    iget-object v1, p0, LX/ES8;->h:LX/ERL;

    invoke-direct {p0}, LX/ES8;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/ERL;->a(J)V

    .line 2122230
    return-object v0
.end method

.method public final a(Lcom/facebook/vault/provider/VaultImageProviderRow;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 2122216
    iput v5, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->h:I

    .line 2122217
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->i:J

    .line 2122218
    invoke-virtual {p1}, Lcom/facebook/vault/provider/VaultImageProviderRow;->b()Landroid/content/ContentValues;

    move-result-object v0

    .line 2122219
    new-array v1, v5, [Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    aput-object v2, v1, v7

    .line 2122220
    iget-object v2, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    sget-object v3, LX/DbE;->a:Landroid/net/Uri;

    const-string v4, "%s = ?"

    new-array v5, v5, [Ljava/lang/Object;

    sget-object v6, LX/DbD;->a:LX/0U1;

    .line 2122221
    iget-object p0, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, p0

    .line 2122222
    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2122223
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/vault/provider/VaultImageProviderRow;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2122201
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 2122202
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "?"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2122203
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2122204
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/vault/provider/VaultImageProviderRow;

    .line 2122205
    iget-object v4, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2122206
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2122207
    iget-object v0, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2122208
    :cond_1
    const-string v4, ",?"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2122209
    iget-object v0, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2122210
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v4, 0x3e7

    if-ne v0, v4, :cond_0

    .line 2122211
    invoke-direct {p0, v1, v2}, LX/ES8;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    .line 2122212
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 2122213
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2122214
    invoke-direct {p0, v1, v2}, LX/ES8;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    .line 2122215
    :cond_4
    return-void
.end method

.method public final b(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2122195
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2122196
    sget-object v1, LX/DbD;->h:LX/0U1;

    .line 2122197
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2122198
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2122199
    iget-object v1, p0, LX/ES8;->f:Landroid/content/ContentResolver;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/DbE;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2122200
    return v0
.end method

.method public final b(J)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2122189
    const-string v0, "%s >= ?"

    sget-object v1, LX/DbD;->c:LX/0U1;

    .line 2122190
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2122191
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2122192
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2122193
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2122194
    invoke-static {p0, v0, v1}, LX/ES8;->a(LX/ES8;Ljava/lang/String;Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 2122176
    iget-object v0, p0, LX/ES8;->g:LX/2TL;

    invoke-virtual {v0}, LX/2TL;->a()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 2122177
    iget-object v0, p0, LX/ES8;->j:LX/03V;

    const-string v1, "vault_table_refresh missing device_oid"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2122178
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2122179
    :cond_0
    :goto_0
    return-object v0

    .line 2122180
    :cond_1
    iget-object v0, p0, LX/ES8;->h:LX/ERL;

    invoke-virtual {v0}, LX/ERL;->a()J

    move-result-wide v2

    .line 2122181
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 2122182
    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 2122183
    invoke-direct {p0}, LX/ES8;->f()J

    move-result-wide v2

    const/16 v4, 0x3e8

    sget-object v5, LX/ERl;->AFTER:LX/ERl;

    const/4 v6, 0x4

    move-object v1, p0

    invoke-static/range {v1 .. v6}, LX/ES8;->a(LX/ES8;JILX/ERl;I)Ljava/util/Map;

    move-result-object v0

    .line 2122184
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 2122185
    invoke-direct {p0, v0}, LX/ES8;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    .line 2122186
    iget-object v1, p0, LX/ES8;->h:LX/ERL;

    invoke-direct {p0}, LX/ES8;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/ERL;->a(J)V

    .line 2122187
    :cond_2
    sget-object v1, LX/ES8;->a:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2122188
    invoke-direct {p0}, LX/ES8;->g()V

    goto :goto_0
.end method

.method public final c()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 2122169
    iget-object v1, p0, LX/ES8;->h:LX/ERL;

    invoke-virtual {v1}, LX/ERL;->a()J

    move-result-wide v2

    .line 2122170
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 2122171
    iget-object v1, p0, LX/ES8;->c:LX/2TN;

    invoke-virtual {v1}, LX/2TN;->a()I

    move-result v1

    if-lez v1, :cond_1

    .line 2122172
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v7

    .line 2122173
    goto :goto_0

    .line 2122174
    :cond_2
    const/4 v4, 0x6

    sget-object v5, LX/ERl;->BEFORE:LX/ERl;

    const/4 v6, 0x4

    move-object v1, p0

    invoke-static/range {v1 .. v6}, LX/ES8;->a(LX/ES8;JILX/ERl;I)Ljava/util/Map;

    move-result-object v1

    .line 2122175
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-gtz v1, :cond_0

    move v0, v7

    goto :goto_0
.end method
