.class public LX/Et3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:LX/Esf;

.field public final b:LX/17W;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/03V;

.field public final e:Landroid/content/Context;

.field public final f:Landroid/content/res/Resources;

.field public final g:LX/Es5;

.field public final h:LX/0Uh;

.field public final i:LX/0ad;

.field public final j:LX/1Cn;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Esf;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/03V;Landroid/content/res/Resources;LX/Es5;LX/0Uh;LX/0ad;LX/1Cn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2177071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2177072
    iput-object p1, p0, LX/Et3;->e:Landroid/content/Context;

    .line 2177073
    iput-object p2, p0, LX/Et3;->a:LX/Esf;

    .line 2177074
    iput-object p3, p0, LX/Et3;->b:LX/17W;

    .line 2177075
    iput-object p4, p0, LX/Et3;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2177076
    iput-object p5, p0, LX/Et3;->d:LX/03V;

    .line 2177077
    iput-object p6, p0, LX/Et3;->f:Landroid/content/res/Resources;

    .line 2177078
    iput-object p7, p0, LX/Et3;->g:LX/Es5;

    .line 2177079
    iput-object p8, p0, LX/Et3;->h:LX/0Uh;

    .line 2177080
    iput-object p9, p0, LX/Et3;->i:LX/0ad;

    .line 2177081
    iput-object p10, p0, LX/Et3;->j:LX/1Cn;

    .line 2177082
    return-void
.end method

.method public static a(LX/0QB;)LX/Et3;
    .locals 14

    .prologue
    .line 2177060
    const-class v1, LX/Et3;

    monitor-enter v1

    .line 2177061
    :try_start_0
    sget-object v0, LX/Et3;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2177062
    sput-object v2, LX/Et3;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2177063
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2177064
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2177065
    new-instance v3, LX/Et3;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/Esf;->a(LX/0QB;)LX/Esf;

    move-result-object v5

    check-cast v5, LX/Esf;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {v0}, LX/Es5;->a(LX/0QB;)LX/Es5;

    move-result-object v10

    check-cast v10, LX/Es5;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {v0}, LX/1Cn;->a(LX/0QB;)LX/1Cn;

    move-result-object v13

    check-cast v13, LX/1Cn;

    invoke-direct/range {v3 .. v13}, LX/Et3;-><init>(Landroid/content/Context;LX/Esf;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/03V;Landroid/content/res/Resources;LX/Es5;LX/0Uh;LX/0ad;LX/1Cn;)V

    .line 2177066
    move-object v0, v3

    .line 2177067
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2177068
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Et3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2177069
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2177070
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/Et3;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1Pm;",
            "Ljava/lang/String;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2177045
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2177046
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2177047
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    if-eqz v1, :cond_2

    .line 2177048
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 2177049
    :goto_0
    move-object v3, v1

    .line 2177050
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->o()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 2177051
    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    .line 2177052
    :cond_0
    new-instance v0, LX/Et2;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LX/Et2;-><init>(LX/Et3;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;LX/1Pm;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    move-object v0, v6

    .line 2177053
    goto :goto_1

    .line 2177054
    :cond_2
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    if-eqz v1, :cond_3

    .line 2177055
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    goto :goto_0

    .line 2177056
    :cond_3
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    if-eqz v1, :cond_4

    .line 2177057
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v1

    .line 2177058
    invoke-static {v1}, LX/6X1;->a(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    goto :goto_0

    .line 2177059
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/Et3;Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;LX/1Pm;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2177015
    iget-object v0, p0, LX/Et3;->j:LX/1Cn;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2177016
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/Et3;->e:Landroid/content/Context;

    const-class v2, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2177017
    const-string v1, "campaign_id"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2177018
    const-string v1, "campaign_type"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l()Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2177019
    const-string v1, "source"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2177020
    const-string v1, "direct_source"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2177021
    const-string v1, "share_preview"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2177022
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->q()Ljava/lang/String;

    move-result-object v1

    .line 2177023
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2177024
    const-string v2, "default_share_message"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2177025
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->s()Ljava/lang/String;

    move-result-object v1

    .line 2177026
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2177027
    const-string v2, "placeholder_text"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2177028
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->r()Ljava/lang/String;

    move-result-object v1

    .line 2177029
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2177030
    const-string v2, "share_preview_title"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2177031
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->o()LX/0Px;

    move-result-object v1

    .line 2177032
    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2177033
    const-string v2, "tagged_users"

    invoke-static {v1}, LX/CFE;->a(Ljava/util/List;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2177034
    :cond_3
    const-string v1, "composer_source_surface"

    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-static {v2}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2177035
    iget-object v1, p0, LX/Et3;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Et3;->e:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2177036
    return-void
.end method

.method public static b(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2177037
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    if-eqz v0, :cond_0

    .line 2177038
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 2177039
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->m()Ljava/lang/String;

    move-result-object v0

    .line 2177040
    :goto_0
    return-object v0

    .line 2177041
    :cond_0
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    if-eqz v0, :cond_1

    .line 2177042
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2177043
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2177044
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
