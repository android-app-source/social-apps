.class public final enum LX/Dfd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dfd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dfd;

.field public static final enum COLLAPSED_UNIT:LX/Dfd;

.field public static final enum CREATE_ROOM:LX/Dfd;

.field public static final enum MORE_FOOTER:LX/Dfd;

.field public static final enum SECTION_HEADER:LX/Dfd;


# instance fields
.field public final analyticsString:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2027600
    new-instance v0, LX/Dfd;

    const-string v1, "SECTION_HEADER"

    const-string v2, "h"

    invoke-direct {v0, v1, v3, v2}, LX/Dfd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Dfd;->SECTION_HEADER:LX/Dfd;

    .line 2027601
    new-instance v0, LX/Dfd;

    const-string v1, "MORE_FOOTER"

    const-string v2, "m"

    invoke-direct {v0, v1, v4, v2}, LX/Dfd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Dfd;->MORE_FOOTER:LX/Dfd;

    .line 2027602
    new-instance v0, LX/Dfd;

    const-string v1, "COLLAPSED_UNIT"

    const-string v2, "c"

    invoke-direct {v0, v1, v5, v2}, LX/Dfd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Dfd;->COLLAPSED_UNIT:LX/Dfd;

    .line 2027603
    new-instance v0, LX/Dfd;

    const-string v1, "CREATE_ROOM"

    const-string v2, "r"

    invoke-direct {v0, v1, v6, v2}, LX/Dfd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Dfd;->CREATE_ROOM:LX/Dfd;

    .line 2027604
    const/4 v0, 0x4

    new-array v0, v0, [LX/Dfd;

    sget-object v1, LX/Dfd;->SECTION_HEADER:LX/Dfd;

    aput-object v1, v0, v3

    sget-object v1, LX/Dfd;->MORE_FOOTER:LX/Dfd;

    aput-object v1, v0, v4

    sget-object v1, LX/Dfd;->COLLAPSED_UNIT:LX/Dfd;

    aput-object v1, v0, v5

    sget-object v1, LX/Dfd;->CREATE_ROOM:LX/Dfd;

    aput-object v1, v0, v6

    sput-object v0, LX/Dfd;->$VALUES:[LX/Dfd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2027597
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2027598
    iput-object p3, p0, LX/Dfd;->analyticsString:Ljava/lang/String;

    .line 2027599
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dfd;
    .locals 1

    .prologue
    .line 2027605
    const-class v0, LX/Dfd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dfd;

    return-object v0
.end method

.method public static values()[LX/Dfd;
    .locals 1

    .prologue
    .line 2027596
    sget-object v0, LX/Dfd;->$VALUES:[LX/Dfd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dfd;

    return-object v0
.end method
