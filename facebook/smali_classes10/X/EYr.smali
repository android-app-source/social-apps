.class public LX/EYr;
.super Ljava/io/IOException;
.source ""


# instance fields
.field public unfinishedMessage:LX/EWW;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2138111
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 2138112
    const/4 v0, 0x0

    iput-object v0, p0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2138113
    return-void
.end method

.method public static b()LX/EYr;
    .locals 2

    .prologue
    .line 2138110
    new-instance v0, LX/EYr;

    const-string v1, "While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length."

    invoke-direct {v0, v1}, LX/EYr;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static c()LX/EYr;
    .locals 2

    .prologue
    .line 2138109
    new-instance v0, LX/EYr;

    const-string v1, "CodedInputStream encountered an embedded string or message which claimed to have negative size."

    invoke-direct {v0, v1}, LX/EYr;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static d()LX/EYr;
    .locals 2

    .prologue
    .line 2138106
    new-instance v0, LX/EYr;

    const-string v1, "CodedInputStream encountered a malformed varint."

    invoke-direct {v0, v1}, LX/EYr;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static g()LX/EYr;
    .locals 2

    .prologue
    .line 2138108
    new-instance v0, LX/EYr;

    const-string v1, "Protocol message tag had invalid wire type."

    invoke-direct {v0, v1}, LX/EYr;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static h()LX/EYr;
    .locals 2

    .prologue
    .line 2138107
    new-instance v0, LX/EYr;

    const-string v1, "Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit."

    invoke-direct {v0, v1}, LX/EYr;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
