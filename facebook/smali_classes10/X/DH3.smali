.class public final LX/DH3;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3Dm;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/DGK;

.field public b:LX/1Pd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public final synthetic g:LX/3Dm;


# direct methods
.method public constructor <init>(LX/3Dm;)V
    .locals 1

    .prologue
    .line 1981216
    iput-object p1, p0, LX/DH3;->g:LX/3Dm;

    .line 1981217
    move-object v0, p1

    .line 1981218
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1981219
    sget v0, LX/DH4;->a:I

    iput v0, p0, LX/DH3;->c:I

    .line 1981220
    sget v0, LX/DH4;->b:I

    iput v0, p0, LX/DH3;->d:I

    .line 1981221
    sget v0, LX/DH4;->c:I

    iput v0, p0, LX/DH3;->e:I

    .line 1981222
    const v0, 0x7fffffff

    iput v0, p0, LX/DH3;->f:I

    .line 1981223
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1981224
    const-string v0, "NetEgoStorySetPageHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1981225
    if-ne p0, p1, :cond_1

    .line 1981226
    :cond_0
    :goto_0
    return v0

    .line 1981227
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1981228
    goto :goto_0

    .line 1981229
    :cond_3
    check-cast p1, LX/DH3;

    .line 1981230
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1981231
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1981232
    if-eq v2, v3, :cond_0

    .line 1981233
    iget-object v2, p0, LX/DH3;->a:LX/DGK;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DH3;->a:LX/DGK;

    iget-object v3, p1, LX/DH3;->a:LX/DGK;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1981234
    goto :goto_0

    .line 1981235
    :cond_5
    iget-object v2, p1, LX/DH3;->a:LX/DGK;

    if-nez v2, :cond_4

    .line 1981236
    :cond_6
    iget-object v2, p0, LX/DH3;->b:LX/1Pd;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DH3;->b:LX/1Pd;

    iget-object v3, p1, LX/DH3;->b:LX/1Pd;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1981237
    goto :goto_0

    .line 1981238
    :cond_8
    iget-object v2, p1, LX/DH3;->b:LX/1Pd;

    if-nez v2, :cond_7

    .line 1981239
    :cond_9
    iget v2, p0, LX/DH3;->c:I

    iget v3, p1, LX/DH3;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1981240
    goto :goto_0

    .line 1981241
    :cond_a
    iget v2, p0, LX/DH3;->d:I

    iget v3, p1, LX/DH3;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1981242
    goto :goto_0

    .line 1981243
    :cond_b
    iget v2, p0, LX/DH3;->e:I

    iget v3, p1, LX/DH3;->e:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1981244
    goto :goto_0

    .line 1981245
    :cond_c
    iget v2, p0, LX/DH3;->f:I

    iget v3, p1, LX/DH3;->f:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1981246
    goto :goto_0
.end method
