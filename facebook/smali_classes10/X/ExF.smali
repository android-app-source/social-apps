.class public final LX/ExF;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/Euc;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2184226
    iput-object p1, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2184227
    iget-object v0, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->z:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->g()V

    .line 2184228
    iget-object v0, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->a$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;Ljava/lang/Throwable;)V

    .line 2184229
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2184230
    check-cast p1, LX/Euc;

    const/4 v0, 0x0

    .line 2184231
    iget-object v1, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->z:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v1}, LX/62l;->f()V

    .line 2184232
    iget-object v1, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-static {v1}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->d$redex0(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    .line 2184233
    iget-object v1, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->L:LX/Eui;

    invoke-virtual {v1}, LX/Eui;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2184234
    iget-object v1, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    invoke-virtual {v1, v0}, LX/Exj;->a(Z)V

    .line 2184235
    :cond_0
    iget-object v1, p1, LX/Euc;->a:LX/0Px;

    move-object v1, v1

    .line 2184236
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2184237
    iget-object v0, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 2184238
    iget-object v0, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->c(Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;)V

    .line 2184239
    :goto_0
    return-void

    .line 2184240
    :cond_1
    iget-object v1, p1, LX/Euc;->a:LX/0Px;

    move-object v2, v1

    .line 2184241
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2184242
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v4, v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->m()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2184243
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2184244
    iget-object v6, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v6, v6, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2184245
    iget-object v6, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v6, v6, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2184246
    iget-object v5, p1, LX/Euc;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2184247
    invoke-static {v0, v5}, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->b(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;Ljava/lang/String;)LX/Eus;

    move-result-object v0

    invoke-virtual {v6, v4, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2184248
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2184249
    :cond_3
    iget-object v0, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    instance-of v0, v0, LX/Exw;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2184250
    iget-object v0, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->y:LX/Exj;

    check-cast v0, LX/Exw;

    iget-object v1, p0, LX/ExF;->a:Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/suggestions/FriendsCenterSuggestionsFragment;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Exw;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method
