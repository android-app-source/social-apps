.class public LX/EKU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/EKv;

.field public final c:LX/EKf;

.field public final d:LX/EKj;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const v4, -0xcac97

    const v3, -0xaa7201

    .line 2106859
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "dem"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "Democratic"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "gop"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "Republican"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/EKU;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/EKv;LX/EKf;LX/EKj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2106872
    iput-object p1, p0, LX/EKU;->b:LX/EKv;

    .line 2106873
    iput-object p2, p0, LX/EKU;->c:LX/EKf;

    .line 2106874
    iput-object p3, p0, LX/EKU;->d:LX/EKj;

    .line 2106875
    return-void
.end method

.method public static a(LX/0QB;)LX/EKU;
    .locals 6

    .prologue
    .line 2106860
    const-class v1, LX/EKU;

    monitor-enter v1

    .line 2106861
    :try_start_0
    sget-object v0, LX/EKU;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106862
    sput-object v2, LX/EKU;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106863
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106864
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106865
    new-instance p0, LX/EKU;

    invoke-static {v0}, LX/EKv;->a(LX/0QB;)LX/EKv;

    move-result-object v3

    check-cast v3, LX/EKv;

    invoke-static {v0}, LX/EKf;->a(LX/0QB;)LX/EKf;

    move-result-object v4

    check-cast v4, LX/EKf;

    invoke-static {v0}, LX/EKj;->a(LX/0QB;)LX/EKj;

    move-result-object v5

    check-cast v5, LX/EKj;

    invoke-direct {p0, v3, v4, v5}, LX/EKU;-><init>(LX/EKv;LX/EKf;LX/EKj;)V

    .line 2106866
    move-object v0, p0

    .line 2106867
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106868
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EKU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106869
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
