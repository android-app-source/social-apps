.class public LX/EJ0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LX/0Uh;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Uh;LX/0Ot;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2102829
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/EJ0;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2102830
    iput-object p1, p0, LX/EJ0;->a:Ljava/util/concurrent/ExecutorService;

    .line 2102831
    iput-object p2, p0, LX/EJ0;->b:LX/0Uh;

    .line 2102832
    iput-object p3, p0, LX/EJ0;->c:LX/0Ot;

    .line 2102833
    return-void
.end method
