.class public LX/Dww;
.super LX/3pF;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Landroid/os/Bundle;

.field public g:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;

.field public i:LX/Dwr;

.field private j:LX/0yc;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field public o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/photos/simplepicker/SimplePickerFragment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2062041
    const-class v0, LX/Dww;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Dww;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Ot;LX/0Or;LX/Dwr;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;LX/0gc;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;LX/0yc;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/Dwr;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "LX/0gc;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/lang/String;",
            "LX/0yc;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2062021
    invoke-direct {p0, p8}, LX/3pF;-><init>(LX/0gc;)V

    .line 2062022
    iput-object p2, p0, LX/Dww;->b:LX/0Ot;

    .line 2062023
    iput-object p1, p0, LX/Dww;->c:Ljava/lang/String;

    .line 2062024
    iput-object p5, p0, LX/Dww;->d:Ljava/lang/String;

    .line 2062025
    iput-object p6, p0, LX/Dww;->e:Ljava/lang/String;

    .line 2062026
    iput-object p7, p0, LX/Dww;->f:Landroid/os/Bundle;

    .line 2062027
    iput-object p9, p0, LX/Dww;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 2062028
    iput-object p10, p0, LX/Dww;->h:Ljava/lang/String;

    .line 2062029
    iput-object p4, p0, LX/Dww;->i:LX/Dwr;

    .line 2062030
    iput-object p11, p0, LX/Dww;->j:LX/0yc;

    .line 2062031
    iget-object v0, p0, LX/Dww;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062032
    iget-object v0, p0, LX/Dww;->c:Ljava/lang/String;

    iput-object v0, p0, LX/Dww;->d:Ljava/lang/String;

    .line 2062033
    :cond_0
    iget-object v0, p0, LX/Dww;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dww;->c:Ljava/lang/String;

    invoke-static {p5, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2062034
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Dww;->e:Ljava/lang/String;

    .line 2062035
    :cond_1
    const-string v0, "disable_tagged_media_set"

    invoke-virtual {p7, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LX/Dww;->d:Ljava/lang/String;

    iget-object v2, p0, LX/Dww;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "has_tagged_mediaset"

    invoke-virtual {p7, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/Dww;->k:Z

    .line 2062036
    const-string v0, "extra_should_merge_camera_roll"

    invoke-virtual {p7, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/Dww;->l:Z

    .line 2062037
    const-string v0, "extra_should_show_suggested_photos"

    invoke-virtual {p7, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Dww;->m:Z

    .line 2062038
    const-string v0, "extra_should_show_suggested_photos_before_camera_roll"

    invoke-virtual {p7, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Dww;->n:Z

    .line 2062039
    return-void

    :cond_3
    move v0, v1

    .line 2062040
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2062020
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2062015
    iget-object v0, p0, LX/Dww;->f:Landroid/os/Bundle;

    .line 2062016
    iget-object v1, p0, LX/Dww;->f:Landroid/os/Bundle;

    const-string v2, "extra_simple_picker_launcher_configuration"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-object v1, v1

    .line 2062017
    iget-object v2, p0, LX/Dww;->h:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Landroid/os/Bundle;Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;Ljava/lang/String;)Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    move-result-object v0

    .line 2062018
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/Dww;->p:Ljava/lang/ref/WeakReference;

    .line 2062019
    return-object v0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 2062042
    iget-object v0, p0, LX/Dww;->c:Ljava/lang/String;

    iget-object v1, p0, LX/Dww;->d:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2061964
    iget-boolean v0, p0, LX/Dww;->l:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 2061965
    :goto_0
    iget-boolean v3, p0, LX/Dww;->m:Z

    if-eqz v3, :cond_1

    move v3, v1

    .line 2061966
    :goto_1
    iget-boolean v4, p0, LX/Dww;->k:Z

    if-eqz v4, :cond_2

    move v4, v1

    .line 2061967
    :goto_2
    iget-boolean v5, p0, LX/Dww;->m:Z

    if-eqz v5, :cond_4

    iget-boolean v5, p0, LX/Dww;->n:Z

    if-eqz v5, :cond_4

    .line 2061968
    if-nez p1, :cond_3

    .line 2061969
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f08127f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2061970
    :goto_3
    return-object v0

    :cond_0
    move v0, v2

    .line 2061971
    goto :goto_0

    :cond_1
    move v3, v2

    .line 2061972
    goto :goto_1

    :cond_2
    move v4, v2

    .line 2061973
    goto :goto_2

    .line 2061974
    :cond_3
    if-ne p1, v2, :cond_7

    iget-boolean v2, p0, LX/Dww;->l:Z

    if-eqz v2, :cond_7

    .line 2061975
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f08127e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 2061976
    :cond_4
    iget-boolean v5, p0, LX/Dww;->m:Z

    if-eqz v5, :cond_6

    iget-boolean v5, p0, LX/Dww;->n:Z

    if-nez v5, :cond_6

    .line 2061977
    if-nez p1, :cond_5

    .line 2061978
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f08127e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 2061979
    :cond_5
    if-ne p1, v2, :cond_7

    .line 2061980
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f08127f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 2061981
    :cond_6
    iget-boolean v2, p0, LX/Dww;->m:Z

    if-nez v2, :cond_7

    if-nez p1, :cond_7

    iget-boolean v2, p0, LX/Dww;->l:Z

    if-eqz v2, :cond_7

    .line 2061982
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f08127e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 2061983
    :cond_7
    add-int/lit8 v2, v3, 0x0

    .line 2061984
    add-int/2addr v0, v2

    .line 2061985
    rsub-int/lit8 v2, v0, 0x2

    if-ne p1, v2, :cond_c

    iget-boolean v2, p0, LX/Dww;->k:Z

    if-eqz v2, :cond_c

    .line 2061986
    invoke-direct {p0}, LX/Dww;->i()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2061987
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f081286

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2061988
    :cond_8
    iget-object v0, p0, LX/Dww;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2061989
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f081288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2061990
    :cond_9
    iget-object v0, p0, LX/Dww;->e:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_a

    iget-object v0, p0, LX/Dww;->e:Ljava/lang/String;

    iget-object v2, p0, LX/Dww;->e:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2061991
    :goto_4
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v2, 0x7f081288

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_b

    :goto_5
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2061992
    :cond_a
    iget-object v0, p0, LX/Dww;->e:Ljava/lang/String;

    move-object v1, v0

    goto :goto_4

    .line 2061993
    :cond_b
    const-string v1, ""

    goto :goto_5

    .line 2061994
    :cond_c
    add-int/2addr v0, v4

    .line 2061995
    rsub-int/lit8 v1, v0, 0x3

    if-ne p1, v1, :cond_d

    .line 2061996
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f081280

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2061997
    :cond_d
    rsub-int/lit8 v1, v0, 0x4

    if-ne p1, v1, :cond_e

    .line 2061998
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f081281

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2061999
    :cond_e
    rsub-int/lit8 v0, v0, 0x5

    if-ne p1, v0, :cond_f

    .line 2062000
    iget-object v0, p0, LX/Dww;->i:LX/Dwr;

    iget-object v1, p0, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-virtual {v0, v1}, LX/Dwr;->a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)LX/Dwq;

    move-result-object v0

    .line 2062001
    sget-object v1, LX/Dwv;->a:[I

    invoke-virtual {v0}, LX/Dwq;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2062002
    sget-object v1, LX/Dww;->a:Ljava/lang/String;

    const-string v2, "Unknown sync tab state: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2062003
    const-string v0, ""

    :goto_6
    move-object v0, v0

    .line 2062004
    goto/16 :goto_3

    .line 2062005
    :cond_f
    const-string v0, ""

    goto/16 :goto_3

    .line 2062006
    :pswitch_0
    const-string v0, ""

    goto :goto_6

    .line 2062007
    :pswitch_1
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f081282

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 2062008
    :pswitch_2
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f081283

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 2062009
    :pswitch_3
    iget-object v0, p0, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    if-eqz v0, :cond_10

    iget-object v0, p0, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2062010
    iget-boolean v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->d:Z

    move v0, v1

    .line 2062011
    if-eqz v0, :cond_10

    .line 2062012
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f081284

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 2062013
    :cond_10
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f081282

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 2062014
    :pswitch_4
    iget-object v0, p0, LX/Dww;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const v1, 0x7f081284

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Dww;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    const/4 v0, -0x2

    .line 2061959
    iget-object v1, p0, LX/Dww;->i:LX/Dwr;

    iget-object v2, p0, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-virtual {v1, v2}, LX/Dwr;->a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)LX/Dwq;

    move-result-object v1

    .line 2061960
    instance-of v2, p1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    if-eqz v2, :cond_0

    sget-object v2, LX/Dwq;->SYNC:LX/Dwq;

    if-ne v1, v2, :cond_4

    :cond_0
    instance-of v2, p1, Lcom/facebook/photos/pandora/ui/PandoraSyncTabLoadingFragment;

    if-eqz v2, :cond_1

    sget-object v2, LX/Dwq;->LOADING:LX/Dwq;

    if-ne v1, v2, :cond_4

    :cond_1
    instance-of v2, p1, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;

    if-eqz v2, :cond_2

    sget-object v2, LX/Dwq;->MOMENTS_PROMOTION:LX/Dwq;

    if-ne v1, v2, :cond_4

    :cond_2
    instance-of v2, p1, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;

    if-eqz v2, :cond_3

    sget-object v2, LX/Dwq;->MOMENTS_SEGUE:LX/Dwq;

    if-ne v1, v2, :cond_4

    :cond_3
    instance-of v2, p1, Lcom/facebook/vault/ui/VaultSyncNotSupportedFragment;

    if-eqz v2, :cond_5

    sget-object v2, LX/Dwq;->SYNC_UNSUPPORTED:LX/Dwq;

    if-eq v1, v2, :cond_5

    .line 2061961
    :cond_4
    :goto_0
    return v0

    .line 2061962
    :cond_5
    instance-of v1, p1, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    if-nez v1, :cond_4

    .line 2061963
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2061866
    iget-object v0, p0, LX/Dww;->j:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dww;->j:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->k()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2061867
    :goto_0
    iget-boolean v3, p0, LX/Dww;->l:Z

    if-eqz v3, :cond_1

    move v3, v2

    .line 2061868
    :goto_1
    iget-boolean v4, p0, LX/Dww;->m:Z

    if-eqz v4, :cond_2

    move v4, v2

    .line 2061869
    :goto_2
    iget-boolean v5, p0, LX/Dww;->k:Z

    if-eqz v5, :cond_3

    move v5, v2

    .line 2061870
    :goto_3
    iget-boolean v6, p0, LX/Dww;->m:Z

    if-eqz v6, :cond_5

    iget-boolean v6, p0, LX/Dww;->n:Z

    if-eqz v6, :cond_5

    .line 2061871
    if-nez p1, :cond_4

    .line 2061872
    iget-object v0, p0, LX/Dww;->f:Landroid/os/Bundle;

    iget-object v1, p0, LX/Dww;->d:Ljava/lang/String;

    iget-object v2, p0, LX/Dww;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v0, v1, v2}, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;

    move-result-object v0

    .line 2061873
    :goto_4
    return-object v0

    :cond_0
    move v0, v2

    .line 2061874
    goto :goto_0

    :cond_1
    move v3, v1

    .line 2061875
    goto :goto_1

    :cond_2
    move v4, v1

    .line 2061876
    goto :goto_2

    :cond_3
    move v5, v1

    .line 2061877
    goto :goto_3

    .line 2061878
    :cond_4
    if-ne p1, v1, :cond_8

    iget-boolean v1, p0, LX/Dww;->l:Z

    if-eqz v1, :cond_8

    .line 2061879
    invoke-direct {p0}, LX/Dww;->e()Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_4

    .line 2061880
    :cond_5
    iget-boolean v6, p0, LX/Dww;->m:Z

    if-eqz v6, :cond_7

    iget-boolean v6, p0, LX/Dww;->n:Z

    if-nez v6, :cond_7

    .line 2061881
    if-nez p1, :cond_6

    .line 2061882
    invoke-direct {p0}, LX/Dww;->e()Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_4

    .line 2061883
    :cond_6
    if-ne p1, v1, :cond_8

    .line 2061884
    iget-object v0, p0, LX/Dww;->f:Landroid/os/Bundle;

    iget-object v1, p0, LX/Dww;->d:Ljava/lang/String;

    iget-object v2, p0, LX/Dww;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v0, v1, v2}, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;

    move-result-object v0

    goto :goto_4

    .line 2061885
    :cond_7
    iget-boolean v1, p0, LX/Dww;->m:Z

    if-nez v1, :cond_8

    if-nez p1, :cond_8

    iget-boolean v1, p0, LX/Dww;->l:Z

    if-eqz v1, :cond_8

    .line 2061886
    invoke-direct {p0}, LX/Dww;->e()Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_4

    .line 2061887
    :cond_8
    add-int/lit8 v1, v4, 0x0

    .line 2061888
    add-int/2addr v1, v3

    .line 2061889
    rsub-int/lit8 v3, v1, 0x2

    if-ne p1, v3, :cond_9

    iget-boolean v3, p0, LX/Dww;->k:Z

    if-eqz v3, :cond_9

    .line 2061890
    if-eqz v0, :cond_d

    sget-object v1, LX/DwS;->TAGGED:LX/DwS;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Dww;->d:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->a(LX/DwS;ILjava/lang/String;)Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    move-result-object v1

    :goto_5
    move-object v0, v1

    .line 2061891
    goto :goto_4

    .line 2061892
    :cond_9
    add-int/2addr v1, v5

    .line 2061893
    rsub-int/lit8 v3, v1, 0x3

    if-ne p1, v3, :cond_a

    .line 2061894
    const/4 v4, 0x0

    .line 2061895
    if-eqz v0, :cond_11

    sget-object v1, LX/DwS;->UPLOADED:LX/DwS;

    iget-object v2, p0, LX/Dww;->d:Ljava/lang/String;

    invoke-static {v1, v4, v2}, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->a(LX/DwS;ILjava/lang/String;)Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    move-result-object v1

    :goto_6
    move-object v0, v1

    .line 2061896
    goto :goto_4

    .line 2061897
    :cond_a
    rsub-int/lit8 v0, v1, 0x4

    if-ne p1, v0, :cond_b

    .line 2061898
    iget-object v0, p0, LX/Dww;->f:Landroid/os/Bundle;

    iget-object v1, p0, LX/Dww;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a(Landroid/os/Bundle;Ljava/lang/String;Z)Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    move-result-object v0

    goto :goto_4

    .line 2061899
    :cond_b
    rsub-int/lit8 v0, v1, 0x5

    if-ne p1, v0, :cond_c

    .line 2061900
    const/4 v0, 0x0

    .line 2061901
    iget-object v1, p0, LX/Dww;->i:LX/Dwr;

    iget-object v2, p0, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-virtual {v1, v2}, LX/Dwr;->a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)LX/Dwq;

    move-result-object v1

    .line 2061902
    sget-object v2, LX/Dwv;->a:[I

    invoke-virtual {v1}, LX/Dwq;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2061903
    sget-object v2, LX/Dww;->a:Ljava/lang/String;

    const-string v3, "Unknown sync tab state: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2061904
    :goto_7
    :pswitch_0
    move-object v0, v0

    .line 2061905
    goto/16 :goto_4

    .line 2061906
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_d
    iget-object v1, p0, LX/Dww;->f:Landroid/os/Bundle;

    iget-object v2, p0, LX/Dww;->d:Ljava/lang/String;

    iget-object v3, p0, LX/Dww;->e:Ljava/lang/String;

    iget-object v4, p0, LX/Dww;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 2061907
    new-instance p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;-><init>()V

    .line 2061908
    if-nez v1, :cond_e

    .line 2061909
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2061910
    :cond_e
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 2061911
    const-string v0, "userId"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2061912
    :cond_f
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2061913
    const-string v0, "userName"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2061914
    :cond_10
    const-string v0, "callerContext"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2061915
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2061916
    move-object v1, p0

    .line 2061917
    goto :goto_5

    :cond_11
    iget-object v1, p0, LX/Dww;->f:Landroid/os/Bundle;

    iget-object v2, p0, LX/Dww;->d:Ljava/lang/String;

    iget-object v3, p0, LX/Dww;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 2061918
    new-instance p0, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;

    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;-><init>()V

    .line 2061919
    if-nez v1, :cond_12

    .line 2061920
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2061921
    :cond_12
    invoke-static {v1, v2, v4, v3}, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;->b(Landroid/os/Bundle;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 2061922
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2061923
    move-object v1, p0

    .line 2061924
    goto/16 :goto_6

    .line 2061925
    :pswitch_1
    new-instance v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {v0}, Lcom/facebook/vault/ui/VaultSyncScreenFragment;-><init>()V

    move-object v0, v0

    .line 2061926
    goto :goto_7

    .line 2061927
    :pswitch_2
    new-instance v0, Lcom/facebook/photos/pandora/ui/PandoraSyncTabLoadingFragment;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/ui/PandoraSyncTabLoadingFragment;-><init>()V

    move-object v0, v0

    .line 2061928
    goto :goto_7

    .line 2061929
    :pswitch_3
    iget-object v0, p0, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2061930
    new-instance v1, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;

    invoke-direct {v1}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionFragment;-><init>()V

    .line 2061931
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2061932
    const-string v3, "arg_moments_app_info"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2061933
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2061934
    move-object v0, v1

    .line 2061935
    goto :goto_7

    .line 2061936
    :pswitch_4
    iget-object v0, p0, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2061937
    new-instance v1, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;

    invoke-direct {v1}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppSegueFragment;-><init>()V

    .line 2061938
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2061939
    const-string v3, "arg_moments_app_info"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2061940
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2061941
    move-object v0, v1

    .line 2061942
    goto/16 :goto_7

    .line 2061943
    :pswitch_5
    new-instance v0, Lcom/facebook/vault/ui/VaultSyncNotSupportedFragment;

    invoke-direct {v0}, Lcom/facebook/vault/ui/VaultSyncNotSupportedFragment;-><init>()V

    move-object v0, v0

    .line 2061944
    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 2061948
    const/4 v0, 0x2

    .line 2061949
    iget-boolean v1, p0, LX/Dww;->k:Z

    if-eqz v1, :cond_0

    .line 2061950
    const/4 v0, 0x3

    .line 2061951
    :cond_0
    iget-boolean v1, p0, LX/Dww;->l:Z

    if-eqz v1, :cond_1

    .line 2061952
    add-int/lit8 v0, v0, 0x1

    .line 2061953
    :cond_1
    iget-boolean v1, p0, LX/Dww;->m:Z

    if-eqz v1, :cond_2

    .line 2061954
    add-int/lit8 v0, v0, 0x1

    .line 2061955
    :cond_2
    iget-object v1, p0, LX/Dww;->i:LX/Dwr;

    iget-object v2, p0, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-virtual {v1, v2}, LX/Dwr;->a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)LX/Dwq;

    move-result-object v1

    sget-object v2, LX/Dwq;->NONE:LX/Dwq;

    if-eq v1, v2, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2061956
    if-eqz v1, :cond_3

    invoke-direct {p0}, LX/Dww;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2061957
    add-int/lit8 v0, v0, 0x1

    .line 2061958
    :cond_3
    return v0

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/facebook/photos/simplepicker/SimplePickerFragment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2061945
    iget-object v0, p0, LX/Dww;->p:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 2061946
    iget-object v0, p0, LX/Dww;->p:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 2061947
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
