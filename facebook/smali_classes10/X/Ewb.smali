.class public final LX/Ewb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterFetchRequestsGraphQLModels$FriendCenterRequestEdgeModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V
    .locals 0

    .prologue
    .line 2183291
    iput-object p1, p0, LX/Ewb;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2183292
    const/16 v3, 0x14

    .line 2183293
    iget-object v0, p0, LX/Ewb;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-boolean v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->H:Z

    if-eqz v0, :cond_0

    .line 2183294
    iget-object v0, p0, LX/Ewb;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aj:LX/EuY;

    sget-object v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    new-instance v2, LX/Ewa;

    invoke-direct {v2, p0}, LX/Ewa;-><init>(LX/Ewb;)V

    .line 2183295
    invoke-static {v0, v1, v3}, LX/EuY;->b(LX/EuY;Lcom/facebook/common/callercontext/CallerContext;I)LX/0zO;

    move-result-object v5

    .line 2183296
    iget-object v4, v0, LX/EuY;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2dl;

    iget-object v6, v0, LX/EuY;->e:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1My;

    new-instance v7, LX/EuW;

    invoke-direct {v7, v0, v2}, LX/EuW;-><init>(LX/EuY;LX/0TF;)V

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 2183297
    iget-object v9, v5, LX/0zO;->m:LX/0gW;

    move-object v9, v9

    .line 2183298
    iget-object v10, v9, LX/0gW;->f:Ljava/lang/String;

    move-object v9, v10

    .line 2183299
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, LX/EuY;->f:I

    add-int/lit8 v10, v9, 0x1

    iput v10, v0, LX/EuY;->f:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "FC_REQUESTS_QUERY"

    invoke-virtual/range {v4 .. v9}, LX/2dl;->a(LX/0zO;LX/1My;LX/0TF;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2183300
    invoke-static {v0, v4}, LX/EuY;->a(LX/EuY;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2183301
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Ewb;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->aj:LX/EuY;

    sget-object v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->j:Lcom/facebook/common/callercontext/CallerContext;

    .line 2183302
    iget-object v2, v0, LX/EuY;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2dl;

    invoke-static {v0, v1, v3}, LX/EuY;->b(LX/EuY;Lcom/facebook/common/callercontext/CallerContext;I)LX/0zO;

    move-result-object v4

    const-string v5, "FC_REQUESTS_QUERY"

    invoke-virtual {v2, v4, v5}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {v0, v2}, LX/EuY;->a(LX/EuY;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2183303
    goto :goto_0
.end method
