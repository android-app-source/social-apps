.class public LX/Eu4;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Eu2;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Eu6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2179010
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Eu4;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Eu6;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2179032
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2179033
    iput-object p1, p0, LX/Eu4;->b:LX/0Ot;

    .line 2179034
    return-void
.end method

.method public static a(LX/0QB;)LX/Eu4;
    .locals 4

    .prologue
    .line 2179021
    const-class v1, LX/Eu4;

    monitor-enter v1

    .line 2179022
    :try_start_0
    sget-object v0, LX/Eu4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2179023
    sput-object v2, LX/Eu4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2179024
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2179025
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2179026
    new-instance v3, LX/Eu4;

    const/16 p0, 0x222b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Eu4;-><init>(LX/0Ot;)V

    .line 2179027
    move-object v0, v3

    .line 2179028
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2179029
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Eu4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2179030
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2179031
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2179035
    check-cast p2, LX/Eu3;

    .line 2179036
    iget-object v0, p0, LX/Eu4;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eu6;

    iget-object v2, p2, LX/Eu3;->a:Ljava/lang/CharSequence;

    iget-object v3, p2, LX/Eu3;->b:Ljava/lang/CharSequence;

    iget-object v4, p2, LX/Eu3;->c:Ljava/lang/CharSequence;

    iget-object v5, p2, LX/Eu3;->d:LX/1dc;

    iget v6, p2, LX/Eu3;->e:I

    iget v7, p2, LX/Eu3;->f:I

    iget v8, p2, LX/Eu3;->g:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, LX/Eu6;->a(LX/1De;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dc;III)LX/1Dg;

    move-result-object v0

    .line 2179037
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2179019
    invoke-static {}, LX/1dS;->b()V

    .line 2179020
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/Eu2;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2179011
    new-instance v1, LX/Eu3;

    invoke-direct {v1, p0}, LX/Eu3;-><init>(LX/Eu4;)V

    .line 2179012
    sget-object v2, LX/Eu4;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Eu2;

    .line 2179013
    if-nez v2, :cond_0

    .line 2179014
    new-instance v2, LX/Eu2;

    invoke-direct {v2}, LX/Eu2;-><init>()V

    .line 2179015
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Eu2;->a$redex0(LX/Eu2;LX/1De;IILX/Eu3;)V

    .line 2179016
    move-object v1, v2

    .line 2179017
    move-object v0, v1

    .line 2179018
    return-object v0
.end method
