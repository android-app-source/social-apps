.class public final LX/Eps;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/graphics/PointF;

.field public final synthetic d:LX/Ept;


# direct methods
.method public constructor <init>(LX/Ept;Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;Ljava/lang/String;Landroid/graphics/PointF;)V
    .locals 0

    .prologue
    .line 2170706
    iput-object p1, p0, LX/Eps;->d:LX/Ept;

    iput-object p2, p0, LX/Eps;->a:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    iput-object p3, p0, LX/Eps;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Eps;->c:Landroid/graphics/PointF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2170707
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2170708
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .line 2170709
    iget-object v0, p0, LX/Eps;->d:LX/Ept;

    iget-object v0, v0, LX/Ept;->d:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    iget-object v1, p0, LX/Eps;->a:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Eps;->d:LX/Ept;

    iget-object v0, v0, LX/Ept;->c:Ljava/lang/String;

    iget-object v1, p0, LX/Eps;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2170710
    iget-object v0, p0, LX/Eps;->a:Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;

    iget-object v1, p0, LX/Eps;->c:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/entitycardsplugins/person/widget/header/PersonCardHeaderView;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PointF;)V

    .line 2170711
    :cond_0
    return-void
.end method
