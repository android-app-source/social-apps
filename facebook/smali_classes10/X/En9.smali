.class public LX/En9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/Bundle;

.field public final b:LX/En7;

.field public final c:LX/Eo3;

.field public final d:LX/EnL;

.field public final e:LX/Enk;

.field public final f:LX/Eny;

.field public final g:LX/Emj;

.field private final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/Emg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Bundle;LX/En7;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/Emj;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/En7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Eo3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/EnL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/Enk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/Eny;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2166687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166688
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/En9;->h:Ljava/util/HashMap;

    .line 2166689
    iput-object p1, p0, LX/En9;->a:Landroid/os/Bundle;

    .line 2166690
    iput-object p3, p0, LX/En9;->c:LX/Eo3;

    .line 2166691
    iput-object p4, p0, LX/En9;->d:LX/EnL;

    .line 2166692
    iput-object p5, p0, LX/En9;->e:LX/Enk;

    .line 2166693
    iput-object p6, p0, LX/En9;->f:LX/Eny;

    .line 2166694
    iput-object p2, p0, LX/En9;->b:LX/En7;

    .line 2166695
    iput-object p7, p0, LX/En9;->g:LX/Emj;

    .line 2166696
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)LX/Emg;
    .locals 10

    .prologue
    .line 2166697
    iget-object v0, p0, LX/En9;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2166698
    iget-object v2, p0, LX/En9;->b:LX/En7;

    iget-object v4, p0, LX/En9;->c:LX/Eo3;

    iget-object v5, p0, LX/En9;->d:LX/EnL;

    iget-object v6, p0, LX/En9;->e:LX/Enk;

    iget-object v7, p0, LX/En9;->f:LX/Eny;

    iget-object v8, p0, LX/En9;->g:LX/Emj;

    iget-object v9, p0, LX/En9;->a:Landroid/os/Bundle;

    move-object v3, p2

    invoke-virtual/range {v2 .. v9}, LX/En7;->a(Ljava/lang/Object;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/Emj;Landroid/os/Bundle;)LX/Emg;

    move-result-object v2

    move-object v0, v2

    .line 2166699
    iget-object v1, p0, LX/En9;->h:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166700
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/En9;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Emg;

    goto :goto_0
.end method
