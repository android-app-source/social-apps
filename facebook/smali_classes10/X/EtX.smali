.class public LX/EtX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2178272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2178273
    iput-object p1, p0, LX/EtX;->a:LX/0Or;

    .line 2178274
    return-void
.end method

.method public static a(LX/0QB;)LX/EtX;
    .locals 4

    .prologue
    .line 2178275
    const-class v1, LX/EtX;

    monitor-enter v1

    .line 2178276
    :try_start_0
    sget-object v0, LX/EtX;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2178277
    sput-object v2, LX/EtX;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2178278
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2178279
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2178280
    new-instance v3, LX/EtX;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EtX;-><init>(LX/0Or;)V

    .line 2178281
    move-object v0, v3

    .line 2178282
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2178283
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EtX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2178284
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2178285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1dc;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;IIIIIIILX/1X1;)LX/1Dg;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p6    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/listitem/Const$ThumbnailSize;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/listitem/annotations/TitleTextAppearenceType;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/listitem/annotations/BodyTextAppearenceType;
        .end annotation
    .end param
    .param p14    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/listitem/annotations/MetaTextAppearenceType;
        .end annotation
    .end param
    .param p15    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Landroid/net/Uri;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "IIIIIII",
            "LX/1X1",
            "<*>;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2178286
    invoke-static/range {p11 .. p11}, LX/6WK;->d(I)I

    move-result v2

    .line 2178287
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v1, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x4

    invoke-interface {v1, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/16 v3, 0x8

    const v4, 0x7f0b0f8d

    invoke-interface {v1, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const v3, 0x7f0a0097

    invoke-interface {v1, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    if-nez p5, :cond_0

    if-nez p6, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Dh;->f(F)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x5

    if-nez p15, :cond_2

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v3, v4, v1}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static/range {p12 .. p12}, LX/6WK;->a(I)I

    move-result v4

    invoke-static {p1, v3, v4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p8}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    :goto_3
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    move-object/from16 v0, p15

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    :cond_0
    if-eqz p5, :cond_1

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    :goto_4
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x5

    const v4, 0x7f0b0f8d

    invoke-interface {v1, v2, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    iget-object v1, p0, LX/EtX;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, p7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, p6}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v1

    sget-object v4, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v4}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v1

    goto :goto_4

    :cond_2
    const v1, 0x7f0b0f8d

    goto/16 :goto_1

    :cond_3
    const/4 v1, 0x0

    invoke-static/range {p13 .. p13}, LX/6WK;->b(I)I

    move-result v4

    invoke-static {p1, v1, v4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p9}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    invoke-static/range {p14 .. p14}, LX/6WK;->c(I)I

    move-result v4

    invoke-static {p1, v1, v4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p10}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    goto :goto_3
.end method
