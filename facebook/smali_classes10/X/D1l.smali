.class public final LX/D1l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/D1o;

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/survey/activities/SurveyDialogActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/survey/activities/SurveyDialogActivity;LX/D1o;J)V
    .locals 1

    .prologue
    .line 1957368
    iput-object p1, p0, LX/D1l;->c:Lcom/facebook/survey/activities/SurveyDialogActivity;

    iput-object p2, p0, LX/D1l;->a:LX/D1o;

    iput-wide p3, p0, LX/D1l;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 1957369
    iget-object v0, p0, LX/D1l;->a:LX/D1o;

    iget-wide v2, p0, LX/D1l;->b:J

    iget-object v1, p0, LX/D1l;->c:Lcom/facebook/survey/activities/SurveyDialogActivity;

    .line 1957370
    const-string v4, "survey/%s"

    invoke-static {v4}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1957371
    iget-object v5, v0, LX/D1o;->a:Lcom/facebook/content/SecureContextHelper;

    new-instance v6, Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    invoke-direct {v6, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v4

    invoke-interface {v5, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1957372
    iget-object v0, p0, LX/D1l;->a:LX/D1o;

    iget-wide v2, p0, LX/D1l;->b:J

    .line 1957373
    iget-object v1, v0, LX/D1o;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 1957374
    sget-object v4, LX/D1o;->c:LX/0Tn;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    move-object v4, v4

    .line 1957375
    const/4 v5, 0x1

    invoke-interface {v1, v4, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1957376
    return-void
.end method
