.class public final LX/DIW;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1SW;


# direct methods
.method public constructor <init>(LX/1SW;)V
    .locals 0

    .prologue
    .line 1983556
    iput-object p1, p0, LX/DIW;->b:LX/1SW;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1983557
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1983558
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1983559
    const v1, 0x7f081044

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1983560
    new-instance v2, LX/DIV;

    invoke-direct {v2, p0}, LX/DIV;-><init>(LX/DIW;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1983561
    iget-object v2, p0, LX/DIW;->b:LX/1SW;

    const v3, 0x7f0208cf

    .line 1983562
    invoke-virtual {v2, v1, v3, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1983563
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1983564
    const/4 v0, 0x1

    return v0
.end method

.method public final d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1983565
    const/4 v0, 0x1

    return v0
.end method
