.class public LX/Dca;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/621;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/621",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2018587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018588
    iput-object p1, p0, LX/Dca;->a:Ljava/lang/String;

    .line 2018589
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Dca;->b:Ljava/util/ArrayList;

    .line 2018590
    iget-object v0, p0, LX/Dca;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2018591
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2018586
    iget-object v0, p0, LX/Dca;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2018585
    return-void
.end method

.method public final synthetic b()Ljava/util/List;
    .locals 1

    .prologue
    .line 2018584
    invoke-virtual {p0}, LX/Dca;->d()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2018583
    const/4 v0, 0x0

    return v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$MenuItemModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2018582
    iget-object v0, p0, LX/Dca;->b:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2018581
    const/4 v0, 0x0

    return v0
.end method
