.class public LX/ELq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ELr;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ELq",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ELr;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109887
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2109888
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ELq;->b:LX/0Zi;

    .line 2109889
    iput-object p1, p0, LX/ELq;->a:LX/0Ot;

    .line 2109890
    return-void
.end method

.method public static a(LX/0QB;)LX/ELq;
    .locals 4

    .prologue
    .line 2109876
    const-class v1, LX/ELq;

    monitor-enter v1

    .line 2109877
    :try_start_0
    sget-object v0, LX/ELq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109878
    sput-object v2, LX/ELq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109879
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109880
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109881
    new-instance v3, LX/ELq;

    const/16 p0, 0x3407

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ELq;-><init>(LX/0Ot;)V

    .line 2109882
    move-object v0, v3

    .line 2109883
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109884
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ELq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109885
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109886
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2109875
    const v0, -0x357bdf6d    # -4329545.5f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2109837
    check-cast p2, LX/ELp;

    .line 2109838
    iget-object v0, p0, LX/ELq;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ELr;

    iget-object v1, p2, LX/ELp;->b:LX/CzL;

    const/4 v5, 0x0

    .line 2109839
    iget-object v2, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2109840
    check-cast v2, LX/8d0;

    .line 2109841
    invoke-static {v1, v5}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v4

    .line 2109842
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const v5, 0x7f0b1734

    invoke-interface {v3, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    const v5, 0x7f020a3d

    invoke-interface {v3, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    const v5, 0x7f0b1733

    invoke-interface {v3, v5}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v3

    const/16 v5, 0x8

    const/4 v6, 0x2

    invoke-interface {v3, v5, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    .line 2109843
    const v5, -0x357bdf6d    # -4329545.5f

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2109844
    invoke-interface {v3, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    .line 2109845
    iget-object v3, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 2109846
    check-cast v3, LX/8d0;

    invoke-interface {v3}, LX/8d0;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v4}, LX/ELM;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v3

    iget-object v5, v0, LX/ELr;->b:LX/EME;

    const/4 v6, 0x0

    .line 2109847
    new-instance p0, LX/EMD;

    invoke-direct {p0, v5}, LX/EMD;-><init>(LX/EME;)V

    .line 2109848
    sget-object p2, LX/EME;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/EMC;

    .line 2109849
    if-nez p2, :cond_0

    .line 2109850
    new-instance p2, LX/EMC;

    invoke-direct {p2}, LX/EMC;-><init>()V

    .line 2109851
    :cond_0
    invoke-static {p2, p1, v6, v6, p0}, LX/EMC;->a$redex0(LX/EMC;LX/1De;IILX/EMD;)V

    .line 2109852
    move-object p0, p2

    .line 2109853
    move-object v6, p0

    .line 2109854
    move-object v5, v6

    .line 2109855
    iget-object v6, v5, LX/EMC;->a:LX/EMD;

    iput-object v2, v6, LX/EMD;->a:LX/8d0;

    .line 2109856
    iget-object v6, v5, LX/EMC;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2109857
    move-object v2, v5

    .line 2109858
    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, LX/ELr;->c:LX/ELu;

    const/4 v5, 0x0

    .line 2109859
    new-instance v6, LX/ELt;

    invoke-direct {v6, v3}, LX/ELt;-><init>(LX/ELu;)V

    .line 2109860
    sget-object p0, LX/ELu;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/ELs;

    .line 2109861
    if-nez p0, :cond_1

    .line 2109862
    new-instance p0, LX/ELs;

    invoke-direct {p0}, LX/ELs;-><init>()V

    .line 2109863
    :cond_1
    invoke-static {p0, p1, v5, v5, v6}, LX/ELs;->a$redex0(LX/ELs;LX/1De;IILX/ELt;)V

    .line 2109864
    move-object v6, p0

    .line 2109865
    move-object v5, v6

    .line 2109866
    move-object v3, v5

    .line 2109867
    iget-object v5, v3, LX/ELs;->a:LX/ELt;

    iput-object v1, v5, LX/ELt;->a:LX/CzL;

    .line 2109868
    iget-object v5, v3, LX/ELs;->d:Ljava/util/BitSet;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2109869
    move-object v3, v3

    .line 2109870
    iget-object v5, v3, LX/ELs;->a:LX/ELt;

    iput-object v4, v5, LX/ELt;->b:Ljava/lang/String;

    .line 2109871
    iget-object v5, v3, LX/ELs;->d:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 2109872
    move-object v3, v3

    .line 2109873
    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2109874
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2109827
    invoke-static {}, LX/1dS;->b()V

    .line 2109828
    iget v0, p1, LX/1dQ;->b:I

    .line 2109829
    packed-switch v0, :pswitch_data_0

    .line 2109830
    :goto_0
    return-object v2

    .line 2109831
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2109832
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2109833
    check-cast v1, LX/ELp;

    .line 2109834
    iget-object v3, p0, LX/ELq;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ELr;

    iget-object p1, v1, LX/ELp;->a:LX/CxP;

    iget-object p2, v1, LX/ELp;->b:LX/CzL;

    .line 2109835
    iget-object p0, v3, LX/ELr;->a:LX/ELB;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, p2, p1, v1}, LX/ELB;->a(LX/CzL;LX/CxP;Landroid/content/Context;)V

    .line 2109836
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x357bdf6d
        :pswitch_0
    .end packed-switch
.end method
