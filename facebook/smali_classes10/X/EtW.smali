.class public LX/EtW;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EtV;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EtX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2178271
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EtW;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EtX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2178244
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2178245
    iput-object p1, p0, LX/EtW;->b:LX/0Ot;

    .line 2178246
    return-void
.end method

.method public static a(LX/0QB;)LX/EtW;
    .locals 4

    .prologue
    .line 2178247
    const-class v1, LX/EtW;

    monitor-enter v1

    .line 2178248
    :try_start_0
    sget-object v0, LX/EtW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2178249
    sput-object v2, LX/EtW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2178250
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2178251
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2178252
    new-instance v3, LX/EtW;

    const/16 p0, 0x2209

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EtW;-><init>(LX/0Ot;)V

    .line 2178253
    move-object v0, v3

    .line 2178254
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2178255
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EtW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2178256
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2178257
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 17

    .prologue
    .line 2178258
    check-cast p2, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;

    .line 2178259
    move-object/from16 v0, p0

    iget-object v1, v0, LX/EtW;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EtX;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->a:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->b:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->c:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->d:LX/1dc;

    move-object/from16 v0, p2

    iget-object v7, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->e:Landroid/net/Uri;

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    move-object/from16 v0, p2

    iget v9, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->g:I

    move-object/from16 v0, p2

    iget v10, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->h:I

    move-object/from16 v0, p2

    iget v11, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->i:I

    move-object/from16 v0, p2

    iget v12, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->j:I

    move-object/from16 v0, p2

    iget v13, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->k:I

    move-object/from16 v0, p2

    iget v14, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->l:I

    move-object/from16 v0, p2

    iget v15, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->m:I

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->n:LX/1X1;

    move-object/from16 v16, v0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v16}, LX/EtX;->a(LX/1De;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1dc;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;IIIIIIILX/1X1;)LX/1Dg;

    move-result-object v1

    .line 2178260
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2178261
    invoke-static {}, LX/1dS;->b()V

    .line 2178262
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/EtV;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2178263
    new-instance v1, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;-><init>(LX/EtW;)V

    .line 2178264
    sget-object v2, LX/EtW;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EtV;

    .line 2178265
    if-nez v2, :cond_0

    .line 2178266
    new-instance v2, LX/EtV;

    invoke-direct {v2}, LX/EtV;-><init>()V

    .line 2178267
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EtV;->a$redex0(LX/EtV;LX/1De;IILcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;)V

    .line 2178268
    move-object v1, v2

    .line 2178269
    move-object v0, v1

    .line 2178270
    return-object v0
.end method
