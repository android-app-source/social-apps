.class public final LX/DFK;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DFL;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/2dy;

.field public final synthetic c:LX/DFL;


# direct methods
.method public constructor <init>(LX/DFL;)V
    .locals 1

    .prologue
    .line 1978017
    iput-object p1, p0, LX/DFK;->c:LX/DFL;

    .line 1978018
    move-object v0, p1

    .line 1978019
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1978020
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1978021
    const-string v0, "PeopleYouMayKnowHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1978022
    if-ne p0, p1, :cond_1

    .line 1978023
    :cond_0
    :goto_0
    return v0

    .line 1978024
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1978025
    goto :goto_0

    .line 1978026
    :cond_3
    check-cast p1, LX/DFK;

    .line 1978027
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1978028
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1978029
    if-eq v2, v3, :cond_0

    .line 1978030
    iget-object v2, p0, LX/DFK;->a:LX/1Pc;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DFK;->a:LX/1Pc;

    iget-object v3, p1, LX/DFK;->a:LX/1Pc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1978031
    goto :goto_0

    .line 1978032
    :cond_5
    iget-object v2, p1, LX/DFK;->a:LX/1Pc;

    if-nez v2, :cond_4

    .line 1978033
    :cond_6
    iget-object v2, p0, LX/DFK;->b:LX/2dy;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/DFK;->b:LX/2dy;

    iget-object v3, p1, LX/DFK;->b:LX/2dy;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1978034
    goto :goto_0

    .line 1978035
    :cond_7
    iget-object v2, p1, LX/DFK;->b:LX/2dy;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
