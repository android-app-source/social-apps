.class public LX/EfP;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final l:Lcom/facebook/resources/ui/FbTextView;

.field public final m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final n:LX/1xf;

.field public final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/1EX;

.field public q:Lcom/facebook/audience/model/Reply;


# direct methods
.method public constructor <init>(LX/1xf;Landroid/widget/LinearLayout;LX/0Or;LX/1EX;)V
    .locals 1
    .param p2    # Landroid/widget/LinearLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1xf;",
            "Landroid/widget/LinearLayout;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/1EX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2154534
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2154535
    iput-object p1, p0, LX/EfP;->n:LX/1xf;

    .line 2154536
    const v0, 0x7f0d06b8

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EfP;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2154537
    const v0, 0x7f0d06b7

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/EfP;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2154538
    iput-object p3, p0, LX/EfP;->o:LX/0Or;

    .line 2154539
    iput-object p4, p0, LX/EfP;->p:LX/1EX;

    .line 2154540
    return-void
.end method

.method public static a(LX/EfP;Landroid/content/res/Resources;Lcom/facebook/audience/model/Reply;)Landroid/text/SpannableString;
    .locals 12

    .prologue
    .line 2154465
    sget-object v6, LX/EfO;->a:[I

    .line 2154466
    iget-object v7, p2, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    move-object v7, v7

    .line 2154467
    invoke-virtual {v7}, LX/7gq;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 2154468
    :pswitch_0
    iget-object v6, p0, LX/EfP;->n:LX/1xf;

    sget-object v7, LX/1lB;->NOTIFICATIONS_STREAM_RELATIVE_STYLE:LX/1lB;

    .line 2154469
    iget-wide v10, p2, Lcom/facebook/audience/model/Reply;->i:J

    move-wide v8, v10

    .line 2154470
    invoke-virtual {v6, v7, v8, v9}, LX/1xf;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v6

    :goto_0
    move-object v2, v6

    .line 2154471
    iget-boolean v0, p2, Lcom/facebook/audience/model/Reply;->c:Z

    move v0, v0

    .line 2154472
    if-eqz v0, :cond_3

    .line 2154473
    iget-object v0, p2, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    move-object v0, v0

    .line 2154474
    sget-object v1, LX/7gq;->SENT:LX/7gq;

    if-eq v0, v1, :cond_2

    .line 2154475
    iget-object v0, p2, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    move-object v0, v0

    .line 2154476
    sget-object v1, LX/EfO;->a:[I

    invoke-virtual {v0}, LX/7gq;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_1

    .line 2154477
    const v1, 0x7f082ab8

    :goto_1
    move v0, v1

    .line 2154478
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2154479
    :goto_2
    iget-object v1, p2, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2154480
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2154481
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2154482
    iget-object v1, p2, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    move-object v1, v1

    .line 2154483
    sget-object v4, LX/7gq;->SENT:LX/7gq;

    if-eq v1, v4, :cond_0

    .line 2154484
    const-string v1, "  "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2154485
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2154486
    :cond_0
    const-string v1, "   "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2154487
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2154488
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2154489
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 2154490
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2154491
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v2, v4, v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x3

    .line 2154492
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    .line 2154493
    add-int/lit8 v4, v0, 0x1

    .line 2154494
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 2154495
    invoke-static {p1, v1, v2, v3}, LX/EfP;->a(Landroid/content/res/Resources;Landroid/text/SpannableString;II)V

    .line 2154496
    sget-object v2, LX/EfO;->a:[I

    .line 2154497
    iget-object v5, p2, Lcom/facebook/audience/model/Reply;->h:LX/7gq;

    move-object v5, v5

    .line 2154498
    invoke-virtual {v5}, LX/7gq;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_2

    .line 2154499
    :goto_3
    const/4 v7, 0x0

    .line 2154500
    const v2, 0x7f0b1b6e

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 2154501
    iget-object v3, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v2

    move v2, v3

    .line 2154502
    float-to-int v2, v2

    .line 2154503
    iget-object v3, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f02071f

    invoke-static {v3, v5}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2154504
    const v5, 0x7f0a0793

    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2154505
    invoke-virtual {v3, v7, v7, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2154506
    new-instance v2, LX/34T;

    const/4 v5, 0x2

    invoke-direct {v2, v3, v5}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    const/16 v3, 0x21

    invoke-virtual {v1, v2, v0, v4, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2154507
    move-object v0, v1

    .line 2154508
    :goto_4
    return-object v0

    .line 2154509
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    :pswitch_1
    move-object v0, v1

    .line 2154510
    goto :goto_4

    .line 2154511
    :pswitch_2
    add-int/lit8 v2, v4, 0x1

    .line 2154512
    new-instance v5, Landroid/text/style/StyleSpan;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v6, 0x21

    invoke-virtual {v1, v5, v2, v3, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2154513
    goto :goto_3

    .line 2154514
    :cond_3
    iget-object v0, p2, Lcom/facebook/audience/model/Reply;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2154515
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2154516
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2154517
    const-string v0, "  "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2154518
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2154519
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2154520
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2154521
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2154522
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v2, v3, v2

    add-int/lit8 v2, v2, -0x1

    .line 2154523
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    .line 2154524
    invoke-static {p1, v0, v2, v1}, LX/EfP;->a(Landroid/content/res/Resources;Landroid/text/SpannableString;II)V

    goto :goto_4

    .line 2154525
    :pswitch_3
    const v6, 0x7f082ab7

    invoke-virtual {p1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 2154526
    :pswitch_4
    const-string v6, ""

    goto/16 :goto_0

    .line 2154527
    :pswitch_5
    const v1, 0x7f082ab8

    goto/16 :goto_1

    .line 2154528
    :pswitch_6
    const v1, 0x7f082ab9

    goto/16 :goto_1

    .line 2154529
    :pswitch_7
    const v1, 0x7f082aba

    goto/16 :goto_1

    .line 2154530
    :pswitch_8
    const v1, 0x7f082abb

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Landroid/content/res/Resources;Landroid/text/SpannableString;II)V
    .locals 3

    .prologue
    const/16 v2, 0x21

    .line 2154531
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const v1, 0x7f0a0793

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p1, v0, p2, p3, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2154532
    new-instance v0, Landroid/text/style/RelativeSizeSpan;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-direct {v0, v1}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {p1, v0, p2, p3, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2154533
    return-void
.end method
