.class public final LX/EYJ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:Z


# instance fields
.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/EYQ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/EYE;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/EYG;",
            "LX/EYP;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/EYG;",
            "LX/EYM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2136978
    const-class v0, LX/EYT;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/EYJ;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([LX/EYQ;)V
    .locals 3

    .prologue
    .line 2136965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2136966
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EYJ;->c:Ljava/util/Map;

    .line 2136967
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EYJ;->d:Ljava/util/Map;

    .line 2136968
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/EYJ;->e:Ljava/util/Map;

    .line 2136969
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/EYJ;->b:Ljava/util/Set;

    .line 2136970
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 2136971
    iget-object v1, p0, LX/EYJ;->b:Ljava/util/Set;

    aget-object v2, p1, v0

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2136972
    aget-object v1, p1, v0

    invoke-direct {p0, v1}, LX/EYJ;->a(LX/EYQ;)V

    .line 2136973
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2136974
    :cond_0
    iget-object v0, p0, LX/EYJ;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYQ;

    .line 2136975
    :try_start_0
    invoke-virtual {v0}, LX/EYQ;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, LX/EYJ;->a(Ljava/lang/String;LX/EYQ;)V
    :try_end_0
    .catch LX/EYK; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2136976
    :catch_0
    sget-boolean v0, LX/EYJ;->a:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2136977
    :cond_2
    return-void
.end method

.method public static a(LX/EYJ;Ljava/lang/String;LX/EYI;)LX/EYE;
    .locals 3

    .prologue
    .line 2136956
    iget-object v0, p0, LX/EYJ;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYE;

    .line 2136957
    if-eqz v0, :cond_2

    .line 2136958
    sget-object v1, LX/EYI;->ALL_SYMBOLS:LX/EYI;

    if-eq p2, v1, :cond_1

    sget-object v1, LX/EYI;->TYPES_ONLY:LX/EYI;

    if-ne p2, v1, :cond_0

    invoke-static {v0}, LX/EYJ;->b(LX/EYE;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    sget-object v1, LX/EYI;->AGGREGATES_ONLY:LX/EYI;

    if-ne p2, v1, :cond_2

    invoke-static {v0}, LX/EYJ;->c(LX/EYE;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2136959
    :cond_1
    :goto_0
    return-object v0

    .line 2136960
    :cond_2
    iget-object v0, p0, LX/EYJ;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYQ;

    .line 2136961
    iget-object v0, v0, LX/EYQ;->h:LX/EYJ;

    iget-object v0, v0, LX/EYJ;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYE;

    .line 2136962
    if-eqz v0, :cond_3

    .line 2136963
    sget-object v2, LX/EYI;->ALL_SYMBOLS:LX/EYI;

    if-eq p2, v2, :cond_1

    sget-object v2, LX/EYI;->TYPES_ONLY:LX/EYI;

    if-ne p2, v2, :cond_4

    invoke-static {v0}, LX/EYJ;->b(LX/EYE;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_4
    sget-object v2, LX/EYI;->AGGREGATES_ONLY:LX/EYI;

    if-ne p2, v2, :cond_3

    invoke-static {v0}, LX/EYJ;->c(LX/EYE;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    .line 2136964
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/EYQ;)V
    .locals 3

    .prologue
    .line 2136951
    iget-object v0, p1, LX/EYQ;->g:[LX/EYQ;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    move-object v0, v0

    .line 2136952
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYQ;

    .line 2136953
    iget-object v2, p0, LX/EYJ;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2136954
    invoke-direct {p0, v0}, LX/EYJ;->a(LX/EYQ;)V

    goto :goto_0

    .line 2136955
    :cond_1
    return-void
.end method

.method private static b(LX/EYE;)Z
    .locals 1

    .prologue
    .line 2136950
    instance-of v0, p0, LX/EYF;

    if-nez v0, :cond_0

    instance-of v0, p0, LX/EYL;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(LX/EYE;)Z
    .locals 1

    .prologue
    .line 2136949
    instance-of v0, p0, LX/EYF;

    if-nez v0, :cond_0

    instance-of v0, p0, LX/EYL;

    if-nez v0, :cond_0

    instance-of v0, p0, LX/EYH;

    if-nez v0, :cond_0

    instance-of v0, p0, LX/EYS;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/EYE;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2136884
    invoke-interface {p0}, LX/EYE;->a()Ljava/lang/String;

    move-result-object v3

    .line 2136885
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 2136886
    new-instance v0, LX/EYK;

    const-string v2, "Missing name."

    invoke-direct {v0, p0, v2}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2136887
    :cond_0
    const/4 v0, 0x1

    move v2, v0

    move v0, v1

    .line 2136888
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 2136889
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 2136890
    const/16 v5, 0x80

    if-lt v4, v5, :cond_1

    move v2, v1

    .line 2136891
    :cond_1
    invoke-static {v4}, Ljava/lang/Character;->isLetter(C)Z

    move-result v5

    if-nez v5, :cond_3

    const/16 v5, 0x5f

    if-eq v4, v5, :cond_3

    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-eqz v4, :cond_2

    if-gtz v0, :cond_3

    :cond_2
    move v2, v1

    .line 2136892
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2136893
    :cond_4
    if-nez v2, :cond_5

    .line 2136894
    new-instance v0, LX/EYK;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" is not a valid identifier."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v2}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2136895
    :cond_5
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/EYE;LX/EYI;)LX/EYE;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 2136928
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2136929
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p3}, LX/EYJ;->a(LX/EYJ;Ljava/lang/String;LX/EYI;)LX/EYE;

    move-result-object v0

    .line 2136930
    :goto_0
    if-nez v0, :cond_4

    .line 2136931
    new-instance v0, LX/EYK;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not defined."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p2, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2136932
    :cond_0
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 2136933
    if-ne v2, v6, :cond_1

    move-object v0, p1

    .line 2136934
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-interface {p2}, LX/EYE;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2136935
    :goto_2
    const-string v1, "."

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 2136936
    if-ne v4, v6, :cond_2

    .line 2136937
    invoke-static {p0, p1, p3}, LX/EYJ;->a(LX/EYJ;Ljava/lang/String;LX/EYI;)LX/EYE;

    move-result-object v0

    goto :goto_0

    .line 2136938
    :cond_1
    invoke-virtual {p1, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2136939
    :cond_2
    add-int/lit8 v1, v4, 0x1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2136940
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2136941
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v5, LX/EYI;->AGGREGATES_ONLY:LX/EYI;

    invoke-static {p0, v1, v5}, LX/EYJ;->a(LX/EYJ;Ljava/lang/String;LX/EYI;)LX/EYE;

    move-result-object v1

    .line 2136942
    if-eqz v1, :cond_3

    .line 2136943
    if-eq v2, v6, :cond_5

    .line 2136944
    add-int/lit8 v0, v4, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2136945
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2136946
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p3}, LX/EYJ;->a(LX/EYJ;Ljava/lang/String;LX/EYI;)LX/EYE;

    move-result-object v0

    goto :goto_0

    .line 2136947
    :cond_3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_2

    .line 2136948
    :cond_4
    return-object v0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/EYE;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2136916
    invoke-static {p1}, LX/EYJ;->d(LX/EYE;)V

    .line 2136917
    invoke-interface {p1}, LX/EYE;->b()Ljava/lang/String;

    move-result-object v1

    .line 2136918
    const/16 v0, 0x2e

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 2136919
    iget-object v0, p0, LX/EYJ;->c:Ljava/util/Map;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYE;

    .line 2136920
    if-eqz v0, :cond_2

    .line 2136921
    iget-object v3, p0, LX/EYJ;->c:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2136922
    invoke-interface {p1}, LX/EYE;->c()LX/EYQ;

    move-result-object v3

    invoke-interface {v0}, LX/EYE;->c()LX/EYQ;

    move-result-object v4

    if-ne v3, v4, :cond_1

    .line 2136923
    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    .line 2136924
    new-instance v0, LX/EYK;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is already defined."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2136925
    :cond_0
    new-instance v0, LX/EYK;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" is already defined in \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v0

    .line 2136926
    :cond_1
    new-instance v2, LX/EYK;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\" is already defined in file \""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, LX/EYE;->c()LX/EYQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EYQ;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p1, v0}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v2

    .line 2136927
    :cond_2
    return-void
.end method

.method public final a(LX/EYP;)V
    .locals 4

    .prologue
    .line 2136906
    new-instance v1, LX/EYG;

    .line 2136907
    iget-object v0, p1, LX/EYP;->h:LX/EYF;

    move-object v0, v0

    .line 2136908
    invoke-virtual {p1}, LX/EYP;->e()I

    move-result v2

    invoke-direct {v1, v0, v2}, LX/EYG;-><init>(LX/EYE;I)V

    .line 2136909
    iget-object v0, p0, LX/EYJ;->d:Ljava/util/Map;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYP;

    .line 2136910
    if-eqz v0, :cond_0

    .line 2136911
    iget-object v2, p0, LX/EYJ;->d:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2136912
    new-instance v1, LX/EYK;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field number "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/EYP;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "has already been used in \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2136913
    iget-object v3, p1, LX/EYP;->h:LX/EYF;

    move-object v3, v3

    .line 2136914
    invoke-virtual {v3}, LX/EYF;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" by field \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LX/EYP;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\"."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p1, v0}, LX/EYK;-><init>(LX/EYE;Ljava/lang/String;)V

    throw v1

    .line 2136915
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/EYQ;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2136896
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 2136897
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    move-object v1, p1

    .line 2136898
    :goto_0
    iget-object v0, p0, LX/EYJ;->c:Ljava/util/Map;

    new-instance v2, LX/EYH;

    invoke-direct {v2, v1, p1, p2}, LX/EYH;-><init>(Ljava/lang/String;Ljava/lang/String;LX/EYQ;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EYE;

    .line 2136899
    if-eqz v0, :cond_1

    .line 2136900
    iget-object v2, p0, LX/EYJ;->c:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2136901
    instance-of v2, v0, LX/EYH;

    if-nez v2, :cond_1

    .line 2136902
    new-instance v2, LX/EYK;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\" is already defined (as something other than a package) in file \""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, LX/EYE;->c()LX/EYQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EYQ;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p2, v0}, LX/EYK;-><init>(LX/EYQ;Ljava/lang/String;)V

    throw v2

    .line 2136903
    :cond_0
    invoke-virtual {p1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, LX/EYJ;->a(Ljava/lang/String;LX/EYQ;)V

    .line 2136904
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2136905
    :cond_1
    return-void
.end method
