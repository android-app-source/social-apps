.class public LX/Dvr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:LX/0aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0aq",
            "<",
            "LX/Dvm;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;",
            ">;>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/Dvr;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2059363
    new-instance v0, LX/0aq;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, LX/0aq;-><init>(I)V

    sput-object v0, LX/Dvr;->a:LX/0aq;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2059364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059365
    return-void
.end method

.method public static a(LX/0QB;)LX/Dvr;
    .locals 3

    .prologue
    .line 2059366
    sget-object v0, LX/Dvr;->b:LX/Dvr;

    if-nez v0, :cond_1

    .line 2059367
    const-class v1, LX/Dvr;

    monitor-enter v1

    .line 2059368
    :try_start_0
    sget-object v0, LX/Dvr;->b:LX/Dvr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2059369
    if-eqz v2, :cond_0

    .line 2059370
    :try_start_1
    new-instance v0, LX/Dvr;

    invoke-direct {v0}, LX/Dvr;-><init>()V

    .line 2059371
    move-object v0, v0

    .line 2059372
    sput-object v0, LX/Dvr;->b:LX/Dvr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2059373
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2059374
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2059375
    :cond_1
    sget-object v0, LX/Dvr;->b:LX/Dvr;

    return-object v0

    .line 2059376
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2059377
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2059378
    sget-object v0, LX/Dvr;->a:LX/0aq;

    invoke-virtual {v0}, LX/0aq;->a()V

    .line 2059379
    return-void
.end method
