.class public final LX/EXj;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EXh;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXj;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EXj;


# instance fields
.field public bitField0_:I

.field public inputType_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public name_:Ljava/lang/Object;

.field public options_:LX/EXn;

.field public outputType_:Ljava/lang/Object;

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2135084
    new-instance v0, LX/EXg;

    invoke-direct {v0}, LX/EXg;-><init>()V

    sput-object v0, LX/EXj;->a:LX/EWZ;

    .line 2135085
    new-instance v0, LX/EXj;

    invoke-direct {v0}, LX/EXj;-><init>()V

    .line 2135086
    sput-object v0, LX/EXj;->c:LX/EXj;

    invoke-direct {v0}, LX/EXj;->z()V

    .line 2135087
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2135088
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2135089
    iput-byte v0, p0, LX/EXj;->memoizedIsInitialized:B

    .line 2135090
    iput v0, p0, LX/EXj;->memoizedSerializedSize:I

    .line 2135091
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2135092
    iput-object v0, p0, LX/EXj;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x1

    .line 2135093
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2135094
    iput-byte v0, p0, LX/EXj;->memoizedIsInitialized:B

    .line 2135095
    iput v0, p0, LX/EXj;->memoizedSerializedSize:I

    .line 2135096
    invoke-direct {p0}, LX/EXj;->z()V

    .line 2135097
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v4

    .line 2135098
    const/4 v0, 0x0

    move v2, v0

    .line 2135099
    :cond_0
    :goto_0
    if-nez v2, :cond_2

    .line 2135100
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2135101
    sparse-switch v0, :sswitch_data_0

    .line 2135102
    invoke-virtual {p0, p1, v4, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v3

    .line 2135103
    goto :goto_0

    :sswitch_0
    move v2, v3

    .line 2135104
    goto :goto_0

    .line 2135105
    :sswitch_1
    iget v0, p0, LX/EXj;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EXj;->bitField0_:I

    .line 2135106
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXj;->name_:Ljava/lang/Object;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2135107
    :catch_0
    move-exception v0

    .line 2135108
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2135109
    move-object v0, v0

    .line 2135110
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2135111
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EXj;->unknownFields:LX/EZQ;

    .line 2135112
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2135113
    :sswitch_2
    :try_start_2
    iget v0, p0, LX/EXj;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EXj;->bitField0_:I

    .line 2135114
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXj;->inputType_:Ljava/lang/Object;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2135115
    :catch_1
    move-exception v0

    .line 2135116
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2135117
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2135118
    move-object v0, v1

    .line 2135119
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2135120
    :sswitch_3
    :try_start_4
    iget v0, p0, LX/EXj;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EXj;->bitField0_:I

    .line 2135121
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXj;->outputType_:Ljava/lang/Object;

    goto :goto_0

    .line 2135122
    :sswitch_4
    const/4 v0, 0x0

    .line 2135123
    iget v1, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v5, 0x8

    if-ne v1, v5, :cond_3

    .line 2135124
    iget-object v0, p0, LX/EXj;->options_:LX/EXn;

    invoke-virtual {v0}, LX/EXn;->j()LX/EXm;

    move-result-object v0

    move-object v1, v0

    .line 2135125
    :goto_1
    sget-object v0, LX/EXn;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EXn;

    iput-object v0, p0, LX/EXj;->options_:LX/EXn;

    .line 2135126
    if-eqz v1, :cond_1

    .line 2135127
    iget-object v0, p0, LX/EXj;->options_:LX/EXn;

    invoke-virtual {v1, v0}, LX/EXm;->a(LX/EXn;)LX/EXm;

    .line 2135128
    invoke-virtual {v1}, LX/EXm;->l()LX/EXn;

    move-result-object v0

    iput-object v0, p0, LX/EXj;->options_:LX/EXn;

    .line 2135129
    :cond_1
    iget v0, p0, LX/EXj;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EXj;->bitField0_:I
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 2135130
    :cond_2
    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXj;->unknownFields:LX/EZQ;

    .line 2135131
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2135132
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2135133
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2135134
    iput-byte v1, p0, LX/EXj;->memoizedIsInitialized:B

    .line 2135135
    iput v1, p0, LX/EXj;->memoizedSerializedSize:I

    .line 2135136
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXj;->unknownFields:LX/EZQ;

    .line 2135137
    return-void
.end method

.method private static d(LX/EXj;)LX/EXi;
    .locals 1

    .prologue
    .line 2135138
    invoke-static {}, LX/EXi;->n()LX/EXi;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EXi;->a(LX/EXj;)LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method private w()LX/EWc;
    .locals 2

    .prologue
    .line 2135139
    iget-object v0, p0, LX/EXj;->name_:Ljava/lang/Object;

    .line 2135140
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2135141
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2135142
    iput-object v0, p0, LX/EXj;->name_:Ljava/lang/Object;

    .line 2135143
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private x()LX/EWc;
    .locals 2

    .prologue
    .line 2135144
    iget-object v0, p0, LX/EXj;->inputType_:Ljava/lang/Object;

    .line 2135145
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2135146
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2135147
    iput-object v0, p0, LX/EXj;->inputType_:Ljava/lang/Object;

    .line 2135148
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private y()LX/EWc;
    .locals 2

    .prologue
    .line 2135149
    iget-object v0, p0, LX/EXj;->outputType_:Ljava/lang/Object;

    .line 2135150
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2135151
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2135152
    iput-object v0, p0, LX/EXj;->outputType_:Ljava/lang/Object;

    .line 2135153
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private z()V
    .locals 1

    .prologue
    .line 2135154
    const-string v0, ""

    iput-object v0, p0, LX/EXj;->name_:Ljava/lang/Object;

    .line 2135155
    const-string v0, ""

    iput-object v0, p0, LX/EXj;->inputType_:Ljava/lang/Object;

    .line 2135156
    const-string v0, ""

    iput-object v0, p0, LX/EXj;->outputType_:Ljava/lang/Object;

    .line 2135157
    sget-object v0, LX/EXn;->c:LX/EXn;

    move-object v0, v0

    .line 2135158
    iput-object v0, p0, LX/EXj;->options_:LX/EXn;

    .line 2135159
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2135160
    new-instance v0, LX/EXi;

    invoke-direct {v0, p1}, LX/EXi;-><init>(LX/EYd;)V

    .line 2135161
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2135162
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2135163
    iget v0, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2135164
    invoke-direct {p0}, LX/EXj;->w()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2135165
    :cond_0
    iget v0, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2135166
    invoke-direct {p0}, LX/EXj;->x()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2135167
    :cond_1
    iget v0, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2135168
    const/4 v0, 0x3

    invoke-direct {p0}, LX/EXj;->y()LX/EWc;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2135169
    :cond_2
    iget v0, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2135170
    iget-object v0, p0, LX/EXj;->options_:LX/EXn;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2135171
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2135172
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2135074
    iget-byte v2, p0, LX/EXj;->memoizedIsInitialized:B

    .line 2135075
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-ne v2, v0, :cond_0

    .line 2135076
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 2135077
    goto :goto_0

    .line 2135078
    :cond_1
    invoke-virtual {p0}, LX/EXj;->p()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2135079
    iget-object v2, p0, LX/EXj;->options_:LX/EXn;

    move-object v2, v2

    .line 2135080
    invoke-virtual {v2}, LX/EWY;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2135081
    iput-byte v1, p0, LX/EXj;->memoizedIsInitialized:B

    move v0, v1

    .line 2135082
    goto :goto_0

    .line 2135083
    :cond_2
    iput-byte v0, p0, LX/EXj;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2135025
    iget v0, p0, LX/EXj;->memoizedSerializedSize:I

    .line 2135026
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2135027
    :goto_0
    return v0

    .line 2135028
    :cond_0
    const/4 v0, 0x0

    .line 2135029
    iget v1, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2135030
    invoke-direct {p0}, LX/EXj;->w()LX/EWc;

    move-result-object v0

    invoke-static {v2, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2135031
    :cond_1
    iget v1, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2135032
    invoke-direct {p0}, LX/EXj;->x()LX/EWc;

    move-result-object v1

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2135033
    :cond_2
    iget v1, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2135034
    const/4 v1, 0x3

    invoke-direct {p0}, LX/EXj;->y()LX/EWc;

    move-result-object v2

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2135035
    :cond_3
    iget v1, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2135036
    iget-object v1, p0, LX/EXj;->options_:LX/EXn;

    invoke-static {v4, v1}, LX/EWf;->e(ILX/EWW;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2135037
    :cond_4
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2135038
    iput v0, p0, LX/EXj;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2135073
    iget-object v0, p0, LX/EXj;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2135039
    sget-object v0, LX/EYC;->r:LX/EYn;

    const-class v1, LX/EXj;

    const-class v2, LX/EXi;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2135040
    sget-object v0, LX/EXj;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2135041
    iget-object v0, p0, LX/EXj;->name_:Ljava/lang/Object;

    .line 2135042
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2135043
    check-cast v0, Ljava/lang/String;

    .line 2135044
    :goto_0
    return-object v0

    .line 2135045
    :cond_0
    check-cast v0, LX/EWc;

    .line 2135046
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2135047
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2135048
    iput-object v1, p0, LX/EXj;->name_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2135049
    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2135050
    iget-object v0, p0, LX/EXj;->inputType_:Ljava/lang/Object;

    .line 2135051
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2135052
    check-cast v0, Ljava/lang/String;

    .line 2135053
    :goto_0
    return-object v0

    .line 2135054
    :cond_0
    check-cast v0, LX/EWc;

    .line 2135055
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2135056
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2135057
    iput-object v1, p0, LX/EXj;->inputType_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2135058
    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2135059
    iget-object v0, p0, LX/EXj;->outputType_:Ljava/lang/Object;

    .line 2135060
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2135061
    check-cast v0, Ljava/lang/String;

    .line 2135062
    :goto_0
    return-object v0

    .line 2135063
    :cond_0
    check-cast v0, LX/EWc;

    .line 2135064
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2135065
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2135066
    iput-object v1, p0, LX/EXj;->outputType_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2135067
    goto :goto_0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 2135068
    iget v0, p0, LX/EXj;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2135069
    invoke-static {p0}, LX/EXj;->d(LX/EXj;)LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2135070
    invoke-static {}, LX/EXi;->n()LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2135071
    invoke-static {p0}, LX/EXj;->d(LX/EXj;)LX/EXi;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2135072
    sget-object v0, LX/EXj;->c:LX/EXj;

    return-object v0
.end method
