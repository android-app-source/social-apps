.class public LX/Eg1;
.super LX/Efo;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public l:LX/ALw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/audience/util/PrefetchUtils;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0fO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/7ga;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/AIk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final q:LX/Efu;

.field public final r:LX/Efv;

.field private final s:Landroid/view/View$OnClickListener;

.field private final t:LX/Efy;

.field public final u:Landroid/view/View;

.field public final v:Lcom/facebook/resources/ui/FbTextView;

.field private final w:Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

.field public x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

.field public y:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2155674
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Eg1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2155675
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155672
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Eg1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155673
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2155655
    invoke-direct {p0, p1, p2, p3}, LX/Efo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2155656
    new-instance v0, LX/Efu;

    invoke-direct {v0, p0}, LX/Efu;-><init>(LX/Eg1;)V

    iput-object v0, p0, LX/Eg1;->q:LX/Efu;

    .line 2155657
    new-instance v0, LX/Efv;

    invoke-direct {v0, p0}, LX/Efv;-><init>(LX/Eg1;)V

    iput-object v0, p0, LX/Eg1;->r:LX/Efv;

    .line 2155658
    new-instance v0, LX/Efw;

    invoke-direct {v0, p0}, LX/Efw;-><init>(LX/Eg1;)V

    iput-object v0, p0, LX/Eg1;->s:Landroid/view/View$OnClickListener;

    .line 2155659
    new-instance v0, LX/Efz;

    invoke-direct {v0, p0}, LX/Efz;-><init>(LX/Eg1;)V

    iput-object v0, p0, LX/Eg1;->t:LX/Efy;

    .line 2155660
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Eg1;->y:Z

    .line 2155661
    const-class v0, LX/Eg1;

    invoke-static {v0, p0}, LX/Eg1;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2155662
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2155663
    const v1, 0x7f031366

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2155664
    invoke-virtual {p0}, LX/Efo;->a()V

    .line 2155665
    const v0, 0x7f0d06b4

    invoke-virtual {p0, v0}, LX/Eg1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Eg1;->u:Landroid/view/View;

    .line 2155666
    const v0, 0x7f0d06b6

    invoke-virtual {p0, v0}, LX/Eg1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Eg1;->v:Lcom/facebook/resources/ui/FbTextView;

    .line 2155667
    const v0, 0x7f0d2ccf

    invoke-virtual {p0, v0}, LX/Eg1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    iput-object v0, p0, LX/Eg1;->w:Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    .line 2155668
    iget-object v0, p0, LX/Eg1;->u:Landroid/view/View;

    iget-object v1, p0, LX/Eg1;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2155669
    iget-object v0, p0, LX/Eg1;->w:Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    iget-object v1, p0, LX/Eg1;->t:LX/Efy;

    .line 2155670
    iput-object v1, v0, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->e:LX/Efy;

    .line 2155671
    return-void
.end method

.method public static a(LX/Eg1;Lcom/facebook/audience/snacks/model/SnacksFriendThread;)V
    .locals 2

    .prologue
    .line 2155644
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2155645
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2155646
    iget-object v0, p0, LX/Eg1;->m:Lcom/facebook/audience/util/PrefetchUtils;

    .line 2155647
    iget-object v1, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2155648
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/util/PrefetchUtils;->a(Landroid/net/Uri;)V

    .line 2155649
    :cond_0
    iget-object v0, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2155650
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2155651
    iget-object v0, p0, LX/Eg1;->m:Lcom/facebook/audience/util/PrefetchUtils;

    .line 2155652
    iget-object v1, p1, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2155653
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/audience/util/PrefetchUtils;->b(Landroid/net/Uri;)V

    .line 2155654
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v1, p1

    check-cast v1, LX/Eg1;

    new-instance v3, LX/ALw;

    invoke-static {v6}, LX/1Fm;->b(LX/0QB;)LX/1Fm;

    move-result-object v2

    check-cast v2, LX/1Fm;

    invoke-direct {v3, v2}, LX/ALw;-><init>(LX/1Fm;)V

    move-object v2, v3

    check-cast v2, LX/ALw;

    invoke-static {v6}, Lcom/facebook/audience/util/PrefetchUtils;->a(LX/0QB;)Lcom/facebook/audience/util/PrefetchUtils;

    move-result-object v3

    check-cast v3, Lcom/facebook/audience/util/PrefetchUtils;

    invoke-static {v6}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v4

    check-cast v4, LX/0fO;

    invoke-static {v6}, LX/7ga;->a(LX/0QB;)LX/7ga;

    move-result-object v5

    check-cast v5, LX/7ga;

    new-instance p1, LX/AIk;

    invoke-static {v6}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p0

    check-cast p0, LX/0tX;

    const/16 v0, 0x15e7

    invoke-static {v6, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-direct {p1, v7, p0, v0}, LX/AIk;-><init>(Ljava/util/concurrent/Executor;LX/0tX;LX/0Or;)V

    move-object v6, p1

    check-cast v6, LX/AIk;

    iput-object v2, v1, LX/Eg1;->l:LX/ALw;

    iput-object v3, v1, LX/Eg1;->m:Lcom/facebook/audience/util/PrefetchUtils;

    iput-object v4, v1, LX/Eg1;->n:LX/0fO;

    iput-object v5, v1, LX/Eg1;->o:LX/7ga;

    iput-object v6, v1, LX/Eg1;->p:LX/AIk;

    return-void
.end method

.method public static b(LX/Eg1;I)V
    .locals 7

    .prologue
    .line 2155632
    iget-object v0, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155633
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 2155634
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 2155635
    iget-object v0, p0, LX/Eg1;->o:LX/7ga;

    iget-object v1, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155636
    iget-object v2, v1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v2

    .line 2155637
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    .line 2155638
    iget-object v2, v3, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2155639
    invoke-virtual {p0}, LX/Eg1;->getCurrentThreadMediaType()LX/7gk;

    move-result-object v4

    sget-object v5, LX/7gZ;->FRIEND:LX/7gZ;

    .line 2155640
    iget-boolean v6, v3, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->c:Z

    move v3, v6

    .line 2155641
    if-nez v3, :cond_0

    const/4 v6, 0x1

    :goto_0
    move v3, p1

    invoke-virtual/range {v0 .. v6}, LX/7ga;->a(Ljava/lang/String;Ljava/lang/String;ILX/7gk;LX/7gZ;Z)V

    .line 2155642
    return-void

    .line 2155643
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 8

    .prologue
    .line 2155676
    invoke-virtual {p0}, LX/Efo;->h()V

    .line 2155677
    iget-object v0, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155678
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 2155679
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 2155680
    new-instance v1, LX/Eg0;

    invoke-direct {v1, v0}, LX/Eg0;-><init>(Lcom/facebook/audience/snacks/model/SnacksFriendThread;)V

    move-object v1, v1

    .line 2155681
    iget-object v2, p0, LX/Efo;->f:Lcom/facebook/audience/ui/ReplyThreadStoryView;

    invoke-virtual {v2, v1}, Lcom/facebook/audience/ui/ReplyThreadStoryView;->a(LX/AKy;)V

    .line 2155682
    iget-object v1, p0, LX/Efo;->e:Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;

    iget-object v2, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155683
    iget-object v3, v2, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v2, v3

    .line 2155684
    invoke-virtual {v2}, Lcom/facebook/audience/model/AudienceControlData;->getName()Ljava/lang/String;

    move-result-object v2

    .line 2155685
    iget-wide v6, v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->d:J

    move-wide v4, v6

    .line 2155686
    iget-object v3, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155687
    iget-object v6, v3, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v3, v6

    .line 2155688
    invoke-virtual {v3}, Lcom/facebook/audience/model/AudienceControlData;->getProfileUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v4, v5, v3}, Lcom/facebook/audience/ui/BackstageReplyHeaderBarView;->a(Ljava/lang/String;JLandroid/net/Uri;)V

    .line 2155689
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2155690
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p0, v1, p1}, LX/Efo;->a(ZI)V

    .line 2155691
    iget-boolean v1, p0, LX/Efo;->i:Z

    if-eqz v1, :cond_0

    .line 2155692
    invoke-virtual {p0}, LX/Efo;->g()V

    .line 2155693
    invoke-static {p0, p1}, LX/Eg1;->b(LX/Eg1;I)V

    .line 2155694
    :goto_0
    return-void

    .line 2155695
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Eg1;->y:Z

    .line 2155696
    invoke-static {p0, v0}, LX/Eg1;->a(LX/Eg1;Lcom/facebook/audience/snacks/model/SnacksFriendThread;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 2155625
    iget-object v0, p0, LX/Eg1;->n:LX/0fO;

    .line 2155626
    invoke-static {v0}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v1

    invoke-virtual {v1}, LX/0i8;->w()Z

    move-result v1

    move v0, v1

    .line 2155627
    if-eqz v0, :cond_1

    .line 2155628
    iget-object v0, p0, LX/Eg1;->w:Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2155629
    iget-object v0, p0, LX/Eg1;->w:Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->a()V

    .line 2155630
    :cond_0
    iget-object v0, p0, LX/Eg1;->w:Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->a(II)V

    .line 2155631
    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2155617
    iget-object v0, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155618
    iget-object v3, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v0, v3

    .line 2155619
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2155620
    if-gt v0, v2, :cond_0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155621
    iget-object v3, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v0, v3

    .line 2155622
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 2155623
    iget-object v3, v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    move-object v0, v3

    .line 2155624
    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2155582
    iget-object v0, p0, LX/Eg1;->w:Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2155583
    iget-object v0, p0, LX/Eg1;->w:Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;

    invoke-virtual {v0}, Lcom/facebook/audience/snacks/view/bucket/SnacksReactionRadioDockView;->a()V

    .line 2155584
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 7

    .prologue
    .line 2155602
    invoke-super {p0}, LX/Efo;->e()V

    .line 2155603
    iget-boolean v0, p0, LX/Eg1;->y:Z

    if-eqz v0, :cond_0

    .line 2155604
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Eg1;->y:Z

    .line 2155605
    iget-object v0, p0, LX/Efo;->h:LX/Efj;

    .line 2155606
    iget v1, v0, LX/Efj;->b:I

    move v0, v1

    .line 2155607
    invoke-static {p0, v0}, LX/Eg1;->b(LX/Eg1;I)V

    .line 2155608
    :cond_0
    iget-object v0, p0, LX/Eg1;->o:LX/7ga;

    iget-object v1, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155609
    iget-object v2, v1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v2

    .line 2155610
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    iget-object v3, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155611
    iget v4, v3, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    move v3, v4

    .line 2155612
    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    sget-object v4, LX/7gZ;->FRIEND:LX/7gZ;

    iget-object v5, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155613
    iget-object v6, v5, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v5, v6

    .line 2155614
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    sget-object v6, LX/7gd;->VIEWER:LX/7gd;

    invoke-virtual/range {v0 .. v6}, LX/7ga;->a(Ljava/lang/String;IZLX/7gZ;ILX/7gd;)V

    .line 2155615
    return-void

    .line 2155616
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getCurrentThreadId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2155595
    iget-object v0, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155596
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 2155597
    iget-object v1, p0, LX/Efo;->h:LX/Efj;

    .line 2155598
    iget p0, v1, LX/Efj;->b:I

    move v1, p0

    .line 2155599
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 2155600
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2155601
    return-object v0
.end method

.method public getCurrentThreadMediaType()LX/7gk;
    .locals 2

    .prologue
    .line 2155588
    iget-object v0, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155589
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 2155590
    iget-object v1, p0, LX/Efo;->h:LX/Efj;

    .line 2155591
    iget p0, v1, LX/Efj;->b:I

    move v1, p0

    .line 2155592
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;

    .line 2155593
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendThread;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2155594
    if-nez v0, :cond_0

    sget-object v0, LX/7gk;->PHOTO:LX/7gk;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/7gk;->VIDEO:LX/7gk;

    goto :goto_0
.end method

.method public getNumberOfStoriesInCurrentUserBucket()I
    .locals 1

    .prologue
    .line 2155585
    iget-object v0, p0, LX/Eg1;->x:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 2155586
    iget-object p0, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v0, p0

    .line 2155587
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
