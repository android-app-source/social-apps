.class public final LX/CoI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field public final synthetic a:LX/CoJ;


# direct methods
.method public constructor <init>(LX/CoJ;)V
    .locals 0

    .prologue
    .line 1935332
    iput-object p1, p0, LX/CoI;->a:LX/CoJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1935333
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1935334
    iget-object v0, p0, LX/CoI;->a:LX/CoJ;

    iget-object v0, v0, LX/CoJ;->n:Landroid/support/v7/widget/RecyclerView;

    if-ne p1, v0, :cond_0

    .line 1935335
    iget-object v0, p0, LX/CoI;->a:LX/CoJ;

    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 1935336
    iget-object v0, p0, LX/CoI;->a:LX/CoJ;

    iget-object v0, v0, LX/CoJ;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1935337
    :cond_0
    return-void
.end method
