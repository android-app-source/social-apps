.class public LX/Dde;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Dde;


# instance fields
.field private a:LX/Ddd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/DiN;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019967
    const/4 v0, -0x1

    iput v0, p0, LX/Dde;->c:I

    .line 2019968
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dde;->d:Z

    .line 2019969
    return-void
.end method

.method public static a(LX/0QB;)LX/Dde;
    .locals 3

    .prologue
    .line 2019935
    sget-object v0, LX/Dde;->e:LX/Dde;

    if-nez v0, :cond_1

    .line 2019936
    const-class v1, LX/Dde;

    monitor-enter v1

    .line 2019937
    :try_start_0
    sget-object v0, LX/Dde;->e:LX/Dde;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2019938
    if-eqz v2, :cond_0

    .line 2019939
    :try_start_1
    new-instance v0, LX/Dde;

    invoke-direct {v0}, LX/Dde;-><init>()V

    .line 2019940
    move-object v0, v0

    .line 2019941
    sput-object v0, LX/Dde;->e:LX/Dde;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2019942
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2019943
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2019944
    :cond_1
    sget-object v0, LX/Dde;->e:LX/Dde;

    return-object v0

    .line 2019945
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2019946
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2019947
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Dde;->d:Z

    .line 2019948
    const/4 v0, 0x0

    iput-object v0, p0, LX/Dde;->b:LX/DiN;

    .line 2019949
    const/4 v0, -0x1

    iput v0, p0, LX/Dde;->c:I

    .line 2019950
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2019951
    iget-object v0, p0, LX/Dde;->a:LX/Ddd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dde;->b:LX/DiN;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dde;->b:LX/DiN;

    .line 2019952
    iget-object v1, v0, LX/DiN;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2019953
    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2019954
    invoke-direct {p0}, LX/Dde;->a()V

    .line 2019955
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2019956
    iget-object v0, p0, LX/Dde;->a:LX/Ddd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dde;->b:LX/DiN;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dde;->b:LX/DiN;

    .line 2019957
    iget-object v1, v0, LX/DiN;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2019958
    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2019959
    if-nez p2, :cond_1

    .line 2019960
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Dde;->d:Z

    .line 2019961
    :cond_0
    :goto_0
    return-void

    .line 2019962
    :cond_1
    invoke-direct {p0}, LX/Dde;->a()V

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2019963
    const/4 v0, 0x0

    iput-object v0, p0, LX/Dde;->a:LX/Ddd;

    .line 2019964
    invoke-direct {p0}, LX/Dde;->a()V

    .line 2019965
    return-void
.end method
