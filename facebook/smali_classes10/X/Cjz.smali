.class public LX/Cjz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cju;


# static fields
.field public static final a:[I

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Cjy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1930341
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/Cjz;->a:[I

    .line 1930342
    new-instance v0, LX/Cjx;

    invoke-direct {v0}, LX/Cjx;-><init>()V

    sput-object v0, LX/Cjz;->b:Ljava/util/List;

    return-void

    .line 1930343
    :array_0
    .array-data 4
        0x280
        0x2ee
        0x4da
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 1930344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1930345
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    .line 1930346
    const/4 v0, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 1930347
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 1930348
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1930349
    sget-object v2, LX/Cjz;->a:[I

    aget v2, v2, v0

    if-ge v1, v2, :cond_1

    .line 1930350
    int-to-float v1, v1

    mul-float/2addr v1, v6

    sget-object v2, LX/Cjz;->a:[I

    aget v2, v2, v0

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {p0, v0, v1}, LX/Cjz;->a(LX/Cjz;IF)V

    .line 1930351
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    const v1, 0x7f0d011b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1930352
    iget-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    const v2, 0x7f0d011e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 1930353
    iget-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    const v3, 0x7f0d011f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1930354
    iget-object v3, p0, LX/Cjz;->c:Ljava/util/Map;

    const v4, 0x7f0d0174

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930355
    iget-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    const v3, 0x7f0d0175

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    add-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930356
    iget-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    const v1, 0x7f0d011d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1930357
    iget-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    const v2, 0x7f0d011b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 1930358
    iget-object v2, p0, LX/Cjz;->c:Ljava/util/Map;

    const v3, 0x7f0d0176

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930359
    iget-object v1, p0, LX/Cjz;->c:Ljava/util/Map;

    const v2, 0x7f0d0177

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1930360
    return-void

    .line 1930361
    :cond_1
    sget-object v2, LX/Cjz;->a:[I

    sget-object v3, LX/Cjz;->a:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget v2, v2, v3

    if-lt v1, v2, :cond_2

    .line 1930362
    sget-object v0, LX/Cjz;->a:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    .line 1930363
    int-to-float v1, v1

    mul-float/2addr v1, v6

    sget-object v2, LX/Cjz;->a:[I

    aget v2, v2, v0

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {p0, v0, v1}, LX/Cjz;->a(LX/Cjz;IF)V

    goto/16 :goto_0

    .line 1930364
    :cond_2
    sget-object v2, LX/Cjz;->a:[I

    array-length v2, v2

    :goto_1
    add-int/lit8 v3, v2, -0x1

    if-ge v0, v3, :cond_0

    .line 1930365
    add-int/lit8 v3, v0, 0x1

    .line 1930366
    sget-object v4, LX/Cjz;->a:[I

    aget v4, v4, v0

    .line 1930367
    sget-object v5, LX/Cjz;->a:[I

    aget v5, v5, v3

    .line 1930368
    if-ne v1, v4, :cond_3

    .line 1930369
    invoke-static {p0, v0, v6}, LX/Cjz;->a(LX/Cjz;IF)V

    goto/16 :goto_0

    .line 1930370
    :cond_3
    if-le v1, v4, :cond_4

    if-ge v1, v5, :cond_4

    .line 1930371
    sub-int/2addr v5, v4

    .line 1930372
    sub-int v4, v1, v4

    .line 1930373
    int-to-float v4, v4

    mul-float/2addr v4, v6

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 1930374
    sget-object v5, LX/Cjz;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Cjy;

    .line 1930375
    iget-object v8, v5, LX/Cjy;->b:[I

    aget v8, v8, v0

    int-to-float v8, v8

    const/high16 p1, 0x3f800000    # 1.0f

    sub-float/2addr p1, v4

    mul-float/2addr v8, p1

    iget-object p1, v5, LX/Cjy;->b:[I

    aget p1, p1, v3

    int-to-float p1, p1

    mul-float/2addr p1, v4

    add-float/2addr v8, p1

    .line 1930376
    iget-object p1, p0, LX/Cjz;->c:Ljava/util/Map;

    iget v5, v5, LX/Cjy;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-interface {p1, v5, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1930377
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static a(LX/Cjz;IF)V
    .locals 4

    .prologue
    .line 1930331
    sget-object v0, LX/Cjz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cjy;

    .line 1930332
    iget-object v2, v0, LX/Cjy;->b:[I

    aget v2, v2, p1

    int-to-float v2, v2

    mul-float/2addr v2, p2

    .line 1930333
    iget-object v3, p0, LX/Cjz;->c:Ljava/util/Map;

    iget v0, v0, LX/Cjy;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1930334
    :cond_0
    return-void
.end method

.method public static e(I)[I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1930335
    sget-object v1, LX/Cjz;->a:[I

    array-length v1, v1

    new-array v1, v1, [I

    .line 1930336
    sget-object v2, LX/Cjz;->a:[I

    aget v2, v2, v0

    .line 1930337
    :goto_0
    sget-object v3, LX/Cjz;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 1930338
    sget-object v3, LX/Cjz;->a:[I

    aget v3, v3, v0

    mul-int/2addr v3, p0

    div-int/2addr v3, v2

    aput v3, v1, v0

    .line 1930339
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1930340
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1930330
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 1930329
    iget-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(I)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1930323
    if-nez p1, :cond_1

    .line 1930324
    :cond_0
    :goto_0
    return v0

    .line 1930325
    :cond_1
    invoke-virtual {p0, p1}, LX/Cjz;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1930326
    new-instance v0, Landroid/content/res/Resources$NotFoundException;

    const-string v1, "id not found"

    invoke-direct {v0, v1}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1930327
    :cond_2
    iget-object v1, p0, LX/Cjz;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1930328
    iget-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public final c(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1930317
    if-nez p1, :cond_1

    .line 1930318
    :cond_0
    :goto_0
    return v0

    .line 1930319
    :cond_1
    invoke-virtual {p0, p1}, LX/Cjz;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1930320
    new-instance v0, Landroid/content/res/Resources$NotFoundException;

    const-string v1, "id not found"

    invoke-direct {v0, v1}, Landroid/content/res/Resources$NotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1930321
    :cond_2
    iget-object v1, p0, LX/Cjz;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1930322
    iget-object v0, p0, LX/Cjz;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method
