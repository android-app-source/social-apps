.class public LX/Dxb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Dxb;


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2063009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2063010
    iput-object p1, p0, LX/Dxb;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2063011
    return-void
.end method

.method public static a(LX/0QB;)LX/Dxb;
    .locals 4

    .prologue
    .line 2063012
    sget-object v0, LX/Dxb;->b:LX/Dxb;

    if-nez v0, :cond_1

    .line 2063013
    const-class v1, LX/Dxb;

    monitor-enter v1

    .line 2063014
    :try_start_0
    sget-object v0, LX/Dxb;->b:LX/Dxb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2063015
    if-eqz v2, :cond_0

    .line 2063016
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2063017
    new-instance p0, LX/Dxb;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3}, LX/Dxb;-><init>(Lcom/facebook/content/SecureContextHelper;)V

    .line 2063018
    move-object v0, p0

    .line 2063019
    sput-object v0, LX/Dxb;->b:LX/Dxb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2063020
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2063021
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2063022
    :cond_1
    sget-object v0, LX/Dxb;->b:LX/Dxb;

    return-object v0

    .line 2063023
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2063024
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 2063025
    iget-object v0, p0, LX/Dxb;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2063026
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->ALBUM:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    sget-object v2, LX/21D;->ALBUM:LX/21D;

    const-string p0, "mediaPickerLauncher"

    invoke-static {v2, p0}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2063027
    iput-object v2, v1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2063028
    move-object v1, v1

    .line 2063029
    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v1

    .line 2063030
    move-object v1, v1

    .line 2063031
    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2063032
    return-void
.end method
