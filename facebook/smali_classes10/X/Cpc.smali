.class public final LX/Cpc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1938327
    iput-object p1, p0, LX/Cpc;->c:Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    iput-object p2, p0, LX/Cpc;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Cpc;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x6155c573

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1938294
    iget-object v1, p0, LX/Cpc;->c:Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    iget-object v2, p0, LX/Cpc;->a:Ljava/lang/String;

    iget-object v3, p0, LX/Cpc;->b:Ljava/lang/String;

    .line 1938295
    new-instance v6, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v6, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1938296
    if-nez v2, :cond_4

    const/4 v5, 0x0

    :goto_0
    invoke-virtual {v6, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1938297
    const-string v5, "extra_instant_articles_id"

    invoke-virtual {v6, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938298
    const-string v5, "extra_instant_articles_canonical_url"

    invoke-virtual {v6, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938299
    const-string v5, "com.android.browser.headers"

    invoke-static {}, LX/Cs3;->a()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {v6, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1938300
    const-string v5, "extra_instant_articles_referrer"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->y:Ljava/lang/String;

    invoke-virtual {v6, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938301
    const-string v5, "extra_parent_article_click_source"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->g:LX/Chi;

    .line 1938302
    iget-object p1, p0, LX/Chi;->j:Ljava/lang/String;

    move-object p0, p1

    .line 1938303
    invoke-virtual {v6, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938304
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->e:LX/ClD;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/ClD;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1938305
    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 1938306
    const-string p0, "click_source_document_chaining_id"

    invoke-virtual {v6, p0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938307
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->e:LX/ClD;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v5

    .line 1938308
    const/4 p0, -0x1

    if-eq v5, p0, :cond_0

    .line 1938309
    const-string p0, "click_source_document_depth"

    invoke-virtual {v6, p0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1938310
    :cond_0
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1938311
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->i:LX/Cig;

    new-instance p0, LX/Cin;

    invoke-direct {p0}, LX/Cin;-><init>()V

    invoke-virtual {v5, p0}, LX/0b4;->a(LX/0b7;)V

    .line 1938312
    :cond_1
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v5, v6, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1938313
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1938314
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1938315
    const-string v5, "article_ID"

    invoke-interface {v6, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938316
    :cond_2
    const-string v5, "ia_source"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->y:Ljava/lang/String;

    invoke-interface {v6, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938317
    const-string v5, "position"

    iget p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->w:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v6, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938318
    const-string p0, "is_instant_article"

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v6, p0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938319
    const-string v5, "click_source"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->y:Ljava/lang/String;

    invoke-interface {v6, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938320
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->B:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 1938321
    const-string v5, "block_id"

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->B:Ljava/lang/String;

    invoke-interface {v6, v5, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938322
    :cond_3
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->c:LX/Ckw;

    invoke-virtual {v5, v2, v6}, LX/Ckw;->b(Ljava/lang/String;Ljava/util/Map;)V

    .line 1938323
    iget-object v5, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->c:LX/Ckw;

    iget-object v6, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->y:Ljava/lang/String;

    iget-object p0, v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->B:Ljava/lang/String;

    invoke-virtual {v5, v2, v6, p0}, LX/Ckw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1938324
    const v1, -0x56dd9aeb

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1938325
    :cond_4
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    goto/16 :goto_0

    .line 1938326
    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method
