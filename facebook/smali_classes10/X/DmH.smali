.class public abstract LX/DmH;
.super LX/1a1;
.source ""


# instance fields
.field public final m:Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;

.field public final n:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public final o:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2039118
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2039119
    const v0, 0x7f0d05ab

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;

    iput-object v0, p0, LX/DmH;->m:Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;

    .line 2039120
    const v0, 0x7f0d05ac

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, LX/DmH;->n:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2039121
    const v0, 0x7f0d05ad

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/DmH;->o:Landroid/widget/TextView;

    .line 2039122
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentFieldsModel;)V
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039110
    iget-object v0, p0, LX/DmH;->m:Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/messaging/professionalservices/booking/ui/CalendarDateView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039111
    iget-object v0, p0, LX/DmH;->n:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2039112
    iget-object v0, p0, LX/DmH;->n:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2039113
    if-eqz p5, :cond_0

    .line 2039114
    iget-object v0, p0, LX/DmH;->o:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2039115
    iget-object v0, p0, LX/DmH;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2039116
    :goto_0
    return-void

    .line 2039117
    :cond_0
    iget-object v0, p0, LX/DmH;->o:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
