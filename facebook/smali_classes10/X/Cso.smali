.class public LX/Cso;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Cso;


# instance fields
.field private final a:LX/0Uh;

.field private final b:Z


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1943338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1943339
    iput-object p1, p0, LX/Cso;->a:LX/0Uh;

    .line 1943340
    iget-object v0, p0, LX/Cso;->a:LX/0Uh;

    const/16 v1, 0x6f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Cso;->b:Z

    .line 1943341
    return-void
.end method

.method public static b(LX/0QB;)LX/Cso;
    .locals 4

    .prologue
    .line 1943342
    sget-object v0, LX/Cso;->c:LX/Cso;

    if-nez v0, :cond_1

    .line 1943343
    const-class v1, LX/Cso;

    monitor-enter v1

    .line 1943344
    :try_start_0
    sget-object v0, LX/Cso;->c:LX/Cso;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1943345
    if-eqz v2, :cond_0

    .line 1943346
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1943347
    new-instance p0, LX/Cso;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/Cso;-><init>(LX/0Uh;)V

    .line 1943348
    move-object v0, p0

    .line 1943349
    sput-object v0, LX/Cso;->c:LX/Cso;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1943350
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1943351
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1943352
    :cond_1
    sget-object v0, LX/Cso;->c:LX/Cso;

    return-object v0

    .line 1943353
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1943354
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public a(ZZ)LX/Cuo;
    .locals 1

    .prologue
    .line 1943355
    iget-boolean v0, p0, LX/Cso;->b:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    .line 1943356
    invoke-static {}, LX/Cuo;->a()LX/Cuo;

    move-result-object v0

    .line 1943357
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/Cuo;->b()LX/Cuo;

    move-result-object v0

    goto :goto_0
.end method
