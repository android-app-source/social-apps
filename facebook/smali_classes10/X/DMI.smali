.class public final enum LX/DMI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DMI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DMI;

.field public static final enum EVENT_ROW:LX/DMI;

.field public static final enum EVENT_TIME_BUCKET_HEADER:LX/DMI;

.field public static final enum LOADING_BAR:LX/DMI;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1989710
    new-instance v0, LX/DMI;

    const-string v1, "EVENT_ROW"

    invoke-direct {v0, v1, v2}, LX/DMI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DMI;->EVENT_ROW:LX/DMI;

    .line 1989711
    new-instance v0, LX/DMI;

    const-string v1, "EVENT_TIME_BUCKET_HEADER"

    invoke-direct {v0, v1, v3}, LX/DMI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DMI;->EVENT_TIME_BUCKET_HEADER:LX/DMI;

    .line 1989712
    new-instance v0, LX/DMI;

    const-string v1, "LOADING_BAR"

    invoke-direct {v0, v1, v4}, LX/DMI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DMI;->LOADING_BAR:LX/DMI;

    .line 1989713
    const/4 v0, 0x3

    new-array v0, v0, [LX/DMI;

    sget-object v1, LX/DMI;->EVENT_ROW:LX/DMI;

    aput-object v1, v0, v2

    sget-object v1, LX/DMI;->EVENT_TIME_BUCKET_HEADER:LX/DMI;

    aput-object v1, v0, v3

    sget-object v1, LX/DMI;->LOADING_BAR:LX/DMI;

    aput-object v1, v0, v4

    sput-object v0, LX/DMI;->$VALUES:[LX/DMI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1989709
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DMI;
    .locals 1

    .prologue
    .line 1989715
    const-class v0, LX/DMI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DMI;

    return-object v0
.end method

.method public static values()[LX/DMI;
    .locals 1

    .prologue
    .line 1989714
    sget-object v0, LX/DMI;->$VALUES:[LX/DMI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DMI;

    return-object v0
.end method
