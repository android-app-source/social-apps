.class public final LX/Dwj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumFeedbackQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 0

    .prologue
    .line 2061250
    iput-object p1, p0, LX/Dwj;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2061251
    iget-object v0, p0, LX/Dwj;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v0, v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dy7;

    iget-object v1, p0, LX/Dwj;->a:Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    iget-object v1, v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    const/4 p0, 0x0

    .line 2061252
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2061253
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "album Node ID cannot be null."

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Exception;)LX/52H;

    move-result-object v2

    .line 2061254
    :goto_0
    move-object v0, v2

    .line 2061255
    return-object v0

    .line 2061256
    :cond_0
    new-instance v2, LX/5gZ;

    invoke-direct {v2}, LX/5gZ;-><init>()V

    move-object v3, v2

    .line 2061257
    const-string v2, "node_id"

    invoke-virtual {v3, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v4, "max_comments"

    const/16 v5, 0x19

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v4, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v4, "fetch_reshare_counts"

    iget-object v5, v0, LX/Dy7;->f:LX/0ad;

    sget-short v6, LX/0wn;->aF:S

    invoke-interface {v5, v6, p0}, LX/0ad;->a(SZ)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2061258
    iget-object v2, v0, LX/Dy7;->d:LX/0tG;

    invoke-virtual {v2, v3}, LX/0tG;->a(LX/0gW;)V

    .line 2061259
    iget-object v2, v0, LX/Dy7;->d:LX/0tG;

    invoke-virtual {v2, v3}, LX/0tG;->b(LX/0gW;)V

    .line 2061260
    iget-object v2, v0, LX/Dy7;->e:LX/0tE;

    invoke-virtual {v2, v3, p0}, LX/0tE;->a(LX/0gW;Z)V

    .line 2061261
    iget-object v2, v0, LX/Dy7;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    goto :goto_0
.end method
