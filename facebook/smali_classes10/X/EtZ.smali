.class public LX/EtZ;
.super LX/6Ua;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/5P0;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field private d:I

.field public e:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/0ad;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/5P0;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2178289
    invoke-direct {p0}, LX/6Ua;-><init>()V

    .line 2178290
    iput-object p1, p0, LX/EtZ;->a:Landroid/content/Context;

    .line 2178291
    iput-object p2, p0, LX/EtZ;->b:LX/0Px;

    .line 2178292
    iput-object p3, p0, LX/EtZ;->e:LX/0ad;

    .line 2178293
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2178294
    sget-object v2, LX/EtY;->a:[I

    iget-object v0, p0, LX/EtZ;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5P0;

    invoke-virtual {v0}, LX/5P0;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 2178295
    :goto_0
    return-object v0

    .line 2178296
    :pswitch_0
    iget v0, p0, LX/EtZ;->c:I

    if-lez v0, :cond_0

    iget v0, p0, LX/EtZ;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    goto :goto_0

    .line 2178297
    :pswitch_1
    iget v0, p0, LX/EtZ;->d:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget v0, p0, LX/EtZ;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 2178298
    sget-object v1, LX/EtY;->a:[I

    iget-object v0, p0, LX/EtZ;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5P0;

    invoke-virtual {v0}, LX/5P0;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2178299
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2178300
    :pswitch_0
    iget-object v0, p0, LX/EtZ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0818a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2178301
    :pswitch_1
    iget-object v0, p0, LX/EtZ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0818a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2178302
    :pswitch_2
    iget-object v0, p0, LX/EtZ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f005a

    iget v2, p0, LX/EtZ;->c:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, LX/EtZ;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2178303
    :pswitch_3
    iget-object v0, p0, LX/EtZ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0818a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2178304
    :pswitch_4
    iget-object v0, p0, LX/EtZ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0818a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2178305
    :pswitch_5
    iget-object v0, p0, LX/EtZ;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0818a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 2178306
    iget v0, p0, LX/EtZ;->c:I

    if-ne v0, p1, :cond_0

    .line 2178307
    :goto_0
    return-void

    .line 2178308
    :cond_0
    iput p1, p0, LX/EtZ;->c:I

    .line 2178309
    invoke-virtual {p0}, LX/6UZ;->b()V

    goto :goto_0
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 2178310
    iget-object v0, p0, LX/EtZ;->e:LX/0ad;

    sget-short v1, LX/2hr;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2178311
    if-eqz v0, :cond_0

    iget v0, p0, LX/EtZ;->d:I

    if-ne v0, p1, :cond_1

    .line 2178312
    :cond_0
    :goto_0
    return-void

    .line 2178313
    :cond_1
    iput p1, p0, LX/EtZ;->d:I

    .line 2178314
    invoke-virtual {p0}, LX/6UZ;->b()V

    goto :goto_0
.end method
