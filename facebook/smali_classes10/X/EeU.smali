.class public LX/EeU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/1sV;


# direct methods
.method public constructor <init>(Ljava/lang/Long;LX/1sV;)V
    .locals 0
    .param p1    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2152724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152725
    iput-object p1, p0, LX/EeU;->a:Ljava/lang/Long;

    .line 2152726
    iput-object p2, p0, LX/EeU;->b:LX/1sV;

    .line 2152727
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2152728
    iget-object v0, p0, LX/EeU;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2152729
    iget-object v0, p0, LX/EeU;->b:LX/1sV;

    iget-object v1, p0, LX/EeU;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/1sV;->a(J)V

    .line 2152730
    :cond_0
    return-void
.end method

.method public final a(LX/EeX;)V
    .locals 9

    .prologue
    .line 2152731
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2152732
    const/4 v2, 0x0

    .line 2152733
    :try_start_0
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2152734
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2152735
    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    .line 2152736
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 2152737
    iget-object v1, p0, LX/EeU;->a:Ljava/lang/Long;

    if-nez v1, :cond_2

    .line 2152738
    iget-object v1, p0, LX/EeU;->b:LX/1sV;

    .line 2152739
    iget-object v4, v1, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v4, :cond_0

    .line 2152740
    new-instance v4, LX/2ze;

    iget-object v5, v1, LX/1sV;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/2ze;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, LX/2ze;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    iput-object v4, v1, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 2152741
    :cond_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2152742
    const-string v5, "data"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2152743
    iget-object v5, v1, LX/1sV;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "app_updates"

    const/4 v7, 0x0

    const v8, 0x74a70d03

    invoke-static {v8}, LX/03h;->a(I)V

    invoke-virtual {v5, v6, v7, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    const v6, -0x516831fe

    invoke-static {v6}, LX/03h;->a(I)V

    move-wide v0, v4

    .line 2152744
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, LX/EeU;->a:Ljava/lang/Long;

    .line 2152745
    :goto_0
    return-void

    .line 2152746
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/ObjectOutputStream;->close()V

    :cond_1
    throw v0

    .line 2152747
    :cond_2
    iget-object v1, p0, LX/EeU;->b:LX/1sV;

    iget-object v2, p0, LX/EeU;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, LX/1sV;->a(J[B)V

    goto :goto_0

    .line 2152748
    :catchall_1
    move-exception v0

    goto :goto_1
.end method
