.class public final LX/DqN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field public final synthetic b:Lcom/facebook/widget/SwitchCompat;

.field public final synthetic c:Landroid/widget/Button;

.field public final synthetic d:LX/DqO;


# direct methods
.method public constructor <init>(LX/DqO;Lcom/facebook/fbui/widget/contentview/CheckedContentView;Lcom/facebook/widget/SwitchCompat;Landroid/widget/Button;)V
    .locals 0

    .prologue
    .line 2048284
    iput-object p1, p0, LX/DqN;->d:LX/DqO;

    iput-object p2, p0, LX/DqN;->a:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object p3, p0, LX/DqN;->b:Lcom/facebook/widget/SwitchCompat;

    iput-object p4, p0, LX/DqN;->c:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, -0x525a52cc

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2048285
    iget-object v1, p0, LX/DqN;->d:LX/DqO;

    iget-boolean v1, v1, LX/DqO;->g:Z

    iget-object v2, p0, LX/DqN;->d:LX/DqO;

    iget-boolean v2, v2, LX/DqO;->f:Z

    if-eq v1, v2, :cond_0

    .line 2048286
    iget-object v1, p0, LX/DqN;->d:LX/DqO;

    const-string v2, "notifications"

    iget-object v3, p0, LX/DqN;->d:LX/DqO;

    iget-boolean v3, v3, LX/DqO;->f:Z

    invoke-static {v1, v2, v3}, LX/DqO;->a$redex0(LX/DqO;Ljava/lang/String;Z)V

    .line 2048287
    :cond_0
    iget-object v1, p0, LX/DqN;->d:LX/DqO;

    iget-object v1, v1, LX/DqO;->c:LX/3Q7;

    invoke-virtual {v1}, LX/3Q7;->b()Z

    move-result v1

    iget-object v2, p0, LX/DqN;->a:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->isChecked()Z

    move-result v2

    if-eq v1, v2, :cond_1

    .line 2048288
    iget-object v1, p0, LX/DqN;->d:LX/DqO;

    const-string v2, "light"

    iget-object v3, p0, LX/DqN;->a:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v3}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->isChecked()Z

    move-result v3

    invoke-static {v1, v2, v3}, LX/DqO;->a$redex0(LX/DqO;Ljava/lang/String;Z)V

    .line 2048289
    :cond_1
    iget-object v1, p0, LX/DqN;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v1}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/DqN;->d:LX/DqO;

    iget-boolean v1, v1, LX/DqO;->f:Z

    if-nez v1, :cond_2

    .line 2048290
    iget-object v1, p0, LX/DqN;->d:LX/DqO;

    .line 2048291
    iput-boolean v5, v1, LX/DqO;->f:Z

    .line 2048292
    :cond_2
    iget-object v1, p0, LX/DqN;->d:LX/DqO;

    iget-object v1, v1, LX/DqO;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->l:LX/0Tn;

    iget-object v3, p0, LX/DqN;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v3}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v3

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->F:LX/0Tn;

    iget-object v3, p0, LX/DqN;->d:LX/DqO;

    iget-boolean v3, v3, LX/DqO;->f:Z

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->G:LX/0Tn;

    iget-object v3, p0, LX/DqN;->a:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v3}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->isChecked()Z

    move-result v3

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2048293
    iget-object v1, p0, LX/DqN;->d:LX/DqO;

    iget-object v1, v1, LX/DqO;->h:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_3

    .line 2048294
    iget-object v1, p0, LX/DqN;->d:LX/DqO;

    iget-object v1, v1, LX/DqO;->h:Landroid/view/View$OnClickListener;

    iget-object v2, p0, LX/DqN;->c:Landroid/widget/Button;

    invoke-interface {v1, v2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2048295
    :cond_3
    const v1, -0x518fa2d3

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
