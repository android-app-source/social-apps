.class public final LX/Eh5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 2157860
    const/16 v17, 0x0

    .line 2157861
    const/16 v16, 0x0

    .line 2157862
    const/4 v15, 0x0

    .line 2157863
    const/4 v14, 0x0

    .line 2157864
    const/4 v13, 0x0

    .line 2157865
    const/4 v12, 0x0

    .line 2157866
    const/4 v11, 0x0

    .line 2157867
    const/4 v10, 0x0

    .line 2157868
    const/4 v9, 0x0

    .line 2157869
    const/4 v8, 0x0

    .line 2157870
    const/4 v7, 0x0

    .line 2157871
    const/4 v6, 0x0

    .line 2157872
    const/4 v5, 0x0

    .line 2157873
    const/4 v4, 0x0

    .line 2157874
    const/4 v3, 0x0

    .line 2157875
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 2157876
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2157877
    const/4 v3, 0x0

    .line 2157878
    :goto_0
    return v3

    .line 2157879
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2157880
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_c

    .line 2157881
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 2157882
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2157883
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 2157884
    const-string v19, "bookmarked_node"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 2157885
    invoke-static/range {p0 .. p1}, LX/Eh2;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 2157886
    :cond_2
    const-string v19, "has_user_pinned"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 2157887
    const/4 v6, 0x1

    .line 2157888
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 2157889
    :cond_3
    const-string v19, "id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 2157890
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 2157891
    :cond_4
    const-string v19, "image"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 2157892
    invoke-static/range {p0 .. p1}, LX/Eh3;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 2157893
    :cond_5
    const-string v19, "is_pinnable"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 2157894
    const/4 v5, 0x1

    .line 2157895
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 2157896
    :cond_6
    const-string v19, "max_unread_count"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 2157897
    const/4 v4, 0x1

    .line 2157898
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto :goto_1

    .line 2157899
    :cond_7
    const-string v19, "post_name_icon"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 2157900
    invoke-static/range {p0 .. p1}, LX/Eh4;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 2157901
    :cond_8
    const-string v19, "section"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 2157902
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLBookmarkSection;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBookmarkSection;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto/16 :goto_1

    .line 2157903
    :cond_9
    const-string v19, "unread_count"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 2157904
    const/4 v3, 0x1

    .line 2157905
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 2157906
    :cond_a
    const-string v19, "unread_count_string"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 2157907
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 2157908
    :cond_b
    const-string v19, "url"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 2157909
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 2157910
    :cond_c
    const/16 v18, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2157911
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2157912
    if-eqz v6, :cond_d

    .line 2157913
    const/4 v6, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 2157914
    :cond_d
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 2157915
    const/4 v6, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 2157916
    if-eqz v5, :cond_e

    .line 2157917
    const/4 v5, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->a(IZ)V

    .line 2157918
    :cond_e
    if-eqz v4, :cond_f

    .line 2157919
    const/4 v4, 0x5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12, v5}, LX/186;->a(III)V

    .line 2157920
    :cond_f
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 2157921
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 2157922
    if-eqz v3, :cond_10

    .line 2157923
    const/16 v3, 0x8

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 2157924
    :cond_10
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 2157925
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2157926
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x0

    .line 2157927
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2157928
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2157929
    if-eqz v0, :cond_0

    .line 2157930
    const-string v1, "bookmarked_node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157931
    invoke-static {p0, v0, p2}, LX/Eh2;->a(LX/15i;ILX/0nX;)V

    .line 2157932
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2157933
    if-eqz v0, :cond_1

    .line 2157934
    const-string v1, "has_user_pinned"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157935
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2157936
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2157937
    if-eqz v0, :cond_2

    .line 2157938
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157939
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2157940
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2157941
    if-eqz v0, :cond_3

    .line 2157942
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157943
    invoke-static {p0, v0, p2}, LX/Eh3;->a(LX/15i;ILX/0nX;)V

    .line 2157944
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2157945
    if-eqz v0, :cond_4

    .line 2157946
    const-string v1, "is_pinnable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157947
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2157948
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2157949
    if-eqz v0, :cond_5

    .line 2157950
    const-string v1, "max_unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157951
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2157952
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2157953
    if-eqz v0, :cond_6

    .line 2157954
    const-string v1, "post_name_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157955
    invoke-static {p0, v0, p2}, LX/Eh4;->a(LX/15i;ILX/0nX;)V

    .line 2157956
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2157957
    if-eqz v0, :cond_7

    .line 2157958
    const-string v0, "section"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157959
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2157960
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2157961
    if-eqz v0, :cond_8

    .line 2157962
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157963
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2157964
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2157965
    if-eqz v0, :cond_9

    .line 2157966
    const-string v1, "unread_count_string"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157967
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2157968
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2157969
    if-eqz v0, :cond_a

    .line 2157970
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2157971
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2157972
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2157973
    return-void
.end method
