.class public LX/DTZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/17Y;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:Ljava/lang/String;

.field private final f:LX/DTh;

.field public final g:LX/DT7;

.field public final h:LX/00H;

.field public i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/DSE;

.field public l:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field public m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/DTh;LX/DT7;LX/00H;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2000589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2000590
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/DTZ;->i:Ljava/util/Set;

    .line 2000591
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/DTZ;->j:Ljava/util/Set;

    .line 2000592
    iput-object p1, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    .line 2000593
    iput-object p2, p0, LX/DTZ;->a:Ljava/lang/String;

    .line 2000594
    iput-object p3, p0, LX/DTZ;->e:Ljava/lang/String;

    .line 2000595
    iput-object p4, p0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2000596
    iput-object p5, p0, LX/DTZ;->c:LX/17Y;

    .line 2000597
    iput-object p6, p0, LX/DTZ;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2000598
    iput-object p7, p0, LX/DTZ;->f:LX/DTh;

    .line 2000599
    iput-object p8, p0, LX/DTZ;->g:LX/DT7;

    .line 2000600
    iput-object p9, p0, LX/DTZ;->h:LX/00H;

    .line 2000601
    return-void
.end method

.method public static a(LX/DTZ;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1

    .prologue
    .line 2000588
    new-instance v0, LX/DTY;

    invoke-direct {v0, p0, p1}, LX/DTY;-><init>(LX/DTZ;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLObjectType;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1

    .prologue
    .line 2000587
    new-instance v0, LX/DTD;

    invoke-direct {v0, p0, p1, p2, p3}, LX/DTD;-><init>(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLObjectType;)V

    return-object v0
.end method

.method public static a(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1

    .prologue
    .line 2000586
    new-instance v0, LX/DTN;

    invoke-direct {v0, p0, p1, p2, p3}, LX/DTN;-><init>(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    return-object v0
.end method

.method public static a(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Z)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 6

    .prologue
    .line 2000585
    new-instance v0, LX/DTX;

    move-object v1, p0

    move-object v2, p1

    move v3, p4

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/DTX;-><init>(LX/DTZ;Ljava/lang/String;ZLandroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(LX/DTZ;Landroid/content/Context;LX/DSf;Ljava/lang/String;Ljava/lang/String;LX/5OG;)V
    .locals 2

    .prologue
    .line 2000574
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v0, v1, :cond_0

    .line 2000575
    iget-object v0, p2, LX/DSf;->e:LX/DS2;

    move-object v0, v0

    .line 2000576
    iget-object v1, v0, LX/DS2;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2000577
    iget-object v1, p0, LX/DTZ;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2000578
    :cond_0
    iget-object v0, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    const v1, 0x7f082f8a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 2000579
    new-instance v1, LX/DTL;

    invoke-direct {v1, p0, p3, p4, p1}, LX/DTL;-><init>(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    move-object v1, v1

    .line 2000580
    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000581
    iget-object v0, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    const v1, 0x7f082f8e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 2000582
    new-instance v1, LX/DTJ;

    invoke-direct {v1, p0, p3, p4}, LX/DTJ;-><init>(LX/DTZ;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v1

    .line 2000583
    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000584
    :cond_1
    return-void
.end method

.method private a(Landroid/view/View;LX/DSf;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;Z)V
    .locals 9

    .prologue
    .line 2000492
    if-eqz p5, :cond_4

    invoke-virtual {p5}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p5}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;->j()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 2000493
    :goto_0
    new-instance v8, LX/5OM;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2000494
    const/4 v0, 0x0

    .line 2000495
    iput-boolean v0, v8, LX/0ht;->e:Z

    .line 2000496
    invoke-virtual {v8}, LX/5OM;->c()LX/5OG;

    move-result-object v5

    .line 2000497
    invoke-virtual {v8, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2000498
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v0, v1, :cond_10

    .line 2000499
    :cond_0
    invoke-virtual {p2}, LX/DSf;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2000500
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, LX/DTZ;->a(LX/DTZ;Landroid/content/Context;LX/DSf;Ljava/lang/String;Ljava/lang/String;LX/5OG;)V

    .line 2000501
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2000502
    invoke-static {p2}, LX/DTZ;->a(LX/DSf;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 2000503
    :cond_2
    :goto_2
    invoke-virtual {v5}, LX/5OG;->getCount()I

    move-result v0

    if-lez v0, :cond_11

    .line 2000504
    invoke-virtual {v8}, LX/0ht;->d()V

    .line 2000505
    :cond_3
    :goto_3
    return-void

    :cond_4
    move-object v7, p3

    .line 2000506
    goto :goto_0

    .line 2000507
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v0, v1, :cond_a

    .line 2000508
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v6, p6

    const/4 p5, 0x1

    const/4 p4, 0x0

    .line 2000509
    invoke-virtual {v2}, LX/DSf;->f()Z

    move-result p3

    if-nez p3, :cond_1a

    .line 2000510
    invoke-virtual {v2}, LX/DSf;->d()Z

    move-result p3

    if-nez p3, :cond_6

    iget-object p3, v0, LX/DTZ;->i:Ljava/util/Set;

    invoke-interface {p3, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_15

    :cond_6
    move p3, p5

    .line 2000511
    :goto_4
    invoke-virtual {v2}, LX/DSf;->e()Z

    move-result p6

    if-nez p6, :cond_7

    iget-object p6, v0, LX/DTZ;->j:Ljava/util/Set;

    invoke-interface {p6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p6

    if-eqz p6, :cond_16

    .line 2000512
    :cond_7
    :goto_5
    iget-object p6, v0, LX/DTZ;->a:Ljava/lang/String;

    invoke-virtual {p6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    .line 2000513
    if-eqz p3, :cond_17

    .line 2000514
    iget-object p3, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p4, 0x7f082f9b

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p3

    .line 2000515
    new-instance p4, LX/DTB;

    invoke-direct {p4, v0, v3, v1, v4}, LX/DTB;-><init>(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    move-object p4, p4

    .line 2000516
    invoke-virtual {p3, p4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000517
    if-eqz p6, :cond_8

    .line 2000518
    iget-object p3, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p4, 0x7f082fb1

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p3

    .line 2000519
    new-instance p4, LX/DTT;

    invoke-direct {p4, v0, v3, v1}, LX/DTT;-><init>(LX/DTZ;Ljava/lang/String;Landroid/content/Context;)V

    move-object p4, p4

    .line 2000520
    invoke-virtual {p3, p4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000521
    :cond_8
    :goto_6
    if-nez p6, :cond_9

    .line 2000522
    iget-object p3, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p4, 0x7f082f91

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p3

    invoke-static {v0, v3, v4, v1}, LX/DTZ;->c(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p4

    invoke-virtual {p3, p4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000523
    iget-object p3, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p4, 0x7f082f86

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p3

    invoke-static {v0, v3, v4, v1}, LX/DTZ;->a(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p4

    invoke-virtual {p3, p4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000524
    :cond_9
    :goto_7
    goto/16 :goto_1

    .line 2000525
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-ne v0, v1, :cond_1

    .line 2000526
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    const/4 p3, 0x0

    const/4 p4, 0x1

    .line 2000527
    invoke-virtual {v2}, LX/DSf;->f()Z

    move-result v6

    if-nez v6, :cond_1c

    .line 2000528
    invoke-virtual {v2}, LX/DSf;->d()Z

    move-result v6

    if-nez v6, :cond_b

    iget-object v6, v0, LX/DTZ;->i:Ljava/util/Set;

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    :cond_b
    move v6, p4

    .line 2000529
    :goto_8
    invoke-virtual {v2}, LX/DSf;->e()Z

    move-result p5

    if-nez p5, :cond_c

    iget-object p5, v0, LX/DTZ;->j:Ljava/util/Set;

    invoke-interface {p5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p5

    if-eqz p5, :cond_d

    :cond_c
    move p3, p4

    .line 2000530
    :cond_d
    iget-object p5, v0, LX/DTZ;->a:Ljava/lang/String;

    invoke-virtual {p5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p5

    .line 2000531
    if-eqz p5, :cond_e

    .line 2000532
    iget-object p5, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p6, 0x7f082fac

    invoke-virtual {p5, p6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p5

    invoke-virtual {v5, p5}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p5

    invoke-static {v0, v3, v4, v1, p4}, LX/DTZ;->a(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Z)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p4

    invoke-virtual {p5, p4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000533
    :cond_e
    if-nez v6, :cond_f

    if-nez p3, :cond_f

    .line 2000534
    iget-object v6, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p3, 0x7f082f91

    invoke-virtual {v6, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    invoke-static {v0, v3, v4, v1}, LX/DTZ;->c(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p3

    invoke-virtual {v6, p3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000535
    iget-object v6, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p3, 0x7f082f86

    invoke-virtual {v6, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    invoke-static {v0, v3, v4, v1}, LX/DTZ;->a(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p3

    invoke-virtual {v6, p3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000536
    :cond_f
    :goto_9
    goto/16 :goto_1

    .line 2000537
    :cond_10
    iget-object v0, p0, LX/DTZ;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, LX/DSf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, LX/DSf;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2000538
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, LX/DTZ;->a(LX/DTZ;Landroid/content/Context;LX/DSf;Ljava/lang/String;Ljava/lang/String;LX/5OG;)V

    goto/16 :goto_1

    .line 2000539
    :cond_11
    invoke-static {p2}, LX/DTZ;->a(LX/DSf;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2000540
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2000541
    iget-object v1, p2, LX/DSf;->d:LX/DUV;

    move-object v1, v1

    .line 2000542
    invoke-interface {v1}, LX/DUU;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-static {p0, v7, v0, v1}, LX/DTZ;->b(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLObjectType;)Z

    goto/16 :goto_3

    .line 2000543
    :cond_12
    iget-object v1, p2, LX/DSf;->d:LX/DUV;

    move-object v1, v1

    .line 2000544
    invoke-interface {v1}, LX/DUU;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 2000545
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x25d6af

    if-ne v1, v2, :cond_13

    iget-object v1, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    const v2, 0x7f082f84

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2000546
    :goto_a
    iget-object v2, p0, LX/DTZ;->h:LX/00H;

    .line 2000547
    iget-object v3, v2, LX/00H;->j:LX/01T;

    move-object v2, v3

    .line 2000548
    sget-object v3, LX/01T;->GROUPS:LX/01T;

    if-ne v2, v3, :cond_14

    .line 2000549
    invoke-virtual {v5, v1}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    .line 2000550
    iget-object v2, p2, LX/DSf;->d:LX/DUV;

    move-object v2, v2

    .line 2000551
    invoke-interface {v2}, LX/DUU;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-static {p0, v7, v0, v2}, LX/DTZ;->a(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLObjectType;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000552
    iget-object v1, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    const v2, 0x7f082fb8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    .line 2000553
    new-instance v2, LX/DTC;

    invoke-direct {v2, p0, v0, v7}, LX/DTC;-><init>(LX/DTZ;Landroid/content/Context;Ljava/lang/String;)V

    move-object v2, v2

    .line 2000554
    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 2000555
    :cond_13
    iget-object v1, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    const v2, 0x7f082f82

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_a

    .line 2000556
    :cond_14
    invoke-virtual {v5}, LX/5OG;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 2000557
    invoke-virtual {v5, v1}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    .line 2000558
    iget-object v2, p2, LX/DSf;->d:LX/DUV;

    move-object v2, v2

    .line 2000559
    invoke-interface {v2}, LX/DUU;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-static {p0, v7, v0, v2}, LX/DTZ;->a(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLObjectType;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_2

    :cond_15
    move p3, p4

    .line 2000560
    goto/16 :goto_4

    :cond_16
    move p5, p4

    .line 2000561
    goto/16 :goto_5

    .line 2000562
    :cond_17
    if-eqz p5, :cond_19

    .line 2000563
    if-nez v6, :cond_18

    .line 2000564
    iget-object p3, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p5, 0x7f082f85

    invoke-virtual {p3, p5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p3

    invoke-static {v0, v3}, LX/DTZ;->a(LX/DTZ;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p5

    invoke-virtual {p3, p5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000565
    :cond_18
    iget-object p3, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p5, 0x7f082fac

    invoke-virtual {p3, p5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p3

    invoke-static {v0, v3, v4, v1, p4}, LX/DTZ;->a(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Z)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p4

    invoke-virtual {p3, p4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_6

    .line 2000566
    :cond_19
    if-nez v6, :cond_8

    .line 2000567
    iget-object p3, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p4, 0x7f082fa7

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p3

    .line 2000568
    new-instance p4, LX/DTV;

    invoke-direct {p4, v0, v3, v1, v4}, LX/DTV;-><init>(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    move-object p4, p4

    .line 2000569
    invoke-virtual {p3, p4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2000570
    iget-object p3, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p4, 0x7f082f85

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p3

    invoke-static {v0, v3}, LX/DTZ;->a(LX/DTZ;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p4

    invoke-virtual {p3, p4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_6

    .line 2000571
    :cond_1a
    iget-object p3, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p4, 0x7f082f87

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p3

    invoke-static {v0, v3, v4, v1}, LX/DTZ;->b(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p4

    invoke-virtual {p3, p4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_7

    :cond_1b
    move v6, p3

    .line 2000572
    goto/16 :goto_8

    .line 2000573
    :cond_1c
    iget-object v6, v0, LX/DTZ;->b:Landroid/content/res/Resources;

    const p3, 0x7f082f87

    invoke-virtual {v6, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    invoke-static {v0, v3, v4, v1}, LX/DTZ;->b(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object p3

    invoke-virtual {v6, p3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_9
.end method

.method public static a(LX/DSf;)Z
    .locals 2

    .prologue
    .line 2000489
    if-eqz p0, :cond_0

    .line 2000490
    iget-object v0, p0, LX/DSf;->f:LX/DSd;

    if-eqz v0, :cond_1

    sget-object v0, LX/DSd;->EMAIL_INVITE:LX/DSd;

    invoke-virtual {v0}, LX/DSd;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/DSf;->f:LX/DSd;

    invoke-virtual {v1}, LX/DSd;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2000491
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/DTZ;ILjava/lang/String;)Landroid/text/SpannableString;
    .locals 7
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v6, 0x21

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2000602
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v3

    invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2000603
    :goto_0
    iget-object v1, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    const v2, 0x7f082faa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2000604
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v3

    aput-object v1, v2, v4

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2000605
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2000606
    new-instance v3, LX/DTI;

    invoke-direct {v3, p0}, LX/DTI;-><init>(LX/DTZ;)V

    .line 2000607
    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2000608
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    iget-object v4, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    const v5, 0x7f0a05ee

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2000609
    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v3, v0, v1, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2000610
    return-object v2

    .line 2000611
    :cond_0
    iget-object v0, p0, LX/DTZ;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2000488
    const-class v0, Landroid/app/Activity;

    invoke-static {p0, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method public static b(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1

    .prologue
    .line 2000487
    new-instance v0, LX/DTP;

    invoke-direct {v0, p0, p1, p2, p3}, LX/DTP;-><init>(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    return-object v0
.end method

.method public static b(LX/DTZ;Ljava/lang/String;Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLObjectType;)Z
    .locals 3

    .prologue
    .line 2000479
    invoke-static {p2}, LX/DTZ;->b(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v0

    .line 2000480
    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x25d6af

    if-ne v1, v2, :cond_0

    .line 2000481
    iget-object v1, p0, LX/DTZ;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DTZ;->c:LX/17Y;

    .line 2000482
    sget-object p0, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2000483
    invoke-interface {v2, v0, p0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 2000484
    invoke-interface {v1, p0, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2000485
    const/4 p0, 0x1

    move v0, p0

    .line 2000486
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, LX/DTZ;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/DTZ;->c:LX/17Y;

    invoke-static {p1, v1, v0, v2}, LX/DTo;->a(Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;Landroid/app/Activity;LX/17Y;)Z

    move-result v0

    goto :goto_0
.end method

.method public static c(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1

    .prologue
    .line 2000459
    new-instance v0, LX/DTR;

    invoke-direct {v0, p0, p1, p2, p3}, LX/DTR;-><init>(LX/DTZ;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/DRo;)V
    .locals 1

    .prologue
    .line 2000477
    iget-object v0, p0, LX/DTZ;->f:LX/DTh;

    invoke-virtual {v0, p1}, LX/0b4;->a(LX/0b2;)Z

    .line 2000478
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2000474
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iget-object v1, p0, LX/DTZ;->m:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/DTZ;->h:LX/00H;

    .line 2000475
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 2000476
    sget-object v1, LX/01T;->GROUPS:LX/01T;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/DRo;)V
    .locals 1

    .prologue
    .line 2000472
    iget-object v0, p0, LX/DTZ;->f:LX/DTh;

    invoke-virtual {v0, p1}, LX/0b4;->b(LX/0b2;)Z

    .line 2000473
    return-void
.end method

.method public final b(LX/DSE;)V
    .locals 1

    .prologue
    .line 2000469
    iget-object v0, p0, LX/DTZ;->k:LX/DSE;

    if-ne v0, p1, :cond_0

    .line 2000470
    const/4 v0, 0x0

    iput-object v0, p0, LX/DTZ;->k:LX/DSE;

    .line 2000471
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;LX/DSf;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Z)V
    .locals 7
    .param p2    # LX/DSf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2000460
    if-eqz p2, :cond_0

    .line 2000461
    iget-object v0, p2, LX/DSf;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    move-object v5, v0

    .line 2000462
    iget-object v0, p2, LX/DSf;->d:LX/DUV;

    move-object v0, v0

    .line 2000463
    invoke-interface {v0}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v3

    .line 2000464
    iget-object v0, p2, LX/DSf;->d:LX/DUV;

    move-object v0, v0

    .line 2000465
    invoke-interface {v0}, LX/DUU;->kA_()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v6, p4

    .line 2000466
    invoke-direct/range {v0 .. v6}, LX/DTZ;->a(Landroid/view/View;LX/DSf;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;Z)V

    .line 2000467
    iput-object p3, p0, LX/DTZ;->l:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2000468
    :cond_0
    return-void
.end method
