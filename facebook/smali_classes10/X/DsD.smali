.class public final LX/DsD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)V
    .locals 0

    .prologue
    .line 2050132
    iput-object p1, p0, LX/DsD;->a:Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x524e2d8a

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2050133
    iget-object v2, p0, LX/DsD;->a:Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;

    invoke-static {v2}, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->p(Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2050134
    :goto_0
    iget-object v2, p0, LX/DsD;->a:Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;

    .line 2050135
    iget-object p0, v2, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    sget-object p1, LX/0hM;->n:LX/0Tn;

    invoke-interface {p0, p1, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object p0

    invoke-interface {p0}, LX/0hN;->commit()V

    .line 2050136
    iget-object p0, v2, Lcom/facebook/notifications/widget/NotificationSettingsAlertsFragment;->c:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 2050137
    const v0, 0x28d9e776

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2050138
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
