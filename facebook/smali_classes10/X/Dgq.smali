.class public final enum LX/Dgq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dgq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dgq;

.field public static final enum LOCATION_FETCH_FAILED:LX/Dgq;

.field public static final enum PLACES_FETCH_FAILED:LX/Dgq;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2029915
    new-instance v0, LX/Dgq;

    const-string v1, "LOCATION_FETCH_FAILED"

    invoke-direct {v0, v1, v2}, LX/Dgq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dgq;->LOCATION_FETCH_FAILED:LX/Dgq;

    new-instance v0, LX/Dgq;

    const-string v1, "PLACES_FETCH_FAILED"

    invoke-direct {v0, v1, v3}, LX/Dgq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dgq;->PLACES_FETCH_FAILED:LX/Dgq;

    .line 2029916
    const/4 v0, 0x2

    new-array v0, v0, [LX/Dgq;

    sget-object v1, LX/Dgq;->LOCATION_FETCH_FAILED:LX/Dgq;

    aput-object v1, v0, v2

    sget-object v1, LX/Dgq;->PLACES_FETCH_FAILED:LX/Dgq;

    aput-object v1, v0, v3

    sput-object v0, LX/Dgq;->$VALUES:[LX/Dgq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2029917
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dgq;
    .locals 1

    .prologue
    .line 2029918
    const-class v0, LX/Dgq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dgq;

    return-object v0
.end method

.method public static values()[LX/Dgq;
    .locals 1

    .prologue
    .line 2029919
    sget-object v0, LX/Dgq;->$VALUES:[LX/Dgq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dgq;

    return-object v0
.end method
