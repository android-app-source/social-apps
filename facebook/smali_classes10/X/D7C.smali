.class public LX/D7C;
.super LX/2oy;
.source ""


# instance fields
.field public final a:Lcom/facebook/widget/text/BetterTextView;

.field public final b:Landroid/widget/ProgressBar;

.field public c:LX/D7B;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1966848
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D7C;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1966849
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1966846
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D7C;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966847
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1966841
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966842
    const v0, 0x7f030987

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1966843
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/D7C;->b:Landroid/widget/ProgressBar;

    .line 1966844
    const v0, 0x7f0d1857

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/D7C;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1966845
    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 1966837
    iget-object v0, p0, LX/D7C;->c:LX/D7B;

    if-eqz v0, :cond_0

    .line 1966838
    iget-object v0, p0, LX/D7C;->c:LX/D7B;

    invoke-virtual {v0}, LX/D7B;->cancel()V

    .line 1966839
    const/4 v0, 0x0

    iput-object v0, p0, LX/D7C;->c:LX/D7B;

    .line 1966840
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 0

    .prologue
    .line 1966835
    invoke-direct {p0}, LX/D7C;->h()V

    .line 1966836
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1966833
    invoke-direct {p0}, LX/D7C;->h()V

    .line 1966834
    return-void
.end method
