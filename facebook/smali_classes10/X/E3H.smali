.class public LX/E3H;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/03V;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2073794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2073795
    iput-object p1, p0, LX/E3H;->a:LX/03V;

    .line 2073796
    return-void
.end method

.method public static a(LX/0QB;)LX/E3H;
    .locals 4

    .prologue
    .line 2073797
    const-class v1, LX/E3H;

    monitor-enter v1

    .line 2073798
    :try_start_0
    sget-object v0, LX/E3H;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2073799
    sput-object v2, LX/E3H;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2073800
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2073801
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2073802
    new-instance p0, LX/E3H;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/E3H;-><init>(LX/03V;)V

    .line 2073803
    move-object v0, p0

    .line 2073804
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2073805
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/E3H;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2073806
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2073807
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
