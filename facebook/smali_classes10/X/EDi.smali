.class public final LX/EDi;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/contacts/graphql/Contact;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;)V
    .locals 0

    .prologue
    .line 2091789
    iput-object p1, p0, LX/EDi;->a:LX/EDx;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2091790
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2091791
    check-cast p1, LX/0Px;

    const/4 v2, 0x0

    .line 2091792
    if-nez p1, :cond_1

    .line 2091793
    :cond_0
    return-void

    .line 2091794
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_3

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2091795
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2091796
    iget-object v1, p0, LX/EDi;->a:LX/EDx;

    iget-object v1, v1, LX/EDx;->ae:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EGE;

    .line 2091797
    if-eqz v1, :cond_2

    .line 2091798
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, LX/EGE;->d:Ljava/lang/String;

    .line 2091799
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/EGE;->c:Ljava/lang/String;

    .line 2091800
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2091801
    :cond_3
    iget-object v0, p0, LX/EDi;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/EDi;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EC0;

    .line 2091802
    invoke-virtual {v0}, LX/EC0;->p()V

    .line 2091803
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
