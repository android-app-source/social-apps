.class public abstract LX/Ep8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EoA;


# static fields
.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/EnR;

.field public final b:LX/Eod;

.field public final c:LX/Enj;

.field private final e:LX/EoB;

.field private final f:LX/EpA;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2170013
    const-class v0, LX/Ep8;

    sput-object v0, LX/Ep8;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/EoB;LX/EpA;LX/EnR;LX/Eod;LX/Enj;)V
    .locals 0

    .prologue
    .line 2170006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2170007
    iput-object p1, p0, LX/Ep8;->e:LX/EoB;

    .line 2170008
    iput-object p2, p0, LX/Ep8;->f:LX/EpA;

    .line 2170009
    iput-object p3, p0, LX/Ep8;->a:LX/EnR;

    .line 2170010
    iput-object p4, p0, LX/Ep8;->b:LX/Eod;

    .line 2170011
    iput-object p5, p0, LX/Ep8;->c:LX/Enj;

    .line 2170012
    return-void
.end method

.method private static c(Landroid/os/Bundle;)LX/0P1;
    .locals 8
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2169989
    if-nez p0, :cond_0

    .line 2169990
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2169991
    :goto_0
    return-object v0

    .line 2169992
    :cond_0
    const-string v0, "preliminary_entities"

    invoke-static {p0, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2169993
    if-nez v0, :cond_1

    .line 2169994
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2169995
    goto :goto_0

    .line 2169996
    :cond_1
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 2169997
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 2169998
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    .line 2169999
    invoke-virtual {v1}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 2170000
    invoke-virtual {v4, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2170001
    sget-object v1, LX/Ep8;->d:Ljava/lang/Class;

    const-string v6, "Duplicate model IDs in the list of preliminary entities"

    invoke-static {v1, v6}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2170002
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2170003
    :cond_2
    invoke-virtual {v4, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2170004
    invoke-virtual {v3, v6, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_2

    .line 2170005
    :cond_3
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(LX/EnQ;Lcom/facebook/common/callercontext/CallerContext;Landroid/os/Bundle;)LX/End;
    .locals 1
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2169988
    new-instance v0, LX/Ene;

    invoke-direct {v0, p1}, LX/Ene;-><init>(LX/EnQ;)V

    return-object v0
.end method

.method public final a(LX/1My;LX/0Px;Ljava/lang/String;LX/Emq;LX/Emy;Lcom/facebook/common/callercontext/CallerContext;Landroid/os/Bundle;)LX/Enl;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1My;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/Emq;",
            "LX/Emy;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Landroid/os/Bundle;",
            ")",
            "Lcom/facebook/entitycards/model/EntityCardsDataSource;"
        }
    .end annotation

    .prologue
    .line 2169979
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2169980
    invoke-static {p2, p3}, LX/EnR;->a(LX/0Px;Ljava/lang/String;)LX/EnQ;

    move-result-object v3

    .line 2169981
    iget-object v2, p0, LX/Ep8;->b:LX/Eod;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v2, v4, v0}, LX/Eod;->a(LX/0am;Lcom/facebook/common/callercontext/CallerContext;)LX/Eoc;

    move-result-object v5

    .line 2169982
    iget-object v2, p0, LX/Ep8;->c:LX/Enj;

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-virtual {p0, v3, v0, v1}, LX/Ep8;->a(LX/EnQ;Lcom/facebook/common/callercontext/CallerContext;Landroid/os/Bundle;)LX/End;

    move-result-object v4

    invoke-virtual {p0}, LX/Ep8;->a()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v2, v4, v5, v6, v0}, LX/Enj;->a(LX/End;LX/Eoc;Ljava/lang/String;Landroid/os/Bundle;)LX/Eni;

    move-result-object v4

    .line 2169983
    invoke-static/range {p7 .. p7}, LX/Ep8;->c(Landroid/os/Bundle;)LX/0P1;

    move-result-object v10

    .line 2169984
    iget-object v2, p0, LX/Ep8;->e:LX/EoB;

    invoke-virtual {p0}, LX/Ep8;->a()Ljava/lang/String;

    move-result-object v8

    iget-object v9, v3, LX/EnQ;->c:LX/En3;

    iget-object v11, v3, LX/EnQ;->b:Ljava/lang/String;

    move-object v3, p1

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v11}, LX/EoB;->a(LX/1My;LX/Enh;LX/Eoc;LX/Emq;LX/Emy;Ljava/lang/String;LX/En3;LX/0P1;Ljava/lang/String;)LX/Enl;

    move-result-object v2

    return-object v2
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 2169985
    iget-object v0, p0, LX/Ep8;->f:LX/EpA;

    .line 2169986
    invoke-virtual {v0}, LX/EpA;->a()I

    move-result v1

    invoke-static {v0}, LX/EpA;->g(LX/EpA;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0}, LX/EpA;->h(LX/EpA;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0}, LX/EpA;->d()I

    move-result v2

    iget-object v3, v0, LX/EpA;->b:Landroid/content/res/Resources;

    const p0, 0x7f0b2227

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    move v0, v1

    .line 2169987
    return v0
.end method
