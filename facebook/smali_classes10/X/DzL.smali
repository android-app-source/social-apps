.class public LX/DzL;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/DzK;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066760
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2066761
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/9jJ;Landroid/app/Activity;Lcom/facebook/places/checkin/PlacePickerFragment;LX/6Zb;)LX/DzK;
    .locals 19

    .prologue
    .line 2066758
    new-instance v1, LX/DzK;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/9j5;->a(LX/0QB;)LX/9j5;

    move-result-object v8

    check-cast v8, LX/9j5;

    invoke-static/range {p0 .. p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v9

    check-cast v9, LX/0y3;

    invoke-static/range {p0 .. p0}, LX/0aW;->a(LX/0QB;)LX/0aW;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/1Ml;->a(LX/0QB;)LX/1Ml;

    move-result-object v11

    check-cast v11, LX/1Ml;

    const-class v2, LX/0i4;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/0i4;

    invoke-static/range {p0 .. p0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v13

    check-cast v13, LX/0s6;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v14

    check-cast v14, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v15

    check-cast v15, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v16

    check-cast v16, LX/0if;

    const-class v2, LX/DzJ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/DzJ;

    const-class v2, LX/Dz8;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/Dz8;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v18}, LX/DzK;-><init>(Landroid/view/View;LX/9jJ;Landroid/app/Activity;Lcom/facebook/places/checkin/PlacePickerFragment;LX/6Zb;LX/0ad;LX/9j5;LX/0y3;LX/0Xl;LX/1Ml;LX/0i4;LX/0s6;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/0if;LX/DzJ;LX/Dz8;)V

    .line 2066759
    return-object v1
.end method
