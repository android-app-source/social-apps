.class public LX/D15;
.super LX/D14;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final f:Ljava/lang/String;

.field private static k:LX/0Xm;


# instance fields
.field public final g:Landroid/content/Context;

.field public final h:LX/BiM;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/D12;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1956283
    const-class v0, LX/D15;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/D15;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/BiM;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BiM;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1956278
    invoke-direct {p0}, LX/D14;-><init>()V

    .line 1956279
    iput-object p1, p0, LX/D15;->g:Landroid/content/Context;

    .line 1956280
    iput-object p2, p0, LX/D15;->h:LX/BiM;

    .line 1956281
    iput-object p3, p0, LX/D15;->i:LX/0Ot;

    .line 1956282
    return-void
.end method

.method public static a(LX/0QB;)LX/D15;
    .locals 6

    .prologue
    .line 1956251
    const-class v1, LX/D15;

    monitor-enter v1

    .line 1956252
    :try_start_0
    sget-object v0, LX/D15;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1956253
    sput-object v2, LX/D15;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1956254
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1956255
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1956256
    new-instance v5, LX/D15;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/BiM;->b(LX/0QB;)LX/BiM;

    move-result-object v4

    check-cast v4, LX/BiM;

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/D15;-><init>(Landroid/content/Context;LX/BiM;LX/0Ot;)V

    .line 1956257
    move-object v0, v5

    .line 1956258
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1956259
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D15;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1956260
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1956261
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1956275
    iget-object v0, p0, LX/D15;->j:LX/D12;

    .line 1956276
    iget-object p0, v0, LX/D12;->d:LX/0Px;

    move-object v0, p0

    .line 1956277
    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1956269
    iget-object v0, p0, LX/D15;->j:LX/D12;

    .line 1956270
    iput-object p1, v0, LX/D12;->d:LX/0Px;

    .line 1956271
    iget-object v0, p0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->removeAllViews()V

    .line 1956272
    iget-object v0, p0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget v1, p0, LX/D14;->c:I

    iget v2, p0, LX/D14;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->h(II)V

    .line 1956273
    iget-object v0, p0, LX/D15;->j:LX/D12;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1956274
    return-void
.end method

.method public final a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 2

    .prologue
    .line 1956263
    iput-object p1, p0, LX/D15;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1956264
    new-instance v0, LX/D12;

    invoke-direct {v0, p0}, LX/D12;-><init>(LX/D15;)V

    iput-object v0, p0, LX/D15;->j:LX/D12;

    .line 1956265
    iget-object v0, p0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, LX/D15;->j:LX/D12;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1956266
    iget-object v0, p0, LX/D14;->a:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    new-instance v1, LX/D0z;

    invoke-direct {v1, p0}, LX/D0z;-><init>(LX/D15;)V

    .line 1956267
    iput-object v1, v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->n:LX/2ec;

    .line 1956268
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;
    .locals 1

    .prologue
    .line 1956262
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;->COMPACT:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    return-object v0
.end method
