.class public final LX/Edy;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/about/AboutActivity;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/about/AboutActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2151724
    iput-object p1, p0, LX/Edy;->a:Lcom/facebook/about/AboutActivity;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 2151725
    iput-object p2, p0, LX/Edy;->b:Ljava/lang/String;

    .line 2151726
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2151727
    iget-object v0, p0, LX/Edy;->a:Lcom/facebook/about/AboutActivity;

    iget-object v1, p0, LX/Edy;->a:Lcom/facebook/about/AboutActivity;

    iget-object v2, p0, LX/Edy;->b:Ljava/lang/String;

    .line 2151728
    invoke-static {v0, v1, v2}, Lcom/facebook/about/AboutActivity;->a$redex0(Lcom/facebook/about/AboutActivity;Landroid/content/Context;Ljava/lang/String;)V

    .line 2151729
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2151730
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2151731
    iget-object v0, p0, LX/Edy;->a:Lcom/facebook/about/AboutActivity;

    if-eqz v0, :cond_0

    .line 2151732
    iget-object v0, p0, LX/Edy;->a:Lcom/facebook/about/AboutActivity;

    invoke-virtual {v0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2151733
    :cond_0
    return-void
.end method
