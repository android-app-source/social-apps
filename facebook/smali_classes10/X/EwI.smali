.class public final LX/EwI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EwJ;


# direct methods
.method public constructor <init>(LX/EwJ;)V
    .locals 0

    .prologue
    .line 2183103
    iput-object p1, p0, LX/EwI;->a:LX/EwJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2183104
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2183105
    check-cast p1, LX/0Px;

    const/4 v0, 0x0

    .line 2183106
    if-nez p1, :cond_0

    .line 2183107
    :goto_0
    return-void

    .line 2183108
    :cond_0
    new-instance v4, LX/0cA;

    invoke-direct {v4}, LX/0cA;-><init>()V

    .line 2183109
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v0

    move v2, v0

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2183110
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2183111
    iget-object v1, p0, LX/EwI;->a:LX/EwJ;

    iget-object v1, v1, LX/EwJ;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v1, v1, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/83X;

    .line 2183112
    if-eqz v1, :cond_2

    instance-of v8, v1, LX/Ewo;

    if-eqz v8, :cond_2

    .line 2183113
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->m()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2183114
    iget-object v0, p0, LX/EwI;->a:LX/EwJ;

    iget-object v0, v0, LX/EwJ;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2183115
    check-cast v1, LX/Ewo;

    invoke-virtual {v4, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2183116
    const/4 v0, 0x1

    .line 2183117
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_1

    .line 2183118
    :cond_1
    invoke-interface {v1}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    if-eq v6, v7, :cond_2

    .line 2183119
    invoke-virtual {v0}, Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-interface {v1, v0}, LX/2np;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    :cond_2
    move v0, v2

    goto :goto_2

    .line 2183120
    :cond_3
    if-eqz v2, :cond_5

    .line 2183121
    iget-object v0, p0, LX/EwI;->a:LX/EwJ;

    iget-object v0, v0, LX/EwJ;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    .line 2183122
    iget-object v2, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 2183123
    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183124
    iget-object v2, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->w:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ak:LX/Eui;

    invoke-virtual {v2}, LX/Eui;->a()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2183125
    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->v(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183126
    :cond_4
    goto/16 :goto_0

    .line 2183127
    :cond_5
    iget-object v0, p0, LX/EwI;->a:LX/EwJ;

    iget-object v0, v0, LX/EwJ;->a:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    iget-object v0, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->ai:LX/EwG;

    const v1, 0x55b1dfbb

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_0
.end method
