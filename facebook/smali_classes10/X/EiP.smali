.class public final LX/EiP;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V
    .locals 0

    .prologue
    .line 2159360
    iput-object p1, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 2159361
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v1, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159362
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159363
    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159364
    iget-object v2, v0, LX/2U9;->a:LX/0Zb;

    sget-object v3, LX/Eiw;->CONFIRMATION_FAILURE:LX/Eiw;

    invoke-virtual {v3}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2159365
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2159366
    const-string v3, "confirmation"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159367
    const-string v3, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159368
    const-string v3, "error_code"

    invoke-static {p1}, LX/2U9;->a(Lcom/facebook/fbservice/service/ServiceException;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2159369
    const-string v3, "error_desc"

    .line 2159370
    iget-object v4, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v4, v4

    .line 2159371
    iget-object v0, v4, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v4, v0

    .line 2159372
    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159373
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2159374
    :cond_0
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "pin"

    iget-object v2, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v2, v2, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2159375
    iget-object v1, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->l:LX/2U9;

    sget-object v2, LX/Eiw;->INVALID_PIN:LX/Eiw;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159376
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2159377
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2159378
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v1, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159379
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159380
    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159381
    iget-object v2, v0, LX/2U9;->a:LX/0Zb;

    sget-object v3, LX/Eiw;->CONFIRMATION_SUCCESS:LX/Eiw;

    invoke-virtual {v3}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    const/4 p1, 0x1

    invoke-interface {v2, v3, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2159382
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2159383
    const-string v3, "confirmation"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159384
    const-string v3, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159385
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2159386
    :cond_0
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "pin"

    iget-object v2, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v2, v2, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2159387
    iget-object v1, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->l:LX/2U9;

    sget-object v2, LX/Eiw;->VALID_PIN:LX/Eiw;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, LX/2U9;->a(LX/Eiw;Ljava/lang/String;LX/1rQ;)V

    .line 2159388
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->l:LX/2U9;

    invoke-virtual {v0}, LX/2U9;->a()V

    .line 2159389
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159390
    iget-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->c:Z

    move v0, v1

    .line 2159391
    if-eqz v0, :cond_2

    .line 2159392
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0833f6

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2159393
    :goto_0
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159394
    iget-boolean v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->b:Z

    move v0, v1

    .line 2159395
    if-eqz v0, :cond_1

    .line 2159396
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->d:LX/EjB;

    invoke-virtual {v0}, LX/EjB;->a()V

    .line 2159397
    :cond_1
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    sget-object v1, LX/EiG;->CODE_SUCCESS:LX/EiG;

    invoke-virtual {v0, v1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(LX/EiG;)V

    .line 2159398
    return-void

    .line 2159399
    :cond_2
    iget-object v0, p0, LX/EiP;->a:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0833f5

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_0
.end method
