.class public LX/EVc;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field public b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2128502
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EVc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128503
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2128504
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/EVc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V

    .line 2128505
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2128506
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2128507
    const v0, 0x7f0315c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2128508
    const v0, 0x7f0d3114

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/EVc;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2128509
    const v0, 0x7f0d0721

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/EVc;->b:Landroid/view/View;

    .line 2128510
    const v0, 0x7f0a00c0

    invoke-virtual {p0, v0}, LX/EVc;->setBackgroundResource(I)V

    .line 2128511
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/EVc;->setOrientation(I)V

    .line 2128512
    return-void
.end method
