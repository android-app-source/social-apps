.class public LX/EOj;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EOh;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2115126
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/EOj;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115123
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2115124
    iput-object p1, p0, LX/EOj;->b:LX/0Ot;

    .line 2115125
    return-void
.end method

.method public static a(LX/0QB;)LX/EOj;
    .locals 4

    .prologue
    .line 2115112
    const-class v1, LX/EOj;

    monitor-enter v1

    .line 2115113
    :try_start_0
    sget-object v0, LX/EOj;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115114
    sput-object v2, LX/EOj;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115115
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115116
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115117
    new-instance v3, LX/EOj;

    const/16 p0, 0x346e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/EOj;-><init>(LX/0Ot;)V

    .line 2115118
    move-object v0, v3

    .line 2115119
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115120
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115121
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 13

    .prologue
    .line 2115107
    check-cast p2, LX/EOi;

    .line 2115108
    iget-object v0, p0, LX/EOj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;

    iget-object v2, p2, LX/EOi;->c:LX/EOS;

    iget-object v3, p2, LX/EOi;->a:LX/CzL;

    iget-object v4, p2, LX/EOi;->b:Ljava/lang/String;

    iget-object v5, p2, LX/EOi;->d:Ljava/lang/String;

    iget-object v6, p2, LX/EOi;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v1, p1

    .line 2115109
    if-eqz v2, :cond_0

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    move-object v10, v5

    move-object v11, v6

    move-object v12, v1

    .line 2115110
    invoke-interface/range {v7 .. v12}, LX/EOS;->onClick(LX/CzL;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Landroid/view/View;)V

    .line 2115111
    :cond_0
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2115106
    const v0, -0x43b1df81

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2115071
    check-cast p2, LX/EOi;

    .line 2115072
    iget-object v0, p0, LX/EOj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;

    iget-object v1, p2, LX/EOi;->a:LX/CzL;

    iget-object v2, p2, LX/EOi;->b:Ljava/lang/String;

    const/16 p2, 0x8

    const/4 p0, 0x1

    const/4 v8, 0x0

    .line 2115073
    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v3

    const v4, 0x7f021447

    invoke-virtual {v3, v4}, LX/1nm;->h(I)LX/1nm;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    .line 2115074
    iget-object v3, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 2115075
    check-cast v3, LX/8d2;

    .line 2115076
    invoke-interface {v3}, LX/8d2;->B()LX/1Fb;

    move-result-object v4

    if-nez v4, :cond_0

    .line 2115077
    const/4 v3, 0x0

    .line 2115078
    :goto_0
    move-object v0, v3

    .line 2115079
    return-object v0

    .line 2115080
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    .line 2115081
    const v6, -0x43b1df81

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 2115082
    invoke-interface {v4, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x4

    invoke-interface {v4, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a00d5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-interface {v4, v6}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v4

    .line 2115083
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 2115084
    iget-object v6, v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->d:LX/0sX;

    invoke-virtual {v6}, LX/0sX;->a()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, LX/8d2;->A()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-interface {v3}, LX/8d2;->A()Ljava/lang/String;

    move-result-object v6

    :goto_1
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2115085
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2115086
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v6, v6

    .line 2115087
    invoke-interface {v4, v6}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v7

    iget-object v4, v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-interface {v3}, LX/8d2;->B()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v3

    sget-object v4, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    sget-object v4, LX/1Up;->g:LX/1Up;

    invoke-virtual {v3, v4}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, LX/1up;->c(F)LX/1up;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    .line 2115088
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2115089
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Di;->c(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2, v8}, LX/1Di;->k(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00d5

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Di;->c(I)LX/1Di;

    move-result-object v5

    invoke-interface {v5, v8, v8}, LX/1Di;->k(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x3

    invoke-interface {v5, v6, v8}, LX/1Di;->k(II)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0061

    invoke-interface {v5, p2, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2115090
    :cond_1
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    .line 2115091
    :cond_2
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v1, 0x7f080072

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2115100
    invoke-static {}, LX/1dS;->b()V

    .line 2115101
    iget v0, p1, LX/1dQ;->b:I

    .line 2115102
    packed-switch v0, :pswitch_data_0

    .line 2115103
    :goto_0
    return-object v2

    .line 2115104
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2115105
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/EOj;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x43b1df81
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/EOh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2115092
    new-instance v1, LX/EOi;

    invoke-direct {v1, p0}, LX/EOi;-><init>(LX/EOj;)V

    .line 2115093
    sget-object v2, LX/EOj;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EOh;

    .line 2115094
    if-nez v2, :cond_0

    .line 2115095
    new-instance v2, LX/EOh;

    invoke-direct {v2}, LX/EOh;-><init>()V

    .line 2115096
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/EOh;->a$redex0(LX/EOh;LX/1De;IILX/EOi;)V

    .line 2115097
    move-object v1, v2

    .line 2115098
    move-object v0, v1

    .line 2115099
    return-object v0
.end method
