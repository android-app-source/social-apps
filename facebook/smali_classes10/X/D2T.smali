.class public LX/D2T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final h:Ljava/lang/Object;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/0SG;

.field private d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private e:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private f:Z

.field public g:LX/D2S;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1958806
    const-class v0, LX/D2T;

    sput-object v0, LX/D2T;->a:Ljava/lang/Class;

    .line 1958807
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/D2T;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1958799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958800
    iput-object p1, p0, LX/D2T;->b:Ljava/util/concurrent/ExecutorService;

    .line 1958801
    iput-object p2, p0, LX/D2T;->c:LX/0SG;

    .line 1958802
    iput-object p3, p0, LX/D2T;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1958803
    iput-object p4, p0, LX/D2T;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1958804
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D2T;->f:Z

    .line 1958805
    return-void
.end method

.method private a(LX/0Tn;)LX/0Tn;
    .locals 1

    .prologue
    .line 1958796
    iget-object v0, p0, LX/D2T;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1958797
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1958798
    invoke-virtual {p1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static a(LX/0QB;)LX/D2T;
    .locals 10

    .prologue
    .line 1958767
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1958768
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1958769
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1958770
    if-nez v1, :cond_0

    .line 1958771
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1958772
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1958773
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1958774
    sget-object v1, LX/D2T;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1958775
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1958776
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1958777
    :cond_1
    if-nez v1, :cond_4

    .line 1958778
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1958779
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1958780
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1958781
    new-instance p0, LX/D2T;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v9

    check-cast v9, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {p0, v1, v7, v8, v9}, LX/D2T;-><init>(Ljava/util/concurrent/ExecutorService;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1958782
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1958783
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1958784
    if-nez v1, :cond_2

    .line 1958785
    sget-object v0, LX/D2T;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D2T;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1958786
    :goto_1
    if-eqz v0, :cond_3

    .line 1958787
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1958788
    :goto_3
    check-cast v0, LX/D2T;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1958789
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1958790
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1958791
    :catchall_1
    move-exception v0

    .line 1958792
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1958793
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1958794
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1958795
    :cond_2
    :try_start_8
    sget-object v0, LX/D2T;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D2T;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static c(LX/D2T;)V
    .locals 1

    .prologue
    .line 1958761
    invoke-direct {p0}, LX/D2T;->h()V

    .line 1958762
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    invoke-virtual {v0}, LX/D2S;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1958763
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    invoke-virtual {v0}, LX/D2S;->c()Ljava/lang/String;

    move-result-object v0

    .line 1958764
    invoke-static {p0, v0}, LX/D2T;->d(LX/D2T;Ljava/lang/String;)V

    .line 1958765
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/D2T;->g:LX/D2S;

    .line 1958766
    return-void
.end method

.method public static d(LX/D2T;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1958720
    iget-object v0, p0, LX/D2T;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/timeline/profilevideo/store/OptimisticProfileVideoStore$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/timeline/profilevideo/store/OptimisticProfileVideoStore$1;-><init>(LX/D2T;Ljava/lang/String;)V

    const v2, 0x2329ec86

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1958721
    return-void
.end method

.method public static e(LX/D2T;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1958756
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    if-nez v0, :cond_0

    .line 1958757
    const/4 v0, 0x0

    .line 1958758
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    .line 1958759
    iget-object p0, v0, LX/D2S;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1958760
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static f(LX/D2T;)V
    .locals 3

    .prologue
    .line 1958749
    iget-boolean v0, p0, LX/D2T;->f:Z

    if-eqz v0, :cond_0

    .line 1958750
    :goto_0
    return-void

    .line 1958751
    :cond_0
    iget-object v0, p0, LX/D2T;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/D2U;->a:LX/0Tn;

    invoke-direct {p0, v1}, LX/D2T;->a(LX/0Tn;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1958752
    if-eqz v0, :cond_1

    .line 1958753
    invoke-static {p0, v0}, LX/D2T;->d(LX/D2T;Ljava/lang/String;)V

    .line 1958754
    :cond_1
    invoke-direct {p0}, LX/D2T;->h()V

    .line 1958755
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D2T;->f:Z

    goto :goto_0
.end method

.method public static f(LX/D2T;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1958746
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    invoke-virtual {v0}, LX/D2S;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1958747
    :cond_0
    const/4 v0, 0x0

    .line 1958748
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    invoke-virtual {v0}, LX/D2S;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static g(LX/D2T;)V
    .locals 3

    .prologue
    .line 1958741
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    if-eqz v0, :cond_0

    .line 1958742
    iget-object v0, p0, LX/D2T;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 1958743
    sget-object v1, LX/D2U;->a:LX/0Tn;

    invoke-direct {p0, v1}, LX/D2T;->a(LX/0Tn;)LX/0Tn;

    move-result-object v1

    iget-object v2, p0, LX/D2T;->g:LX/D2S;

    invoke-virtual {v2}, LX/D2S;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1958744
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1958745
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1958737
    iget-object v0, p0, LX/D2T;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 1958738
    sget-object v1, LX/D2U;->a:LX/0Tn;

    invoke-direct {p0, v1}, LX/D2T;->a(LX/0Tn;)LX/0Tn;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1958739
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1958740
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/D2S;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1958725
    invoke-static {p0}, LX/D2T;->f(LX/D2T;)V

    .line 1958726
    iget-object v1, p0, LX/D2T;->g:LX/D2S;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/D2T;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iget-object v3, p0, LX/D2T;->g:LX/D2S;

    .line 1958727
    iget-wide v5, v3, LX/D2S;->c:J

    move-wide v3, v5

    .line 1958728
    sub-long/2addr v1, v3

    const-wide/32 v3, 0x5265c00

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 1958729
    invoke-static {p0}, LX/D2T;->c(LX/D2T;)V

    .line 1958730
    :cond_0
    invoke-static {p0, p1}, LX/D2T;->f(LX/D2T;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1958731
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    .line 1958732
    :goto_0
    return-object v0

    .line 1958733
    :cond_1
    const-string v0, "uploading"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    if-eqz v0, :cond_2

    .line 1958734
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    invoke-virtual {v0}, LX/D2S;->b()Ljava/lang/String;

    .line 1958735
    invoke-static {p0}, LX/D2T;->c(LX/D2T;)V

    .line 1958736
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 2

    .prologue
    .line 1958722
    iget-object v0, p0, LX/D2T;->g:LX/D2S;

    if-eqz v0, :cond_0

    const-string v0, "uploading"

    iget-object v1, p0, LX/D2T;->g:LX/D2S;

    invoke-virtual {v1}, LX/D2S;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1958723
    :cond_0
    :goto_0
    return-void

    .line 1958724
    :cond_1
    invoke-static {p0}, LX/D2T;->c(LX/D2T;)V

    goto :goto_0
.end method
