.class public final LX/EkC;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/text/style/URLSpan;

.field public final synthetic b:LX/EkI;


# direct methods
.method public constructor <init>(LX/EkI;Landroid/text/style/URLSpan;)V
    .locals 0

    .prologue
    .line 2163106
    iput-object p1, p0, LX/EkC;->b:LX/EkI;

    iput-object p2, p0, LX/EkC;->a:Landroid/text/style/URLSpan;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2163107
    iget-object v0, p0, LX/EkC;->b:LX/EkI;

    iget-object v0, v0, LX/EkI;->y:LX/EkA;

    iget-object v1, p0, LX/EkC;->a:Landroid/text/style/URLSpan;

    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    .line 2163108
    iget-object v3, v0, LX/EkA;->d:LX/17W;

    iget-object v4, v0, LX/EkA;->b:Landroid/content/Context;

    .line 2163109
    sget-object v5, LX/0ax;->do:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, p1

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 2163110
    invoke-virtual {v3, v4, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2163111
    iget-object v0, p0, LX/EkC;->b:LX/EkI;

    iget-object v0, v0, LX/EkI;->h:LX/Ek6;

    sget-object v1, LX/Ek7;->QP_HELP_CENTER:LX/Ek7;

    invoke-virtual {v0, v1, v2, v2}, LX/Ek6;->a(LX/Ek7;Ljava/lang/String;LX/1rQ;)V

    .line 2163112
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2163113
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2163114
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2163115
    return-void
.end method
