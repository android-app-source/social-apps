.class public final LX/Dz5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Dz6;


# direct methods
.method public constructor <init>(LX/Dz6;)V
    .locals 0

    .prologue
    .line 2066399
    iput-object p1, p0, LX/Dz5;->a:LX/Dz6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2066400
    iget-object v0, p0, LX/Dz5;->a:LX/Dz6;

    invoke-static {v0}, LX/Dz6;->n(LX/Dz6;)V

    .line 2066401
    iget-object v0, p0, LX/Dz5;->a:LX/Dz6;

    iget-object v0, v0, LX/Dz6;->e:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2066402
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2066403
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v1}, LX/DzG;->f()V

    .line 2066404
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2066405
    invoke-static {v1}, LX/Dz6;->n(LX/Dz6;)V

    .line 2066406
    iget-object p0, v1, LX/Dz6;->h:Landroid/location/Location;

    if-nez p0, :cond_0

    .line 2066407
    const/4 p0, 0x0

    invoke-static {v1, p0}, LX/Dz6;->d(LX/Dz6;Landroid/location/Location;)V

    .line 2066408
    :cond_0
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2066409
    const/4 p0, 0x1

    iput-boolean p0, v1, LX/9j5;->r:Z

    .line 2066410
    iget-object p0, v1, LX/9j5;->a:LX/0Zb;

    const-string p1, "place_picker_location_failed"

    invoke-static {v1, p1}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2066411
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->r:LX/9j7;

    invoke-virtual {v1}, LX/9j7;->a()V

    .line 2066412
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2066413
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2066414
    if-nez p1, :cond_0

    .line 2066415
    :goto_0
    return-void

    .line 2066416
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    .line 2066417
    iget-object v1, p0, LX/Dz5;->a:LX/Dz6;

    invoke-static {v1, v0}, LX/Dz6;->c(LX/Dz6;Landroid/location/Location;)V

    goto :goto_0
.end method
