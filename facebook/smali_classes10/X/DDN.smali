.class public LX/DDN;
.super LX/3mW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mW",
        "<",
        "LX/DDZ;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final d:LX/DDW;

.field private final e:LX/DDM;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;LX/1Po;LX/25M;LX/DDW;LX/DD9;LX/DDM;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Po;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;",
            ">;",
            "LX/0Px",
            "<",
            "LX/DDZ;",
            ">;TE;",
            "LX/25M;",
            "LX/DDW;",
            "LX/DD9;",
            "LX/DDM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975521
    new-instance v3, LX/DD4;

    .line 1975522
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1975523
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-direct {v3, v0, p7}, LX/DD4;-><init>(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;LX/DD9;)V

    move-object v4, p4

    check-cast v4, LX/1Pq;

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/3mW;-><init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V

    .line 1975524
    iput-object p4, p0, LX/DDN;->c:LX/1Po;

    .line 1975525
    iput-object p6, p0, LX/DDN;->d:LX/DDW;

    .line 1975526
    iput-object p8, p0, LX/DDN;->e:LX/DDM;

    .line 1975527
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1975528
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 1975488
    check-cast p2, LX/DDZ;

    .line 1975489
    iget v0, p2, LX/DDZ;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1975490
    iget-object v0, p0, LX/DDN;->e:LX/DDM;

    const/4 v1, 0x0

    .line 1975491
    new-instance v2, LX/DDL;

    invoke-direct {v2, v0}, LX/DDL;-><init>(LX/DDM;)V

    .line 1975492
    sget-object p0, LX/DDM;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DDK;

    .line 1975493
    if-nez p0, :cond_0

    .line 1975494
    new-instance p0, LX/DDK;

    invoke-direct {p0}, LX/DDK;-><init>()V

    .line 1975495
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/DDK;->a$redex0(LX/DDK;LX/1De;IILX/DDL;)V

    .line 1975496
    move-object v2, p0

    .line 1975497
    move-object v1, v2

    .line 1975498
    move-object v0, v1

    .line 1975499
    iget-object v1, p2, LX/DDZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975500
    iget-object v2, v0, LX/DDK;->a:LX/DDL;

    iput-object v1, v2, LX/DDL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975501
    iget-object v2, v0, LX/DDK;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1975502
    move-object v0, v0

    .line 1975503
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1975504
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/DDN;->d:LX/DDW;

    const/4 v1, 0x0

    .line 1975505
    new-instance v2, LX/DDV;

    invoke-direct {v2, v0}, LX/DDV;-><init>(LX/DDW;)V

    .line 1975506
    iget-object v3, v0, LX/DDW;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DDU;

    .line 1975507
    if-nez v3, :cond_2

    .line 1975508
    new-instance v3, LX/DDU;

    invoke-direct {v3, v0}, LX/DDU;-><init>(LX/DDW;)V

    .line 1975509
    :cond_2
    invoke-static {v3, p1, v1, v1, v2}, LX/DDU;->a$redex0(LX/DDU;LX/1De;IILX/DDV;)V

    .line 1975510
    move-object v2, v3

    .line 1975511
    move-object v1, v2

    .line 1975512
    move-object v0, v1

    .line 1975513
    iget-object v1, p0, LX/DDN;->c:LX/1Po;

    .line 1975514
    iget-object v2, v0, LX/DDU;->a:LX/DDV;

    iput-object v1, v2, LX/DDV;->b:LX/1Po;

    .line 1975515
    iget-object v2, v0, LX/DDU;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1975516
    move-object v0, v0

    .line 1975517
    iget-object v1, v0, LX/DDU;->a:LX/DDV;

    iput-object p2, v1, LX/DDV;->a:LX/DDZ;

    .line 1975518
    iget-object v1, v0, LX/DDU;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1975519
    move-object v0, v0

    .line 1975520
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1975487
    const/4 v0, 0x0

    return v0
.end method
