.class public final LX/DEr;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPageInfo;

.field public final synthetic c:LX/2e2;


# direct methods
.method public constructor <init>(LX/2e2;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;Lcom/facebook/graphql/model/GraphQLPageInfo;)V
    .locals 0

    .prologue
    .line 1977372
    iput-object p1, p0, LX/DEr;->c:LX/2e2;

    iput-object p2, p0, LX/DEr;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    iput-object p3, p0, LX/DEr;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1977384
    iget-object v0, p0, LX/DEr;->c:LX/2e2;

    iget-object v0, v0, LX/2e2;->a:Ljava/util/Set;

    iget-object v1, p0, LX/DEr;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1977385
    iget-object v0, p0, LX/DEr;->c:LX/2e2;

    iget-object v1, p0, LX/DEr;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v0, v1}, LX/2e2;->c(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    .line 1977386
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1977373
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;

    const v2, 0x2e0001

    .line 1977374
    iget-object v0, p0, LX/DEr;->c:LX/2e2;

    iget-object v0, v0, LX/2e2;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "PaginatedPymkFeedUnitTTI"

    invoke-interface {v0, v2, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->e(ILjava/lang/String;)V

    .line 1977375
    iget-object v0, p0, LX/DEr;->c:LX/2e2;

    iget-object v0, v0, LX/2e2;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const-string v1, "PaginatedPymkFeedUnitTTI"

    invoke-interface {v0, v2, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 1977376
    iget-object v0, p0, LX/DEr;->c:LX/2e2;

    iget-object v0, v0, LX/2e2;->a:Ljava/util/Set;

    iget-object v1, p0, LX/DEr;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1977377
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977378
    iget-object v0, p0, LX/DEr;->c:LX/2e2;

    iget-object v1, p0, LX/DEr;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v0, v1}, LX/2e2;->c(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)V

    .line 1977379
    :goto_0
    return-void

    .line 1977380
    :cond_0
    iget-object v0, p0, LX/DEr;->c:LX/2e2;

    iget-object v0, v0, LX/2e2;->g:LX/189;

    iget-object v1, p0, LX/DEr;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v0, p1, v1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnitUsersConnection;Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    move-result-object v0

    .line 1977381
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;->o()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1977382
    iget-object v2, p0, LX/DEr;->c:LX/2e2;

    iget-object v2, v2, LX/2e2;->d:LX/03V;

    const-class v3, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/DEr;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/03V;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1977383
    iget-object v1, p0, LX/DEr;->c:LX/2e2;

    iget-object v1, v1, LX/2e2;->m:LX/2di;

    invoke-virtual {v1, v0}, LX/2di;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    goto :goto_0
.end method
