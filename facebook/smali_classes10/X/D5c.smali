.class public final LX/D5c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/2pa;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 0

    .prologue
    .line 1963767
    iput-object p1, p0, LX/D5c;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1963768
    check-cast p1, LX/2pa;

    .line 1963769
    iget-object v0, p0, LX/D5c;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-virtual {v0}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1963770
    iget-object v1, p0, LX/D5c;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->D:LX/0sV;

    invoke-virtual {v1}, LX/0sV;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/2pa;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1963771
    iget-object v1, p0, LX/D5c;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v1, v1, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->ai:Landroid/view/OrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->enable()V

    .line 1963772
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1963773
    :cond_0
    iget-object v0, p0, LX/D5c;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v0, v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->r:LX/D4m;

    invoke-virtual {v0}, LX/D4m;->a()V

    .line 1963774
    const/4 v0, 0x0

    return-object v0
.end method
