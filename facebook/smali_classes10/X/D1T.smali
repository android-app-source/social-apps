.class public final enum LX/D1T;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D1T;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D1T;

.field public static final enum CANVAS_AD:LX/D1T;

.field public static final enum STORE_VISITS_AD:LX/D1T;

.field public static final enum WATCH_AND_LOCAL:LX/D1T;


# instance fields
.field private final label:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1956840
    new-instance v0, LX/D1T;

    const-string v1, "STORE_VISITS_AD"

    const-string v2, "store_visits_ad"

    invoke-direct {v0, v1, v3, v2}, LX/D1T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D1T;->STORE_VISITS_AD:LX/D1T;

    .line 1956841
    new-instance v0, LX/D1T;

    const-string v1, "CANVAS_AD"

    const-string v2, "canvas_ad"

    invoke-direct {v0, v1, v4, v2}, LX/D1T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D1T;->CANVAS_AD:LX/D1T;

    .line 1956842
    new-instance v0, LX/D1T;

    const-string v1, "WATCH_AND_LOCAL"

    const-string v2, "watch_and_local"

    invoke-direct {v0, v1, v5, v2}, LX/D1T;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D1T;->WATCH_AND_LOCAL:LX/D1T;

    .line 1956843
    const/4 v0, 0x3

    new-array v0, v0, [LX/D1T;

    sget-object v1, LX/D1T;->STORE_VISITS_AD:LX/D1T;

    aput-object v1, v0, v3

    sget-object v1, LX/D1T;->CANVAS_AD:LX/D1T;

    aput-object v1, v0, v4

    sget-object v1, LX/D1T;->WATCH_AND_LOCAL:LX/D1T;

    aput-object v1, v0, v5

    sput-object v0, LX/D1T;->$VALUES:[LX/D1T;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1956844
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1956845
    iput-object p3, p0, LX/D1T;->label:Ljava/lang/String;

    .line 1956846
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D1T;
    .locals 1

    .prologue
    .line 1956847
    const-class v0, LX/D1T;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D1T;

    return-object v0
.end method

.method public static values()[LX/D1T;
    .locals 1

    .prologue
    .line 1956848
    sget-object v0, LX/D1T;->$VALUES:[LX/D1T;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D1T;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1956849
    iget-object v0, p0, LX/D1T;->label:Ljava/lang/String;

    return-object v0
.end method
