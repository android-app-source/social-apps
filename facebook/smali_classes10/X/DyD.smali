.class public final LX/DyD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V
    .locals 0

    .prologue
    .line 2064043
    iput-object p1, p0, LX/DyD;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2064044
    iget-object v0, p0, LX/DyD;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchInitialAlbumsList"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2064045
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2064046
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2064047
    if-eqz p1, :cond_0

    .line 2064048
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2064049
    if-nez v0, :cond_1

    .line 2064050
    :cond_0
    :goto_0
    return-void

    .line 2064051
    :cond_1
    iget-object v0, p0, LX/DyD;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    const/4 v1, 0x1

    .line 2064052
    iput-boolean v1, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->n:Z

    .line 2064053
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;

    .line 2064054
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2064055
    iget-object v1, p0, LX/DyD;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v2

    .line 2064056
    iput-object v2, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->o:Ljava/lang/String;

    .line 2064057
    iget-object v1, p0, LX/DyD;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v2

    .line 2064058
    iput-boolean v2, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->p:Z

    .line 2064059
    :goto_1
    iget-object v1, p0, LX/DyD;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v2, p0, LX/DyD;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-boolean v2, v2, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->p:Z

    invoke-static {v1, v0, v2}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a$redex0(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;Lcom/facebook/graphql/model/GraphQLAlbumsConnection;Z)V

    goto :goto_0

    .line 2064060
    :cond_2
    iget-object v1, p0, LX/DyD;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    const/4 v2, 0x0

    .line 2064061
    iput-boolean v2, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->p:Z

    .line 2064062
    goto :goto_1
.end method
