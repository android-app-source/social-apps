.class public final LX/D5j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedRootView;)V
    .locals 0

    .prologue
    .line 1963816
    iput-object p1, p0, LX/D5j;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1963817
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 1963818
    if-nez p1, :cond_0

    .line 1963819
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1963820
    :goto_0
    return-object v0

    .line 1963821
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1963822
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1963823
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1963824
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1963825
    :cond_1
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    .line 1963826
    if-nez v1, :cond_2

    .line 1963827
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1963828
    :cond_2
    iget-object v2, p0, LX/D5j;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v2, v2, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->U:LX/D4U;

    iget-object v3, p0, LX/D5j;->a:Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    iget-object v3, v3, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->g:LX/D5q;

    invoke-virtual {v3, p1, v1}, LX/D5q;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/D5p;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, LX/1Qj;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D5r;

    invoke-virtual {v0}, LX/D5r;->b()LX/2oO;

    move-result-object v0

    invoke-virtual {v0}, LX/2oO;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method
