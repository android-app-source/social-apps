.class public final LX/Dr6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/notifications/settings/data/NotificationSettingsDataFetcher$FetchCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2048960
    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2048961
    iput-object p1, p0, LX/Dr6;->a:Ljava/util/List;

    .line 2048962
    iput-object p2, p0, LX/Dr6;->b:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    .line 2048963
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2048964
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2048965
    check-cast p1, Ljava/util/Map;

    .line 2048966
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2048967
    iget-object v0, p0, LX/Dr6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2048968
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2048969
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2048970
    :cond_1
    iget-object v0, p0, LX/Dr6;->b:Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/settings/fragment/NotificationSettingsFragment;->a(Ljava/util/List;)V

    .line 2048971
    return-void
.end method
