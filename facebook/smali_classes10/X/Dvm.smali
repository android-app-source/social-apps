.class public LX/Dvm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

.field public final b:I

.field public final c:I

.field public final d:LX/DvW;

.field public final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IILX/DvW;Z)V
    .locals 0

    .prologue
    .line 2059168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059169
    iput-object p1, p0, LX/Dvm;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    .line 2059170
    iput p2, p0, LX/Dvm;->b:I

    .line 2059171
    iput p3, p0, LX/Dvm;->c:I

    .line 2059172
    iput-object p4, p0, LX/Dvm;->d:LX/DvW;

    .line 2059173
    iput-boolean p5, p0, LX/Dvm;->e:Z

    .line 2059174
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Z)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2059175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059176
    iput-object p1, p0, LX/Dvm;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    .line 2059177
    iput v0, p0, LX/Dvm;->b:I

    .line 2059178
    iput v0, p0, LX/Dvm;->c:I

    .line 2059179
    iput-object p2, p0, LX/Dvm;->d:LX/DvW;

    .line 2059180
    iput-boolean p3, p0, LX/Dvm;->e:Z

    .line 2059181
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2059182
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/Dvm;

    if-nez v1, :cond_1

    .line 2059183
    :cond_0
    :goto_0
    return v0

    .line 2059184
    :cond_1
    check-cast p1, LX/Dvm;

    .line 2059185
    iget-object v1, p0, LX/Dvm;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-object v2, p1, LX/Dvm;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LX/Dvm;->b:I

    iget v2, p1, LX/Dvm;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/Dvm;->c:I

    iget v2, p1, LX/Dvm;->c:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/Dvm;->d:LX/DvW;

    iget-object v2, p1, LX/Dvm;->d:LX/DvW;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2059186
    iget-object v0, p0, LX/Dvm;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget v1, p0, LX/Dvm;->b:I

    iget v2, p0, LX/Dvm;->c:I

    mul-int/2addr v1, v2

    iget-object v2, p0, LX/Dvm;->d:LX/DvW;

    invoke-virtual {v2}, LX/DvW;->ordinal()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iget v1, p0, LX/Dvm;->b:I

    add-int/2addr v0, v1

    return v0
.end method
