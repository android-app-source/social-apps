.class public final LX/EcV;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EcT;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcV;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EcV;


# instance fields
.field public bitField0_:I

.field public cipherKey_:LX/EWc;

.field public index_:I

.field public iv_:LX/EWc;

.field public macKey_:LX/EWc;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2146965
    new-instance v0, LX/EcS;

    invoke-direct {v0}, LX/EcS;-><init>()V

    sput-object v0, LX/EcV;->a:LX/EWZ;

    .line 2146966
    new-instance v0, LX/EcV;

    invoke-direct {v0}, LX/EcV;-><init>()V

    .line 2146967
    sput-object v0, LX/EcV;->c:LX/EcV;

    invoke-direct {v0}, LX/EcV;->y()V

    .line 2146968
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2146960
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2146961
    iput-byte v0, p0, LX/EcV;->memoizedIsInitialized:B

    .line 2146962
    iput v0, p0, LX/EcV;->memoizedSerializedSize:I

    .line 2146963
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2146964
    iput-object v0, p0, LX/EcV;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2146926
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2146927
    iput-byte v0, p0, LX/EcV;->memoizedIsInitialized:B

    .line 2146928
    iput v0, p0, LX/EcV;->memoizedSerializedSize:I

    .line 2146929
    invoke-direct {p0}, LX/EcV;->y()V

    .line 2146930
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2146931
    const/4 v0, 0x0

    .line 2146932
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2146933
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2146934
    sparse-switch v3, :sswitch_data_0

    .line 2146935
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2146936
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2146937
    goto :goto_0

    .line 2146938
    :sswitch_1
    iget v3, p0, LX/EcV;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EcV;->bitField0_:I

    .line 2146939
    invoke-virtual {p1}, LX/EWd;->l()I

    move-result v3

    iput v3, p0, LX/EcV;->index_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2146940
    :catch_0
    move-exception v0

    .line 2146941
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2146942
    move-object v0, v0

    .line 2146943
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2146944
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EcV;->unknownFields:LX/EZQ;

    .line 2146945
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2146946
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EcV;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EcV;->bitField0_:I

    .line 2146947
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EcV;->cipherKey_:LX/EWc;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2146948
    :catch_1
    move-exception v0

    .line 2146949
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2146950
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2146951
    move-object v0, v1

    .line 2146952
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2146953
    :sswitch_3
    :try_start_4
    iget v3, p0, LX/EcV;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, LX/EcV;->bitField0_:I

    .line 2146954
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EcV;->macKey_:LX/EWc;

    goto :goto_0

    .line 2146955
    :sswitch_4
    iget v3, p0, LX/EcV;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, LX/EcV;->bitField0_:I

    .line 2146956
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v3

    iput-object v3, p0, LX/EcV;->iv_:LX/EWc;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 2146957
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcV;->unknownFields:LX/EZQ;

    .line 2146958
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2146959
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2146921
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2146922
    iput-byte v1, p0, LX/EcV;->memoizedIsInitialized:B

    .line 2146923
    iput v1, p0, LX/EcV;->memoizedSerializedSize:I

    .line 2146924
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EcV;->unknownFields:LX/EZQ;

    .line 2146925
    return-void
.end method

.method private static a(LX/EcV;)LX/EcU;
    .locals 1

    .prologue
    .line 2146920
    invoke-static {}, LX/EcU;->u()LX/EcU;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EcU;->a(LX/EcV;)LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method private y()V
    .locals 1

    .prologue
    .line 2146915
    const/4 v0, 0x0

    iput v0, p0, LX/EcV;->index_:I

    .line 2146916
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcV;->cipherKey_:LX/EWc;

    .line 2146917
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcV;->macKey_:LX/EWc;

    .line 2146918
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcV;->iv_:LX/EWc;

    .line 2146919
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2146902
    new-instance v0, LX/EcU;

    invoke-direct {v0, p1}, LX/EcU;-><init>(LX/EYd;)V

    .line 2146903
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2146904
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2146905
    iget v0, p0, LX/EcV;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2146906
    iget v0, p0, LX/EcV;->index_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->c(II)V

    .line 2146907
    :cond_0
    iget v0, p0, LX/EcV;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2146908
    iget-object v0, p0, LX/EcV;->cipherKey_:LX/EWc;

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2146909
    :cond_1
    iget v0, p0, LX/EcV;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 2146910
    const/4 v0, 0x3

    iget-object v1, p0, LX/EcV;->macKey_:LX/EWc;

    invoke-virtual {p1, v0, v1}, LX/EWf;->a(ILX/EWc;)V

    .line 2146911
    :cond_2
    iget v0, p0, LX/EcV;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2146912
    iget-object v0, p0, LX/EcV;->iv_:LX/EWc;

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2146913
    :cond_3
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2146914
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2146969
    iget-byte v1, p0, LX/EcV;->memoizedIsInitialized:B

    .line 2146970
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2146971
    :goto_0
    return v0

    .line 2146972
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2146973
    :cond_1
    iput-byte v0, p0, LX/EcV;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2146882
    iget v0, p0, LX/EcV;->memoizedSerializedSize:I

    .line 2146883
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2146884
    :goto_0
    return v0

    .line 2146885
    :cond_0
    const/4 v0, 0x0

    .line 2146886
    iget v1, p0, LX/EcV;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2146887
    iget v0, p0, LX/EcV;->index_:I

    invoke-static {v2, v0}, LX/EWf;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2146888
    :cond_1
    iget v1, p0, LX/EcV;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2146889
    iget-object v1, p0, LX/EcV;->cipherKey_:LX/EWc;

    invoke-static {v3, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2146890
    :cond_2
    iget v1, p0, LX/EcV;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 2146891
    const/4 v1, 0x3

    iget-object v2, p0, LX/EcV;->macKey_:LX/EWc;

    invoke-static {v1, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2146892
    :cond_3
    iget v1, p0, LX/EcV;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 2146893
    iget-object v1, p0, LX/EcV;->iv_:LX/EWc;

    invoke-static {v4, v1}, LX/EWf;->c(ILX/EWc;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2146894
    :cond_4
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2146895
    iput v0, p0, LX/EcV;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2146881
    iget-object v0, p0, LX/EcV;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2146896
    sget-object v0, LX/Eck;->h:LX/EYn;

    const-class v1, LX/EcV;

    const-class v2, LX/EcU;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EcV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2146897
    sget-object v0, LX/EcV;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2146898
    invoke-static {p0}, LX/EcV;->a(LX/EcV;)LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2146899
    invoke-static {}, LX/EcU;->u()LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2146900
    invoke-static {p0}, LX/EcV;->a(LX/EcV;)LX/EcU;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2146901
    sget-object v0, LX/EcV;->c:LX/EcV;

    return-object v0
.end method
