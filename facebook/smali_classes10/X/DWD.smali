.class public LX/DWD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2006634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2006635
    iput-object p1, p0, LX/DWD;->a:LX/0tX;

    .line 2006636
    return-void
.end method

.method public static a(LX/0QB;)LX/DWD;
    .locals 1

    .prologue
    .line 2006633
    invoke-static {p0}, LX/DWD;->b(LX/0QB;)LX/DWD;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/DWD;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .param p3    # LX/0Px;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2006637
    new-instance v0, LX/4Fa;

    invoke-direct {v0}, LX/4Fa;-><init>()V

    .line 2006638
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2006639
    move-object v0, v0

    .line 2006640
    const-string v1, "email_addresses"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2006641
    move-object v0, v0

    .line 2006642
    const-string v1, "user_ids"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2006643
    move-object v0, v0

    .line 2006644
    const-string v1, "message"

    invoke-virtual {v0, v1, p5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2006645
    move-object v0, v0

    .line 2006646
    const-string v1, "recommendation_key"

    invoke-virtual {v0, v1, p6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2006647
    move-object v0, v0

    .line 2006648
    if-eqz p4, :cond_0

    .line 2006649
    const-string v1, "source"

    invoke-virtual {v0, v1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2006650
    :cond_0
    new-instance v1, LX/DVx;

    invoke-direct {v1}, LX/DVx;-><init>()V

    move-object v1, v1

    .line 2006651
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2006652
    iget-object v0, p0, LX/DWD;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/DWD;
    .locals 2

    .prologue
    .line 2006631
    new-instance v1, LX/DWD;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/DWD;-><init>(LX/0tX;)V

    .line 2006632
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2006629
    const-string v6, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, LX/DWD;->a(LX/DWD;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2006630
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/DWD;->a(LX/DWD;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
