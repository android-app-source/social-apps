.class public final enum LX/Emi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Emi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Emi;

.field public static final enum CARD_SCROLLED_AWAY:LX/Emi;

.field public static final enum SUCCEEDED:LX/Emi;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2166064
    new-instance v0, LX/Emi;

    const-string v1, "CARD_SCROLLED_AWAY"

    const-string v2, "ec_card_scrolled"

    invoke-direct {v0, v1, v3, v2}, LX/Emi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emi;->CARD_SCROLLED_AWAY:LX/Emi;

    .line 2166065
    new-instance v0, LX/Emi;

    const-string v1, "SUCCEEDED"

    const-string v2, "ec_success"

    invoke-direct {v0, v1, v4, v2}, LX/Emi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Emi;->SUCCEEDED:LX/Emi;

    .line 2166066
    const/4 v0, 0x2

    new-array v0, v0, [LX/Emi;

    sget-object v1, LX/Emi;->CARD_SCROLLED_AWAY:LX/Emi;

    aput-object v1, v0, v3

    sget-object v1, LX/Emi;->SUCCEEDED:LX/Emi;

    aput-object v1, v0, v4

    sput-object v0, LX/Emi;->$VALUES:[LX/Emi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2166067
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2166068
    iput-object p3, p0, LX/Emi;->name:Ljava/lang/String;

    .line 2166069
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Emi;
    .locals 1

    .prologue
    .line 2166070
    const-class v0, LX/Emi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Emi;

    return-object v0
.end method

.method public static values()[LX/Emi;
    .locals 1

    .prologue
    .line 2166071
    sget-object v0, LX/Emi;->$VALUES:[LX/Emi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Emi;

    return-object v0
.end method
