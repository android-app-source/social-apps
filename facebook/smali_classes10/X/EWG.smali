.class public LX/EWG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EVz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/DaW;",
        ">",
        "Ljava/lang/Object;",
        "LX/EVz;"
    }
.end annotation


# instance fields
.field public a:D

.field public b:I

.field public c:I

.field public d:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/EW4;

.field private h:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(LX/EW4;)V
    .locals 2

    .prologue
    .line 2129792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129793
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/EWG;->h:Landroid/graphics/Point;

    .line 2129794
    iput-object p1, p0, LX/EWG;->g:LX/EW4;

    .line 2129795
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/EWG;->a:D

    .line 2129796
    return-void
.end method

.method public static a(IIF)I
    .locals 2

    .prologue
    .line 2129797
    int-to-float v0, p0

    sub-int v1, p1, p0

    int-to-float v1, v1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static b(LX/EWG;II)V
    .locals 0

    .prologue
    .line 2129798
    iput p1, p0, LX/EWG;->b:I

    .line 2129799
    iput p2, p0, LX/EWG;->c:I

    .line 2129800
    return-void
.end method

.method public static c(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 2129801
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method


# virtual methods
.method public final a(D)Landroid/graphics/Point;
    .locals 5

    .prologue
    .line 2129802
    iput-wide p1, p0, LX/EWG;->a:D

    .line 2129803
    iget-object v0, p0, LX/EWG;->d:Landroid/view/View;

    check-cast v0, LX/DaW;

    invoke-interface {v0, p1, p2}, LX/DaW;->a(D)V

    .line 2129804
    iget-object v0, p0, LX/EWG;->h:Landroid/graphics/Point;

    iget-object v1, p0, LX/EWG;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, LX/EWG;->f:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 2129805
    iget-object v0, p0, LX/EWG;->h:Landroid/graphics/Point;

    iget-object v1, p0, LX/EWG;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, LX/EWG;->f:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 2129806
    iget-object v0, p0, LX/EWG;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/EWG;->h:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 2129807
    iget-object v1, p0, LX/EWG;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/EWG;->h:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v2

    .line 2129808
    iget-object v2, p0, LX/EWG;->f:Landroid/graphics/Rect;

    iget-object v3, p0, LX/EWG;->d:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, LX/EWG;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2129809
    iget-object v0, p0, LX/EWG;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/EWG;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-static {p0, v0, v1}, LX/EWG;->b(LX/EWG;II)V

    .line 2129810
    iget-object v0, p0, LX/EWG;->h:Landroid/graphics/Point;

    return-object v0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 2129811
    iget-object v0, p0, LX/EWG;->f:Landroid/graphics/Rect;

    iget-object v1, p0, LX/EWG;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p1

    iget-object v2, p0, LX/EWG;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 2129812
    iget-object v0, p0, LX/EWG;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/EWG;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-static {p0, v0, v1}, LX/EWG;->b(LX/EWG;II)V

    .line 2129813
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2129814
    iget-object v0, p0, LX/EWG;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2129815
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 2129816
    iget v1, p0, LX/EWG;->b:I

    int-to-float v1, v1

    iget v2, p0, LX/EWG;->c:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2129817
    iget-object v1, p0, LX/EWG;->d:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 2129818
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2129819
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2129820
    instance-of v0, p1, LX/DaW;

    if-nez v0, :cond_0

    .line 2129821
    const-string v0, "DragSortGridView"

    const-string v1, "Trying to use ViewHoverRenderer on a non hoverable view"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2129822
    :goto_0
    return-void

    .line 2129823
    :cond_0
    invoke-static {p1}, LX/EWG;->c(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, LX/EWG;->e:Landroid/graphics/Rect;

    .line 2129824
    invoke-static {p1}, LX/EWG;->c(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, LX/EWG;->f:Landroid/graphics/Rect;

    .line 2129825
    iget-object v0, p0, LX/EWG;->g:LX/EW4;

    invoke-virtual {v0, p1}, LX/EW4;->b(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 2129826
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 2129827
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 2129828
    iput-object v0, p0, LX/EWG;->d:Landroid/view/View;

    .line 2129829
    iget-object v0, p0, LX/EWG;->d:Landroid/view/View;

    check-cast v0, LX/DaW;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/DaW;->setHovering(Z)V

    .line 2129830
    iget-object v0, p0, LX/EWG;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, LX/EWG;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-static {p0, v0, v1}, LX/EWG;->b(LX/EWG;II)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2129831
    iget-object v0, p0, LX/EWG;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2129832
    iget-object v0, p0, LX/EWG;->d:Landroid/view/View;

    check-cast v0, LX/DaW;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/DaW;->setHovering(Z)V

    .line 2129833
    const/4 v0, 0x0

    iput-object v0, p0, LX/EWG;->d:Landroid/view/View;

    .line 2129834
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2129835
    iget-object v0, p0, LX/EWG;->f:Landroid/graphics/Rect;

    invoke-static {p1}, LX/EWG;->c(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2129836
    new-instance v2, LX/EWD;

    invoke-direct {v2, p0}, LX/EWD;-><init>(LX/EWG;)V

    .line 2129837
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, LX/EWG;->a:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 2129838
    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2129839
    iget-object v3, p0, LX/EWG;->d:Landroid/view/View;

    invoke-static {v3}, LX/EWG;->c(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    .line 2129840
    iget v4, p0, LX/EWG;->b:I

    iget v5, p0, LX/EWG;->c:I

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 2129841
    new-instance v4, LX/EWE;

    invoke-direct {v4, p0, v3}, LX/EWE;-><init>(LX/EWG;Landroid/graphics/Rect;)V

    invoke-virtual {v2, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2129842
    new-instance v3, LX/EWF;

    invoke-direct {v3, p0, p1}, LX/EWF;-><init>(LX/EWG;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2129843
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 2129844
    return-void
.end method

.method public final c()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2129845
    iget-object v0, p0, LX/EWG;->e:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final d()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 2129846
    iget-object v0, p0, LX/EWG;->f:Landroid/graphics/Rect;

    return-object v0
.end method
