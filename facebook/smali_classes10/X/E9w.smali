.class public final LX/E9w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/E9x;


# direct methods
.method public constructor <init>(LX/E9x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2084942
    iput-object p1, p0, LX/E9w;->f:LX/E9x;

    iput-object p2, p0, LX/E9w;->a:Ljava/lang/String;

    iput-object p3, p0, LX/E9w;->b:Ljava/lang/String;

    iput-object p4, p0, LX/E9w;->c:Ljava/lang/String;

    iput-object p5, p0, LX/E9w;->d:Landroid/content/Context;

    iput-object p6, p0, LX/E9w;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 2084943
    iget-object v0, p0, LX/E9w;->f:LX/E9x;

    iget-object v0, v0, LX/E9x;->g:LX/79D;

    iget-object v1, p0, LX/E9w;->a:Ljava/lang/String;

    iget-object v2, p0, LX/E9w;->b:Ljava/lang/String;

    iget-object v3, p0, LX/E9w;->c:Ljava/lang/String;

    sget-object v4, LX/79C;->DELETE_DIALOG_CONFIRMATION_BUTTON:LX/79C;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/79D;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/79C;)V

    .line 2084944
    iget-object v0, p0, LX/E9w;->f:LX/E9x;

    iget-object v0, v0, LX/E9x;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reviews/handler/DeleteReviewHandler;

    iget-object v1, p0, LX/E9w;->d:Landroid/content/Context;

    new-instance v2, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;

    iget-object v3, p0, LX/E9w;->b:Ljava/lang/String;

    const-string v4, "edit_menu"

    iget-object v5, p0, LX/E9w;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, LX/E9w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/reviews/handler/DeleteReviewHandler;->a(Landroid/content/Context;Lcom/facebook/common/pagesprotocol/DeletePageReviewParams;Ljava/lang/String;)V

    .line 2084945
    return-void
.end method
