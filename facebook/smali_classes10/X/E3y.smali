.class public final LX/E3y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Aj7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Aj7",
        "<",
        "LX/26M;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;LX/0Px;LX/0Px;)V
    .locals 0

    .prologue
    .line 2075677
    iput-object p1, p0, LX/E3y;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;

    iput-object p2, p0, LX/E3y;->a:LX/0Px;

    iput-object p3, p0, LX/E3y;->b:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/26N;I)V
    .locals 5

    .prologue
    .line 2075678
    iget-object v1, p0, LX/E3y;->c:Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;

    iget-object v0, p0, LX/E3y;->a:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26M;

    .line 2075679
    iget-object v2, v0, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v2

    .line 2075680
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 2075681
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v2, p0, LX/E3y;->b:LX/0Px;

    .line 2075682
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2075683
    invoke-static {v2, p3}, LX/9hF;->a(LX/0Px;I)LX/9hE;

    move-result-object v3

    sget-object v4, LX/74S;->REACTION_PHOTO_ITEM:LX/74S;

    invoke-virtual {v3, v4}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v3

    invoke-virtual {v3}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v3

    .line 2075684
    iget-object v4, v1, Lcom/facebook/reaction/feed/unitcomponents/partdefinition/ReactionPhotoGridPartDefinition;->g:LX/23R;

    invoke-virtual {p1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object p0

    const/4 p2, 0x0

    invoke-interface {v4, p0, v3, p2}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2075685
    return-void
.end method
