.class public final enum LX/Ckb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ckb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ckb;

.field public static final enum BAR:LX/Ckb;

.field public static final enum BYLINE:LX/Ckb;

.field public static final enum BYLINE_AREA:LX/Ckb;

.field public static final enum COVER_IMAGE:LX/Ckb;

.field public static final enum COVER_VIDEO:LX/Ckb;

.field public static final enum DESCRIPTION:LX/Ckb;

.field public static final enum HEADLINE:LX/Ckb;

.field public static final enum NONE:LX/Ckb;

.field public static final enum OVERLAY:LX/Ckb;

.field public static final enum SOURCE_IMAGE:LX/Ckb;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1931133
    new-instance v0, LX/Ckb;

    const-string v1, "NONE"

    const-string v2, "NONE"

    invoke-direct {v0, v1, v4, v2}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->NONE:LX/Ckb;

    .line 1931134
    new-instance v0, LX/Ckb;

    const-string v1, "COVER_IMAGE"

    const-string v2, "coverImage"

    invoke-direct {v0, v1, v5, v2}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->COVER_IMAGE:LX/Ckb;

    .line 1931135
    new-instance v0, LX/Ckb;

    const-string v1, "COVER_VIDEO"

    const-string v2, "coverVideo"

    invoke-direct {v0, v1, v6, v2}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->COVER_VIDEO:LX/Ckb;

    .line 1931136
    new-instance v0, LX/Ckb;

    const-string v1, "HEADLINE"

    const-string v2, "headline"

    invoke-direct {v0, v1, v7, v2}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->HEADLINE:LX/Ckb;

    .line 1931137
    new-instance v0, LX/Ckb;

    const-string v1, "DESCRIPTION"

    const-string v2, "description"

    invoke-direct {v0, v1, v8, v2}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->DESCRIPTION:LX/Ckb;

    .line 1931138
    new-instance v0, LX/Ckb;

    const-string v1, "BYLINE"

    const/4 v2, 0x5

    const-string v3, "byline"

    invoke-direct {v0, v1, v2, v3}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->BYLINE:LX/Ckb;

    .line 1931139
    new-instance v0, LX/Ckb;

    const-string v1, "BYLINE_AREA"

    const/4 v2, 0x6

    const-string v3, "bylineArea"

    invoke-direct {v0, v1, v2, v3}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->BYLINE_AREA:LX/Ckb;

    .line 1931140
    new-instance v0, LX/Ckb;

    const-string v1, "SOURCE_IMAGE"

    const/4 v2, 0x7

    const-string v3, "sourceImage"

    invoke-direct {v0, v1, v2, v3}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->SOURCE_IMAGE:LX/Ckb;

    .line 1931141
    new-instance v0, LX/Ckb;

    const-string v1, "BAR"

    const/16 v2, 0x8

    const-string v3, "bar"

    invoke-direct {v0, v1, v2, v3}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->BAR:LX/Ckb;

    .line 1931142
    new-instance v0, LX/Ckb;

    const-string v1, "OVERLAY"

    const/16 v2, 0x9

    const-string v3, "overlay"

    invoke-direct {v0, v1, v2, v3}, LX/Ckb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ckb;->OVERLAY:LX/Ckb;

    .line 1931143
    const/16 v0, 0xa

    new-array v0, v0, [LX/Ckb;

    sget-object v1, LX/Ckb;->NONE:LX/Ckb;

    aput-object v1, v0, v4

    sget-object v1, LX/Ckb;->COVER_IMAGE:LX/Ckb;

    aput-object v1, v0, v5

    sget-object v1, LX/Ckb;->COVER_VIDEO:LX/Ckb;

    aput-object v1, v0, v6

    sget-object v1, LX/Ckb;->HEADLINE:LX/Ckb;

    aput-object v1, v0, v7

    sget-object v1, LX/Ckb;->DESCRIPTION:LX/Ckb;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Ckb;->BYLINE:LX/Ckb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Ckb;->BYLINE_AREA:LX/Ckb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Ckb;->SOURCE_IMAGE:LX/Ckb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Ckb;->BAR:LX/Ckb;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Ckb;->OVERLAY:LX/Ckb;

    aput-object v2, v0, v1

    sput-object v0, LX/Ckb;->$VALUES:[LX/Ckb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1931144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1931145
    iput-object p3, p0, LX/Ckb;->value:Ljava/lang/String;

    .line 1931146
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/Ckb;
    .locals 5

    .prologue
    .line 1931147
    if-nez p0, :cond_1

    .line 1931148
    sget-object v0, LX/Ckb;->NONE:LX/Ckb;

    .line 1931149
    :cond_0
    :goto_0
    return-object v0

    .line 1931150
    :cond_1
    invoke-static {}, LX/Ckb;->values()[LX/Ckb;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 1931151
    iget-object v4, v0, LX/Ckb;->value:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1931152
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1931153
    :cond_2
    sget-object v0, LX/Ckb;->NONE:LX/Ckb;

    goto :goto_0
.end method

.method public static isTextElement(LX/Ckb;)Z
    .locals 1

    .prologue
    .line 1931154
    sget-object v0, LX/Ckb;->HEADLINE:LX/Ckb;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/Ckb;->DESCRIPTION:LX/Ckb;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/Ckb;->BYLINE:LX/Ckb;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ckb;
    .locals 1

    .prologue
    .line 1931155
    const-class v0, LX/Ckb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ckb;

    return-object v0
.end method

.method public static values()[LX/Ckb;
    .locals 1

    .prologue
    .line 1931156
    sget-object v0, LX/Ckb;->$VALUES:[LX/Ckb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ckb;

    return-object v0
.end method
