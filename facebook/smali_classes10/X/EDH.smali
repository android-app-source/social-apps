.class public final enum LX/EDH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDH;

.field public static final enum Basic:LX/EDH;

.field public static final enum Debug:LX/EDH;

.field public static final enum Info:LX/EDH;

.field public static final enum None:LX/EDH;

.field public static final enum Verbose:LX/EDH;

.field public static final enum Warning:LX/EDH;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2091139
    new-instance v0, LX/EDH;

    const-string v1, "None"

    invoke-direct {v0, v1, v3}, LX/EDH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDH;->None:LX/EDH;

    .line 2091140
    new-instance v0, LX/EDH;

    const-string v1, "Basic"

    invoke-direct {v0, v1, v4}, LX/EDH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDH;->Basic:LX/EDH;

    .line 2091141
    new-instance v0, LX/EDH;

    const-string v1, "Debug"

    invoke-direct {v0, v1, v5}, LX/EDH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDH;->Debug:LX/EDH;

    .line 2091142
    new-instance v0, LX/EDH;

    const-string v1, "Warning"

    invoke-direct {v0, v1, v6}, LX/EDH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDH;->Warning:LX/EDH;

    .line 2091143
    new-instance v0, LX/EDH;

    const-string v1, "Info"

    invoke-direct {v0, v1, v7}, LX/EDH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDH;->Info:LX/EDH;

    .line 2091144
    new-instance v0, LX/EDH;

    const-string v1, "Verbose"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/EDH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDH;->Verbose:LX/EDH;

    .line 2091145
    const/4 v0, 0x6

    new-array v0, v0, [LX/EDH;

    sget-object v1, LX/EDH;->None:LX/EDH;

    aput-object v1, v0, v3

    sget-object v1, LX/EDH;->Basic:LX/EDH;

    aput-object v1, v0, v4

    sget-object v1, LX/EDH;->Debug:LX/EDH;

    aput-object v1, v0, v5

    sget-object v1, LX/EDH;->Warning:LX/EDH;

    aput-object v1, v0, v6

    sget-object v1, LX/EDH;->Info:LX/EDH;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/EDH;->Verbose:LX/EDH;

    aput-object v2, v0, v1

    sput-object v0, LX/EDH;->$VALUES:[LX/EDH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2091146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDH;
    .locals 1

    .prologue
    .line 2091147
    const-class v0, LX/EDH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDH;

    return-object v0
.end method

.method public static values()[LX/EDH;
    .locals 1

    .prologue
    .line 2091148
    sget-object v0, LX/EDH;->$VALUES:[LX/EDH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDH;

    return-object v0
.end method
