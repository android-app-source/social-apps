.class public LX/D0J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D0G;


# instance fields
.field private final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Rf;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1955010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1955011
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rf;

    iput-object v0, p0, LX/D0J;->a:LX/0Rf;

    .line 1955012
    iget-object v0, p0, LX/D0J;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must specify display styles"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1955013
    iget-object v0, p0, LX/D0J;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    :goto_1
    const-string v0, "Only one display style specified. Use SingleDisplayStyleMatcher instead."

    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1955014
    return-void

    :cond_0
    move v0, v2

    .line 1955015
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1955016
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/D0G;)Z
    .locals 2

    .prologue
    .line 1955017
    iget-object v0, p0, LX/D0J;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955018
    invoke-interface {p1, v0}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1955019
    const/4 v0, 0x1

    .line 1955020
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955021
    check-cast p1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955022
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/D0J;->a:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
