.class public LX/D37;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/48R;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/03V;

.field private final d:LX/11i;

.field private final e:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final f:LX/0W3;

.field private final g:Lcom/facebook/content/SecureContextHelper;

.field private final h:LX/0SG;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/1sz;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ps;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Zb;

.field public final m:LX/2n3;

.field private final n:LX/0s6;

.field public final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final q:Z

.field public final r:LX/0dx;

.field private final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/0Uh;

.field private u:LX/2nC;

.field public v:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1959715
    const-class v0, LX/D37;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/D37;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/11i;Lcom/facebook/performancelogger/PerformanceLogger;LX/0W3;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/0SG;LX/2n3;LX/0Or;LX/1sz;LX/0Ot;LX/0Zb;LX/0s6;LX/0Or;LX/0Or;Ljava/lang/Boolean;LX/0dx;LX/0Ot;LX/0Uh;LX/2nC;)V
    .locals 2
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/ui/browser/gating/IsPreIABOpenLogEnabled;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/ui/browser/gating/IsWorkInAppBrowserEnabled;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/ui/browser/gating/IsInAppBrowserEnabled;
        .end annotation
    .end param
    .param p16    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p18    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/11i;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0SG;",
            "LX/2n3;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1sz;",
            "LX/0Ot",
            "<",
            "LX/0ps;",
            ">;",
            "LX/0Zb;",
            "LX/0s6;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/0dx;",
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2nC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1959693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1959694
    iput-object p1, p0, LX/D37;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1959695
    iput-object p2, p0, LX/D37;->d:LX/11i;

    .line 1959696
    iput-object p3, p0, LX/D37;->e:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1959697
    iput-object p4, p0, LX/D37;->f:LX/0W3;

    .line 1959698
    iput-object p5, p0, LX/D37;->g:Lcom/facebook/content/SecureContextHelper;

    .line 1959699
    iput-object p6, p0, LX/D37;->c:LX/03V;

    .line 1959700
    iput-object p7, p0, LX/D37;->h:LX/0SG;

    .line 1959701
    iput-object p8, p0, LX/D37;->m:LX/2n3;

    .line 1959702
    iput-object p9, p0, LX/D37;->i:LX/0Or;

    .line 1959703
    iput-object p10, p0, LX/D37;->j:LX/1sz;

    .line 1959704
    iput-object p11, p0, LX/D37;->k:LX/0Ot;

    .line 1959705
    iput-object p12, p0, LX/D37;->l:LX/0Zb;

    .line 1959706
    iput-object p13, p0, LX/D37;->n:LX/0s6;

    .line 1959707
    move-object/from16 v0, p14

    iput-object v0, p0, LX/D37;->o:LX/0Or;

    .line 1959708
    move-object/from16 v0, p15

    iput-object v0, p0, LX/D37;->p:LX/0Or;

    .line 1959709
    invoke-virtual/range {p16 .. p16}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/D37;->q:Z

    .line 1959710
    move-object/from16 v0, p17

    iput-object v0, p0, LX/D37;->r:LX/0dx;

    .line 1959711
    move-object/from16 v0, p18

    iput-object v0, p0, LX/D37;->s:LX/0Ot;

    .line 1959712
    move-object/from16 v0, p19

    iput-object v0, p0, LX/D37;->t:LX/0Uh;

    .line 1959713
    move-object/from16 v0, p20

    iput-object v0, p0, LX/D37;->u:LX/2nC;

    .line 1959714
    return-void
.end method

.method private a(Landroid/content/Intent;)LX/D36;
    .locals 12

    .prologue
    .line 1959654
    const-string v0, "force_in_app_browser"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1959655
    sget-object v0, LX/D36;->WEBVIEW:LX/D36;

    .line 1959656
    :goto_0
    return-object v0

    .line 1959657
    :cond_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1959658
    const-string v2, "force_external_browser"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.facebook.intent.extra.SKIP_IN_APP_BROWSER"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    move v2, v3

    .line 1959659
    :goto_1
    move v0, v2

    .line 1959660
    if-eqz v0, :cond_2

    .line 1959661
    sget-object v0, LX/D36;->EXTERNAL:LX/D36;

    goto :goto_0

    .line 1959662
    :cond_2
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1959663
    iget-boolean v0, p0, LX/D37;->q:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, LX/D37;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    .line 1959664
    :goto_2
    move v0, v0

    .line 1959665
    if-eqz v0, :cond_3

    .line 1959666
    sget-object v0, LX/D36;->WEBVIEW:LX/D36;

    goto :goto_0

    .line 1959667
    :cond_3
    sget-object v0, LX/D36;->EXTERNAL:LX/D36;

    goto :goto_0

    .line 1959668
    :cond_4
    iget-object v2, p0, LX/D37;->p:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    invoke-virtual {v2, v4}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/D37;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/1C0;->a:LX/0Tn;

    invoke-interface {v2, v5, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    .line 1959669
    goto :goto_1

    .line 1959670
    :cond_5
    iget-object v2, p0, LX/D37;->r:LX/0dx;

    invoke-interface {v2}, LX/0dy;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v3

    .line 1959671
    goto :goto_1

    .line 1959672
    :cond_6
    const/4 v6, 0x0

    .line 1959673
    iget-object v7, p0, LX/D37;->t:LX/0Uh;

    const/16 v8, 0xcc

    invoke-virtual {v7, v8, v6}, LX/0Uh;->a(IZ)Z

    move-result v7

    if-nez v7, :cond_9

    .line 1959674
    :cond_7
    :goto_3
    move v2, v6

    .line 1959675
    if-eqz v2, :cond_8

    move v2, v3

    .line 1959676
    goto :goto_1

    :cond_8
    move v2, v4

    .line 1959677
    goto :goto_1

    .line 1959678
    :cond_9
    iget-object v7, p0, LX/D37;->v:[Ljava/lang/String;

    if-nez v7, :cond_a

    .line 1959679
    iget-object v7, p0, LX/D37;->f:LX/0W3;

    sget-wide v8, LX/0X5;->aH:J

    const/4 v10, 0x0

    invoke-interface {v7, v8, v9, v10}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1959680
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 1959681
    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, LX/D37;->v:[Ljava/lang/String;

    .line 1959682
    :cond_a
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 1959683
    if-eqz v7, :cond_7

    .line 1959684
    invoke-static {v7}, LX/1H1;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v7

    .line 1959685
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1959686
    iget-object v9, p0, LX/D37;->v:[Ljava/lang/String;

    array-length v10, v9

    move v7, v6

    :goto_4
    if-ge v7, v10, :cond_7

    aget-object v11, v9, v7

    .line 1959687
    invoke-virtual {v8, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 1959688
    const/4 v6, 0x1

    goto :goto_3

    .line 1959689
    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 1959690
    :cond_c
    iget-object v0, p0, LX/D37;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v2}, LX/03R;->asBoolean(Z)Z

    move-result v0

    .line 1959691
    iget-object v3, p0, LX/D37;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/1C0;->a:LX/0Tn;

    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 1959692
    if-eqz v0, :cond_d

    if-nez v3, :cond_d

    move v0, v1

    goto/16 :goto_2

    :cond_d
    move v0, v2

    goto/16 :goto_2
.end method

.method private a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    const/4 v1, 0x1

    .line 1959634
    if-eqz p2, :cond_0

    instance-of v0, p2, Landroid/app/Activity;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1959635
    :cond_0
    :goto_0
    return v1

    .line 1959636
    :cond_1
    const-string v0, "1"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "2"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    move-object v0, p2

    .line 1959637
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 1959638
    if-eqz v0, :cond_0

    .line 1959639
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 1959640
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1959641
    :cond_3
    iget-object v3, p0, LX/D37;->f:LX/0W3;

    sget-wide v6, LX/0X5;->ht:J

    const-string v5, ""

    invoke-interface {v3, v6, v7, v5}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v0}, LX/D37;->b(LX/D37;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1959642
    if-eqz v3, :cond_0

    .line 1959643
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_4
    move v3, v4

    :goto_1
    packed-switch v3, :pswitch_data_1

    .line 1959644
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move v0, v2

    :goto_2
    move v1, v0

    .line 1959645
    goto :goto_0

    .line 1959646
    :cond_5
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1959647
    iget-object v3, p0, LX/D37;->f:LX/0W3;

    sget-wide v6, LX/0X5;->hu:J

    const-string v5, ""

    invoke-interface {v3, v6, v7, v5}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, LX/D37;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1959648
    if-nez v3, :cond_3

    goto :goto_0

    .line 1959649
    :pswitch_0
    const-string v3, "1"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v2

    goto :goto_1

    :pswitch_1
    const-string v3, "2"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    goto :goto_1

    .line 1959650
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1959651
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1959652
    check-cast p2, Landroid/app/Activity;

    invoke-virtual {p2, v4, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    move v0, v1

    .line 1959653
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1959629
    const/16 v0, 0x2c

    invoke-static {p0, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    .line 1959630
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1959631
    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1959632
    const/4 v0, 0x1

    .line 1959633
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/D37;
    .locals 23

    .prologue
    .line 1959627
    new-instance v2, LX/D37;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v4

    check-cast v4, LX/11i;

    invoke-static/range {p0 .. p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v5

    check-cast v5, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v6

    check-cast v6, LX/0W3;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/2n3;->a(LX/0QB;)LX/2n3;

    move-result-object v10

    check-cast v10, LX/2n3;

    const/16 v11, 0x374

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/1sI;->a(LX/0QB;)LX/1sz;

    move-result-object v12

    check-cast v12, LX/1sz;

    const/16 v13, 0x4c1

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v14

    check-cast v14, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v15

    check-cast v15, LX/0s6;

    const/16 v16, 0x37b

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x36f

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v18

    check-cast v18, Ljava/lang/Boolean;

    invoke-static/range {p0 .. p0}, LX/0dt;->a(LX/0QB;)LX/0dx;

    move-result-object v19

    check-cast v19, LX/0dx;

    const/16 v20, 0x29

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v21

    check-cast v21, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/2nC;->a(LX/0QB;)LX/2nC;

    move-result-object v22

    check-cast v22, LX/2nC;

    invoke-direct/range {v2 .. v22}, LX/D37;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/11i;Lcom/facebook/performancelogger/PerformanceLogger;LX/0W3;Lcom/facebook/content/SecureContextHelper;LX/03V;LX/0SG;LX/2n3;LX/0Or;LX/1sz;LX/0Ot;LX/0Zb;LX/0s6;LX/0Or;LX/0Or;Ljava/lang/Boolean;LX/0dx;LX/0Ot;LX/0Uh;LX/2nC;)V

    .line 1959628
    return-object v2
.end method

.method private static b(LX/D37;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1959614
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 1959615
    :goto_0
    return v0

    .line 1959616
    :cond_1
    const/16 v0, 0x2c

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    .line 1959617
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1959618
    const/16 v3, 0x3d

    invoke-static {v0, v3}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v3

    .line 1959619
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1959620
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1959621
    iget-object v3, p0, LX/D37;->n:LX/0s6;

    const/16 v5, 0x40

    invoke-virtual {v3, p2, v5}, LX/01H;->e(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 1959622
    iget-object v5, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v6, v5

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_2

    aget-object v7, v5, v3

    .line 1959623
    invoke-virtual {v7}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move v0, v1

    .line 1959624
    goto :goto_0

    .line 1959625
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 1959626
    goto :goto_0
.end method

.method private b(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 1959592
    invoke-direct {p0}, LX/D37;->c()V

    .line 1959593
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/047;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1959594
    :cond_0
    invoke-static {p1}, LX/D37;->e(Landroid/content/Intent;)V

    .line 1959595
    const/4 v0, 0x0

    .line 1959596
    :goto_0
    move v0, v0

    .line 1959597
    if-nez v0, :cond_2

    .line 1959598
    :cond_1
    :goto_1
    return v6

    .line 1959599
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1959600
    invoke-static {v0}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "/auth.php"

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 1959601
    if-eqz v0, :cond_3

    .line 1959602
    invoke-direct {p0, p1, p2}, LX/D37;->c(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v6

    goto :goto_1

    .line 1959603
    :cond_3
    iget-boolean v0, p0, LX/D37;->q:Z

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1959604
    invoke-static {v0}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "/work/sso/mobile_reauth"

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_3
    move v0, v1

    .line 1959605
    if-eqz v0, :cond_4

    .line 1959606
    invoke-static {p1}, LX/D37;->e(Landroid/content/Intent;)V

    goto :goto_1

    .line 1959607
    :cond_4
    sget-object v0, LX/D35;->a:[I

    invoke-direct {p0, p1}, LX/D37;->a(Landroid/content/Intent;)LX/D36;

    move-result-object v1

    invoke-virtual {v1}, LX/D36;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1959608
    iget-object v0, p0, LX/D37;->u:LX/2nC;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "iab_click_source"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "tracking_codes"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object v7, v2

    move-object v8, v2

    move v9, v6

    move-object v10, v2

    invoke-virtual/range {v0 .. v10}, LX/2nC;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 1959609
    invoke-static {p1}, LX/D37;->e(Landroid/content/Intent;)V

    .line 1959610
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    iget-object v0, p0, LX/D37;->t:LX/0Uh;

    const/16 v1, 0x416

    invoke-virtual {v0, v1, v6}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1959611
    const-string v0, "android.intent.extra.REFERRER"

    const-string v1, "android-app://m.facebook.com"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 1959612
    :pswitch_0
    iget-object v0, p0, LX/D37;->m:LX/2n3;

    invoke-virtual {v0, p1, p2}, LX/2n3;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1959613
    goto/16 :goto_1

    :cond_5
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private c()V
    .locals 6

    .prologue
    .line 1959586
    iget-object v0, p0, LX/D37;->t:LX/0Uh;

    const/16 v1, 0xd4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1959587
    :cond_0
    :goto_0
    return-void

    .line 1959588
    :cond_1
    iget-object v0, p0, LX/D37;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1C0;->j:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 1959589
    iget-object v2, p0, LX/D37;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 1959590
    sub-long v0, v2, v0

    const-wide/32 v4, 0x5265c00

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    .line 1959591
    iget-object v0, p0, LX/D37;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    new-instance v1, Lcom/facebook/ui/browser/BrowserExternalIntentHandler$1;

    invoke-direct {v1, p0, v2, v3}, Lcom/facebook/ui/browser/BrowserExternalIntentHandler$1;-><init>(LX/D37;J)V

    const v2, -0x61e4e427

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1959544
    iget-object v0, p0, LX/D37;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1C0;->d:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v0, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 1959545
    iget-object v0, p0, LX/D37;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide/32 v6, 0x5265c00

    cmp-long v0, v4, v6

    if-lez v0, :cond_5

    move v0, v1

    .line 1959546
    :goto_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-ge v3, v4, :cond_a

    if-eqz v0, :cond_a

    .line 1959547
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/high16 v4, 0x10000

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 1959548
    iget-object v4, p0, LX/D37;->f:LX/0W3;

    sget-wide v6, LX/0X5;->aG:J

    const-string v5, ""

    invoke-interface {v4, v6, v7, v5}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v3}, LX/D37;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1959549
    :goto_1
    if-eqz v3, :cond_0

    if-nez v0, :cond_9

    .line 1959550
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1959551
    const-string v3, "url"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1959552
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1959553
    const-string v3, "bp"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1959554
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 1959555
    if-eqz v3, :cond_1

    .line 1959556
    :try_start_1
    const-string v4, "url2"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1959557
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1959558
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v4, "Unwrapped Uri is null/empty"

    invoke-direct {v0, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1959559
    :catch_0
    move-exception v0

    .line 1959560
    :goto_2
    iget-object v4, p0, LX/D37;->c:LX/03V;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LX/D37;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".maybeUseInAppBrowser"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Launching SSO to vulnerable browser due to exception"

    invoke-static {v5, v6}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v5

    .line 1959561
    iput-object v0, v5, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1959562
    move-object v0, v5

    .line 1959563
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/03V;->a(LX/0VG;)V

    .line 1959564
    :cond_1
    :goto_3
    if-eqz v3, :cond_2

    .line 1959565
    iget-object v0, p0, LX/D37;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v3, LX/1C0;->d:LX/0Tn;

    iget-object v4, p0, LX/D37;->h:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-interface {v0, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1959566
    :cond_2
    sget-object v0, LX/D36;->WEBVIEW:LX/D36;

    invoke-direct {p0, p1}, LX/D37;->a(Landroid/content/Intent;)LX/D36;

    move-result-object v3

    if-ne v0, v3, :cond_7

    .line 1959567
    :goto_4
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    .line 1959568
    const-string v4, "redirect"

    if-eqz v1, :cond_8

    const-string v0, "1"

    :goto_5
    invoke-virtual {v3, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1959569
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1959570
    invoke-static {p1}, LX/D37;->e(Landroid/content/Intent;)V

    .line 1959571
    const/4 v0, 0x0

    .line 1959572
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1959573
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const-string v3, "url"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1959574
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1959575
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "cb"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1959576
    :cond_3
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1959577
    invoke-direct {p0, p1, p2, v0}, LX/D37;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 1959578
    :cond_4
    :goto_6
    return v2

    :cond_5
    move v0, v2

    .line 1959579
    goto/16 :goto_0

    .line 1959580
    :cond_6
    :try_start_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1959581
    iget-object v0, p0, LX/D37;->g:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p1, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    move v2, v1

    .line 1959582
    goto :goto_6

    :cond_7
    move v1, v2

    .line 1959583
    goto :goto_4

    .line 1959584
    :cond_8
    const-string v0, "0"

    goto :goto_5

    .line 1959585
    :catch_1
    move-exception v0

    move v3, v1

    goto/16 :goto_2

    :cond_9
    move v3, v1

    goto/16 :goto_3

    :cond_a
    move v3, v1

    goto/16 :goto_1
.end method

.method public static e(Landroid/content/Intent;)V
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1959529
    if-nez p0, :cond_0

    .line 1959530
    :goto_0
    return-void

    .line 1959531
    :cond_0
    const-string v0, "og_title"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959532
    const-string v0, "parent_story_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959533
    const-string v0, "conversations_feedback_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959534
    const-string v0, "tracking_codes"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959535
    const-string v0, "com.facebook.intent.extra.SKIP_IN_APP_BROWSER"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959536
    const-string v0, "force_external_browser"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959537
    const-string v0, "post_url_data"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959538
    const-string v0, "iab_click_source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959539
    const-string v0, "extra_survey_config"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1959540
    const-string v0, "browser_metrics_join_key"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;ILandroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 1959543
    invoke-direct {p0, p1, p3}, LX/D37;->b(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z
    .locals 1

    .prologue
    .line 1959542
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/D37;->b(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1959541
    invoke-direct {p0, p1, p2}, LX/D37;->b(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
