.class public abstract LX/EkR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/AU0;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnLongClickListener;"
    }
.end annotation


# instance fields
.field public a:I
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/AU0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2163450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Landroid/view/View;LX/AU0;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;)Z"
        }
    .end annotation
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 4

    .prologue
    .line 2163451
    iget-object v0, p0, LX/EkR;->b:LX/AU0;

    if-nez v0, :cond_0

    .line 2163452
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DAOItem is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163453
    :cond_0
    iget-object v0, p0, LX/EkR;->b:LX/AU0;

    iget v1, p0, LX/EkR;->a:I

    invoke-interface {v0, v1}, LX/AU0;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2163454
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t move dao to position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/EkR;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163455
    :cond_1
    iget-object v0, p0, LX/EkR;->b:LX/AU0;

    invoke-interface {v0}, LX/AU0;->d()J

    move-result-wide v0

    iget-wide v2, p0, LX/EkR;->c:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 2163456
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dao row ID mismatch! old = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/EkR;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " new = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/EkR;->b:LX/AU0;

    invoke-interface {v2}, LX/AU0;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2163457
    :cond_2
    iget-object v0, p0, LX/EkR;->b:LX/AU0;

    invoke-virtual {p0, p1, v0}, LX/EkR;->a(Landroid/view/View;LX/AU0;)Z

    move-result v0

    return v0
.end method
