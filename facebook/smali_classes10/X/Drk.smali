.class public final LX/Drk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;)V
    .locals 0

    .prologue
    .line 2049608
    iput-object p1, p0, LX/Drk;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2049585
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2049586
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v5, 0x739897c2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2049587
    if-eqz p1, :cond_0

    .line 2049588
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2049589
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 2049590
    :goto_2
    return-void

    .line 2049591
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2049592
    check-cast v0, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2049593
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2049594
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2049595
    check-cast v0, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2049596
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_5

    move v0, v1

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_1

    .line 2049597
    :cond_6
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2049598
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2049599
    check-cast v0, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :cond_7
    :goto_5
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    .line 2049600
    if-eqz v6, :cond_a

    invoke-virtual {v5, v6, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    :goto_6
    if-eqz v0, :cond_7

    .line 2049601
    invoke-virtual {v5, v6, v2}, LX/15i;->g(II)I

    move-result v0

    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2049602
    new-instance v6, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;

    invoke-virtual {v5, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    const-class v8, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v5, v0, v1, v8, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-direct {v6, v7, v0}, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V

    .line 2049603
    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 2049604
    :cond_8
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    :cond_9
    move v0, v2

    .line 2049605
    goto :goto_6

    :cond_a
    move v0, v2

    goto :goto_6

    .line 2049606
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2049607
    :cond_b
    iget-object v0, p0, LX/Drk;->a:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v0, v0, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->e:LX/1ro;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1ro;->a(LX/0Px;)V

    goto/16 :goto_2
.end method
