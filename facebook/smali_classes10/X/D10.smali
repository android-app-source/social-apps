.class public final LX/D10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/D15;

.field public final synthetic b:LX/D12;


# direct methods
.method public constructor <init>(LX/D12;LX/D15;)V
    .locals 0

    .prologue
    .line 1956176
    iput-object p1, p0, LX/D10;->b:LX/D12;

    iput-object p2, p0, LX/D10;->a:LX/D15;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x22ef9952

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1956177
    const v0, 0x7f0d01d2

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1956178
    iget-object v2, p0, LX/D10;->b:LX/D12;

    iget-object v2, v2, LX/D12;->a:LX/D15;

    iget-object v2, v2, LX/D14;->e:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v2, v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/lang/Integer;)V

    .line 1956179
    iget-object v2, p0, LX/D10;->b:LX/D12;

    iget-object v2, v2, LX/D12;->d:LX/0Px;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    .line 1956180
    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;->jn_()Ljava/lang/String;

    move-result-object v0

    .line 1956181
    if-nez v0, :cond_0

    .line 1956182
    iget-object v0, p0, LX/D10;->b:LX/D12;

    iget-object v0, v0, LX/D12;->a:LX/D15;

    iget-object v0, v0, LX/D15;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/D15;->f:Ljava/lang/String;

    const-string v3, "null getGetDirectionsUrl in location"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956183
    const v0, 0x196abff3

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1956184
    :goto_0
    return-void

    .line 1956185
    :cond_0
    iget-object v2, p0, LX/D10;->b:LX/D12;

    iget-object v2, v2, LX/D12;->a:LX/D15;

    iget-object v2, v2, LX/D14;->e:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;)V

    .line 1956186
    const v0, -0x605c6db4

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
