.class public final LX/EMH;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EMI;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<+",
            "LX/8dB;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/CxA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public final synthetic e:LX/EMI;


# direct methods
.method public constructor <init>(LX/EMI;)V
    .locals 1

    .prologue
    .line 2110459
    iput-object p1, p0, LX/EMH;->e:LX/EMI;

    .line 2110460
    move-object v0, p1

    .line 2110461
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2110462
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2110463
    const-string v0, "SearchResultsSaveToggleButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2110464
    if-ne p0, p1, :cond_1

    .line 2110465
    :cond_0
    :goto_0
    return v0

    .line 2110466
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2110467
    goto :goto_0

    .line 2110468
    :cond_3
    check-cast p1, LX/EMH;

    .line 2110469
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2110470
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2110471
    if-eq v2, v3, :cond_0

    .line 2110472
    iget-boolean v2, p0, LX/EMH;->a:Z

    iget-boolean v3, p1, LX/EMH;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2110473
    goto :goto_0

    .line 2110474
    :cond_4
    iget-object v2, p0, LX/EMH;->b:LX/CzL;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/EMH;->b:LX/CzL;

    iget-object v3, p1, LX/EMH;->b:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2110475
    goto :goto_0

    .line 2110476
    :cond_6
    iget-object v2, p1, LX/EMH;->b:LX/CzL;

    if-nez v2, :cond_5

    .line 2110477
    :cond_7
    iget-object v2, p0, LX/EMH;->c:LX/CxA;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/EMH;->c:LX/CxA;

    iget-object v3, p1, LX/EMH;->c:LX/CxA;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 2110478
    goto :goto_0

    .line 2110479
    :cond_9
    iget-object v2, p1, LX/EMH;->c:LX/CxA;

    if-nez v2, :cond_8

    .line 2110480
    :cond_a
    iget-object v2, p0, LX/EMH;->d:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/EMH;->d:Ljava/lang/String;

    iget-object v3, p1, LX/EMH;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2110481
    goto :goto_0

    .line 2110482
    :cond_b
    iget-object v2, p1, LX/EMH;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
