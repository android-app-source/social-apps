.class public LX/Cty;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 1

    .prologue
    .line 1945842
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1945843
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Cty;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, LX/Cty;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1945844
    return-void
.end method


# virtual methods
.method public final a(LX/Crd;)Z
    .locals 3

    .prologue
    .line 1945831
    sget-object v0, LX/Crd;->CLICK_MEDIA:LX/Crd;

    if-ne p1, v0, :cond_0

    .line 1945832
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1945833
    iget-object v1, p0, LX/Cty;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 1945834
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1945835
    iget-object v1, p0, LX/Cty;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1945836
    :cond_0
    invoke-super {p0, p1}, LX/Cts;->a(LX/Crd;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1945840
    iget-object v0, p0, LX/Cty;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 1945841
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1945837
    invoke-super {p0}, LX/Cts;->c()V

    .line 1945838
    const/4 v0, 0x0

    iput-object v0, p0, LX/Cty;->b:Landroid/net/Uri;

    .line 1945839
    return-void
.end method
