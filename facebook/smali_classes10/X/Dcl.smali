.class public LX/Dcl;
.super LX/3pF;
.source ""


# instance fields
.field private final a:Lcom/facebook/common/callercontext/CallerContext;

.field private final b:I

.field private final c:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

.field private final d:Z

.field private final e:Ljava/lang/String;

.field private final f:LX/2uF;


# direct methods
.method public constructor <init>(LX/0gc;Lcom/facebook/common/callercontext/CallerContext;ILcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;ZLjava/lang/String;LX/2uF;)V
    .locals 7
    .param p4    # Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2018899
    invoke-direct {p0, p1}, LX/3pF;-><init>(LX/0gc;)V

    .line 2018900
    iput-object p2, p0, LX/Dcl;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2018901
    iput p3, p0, LX/Dcl;->b:I

    .line 2018902
    iput-object p4, p0, LX/Dcl;->c:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    .line 2018903
    iput-boolean p5, p0, LX/Dcl;->d:Z

    .line 2018904
    iput-object p6, p0, LX/Dcl;->e:Ljava/lang/String;

    .line 2018905
    invoke-static {}, LX/2uF;->j()LX/3Si;

    move-result-object v0

    .line 2018906
    invoke-virtual {p7}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2018907
    const/4 v4, 0x0

    const-class v5, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    const/4 v6, 0x0

    invoke-virtual {v3, v2, v4, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    if-eq v4, v5, :cond_0

    .line 2018908
    const v4, -0x3749127d

    invoke-virtual {v0, v3, v2, v4}, LX/3Si;->c(LX/15i;II)LX/3Si;

    goto :goto_0

    .line 2018909
    :cond_1
    invoke-virtual {v0}, LX/3Si;->a()LX/2uF;

    move-result-object v0

    iput-object v0, p0, LX/Dcl;->f:LX/2uF;

    .line 2018910
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2018876
    iget-object v0, p0, LX/Dcl;->f:LX/2uF;

    invoke-virtual {v0, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    const/4 v0, 0x2

    invoke-virtual {v1, v2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 13

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 2018878
    iget-object v1, p0, LX/Dcl;->f:LX/2uF;

    invoke-virtual {v1, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v1

    iget-object v7, v1, LX/1vs;->a:LX/15i;

    iget v8, v1, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2018879
    iget-object v1, p0, LX/Dcl;->c:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    iget-object v2, p0, LX/Dcl;->e:Ljava/lang/String;

    iget v3, p0, LX/Dcl;->b:I

    if-ne p1, v3, :cond_0

    move v3, v0

    :goto_0
    const-class v5, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    const/4 v6, 0x0

    invoke-virtual {v7, v8, v4, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;

    iget-boolean v5, p0, LX/Dcl;->d:Z

    const/4 v6, 0x3

    invoke-virtual {v7, v8, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v8, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/Dcl;->a:Lcom/facebook/common/callercontext/CallerContext;

    move v0, p1

    const/4 p1, 0x1

    .line 2018880
    new-instance v9, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;

    invoke-direct {v9}, Lcom/facebook/localcontent/photos/PhotoCategoryFragment;-><init>()V

    .line 2018881
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2018882
    const-string v11, "local_content_entry_point"

    invoke-virtual {v10, v11, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2018883
    const-string v11, "profileId"

    invoke-virtual {v10, v11, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018884
    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLAvailablePhotoCategoryEnum;->name()Ljava/lang/String;

    move-result-object v11

    .line 2018885
    const-string v12, "pandora_instance_id"

    new-instance p0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;

    invoke-direct {p0, v2, v0, v11, v1}, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;)V

    invoke-virtual {v10, v12, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2018886
    const-string v12, "isDefaultLandingPage"

    invoke-virtual {v10, v12, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2018887
    const-string v12, "callerContext"

    invoke-virtual {v10, v12, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2018888
    const-string v12, "photo_category"

    invoke-virtual {v10, v12, v11}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2018889
    const-string v11, "local_content_photo_upload_enabled"

    invoke-virtual {v10, v11, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2018890
    const-string v11, "local_content_upload_message"

    invoke-virtual {v10, v11, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018891
    const-string v11, "local_content_secondary_upload_message"

    invoke-virtual {v10, v11, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018892
    const-string v11, "pandora_two_views_row"

    invoke-virtual {v10, v11, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2018893
    const-string v11, "pandora_non_highlight_worthy_single_photo"

    invoke-virtual {v10, v11, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2018894
    invoke-virtual {v9, v10}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2018895
    move-object v0, v9

    .line 2018896
    return-object v0

    .line 2018897
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move v3, v4

    .line 2018898
    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2018877
    iget-object v0, p0, LX/Dcl;->f:LX/2uF;

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    return v0
.end method
