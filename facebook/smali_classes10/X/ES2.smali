.class public final enum LX/ES2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ES2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ES2;

.field public static final enum MAINTAIN_RETRY:LX/ES2;

.field public static final enum NO_RETRY:LX/ES2;

.field public static final enum RESET_RETRY:LX/ES2;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2121925
    new-instance v0, LX/ES2;

    const-string v1, "NO_RETRY"

    invoke-direct {v0, v1, v2}, LX/ES2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ES2;->NO_RETRY:LX/ES2;

    new-instance v0, LX/ES2;

    const-string v1, "RESET_RETRY"

    invoke-direct {v0, v1, v3}, LX/ES2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ES2;->RESET_RETRY:LX/ES2;

    new-instance v0, LX/ES2;

    const-string v1, "MAINTAIN_RETRY"

    invoke-direct {v0, v1, v4}, LX/ES2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ES2;->MAINTAIN_RETRY:LX/ES2;

    .line 2121926
    const/4 v0, 0x3

    new-array v0, v0, [LX/ES2;

    sget-object v1, LX/ES2;->NO_RETRY:LX/ES2;

    aput-object v1, v0, v2

    sget-object v1, LX/ES2;->RESET_RETRY:LX/ES2;

    aput-object v1, v0, v3

    sget-object v1, LX/ES2;->MAINTAIN_RETRY:LX/ES2;

    aput-object v1, v0, v4

    sput-object v0, LX/ES2;->$VALUES:[LX/ES2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2121924
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ES2;
    .locals 1

    .prologue
    .line 2121923
    const-class v0, LX/ES2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ES2;

    return-object v0
.end method

.method public static values()[LX/ES2;
    .locals 1

    .prologue
    .line 2121922
    sget-object v0, LX/ES2;->$VALUES:[LX/ES2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ES2;

    return-object v0
.end method
