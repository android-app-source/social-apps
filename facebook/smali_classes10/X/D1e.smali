.class public final LX/D1e;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocatorNearbyLocationsQuery;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/D1i;

.field public final synthetic b:LX/D1g;


# direct methods
.method public constructor <init>(LX/D1g;LX/D1i;)V
    .locals 0

    .prologue
    .line 1957288
    iput-object p1, p0, LX/D1e;->b:LX/D1g;

    iput-object p2, p0, LX/D1e;->a:LX/D1i;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1957289
    iget-object v0, p0, LX/D1e;->b:LX/D1g;

    invoke-static {v0}, LX/D1g;->b(LX/D1g;)V

    .line 1957290
    iget-object v0, p0, LX/D1e;->a:LX/D1i;

    iget-object v0, v0, LX/D1i;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v0, p1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/lang/Throwable;)V

    .line 1957291
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1957292
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1957293
    iget-object v0, p0, LX/D1e;->b:LX/D1g;

    invoke-static {v0}, LX/D1g;->b(LX/D1g;)V

    .line 1957294
    if-eqz p1, :cond_0

    .line 1957295
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1957296
    if-nez v0, :cond_1

    .line 1957297
    :cond_0
    iget-object v0, p0, LX/D1e;->a:LX/D1i;

    iget-object v0, v0, LX/D1i;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(Ljava/lang/Throwable;)V

    .line 1957298
    :goto_0
    return-void

    .line 1957299
    :cond_1
    iget-object v0, p0, LX/D1e;->a:LX/D1i;

    iget-object v1, v0, LX/D1i;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 1957300
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1957301
    check-cast v0, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocatorNearbyLocationsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocatorNearbyLocationsQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(LX/0Px;)V

    goto :goto_0
.end method
