.class public LX/DDW;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DDW",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975687
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1975688
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DDW;->b:LX/0Zi;

    .line 1975689
    iput-object p1, p0, LX/DDW;->a:LX/0Ot;

    .line 1975690
    return-void
.end method

.method public static a(LX/0QB;)LX/DDW;
    .locals 4

    .prologue
    .line 1975691
    const-class v1, LX/DDW;

    monitor-enter v1

    .line 1975692
    :try_start_0
    sget-object v0, LX/DDW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975693
    sput-object v2, LX/DDW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975694
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975695
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975696
    new-instance v3, LX/DDW;

    const/16 p0, 0x1f5e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DDW;-><init>(LX/0Ot;)V

    .line 1975697
    move-object v0, v3

    .line 1975698
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975699
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DDW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975700
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975701
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1975702
    check-cast p2, LX/DDV;

    .line 1975703
    iget-object v0, p0, LX/DDW;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;

    iget-object v1, p2, LX/DDV;->a:LX/DDZ;

    iget-object v2, p2, LX/DDV;->b:LX/1Po;

    .line 1975704
    iget-object v6, v1, LX/DDZ;->b:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 1975705
    const-string v3, ""

    .line 1975706
    iget-object v4, v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->k:LX/DD1;

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    .line 1975707
    if-nez v6, :cond_3

    .line 1975708
    const-string v7, ""

    .line 1975709
    :goto_0
    move-object v7, v7

    .line 1975710
    if-eqz v6, :cond_2

    .line 1975711
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1975712
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    :goto_1
    move-object v3, v2

    .line 1975713
    check-cast v3, LX/1Pr;

    new-instance v8, LX/DDX;

    invoke-direct {v8, v6}, LX/DDX;-><init>(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;)V

    iget-object v4, v1, LX/DDZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975714
    iget-object v9, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v9

    .line 1975715
    check-cast v4, LX/0jW;

    invoke-interface {v3, v8, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DDY;

    .line 1975716
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v8, 0x0

    invoke-interface {v4, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const v8, 0x7f020a3c

    invoke-interface {v4, v8}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x2

    .line 1975717
    const/4 v8, 0x0

    .line 1975718
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 1975719
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 1975720
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    const v10, 0x7f020a3c

    invoke-interface {v9, v10}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v9

    .line 1975721
    iget-object v10, v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->c:LX/1nu;

    invoke-virtual {v10, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v10

    invoke-virtual {v10, v8}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v10

    sget-object v6, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v10, v6}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const v6, 0x7f0b125a

    invoke-interface {v10, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v10

    const v6, 0x7f0b125a

    invoke-interface {v10, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v10

    move-object v8, v10

    .line 1975722
    invoke-interface {v9, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    const/4 v6, 0x1

    .line 1975723
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v9

    const v10, 0x7f020afc

    invoke-virtual {v9, v10}, LX/1o5;->h(I)LX/1o5;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    iget-object v10, v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->a:Landroid/content/res/Resources;

    const p2, 0x7f081c41

    invoke-virtual {v10, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v9

    invoke-interface {v9, v6}, LX/1Di;->c(I)LX/1Di;

    move-result-object v9

    const v10, 0x7f0b1253

    invoke-interface {v9, v6, v10}, LX/1Di;->l(II)LX/1Di;

    move-result-object v9

    const/4 v10, 0x2

    const p2, 0x7f0b1253

    invoke-interface {v9, v10, p2}, LX/1Di;->l(II)LX/1Di;

    move-result-object v9

    const v10, 0x7f0a058e

    invoke-interface {v9, v10}, LX/1Di;->x(I)LX/1Di;

    move-result-object v9

    .line 1975724
    const v10, -0x12ad933b

    const/4 p2, 0x0

    invoke-static {p1, v10, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v10

    move-object v10, v10

    .line 1975725
    invoke-interface {v9, v10}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v9

    move-object v9, v9

    .line 1975726
    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v8, v9}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b1259

    invoke-interface {v8, v9}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b1259

    invoke-interface {v8, v9}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v8

    move-object v6, v8

    .line 1975727
    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 v9, 0x1

    .line 1975728
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v9}, LX/1ne;->t(I)LX/1ne;

    move-result-object v6

    const v8, 0x7f0b0050

    invoke-virtual {v6, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v8, 0x7f0a0099

    invoke-virtual {v6, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v8}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v8, 0x7f0b1250

    invoke-interface {v6, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const v8, 0x7f0b1251

    invoke-interface {v6, v8}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const v8, 0x7f0b1253

    invoke-interface {v6, v9, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    const/4 v8, 0x0

    const v9, 0x7f0b1254

    invoke-interface {v6, v8, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    const/4 v8, 0x2

    const v9, 0x7f0b1254

    invoke-interface {v6, v8, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    move-object v5, v6

    .line 1975729
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 1975730
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00a8

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b1250

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b1251

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x0

    const v8, 0x7f0b1254

    invoke-interface {v5, v6, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x2

    const v8, 0x7f0b1254

    invoke-interface {v5, v6, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 1975731
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->f:LX/DDC;

    const/4 v6, 0x0

    .line 1975732
    new-instance v7, LX/DDA;

    invoke-direct {v7, v5}, LX/DDA;-><init>(LX/DDC;)V

    .line 1975733
    iget-object v8, v5, LX/DDC;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/DDB;

    .line 1975734
    if-nez v8, :cond_1

    .line 1975735
    new-instance v8, LX/DDB;

    invoke-direct {v8, v5}, LX/DDB;-><init>(LX/DDC;)V

    .line 1975736
    :cond_1
    invoke-static {v8, p1, v6, v6, v7}, LX/DDB;->a$redex0(LX/DDB;LX/1De;IILX/DDA;)V

    .line 1975737
    move-object v7, v8

    .line 1975738
    move-object v6, v7

    .line 1975739
    move-object v5, v6

    .line 1975740
    iget-object v6, v5, LX/DDB;->a:LX/DDA;

    iput-object v2, v6, LX/DDA;->c:LX/1Po;

    .line 1975741
    iget-object v6, v5, LX/DDB;->e:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1975742
    move-object v5, v5

    .line 1975743
    iget-object v6, v5, LX/DDB;->a:LX/DDA;

    iput-object v1, v6, LX/DDA;->a:LX/DDZ;

    .line 1975744
    iget-object v6, v5, LX/DDB;->e:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1975745
    move-object v5, v5

    .line 1975746
    iget-object v6, v5, LX/DDB;->a:LX/DDA;

    iput-object v3, v6, LX/DDA;->b:LX/DDY;

    .line 1975747
    iget-object v6, v5, LX/DDB;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1975748
    move-object v3, v5

    .line 1975749
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const/16 v4, 0x8

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/1Di;->j(II)LX/1Di;

    move-result-object v3

    .line 1975750
    const v4, -0x66c17522

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1975751
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1975752
    return-object v0

    :cond_2
    move-object v5, v3

    goto/16 :goto_1

    .line 1975753
    :cond_3
    invoke-static {v5}, LX/DD1;->c(LX/1PT;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 1975754
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1975755
    :cond_4
    invoke-static {v5}, LX/DD1;->b(LX/1PT;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 1975756
    iget-object v7, v4, LX/DD1;->a:Landroid/content/res/Resources;

    const v8, 0x7f081c42

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGroup;->v()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1975757
    :cond_5
    const-string v7, ""

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1975758
    invoke-static {}, LX/1dS;->b()V

    .line 1975759
    iget v0, p1, LX/1dQ;->b:I

    .line 1975760
    sparse-switch v0, :sswitch_data_0

    .line 1975761
    :goto_0
    return-object v2

    .line 1975762
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1975763
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1975764
    check-cast v1, LX/DDV;

    .line 1975765
    iget-object v3, p0, LX/DDW;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;

    iget-object v4, v1, LX/DDV;->a:LX/DDZ;

    .line 1975766
    iget-object p1, v4, LX/DDZ;->b:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 1975767
    sget-object p2, LX/0ax;->bE:Ljava/lang/String;

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object p1

    aput-object p1, p0, v1

    invoke-static {p2, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 1975768
    iget-object p2, v3, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->e:LX/17Y;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {p2, p0, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    .line 1975769
    iget-object p2, v3, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {p2, p1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1975770
    goto :goto_0

    .line 1975771
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1975772
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1975773
    check-cast v1, LX/DDV;

    .line 1975774
    iget-object v3, p0, LX/DDW;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;

    iget-object v4, v1, LX/DDV;->a:LX/DDZ;

    iget-object p1, v1, LX/DDV;->b:LX/1Po;

    invoke-virtual {v3, v4, p1}, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->a(LX/DDZ;LX/1Po;)V

    .line 1975775
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x66c17522 -> :sswitch_0
        -0x12ad933b -> :sswitch_1
    .end sparse-switch
.end method
