.class public LX/E9m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/E9m;


# instance fields
.field public final a:LX/17W;

.field private final b:LX/E9x;

.field public final c:LX/79D;


# direct methods
.method public constructor <init>(LX/17W;LX/E9x;LX/79D;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2084825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2084826
    iput-object p1, p0, LX/E9m;->a:LX/17W;

    .line 2084827
    iput-object p2, p0, LX/E9m;->b:LX/E9x;

    .line 2084828
    iput-object p3, p0, LX/E9m;->c:LX/79D;

    .line 2084829
    return-void
.end method

.method private static a(LX/E9m;Ljava/lang/String;Ljava/lang/String;)LX/4lK;
    .locals 1

    .prologue
    .line 2084830
    new-instance v0, LX/E9k;

    invoke-direct {v0, p0, p1, p2}, LX/E9k;-><init>(LX/E9m;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/E9m;
    .locals 15

    .prologue
    .line 2084831
    sget-object v0, LX/E9m;->d:LX/E9m;

    if-nez v0, :cond_1

    .line 2084832
    const-class v1, LX/E9m;

    monitor-enter v1

    .line 2084833
    :try_start_0
    sget-object v0, LX/E9m;->d:LX/E9m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2084834
    if-eqz v2, :cond_0

    .line 2084835
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2084836
    new-instance v6, LX/E9m;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    .line 2084837
    new-instance v7, LX/E9x;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-static {v0}, LX/1nH;->a(LX/0QB;)LX/1nH;

    move-result-object v9

    check-cast v9, LX/1nI;

    const/16 v10, 0x31c5

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x15e7

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {v0}, LX/E9e;->a(LX/0QB;)LX/E9e;

    move-result-object v12

    check-cast v12, LX/E9e;

    invoke-static {v0}, LX/BNP;->a(LX/0QB;)LX/BNP;

    move-result-object v13

    check-cast v13, LX/BNP;

    invoke-static {v0}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v14

    check-cast v14, LX/79D;

    invoke-direct/range {v7 .. v14}, LX/E9x;-><init>(LX/17W;LX/1nI;LX/0Ot;LX/0Or;LX/E9e;LX/BNP;LX/79D;)V

    .line 2084838
    move-object v4, v7

    .line 2084839
    check-cast v4, LX/E9x;

    invoke-static {v0}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v5

    check-cast v5, LX/79D;

    invoke-direct {v6, v3, v4, v5}, LX/E9m;-><init>(LX/17W;LX/E9x;LX/79D;)V

    .line 2084840
    move-object v0, v6

    .line 2084841
    sput-object v0, LX/E9m;->d:LX/E9m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2084842
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2084843
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2084844
    :cond_1
    sget-object v0, LX/E9m;->d:LX/E9m;

    return-object v0

    .line 2084845
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2084846
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/E9m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/79C;)Landroid/text/style/ClickableSpan;
    .locals 6

    .prologue
    .line 2084847
    new-instance v0, LX/E9j;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/E9j;-><init>(LX/E9m;Ljava/lang/String;Ljava/lang/String;LX/79C;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Landroid/content/Context;)Landroid/text/style/TextAppearanceSpan;
    .locals 2

    .prologue
    .line 2084848
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f0e066d

    invoke-direct {v0, p0, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method private static a(LX/E9m;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v7, 0x21

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2084849
    new-instance v0, LX/47x;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const v1, 0x7f0814f2

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "REVIEW_CREATOR"

    aput-object v3, v2, v4

    const-string v3, "REVIEWED_PAGE"

    aput-object v3, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    .line 2084850
    const-string v1, "REVIEW_CREATOR"

    new-array v2, v6, [Ljava/lang/Object;

    sget-object v3, LX/79C;->REVIEW_CREATOR_NAME:LX/79C;

    invoke-static {p0, p2, p4, p2, v3}, LX/E9m;->a(LX/E9m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/79C;)Landroid/text/style/ClickableSpan;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p1}, LX/E9m;->a(Landroid/content/Context;)Landroid/text/style/TextAppearanceSpan;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, p3, v7, v2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    .line 2084851
    const-string v1, "REVIEWED_PAGE"

    new-array v2, v6, [Ljava/lang/Object;

    sget-object v3, LX/79C;->REVIEW_PAGE_NAME:LX/79C;

    invoke-static {p0, p4, p4, p2, v3}, LX/E9m;->a(LX/E9m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/79C;)Landroid/text/style/ClickableSpan;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p1}, LX/E9m;->a(Landroid/content/Context;)Landroid/text/style/TextAppearanceSpan;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, p5, v7, v2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    .line 2084852
    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/E9m;LX/79C;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2084853
    iget-object v0, p0, LX/E9m;->c:LX/79D;

    const-string v1, "user_reviews_list"

    invoke-virtual {v0, v1, p2, p3, p1}, LX/79D;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/79C;)V

    .line 2084854
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reviews/ui/ReviewFeedRowView;Landroid/util/Pair;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reviews/ui/ReviewFeedRowView;",
            "Landroid/util/Pair",
            "<",
            "LX/5tj;",
            "Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsInterfaces$FetchSingleReviewQuery$RepresentedProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2084855
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/5ti;

    invoke-static {v0}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v2

    .line 2084856
    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/5ti;

    invoke-static {v0}, LX/BNJ;->a(LX/5ti;)Ljava/lang/String;

    move-result-object v3

    .line 2084857
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    invoke-static {v0}, LX/BNJ;->a(Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;)Ljava/lang/String;

    move-result-object v4

    .line 2084858
    iget-object v0, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;

    .line 2084859
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    move-object v5, v1

    .line 2084860
    invoke-virtual {p1}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/E9m;->a(LX/E9m;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 2084861
    iget-object v0, p0, LX/E9m;->b:LX/E9x;

    iget-object v2, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, LX/5tj;

    const-string v6, "user_see_all_reviews"

    const-string v7, "user_reviews_list"

    iget-object v1, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, LX/5ti;

    invoke-static {v1}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v4, v1}, LX/E9m;->a(LX/E9m;Ljava/lang/String;Ljava/lang/String;)LX/4lK;

    move-result-object v8

    sget-object v9, LX/4lJ;->COLLAPSED:LX/4lJ;

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, LX/E9x;->a(Lcom/facebook/reviews/ui/ReviewFeedRowView;LX/5tj;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/4lK;LX/4lJ;)V

    .line 2084862
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/reviews/util/protocol/graphql/FetchUserReviewsModels$FetchSingleReviewQueryModel$RepresentedProfileModel;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reviews/ui/ReviewFeedRowView;Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)V
    .locals 9

    .prologue
    .line 2084863
    invoke-static {p2}, LX/BNJ;->b(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)Ljava/lang/String;

    move-result-object v3

    .line 2084864
    iget-object v0, p0, LX/E9m;->b:LX/E9x;

    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v2

    .line 2084865
    invoke-static {p2}, LX/BNJ;->d(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2084866
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-nez v4, :cond_3

    .line 2084867
    :cond_0
    const/4 v1, 0x0

    .line 2084868
    :goto_0
    move-object v4, v1

    .line 2084869
    const-string v5, "user_see_all_reviews"

    const-string v6, "user_reviews_list"

    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v1

    invoke-static {v1}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v3, v1}, LX/E9m;->a(LX/E9m;Ljava/lang/String;Ljava/lang/String;)LX/4lK;

    move-result-object v7

    sget-object v8, LX/4lJ;->COLLAPSED:LX/4lJ;

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, LX/E9x;->a(Lcom/facebook/reviews/ui/ReviewFeedRowView;LX/5tj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/4lK;LX/4lJ;)V

    .line 2084870
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    move-result-object v0

    if-nez v0, :cond_4

    .line 2084871
    :cond_1
    const/4 v0, 0x0

    .line 2084872
    :goto_1
    move-object v0, v0

    .line 2084873
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setPageAttachmentView(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2084874
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2084875
    if-eqz v0, :cond_2

    .line 2084876
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->b()LX/5tj;

    move-result-object v0

    invoke-static {v0}, LX/BNJ;->b(LX/5ti;)Ljava/lang/String;

    move-result-object v0

    .line 2084877
    if-nez v3, :cond_5

    .line 2084878
    const/4 v1, 0x0

    .line 2084879
    :goto_2
    move-object v0, v1

    .line 2084880
    invoke-virtual {p1, v0}, Lcom/facebook/reviews/ui/ReviewFeedRowView;->setAttachmentClickListener(Landroid/view/View$OnClickListener;)V

    .line 2084881
    :cond_2
    return-void

    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_1

    :cond_5
    new-instance v1, LX/E9l;

    invoke-direct {v1, p0, v3, v0}, LX/E9l;-><init>(LX/E9m;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
