.class public final enum LX/DYS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DYS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DYS;

.field public static final enum MEMBER_REQUEST_ACCEPTED:LX/DYS;

.field public static final enum MEMBER_REQUEST_BLOCKED:LX/DYS;

.field public static final enum MEMBER_REQUEST_IGNORED:LX/DYS;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2011526
    new-instance v0, LX/DYS;

    const-string v1, "MEMBER_REQUEST_IGNORED"

    invoke-direct {v0, v1, v2}, LX/DYS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DYS;->MEMBER_REQUEST_IGNORED:LX/DYS;

    .line 2011527
    new-instance v0, LX/DYS;

    const-string v1, "MEMBER_REQUEST_ACCEPTED"

    invoke-direct {v0, v1, v3}, LX/DYS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DYS;->MEMBER_REQUEST_ACCEPTED:LX/DYS;

    .line 2011528
    new-instance v0, LX/DYS;

    const-string v1, "MEMBER_REQUEST_BLOCKED"

    invoke-direct {v0, v1, v4}, LX/DYS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DYS;->MEMBER_REQUEST_BLOCKED:LX/DYS;

    .line 2011529
    const/4 v0, 0x3

    new-array v0, v0, [LX/DYS;

    sget-object v1, LX/DYS;->MEMBER_REQUEST_IGNORED:LX/DYS;

    aput-object v1, v0, v2

    sget-object v1, LX/DYS;->MEMBER_REQUEST_ACCEPTED:LX/DYS;

    aput-object v1, v0, v3

    sget-object v1, LX/DYS;->MEMBER_REQUEST_BLOCKED:LX/DYS;

    aput-object v1, v0, v4

    sput-object v0, LX/DYS;->$VALUES:[LX/DYS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2011530
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DYS;
    .locals 1

    .prologue
    .line 2011531
    const-class v0, LX/DYS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DYS;

    return-object v0
.end method

.method public static values()[LX/DYS;
    .locals 1

    .prologue
    .line 2011532
    sget-object v0, LX/DYS;->$VALUES:[LX/DYS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DYS;

    return-object v0
.end method
