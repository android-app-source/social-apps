.class public LX/DcJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dc9;


# static fields
.field private static final a:I


# instance fields
.field private final b:LX/03V;

.field private final c:LX/Dc5;

.field private final d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2018203
    sget-object v0, LX/DcG;->STRUCTURED_MENU:LX/DcG;

    invoke-virtual {v0}, LX/DcG;->ordinal()I

    move-result v0

    sput v0, LX/DcJ;->a:I

    return-void
.end method

.method public constructor <init>(LX/03V;LX/Dc5;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2018204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2018205
    iput-object p1, p0, LX/DcJ;->b:LX/03V;

    .line 2018206
    iput-object p2, p0, LX/DcJ;->c:LX/Dc5;

    .line 2018207
    iput-object p3, p0, LX/DcJ;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2018208
    return-void
.end method


# virtual methods
.method public final a()LX/DcG;
    .locals 1

    .prologue
    .line 2018184
    sget-object v0, LX/DcG;->STRUCTURED_MENU:LX/DcG;

    return-object v0
.end method

.method public final a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;)V
    .locals 5
    .param p4    # Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2018186
    if-nez p4, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_4

    move v2, v0

    :goto_1
    if-eqz v2, :cond_6

    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    .line 2018187
    iget-object v0, p0, LX/DcJ;->b:LX/03V;

    const-class v2, LX/DcJ;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Trying to open structured menu but no menu is available"

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018188
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/localcontent/menus/admin/manager/MenuManagementPreviewActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2018189
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2018190
    const-string v2, "extra_menu_type"

    sget-object v3, LX/Dc2;->STRUCTURED_MENU:LX/Dc2;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2018191
    const-string v2, "local_content_food_photos_header_enabled"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2018192
    iget-object v1, p0, LX/DcJ;->d:Lcom/facebook/content/SecureContextHelper;

    sget v2, LX/DcJ;->a:I

    invoke-interface {v1, v0, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2018193
    return-void

    .line 2018194
    :cond_2
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2018195
    if-nez v2, :cond_3

    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_0

    .line 2018196
    :cond_4
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2018197
    invoke-virtual {v3, v2, v1}, LX/15i;->g(II)I

    move-result v2

    if-nez v2, :cond_5

    move v2, v0

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_1

    .line 2018198
    :cond_6
    invoke-virtual {p4}, Lcom/facebook/localcontent/protocol/graphql/MenuManagementQueryModels$MenuManagementInfoFieldsModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v1}, LX/15i;->g(II)I

    move-result v2

    .line 2018199
    const/4 v4, 0x2

    invoke-virtual {v3, v2, v4}, LX/15i;->h(II)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_2
.end method

.method public final a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 2018200
    sget v0, LX/DcJ;->a:I

    if-ne p3, v0, :cond_0

    const/4 v0, -0x1

    if-ne p4, v0, :cond_0

    .line 2018201
    iget-object v0, p0, LX/DcJ;->c:LX/Dc5;

    const-string v1, "structured"

    invoke-virtual {v0, p1, p2, v1}, LX/Dc5;->a(Lcom/facebook/localcontent/menus/admin/manager/PageMenuManagementFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2018202
    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 2018185
    sget v0, LX/DcJ;->a:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/15i;I)Z
    .locals 1
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "isVisibleMenu"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2018183
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Z
    .locals 1
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "shouldShowInManagementScreen"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2018182
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
