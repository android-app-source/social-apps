.class public final LX/DST;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V
    .locals 0

    .prologue
    .line 1999441
    iput-object p1, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1999473
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 1999474
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1999475
    iput-object v1, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    .line 1999476
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    const v1, 0x20ec1553    # 3.9994066E-19f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1999477
    :cond_0
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 1999478
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1999479
    iput-object v1, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    .line 1999480
    :cond_1
    sget-object v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->x:Ljava/lang/Class;

    const-string v1, "Failed to fetch all admin ids."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1999481
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1999442
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1999443
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 1999444
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1999445
    iput-object v1, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    .line 1999446
    :cond_0
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v0, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    if-nez v0, :cond_1

    .line 1999447
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1999448
    iput-object v1, v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    .line 1999449
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999450
    if-eqz v0, :cond_5

    .line 1999451
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999452
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1999453
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999454
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;->a()I

    move-result v0

    if-lez v0, :cond_5

    .line 1999455
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999456
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;->j()LX/0Px;

    move-result-object v2

    .line 1999457
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;

    .line 1999458
    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1999459
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->a()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v5

    if-ne v4, v5, :cond_3

    .line 1999460
    iget-object v4, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v4, v4, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1999461
    :cond_2
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1999462
    :cond_3
    iget-object v4, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    iget-object v4, v4, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1999463
    :cond_4
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->H(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    .line 1999464
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1999465
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1999466
    if-eqz v0, :cond_5

    .line 1999467
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1999468
    iget-object v2, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1999469
    iput-object v0, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->E:Ljava/lang/String;

    .line 1999470
    iget-object v0, p0, LX/DST;->a:Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    invoke-static {v0}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    .line 1999471
    :cond_5
    return-void

    .line 1999472
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
