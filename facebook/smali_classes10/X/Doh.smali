.class public final LX/Doh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/Integer;",
        "[B>;",
        "Landroid/content/ContentValues;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/2Ox;


# direct methods
.method public constructor <init>(LX/2Ox;J)V
    .locals 0

    .prologue
    .line 2042271
    iput-object p1, p0, LX/Doh;->b:LX/2Ox;

    iput-wide p2, p0, LX/Doh;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2042272
    check-cast p1, Landroid/util/Pair;

    .line 2042273
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2042274
    sget-object v0, LX/3GM;->a:LX/0U1;

    .line 2042275
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2042276
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2042277
    sget-object v0, LX/3GM;->b:LX/0U1;

    .line 2042278
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2042279
    iget-object v3, p0, LX/Doh;->b:LX/2Ox;

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [B

    invoke-static {v3, v0}, LX/2Ox;->a$redex0(LX/2Ox;[B)[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2042280
    sget-object v0, LX/3GM;->c:LX/0U1;

    .line 2042281
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 2042282
    iget-wide v2, p0, LX/Doh;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2042283
    return-object v1
.end method
