.class public final LX/ENR;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLNode;

.field public final synthetic c:LX/1Ps;

.field public final synthetic d:Lcom/facebook/fig/button/FigToggleButton;

.field public final synthetic e:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;ZLcom/facebook/graphql/model/GraphQLNode;LX/1Ps;Lcom/facebook/fig/button/FigToggleButton;)V
    .locals 0

    .prologue
    .line 2112594
    iput-object p1, p0, LX/ENR;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;

    iput-boolean p2, p0, LX/ENR;->a:Z

    iput-object p3, p0, LX/ENR;->b:Lcom/facebook/graphql/model/GraphQLNode;

    iput-object p4, p0, LX/ENR;->c:LX/1Ps;

    iput-object p5, p0, LX/ENR;->d:Lcom/facebook/fig/button/FigToggleButton;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2112595
    iget-object v1, p0, LX/ENR;->d:Lcom/facebook/fig/button/FigToggleButton;

    iget-boolean v0, p0, LX/ENR;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fig/button/FigToggleButton;->setChecked(Z)V

    .line 2112596
    iget-object v0, p0, LX/ENR;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2112597
    return-void

    .line 2112598
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2112599
    iget-object v0, p0, LX/ENR;->e:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    iget-boolean v2, p0, LX/ENR;->a:Z

    iget-object v1, p0, LX/ENR;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, LX/ENR;->c:LX/1Ps;

    check-cast v1, LX/Cxc;

    iget-object v4, p0, LX/ENR;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-interface {v1, v4}, LX/Cxc;->d(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    iget-object v1, p0, LX/ENR;->c:LX/1Ps;

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v4, v1}, LX/CvY;->a(ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2112600
    return-void
.end method
