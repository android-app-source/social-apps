.class public LX/CkY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1931088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1931084
    if-nez p0, :cond_0

    .line 1931085
    :goto_0
    return v0

    .line 1931086
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "pt"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 1931087
    :catch_0
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;LX/CkX;LX/Cka;)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1931074
    if-nez p0, :cond_1

    .line 1931075
    :cond_0
    :goto_0
    return v0

    .line 1931076
    :cond_1
    const-string v1, "full_width"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    .line 1931077
    iget v0, p2, LX/Cka;->a:F

    goto :goto_0

    .line 1931078
    :cond_2
    const-string v1, "padding"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1931079
    iget v0, p1, LX/CkX;->b:F

    goto :goto_0

    .line 1931080
    :cond_3
    const-string v1, "pt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1931081
    invoke-static {p0}, LX/CkY;->a(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p1}, LX/CkX;->a()F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_0

    .line 1931082
    :cond_4
    const-string v1, "grid"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1931083
    invoke-static {p0}, LX/CkY;->c(Ljava/lang/String;)F

    move-result v0

    iget v1, p1, LX/CkX;->g:F

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;LX/CkX;LX/Cka;)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1931044
    if-nez p0, :cond_1

    .line 1931045
    :cond_0
    :goto_0
    return v0

    .line 1931046
    :cond_1
    const-string v1, "full_height"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    .line 1931047
    iget v0, p2, LX/Cka;->b:F

    goto :goto_0

    .line 1931048
    :cond_2
    const-string v1, "padding"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1931049
    iget v0, p1, LX/CkX;->a:F

    goto :goto_0

    .line 1931050
    :cond_3
    const-string v1, "pt"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1931051
    invoke-static {p0}, LX/CkY;->a(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p1}, LX/CkX;->a()F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_0

    .line 1931052
    :cond_4
    const-string v1, "grid"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1931053
    invoke-static {p0}, LX/CkY;->c(Ljava/lang/String;)F

    move-result v0

    iget v1, p1, LX/CkX;->h:F

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1931058
    if-nez p0, :cond_0

    .line 1931059
    :goto_0
    return v0

    .line 1931060
    :cond_0
    const/16 v1, 0x23

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1931061
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 1931062
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 1931063
    :cond_1
    invoke-static {p0}, LX/1u4;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 1931064
    array-length v2, v1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 1931065
    aget-byte v0, v1, v0

    add-int/lit16 v0, v0, 0x100

    rem-int/lit16 v0, v0, 0x100

    .line 1931066
    aget-byte v2, v1, v4

    add-int/lit16 v2, v2, 0x100

    rem-int/lit16 v2, v2, 0x100

    .line 1931067
    aget-byte v3, v1, v5

    add-int/lit16 v3, v3, 0x100

    rem-int/lit16 v3, v3, 0x100

    .line 1931068
    const/4 v4, 0x3

    aget-byte v1, v1, v4

    add-int/lit16 v1, v1, 0x100

    rem-int/lit16 v1, v1, 0x100

    .line 1931069
    invoke-static {v0, v2, v3, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    goto :goto_0

    .line 1931070
    :cond_2
    aget-byte v0, v1, v0

    add-int/lit16 v0, v0, 0x100

    rem-int/lit16 v0, v0, 0x100

    .line 1931071
    aget-byte v2, v1, v4

    add-int/lit16 v2, v2, 0x100

    rem-int/lit16 v2, v2, 0x100

    .line 1931072
    aget-byte v1, v1, v5

    add-int/lit16 v1, v1, 0x100

    rem-int/lit16 v1, v1, 0x100

    .line 1931073
    invoke-static {v0, v2, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1931054
    if-nez p0, :cond_0

    .line 1931055
    :goto_0
    return v0

    .line 1931056
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "grid"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 1931057
    :catch_0
    goto :goto_0
.end method
