.class public final LX/EuB;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/EuD;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/EuC;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2179158
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2179159
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "friendModel"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "shouldShowAuxButtons"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "friendingLocation"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/EuB;->b:[Ljava/lang/String;

    .line 2179160
    iput v3, p0, LX/EuB;->c:I

    .line 2179161
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/EuB;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/EuB;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/EuB;LX/1De;IILX/EuC;)V
    .locals 1

    .prologue
    .line 2179154
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2179155
    iput-object p4, p0, LX/EuB;->a:LX/EuC;

    .line 2179156
    iget-object v0, p0, LX/EuB;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2179157
    return-void
.end method


# virtual methods
.method public final a(LX/2h7;)LX/EuB;
    .locals 2

    .prologue
    .line 2179151
    iget-object v0, p0, LX/EuB;->a:LX/EuC;

    iput-object p1, v0, LX/EuC;->c:LX/2h7;

    .line 2179152
    iget-object v0, p0, LX/EuB;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2179153
    return-object p0
.end method

.method public final a(Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;)LX/EuB;
    .locals 2

    .prologue
    .line 2179131
    iget-object v0, p0, LX/EuB;->a:LX/EuC;

    iput-object p1, v0, LX/EuC;->a:Lcom/facebook/friending/center/protocol/FriendsCenterDefaultFieldsGraphQLModels$FriendsCenterDefaultNodeModel;

    .line 2179132
    iget-object v0, p0, LX/EuB;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2179133
    return-object p0
.end method

.method public final a(Z)LX/EuB;
    .locals 2

    .prologue
    .line 2179148
    iget-object v0, p0, LX/EuB;->a:LX/EuC;

    iput-boolean p1, v0, LX/EuC;->b:Z

    .line 2179149
    iget-object v0, p0, LX/EuB;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2179150
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2179144
    invoke-super {p0}, LX/1X5;->a()V

    .line 2179145
    const/4 v0, 0x0

    iput-object v0, p0, LX/EuB;->a:LX/EuC;

    .line 2179146
    sget-object v0, LX/EuD;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2179147
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/EuD;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2179134
    iget-object v1, p0, LX/EuB;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/EuB;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/EuB;->c:I

    if-ge v1, v2, :cond_2

    .line 2179135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2179136
    :goto_0
    iget v2, p0, LX/EuB;->c:I

    if-ge v0, v2, :cond_1

    .line 2179137
    iget-object v2, p0, LX/EuB;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2179138
    iget-object v2, p0, LX/EuB;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2179139
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2179140
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2179141
    :cond_2
    iget-object v0, p0, LX/EuB;->a:LX/EuC;

    .line 2179142
    invoke-virtual {p0}, LX/EuB;->a()V

    .line 2179143
    return-object v0
.end method
