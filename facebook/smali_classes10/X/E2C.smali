.class public LX/E2C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/E2C;


# instance fields
.field public final a:LX/23P;


# direct methods
.method public constructor <init>(LX/23P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2072460
    iput-object p1, p0, LX/E2C;->a:LX/23P;

    .line 2072461
    return-void
.end method

.method public static a(LX/0QB;)LX/E2C;
    .locals 4

    .prologue
    .line 2072446
    sget-object v0, LX/E2C;->b:LX/E2C;

    if-nez v0, :cond_1

    .line 2072447
    const-class v1, LX/E2C;

    monitor-enter v1

    .line 2072448
    :try_start_0
    sget-object v0, LX/E2C;->b:LX/E2C;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072449
    if-eqz v2, :cond_0

    .line 2072450
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072451
    new-instance p0, LX/E2C;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-direct {p0, v3}, LX/E2C;-><init>(LX/23P;)V

    .line 2072452
    move-object v0, p0

    .line 2072453
    sput-object v0, LX/E2C;->b:LX/E2C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072454
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072455
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072456
    :cond_1
    sget-object v0, LX/E2C;->b:LX/E2C;

    return-object v0

    .line 2072457
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072458
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
