.class public LX/E94;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/E93;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2083939
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2083940
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;)LX/E93;
    .locals 21

    .prologue
    .line 2083941
    new-instance v1, LX/E93;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    const-class v2, LX/E2N;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/E2N;

    const-class v2, LX/3Tp;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/3Tp;

    invoke-static/range {p0 .. p0}, LX/E1j;->a(LX/0QB;)LX/E1j;

    move-result-object v9

    check-cast v9, LX/E1j;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v10

    check-cast v10, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v11

    check-cast v11, LX/1Db;

    invoke-static/range {p0 .. p0}, LX/AjP;->a(LX/0QB;)LX/AjP;

    move-result-object v12

    check-cast v12, LX/AjP;

    invoke-static/range {p0 .. p0}, LX/1My;->a(LX/0QB;)LX/1My;

    move-result-object v13

    check-cast v13, LX/1My;

    invoke-static/range {p0 .. p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v14

    check-cast v14, LX/1Db;

    invoke-static/range {p0 .. p0}, LX/E1l;->a(LX/0QB;)LX/E1l;

    move-result-object v15

    check-cast v15, LX/E1l;

    invoke-static/range {p0 .. p0}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v16

    check-cast v16, Lcom/facebook/reaction/ReactionUtil;

    invoke-static/range {p0 .. p0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v17

    check-cast v17, LX/CvY;

    invoke-static/range {p0 .. p0}, LX/Cfw;->a(LX/0QB;)LX/Cfw;

    move-result-object v18

    check-cast v18, LX/Cfw;

    invoke-static/range {p0 .. p0}, LX/3mH;->a(LX/0QB;)LX/3mH;

    move-result-object v19

    check-cast v19, LX/3mH;

    invoke-static/range {p0 .. p0}, LX/967;->a(LX/0QB;)LX/967;

    move-result-object v20

    check-cast v20, LX/967;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-direct/range {v1 .. v20}, LX/E93;-><init>(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;LX/0SG;LX/E2N;LX/3Tp;LX/E1j;LX/0bH;LX/1Db;LX/AjP;LX/1My;LX/1Db;LX/E1l;Lcom/facebook/reaction/ReactionUtil;LX/CvY;LX/Cfw;LX/3mH;LX/967;)V

    .line 2083942
    return-object v1
.end method
