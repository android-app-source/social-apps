.class public final enum LX/DnN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DnN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DnN;

.field public static final enum SERVICE_ITEM:LX/DnN;


# instance fields
.field public layoutResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2040310
    new-instance v0, LX/DnN;

    const-string v1, "SERVICE_ITEM"

    const v2, 0x7f030ea1

    invoke-direct {v0, v1, v3, v2}, LX/DnN;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DnN;->SERVICE_ITEM:LX/DnN;

    .line 2040311
    const/4 v0, 0x1

    new-array v0, v0, [LX/DnN;

    sget-object v1, LX/DnN;->SERVICE_ITEM:LX/DnN;

    aput-object v1, v0, v3

    sput-object v0, LX/DnN;->$VALUES:[LX/DnN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2040312
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2040313
    iput p3, p0, LX/DnN;->layoutResId:I

    .line 2040314
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DnN;
    .locals 1

    .prologue
    .line 2040315
    const-class v0, LX/DnN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DnN;

    return-object v0
.end method

.method public static values()[LX/DnN;
    .locals 1

    .prologue
    .line 2040316
    sget-object v0, LX/DnN;->$VALUES:[LX/DnN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DnN;

    return-object v0
.end method
