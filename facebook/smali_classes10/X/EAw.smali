.class public final LX/EAw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/EB0;


# direct methods
.method public constructor <init>(LX/EB0;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2086003
    iput-object p1, p0, LX/EAw;->d:LX/EB0;

    iput-object p2, p0, LX/EAw;->a:Ljava/lang/String;

    iput p3, p0, LX/EAw;->b:I

    iput-object p4, p0, LX/EAw;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2086004
    iget-object v0, p0, LX/EAw;->d:LX/EB0;

    iget-object v0, v0, LX/EB0;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNn;

    iget-object v1, p0, LX/EAw;->a:Ljava/lang/String;

    iget v2, p0, LX/EAw;->b:I

    iget-object v3, p0, LX/EAw;->c:Ljava/lang/String;

    .line 2086005
    new-instance v4, LX/BNl;

    invoke-direct {v4}, LX/BNl;-><init>()V

    move-object v4, v4

    .line 2086006
    const-string v5, "user_id"

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "after_cursor"

    invoke-virtual {v5, v6, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "review_vertical"

    const-string p0, "PLACES"

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "review_profile_pic_size"

    iget-object p0, v0, LX/BNn;->b:LX/1vn;

    invoke-virtual {p0}, LX/1vn;->b()Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v6, "review_cover_pic_size"

    iget-object p0, v0, LX/BNn;->b:LX/1vn;

    invoke-virtual {p0}, LX/1vn;->c()Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v6, "count"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2086007
    iget-object v5, v0, LX/BNn;->a:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    move-object v0, v4

    .line 2086008
    return-object v0
.end method
