.class public final LX/EN8;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 2111998
    iput-object p1, p0, LX/EN8;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iput-object p2, p0, LX/EN8;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2111999
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2112000
    iget-object v2, p0, LX/EN8;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2112001
    invoke-static {v2}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object p1

    move-object v1, p1

    .line 2112002
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2112003
    iget-object v1, p0, LX/EN8;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iget-object v1, v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/EN8;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iget-object v2, v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2112004
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2112005
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2112006
    iget-object v0, p0, LX/EN8;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2112007
    return-void
.end method
