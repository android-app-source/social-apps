.class public final LX/ENg;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ENh;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<",
            "LX/8dK;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Ps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/CharSequence;

.field public d:I

.field public final synthetic e:LX/ENh;


# direct methods
.method public constructor <init>(LX/ENh;)V
    .locals 1

    .prologue
    .line 2113159
    iput-object p1, p0, LX/ENg;->e:LX/ENh;

    .line 2113160
    move-object v0, p1

    .line 2113161
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2113162
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2113163
    const-string v0, "SearchResultsFlexibleNewsContextTitleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2113164
    if-ne p0, p1, :cond_1

    .line 2113165
    :cond_0
    :goto_0
    return v0

    .line 2113166
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2113167
    goto :goto_0

    .line 2113168
    :cond_3
    check-cast p1, LX/ENg;

    .line 2113169
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2113170
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2113171
    if-eq v2, v3, :cond_0

    .line 2113172
    iget-object v2, p0, LX/ENg;->a:LX/CzL;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ENg;->a:LX/CzL;

    iget-object v3, p1, LX/ENg;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2113173
    goto :goto_0

    .line 2113174
    :cond_5
    iget-object v2, p1, LX/ENg;->a:LX/CzL;

    if-nez v2, :cond_4

    .line 2113175
    :cond_6
    iget-object v2, p0, LX/ENg;->b:LX/1Ps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ENg;->b:LX/1Ps;

    iget-object v3, p1, LX/ENg;->b:LX/1Ps;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2113176
    goto :goto_0

    .line 2113177
    :cond_8
    iget-object v2, p1, LX/ENg;->b:LX/1Ps;

    if-nez v2, :cond_7

    .line 2113178
    :cond_9
    iget-object v2, p0, LX/ENg;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/ENg;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/ENg;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2113179
    goto :goto_0

    .line 2113180
    :cond_b
    iget-object v2, p1, LX/ENg;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_a

    .line 2113181
    :cond_c
    iget v2, p0, LX/ENg;->d:I

    iget v3, p1, LX/ENg;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2113182
    goto :goto_0
.end method
