.class public final LX/E7M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:LX/6WS;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/E7N;


# direct methods
.method public constructor <init>(LX/E7N;LX/6WS;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2081299
    iput-object p1, p0, LX/E7M;->c:LX/E7N;

    iput-object p2, p0, LX/E7M;->a:LX/6WS;

    iput-object p3, p0, LX/E7M;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 2081300
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2081301
    const v1, 0x7f0d3242

    if-ne v0, v1, :cond_2

    .line 2081302
    iget-object v0, p0, LX/E7M;->c:LX/E7N;

    iget-boolean v5, v0, LX/E7N;->c:Z

    .line 2081303
    iget-object v0, p0, LX/E7M;->c:LX/E7N;

    iget-object v0, v0, LX/E7N;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->e:LX/961;

    iget-object v1, p0, LX/E7M;->c:LX/E7N;

    iget-object v1, v1, LX/E7N;->b:LX/5sc;

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v5, :cond_0

    move v2, v8

    :cond_0
    const-string v4, "reaction_dialog"

    new-instance v9, LX/E7L;

    invoke-direct {v9, p0, v5}, LX/E7L;-><init>(LX/E7M;Z)V

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v9}, LX/961;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V

    :goto_0
    move v2, v8

    .line 2081304
    :cond_1
    return v2

    .line 2081305
    :cond_2
    const v1, 0x7f0d3243

    if-ne v0, v1, :cond_3

    .line 2081306
    iget-object v0, p0, LX/E7M;->c:LX/E7N;

    iget-object v0, v0, LX/E7N;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->a:LX/E1i;

    iget-object v1, p0, LX/E7M;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/E7M;->c:LX/E7N;

    iget-object v2, v2, LX/E7N;->b:LX/5sc;

    invoke-interface {v2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/E1i;->C(Landroid/content/Context;Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    .line 2081307
    iget-object v1, p0, LX/E7M;->c:LX/E7N;

    iget-object v1, v1, LX/E7N;->a:LX/E6y;

    iget-object v2, p0, LX/E7M;->c:LX/E7N;

    iget-object v2, v2, LX/E7N;->b:LX/5sc;

    invoke-interface {v2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E7M;->b:Landroid/view/View;

    .line 2081308
    invoke-virtual {v1, v2, v0, v3}, LX/Cfk;->a(Ljava/lang/String;LX/Cfl;Landroid/view/View;)V

    .line 2081309
    goto :goto_0

    .line 2081310
    :cond_3
    const v1, 0x7f0d3244

    if-ne v0, v1, :cond_4

    .line 2081311
    iget-object v0, p0, LX/E7M;->c:LX/E7N;

    iget-object v0, v0, LX/E7N;->a:LX/E6y;

    iget-object v0, v0, LX/E6y;->a:LX/E1i;

    iget-object v1, p0, LX/E7M;->c:LX/E7N;

    iget-object v1, v1, LX/E7N;->b:LX/5sc;

    invoke-interface {v1}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/E1i;->m(Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    .line 2081312
    iget-object v1, p0, LX/E7M;->c:LX/E7N;

    iget-object v1, v1, LX/E7N;->a:LX/E6y;

    iget-object v2, p0, LX/E7M;->c:LX/E7N;

    iget-object v2, v2, LX/E7N;->b:LX/5sc;

    invoke-interface {v2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E7M;->b:Landroid/view/View;

    .line 2081313
    invoke-virtual {v1, v2, v0, v3}, LX/Cfk;->a(Ljava/lang/String;LX/Cfl;Landroid/view/View;)V

    .line 2081314
    goto :goto_0

    .line 2081315
    :cond_4
    const v1, 0x7f0d3245

    if-ne v0, v1, :cond_1

    .line 2081316
    iget-object v0, p0, LX/E7M;->c:LX/E7N;

    iget-object v0, v0, LX/E7N;->b:LX/5sc;

    invoke-interface {v0}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/E7M;->c:LX/E7N;

    iget-object v1, v1, LX/E7N;->b:LX/5sc;

    invoke-interface {v1}, LX/5sc;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/E7M;->c:LX/E7N;

    iget-object v2, v2, LX/E7N;->b:LX/5sc;

    invoke-interface {v2}, LX/5sc;->l()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/E1i;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v0

    .line 2081317
    iget-object v1, p0, LX/E7M;->c:LX/E7N;

    iget-object v1, v1, LX/E7N;->a:LX/E6y;

    iget-object v2, p0, LX/E7M;->c:LX/E7N;

    iget-object v2, v2, LX/E7N;->b:LX/5sc;

    invoke-interface {v2}, LX/5sc;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/E7M;->b:Landroid/view/View;

    .line 2081318
    invoke-virtual {v1, v2, v0, v3}, LX/Cfk;->a(Ljava/lang/String;LX/Cfl;Landroid/view/View;)V

    .line 2081319
    goto/16 :goto_0
.end method
