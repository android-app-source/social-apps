.class public LX/D7h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D7g;


# instance fields
.field public final synthetic a:LX/2my;


# direct methods
.method public constructor <init>(LX/2my;)V
    .locals 0

    .prologue
    .line 1967447
    iput-object p1, p0, LX/D7h;->a:LX/2my;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 1967448
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967449
    :cond_0
    const/4 v0, 0x0

    .line 1967450
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0}, LX/0D5;->getY()F

    move-result v0

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1967421
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967422
    :cond_0
    :goto_0
    return-void

    .line 1967423
    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0D5;->setTranslationY(F)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1967451
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967452
    :cond_0
    :goto_0
    return-void

    .line 1967453
    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0D5;->setScrollX(I)V

    goto :goto_0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 1967454
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967455
    :cond_0
    const/4 v0, 0x0

    .line 1967456
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0}, LX/0D5;->getTranslationY()F

    move-result v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1967457
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967458
    :cond_0
    :goto_0
    return-void

    .line 1967459
    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0D5;->setScrollY(I)V

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1967441
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967442
    :cond_0
    const/4 v0, 0x0

    .line 1967443
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0}, LX/0D5;->getScrollY()I

    move-result v0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1967444
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967445
    :cond_0
    const/4 v0, 0x0

    .line 1967446
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0}, LX/0D5;->getScrollX()I

    move-result v0

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1967438
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967439
    :cond_0
    const/4 v0, 0x0

    .line 1967440
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0}, LX/0D5;->getHorizontalScrollRange()I

    move-result v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1967435
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967436
    :cond_0
    const/4 v0, 0x0

    .line 1967437
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0}, LX/0D5;->getVerticalScrollRange()I

    move-result v0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1967432
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1967433
    :cond_0
    const/4 v0, 0x0

    .line 1967434
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    invoke-virtual {v0}, LX/0D5;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 1967431
    const/4 v0, 0x0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1967430
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->l:Lcom/facebook/browser/lite/BrowserLiteFragment;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/BrowserLiteFragment;->d()LX/0D5;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1967424
    invoke-virtual {p0, v0}, LX/D7h;->a(I)V

    .line 1967425
    invoke-virtual {p0, v0}, LX/D7h;->b(I)V

    .line 1967426
    invoke-virtual {p0, v1}, LX/D7h;->a(F)V

    .line 1967427
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->n:LX/D8S;

    if-nez v0, :cond_0

    .line 1967428
    :goto_0
    return-void

    .line 1967429
    :cond_0
    iget-object v0, p0, LX/D7h;->a:LX/2my;

    iget-object v0, v0, LX/2my;->n:LX/D8S;

    invoke-virtual {v0, v1}, LX/D8S;->a(F)Z

    goto :goto_0
.end method
