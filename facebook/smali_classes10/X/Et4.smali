.class public final LX/Et4;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Et6;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Et5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Et6",
            "<TE;>.UnifiedInProductBrandingAttachmentHeaderComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Et6;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Et6;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2177083
    iput-object p1, p0, LX/Et4;->b:LX/Et6;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2177084
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Et4;->c:[Ljava/lang/String;

    .line 2177085
    iput v3, p0, LX/Et4;->d:I

    .line 2177086
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Et4;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Et4;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Et4;LX/1De;IILX/Et5;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/Et6",
            "<TE;>.UnifiedInProductBrandingAttachmentHeaderComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2177087
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2177088
    iput-object p4, p0, LX/Et4;->a:LX/Et5;

    .line 2177089
    iget-object v0, p0, LX/Et4;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2177090
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2177091
    invoke-super {p0}, LX/1X5;->a()V

    .line 2177092
    const/4 v0, 0x0

    iput-object v0, p0, LX/Et4;->a:LX/Et5;

    .line 2177093
    iget-object v0, p0, LX/Et4;->b:LX/Et6;

    iget-object v0, v0, LX/Et6;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2177094
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Et6;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2177095
    iget-object v1, p0, LX/Et4;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Et4;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Et4;->d:I

    if-ge v1, v2, :cond_2

    .line 2177096
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2177097
    :goto_0
    iget v2, p0, LX/Et4;->d:I

    if-ge v0, v2, :cond_1

    .line 2177098
    iget-object v2, p0, LX/Et4;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2177099
    iget-object v2, p0, LX/Et4;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2177100
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2177101
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2177102
    :cond_2
    iget-object v0, p0, LX/Et4;->a:LX/Et5;

    .line 2177103
    invoke-virtual {p0}, LX/Et4;->a()V

    .line 2177104
    return-object v0
.end method
