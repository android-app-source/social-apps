.class public final LX/Dlu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:D

.field public final synthetic c:D

.field public final synthetic d:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;Ljava/lang/String;DD)V
    .locals 1

    .prologue
    .line 2038773
    iput-object p1, p0, LX/Dlu;->d:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iput-object p2, p0, LX/Dlu;->a:Ljava/lang/String;

    iput-wide p3, p0, LX/Dlu;->b:D

    iput-wide p5, p0, LX/Dlu;->c:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2038774
    iget-object v0, p0, LX/Dlu;->a:Ljava/lang/String;

    iget-wide v2, p0, LX/Dlu;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iget-wide v2, p0, LX/Dlu;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v1, v2, v4, v4}, LX/CK5;->a(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2038775
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2038776
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2038777
    iget-object v0, p0, LX/Dlu;->d:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Dlu;->d:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentRequestDetailAdapter;->c:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2038778
    const/4 v0, 0x1

    return v0
.end method
