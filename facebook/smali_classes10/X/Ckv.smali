.class public LX/Ckv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Cku;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/Ckw;

.field public final c:LX/0So;


# direct methods
.method public constructor <init>(LX/Ckw;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1931640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931641
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Ckv;->a:Ljava/util/Map;

    .line 1931642
    iput-object p1, p0, LX/Ckv;->b:LX/Ckw;

    .line 1931643
    iput-object p2, p0, LX/Ckv;->c:LX/0So;

    .line 1931644
    return-void
.end method

.method public static a(LX/0QB;)LX/Ckv;
    .locals 5

    .prologue
    .line 1931645
    const-class v1, LX/Ckv;

    monitor-enter v1

    .line 1931646
    :try_start_0
    sget-object v0, LX/Ckv;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1931647
    sput-object v2, LX/Ckv;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1931648
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931649
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1931650
    new-instance p0, LX/Ckv;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v3

    check-cast v3, LX/Ckw;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/Ckv;-><init>(LX/Ckw;LX/0So;)V

    .line 1931651
    move-object v0, p0

    .line 1931652
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1931653
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ckv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1931654
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1931655
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    const-wide/16 v8, 0x0

    .line 1931656
    iget-object v0, p0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1931657
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cku;

    .line 1931658
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1931659
    const-wide/16 v12, 0x0

    .line 1931660
    iget-wide v10, v1, LX/Cku;->b:J

    cmp-long v10, v10, v12

    if-lez v10, :cond_4

    iget-wide v10, v1, LX/Cku;->c:J

    cmp-long v10, v10, v12

    if-lez v10, :cond_4

    const/4 v10, 0x1

    :goto_1
    move v3, v10

    .line 1931661
    if-eqz v3, :cond_0

    .line 1931662
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1931663
    const-string v4, "block_id"

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931664
    const-string v0, "download_time"

    invoke-virtual {v1}, LX/Cku;->c()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931665
    const-string v0, "onscreen_time"

    invoke-virtual {v1}, LX/Cku;->d()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931666
    const-string v4, "did_see_content"

    iget-wide v6, v1, LX/Cku;->h:J

    cmp-long v0, v6, v8

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931667
    const-string v0, "onscreen_time"

    invoke-virtual {v1}, LX/Cku;->d()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931668
    const-string v0, "failures_occurred"

    iget-boolean v4, v1, LX/Cku;->a:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931669
    const-string v0, "started_loading_raw_time"

    iget-wide v4, v1, LX/Cku;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931670
    const-string v0, "finished_loading_raw_time"

    iget-wide v4, v1, LX/Cku;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931671
    const-string v0, "download_start_timestamp"

    iget-wide v4, v1, LX/Cku;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931672
    const-string v0, "finished_downloading_raw_time"

    iget-wide v4, v1, LX/Cku;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931673
    const-string v0, "onscreen_raw_time"

    iget-wide v4, v1, LX/Cku;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931674
    const-string v0, "offscreen_raw_time"

    iget-wide v4, v1, LX/Cku;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931675
    iget-wide v4, v1, LX/Cku;->h:J

    cmp-long v0, v4, v8

    if-lez v0, :cond_1

    .line 1931676
    const-string v0, "first_frame_render_time"

    iget-wide v4, v1, LX/Cku;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931677
    :cond_1
    iget-object v0, p0, LX/Ckv;->b:LX/Ckw;

    const-string v1, "android_native_article_native_ad_perf"

    invoke-virtual {v0, v1, v3}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 1931678
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 1931679
    :cond_3
    iget-object v0, p0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1931680
    return-void

    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1931681
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1931682
    :goto_0
    return-void

    .line 1931683
    :cond_0
    new-instance v0, LX/Cku;

    invoke-direct {v0}, LX/Cku;-><init>()V

    .line 1931684
    iget-object v1, p0, LX/Ckv;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
