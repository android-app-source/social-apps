.class public final LX/CmG;
.super LX/Cm8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cm8",
        "<",
        "LX/Clw;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/8Yr;

.field public final b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field public c:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field private d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field public g:Ljava/lang/String;


# direct methods
.method private constructor <init>(ILX/8Yr;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)V
    .locals 0

    .prologue
    .line 1933275
    invoke-direct {p0, p1}, LX/Cm8;-><init>(I)V

    .line 1933276
    iput-object p2, p0, LX/CmG;->a:LX/8Yr;

    .line 1933277
    iput-object p3, p0, LX/CmG;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1933278
    return-void
.end method

.method public constructor <init>(LX/8Yr;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8Yr;",
            "Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalSphericalPhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1933272
    invoke-static {p1, p3}, LX/CmG;->a(LX/8Yr;LX/0Ot;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1b

    :goto_0
    move v0, v0

    .line 1933273
    invoke-direct {p0, v0, p1, p2}, LX/CmG;-><init>(ILX/8Yr;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)V

    .line 1933274
    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static a(LX/8Yr;LX/0Ot;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8Yr;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalSphericalPhoto;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1933258
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/8Yr;->k()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1933259
    :goto_0
    return v0

    .line 1933260
    :cond_1
    if-eqz p1, :cond_3

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K29;

    invoke-virtual {v0}, LX/K29;->a()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 1933261
    goto :goto_0

    .line 1933262
    :cond_3
    invoke-interface {p0}, LX/8Yr;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;

    .line 1933263
    const-string v5, "equirectangular"

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "cubestrip"

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBPhotoEncodingsModel$PhotoEncodingsModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1933264
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 1933265
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_6
    move v0, v1

    .line 1933266
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/Cm7;
    .locals 1

    .prologue
    .line 1933270
    iput-object p1, p0, LX/CmG;->d:Ljava/lang/String;

    .line 1933271
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1933269
    iget-object v0, p0, LX/CmG;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic b()LX/Clr;
    .locals 1

    .prologue
    .line 1933268
    invoke-virtual {p0}, LX/CmG;->c()LX/Clw;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/Clw;
    .locals 2

    .prologue
    .line 1933267
    new-instance v0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;-><init>(LX/CmG;)V

    return-object v0
.end method
