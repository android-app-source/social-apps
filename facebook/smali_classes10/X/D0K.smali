.class public LX/D0K;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D0G;


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V
    .locals 0

    .prologue
    .line 1955023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1955024
    iput-object p1, p0, LX/D0K;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955025
    return-void
.end method


# virtual methods
.method public final a(LX/D0G;)Z
    .locals 1

    .prologue
    .line 1955028
    iget-object v0, p0, LX/D0K;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-interface {p1, v0}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955026
    check-cast p1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955027
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/D0K;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
