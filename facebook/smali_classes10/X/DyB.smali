.class public final LX/DyB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V
    .locals 0

    .prologue
    .line 2064027
    iput-object p1, p0, LX/DyB;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 9

    .prologue
    .line 2064028
    iget-object v0, p0, LX/DyB;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/DyB;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getLastVisiblePosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    iget-object v1, p0, LX/DyB;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    iget-object v1, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    invoke-virtual {v1}, LX/9bF;->getCount()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2064029
    iget-object v0, p0, LX/DyB;->a:Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    .line 2064030
    iget-object v2, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->t:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9bq;

    iget-object v3, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xfa

    sget-object v5, LX/0zS;->d:LX/0zS;

    iget-object v6, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->o:Ljava/lang/String;

    const/16 v7, 0x14

    invoke-virtual/range {v2 .. v7}, LX/9bq;->a(Ljava/lang/String;ILX/0zS;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2064031
    new-instance v4, LX/DyH;

    invoke-direct {v4, v0}, LX/DyH;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    .line 2064032
    iget-object v2, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ck;

    const-string v5, "fetchMoreAlbumsList_%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/DyI;

    invoke-direct {v6, v0, v3}, LX/DyI;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v2, v5, v6, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2064033
    :cond_0
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2064026
    return-void
.end method
