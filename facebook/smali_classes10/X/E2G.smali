.class public LX/E2G;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/3U9;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/E2G;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E2H;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/E2G",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E2H;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2072528
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2072529
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/E2G;->b:LX/0Zi;

    .line 2072530
    iput-object p1, p0, LX/E2G;->a:LX/0Ot;

    .line 2072531
    return-void
.end method

.method public static a(LX/0QB;)LX/E2G;
    .locals 4

    .prologue
    .line 2072532
    sget-object v0, LX/E2G;->c:LX/E2G;

    if-nez v0, :cond_1

    .line 2072533
    const-class v1, LX/E2G;

    monitor-enter v1

    .line 2072534
    :try_start_0
    sget-object v0, LX/E2G;->c:LX/E2G;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2072535
    if-eqz v2, :cond_0

    .line 2072536
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2072537
    new-instance v3, LX/E2G;

    const/16 p0, 0x30bb

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/E2G;-><init>(LX/0Ot;)V

    .line 2072538
    move-object v0, v3

    .line 2072539
    sput-object v0, LX/E2G;->c:LX/E2G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2072540
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2072541
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2072542
    :cond_1
    sget-object v0, LX/E2G;->c:LX/E2G;

    return-object v0

    .line 2072543
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2072544
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2072545
    check-cast p2, LX/E2F;

    .line 2072546
    iget-object v0, p0, LX/E2G;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2H;

    iget-object v1, p2, LX/E2F;->a:LX/2km;

    iget-object v2, p2, LX/E2F;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p2, LX/E2F;->c:LX/0Px;

    const/4 v5, 0x4

    const/high16 v9, 0x3f800000    # 1.0f

    .line 2072547
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v9}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v6

    .line 2072548
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2072549
    iget-object v8, v0, LX/E2H;->a:LX/E1s;

    .line 2072550
    iget-object v10, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v10, v10

    .line 2072551
    invoke-interface {v10}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object p0

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->TOGGLE_STATE:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-ne p0, p2, :cond_1

    .line 2072552
    invoke-static {v8, p1, v1, v10, v2}, LX/E1s;->a(LX/E1s;LX/1De;LX/2km;LX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/E2I;

    move-result-object v10

    .line 2072553
    :goto_1
    move-object v4, v10

    .line 2072554
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v9}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2072555
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2072556
    :cond_0
    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2072557
    return-object v0

    :cond_1
    invoke-static {v8, p1, v4, v1}, LX/E1s;->c(LX/E1s;LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/E4Z;

    move-result-object v10

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2072558
    invoke-static {}, LX/1dS;->b()V

    .line 2072559
    const/4 v0, 0x0

    return-object v0
.end method
