.class public final LX/Dba;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;

.field public final synthetic b:LX/Dbi;

.field public final synthetic c:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;LX/Dbi;)V
    .locals 0

    .prologue
    .line 2017253
    iput-object p1, p0, LX/Dba;->c:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    iput-object p2, p0, LX/Dba;->a:Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;

    iput-object p3, p0, LX/Dba;->b:LX/Dbi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0xb38ca1b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2017254
    iget-object v1, p0, LX/Dba;->c:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    iget-object v1, v1, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->a:LX/DbM;

    iget-object v2, p0, LX/Dba;->c:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    iget-object v2, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->c:Ljava/lang/String;

    iget-object v3, p0, LX/Dba;->a:Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;

    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2017255
    const-string v4, "photo_menu_viewer"

    const-string p1, "menu_photo_tap"

    invoke-static {v4, p1, v2}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2017256
    const-string p1, "photo_id"

    invoke-virtual {v4, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2017257
    iget-object p1, v1, LX/DbM;->a:LX/0Zb;

    invoke-interface {p1, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2017258
    iget-object v1, p0, LX/Dba;->c:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    iget-object v1, v1, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->c:Ljava/lang/String;

    .line 2017259
    new-instance v2, LX/9hE;

    const-class v3, LX/23f;

    new-instance v4, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {v4, v1}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v3

    invoke-direct {v2, v3}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 2017260
    const/4 v3, 0x0

    .line 2017261
    iput-boolean v3, v2, LX/9hD;->n:Z

    .line 2017262
    move-object v3, v2

    .line 2017263
    const/4 v4, 0x1

    .line 2017264
    iput-boolean v4, v3, LX/9hD;->m:Z

    .line 2017265
    move-object v1, v2

    .line 2017266
    sget-object v2, LX/74S;->PAGE_PHOTO_MENUS:LX/74S;

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v1

    iget-object v2, p0, LX/Dba;->a:Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;

    invoke-virtual {v2}, Lcom/facebook/localcontent/protocol/graphql/FetchPhotoMenusGraphQLModels$PhotoMenusDataModel$PagePhotoMenusModel$NodesModel$PagePhotoMenuPhotosModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v1

    invoke-virtual {v1}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 2017267
    iget-object v2, p0, LX/Dba;->c:Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;

    iget-object v2, v2, Lcom/facebook/localcontent/menus/PagePhotoMenuAdapter;->b:LX/23R;

    iget-object v3, p0, LX/Dba;->b:LX/Dbi;

    invoke-virtual {v3}, LX/Dbi;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v1, v4}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2017268
    const v1, 0x1dd39b33

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
