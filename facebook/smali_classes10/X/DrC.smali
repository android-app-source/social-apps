.class public final LX/DrC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;)V
    .locals 0

    .prologue
    .line 2049081
    iput-object p1, p0, LX/DrC;->a:Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2049082
    iget-object v0, p0, LX/DrC;->a:Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;

    iget-object v1, v0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;->c:LX/DrF;

    iget-object v0, p0, LX/DrC;->a:Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;

    iget-boolean v0, v0, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;->a:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0zS;->d:LX/0zS;

    .line 2049083
    :goto_0
    new-instance v2, LX/BAh;

    invoke-direct {v2}, LX/BAh;-><init>()V

    move-object v2, v2

    .line 2049084
    const-string v3, "supported_buckets"

    sget-object p0, LX/DrG;->a:LX/0Px;

    invoke-virtual {v2, v3, p0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2049085
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2049086
    iget-object v3, v1, LX/DrF;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2049087
    new-instance v3, LX/DrE;

    invoke-direct {v3, v1}, LX/DrE;-><init>(LX/DrF;)V

    iget-object p0, v1, LX/DrF;->c:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2049088
    return-object v0

    :cond_0
    sget-object v0, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method
