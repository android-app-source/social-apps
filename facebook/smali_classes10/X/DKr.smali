.class public final enum LX/DKr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DKr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DKr;

.field public static final enum FILE_OR_DOC_ROW:LX/DKr;

.field public static final enum LOADING_BAR:LX/DKr;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1987569
    new-instance v0, LX/DKr;

    const-string v1, "FILE_OR_DOC_ROW"

    invoke-direct {v0, v1, v2}, LX/DKr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DKr;->FILE_OR_DOC_ROW:LX/DKr;

    .line 1987570
    new-instance v0, LX/DKr;

    const-string v1, "LOADING_BAR"

    invoke-direct {v0, v1, v3}, LX/DKr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/DKr;->LOADING_BAR:LX/DKr;

    .line 1987571
    const/4 v0, 0x2

    new-array v0, v0, [LX/DKr;

    sget-object v1, LX/DKr;->FILE_OR_DOC_ROW:LX/DKr;

    aput-object v1, v0, v2

    sget-object v1, LX/DKr;->LOADING_BAR:LX/DKr;

    aput-object v1, v0, v3

    sput-object v0, LX/DKr;->$VALUES:[LX/DKr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1987568
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DKr;
    .locals 1

    .prologue
    .line 1987573
    const-class v0, LX/DKr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DKr;

    return-object v0
.end method

.method public static values()[LX/DKr;
    .locals 1

    .prologue
    .line 1987572
    sget-object v0, LX/DKr;->$VALUES:[LX/DKr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DKr;

    return-object v0
.end method
