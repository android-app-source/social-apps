.class public LX/EQC;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2118352
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/EQC;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2118353
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2118354
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/EQC;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2118355
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2118356
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2118357
    const v0, 0x7f030485

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2118358
    const v0, 0x7f0d0d71

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/EQC;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2118359
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2118360
    iget-object v0, p0, LX/EQC;->a:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2118361
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2118362
    iget-object v0, p0, LX/EQC;->a:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2118363
    return-void
.end method
