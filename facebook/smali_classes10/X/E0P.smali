.class public final enum LX/E0P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E0P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E0P;

.field public static final enum CLOSED:LX/E0P;

.field public static final enum PHOTO:LX/E0P;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2068265
    new-instance v0, LX/E0P;

    const-string v1, "CLOSED"

    invoke-direct {v0, v1, v2}, LX/E0P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0P;->CLOSED:LX/E0P;

    .line 2068266
    new-instance v0, LX/E0P;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v3}, LX/E0P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/E0P;->PHOTO:LX/E0P;

    .line 2068267
    const/4 v0, 0x2

    new-array v0, v0, [LX/E0P;

    sget-object v1, LX/E0P;->CLOSED:LX/E0P;

    aput-object v1, v0, v2

    sget-object v1, LX/E0P;->PHOTO:LX/E0P;

    aput-object v1, v0, v3

    sput-object v0, LX/E0P;->$VALUES:[LX/E0P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2068264
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/E0P;
    .locals 1

    .prologue
    .line 2068263
    const-class v0, LX/E0P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E0P;

    return-object v0
.end method

.method public static values()[LX/E0P;
    .locals 1

    .prologue
    .line 2068262
    sget-object v0, LX/E0P;->$VALUES:[LX/E0P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E0P;

    return-object v0
.end method
