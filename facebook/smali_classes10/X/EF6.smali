.class public final LX/EF6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;)V
    .locals 0

    .prologue
    .line 2094844
    iput-object p1, p0, LX/EF6;->a:Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .prologue
    const/16 v1, 0x12c

    .line 2094845
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 2094846
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-interface {p1, v1, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 2094847
    :cond_0
    iget-object v0, p0, LX/EF6;->a:Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;

    iget-object v0, v0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->m:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2094848
    iget-object v2, p0, LX/EF6;->a:Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;

    iget-wide v2, v2, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->t:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 2094849
    iget-object v2, p0, LX/EF6;->a:Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;

    iget-object v2, v2, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    const-wide/32 v4, 0x2bf20

    invoke-interface {v2, v4, v5}, LX/ECA;->a(J)V

    .line 2094850
    iget-object v2, p0, LX/EF6;->a:Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;

    .line 2094851
    iput-wide v0, v2, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->t:J

    .line 2094852
    :cond_1
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2094853
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2094854
    return-void
.end method
