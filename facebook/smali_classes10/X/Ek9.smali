.class public final LX/Ek9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/EkA;


# direct methods
.method public constructor <init>(LX/EkA;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2163087
    iput-object p1, p0, LX/Ek9;->d:LX/EkA;

    iput-object p2, p0, LX/Ek9;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Ek9;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Ek9;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2163076
    instance-of v1, p1, LX/4Ua;

    if-eqz v1, :cond_0

    .line 2163077
    check-cast p1, LX/4Ua;

    .line 2163078
    iget-object v1, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 2163079
    if-nez v1, :cond_2

    .line 2163080
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2163081
    iget-object v0, p0, LX/Ek9;->d:LX/EkA;

    iget-object v0, v0, LX/EkA;->b:Landroid/content/Context;

    const v1, 0x7f080039

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2163082
    :cond_1
    iget-object v1, p0, LX/Ek9;->d:LX/EkA;

    iget-object v1, v1, LX/EkA;->j:LX/EkI;

    invoke-virtual {v1, v0}, LX/EkI;->setupErrorText(Ljava/lang/String;)V

    .line 2163083
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "error code"

    invoke-virtual {v1, v2, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "phone number"

    iget-object v2, p0, LX/Ek9;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2163084
    iget-object v1, p0, LX/Ek9;->d:LX/EkA;

    iget-object v1, v1, LX/EkA;->k:LX/Ek6;

    sget-object v2, LX/Ek7;->INVALID_NUMBER:LX/Ek7;

    const-string v3, "qp"

    invoke-virtual {v1, v2, v3, v0}, LX/Ek6;->a(LX/Ek7;Ljava/lang/String;LX/1rQ;)V

    .line 2163085
    return-void

    .line 2163086
    :cond_2
    iget-object v0, v1, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2163062
    const/4 v4, 0x0

    .line 2163063
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "phone number"

    iget-object v2, p0, LX/Ek9;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2163064
    iget-object v1, p0, LX/Ek9;->d:LX/EkA;

    iget-object v1, v1, LX/EkA;->k:LX/Ek6;

    sget-object v2, LX/Ek7;->VALID_NUMBER:LX/Ek7;

    const-string v3, "qp"

    invoke-virtual {v1, v2, v3, v0}, LX/Ek6;->a(LX/Ek7;Ljava/lang/String;LX/1rQ;)V

    .line 2163065
    const-string v0, "confirmAccount/?normalized_contactpoint=%s&contactpoint_type=%s&conf_surface=%s&iso_country_code=%s&for_phone_number_confirmation=%s&quick_promotion_id=%s&quick_promotion_type=%s"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Ek9;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "PHONE"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "pna"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, LX/Ek9;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "true"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, LX/Ek9;->d:LX/EkA;

    iget-object v3, v3, LX/EkA;->i:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, LX/Ek9;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2163066
    iget-object v1, p0, LX/Ek9;->d:LX/EkA;

    iget-object v1, v1, LX/EkA;->d:LX/17W;

    iget-object v2, p0, LX/Ek9;->d:LX/EkA;

    iget-object v2, v2, LX/EkA;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2163067
    iget-object v0, p0, LX/Ek9;->d:LX/EkA;

    iget-object v0, v0, LX/EkA;->h:LX/Ejy;

    .line 2163068
    iget-object v1, v0, LX/Ejy;->a:LX/0Uh;

    const/16 v2, 0x425

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2163069
    if-eqz v0, :cond_0

    .line 2163070
    iget-object v0, p0, LX/Ek9;->d:LX/EkA;

    iget-object v0, v0, LX/EkA;->k:LX/Ek6;

    sget-object v1, LX/Ek7;->BACKGROUND_CONFIRM_START:LX/Ek7;

    invoke-virtual {v0, v1, v4, v4}, LX/Ek6;->a(LX/Ek7;Ljava/lang/String;LX/1rQ;)V

    .line 2163071
    iget-object v0, p0, LX/Ek9;->d:LX/EkA;

    iget-object v0, v0, LX/EkA;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U8;

    iget-object v1, p0, LX/Ek9;->a:Ljava/lang/String;

    iget-object v2, p0, LX/Ek9;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2U8;->a(Lcom/facebook/growth/model/Contactpoint;)Z

    .line 2163072
    :cond_0
    iget-object v0, p0, LX/Ek9;->d:LX/EkA;

    iget-object v0, v0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->b()V

    .line 2163073
    iget-object v0, p0, LX/Ek9;->d:LX/EkA;

    iget-object v0, v0, LX/76U;->a:LX/78A;

    invoke-virtual {v0}, LX/78A;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2163074
    iget-object v0, p0, LX/Ek9;->d:LX/EkA;

    invoke-virtual {v0}, LX/76U;->f()V

    .line 2163075
    :cond_1
    return-void
.end method
