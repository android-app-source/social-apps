.class public final LX/EDZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;)V
    .locals 0

    .prologue
    .line 2091447
    iput-object p1, p0, LX/EDZ;->a:LX/EDx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    const/16 v1, 0x26

    const v2, -0x47cc272e

    invoke-static {v3, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2091448
    iget-object v2, p0, LX/EDZ;->a:LX/EDx;

    invoke-virtual {v2}, LX/EDx;->aa()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2091449
    const/16 v0, 0x27

    const v2, 0x7b4711c6

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2091450
    :goto_0
    return-void

    .line 2091451
    :cond_0
    iget-object v2, p0, LX/EDZ;->a:LX/EDx;

    const-string v3, "state"

    invoke-virtual {p2, v3, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v0, 0x1

    .line 2091452
    :cond_1
    iput-boolean v0, v2, LX/EDx;->be:Z

    .line 2091453
    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    iget-boolean v0, v0, LX/EDx;->be:Z

    if-eqz v0, :cond_5

    .line 2091454
    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    iget-object v2, p0, LX/EDZ;->a:LX/EDx;

    iget-boolean v2, v2, LX/EDx;->bf:Z

    .line 2091455
    iput-boolean v2, v0, LX/EDx;->bd:Z

    .line 2091456
    :cond_2
    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    sget-object v2, LX/EDq;->EARPIECE:LX/EDq;

    invoke-virtual {v0, v2}, LX/EDx;->a(LX/EDq;)V

    .line 2091457
    :goto_1
    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    invoke-static {v0}, LX/EDx;->cr(LX/EDx;)V

    .line 2091458
    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    iget-object v0, v0, LX/EDx;->n:LX/34I;

    invoke-virtual {v0}, LX/34I;->c()LX/7TP;

    move-result-object v0

    .line 2091459
    sget-object v2, LX/7TP;->AudioOutputRouteEarpiece:LX/7TP;

    if-ne v0, v2, :cond_3

    iget-object v2, p0, LX/EDZ;->a:LX/EDx;

    iget-boolean v2, v2, LX/EDx;->be:Z

    if-eqz v2, :cond_3

    .line 2091460
    sget-object v0, LX/7TP;->AudioOutputRouteHeadset:LX/7TP;

    .line 2091461
    :cond_3
    iget-object v2, p0, LX/EDZ;->a:LX/EDx;

    iget-object v2, v2, LX/EDx;->T:LX/2Tm;

    if-eqz v2, :cond_4

    .line 2091462
    iget-object v2, p0, LX/EDZ;->a:LX/EDx;

    iget-object v2, v2, LX/EDx;->T:LX/2Tm;

    invoke-virtual {v2, v0}, LX/2Tm;->a(LX/7TP;)V

    .line 2091463
    :cond_4
    const v0, -0x7012ba5

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0

    .line 2091464
    :cond_5
    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aI()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2091465
    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    sget-object v2, LX/EDq;->BLUETOOTH:LX/EDq;

    invoke-virtual {v0, v2}, LX/EDx;->a(LX/EDq;)V

    goto :goto_1

    .line 2091466
    :cond_6
    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    iget-boolean v0, v0, LX/EDx;->bd:Z

    if-nez v0, :cond_7

    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    iget-boolean v0, v0, LX/EDx;->br:Z

    if-eqz v0, :cond_2

    .line 2091467
    :cond_7
    iget-object v0, p0, LX/EDZ;->a:LX/EDx;

    sget-object v2, LX/EDq;->SPEAKERPHONE:LX/EDq;

    invoke-virtual {v0, v2}, LX/EDx;->a(LX/EDq;)V

    goto :goto_1
.end method
