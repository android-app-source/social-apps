.class public LX/EFW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0wM;

.field private final c:Landroid/content/res/Resources;

.field public final d:I

.field public e:I

.field private f:I

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Landroid/graphics/drawable/Drawable;

.field public i:Landroid/graphics/drawable/Drawable;

.field public j:Landroid/graphics/drawable/Drawable;

.field public k:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0wM;Landroid/content/res/Resources;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2095965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2095966
    iput-object p1, p0, LX/EFW;->a:Landroid/content/Context;

    .line 2095967
    iput-object p2, p0, LX/EFW;->b:LX/0wM;

    .line 2095968
    iput-object p3, p0, LX/EFW;->c:Landroid/content/res/Resources;

    .line 2095969
    const v0, 0x7f010053

    iget-object v1, p0, LX/EFW;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a02f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {p1, v0, v1}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, LX/EFW;->e:I

    .line 2095970
    iget-object v0, p0, LX/EFW;->a:Landroid/content/Context;

    const v1, 0x7f0a0305

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/EFW;->d:I

    .line 2095971
    iget-object v0, p0, LX/EFW;->a:Landroid/content/Context;

    const v1, 0x7f0a018d

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/EFW;->f:I

    .line 2095972
    return-void
.end method

.method public static b(LX/0QB;)LX/EFW;
    .locals 4

    .prologue
    .line 2095950
    new-instance v3, LX/EFW;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {v3, v0, v1, v2}, LX/EFW;-><init>(Landroid/content/Context;LX/0wM;Landroid/content/res/Resources;)V

    .line 2095951
    return-object v3
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 2095959
    iget-object v0, p0, LX/EFW;->g:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 2095960
    iget-object v0, p0, LX/EFW;->b:LX/0wM;

    .line 2095961
    const v1, 0x7f021aba

    move v1, v1

    .line 2095962
    iget v2, p0, LX/EFW;->e:I

    move v2, v2

    .line 2095963
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/EFW;->g:Landroid/graphics/drawable/Drawable;

    .line 2095964
    :cond_0
    iget-object v0, p0, LX/EFW;->g:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final a(Z)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 2095973
    if-eqz p1, :cond_1

    .line 2095974
    iget-object v0, p0, LX/EFW;->j:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 2095975
    iget-object v0, p0, LX/EFW;->b:LX/0wM;

    const v1, 0x7f020cee

    .line 2095976
    iget v2, p0, LX/EFW;->e:I

    move v2, v2

    .line 2095977
    const/4 p1, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/EFW;->j:Landroid/graphics/drawable/Drawable;

    .line 2095978
    :cond_0
    iget-object v0, p0, LX/EFW;->j:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 2095979
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LX/EFW;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Z)Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 2095952
    if-eqz p1, :cond_1

    .line 2095953
    iget-object v0, p0, LX/EFW;->k:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 2095954
    iget-object v0, p0, LX/EFW;->b:LX/0wM;

    const v1, 0x7f020d47

    .line 2095955
    iget v2, p0, LX/EFW;->e:I

    move v2, v2

    .line 2095956
    const/4 p1, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/EFW;->k:Landroid/graphics/drawable/Drawable;

    .line 2095957
    :cond_0
    iget-object v0, p0, LX/EFW;->k:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 2095958
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LX/EFW;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 2095944
    iget-object v0, p0, LX/EFW;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 2095945
    iget-object v0, p0, LX/EFW;->b:LX/0wM;

    .line 2095946
    const v1, 0x7f021ad3

    move v1, v1

    .line 2095947
    iget v2, p0, LX/EFW;->e:I

    move v2, v2

    .line 2095948
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/EFW;->h:Landroid/graphics/drawable/Drawable;

    .line 2095949
    :cond_0
    iget-object v0, p0, LX/EFW;->h:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method
