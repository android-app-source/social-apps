.class public LX/EwG;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/2ht;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23P;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/Euk;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3UJ;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/Exs;

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1mR;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EuP;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0ad;

.field private final i:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ewe;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/Ewe;

.field private l:Z

.field public m:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

.field private n:I

.field public o:I

.field public p:Landroid/content/res/Resources;

.field public q:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

.field public r:J

.field public s:Z

.field public t:LX/5P1;

.field public u:LX/Ew0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/2dl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;LX/Exs;LX/0ad;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2182969
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2182970
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2182971
    iput-object v0, p0, LX/EwG;->d:LX/0Ot;

    .line 2182972
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2182973
    iput-object v0, p0, LX/EwG;->f:LX/0Ot;

    .line 2182974
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2182975
    iput-object v0, p0, LX/EwG;->g:LX/0Ot;

    .line 2182976
    iput-boolean v1, p0, LX/EwG;->s:Z

    .line 2182977
    sget-object v0, LX/5P1;->NONE:LX/5P1;

    iput-object v0, p0, LX/EwG;->t:LX/5P1;

    .line 2182978
    const v0, 0x7f0a00d5

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/EwG;->i:I

    .line 2182979
    iput-object p2, p0, LX/EwG;->e:LX/Exs;

    .line 2182980
    iput-object p3, p0, LX/EwG;->h:LX/0ad;

    .line 2182981
    iput-object p1, p0, LX/EwG;->p:Landroid/content/res/Resources;

    .line 2182982
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EwG;->j:Ljava/util/List;

    .line 2182983
    new-instance v0, LX/Ewr;

    invoke-direct {v0}, LX/Ewr;-><init>()V

    iput-object v0, p0, LX/EwG;->k:LX/Ewe;

    .line 2182984
    iput v1, p0, LX/EwG;->n:I

    .line 2182985
    iput v1, p0, LX/EwG;->o:I

    .line 2182986
    iget-object v0, p0, LX/EwG;->e:LX/Exs;

    const-string v1, "request"

    .line 2182987
    iput-object v1, v0, LX/Exs;->u:Ljava/lang/String;

    .line 2182988
    iget-object v0, p0, LX/EwG;->e:LX/Exs;

    new-instance v1, LX/Ew7;

    invoke-direct {v1, p0}, LX/Ew7;-><init>(LX/EwG;)V

    .line 2182989
    iput-object v1, v0, LX/Exs;->r:LX/Ew6;

    .line 2182990
    return-void
.end method

.method public static a(LX/0QB;)LX/EwG;
    .locals 1

    .prologue
    .line 2182991
    invoke-static {p0}, LX/EwG;->b(LX/0QB;)LX/EwG;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2182992
    if-nez p1, :cond_1

    .line 2182993
    invoke-static {p2}, LX/EwG;->a(Landroid/view/ViewGroup;)Landroid/widget/TextView;

    move-result-object v0

    .line 2182994
    :goto_0
    check-cast v0, Landroid/widget/TextView;

    .line 2182995
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 2182996
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2182997
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2182998
    iget v1, p0, LX/EwG;->n:I

    if-nez v1, :cond_0

    .line 2182999
    const/4 p3, 0x0

    .line 2183000
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 p1, -0x1

    const/4 p2, -0x2

    invoke-direct {v1, p1, p2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2183001
    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->measure(II)V

    .line 2183002
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v1, v1

    .line 2183003
    iput v1, p0, LX/EwG;->n:I

    .line 2183004
    :cond_0
    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method private static a(Landroid/view/ViewGroup;)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 2183005
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306db

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public static a$redex0(LX/EwG;LX/Ewi;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)LX/84H;
    .locals 1

    .prologue
    .line 2183006
    new-instance v0, LX/EwF;

    invoke-direct {v0, p0, p1, p2}, LX/EwF;-><init>(LX/EwG;LX/Ewi;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    return-object v0
.end method

.method public static b(LX/0QB;)LX/EwG;
    .locals 10

    .prologue
    .line 2183007
    new-instance v0, LX/EwG;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/Exs;->b(LX/0QB;)LX/Exs;

    move-result-object v2

    check-cast v2, LX/Exs;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v0, v1, v2, v3}, LX/EwG;-><init>(Landroid/content/res/Resources;LX/Exs;LX/0ad;)V

    .line 2183008
    const/16 v1, 0x5cb

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0xa77

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xb0b

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2232

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/Euk;->a(LX/0QB;)LX/Euk;

    move-result-object v5

    check-cast v5, LX/Euk;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    .line 2183009
    new-instance v9, LX/Ew0;

    invoke-direct {v9}, LX/Ew0;-><init>()V

    .line 2183010
    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    .line 2183011
    iput-object v7, v9, LX/Ew0;->a:Landroid/content/res/Resources;

    iput-object v8, v9, LX/Ew0;->f:LX/0ad;

    .line 2183012
    move-object v7, v9

    .line 2183013
    check-cast v7, LX/Ew0;

    invoke-static {p0}, LX/2dl;->a(LX/0QB;)LX/2dl;

    move-result-object v8

    check-cast v8, LX/2dl;

    .line 2183014
    iput-object v1, v0, LX/EwG;->a:LX/0Or;

    iput-object v2, v0, LX/EwG;->d:LX/0Ot;

    iput-object v3, v0, LX/EwG;->f:LX/0Ot;

    iput-object v4, v0, LX/EwG;->g:LX/0Ot;

    iput-object v5, v0, LX/EwG;->b:LX/Euk;

    iput-object v6, v0, LX/EwG;->c:LX/0SG;

    iput-object v7, v0, LX/EwG;->u:LX/Ew0;

    iput-object v8, v0, LX/EwG;->v:LX/2dl;

    .line 2183015
    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2183019
    iget-object v0, p0, LX/EwG;->h:LX/0ad;

    sget-short v1, LX/2ez;->p:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2183020
    iget-object v0, p0, LX/EwG;->v:LX/2dl;

    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2dl;->a(Ljava/util/Collection;)V

    .line 2183021
    :goto_0
    return-void

    .line 2183022
    :cond_0
    iget-object v0, p0, LX/EwG;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method private static c(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendRequestItemView;
    .locals 2

    .prologue
    .line 2183016
    new-instance v0, Lcom/facebook/friending/common/list/FriendRequestItemView;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friending/common/list/FriendRequestItemView;-><init>(Landroid/content/Context;)V

    .line 2183017
    sget-object v1, LX/6VF;->XLARGE:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2183018
    return-object v0
.end method

.method private static d(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendListItemView;
    .locals 2

    .prologue
    .line 2183039
    new-instance v0, Lcom/facebook/friending/common/list/FriendListItemView;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;-><init>(Landroid/content/Context;)V

    .line 2183040
    sget-object v1, LX/6VF;->XLARGE:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2183041
    return-object v0
.end method

.method public static e(LX/EwG;)V
    .locals 1

    .prologue
    .line 2183034
    const-string v0, "FC_REQUESTS_QUERY"

    invoke-direct {p0, v0}, LX/EwG;->b(Ljava/lang/String;)V

    .line 2183035
    return-void
.end method

.method public static f$redex0(LX/EwG;)V
    .locals 1

    .prologue
    .line 2183036
    const-string v0, "FC_FRIENDS_QUERY"

    invoke-direct {p0, v0}, LX/EwG;->b(Ljava/lang/String;)V

    .line 2183037
    iget-object v0, p0, LX/EwG;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EuP;

    invoke-virtual {v0}, LX/EuP;->d()V

    .line 2183038
    return-void
.end method

.method public static g(LX/EwG;)V
    .locals 1

    .prologue
    .line 2183031
    const-string v0, "FC_SUGGESTIONS_QUERY"

    invoke-direct {p0, v0}, LX/EwG;->b(Ljava/lang/String;)V

    .line 2183032
    iget-object v0, p0, LX/EwG;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EuP;

    invoke-virtual {v0}, LX/EuP;->b()V

    .line 2183033
    return-void
.end method

.method public static i(LX/EwG;)V
    .locals 2

    .prologue
    .line 2183023
    iget-object v0, p0, LX/EwG;->m:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EwG;->q:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    if-eqz v0, :cond_0

    .line 2183024
    iget-object v0, p0, LX/EwG;->q:Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;

    .line 2183025
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->K:Z

    .line 2183026
    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->m$redex0(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183027
    iget-object v1, v0, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->p:LX/2hs;

    invoke-virtual {v1}, LX/2hs;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2183028
    invoke-static {v0}, Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;->y(Lcom/facebook/friending/center/tabs/requests/FriendsCenterRequestsFragment;)V

    .line 2183029
    iget-object v0, p0, LX/EwG;->m:Lcom/facebook/friending/center/FriendsCenterHomeFragment;

    invoke-virtual {v0}, Lcom/facebook/friending/center/FriendsCenterHomeFragment;->d()V

    .line 2183030
    :cond_0
    return-void
.end method

.method public static k$redex0(LX/EwG;)V
    .locals 6

    .prologue
    .line 2182959
    iget-boolean v0, p0, LX/EwG;->s:Z

    if-eqz v0, :cond_0

    .line 2182960
    iget-object v0, p0, LX/EwG;->b:LX/Euk;

    iget-wide v2, p0, LX/EwG;->r:J

    .line 2182961
    iget-object v1, v0, LX/Euk;->a:LX/0if;

    sget-object v4, LX/0ig;->P:LX/0ih;

    invoke-virtual {v1, v4, v2, v3}, LX/0if;->c(LX/0ih;J)V

    .line 2182962
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/EwG;->r:J

    .line 2182963
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EwG;->s:Z

    .line 2182964
    :cond_0
    return-void
.end method

.method public static l(LX/EwG;)V
    .locals 11

    .prologue
    .line 2182965
    iget-object v0, p0, LX/EwG;->b:LX/Euk;

    iget-wide v2, p0, LX/EwG;->r:J

    iget-object v1, p0, LX/EwG;->t:LX/5P1;

    invoke-virtual {v1}, LX/5P1;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2182966
    iget-object v4, v0, LX/Euk;->a:LX/0if;

    sget-object v5, LX/0ig;->P:LX/0ih;

    sget-object v6, LX/Euj;->DELETE_ALL_ACTION_TAKEN:LX/Euj;

    invoke-virtual {v6}, LX/Euj;->toString()Ljava/lang/String;

    move-result-object v8

    move-wide v6, v2

    move-object v9, v1

    invoke-virtual/range {v4 .. v9}, LX/0if;->a(LX/0ih;JLjava/lang/String;Ljava/lang/String;)V

    .line 2182967
    invoke-static {p0}, LX/EwG;->k$redex0(LX/EwG;)V

    .line 2182968
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 2182829
    sget-object v0, LX/Ew5;->a:[I

    invoke-static {}, LX/Ewp;->values()[LX/Ewp;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, LX/Ewp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2182830
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2182831
    :pswitch_0
    invoke-static {p2}, LX/EwG;->a(Landroid/view/ViewGroup;)Landroid/widget/TextView;

    move-result-object v0

    .line 2182832
    const v1, 0x7f080f70

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2182833
    :goto_0
    return-object v0

    .line 2182834
    :pswitch_1
    invoke-static {p2}, LX/EwG;->a(Landroid/view/ViewGroup;)Landroid/widget/TextView;

    move-result-object v0

    .line 2182835
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2182836
    const v1, 0x7f080f77

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 2182837
    :pswitch_2
    invoke-static {p2}, LX/EwG;->c(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendRequestItemView;

    move-result-object v0

    goto :goto_0

    .line 2182838
    :pswitch_3
    new-instance v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2182839
    sget-object v1, LX/6VF;->XLARGE:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2182840
    const v1, 0x7f080f8c

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 2182841
    move-object v0, v0

    .line 2182842
    goto :goto_0

    .line 2182843
    :pswitch_4
    invoke-static {p2}, LX/EwG;->d(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendListItemView;

    move-result-object v0

    goto :goto_0

    .line 2182844
    :pswitch_5
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03070a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 2182845
    goto :goto_0

    .line 2182846
    :pswitch_6
    invoke-static {p2}, LX/EwG;->c(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendRequestItemView;

    move-result-object v0

    goto :goto_0

    .line 2182847
    :pswitch_7
    invoke-static {p2}, LX/EwG;->d(Landroid/view/ViewGroup;)Lcom/facebook/friending/common/list/FriendListItemView;

    move-result-object v0

    goto :goto_0

    .line 2182848
    :pswitch_8
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03070e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 2182849
    goto :goto_0

    .line 2182850
    :pswitch_9
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03070b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 2182851
    goto :goto_0

    .line 2182852
    :pswitch_a
    iget-boolean v5, p0, LX/EwG;->s:Z

    if-nez v5, :cond_0

    .line 2182853
    iget-object v5, p0, LX/EwG;->c:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    iput-wide v5, p0, LX/EwG;->r:J

    .line 2182854
    iget-object v5, p0, LX/EwG;->b:LX/Euk;

    iget-wide v7, p0, LX/EwG;->r:J

    .line 2182855
    iget-object v6, v5, LX/Euk;->a:LX/0if;

    sget-object v9, LX/0ig;->P:LX/0ih;

    invoke-virtual {v6, v9, v7, v8}, LX/0if;->a(LX/0ih;J)V

    .line 2182856
    iget-object v6, v5, LX/Euk;->a:LX/0if;

    sget-object v9, LX/0ig;->P:LX/0ih;

    sget-object v10, LX/Euj;->DELETE_ALL_SHOWN:LX/Euj;

    invoke-virtual {v10}, LX/Euj;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v7, v8, v10}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 2182857
    const/4 v5, 0x1

    iput-boolean v5, p0, LX/EwG;->s:Z

    .line 2182858
    :cond_0
    new-instance v3, Lcom/facebook/friending/common/list/FriendListItemView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/friending/common/list/FriendListItemView;-><init>(Landroid/content/Context;)V

    .line 2182859
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2182860
    const v4, 0x7f080f93

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2182861
    const v4, 0x7f080f94

    const/4 v6, 0x0

    .line 2182862
    iget-object v5, p0, LX/EwG;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/23P;

    .line 2182863
    if-nez v4, :cond_1

    move-object v5, v6

    :goto_1
    move-object v4, v5

    .line 2182864
    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonText(Ljava/lang/CharSequence;)V

    .line 2182865
    new-instance v4, LX/Ew8;

    invoke-direct {v4, p0}, LX/Ew8;-><init>(LX/EwG;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2182866
    move-object v0, v3

    .line 2182867
    goto/16 :goto_0

    .line 2182868
    :pswitch_b
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030709

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 2182869
    goto/16 :goto_0

    :cond_1
    iget-object v7, p0, LX/EwG;->p:Landroid/content/res/Resources;

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7, v6}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v5

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 2182870
    sget-object v0, LX/Ew5;->a:[I

    invoke-static {}, LX/Ewp;->values()[LX/Ewp;

    move-result-object v1

    aget-object v1, v1, p4

    invoke-virtual {v1}, LX/Ewp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2182871
    :goto_0
    :pswitch_0
    return-void

    .line 2182872
    :pswitch_1
    iget-object v2, p0, LX/EwG;->e:LX/Exs;

    check-cast p3, Lcom/facebook/friending/common/list/FriendRequestItemView;

    move-object v0, p2

    check-cast v0, LX/Ewi;

    move-object v1, p2

    check-cast v1, LX/Ewi;

    .line 2182873
    iget-boolean v3, v1, LX/Eut;->e:Z

    move v3, v3

    .line 2182874
    if-eqz v3, :cond_1

    .line 2182875
    new-instance v3, LX/EwD;

    invoke-direct {v3, p0, v1}, LX/EwD;-><init>(LX/EwG;LX/Ewi;)V

    move-object v3, v3

    .line 2182876
    :goto_1
    move-object v1, v3

    .line 2182877
    check-cast p2, LX/Ewi;

    .line 2182878
    iget-boolean v3, p2, LX/Eut;->e:Z

    move v3, v3

    .line 2182879
    if-eqz v3, :cond_3

    .line 2182880
    new-instance v3, LX/EwE;

    invoke-direct {v3, p0, p2}, LX/EwE;-><init>(LX/EwG;LX/Ewi;)V

    move-object v3, v3

    .line 2182881
    :goto_2
    move-object v3, v3

    .line 2182882
    const/4 p2, 0x0

    .line 2182883
    invoke-virtual {v0}, LX/Eus;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, LX/EyP;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2182884
    invoke-virtual {v0}, LX/Eus;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, LX/EyP;->setThumbnailUri(Ljava/lang/String;)V

    .line 2182885
    const-string v4, ""

    invoke-interface {p3, v4}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2182886
    iget-boolean v4, v0, LX/Eut;->c:Z

    move v4, v4

    .line 2182887
    if-eqz v4, :cond_6

    .line 2182888
    const v5, 0x7f080f7b

    .line 2182889
    const p0, 0x7f080f7c

    .line 2182890
    const v4, 0x7f080f80

    .line 2182891
    iget-object p1, v0, LX/Eut;->b:Ljava/lang/String;

    move-object p1, p1

    .line 2182892
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_5

    .line 2182893
    invoke-virtual {v0}, LX/Eus;->h()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p3, p1}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    move p1, p0

    move p0, v5

    move v5, v4

    .line 2182894
    :goto_3
    iget-object v4, v2, LX/Exs;->n:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ad;

    sget-short p4, LX/2hr;->w:S

    invoke-interface {v4, p4, p2}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2182895
    iget-boolean v4, v0, LX/Eut;->g:Z

    move v4, v4

    .line 2182896
    if-eqz v4, :cond_7

    const v4, 0x7f0a004f

    :goto_4
    invoke-interface {p3, v4}, LX/EyP;->setBackgroundResource(I)V

    .line 2182897
    :cond_0
    invoke-static {v2, p0}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v2, p1}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p3, v4, p0}, LX/EyT;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2182898
    invoke-interface {p3, v1}, LX/EyT;->setPositiveButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2182899
    invoke-static {v2, v5}, LX/Exs;->b(LX/Exs;I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {p3, v4}, LX/EyT;->setNegativeButtonText(Ljava/lang/CharSequence;)V

    .line 2182900
    invoke-interface {p3, v3}, LX/EyT;->setNegativeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2182901
    invoke-static {p3}, LX/Exs;->a(LX/EyP;)V

    .line 2182902
    invoke-static {v2, p3, v0}, LX/Exs;->f(LX/Exs;LX/EyP;LX/83X;)V

    .line 2182903
    goto/16 :goto_0

    .line 2182904
    :pswitch_2
    check-cast p3, Lcom/facebook/fbui/widget/contentview/ContentView;

    check-cast p2, LX/Ewf;

    .line 2182905
    invoke-virtual {p2}, LX/Eup;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2182906
    invoke-virtual {p2}, LX/Eup;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2182907
    goto/16 :goto_0

    .line 2182908
    :pswitch_3
    iget-object v0, p0, LX/EwG;->e:LX/Exs;

    check-cast p3, Lcom/facebook/friending/common/list/FriendListItemView;

    check-cast p2, LX/Eus;

    invoke-virtual {v0, p3, p2}, LX/Exs;->a(LX/EyQ;LX/83X;)V

    goto/16 :goto_0

    .line 2182909
    :pswitch_4
    iget-object v0, p0, LX/EwG;->e:LX/Exs;

    check-cast p3, Lcom/facebook/friending/common/list/FriendRequestItemView;

    check-cast p2, LX/Eus;

    invoke-virtual {v0, p3, p2}, LX/Exs;->a(LX/EyT;LX/83X;)V

    goto/16 :goto_0

    .line 2182910
    :pswitch_5
    iget-object v0, p0, LX/EwG;->e:LX/Exs;

    check-cast p3, Lcom/facebook/friending/common/list/FriendListItemView;

    check-cast p2, LX/Eus;

    const/4 v1, 0x1

    invoke-virtual {v0, p3, p2, v1}, LX/Exs;->a(LX/EyQ;LX/83X;Z)V

    goto/16 :goto_0

    .line 2182911
    :cond_1
    iget-boolean v3, v1, LX/Eut;->c:Z

    move v3, v3

    .line 2182912
    if-eqz v3, :cond_2

    .line 2182913
    new-instance v3, LX/Ew9;

    invoke-direct {v3, p0, v1}, LX/Ew9;-><init>(LX/EwG;LX/Ewi;)V

    move-object v3, v3

    .line 2182914
    goto/16 :goto_1

    .line 2182915
    :cond_2
    new-instance v3, LX/EwB;

    invoke-direct {v3, p0, v1}, LX/EwB;-><init>(LX/EwG;LX/Ewi;)V

    move-object v3, v3

    .line 2182916
    goto/16 :goto_1

    .line 2182917
    :cond_3
    iget-boolean v3, p2, LX/Eut;->c:Z

    move v3, v3

    .line 2182918
    if-eqz v3, :cond_4

    .line 2182919
    new-instance v3, LX/EwA;

    invoke-direct {v3, p0, p2}, LX/EwA;-><init>(LX/EwG;LX/Ewi;)V

    move-object v3, v3

    .line 2182920
    goto/16 :goto_2

    .line 2182921
    :cond_4
    new-instance v3, LX/EwC;

    invoke-direct {v3, p0, p2}, LX/EwC;-><init>(LX/EwG;LX/Ewi;)V

    move-object v3, v3

    .line 2182922
    goto/16 :goto_2

    .line 2182923
    :cond_5
    iget-object p4, v2, LX/Exs;->p:Landroid/content/res/Resources;

    const p5, 0x7f081fe6

    invoke-virtual {p4, p5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p4

    const/4 p5, 0x1

    new-array p5, p5, [Ljava/lang/Object;

    aput-object p1, p5, p2

    invoke-static {p4, p5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p3, p1}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    move p1, p0

    move p0, v5

    move v5, v4

    .line 2182924
    goto/16 :goto_3

    .line 2182925
    :cond_6
    invoke-virtual {v0}, LX/Eus;->h()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3, v4}, LX/EyP;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2182926
    const v5, 0x7f080f83

    .line 2182927
    const v4, 0x7f080f81

    move p0, v5

    move p1, p2

    move v5, v4

    goto/16 :goto_3

    .line 2182928
    :cond_7
    const v4, 0x7f020b86

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Ewe;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2182929
    iput-object p1, p0, LX/EwG;->j:Ljava/util/List;

    .line 2182930
    iget-boolean v0, p0, LX/EwG;->l:Z

    if-eqz v0, :cond_0

    .line 2182931
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    iget-object v1, p0, LX/EwG;->k:LX/Ewe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2182932
    :cond_0
    const v0, 0x1cd7363e

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2182933
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2182934
    iget-boolean v0, p0, LX/EwG;->l:Z

    if-ne v0, p1, :cond_1

    .line 2182935
    :cond_0
    :goto_0
    return-void

    .line 2182936
    :cond_1
    iput-boolean p1, p0, LX/EwG;->l:Z

    .line 2182937
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2182938
    iget-boolean v0, p0, LX/EwG;->l:Z

    if-eqz v0, :cond_2

    .line 2182939
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    iget-object v1, p0, LX/EwG;->k:LX/Ewe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2182940
    :goto_1
    const v0, -0x78b5cad4

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2182941
    :cond_2
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    iget-object v1, p0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2182942
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewe;

    .line 2182943
    sget-object v2, LX/Ew5;->a:[I

    invoke-interface {v0}, LX/Ewe;->lg_()LX/Ewp;

    move-result-object v0

    invoke-virtual {v0}, LX/Ewp;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    .line 2182944
    :goto_0
    return-object v0

    .line 2182945
    :pswitch_1
    const v0, 0x7f080f70

    invoke-direct {p0, p2, p3, v0}, LX/EwG;->a(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    move-object v0, v1

    .line 2182946
    goto :goto_0

    .line 2182947
    :pswitch_3
    const v0, 0x7f080f77

    invoke-direct {p0, p2, p3, v0}, LX/EwG;->a(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2182948
    iget v0, p0, LX/EwG;->i:I

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2182949
    iget v0, p0, LX/EwG;->n:I

    return v0
.end method

.method public final f(I)Z
    .locals 2

    .prologue
    .line 2182950
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewe;

    invoke-interface {v0}, LX/Ewe;->lg_()LX/Ewp;

    move-result-object v0

    sget-object v1, LX/Ewp;->FRIEND_REQUEST_HEADER:LX/Ewp;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewe;

    invoke-interface {v0}, LX/Ewe;->lg_()LX/Ewp;

    move-result-object v0

    sget-object v1, LX/Ewp;->PYMK_HEADER:LX/Ewp;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2182951
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2182952
    iget v0, p0, LX/EwG;->o:I

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/EwG;->o:I

    .line 2182953
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2182954
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2182955
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewe;

    invoke-interface {v0}, LX/Ewe;->lg_()LX/Ewp;

    move-result-object v0

    invoke-virtual {v0}, LX/Ewp;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2182956
    invoke-static {}, LX/Ewp;->values()[LX/Ewp;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 2182957
    iget-object v0, p0, LX/EwG;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ewe;

    invoke-interface {v0}, LX/Ewe;->lh_()Z

    move-result v0

    return v0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2182958
    const/4 v0, 0x0

    return v0
.end method
