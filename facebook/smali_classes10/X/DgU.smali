.class public final LX/DgU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DgT;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;)V
    .locals 0

    .prologue
    .line 2029500
    iput-object p1, p0, LX/DgU;->a:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 2029486
    iget-object v0, p0, LX/DgU;->a:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    .line 2029487
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    sget-object v2, LX/Dgb;->UNSET:LX/Dgb;

    if-ne v1, v2, :cond_1

    .line 2029488
    const-string v1, "LocationSendingMainFragment"

    const-string v2, "Selected location is unset, this should never happen."

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    const/4 v2, 0x1

    .line 2029489
    iput v2, v1, LX/0VK;->e:I

    .line 2029490
    move-object v1, v1

    .line 2029491
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 2029492
    iget-object v2, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->c:LX/03V;

    invoke-virtual {v2, v1}, LX/03V;->a(LX/0VG;)V

    .line 2029493
    :cond_0
    :goto_0
    return-void

    .line 2029494
    :cond_1
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    sget-object v2, LX/Dgb;->USER_LOCATION:LX/Dgb;

    if-ne v1, v2, :cond_2

    .line 2029495
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->d:LX/DgO;

    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    iget-object v3, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->h:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    iget-object v5, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->h:Landroid/location/Location;

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v2}, LX/DgO;->a(Lcom/facebook/android/maps/model/LatLng;)V

    goto :goto_0

    .line 2029496
    :cond_2
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    sget-object v2, LX/Dgb;->NEARBY_PLACE:LX/Dgb;

    if-ne v1, v2, :cond_3

    .line 2029497
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->d:LX/DgO;

    iget-object v2, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->i:Lcom/facebook/messaging/location/sending/NearbyPlace;

    invoke-interface {v1, v2}, LX/DgO;->a(Lcom/facebook/messaging/location/sending/NearbyPlace;)V

    goto :goto_0

    .line 2029498
    :cond_3
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->f:LX/Dgb;

    sget-object v2, LX/Dgb;->PINNED_LOCATION:LX/Dgb;

    if-ne v1, v2, :cond_0

    .line 2029499
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->d:LX/DgO;

    iget-object v2, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->j:Lcom/facebook/android/maps/model/LatLng;

    invoke-interface {v1, v2}, LX/DgO;->b(Lcom/facebook/android/maps/model/LatLng;)V

    goto :goto_0
.end method
