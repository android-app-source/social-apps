.class public final LX/DUd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2003410
    const-wide/16 v10, 0x0

    .line 2003411
    const/4 v8, 0x0

    .line 2003412
    const/4 v7, 0x0

    .line 2003413
    const/4 v6, 0x0

    .line 2003414
    const/4 v5, 0x0

    .line 2003415
    const/4 v4, 0x0

    .line 2003416
    const/4 v3, 0x0

    .line 2003417
    const/4 v2, 0x0

    .line 2003418
    const/4 v1, 0x0

    .line 2003419
    const/4 v0, 0x0

    .line 2003420
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v12, :cond_c

    .line 2003421
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2003422
    const/4 v0, 0x0

    .line 2003423
    :goto_0
    return v0

    .line 2003424
    :cond_0
    const-string v4, "friendMembers"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2003425
    invoke-static {p0, p1}, LX/DUj;->a(LX/15w;LX/186;)I

    move-result v1

    move v13, v1

    .line 2003426
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 2003427
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 2003428
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2003429
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v1, :cond_1

    .line 2003430
    const-string v4, "archived_time"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2003431
    const/4 v0, 0x1

    .line 2003432
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 2003433
    :cond_2
    const-string v4, "group_id"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2003434
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v12, v1

    goto :goto_1

    .line 2003435
    :cond_3
    const-string v4, "id"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2003436
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v11, v1

    goto :goto_1

    .line 2003437
    :cond_4
    const-string v4, "invitedMembers"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2003438
    invoke-static {p0, p1}, LX/DUj;->a(LX/15w;LX/186;)I

    move-result v1

    move v10, v1

    goto :goto_1

    .line 2003439
    :cond_5
    const-string v4, "name"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2003440
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v9, v1

    goto :goto_1

    .line 2003441
    :cond_6
    const-string v4, "otherMembers"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2003442
    invoke-static {p0, p1}, LX/DUj;->a(LX/15w;LX/186;)I

    move-result v1

    move v8, v1

    goto :goto_1

    .line 2003443
    :cond_7
    const-string v4, "viewer_admin_type"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2003444
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 2003445
    :cond_8
    const-string v4, "visibility"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2003446
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 2003447
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2003448
    :cond_a
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2003449
    if-eqz v0, :cond_b

    .line 2003450
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2003451
    :cond_b
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 2003452
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 2003453
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2003454
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2003455
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2003456
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2003457
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2003458
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2003459
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_c
    move v9, v4

    move v12, v7

    move v13, v8

    move v7, v2

    move v8, v3

    move-wide v2, v10

    move v10, v5

    move v11, v6

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x7

    .line 2003371
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2003372
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2003373
    cmp-long v2, v0, v6

    if-eqz v2, :cond_0

    .line 2003374
    const-string v2, "archived_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003375
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2003376
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2003377
    if-eqz v0, :cond_1

    .line 2003378
    const-string v1, "friendMembers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003379
    invoke-static {p0, v0, p2, p3}, LX/DUj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2003380
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2003381
    if-eqz v0, :cond_2

    .line 2003382
    const-string v1, "group_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003383
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2003384
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2003385
    if-eqz v0, :cond_3

    .line 2003386
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003387
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2003388
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2003389
    if-eqz v0, :cond_4

    .line 2003390
    const-string v1, "invitedMembers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003391
    invoke-static {p0, v0, p2, p3}, LX/DUj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2003392
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2003393
    if-eqz v0, :cond_5

    .line 2003394
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003395
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2003396
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2003397
    if-eqz v0, :cond_6

    .line 2003398
    const-string v1, "otherMembers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003399
    invoke-static {p0, v0, p2, p3}, LX/DUj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2003400
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2003401
    if-eqz v0, :cond_7

    .line 2003402
    const-string v0, "viewer_admin_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003403
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2003404
    :cond_7
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2003405
    if-eqz v0, :cond_8

    .line 2003406
    const-string v0, "visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003407
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2003408
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2003409
    return-void
.end method
