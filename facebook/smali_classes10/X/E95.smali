.class public LX/E95;
.super LX/1OM;
.source ""

# interfaces
.implements LX/0Vf;
.implements LX/E6V;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/E8r;",
        ">;",
        "LX/0Vf;",
        "LX/E6V;"
    }
.end annotation


# instance fields
.field private final a:LX/E37;

.field private final b:LX/E8Q;

.field private c:LX/1Qq;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0o8;Ljava/lang/String;LX/2jY;Ljava/lang/String;Ljava/lang/String;LX/1PY;LX/E37;LX/E8R;)V
    .locals 10
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0o8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/2jY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2083961
    invoke-direct {p0}, LX/1OM;-><init>()V

    move-object/from16 v1, p9

    move-object v2, p3

    move-object v3, p0

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    .line 2083962
    invoke-virtual/range {v1 .. v6}, LX/E8R;->a(Ljava/lang/String;LX/E6V;LX/2jY;Ljava/lang/String;Ljava/lang/String;)LX/E8Q;

    move-result-object v1

    iput-object v1, p0, LX/E95;->b:LX/E8Q;

    .line 2083963
    move-object/from16 v0, p8

    iput-object v0, p0, LX/E95;->a:LX/E37;

    .line 2083964
    iget-object v1, p0, LX/E95;->a:LX/E37;

    invoke-static {}, LX/E38;->b()LX/E38;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v2, p0, LX/E95;->b:LX/E8Q;

    invoke-virtual {v2}, LX/E8Q;->a()LX/E39;

    move-result-object v5

    new-instance v6, Lcom/facebook/reaction/ui/recyclerview/ReactionShowMoreComponentsRecyclerViewAdapter$1;

    invoke-direct {v6, p0}, Lcom/facebook/reaction/ui/recyclerview/ReactionShowMoreComponentsRecyclerViewAdapter$1;-><init>(LX/E95;)V

    move-object v2, p1

    move-object v7, p2

    move-object v8, p4

    move-object/from16 v9, p7

    invoke-virtual/range {v1 .. v9}, LX/E37;->a(Landroid/content/Context;LX/1PT;LX/1SX;LX/E39;Ljava/lang/Runnable;LX/0o8;LX/2jY;LX/1PY;)LX/1Qq;

    move-result-object v1

    iput-object v1, p0, LX/E95;->c:LX/1Qq;

    .line 2083965
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2083959
    iget-object v0, p0, LX/E95;->c:LX/1Qq;

    invoke-interface {v0, p2, p1}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2083960
    new-instance v1, LX/E8r;

    invoke-direct {v1, p1, v0}, LX/E8r;-><init>(Landroid/view/ViewGroup;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2083954
    check-cast p1, LX/E8r;

    .line 2083955
    iget-object v0, p0, LX/E95;->c:LX/1Qq;

    iget-object v1, p0, LX/E95;->c:LX/1Qq;

    invoke-interface {v1, p2}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v4

    .line 2083956
    iget-object v1, p1, LX/E8r;->l:Landroid/view/ViewGroup;

    move-object v5, v1

    .line 2083957
    move v1, p2

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 2083958
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2083951
    iget-object v0, p0, LX/E95;->c:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2083952
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2083953
    return-void
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 2083966
    iget-object v0, p0, LX/E95;->c:LX/1Qq;

    if-eqz v0, :cond_0

    .line 2083967
    iget-object v0, p0, LX/E95;->c:LX/1Qq;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 2083968
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/E95;->d:Z

    .line 2083969
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2083949
    iget-object v0, p0, LX/E95;->b:LX/E8Q;

    invoke-virtual {v0}, LX/E8N;->b()V

    .line 2083950
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2083948
    iget-object v0, p0, LX/E95;->c:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qq;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2083947
    iget-object v0, p0, LX/E95;->c:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 2083946
    iget-boolean v0, p0, LX/E95;->d:Z

    return v0
.end method
