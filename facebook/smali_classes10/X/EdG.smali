.class public LX/EdG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;

.field private static volatile b:J


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:Landroid/net/ConnectivityManager;

.field public e:Z

.field public f:I

.field private g:I

.field public h:Ljava/util/Timer;

.field public final i:LX/EdB;

.field public final j:Landroid/content/IntentFilter;

.field public final k:Landroid/content/BroadcastReceiver;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2149009
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "already active"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "request started"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "type not available"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "request failed"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "already inactive"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "unknown"

    aput-object v2, v0, v1

    sput-object v0, LX/EdG;->a:[Ljava/lang/String;

    .line 2149010
    const-wide/32 v0, 0x2bf20

    sput-wide v0, LX/EdG;->b:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2148999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2149000
    new-instance v0, LX/EdF;

    invoke-direct {v0, p0}, LX/EdF;-><init>(LX/EdG;)V

    iput-object v0, p0, LX/EdG;->k:Landroid/content/BroadcastReceiver;

    .line 2149001
    iput-object p1, p0, LX/EdG;->c:Landroid/content/Context;

    .line 2149002
    iget-object v0, p0, LX/EdG;->c:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    .line 2149003
    new-instance v0, LX/EdB;

    iget-object v1, p0, LX/EdG;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/EdB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/EdG;->i:LX/EdB;

    .line 2149004
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, LX/EdG;->j:Landroid/content/IntentFilter;

    .line 2149005
    iget-object v0, p0, LX/EdG;->j:Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2149006
    iput v2, p0, LX/EdG;->f:I

    .line 2149007
    iput v2, p0, LX/EdG;->g:I

    .line 2149008
    return-void
.end method

.method public static synthetic a(LX/EdG;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2148991
    iget v0, p0, LX/EdG;->f:I

    if-gtz v0, :cond_1

    .line 2148992
    :cond_0
    :goto_0
    return-void

    .line 2148993
    :cond_1
    iget-object v0, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    const/4 p1, 0x2

    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 2148994
    if-eqz v0, :cond_0

    .line 2148995
    const-string p1, "2GVoiceCallEnded"

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getReason()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 2148996
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    .line 2148997
    sget-object p1, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v0, p1, :cond_2

    sget-object p1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v0, p1, :cond_0

    invoke-static {p0}, LX/EdG;->o(LX/EdG;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2148998
    :cond_2
    invoke-static {p0}, LX/EdG;->g(LX/EdG;)V

    goto :goto_0
.end method

.method public static g(LX/EdG;)V
    .locals 1

    .prologue
    .line 2148988
    monitor-enter p0

    .line 2148989
    const v0, 0x4b27c4f5    # 1.0994933E7f

    :try_start_0
    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 2148990
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 2148984
    iget-object v0, p0, LX/EdG;->h:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 2148985
    iget-object v0, p0, LX/EdG;->h:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 2148986
    const/4 v0, 0x0

    iput-object v0, p0, LX/EdG;->h:Ljava/util/Timer;

    .line 2148987
    :cond_0
    return-void
.end method

.method public static j(LX/EdG;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 2149011
    :try_start_0
    iget-object v1, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "startUsingNetworkFeature"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2149012
    if-eqz v1, :cond_5

    .line 2149013
    iget-object v2, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "enableMMS"

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2149014
    :goto_0
    move v1, v1

    .line 2149015
    if-nez v1, :cond_1

    .line 2149016
    iget-object v4, p0, LX/EdG;->h:Ljava/util/Timer;

    if-nez v4, :cond_0

    .line 2149017
    new-instance v4, Ljava/util/Timer;

    const-string v5, "mms_network_extension_timer"

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Ljava/util/Timer;-><init>(Ljava/lang/String;Z)V

    iput-object v4, p0, LX/EdG;->h:Ljava/util/Timer;

    .line 2149018
    iget-object v4, p0, LX/EdG;->h:Ljava/util/Timer;

    new-instance v5, Landroid_src/mmsv2/MmsNetworkManager$2;

    invoke-direct {v5, p0}, Landroid_src/mmsv2/MmsNetworkManager$2;-><init>(LX/EdG;)V

    const-wide/16 v6, 0x7530

    invoke-virtual {v4, v5, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 2149019
    :cond_0
    :goto_1
    return v0

    .line 2149020
    :cond_1
    if-eq v1, v0, :cond_4

    .line 2149021
    invoke-direct {p0}, LX/EdG;->i()V

    .line 2149022
    new-instance v0, LX/EdE;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot acquire MMS network: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2149023
    if-ltz v1, :cond_2

    sget-object v3, LX/EdG;->a:[Ljava/lang/String;

    array-length v3, v3

    if-lt v1, v3, :cond_3

    .line 2149024
    :cond_2
    sget-object v3, LX/EdG;->a:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v1, v3, -0x1

    .line 2149025
    :cond_3
    sget-object v3, LX/EdG;->a:[Ljava/lang/String;

    aget-object v3, v3, v1

    move-object v1, v3

    .line 2149026
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EdE;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2149027
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2149028
    :catch_0
    move-exception v1

    .line 2149029
    const-string v2, "MmsLib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ConnectivityManager.startUsingNetworkFeature failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149030
    :cond_5
    const/4 v1, 0x3

    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2148980
    iget-boolean v0, p0, LX/EdG;->e:Z

    if-eqz v0, :cond_0

    .line 2148981
    iget-object v0, p0, LX/EdG;->c:Landroid/content/Context;

    iget-object v1, p0, LX/EdG;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2148982
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EdG;->e:Z

    .line 2148983
    :cond_0
    return-void
.end method

.method public static o(LX/EdG;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2148972
    :try_start_0
    iget-object v0, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 2148973
    const-string v2, "getMobileDataEnabled"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2148974
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2148975
    iget-object v2, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2148976
    :goto_0
    return v0

    .line 2148977
    :catch_0
    move-exception v0

    .line 2148978
    const-string v2, "MmsLib"

    const-string v3, "TelephonyManager.getMobileDataEnabled failed"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 2148979
    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/net/ConnectivityManager;
    .locals 1

    .prologue
    .line 2148971
    iget-object v0, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 2148942
    monitor-enter p0

    .line 2148943
    :try_start_0
    iget v0, p0, LX/EdG;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EdG;->f:I

    .line 2148944
    iget v0, p0, LX/EdG;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EdG;->g:I

    .line 2148945
    iget v0, p0, LX/EdG;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2148946
    iget-boolean v0, p0, LX/EdG;->e:Z

    if-nez v0, :cond_0

    .line 2148947
    iget-object v0, p0, LX/EdG;->c:Landroid/content/Context;

    iget-object v1, p0, LX/EdG;->k:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, LX/EdG;->j:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2148948
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EdG;->e:Z

    .line 2148949
    :cond_0
    sget-wide v0, LX/EdG;->b:J

    .line 2148950
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 2148951
    :cond_1
    invoke-static {p0}, LX/EdG;->o(LX/EdG;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2148952
    new-instance v0, LX/EdE;

    const-string v1, "Mobile data is disabled"

    invoke-direct {v0, v1}, LX/EdE;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2148953
    :catchall_0
    move-exception v0

    :try_start_1
    iget v1, p0, LX/EdG;->g:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/EdG;->g:I

    .line 2148954
    iget v1, p0, LX/EdG;->g:I

    if-nez v1, :cond_2

    .line 2148955
    invoke-direct {p0}, LX/EdG;->n()V

    :cond_2
    throw v0

    .line 2148956
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 2148957
    :cond_3
    :try_start_2
    invoke-static {p0}, LX/EdG;->j(LX/EdG;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-eqz v4, :cond_5

    .line 2148958
    :try_start_3
    iget v0, p0, LX/EdG;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/EdG;->g:I

    .line 2148959
    iget v0, p0, LX/EdG;->g:I

    if-nez v0, :cond_4

    .line 2148960
    invoke-direct {p0}, LX/EdG;->n()V

    :cond_4
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_0
    return-void

    .line 2148961
    :cond_5
    const-wide/16 v4, 0x3a98

    :try_start_4
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    const v4, -0x5aacdc12

    invoke-static {p0, v0, v1, v4}, LX/02L;->a(Ljava/lang/Object;JI)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2148962
    :goto_1
    :try_start_5
    sget-wide v0, LX/EdG;->b:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v2

    sub-long/2addr v0, v4

    .line 2148963
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gtz v4, :cond_1

    .line 2148964
    invoke-static {p0}, LX/EdG;->j(LX/EdG;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v0

    if-eqz v0, :cond_7

    .line 2148965
    :try_start_6
    iget v0, p0, LX/EdG;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/EdG;->g:I

    .line 2148966
    iget v0, p0, LX/EdG;->g:I

    if-nez v0, :cond_6

    .line 2148967
    invoke-direct {p0}, LX/EdG;->n()V

    :cond_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 2148968
    :catch_0
    move-exception v0

    .line 2148969
    const-string v1, "MmsLib"

    const-string v4, "Unexpected exception"

    invoke-static {v1, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2148970
    :cond_7
    new-instance v0, LX/EdE;

    const-string v1, "Acquiring MMS network timed out"

    invoke-direct {v0, v1}, LX/EdE;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 2148927
    monitor-enter p0

    .line 2148928
    :try_start_0
    iget v0, p0, LX/EdG;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/EdG;->f:I

    .line 2148929
    iget v0, p0, LX/EdG;->f:I

    if-nez v0, :cond_0

    .line 2148930
    invoke-direct {p0}, LX/EdG;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2148931
    :try_start_1
    iget-object v0, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "stopUsingNetworkFeature"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2148932
    if-eqz v0, :cond_0

    .line 2148933
    iget-object v1, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "enableMMS"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2148934
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2148935
    :catch_0
    move-exception v0

    .line 2148936
    const-string v1, "MmsLib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConnectivityManager.stopUsingNetworkFeature failed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2148937
    const/4 v0, 0x0

    .line 2148938
    iget-object v1, p0, LX/EdG;->d:Landroid/net/ConnectivityManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 2148939
    if-eqz v1, :cond_0

    .line 2148940
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    move-result-object v0

    .line 2148941
    :cond_0
    return-object v0
.end method
