.class public final LX/EvZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2181913
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2181914
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2181915
    :goto_0
    return v1

    .line 2181916
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 2181917
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2181918
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2181919
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 2181920
    const-string v7, "is_seen"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2181921
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 2181922
    :cond_1
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2181923
    invoke-static {p0, p1}, LX/Evb;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2181924
    :cond_2
    const-string v7, "suggesters"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2181925
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2181926
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_3

    .line 2181927
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_3

    .line 2181928
    invoke-static {p0, p1}, LX/EvY;->b(LX/15w;LX/186;)I

    move-result v6

    .line 2181929
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2181930
    :cond_3
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2181931
    goto :goto_1

    .line 2181932
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2181933
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2181934
    if-eqz v0, :cond_6

    .line 2181935
    invoke-virtual {p1, v1, v5}, LX/186;->a(IZ)V

    .line 2181936
    :cond_6
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2181937
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2181938
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2181894
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2181895
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2181896
    if-eqz v0, :cond_0

    .line 2181897
    const-string v1, "is_seen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181898
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2181899
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2181900
    if-eqz v0, :cond_1

    .line 2181901
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181902
    invoke-static {p0, v0, p2, p3}, LX/Evb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2181903
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2181904
    if-eqz v0, :cond_3

    .line 2181905
    const-string v1, "suggesters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2181906
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2181907
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_2

    .line 2181908
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2}, LX/EvY;->a(LX/15i;ILX/0nX;)V

    .line 2181909
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2181910
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2181911
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2181912
    return-void
.end method
