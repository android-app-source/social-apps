.class public final LX/Ef9;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;)V
    .locals 0

    .prologue
    .line 2153840
    iput-object p1, p0, LX/Ef9;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 2

    .prologue
    .line 2153841
    iget-object v0, p0, LX/Ef9;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v0, v0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->a:LX/0he;

    iget-object v1, p0, LX/Ef9;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v1, v1, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->g:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v1, p1}, LX/0he;->c(II)V

    .line 2153842
    iget-object v0, p0, LX/Ef9;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v0, v0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->f:Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;

    invoke-virtual {v0, p1}, Lcom/facebook/audience/ui/BackstageReplyPageIndicatorView;->setCurrentSegmentIndex(I)V

    .line 2153843
    iget-object v0, p0, LX/Ef9;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    .line 2153844
    iput p1, v0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->h:I

    .line 2153845
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 2153846
    iget-object v0, p0, LX/Ef9;->a:Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;

    iget-object v0, v0, Lcom/facebook/audience/direct/app/SnacksReplyThreadFragment;->j:LX/Efh;

    .line 2153847
    const/4 p0, 0x0

    cmpl-float p0, p2, p0

    if-nez p0, :cond_1

    .line 2153848
    iget-object p0, v0, LX/Efh;->e:Landroid/util/SparseArray;

    invoke-virtual {p0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Efg;

    .line 2153849
    if-eqz p0, :cond_0

    .line 2153850
    invoke-virtual {p0}, LX/Efg;->a()V

    .line 2153851
    :cond_0
    :goto_0
    return-void

    .line 2153852
    :cond_1
    iget-object p0, v0, LX/Efh;->e:Landroid/util/SparseArray;

    invoke-virtual {p0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Efg;

    .line 2153853
    if-eqz p0, :cond_2

    .line 2153854
    invoke-virtual {p0}, LX/Efg;->b()V

    .line 2153855
    :cond_2
    add-int/lit8 p0, p1, 0x1

    iget-object p3, v0, LX/Efh;->a:LX/0Px;

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result p3

    if-ge p0, p3, :cond_0

    .line 2153856
    iget-object p0, v0, LX/Efh;->e:Landroid/util/SparseArray;

    add-int/lit8 p3, p1, 0x1

    invoke-virtual {p0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Efg;

    .line 2153857
    if-eqz p0, :cond_0

    .line 2153858
    invoke-virtual {p0}, LX/Efg;->b()V

    goto :goto_0
.end method
