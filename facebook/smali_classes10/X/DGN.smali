.class public LX/DGN;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DGN",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979646
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1979647
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DGN;->b:LX/0Zi;

    .line 1979648
    iput-object p1, p0, LX/DGN;->a:LX/0Ot;

    .line 1979649
    return-void
.end method

.method public static a(LX/0QB;)LX/DGN;
    .locals 4

    .prologue
    .line 1979650
    const-class v1, LX/DGN;

    monitor-enter v1

    .line 1979651
    :try_start_0
    sget-object v0, LX/DGN;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979652
    sput-object v2, LX/DGN;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979653
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979654
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979655
    new-instance v3, LX/DGN;

    const/16 p0, 0x21a4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DGN;-><init>(LX/0Ot;)V

    .line 1979656
    move-object v0, v3

    .line 1979657
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979658
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DGN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979659
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 12

    .prologue
    .line 1979661
    check-cast p2, LX/DGM;

    .line 1979662
    iget-object v0, p0, LX/DGN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;

    iget-object v1, p2, LX/DGM;->b:LX/1Pd;

    iget-object v2, p2, LX/DGM;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979663
    iget-object v3, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->d:LX/2mt;

    invoke-virtual {v3, v2}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v7

    .line 1979664
    iget-object v3, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->b:LX/2yJ;

    iget-object v6, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->c:LX/2yN;

    move-object v4, v1

    check-cast v4, LX/1Pv;

    invoke-static {v2, v4}, LX/C8d;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pv;)LX/2yV;

    move-result-object v8

    move-object v4, v1

    check-cast v4, LX/1Pv;

    invoke-static {v2, v4}, LX/C8d;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pv;)LX/2yi;

    move-result-object v9

    invoke-static {v2}, LX/C8d;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v10

    move-object v11, v1

    check-cast v11, LX/1Pq;

    move-object v4, p1

    move-object v5, v2

    invoke-virtual/range {v3 .. v11}, LX/2yJ;->onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yN;Ljava/lang/String;LX/2yV;LX/2yi;ZLX/1Pq;)V

    .line 1979665
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1979666
    const v0, -0x18f3e90c

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1979667
    check-cast p2, LX/DGM;

    .line 1979668
    iget-object v0, p0, LX/DGN;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;

    iget-object v1, p2, LX/DGM;->a:LX/DGK;

    iget-object v2, p2, LX/DGM;->b:LX/1Pd;

    .line 1979669
    iget-object v3, v1, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v3

    .line 1979670
    invoke-static {v3}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1979671
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    .line 1979672
    const p2, -0x18f3e90c

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 1979673
    invoke-interface {p0, p2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object p0

    .line 1979674
    const p2, -0x6bd759ae

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 1979675
    invoke-interface {p0, p2}, LX/1Dh;->e(LX/1dQ;)LX/1Dh;

    move-result-object p0

    iget-object p2, v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->a:LX/1nu;

    invoke-virtual {p2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p2

    .line 1979676
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v0

    .line 1979677
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1979678
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v3, v0

    .line 1979679
    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    const p2, 0x3ff745d1

    invoke-virtual {v3, p2}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    const p2, 0x7f0a045d

    invoke-virtual {v3, p2}, LX/1nw;->h(I)LX/1nw;

    move-result-object v3

    sget-object p2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    check-cast v2, LX/1Pp;

    invoke-virtual {v3, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v3

    const/4 p2, 0x1

    invoke-virtual {v3, p2}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v3

    invoke-interface {p0, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1979680
    return-object v0

    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1979681
    invoke-static {}, LX/1dS;->b()V

    .line 1979682
    iget v1, p1, LX/1dQ;->b:I

    .line 1979683
    sparse-switch v1, :sswitch_data_0

    .line 1979684
    :goto_0
    return-object v0

    .line 1979685
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1979686
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v1, v2}, LX/DGN;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 1979687
    :sswitch_1
    check-cast p2, LX/48C;

    .line 1979688
    iget-object v0, p2, LX/48C;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1979689
    check-cast v1, LX/DGM;

    .line 1979690
    iget-object v2, p0, LX/DGN;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;

    iget-object p1, v1, LX/DGM;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979691
    iget-object p0, v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->e:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C8h;

    invoke-virtual {p0, v0, p1}, LX/C8h;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1979692
    const/4 p0, 0x1

    move v2, p0

    .line 1979693
    move v0, v2

    .line 1979694
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6bd759ae -> :sswitch_1
        -0x18f3e90c -> :sswitch_0
    .end sparse-switch
.end method
