.class public LX/Ehz;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ei2;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/Ei2;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2158937
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2158938
    iput-object p2, p0, LX/Ehz;->a:Ljava/util/List;

    .line 2158939
    iput-object p1, p0, LX/Ehz;->b:Landroid/content/Context;

    .line 2158940
    return-void
.end method

.method private a(I)LX/Ei2;
    .locals 1

    .prologue
    .line 2158962
    iget-object v0, p0, LX/Ehz;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ei2;

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2158960
    iget-object v0, p0, LX/Ehz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2158961
    invoke-direct {p0, p1}, LX/Ehz;->a(I)LX/Ei2;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2158959
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2158942
    if-nez p2, :cond_0

    .line 2158943
    iget-object v0, p0, LX/Ehz;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2158944
    const v1, 0x7f0300e9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2158945
    new-instance v1, LX/Ehy;

    invoke-direct {v1}, LX/Ehy;-><init>()V

    .line 2158946
    iput p1, v1, LX/Ehy;->c:I

    .line 2158947
    const v0, 0x7f0d054b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, LX/Ehy;->a:Landroid/widget/ImageView;

    .line 2158948
    const v0, 0x7f0d054c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, v1, LX/Ehy;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2158949
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 2158950
    :goto_0
    invoke-direct {p0, p1}, LX/Ehz;->a(I)LX/Ei2;

    move-result-object v1

    .line 2158951
    iget-object v2, v0, LX/Ehy;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2158952
    iget-object v3, v1, LX/Ei2;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2158953
    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2158954
    iget-object v0, v0, LX/Ehy;->a:Landroid/widget/ImageView;

    .line 2158955
    iget-object v2, v1, LX/Ei2;->b:Landroid/graphics/drawable/Drawable;

    move-object v1, v2

    .line 2158956
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2158957
    return-object p2

    .line 2158958
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ehy;

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 2158941
    const/4 v0, 0x1

    return v0
.end method
