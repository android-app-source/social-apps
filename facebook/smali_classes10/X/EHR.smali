.class public final LX/EHR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/views/RtcIncomingCallButtons;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/views/RtcIncomingCallButtons;)V
    .locals 0

    .prologue
    .line 2099498
    iput-object p1, p0, LX/EHR;->a:Lcom/facebook/rtc/views/RtcIncomingCallButtons;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x739d7583

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2099499
    iget-object v1, p0, LX/EHR;->a:Lcom/facebook/rtc/views/RtcIncomingCallButtons;

    iget-object v1, v1, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->g:LX/EBm;

    .line 2099500
    iget-object v3, v1, LX/EBm;->a:Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    .line 2099501
    const/4 p0, 0x3

    new-array p0, p0, [Ljava/lang/CharSequence;

    const/4 p1, 0x0

    const v1, 0x7f08077f

    invoke-virtual {v3, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p0, p1

    const/4 p1, 0x1

    const v1, 0x7f080780

    invoke-virtual {v3, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p0, p1

    const/4 p1, 0x2

    const v1, 0x7f080781

    invoke-virtual {v3, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, p0, p1

    .line 2099502
    new-instance p1, LX/EC5;

    invoke-direct {p1, v3}, LX/EC5;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2099503
    const/4 v1, 0x0

    invoke-static {v3, p0, p1, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;[Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2099504
    iget-object p0, v3, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aq:LX/2EJ;

    invoke-virtual {p0}, LX/2EJ;->show()V

    .line 2099505
    const v1, 0x1ceff4a6

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
