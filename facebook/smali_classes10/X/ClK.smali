.class public LX/ClK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/ClK;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Ckw;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/ClJ;",
            ">;>;"
        }
    .end annotation
.end field

.field public final b:LX/0So;

.field private final c:LX/1sz;

.field private final d:LX/0Uh;

.field private final e:Z


# direct methods
.method public constructor <init>(LX/0So;LX/1sz;LX/0Uh;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1932376
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1932377
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ClK;->a:Ljava/util/Map;

    .line 1932378
    iput-object p1, p0, LX/ClK;->b:LX/0So;

    .line 1932379
    iput-object p2, p0, LX/ClK;->c:LX/1sz;

    .line 1932380
    iput-object p3, p0, LX/ClK;->d:LX/0Uh;

    .line 1932381
    iget-object v0, p0, LX/ClK;->d:LX/0Uh;

    const/16 v1, 0x3f0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/ClK;->e:Z

    .line 1932382
    return-void
.end method

.method public static a(LX/0QB;)LX/ClK;
    .locals 6

    .prologue
    .line 1932319
    sget-object v0, LX/ClK;->f:LX/ClK;

    if-nez v0, :cond_1

    .line 1932320
    const-class v1, LX/ClK;

    monitor-enter v1

    .line 1932321
    :try_start_0
    sget-object v0, LX/ClK;->f:LX/ClK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1932322
    if-eqz v2, :cond_0

    .line 1932323
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1932324
    new-instance p0, LX/ClK;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/1sI;->a(LX/0QB;)LX/1sz;

    move-result-object v4

    check-cast v4, LX/1sz;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/ClK;-><init>(LX/0So;LX/1sz;LX/0Uh;)V

    .line 1932325
    move-object v0, p0

    .line 1932326
    sput-object v0, LX/ClK;->f:LX/ClK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1932327
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1932328
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1932329
    :cond_1
    sget-object v0, LX/ClK;->f:LX/ClK;

    return-object v0

    .line 1932330
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1932331
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/ClI;LX/ClJ;)V
    .locals 6

    .prologue
    .line 1932383
    iget-boolean v0, p0, LX/ClK;->e:Z

    if-nez v0, :cond_1

    .line 1932384
    :cond_0
    :goto_0
    return-void

    .line 1932385
    :cond_1
    iget-object v0, p0, LX/ClK;->c:LX/1sz;

    invoke-virtual {v0}, LX/1sz;->a()LX/2GO;

    move-result-object v0

    .line 1932386
    sget-object v1, LX/ClI;->LOAD_START:LX/ClI;

    if-ne p1, v1, :cond_2

    .line 1932387
    invoke-virtual {v0}, LX/2GO;->a()J

    move-result-wide v2

    iput-wide v2, p2, LX/ClJ;->k:J

    .line 1932388
    iget-wide v4, v0, LX/2GO;->b:J

    move-wide v2, v4

    .line 1932389
    iput-wide v2, p2, LX/ClJ;->m:J

    .line 1932390
    invoke-virtual {v0}, LX/2GO;->c()J

    move-result-wide v2

    iput-wide v2, p2, LX/ClJ;->n:J

    .line 1932391
    invoke-virtual {v0}, LX/2GO;->d()Z

    move-result v0

    iput-boolean v0, p2, LX/ClJ;->o:Z

    goto :goto_0

    .line 1932392
    :cond_2
    sget-object v1, LX/ClI;->LOAD_FINISH:LX/ClI;

    if-ne p1, v1, :cond_0

    .line 1932393
    invoke-virtual {v0}, LX/2GO;->a()J

    move-result-wide v2

    iput-wide v2, p2, LX/ClJ;->l:J

    .line 1932394
    invoke-virtual {v0}, LX/2GO;->d()Z

    move-result v0

    iput-boolean v0, p2, LX/ClJ;->p:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Ckw;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1932332
    iget-object v0, p0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1932333
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1932334
    :cond_0
    :goto_0
    return-void

    .line 1932335
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClJ;

    .line 1932336
    invoke-static {v0}, LX/ClJ;->a$redex0(LX/ClJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1932337
    iget-object v1, p0, LX/ClK;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/ClJ;->e:J

    goto :goto_0
.end method

.method public final b(LX/Ckw;)V
    .locals 14

    .prologue
    const-wide/16 v8, 0x0

    .line 1932338
    iget-object v0, p0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1932339
    if-nez v0, :cond_0

    .line 1932340
    :goto_0
    return-void

    .line 1932341
    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1932342
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ClJ;

    .line 1932343
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1932344
    const-wide/16 v12, 0x0

    .line 1932345
    iget-wide v10, v1, LX/ClJ;->e:J

    cmp-long v10, v10, v12

    if-lez v10, :cond_6

    iget-wide v10, v1, LX/ClJ;->f:J

    cmp-long v10, v10, v12

    if-lez v10, :cond_6

    iget-wide v10, v1, LX/ClJ;->g:J

    cmp-long v10, v10, v12

    if-lez v10, :cond_6

    const/4 v10, 0x1

    :goto_2
    move v3, v10

    .line 1932346
    if-eqz v3, :cond_1

    .line 1932347
    iget-boolean v3, v1, LX/ClJ;->d:Z

    if-nez v3, :cond_1

    .line 1932348
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1932349
    const-string v4, "block_id"

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932350
    const-string v0, "block_index_in_article"

    iget v4, v1, LX/ClJ;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932351
    const-string v0, "webview_type"

    iget-object v4, v1, LX/ClJ;->b:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932352
    const-string v0, "queue_time"

    invoke-static {v1}, LX/ClJ;->c$redex0(LX/ClJ;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932353
    const-string v0, "download_time"

    invoke-static {v1}, LX/ClJ;->d$redex0(LX/ClJ;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932354
    const-string v0, "onscreen_time"

    invoke-static {v1}, LX/ClJ;->e$redex0(LX/ClJ;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932355
    const-string v0, "user_wait_time_seconds"

    iget-object v4, p0, LX/ClK;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, LX/ClJ;->a$redex0(LX/ClJ;J)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932356
    const-string v4, "did_see_content"

    iget-wide v6, v1, LX/ClJ;->j:J

    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932357
    const-string v0, "onscreen_time"

    invoke-static {v1}, LX/ClJ;->e$redex0(LX/ClJ;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932358
    const-string v0, "failures_occurred"

    iget-boolean v4, v1, LX/ClJ;->c:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932359
    const-string v0, "queue_start_timestamp"

    iget-wide v4, v1, LX/ClJ;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932360
    const-string v0, "download_start_timestamp"

    iget-wide v4, v1, LX/ClJ;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932361
    const-string v0, "finished_downloading_raw_time"

    iget-wide v4, v1, LX/ClJ;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932362
    const-string v0, "onscreen_raw_time"

    iget-wide v4, v1, LX/ClJ;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932363
    const-string v0, "offscreen_raw_time"

    iget-wide v4, v1, LX/ClJ;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932364
    iget-wide v4, v1, LX/ClJ;->j:J

    cmp-long v0, v4, v8

    if-lez v0, :cond_2

    .line 1932365
    const-string v0, "first_frame_render_time"

    iget-wide v4, v1, LX/ClJ;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932366
    :cond_2
    iget-boolean v0, p0, LX/ClK;->e:Z

    if-eqz v0, :cond_3

    .line 1932367
    const-string v0, "load_start_available_memory"

    iget-wide v4, v1, LX/ClJ;->k:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932368
    const-string v0, "load_finish_available_memory"

    iget-wide v4, v1, LX/ClJ;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932369
    const-string v0, "total_memory"

    iget-wide v4, v1, LX/ClJ;->m:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932370
    const-string v0, "low_memory_threshold"

    iget-wide v4, v1, LX/ClJ;->n:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932371
    const-string v0, "load_start_low_memory"

    iget-boolean v4, v1, LX/ClJ;->o:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932372
    const-string v0, "load_finish_low_memory"

    iget-boolean v1, v1, LX/ClJ;->p:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1932373
    :cond_3
    const-string v0, "android_native_article_webview_perf"

    invoke-virtual {p1, v0, v3}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_1

    .line 1932374
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 1932375
    :cond_5
    iget-object v0, p0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_2
.end method

.method public final b(LX/Ckw;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1932302
    iget-object v0, p0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1932303
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1932304
    :cond_0
    :goto_0
    return-void

    .line 1932305
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClJ;

    .line 1932306
    invoke-static {v0}, LX/ClJ;->a$redex0(LX/ClJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1932307
    iget-object v1, p0, LX/ClK;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/ClJ;->f:J

    .line 1932308
    sget-object v1, LX/ClI;->LOAD_START:LX/ClI;

    invoke-direct {p0, v1, v0}, LX/ClK;->a(LX/ClI;LX/ClJ;)V

    goto :goto_0
.end method

.method public final c(LX/Ckw;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1932295
    iget-object v0, p0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1932296
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1932297
    :cond_0
    :goto_0
    return-void

    .line 1932298
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClJ;

    .line 1932299
    invoke-static {v0}, LX/ClJ;->a$redex0(LX/ClJ;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1932300
    iget-object v1, p0, LX/ClK;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/ClJ;->g:J

    .line 1932301
    sget-object v1, LX/ClI;->LOAD_FINISH:LX/ClI;

    invoke-direct {p0, v1, v0}, LX/ClK;->a(LX/ClI;LX/ClJ;)V

    goto :goto_0
.end method

.method public final g(LX/Ckw;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1932309
    iget-object v0, p0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1932310
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1932311
    :cond_0
    :goto_0
    return-void

    .line 1932312
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClJ;

    .line 1932313
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/ClJ;->c:Z

    goto :goto_0
.end method

.method public final h(LX/Ckw;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1932314
    iget-object v0, p0, LX/ClK;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1932315
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1932316
    :cond_0
    :goto_0
    return-void

    .line 1932317
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClJ;

    .line 1932318
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/ClJ;->d:Z

    goto :goto_0
.end method
