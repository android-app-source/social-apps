.class public final LX/DyT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DyU;


# direct methods
.method public constructor <init>(LX/DyU;)V
    .locals 0

    .prologue
    .line 2064642
    iput-object p1, p0, LX/DyT;->a:LX/DyU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x52d37bf3

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2064643
    iget-object v2, p0, LX/DyT;->a:LX/DyU;

    iget-object v2, v2, LX/DyU;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v2, v2, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iget-object v3, p0, LX/DyT;->a:LX/DyU;

    iget-object v3, v3, LX/DyU;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v3, v3, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064644
    iget-object p1, v3, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v3, p1

    .line 2064645
    if-eqz v3, :cond_2

    .line 2064646
    :goto_0
    iget-object v3, v2, LX/9j5;->a:LX/0Zb;

    const-string v4, "place_picker_removed_place"

    invoke-static {v2, v4}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p1, "close_out_explicit"

    invoke-virtual {v4, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2064647
    iget-object v0, p0, LX/DyT;->a:LX/DyU;

    iget-object v0, v0, LX/DyU;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2064648
    iget-object v0, p0, LX/DyT;->a:LX/DyU;

    iget-object v0, v0, LX/DyU;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->y:LX/92o;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/92o;->a(Ljava/lang/String;)V

    .line 2064649
    iget-object v0, p0, LX/DyT;->a:LX/DyU;

    iget-object v0, v0, LX/DyU;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    iget-object v0, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    .line 2064650
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2064651
    const-string v3, "extra_xed_location"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2064652
    iget-object v3, v0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064653
    iget-object v4, v3, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    move-object v3, v4

    .line 2064654
    if-eqz v3, :cond_0

    .line 2064655
    const-string v3, "media_id"

    iget-object v4, v0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064656
    iget-object p0, v4, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->n:Ljava/lang/String;

    move-object v4, p0

    .line 2064657
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2064658
    :cond_0
    iget-object v3, v0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064659
    iget-object v4, v3, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    move-object v3, v4

    .line 2064660
    if-eqz v3, :cond_1

    .line 2064661
    const-string v3, "launcher_type"

    iget-object v4, v0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064662
    iget-object p0, v4, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->l:Ljava/lang/String;

    move-object v4, p0

    .line 2064663
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2064664
    :cond_1
    iget-object v3, v0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 v4, -0x1

    invoke-virtual {v3, v4, v2}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(ILandroid/content/Intent;)V

    .line 2064665
    const v0, 0x112deff8

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2064666
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
