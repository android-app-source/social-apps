.class public final LX/EcK;
.super LX/EWj;
.source ""

# interfaces
.implements LX/EcJ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EWj",
        "<",
        "LX/EcK;",
        ">;",
        "LX/EcJ;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:I

.field private c:LX/EWc;

.field private d:LX/EWc;

.field private e:LX/EWc;

.field private f:I

.field public g:LX/EcW;

.field public h:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/EcW;",
            "LX/EcN;",
            "LX/EcM;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EcW;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/EZ2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ2",
            "<",
            "LX/EcW;",
            "LX/EcN;",
            "LX/EcM;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/Eca;

.field public l:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/Eca;",
            "LX/EcZ;",
            "LX/EcY;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/Ece;

.field public n:LX/EZ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EZ7",
            "<",
            "LX/Ece;",
            "LX/Ecd;",
            "LX/Ecc;",
            ">;"
        }
    .end annotation
.end field

.field private o:I

.field private p:I

.field public q:Z

.field private r:LX/EWc;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2146231
    invoke-direct {p0}, LX/EWj;-><init>()V

    .line 2146232
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcK;->c:LX/EWc;

    .line 2146233
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcK;->d:LX/EWc;

    .line 2146234
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcK;->e:LX/EWc;

    .line 2146235
    sget-object v0, LX/EcW;->c:LX/EcW;

    move-object v0, v0

    .line 2146236
    iput-object v0, p0, LX/EcK;->g:LX/EcW;

    .line 2146237
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcK;->i:Ljava/util/List;

    .line 2146238
    sget-object v0, LX/Eca;->c:LX/Eca;

    move-object v0, v0

    .line 2146239
    iput-object v0, p0, LX/EcK;->k:LX/Eca;

    .line 2146240
    sget-object v0, LX/Ece;->c:LX/Ece;

    move-object v0, v0

    .line 2146241
    iput-object v0, p0, LX/EcK;->m:LX/Ece;

    .line 2146242
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcK;->r:LX/EWc;

    .line 2146243
    invoke-direct {p0}, LX/EcK;->w()V

    .line 2146244
    return-void
.end method

.method public constructor <init>(LX/EYd;)V
    .locals 1

    .prologue
    .line 2146245
    invoke-direct {p0, p1}, LX/EWj;-><init>(LX/EYd;)V

    .line 2146246
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcK;->c:LX/EWc;

    .line 2146247
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcK;->d:LX/EWc;

    .line 2146248
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcK;->e:LX/EWc;

    .line 2146249
    sget-object v0, LX/EcW;->c:LX/EcW;

    move-object v0, v0

    .line 2146250
    iput-object v0, p0, LX/EcK;->g:LX/EcW;

    .line 2146251
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcK;->i:Ljava/util/List;

    .line 2146252
    sget-object v0, LX/Eca;->c:LX/Eca;

    move-object v0, v0

    .line 2146253
    iput-object v0, p0, LX/EcK;->k:LX/Eca;

    .line 2146254
    sget-object v0, LX/Ece;->c:LX/Ece;

    move-object v0, v0

    .line 2146255
    iput-object v0, p0, LX/EcK;->m:LX/Ece;

    .line 2146256
    sget-object v0, LX/EWc;->a:LX/EWc;

    iput-object v0, p0, LX/EcK;->r:LX/EWc;

    .line 2146257
    invoke-direct {p0}, LX/EcK;->w()V

    .line 2146258
    return-void
.end method

.method public static B(LX/EcK;)V
    .locals 2

    .prologue
    .line 2146259
    iget v0, p0, LX/EcK;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_0

    .line 2146260
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/EcK;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/EcK;->i:Ljava/util/List;

    .line 2146261
    iget v0, p0, LX/EcK;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, LX/EcK;->a:I

    .line 2146262
    :cond_0
    return-void
.end method

.method private C()LX/EZ2;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EZ2",
            "<",
            "LX/EcW;",
            "LX/EcN;",
            "LX/EcM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2146263
    iget-object v0, p0, LX/EcK;->j:LX/EZ2;

    if-nez v0, :cond_0

    .line 2146264
    new-instance v1, LX/EZ2;

    iget-object v2, p0, LX/EcK;->i:Ljava/util/List;

    iget v0, p0, LX/EcK;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v3

    .line 2146265
    iget-boolean v4, p0, LX/EWj;->c:Z

    move v4, v4

    .line 2146266
    invoke-direct {v1, v2, v0, v3, v4}, LX/EZ2;-><init>(Ljava/util/List;ZLX/EYd;Z)V

    iput-object v1, p0, LX/EcK;->j:LX/EZ2;

    .line 2146267
    const/4 v0, 0x0

    iput-object v0, p0, LX/EcK;->i:Ljava/util/List;

    .line 2146268
    :cond_0
    iget-object v0, p0, LX/EcK;->j:LX/EZ2;

    return-object v0

    .line 2146269
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/EWY;)LX/EcK;
    .locals 1

    .prologue
    .line 2146270
    instance-of v0, p1, LX/Ecf;

    if-eqz v0, :cond_0

    .line 2146271
    check-cast p1, LX/Ecf;

    invoke-virtual {p0, p1}, LX/EcK;->a(LX/Ecf;)LX/EcK;

    move-result-object p0

    .line 2146272
    :goto_0
    return-object p0

    .line 2146273
    :cond_0
    invoke-super {p0, p1}, LX/EWj;->a(LX/EWY;)LX/EWV;

    goto :goto_0
.end method

.method private d(LX/EWd;LX/EYZ;)LX/EcK;
    .locals 4

    .prologue
    .line 2146274
    const/4 v2, 0x0

    .line 2146275
    :try_start_0
    sget-object v0, LX/Ecf;->a:LX/EWZ;

    invoke-virtual {v0, p1, p2}, LX/EWZ;->a(LX/EWd;LX/EYZ;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ecf;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2146276
    if-eqz v0, :cond_0

    .line 2146277
    invoke-virtual {p0, v0}, LX/EcK;->a(LX/Ecf;)LX/EcK;

    .line 2146278
    :cond_0
    return-object p0

    .line 2146279
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2146280
    :try_start_1
    iget-object v0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    move-object v0, v0

    .line 2146281
    check-cast v0, LX/Ecf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2146282
    :try_start_2
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2146283
    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    if-eqz v1, :cond_1

    .line 2146284
    invoke-virtual {p0, v1}, LX/EcK;->a(LX/Ecf;)LX/EcK;

    :cond_1
    throw v0

    .line 2146285
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method private w()V
    .locals 4

    .prologue
    .line 2146286
    sget-boolean v0, LX/EWp;->b:Z

    if-eqz v0, :cond_2

    .line 2146287
    iget-object v0, p0, LX/EcK;->h:LX/EZ7;

    if-nez v0, :cond_0

    .line 2146288
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/EcK;->g:LX/EcW;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2146289
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2146290
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/EcK;->h:LX/EZ7;

    .line 2146291
    const/4 v0, 0x0

    iput-object v0, p0, LX/EcK;->g:LX/EcW;

    .line 2146292
    :cond_0
    invoke-direct {p0}, LX/EcK;->C()LX/EZ2;

    .line 2146293
    iget-object v0, p0, LX/EcK;->l:LX/EZ7;

    if-nez v0, :cond_1

    .line 2146294
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/EcK;->k:LX/Eca;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2146295
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2146296
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/EcK;->l:LX/EZ7;

    .line 2146297
    const/4 v0, 0x0

    iput-object v0, p0, LX/EcK;->k:LX/Eca;

    .line 2146298
    :cond_1
    iget-object v0, p0, LX/EcK;->n:LX/EZ7;

    if-nez v0, :cond_2

    .line 2146299
    new-instance v0, LX/EZ7;

    iget-object v1, p0, LX/EcK;->m:LX/Ece;

    invoke-virtual {p0}, LX/EWj;->s()LX/EYd;

    move-result-object v2

    .line 2146300
    iget-boolean v3, p0, LX/EWj;->c:Z

    move v3, v3

    .line 2146301
    invoke-direct {v0, v1, v2, v3}, LX/EZ7;-><init>(LX/EWp;LX/EYd;Z)V

    iput-object v0, p0, LX/EcK;->n:LX/EZ7;

    .line 2146302
    const/4 v0, 0x0

    iput-object v0, p0, LX/EcK;->m:LX/Ece;

    .line 2146303
    :cond_2
    return-void
.end method

.method public static x()LX/EcK;
    .locals 1

    .prologue
    .line 2146304
    new-instance v0, LX/EcK;

    invoke-direct {v0}, LX/EcK;-><init>()V

    return-object v0
.end method

.method private y()LX/EcK;
    .locals 2

    .prologue
    .line 2146305
    invoke-static {}, LX/EcK;->x()LX/EcK;

    move-result-object v0

    invoke-virtual {p0}, LX/EcK;->m()LX/Ecf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EcK;->a(LX/Ecf;)LX/EcK;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/EWY;)LX/EWV;
    .locals 1

    .prologue
    .line 2146427
    invoke-direct {p0, p1}, LX/EcK;->d(LX/EWY;)LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/EWd;LX/EYZ;)LX/EWV;
    .locals 1

    .prologue
    .line 2146306
    invoke-direct {p0, p1, p2}, LX/EcK;->d(LX/EWd;LX/EYZ;)LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)LX/EcK;
    .locals 1

    .prologue
    .line 2146307
    iget v0, p0, LX/EcK;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EcK;->a:I

    .line 2146308
    iput p1, p0, LX/EcK;->b:I

    .line 2146309
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146310
    return-object p0
.end method

.method public final a(ILX/EcW;)LX/EcK;
    .locals 1

    .prologue
    .line 2146119
    iget-object v0, p0, LX/EcK;->j:LX/EZ2;

    if-nez v0, :cond_1

    .line 2146120
    if-nez p2, :cond_0

    .line 2146121
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146122
    :cond_0
    invoke-static {p0}, LX/EcK;->B(LX/EcK;)V

    .line 2146123
    iget-object v0, p0, LX/EcK;->i:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2146124
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146125
    :goto_0
    return-object p0

    .line 2146126
    :cond_1
    iget-object v0, p0, LX/EcK;->j:LX/EZ2;

    invoke-virtual {v0, p1, p2}, LX/EZ2;->a(ILX/EWp;)LX/EZ2;

    goto :goto_0
.end method

.method public final a(LX/EWc;)LX/EcK;
    .locals 1

    .prologue
    .line 2146311
    if-nez p1, :cond_0

    .line 2146312
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146313
    :cond_0
    iget v0, p0, LX/EcK;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EcK;->a:I

    .line 2146314
    iput-object p1, p0, LX/EcK;->c:LX/EWc;

    .line 2146315
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146316
    return-object p0
.end method

.method public final a(LX/EcW;)LX/EcK;
    .locals 1

    .prologue
    .line 2146317
    iget-object v0, p0, LX/EcK;->h:LX/EZ7;

    if-nez v0, :cond_1

    .line 2146318
    if-nez p1, :cond_0

    .line 2146319
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146320
    :cond_0
    iput-object p1, p0, LX/EcK;->g:LX/EcW;

    .line 2146321
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146322
    :goto_0
    iget v0, p0, LX/EcK;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, LX/EcK;->a:I

    .line 2146323
    return-object p0

    .line 2146324
    :cond_1
    iget-object v0, p0, LX/EcK;->h:LX/EZ7;

    invoke-virtual {v0, p1}, LX/EZ7;->a(LX/EWp;)LX/EZ7;

    goto :goto_0
.end method

.method public final a(LX/Ece;)LX/EcK;
    .locals 1

    .prologue
    .line 2146325
    iget-object v0, p0, LX/EcK;->n:LX/EZ7;

    if-nez v0, :cond_1

    .line 2146326
    if-nez p1, :cond_0

    .line 2146327
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146328
    :cond_0
    iput-object p1, p0, LX/EcK;->m:LX/Ece;

    .line 2146329
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146330
    :goto_0
    iget v0, p0, LX/EcK;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, LX/EcK;->a:I

    .line 2146331
    return-object p0

    .line 2146332
    :cond_1
    iget-object v0, p0, LX/EcK;->n:LX/EZ7;

    invoke-virtual {v0, p1}, LX/EZ7;->a(LX/EWp;)LX/EZ7;

    goto :goto_0
.end method

.method public final a(LX/Ecf;)LX/EcK;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2146333
    sget-object v1, LX/Ecf;->c:LX/Ecf;

    move-object v1, v1

    .line 2146334
    if-ne p1, v1, :cond_0

    .line 2146335
    :goto_0
    return-object p0

    .line 2146336
    :cond_0
    const/4 v1, 0x1

    .line 2146337
    iget v2, p1, LX/Ecf;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_12

    :goto_1
    move v1, v1

    .line 2146338
    if-eqz v1, :cond_1

    .line 2146339
    iget v1, p1, LX/Ecf;->sessionVersion_:I

    move v1, v1

    .line 2146340
    invoke-virtual {p0, v1}, LX/EcK;->a(I)LX/EcK;

    .line 2146341
    :cond_1
    iget v1, p1, LX/Ecf;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_13

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2146342
    if-eqz v1, :cond_2

    .line 2146343
    iget-object v1, p1, LX/Ecf;->localIdentityPublic_:LX/EWc;

    move-object v1, v1

    .line 2146344
    invoke-virtual {p0, v1}, LX/EcK;->a(LX/EWc;)LX/EcK;

    .line 2146345
    :cond_2
    invoke-virtual {p1}, LX/Ecf;->o()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2146346
    iget-object v1, p1, LX/Ecf;->remoteIdentityPublic_:LX/EWc;

    move-object v1, v1

    .line 2146347
    invoke-virtual {p0, v1}, LX/EcK;->b(LX/EWc;)LX/EcK;

    .line 2146348
    :cond_3
    iget v1, p1, LX/Ecf;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_14

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 2146349
    if-eqz v1, :cond_4

    .line 2146350
    iget-object v1, p1, LX/Ecf;->rootKey_:LX/EWc;

    move-object v1, v1

    .line 2146351
    invoke-virtual {p0, v1}, LX/EcK;->c(LX/EWc;)LX/EcK;

    .line 2146352
    :cond_4
    iget v1, p1, LX/Ecf;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_15

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 2146353
    if-eqz v1, :cond_5

    .line 2146354
    iget v1, p1, LX/Ecf;->previousCounter_:I

    move v1, v1

    .line 2146355
    invoke-virtual {p0, v1}, LX/EcK;->b(I)LX/EcK;

    .line 2146356
    :cond_5
    invoke-virtual {p1}, LX/Ecf;->y()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2146357
    iget-object v1, p1, LX/Ecf;->senderChain_:LX/EcW;

    move-object v1, v1

    .line 2146358
    iget-object v2, p0, LX/EcK;->h:LX/EZ7;

    if-nez v2, :cond_17

    .line 2146359
    iget v2, p0, LX/EcK;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_16

    iget-object v2, p0, LX/EcK;->g:LX/EcW;

    .line 2146360
    sget-object v3, LX/EcW;->c:LX/EcW;

    move-object v3, v3

    .line 2146361
    if-eq v2, v3, :cond_16

    .line 2146362
    iget-object v2, p0, LX/EcK;->g:LX/EcW;

    invoke-static {v2}, LX/EcW;->a(LX/EcW;)LX/EcN;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/EcN;->a(LX/EcW;)LX/EcN;

    move-result-object v2

    invoke-virtual {v2}, LX/EcN;->m()LX/EcW;

    move-result-object v2

    iput-object v2, p0, LX/EcK;->g:LX/EcW;

    .line 2146363
    :goto_5
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146364
    :goto_6
    iget v2, p0, LX/EcK;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, LX/EcK;->a:I

    .line 2146365
    :cond_6
    iget-object v1, p0, LX/EcK;->j:LX/EZ2;

    if-nez v1, :cond_f

    .line 2146366
    iget-object v0, p1, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2146367
    iget-object v0, p0, LX/EcK;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2146368
    iget-object v0, p1, LX/Ecf;->receiverChains_:Ljava/util/List;

    iput-object v0, p0, LX/EcK;->i:Ljava/util/List;

    .line 2146369
    iget v0, p0, LX/EcK;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, LX/EcK;->a:I

    .line 2146370
    :goto_7
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146371
    :cond_7
    :goto_8
    iget v0, p1, LX/Ecf;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_18

    const/4 v0, 0x1

    :goto_9
    move v0, v0

    .line 2146372
    if-eqz v0, :cond_8

    .line 2146373
    iget-object v0, p1, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    move-object v0, v0

    .line 2146374
    iget-object v1, p0, LX/EcK;->l:LX/EZ7;

    if-nez v1, :cond_1a

    .line 2146375
    iget v1, p0, LX/EcK;->a:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_19

    iget-object v1, p0, LX/EcK;->k:LX/Eca;

    .line 2146376
    sget-object v2, LX/Eca;->c:LX/Eca;

    move-object v2, v2

    .line 2146377
    if-eq v1, v2, :cond_19

    .line 2146378
    iget-object v1, p0, LX/EcK;->k:LX/Eca;

    invoke-static {v1}, LX/Eca;->a(LX/Eca;)LX/EcZ;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/EcZ;->a(LX/Eca;)LX/EcZ;

    move-result-object v1

    invoke-virtual {v1}, LX/EcZ;->l()LX/Eca;

    move-result-object v1

    iput-object v1, p0, LX/EcK;->k:LX/Eca;

    .line 2146379
    :goto_a
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146380
    :goto_b
    iget v1, p0, LX/EcK;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, LX/EcK;->a:I

    .line 2146381
    :cond_8
    invoke-virtual {p1}, LX/Ecf;->D()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2146382
    iget-object v0, p1, LX/Ecf;->pendingPreKey_:LX/Ece;

    move-object v0, v0

    .line 2146383
    iget-object v1, p0, LX/EcK;->n:LX/EZ7;

    if-nez v1, :cond_1c

    .line 2146384
    iget v1, p0, LX/EcK;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_1b

    iget-object v1, p0, LX/EcK;->m:LX/Ece;

    .line 2146385
    sget-object v2, LX/Ece;->c:LX/Ece;

    move-object v2, v2

    .line 2146386
    if-eq v1, v2, :cond_1b

    .line 2146387
    iget-object v1, p0, LX/EcK;->m:LX/Ece;

    invoke-static {v1}, LX/Ece;->a(LX/Ece;)LX/Ecd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Ecd;->a(LX/Ece;)LX/Ecd;

    move-result-object v1

    invoke-virtual {v1}, LX/Ecd;->m()LX/Ece;

    move-result-object v1

    iput-object v1, p0, LX/EcK;->m:LX/Ece;

    .line 2146388
    :goto_c
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146389
    :goto_d
    iget v1, p0, LX/EcK;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, LX/EcK;->a:I

    .line 2146390
    :cond_9
    iget v0, p1, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_1d

    const/4 v0, 0x1

    :goto_e
    move v0, v0

    .line 2146391
    if-eqz v0, :cond_a

    .line 2146392
    iget v0, p1, LX/Ecf;->remoteRegistrationId_:I

    move v0, v0

    .line 2146393
    invoke-virtual {p0, v0}, LX/EcK;->d(I)LX/EcK;

    .line 2146394
    :cond_a
    iget v0, p1, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_1e

    const/4 v0, 0x1

    :goto_f
    move v0, v0

    .line 2146395
    if-eqz v0, :cond_b

    .line 2146396
    iget v0, p1, LX/Ecf;->localRegistrationId_:I

    move v0, v0

    .line 2146397
    invoke-virtual {p0, v0}, LX/EcK;->e(I)LX/EcK;

    .line 2146398
    :cond_b
    iget v0, p1, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_1f

    const/4 v0, 0x1

    :goto_10
    move v0, v0

    .line 2146399
    if-eqz v0, :cond_c

    .line 2146400
    iget-boolean v0, p1, LX/Ecf;->needsRefresh_:Z

    move v0, v0

    .line 2146401
    iget v1, p0, LX/EcK;->a:I

    or-int/lit16 v1, v1, 0x800

    iput v1, p0, LX/EcK;->a:I

    .line 2146402
    iput-boolean v0, p0, LX/EcK;->q:Z

    .line 2146403
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146404
    :cond_c
    iget v0, p1, LX/Ecf;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_20

    const/4 v0, 0x1

    :goto_11
    move v0, v0

    .line 2146405
    if-eqz v0, :cond_d

    .line 2146406
    iget-object v0, p1, LX/Ecf;->aliceBaseKey_:LX/EWc;

    move-object v0, v0

    .line 2146407
    invoke-virtual {p0, v0}, LX/EcK;->d(LX/EWc;)LX/EcK;

    .line 2146408
    :cond_d
    invoke-virtual {p1}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/EWj;->c(LX/EZQ;)LX/EWj;

    goto/16 :goto_0

    .line 2146409
    :cond_e
    invoke-static {p0}, LX/EcK;->B(LX/EcK;)V

    .line 2146410
    iget-object v0, p0, LX/EcK;->i:Ljava/util/List;

    iget-object v1, p1, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    .line 2146411
    :cond_f
    iget-object v1, p1, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 2146412
    iget-object v1, p0, LX/EcK;->j:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->d()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 2146413
    iget-object v1, p0, LX/EcK;->j:LX/EZ2;

    invoke-virtual {v1}, LX/EZ2;->b()V

    .line 2146414
    iput-object v0, p0, LX/EcK;->j:LX/EZ2;

    .line 2146415
    iget-object v1, p1, LX/Ecf;->receiverChains_:Ljava/util/List;

    iput-object v1, p0, LX/EcK;->i:Ljava/util/List;

    .line 2146416
    iget v1, p0, LX/EcK;->a:I

    and-int/lit8 v1, v1, -0x41

    iput v1, p0, LX/EcK;->a:I

    .line 2146417
    sget-boolean v1, LX/EWp;->b:Z

    if-eqz v1, :cond_10

    invoke-direct {p0}, LX/EcK;->C()LX/EZ2;

    move-result-object v0

    :cond_10
    iput-object v0, p0, LX/EcK;->j:LX/EZ2;

    goto/16 :goto_8

    .line 2146418
    :cond_11
    iget-object v0, p0, LX/EcK;->j:LX/EZ2;

    iget-object v1, p1, LX/Ecf;->receiverChains_:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/EZ2;->a(Ljava/lang/Iterable;)LX/EZ2;

    goto/16 :goto_8

    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_13
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_14
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_15
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 2146419
    :cond_16
    iput-object v1, p0, LX/EcK;->g:LX/EcW;

    goto/16 :goto_5

    .line 2146420
    :cond_17
    iget-object v2, p0, LX/EcK;->h:LX/EZ7;

    invoke-virtual {v2, v1}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto/16 :goto_6

    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_9

    .line 2146421
    :cond_19
    iput-object v0, p0, LX/EcK;->k:LX/Eca;

    goto/16 :goto_a

    .line 2146422
    :cond_1a
    iget-object v1, p0, LX/EcK;->l:LX/EZ7;

    invoke-virtual {v1, v0}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto/16 :goto_b

    .line 2146423
    :cond_1b
    iput-object v0, p0, LX/EcK;->m:LX/Ece;

    goto/16 :goto_c

    .line 2146424
    :cond_1c
    iget-object v1, p0, LX/EcK;->n:LX/EZ7;

    invoke-virtual {v1, v0}, LX/EZ7;->b(LX/EWp;)LX/EZ7;

    goto/16 :goto_d

    :cond_1d
    const/4 v0, 0x0

    goto/16 :goto_e

    :cond_1e
    const/4 v0, 0x0

    goto/16 :goto_f

    :cond_1f
    const/4 v0, 0x0

    goto/16 :goto_10

    :cond_20
    const/4 v0, 0x0

    goto/16 :goto_11
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2146425
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic b(LX/EWd;LX/EYZ;)LX/EWS;
    .locals 1

    .prologue
    .line 2146426
    invoke-direct {p0, p1, p2}, LX/EcK;->d(LX/EWd;LX/EYZ;)LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/EWV;
    .locals 1

    .prologue
    .line 2146226
    invoke-direct {p0}, LX/EcK;->y()LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)LX/EcK;
    .locals 1

    .prologue
    .line 2146227
    iget v0, p0, LX/EcK;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, LX/EcK;->a:I

    .line 2146228
    iput p1, p0, LX/EcK;->f:I

    .line 2146229
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146230
    return-object p0
.end method

.method public final b(LX/EWc;)LX/EcK;
    .locals 1

    .prologue
    .line 2146094
    if-nez p1, :cond_0

    .line 2146095
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146096
    :cond_0
    iget v0, p0, LX/EcK;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EcK;->a:I

    .line 2146097
    iput-object p1, p0, LX/EcK;->d:LX/EWc;

    .line 2146098
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146099
    return-object p0
.end method

.method public final b(LX/EcW;)LX/EcK;
    .locals 1

    .prologue
    .line 2146100
    iget-object v0, p0, LX/EcK;->j:LX/EZ2;

    if-nez v0, :cond_1

    .line 2146101
    if-nez p1, :cond_0

    .line 2146102
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146103
    :cond_0
    invoke-static {p0}, LX/EcK;->B(LX/EcK;)V

    .line 2146104
    iget-object v0, p0, LX/EcK;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2146105
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146106
    :goto_0
    return-object p0

    .line 2146107
    :cond_1
    iget-object v0, p0, LX/EcK;->j:LX/EZ2;

    invoke-virtual {v0, p1}, LX/EZ2;->a(LX/EWp;)LX/EZ2;

    goto :goto_0
.end method

.method public final synthetic c(LX/EWd;LX/EYZ;)LX/EWR;
    .locals 1

    .prologue
    .line 2146108
    invoke-direct {p0, p1, p2}, LX/EcK;->d(LX/EWd;LX/EYZ;)LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()LX/EWS;
    .locals 1

    .prologue
    .line 2146109
    invoke-direct {p0}, LX/EcK;->y()LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/EWY;)LX/EWU;
    .locals 1

    .prologue
    .line 2146110
    invoke-direct {p0, p1}, LX/EcK;->d(LX/EWY;)LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/EWc;)LX/EcK;
    .locals 1

    .prologue
    .line 2146111
    if-nez p1, :cond_0

    .line 2146112
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146113
    :cond_0
    iget v0, p0, LX/EcK;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EcK;->a:I

    .line 2146114
    iput-object p1, p0, LX/EcK;->e:LX/EWc;

    .line 2146115
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146116
    return-object p0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2146117
    invoke-direct {p0}, LX/EcK;->y()LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/EYn;
    .locals 3

    .prologue
    .line 2146118
    sget-object v0, LX/Eck;->b:LX/EYn;

    const-class v1, LX/Ecf;

    const-class v2, LX/EcK;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)LX/EcK;
    .locals 1

    .prologue
    .line 2146127
    iget v0, p0, LX/EcK;->a:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, LX/EcK;->a:I

    .line 2146128
    iput p1, p0, LX/EcK;->o:I

    .line 2146129
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146130
    return-object p0
.end method

.method public final d(LX/EWc;)LX/EcK;
    .locals 1

    .prologue
    .line 2146131
    if-nez p1, :cond_0

    .line 2146132
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2146133
    :cond_0
    iget v0, p0, LX/EcK;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, LX/EcK;->a:I

    .line 2146134
    iput-object p1, p0, LX/EcK;->r:LX/EWc;

    .line 2146135
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146136
    return-object p0
.end method

.method public final e()LX/EYF;
    .locals 1

    .prologue
    .line 2146137
    sget-object v0, LX/Eck;->a:LX/EYF;

    return-object v0
.end method

.method public final e(I)LX/EcK;
    .locals 1

    .prologue
    .line 2146138
    iget v0, p0, LX/EcK;->a:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, LX/EcK;->a:I

    .line 2146139
    iput p1, p0, LX/EcK;->p:I

    .line 2146140
    invoke-virtual {p0}, LX/EWj;->t()V

    .line 2146141
    return-object p0
.end method

.method public final synthetic f()LX/EWj;
    .locals 1

    .prologue
    .line 2146142
    invoke-direct {p0}, LX/EcK;->y()LX/EcK;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic h()LX/EWY;
    .locals 1

    .prologue
    .line 2146143
    invoke-virtual {p0}, LX/EcK;->m()LX/Ecf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic i()LX/EWY;
    .locals 1

    .prologue
    .line 2146144
    invoke-virtual {p0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j()LX/EWW;
    .locals 1

    .prologue
    .line 2146145
    invoke-virtual {p0}, LX/EcK;->m()LX/Ecf;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()LX/EWW;
    .locals 1

    .prologue
    .line 2146146
    invoke-virtual {p0}, LX/EcK;->l()LX/Ecf;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Ecf;
    .locals 2

    .prologue
    .line 2146147
    invoke-virtual {p0}, LX/EcK;->m()LX/Ecf;

    move-result-object v0

    .line 2146148
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2146149
    invoke-static {v0}, LX/EWV;->b(LX/EWY;)LX/EZL;

    move-result-object v0

    throw v0

    .line 2146150
    :cond_0
    return-object v0
.end method

.method public final m()LX/Ecf;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2146151
    new-instance v2, LX/Ecf;

    invoke-direct {v2, p0}, LX/Ecf;-><init>(LX/EWj;)V

    .line 2146152
    iget v3, p0, LX/EcK;->a:I

    .line 2146153
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_10

    .line 2146154
    :goto_0
    iget v1, p0, LX/EcK;->b:I

    .line 2146155
    iput v1, v2, LX/Ecf;->sessionVersion_:I

    .line 2146156
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 2146157
    or-int/lit8 v0, v0, 0x2

    .line 2146158
    :cond_0
    iget-object v1, p0, LX/EcK;->c:LX/EWc;

    .line 2146159
    iput-object v1, v2, LX/Ecf;->localIdentityPublic_:LX/EWc;

    .line 2146160
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    .line 2146161
    or-int/lit8 v0, v0, 0x4

    .line 2146162
    :cond_1
    iget-object v1, p0, LX/EcK;->d:LX/EWc;

    .line 2146163
    iput-object v1, v2, LX/Ecf;->remoteIdentityPublic_:LX/EWc;

    .line 2146164
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    .line 2146165
    or-int/lit8 v0, v0, 0x8

    .line 2146166
    :cond_2
    iget-object v1, p0, LX/EcK;->e:LX/EWc;

    .line 2146167
    iput-object v1, v2, LX/Ecf;->rootKey_:LX/EWc;

    .line 2146168
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    .line 2146169
    or-int/lit8 v0, v0, 0x10

    .line 2146170
    :cond_3
    iget v1, p0, LX/EcK;->f:I

    .line 2146171
    iput v1, v2, LX/Ecf;->previousCounter_:I

    .line 2146172
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_f

    .line 2146173
    or-int/lit8 v0, v0, 0x20

    move v1, v0

    .line 2146174
    :goto_1
    iget-object v0, p0, LX/EcK;->h:LX/EZ7;

    if-nez v0, :cond_b

    .line 2146175
    iget-object v0, p0, LX/EcK;->g:LX/EcW;

    .line 2146176
    iput-object v0, v2, LX/Ecf;->senderChain_:LX/EcW;

    .line 2146177
    :goto_2
    iget-object v0, p0, LX/EcK;->j:LX/EZ2;

    if-nez v0, :cond_c

    .line 2146178
    iget v0, p0, LX/EcK;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v4, 0x40

    if-ne v0, v4, :cond_4

    .line 2146179
    iget-object v0, p0, LX/EcK;->i:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EcK;->i:Ljava/util/List;

    .line 2146180
    iget v0, p0, LX/EcK;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, LX/EcK;->a:I

    .line 2146181
    :cond_4
    iget-object v0, p0, LX/EcK;->i:Ljava/util/List;

    .line 2146182
    iput-object v0, v2, LX/Ecf;->receiverChains_:Ljava/util/List;

    .line 2146183
    :goto_3
    and-int/lit16 v0, v3, 0x80

    const/16 v4, 0x80

    if-ne v0, v4, :cond_5

    .line 2146184
    or-int/lit8 v1, v1, 0x40

    .line 2146185
    :cond_5
    iget-object v0, p0, LX/EcK;->l:LX/EZ7;

    if-nez v0, :cond_d

    .line 2146186
    iget-object v0, p0, LX/EcK;->k:LX/Eca;

    .line 2146187
    iput-object v0, v2, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    .line 2146188
    :goto_4
    and-int/lit16 v0, v3, 0x100

    const/16 v4, 0x100

    if-ne v0, v4, :cond_6

    .line 2146189
    or-int/lit16 v1, v1, 0x80

    .line 2146190
    :cond_6
    iget-object v0, p0, LX/EcK;->n:LX/EZ7;

    if-nez v0, :cond_e

    .line 2146191
    iget-object v0, p0, LX/EcK;->m:LX/Ece;

    .line 2146192
    iput-object v0, v2, LX/Ecf;->pendingPreKey_:LX/Ece;

    .line 2146193
    :goto_5
    and-int/lit16 v0, v3, 0x200

    const/16 v4, 0x200

    if-ne v0, v4, :cond_7

    .line 2146194
    or-int/lit16 v1, v1, 0x100

    .line 2146195
    :cond_7
    iget v0, p0, LX/EcK;->o:I

    .line 2146196
    iput v0, v2, LX/Ecf;->remoteRegistrationId_:I

    .line 2146197
    and-int/lit16 v0, v3, 0x400

    const/16 v4, 0x400

    if-ne v0, v4, :cond_8

    .line 2146198
    or-int/lit16 v1, v1, 0x200

    .line 2146199
    :cond_8
    iget v0, p0, LX/EcK;->p:I

    .line 2146200
    iput v0, v2, LX/Ecf;->localRegistrationId_:I

    .line 2146201
    and-int/lit16 v0, v3, 0x800

    const/16 v4, 0x800

    if-ne v0, v4, :cond_9

    .line 2146202
    or-int/lit16 v1, v1, 0x400

    .line 2146203
    :cond_9
    iget-boolean v0, p0, LX/EcK;->q:Z

    .line 2146204
    iput-boolean v0, v2, LX/Ecf;->needsRefresh_:Z

    .line 2146205
    and-int/lit16 v0, v3, 0x1000

    const/16 v3, 0x1000

    if-ne v0, v3, :cond_a

    .line 2146206
    or-int/lit16 v1, v1, 0x800

    .line 2146207
    :cond_a
    iget-object v0, p0, LX/EcK;->r:LX/EWc;

    .line 2146208
    iput-object v0, v2, LX/Ecf;->aliceBaseKey_:LX/EWc;

    .line 2146209
    iput v1, v2, LX/Ecf;->bitField0_:I

    .line 2146210
    invoke-virtual {p0}, LX/EWj;->p()V

    .line 2146211
    return-object v2

    .line 2146212
    :cond_b
    iget-object v0, p0, LX/EcK;->h:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/EcW;

    .line 2146213
    iput-object v0, v2, LX/Ecf;->senderChain_:LX/EcW;

    .line 2146214
    goto :goto_2

    .line 2146215
    :cond_c
    iget-object v0, p0, LX/EcK;->j:LX/EZ2;

    invoke-virtual {v0}, LX/EZ2;->f()Ljava/util/List;

    move-result-object v0

    .line 2146216
    iput-object v0, v2, LX/Ecf;->receiverChains_:Ljava/util/List;

    .line 2146217
    goto :goto_3

    .line 2146218
    :cond_d
    iget-object v0, p0, LX/EcK;->l:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/Eca;

    .line 2146219
    iput-object v0, v2, LX/Ecf;->pendingKeyExchange_:LX/Eca;

    .line 2146220
    goto :goto_4

    .line 2146221
    :cond_e
    iget-object v0, p0, LX/EcK;->n:LX/EZ7;

    invoke-virtual {v0}, LX/EZ7;->d()LX/EWp;

    move-result-object v0

    check-cast v0, LX/Ece;

    .line 2146222
    iput-object v0, v2, LX/Ecf;->pendingPreKey_:LX/Ece;

    .line 2146223
    goto :goto_5

    :cond_f
    move v1, v0

    goto/16 :goto_1

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2146224
    sget-object v0, LX/Ecf;->c:LX/Ecf;

    move-object v0, v0

    .line 2146225
    return-object v0
.end method
