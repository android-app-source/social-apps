.class public LX/D9L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/KeepGettersAndSetters;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/WindowManager;

.field private b:Landroid/view/WindowManager$LayoutParams;

.field private c:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:Landroid/os/Handler;

.field private f:Landroid/view/View$OnTouchListener;

.field private g:I

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;Landroid/view/WindowManager$LayoutParams;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1970036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970037
    iput-object p1, p0, LX/D9L;->a:Landroid/view/WindowManager;

    .line 1970038
    iput-object p2, p0, LX/D9L;->b:Landroid/view/WindowManager$LayoutParams;

    .line 1970039
    iput-boolean v2, p0, LX/D9L;->h:Z

    .line 1970040
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/D9L;->e:Landroid/os/Handler;

    .line 1970041
    invoke-virtual {p0, v2}, LX/D9L;->setTouchable(Z)V

    .line 1970042
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/D9L;->setFocusable(Z)V

    .line 1970043
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 1970026
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1970027
    if-eqz p2, :cond_1

    .line 1970028
    iget v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/2addr v0, p1

    .line 1970029
    :goto_0
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    if-eq v0, v2, :cond_0

    .line 1970030
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1970031
    invoke-virtual {p0, v1}, LX/D9L;->setLayoutParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 1970032
    :cond_0
    return-void

    .line 1970033
    :cond_1
    iget v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public getAlpha()F
    .locals 1

    .prologue
    .line 1970034
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 1970035
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    return v0
.end method

.method public getLayoutParams()Landroid/view/WindowManager$LayoutParams;
    .locals 1

    .prologue
    .line 1969978
    iget-boolean v0, p0, LX/D9L;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/D9L;->g:I

    if-nez v0, :cond_0

    .line 1969979
    iget-object v0, p0, LX/D9L;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 1969980
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/D9L;->b:Landroid/view/WindowManager$LayoutParams;

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 1970044
    iget-object v0, p0, LX/D9L;->c:Landroid/view/View;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 1970045
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    return v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 1970046
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 1970020
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    return v0
.end method

.method public setAlpha(F)V
    .locals 2

    .prologue
    .line 1970021
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1970022
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    .line 1970023
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    .line 1970024
    invoke-virtual {p0, v0}, LX/D9L;->setLayoutParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 1970025
    :cond_0
    return-void
.end method

.method public setFocusable(Z)V
    .locals 2

    .prologue
    .line 1970017
    const/16 v1, 0x8

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, LX/D9L;->a(IZ)V

    .line 1970018
    return-void

    .line 1970019
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setHeight(I)V
    .locals 2

    .prologue
    .line 1970012
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1970013
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v1, p1, :cond_0

    .line 1970014
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1970015
    invoke-virtual {p0, v0}, LX/D9L;->setLayoutParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 1970016
    :cond_0
    return-void
.end method

.method public setIsIMFocusable(Z)V
    .locals 1

    .prologue
    .line 1970010
    const/high16 v0, 0x20000

    invoke-direct {p0, v0, p1}, LX/D9L;->a(IZ)V

    .line 1970011
    return-void
.end method

.method public setLayoutParams(Landroid/view/WindowManager$LayoutParams;)V
    .locals 2

    .prologue
    .line 1970004
    iget-boolean v0, p0, LX/D9L;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/D9L;->g:I

    if-nez v0, :cond_0

    .line 1970005
    iget-object v0, p0, LX/D9L;->a:Landroid/view/WindowManager;

    iget-object v1, p0, LX/D9L;->c:Landroid/view/View;

    invoke-interface {v0, v1, p1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1970006
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/D9L;->h:Z

    .line 1970007
    :goto_0
    return-void

    .line 1970008
    :cond_0
    iput-object p1, p0, LX/D9L;->b:Landroid/view/WindowManager$LayoutParams;

    .line 1970009
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/D9L;->h:Z

    goto :goto_0
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 2

    .prologue
    .line 1970001
    iput-object p1, p0, LX/D9L;->f:Landroid/view/View$OnTouchListener;

    .line 1970002
    iget-object v0, p0, LX/D9L;->c:Landroid/view/View;

    iget-object v1, p0, LX/D9L;->f:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1970003
    return-void
.end method

.method public setTouchable(Z)V
    .locals 2

    .prologue
    .line 1969998
    const/16 v1, 0x10

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, LX/D9L;->a(IZ)V

    .line 1969999
    return-void

    .line 1970000
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setWidth(I)V
    .locals 2

    .prologue
    .line 1969993
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1969994
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v1, p1, :cond_0

    .line 1969995
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1969996
    invoke-virtual {p0, v0}, LX/D9L;->setLayoutParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 1969997
    :cond_0
    return-void
.end method

.method public setX(I)V
    .locals 2

    .prologue
    .line 1969987
    iget-object v0, p0, LX/D9L;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1969988
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1969989
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    if-eq v1, p1, :cond_0

    .line 1969990
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1969991
    invoke-virtual {p0, v0}, LX/D9L;->setLayoutParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 1969992
    :cond_0
    return-void
.end method

.method public setY(I)V
    .locals 2

    .prologue
    .line 1969981
    iget-object v0, p0, LX/D9L;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1969982
    invoke-virtual {p0}, LX/D9L;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1969983
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    if-eq v1, p1, :cond_0

    .line 1969984
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1969985
    invoke-virtual {p0, v0}, LX/D9L;->setLayoutParams(Landroid/view/WindowManager$LayoutParams;)V

    .line 1969986
    :cond_0
    return-void
.end method
