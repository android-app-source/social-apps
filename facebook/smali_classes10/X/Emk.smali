.class public LX/Emk;
.super LX/3sJ;
.source ""

# interfaces
.implements LX/Emj;


# static fields
.field public static final a:LX/Emh;

.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/Emc;

.field public final d:LX/11i;

.field public final e:LX/0So;

.field public final f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/026;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/String;

.field private j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2166072
    new-instance v0, LX/Emh;

    invoke-direct {v0}, LX/Emh;-><init>()V

    sput-object v0, LX/Emk;->a:LX/Emh;

    .line 2166073
    const-string v0, "ec_config_cover_photo"

    const-string v1, "ec_config_profile_picture"

    const-string v2, "ec_config_action_bar"

    const-string v3, "ec_config_context_rows"

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Emk;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/11i;LX/0So;LX/Emc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p3    # LX/Emc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11i;",
            "LX/0So;",
            "LX/Emc;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2166143
    invoke-direct {p0}, LX/3sJ;-><init>()V

    .line 2166144
    iput-object p3, p0, LX/Emk;->c:LX/Emc;

    .line 2166145
    iput-object p1, p0, LX/Emk;->d:LX/11i;

    .line 2166146
    iput-object p2, p0, LX/Emk;->e:LX/0So;

    .line 2166147
    invoke-static {p4, p5, p6}, LX/Emq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/Emk;->f:LX/0P1;

    .line 2166148
    invoke-static {p7}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/Emk;->g:LX/0Rf;

    .line 2166149
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/Emk;->h:LX/026;

    .line 2166150
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Emk;->j:Z

    .line 2166151
    return-void
.end method

.method public static a(LX/Emk;Ljava/lang/String;LX/Emi;)V
    .locals 6

    .prologue
    .line 2166131
    if-nez p1, :cond_1

    .line 2166132
    :cond_0
    :goto_0
    return-void

    .line 2166133
    :cond_1
    iget-object v0, p0, LX/Emk;->d:LX/11i;

    sget-object v1, LX/Emk;->a:LX/Emh;

    invoke-interface {v0, v1, p1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v0

    .line 2166134
    if-eqz v0, :cond_0

    .line 2166135
    iget-object v0, p0, LX/Emk;->d:LX/11i;

    sget-object v1, LX/Emk;->a:LX/Emh;

    .line 2166136
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 2166137
    iget-object v3, p0, LX/Emk;->f:LX/0P1;

    invoke-virtual {v2, v3}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2166138
    const-string v3, "reason"

    iget-object v4, p2, LX/Emi;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2166139
    const-string v3, "fbid"

    invoke-virtual {v2, v3, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2166140
    const-string v3, "initial_card"

    iget-object v4, p0, LX/Emk;->g:LX/0Rf;

    invoke-virtual {v4, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2166141
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    move-object v3, v2

    .line 2166142
    iget-object v2, p0, LX/Emk;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    goto :goto_0
.end method


# virtual methods
.method public final B_(I)V
    .locals 8

    .prologue
    .line 2166097
    const/4 v1, 0x0

    .line 2166098
    iget-object v0, p0, LX/Emk;->c:LX/Emc;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-lt p1, v0, :cond_3

    move-object v0, v1

    .line 2166099
    :goto_0
    move-object v0, v0

    .line 2166100
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Emk;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2166101
    :goto_1
    return-void

    .line 2166102
    :cond_0
    iget-object v1, p0, LX/Emk;->i:Ljava/lang/String;

    .line 2166103
    if-nez v1, :cond_6

    .line 2166104
    :cond_1
    :goto_2
    iput-object v0, p0, LX/Emk;->i:Ljava/lang/String;

    .line 2166105
    iget-object v0, p0, LX/Emk;->i:Ljava/lang/String;

    .line 2166106
    if-nez v0, :cond_9

    .line 2166107
    :cond_2
    :goto_3
    goto :goto_1

    .line 2166108
    :cond_3
    iget-object v0, p0, LX/Emk;->c:LX/Emc;

    invoke-virtual {v0, p1}, LX/Emc;->e(I)Ljava/lang/Object;

    move-result-object v0

    .line 2166109
    if-eqz v0, :cond_4

    instance-of v2, v0, LX/16i;

    if-nez v2, :cond_5

    :cond_4
    move-object v0, v1

    .line 2166110
    goto :goto_0

    .line 2166111
    :cond_5
    check-cast v0, LX/16i;

    invoke-interface {v0}, LX/16i;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2166112
    :cond_6
    iget-object v2, p0, LX/Emk;->d:LX/11i;

    sget-object v3, LX/Emk;->a:LX/Emh;

    invoke-interface {v2, v3, v1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v4

    .line 2166113
    if-eqz v4, :cond_1

    .line 2166114
    sget-object v2, LX/Emk;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_4
    if-ge v3, v5, :cond_8

    sget-object v2, LX/Emk;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2166115
    invoke-interface {v4, v2}, LX/11o;->f(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 2166116
    const p1, 0x662d990c

    invoke-static {v4, v2, p1}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2166117
    :cond_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 2166118
    :cond_8
    sget-object v2, LX/Emi;->CARD_SCROLLED_AWAY:LX/Emi;

    invoke-static {p0, v1, v2}, LX/Emk;->a(LX/Emk;Ljava/lang/String;LX/Emi;)V

    goto :goto_2

    .line 2166119
    :cond_9
    iget-object v2, p0, LX/Emk;->d:LX/11i;

    sget-object v3, LX/Emk;->a:LX/Emh;

    iget-object v5, p0, LX/Emk;->f:LX/0P1;

    iget-object v4, p0, LX/Emk;->e:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v6

    move-object v4, v0

    invoke-interface/range {v2 .. v7}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    .line 2166120
    iget-object v2, p0, LX/Emk;->d:LX/11i;

    sget-object v3, LX/Emk;->a:LX/Emh;

    invoke-interface {v2, v3, v0}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v4

    .line 2166121
    if-eqz v4, :cond_2

    .line 2166122
    sget-object v2, LX/Emk;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_5
    if-ge v3, v5, :cond_a

    sget-object v2, LX/Emk;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2166123
    const v6, 0xf702b49

    invoke-static {v4, v2, v6}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2166124
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    .line 2166125
    :cond_a
    iget-object v2, p0, LX/Emk;->h:LX/026;

    invoke-virtual {v2, v0}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 2166126
    if-eqz v2, :cond_2

    .line 2166127
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2166128
    const v6, 0x770aba7b

    invoke-static {v4, v3, v6}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_6

    .line 2166129
    :cond_b
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    sget-object v3, LX/Emk;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 2166130
    sget-object v2, LX/Emi;->SUCCEEDED:LX/Emi;

    invoke-static {p0, v0, v2}, LX/Emk;->a(LX/Emk;Ljava/lang/String;LX/Emi;)V

    goto/16 :goto_3
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 2166096
    return-void
.end method

.method public final a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Emo;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2166095
    return-void
.end method

.method public final a(LX/EnB;)V
    .locals 1

    .prologue
    .line 2166092
    sget-object v0, LX/EnB;->FINAL:LX/EnB;

    if-ne p1, v0, :cond_0

    .line 2166093
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Emk;->j:Z

    .line 2166094
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2166091
    return-void
.end method

.method public final a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2166076
    const-string v0, "ec_card_recycled"

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2166077
    iget-object v0, p0, LX/Emk;->h:LX/026;

    invoke-virtual {v0, p3}, LX/01J;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166078
    :cond_0
    :goto_0
    return-void

    .line 2166079
    :cond_1
    sget-object v0, LX/Emk;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Emk;->j:Z

    if-nez v0, :cond_0

    .line 2166080
    iget-object v0, p0, LX/Emk;->h:LX/026;

    invoke-virtual {v0, p3}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 2166081
    if-nez v0, :cond_2

    .line 2166082
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    .line 2166083
    iget-object v1, p0, LX/Emk;->h:LX/026;

    invoke-virtual {v1, p3, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166084
    :cond_2
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2166085
    iget-object v1, p0, LX/Emk;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Emk;->i:Ljava/lang/String;

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2166086
    iget-object v1, p0, LX/Emk;->d:LX/11i;

    sget-object v2, LX/Emk;->a:LX/Emh;

    iget-object v3, p0, LX/Emk;->i:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v1

    .line 2166087
    if-eqz v1, :cond_3

    invoke-interface {v1, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2166088
    const v2, 0x414cf912

    invoke-static {v1, p1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2166089
    :cond_3
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    sget-object v1, LX/Emk;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2166090
    iget-object v0, p0, LX/Emk;->i:Ljava/lang/String;

    sget-object v1, LX/Emi;->SUCCEEDED:LX/Emi;

    invoke-static {p0, v0, v1}, LX/Emk;->a(LX/Emk;Ljava/lang/String;LX/Emi;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;D)V
    .locals 0

    .prologue
    .line 2166075
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2166074
    return-void
.end method
