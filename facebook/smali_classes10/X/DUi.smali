.class public final LX/DUi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 2003622
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 2003623
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2003624
    :goto_0
    return v6

    .line 2003625
    :cond_0
    const-string v12, "added_time"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2003626
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v1

    .line 2003627
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 2003628
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2003629
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2003630
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2003631
    const-string v12, "added_by"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 2003632
    invoke-static {p0, p1}, LX/DUf;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2003633
    :cond_2
    const-string v12, "admin_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2003634
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 2003635
    :cond_3
    const-string v12, "bio_story"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2003636
    invoke-static {p0, p1}, LX/DUg;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2003637
    :cond_4
    const-string v12, "node"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2003638
    invoke-static {p0, p1}, LX/DUh;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2003639
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2003640
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2003641
    invoke-virtual {p1, v6, v10}, LX/186;->b(II)V

    .line 2003642
    if-eqz v0, :cond_7

    move-object v0, p1

    .line 2003643
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2003644
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2003645
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2003646
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2003647
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_0

    :cond_8
    move v0, v6

    move v7, v6

    move v8, v6

    move v9, v6

    move-wide v2, v4

    move v10, v6

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x2

    .line 2003648
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2003649
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2003650
    if-eqz v0, :cond_0

    .line 2003651
    const-string v1, "added_by"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003652
    invoke-static {p0, v0, p2}, LX/DUf;->a(LX/15i;ILX/0nX;)V

    .line 2003653
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2003654
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 2003655
    const-string v2, "added_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003656
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2003657
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2003658
    if-eqz v0, :cond_2

    .line 2003659
    const-string v0, "admin_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003660
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2003661
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2003662
    if-eqz v0, :cond_3

    .line 2003663
    const-string v1, "bio_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003664
    invoke-static {p0, v0, p2}, LX/DUg;->a(LX/15i;ILX/0nX;)V

    .line 2003665
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2003666
    if-eqz v0, :cond_4

    .line 2003667
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2003668
    invoke-static {p0, v0, p2, p3}, LX/DUh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2003669
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2003670
    return-void
.end method
