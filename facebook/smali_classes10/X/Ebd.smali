.class public LX/Ebd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2144179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144180
    return-void
.end method

.method private static a([B)LX/Ebc;
    .locals 7

    .prologue
    const/16 v3, 0x20

    const/4 v6, 0x0

    .line 2144175
    new-instance v0, LX/Eb7;

    invoke-direct {v0}, LX/Eb7;-><init>()V

    .line 2144176
    const-string v1, "WhisperText"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v0, p0, v1, v2}, LX/Eb5;->a([B[BI)[B

    move-result-object v1

    .line 2144177
    invoke-static {v1, v3, v3}, LX/Eco;->a([BII)[[B

    move-result-object v1

    .line 2144178
    new-instance v2, LX/Ebc;

    new-instance v3, LX/Ebe;

    aget-object v4, v1, v6

    invoke-direct {v3, v0, v4}, LX/Ebe;-><init>(LX/Eb5;[B)V

    new-instance v4, LX/Eba;

    const/4 v5, 0x1

    aget-object v1, v1, v5

    invoke-direct {v4, v0, v1, v6}, LX/Eba;-><init>(LX/Eb5;[BI)V

    invoke-direct {v2, v3, v4}, LX/Ebc;-><init>(LX/Ebe;LX/Eba;)V

    return-object v2
.end method

.method public static a(LX/Ebj;LX/EbX;)V
    .locals 5

    .prologue
    .line 2144132
    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0}, LX/Ebj;->a(I)V

    .line 2144133
    iget-object v0, p1, LX/EbX;->c:LX/Eae;

    move-object v0, v0

    .line 2144134
    invoke-virtual {p0, v0}, LX/Ebj;->a(LX/Eae;)V

    .line 2144135
    iget-object v0, p1, LX/EbX;->a:LX/Eaf;

    move-object v0, v0

    .line 2144136
    iget-object v1, v0, LX/Eaf;->a:LX/Eae;

    move-object v0, v1

    .line 2144137
    invoke-virtual {p0, v0}, LX/Ebj;->b(LX/Eae;)V

    .line 2144138
    invoke-static {}, LX/Ear;->a()LX/Eau;

    move-result-object v1

    .line 2144139
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2144140
    invoke-static {}, LX/Ebd;->a()[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144141
    iget-object v0, p1, LX/EbX;->d:LX/Eat;

    move-object v0, v0

    .line 2144142
    iget-object v3, p1, LX/EbX;->a:LX/Eaf;

    move-object v3, v3

    .line 2144143
    iget-object v4, v3, LX/Eaf;->b:LX/Eas;

    move-object v3, v4

    .line 2144144
    invoke-static {v0, v3}, LX/Ear;->a(LX/Eat;LX/Eas;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144145
    iget-object v0, p1, LX/EbX;->c:LX/Eae;

    move-object v0, v0

    .line 2144146
    iget-object v3, v0, LX/Eae;->a:LX/Eat;

    move-object v0, v3

    .line 2144147
    iget-object v3, p1, LX/EbX;->b:LX/Eau;

    move-object v3, v3

    .line 2144148
    iget-object v4, v3, LX/Eau;->b:LX/Eas;

    move-object v3, v4

    .line 2144149
    invoke-static {v0, v3}, LX/Ear;->a(LX/Eat;LX/Eas;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144150
    iget-object v0, p1, LX/EbX;->d:LX/Eat;

    move-object v0, v0

    .line 2144151
    iget-object v3, p1, LX/EbX;->b:LX/Eau;

    move-object v3, v3

    .line 2144152
    iget-object v4, v3, LX/Eau;->b:LX/Eas;

    move-object v3, v4

    .line 2144153
    invoke-static {v0, v3}, LX/Ear;->a(LX/Eat;LX/Eas;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144154
    iget-object v0, p1, LX/EbX;->e:LX/Ecs;

    move-object v0, v0

    .line 2144155
    invoke-virtual {v0}, LX/Ecs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2144156
    iget-object v0, p1, LX/EbX;->e:LX/Ecs;

    move-object v0, v0

    .line 2144157
    invoke-virtual {v0}, LX/Ecs;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eat;

    .line 2144158
    iget-object v3, p1, LX/EbX;->b:LX/Eau;

    move-object v3, v3

    .line 2144159
    iget-object v4, v3, LX/Eau;->b:LX/Eas;

    move-object v3, v4

    .line 2144160
    invoke-static {v0, v3}, LX/Ear;->a(LX/Eat;LX/Eas;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144161
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, LX/Ebd;->a([B)LX/Ebc;

    move-result-object v0

    .line 2144162
    iget-object v2, v0, LX/Ebc;->a:LX/Ebe;

    move-object v2, v2

    .line 2144163
    iget-object v3, p1, LX/EbX;->f:LX/Eat;

    move-object v3, v3

    .line 2144164
    invoke-virtual {v2, v3, v1}, LX/Ebe;->a(LX/Eat;LX/Eau;)LX/Ecr;

    move-result-object v2

    .line 2144165
    iget-object v3, p1, LX/EbX;->f:LX/Eat;

    move-object v3, v3

    .line 2144166
    iget-object v4, v0, LX/Ebc;->b:LX/Eba;

    move-object v0, v4

    .line 2144167
    invoke-virtual {p0, v3, v0}, LX/Ebj;->a(LX/Eat;LX/Eba;)V

    .line 2144168
    iget-object v0, v2, LX/Ecr;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 2144169
    check-cast v0, LX/Eba;

    invoke-virtual {p0, v1, v0}, LX/Ebj;->a(LX/Eau;LX/Eba;)V

    .line 2144170
    iget-object v0, v2, LX/Ecr;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2144171
    check-cast v0, LX/Ebe;

    invoke-virtual {p0, v0}, LX/Ebj;->a(LX/Ebe;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2144172
    return-void

    .line 2144173
    :catch_0
    move-exception v0

    .line 2144174
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static a(LX/Ebj;LX/EbZ;)V
    .locals 4

    .prologue
    .line 2144092
    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0}, LX/Ebj;->a(I)V

    .line 2144093
    iget-object v0, p1, LX/EbZ;->e:LX/Eae;

    move-object v0, v0

    .line 2144094
    invoke-virtual {p0, v0}, LX/Ebj;->a(LX/Eae;)V

    .line 2144095
    iget-object v0, p1, LX/EbZ;->a:LX/Eaf;

    move-object v0, v0

    .line 2144096
    iget-object v1, v0, LX/Eaf;->a:LX/Eae;

    move-object v0, v1

    .line 2144097
    invoke-virtual {p0, v0}, LX/Ebj;->b(LX/Eae;)V

    .line 2144098
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2144099
    invoke-static {}, LX/Ebd;->a()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144100
    iget-object v0, p1, LX/EbZ;->e:LX/Eae;

    move-object v0, v0

    .line 2144101
    iget-object v2, v0, LX/Eae;->a:LX/Eat;

    move-object v0, v2

    .line 2144102
    iget-object v2, p1, LX/EbZ;->b:LX/Eau;

    move-object v2, v2

    .line 2144103
    iget-object v3, v2, LX/Eau;->b:LX/Eas;

    move-object v2, v3

    .line 2144104
    invoke-static {v0, v2}, LX/Ear;->a(LX/Eat;LX/Eas;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144105
    iget-object v0, p1, LX/EbZ;->f:LX/Eat;

    move-object v0, v0

    .line 2144106
    iget-object v2, p1, LX/EbZ;->a:LX/Eaf;

    move-object v2, v2

    .line 2144107
    iget-object v3, v2, LX/Eaf;->b:LX/Eas;

    move-object v2, v3

    .line 2144108
    invoke-static {v0, v2}, LX/Ear;->a(LX/Eat;LX/Eas;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144109
    iget-object v0, p1, LX/EbZ;->f:LX/Eat;

    move-object v0, v0

    .line 2144110
    iget-object v2, p1, LX/EbZ;->b:LX/Eau;

    move-object v2, v2

    .line 2144111
    iget-object v3, v2, LX/Eau;->b:LX/Eas;

    move-object v2, v3

    .line 2144112
    invoke-static {v0, v2}, LX/Ear;->a(LX/Eat;LX/Eas;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144113
    iget-object v0, p1, LX/EbZ;->c:LX/Ecs;

    move-object v0, v0

    .line 2144114
    invoke-virtual {v0}, LX/Ecs;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2144115
    iget-object v0, p1, LX/EbZ;->f:LX/Eat;

    move-object v2, v0

    .line 2144116
    iget-object v0, p1, LX/EbZ;->c:LX/Ecs;

    move-object v0, v0

    .line 2144117
    invoke-virtual {v0}, LX/Ecs;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Eau;

    .line 2144118
    iget-object v3, v0, LX/Eau;->b:LX/Eas;

    move-object v0, v3

    .line 2144119
    invoke-static {v2, v0}, LX/Ear;->a(LX/Eat;LX/Eas;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 2144120
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, LX/Ebd;->a([B)LX/Ebc;

    move-result-object v0

    .line 2144121
    iget-object v1, p1, LX/EbZ;->d:LX/Eau;

    move-object v1, v1

    .line 2144122
    iget-object v2, v0, LX/Ebc;->b:LX/Eba;

    move-object v2, v2

    .line 2144123
    invoke-virtual {p0, v1, v2}, LX/Ebj;->a(LX/Eau;LX/Eba;)V

    .line 2144124
    iget-object v1, v0, LX/Ebc;->a:LX/Ebe;

    move-object v0, v1

    .line 2144125
    invoke-virtual {p0, v0}, LX/Ebj;->a(LX/Ebe;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2144126
    return-void

    .line 2144127
    :catch_0
    move-exception v0

    .line 2144128
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static a()[B
    .locals 2

    .prologue
    .line 2144129
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 2144130
    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 2144131
    return-object v0
.end method
