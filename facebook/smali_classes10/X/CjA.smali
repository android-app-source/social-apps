.class public LX/CjA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929158
    iput-object p1, p0, LX/CjA;->a:LX/0tX;

    .line 1929159
    return-void
.end method

.method public static b(LX/0QB;)LX/CjA;
    .locals 2

    .prologue
    .line 1929160
    new-instance v1, LX/CjA;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v0}, LX/CjA;-><init>(LX/0tX;)V

    .line 1929161
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1929162
    new-instance v0, LX/4GR;

    invoke-direct {v0}, LX/4GR;-><init>()V

    .line 1929163
    invoke-virtual {v0, p2}, LX/4GR;->c(Ljava/lang/String;)LX/4GR;

    .line 1929164
    const-string v1, "INLINE_CTA"

    invoke-virtual {v0, v1}, LX/4GR;->d(Ljava/lang/String;)LX/4GR;

    .line 1929165
    invoke-virtual {v0, p1}, LX/4GR;->b(Ljava/lang/String;)LX/4GR;

    .line 1929166
    invoke-static {}, LX/8bB;->a()LX/8bA;

    move-result-object v1

    .line 1929167
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1929168
    iget-object v0, p0, LX/CjA;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1929169
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1929170
    if-nez p3, :cond_0

    .line 1929171
    :goto_0
    return-void

    .line 1929172
    :cond_0
    new-instance v0, LX/4GQ;

    invoke-direct {v0}, LX/4GQ;-><init>()V

    .line 1929173
    invoke-virtual {v0, p2}, LX/4GQ;->c(Ljava/lang/String;)LX/4GQ;

    .line 1929174
    const-string v1, "INLINE_CTA"

    invoke-virtual {v0, v1}, LX/4GQ;->d(Ljava/lang/String;)LX/4GQ;

    .line 1929175
    invoke-virtual {v0, p1}, LX/4GQ;->b(Ljava/lang/String;)LX/4GQ;

    .line 1929176
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 1929177
    invoke-virtual {v0, v1}, LX/4GQ;->a(Ljava/util/List;)LX/4GQ;

    .line 1929178
    invoke-static {}, LX/8bB;->b()LX/8b9;

    move-result-object v1

    .line 1929179
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1929180
    iget-object v0, p0, LX/CjA;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
