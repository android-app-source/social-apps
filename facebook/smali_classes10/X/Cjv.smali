.class public LX/Cjv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cju;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/8bZ;

.field private final b:LX/Cju;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8bZ;LX/0hB;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1929938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929939
    iput-object p2, p0, LX/Cjv;->a:LX/8bZ;

    .line 1929940
    iget-object v0, p0, LX/Cjv;->a:LX/8bZ;

    invoke-virtual {v0}, LX/8bZ;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/Cjw;

    invoke-direct {v0, p3}, LX/Cjw;-><init>(LX/0hB;)V

    :goto_0
    iput-object v0, p0, LX/Cjv;->b:LX/Cju;

    .line 1929941
    return-void

    .line 1929942
    :cond_0
    new-instance v0, LX/Cjz;

    invoke-direct {v0, p1}, LX/Cjz;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Cjv;
    .locals 6

    .prologue
    .line 1929947
    const-class v1, LX/Cjv;

    monitor-enter v1

    .line 1929948
    :try_start_0
    sget-object v0, LX/Cjv;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1929949
    sput-object v2, LX/Cjv;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1929950
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1929951
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1929952
    new-instance p0, LX/Cjv;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object v4

    check-cast v4, LX/8bZ;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v5

    check-cast v5, LX/0hB;

    invoke-direct {p0, v3, v4, v5}, LX/Cjv;-><init>(Landroid/content/Context;LX/8bZ;LX/0hB;)V

    .line 1929953
    move-object v0, p0

    .line 1929954
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1929955
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Cjv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1929956
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1929957
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1929945
    iget-object v0, p0, LX/Cjv;->b:LX/Cju;

    invoke-interface {v0}, LX/Cju;->a()V

    .line 1929946
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1929958
    iget-object v0, p0, LX/Cjv;->b:LX/Cju;

    invoke-interface {v0, p1}, LX/Cju;->a(I)Z

    move-result v0

    return v0
.end method

.method public final b(I)F
    .locals 1

    .prologue
    .line 1929944
    iget-object v0, p0, LX/Cjv;->b:LX/Cju;

    invoke-interface {v0, p1}, LX/Cju;->b(I)F

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 1929943
    iget-object v0, p0, LX/Cjv;->b:LX/Cju;

    invoke-interface {v0, p1}, LX/Cju;->c(I)I

    move-result v0

    return v0
.end method
