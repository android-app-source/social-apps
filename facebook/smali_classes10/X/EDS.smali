.class public final enum LX/EDS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/EDS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/EDS;

.field public static final enum VIDEO_CHAT_HEAD:LX/EDS;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2091356
    new-instance v0, LX/EDS;

    const-string v1, "VIDEO_CHAT_HEAD"

    invoke-direct {v0, v1, v2}, LX/EDS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/EDS;->VIDEO_CHAT_HEAD:LX/EDS;

    .line 2091357
    const/4 v0, 0x1

    new-array v0, v0, [LX/EDS;

    sget-object v1, LX/EDS;->VIDEO_CHAT_HEAD:LX/EDS;

    aput-object v1, v0, v2

    sput-object v0, LX/EDS;->$VALUES:[LX/EDS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2091358
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/EDS;
    .locals 1

    .prologue
    .line 2091359
    const-class v0, LX/EDS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EDS;

    return-object v0
.end method

.method public static values()[LX/EDS;
    .locals 1

    .prologue
    .line 2091360
    sget-object v0, LX/EDS;->$VALUES:[LX/EDS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/EDS;

    return-object v0
.end method
