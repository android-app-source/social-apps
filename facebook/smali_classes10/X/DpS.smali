.class public LX/DpS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final pre_keys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/DpU;",
            ">;"
        }
    .end annotation
.end field

.field public final signed_pre_key_with_id:LX/Dpf;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2045438
    new-instance v0, LX/1sv;

    const-string v1, "PreKeyUploadPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/DpS;->b:LX/1sv;

    .line 2045439
    new-instance v0, LX/1sw;

    const-string v1, "pre_keys"

    const/16 v2, 0xf

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpS;->c:LX/1sw;

    .line 2045440
    new-instance v0, LX/1sw;

    const-string v1, "signed_pre_key_with_id"

    const/16 v2, 0xc

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/DpS;->d:LX/1sw;

    .line 2045441
    const/4 v0, 0x1

    sput-boolean v0, LX/DpS;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;LX/Dpf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/DpU;",
            ">;",
            "LX/Dpf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2045442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2045443
    iput-object p1, p0, LX/DpS;->pre_keys:Ljava/util/List;

    .line 2045444
    iput-object p2, p0, LX/DpS;->signed_pre_key_with_id:LX/Dpf;

    .line 2045445
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2045446
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2045447
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2045448
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2045449
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PreKeyUploadPayload"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2045450
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045451
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045452
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045453
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045454
    const-string v4, "pre_keys"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045455
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045456
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045457
    iget-object v4, p0, LX/DpS;->pre_keys:Ljava/util/List;

    if-nez v4, :cond_3

    .line 2045458
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045459
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045460
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045461
    const-string v4, "signed_pre_key_with_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045462
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045463
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045464
    iget-object v0, p0, LX/DpS;->signed_pre_key_with_id:LX/Dpf;

    if-nez v0, :cond_4

    .line 2045465
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045466
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045467
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2045468
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2045469
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2045470
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2045471
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2045472
    :cond_3
    iget-object v4, p0, LX/DpS;->pre_keys:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2045473
    :cond_4
    iget-object v0, p0, LX/DpS;->signed_pre_key_with_id:LX/Dpf;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 2045474
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2045475
    iget-object v0, p0, LX/DpS;->pre_keys:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2045476
    sget-object v0, LX/DpS;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045477
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/DpS;->pre_keys:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2045478
    iget-object v0, p0, LX/DpS;->pre_keys:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DpU;

    .line 2045479
    invoke-virtual {v0, p1}, LX/DpU;->a(LX/1su;)V

    goto :goto_0

    .line 2045480
    :cond_0
    iget-object v0, p0, LX/DpS;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v0, :cond_1

    .line 2045481
    sget-object v0, LX/DpS;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2045482
    iget-object v0, p0, LX/DpS;->signed_pre_key_with_id:LX/Dpf;

    invoke-virtual {v0, p1}, LX/Dpf;->a(LX/1su;)V

    .line 2045483
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2045484
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2045485
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2045486
    if-nez p1, :cond_1

    .line 2045487
    :cond_0
    :goto_0
    return v0

    .line 2045488
    :cond_1
    instance-of v1, p1, LX/DpS;

    if-eqz v1, :cond_0

    .line 2045489
    check-cast p1, LX/DpS;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2045490
    if-nez p1, :cond_3

    .line 2045491
    :cond_2
    :goto_1
    move v0, v2

    .line 2045492
    goto :goto_0

    .line 2045493
    :cond_3
    iget-object v0, p0, LX/DpS;->pre_keys:Ljava/util/List;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2045494
    :goto_2
    iget-object v3, p1, LX/DpS;->pre_keys:Ljava/util/List;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2045495
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2045496
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045497
    iget-object v0, p0, LX/DpS;->pre_keys:Ljava/util/List;

    iget-object v3, p1, LX/DpS;->pre_keys:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2045498
    :cond_5
    iget-object v0, p0, LX/DpS;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2045499
    :goto_4
    iget-object v3, p1, LX/DpS;->signed_pre_key_with_id:LX/Dpf;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2045500
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2045501
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2045502
    iget-object v0, p0, LX/DpS;->signed_pre_key_with_id:LX/Dpf;

    iget-object v3, p1, LX/DpS;->signed_pre_key_with_id:LX/Dpf;

    invoke-virtual {v0, v3}, LX/Dpf;->a(LX/Dpf;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 2045503
    goto :goto_1

    :cond_8
    move v0, v2

    .line 2045504
    goto :goto_2

    :cond_9
    move v3, v2

    .line 2045505
    goto :goto_3

    :cond_a
    move v0, v2

    .line 2045506
    goto :goto_4

    :cond_b
    move v3, v2

    .line 2045507
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2045508
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2045509
    sget-boolean v0, LX/DpS;->a:Z

    .line 2045510
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/DpS;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2045511
    return-object v0
.end method
