.class public final LX/EFn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field public final synthetic a:LX/EFp;


# direct methods
.method public constructor <init>(LX/EFp;)V
    .locals 0

    .prologue
    .line 2096401
    iput-object p1, p0, LX/EFn;->a:LX/EFp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 2096392
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 2096393
    iput v1, v0, LX/EFp;->j:I

    .line 2096394
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 2096395
    iput v1, v0, LX/EFp;->k:I

    .line 2096396
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v1, p0, LX/EFn;->a:LX/EFp;

    iget v1, v1, LX/EFp;->j:I

    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->h:I

    sub-int/2addr v1, v2

    .line 2096397
    iput v1, v0, LX/EFp;->l:I

    .line 2096398
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v1, p0, LX/EFn;->a:LX/EFp;

    iget v1, v1, LX/EFp;->k:I

    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->i:I

    sub-int/2addr v1, v2

    .line 2096399
    iput v1, v0, LX/EFp;->m:I

    .line 2096400
    const/4 v0, 0x1

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10

    .prologue
    .line 2096341
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->g:LX/EGb;

    invoke-virtual {v0}, LX/EGb;->g()Landroid/graphics/Rect;

    move-result-object v3

    .line 2096342
    iget v0, v3, Landroid/graphics/Rect;->right:I

    iget v1, v3, Landroid/graphics/Rect;->left:I

    sub-int v4, v0, v1

    .line 2096343
    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    iget v1, v3, Landroid/graphics/Rect;->top:I

    sub-int v5, v0, v1

    .line 2096344
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    .line 2096345
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v6, v2

    .line 2096346
    const-wide v8, 0x40a7700000000000L    # 3000.0

    cmpg-double v2, v6, v8

    if-gez v2, :cond_0

    const-wide v6, 0x40a7700000000000L    # 3000.0

    cmpg-double v2, v0, v6

    if-gez v2, :cond_0

    .line 2096347
    const/4 v0, 0x0

    .line 2096348
    :goto_0
    return v0

    .line 2096349
    :cond_0
    const-wide v6, 0x408f400000000000L    # 1000.0

    cmpg-double v0, v0, v6

    if-gez v0, :cond_1

    .line 2096350
    const/4 p3, 0x0

    .line 2096351
    :cond_1
    const/4 v1, 0x0

    .line 2096352
    const/4 v2, 0x0

    .line 2096353
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_b

    .line 2096354
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget v0, v0, LX/EFp;->h:I

    sub-int v0, v4, v0

    if-lez v0, :cond_a

    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget v0, v0, LX/EFp;->h:I

    sub-int v0, v4, v0

    int-to-float v0, v0

    div-float/2addr v0, p3

    :goto_1
    move v1, v0

    .line 2096355
    :cond_2
    :goto_2
    const/4 v0, 0x0

    cmpl-float v0, p4, v0

    if-lez v0, :cond_c

    .line 2096356
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget v0, v0, LX/EFp;->i:I

    sub-int v0, v5, v0

    int-to-float v0, v0

    div-float/2addr v0, p4

    .line 2096357
    :goto_3
    cmpl-float v2, v0, v1

    if-lez v2, :cond_f

    .line 2096358
    const/4 v2, 0x0

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_d

    :goto_4
    mul-float/2addr v0, p4

    iget-object v1, p0, LX/EFn;->a:LX/EFp;

    iget v1, v1, LX/EFp;->i:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v1, v0

    .line 2096359
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_e

    iget v0, v3, Landroid/graphics/Rect;->right:I

    .line 2096360
    :goto_5
    const/4 v2, 0x0

    cmpl-float v2, p3, v2

    if-nez v2, :cond_3

    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget-object v2, v2, LX/EFp;->p:LX/0wd;

    if-eqz v2, :cond_3

    .line 2096361
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->p:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v6

    double-to-int v0, v6

    .line 2096362
    :cond_3
    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->h:I

    iget v5, v3, Landroid/graphics/Rect;->left:I

    if-lt v2, v5, :cond_4

    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->h:I

    iget v5, v3, Landroid/graphics/Rect;->right:I

    if-lt v2, v5, :cond_5

    .line 2096363
    :cond_4
    iget-object v1, p0, LX/EFn;->a:LX/EFp;

    iget v1, v1, LX/EFp;->i:I

    .line 2096364
    :cond_5
    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->i:I

    iget v5, v3, Landroid/graphics/Rect;->top:I

    if-lt v2, v5, :cond_6

    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->i:I

    iget v5, v3, Landroid/graphics/Rect;->bottom:I

    if-lt v2, v5, :cond_7

    .line 2096365
    :cond_6
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget v0, v0, LX/EFp;->h:I

    .line 2096366
    :cond_7
    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    invoke-static {v2, v1}, LX/EFp;->e(LX/EFp;I)I

    move-result v1

    .line 2096367
    div-int/lit8 v2, v4, 0x2

    if-le v0, v2, :cond_12

    iget v0, v3, Landroid/graphics/Rect;->right:I

    .line 2096368
    :goto_6
    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    const/4 v3, 0x0

    .line 2096369
    iput-boolean v3, v2, LX/EFp;->n:Z

    .line 2096370
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/EFn;->a:LX/EFp;

    iget v4, v4, LX/EFp;->h:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/EFn;->a:LX/EFp;

    iget v4, v4, LX/EFp;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    float-to-int v4, p3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    float-to-int v4, p4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 2096371
    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget-object v2, v2, LX/EFp;->p:LX/0wd;

    if-eqz v2, :cond_8

    .line 2096372
    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget-object v2, v2, LX/EFp;->p:LX/0wd;

    iget-object v3, p0, LX/EFn;->a:LX/EFp;

    iget v3, v3, LX/EFp;->h:I

    int-to-double v4, v3

    invoke-virtual {v2, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 2096373
    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget-object v2, v2, LX/EFp;->p:LX/0wd;

    float-to-double v4, p3

    invoke-virtual {v2, v4, v5}, LX/0wd;->c(D)LX/0wd;

    .line 2096374
    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget-object v2, v2, LX/EFp;->p:LX/0wd;

    int-to-double v4, v0

    invoke-virtual {v2, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 2096375
    :cond_8
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->q:LX/0wd;

    if-eqz v0, :cond_9

    .line 2096376
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->q:LX/0wd;

    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->i:I

    int-to-double v2, v2

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 2096377
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->q:LX/0wd;

    float-to-double v2, p4

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    .line 2096378
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->q:LX/0wd;

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2096379
    :cond_9
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2096380
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2096381
    :cond_b
    const/4 v0, 0x0

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 2096382
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget v0, v0, LX/EFp;->h:I

    int-to-float v0, v0

    const/high16 v1, -0x40800000    # -1.0f

    mul-float/2addr v1, p3

    div-float v1, v0, v1

    goto/16 :goto_2

    .line 2096383
    :cond_c
    const/4 v0, 0x0

    cmpg-float v0, p4, v0

    if-gez v0, :cond_13

    .line 2096384
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget v0, v0, LX/EFp;->i:I

    int-to-float v0, v0

    const/high16 v2, -0x40800000    # -1.0f

    mul-float/2addr v2, p4

    div-float/2addr v0, v2

    goto/16 :goto_3

    :cond_d
    move v0, v1

    .line 2096385
    goto/16 :goto_4

    .line 2096386
    :cond_e
    iget v0, v3, Landroid/graphics/Rect;->left:I

    goto/16 :goto_5

    .line 2096387
    :cond_f
    const/4 v0, 0x0

    cmpl-float v0, p4, v0

    if-lez v0, :cond_10

    iget v1, v3, Landroid/graphics/Rect;->bottom:I

    .line 2096388
    :goto_7
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_11

    iget v0, v3, Landroid/graphics/Rect;->right:I

    goto/16 :goto_5

    .line 2096389
    :cond_10
    iget v1, v3, Landroid/graphics/Rect;->top:I

    goto :goto_7

    .line 2096390
    :cond_11
    iget v0, v3, Landroid/graphics/Rect;->left:I

    goto/16 :goto_5

    .line 2096391
    :cond_12
    iget v0, v3, Landroid/graphics/Rect;->left:I

    goto/16 :goto_6

    :cond_13
    move v0, v2

    goto/16 :goto_3
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 2096320
    return-void
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2096325
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 2096326
    iput v1, v0, LX/EFp;->j:I

    .line 2096327
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 2096328
    iput v1, v0, LX/EFp;->k:I

    .line 2096329
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v1, p0, LX/EFn;->a:LX/EFp;

    iget v1, v1, LX/EFp;->j:I

    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->l:I

    sub-int/2addr v1, v2

    .line 2096330
    iput v1, v0, LX/EFp;->h:I

    .line 2096331
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v1, p0, LX/EFn;->a:LX/EFp;

    iget v1, v1, LX/EFp;->k:I

    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->m:I

    sub-int/2addr v1, v2

    .line 2096332
    iput v1, v0, LX/EFp;->i:I

    .line 2096333
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v1, p0, LX/EFn;->a:LX/EFp;

    iget-object v2, p0, LX/EFn;->a:LX/EFp;

    iget v2, v2, LX/EFp;->i:I

    invoke-static {v1, v2}, LX/EFp;->e(LX/EFp;I)I

    move-result v1

    .line 2096334
    iput v1, v0, LX/EFp;->i:I

    .line 2096335
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    .line 2096336
    iput-boolean v3, v0, LX/EFp;->n:Z

    .line 2096337
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    .line 2096338
    iput-boolean v3, v0, LX/EFp;->o:Z

    .line 2096339
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->g:LX/EGb;

    invoke-virtual {v0}, LX/EGb;->f()V

    .line 2096340
    const/4 v0, 0x0

    return v0
.end method

.method public final onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 2096324
    return-void
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2096321
    iget-object v0, p0, LX/EFn;->a:LX/EFp;

    iget-object v0, v0, LX/EFp;->g:LX/EGb;

    .line 2096322
    iget-object p0, v0, LX/EGb;->a:LX/EGe;

    invoke-virtual {p0}, LX/EGe;->B()Z

    move-result p0

    move v0, p0

    .line 2096323
    return v0
.end method
