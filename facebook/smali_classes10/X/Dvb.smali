.class public abstract LX/Dvb;
.super LX/DvZ;
.source ""


# static fields
.field private static final u:Ljava/lang/Object;

.field private static final v:Ljava/lang/Object;


# instance fields
.field public final a:LX/Dvf;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dux;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/Dw8;

.field public f:Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

.field public g:LX/Dvm;

.field public h:LX/Dv0;

.field public i:LX/Dvc;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:Z

.field public p:Z

.field public q:LX/DwK;

.field public r:Z

.field public s:LX/Dw7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2058861
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Dvb;->u:Ljava/lang/Object;

    .line 2058862
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Dvb;->v:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/Dvf;LX/0Ot;Ljava/lang/String;LX/DwK;LX/Dw8;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dux;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvd;",
            ">;",
            "LX/Dvf;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/lang/String;",
            "LX/DwK;",
            "LX/Dw8;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2058863
    invoke-direct {p0}, LX/DvZ;-><init>()V

    .line 2058864
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Dvb;->m:Z

    .line 2058865
    iput-object p1, p0, LX/Dvb;->b:LX/0Ot;

    .line 2058866
    iput-object p3, p0, LX/Dvb;->t:LX/0Ot;

    .line 2058867
    iput-object p4, p0, LX/Dvb;->a:LX/Dvf;

    .line 2058868
    iput-object p2, p0, LX/Dvb;->c:LX/0Ot;

    .line 2058869
    iput-object p5, p0, LX/Dvb;->d:LX/0Ot;

    .line 2058870
    iput-object p6, p0, LX/Dvb;->k:Ljava/lang/String;

    .line 2058871
    iput-object p6, p0, LX/Dvb;->j:Ljava/lang/String;

    .line 2058872
    iput-object p7, p0, LX/Dvb;->q:LX/DwK;

    .line 2058873
    iput-object p8, p0, LX/Dvb;->e:LX/Dw8;

    .line 2058874
    return-void
.end method

.method public static a(Landroid/content/Context;)LX/Dw3;
    .locals 4

    .prologue
    .line 2058875
    new-instance v0, LX/Dw3;

    invoke-direct {v0, p0}, LX/Dw3;-><init>(Landroid/content/Context;)V

    .line 2058876
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 2058877
    invoke-virtual {v0, v1}, LX/Dw3;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2058878
    return-object v0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;)LX/DwB;
    .locals 9

    .prologue
    .line 2058879
    if-eqz p1, :cond_0

    .line 2058880
    check-cast p1, LX/DwB;

    move-object v0, p1

    .line 2058881
    :goto_0
    iget-object v2, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {p0}, LX/Dvb;->f()LX/DvW;

    move-result-object v3

    iget-object v4, p0, LX/Dvb;->n:Ljava/lang/String;

    iget-boolean v5, p0, LX/Dvb;->o:Z

    iget-boolean v6, p0, LX/Dvb;->p:Z

    iget-boolean v7, p0, LX/Dvb;->r:Z

    iget-object v8, p0, LX/Dvb;->s:LX/Dw7;

    move-object v1, p3

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V

    .line 2058882
    return-object v0

    .line 2058883
    :cond_0
    new-instance v0, LX/DwB;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DwB;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;)LX/DwD;
    .locals 9

    .prologue
    .line 2058884
    if-nez p1, :cond_0

    new-instance v0, LX/DwD;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DwD;-><init>(Landroid/content/Context;)V

    .line 2058885
    :goto_0
    iget-object v2, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {p0}, LX/Dvb;->f()LX/DvW;

    move-result-object v3

    iget-object v4, p0, LX/Dvb;->n:Ljava/lang/String;

    iget-boolean v5, p0, LX/Dvb;->o:Z

    iget-boolean v6, p0, LX/Dvb;->p:Z

    iget-boolean v7, p0, LX/Dvb;->r:Z

    iget-object v8, p0, LX/Dvb;->s:LX/Dw7;

    move-object v1, p3

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V

    .line 2058886
    return-object v0

    .line 2058887
    :cond_0
    check-cast p1, LX/DwD;

    move-object v0, p1

    goto :goto_0
.end method

.method private static b(LX/Dvb;Landroid/view/View;Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2058888
    invoke-virtual {p0}, LX/Dvb;->k()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2058889
    invoke-virtual {p0, p1, p2}, LX/Dvb;->a(Landroid/view/View;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private c(Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;)LX/DwC;
    .locals 9

    .prologue
    .line 2058890
    if-eqz p1, :cond_0

    .line 2058891
    check-cast p1, LX/DwC;

    move-object v0, p1

    .line 2058892
    :goto_0
    iget-object v2, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {p0}, LX/Dvb;->f()LX/DvW;

    move-result-object v3

    iget-object v4, p0, LX/Dvb;->n:Ljava/lang/String;

    iget-boolean v5, p0, LX/Dvb;->o:Z

    iget-boolean v6, p0, LX/Dvb;->p:Z

    iget-boolean v7, p0, LX/Dvb;->r:Z

    iget-object v8, p0, LX/Dvb;->s:LX/Dw7;

    move-object v1, p3

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V

    .line 2058893
    return-object v0

    .line 2058894
    :cond_0
    new-instance v0, LX/DwC;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DwC;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private d(Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;)LX/Dw4;
    .locals 9

    .prologue
    .line 2058895
    if-eqz p1, :cond_0

    .line 2058896
    check-cast p1, LX/Dw4;

    move-object v0, p1

    .line 2058897
    :goto_0
    iget-object v2, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {p0}, LX/Dvb;->f()LX/DvW;

    move-result-object v3

    iget-object v4, p0, LX/Dvb;->n:Ljava/lang/String;

    iget-boolean v5, p0, LX/Dvb;->o:Z

    iget-boolean v6, p0, LX/Dvb;->p:Z

    iget-boolean v7, p0, LX/Dvb;->r:Z

    iget-object v8, p0, LX/Dvb;->s:LX/Dw7;

    move-object v1, p3

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V

    .line 2058898
    return-object v0

    .line 2058899
    :cond_0
    new-instance v0, LX/Dw4;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Dw4;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private o()V
    .locals 5

    .prologue
    .line 2059001
    iget-object v0, p0, LX/Dvb;->h:LX/Dv0;

    .line 2059002
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2059003
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dvb;->h:LX/Dv0;

    .line 2059004
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2059005
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2059006
    iget-object v0, p0, LX/Dvb;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dvd;

    .line 2059007
    iget-object v1, p0, LX/Dvb;->g:LX/Dvm;

    if-nez v1, :cond_0

    .line 2059008
    new-instance v1, LX/Dvm;

    iget-object v2, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {p0}, LX/Dvb;->f()LX/DvW;

    move-result-object v3

    iget-boolean v4, p0, LX/Dvb;->m:Z

    invoke-direct {v1, v2, v3, v4}, LX/Dvm;-><init>(Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Z)V

    iput-object v1, p0, LX/Dvb;->g:LX/Dvm;

    .line 2059009
    :cond_0
    iget-object v1, p0, LX/Dvb;->g:LX/Dvm;

    move-object v1, v1

    .line 2059010
    iget-object v2, p0, LX/Dvb;->a:LX/Dvf;

    iget-object v3, p0, LX/Dvb;->h:LX/Dv0;

    .line 2059011
    iget-object v4, v3, LX/Dv0;->b:LX/0Px;

    move-object v3, v4

    .line 2059012
    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Dvd;->a(LX/Dvm;LX/Dvf;LX/0Px;Z)LX/0Px;

    move-result-object v0

    .line 2059013
    iget-object v1, p0, LX/Dvb;->i:LX/Dvc;

    invoke-virtual {v1, v0}, LX/Dvc;->a(LX/0Px;)V

    .line 2059014
    :goto_0
    const v0, -0x40be8450

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2059015
    return-void

    .line 2059016
    :cond_1
    invoke-virtual {p0}, LX/Dvb;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Class",
            "<+",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2058900
    const-class v0, LX/Dw5;

    const-class v1, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    const-class v2, LX/DwB;

    const-class v3, LX/DwD;

    const-class v4, LX/DwC;

    const-class v5, LX/Dw4;

    const-class v6, LX/Dw3;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2058901
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2058937
    sget v1, LX/Dva;->a:I

    if-ne p2, v1, :cond_0

    .line 2058938
    :goto_0
    return-object v0

    .line 2058939
    :cond_0
    sget v1, LX/Dva;->b:I

    if-ne p2, v1, :cond_1

    .line 2058940
    invoke-static {p0, v0, p1}, LX/Dvb;->b(LX/Dvb;Landroid/view/View;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2058941
    :cond_1
    sget v0, LX/Dva;->c:I

    if-ne p2, v0, :cond_2

    .line 2058942
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2058943
    :cond_2
    sget v0, LX/Dva;->h:I

    if-ne p2, v0, :cond_3

    .line 2058944
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/Dvb;->a(Landroid/content/Context;)LX/Dw3;

    move-result-object v0

    goto :goto_0

    .line 2058945
    :cond_3
    sget v0, LX/Dva;->d:I

    if-ne p2, v0, :cond_4

    .line 2058946
    new-instance v0, LX/DwB;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DwB;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2058947
    :cond_4
    sget v0, LX/Dva;->e:I

    if-ne p2, v0, :cond_5

    .line 2058948
    new-instance v0, LX/DwD;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DwD;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2058949
    :cond_5
    sget v0, LX/Dva;->f:I

    if-ne p2, v0, :cond_6

    .line 2058950
    new-instance v0, LX/DwC;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DwC;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2058951
    :cond_6
    sget v0, LX/Dva;->g:I

    if-ne p2, v0, :cond_7

    .line 2058952
    new-instance v0, LX/Dw4;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Dw4;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2058953
    :cond_7
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MPK we have a problem - multiPhotoRow has no photos to render"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(I)Ljava/lang/Class;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/Class",
            "<+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2058954
    invoke-virtual {p0, p1}, LX/Dvb;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 2058955
    if-nez v1, :cond_0

    move-object v0, v2

    .line 2058956
    :goto_0
    return-object v0

    .line 2058957
    :cond_0
    instance-of v0, v1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraStoryHeaderRow;

    if-nez v0, :cond_1

    sget-object v0, LX/Dvb;->u:Ljava/lang/Object;

    if-ne v1, v0, :cond_2

    .line 2058958
    :cond_1
    const-class v0, LX/Dw5;

    goto :goto_0

    .line 2058959
    :cond_2
    sget-object v0, LX/Dvb;->v:Ljava/lang/Object;

    if-ne v1, v0, :cond_3

    .line 2058960
    const-class v0, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    goto :goto_0

    .line 2058961
    :cond_3
    instance-of v0, v1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 2058962
    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    .line 2058963
    iget-object v3, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    .line 2058964
    packed-switch v3, :pswitch_data_0

    .line 2058965
    :cond_4
    instance-of v0, v1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererCaptionRow;

    if-eqz v0, :cond_7

    .line 2058966
    const-class v0, LX/Dw3;

    goto :goto_0

    .line 2058967
    :pswitch_0
    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2058968
    iget-boolean v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, LX/Dvb;->m()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2058969
    const-class v0, LX/DwC;

    goto :goto_0

    .line 2058970
    :cond_5
    const-class v0, LX/DwB;

    goto :goto_0

    .line 2058971
    :pswitch_1
    invoke-virtual {p0}, LX/Dvb;->l()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2058972
    const-class v0, LX/DwD;

    goto :goto_0

    .line 2058973
    :cond_6
    :pswitch_2
    const-class v0, LX/DwC;

    goto :goto_0

    .line 2058974
    :pswitch_3
    const-class v0, LX/Dw4;

    goto :goto_0

    :cond_7
    move-object v0, v2

    .line 2058975
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Ljava/lang/String;ZZZ)V
    .locals 2

    .prologue
    .line 2058976
    iput-object p3, p0, LX/Dvb;->n:Ljava/lang/String;

    .line 2058977
    iput-boolean p4, p0, LX/Dvb;->o:Z

    .line 2058978
    iput-boolean p5, p0, LX/Dvb;->p:Z

    .line 2058979
    iput-boolean p6, p0, LX/Dvb;->r:Z

    .line 2058980
    iget-object v0, p0, LX/Dvb;->j:Ljava/lang/String;

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058981
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2058982
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058983
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2058984
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Dvb;->h:LX/Dv0;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dvb;->h:LX/Dv0;

    .line 2058985
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2058986
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Dvb;->h:LX/Dv0;

    .line 2058987
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2058988
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2058989
    :cond_0
    :goto_0
    return-void

    .line 2058990
    :cond_1
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2058991
    iput-object p1, p0, LX/Dvb;->j:Ljava/lang/String;

    .line 2058992
    iput-object p2, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    .line 2058993
    iget-object v0, p0, LX/Dvb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dux;

    invoke-virtual {p0}, LX/Dvb;->i()Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Dux;->a(Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;)LX/Dv0;

    move-result-object v0

    iput-object v0, p0, LX/Dvb;->h:LX/Dv0;

    .line 2058994
    new-instance v0, LX/Dvc;

    invoke-direct {v0}, LX/Dvc;-><init>()V

    iput-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058995
    iget-boolean v0, p0, LX/Dvb;->r:Z

    if-eqz v0, :cond_2

    .line 2058996
    iget-object v0, p0, LX/Dvb;->e:LX/Dw8;

    new-instance v1, Lcom/facebook/photos/pandora/common/ui/feedadapter/PandoraBasicFeedAdapter$1;

    invoke-direct {v1, p0}, Lcom/facebook/photos/pandora/common/ui/feedadapter/PandoraBasicFeedAdapter$1;-><init>(LX/Dvb;)V

    .line 2058997
    new-instance p4, LX/Dw7;

    invoke-static {v0}, LX/1QF;->b(LX/0QB;)LX/1QF;

    move-result-object p1

    check-cast p1, LX/1QF;

    const-class p2, LX/1QC;

    invoke-interface {v0, p2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p2

    check-cast p2, LX/1QC;

    invoke-static {v0}, LX/1Q2;->a(LX/0QB;)LX/1Q2;

    move-result-object p3

    check-cast p3, LX/1Q2;

    invoke-direct {p4, v1, p1, p2, p3}, LX/Dw7;-><init>(Ljava/lang/Runnable;LX/1QF;LX/1QC;LX/1Q2;)V

    .line 2058998
    move-object v0, p4

    .line 2058999
    iput-object v0, p0, LX/Dvb;->s:LX/Dw7;

    .line 2059000
    :cond_2
    invoke-direct {p0}, LX/Dvb;->o()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 7

    .prologue
    .line 2058859
    new-instance v2, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    invoke-direct {v2, p1}, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, LX/Dvb;->a(Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Ljava/lang/String;ZZZ)V

    .line 2058860
    return-void
.end method

.method public final b(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2058902
    if-nez p1, :cond_0

    invoke-virtual {p0}, LX/Dvb;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2058903
    sget v0, LX/Dva;->b:I

    .line 2058904
    :goto_0
    return v0

    .line 2058905
    :cond_0
    invoke-virtual {p0}, LX/Dvb;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/Dvb;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 2058906
    sget v0, LX/Dva;->c:I

    goto :goto_0

    .line 2058907
    :cond_1
    invoke-virtual {p0}, LX/Dvb;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 p1, p1, -0x1

    .line 2058908
    :cond_2
    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058909
    iget-object v2, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v2

    .line 2058910
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058911
    iget-object v2, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v2

    .line 2058912
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    if-ltz p1, :cond_3

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058913
    iget-object v2, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v2

    .line 2058914
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_4

    .line 2058915
    :cond_3
    sget v0, LX/Dva;->a:I

    goto :goto_0

    .line 2058916
    :cond_4
    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058917
    iget-object v2, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v2

    .line 2058918
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;

    .line 2058919
    instance-of v2, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraStoryHeaderRow;

    if-eqz v2, :cond_5

    .line 2058920
    sget v0, LX/Dva;->b:I

    goto :goto_0

    .line 2058921
    :cond_5
    instance-of v2, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererCaptionRow;

    if-eqz v2, :cond_6

    .line 2058922
    sget v0, LX/Dva;->h:I

    goto :goto_0

    .line 2058923
    :cond_6
    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    .line 2058924
    iget-object v2, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 2058925
    packed-switch v2, :pswitch_data_0

    .line 2058926
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MPK we have a problem - multiPhotoRow has no photos to render"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2058927
    :pswitch_0
    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2058928
    iget-boolean v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    if-nez v0, :cond_7

    invoke-virtual {p0}, LX/Dvb;->m()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_7
    const/4 v0, 0x1

    .line 2058929
    :goto_1
    if-eqz v0, :cond_9

    sget v0, LX/Dva;->d:I

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 2058930
    goto :goto_1

    .line 2058931
    :cond_9
    sget v0, LX/Dva;->f:I

    goto/16 :goto_0

    .line 2058932
    :pswitch_1
    invoke-virtual {p0}, LX/Dvb;->l()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2058933
    sget v0, LX/Dva;->e:I

    goto/16 :goto_0

    .line 2058934
    :cond_a
    sget v0, LX/Dva;->f:I

    goto/16 :goto_0

    .line 2058935
    :pswitch_2
    sget v0, LX/Dva;->f:I

    goto/16 :goto_0

    .line 2058936
    :pswitch_3
    sget v0, LX/Dva;->g:I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2058750
    iget-object v1, p0, LX/Dvb;->h:LX/Dv0;

    if-nez v1, :cond_1

    .line 2058751
    :cond_0
    :goto_0
    return v0

    .line 2058752
    :cond_1
    iget-object v1, p0, LX/Dvb;->h:LX/Dv0;

    .line 2058753
    iget-object v2, v1, LX/Dv0;->b:LX/0Px;

    move-object v1, v2

    .line 2058754
    if-eqz v1, :cond_0

    .line 2058755
    iget-object v1, p0, LX/Dvb;->h:LX/Dv0;

    .line 2058756
    iget-object v2, v1, LX/Dv0;->b:LX/0Px;

    move-object v1, v2

    .line 2058757
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2058768
    invoke-virtual {p0}, LX/Dvb;->j()V

    .line 2058769
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Dvb;->m:Z

    .line 2058770
    iget-object v0, p0, LX/Dvb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dux;

    invoke-virtual {p0}, LX/Dvb;->i()Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Dux;->a(Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;)LX/Dv0;

    move-result-object v0

    iput-object v0, p0, LX/Dvb;->h:LX/Dv0;

    .line 2058771
    new-instance v0, LX/Dvc;

    invoke-direct {v0}, LX/Dvc;-><init>()V

    iput-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058772
    invoke-direct {p0}, LX/Dvb;->o()V

    .line 2058773
    return-void
.end method

.method public abstract d()V
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()LX/DvW;
.end method

.method public final getCount()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2058758
    invoke-virtual {p0}, LX/Dvb;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2058759
    :goto_0
    invoke-virtual {p0}, LX/Dvb;->n()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2058760
    :goto_1
    invoke-virtual {p0}, LX/Dvb;->h()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2058761
    add-int/2addr v0, v1

    .line 2058762
    :goto_2
    return v0

    :cond_0
    move v0, v2

    .line 2058763
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2058764
    goto :goto_1

    .line 2058765
    :cond_2
    iget-object v2, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058766
    iget-object v3, v2, LX/Dvc;->a:LX/0Px;

    move-object v2, v3

    .line 2058767
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    goto :goto_2
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2058774
    if-nez p1, :cond_0

    invoke-virtual {p0}, LX/Dvb;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2058775
    sget-object v0, LX/Dvb;->u:Ljava/lang/Object;

    .line 2058776
    :goto_0
    return-object v0

    .line 2058777
    :cond_0
    invoke-virtual {p0}, LX/Dvb;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/Dvb;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 2058778
    sget-object v0, LX/Dvb;->v:Ljava/lang/Object;

    goto :goto_0

    .line 2058779
    :cond_1
    invoke-virtual {p0}, LX/Dvb;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 p1, p1, -0x1

    .line 2058780
    :cond_2
    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058781
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2058782
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058783
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2058784
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    if-ltz p1, :cond_3

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058785
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2058786
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_4

    .line 2058787
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 2058788
    :cond_4
    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058789
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2058790
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2058791
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2058792
    invoke-virtual {p0, p1}, LX/Dvb;->b(I)I

    move-result v0

    .line 2058793
    sget v1, LX/Dva;->a:I

    if-ne v0, v1, :cond_0

    .line 2058794
    const/4 v0, 0x0

    .line 2058795
    :goto_0
    return-object v0

    .line 2058796
    :cond_0
    sget v1, LX/Dva;->b:I

    if-ne v0, v1, :cond_1

    .line 2058797
    invoke-static {p0, p2, p3}, LX/Dvb;->b(LX/Dvb;Landroid/view/View;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2058798
    :cond_1
    sget v1, LX/Dva;->c:I

    if-ne v0, v1, :cond_2

    .line 2058799
    if-eqz p2, :cond_8

    .line 2058800
    check-cast p2, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    .line 2058801
    :goto_1
    move-object v0, p2

    .line 2058802
    goto :goto_0

    .line 2058803
    :cond_2
    sget v1, LX/Dva;->h:I

    if-ne v0, v1, :cond_3

    .line 2058804
    invoke-virtual {p0, p1}, LX/Dvb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererCaptionRow;

    .line 2058805
    if-eqz p2, :cond_9

    .line 2058806
    check-cast p2, LX/Dw3;

    .line 2058807
    :goto_2
    iget-object v1, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererCaptionRow;->a:Ljava/lang/String;

    .line 2058808
    iput-object v1, p2, LX/Dw3;->a:Ljava/lang/String;

    .line 2058809
    iget-object p3, p2, LX/Dw3;->a:Ljava/lang/String;

    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_a

    .line 2058810
    const-string p3, ""

    invoke-virtual {p2, p3}, LX/Dw3;->setText(Ljava/lang/CharSequence;)V

    .line 2058811
    :goto_3
    move-object v0, p2

    .line 2058812
    goto :goto_0

    .line 2058813
    :cond_3
    sget v1, LX/Dva;->d:I

    if-ne v0, v1, :cond_4

    .line 2058814
    invoke-virtual {p0, p1}, LX/Dvb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    invoke-direct {p0, p2, p3, v0}, LX/Dvb;->a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;)LX/DwB;

    move-result-object v0

    goto :goto_0

    .line 2058815
    :cond_4
    sget v1, LX/Dva;->e:I

    if-ne v0, v1, :cond_5

    .line 2058816
    invoke-virtual {p0, p1}, LX/Dvb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    invoke-direct {p0, p2, p3, v0}, LX/Dvb;->b(Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;)LX/DwD;

    move-result-object v0

    goto :goto_0

    .line 2058817
    :cond_5
    sget v1, LX/Dva;->f:I

    if-ne v0, v1, :cond_6

    .line 2058818
    invoke-virtual {p0, p1}, LX/Dvb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    invoke-direct {p0, p2, p3, v0}, LX/Dvb;->c(Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;)LX/DwC;

    move-result-object v0

    goto :goto_0

    .line 2058819
    :cond_6
    sget v1, LX/Dva;->g:I

    if-ne v0, v1, :cond_7

    .line 2058820
    invoke-virtual {p0, p1}, LX/Dvb;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    invoke-direct {p0, p2, p3, v0}, LX/Dvb;->d(Landroid/view/View;Landroid/view/ViewGroup;Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;)LX/Dw4;

    move-result-object v0

    goto :goto_0

    .line 2058821
    :cond_7
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MPK we have a problem - multiPhotoRow has no photos to render"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2058822
    :cond_8
    new-instance p2, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 2058823
    :cond_9
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/Dvb;->a(Landroid/content/Context;)LX/Dw3;

    move-result-object p2

    goto :goto_2

    .line 2058824
    :cond_a
    iget-object p3, p2, LX/Dw3;->a:Ljava/lang/String;

    invoke-virtual {p2, p3}, LX/Dw3;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 2058825
    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058826
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2058827
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058828
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2058829
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;
    .locals 3

    .prologue
    .line 2058830
    iget-object v0, p0, LX/Dvb;->f:Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    if-nez v0, :cond_1

    .line 2058831
    iget-object v0, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    if-nez v0, :cond_0

    .line 2058832
    iget-object v0, p0, LX/Dvb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, LX/Dvb;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mPandoraInstanceId was null when trying to create MemoryCacheEntryKey"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2058833
    :cond_0
    new-instance v0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    iget-object v1, p0, LX/Dvb;->l:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {p0}, LX/Dvb;->f()LX/DvW;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;-><init>(Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;)V

    iput-object v0, p0, LX/Dvb;->f:Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    .line 2058834
    :cond_1
    iget-object v0, p0, LX/Dvb;->f:Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    return-object v0
.end method

.method public final j()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2058835
    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    if-eqz v0, :cond_0

    .line 2058836
    iget-object v0, p0, LX/Dvb;->i:LX/Dvc;

    .line 2058837
    const/4 v2, 0x0

    iput-object v2, v0, LX/Dvc;->a:LX/0Px;

    .line 2058838
    :cond_0
    iget-object v0, p0, LX/Dvb;->h:LX/Dv0;

    if-eqz v0, :cond_1

    .line 2058839
    iget-object v0, p0, LX/Dvb;->h:LX/Dv0;

    const/4 v3, 0x0

    .line 2058840
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Dv0;->c:Z

    .line 2058841
    iput-object v3, v0, LX/Dv0;->d:Ljava/lang/String;

    .line 2058842
    iput-object v3, v0, LX/Dv0;->b:LX/0Px;

    .line 2058843
    iput-object v1, p0, LX/Dvb;->h:LX/Dv0;

    .line 2058844
    :cond_1
    iget-object v0, p0, LX/Dvb;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dvd;

    .line 2058845
    iget-object v2, v0, LX/Dvd;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Dvr;

    .line 2058846
    invoke-virtual {v2}, LX/Dvr;->clearUserData()V

    .line 2058847
    iput-object v1, p0, LX/Dvb;->g:LX/Dvm;

    .line 2058848
    iget-object v0, p0, LX/Dvb;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {p0}, LX/Dvb;->i()Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    move-result-object v0

    .line 2058849
    if-nez v0, :cond_2

    .line 2058850
    :goto_0
    iput-object v1, p0, LX/Dvb;->f:Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    .line 2058851
    iget-object v0, p0, LX/Dvb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {p0}, LX/Dvb;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2058852
    const v0, -0x1e9f87bb

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2058853
    return-void

    .line 2058854
    :cond_2
    sget-object v2, LX/Dux;->b:LX/0aq;

    invoke-virtual {v2, v0}, LX/0aq;->b(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 2058855
    const/4 v0, 0x0

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 2058856
    const/4 v0, 0x0

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 2058857
    const/4 v0, 0x0

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 2058858
    const/4 v0, 0x0

    return v0
.end method
