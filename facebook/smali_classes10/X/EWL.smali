.class public LX/EWL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EWH;
.implements LX/DSX;


# static fields
.field public static final a:LX/EWL;


# instance fields
.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/EWI;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2129929
    new-instance v0, LX/EWL;

    .line 2129930
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2129931
    invoke-direct {v0, v1}, LX/EWL;-><init>(LX/0Px;)V

    sput-object v0, LX/EWL;->a:LX/EWL;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/EWI;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2129919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129920
    iput-object p1, p0, LX/EWL;->b:LX/0Px;

    .line 2129921
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2129922
    iget-object v1, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v0, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWI;

    .line 2129923
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2129924
    invoke-virtual {v0}, LX/EWI;->a()I

    move-result v0

    add-int/2addr v2, v0

    .line 2129925
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2129926
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/EWL;->c:LX/0Px;

    .line 2129927
    iput v2, p0, LX/EWL;->d:I

    .line 2129928
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2129916
    invoke-virtual {p0, p1}, LX/EWL;->getSectionForPosition(I)I

    move-result v1

    .line 2129917
    iget-object v0, p0, LX/EWL;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v2, p1, v0

    .line 2129918
    iget-object v0, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWI;

    invoke-virtual {v0, v2}, LX/EWI;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2129907
    if-ltz p1, :cond_0

    .line 2129908
    iget v1, p0, LX/EWL;->d:I

    move v1, v1

    .line 2129909
    if-lt p1, v1, :cond_1

    .line 2129910
    :cond_0
    :goto_0
    return v0

    .line 2129911
    :cond_1
    invoke-virtual {p0, p1}, LX/EWL;->getSectionForPosition(I)I

    move-result v1

    .line 2129912
    iget-object v2, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2129913
    invoke-virtual {p0, p1}, LX/EWL;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 2129914
    instance-of v1, v1, LX/EWJ;

    if-nez v1, :cond_0

    .line 2129915
    invoke-virtual {p0, p1}, LX/EWL;->getSectionForPosition(I)I

    move-result v1

    invoke-virtual {p0, v1}, LX/EWL;->getPositionForSection(I)I

    move-result v1

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getPositionForSection(I)I
    .locals 1

    .prologue
    .line 2129904
    iget-object v0, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2129905
    const/4 v0, 0x0

    .line 2129906
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/EWL;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final getSectionForPosition(I)I
    .locals 2

    .prologue
    .line 2129863
    iget-object v0, p0, LX/EWL;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 2129864
    iget-object v0, p0, LX/EWL;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2129865
    if-gt v0, p1, :cond_0

    .line 2129866
    return v1

    .line 2129867
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 2129868
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "Position is not contained within any section"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2129879
    iget-object v0, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/Object;

    move v1, v2

    .line 2129880
    :goto_0
    iget-object v0, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 2129881
    iget-object v0, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWI;

    .line 2129882
    const/4 v3, 0x0

    .line 2129883
    iget-boolean v5, v0, LX/EWI;->d:Z

    move v5, v5

    .line 2129884
    if-eqz v5, :cond_4

    .line 2129885
    iget-boolean v5, v0, LX/EWI;->d:Z

    if-eqz v5, :cond_0

    iget-object v5, v0, LX/EWI;->c:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2129886
    :cond_0
    const/4 v5, 0x0

    .line 2129887
    :goto_1
    move-object v0, v5

    .line 2129888
    instance-of v5, v0, Ljava/lang/Character;

    if-eqz v5, :cond_2

    .line 2129889
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2129890
    :goto_2
    if-nez v0, :cond_1

    .line 2129891
    const-string v0, ""

    .line 2129892
    :cond_1
    aput-object v0, v4, v1

    .line 2129893
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2129894
    :cond_2
    instance-of v5, v0, Ljava/lang/CharSequence;

    if-eqz v5, :cond_3

    .line 2129895
    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_2

    .line 2129896
    :cond_3
    instance-of v5, v0, LX/EWK;

    if-eqz v5, :cond_7

    .line 2129897
    check-cast v0, LX/EWK;

    invoke-interface {v0}, LX/EWK;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2129898
    :cond_4
    iget-object v5, v0, LX/EWI;->c:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    move v5, v5

    .line 2129899
    if-nez v5, :cond_6

    .line 2129900
    invoke-virtual {v0, v2}, LX/EWI;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 2129901
    instance-of v5, v0, LX/EWK;

    if-eqz v5, :cond_6

    .line 2129902
    check-cast v0, LX/EWK;

    invoke-interface {v0}, LX/EWK;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2129903
    :cond_5
    return-object v4

    :cond_6
    move-object v0, v3

    goto :goto_2

    :cond_7
    move-object v0, v3

    goto :goto_2

    :cond_8
    iget-object v5, v0, LX/EWI;->c:LX/0Px;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2129878
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "sections"

    iget-object v2, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "sectionPositions"

    iget-object v2, p0, LX/EWL;->c:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v_(I)Z
    .locals 2

    .prologue
    .line 2129869
    if-ltz p1, :cond_0

    .line 2129870
    iget v0, p0, LX/EWL;->d:I

    move v0, v0

    .line 2129871
    if-lt p1, v0, :cond_1

    .line 2129872
    :cond_0
    const/4 v0, 0x0

    .line 2129873
    :goto_0
    return v0

    .line 2129874
    :cond_1
    invoke-virtual {p0, p1}, LX/EWL;->getSectionForPosition(I)I

    move-result v0

    .line 2129875
    iget-object v1, p0, LX/EWL;->b:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWI;

    .line 2129876
    iget-boolean v1, v0, LX/EWI;->d:Z

    move v0, v1

    .line 2129877
    goto :goto_0
.end method
