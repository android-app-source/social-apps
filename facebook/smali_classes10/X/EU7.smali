.class public LX/EU7;
.super LX/ETf;
.source ""


# instance fields
.field private final a:LX/EU4;


# direct methods
.method public constructor <init>(LX/EU4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2125771
    invoke-direct {p0}, LX/ETf;-><init>()V

    .line 2125772
    iput-object p1, p0, LX/EU7;->a:LX/EU4;

    .line 2125773
    return-void
.end method

.method public static b(LX/0QB;)LX/EU7;
    .locals 2

    .prologue
    .line 2125774
    new-instance v1, LX/EU7;

    invoke-static {p0}, LX/EU4;->a(LX/0QB;)LX/EU4;

    move-result-object v0

    check-cast v0, LX/EU4;

    invoke-direct {v1, v0}, LX/EU7;-><init>(LX/EU4;)V

    .line 2125775
    move-object v0, v1

    .line 2125776
    return-object v0
.end method


# virtual methods
.method public final a(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 6

    .prologue
    .line 2125777
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2125778
    invoke-interface {p3}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2125779
    invoke-interface {p3}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v5

    .line 2125780
    if-eqz v4, :cond_1

    .line 2125781
    iget-object v0, p0, LX/EU7;->a:LX/EU4;

    move v1, p2

    move v2, p1

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, LX/EU4;->a(IILcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 2125782
    :cond_0
    :goto_0
    return-void

    .line 2125783
    :cond_1
    invoke-static {p4}, LX/ESx;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2125784
    invoke-interface {p3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->ah()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionOpenVideoChannelFieldsModel$VideoChannelModel;->m()Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;

    move-result-object v0

    .line 2125785
    new-instance v1, LX/7Qj;

    invoke-direct {v1}, LX/7Qj;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoChannelFragmentsModels$VideoHomePublisherChannelFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2125786
    iput-object v0, v1, LX/7Qj;->a:Ljava/lang/String;

    .line 2125787
    move-object v0, v1

    .line 2125788
    iget-object v1, p4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2125789
    iput-object v1, v0, LX/7Qj;->b:Ljava/lang/String;

    .line 2125790
    move-object v0, v0

    .line 2125791
    iput-object v5, v0, LX/7Qj;->c:Ljava/lang/String;

    .line 2125792
    move-object v0, v0

    .line 2125793
    iput p2, v0, LX/7Qj;->d:I

    .line 2125794
    move-object v0, v0

    .line 2125795
    iput p1, v0, LX/7Qj;->e:I

    .line 2125796
    move-object v0, v0

    .line 2125797
    new-instance v1, LX/7Qk;

    invoke-direct {v1, v0}, LX/7Qk;-><init>(LX/7Qj;)V

    move-object v0, v1

    .line 2125798
    iget-object v1, p0, LX/EU7;->a:LX/EU4;

    sget-object v2, LX/04D;->VIDEO_HOME:LX/04D;

    invoke-virtual {v1, v2, v0}, LX/EU4;->a(LX/04D;LX/7Qk;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 1

    .prologue
    .line 2125799
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2125800
    iget-object v0, p0, LX/EU7;->a:LX/EU4;

    invoke-virtual {v0, p1}, LX/EU4;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2125801
    return-void
.end method

.method public final b(IILX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 6

    .prologue
    .line 2125802
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2125803
    invoke-interface {p3}, LX/9uc;->aw()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2125804
    invoke-interface {p3}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v5

    .line 2125805
    if-eqz v4, :cond_0

    .line 2125806
    iget-object v0, p0, LX/EU7;->a:LX/EU4;

    move v1, p2

    move v2, p1

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, LX/EU4;->a(IILcom/facebook/reaction/common/ReactionUnitComponentNode;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 2125807
    :cond_0
    return-void
.end method
