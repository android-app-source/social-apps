.class public LX/ESH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:Landroid/widget/ProgressBar;

.field public c:Landroid/widget/ProgressBar;

.field public d:Landroid/widget/ImageView;

.field private e:LX/ES8;

.field private f:LX/74w;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/74w;)V
    .locals 1

    .prologue
    .line 2122738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2122739
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/ES8;->a(LX/0QB;)LX/ES8;

    move-result-object v0

    check-cast v0, LX/ES8;

    iput-object v0, p0, LX/ESH;->e:LX/ES8;

    .line 2122740
    iput-object p2, p0, LX/ESH;->f:LX/74w;

    .line 2122741
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 2122742
    iget-object v0, p0, LX/ESH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(I)V

    .line 2122743
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/16 v4, 0x8

    .line 2122744
    iget-object v0, p0, LX/ESH;->f:LX/74w;

    instance-of v0, v0, Lcom/facebook/photos/base/photos/VaultLocalPhoto;

    if-nez v0, :cond_1

    .line 2122745
    :cond_0
    :goto_0
    return-void

    .line 2122746
    :cond_1
    iget-object v1, p0, LX/ESH;->e:LX/ES8;

    iget-object v0, p0, LX/ESH;->f:LX/74w;

    check-cast v0, Lcom/facebook/photos/base/photos/VaultLocalPhoto;

    .line 2122747
    iget-object v2, v0, Lcom/facebook/photos/base/photos/VaultLocalPhoto;->f:Ljava/lang/String;

    move-object v0, v2

    .line 2122748
    invoke-virtual {v1, v0}, LX/ES8;->a(Ljava/lang/String;)Lcom/facebook/vault/provider/VaultImageProviderRow;

    move-result-object v0

    .line 2122749
    if-eqz v0, :cond_0

    .line 2122750
    iget v1, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_3

    .line 2122751
    iget v1, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    if-eqz v1, :cond_2

    iget v1, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->f:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 2122752
    :cond_2
    const/16 v1, 0xff

    invoke-direct {p0, v1}, LX/ESH;->b(I)V

    .line 2122753
    iget-object v1, p0, LX/ESH;->f:LX/74w;

    iget-wide v2, v0, Lcom/facebook/vault/provider/VaultImageProviderRow;->b:J

    .line 2122754
    iput-wide v2, v1, LX/74w;->a:J

    .line 2122755
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/vault/provider/VaultImageProviderRow;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2122756
    iget-object v0, p0, LX/ESH;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2122757
    :cond_3
    :goto_2
    iget-object v0, p0, LX/ESH;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2122758
    iget-object v0, p0, LX/ESH;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 2122759
    :cond_4
    const/16 v1, 0x4b

    invoke-direct {p0, v1}, LX/ESH;->b(I)V

    goto :goto_1

    .line 2122760
    :cond_5
    iget-object v0, p0, LX/ESH;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2122761
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/ESG;

    if-eqz v0, :cond_0

    .line 2122762
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ESG;

    .line 2122763
    iget-object v1, v0, LX/ESG;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v1, p0, LX/ESH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2122764
    iget-object v1, v0, LX/ESG;->b:Landroid/widget/ProgressBar;

    iput-object v1, p0, LX/ESH;->b:Landroid/widget/ProgressBar;

    .line 2122765
    iget-object v1, v0, LX/ESG;->c:Landroid/widget/ProgressBar;

    iput-object v1, p0, LX/ESH;->c:Landroid/widget/ProgressBar;

    .line 2122766
    iget-object v0, v0, LX/ESG;->d:Landroid/widget/ImageView;

    iput-object v0, p0, LX/ESH;->d:Landroid/widget/ImageView;

    .line 2122767
    :goto_0
    iget-object v0, p0, LX/ESH;->b:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 2122768
    return-void

    .line 2122769
    :cond_0
    new-instance v1, LX/ESG;

    invoke-direct {v1}, LX/ESG;-><init>()V

    .line 2122770
    const v0, 0x7f0d303c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/ESH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2122771
    const v0, 0x7f0d303e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/ESH;->b:Landroid/widget/ProgressBar;

    .line 2122772
    const v0, 0x7f0d303f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/ESH;->c:Landroid/widget/ProgressBar;

    .line 2122773
    const v0, 0x7f0d303d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/ESH;->d:Landroid/widget/ImageView;

    .line 2122774
    iget-object v0, p0, LX/ESH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, v1, LX/ESG;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2122775
    iget-object v0, p0, LX/ESH;->b:Landroid/widget/ProgressBar;

    iput-object v0, v1, LX/ESG;->b:Landroid/widget/ProgressBar;

    .line 2122776
    iget-object v0, p0, LX/ESH;->c:Landroid/widget/ProgressBar;

    iput-object v0, v1, LX/ESG;->c:Landroid/widget/ProgressBar;

    .line 2122777
    iget-object v0, p0, LX/ESH;->d:Landroid/widget/ImageView;

    iput-object v0, v1, LX/ESG;->d:Landroid/widget/ImageView;

    .line 2122778
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method
