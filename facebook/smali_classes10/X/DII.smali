.class public final LX/DII;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/goodfriends/audience/AudienceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/audience/AudienceFragment;)V
    .locals 0

    .prologue
    .line 1983156
    iput-object p1, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1983168
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1983167
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    .line 1983157
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1983158
    iget-object v0, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->l:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1983159
    :goto_0
    iget-object v0, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1983160
    iput-object v1, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->m:Ljava/lang/String;

    .line 1983161
    iget-object v0, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->n:Landroid/os/Handler;

    iget-object v1, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/audience/AudienceFragment;->f:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1983162
    iget-object v0, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->n:Landroid/os/Handler;

    iget-object v1, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/audience/AudienceFragment;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    const v4, 0x7ca82480

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1983163
    iget-object v0, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->c:LX/9J2;

    sget-object v1, LX/9J1;->PICKER_SEARCH:LX/9J1;

    invoke-virtual {v0, v1}, LX/9J2;->a(LX/9J1;)V

    .line 1983164
    iget-object v0, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->c:LX/9J2;

    const-string v1, "Picker Search"

    invoke-virtual {v0, v1}, LX/9J2;->a(Ljava/lang/String;)V

    .line 1983165
    return-void

    .line 1983166
    :cond_0
    iget-object v0, p0, LX/DII;->a:Lcom/facebook/goodfriends/audience/AudienceFragment;

    iget-object v0, v0, Lcom/facebook/goodfriends/audience/AudienceFragment;->l:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method
