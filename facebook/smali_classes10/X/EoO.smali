.class public LX/EoO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Enm;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/EoO;


# instance fields
.field public final a:LX/EoJ;

.field public final b:LX/EoJ;

.field public final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/En6",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/EoJ;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/EoT;


# direct methods
.method public constructor <init>(LX/EoT;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2168125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2168126
    iput-object p1, p0, LX/EoO;->e:LX/EoT;

    .line 2168127
    new-instance v0, LX/EoK;

    invoke-direct {v0, p0}, LX/EoK;-><init>(LX/EoO;)V

    iput-object v0, p0, LX/EoO;->a:LX/EoJ;

    .line 2168128
    new-instance v0, LX/EoL;

    invoke-direct {v0, p0}, LX/EoL;-><init>(LX/EoO;)V

    iput-object v0, p0, LX/EoO;->b:LX/EoJ;

    .line 2168129
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-class v1, Lcom/facebook/entitycards/model/ScrollLoadTrigger;

    new-instance v2, LX/EoN;

    invoke-direct {v2, p0}, LX/EoN;-><init>(LX/EoO;)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-class v1, Lcom/facebook/entitycards/model/ScrollLoadError;

    new-instance v2, LX/EoM;

    invoke-direct {v2, p0}, LX/EoM;-><init>(LX/EoO;)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/EoO;->c:LX/0P1;

    .line 2168130
    iget-object v0, p0, LX/EoO;->a:LX/EoJ;

    iget-object v1, p0, LX/EoO;->b:LX/EoJ;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/EoO;->d:LX/0Rf;

    .line 2168131
    return-void
.end method

.method public static a(LX/0QB;)LX/EoO;
    .locals 4

    .prologue
    .line 2168132
    sget-object v0, LX/EoO;->f:LX/EoO;

    if-nez v0, :cond_1

    .line 2168133
    const-class v1, LX/EoO;

    monitor-enter v1

    .line 2168134
    :try_start_0
    sget-object v0, LX/EoO;->f:LX/EoO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2168135
    if-eqz v2, :cond_0

    .line 2168136
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2168137
    new-instance p0, LX/EoO;

    const-class v3, LX/EoT;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/EoT;

    invoke-direct {p0, v3}, LX/EoO;-><init>(LX/EoT;)V

    .line 2168138
    move-object v0, p0

    .line 2168139
    sput-object v0, LX/EoO;->f:LX/EoO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2168140
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2168141
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2168142
    :cond_1
    sget-object v0, LX/EoO;->f:LX/EoO;

    return-object v0

    .line 2168143
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2168144
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/EoJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2168145
    iget-object v0, p0, LX/EoO;->d:LX/0Rf;

    return-object v0
.end method

.method public final b()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/En6",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 2168146
    iget-object v0, p0, LX/EoO;->c:LX/0P1;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2168147
    const/4 v0, 0x1

    return v0
.end method
