.class public LX/Esa;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/EsY;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Esb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2175993
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Esa;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Esb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175994
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2175995
    iput-object p1, p0, LX/Esa;->b:LX/0Ot;

    .line 2175996
    return-void
.end method

.method public static a(LX/0QB;)LX/Esa;
    .locals 4

    .prologue
    .line 2175997
    const-class v1, LX/Esa;

    monitor-enter v1

    .line 2175998
    :try_start_0
    sget-object v0, LX/Esa;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175999
    sput-object v2, LX/Esa;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176000
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176001
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176002
    new-instance v3, LX/Esa;

    const/16 p0, 0x1f24

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Esa;-><init>(LX/0Ot;)V

    .line 2176003
    move-object v0, v3

    .line 2176004
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176005
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Esa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176006
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176007
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2176008
    const v0, 0x1d879c5b

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2176009
    const v0, 0x1d879c5b

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2176010
    check-cast p2, LX/EsZ;

    .line 2176011
    iget-object v0, p0, LX/Esa;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Esb;

    iget-object v1, p2, LX/EsZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    const/4 p2, 0x6

    const/4 p0, 0x0

    const/4 v7, 0x1

    .line 2176012
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2176013
    check-cast v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->D()LX/0Px;

    move-result-object v2

    const v4, -0x7d55b6c1

    invoke-static {v2, v4}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v4

    .line 2176014
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2176015
    check-cast v2, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2176016
    invoke-static {v2}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-nez v5, :cond_1

    move-object v2, v3

    .line 2176017
    :goto_0
    if-nez v2, :cond_2

    move-object v2, v3

    .line 2176018
    :goto_1
    if-eqz v2, :cond_0

    .line 2176019
    iget-object v3, v0, LX/Esb;->b:LX/1zC;

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v3, v2, v5}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 2176020
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x2

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    const v6, 0x7f020839

    invoke-virtual {v5, v6}, LX/1o5;->h(I)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Di;->r(I)LX/1Di;

    move-result-object v5

    const/16 v6, 0x14

    invoke-interface {v5, v6}, LX/1Di;->j(I)LX/1Di;

    move-result-object v5

    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a015e

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b1d8c

    invoke-interface {v4, p2, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b1d8d

    invoke-interface {v4, v7, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-static {p1}, LX/Esa;->onClick(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v4, 0x7f0b004e

    invoke-virtual {v2, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v4, 0x7f0a015e

    invoke-virtual {v2, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v4, 0x7f0b1d8c

    invoke-interface {v2, p2, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    invoke-static {p1}, LX/Esa;->onClick(LX/1De;)LX/1dQ;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2176021
    return-object v0

    .line 2176022
    :cond_1
    invoke-static {v2}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2176023
    :cond_2
    invoke-static {v2}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2176024
    invoke-static {}, LX/1dS;->b()V

    .line 2176025
    iget v0, p1, LX/1dQ;->b:I

    .line 2176026
    packed-switch v0, :pswitch_data_0

    .line 2176027
    :goto_0
    return-object v2

    .line 2176028
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2176029
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2176030
    check-cast v1, LX/EsZ;

    .line 2176031
    iget-object v3, p0, LX/Esa;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Esb;

    iget-object v4, v1, LX/EsZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176032
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2176033
    check-cast v5, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2176034
    iget-object v6, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 2176035
    check-cast v6, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v6

    .line 2176036
    iget-object p1, v3, LX/Esb;->c:LX/1Cn;

    .line 2176037
    const/4 p2, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result p0

    sparse-switch p0, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch p2, :pswitch_data_1

    .line 2176038
    const/4 p2, 0x0

    :goto_2
    move-object p2, p2

    .line 2176039
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object p0

    .line 2176040
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/3lw;->GOODWILL_THROWBACK_RESHARE_PROMOTION_SEE_ORIGINAL_POST:LX/3lw;

    iget-object v4, v4, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "goodwill"

    .line 2176041
    iput-object v4, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2176042
    move-object v1, v1

    .line 2176043
    const-string v4, "render_style"

    invoke-virtual {v1, v4, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v4, "story_type"

    invoke-virtual {v1, v4, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v4, "story_id"

    invoke-virtual {v1, v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2176044
    iget-object v4, p1, LX/1Cn;->a:LX/0Zb;

    invoke-interface {v4, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2176045
    iget-object v6, v3, LX/Esb;->a:LX/3Do;

    invoke-static {v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/3Do;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2tm;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/2tm;->onClick(Landroid/view/View;)V

    .line 2176046
    goto :goto_0

    .line 2176047
    :sswitch_0
    const-string p0, "reshare_status_update"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p2, 0x0

    goto :goto_1

    :sswitch_1
    const-string p0, "reshare_photo"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p2, 0x1

    goto :goto_1

    :sswitch_2
    const-string p0, "reshare_video"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p2, 0x2

    goto :goto_1

    .line 2176048
    :pswitch_1
    const-string p2, "reshare_status_update"

    goto :goto_2

    .line 2176049
    :pswitch_2
    const-string p2, "reshare_photo"

    goto :goto_2

    .line 2176050
    :pswitch_3
    const-string p2, "reshare_video"

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1d879c5b
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x201d4843 -> :sswitch_0
        0x578271bf -> :sswitch_1
        0x57d74808 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
