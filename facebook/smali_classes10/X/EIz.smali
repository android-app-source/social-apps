.class public LX/EIz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0Ot",
            "<",
            "LX/1RB",
            "<*>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/PulseStoriesGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/composer/SearchComposerSinglePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityPhotoPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsAwarenessNodePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsWayfinderPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2102653
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/EIz;->a:Ljava/util/Map;

    .line 2102654
    const-class v1, Lcom/facebook/search/results/model/SearchResultsBridge;

    invoke-direct {p0, v1, p1}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102655
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;

    invoke-direct {p0, v1, p2}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102656
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;

    invoke-direct {p0, v1, p3}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102657
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsPostHeaderUnit;

    invoke-direct {p0, v1, p4}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102658
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;

    invoke-direct {p0, v1, p5}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102659
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;

    invoke-direct {p0, v1, p6}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102660
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    invoke-direct {p0, v1, p7}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102661
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;

    invoke-direct {p0, v1, p8}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102662
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    invoke-direct {p0, v1, p9}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102663
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;

    invoke-direct {p0, v1, p10}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102664
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-direct {p0, v1, p11}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102665
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-direct {p0, v1, p12}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102666
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-direct {p0, v1, p13}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102667
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102668
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;

    move-object/from16 v0, p14

    invoke-direct {p0, v1, v0}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102669
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;

    move-object/from16 v0, p15

    invoke-direct {p0, v1, v0}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102670
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    move-object/from16 v0, p18

    invoke-direct {p0, v1, v0}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102671
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;

    move-object/from16 v0, p19

    invoke-direct {p0, v1, v0}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102672
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;

    move-object/from16 v0, p16

    invoke-direct {p0, v1, v0}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102673
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsAwarenessUnit;

    move-object/from16 v0, p20

    invoke-direct {p0, v1, v0}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102674
    const-class v1, Lcom/facebook/search/results/model/unit/SearchResultsWayfinderUnit;

    move-object/from16 v0, p21

    invoke-direct {p0, v1, v0}, LX/EIz;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102675
    return-void
.end method

.method public static a(LX/0QB;)LX/EIz;
    .locals 3

    .prologue
    .line 2102640
    const-class v1, LX/EIz;

    monitor-enter v1

    .line 2102641
    :try_start_0
    sget-object v0, LX/EIz;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2102642
    sput-object v2, LX/EIz;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2102643
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102644
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/EIz;->b(LX/0QB;)LX/EIz;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2102645
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EIz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2102646
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2102647
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/Class;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+TT;>;",
            "LX/0Ot",
            "<",
            "LX/1RB",
            "<+TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 2102650
    iget-object v0, p0, LX/EIz;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102651
    return-void
.end method

.method private static b(LX/0QB;)LX/EIz;
    .locals 24

    .prologue
    .line 2102648
    new-instance v2, LX/EIz;

    const/16 v3, 0x3373

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x3450

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3496

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x33c0

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x118d

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x344f

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3477

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x347e

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x347d

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x1166

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x337a

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x3489

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x349b

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x33bb

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x114e

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x349c

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x3414

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x3391

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x341b

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x1154

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x1178

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    invoke-direct/range {v2 .. v23}, LX/EIz;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2102649
    return-object v2
.end method
