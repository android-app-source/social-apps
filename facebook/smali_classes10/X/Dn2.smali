.class public final LX/Dn2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Dju;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

.field public final synthetic b:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;)V
    .locals 0

    .prologue
    .line 2039979
    iput-object p1, p0, LX/Dn2;->b:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    iput-object p2, p0, LX/Dn2;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/messaging/professionalservices/booking/protocol/FetchBookRequestsModels$AppointmentAdminNoteFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2039980
    if-nez p1, :cond_0

    .line 2039981
    const-string v0, ""

    .line 2039982
    iget-object v1, p0, LX/Dn2;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    const/4 v2, 0x0

    .line 2039983
    iput-object v2, v1, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel$AppointmentNote;

    .line 2039984
    iget-object v1, p0, LX/Dn2;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    .line 2039985
    iput-object v0, v1, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->c:Ljava/lang/String;

    .line 2039986
    iget-object v0, p0, LX/Dn2;->a:Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;

    sget-object v1, Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;->NO_NOTE:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2039987
    iput-object v1, v0, Lcom/facebook/messaging/professionalservices/booking/model/AppointmentNoteViewStateModel;->b:Lcom/facebook/messaging/professionalservices/booking/ui/AppointmentNoteView$ViewState;

    .line 2039988
    iget-object v0, p0, LX/Dn2;->b:Lcom/facebook/messaging/professionalservices/booking/ui/PageAdminAppointmentDetailAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2039989
    :cond_0
    return-void
.end method
