.class public LX/Da0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Da0;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2014431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2014432
    iput-object p1, p0, LX/Da0;->a:LX/0Ot;

    .line 2014433
    iput-object p2, p0, LX/Da0;->b:LX/0Ot;

    .line 2014434
    return-void
.end method

.method public static a(LX/0QB;)LX/Da0;
    .locals 5

    .prologue
    .line 2014418
    sget-object v0, LX/Da0;->c:LX/Da0;

    if-nez v0, :cond_1

    .line 2014419
    const-class v1, LX/Da0;

    monitor-enter v1

    .line 2014420
    :try_start_0
    sget-object v0, LX/Da0;->c:LX/Da0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2014421
    if-eqz v2, :cond_0

    .line 2014422
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2014423
    new-instance v3, LX/Da0;

    const/16 v4, 0x455

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xc49

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/Da0;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2014424
    move-object v0, v3

    .line 2014425
    sput-object v0, LX/Da0;->c:LX/Da0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2014426
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2014427
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2014428
    :cond_1
    sget-object v0, LX/Da0;->c:LX/Da0;

    return-object v0

    .line 2014429
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2014430
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
