.class public LX/Dpn;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static b:Z

.field private static c:Ljava/security/MessageDigest;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2047291
    const-class v0, LX/Dpn;

    sput-object v0, LX/Dpn;->a:Ljava/lang/Class;

    .line 2047292
    const/4 v0, 0x0

    sput-boolean v0, LX/Dpn;->b:Z

    .line 2047293
    const/4 v0, 0x0

    sput-object v0, LX/Dpn;->c:Ljava/security/MessageDigest;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2047289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047290
    return-void
.end method

.method public static declared-synchronized a()Ljava/security/MessageDigest;
    .locals 4

    .prologue
    .line 2047280
    const-class v1, LX/Dpn;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, LX/Dpn;->b:Z

    if-eqz v0, :cond_0

    .line 2047281
    sget-object v0, LX/Dpn;->c:Ljava/security/MessageDigest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2047282
    :goto_0
    monitor-exit v1

    return-object v0

    .line 2047283
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, LX/Dpn;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2047284
    :try_start_2
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    sput-object v0, LX/Dpn;->c:Ljava/security/MessageDigest;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2047285
    :goto_1
    :try_start_3
    sget-object v0, LX/Dpn;->c:Ljava/security/MessageDigest;

    goto :goto_0

    .line 2047286
    :catch_0
    move-exception v0

    .line 2047287
    sget-object v2, LX/Dpn;->a:Ljava/lang/Class;

    const-string v3, "SHA-256 Hasher unavailable"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 2047288
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(LX/1u2;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/1u2;",
            ">(TT;)[B"
        }
    .end annotation

    .prologue
    .line 2047271
    const/4 v0, 0x0

    .line 2047272
    :try_start_0
    new-instance v1, LX/1sp;

    invoke-direct {v1}, LX/1sp;-><init>()V

    .line 2047273
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2047274
    new-instance v3, LX/1sr;

    invoke-direct {v3, v2}, LX/1sr;-><init>(Ljava/io/OutputStream;)V

    invoke-interface {v1, v3}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v1

    .line 2047275
    invoke-interface {p0, v1}, LX/1u2;->a(LX/1su;)V

    .line 2047276
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2047277
    :goto_0
    return-object v0

    .line 2047278
    :catch_0
    move-exception v1

    .line 2047279
    sget-object v2, LX/Dpn;->a:Ljava/lang/Class;

    invoke-virtual {v1}, LX/7H0;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b([B)LX/DpY;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2047217
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 2047218
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 2047219
    new-instance v2, LX/1sr;

    invoke-direct {v2, v1}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    .line 2047220
    invoke-interface {v0, v2}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 2047221
    :try_start_0
    const/4 v4, 0x0

    .line 2047222
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    .line 2047223
    :goto_0
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v8

    .line 2047224
    iget-byte v9, v8, LX/1sw;->b:B

    if-eqz v9, :cond_5

    .line 2047225
    iget-short v9, v8, LX/1sw;->c:S

    packed-switch v9, :pswitch_data_0

    .line 2047226
    iget-byte v8, v8, LX/1sw;->b:B

    invoke-static {v0, v8}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047227
    :pswitch_0
    iget-byte v9, v8, LX/1sw;->b:B

    const/16 v10, 0x8

    if-ne v9, v10, :cond_0

    .line 2047228
    invoke-virtual {v0}, LX/1su;->m()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto :goto_0

    .line 2047229
    :cond_0
    iget-byte v8, v8, LX/1sw;->b:B

    invoke-static {v0, v8}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047230
    :pswitch_1
    iget-byte v9, v8, LX/1sw;->b:B

    const/16 v10, 0xc

    if-ne v9, v10, :cond_2

    .line 2047231
    new-instance v6, LX/DpZ;

    invoke-direct {v6}, LX/DpZ;-><init>()V

    .line 2047232
    new-instance v6, LX/DpZ;

    invoke-direct {v6}, LX/DpZ;-><init>()V

    .line 2047233
    const/4 v8, 0x0

    iput v8, v6, LX/DpZ;->setField_:I

    .line 2047234
    const/4 v8, 0x0

    iput-object v8, v6, LX/DpZ;->value_:Ljava/lang/Object;

    .line 2047235
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    .line 2047236
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v8

    .line 2047237
    invoke-virtual {v6, v0, v8}, LX/DpZ;->a(LX/1su;LX/1sw;)Ljava/lang/Object;

    move-result-object v9

    iput-object v9, v6, LX/DpZ;->value_:Ljava/lang/Object;

    .line 2047238
    iget-object v9, v6, LX/6kT;->value_:Ljava/lang/Object;

    if-eqz v9, :cond_1

    .line 2047239
    iget-short v8, v8, LX/1sw;->c:S

    iput v8, v6, LX/DpZ;->setField_:I

    .line 2047240
    :cond_1
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    .line 2047241
    invoke-virtual {v0}, LX/1su;->e()V

    .line 2047242
    move-object v6, v6

    .line 2047243
    move-object v6, v6

    .line 2047244
    goto :goto_0

    .line 2047245
    :cond_2
    iget-byte v8, v8, LX/1sw;->b:B

    invoke-static {v0, v8}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047246
    :pswitch_2
    iget-byte v9, v8, LX/1sw;->b:B

    const/16 v10, 0xb

    if-ne v9, v10, :cond_3

    .line 2047247
    invoke-virtual {v0}, LX/1su;->q()[B

    move-result-object v5

    goto :goto_0

    .line 2047248
    :cond_3
    iget-byte v8, v8, LX/1sw;->b:B

    invoke-static {v0, v8}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047249
    :pswitch_3
    iget-byte v9, v8, LX/1sw;->b:B

    const/16 v10, 0xa

    if-ne v9, v10, :cond_4

    .line 2047250
    invoke-virtual {v0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto/16 :goto_0

    .line 2047251
    :cond_4
    iget-byte v8, v8, LX/1sw;->b:B

    invoke-static {v0, v8}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2047252
    :cond_5
    invoke-virtual {v0}, LX/1su;->e()V

    .line 2047253
    new-instance v8, LX/DpY;

    invoke-direct {v8, v7, v6, v5, v4}, LX/DpY;-><init>(Ljava/lang/Integer;LX/DpZ;[BLjava/lang/Long;)V

    .line 2047254
    invoke-static {v8}, LX/DpY;->a(LX/DpY;)V

    .line 2047255
    move-object v0, v8
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2047256
    invoke-virtual {v2}, LX/1sr;->a()V

    :goto_1
    return-object v0

    .line 2047257
    :catch_0
    move-exception v0

    .line 2047258
    :goto_2
    :try_start_1
    sget-object v1, LX/Dpn;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2047259
    invoke-virtual {v2}, LX/1sr;->a()V

    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/1sr;->a()V

    throw v0

    .line 2047260
    :catch_1
    move-exception v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c([B)LX/DpK;
    .locals 4

    .prologue
    .line 2047261
    new-instance v0, LX/1sp;

    invoke-direct {v0}, LX/1sp;-><init>()V

    .line 2047262
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 2047263
    new-instance v2, LX/1sr;

    invoke-direct {v2, v1}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    .line 2047264
    invoke-interface {v0, v2}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 2047265
    :try_start_0
    invoke-static {v0}, LX/DpK;->b(LX/1su;)LX/DpK;
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2047266
    invoke-virtual {v2}, LX/1sr;->a()V

    :goto_0
    return-object v0

    .line 2047267
    :catch_0
    move-exception v0

    .line 2047268
    :goto_1
    :try_start_1
    sget-object v1, LX/Dpn;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2047269
    invoke-virtual {v2}, LX/1sr;->a()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/1sr;->a()V

    throw v0

    .line 2047270
    :catch_1
    move-exception v0

    goto :goto_1
.end method
