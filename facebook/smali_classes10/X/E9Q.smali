.class public abstract LX/E9Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field private b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/E9T;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2084377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/E9W;
.end method

.method public final a(LX/E9T;)V
    .locals 1
    .param p1    # LX/E9T;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2084378
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/E9Q;->b:LX/0am;

    .line 2084379
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2084380
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/E9Q;->a:Z

    .line 2084381
    iget-object v0, p0, LX/E9Q;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2084382
    iget-object v0, p0, LX/E9Q;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9T;

    invoke-interface {v0}, LX/E9T;->f()V

    .line 2084383
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2084384
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/E9Q;->a:Z

    .line 2084385
    iget-object v0, p0, LX/E9Q;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2084386
    iget-object v0, p0, LX/E9Q;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E9T;

    invoke-interface {v0}, LX/E9T;->f()V

    .line 2084387
    :cond_0
    return-void
.end method
