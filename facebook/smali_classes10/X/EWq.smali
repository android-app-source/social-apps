.class public final LX/EWq;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EWn;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EWq;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EWq;


# instance fields
.field public bitField0_:I

.field public end_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public start_:I

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2131530
    new-instance v0, LX/EWm;

    invoke-direct {v0}, LX/EWm;-><init>()V

    sput-object v0, LX/EWq;->a:LX/EWZ;

    .line 2131531
    new-instance v0, LX/EWq;

    invoke-direct {v0}, LX/EWq;-><init>()V

    .line 2131532
    sput-object v0, LX/EWq;->c:LX/EWq;

    invoke-direct {v0}, LX/EWq;->o()V

    .line 2131533
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2131525
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2131526
    iput-byte v0, p0, LX/EWq;->memoizedIsInitialized:B

    .line 2131527
    iput v0, p0, LX/EWq;->memoizedSerializedSize:I

    .line 2131528
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2131529
    iput-object v0, p0, LX/EWq;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 2131495
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2131496
    iput-byte v0, p0, LX/EWq;->memoizedIsInitialized:B

    .line 2131497
    iput v0, p0, LX/EWq;->memoizedSerializedSize:I

    .line 2131498
    invoke-direct {p0}, LX/EWq;->o()V

    .line 2131499
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v2

    .line 2131500
    const/4 v0, 0x0

    .line 2131501
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 2131502
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v3

    .line 2131503
    sparse-switch v3, :sswitch_data_0

    .line 2131504
    invoke-virtual {p0, p1, v2, p2, v3}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    .line 2131505
    goto :goto_0

    :sswitch_0
    move v0, v1

    .line 2131506
    goto :goto_0

    .line 2131507
    :sswitch_1
    iget v3, p0, LX/EWq;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, LX/EWq;->bitField0_:I

    .line 2131508
    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v3

    iput v3, p0, LX/EWq;->start_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2131509
    :catch_0
    move-exception v0

    .line 2131510
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2131511
    move-object v0, v0

    .line 2131512
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2131513
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EWq;->unknownFields:LX/EZQ;

    .line 2131514
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2131515
    :sswitch_2
    :try_start_2
    iget v3, p0, LX/EWq;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, LX/EWq;->bitField0_:I

    .line 2131516
    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v3

    iput v3, p0, LX/EWq;->end_:I
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2131517
    :catch_1
    move-exception v0

    .line 2131518
    :try_start_3
    new-instance v1, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2131519
    iput-object p0, v1, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2131520
    move-object v0, v1

    .line 2131521
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2131522
    :cond_1
    invoke-virtual {v2}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EWq;->unknownFields:LX/EZQ;

    .line 2131523
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2131524
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2131490
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2131491
    iput-byte v1, p0, LX/EWq;->memoizedIsInitialized:B

    .line 2131492
    iput v1, p0, LX/EWq;->memoizedSerializedSize:I

    .line 2131493
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EWq;->unknownFields:LX/EZQ;

    .line 2131494
    return-void
.end method

.method private static a(LX/EWq;)LX/EWo;
    .locals 1

    .prologue
    .line 2131489
    invoke-static {}, LX/EWo;->m()LX/EWo;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EWo;->a(LX/EWq;)LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2131486
    iput v0, p0, LX/EWq;->start_:I

    .line 2131487
    iput v0, p0, LX/EWq;->end_:I

    .line 2131488
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2131484
    new-instance v0, LX/EWo;

    invoke-direct {v0, p1}, LX/EWo;-><init>(LX/EYd;)V

    .line 2131485
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2131477
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2131478
    iget v0, p0, LX/EWq;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2131479
    iget v0, p0, LX/EWq;->start_:I

    invoke-virtual {p1, v1, v0}, LX/EWf;->a(II)V

    .line 2131480
    :cond_0
    iget v0, p0, LX/EWq;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2131481
    iget v0, p0, LX/EWq;->end_:I

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(II)V

    .line 2131482
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2131483
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2131534
    iget-byte v1, p0, LX/EWq;->memoizedIsInitialized:B

    .line 2131535
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2131536
    :goto_0
    return v0

    .line 2131537
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2131538
    :cond_1
    iput-byte v0, p0, LX/EWq;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2131467
    iget v0, p0, LX/EWq;->memoizedSerializedSize:I

    .line 2131468
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2131469
    :goto_0
    return v0

    .line 2131470
    :cond_0
    const/4 v0, 0x0

    .line 2131471
    iget v1, p0, LX/EWq;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2131472
    iget v0, p0, LX/EWq;->start_:I

    invoke-static {v2, v0}, LX/EWf;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2131473
    :cond_1
    iget v1, p0, LX/EWq;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2131474
    iget v1, p0, LX/EWq;->end_:I

    invoke-static {v3, v1}, LX/EWf;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2131475
    :cond_2
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2131476
    iput v0, p0, LX/EWq;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2131466
    iget-object v0, p0, LX/EWq;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2131465
    sget-object v0, LX/EYC;->h:LX/EYn;

    const-class v1, LX/EWq;

    const-class v2, LX/EWo;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EWq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2131464
    sget-object v0, LX/EWq;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2131463
    invoke-static {p0}, LX/EWq;->a(LX/EWq;)LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2131462
    invoke-static {}, LX/EWo;->m()LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2131460
    invoke-static {p0}, LX/EWq;->a(LX/EWq;)LX/EWo;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2131461
    sget-object v0, LX/EWq;->c:LX/EWq;

    return-object v0
.end method
