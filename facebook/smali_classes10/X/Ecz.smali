.class public LX/Ecz;
.super LX/Ecy;
.source ""


# instance fields
.field private final b:LX/Ed2;

.field private final c:Landroid/content/ContentValues;


# direct methods
.method public constructor <init>(Lorg/xmlpull/v1/XmlPullParser;LX/Ed2;)V
    .locals 1

    .prologue
    .line 2148145
    invoke-direct {p0, p1}, LX/Ecy;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 2148146
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, LX/Ecz;->c:Landroid/content/ContentValues;

    .line 2148147
    iput-object p2, p0, LX/Ecz;->b:LX/Ed2;

    .line 2148148
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 2148149
    const-string v0, "apn"

    iget-object v1, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2148150
    iget-object v0, p0, LX/Ecz;->c:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 2148151
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2148152
    iget-object v1, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    .line 2148153
    if-eqz v1, :cond_0

    .line 2148154
    iget-object v2, p0, LX/Ecz;->c:Landroid/content/ContentValues;

    iget-object v3, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v3, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2148155
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2148156
    :cond_1
    iget-object v0, p0, LX/Ecz;->b:LX/Ed2;

    if-eqz v0, :cond_3

    .line 2148157
    iget-object v0, p0, LX/Ecz;->b:LX/Ed2;

    iget-object v1, p0, LX/Ecz;->c:Landroid/content/ContentValues;

    .line 2148158
    const-string v2, "mcc"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/Ed5;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2148159
    const-string v3, "mnc"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/Ed5;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2148160
    const-string v4, "apn"

    invoke-virtual {v1, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/Ed5;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2148161
    :try_start_0
    iget-object v5, v0, LX/Ed2;->a:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-ne v5, v2, :cond_3

    iget-object v2, v0, LX/Ed2;->a:[I

    const/4 v5, 0x1

    aget v2, v2, v5

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, v0, LX/Ed2;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v0, LX/Ed2;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2148162
    :cond_2
    const-string v2, "type"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2148163
    const-string v3, "mmsc"

    invoke-virtual {v1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2148164
    const-string v4, "mmsproxy"

    invoke-virtual {v1, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2148165
    const-string v5, "mmsport"

    invoke-virtual {v1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2148166
    iget-object v6, v0, LX/Ed2;->c:Ljava/util/List;

    const/4 v8, 0x0

    .line 2148167
    if-nez v6, :cond_5

    move-object v7, v8

    .line 2148168
    :goto_1
    move-object v2, v7

    .line 2148169
    if-eqz v2, :cond_3

    .line 2148170
    iget-object v3, v0, LX/Ed2;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2148171
    :cond_3
    :goto_2
    iget-object v0, p0, LX/Ecy;->a:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    .line 2148172
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expecting end tag @"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ecy;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2148173
    :cond_4
    return-void

    :catch_0
    goto :goto_2

    .line 2148174
    :cond_5
    invoke-static {v2, v3, v4, v5}, LX/Ed3;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Ed3;

    move-result-object v9

    .line 2148175
    if-nez v9, :cond_6

    move-object v7, v8

    .line 2148176
    goto :goto_1

    .line 2148177
    :cond_6
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/Ecx;

    .line 2148178
    instance-of v1, v7, LX/Ed4;

    if-eqz v1, :cond_7

    check-cast v7, LX/Ed4;

    .line 2148179
    if-nez v9, :cond_9

    .line 2148180
    const/4 v1, 0x0

    .line 2148181
    :goto_3
    move v7, v1

    .line 2148182
    if-eqz v7, :cond_7

    move-object v7, v8

    .line 2148183
    goto :goto_1

    .line 2148184
    :cond_8
    new-instance v7, LX/Ed4;

    invoke-direct {v7, v6, v9}, LX/Ed4;-><init>(Ljava/util/List;LX/Ed3;)V

    goto :goto_1

    :cond_9
    iget-object v1, v7, LX/Ed4;->b:LX/Ed3;

    .line 2148185
    iget-object v2, v1, LX/Ed3;->a:Ljava/lang/String;

    invoke-virtual {v9}, LX/Ed3;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, v1, LX/Ed3;->b:Ljava/lang/String;

    invoke-virtual {v9}, LX/Ed3;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget v2, v1, LX/Ed3;->c:I

    invoke-virtual {v9}, LX/Ed3;->c()I

    move-result v7

    if-ne v2, v7, :cond_a

    const/4 v2, 0x1

    :goto_4
    move v1, v2

    .line 2148186
    goto :goto_3

    :cond_a
    const/4 v2, 0x0

    goto :goto_4
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148187
    const-string v0, "apns"

    return-object v0
.end method
