.class public LX/EA0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/EA6;

.field private final b:LX/1DS;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Db;

.field public final e:LX/EAV;

.field public final f:LX/EAH;

.field public final g:LX/79D;

.field public final h:LX/EAW;

.field public final i:LX/0kL;

.field public j:LX/EA5;

.field public k:Z

.field public l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

.field public m:LX/1Qq;

.field private n:Z

.field private o:Z

.field public p:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

.field private r:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

.field private s:LX/175;

.field public t:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/EA6;LX/1DS;LX/0Ot;LX/1Db;LX/EAV;LX/EAH;LX/79D;LX/EAW;LX/0kL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EA6;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;",
            "LX/1Db;",
            "LX/EAV;",
            "LX/EAH;",
            "LX/79D;",
            "LX/EAW;",
            "LX/0kL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2085093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2085094
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/EA0;->p:LX/0am;

    .line 2085095
    iput-object p1, p0, LX/EA0;->a:LX/EA6;

    .line 2085096
    iput-object p2, p0, LX/EA0;->b:LX/1DS;

    .line 2085097
    iput-object p3, p0, LX/EA0;->c:LX/0Ot;

    .line 2085098
    iput-object p4, p0, LX/EA0;->d:LX/1Db;

    .line 2085099
    iput-object p5, p0, LX/EA0;->e:LX/EAV;

    .line 2085100
    iput-object p6, p0, LX/EA0;->f:LX/EAH;

    .line 2085101
    iput-object p7, p0, LX/EA0;->g:LX/79D;

    .line 2085102
    iput-object p8, p0, LX/EA0;->h:LX/EAW;

    .line 2085103
    iput-object p9, p0, LX/EA0;->i:LX/0kL;

    .line 2085104
    return-void
.end method

.method private static a(LX/EA0;Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;LX/175;)V
    .locals 6
    .param p1    # Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2085008
    if-nez p1, :cond_0

    .line 2085009
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->o()Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->setVisibility(I)V

    .line 2085010
    :goto_0
    return-void

    .line 2085011
    :cond_0
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->o()Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->setVisibility(I)V

    .line 2085012
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->o()Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;

    move-result-object v0

    iget-object v1, p0, LX/EA0;->t:Ljava/lang/String;

    .line 2085013
    if-eqz p2, :cond_1

    invoke-interface {p2}, LX/175;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2085014
    :cond_1
    invoke-static {v0, p1}, LX/E9Z;->b(Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;)V

    .line 2085015
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->d()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->b()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a(DI)V

    .line 2085016
    :goto_1
    goto :goto_0

    .line 2085017
    :cond_2
    invoke-static {v0, p1}, LX/E9Z;->b(Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;)V

    .line 2085018
    const-string v2, "reviews_feed"

    invoke-virtual {v0, p2, v2, v1}, Lcom/facebook/reviews/ui/ReviewFeedOverallRatingView;->a(LX/175;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/EA0;
    .locals 10

    .prologue
    .line 2085088
    new-instance v0, LX/EA0;

    const-class v1, LX/EA6;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/EA6;

    invoke-static {p0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v2

    check-cast v2, LX/1DS;

    const/16 v3, 0x6bd

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v4

    check-cast v4, LX/1Db;

    invoke-static {p0}, LX/EAV;->b(LX/0QB;)LX/EAV;

    move-result-object v5

    check-cast v5, LX/EAV;

    invoke-static {p0}, LX/EAH;->b(LX/0QB;)LX/EAH;

    move-result-object v6

    check-cast v6, LX/EAH;

    invoke-static {p0}, LX/79D;->a(LX/0QB;)LX/79D;

    move-result-object v7

    check-cast v7, LX/79D;

    .line 2085089
    new-instance v9, LX/EAW;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-direct {v9, v8}, LX/EAW;-><init>(LX/0SG;)V

    .line 2085090
    move-object v8, v9

    .line 2085091
    check-cast v8, LX/EAW;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-direct/range {v0 .. v9}, LX/EA0;-><init>(LX/EA6;LX/1DS;LX/0Ot;LX/1Db;LX/EAV;LX/EAH;LX/79D;LX/EAW;LX/0kL;)V

    .line 2085092
    return-object v0
.end method


# virtual methods
.method public final a(DILX/0Px;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DI",
            "LX/0Px",
            "<",
            "Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel$HistogramModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2085065
    iget-object v0, p0, LX/EA0;->r:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    invoke-static {v0}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->a(Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;)Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    .line 2085066
    new-instance v3, LX/2tL;

    invoke-direct {v3}, LX/2tL;-><init>()V

    .line 2085067
    if-eqz v0, :cond_0

    .line 2085068
    new-instance v6, LX/2tL;

    invoke-direct {v6}, LX/2tL;-><init>()V

    .line 2085069
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->a()LX/0Px;

    move-result-object v7

    iput-object v7, v6, LX/2tL;->a:LX/0Px;

    .line 2085070
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->b()I

    move-result v7

    iput v7, v6, LX/2tL;->b:I

    .line 2085071
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->c()I

    move-result v7

    iput v7, v6, LX/2tL;->c:I

    .line 2085072
    invoke-virtual {v0}, Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;->d()D

    move-result-wide v8

    iput-wide v8, v6, LX/2tL;->d:D

    .line 2085073
    move-object v3, v6

    .line 2085074
    :goto_0
    iput-wide p1, v3, LX/2tL;->d:D

    .line 2085075
    move-object v3, v3

    .line 2085076
    iput p3, v3, LX/2tL;->b:I

    .line 2085077
    move-object v3, v3

    .line 2085078
    iput-object p4, v3, LX/2tL;->a:LX/0Px;

    .line 2085079
    move-object v3, v3

    .line 2085080
    invoke-virtual {v3}, LX/2tL;->a()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v3

    move-object v0, v3

    .line 2085081
    iput-object v0, p0, LX/EA0;->r:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 2085082
    iget-object v0, p0, LX/EA0;->r:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    iget-object v1, p0, LX/EA0;->s:LX/175;

    invoke-static {p0, v0, v1}, LX/EA0;->a(LX/EA0;Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;LX/175;)V

    .line 2085083
    return-void

    .line 2085084
    :cond_0
    const/4 v4, 0x5

    .line 2085085
    iput v4, v3, LX/2tL;->c:I

    .line 2085086
    move-object v3, v3

    .line 2085087
    goto :goto_0
.end method

.method public final a(Landroid/widget/ListView;Ljava/lang/String;ZLcom/facebook/reviews/ui/PageReviewsFeedFragment;)V
    .locals 10

    .prologue
    .line 2085105
    iput-object p2, p0, LX/EA0;->t:Ljava/lang/String;

    .line 2085106
    iput-boolean p3, p0, LX/EA0;->o:Z

    .line 2085107
    iput-object p4, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    .line 2085108
    iget-object v0, p0, LX/EA0;->a:LX/EA6;

    invoke-virtual {p1}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2085109
    sget-object v2, LX/EAC;->a:LX/EAC;

    move-object v2, v2

    .line 2085110
    new-instance v3, Lcom/facebook/reviews/controller/ReviewStoriesFeedController$1;

    invoke-direct {v3, p0}, Lcom/facebook/reviews/controller/ReviewStoriesFeedController$1;-><init>(LX/EA0;)V

    .line 2085111
    new-instance v4, LX/EA5;

    const-class v5, LX/EAP;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/EAP;

    move-object v6, v1

    move-object v7, v2

    move-object v8, v3

    move-object v9, p0

    invoke-direct/range {v4 .. v9}, LX/EA5;-><init>(LX/EAP;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/EA0;)V

    .line 2085112
    move-object v0, v4

    .line 2085113
    iput-object v0, p0, LX/EA0;->j:LX/EA5;

    .line 2085114
    iget-object v0, p0, LX/EA0;->b:LX/1DS;

    iget-object v1, p0, LX/EA0;->c:LX/0Ot;

    iget-object v2, p0, LX/EA0;->h:LX/EAW;

    invoke-virtual {v0, v1, v2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    iget-object v1, p0, LX/EA0;->j:LX/EA5;

    .line 2085115
    iput-object v1, v0, LX/1Ql;->f:LX/1PW;

    .line 2085116
    move-object v0, v0

    .line 2085117
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/EA0;->m:LX/1Qq;

    .line 2085118
    iget-object v0, p0, LX/EA0;->m:LX/1Qq;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2085119
    new-instance v0, LX/E9y;

    invoke-direct {v0, p0}, LX/E9y;-><init>(LX/EA0;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 2085120
    iget-object v0, p0, LX/EA0;->e:LX/EAV;

    iget-object v1, p0, LX/EA0;->t:Ljava/lang/String;

    iget-object v2, p0, LX/EA0;->j:LX/EA5;

    iget-object v3, p0, LX/EA0;->h:LX/EAW;

    invoke-virtual {v0, v1, v2, v3, p0}, LX/EAV;->a(Ljava/lang/String;LX/EA5;LX/EAW;LX/EA0;)V

    .line 2085121
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    new-instance v1, LX/E9z;

    invoke-direct {v1, p0}, LX/E9z;-><init>(LX/EA0;)V

    invoke-virtual {v0, v1}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->a(LX/62n;)V

    .line 2085122
    iget-object v0, p0, LX/EA0;->q:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    if-eqz v0, :cond_0

    .line 2085123
    iget-object v0, p0, LX/EA0;->q:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/EA0;->a(Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;Z)V

    .line 2085124
    :goto_0
    return-void

    .line 2085125
    :cond_0
    invoke-virtual {p0}, LX/EA0;->f()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;Z)V
    .locals 6
    .param p1    # Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2085034
    iget-object v0, p0, LX/EA0;->g:LX/79D;

    iget-object v2, p0, LX/EA0;->t:Ljava/lang/String;

    .line 2085035
    const-string v3, "reviews_feed_header_load_success"

    const-string v4, "reviews_feed"

    invoke-static {v0, v3, v4, v2}, LX/79D;->b(LX/79D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2085036
    iput-object p1, p0, LX/EA0;->q:Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;

    .line 2085037
    if-nez p1, :cond_1

    move-object v0, v1

    :goto_0
    iput-object v0, p0, LX/EA0;->r:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    .line 2085038
    if-nez p1, :cond_2

    :goto_1
    iput-object v1, p0, LX/EA0;->s:LX/175;

    .line 2085039
    iget-object v0, p0, LX/EA0;->r:Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    iget-object v1, p0, LX/EA0;->s:LX/175;

    invoke-static {p0, v0, v1}, LX/EA0;->a(LX/EA0;Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;LX/175;)V

    .line 2085040
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/EA0;->n:Z

    .line 2085041
    iget-boolean v0, p0, LX/EA0;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/EA0;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->d()Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel$ViewerRecommendationModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2085042
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    .line 2085043
    iget-object v1, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2085044
    iget-object v1, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->d:LX/EAB;

    iget-object v2, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->r:Landroid/widget/FrameLayout;

    .line 2085045
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f0311e2

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2085046
    iget-object v4, v1, LX/EAB;->a:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/User;

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2085047
    move-object v1, v3

    .line 2085048
    iget-object v2, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->d:LX/EAB;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->i:Ljava/lang/String;

    iget-object v5, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->j:Ljava/lang/String;

    .line 2085049
    new-instance p1, LX/EAA;

    invoke-direct {p1, v2, v4, v3, v5}, LX/EAA;-><init>(LX/EAB;Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2085050
    iget-object v2, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2085051
    :cond_0
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->m()V

    .line 2085052
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->p()V

    .line 2085053
    if-eqz p2, :cond_3

    .line 2085054
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/EA0;->k:Z

    .line 2085055
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q()V

    .line 2085056
    :goto_2
    return-void

    .line 2085057
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->b()Lcom/facebook/reviews/protocol/graphql/PageReviewsFragmentsModels$PageOverallStarRatingModel;

    move-result-object v0

    goto :goto_0

    .line 2085058
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/reviews/protocol/feedquery/ReviewsFeedQueryModels$ReviewsFeedHeaderModel;->c()LX/175;

    move-result-object v1

    goto :goto_1

    .line 2085059
    :cond_3
    iget-object v0, p0, LX/EA0;->h:LX/EAW;

    .line 2085060
    iget-object v1, v0, LX/EAW;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2085061
    iget-object v0, p0, LX/EA0;->m:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2085062
    iget-object v0, p0, LX/EA0;->f:LX/EAH;

    iget-object v1, p0, LX/EA0;->t:Ljava/lang/String;

    const/16 v2, 0xa

    .line 2085063
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p0, v3}, LX/EAH;->a(Ljava/lang/String;ILX/EA0;Ljava/lang/String;)V

    .line 2085064
    goto :goto_2
.end method

.method public final f()V
    .locals 6

    .prologue
    .line 2085022
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/EA0;->k:Z

    .line 2085023
    invoke-virtual {p0}, LX/EA0;->g()V

    .line 2085024
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->l()V

    .line 2085025
    iget-object v0, p0, LX/EA0;->f:LX/EAH;

    invoke-virtual {v0}, LX/EAH;->a()V

    .line 2085026
    iget-object v0, p0, LX/EA0;->f:LX/EAH;

    iget-object v1, p0, LX/EA0;->t:Ljava/lang/String;

    .line 2085027
    new-instance v2, LX/EB2;

    invoke-direct {v2}, LX/EB2;-><init>()V

    move-object v2, v2

    .line 2085028
    const-string v3, "page_id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2085029
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2085030
    invoke-static {v0, v2}, LX/EAH;->a(LX/EAH;LX/0zO;)V

    .line 2085031
    iget-object v3, v0, LX/EAH;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2085032
    iget-object v3, v0, LX/EAH;->b:LX/1Ck;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "key_load_feed_header"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/EAD;

    invoke-direct {v5, v0, p0}, LX/EAD;-><init>(LX/EAH;LX/EA0;)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2085033
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2085019
    iget-object v0, p0, LX/EA0;->l:Lcom/facebook/reviews/ui/PageReviewsFeedFragment;

    .line 2085020
    iget-object p0, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->r:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2085021
    return-void
.end method
