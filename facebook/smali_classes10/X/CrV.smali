.class public final LX/CrV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CqY;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/CqY",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/text/DecimalFormat;


# instance fields
.field private final b:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1941050
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/CrV;->a:Ljava/text/DecimalFormat;

    return-void
.end method

.method public constructor <init>(F)V
    .locals 0

    .prologue
    .line 1941047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1941048
    iput p1, p0, LX/CrV;->b:F

    .line 1941049
    return-void
.end method


# virtual methods
.method public final a(LX/CqY;F)LX/CqY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CqY",
            "<",
            "Ljava/lang/Float;",
            ">;F)",
            "LX/CqY",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1941045
    iget v1, p0, LX/CrV;->b:F

    invoke-interface {p1}, LX/CqY;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v1, v0, p2}, LX/CrU;->a(FFF)F

    move-result v0

    .line 1941046
    new-instance v1, LX/CrV;

    invoke-direct {v1, v0}, LX/CrV;-><init>(F)V

    return-object v1
.end method

.method public final a()LX/CrQ;
    .locals 1

    .prologue
    .line 1941044
    sget-object v0, LX/CrQ;->OPACITY:LX/CrQ;

    return-object v0
.end method

.method public final b()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 1941034
    iget v0, p0, LX/CrV;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/CqY;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/CqY",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1941043
    new-instance v0, LX/CrV;

    iget v1, p0, LX/CrV;->b:F

    invoke-direct {v0, v1}, LX/CrV;-><init>(F)V

    return-object v0
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1941042
    invoke-virtual {p0}, LX/CrV;->b()Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1941037
    if-ne p0, p1, :cond_1

    .line 1941038
    :cond_0
    :goto_0
    return v0

    .line 1941039
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1941040
    :cond_3
    check-cast p1, LX/CrV;

    .line 1941041
    iget v2, p0, LX/CrV;->b:F

    iget v3, p1, LX/CrV;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1941036
    iget v0, p0, LX/CrV;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1941035
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "{type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/CrV;->a()LX/CrQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", opacity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/CrV;->a:Ljava/text/DecimalFormat;

    iget v2, p0, LX/CrV;->b:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
