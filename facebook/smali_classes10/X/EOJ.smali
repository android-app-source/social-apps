.class public LX/EOJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/EOC;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EOC",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nB;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/EOC;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EOC;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2114266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2114267
    iput-object p1, p0, LX/EOJ;->a:LX/EOC;

    .line 2114268
    iput-object p2, p0, LX/EOJ;->b:LX/0Ot;

    .line 2114269
    iput-object p3, p0, LX/EOJ;->c:LX/0Ot;

    .line 2114270
    iput-object p4, p0, LX/EOJ;->d:LX/0Ot;

    .line 2114271
    iput-object p5, p0, LX/EOJ;->e:LX/0ad;

    .line 2114272
    return-void
.end method

.method public static a(LX/0QB;)LX/EOJ;
    .locals 9

    .prologue
    .line 2114273
    const-class v1, LX/EOJ;

    monitor-enter v1

    .line 2114274
    :try_start_0
    sget-object v0, LX/EOJ;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114275
    sput-object v2, LX/EOJ;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114276
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114277
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114278
    new-instance v3, LX/EOJ;

    invoke-static {v0}, LX/EOC;->a(LX/0QB;)LX/EOC;

    move-result-object v4

    check-cast v4, LX/EOC;

    const/16 v5, 0x455

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xbc6

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x32d4

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/EOJ;-><init>(LX/EOC;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V

    .line 2114279
    move-object v0, v3

    .line 2114280
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114281
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/EOJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114282
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114283
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/CzL;LX/0Px;LX/1Ps;)LX/1Dg;
    .locals 10
    .param p2    # LX/CzL;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/0Px;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Ps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleInterfaces$SearchResultsOpinionModule;",
            ">;",
            "LX/0Px",
            "<",
            "LX/C33;",
            ">;TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2114284
    iget-object v1, p0, LX/EOJ;->e:LX/0ad;

    sget-short v2, LX/100;->y:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 2114285
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    move v7, v0

    .line 2114286
    :goto_0
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v0

    if-ge v7, v0, :cond_2

    .line 2114287
    invoke-virtual {p3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/C33;

    .line 2114288
    invoke-virtual {v6}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v9

    .line 2114289
    invoke-virtual {v6}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2114290
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v1

    .line 2114291
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2114292
    invoke-virtual {p3}, LX/0Px;->size()I

    .line 2114293
    new-instance v0, LX/EOI;

    move-object v1, p0

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/EOI;-><init>(LX/EOJ;ZLcom/facebook/graphql/model/GraphQLStory;LX/1Ps;LX/CzL;)V

    .line 2114294
    new-instance v1, LX/C33;

    iget-object v3, v6, LX/C33;->b:LX/C34;

    iget-object v4, v6, LX/C33;->d:Ljava/lang/String;

    invoke-direct {v1, v0, v3, v9, v4}, LX/C33;-><init>(Landroid/view/View$OnClickListener;LX/C34;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 2114295
    iget-object v0, p0, LX/EOJ;->a:LX/EOC;

    const/4 v3, 0x0

    .line 2114296
    new-instance v4, LX/EOA;

    invoke-direct {v4, v0}, LX/EOA;-><init>(LX/EOC;)V

    .line 2114297
    iget-object v5, v0, LX/EOC;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/EO9;

    .line 2114298
    if-nez v5, :cond_0

    .line 2114299
    new-instance v5, LX/EO9;

    invoke-direct {v5, v0}, LX/EO9;-><init>(LX/EOC;)V

    .line 2114300
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/EO9;->a$redex0(LX/EO9;LX/1De;IILX/EOA;)V

    .line 2114301
    move-object v4, v5

    .line 2114302
    move-object v3, v4

    .line 2114303
    move-object v0, v3

    .line 2114304
    iget-object v3, v0, LX/EO9;->a:LX/EOA;

    iput-object v1, v3, LX/EOA;->a:LX/C33;

    .line 2114305
    iget-object v3, v0, LX/EO9;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2114306
    move-object v0, v0

    .line 2114307
    iget-object v1, v0, LX/EO9;->a:LX/EOA;

    iput-object p4, v1, LX/EOA;->b:LX/1Ps;

    .line 2114308
    iget-object v1, v0, LX/EO9;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2114309
    move-object v0, v0

    .line 2114310
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "story_key"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2114311
    iget-object v3, v0, LX/1X5;->d:LX/1X1;

    .line 2114312
    iput-object v1, v3, LX/1X1;->d:Ljava/lang/String;

    .line 2114313
    move-object v0, v0

    .line 2114314
    iget-object v1, v0, LX/EO9;->a:LX/EOA;

    iput-boolean v2, v1, LX/EOA;->c:Z

    .line 2114315
    iget-object v1, v0, LX/EO9;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2114316
    move-object v0, v0

    .line 2114317
    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2114318
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v7, v0, :cond_1

    .line 2114319
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const v1, 0x7f0a011a

    invoke-virtual {v0, v1}, LX/25Q;->i(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0033

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x4

    const v3, 0x7f0b0060

    invoke-interface {v0, v1, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2114320
    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0

    .line 2114321
    :cond_2
    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
