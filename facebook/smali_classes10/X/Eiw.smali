.class public final enum LX/Eiw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Eiw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Eiw;

.field public static final enum BACKGROUND_CONFIRMATION_START:LX/Eiw;

.field public static final enum BACKGROUND_CONFIRM_START:LX/Eiw;

.field public static final enum BACKGROUND_CONFIRM_SUCCEED:LX/Eiw;

.field public static final enum BACKGROUND_EMAIL_CONFIRMATION_FAILURE:LX/Eiw;

.field public static final enum BACKGROUND_EMAIL_CONFIRMATION_SUCCESS:LX/Eiw;

.field public static final enum BACKGROUND_EMAIL_CONFIRMATION_TASK_START:LX/Eiw;

.field public static final enum BACKGROUND_SMS_CONFIRMATION_ATTEMPT:LX/Eiw;

.field public static final enum BACKGROUND_SMS_CONFIRMATION_FAILURE:LX/Eiw;

.field public static final enum BACKGROUND_SMS_CONFIRMATION_NETWORK:LX/Eiw;

.field public static final enum BACKGROUND_SMS_CONFIRMATION_SUCCESS:LX/Eiw;

.field public static final enum BACKGROUND_SMS_DETECTED:LX/Eiw;

.field public static final enum BACK_PRESS:LX/Eiw;

.field public static final enum CANCEL_CLICK:LX/Eiw;

.field public static final enum CHANGE_CONTACTPOINT_ATTEMPT:LX/Eiw;

.field public static final enum CHANGE_CONTACTPOINT_COUNTRY_SELECTED:LX/Eiw;

.field public static final enum CHANGE_CONTACTPOINT_FAILURE:LX/Eiw;

.field public static final enum CHANGE_CONTACTPOINT_FLOW_ENTER:LX/Eiw;

.field public static final enum CHANGE_CONTACTPOINT_SUCCESS:LX/Eiw;

.field public static final enum CHANGE_NUMBER:LX/Eiw;

.field public static final enum CLOSE_NATIVE_FLOW:LX/Eiw;

.field public static final enum CONFIRMATION_ATTEMPT:LX/Eiw;

.field public static final enum CONFIRMATION_FAILURE:LX/Eiw;

.field public static final enum CONFIRMATION_IMPRESSION:LX/Eiw;

.field public static final enum CONFIRMATION_SPLIT_FIELD_CODE_PASTE:LX/Eiw;

.field public static final enum CONFIRMATION_SUCCESS:LX/Eiw;

.field public static final enum ENTER_PIN:LX/Eiw;

.field public static final enum FLOW_ENTER:LX/Eiw;

.field public static final enum FLOW_EXIT_SINCE_BACKGROUND_CONF:LX/Eiw;

.field public static final enum INVALID_NUMBER:LX/Eiw;

.field public static final enum INVALID_PIN:LX/Eiw;

.field public static final enum LOGOUT_CLICK:LX/Eiw;

.field public static final enum PHONE_NUMBER_ADD_ATTEMPT:LX/Eiw;

.field public static final enum RESEND_CODE:LX/Eiw;

.field public static final enum RESEND_CODE_ATTEMPT:LX/Eiw;

.field public static final enum RESEND_CODE_FAILURE:LX/Eiw;

.field public static final enum RESEND_CODE_SUCCESS:LX/Eiw;

.field public static final enum UPDATE_PHONE_NUMBER_IMPRESSION:LX/Eiw;

.field public static final enum VALID_NUMBER:LX/Eiw;

.field public static final enum VALID_PIN:LX/Eiw;


# instance fields
.field private final mAnalyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2160557
    new-instance v0, LX/Eiw;

    const-string v1, "FLOW_ENTER"

    const-string v2, "confirmation_flow_enter"

    invoke-direct {v0, v1, v4, v2}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->FLOW_ENTER:LX/Eiw;

    .line 2160558
    new-instance v0, LX/Eiw;

    const-string v1, "BACK_PRESS"

    const-string v2, "confirmation_back_press"

    invoke-direct {v0, v1, v5, v2}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACK_PRESS:LX/Eiw;

    .line 2160559
    new-instance v0, LX/Eiw;

    const-string v1, "CANCEL_CLICK"

    const-string v2, "confirmation_cancel_click"

    invoke-direct {v0, v1, v6, v2}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CANCEL_CLICK:LX/Eiw;

    .line 2160560
    new-instance v0, LX/Eiw;

    const-string v1, "LOGOUT_CLICK"

    const-string v2, "confirmation_logout_click"

    invoke-direct {v0, v1, v7, v2}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->LOGOUT_CLICK:LX/Eiw;

    .line 2160561
    new-instance v0, LX/Eiw;

    const-string v1, "FLOW_EXIT_SINCE_BACKGROUND_CONF"

    const-string v2, "flow_exit_since_background_conf"

    invoke-direct {v0, v1, v8, v2}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->FLOW_EXIT_SINCE_BACKGROUND_CONF:LX/Eiw;

    .line 2160562
    new-instance v0, LX/Eiw;

    const-string v1, "CONFIRMATION_ATTEMPT"

    const/4 v2, 0x5

    const-string v3, "confirmation_attempt"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CONFIRMATION_ATTEMPT:LX/Eiw;

    .line 2160563
    new-instance v0, LX/Eiw;

    const-string v1, "CONFIRMATION_SUCCESS"

    const/4 v2, 0x6

    const-string v3, "confirmation_success"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CONFIRMATION_SUCCESS:LX/Eiw;

    .line 2160564
    new-instance v0, LX/Eiw;

    const-string v1, "CONFIRMATION_FAILURE"

    const/4 v2, 0x7

    const-string v3, "confirmation_failure"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CONFIRMATION_FAILURE:LX/Eiw;

    .line 2160565
    new-instance v0, LX/Eiw;

    const-string v1, "RESEND_CODE_ATTEMPT"

    const/16 v2, 0x8

    const-string v3, "confirmation_resend_code_attempt"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->RESEND_CODE_ATTEMPT:LX/Eiw;

    .line 2160566
    new-instance v0, LX/Eiw;

    const-string v1, "RESEND_CODE_SUCCESS"

    const/16 v2, 0x9

    const-string v3, "confirmation_resend_code_success"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->RESEND_CODE_SUCCESS:LX/Eiw;

    .line 2160567
    new-instance v0, LX/Eiw;

    const-string v1, "RESEND_CODE_FAILURE"

    const/16 v2, 0xa

    const-string v3, "confirmation_resend_code_failure"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->RESEND_CODE_FAILURE:LX/Eiw;

    .line 2160568
    new-instance v0, LX/Eiw;

    const-string v1, "CHANGE_CONTACTPOINT_ATTEMPT"

    const/16 v2, 0xb

    const-string v3, "confirmation_change_contactpoint_attempt"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CHANGE_CONTACTPOINT_ATTEMPT:LX/Eiw;

    .line 2160569
    new-instance v0, LX/Eiw;

    const-string v1, "CHANGE_CONTACTPOINT_SUCCESS"

    const/16 v2, 0xc

    const-string v3, "confirmation_change_contactpoint_success"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CHANGE_CONTACTPOINT_SUCCESS:LX/Eiw;

    .line 2160570
    new-instance v0, LX/Eiw;

    const-string v1, "CHANGE_CONTACTPOINT_FAILURE"

    const/16 v2, 0xd

    const-string v3, "confirmation_change_contactpoint_failure"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CHANGE_CONTACTPOINT_FAILURE:LX/Eiw;

    .line 2160571
    new-instance v0, LX/Eiw;

    const-string v1, "CHANGE_CONTACTPOINT_FLOW_ENTER"

    const/16 v2, 0xe

    const-string v3, "confirmation_change_contactpoint_enter"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CHANGE_CONTACTPOINT_FLOW_ENTER:LX/Eiw;

    .line 2160572
    new-instance v0, LX/Eiw;

    const-string v1, "CHANGE_CONTACTPOINT_COUNTRY_SELECTED"

    const/16 v2, 0xf

    const-string v3, "confirmation_change_contactpoint_country_selected"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CHANGE_CONTACTPOINT_COUNTRY_SELECTED:LX/Eiw;

    .line 2160573
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_SMS_DETECTED"

    const/16 v2, 0x10

    const-string v3, "sms_auto_confirm_detected_v2"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_SMS_DETECTED:LX/Eiw;

    .line 2160574
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_CONFIRMATION_START"

    const/16 v2, 0x11

    const-string v3, "sms_auto_confirm_start_v2"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_CONFIRMATION_START:LX/Eiw;

    .line 2160575
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_SMS_CONFIRMATION_ATTEMPT"

    const/16 v2, 0x12

    const-string v3, "sms_auto_confirm_attempt_v2"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_ATTEMPT:LX/Eiw;

    .line 2160576
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_SMS_CONFIRMATION_SUCCESS"

    const/16 v2, 0x13

    const-string v3, "sms_auto_confirm_success"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_SUCCESS:LX/Eiw;

    .line 2160577
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_SMS_CONFIRMATION_FAILURE"

    const/16 v2, 0x14

    const-string v3, "sms_auto_confirm_failure"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_FAILURE:LX/Eiw;

    .line 2160578
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_SMS_CONFIRMATION_NETWORK"

    const/16 v2, 0x15

    const-string v3, "sms_auto_confirm_network_failure"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_NETWORK:LX/Eiw;

    .line 2160579
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_EMAIL_CONFIRMATION_TASK_START"

    const/16 v2, 0x16

    const-string v3, "email_auto_confirm_task_start"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_EMAIL_CONFIRMATION_TASK_START:LX/Eiw;

    .line 2160580
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_EMAIL_CONFIRMATION_SUCCESS"

    const/16 v2, 0x17

    const-string v3, "email_auto_confirm_success"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_EMAIL_CONFIRMATION_SUCCESS:LX/Eiw;

    .line 2160581
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_EMAIL_CONFIRMATION_FAILURE"

    const/16 v2, 0x18

    const-string v3, "email_auto_confirm_failure"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_EMAIL_CONFIRMATION_FAILURE:LX/Eiw;

    .line 2160582
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_CONFIRM_START"

    const/16 v2, 0x19

    const-string v3, "background_confirm_start"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_CONFIRM_START:LX/Eiw;

    .line 2160583
    new-instance v0, LX/Eiw;

    const-string v1, "CONFIRMATION_IMPRESSION"

    const/16 v2, 0x1a

    const-string v3, "confirmation_impression"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CONFIRMATION_IMPRESSION:LX/Eiw;

    .line 2160584
    new-instance v0, LX/Eiw;

    const-string v1, "ENTER_PIN"

    const/16 v2, 0x1b

    const-string v3, "enter_pin"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->ENTER_PIN:LX/Eiw;

    .line 2160585
    new-instance v0, LX/Eiw;

    const-string v1, "VALID_PIN"

    const/16 v2, 0x1c

    const-string v3, "valid_pin"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->VALID_PIN:LX/Eiw;

    .line 2160586
    new-instance v0, LX/Eiw;

    const-string v1, "INVALID_PIN"

    const/16 v2, 0x1d

    const-string v3, "invalid_pin"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->INVALID_PIN:LX/Eiw;

    .line 2160587
    new-instance v0, LX/Eiw;

    const-string v1, "RESEND_CODE"

    const/16 v2, 0x1e

    const-string v3, "resend_code"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->RESEND_CODE:LX/Eiw;

    .line 2160588
    new-instance v0, LX/Eiw;

    const-string v1, "CHANGE_NUMBER"

    const/16 v2, 0x1f

    const-string v3, "change_number"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CHANGE_NUMBER:LX/Eiw;

    .line 2160589
    new-instance v0, LX/Eiw;

    const-string v1, "UPDATE_PHONE_NUMBER_IMPRESSION"

    const/16 v2, 0x20

    const-string v3, "update_phone_number_impression"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->UPDATE_PHONE_NUMBER_IMPRESSION:LX/Eiw;

    .line 2160590
    new-instance v0, LX/Eiw;

    const-string v1, "PHONE_NUMBER_ADD_ATTEMPT"

    const/16 v2, 0x21

    const-string v3, "phone_number_add_attempt"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->PHONE_NUMBER_ADD_ATTEMPT:LX/Eiw;

    .line 2160591
    new-instance v0, LX/Eiw;

    const-string v1, "INVALID_NUMBER"

    const/16 v2, 0x22

    const-string v3, "invalid_number"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->INVALID_NUMBER:LX/Eiw;

    .line 2160592
    new-instance v0, LX/Eiw;

    const-string v1, "VALID_NUMBER"

    const/16 v2, 0x23

    const-string v3, "valid_number"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->VALID_NUMBER:LX/Eiw;

    .line 2160593
    new-instance v0, LX/Eiw;

    const-string v1, "CLOSE_NATIVE_FLOW"

    const/16 v2, 0x24

    const-string v3, "close_native_flow"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CLOSE_NATIVE_FLOW:LX/Eiw;

    .line 2160594
    new-instance v0, LX/Eiw;

    const-string v1, "BACKGROUND_CONFIRM_SUCCEED"

    const/16 v2, 0x25

    const-string v3, "background_confirm_succeed"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->BACKGROUND_CONFIRM_SUCCEED:LX/Eiw;

    .line 2160595
    new-instance v0, LX/Eiw;

    const-string v1, "CONFIRMATION_SPLIT_FIELD_CODE_PASTE"

    const/16 v2, 0x26

    const-string v3, "confirmation_split_field_code_paste"

    invoke-direct {v0, v1, v2, v3}, LX/Eiw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Eiw;->CONFIRMATION_SPLIT_FIELD_CODE_PASTE:LX/Eiw;

    .line 2160596
    const/16 v0, 0x27

    new-array v0, v0, [LX/Eiw;

    sget-object v1, LX/Eiw;->FLOW_ENTER:LX/Eiw;

    aput-object v1, v0, v4

    sget-object v1, LX/Eiw;->BACK_PRESS:LX/Eiw;

    aput-object v1, v0, v5

    sget-object v1, LX/Eiw;->CANCEL_CLICK:LX/Eiw;

    aput-object v1, v0, v6

    sget-object v1, LX/Eiw;->LOGOUT_CLICK:LX/Eiw;

    aput-object v1, v0, v7

    sget-object v1, LX/Eiw;->FLOW_EXIT_SINCE_BACKGROUND_CONF:LX/Eiw;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Eiw;->CONFIRMATION_ATTEMPT:LX/Eiw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Eiw;->CONFIRMATION_SUCCESS:LX/Eiw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Eiw;->CONFIRMATION_FAILURE:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Eiw;->RESEND_CODE_ATTEMPT:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Eiw;->RESEND_CODE_SUCCESS:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Eiw;->RESEND_CODE_FAILURE:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Eiw;->CHANGE_CONTACTPOINT_ATTEMPT:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Eiw;->CHANGE_CONTACTPOINT_SUCCESS:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Eiw;->CHANGE_CONTACTPOINT_FAILURE:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Eiw;->CHANGE_CONTACTPOINT_FLOW_ENTER:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Eiw;->CHANGE_CONTACTPOINT_COUNTRY_SELECTED:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Eiw;->BACKGROUND_SMS_DETECTED:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/Eiw;->BACKGROUND_CONFIRMATION_START:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_ATTEMPT:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_SUCCESS:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_FAILURE:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/Eiw;->BACKGROUND_SMS_CONFIRMATION_NETWORK:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/Eiw;->BACKGROUND_EMAIL_CONFIRMATION_TASK_START:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/Eiw;->BACKGROUND_EMAIL_CONFIRMATION_SUCCESS:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/Eiw;->BACKGROUND_EMAIL_CONFIRMATION_FAILURE:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/Eiw;->BACKGROUND_CONFIRM_START:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/Eiw;->CONFIRMATION_IMPRESSION:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/Eiw;->ENTER_PIN:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/Eiw;->VALID_PIN:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/Eiw;->INVALID_PIN:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/Eiw;->RESEND_CODE:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/Eiw;->CHANGE_NUMBER:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/Eiw;->UPDATE_PHONE_NUMBER_IMPRESSION:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/Eiw;->PHONE_NUMBER_ADD_ATTEMPT:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/Eiw;->INVALID_NUMBER:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/Eiw;->VALID_NUMBER:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/Eiw;->CLOSE_NATIVE_FLOW:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/Eiw;->BACKGROUND_CONFIRM_SUCCEED:LX/Eiw;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/Eiw;->CONFIRMATION_SPLIT_FIELD_CODE_PASTE:LX/Eiw;

    aput-object v2, v0, v1

    sput-object v0, LX/Eiw;->$VALUES:[LX/Eiw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2160597
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2160598
    iput-object p3, p0, LX/Eiw;->mAnalyticsName:Ljava/lang/String;

    .line 2160599
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Eiw;
    .locals 1

    .prologue
    .line 2160600
    const-class v0, LX/Eiw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Eiw;

    return-object v0
.end method

.method public static values()[LX/Eiw;
    .locals 1

    .prologue
    .line 2160601
    sget-object v0, LX/Eiw;->$VALUES:[LX/Eiw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Eiw;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2160602
    iget-object v0, p0, LX/Eiw;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2160603
    iget-object v0, p0, LX/Eiw;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method
