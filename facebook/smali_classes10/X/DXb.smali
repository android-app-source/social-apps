.class public final LX/DXb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DXd;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

.field public b:Z

.field public final synthetic c:LX/DXd;


# direct methods
.method public constructor <init>(LX/DXd;)V
    .locals 1

    .prologue
    .line 2010456
    iput-object p1, p0, LX/DXb;->c:LX/DXd;

    .line 2010457
    move-object v0, p1

    .line 2010458
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2010459
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2010476
    const-string v0, "ShareLinkRow"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2010463
    if-ne p0, p1, :cond_1

    .line 2010464
    :cond_0
    :goto_0
    return v0

    .line 2010465
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2010466
    goto :goto_0

    .line 2010467
    :cond_3
    check-cast p1, LX/DXb;

    .line 2010468
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2010469
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2010470
    if-eq v2, v3, :cond_0

    .line 2010471
    iget-object v2, p0, LX/DXb;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DXb;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    iget-object v3, p1, LX/DXb;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2010472
    goto :goto_0

    .line 2010473
    :cond_5
    iget-object v2, p1, LX/DXb;->a:Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;

    if-nez v2, :cond_4

    .line 2010474
    :cond_6
    iget-boolean v2, p0, LX/DXb;->b:Z

    iget-boolean v3, p1, LX/DXb;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2010475
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2010460
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/DXb;

    .line 2010461
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/DXb;->b:Z

    .line 2010462
    return-object v0
.end method
