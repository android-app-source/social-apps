.class public final enum LX/ESz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ESz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ESz;

.field public static final enum LIVE:LX/ESz;

.field public static final enum SEEN:LX/ESz;

.field public static final enum UNSEEN:LX/ESz;


# instance fields
.field private final status:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2123878
    new-instance v0, LX/ESz;

    const-string v1, "UNSEEN"

    const-string v2, "unseen"

    invoke-direct {v0, v1, v3, v2}, LX/ESz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ESz;->UNSEEN:LX/ESz;

    .line 2123879
    new-instance v0, LX/ESz;

    const-string v1, "SEEN"

    const-string v2, "seen"

    invoke-direct {v0, v1, v4, v2}, LX/ESz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ESz;->SEEN:LX/ESz;

    .line 2123880
    new-instance v0, LX/ESz;

    const-string v1, "LIVE"

    const-string v2, "live"

    invoke-direct {v0, v1, v5, v2}, LX/ESz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ESz;->LIVE:LX/ESz;

    .line 2123881
    const/4 v0, 0x3

    new-array v0, v0, [LX/ESz;

    sget-object v1, LX/ESz;->UNSEEN:LX/ESz;

    aput-object v1, v0, v3

    sget-object v1, LX/ESz;->SEEN:LX/ESz;

    aput-object v1, v0, v4

    sget-object v1, LX/ESz;->LIVE:LX/ESz;

    aput-object v1, v0, v5

    sput-object v0, LX/ESz;->$VALUES:[LX/ESz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2123889
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2123890
    iput-object p3, p0, LX/ESz;->status:Ljava/lang/String;

    .line 2123891
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/ESz;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2123885
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2123886
    :cond_0
    :goto_0
    return-object v0

    .line 2123887
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 2123888
    sget-object v2, LX/ESz;->LIVE:LX/ESz;

    invoke-virtual {v2}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, LX/ESz;->LIVE:LX/ESz;

    goto :goto_0

    :cond_2
    sget-object v2, LX/ESz;->UNSEEN:LX/ESz;

    invoke-virtual {v2}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, LX/ESz;->UNSEEN:LX/ESz;

    goto :goto_0

    :cond_3
    sget-object v2, LX/ESz;->SEEN:LX/ESz;

    invoke-virtual {v2}, LX/ESz;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, LX/ESz;->SEEN:LX/ESz;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/ESz;
    .locals 1

    .prologue
    .line 2123884
    const-class v0, LX/ESz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ESz;

    return-object v0
.end method

.method public static values()[LX/ESz;
    .locals 1

    .prologue
    .line 2123883
    sget-object v0, LX/ESz;->$VALUES:[LX/ESz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ESz;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2123882
    iget-object v0, p0, LX/ESz;->status:Ljava/lang/String;

    return-object v0
.end method
