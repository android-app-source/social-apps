.class public final LX/DJa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ARN;


# instance fields
.field public final synthetic c:LX/DJc;


# direct methods
.method public constructor <init>(LX/DJc;)V
    .locals 0

    .prologue
    .line 1985366
    iput-object p1, p0, LX/DJa;->c:LX/DJc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 1985367
    iget-object v1, p0, LX/DJa;->c:LX/DJc;

    iget-boolean v1, v1, LX/DJc;->i:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/DJa;->c:LX/DJc;

    const/4 v2, 0x0

    .line 1985368
    iget-object v3, v1, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v3}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getShouldPostToMarketplace()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1985369
    :cond_0
    :goto_0
    move v1, v2

    .line 1985370
    if-eqz v1, :cond_2

    .line 1985371
    :cond_1
    :goto_1
    return v0

    .line 1985372
    :cond_2
    iget-object v1, p0, LX/DJa;->c:LX/DJc;

    const/4 v2, 0x0

    .line 1985373
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    invoke-virtual {v3}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    .line 1985374
    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSavedSessionLoadAttempts()I

    move-result v4

    if-eqz v4, :cond_5

    .line 1985375
    :cond_3
    :goto_2
    move v1, v2

    .line 1985376
    if-nez v1, :cond_1

    .line 1985377
    iget-object v0, p0, LX/DJa;->c:LX/DJc;

    invoke-static {v0}, LX/DJc;->aN(LX/DJc;)V

    .line 1985378
    const/4 v0, 0x0

    goto :goto_1

    .line 1985379
    :cond_4
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    invoke-virtual {v3}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v3

    .line 1985380
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1985381
    iget-object v2, v1, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    const v3, 0x7f082f29

    invoke-virtual {v2, v3}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c(I)V

    .line 1985382
    const/4 v2, 0x1

    goto :goto_0

    .line 1985383
    :cond_5
    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getMarketplaceCrossPostSettingModel()Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    move-result-object v4

    .line 1985384
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->a()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->c()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1985385
    invoke-interface {v3}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v5

    .line 1985386
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1985387
    iget-object v5, v1, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v5}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getShouldPostToMarketplace()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1985388
    new-instance v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;

    invoke-direct {v2}, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;-><init>()V

    .line 1985389
    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    iget-wide v6, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 1985390
    iput-object v4, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    .line 1985391
    iget-object v5, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v5, v5, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptAcceptButtonLabel:Ljava/lang/String;

    iput-object v5, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->p:Ljava/lang/String;

    .line 1985392
    iget-object v5, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v5, v5, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->interceptDeclineButtonLabel:Ljava/lang/String;

    iput-object v5, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->q:Ljava/lang/String;

    .line 1985393
    const/4 v5, 0x1

    iput-boolean v5, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->s:Z

    .line 1985394
    const/4 v5, 0x0

    iput-boolean v5, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->t:Z

    .line 1985395
    iput-object v3, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->u:Ljava/lang/String;

    .line 1985396
    new-instance v3, LX/DJN;

    invoke-direct {v3, v1}, LX/DJN;-><init>(LX/DJc;)V

    .line 1985397
    iput-object v3, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->m:Landroid/view/View$OnClickListener;

    .line 1985398
    new-instance v3, LX/DJO;

    invoke-direct {v3, v1}, LX/DJO;-><init>(LX/DJc;)V

    .line 1985399
    iput-object v3, v2, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->n:Landroid/view/View$OnClickListener;

    .line 1985400
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    invoke-virtual {v3}, LX/B5j;->f()LX/Ar6;

    move-result-object v3

    const-string v4, "FOR_SALE_POST_TO_MARKETPLACE_INFO"

    invoke-interface {v3, v2, v4}, LX/Ar6;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;Ljava/lang/String;)V

    .line 1985401
    const/4 v2, 0x1

    goto/16 :goto_2
.end method
