.class public LX/Cya;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field private static volatile c:LX/Cya;


# instance fields
.field private final b:LX/0oz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1953332
    const-class v0, LX/Cya;

    sput-object v0, LX/Cya;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0oz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1953333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1953334
    iput-object p1, p0, LX/Cya;->b:LX/0oz;

    .line 1953335
    return-void
.end method

.method public static a(LX/0QB;)LX/Cya;
    .locals 4

    .prologue
    .line 1953336
    sget-object v0, LX/Cya;->c:LX/Cya;

    if-nez v0, :cond_1

    .line 1953337
    const-class v1, LX/Cya;

    monitor-enter v1

    .line 1953338
    :try_start_0
    sget-object v0, LX/Cya;->c:LX/Cya;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1953339
    if-eqz v2, :cond_0

    .line 1953340
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1953341
    new-instance p0, LX/Cya;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v3

    check-cast v3, LX/0oz;

    invoke-direct {p0, v3}, LX/Cya;-><init>(LX/0oz;)V

    .line 1953342
    move-object v0, p0

    .line 1953343
    sput-object v0, LX/Cya;->c:LX/Cya;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1953344
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1953345
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1953346
    :cond_1
    sget-object v0, LX/Cya;->c:LX/Cya;

    return-object v0

    .line 1953347
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1953348
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
