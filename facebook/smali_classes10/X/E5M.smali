.class public LX/E5M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/E5M;


# instance fields
.field public final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078284
    iput-object p1, p0, LX/E5M;->a:LX/0SG;

    .line 2078285
    return-void
.end method

.method public static a(LX/0QB;)LX/E5M;
    .locals 4

    .prologue
    .line 2078286
    sget-object v0, LX/E5M;->b:LX/E5M;

    if-nez v0, :cond_1

    .line 2078287
    const-class v1, LX/E5M;

    monitor-enter v1

    .line 2078288
    :try_start_0
    sget-object v0, LX/E5M;->b:LX/E5M;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078289
    if-eqz v2, :cond_0

    .line 2078290
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2078291
    new-instance p0, LX/E5M;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/E5M;-><init>(LX/0SG;)V

    .line 2078292
    move-object v0, p0

    .line 2078293
    sput-object v0, LX/E5M;->b:LX/E5M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078294
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078295
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078296
    :cond_1
    sget-object v0, LX/E5M;->b:LX/E5M;

    return-object v0

    .line 2078297
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078298
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
