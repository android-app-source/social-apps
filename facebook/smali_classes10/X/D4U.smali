.class public LX/D4U;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/7ze;
.implements LX/1Pe;
.implements LX/1Pf;
.implements LX/1PW;
.implements LX/7Lk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1Qj;",
        "LX/7ze",
        "<",
        "LX/D5z;",
        ">;",
        "LX/1Pe;",
        "LX/1Pf;",
        "Lcom/facebook/video/channelfeed/CanReusePlayer;",
        "Lcom/facebook/video/channelfeed/HasChannelFeedParams;",
        "Lcom/facebook/video/channelfeed/HasFullscreenPlayer;",
        "Lcom/facebook/video/channelfeed/HasPlayerOrigin;",
        "Lcom/facebook/video/channelfeed/HasSinglePublisherChannelInfo;",
        "Lcom/facebook/video/channelfeed/HasVideoPlayerCallbackListener;",
        "LX/7Lk;"
    }
.end annotation


# instance fields
.field private final n:LX/1PT;

.field private final o:LX/1SX;

.field private final p:LX/7za;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7za",
            "<",
            "LX/D5z;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/04D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/2pa;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/D4m;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/D6I;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Z

.field public v:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/3Qw;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/3It;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/7za;LX/BwE;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1962245
    sget-object v0, LX/1PU;->b:LX/1PY;

    invoke-direct {p0, p1, p3, v0}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 1962246
    iput-object p2, p0, LX/D4U;->n:LX/1PT;

    .line 1962247
    iput-object p4, p0, LX/D4U;->p:LX/7za;

    .line 1962248
    sget-object v0, LX/0wD;->VIDEO_CHANNEL:LX/0wD;

    const-string v1, "video_channel_feed"

    invoke-virtual {p5, p0, v0, v1}, LX/BwE;->a(LX/1Pf;LX/0wD;Ljava/lang/String;)LX/Bur;

    move-result-object v0

    iput-object v0, p0, LX/D4U;->o:LX/1SX;

    .line 1962249
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1962244
    return-void
.end method

.method public final a(Landroid/view/View;LX/2oV;)V
    .locals 1

    .prologue
    .line 1962250
    check-cast p1, LX/D5z;

    .line 1962251
    iget-object v0, p0, LX/D4U;->p:LX/7za;

    invoke-virtual {v0, p1, p2}, LX/7za;->a(Landroid/view/View;LX/2oV;)V

    .line 1962252
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 0

    .prologue
    .line 1962232
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1962242
    return-void
.end method

.method public final bridge synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1962243
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 1962239
    iget-object v0, p0, LX/D4U;->s:LX/D4m;

    if-eqz v0, :cond_0

    .line 1962240
    iget-object v0, p0, LX/D4U;->s:LX/D4m;

    invoke-virtual {v0}, LX/D4m;->a()V

    .line 1962241
    :cond_0
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 1962238
    iget-object v0, p0, LX/D4U;->n:LX/1PT;

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 1962235
    if-eqz p1, :cond_0

    .line 1962236
    invoke-static {p1}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bs()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1962237
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 1962234
    iget-object v0, p0, LX/D4U;->o:LX/1SX;

    return-object v0
.end method

.method public final n()LX/1SX;
    .locals 1

    .prologue
    .line 1962233
    invoke-virtual {p0}, LX/D4U;->e()LX/1SX;

    move-result-object v0

    return-object v0
.end method
