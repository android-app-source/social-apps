.class public final LX/ESZ;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Z)V
    .locals 0

    .prologue
    .line 2123151
    iput-object p1, p0, LX/ESZ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 2123152
    iput-boolean p2, p0, LX/ESZ;->b:Z

    .line 2123153
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2123154
    iget-object v0, p0, LX/ESZ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->i:LX/ES8;

    invoke-virtual {v0}, LX/ES8;->b()Ljava/util/Set;

    .line 2123155
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2123156
    iget-object v0, p0, LX/ESZ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ESZ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    invoke-virtual {v0}, LX/ESY;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2123157
    iget-object v0, p0, LX/ESZ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/ESY;->cancel(Z)Z

    .line 2123158
    :cond_0
    iget-object v0, p0, LX/ESZ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    new-instance v1, LX/ESY;

    iget-object v2, p0, LX/ESZ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-boolean v3, p0, LX/ESZ;->b:Z

    invoke-direct {v1, v2, v3}, LX/ESY;-><init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Z)V

    .line 2123159
    iput-object v1, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    .line 2123160
    iget-object v0, p0, LX/ESZ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v0, v0, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->e:LX/0Sh;

    iget-object v1, p0, LX/ESZ;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    iget-object v1, v1, Lcom/facebook/vault/ui/VaultSyncScreenFragment;->C:LX/ESY;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 2123161
    return-void
.end method
