.class public final LX/E4p;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/E4q;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/text/SpannableStringBuilder;

.field public b:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public final synthetic c:LX/E4q;


# direct methods
.method public constructor <init>(LX/E4q;)V
    .locals 1

    .prologue
    .line 2077338
    iput-object p1, p0, LX/E4p;->c:LX/E4q;

    .line 2077339
    move-object v0, p1

    .line 2077340
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2077341
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2077323
    const-string v0, "ReactionCenteredParagraphUnitComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2077324
    if-ne p0, p1, :cond_1

    .line 2077325
    :cond_0
    :goto_0
    return v0

    .line 2077326
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2077327
    goto :goto_0

    .line 2077328
    :cond_3
    check-cast p1, LX/E4p;

    .line 2077329
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2077330
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2077331
    if-eq v2, v3, :cond_0

    .line 2077332
    iget-object v2, p0, LX/E4p;->a:Landroid/text/SpannableStringBuilder;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/E4p;->a:Landroid/text/SpannableStringBuilder;

    iget-object v3, p1, LX/E4p;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2077333
    goto :goto_0

    .line 2077334
    :cond_5
    iget-object v2, p1, LX/E4p;->a:Landroid/text/SpannableStringBuilder;

    if-nez v2, :cond_4

    .line 2077335
    :cond_6
    iget-object v2, p0, LX/E4p;->b:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/E4p;->b:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iget-object v3, p1, LX/E4p;->b:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2077336
    goto :goto_0

    .line 2077337
    :cond_7
    iget-object v2, p1, LX/E4p;->b:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
