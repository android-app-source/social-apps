.class public LX/DvM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/DvM;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2058290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2058291
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2058292
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2058293
    :goto_0
    return-object v0

    .line 2058294
    :cond_1
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2058295
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2058296
    new-instance v3, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-direct {v3, v0}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2058297
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2058298
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/DvM;
    .locals 3

    .prologue
    .line 2058299
    sget-object v0, LX/DvM;->a:LX/DvM;

    if-nez v0, :cond_1

    .line 2058300
    const-class v1, LX/DvM;

    monitor-enter v1

    .line 2058301
    :try_start_0
    sget-object v0, LX/DvM;->a:LX/DvM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2058302
    if-eqz v2, :cond_0

    .line 2058303
    :try_start_1
    new-instance v0, LX/DvM;

    invoke-direct {v0}, LX/DvM;-><init>()V

    .line 2058304
    move-object v0, v0

    .line 2058305
    sput-object v0, LX/DvM;->a:LX/DvM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2058306
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2058307
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2058308
    :cond_1
    sget-object v0, LX/DvM;->a:LX/DvM;

    return-object v0

    .line 2058309
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2058310
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;)Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 2

    .prologue
    .line 2058311
    new-instance v0, LX/17L;

    invoke-direct {v0}, LX/17L;-><init>()V

    .line 2058312
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2058313
    :cond_0
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    .line 2058314
    :goto_0
    return-object v0

    .line 2058315
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v1

    .line 2058316
    iput-object v1, v0, LX/17L;->f:Ljava/lang/String;

    .line 2058317
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2058318
    iput-object v1, v0, LX/17L;->c:Ljava/lang/String;

    .line 2058319
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v1

    .line 2058320
    iput-boolean v1, v0, LX/17L;->d:Z

    .line 2058321
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v1

    .line 2058322
    iput-boolean v1, v0, LX/17L;->e:Z

    .line 2058323
    invoke-virtual {v0}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2058324
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2058325
    if-eqz p1, :cond_0

    .line 2058326
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058327
    if-nez v0, :cond_1

    .line 2058328
    :cond_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2058329
    :goto_0
    return-object v0

    .line 2058330
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058331
    instance-of v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;

    if-nez v0, :cond_2

    .line 2058332
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058333
    instance-of v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;

    if-nez v0, :cond_2

    .line 2058334
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058335
    instance-of v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;

    if-nez v0, :cond_2

    .line 2058336
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058337
    instance-of v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;

    if-nez v0, :cond_2

    .line 2058338
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2058339
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058340
    instance-of v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;

    if-eqz v0, :cond_5

    .line 2058341
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058342
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;

    .line 2058343
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2058344
    :cond_3
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2058345
    :goto_1
    move-object v1, v1

    .line 2058346
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058347
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;

    .line 2058348
    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    .line 2058349
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    if-nez v3, :cond_e

    .line 2058350
    :cond_4
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 2058351
    :goto_2
    move-object v0, v2

    .line 2058352
    :goto_3
    new-instance v2, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;

    invoke-direct {v2, v0, v1}, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;-><init>(Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0Px;)V

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    .line 2058353
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058354
    instance-of v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;

    if-eqz v0, :cond_6

    .line 2058355
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058356
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;

    invoke-static {v0}, LX/DvM;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;)LX/0Px;

    move-result-object v1

    .line 2058357
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058358
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;

    invoke-static {v0}, LX/DvM;->b(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediasetQueryModel;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    goto :goto_3

    .line 2058359
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058360
    instance-of v0, v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;

    if-eqz v0, :cond_9

    .line 2058361
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058362
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;

    .line 2058363
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2058364
    :cond_7
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2058365
    :goto_4
    move-object v1, v1

    .line 2058366
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058367
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;

    .line 2058368
    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    .line 2058369
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    if-nez v3, :cond_11

    .line 2058370
    :cond_8
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 2058371
    :goto_5
    move-object v0, v2

    .line 2058372
    goto/16 :goto_3

    .line 2058373
    :cond_9
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058374
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;

    .line 2058375
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2058376
    :cond_a
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2058377
    :goto_6
    move-object v1, v1

    .line 2058378
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2058379
    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;

    .line 2058380
    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    .line 2058381
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 2058382
    :cond_b
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 2058383
    :goto_7
    move-object v0, v2

    .line 2058384
    goto/16 :goto_3

    .line 2058385
    :cond_c
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2058386
    const/4 v1, 0x0

    move v2, v1

    :goto_8
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_d

    .line 2058387
    new-instance p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-direct {p0, v1}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;)V

    invoke-virtual {v3, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2058388
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_8

    .line 2058389
    :cond_d
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_1

    .line 2058390
    :cond_e
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v3

    .line 2058391
    iput-object v3, v2, LX/17L;->f:Ljava/lang/String;

    .line 2058392
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2058393
    iput-object v3, v2, LX/17L;->c:Ljava/lang/String;

    .line 2058394
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v3

    .line 2058395
    iput-boolean v3, v2, LX/17L;->d:Z

    .line 2058396
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraTaggedMediasetQueryModel$TaggedMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v3

    .line 2058397
    iput-boolean v3, v2, LX/17L;->e:Z

    .line 2058398
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto/16 :goto_2

    .line 2058399
    :cond_f
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2058400
    const/4 v1, 0x0

    move v2, v1

    :goto_9
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_10

    .line 2058401
    new-instance p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8IG;

    invoke-static {v1}, LX/8I6;->a(LX/8IG;)LX/8IH;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(LX/8IH;)V

    invoke-virtual {v3, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2058402
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_9

    .line 2058403
    :cond_10
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_4

    .line 2058404
    :cond_11
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v3

    .line 2058405
    iput-object v3, v2, LX/17L;->f:Ljava/lang/String;

    .line 2058406
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2058407
    iput-object v3, v2, LX/17L;->c:Ljava/lang/String;

    .line 2058408
    const/4 v3, 0x0

    .line 2058409
    iput-boolean v3, v2, LX/17L;->d:Z

    .line 2058410
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraProfileSuggestedPhotoQueryModel$ProfileWizardRefresherModel$ProfilePictureSuggestionsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v3

    .line 2058411
    iput-boolean v3, v2, LX/17L;->e:Z

    .line 2058412
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto/16 :goto_5

    .line 2058413
    :cond_12
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2058414
    const/4 v1, 0x0

    move v2, v1

    :goto_a
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_13

    .line 2058415
    new-instance p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-direct {p0, v1}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;)V

    invoke-virtual {v3, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2058416
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_a

    .line 2058417
    :cond_13
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_6

    .line 2058418
    :cond_14
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v3

    .line 2058419
    iput-object v3, v2, LX/17L;->f:Ljava/lang/String;

    .line 2058420
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2058421
    iput-object v3, v2, LX/17L;->c:Ljava/lang/String;

    .line 2058422
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v3

    .line 2058423
    iput-boolean v3, v2, LX/17L;->d:Z

    .line 2058424
    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraAlbumQueryModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v3

    .line 2058425
    iput-boolean v3, v2, LX/17L;->e:Z

    .line 2058426
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto/16 :goto_7
.end method
