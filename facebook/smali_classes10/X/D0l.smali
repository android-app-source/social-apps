.class public final LX/D0l;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/D0m;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:I

.field public final synthetic f:LX/D0m;


# direct methods
.method public constructor <init>(LX/D0m;)V
    .locals 1

    .prologue
    .line 1955908
    iput-object p1, p0, LX/D0l;->f:LX/D0m;

    .line 1955909
    move-object v0, p1

    .line 1955910
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1955911
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1955912
    const-string v0, "DTIDescriptionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1955913
    if-ne p0, p1, :cond_1

    .line 1955914
    :cond_0
    :goto_0
    return v0

    .line 1955915
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1955916
    goto :goto_0

    .line 1955917
    :cond_3
    check-cast p1, LX/D0l;

    .line 1955918
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1955919
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1955920
    if-eq v2, v3, :cond_0

    .line 1955921
    iget-object v2, p0, LX/D0l;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/D0l;->a:Ljava/lang/String;

    iget-object v3, p1, LX/D0l;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1955922
    goto :goto_0

    .line 1955923
    :cond_5
    iget-object v2, p1, LX/D0l;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1955924
    :cond_6
    iget-object v2, p0, LX/D0l;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/D0l;->b:Ljava/lang/String;

    iget-object v3, p1, LX/D0l;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1955925
    goto :goto_0

    .line 1955926
    :cond_8
    iget-object v2, p1, LX/D0l;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1955927
    :cond_9
    iget v2, p0, LX/D0l;->c:I

    iget v3, p1, LX/D0l;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1955928
    goto :goto_0

    .line 1955929
    :cond_a
    iget v2, p0, LX/D0l;->d:I

    iget v3, p1, LX/D0l;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1955930
    goto :goto_0

    .line 1955931
    :cond_b
    iget v2, p0, LX/D0l;->e:I

    iget v3, p1, LX/D0l;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1955932
    goto :goto_0
.end method
