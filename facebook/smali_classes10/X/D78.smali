.class public LX/D78;
.super LX/2oy;
.source ""


# instance fields
.field public final a:LX/D76;

.field public b:LX/3H0;

.field public c:Lcom/facebook/widget/text/BetterTextView;

.field public d:Landroid/widget/ProgressBar;

.field public e:Landroid/view/View;

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1966781
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/D78;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1966782
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1966783
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/D78;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966784
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1966785
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1966786
    sget-object v0, LX/3H0;->VOD:LX/3H0;

    iput-object v0, p0, LX/D78;->b:LX/3H0;

    .line 1966787
    iput v2, p0, LX/D78;->f:I

    .line 1966788
    const v0, 0x7f030987

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1966789
    const v0, 0x7f0d0553

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/D78;->e:Landroid/view/View;

    .line 1966790
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/D78;->d:Landroid/widget/ProgressBar;

    .line 1966791
    const v0, 0x7f0d1857

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/D78;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1966792
    new-instance v0, LX/D76;

    invoke-direct {v0, p0}, LX/D76;-><init>(LX/D78;)V

    iput-object v0, p0, LX/D78;->a:LX/D76;

    .line 1966793
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/D77;

    invoke-direct {v1, p0}, LX/D77;-><init>(LX/D78;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1966794
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 1966795
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    int-to-long v0, v0

    .line 1966796
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1966797
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->c:I

    iput v0, p0, LX/D78;->f:I

    .line 1966798
    :cond_0
    iget-object v0, p0, LX/D78;->a:LX/D76;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/D76;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1966799
    iget-object v0, p0, LX/D78;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1966800
    iget-object v0, p0, LX/D78;->d:Landroid/widget/ProgressBar;

    iget v1, p0, LX/D78;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1966801
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1966802
    iget-object v0, p0, LX/D78;->a:LX/D76;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/D76;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1966803
    const/4 v0, 0x0

    iput v0, p0, LX/D78;->f:I

    .line 1966804
    return-void
.end method
