.class public LX/Eek;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EeO;


# instance fields
.field private final a:LX/1sZ;

.field private final b:Landroid/app/DownloadManager;

.field private final c:LX/Eel;

.field private final d:LX/Een;

.field private final e:LX/1wh;

.field public f:J


# direct methods
.method public constructor <init>(LX/1sZ;Landroid/app/DownloadManager;LX/Eel;LX/Een;LX/1wh;)V
    .locals 0

    .prologue
    .line 2153263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2153264
    iput-object p1, p0, LX/Eek;->a:LX/1sZ;

    .line 2153265
    iput-object p2, p0, LX/Eek;->b:Landroid/app/DownloadManager;

    .line 2153266
    iput-object p3, p0, LX/Eek;->c:LX/Eel;

    .line 2153267
    iput-object p4, p0, LX/Eek;->d:LX/Een;

    .line 2153268
    iput-object p5, p0, LX/Eek;->e:LX/1wh;

    .line 2153269
    return-void
.end method

.method private a()J
    .locals 2

    .prologue
    .line 2153270
    iget-wide v0, p0, LX/Eek;->f:J

    return-wide v0
.end method


# virtual methods
.method public final a(LX/EeX;)LX/EeY;
    .locals 14

    .prologue
    .line 2153271
    iget-object v0, p1, LX/EeX;->operationState$:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2153272
    new-instance v0, LX/EeY;

    invoke-direct {v0}, LX/EeY;-><init>()V

    .line 2153273
    :goto_0
    return-object v0

    .line 2153274
    :cond_0
    new-instance v0, Landroid/app/DownloadManager$Query;

    invoke-direct {v0}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 2153275
    const/4 v1, 0x1

    new-array v1, v1, [J

    const/4 v2, 0x0

    iget-wide v4, p1, LX/EeX;->downloadId:J

    aput-wide v4, v1, v2

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 2153276
    iget-object v1, p0, LX/Eek;->b:Landroid/app/DownloadManager;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    .line 2153277
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v2, "Download cursor is null!"

    invoke-static {v0, v2}, LX/1wm;->a(ZLjava/lang/String;)V

    .line 2153278
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 2153279
    const-string v2, "Download not available for checking completion"

    invoke-static {v0, v2}, LX/1wm;->a(ZLjava/lang/String;)V

    .line 2153280
    const-string v0, "bytes_so_far"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2153281
    const-string v0, "total_size"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 2153282
    const-string v0, "status"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 2153283
    const-string v6, "reason"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 2153284
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2153285
    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 2153286
    iget-object v1, p0, LX/Eek;->a:LX/1sZ;

    iget-wide v8, p1, LX/EeX;->downloadId:J

    .line 2153287
    invoke-static {v1}, LX/1sZ;->a(LX/1sZ;)Ljava/io/File;

    move-result-object v7

    .line 2153288
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "temp_"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".apk"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v7, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2153289
    move-object v1, v10

    .line 2153290
    iget-object v7, p0, LX/Eek;->b:Landroid/app/DownloadManager;

    iget-wide v8, p1, LX/EeX;->downloadId:J

    invoke-virtual {v7, v8, v9}, Landroid/app/DownloadManager;->openDownloadedFile(J)Landroid/os/ParcelFileDescriptor;

    move-result-object v7

    invoke-static {v7, v1}, LX/Eem;->a(Landroid/os/ParcelFileDescriptor;Ljava/io/File;)V

    .line 2153291
    invoke-static {}, LX/Eem;->b()J

    move-result-wide v8

    .line 2153292
    iget-object v7, p0, LX/Eek;->e:LX/1wh;

    const-string v10, "appupdate_download_successful"

    invoke-direct {p0}, LX/Eek;->a()J

    move-result-wide v12

    invoke-virtual {p1, v12, v13, v8, v9}, LX/EeX;->a(JJ)Lorg/json/JSONObject;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, LX/1wh;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2153293
    iget-object v7, p0, LX/Eek;->e:LX/1wh;

    const-string v8, "appupdate_download_successful"

    iget-object v9, p1, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    invoke-virtual {p1}, LX/EeX;->d()LX/Eeh;

    move-result-object v10

    const-string v11, "task_success"

    invoke-virtual {v7, v8, v9, v10, v11}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V

    .line 2153294
    invoke-virtual {p1}, LX/EeX;->b()Z

    move-result v7

    if-nez v7, :cond_2

    .line 2153295
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, Ljava/io/File;->setReadable(ZZ)Z

    .line 2153296
    new-instance v7, LX/EeW;

    invoke-direct {v7, p1}, LX/EeW;-><init>(LX/EeX;)V

    const/4 v8, 0x4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 2153297
    iput-object v8, v7, LX/EeW;->e:Ljava/lang/Integer;

    .line 2153298
    move-object v7, v7

    .line 2153299
    invoke-virtual {v7, v2, v3}, LX/EeW;->b(J)LX/EeW;

    move-result-object v2

    .line 2153300
    iput v0, v2, LX/EeW;->l:I

    .line 2153301
    move-object v0, v2

    .line 2153302
    iput v6, v0, LX/EeW;->m:I

    .line 2153303
    move-object v0, v0

    .line 2153304
    invoke-virtual {v0, v4, v5}, LX/EeW;->c(J)LX/EeW;

    move-result-object v0

    .line 2153305
    iput-object v1, v0, LX/EeW;->i:Ljava/io/File;

    .line 2153306
    move-object v0, v0

    .line 2153307
    invoke-virtual {v0}, LX/EeW;->a()LX/EeX;

    move-result-object v1

    .line 2153308
    new-instance v0, LX/EeY;

    iget-object v2, p0, LX/Eek;->d:LX/Een;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, LX/EeY;-><init>(LX/EeX;LX/EeO;J)V

    goto/16 :goto_0

    .line 2153309
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2153310
    :cond_2
    new-instance v7, LX/EeW;

    invoke-direct {v7, p1}, LX/EeW;-><init>(LX/EeX;)V

    const/4 v8, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 2153311
    iput-object v8, v7, LX/EeW;->e:Ljava/lang/Integer;

    .line 2153312
    move-object v7, v7

    .line 2153313
    invoke-virtual {v7, v2, v3}, LX/EeW;->b(J)LX/EeW;

    move-result-object v2

    .line 2153314
    iput v0, v2, LX/EeW;->l:I

    .line 2153315
    move-object v0, v2

    .line 2153316
    iput v6, v0, LX/EeW;->m:I

    .line 2153317
    move-object v0, v0

    .line 2153318
    invoke-virtual {v0, v4, v5}, LX/EeW;->c(J)LX/EeW;

    move-result-object v0

    .line 2153319
    iput-object v1, v0, LX/EeW;->j:Ljava/io/File;

    .line 2153320
    move-object v0, v0

    .line 2153321
    invoke-virtual {v0}, LX/EeW;->a()LX/EeX;

    move-result-object v1

    .line 2153322
    new-instance v0, LX/EeY;

    iget-object v2, p0, LX/Eek;->c:LX/Eel;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, LX/EeY;-><init>(LX/EeX;LX/EeO;J)V

    goto/16 :goto_0

    .line 2153323
    :cond_3
    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 2153324
    sparse-switch v6, :sswitch_data_0

    .line 2153325
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UNKNOWN("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v0, v0

    .line 2153326
    new-instance v1, LX/Ef0;

    invoke-direct {v1, v0}, LX/Ef0;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2153327
    :cond_4
    new-instance v0, LX/EeY;

    invoke-direct {v0}, LX/EeY;-><init>()V

    goto/16 :goto_0

    .line 2153328
    :sswitch_0
    const-string v0, "ERROR_CANNOT_RESUME"

    goto :goto_2

    .line 2153329
    :sswitch_1
    const-string v0, "ERROR_DEVICE_NOT_FOUND"

    goto :goto_2

    .line 2153330
    :sswitch_2
    const-string v0, "ERROR_FILE_ALREADY_EXISTS"

    goto :goto_2

    .line 2153331
    :sswitch_3
    const-string v0, "ERROR_FILE_ERROR"

    goto :goto_2

    .line 2153332
    :sswitch_4
    const-string v0, "ERROR_HTTP_DATA_ERROR"

    goto :goto_2

    .line 2153333
    :sswitch_5
    const-string v0, "ERROR_INSUFFICIENT_SPACE"

    goto :goto_2

    .line 2153334
    :sswitch_6
    const-string v0, "ERROR_TOO_MANY_REDIRECTS"

    goto :goto_2

    .line 2153335
    :sswitch_7
    const-string v0, "ERROR_UNHANDLED_HTTP_CODE"

    goto :goto_2

    .line 2153336
    :sswitch_8
    const-string v0, "ERROR_UNKNOWN"

    goto :goto_2

    .line 2153337
    :sswitch_9
    const-string v0, "PAUSED_QUEUED_FOR_WIFI"

    goto :goto_2

    .line 2153338
    :sswitch_a
    const-string v0, "PAUSED_UNKNOWN"

    goto :goto_2

    .line 2153339
    :sswitch_b
    const-string v0, "PAUSED_WAITING_FOR_NETWORK"

    goto :goto_2

    .line 2153340
    :sswitch_c
    const-string v0, "PAUSED_WAITING_TO_RETRY"

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_c
        0x2 -> :sswitch_b
        0x3 -> :sswitch_9
        0x4 -> :sswitch_a
        0x3e8 -> :sswitch_8
        0x3e9 -> :sswitch_3
        0x3ea -> :sswitch_7
        0x3ec -> :sswitch_4
        0x3ed -> :sswitch_6
        0x3ee -> :sswitch_5
        0x3ef -> :sswitch_1
        0x3f0 -> :sswitch_0
        0x3f1 -> :sswitch_2
    .end sparse-switch
.end method
