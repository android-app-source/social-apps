.class public final LX/DoV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/crypto/keychain/KeyChain;


# instance fields
.field public final synthetic a:LX/2Oy;

.field private final b:[B


# direct methods
.method public constructor <init>(LX/2Oy;[B)V
    .locals 0

    .prologue
    .line 2041842
    iput-object p1, p0, LX/DoV;->a:LX/2Oy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2041843
    iput-object p2, p0, LX/DoV;->b:[B

    .line 2041844
    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 1

    .prologue
    .line 2041845
    iget-object v0, p0, LX/DoV;->b:[B

    return-object v0
.end method

.method public final b()[B
    .locals 2

    .prologue
    .line 2041846
    sget-object v0, LX/2Oy;->a:LX/1Hy;

    iget v0, v0, LX/1Hy;->ivLength:I

    new-array v0, v0, [B

    .line 2041847
    iget-object v1, p0, LX/DoV;->a:LX/2Oy;

    iget-object v1, v1, LX/2Oy;->c:LX/1Hn;

    iget-object v1, v1, LX/1Ho;->b:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 2041848
    return-object v0
.end method
