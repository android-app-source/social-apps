.class public LX/DGu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/DGu",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980937
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1980938
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/DGu;->b:LX/0Zi;

    .line 1980939
    iput-object p1, p0, LX/DGu;->a:LX/0Ot;

    .line 1980940
    return-void
.end method

.method public static a(LX/0QB;)LX/DGu;
    .locals 4

    .prologue
    .line 1980941
    const-class v1, LX/DGu;

    monitor-enter v1

    .line 1980942
    :try_start_0
    sget-object v0, LX/DGu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980943
    sput-object v2, LX/DGu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980944
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980945
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980946
    new-instance v3, LX/DGu;

    const/16 p0, 0x21c0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/DGu;-><init>(LX/0Ot;)V

    .line 1980947
    move-object v0, v3

    .line 1980948
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980949
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/DGu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980950
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980951
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1X1;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1X1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1980952
    check-cast p3, LX/DGt;

    .line 1980953
    iget-object v0, p0, LX/DGu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    iget-object v1, p3, LX/DGt;->a:LX/1Pv;

    .line 1980954
    const/4 v3, 0x0

    .line 1980955
    if-eqz p2, :cond_0

    .line 1980956
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1980957
    if-nez v2, :cond_3

    :cond_0
    move-object v2, v3

    .line 1980958
    :cond_1
    :goto_0
    move-object v6, v2

    .line 1980959
    if-nez v6, :cond_2

    .line 1980960
    :goto_1
    return-void

    .line 1980961
    :cond_2
    iget-object v2, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->h:LX/2yJ;

    iget-object v5, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->j:LX/2yN;

    invoke-static {p2, v1}, LX/C8d;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pv;)LX/2yV;

    move-result-object v7

    invoke-static {p2, v1}, LX/C8d;->b(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pv;)LX/2yi;

    move-result-object v8

    invoke-static {p2}, LX/C8d;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v9

    move-object v10, v1

    check-cast v10, LX/1Pq;

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v2 .. v10}, LX/2yJ;->onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yN;Ljava/lang/String;LX/2yV;LX/2yi;ZLX/1Pq;)V

    goto :goto_1

    .line 1980962
    :cond_3
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1980963
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v2

    .line 1980964
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v2, v3

    goto :goto_0
.end method

.method public static onClick(LX/1X1;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1980965
    const v0, -0x6878decd

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1980966
    check-cast p2, LX/DGt;

    .line 1980967
    iget-object v0, p0, LX/DGu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    iget-object v1, p2, LX/DGt;->a:LX/1Pv;

    iget-object v2, p2, LX/DGt;->b:LX/DGm;

    .line 1980968
    iget-object v3, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->e:LX/1DR;

    iget-object v4, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->c:Landroid/content/Context;

    invoke-virtual {v3, v4}, LX/1DR;->a(Landroid/content/Context;)I

    move-result v4

    .line 1980969
    iget-object v3, v2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v5, v3

    .line 1980970
    invoke-static {v5}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 1980971
    iget-object v3, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1980972
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1980973
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v4}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x6

    const/4 v9, 0x2

    invoke-interface {v7, v8, v9}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    .line 1980974
    if-eqz v6, :cond_1

    .line 1980975
    iget-object v9, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v9, v9

    .line 1980976
    if-eqz v9, :cond_1

    .line 1980977
    iget-object v9, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v9, v9

    .line 1980978
    check-cast v9, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 1980979
    iget-object v9, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v9, v9

    .line 1980980
    check-cast v9, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    if-eqz v9, :cond_1

    const/4 v9, 0x1

    .line 1980981
    :goto_0
    iget-object v2, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->d:LX/1nu;

    invoke-virtual {v2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v2

    if-eqz v9, :cond_2

    .line 1980982
    iget-object v9, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v9, v9

    .line 1980983
    check-cast v9, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    invoke-static {v9}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v9

    :goto_1
    invoke-virtual {v2, v9}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v9

    sget-object v2, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v9, v2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v9

    const v2, 0x7f0a045d

    invoke-virtual {v9, v2}, LX/1nw;->h(I)LX/1nw;

    move-result-object v9

    const v2, 0x3ff745d1

    invoke-virtual {v9, v2}, LX/1nw;->c(F)LX/1nw;

    move-result-object v9

    sget-object v2, LX/1Up;->g:LX/1Up;

    invoke-virtual {v9, v2}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    add-int/lit8 v2, v4, -0x4

    invoke-interface {v9, v2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v9

    move-object v4, v9

    .line 1980984
    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 v2, 0x1

    .line 1980985
    iget-object v8, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->g:LX/DGp;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LINK_ONLY_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v8, v6, v9}, LX/DGp;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)LX/DGo;

    move-result-object v8

    .line 1980986
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const/16 p0, 0x8

    const p2, 0x7f0b0f1d

    invoke-interface {v9, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v9

    const/4 p0, 0x0

    invoke-interface {v9, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v2}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v9

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    iget-object p2, v8, LX/DGo;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0050

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0a0161

    invoke-virtual {p0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1ne;->t(I)LX/1ne;

    move-result-object p0

    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object p0

    invoke-interface {v9, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    iget-object v8, v8, LX/DGo;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const p0, 0x7f0b004e

    invoke-virtual {v8, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const p0, 0x7f0a0162

    invoke-virtual {v8, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/4 p0, 0x2

    invoke-interface {v8, v2, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    invoke-interface {v9, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    move-object v8, v8

    .line 1980987
    invoke-interface {v4, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 1980988
    const v8, -0x6878decd

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v6, v9, p0

    invoke-static {p1, v8, v9}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v8

    move-object v6, v8

    .line 1980989
    invoke-interface {v4, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v7, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 1980990
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v6

    const v7, 0x7f0a011a

    invoke-virtual {v6, v7}, LX/25Q;->i(I)LX/25Q;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v6, v7}, LX/1Di;->r(I)LX/1Di;

    move-result-object v6

    move-object v6, v6

    .line 1980991
    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 1980992
    iget-object v6, v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->f:LX/DH0;

    const/4 v7, 0x0

    .line 1980993
    new-instance v8, LX/DGz;

    invoke-direct {v8, v6}, LX/DGz;-><init>(LX/DH0;)V

    .line 1980994
    iget-object v9, v6, LX/DH0;->b:LX/0Zi;

    invoke-virtual {v9}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/DGy;

    .line 1980995
    if-nez v9, :cond_0

    .line 1980996
    new-instance v9, LX/DGy;

    invoke-direct {v9, v6}, LX/DGy;-><init>(LX/DH0;)V

    .line 1980997
    :cond_0
    invoke-static {v9, p1, v7, v7, v8}, LX/DGy;->a$redex0(LX/DGy;LX/1De;IILX/DGz;)V

    .line 1980998
    move-object v8, v9

    .line 1980999
    move-object v7, v8

    .line 1981000
    move-object v6, v7

    .line 1981001
    iget-object v7, v6, LX/DGy;->a:LX/DGz;

    iput-object v5, v7, LX/DGz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981002
    iget-object v7, v6, LX/DGy;->e:Ljava/util/BitSet;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 1981003
    move-object v6, v6

    .line 1981004
    sget-object v7, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    .line 1981005
    iget-object v8, v6, LX/DGy;->a:LX/DGz;

    iput-object v7, v8, LX/DGz;->b:LX/1Wk;

    .line 1981006
    iget-object v8, v6, LX/DGy;->e:Ljava/util/BitSet;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 1981007
    move-object v6, v6

    .line 1981008
    new-instance v7, LX/DGv;

    invoke-direct {v7, v0, v3, v1, v5}, LX/DGv;-><init>(Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pv;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v7, v7

    .line 1981009
    iget-object v8, v6, LX/DGy;->a:LX/DGz;

    iput-object v7, v8, LX/DGz;->d:LX/20Z;

    .line 1981010
    iget-object v8, v6, LX/DGy;->e:Ljava/util/BitSet;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 1981011
    move-object v6, v6

    .line 1981012
    check-cast v1, LX/1Pr;

    .line 1981013
    iget-object v7, v6, LX/DGy;->a:LX/DGz;

    iput-object v1, v7, LX/DGz;->c:LX/1Pr;

    .line 1981014
    iget-object v7, v6, LX/DGy;->e:Ljava/util/BitSet;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 1981015
    move-object v6, v6

    .line 1981016
    move-object v3, v6

    .line 1981017
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f020aef

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1981018
    return-object v0

    .line 1981019
    :cond_1
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 1981020
    :cond_2
    const/4 v9, 0x0

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1981021
    invoke-static {}, LX/1dS;->b()V

    .line 1981022
    iget v0, p1, LX/1dQ;->b:I

    .line 1981023
    packed-switch v0, :pswitch_data_0

    .line 1981024
    :goto_0
    return-object v3

    .line 1981025
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1981026
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v1, v0, v2}, LX/DGu;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1X1;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x6878decd
        :pswitch_0
    .end packed-switch
.end method
