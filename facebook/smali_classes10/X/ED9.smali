.class public final enum LX/ED9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ED9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ED9;

.field public static final enum CANCELLED:LX/ED9;

.field public static final enum INCOMING:LX/ED9;

.field public static final enum MISSED:LX/ED9;

.field public static final enum OUTGOING:LX/ED9;


# instance fields
.field private final mCallType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2090990
    new-instance v0, LX/ED9;

    const-string v1, "INCOMING"

    invoke-direct {v0, v1, v5, v2}, LX/ED9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/ED9;->INCOMING:LX/ED9;

    .line 2090991
    new-instance v0, LX/ED9;

    const-string v1, "OUTGOING"

    invoke-direct {v0, v1, v2, v3}, LX/ED9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/ED9;->OUTGOING:LX/ED9;

    .line 2090992
    new-instance v0, LX/ED9;

    const-string v1, "MISSED"

    invoke-direct {v0, v1, v3, v4}, LX/ED9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/ED9;->MISSED:LX/ED9;

    .line 2090993
    new-instance v0, LX/ED9;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v4, v6}, LX/ED9;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/ED9;->CANCELLED:LX/ED9;

    .line 2090994
    new-array v0, v6, [LX/ED9;

    sget-object v1, LX/ED9;->INCOMING:LX/ED9;

    aput-object v1, v0, v5

    sget-object v1, LX/ED9;->OUTGOING:LX/ED9;

    aput-object v1, v0, v2

    sget-object v1, LX/ED9;->MISSED:LX/ED9;

    aput-object v1, v0, v3

    sget-object v1, LX/ED9;->CANCELLED:LX/ED9;

    aput-object v1, v0, v4

    sput-object v0, LX/ED9;->$VALUES:[LX/ED9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2090995
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2090996
    iput p3, p0, LX/ED9;->mCallType:I

    .line 2090997
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ED9;
    .locals 1

    .prologue
    .line 2090998
    const-class v0, LX/ED9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ED9;

    return-object v0
.end method

.method public static values()[LX/ED9;
    .locals 1

    .prologue
    .line 2090999
    sget-object v0, LX/ED9;->$VALUES:[LX/ED9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ED9;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 2091000
    iget v0, p0, LX/ED9;->mCallType:I

    return v0
.end method
