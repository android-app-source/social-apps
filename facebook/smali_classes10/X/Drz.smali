.class public final LX/Drz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Ds0;


# direct methods
.method public constructor <init>(LX/Ds0;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2049831
    iput-object p1, p0, LX/Drz;->c:LX/Ds0;

    iput-object p2, p0, LX/Drz;->a:Landroid/content/Intent;

    iput-object p3, p0, LX/Drz;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2049832
    iget-object v0, p0, LX/Drz;->c:LX/Ds0;

    iget-object v0, v0, LX/Ds0;->g:LX/2c4;

    iget-object v1, p0, LX/Drz;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2c4;->a(Ljava/lang/String;I)V

    .line 2049833
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2049834
    iget-object v0, p0, LX/Drz;->c:LX/Ds0;

    iget-object v0, v0, LX/Ds0;->e:LX/Drl;

    iget-object v1, p0, LX/Drz;->a:Landroid/content/Intent;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->ACTION_FEEDBACK_PRIMARY_TEXT:Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLMobilePushNotifActionKey;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Drz;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/Drl;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2049835
    return-void
.end method
