.class public LX/D0j;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/2g9;


# direct methods
.method public constructor <init>(LX/2g9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1955857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1955858
    iput-object p1, p0, LX/D0j;->a:LX/2g9;

    .line 1955859
    return-void
.end method

.method public static a(LX/0QB;)LX/D0j;
    .locals 4

    .prologue
    .line 1955860
    const-class v1, LX/D0j;

    monitor-enter v1

    .line 1955861
    :try_start_0
    sget-object v0, LX/D0j;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1955862
    sput-object v2, LX/D0j;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1955863
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1955864
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1955865
    new-instance p0, LX/D0j;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v3

    check-cast v3, LX/2g9;

    invoke-direct {p0, v3}, LX/D0j;-><init>(LX/2g9;)V

    .line 1955866
    move-object v0, p0

    .line 1955867
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1955868
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/D0j;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1955869
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1955870
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
