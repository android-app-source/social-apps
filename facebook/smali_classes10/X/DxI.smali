.class public final LX/DxI;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/DxJ;


# direct methods
.method public constructor <init>(LX/DxJ;)V
    .locals 0

    .prologue
    .line 2062667
    iput-object p1, p0, LX/DxI;->a:LX/DxJ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2062668
    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/Dvb;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchMediaSet"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2062669
    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    const/4 v1, 0x0

    .line 2062670
    iput-boolean v1, v0, LX/DxJ;->m:Z

    .line 2062671
    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    const v1, 0x3cc59a70

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2062672
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2062673
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/16 v5, 0xc

    .line 2062674
    if-eqz p1, :cond_0

    .line 2062675
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2062676
    if-nez v0, :cond_1

    .line 2062677
    :cond_0
    :goto_0
    return-void

    .line 2062678
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;

    .line 2062679
    iget-object v1, p0, LX/DxI;->a:LX/DxJ;

    iget-boolean v1, v1, LX/Dvb;->o:Z

    if-eqz v1, :cond_2

    .line 2062680
    iget-object v1, p0, LX/DxI;->a:LX/DxJ;

    iget-object v1, v1, LX/Dvb;->q:LX/DwK;

    const-string v2, "LoadImageURLs"

    const-string v3, "ExtraLoadImageURLsSource"

    iget-object v4, p0, LX/DxI;->a:LX/DxJ;

    iget-object v4, v4, LX/Dvb;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, LX/DwK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2062681
    :cond_2
    if-eqz v0, :cond_0

    .line 2062682
    iget-object v1, p0, LX/DxI;->a:LX/DxJ;

    iget-object v1, v1, LX/Dvb;->i:LX/Dvc;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/Dvc;->a(LX/0Px;)V

    .line 2062683
    iget-object v1, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    .line 2062684
    iget-boolean v2, v0, LX/Dv0;->c:Z

    move v0, v2

    .line 2062685
    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 2062686
    :goto_1
    iput-boolean v0, v1, LX/DxJ;->m:Z

    .line 2062687
    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    .line 2062688
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2062689
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    .line 2062690
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2062691
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt v0, v5, :cond_3

    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/Dvb;->h:LX/Dv0;

    .line 2062692
    iget-object v1, v0, LX/Dv0;->b:LX/0Px;

    move-object v0, v1

    .line 2062693
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt v0, v5, :cond_3

    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-boolean v0, v0, LX/Dvb;->o:Z

    if-eqz v0, :cond_3

    .line 2062694
    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/Dvb;->q:LX/DwK;

    const-string v1, "LoadScreenImages"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2062695
    :cond_3
    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/DxJ;->y:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2062696
    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    iget-object v0, v0, LX/DxJ;->y:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dce;

    invoke-interface {v0}, LX/Dce;->b()V

    .line 2062697
    :cond_4
    iget-object v0, p0, LX/DxI;->a:LX/DxJ;

    const v1, -0x17b59d07

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_0

    .line 2062698
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method
