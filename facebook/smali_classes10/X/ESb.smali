.class public final LX/ESb;
.super Landroid/animation/IntEvaluator;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/vault/ui/VaultSyncScreenFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2123191
    iput-object p1, p0, LX/ESb;->a:Lcom/facebook/vault/ui/VaultSyncScreenFragment;

    invoke-direct {p0}, Landroid/animation/IntEvaluator;-><init>()V

    .line 2123192
    iput-object p2, p0, LX/ESb;->b:Landroid/view/View;

    .line 2123193
    return-void
.end method


# virtual methods
.method public final evaluate(FLjava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2123194
    invoke-super {p0, p1, p2, p3}, Landroid/animation/IntEvaluator;->evaluate(FLjava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2123195
    iget-object v1, p0, LX/ESb;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 2123196
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    new-instance v3, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v4, -0x1

    invoke-direct {v3, v4, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 2123197
    invoke-virtual {v2, v5, v0, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2123198
    iget-object v1, p0, LX/ESb;->b:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2123199
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2123200
    check-cast p2, Ljava/lang/Integer;

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, p3}, LX/ESb;->evaluate(FLjava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
