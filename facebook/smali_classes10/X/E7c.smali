.class public LX/E7c;
.super Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler",
        "<",
        "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesInterfaces$MediaWithAttributionFetchFromReactionStory;",
        "LX/5kF;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/E7f;

.field private final d:Lcom/facebook/reaction/ReactionUtil;


# direct methods
.method public constructor <init>(LX/E7f;LX/0Or;LX/E1i;LX/3Tx;LX/3Tz;Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V
    .locals 6
    .param p7    # Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/E7f;",
            "LX/0Or",
            "<",
            "LX/25T;",
            ">;",
            "LX/E1i;",
            "LX/3Tx;",
            "LX/3Tz;",
            "Lcom/facebook/reaction/ReactionUtil;",
            "Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2081874
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;-><init>(LX/0Or;LX/E1i;LX/3Tx;LX/3Tz;Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;)V

    .line 2081875
    iput-object p1, p0, LX/E7c;->c:LX/E7f;

    .line 2081876
    iput-object p6, p0, LX/E7c;->d:Lcom/facebook/reaction/ReactionUtil;

    .line 2081877
    return-void
.end method

.method private static a(LX/5kF;)Z
    .locals 4
    .param p0    # LX/5kF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2081836
    if-eqz p0, :cond_1

    invoke-interface {p0}, LX/5kE;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p0}, LX/5kE;->j()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, LX/5kE;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, LX/5kE;->l()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, LX/5kE;->l()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PageMediaWithAttributionModel$OwnerModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2081837
    invoke-interface {p0}, LX/5kF;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2081838
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2081839
    invoke-interface {p0}, LX/5kF;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2081840
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2081841
    invoke-interface {p0}, LX/5kF;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2081842
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/util/List;
    .locals 8
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesInterfaces$MediaWithAttributionFetchFromReactionStory;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/5kF;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2081857
    if-eqz p1, :cond_0

    .line 2081858
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2081859
    if-nez v0, :cond_1

    .line 2081860
    :cond_0
    :goto_0
    return-object v1

    .line 2081861
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2081862
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2081863
    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel;

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel;->a()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel;

    move-result-object v4

    .line 2081864
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2081865
    invoke-virtual {v4}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_3

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;

    .line 2081866
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v7

    invoke-static {v7}, LX/E7c;->a(LX/5kF;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2081867
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel$NodesModel;->a()LX/5kF;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2081868
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2081869
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel;->b()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaWithAttributionFetchFromReactionStoryModel$ReactionAttachmentsModel;->b()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2081870
    :goto_2
    iput-object v0, p0, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->h:Ljava/lang/String;

    .line 2081871
    move-object v1, v2

    .line 2081872
    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 2081873
    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V
    .locals 6

    .prologue
    .line 2081849
    iget-object v0, p0, LX/E7c;->d:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;->g()LX/0Vd;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    .line 2081850
    iget-object p0, v0, Lcom/facebook/reaction/ReactionUtil;->q:LX/1vm;

    new-instance p1, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {p1, v2}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    .line 2081851
    new-instance p3, LX/9h2;

    invoke-static {p0}, LX/9h9;->a(LX/0QB;)LX/9h9;

    move-result-object p2

    check-cast p2, LX/9h9;

    invoke-direct {p3, p1, v4, p2}, LX/9h2;-><init>(Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;Lcom/facebook/common/callercontext/CallerContext;LX/9h9;)V

    .line 2081852
    move-object p0, p3

    .line 2081853
    invoke-virtual {p0, v3, v1}, LX/9gr;->b(ILjava/lang/String;)LX/0zO;

    move-result-object p0

    sget-object p1, LX/0zS;->d:LX/0zS;

    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p0

    .line 2081854
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {p1, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    .line 2081855
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    invoke-virtual {p1, v2, p0, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2081856
    return-void
.end method

.method public final bridge synthetic a(LX/1U8;)Z
    .locals 1
    .param p1    # LX/1U8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2081848
    check-cast p1, LX/5kF;

    invoke-static {p1}, LX/E7c;->a(LX/5kF;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;)LX/1U8;
    .locals 1

    .prologue
    .line 2081847
    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->P()LX/5kF;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;)LX/E7X;
    .locals 8

    .prologue
    .line 2081843
    iget-object v0, p0, LX/E7c;->c:LX/E7f;

    .line 2081844
    new-instance v1, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v2

    check-cast v2, LX/0W9;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v3

    check-cast v3, LX/11S;

    move-object v4, p0

    move-object v5, p3

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosWithAttributionRecyclerAdapter;-><init>(LX/0W9;LX/11S;Lcom/facebook/reaction/ui/attachment/handler/photos/ReactionPhotosHandler;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 2081845
    move-object v0, v1

    .line 2081846
    return-object v0
.end method
