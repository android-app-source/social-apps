.class public abstract LX/DNA;
.super LX/1La;
.source ""

# interfaces
.implements LX/DN9;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field public static final f:Ljava/lang/Class;


# instance fields
.field public A:Z

.field public B:Z

.field private C:Z

.field private D:Z

.field public final a:LX/0kb;

.field public final b:LX/0ad;

.field public c:Landroid/view/View;

.field public d:LX/0g8;

.field public e:LX/DNB;

.field public final g:LX/DCI;

.field public final h:LX/DCL;

.field public final i:LX/DCE;

.field public final j:LX/DCC;

.field public final k:LX/DCO;

.field public final l:LX/DN8;

.field public final m:LX/DCQ;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/1Db;

.field public final p:LX/1CY;

.field public final q:LX/1Kt;

.field public final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2cm;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/1LV;

.field public final t:LX/1B1;

.field public final u:LX/0bH;

.field private final v:LX/88l;

.field private final w:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/0Yb;

.field public y:LX/DNR;

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1990960
    const-class v0, LX/DNA;

    sput-object v0, LX/DNA;->f:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/1B1;LX/0kb;LX/DCC;LX/DCE;LX/0bH;LX/DCI;LX/1LV;LX/DCL;LX/DCO;LX/DCQ;LX/1CY;LX/0Ot;LX/1Db;LX/0Ot;LX/0ad;LX/1Kt;LX/88l;)V
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/groups/feed/controller/annotations/IsGroupsFeedVPVLoggingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1B1;",
            "LX/0kb;",
            "LX/DCC;",
            "LX/DCE;",
            "LX/0bH;",
            "LX/DCI;",
            "LX/1LV;",
            "LX/DCL;",
            "LX/DCO;",
            "LX/DCQ;",
            "LX/1CY;",
            "LX/0Ot",
            "<",
            "LX/5Mk;",
            ">;",
            "LX/1Db;",
            "LX/0Ot",
            "<",
            "LX/2cm;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            "LX/88l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1990938
    invoke-direct {p0}, LX/1La;-><init>()V

    .line 1990939
    iput-object p4, p0, LX/DNA;->j:LX/DCC;

    .line 1990940
    move-object/from16 v0, p13

    iput-object v0, p0, LX/DNA;->n:LX/0Ot;

    .line 1990941
    iput-object p5, p0, LX/DNA;->i:LX/DCE;

    .line 1990942
    iput-object p2, p0, LX/DNA;->t:LX/1B1;

    .line 1990943
    iput-object p6, p0, LX/DNA;->u:LX/0bH;

    .line 1990944
    iput-object p8, p0, LX/DNA;->s:LX/1LV;

    .line 1990945
    iput-object p11, p0, LX/DNA;->m:LX/DCQ;

    .line 1990946
    iput-object p12, p0, LX/DNA;->p:LX/1CY;

    .line 1990947
    iput-object p7, p0, LX/DNA;->g:LX/DCI;

    .line 1990948
    iput-object p3, p0, LX/DNA;->a:LX/0kb;

    .line 1990949
    move-object/from16 v0, p14

    iput-object v0, p0, LX/DNA;->o:LX/1Db;

    .line 1990950
    iput-object p9, p0, LX/DNA;->h:LX/DCL;

    .line 1990951
    move-object/from16 v0, p15

    iput-object v0, p0, LX/DNA;->r:LX/0Ot;

    .line 1990952
    move-object/from16 v0, p17

    iput-object v0, p0, LX/DNA;->q:LX/1Kt;

    .line 1990953
    iput-object p10, p0, LX/DNA;->k:LX/DCO;

    .line 1990954
    iput-object p1, p0, LX/DNA;->w:LX/0Or;

    .line 1990955
    new-instance v1, LX/DN8;

    invoke-direct {v1, p0}, LX/DN8;-><init>(LX/DNA;)V

    iput-object v1, p0, LX/DNA;->l:LX/DN8;

    .line 1990956
    move-object/from16 v0, p16

    iput-object v0, p0, LX/DNA;->b:LX/0ad;

    .line 1990957
    move-object/from16 v0, p18

    iput-object v0, p0, LX/DNA;->v:LX/88l;

    .line 1990958
    return-void
.end method

.method public static t(LX/DNA;)Z
    .locals 2

    .prologue
    .line 1990959
    iget-object v0, p0, LX/DNA;->w:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(I)Ljava/lang/Object;
.end method

.method public abstract a(LX/0fz;)V
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1990961
    invoke-super {p0, p1, p2, p3}, LX/1La;->a(Landroid/support/v4/app/Fragment;Landroid/view/View;Landroid/os/Bundle;)V

    .line 1990962
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DNA;->z:Z

    .line 1990963
    return-void
.end method

.method public final a(Landroid/view/View;LX/DNR;LX/0fz;Lcom/facebook/base/fragment/FbFragment;LX/DNB;LX/0fu;Z)V
    .locals 2

    .prologue
    .line 1990964
    iput-boolean p7, p0, LX/DNA;->C:Z

    .line 1990965
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DNA;->z:Z

    .line 1990966
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DNA;->D:Z

    .line 1990967
    iput-object p2, p0, LX/DNA;->y:LX/DNR;

    .line 1990968
    invoke-virtual {p4, p0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 1990969
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1990970
    iget-object v1, p0, LX/DNA;->x:LX/0Yb;

    if-nez v1, :cond_0

    .line 1990971
    new-instance v1, LX/0Xj;

    invoke-direct {v1, v0}, LX/0Xj;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, LX/0Xk;->a()LX/0YX;

    move-result-object v1

    const-string p2, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance p4, LX/DN7;

    invoke-direct {p4, p0}, LX/DN7;-><init>(LX/DNA;)V

    invoke-interface {v1, p2, p4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v1

    invoke-interface {v1}, LX/0YX;->a()LX/0Yb;

    move-result-object v1

    iput-object v1, p0, LX/DNA;->x:LX/0Yb;

    .line 1990972
    iget-object v1, p0, LX/DNA;->x:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 1990973
    :cond_0
    iput-object p5, p0, LX/DNA;->e:LX/DNB;

    .line 1990974
    iput-object p1, p0, LX/DNA;->c:Landroid/view/View;

    .line 1990975
    iget-object v0, p0, LX/DNA;->s:LX/1LV;

    const-string v1, "group_feed"

    invoke-virtual {v0, v1}, LX/1LV;->a(Ljava/lang/String;)V

    .line 1990976
    invoke-static {p0}, LX/DNA;->t(LX/DNA;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1990977
    iget-object v0, p0, LX/DNA;->q:LX/1Kt;

    iget-object v1, p0, LX/DNA;->s:LX/1LV;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 1990978
    :cond_1
    invoke-virtual {p0}, LX/DNA;->d()V

    .line 1990979
    if-eqz p6, :cond_2

    .line 1990980
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    invoke-interface {v0, p6}, LX/0g8;->b(LX/0fu;)V

    .line 1990981
    :cond_2
    iget-object v0, p0, LX/DNA;->e:LX/DNB;

    invoke-interface {v0, p1}, LX/DNB;->a(Landroid/view/View;)V

    .line 1990982
    invoke-virtual {p0}, LX/DNA;->s()V

    .line 1990983
    invoke-virtual {p0, p3}, LX/DNA;->a(LX/0fz;)V

    .line 1990984
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    new-instance v1, LX/DN4;

    invoke-direct {v1, p0}, LX/DN4;-><init>(LX/DNA;)V

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 1990985
    invoke-virtual {p0}, LX/DNA;->e()V

    .line 1990986
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    iget-object v1, p0, LX/DNA;->o:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 1990987
    invoke-virtual {p0}, LX/DNA;->p()V

    .line 1990988
    new-instance v0, LX/DN5;

    invoke-direct {v0, p0}, LX/DN5;-><init>(LX/DNA;)V

    .line 1990989
    iget-object v1, p0, LX/DNA;->g:LX/DCI;

    .line 1990990
    iget-object p1, p3, LX/0fz;->a:LX/0qq;

    move-object p1, p1

    .line 1990991
    invoke-virtual {v1, p1, v0}, LX/DCI;->a(LX/0qq;LX/0g4;)V

    .line 1990992
    iget-object v1, p0, LX/DNA;->h:LX/DCL;

    .line 1990993
    iget-object p1, p3, LX/0fz;->a:LX/0qq;

    move-object p1, p1

    .line 1990994
    invoke-virtual {v1, p1, v0}, LX/DCL;->a(LX/0qq;LX/0g4;)V

    .line 1990995
    iget-object v1, p0, LX/DNA;->i:LX/DCE;

    .line 1990996
    iget-object p1, p3, LX/0fz;->a:LX/0qq;

    move-object p1, p1

    .line 1990997
    invoke-virtual {v1, p1, v0}, LX/DCE;->a(LX/0qq;LX/0g4;)V

    .line 1990998
    iget-object v1, p0, LX/DNA;->j:LX/DCC;

    .line 1990999
    iget-object p1, p3, LX/0fz;->a:LX/0qq;

    move-object p1, p1

    .line 1991000
    invoke-virtual {v1, p1, v0}, LX/DCC;->a(LX/0qq;LX/0g4;)V

    .line 1991001
    iget-object v1, p0, LX/DNA;->k:LX/DCO;

    .line 1991002
    iget-object p1, p3, LX/0fz;->a:LX/0qq;

    move-object p1, p1

    .line 1991003
    invoke-virtual {v1, p1, v0}, LX/DCO;->a(LX/0qq;LX/0g4;)V

    .line 1991004
    iget-object v1, p0, LX/DNA;->m:LX/DCQ;

    .line 1991005
    iget-object p1, p3, LX/0fz;->a:LX/0qq;

    move-object p1, p1

    .line 1991006
    invoke-virtual {v1, p1, v0}, LX/DCQ;->a(LX/0qq;LX/0g4;)V

    .line 1991007
    iget-object v0, p0, LX/DNA;->t:LX/1B1;

    iget-object v1, p0, LX/DNA;->l:LX/DN8;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 1991008
    iget-object v0, p0, LX/DNA;->t:LX/1B1;

    iget-object v1, p0, LX/DNA;->u:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 1991009
    new-instance v0, LX/DN6;

    invoke-direct {v0, p0}, LX/DN6;-><init>(LX/DNA;)V

    .line 1991010
    iget-object v1, p0, LX/DNA;->p:LX/1CY;

    invoke-virtual {v1, p3, v0}, LX/1CY;->a(LX/0fz;LX/0g4;)V

    .line 1991011
    iget-object v0, p0, LX/DNA;->y:LX/DNR;

    invoke-virtual {v0}, LX/DNR;->c()V

    .line 1991012
    return-void
.end method

.method public abstract a(ZZ)V
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1991013
    invoke-super {p0, p1}, LX/1La;->b(Landroid/support/v4/app/Fragment;)V

    .line 1991014
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DNA;->z:Z

    .line 1991015
    return-void
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1991016
    iget-object v3, p0, LX/DNA;->e:LX/DNB;

    iget-object v0, p0, LX/DNA;->y:LX/DNR;

    invoke-virtual {v0}, LX/DNR;->l()Z

    move-result v4

    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    invoke-interface {v0}, LX/0g8;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v4, v0}, LX/DNB;->a(ZZ)V

    .line 1991017
    iget-object v0, p0, LX/DNA;->y:LX/DNR;

    invoke-virtual {v0}, LX/DNR;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1991018
    invoke-virtual {p0, p1}, LX/DNA;->d(Z)V

    .line 1991019
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 1991020
    goto :goto_0

    .line 1991021
    :cond_2
    invoke-virtual {p0}, LX/DNA;->q()V

    .line 1991022
    const/4 v4, 0x0

    .line 1991023
    move v3, v4

    :goto_2
    invoke-virtual {p0}, LX/DNA;->h()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1991024
    invoke-virtual {p0, v3}, LX/DNA;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Rk;

    invoke-virtual {v0}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1991025
    instance-of p1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz p1, :cond_6

    .line 1991026
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object p1, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, p1, :cond_6

    .line 1991027
    const/4 v4, 0x1

    .line 1991028
    :cond_3
    move v0, v4

    .line 1991029
    if-nez v0, :cond_4

    .line 1991030
    invoke-virtual {p0, v1, v2}, LX/DNA;->a(ZZ)V

    goto :goto_1

    .line 1991031
    :cond_4
    invoke-virtual {p0, v2, v2}, LX/DNA;->a(ZZ)V

    .line 1991032
    invoke-virtual {p0}, LX/DNA;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/DNA;->C:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/DNA;->D:Z

    if-nez v0, :cond_0

    .line 1991033
    iput-boolean v1, p0, LX/DNA;->D:Z

    .line 1991034
    iget-object v0, p0, LX/DNA;->v:LX/88l;

    invoke-virtual {v0}, LX/88l;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/DNA;->b:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-short v4, LX/88j;->d:S

    invoke-interface {v0, v3, v4, v1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1991035
    :goto_3
    if-eqz v1, :cond_0

    .line 1991036
    iget-object v0, p0, LX/DNA;->d:LX/0g8;

    new-instance v1, Lcom/facebook/groups/feed/controller/GroupsFeedBaseController$1;

    invoke-direct {v1, p0}, Lcom/facebook/groups/feed/controller/GroupsFeedBaseController$1;-><init>(LX/DNA;)V

    invoke-interface {v0, v1}, LX/0g8;->a(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_5
    move v1, v2

    .line 1991037
    goto :goto_3

    .line 1991038
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2
.end method

.method public final c(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 1991039
    invoke-static {p0}, LX/DNA;->t(LX/DNA;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991040
    iget-object v0, p0, LX/DNA;->q:LX/1Kt;

    iget-object v1, p0, LX/DNA;->d:LX/0g8;

    invoke-virtual {v0, v1}, LX/1Kt;->c(LX/0g8;)V

    .line 1991041
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 1991042
    iget-object v0, p0, LX/DNA;->y:LX/DNR;

    invoke-virtual {v0, p1}, LX/DNR;->a(Z)V

    .line 1991043
    return-void
.end method

.method public abstract d()V
.end method

.method public final d(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 1990933
    invoke-static {p0}, LX/DNA;->t(LX/DNA;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1990934
    iget-object v0, p0, LX/DNA;->q:LX/1Kt;

    const/4 v1, 0x1

    iget-object v2, p0, LX/DNA;->d:LX/0g8;

    invoke-virtual {v0, v1, v2}, LX/1Kt;->a(ZLX/0g8;)V

    .line 1990935
    :cond_0
    invoke-virtual {p0}, LX/DNA;->i()V

    .line 1990936
    iget-object v0, p0, LX/DNA;->p:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->d()V

    .line 1990937
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 1990912
    return-void
.end method

.method public abstract e()V
.end method

.method public e(Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 1990913
    invoke-virtual {p0}, LX/DNA;->n()V

    .line 1990914
    return-void
.end method

.method public abstract f()I
.end method

.method public abstract g()Ljava/lang/Object;
.end method

.method public abstract h()I
.end method

.method public abstract i()V
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1990915
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/DNA;->c(Z)V

    .line 1990916
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 1990917
    iget-object v0, p0, LX/DNA;->g:LX/DCI;

    invoke-virtual {v0}, LX/DCI;->a()V

    .line 1990918
    iget-object v0, p0, LX/DNA;->h:LX/DCL;

    invoke-virtual {v0}, LX/DCL;->a()V

    .line 1990919
    iget-object v0, p0, LX/DNA;->i:LX/DCE;

    invoke-virtual {v0}, LX/DCE;->a()V

    .line 1990920
    iget-object v0, p0, LX/DNA;->j:LX/DCC;

    invoke-virtual {v0}, LX/DCC;->a()V

    .line 1990921
    iget-object v0, p0, LX/DNA;->k:LX/DCO;

    invoke-virtual {v0}, LX/DCO;->a()V

    .line 1990922
    iget-object v0, p0, LX/DNA;->t:LX/1B1;

    iget-object v1, p0, LX/DNA;->u:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 1990923
    iget-object v0, p0, LX/DNA;->p:LX/1CY;

    invoke-virtual {v0}, LX/1CY;->e()V

    .line 1990924
    iget-object v0, p0, LX/DNA;->m:LX/DCQ;

    invoke-virtual {v0}, LX/DCQ;->a()V

    .line 1990925
    iget-object v0, p0, LX/DNA;->x:LX/0Yb;

    if-eqz v0, :cond_0

    .line 1990926
    iget-object v0, p0, LX/DNA;->x:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 1990927
    const/4 v0, 0x0

    iput-object v0, p0, LX/DNA;->x:LX/0Yb;

    .line 1990928
    :cond_0
    return-void
.end method

.method public p()V
    .locals 0

    .prologue
    .line 1990932
    return-void
.end method

.method public q()V
    .locals 0

    .prologue
    .line 1990929
    return-void
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 1990930
    const/4 v0, 0x0

    return v0
.end method

.method public s()V
    .locals 0

    .prologue
    .line 1990931
    return-void
.end method
