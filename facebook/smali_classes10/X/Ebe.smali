.class public LX/Ebe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Eb5;

.field public final b:[B


# direct methods
.method public constructor <init>(LX/Eb5;[B)V
    .locals 0

    .prologue
    .line 2144181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2144182
    iput-object p1, p0, LX/Ebe;->a:LX/Eb5;

    .line 2144183
    iput-object p2, p0, LX/Ebe;->b:[B

    .line 2144184
    return-void
.end method


# virtual methods
.method public final a(LX/Eat;LX/Eau;)LX/Ecr;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/whispersystems/libsignal/ecc/ECPublicKey;",
            "LX/Eau;",
            ")",
            "LX/Ecr",
            "<",
            "LX/Ebe;",
            "LX/Eba;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2144185
    iget-object v0, p2, LX/Eau;->b:LX/Eas;

    move-object v0, v0

    .line 2144186
    invoke-static {p1, v0}, LX/Ear;->a(LX/Eat;LX/Eas;)[B

    move-result-object v0

    .line 2144187
    iget-object v1, p0, LX/Ebe;->a:LX/Eb5;

    iget-object v2, p0, LX/Ebe;->b:[B

    const-string v3, "WhisperRatchet"

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/16 v4, 0x40

    invoke-virtual {v1, v0, v2, v3, v4}, LX/Eb5;->a([B[B[BI)[B

    move-result-object v0

    .line 2144188
    new-instance v1, LX/Eb4;

    invoke-direct {v1, v0}, LX/Eb4;-><init>([B)V

    .line 2144189
    new-instance v0, LX/Ebe;

    iget-object v2, p0, LX/Ebe;->a:LX/Eb5;

    .line 2144190
    iget-object v3, v1, LX/Eb4;->a:[B

    move-object v3, v3

    .line 2144191
    invoke-direct {v0, v2, v3}, LX/Ebe;-><init>(LX/Eb5;[B)V

    .line 2144192
    new-instance v2, LX/Eba;

    iget-object v3, p0, LX/Ebe;->a:LX/Eb5;

    .line 2144193
    iget-object v4, v1, LX/Eb4;->b:[B

    move-object v1, v4

    .line 2144194
    const/4 v4, 0x0

    invoke-direct {v2, v3, v1, v4}, LX/Eba;-><init>(LX/Eb5;[BI)V

    .line 2144195
    new-instance v1, LX/Ecr;

    invoke-direct {v1, v0, v2}, LX/Ecr;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method
