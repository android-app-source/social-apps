.class public final LX/DSy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/DT7;


# direct methods
.method public constructor <init>(LX/DT7;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2000115
    iput-object p1, p0, LX/DSy;->d:LX/DT7;

    iput-object p2, p0, LX/DSy;->a:Ljava/lang/String;

    iput-object p3, p0, LX/DSy;->b:Ljava/lang/String;

    iput-object p4, p0, LX/DSy;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2000106
    const/4 v0, 0x0

    .line 2000107
    instance-of v1, p1, LX/2Oo;

    if-eqz v1, :cond_0

    .line 2000108
    check-cast p1, LX/2Oo;

    .line 2000109
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2000110
    invoke-virtual {p1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    .line 2000111
    :cond_0
    const v1, 0x14ff31

    if-ne v0, v1, :cond_1

    .line 2000112
    iget-object v0, p0, LX/DSy;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DSy;->d:LX/DT7;

    iget-object v2, v2, LX/DT7;->c:Landroid/content/res/Resources;

    const v3, 0x7f082fa2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2000113
    :goto_0
    return-void

    .line 2000114
    :cond_1
    iget-object v0, p0, LX/DSy;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DSy;->d:LX/DT7;

    iget-object v2, v2, LX/DT7;->c:Landroid/content/res/Resources;

    const v3, 0x7f082fa1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2000102
    const/4 v7, 0x1

    .line 2000103
    iget-object v0, p0, LX/DSy;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/DSy;->d:LX/DT7;

    iget-object v2, v2, LX/DT7;->c:Landroid/content/res/Resources;

    const v3, 0x7f082f9f

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/DSy;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2000104
    iget-object v0, p0, LX/DSy;->d:LX/DT7;

    iget-object v0, v0, LX/DT7;->f:LX/DTh;

    new-instance v1, LX/DTj;

    iget-object v2, p0, LX/DSy;->b:Ljava/lang/String;

    iget-object v3, p0, LX/DSy;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v7}, LX/DTj;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2000105
    return-void
.end method
