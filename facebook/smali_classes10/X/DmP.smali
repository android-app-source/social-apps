.class public final enum LX/DmP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/DmP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/DmP;

.field public static final enum APPOINTMENTS_LIST_ITEM:LX/DmP;

.field public static final enum APPOINTMENT_REQUESTS_LIST_ITEM:LX/DmP;


# instance fields
.field private final viewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2039172
    new-instance v0, LX/DmP;

    const-string v1, "APPOINTMENT_REQUESTS_LIST_ITEM"

    const v2, 0x7f0d01dc

    invoke-direct {v0, v1, v3, v2}, LX/DmP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DmP;->APPOINTMENT_REQUESTS_LIST_ITEM:LX/DmP;

    .line 2039173
    new-instance v0, LX/DmP;

    const-string v1, "APPOINTMENTS_LIST_ITEM"

    const v2, 0x7f0d01db

    invoke-direct {v0, v1, v4, v2}, LX/DmP;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/DmP;->APPOINTMENTS_LIST_ITEM:LX/DmP;

    .line 2039174
    const/4 v0, 0x2

    new-array v0, v0, [LX/DmP;

    sget-object v1, LX/DmP;->APPOINTMENT_REQUESTS_LIST_ITEM:LX/DmP;

    aput-object v1, v0, v3

    sget-object v1, LX/DmP;->APPOINTMENTS_LIST_ITEM:LX/DmP;

    aput-object v1, v0, v4

    sput-object v0, LX/DmP;->$VALUES:[LX/DmP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2039175
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2039176
    iput p3, p0, LX/DmP;->viewType:I

    .line 2039177
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/DmP;
    .locals 1

    .prologue
    .line 2039178
    const-class v0, LX/DmP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/DmP;

    return-object v0
.end method

.method public static values()[LX/DmP;
    .locals 1

    .prologue
    .line 2039179
    sget-object v0, LX/DmP;->$VALUES:[LX/DmP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/DmP;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 2039180
    iget v0, p0, LX/DmP;->viewType:I

    return v0
.end method
