.class public LX/Cv6;
.super LX/16T;
.source ""

# interfaces
.implements LX/16E;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Cv6;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1947700
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1947701
    return-void
.end method

.method public static a(LX/0QB;)LX/Cv6;
    .locals 3

    .prologue
    .line 1947702
    sget-object v0, LX/Cv6;->b:LX/Cv6;

    if-nez v0, :cond_1

    .line 1947703
    const-class v1, LX/Cv6;

    monitor-enter v1

    .line 1947704
    :try_start_0
    sget-object v0, LX/Cv6;->b:LX/Cv6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1947705
    if-eqz v2, :cond_0

    .line 1947706
    :try_start_1
    new-instance v0, LX/Cv6;

    invoke-direct {v0}, LX/Cv6;-><init>()V

    .line 1947707
    move-object v0, v0

    .line 1947708
    sput-object v0, LX/Cv6;->b:LX/Cv6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1947709
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1947710
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1947711
    :cond_1
    sget-object v0, LX/Cv6;->b:LX/Cv6;

    return-object v0

    .line 1947712
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1947713
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 1947714
    iget-boolean v0, p0, LX/Cv6;->a:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1947715
    if-eqz p2, :cond_0

    instance-of v0, p2, Landroid/view/View;

    if-nez v0, :cond_1

    .line 1947716
    :cond_0
    :goto_0
    return-void

    .line 1947717
    :cond_1
    check-cast p2, Landroid/view/View;

    .line 1947718
    new-instance v0, LX/0hs;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1947719
    const/4 v1, -0x1

    .line 1947720
    iput v1, v0, LX/0hs;->t:I

    .line 1947721
    const v1, 0x7f080d44

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1947722
    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1947723
    invoke-virtual {v0, v2}, LX/0ht;->e(Z)V

    .line 1947724
    invoke-virtual {v0, p2}, LX/0ht;->f(Landroid/view/View;)V

    .line 1947725
    iput-boolean v2, p0, LX/Cv6;->a:Z

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1947726
    const-string v0, "4286"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1947727
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_DASHBOARD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
