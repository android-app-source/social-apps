.class public final LX/EUe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0hn;


# direct methods
.method public constructor <init>(LX/0hn;)V
    .locals 0

    .prologue
    .line 2126299
    iput-object p1, p0, LX/EUe;->a:LX/0hn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2126363
    sget-object v0, LX/0hn;->d:Ljava/lang/String;

    const-string v1, "Video Home prefetch metadata query failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2126364
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2126300
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2126301
    if-eqz p1, :cond_0

    .line 2126302
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2126303
    if-nez v0, :cond_2

    .line 2126304
    :cond_0
    sget-object v0, LX/0hn;->d:Ljava/lang/String;

    const-string v1, "Video Home prefetch metadata query succeeded but result was null."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2126305
    :cond_1
    :goto_0
    return-void

    .line 2126306
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2126307
    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->j()I

    move-result v3

    .line 2126308
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2126309
    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->a()I

    move-result v4

    .line 2126310
    iget-object v0, p0, LX/EUe;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AW;

    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    iget-object v1, v1, LX/0hn;->w:LX/095;

    .line 2126311
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/0JT;->VIDEO_HOME_METADATA_FETCHED:LX/0JT;

    iget-object v5, v5, LX/0JT;->value:Ljava/lang/String;

    invoke-direct {v2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2126312
    sget-object v5, LX/04F;->PLAYER_ORIGIN:LX/04F;

    iget-object v5, v5, LX/04F;->value:Ljava/lang/String;

    sget-object v6, LX/04D;->VIDEO_HOME:LX/04D;

    iget-object v6, v6, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2126313
    sget-object v5, LX/0JS;->FETCH_REASON:LX/0JS;

    iget-object v5, v5, LX/0JS;->value:Ljava/lang/String;

    iget-object v6, v1, LX/095;->value:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2126314
    sget-object v5, LX/0JS;->BADGE_COUNT:LX/0JS;

    iget-object v5, v5, LX/0JS;->value:Ljava/lang/String;

    invoke-virtual {v2, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2126315
    invoke-static {v0, v2}, LX/3AW;->a(LX/3AW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2126316
    new-instance v0, LX/EUb;

    invoke-direct {v0}, LX/EUb;-><init>()V

    .line 2126317
    iput v3, v0, LX/EUb;->c:I

    .line 2126318
    move-object v0, v0

    .line 2126319
    iput v4, v0, LX/EUb;->a:I

    .line 2126320
    move-object v1, v0

    .line 2126321
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2126322
    check-cast v0, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/videohome/protocol/VideoHomeQueryModels$VideoHomePrefetchMetadataQueryModel;->k()I

    move-result v0

    int-to-long v6, v0

    .line 2126323
    iput-wide v6, v1, LX/EUb;->e:J

    .line 2126324
    move-object v0, v1

    .line 2126325
    invoke-virtual {v0}, LX/EUb;->a()LX/EUc;

    move-result-object v5

    .line 2126326
    iget-object v0, p0, LX/EUe;->a:LX/0hn;

    iget-object v0, v0, LX/0hn;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hp;

    .line 2126327
    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    iget-object v1, v1, LX/0hn;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ETB;

    invoke-virtual {v1}, LX/ETB;->c()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2126328
    iget v1, v5, LX/EUc;->a:I

    move v1, v1

    .line 2126329
    invoke-virtual {v0, v1}, LX/0hp;->b(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    move v2, v1

    .line 2126330
    :goto_1
    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    iget-object v1, v1, LX/0hn;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xX;

    sget-object v6, LX/1vy;->PREFETCH_CONTROLLER:LX/1vy;

    invoke-virtual {v1, v6}, LX/0xX;->a(LX/1vy;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2126331
    if-eqz v2, :cond_3

    .line 2126332
    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    invoke-static {v1}, LX/0hn;->m(LX/0hn;)V

    .line 2126333
    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    new-instance v3, LX/EUb;

    invoke-direct {v3, v5}, LX/EUb;-><init>(LX/EUc;)V

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 2126334
    iput-object v4, v3, LX/EUb;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 2126335
    move-object v3, v3

    .line 2126336
    invoke-virtual {v3}, LX/EUb;->a()LX/EUc;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0hn;->a(LX/EUc;)V

    .line 2126337
    :cond_3
    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    iget-object v1, v1, LX/0hn;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3AX;

    .line 2126338
    iget-object v8, v1, LX/3AX;->k:LX/EUc;

    if-nez v8, :cond_8

    sget-object v8, LX/0JU;->INITIAL:LX/0JU;

    .line 2126339
    :goto_2
    iput-object v5, v1, LX/3AX;->k:LX/EUc;

    .line 2126340
    iget-object v9, v1, LX/3AX;->g:LX/ETB;

    .line 2126341
    iget-wide v12, v5, LX/EUc;->e:J

    move-wide v10, v12

    .line 2126342
    invoke-virtual {v9, v10, v11}, LX/ETB;->a(J)V

    .line 2126343
    invoke-static {v1, v8}, LX/3AX;->a(LX/3AX;LX/0JU;)Z

    .line 2126344
    if-eqz v2, :cond_1

    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    iget-object v1, v1, LX/0hn;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3AX;

    invoke-virtual {v1}, LX/3AX;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    invoke-virtual {v1}, LX/0hn;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    invoke-static {v1}, LX/0hn;->p(LX/0hn;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2126345
    iget v1, v5, LX/EUc;->a:I

    move v1, v1

    .line 2126346
    invoke-virtual {v0, v1}, LX/0hp;->a(I)V

    goto/16 :goto_0

    .line 2126347
    :cond_4
    const/4 v1, 0x0

    move v2, v1

    goto :goto_1

    .line 2126348
    :cond_5
    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    invoke-virtual {v1, v3, v4}, LX/0hn;->a(II)Z

    move-result v1

    .line 2126349
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2126350
    iget-object v3, p0, LX/EUe;->a:LX/0hn;

    invoke-virtual {v3}, LX/0hn;->b()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2126351
    if-eqz v2, :cond_1

    .line 2126352
    iget-object v0, p0, LX/EUe;->a:LX/0hn;

    invoke-static {v0}, LX/0hn;->m(LX/0hn;)V

    .line 2126353
    iget-object v0, p0, LX/EUe;->a:LX/0hn;

    new-instance v1, LX/EUb;

    invoke-direct {v1, v5}, LX/EUb;-><init>(LX/EUc;)V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;->LIVE_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 2126354
    iput-object v2, v1, LX/EUb;->f:Lcom/facebook/graphql/enums/GraphQLVideoHomeBadgeUpdateReason;

    .line 2126355
    move-object v1, v1

    .line 2126356
    invoke-virtual {v1}, LX/EUb;->a()LX/EUc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hn;->a(LX/EUc;)V

    goto/16 :goto_0

    .line 2126357
    :cond_6
    if-eqz v1, :cond_7

    .line 2126358
    iget-object v0, p0, LX/EUe;->a:LX/0hn;

    sget-object v1, LX/0JU;->BADGE_FETCH:LX/0JU;

    invoke-static {v0, v5, v1}, LX/0hn;->a$redex0(LX/0hn;LX/EUc;LX/0JU;)V

    goto/16 :goto_0

    .line 2126359
    :cond_7
    if-eqz v2, :cond_1

    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    invoke-virtual {v1, v5}, LX/0hn;->b(LX/EUc;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/EUe;->a:LX/0hn;

    invoke-static {v1}, LX/0hn;->p(LX/0hn;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2126360
    iget v1, v5, LX/EUc;->a:I

    move v1, v1

    .line 2126361
    invoke-virtual {v0, v1}, LX/0hp;->a(I)V

    goto/16 :goto_0

    .line 2126362
    :cond_8
    sget-object v8, LX/0JU;->FOREGROUNDING:LX/0JU;

    goto/16 :goto_2
.end method
