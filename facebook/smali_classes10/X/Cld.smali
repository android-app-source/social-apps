.class public final synthetic LX/Cld;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1932595
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->values()[Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Cld;->b:[I

    :try_start_0
    sget-object v0, LX/Cld;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_10

    :goto_0
    :try_start_1
    sget-object v0, LX/Cld;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->MENTION:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_f

    .line 1932596
    :goto_1
    invoke-static {}, LX/Clb;->values()[LX/Clb;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Cld;->a:[I

    :try_start_2
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->KICKER:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_e

    :goto_2
    :try_start_3
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->TITLE:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_d

    :goto_3
    :try_start_4
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->SUBTITLE:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_c

    :goto_4
    :try_start_5
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->HEADER_ONE:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_b

    :goto_5
    :try_start_6
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->HEADER_TWO:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_a

    :goto_6
    :try_start_7
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->BODY:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_9

    :goto_7
    :try_start_8
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->BLOCK_QUOTE:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :goto_8
    :try_start_9
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->PULL_QUOTE:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_7

    :goto_9
    :try_start_a
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->PULL_QUOTE_ATTRIBUTION:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_6

    :goto_a
    :try_start_b
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->RELATED_ARTICLES:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_5

    :goto_b
    :try_start_c
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->CAPTION_TITLE:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_4

    :goto_c
    :try_start_d
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->CAPTION_SUBTITLE:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_3

    :goto_d
    :try_start_e
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->BYLINE:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_2

    :goto_e
    :try_start_f
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->CREDITS:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_1

    :goto_f
    :try_start_10
    sget-object v0, LX/Cld;->a:[I

    sget-object v1, LX/Clb;->COPYRIGHT:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_0

    :goto_10
    return-void

    :catch_0
    goto :goto_10

    :catch_1
    goto :goto_f

    :catch_2
    goto :goto_e

    :catch_3
    goto :goto_d

    :catch_4
    goto :goto_c

    :catch_5
    goto :goto_b

    :catch_6
    goto :goto_a

    :catch_7
    goto :goto_9

    :catch_8
    goto :goto_8

    :catch_9
    goto :goto_7

    :catch_a
    goto/16 :goto_6

    :catch_b
    goto/16 :goto_5

    :catch_c
    goto/16 :goto_4

    :catch_d
    goto/16 :goto_3

    :catch_e
    goto/16 :goto_2

    :catch_f
    goto/16 :goto_1

    :catch_10
    goto/16 :goto_0
.end method
