.class public LX/DGK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:I

.field public final d:I

.field public final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;IIZ)V"
        }
    .end annotation

    .prologue
    .line 1979591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1979592
    iput p2, p0, LX/DGK;->c:I

    .line 1979593
    iput-object p1, p0, LX/DGK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979594
    iput p3, p0, LX/DGK;->d:I

    .line 1979595
    iput-boolean p4, p0, LX/DGK;->e:Z

    .line 1979596
    iget-object v0, p0, LX/DGK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979597
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1979598
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/0Px;

    move-result-object v0

    .line 1979599
    iget v1, p0, LX/DGK;->c:I

    if-ltz v1, :cond_0

    iget v1, p0, LX/DGK;->c:I

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v1, p0, LX/DGK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v2, p0, LX/DGK;->c:I

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979600
    return-void

    .line 1979601
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
