.class public final LX/EGT;
.super LX/EC0;
.source ""


# instance fields
.field public final synthetic a:LX/EGe;


# direct methods
.method public constructor <init>(LX/EGe;)V
    .locals 0

    .prologue
    .line 2097360
    iput-object p1, p0, LX/EGT;->a:LX/EGe;

    invoke-direct {p0}, LX/EC0;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 2097358
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->A:LX/EFt;

    new-instance v1, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;-><init>(LX/EGT;)V

    invoke-virtual {v0, v1}, LX/EFt;->a(Ljava/lang/Runnable;)V

    .line 2097359
    return-void
.end method

.method public final a(ZZ)V
    .locals 5

    .prologue
    .line 2097348
    if-eqz p1, :cond_2

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-eqz v0, :cond_2

    .line 2097349
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097350
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->X(LX/EGe;)V

    .line 2097351
    :cond_0
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2097352
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->Z(LX/EGe;)V

    .line 2097353
    :cond_1
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->af(LX/EGe;)V

    .line 2097354
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-wide v0, v0, LX/EGe;->ac:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 2097355
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v1, p0, LX/EGT;->a:LX/EGe;

    iget-object v1, v1, LX/EGe;->m:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 2097356
    iput-wide v2, v0, LX/EGe;->ac:J

    .line 2097357
    :cond_2
    return-void
.end method

.method public final a(IJZZZ)Z
    .locals 8

    .prologue
    .line 2097294
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-nez v0, :cond_1

    .line 2097295
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097296
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->D:LX/EI9;

    sget-object v1, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    invoke-virtual {v0, v1}, LX/EI9;->setViewType(LX/EI8;)V

    .line 2097297
    :cond_0
    const/4 v0, 0x0

    .line 2097298
    :goto_0
    return v0

    .line 2097299
    :cond_1
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->aD(LX/EGe;)V

    .line 2097300
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2097301
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v1, v0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ay()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/EI9;->setPeerName(Ljava/lang/String;)V

    .line 2097302
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->D:LX/EI9;

    sget-object v1, LX/EI8;->END_CALL_STATE_VOICEMAIL:LX/EI8;

    invoke-virtual {v0, v1}, LX/EI9;->setViewType(LX/EI8;)V

    .line 2097303
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v1, v0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->A()Z

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/EI9;->a(ZZ)V

    .line 2097304
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v1, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v1}, LX/EGe;->F(LX/EGe;)I

    move-result v1

    iget-object v2, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v2}, LX/EGe;->F(LX/EGe;)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LX/EGT;->a:LX/EGe;

    iget v3, v3, LX/EGe;->H:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iget-object v3, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v3, v3, LX/EGe;->X:Z

    invoke-static {v0, v1, v2, v3}, LX/EGe;->a$redex0(LX/EGe;IIZ)V

    .line 2097305
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, LX/EGe;->a$redex0(LX/EGe;ZZZ)V

    .line 2097306
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v1, v0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EFj;

    invoke-static {}, LX/7TQ;->values()[LX/7TQ;

    move-result-object v2

    aget-object v2, v2, p1

    sget-object v3, LX/EFi;->ChatHead:LX/EFi;

    invoke-virtual {v0, v2, v3}, LX/EFj;->a(LX/7TQ;LX/EFi;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/EI9;->a(Ljava/lang/String;)V

    .line 2097307
    :cond_2
    :goto_1
    if-nez p4, :cond_3

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->s()Z

    move-result v0

    if-nez v0, :cond_3

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_3

    .line 2097308
    invoke-static {}, LX/7TQ;->values()[LX/7TQ;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2097309
    sget-object v1, LX/7TQ;->CallEndIgnoreCall:LX/7TQ;

    if-eq v0, v1, :cond_b

    sget-object v1, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    if-eq v0, v1, :cond_b

    const/4 v0, 0x1

    move v1, v0

    .line 2097310
    :goto_2
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v2, v0, LX/EGe;->q:LX/EDC;

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097311
    iget-wide v6, v0, LX/EDx;->ak:J

    move-wide v4, v6

    .line 2097312
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097313
    iget-boolean v3, v0, LX/EDx;->aB:Z

    move v3, v3

    .line 2097314
    if-eqz v1, :cond_c

    sget-object v0, LX/EDB;->INSTANT_VIDEO_ENDED_WITH_ERROR:LX/EDB;

    :goto_3
    invoke-virtual {v2, v4, v5, v3, v0}, LX/EDC;->a(JZLX/EDB;)Z

    .line 2097315
    :cond_3
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_d

    if-eqz p6, :cond_d

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->ai:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDT;

    invoke-virtual {v0}, LX/EDT;->a()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-nez v0, :cond_d

    .line 2097316
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->h:LX/2S7;

    invoke-virtual {v0}, LX/2S7;->g()V

    .line 2097317
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2097318
    :cond_4
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-virtual {v0}, LX/EGe;->r()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2097319
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    .line 2097320
    invoke-static {}, LX/7TQ;->values()[LX/7TQ;

    move-result-object v1

    aget-object v1, v1, p1

    .line 2097321
    sget-object v2, LX/EGS;->a:[I

    invoke-virtual {v1}, LX/7TQ;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2097322
    sget-object v1, LX/EGZ;->NO_END_CALL:LX/EGZ;

    :goto_4
    move-object v1, v1

    .line 2097323
    sget-object v0, LX/EGZ;->NO_END_CALL:LX/EGZ;

    if-eq v1, v0, :cond_6

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    .line 2097324
    :goto_5
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-eqz v2, :cond_5

    .line 2097325
    iget-object v2, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v2}, LX/EGe;->aB(LX/EGe;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 2097326
    :cond_5
    if-eqz v0, :cond_a

    .line 2097327
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v2, v0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ay()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/EI9;->setPeerName(Ljava/lang/String;)V

    .line 2097328
    sget-object v0, LX/EGZ;->END_CALL_WITH_RETRY:LX/EGZ;

    if-ne v1, v0, :cond_7

    const/4 v0, 0x1

    move v1, v0

    .line 2097329
    :goto_6
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v2, v0, LX/EGe;->D:LX/EI9;

    if-eqz v1, :cond_8

    sget-object v0, LX/EI8;->END_CALL_STATE_WITH_RETRY:LX/EI8;

    :goto_7
    invoke-virtual {v2, v0}, LX/EI9;->setViewType(LX/EI8;)V

    .line 2097330
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v2, v0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->A()Z

    move-result v3

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_8
    invoke-virtual {v2, v3, v0}, LX/EI9;->a(ZZ)V

    .line 2097331
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v2, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v2}, LX/EGe;->F(LX/EGe;)I

    move-result v2

    iget-object v3, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v3}, LX/EGe;->F(LX/EGe;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, LX/EGT;->a:LX/EGe;

    iget v4, v4, LX/EGe;->H:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget-object v4, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v4, v4, LX/EGe;->X:Z

    invoke-static {v0, v2, v3, v4}, LX/EGe;->a$redex0(LX/EGe;IIZ)V

    .line 2097332
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v2, v1, v3}, LX/EGe;->a$redex0(LX/EGe;ZZZ)V

    .line 2097333
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v1, v0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EFj;

    invoke-static {}, LX/7TQ;->values()[LX/7TQ;

    move-result-object v2

    aget-object v2, v2, p1

    sget-object v3, LX/EFi;->ChatHead:LX/EFi;

    invoke-virtual {v0, v2, v3}, LX/EFj;->a(LX/7TQ;LX/EFi;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/EI9;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2097334
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 2097335
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    goto :goto_6

    .line 2097336
    :cond_8
    sget-object v0, LX/EI8;->END_CALL_STATE:LX/EI8;

    goto :goto_7

    .line 2097337
    :cond_9
    const/4 v0, 0x0

    goto :goto_8

    .line 2097338
    :cond_a
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/EGe;->c(LX/EGe;Z)V

    goto/16 :goto_1

    .line 2097339
    :cond_b
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_2

    .line 2097340
    :cond_c
    sget-object v0, LX/EDB;->INSTANT_VIDEO_ENDED:LX/EDB;

    goto/16 :goto_3

    .line 2097341
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2097342
    :pswitch_0
    if-eqz p4, :cond_e

    sget-object v1, LX/EGZ;->END_CALL:LX/EGZ;

    goto/16 :goto_4

    :cond_e
    sget-object v1, LX/EGZ;->END_CALL_WITH_RETRY:LX/EGZ;

    goto/16 :goto_4

    .line 2097343
    :pswitch_1
    sget-object v1, LX/EGZ;->END_CALL_WITH_RETRY:LX/EGZ;

    goto/16 :goto_4

    .line 2097344
    :pswitch_2
    sget-object v1, LX/EGZ;->END_CALL:LX/EGZ;

    goto/16 :goto_4

    .line 2097345
    :pswitch_3
    if-nez p4, :cond_f

    iget-object v1, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->q()Z

    move-result v1

    if-nez v1, :cond_f

    sget-object v1, LX/EGZ;->END_CALL:LX/EGZ;

    goto/16 :goto_4

    :cond_f
    sget-object v1, LX/EGZ;->NO_END_CALL:LX/EGZ;

    goto/16 :goto_4

    .line 2097346
    :pswitch_4
    if-eqz p4, :cond_10

    sget-object v1, LX/EGZ;->END_CALL:LX/EGZ;

    goto/16 :goto_4

    :cond_10
    sget-object v1, LX/EGZ;->NO_END_CALL:LX/EGZ;

    goto/16 :goto_4

    .line 2097347
    :pswitch_5
    sget-object v1, LX/EGZ;->NO_END_CALL:LX/EGZ;

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2097292
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->al(LX/EGe;)V

    .line 2097293
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2097284
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-nez v0, :cond_0

    .line 2097285
    :goto_0
    return-void

    .line 2097286
    :cond_0
    if-eqz p1, :cond_2

    .line 2097287
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->X(LX/EGe;)V

    .line 2097288
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2097289
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->Z(LX/EGe;)V

    .line 2097290
    :cond_1
    :goto_1
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->af(LX/EGe;)V

    goto :goto_0

    .line 2097291
    :cond_2
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->aa(LX/EGe;)V

    goto :goto_1
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2097280
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-nez v0, :cond_1

    .line 2097281
    :cond_0
    :goto_0
    return-void

    .line 2097282
    :cond_1
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-virtual {v0}, LX/EGe;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2097283
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->T(LX/EGe;)V

    goto :goto_0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 2097247
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-nez v0, :cond_1

    .line 2097248
    :cond_0
    :goto_0
    return-void

    .line 2097249
    :cond_1
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097250
    :cond_2
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->D:LX/EI9;

    iget-object v1, p0, LX/EGT;->a:LX/EGe;

    const v2, 0x7f08072d

    invoke-virtual {v1, v2}, LX/EGe;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EI9;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2097275
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-nez v0, :cond_1

    .line 2097276
    :cond_0
    :goto_0
    return-void

    .line 2097277
    :cond_1
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-eqz v0, :cond_2

    .line 2097278
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->X(LX/EGe;)V

    .line 2097279
    :cond_2
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->af(LX/EGe;)V

    goto :goto_0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 2097270
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-nez v0, :cond_0

    .line 2097271
    :goto_0
    return-void

    .line 2097272
    :cond_0
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->Y(LX/EGe;)V

    .line 2097273
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->af(LX/EGe;)V

    .line 2097274
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    const/4 v1, 0x1

    iget-object v2, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v2, v2, LX/EGe;->X:Z

    invoke-static {v0, v1, v2}, LX/EGe;->b(LX/EGe;ZZ)V

    goto :goto_0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 2097262
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-nez v0, :cond_0

    .line 2097263
    :goto_0
    return-void

    .line 2097264
    :cond_0
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2097265
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->Q(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2097266
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->X(LX/EGe;)V

    .line 2097267
    :cond_1
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->R$redex0(LX/EGe;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2097268
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->Z(LX/EGe;)V

    .line 2097269
    :cond_2
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->af(LX/EGe;)V

    goto :goto_0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2097259
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->aF(LX/EGe;)V

    .line 2097260
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->aG(LX/EGe;)V

    .line 2097261
    return-void
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 2097258
    const/4 v0, 0x2

    return v0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 2097256
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->aG(LX/EGe;)V

    .line 2097257
    return-void
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 2097253
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->aF(LX/EGe;)V

    .line 2097254
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->aG(LX/EGe;)V

    .line 2097255
    return-void
.end method

.method public final v()V
    .locals 2

    .prologue
    .line 2097251
    iget-object v0, p0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->A:LX/EFt;

    new-instance v1, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$1;-><init>(LX/EGT;)V

    invoke-virtual {v0, v1}, LX/EFt;->a(Ljava/lang/Runnable;)V

    .line 2097252
    return-void
.end method
