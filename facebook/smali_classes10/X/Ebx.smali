.class public final LX/Ebx;
.super LX/EWp;
.source ""

# interfaces
.implements LX/Ebv;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ebx;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/Ebx;


# instance fields
.field public bitField0_:I

.field public currentSession_:LX/Ecf;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public previousSessions_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ecf;",
            ">;"
        }
    .end annotation
.end field

.field private final unknownFields:LX/EZQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2145108
    new-instance v0, LX/Ebu;

    invoke-direct {v0}, LX/Ebu;-><init>()V

    sput-object v0, LX/Ebx;->a:LX/EWZ;

    .line 2145109
    new-instance v0, LX/Ebx;

    invoke-direct {v0}, LX/Ebx;-><init>()V

    .line 2145110
    sput-object v0, LX/Ebx;->c:LX/Ebx;

    invoke-direct {v0}, LX/Ebx;->q()V

    .line 2145111
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2145103
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145104
    iput-byte v0, p0, LX/Ebx;->memoizedIsInitialized:B

    .line 2145105
    iput v0, p0, LX/Ebx;->memoizedSerializedSize:I

    .line 2145106
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2145107
    iput-object v0, p0, LX/Ebx;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v6, 0x2

    const/4 v4, 0x1

    .line 2145058
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2145059
    iput-byte v1, p0, LX/Ebx;->memoizedIsInitialized:B

    .line 2145060
    iput v1, p0, LX/Ebx;->memoizedSerializedSize:I

    .line 2145061
    invoke-direct {p0}, LX/Ebx;->q()V

    .line 2145062
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v5

    move v3, v0

    move v1, v0

    .line 2145063
    :goto_0
    if-nez v3, :cond_2

    .line 2145064
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2145065
    sparse-switch v0, :sswitch_data_0

    .line 2145066
    invoke-virtual {p0, p1, v5, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_6

    move v3, v4

    .line 2145067
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 2145068
    goto :goto_0

    .line 2145069
    :sswitch_1
    const/4 v0, 0x0

    .line 2145070
    iget v2, p0, LX/Ebx;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_5

    .line 2145071
    iget-object v0, p0, LX/Ebx;->currentSession_:LX/Ecf;

    invoke-virtual {v0}, LX/Ecf;->O()LX/EcK;

    move-result-object v0

    move-object v2, v0

    .line 2145072
    :goto_1
    sget-object v0, LX/Ecf;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/Ecf;

    iput-object v0, p0, LX/Ebx;->currentSession_:LX/Ecf;

    .line 2145073
    if-eqz v2, :cond_0

    .line 2145074
    iget-object v0, p0, LX/Ebx;->currentSession_:LX/Ecf;

    invoke-virtual {v2, v0}, LX/EcK;->a(LX/Ecf;)LX/EcK;

    .line 2145075
    invoke-virtual {v2}, LX/EcK;->m()LX/Ecf;

    move-result-object v0

    iput-object v0, p0, LX/Ebx;->currentSession_:LX/Ecf;

    .line 2145076
    :cond_0
    iget v0, p0, LX/Ebx;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ebx;->bitField0_:I
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2145077
    :catch_0
    move-exception v0

    .line 2145078
    :goto_2
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145079
    move-object v0, v0

    .line 2145080
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2145081
    :catchall_0
    move-exception v0

    :goto_3
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v6, :cond_1

    .line 2145082
    iget-object v1, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    .line 2145083
    :cond_1
    invoke-virtual {v5}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/Ebx;->unknownFields:LX/EZQ;

    .line 2145084
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2145085
    :sswitch_2
    and-int/lit8 v0, v1, 0x2

    if-eq v0, v6, :cond_4

    .line 2145086
    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ebx;->previousSessions_:Ljava/util/List;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2145087
    or-int/lit8 v0, v1, 0x2

    .line 2145088
    :goto_4
    :try_start_3
    iget-object v1, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    sget-object v2, LX/Ecf;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch LX/EYr; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_5
    move v1, v0

    .line 2145089
    goto :goto_0

    .line 2145090
    :cond_2
    and-int/lit8 v0, v1, 0x2

    if-ne v0, v6, :cond_3

    .line 2145091
    iget-object v0, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    .line 2145092
    :cond_3
    invoke-virtual {v5}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ebx;->unknownFields:LX/EZQ;

    .line 2145093
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2145094
    return-void

    .line 2145095
    :catch_1
    move-exception v0

    .line 2145096
    :goto_6
    :try_start_4
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2145097
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2145098
    move-object v0, v2

    .line 2145099
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2145100
    :catchall_1
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_3

    .line 2145101
    :catch_2
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_6

    .line 2145102
    :catch_3
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_4

    :cond_5
    move-object v2, v0

    goto/16 :goto_1

    :cond_6
    move v0, v1

    goto :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2145053
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2145054
    iput-byte v1, p0, LX/Ebx;->memoizedIsInitialized:B

    .line 2145055
    iput v1, p0, LX/Ebx;->memoizedSerializedSize:I

    .line 2145056
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/Ebx;->unknownFields:LX/EZQ;

    .line 2145057
    return-void
.end method

.method private static b(LX/Ebx;)LX/Ebw;
    .locals 1

    .prologue
    .line 2145052
    invoke-static {}, LX/Ebw;->u()LX/Ebw;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/Ebw;->a(LX/Ebx;)LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 1

    .prologue
    .line 2145048
    sget-object v0, LX/Ecf;->c:LX/Ecf;

    move-object v0, v0

    .line 2145049
    iput-object v0, p0, LX/Ebx;->currentSession_:LX/Ecf;

    .line 2145050
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    .line 2145051
    return-void
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2145046
    new-instance v0, LX/Ebw;

    invoke-direct {v0, p1}, LX/Ebw;-><init>(LX/EYd;)V

    .line 2145047
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2145038
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2145039
    iget v0, p0, LX/Ebx;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2145040
    iget-object v0, p0, LX/Ebx;->currentSession_:LX/Ecf;

    invoke-virtual {p1, v1, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2145041
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2145042
    const/4 v2, 0x2

    iget-object v0, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v2, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2145043
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2145044
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2145045
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2145112
    iget-byte v1, p0, LX/Ebx;->memoizedIsInitialized:B

    .line 2145113
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v0, :cond_0

    .line 2145114
    :goto_0
    return v0

    .line 2145115
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2145116
    :cond_1
    iput-byte v0, p0, LX/Ebx;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2145021
    iget v0, p0, LX/Ebx;->memoizedSerializedSize:I

    .line 2145022
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2145023
    :goto_0
    return v0

    .line 2145024
    :cond_0
    iget v0, p0, LX/Ebx;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 2145025
    iget-object v0, p0, LX/Ebx;->currentSession_:LX/Ecf;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v0

    .line 2145026
    :goto_2
    iget-object v0, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2145027
    const/4 v3, 0x2

    iget-object v0, p0, LX/Ebx;->previousSessions_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v3, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v0, v2

    .line 2145028
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 2145029
    :cond_1
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0}, LX/EZQ;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 2145030
    iput v0, p0, LX/Ebx;->memoizedSerializedSize:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2145037
    iget-object v0, p0, LX/Ebx;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2145036
    sget-object v0, LX/Eck;->n:LX/EYn;

    const-class v1, LX/Ebx;

    const-class v2, LX/Ebw;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/Ebx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2145035
    sget-object v0, LX/Ebx;->a:LX/EWZ;

    return-object v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2145034
    invoke-static {p0}, LX/Ebx;->b(LX/Ebx;)LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2145033
    invoke-static {}, LX/Ebw;->u()LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2145032
    invoke-static {p0}, LX/Ebx;->b(LX/Ebx;)LX/Ebw;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2145031
    sget-object v0, LX/Ebx;->c:LX/Ebx;

    return-object v0
.end method
