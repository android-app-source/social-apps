.class public final LX/EiQ;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;I)V
    .locals 0

    .prologue
    .line 2159400
    iput-object p1, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iput p2, p0, LX/EiQ;->a:I

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 2159401
    iget-object v0, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v1, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159402
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159403
    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159404
    iget-object v2, v0, LX/2U9;->a:LX/0Zb;

    sget-object v3, LX/Eiw;->RESEND_CODE_FAILURE:LX/Eiw;

    invoke-virtual {v3}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2159405
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2159406
    const-string v3, "confirmation"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159407
    const-string v3, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159408
    const-string v3, "error_code"

    invoke-static {p1}, LX/2U9;->a(Lcom/facebook/fbservice/service/ServiceException;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2159409
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2159410
    :cond_0
    iget-object v0, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->b(Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/String;

    move-result-object v0

    .line 2159411
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2159412
    iget-object v1, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->c:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2159413
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2159414
    iget-object v0, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->q:LX/2U9;

    iget-object v1, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v1, v1, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159415
    iget-object v2, v1, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v1, v2

    .line 2159416
    iget-object v1, v1, Lcom/facebook/growth/model/Contactpoint;->type:Lcom/facebook/growth/model/ContactpointType;

    .line 2159417
    iget-object v2, v0, LX/2U9;->a:LX/0Zb;

    sget-object v3, LX/Eiw;->RESEND_CODE_SUCCESS:LX/Eiw;

    invoke-virtual {v3}, LX/Eiw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    const/4 p1, 0x1

    invoke-interface {v2, v3, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2159418
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2159419
    const-string v3, "confirmation"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2159420
    const-string v3, "current_contactpoint_type"

    invoke-virtual {v1}, Lcom/facebook/growth/model/ContactpointType;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2159421
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2159422
    :cond_0
    iget-object v0, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfInputFragment;->m:Lcom/facebook/confirmation/model/AccountConfirmationData;

    .line 2159423
    iget-object v1, v0, Lcom/facebook/confirmation/model/AccountConfirmationData;->a:Lcom/facebook/growth/model/Contactpoint;

    move-object v0, v1

    .line 2159424
    invoke-virtual {v0}, Lcom/facebook/growth/model/Contactpoint;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2159425
    iget-object v0, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    sget-object v1, LX/EiG;->UPDATE_PHONE:LX/EiG;

    invoke-virtual {v0, v1}, Lcom/facebook/confirmation/fragment/ConfInputFragment;->a(LX/EiG;)V

    .line 2159426
    :cond_1
    iget-object v0, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    iget-object v0, v0, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    iget v2, p0, LX/EiQ;->a:I

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2159427
    iget-object v0, p0, LX/EiQ;->b:Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;

    invoke-static {v0}, Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;->D(Lcom/facebook/confirmation/fragment/ConfCodeInputFragment;)V

    .line 2159428
    return-void
.end method
