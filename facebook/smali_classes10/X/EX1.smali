.class public abstract LX/EX1;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EWx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "LX/EX1;",
        ">",
        "LX/EWp;",
        "LX/EWx",
        "<TMessageType;>;"
    }
.end annotation


# instance fields
.field public final extensions:LX/EYc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EYc",
            "<",
            "LX/EYP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2132234
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2132235
    invoke-static {}, LX/EYc;->a()LX/EYc;

    move-result-object v0

    iput-object v0, p0, LX/EX1;->extensions:LX/EYc;

    .line 2132236
    return-void
.end method

.method public constructor <init>(LX/EWy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWy",
            "<TMessageType;*>;)V"
        }
    .end annotation

    .prologue
    .line 2132229
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2132230
    iget-object v0, p1, LX/EWy;->a:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->c()V

    .line 2132231
    iget-object v0, p1, LX/EWy;->a:LX/EYc;

    move-object v0, v0

    .line 2132232
    iput-object v0, p0, LX/EX1;->extensions:LX/EYc;

    .line 2132233
    return-void
.end method

.method private c(LX/EYP;)V
    .locals 2

    .prologue
    .line 2132225
    iget-object v0, p1, LX/EYP;->h:LX/EYF;

    move-object v0, v0

    .line 2132226
    invoke-virtual {p0}, LX/EWp;->e()LX/EYF;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2132227
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FieldDescriptor does not match message type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2132228
    :cond_0
    return-void
.end method


# virtual methods
.method public final E()V
    .locals 1

    .prologue
    .line 2132223
    iget-object v0, p0, LX/EX1;->extensions:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->c()V

    .line 2132224
    return-void
.end method

.method public final F()Z
    .locals 1

    .prologue
    .line 2132221
    iget-object v0, p0, LX/EX1;->extensions:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->h()Z

    move-result v0

    return v0
.end method

.method public final G()LX/EYf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/EX1",
            "<TMessageType;>.ExtensionWriter;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2132222
    new-instance v0, LX/EYf;

    invoke-direct {v0, p0, v1}, LX/EYf;-><init>(LX/EX1;Z)V

    return-object v0
.end method

.method public final H()I
    .locals 1

    .prologue
    .line 2132237
    iget-object v0, p0, LX/EX1;->extensions:LX/EYc;

    invoke-virtual {v0}, LX/EYc;->i()I

    move-result v0

    return v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 2132202
    invoke-super {p0}, LX/EWp;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/EX1;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/EWd;LX/EZM;LX/EYZ;I)Z
    .locals 7

    .prologue
    .line 2132203
    invoke-virtual {p0}, LX/EWp;->e()LX/EYF;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, LX/EX1;->extensions:LX/EYc;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, LX/EWV;->a(LX/EWd;LX/EZM;LX/EYZ;LX/EYF;LX/EWU;LX/EYc;I)Z

    move-result v0

    return v0
.end method

.method public final a(LX/EYP;)Z
    .locals 1

    .prologue
    .line 2132204
    invoke-virtual {p1}, LX/EYP;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2132205
    invoke-direct {p0, p1}, LX/EX1;->c(LX/EYP;)V

    .line 2132206
    iget-object v0, p0, LX/EX1;->extensions:LX/EYc;

    invoke-virtual {v0, p1}, LX/EYc;->a(LX/EYP;)Z

    move-result v0

    .line 2132207
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/EWp;->a(LX/EYP;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/EYP;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2132208
    invoke-virtual {p1}, LX/EYP;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2132209
    invoke-direct {p0, p1}, LX/EX1;->c(LX/EYP;)V

    .line 2132210
    iget-object v0, p0, LX/EX1;->extensions:LX/EYc;

    invoke-virtual {v0, p1}, LX/EYc;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    .line 2132211
    if-nez v0, :cond_0

    .line 2132212
    invoke-virtual {p1}, LX/EYP;->f()LX/EYN;

    move-result-object v0

    sget-object v1, LX/EYN;->MESSAGE:LX/EYN;

    if-ne v0, v1, :cond_1

    .line 2132213
    invoke-virtual {p1}, LX/EYP;->t()LX/EYF;

    move-result-object v0

    invoke-static {v0}, LX/EYW;->a(LX/EYF;)LX/EYW;

    move-result-object v0

    .line 2132214
    :cond_0
    :goto_0
    return-object v0

    .line 2132215
    :cond_1
    invoke-virtual {p1}, LX/EYP;->p()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2132216
    :cond_2
    invoke-super {p0, p1}, LX/EWp;->b(LX/EYP;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final kb_()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/EYP;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2132217
    invoke-static {p0}, LX/EWp;->j(LX/EWp;)Ljava/util/Map;

    move-result-object v0

    move-object v0, v0

    .line 2132218
    iget-object v1, p0, LX/EX1;->extensions:LX/EYc;

    invoke-virtual {v1}, LX/EYc;->f()Ljava/util/Map;

    move-result-object v1

    move-object v1, v1

    .line 2132219
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2132220
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
