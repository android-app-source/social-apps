.class public final LX/EgL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/Collection",
        "<",
        "Lcom/facebook/bookmark/model/Bookmark;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/bookmark/client/BookmarkClient;


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/client/BookmarkClient;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2156030
    iput-object p1, p0, LX/EgL;->b:Lcom/facebook/bookmark/client/BookmarkClient;

    iput-object p2, p0, LX/EgL;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2156031
    iget-object v1, p0, LX/EgL;->b:Lcom/facebook/bookmark/client/BookmarkClient;

    monitor-enter v1

    .line 2156032
    :try_start_0
    iget-object v0, p0, LX/EgL;->b:Lcom/facebook/bookmark/client/BookmarkClient;

    iget-object v0, v0, Lcom/facebook/bookmark/client/BookmarkClient;->n:Ljava/util/Map;

    iget-object v2, p0, LX/EgL;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2156033
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2156034
    iget-object v0, p0, LX/EgL;->b:Lcom/facebook/bookmark/client/BookmarkClient;

    const-string v1, "loadHiddenBookmarksFromDB"

    invoke-static {v0, p1, v1}, Lcom/facebook/bookmark/client/BookmarkClient;->a$redex0(Lcom/facebook/bookmark/client/BookmarkClient;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2156035
    return-void

    .line 2156036
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2156037
    iget-object v1, p0, LX/EgL;->b:Lcom/facebook/bookmark/client/BookmarkClient;

    monitor-enter v1

    .line 2156038
    :try_start_0
    iget-object v0, p0, LX/EgL;->b:Lcom/facebook/bookmark/client/BookmarkClient;

    iget-object v0, v0, Lcom/facebook/bookmark/client/BookmarkClient;->n:Ljava/util/Map;

    iget-object v2, p0, LX/EgL;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2156039
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
