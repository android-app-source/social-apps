.class public LX/E5o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/E5o;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2078951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2078952
    return-void
.end method

.method public static a(LX/0QB;)LX/E5o;
    .locals 3

    .prologue
    .line 2078953
    sget-object v0, LX/E5o;->a:LX/E5o;

    if-nez v0, :cond_1

    .line 2078954
    const-class v1, LX/E5o;

    monitor-enter v1

    .line 2078955
    :try_start_0
    sget-object v0, LX/E5o;->a:LX/E5o;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2078956
    if-eqz v2, :cond_0

    .line 2078957
    :try_start_1
    new-instance v0, LX/E5o;

    invoke-direct {v0}, LX/E5o;-><init>()V

    .line 2078958
    move-object v0, v0

    .line 2078959
    sput-object v0, LX/E5o;->a:LX/E5o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2078960
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2078961
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2078962
    :cond_1
    sget-object v0, LX/E5o;->a:LX/E5o;

    return-object v0

    .line 2078963
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2078964
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
