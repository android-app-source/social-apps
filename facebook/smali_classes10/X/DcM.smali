.class public final LX/DcM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;

.field public final synthetic b:LX/DcN;


# direct methods
.method public constructor <init>(LX/DcN;Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;)V
    .locals 0

    .prologue
    .line 2018286
    iput-object p1, p0, LX/DcM;->b:LX/DcN;

    iput-object p2, p0, LX/DcM;->a:Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2018282
    iget-object v0, p0, LX/DcM;->a:Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;

    .line 2018283
    iget-object p0, v0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const p1, 0x7f0828d7

    invoke-virtual {p0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 2018284
    iget-object p0, v0, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2018285
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2018262
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2018263
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v1, v0

    .line 2018264
    if-eqz p1, :cond_2

    .line 2018265
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2018266
    if-eqz v0, :cond_2

    .line 2018267
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2018268
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2018269
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2018270
    check-cast v0, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel;

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel;->a()Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel;->a()LX/0Px;

    move-result-object v0

    .line 2018271
    :goto_0
    iget-object v1, p0, LX/DcM;->a:Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;

    .line 2018272
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const v3, 0x7f0828d9

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 2018273
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2018274
    iget-object v2, v1, Lcom/facebook/localcontent/menus/structured/StructuredMenuListFragment;->a:LX/DcZ;

    .line 2018275
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p0

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, p0, :cond_1

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel;

    .line 2018276
    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel;->j()Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel$ProductMenuItemsModel;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel;->j()Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel$ProductMenuItemsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel$ProductMenuItemsModel;->a()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    .line 2018277
    new-instance p1, LX/Dca;

    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel;->j()Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel$ProductMenuItemsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/localcontent/protocol/graphql/FetchStructuredMenuListModels$StructuredMenuListDataModel$MenuSubListModel$NodesModel$ProductMenuItemsModel;->a()LX/0Px;

    move-result-object v3

    invoke-direct {p1, v1, v3}, LX/Dca;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2018278
    iget-object v3, v2, LX/DcZ;->c:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2018279
    :cond_0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2018280
    :cond_1
    const v3, 0x5c94a716

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2018281
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
