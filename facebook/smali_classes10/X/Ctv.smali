.class public LX/Ctv;
.super LX/Cts;
.source ""

# interfaces
.implements LX/Cov;
.implements LX/20T;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "LX/Cov;",
        "LX/20T;"
    }
.end annotation


# static fields
.field private static final c:LX/0wT;

.field private static final d:LX/0wT;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

.field public final f:LX/215;

.field private final g:LX/0wd;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1945796
    sget-wide v0, LX/CoL;->J:D

    sget-wide v2, LX/CoL;->K:D

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/Ctv;->c:LX/0wT;

    .line 1945797
    sget-wide v0, LX/CoL;->F:D

    sget-wide v2, LX/CoL;->G:D

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/Ctv;->d:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/Ctg;Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1945777
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 1945778
    const-class v0, LX/Ctv;

    invoke-static {v0, p0}, LX/Ctv;->a(Ljava/lang/Class;LX/02k;)V

    .line 1945779
    iget-object v0, p0, LX/Ctv;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    iput-object v0, p0, LX/Ctv;->f:LX/215;

    .line 1945780
    iget-object v0, p0, LX/Ctv;->f:LX/215;

    invoke-virtual {v0, p0}, LX/215;->a(LX/20T;)V

    .line 1945781
    iget-object v0, p0, LX/Ctv;->f:LX/215;

    .line 1945782
    iput-boolean v4, v0, LX/215;->d:Z

    .line 1945783
    iget-object v0, p0, LX/Ctv;->f:LX/215;

    const v1, 0x3f666666    # 0.9f

    .line 1945784
    iput v1, v0, LX/215;->b:F

    .line 1945785
    iget-object v0, p0, LX/Ctv;->f:LX/215;

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1945786
    iput v1, v0, LX/215;->c:F

    .line 1945787
    iget-object v0, p0, LX/Ctv;->f:LX/215;

    sget-object v1, LX/Ctv;->c:LX/0wT;

    invoke-virtual {v0, v1}, LX/215;->a(LX/0wT;)V

    .line 1945788
    iget-object v0, p0, LX/Ctv;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/Ctv;->d:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    .line 1945789
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 1945790
    move-object v0, v0

    .line 1945791
    iput-object v0, p0, LX/Ctv;->g:LX/0wd;

    .line 1945792
    iget-object v0, p0, LX/Ctv;->g:LX/0wd;

    new-instance v1, LX/Ctt;

    invoke-direct {v1, p0}, LX/Ctt;-><init>(LX/Ctv;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1945793
    iput-object p2, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    .line 1945794
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    new-instance v1, LX/Ctu;

    invoke-direct {v1, p0}, LX/Ctu;-><init>(LX/Ctv;)V

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1945795
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Ctv;

    const/16 p0, 0x13a4

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    iput-object p0, p1, LX/Ctv;->a:LX/0Or;

    iput-object v1, p1, LX/Ctv;->b:LX/0wW;

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 1945774
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->setScaleX(F)V

    .line 1945775
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->setScaleY(F)V

    .line 1945776
    return-void
.end method

.method public final a(LX/CrS;)V
    .locals 4

    .prologue
    .line 1945798
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    if-nez v0, :cond_0

    .line 1945799
    :goto_0
    return-void

    .line 1945800
    :cond_0
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->jk_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1945801
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->setVisibility(I)V

    goto :goto_0

    .line 1945802
    :cond_1
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->setVisibility(I)V

    .line 1945803
    iget-object v0, p0, LX/Ctv;->g:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1945804
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->setAlpha(F)V

    .line 1945805
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-virtual {v0}, LX/CsC;->a()Z

    goto :goto_0
.end method

.method public final a(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V
    .locals 4

    .prologue
    .line 1945771
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-virtual {v0}, LX/CsC;->e()V

    .line 1945772
    iget-object v0, p0, LX/Ctv;->g:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1945773
    return-void
.end method

.method public final b(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V
    .locals 4

    .prologue
    .line 1945768
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-virtual {v0}, LX/CsC;->e()V

    .line 1945769
    iget-object v0, p0, LX/Ctv;->g:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1945770
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1945766
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-virtual {v0}, LX/CsC;->e()V

    .line 1945767
    return-void
.end method

.method public final isPressed()Z
    .locals 1

    .prologue
    .line 1945765
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->isPressed()Z

    move-result v0

    return v0
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 1945763
    iget-object v0, p0, LX/Ctv;->e:Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;

    invoke-virtual {v0}, LX/CsC;->b()V

    .line 1945764
    const/4 v0, 0x0

    return v0
.end method
