.class public final LX/ECn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3O4;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/2EJ;

.field public final synthetic d:LX/3LH;

.field public final synthetic e:LX/ECp;


# direct methods
.method public constructor <init>(LX/ECp;ILandroid/content/Context;LX/2EJ;LX/3LH;)V
    .locals 0

    .prologue
    .line 2090359
    iput-object p1, p0, LX/ECn;->e:LX/ECp;

    iput p2, p0, LX/ECn;->a:I

    iput-object p3, p0, LX/ECn;->b:Landroid/content/Context;

    iput-object p4, p0, LX/ECn;->c:LX/2EJ;

    iput-object p5, p0, LX/ECn;->d:LX/3LH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/3OQ;I)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2090360
    check-cast p1, LX/3OO;

    .line 2090361
    invoke-virtual {p1}, LX/3OP;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2090362
    invoke-virtual {p1, v1}, LX/3OP;->a(Z)V

    .line 2090363
    iget-object v2, p0, LX/ECn;->e:LX/ECp;

    .line 2090364
    iget v3, v2, LX/ECp;->g:I

    add-int/lit8 v4, v3, -0x1

    iput v4, v2, LX/ECp;->g:I

    .line 2090365
    :goto_0
    iget-object v2, p0, LX/ECn;->c:LX/2EJ;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v2

    iget-object v3, p0, LX/ECn;->e:LX/ECp;

    iget v3, v3, LX/ECp;->g:I

    if-lez v3, :cond_2

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2090366
    iget-object v0, p0, LX/ECn;->d:LX/3LH;

    const v1, 0x26abb56f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2090367
    return-void

    .line 2090368
    :cond_0
    iget-object v2, p0, LX/ECn;->e:LX/ECp;

    iget v2, v2, LX/ECp;->g:I

    iget v3, p0, LX/ECn;->a:I

    if-lt v2, v3, :cond_1

    .line 2090369
    iget-object v2, p0, LX/ECn;->e:LX/ECp;

    iget-object v2, v2, LX/ECp;->e:LX/0kL;

    new-instance v3, LX/27k;

    iget-object v4, p0, LX/ECn;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0032

    iget v6, p0, LX/ECn;->a:I

    new-array v7, v0, [Ljava/lang/Object;

    iget v8, p0, LX/ECn;->a:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v3}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_0

    .line 2090370
    :cond_1
    invoke-virtual {p1, v0}, LX/3OP;->a(Z)V

    .line 2090371
    iget-object v2, p0, LX/ECn;->e:LX/ECp;

    .line 2090372
    iget v3, v2, LX/ECp;->g:I

    add-int/lit8 v4, v3, 0x1

    iput v4, v2, LX/ECp;->g:I

    .line 2090373
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2090374
    goto :goto_1
.end method
