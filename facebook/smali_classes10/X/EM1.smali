.class public final LX/EM1;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/EM2;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CzL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/EM2;


# direct methods
.method public constructor <init>(LX/EM2;)V
    .locals 1

    .prologue
    .line 2110092
    iput-object p1, p0, LX/EM1;->b:LX/EM2;

    .line 2110093
    move-object v0, p1

    .line 2110094
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2110095
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2110096
    const-string v0, "SearchResultsRelatedPagesHScrollCardFooterTitleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2110081
    if-ne p0, p1, :cond_1

    .line 2110082
    :cond_0
    :goto_0
    return v0

    .line 2110083
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2110084
    goto :goto_0

    .line 2110085
    :cond_3
    check-cast p1, LX/EM1;

    .line 2110086
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2110087
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2110088
    if-eq v2, v3, :cond_0

    .line 2110089
    iget-object v2, p0, LX/EM1;->a:LX/CzL;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/EM1;->a:LX/CzL;

    iget-object v3, p1, LX/EM1;->a:LX/CzL;

    invoke-virtual {v2, v3}, LX/CzL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2110090
    goto :goto_0

    .line 2110091
    :cond_4
    iget-object v2, p1, LX/EM1;->a:LX/CzL;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
