.class public final enum LX/ESB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ESB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ESB;

.field public static final enum FETCHED_ALL:LX/ESB;

.field public static final enum FETCHING:LX/ESB;

.field public static final enum IDLE:LX/ESB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2122560
    new-instance v0, LX/ESB;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, LX/ESB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESB;->IDLE:LX/ESB;

    new-instance v0, LX/ESB;

    const-string v1, "FETCHING"

    invoke-direct {v0, v1, v3}, LX/ESB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESB;->FETCHING:LX/ESB;

    new-instance v0, LX/ESB;

    const-string v1, "FETCHED_ALL"

    invoke-direct {v0, v1, v4}, LX/ESB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ESB;->FETCHED_ALL:LX/ESB;

    const/4 v0, 0x3

    new-array v0, v0, [LX/ESB;

    sget-object v1, LX/ESB;->IDLE:LX/ESB;

    aput-object v1, v0, v2

    sget-object v1, LX/ESB;->FETCHING:LX/ESB;

    aput-object v1, v0, v3

    sget-object v1, LX/ESB;->FETCHED_ALL:LX/ESB;

    aput-object v1, v0, v4

    sput-object v0, LX/ESB;->$VALUES:[LX/ESB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2122557
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ESB;
    .locals 1

    .prologue
    .line 2122559
    const-class v0, LX/ESB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ESB;

    return-object v0
.end method

.method public static values()[LX/ESB;
    .locals 1

    .prologue
    .line 2122558
    sget-object v0, LX/ESB;->$VALUES:[LX/ESB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ESB;

    return-object v0
.end method
