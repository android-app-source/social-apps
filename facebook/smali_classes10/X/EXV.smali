.class public final LX/EXV;
.super LX/EWp;
.source ""

# interfaces
.implements LX/EXT;


# static fields
.field public static a:LX/EWZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXV;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/EXV;


# instance fields
.field public bitField0_:I

.field public dependency_:LX/EYv;

.field public enumType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EWv;",
            ">;"
        }
    .end annotation
.end field

.field public extension_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EXL;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field public messageType_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EWr;",
            ">;"
        }
    .end annotation
.end field

.field public name_:Ljava/lang/Object;

.field public options_:LX/EXb;

.field public package_:Ljava/lang/Object;

.field public publicDependency_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public service_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/EXr;",
            ">;"
        }
    .end annotation
.end field

.field public sourceCodeInfo_:LX/EY3;

.field private final unknownFields:LX/EZQ;

.field public weakDependency_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2134123
    new-instance v0, LX/EXS;

    invoke-direct {v0}, LX/EXS;-><init>()V

    sput-object v0, LX/EXV;->a:LX/EWZ;

    .line 2134124
    new-instance v0, LX/EXV;

    invoke-direct {v0}, LX/EXV;-><init>()V

    .line 2134125
    sput-object v0, LX/EXV;->c:LX/EXV;

    invoke-direct {v0}, LX/EXV;->I()V

    .line 2134126
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2133984
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2133985
    iput-byte v0, p0, LX/EXV;->memoizedIsInitialized:B

    .line 2133986
    iput v0, p0, LX/EXV;->memoizedSerializedSize:I

    .line 2133987
    sget-object v0, LX/EZQ;->a:LX/EZQ;

    move-object v0, v0

    .line 2133988
    iput-object v0, p0, LX/EXV;->unknownFields:LX/EZQ;

    return-void
.end method

.method public constructor <init>(LX/EWd;LX/EYZ;)V
    .locals 12

    .prologue
    const/16 v10, 0x40

    const/16 v9, 0x20

    const/16 v8, 0x10

    const/4 v7, 0x4

    const/16 v6, 0x8

    .line 2133989
    invoke-direct {p0}, LX/EWp;-><init>()V

    .line 2133990
    const/4 v0, -0x1

    iput-byte v0, p0, LX/EXV;->memoizedIsInitialized:B

    .line 2133991
    const/4 v0, -0x1

    iput v0, p0, LX/EXV;->memoizedSerializedSize:I

    .line 2133992
    invoke-direct {p0}, LX/EXV;->I()V

    .line 2133993
    const/4 v1, 0x0

    .line 2133994
    invoke-static {}, LX/EZM;->e()LX/EZM;

    move-result-object v4

    .line 2133995
    const/4 v0, 0x0

    move v3, v0

    .line 2133996
    :goto_0
    if-nez v3, :cond_13

    .line 2133997
    :try_start_0
    invoke-virtual {p1}, LX/EWd;->a()I

    move-result v0

    .line 2133998
    sparse-switch v0, :sswitch_data_0

    .line 2133999
    invoke-virtual {p0, p1, v4, p2, v0}, LX/EWp;->a(LX/EWd;LX/EZM;LX/EYZ;I)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 2134000
    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    .line 2134001
    :sswitch_0
    const/4 v0, 0x1

    move v3, v0

    .line 2134002
    goto :goto_0

    .line 2134003
    :sswitch_1
    iget v0, p0, LX/EXV;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/EXV;->bitField0_:I

    .line 2134004
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->name_:Ljava/lang/Object;
    :try_end_0
    .catch LX/EYr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2134005
    :catch_0
    move-exception v0

    .line 2134006
    :goto_1
    :try_start_1
    iput-object p0, v0, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2134007
    move-object v0, v0

    .line 2134008
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2134009
    :catchall_0
    move-exception v0

    :goto_2
    and-int/lit8 v2, v1, 0x4

    if-ne v2, v7, :cond_0

    .line 2134010
    new-instance v2, LX/EZT;

    iget-object v3, p0, LX/EXV;->dependency_:LX/EYv;

    invoke-direct {v2, v3}, LX/EZT;-><init>(LX/EYv;)V

    iput-object v2, p0, LX/EXV;->dependency_:LX/EYv;

    .line 2134011
    :cond_0
    and-int/lit8 v2, v1, 0x20

    if-ne v2, v9, :cond_1

    .line 2134012
    iget-object v2, p0, LX/EXV;->messageType_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EXV;->messageType_:Ljava/util/List;

    .line 2134013
    :cond_1
    and-int/lit8 v2, v1, 0x40

    if-ne v2, v10, :cond_2

    .line 2134014
    iget-object v2, p0, LX/EXV;->enumType_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EXV;->enumType_:Ljava/util/List;

    .line 2134015
    :cond_2
    and-int/lit16 v2, v1, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_3

    .line 2134016
    iget-object v2, p0, LX/EXV;->service_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EXV;->service_:Ljava/util/List;

    .line 2134017
    :cond_3
    and-int/lit16 v2, v1, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_4

    .line 2134018
    iget-object v2, p0, LX/EXV;->extension_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EXV;->extension_:Ljava/util/List;

    .line 2134019
    :cond_4
    and-int/lit8 v2, v1, 0x8

    if-ne v2, v6, :cond_5

    .line 2134020
    iget-object v2, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    .line 2134021
    :cond_5
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v8, :cond_6

    .line 2134022
    iget-object v1, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    .line 2134023
    :cond_6
    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v1

    iput-object v1, p0, LX/EXV;->unknownFields:LX/EZQ;

    .line 2134024
    invoke-virtual {p0}, LX/EWp;->E()V

    throw v0

    .line 2134025
    :sswitch_2
    :try_start_2
    iget v0, p0, LX/EXV;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/EXV;->bitField0_:I

    .line 2134026
    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->package_:Ljava/lang/Object;
    :try_end_2
    .catch LX/EYr; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 2134027
    :catch_1
    move-exception v0

    .line 2134028
    :goto_3
    :try_start_3
    new-instance v2, LX/EYr;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/EYr;-><init>(Ljava/lang/String;)V

    .line 2134029
    iput-object p0, v2, LX/EYr;->unfinishedMessage:LX/EWW;

    .line 2134030
    move-object v0, v2

    .line 2134031
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2134032
    :sswitch_3
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v7, :cond_7

    .line 2134033
    :try_start_4
    new-instance v0, LX/EYw;

    invoke-direct {v0}, LX/EYw;-><init>()V

    iput-object v0, p0, LX/EXV;->dependency_:LX/EYv;

    .line 2134034
    or-int/lit8 v1, v1, 0x4

    .line 2134035
    :cond_7
    iget-object v0, p0, LX/EXV;->dependency_:LX/EYv;

    invoke-virtual {p1}, LX/EWd;->k()LX/EWc;

    move-result-object v2

    invoke-interface {v0, v2}, LX/EYv;->a(LX/EWc;)V

    goto/16 :goto_0

    .line 2134036
    :sswitch_4
    and-int/lit8 v0, v1, 0x20

    if-eq v0, v9, :cond_8

    .line 2134037
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    .line 2134038
    or-int/lit8 v1, v1, 0x20

    .line 2134039
    :cond_8
    iget-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    sget-object v2, LX/EWr;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2134040
    :sswitch_5
    and-int/lit8 v0, v1, 0x40

    if-eq v0, v10, :cond_9

    .line 2134041
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    .line 2134042
    or-int/lit8 v1, v1, 0x40

    .line 2134043
    :cond_9
    iget-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    sget-object v2, LX/EWv;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2134044
    :sswitch_6
    and-int/lit16 v0, v1, 0x80

    const/16 v2, 0x80

    if-eq v0, v2, :cond_a

    .line 2134045
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    .line 2134046
    or-int/lit16 v1, v1, 0x80

    .line 2134047
    :cond_a
    iget-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    sget-object v2, LX/EXr;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2134048
    :sswitch_7
    and-int/lit16 v0, v1, 0x100

    const/16 v2, 0x100

    if-eq v0, v2, :cond_b

    .line 2134049
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    .line 2134050
    or-int/lit16 v1, v1, 0x100

    .line 2134051
    :cond_b
    iget-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    sget-object v2, LX/EXL;->a:LX/EWZ;

    invoke-virtual {p1, v2, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2134052
    :sswitch_8
    const/4 v0, 0x0

    .line 2134053
    iget v2, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v7, :cond_1d

    .line 2134054
    iget-object v0, p0, LX/EXV;->options_:LX/EXb;

    invoke-virtual {v0}, LX/EXb;->D()LX/EXY;

    move-result-object v0

    move-object v2, v0

    .line 2134055
    :goto_4
    sget-object v0, LX/EXb;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EXb;

    iput-object v0, p0, LX/EXV;->options_:LX/EXb;

    .line 2134056
    if-eqz v2, :cond_c

    .line 2134057
    iget-object v0, p0, LX/EXV;->options_:LX/EXb;

    invoke-virtual {v2, v0}, LX/EXY;->a(LX/EXb;)LX/EXY;

    .line 2134058
    invoke-virtual {v2}, LX/EXY;->l()LX/EXb;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->options_:LX/EXb;

    .line 2134059
    :cond_c
    iget v0, p0, LX/EXV;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, LX/EXV;->bitField0_:I

    goto/16 :goto_0

    .line 2134060
    :sswitch_9
    const/4 v0, 0x0

    .line 2134061
    iget v2, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_1c

    .line 2134062
    iget-object v0, p0, LX/EXV;->sourceCodeInfo_:LX/EY3;

    invoke-virtual {v0}, LX/EY3;->j()LX/EXy;

    move-result-object v0

    move-object v2, v0

    .line 2134063
    :goto_5
    sget-object v0, LX/EY3;->a:LX/EWZ;

    invoke-virtual {p1, v0, p2}, LX/EWd;->a(LX/EWZ;LX/EYZ;)LX/EWW;

    move-result-object v0

    check-cast v0, LX/EY3;

    iput-object v0, p0, LX/EXV;->sourceCodeInfo_:LX/EY3;

    .line 2134064
    if-eqz v2, :cond_d

    .line 2134065
    iget-object v0, p0, LX/EXV;->sourceCodeInfo_:LX/EY3;

    invoke-virtual {v2, v0}, LX/EXy;->a(LX/EY3;)LX/EXy;

    .line 2134066
    invoke-virtual {v2}, LX/EXy;->l()LX/EY3;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->sourceCodeInfo_:LX/EY3;

    .line 2134067
    :cond_d
    iget v0, p0, LX/EXV;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, LX/EXV;->bitField0_:I

    goto/16 :goto_0

    .line 2134068
    :sswitch_a
    and-int/lit8 v0, v1, 0x8

    if-eq v0, v6, :cond_e

    .line 2134069
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    .line 2134070
    or-int/lit8 v1, v1, 0x8

    .line 2134071
    :cond_e
    iget-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2134072
    :sswitch_b
    invoke-virtual {p1}, LX/EWd;->r()I

    move-result v0

    .line 2134073
    invoke-virtual {p1, v0}, LX/EWd;->c(I)I

    move-result v0

    .line 2134074
    and-int/lit8 v2, v1, 0x8

    if-eq v2, v6, :cond_f

    invoke-virtual {p1}, LX/EWd;->s()I

    move-result v2

    if-lez v2, :cond_f

    .line 2134075
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    .line 2134076
    or-int/lit8 v1, v1, 0x8

    .line 2134077
    :cond_f
    :goto_6
    invoke-virtual {p1}, LX/EWd;->s()I

    move-result v2

    if-lez v2, :cond_10

    .line 2134078
    iget-object v2, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 2134079
    :cond_10
    invoke-virtual {p1, v0}, LX/EWd;->d(I)V

    goto/16 :goto_0

    .line 2134080
    :sswitch_c
    and-int/lit8 v0, v1, 0x10

    if-eq v0, v8, :cond_11

    .line 2134081
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    .line 2134082
    or-int/lit8 v1, v1, 0x10

    .line 2134083
    :cond_11
    iget-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2134084
    :sswitch_d
    invoke-virtual {p1}, LX/EWd;->r()I

    move-result v0

    .line 2134085
    invoke-virtual {p1, v0}, LX/EWd;->c(I)I

    move-result v2

    .line 2134086
    and-int/lit8 v0, v1, 0x10

    if-eq v0, v8, :cond_1b

    invoke-virtual {p1}, LX/EWd;->s()I

    move-result v0

    if-lez v0, :cond_1b

    .line 2134087
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;
    :try_end_4
    .catch LX/EYr; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2134088
    or-int/lit8 v0, v1, 0x10

    .line 2134089
    :goto_7
    :try_start_5
    invoke-virtual {p1}, LX/EWd;->s()I

    move-result v1

    if-lez v1, :cond_12

    .line 2134090
    iget-object v1, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    invoke-virtual {p1}, LX/EWd;->f()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2134091
    :catch_2
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto/16 :goto_1

    .line 2134092
    :cond_12
    invoke-virtual {p1, v2}, LX/EWd;->d(I)V
    :try_end_5
    .catch LX/EYr; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_8
    move v1, v0

    .line 2134093
    goto/16 :goto_0

    .line 2134094
    :cond_13
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v7, :cond_14

    .line 2134095
    new-instance v0, LX/EZT;

    iget-object v2, p0, LX/EXV;->dependency_:LX/EYv;

    invoke-direct {v0, v2}, LX/EZT;-><init>(LX/EYv;)V

    iput-object v0, p0, LX/EXV;->dependency_:LX/EYv;

    .line 2134096
    :cond_14
    and-int/lit8 v0, v1, 0x20

    if-ne v0, v9, :cond_15

    .line 2134097
    iget-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    .line 2134098
    :cond_15
    and-int/lit8 v0, v1, 0x40

    if-ne v0, v10, :cond_16

    .line 2134099
    iget-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    .line 2134100
    :cond_16
    and-int/lit16 v0, v1, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_17

    .line 2134101
    iget-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    .line 2134102
    :cond_17
    and-int/lit16 v0, v1, 0x100

    const/16 v2, 0x100

    if-ne v0, v2, :cond_18

    .line 2134103
    iget-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    .line 2134104
    :cond_18
    and-int/lit8 v0, v1, 0x8

    if-ne v0, v6, :cond_19

    .line 2134105
    iget-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    .line 2134106
    :cond_19
    and-int/lit8 v0, v1, 0x10

    if-ne v0, v8, :cond_1a

    .line 2134107
    iget-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    .line 2134108
    :cond_1a
    invoke-virtual {v4}, LX/EZM;->b()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->unknownFields:LX/EZQ;

    .line 2134109
    invoke-virtual {p0}, LX/EWp;->E()V

    .line 2134110
    return-void

    .line 2134111
    :catchall_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto/16 :goto_2

    .line 2134112
    :catch_3
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto/16 :goto_3

    :cond_1b
    move v0, v1

    goto/16 :goto_7

    :cond_1c
    move-object v2, v0

    goto/16 :goto_5

    :cond_1d
    move-object v2, v0

    goto/16 :goto_4

    :cond_1e
    move v0, v1

    goto :goto_8

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x52 -> :sswitch_b
        0x58 -> :sswitch_c
        0x5a -> :sswitch_d
    .end sparse-switch
.end method

.method public constructor <init>(LX/EWj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EWj",
            "<*>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 2134113
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/EWp;-><init>(B)V

    .line 2134114
    iput-byte v1, p0, LX/EXV;->memoizedIsInitialized:B

    .line 2134115
    iput v1, p0, LX/EXV;->memoizedSerializedSize:I

    .line 2134116
    invoke-virtual {p1}, LX/EWj;->g()LX/EZQ;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->unknownFields:LX/EZQ;

    .line 2134117
    return-void
.end method

.method private C()LX/EWc;
    .locals 2

    .prologue
    .line 2134118
    iget-object v0, p0, LX/EXV;->name_:Ljava/lang/Object;

    .line 2134119
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2134120
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2134121
    iput-object v0, p0, LX/EXV;->name_:Ljava/lang/Object;

    .line 2134122
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private D()LX/EWc;
    .locals 2

    .prologue
    .line 2134127
    iget-object v0, p0, LX/EXV;->package_:Ljava/lang/Object;

    .line 2134128
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2134129
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/EWc;->a(Ljava/lang/String;)LX/EWc;

    move-result-object v0

    .line 2134130
    iput-object v0, p0, LX/EXV;->package_:Ljava/lang/Object;

    .line 2134131
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/EWc;

    goto :goto_0
.end method

.method private I()V
    .locals 1

    .prologue
    .line 2134132
    const-string v0, ""

    iput-object v0, p0, LX/EXV;->name_:Ljava/lang/Object;

    .line 2134133
    const-string v0, ""

    iput-object v0, p0, LX/EXV;->package_:Ljava/lang/Object;

    .line 2134134
    sget-object v0, LX/EYw;->a:LX/EYv;

    iput-object v0, p0, LX/EXV;->dependency_:LX/EYv;

    .line 2134135
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    .line 2134136
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    .line 2134137
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    .line 2134138
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    .line 2134139
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    .line 2134140
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    .line 2134141
    sget-object v0, LX/EXb;->c:LX/EXb;

    move-object v0, v0

    .line 2134142
    iput-object v0, p0, LX/EXV;->options_:LX/EXb;

    .line 2134143
    sget-object v0, LX/EY3;->c:LX/EY3;

    move-object v0, v0

    .line 2134144
    iput-object v0, p0, LX/EXV;->sourceCodeInfo_:LX/EY3;

    .line 2134145
    return-void
.end method

.method private static j(LX/EXV;)LX/EXU;
    .locals 1

    .prologue
    .line 2133982
    invoke-static {}, LX/EXU;->n()LX/EXU;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/EXU;->a(LX/EXV;)LX/EXU;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/EYd;)LX/EWU;
    .locals 2

    .prologue
    .line 2134146
    new-instance v0, LX/EXU;

    invoke-direct {v0, p1}, LX/EXU;-><init>(LX/EYd;)V

    .line 2134147
    return-object v0
.end method

.method public final a(LX/EWf;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2134148
    invoke-virtual {p0}, LX/EWY;->b()I

    .line 2134149
    iget v0, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 2134150
    invoke-direct {p0}, LX/EXV;->C()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(ILX/EWc;)V

    .line 2134151
    :cond_0
    iget v0, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 2134152
    invoke-direct {p0}, LX/EXV;->D()LX/EWc;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(ILX/EWc;)V

    :cond_1
    move v0, v1

    .line 2134153
    :goto_0
    iget-object v2, p0, LX/EXV;->dependency_:LX/EYv;

    invoke-interface {v2}, LX/EYv;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 2134154
    const/4 v2, 0x3

    iget-object v3, p0, LX/EXV;->dependency_:LX/EYv;

    invoke-interface {v3, v0}, LX/EYv;->a(I)LX/EWc;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, LX/EWf;->a(ILX/EWc;)V

    .line 2134155
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v2, v1

    .line 2134156
    :goto_1
    iget-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2134157
    iget-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v4, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2134158
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v2, v1

    .line 2134159
    :goto_2
    iget-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2134160
    const/4 v3, 0x5

    iget-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2134161
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    move v2, v1

    .line 2134162
    :goto_3
    iget-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 2134163
    const/4 v3, 0x6

    iget-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2134164
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_5
    move v2, v1

    .line 2134165
    :goto_4
    iget-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 2134166
    const/4 v3, 0x7

    iget-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-virtual {p1, v3, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2134167
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2134168
    :cond_6
    iget v0, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_7

    .line 2134169
    iget-object v0, p0, LX/EXV;->options_:LX/EXb;

    invoke-virtual {p1, v5, v0}, LX/EWf;->b(ILX/EWW;)V

    .line 2134170
    :cond_7
    iget v0, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_8

    .line 2134171
    const/16 v0, 0x9

    iget-object v2, p0, LX/EXV;->sourceCodeInfo_:LX/EY3;

    invoke-virtual {p1, v0, v2}, LX/EWf;->b(ILX/EWW;)V

    :cond_8
    move v2, v1

    .line 2134172
    :goto_5
    iget-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 2134173
    const/16 v3, 0xa

    iget-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v3, v0}, LX/EWf;->a(II)V

    .line 2134174
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 2134175
    :cond_9
    :goto_6
    iget-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 2134176
    const/16 v2, 0xb

    iget-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, LX/EWf;->a(II)V

    .line 2134177
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 2134178
    :cond_a
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/EZQ;->a(LX/EWf;)V

    .line 2134179
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2134180
    iget-byte v0, p0, LX/EXV;->memoizedIsInitialized:B

    .line 2134181
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2134182
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 2134183
    :goto_1
    invoke-virtual {p0}, LX/EXV;->p()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 2134184
    invoke-virtual {p0, v0}, LX/EXV;->c(I)LX/EWr;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2134185
    iput-byte v1, p0, LX/EXV;->memoizedIsInitialized:B

    goto :goto_0

    .line 2134186
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2134187
    :goto_2
    invoke-virtual {p0}, LX/EXV;->q()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 2134188
    invoke-virtual {p0, v0}, LX/EXV;->d(I)LX/EWv;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2134189
    iput-byte v1, p0, LX/EXV;->memoizedIsInitialized:B

    goto :goto_0

    .line 2134190
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v1

    .line 2134191
    :goto_3
    invoke-virtual {p0}, LX/EXV;->r()I

    move-result v3

    if-ge v0, v3, :cond_7

    .line 2134192
    invoke-virtual {p0, v0}, LX/EXV;->e(I)LX/EXr;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_6

    .line 2134193
    iput-byte v1, p0, LX/EXV;->memoizedIsInitialized:B

    goto :goto_0

    .line 2134194
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    move v0, v1

    .line 2134195
    :goto_4
    invoke-virtual {p0}, LX/EXV;->w()I

    move-result v3

    if-ge v0, v3, :cond_9

    .line 2134196
    invoke-virtual {p0, v0}, LX/EXV;->f(I)LX/EXL;

    move-result-object v3

    invoke-virtual {v3}, LX/EWY;->a()Z

    move-result v3

    if-nez v3, :cond_8

    .line 2134197
    iput-byte v1, p0, LX/EXV;->memoizedIsInitialized:B

    goto :goto_0

    .line 2134198
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2134199
    :cond_9
    invoke-virtual {p0}, LX/EXV;->x()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2134200
    iget-object v0, p0, LX/EXV;->options_:LX/EXb;

    move-object v0, v0

    .line 2134201
    invoke-virtual {v0}, LX/EWY;->a()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2134202
    iput-byte v1, p0, LX/EXV;->memoizedIsInitialized:B

    goto :goto_0

    .line 2134203
    :cond_a
    iput-byte v2, p0, LX/EXV;->memoizedIsInitialized:B

    move v1, v2

    .line 2134204
    goto :goto_0
.end method

.method public final b()I
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2134205
    iget v0, p0, LX/EXV;->memoizedSerializedSize:I

    .line 2134206
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 2134207
    :goto_0
    return v0

    .line 2134208
    :cond_0
    iget v0, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_b

    .line 2134209
    invoke-direct {p0}, LX/EXV;->C()LX/EWc;

    move-result-object v0

    invoke-static {v3, v0}, LX/EWf;->c(ILX/EWc;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2134210
    :goto_1
    iget v2, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_1

    .line 2134211
    invoke-direct {p0}, LX/EXV;->D()LX/EWc;

    move-result-object v2

    invoke-static {v4, v2}, LX/EWf;->c(ILX/EWc;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    move v2, v1

    move v3, v1

    .line 2134212
    :goto_2
    iget-object v4, p0, LX/EXV;->dependency_:LX/EYv;

    invoke-interface {v4}, LX/EYv;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 2134213
    iget-object v4, p0, LX/EXV;->dependency_:LX/EYv;

    invoke-interface {v4, v2}, LX/EYv;->a(I)LX/EWc;

    move-result-object v4

    invoke-static {v4}, LX/EWf;->b(LX/EWc;)I

    move-result v4

    add-int/2addr v3, v4

    .line 2134214
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2134215
    :cond_2
    add-int/2addr v0, v3

    .line 2134216
    iget-object v2, p0, LX/EXV;->dependency_:LX/EYv;

    move-object v2, v2

    .line 2134217
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    move v2, v1

    move v3, v0

    .line 2134218
    :goto_3
    iget-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2134219
    iget-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v5, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2134220
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_3
    move v2, v1

    .line 2134221
    :goto_4
    iget-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2134222
    const/4 v4, 0x5

    iget-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v4, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2134223
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_4
    move v2, v1

    .line 2134224
    :goto_5
    iget-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 2134225
    const/4 v4, 0x6

    iget-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v4, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2134226
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_5
    move v2, v1

    .line 2134227
    :goto_6
    iget-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 2134228
    const/4 v4, 0x7

    iget-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWW;

    invoke-static {v4, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2134229
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 2134230
    :cond_6
    iget v0, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_7

    .line 2134231
    iget-object v0, p0, LX/EXV;->options_:LX/EXb;

    invoke-static {v6, v0}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    .line 2134232
    :cond_7
    iget v0, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v6, :cond_8

    .line 2134233
    const/16 v0, 0x9

    iget-object v2, p0, LX/EXV;->sourceCodeInfo_:LX/EY3;

    invoke-static {v0, v2}, LX/EWf;->e(ILX/EWW;)I

    move-result v0

    add-int/2addr v3, v0

    :cond_8
    move v2, v1

    move v4, v1

    .line 2134234
    :goto_7
    iget-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 2134235
    iget-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/EWf;->g(I)I

    move-result v0

    add-int/2addr v4, v0

    .line 2134236
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 2134237
    :cond_9
    add-int v0, v3, v4

    .line 2134238
    iget-object v2, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    move-object v2, v2

    .line 2134239
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    move v2, v1

    .line 2134240
    :goto_8
    iget-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 2134241
    iget-object v0, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/EWf;->g(I)I

    move-result v0

    add-int/2addr v0, v2

    .line 2134242
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_8

    .line 2134243
    :cond_a
    add-int v0, v3, v2

    .line 2134244
    iget-object v1, p0, LX/EXV;->weakDependency_:Ljava/util/List;

    move-object v1, v1

    .line 2134245
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2134246
    invoke-virtual {p0}, LX/EWp;->g()LX/EZQ;

    move-result-object v1

    invoke-virtual {v1}, LX/EZQ;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 2134247
    iput v0, p0, LX/EXV;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 2134248
    iget-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final c(I)LX/EWr;
    .locals 1

    .prologue
    .line 2134249
    iget-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWr;

    return-object v0
.end method

.method public final d(I)LX/EWv;
    .locals 1

    .prologue
    .line 2134250
    iget-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EWv;

    return-object v0
.end method

.method public final e(I)LX/EXr;
    .locals 1

    .prologue
    .line 2133983
    iget-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXr;

    return-object v0
.end method

.method public final f(I)LX/EXL;
    .locals 1

    .prologue
    .line 2133958
    iget-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EXL;

    return-object v0
.end method

.method public final g()LX/EZQ;
    .locals 1

    .prologue
    .line 2133959
    iget-object v0, p0, LX/EXV;->unknownFields:LX/EZQ;

    return-object v0
.end method

.method public final h()LX/EYn;
    .locals 3

    .prologue
    .line 2133960
    sget-object v0, LX/EYC;->d:LX/EYn;

    const-class v1, LX/EXV;

    const-class v2, LX/EXU;

    invoke-virtual {v0, v1, v2}, LX/EYn;->a(Ljava/lang/Class;Ljava/lang/Class;)LX/EYn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/EWZ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Parser",
            "<",
            "LX/EXV;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2133961
    sget-object v0, LX/EXV;->a:LX/EWZ;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2133962
    iget-object v0, p0, LX/EXV;->name_:Ljava/lang/Object;

    .line 2133963
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2133964
    check-cast v0, Ljava/lang/String;

    .line 2133965
    :goto_0
    return-object v0

    .line 2133966
    :cond_0
    check-cast v0, LX/EWc;

    .line 2133967
    invoke-virtual {v0}, LX/EWc;->e()Ljava/lang/String;

    move-result-object v1

    .line 2133968
    invoke-virtual {v0}, LX/EWc;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2133969
    iput-object v1, p0, LX/EXV;->name_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 2133970
    goto :goto_0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2133971
    iget-object v0, p0, LX/EXV;->dependency_:LX/EYv;

    invoke-interface {v0}, LX/EYv;->size()I

    move-result v0

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 2133972
    iget-object v0, p0, LX/EXV;->publicDependency_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 2133973
    iget-object v0, p0, LX/EXV;->messageType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 2133974
    iget-object v0, p0, LX/EXV;->enumType_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 2133975
    iget-object v0, p0, LX/EXV;->service_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic s()LX/EWU;
    .locals 1

    .prologue
    .line 2133976
    invoke-static {p0}, LX/EXV;->j(LX/EXV;)LX/EXU;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/EWU;
    .locals 1

    .prologue
    .line 2133981
    invoke-static {}, LX/EXU;->n()LX/EXU;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic u()LX/EWR;
    .locals 1

    .prologue
    .line 2133977
    invoke-static {p0}, LX/EXV;->j(LX/EXV;)LX/EXU;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/EWY;
    .locals 1

    .prologue
    .line 2133978
    sget-object v0, LX/EXV;->c:LX/EXV;

    return-object v0
.end method

.method public final w()I
    .locals 1

    .prologue
    .line 2133979
    iget-object v0, p0, LX/EXV;->extension_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 2133980
    iget v0, p0, LX/EXV;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
