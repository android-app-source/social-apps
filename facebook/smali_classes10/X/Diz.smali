.class public final LX/Diz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;)V
    .locals 0

    .prologue
    .line 2032756
    iput-object p1, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    .line 2032757
    iget-object v0, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;Z)V

    .line 2032758
    iget-object v0, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    const v1, 0x7f08003a

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;I)V

    .line 2032759
    iget-object v0, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    .line 2032760
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2032761
    const-string v1, "thread_booking_requests"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2032762
    iget-object v1, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->d:LX/Dih;

    iget-object v2, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v2, v2, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->l:LX/DkQ;

    invoke-virtual {v2}, LX/DkQ;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v3, v3, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->c:Ljava/lang/String;

    .line 2032763
    iget-object v4, v1, LX/Dih;->a:LX/0Zb;

    const-string v5, "profservices_booking_error"

    const/4 v6, 0x0

    invoke-static {v5, v6}, LX/Dih;->i(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "error_category"

    const-string v7, "load_appointment_list"

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "query_config"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "viewer"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "thread_booking_requests"

    invoke-virtual {v5, v6, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2032764
    iget-object v1, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->e:LX/03V;

    const-string v2, "load_appointment_list"

    const-string v3, "%s,%s,viewer:%s,threadBookingRequests%s"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v5, v5, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->l:LX/DkQ;

    invoke-virtual {v5}, LX/DkQ;->f()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/Diz;->a:Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;

    iget-object v6, v6, Lcom/facebook/messaging/professionalservices/booking/fragments/AppointmentsListFragment;->c:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2032765
    return-void
.end method
