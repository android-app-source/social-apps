.class public LX/DgD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6ZZ;


# instance fields
.field public a:LX/0y3;

.field public b:LX/0ad;

.field public c:LX/6Zb;

.field public d:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;


# direct methods
.method public constructor <init>(LX/0y3;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2029281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2029282
    iput-object p1, p0, LX/DgD;->a:LX/0y3;

    .line 2029283
    iput-object p2, p0, LX/DgD;->b:LX/0ad;

    .line 2029284
    return-void
.end method


# virtual methods
.method public final a(LX/6ZY;)V
    .locals 2

    .prologue
    .line 2029285
    sget-object v0, LX/DgC;->a:[I

    invoke-virtual {p1}, LX/6ZY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2029286
    :goto_0
    return-void

    .line 2029287
    :pswitch_0
    iget-object v0, p0, LX/DgD;->d:Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;

    .line 2029288
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->g:Lcom/facebook/messaging/location/sending/MapDisplayFragment;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Lcom/facebook/messaging/location/sending/MapDisplayFragment;->a(Z)V

    .line 2029289
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->l:Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;

    if-eqz v1, :cond_0

    .line 2029290
    iget-object v1, v0, Lcom/facebook/messaging/location/sending/LocationSendingMainFragment;->l:Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;

    invoke-virtual {v1}, Lcom/facebook/messaging/location/sending/SuggestedNearbyPlacesFragment;->b()V

    .line 2029291
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
