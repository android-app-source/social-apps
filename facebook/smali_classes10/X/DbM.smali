.class public LX/DbM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2016998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2016999
    iput-object p1, p0, LX/DbM;->a:LX/0Zb;

    .line 2017000
    return-void
.end method

.method public static b(LX/0QB;)LX/DbM;
    .locals 2

    .prologue
    .line 2016996
    new-instance v1, LX/DbM;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/DbM;-><init>(LX/0Zb;)V

    .line 2016997
    return-object v1
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2016989
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2016990
    iput-object p0, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2016991
    move-object v0, v0

    .line 2016992
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/DbM;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2016994
    iget-object v0, p0, LX/DbM;->a:LX/0Zb;

    invoke-static {p1, p2}, LX/DbM;->e(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2016995
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 2016993
    const-string v0, "upload_photo_menu"

    invoke-static {v0, p0, p1}, LX/DbM;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method
