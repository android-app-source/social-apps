.class public LX/Dvn;
.super LX/Dvk;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DwH;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvr;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dvr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DwH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dvs;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2059235
    invoke-direct {p0}, LX/Dvk;-><init>()V

    .line 2059236
    iput-object p2, p0, LX/Dvn;->a:LX/0Ot;

    .line 2059237
    iput-object p1, p0, LX/Dvn;->b:LX/0Ot;

    .line 2059238
    iput-object p3, p0, LX/Dvn;->c:LX/0Ot;

    .line 2059239
    return-void
.end method

.method private a(LX/Dvm;Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 2059187
    iget-object v0, p0, LX/Dvn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DwH;

    invoke-virtual {v0, p3}, LX/DwH;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    .line 2059188
    if-nez v0, :cond_0

    .line 2059189
    :goto_0
    return-void

    .line 2059190
    :cond_0
    new-instance v1, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;

    iget-object v2, p0, LX/Dvn;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/Dvs;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)D

    move-result-wide v2

    invoke-direct {v1, p2, v0, v2, v3}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;-><init>(Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;Lcom/facebook/graphql/model/GraphQLPhoto;D)V

    .line 2059191
    invoke-static {p0, p1}, LX/Dvn;->d(LX/Dvn;LX/Dvm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static d(LX/Dvn;LX/Dvm;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dvm;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2059226
    iget-object v0, p0, LX/Dvn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2059227
    if-nez p1, :cond_0

    .line 2059228
    const/4 v0, 0x0

    .line 2059229
    :goto_0
    move-object v0, v0

    .line 2059230
    return-object v0

    .line 2059231
    :cond_0
    sget-object v0, LX/Dvr;->a:LX/0aq;

    invoke-virtual {v0, p1}, LX/0aq;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2059232
    if-nez v0, :cond_1

    .line 2059233
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2059234
    :cond_1
    sget-object p0, LX/Dvr;->a:LX/0aq;

    invoke-virtual {p0, p1, v0}, LX/0aq;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Dvm;Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dvm;",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2059198
    invoke-virtual {p0, p2}, LX/Dvk;->a(Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2059199
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2059200
    :goto_0
    return-object v0

    .line 2059201
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;->a()LX/Dv3;

    move-result-object v0

    sget-object v2, LX/Dv3;->SINGLE_PHOTO_STORY:LX/Dv3;

    if-ne v0, v2, :cond_1

    .line 2059202
    check-cast p2, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoStoryModel;

    .line 2059203
    iget-object v0, p2, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoStoryModel;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, p1, p2, v0}, LX/Dvn;->a(LX/Dvm;Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2059204
    invoke-virtual {p0, p1}, LX/Dvn;->b(LX/Dvm;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2059205
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;->a()LX/Dv3;

    move-result-object v0

    sget-object v2, LX/Dv3;->ALBUM_POST_SECTION:LX/Dv3;

    if-ne v0, v2, :cond_3

    .line 2059206
    check-cast p2, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;

    .line 2059207
    iget-object v2, p2, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2059208
    invoke-direct {p0, p1, p2, v0}, LX/Dvn;->a(LX/Dvm;Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2059209
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2059210
    :cond_2
    invoke-virtual {p0, p1}, LX/Dvn;->b(LX/Dvm;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2059211
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;->a()LX/Dv3;

    move-result-object v0

    sget-object v2, LX/Dv3;->SINGLE_PHOTO:LX/Dv3;

    if-ne v0, v2, :cond_4

    .line 2059212
    check-cast p2, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoModel;

    .line 2059213
    iget-object v8, p2, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoModel;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2059214
    if-nez v8, :cond_6

    .line 2059215
    :goto_2
    invoke-virtual {p0, p1}, LX/Dvn;->b(LX/Dvm;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2059216
    :cond_4
    invoke-static {p0, p1}, LX/Dvn;->d(LX/Dvn;LX/Dvm;)Ljava/util/List;

    move-result-object v2

    .line 2059217
    check-cast p2, Lcom/facebook/photos/pandora/common/data/model/PandoraMultiPhotoStoryModel;

    .line 2059218
    iget-object v0, p0, LX/Dvn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DwH;

    iget-object v3, p2, Lcom/facebook/photos/pandora/common/data/model/PandoraMultiPhotoStoryModel;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v3}, LX/DwH;->b(Lcom/facebook/graphql/model/GraphQLStory;)LX/0Px;

    move-result-object v3

    .line 2059219
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_3
    if-ge v1, v4, :cond_5

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2059220
    new-instance v5, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;

    iget-object v6, p0, LX/Dvn;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/Dvs;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)D

    move-result-wide v6

    invoke-direct {v5, p2, v0, v6, v7}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;-><init>(Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;Lcom/facebook/graphql/model/GraphQLPhoto;D)V

    .line 2059221
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2059222
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2059223
    :cond_5
    invoke-virtual {p0, p1}, LX/Dvn;->b(LX/Dvm;)LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 2059224
    :cond_6
    new-instance v9, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;

    iget-object v10, p0, LX/Dvn;->c:LX/0Ot;

    invoke-interface {v10}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v8}, LX/Dvs;->a(Lcom/facebook/graphql/model/GraphQLPhoto;)D

    move-result-wide v10

    invoke-direct {v9, p2, v8, v10, v11}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;-><init>(Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;Lcom/facebook/graphql/model/GraphQLPhoto;D)V

    .line 2059225
    invoke-static {p0, p1}, LX/Dvn;->d(LX/Dvn;LX/Dvm;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public final a(LX/Dvm;Z)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dvm;",
            "Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2059240
    invoke-static {p0, p1}, LX/Dvn;->d(LX/Dvn;LX/Dvm;)Ljava/util/List;

    move-result-object v5

    .line 2059241
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2059242
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2059243
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    move v3, v2

    move-object v4, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;

    .line 2059244
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2059245
    add-int/lit8 v3, v3, 0x1

    .line 2059246
    rem-int/lit8 v0, v3, 0x3

    if-nez v0, :cond_3

    .line 2059247
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;-><init>(LX/0Px;)V

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2059248
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2059249
    add-int/lit8 v0, v1, 0x3

    move-object v1, v4

    :goto_1
    move-object v4, v1

    move v1, v0

    .line 2059250
    goto :goto_0

    .line 2059251
    :cond_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    move v0, v2

    .line 2059252
    :goto_2
    if-ge v0, v1, :cond_1

    if-ge v0, v3, :cond_1

    .line 2059253
    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2059254
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2059255
    :cond_1
    if-eqz p2, :cond_2

    .line 2059256
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;-><init>(LX/0Px;)V

    .line 2059257
    iget-object v1, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2059258
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2059259
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    move-object v1, v4

    goto :goto_1
.end method

.method public final a(LX/Dvm;)Z
    .locals 1

    .prologue
    .line 2059197
    invoke-static {p0, p1}, LX/Dvn;->d(LX/Dvn;LX/Dvm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;)Z
    .locals 1

    .prologue
    .line 2059196
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/Dvm;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Dvm;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2059192
    invoke-static {p0, p1}, LX/Dvn;->d(LX/Dvn;LX/Dvm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2059193
    if-nez v0, :cond_0

    .line 2059194
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2059195
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Dvn;->a(LX/Dvm;Z)LX/0Px;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
