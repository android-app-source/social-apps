.class public final LX/DDA;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/DDC;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/DDZ;

.field public b:LX/DDY;

.field public c:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/DDC;


# direct methods
.method public constructor <init>(LX/DDC;)V
    .locals 1

    .prologue
    .line 1975032
    iput-object p1, p0, LX/DDA;->d:LX/DDC;

    .line 1975033
    move-object v0, p1

    .line 1975034
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1975035
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1975036
    const-string v0, "ActionButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1975037
    if-ne p0, p1, :cond_1

    .line 1975038
    :cond_0
    :goto_0
    return v0

    .line 1975039
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1975040
    goto :goto_0

    .line 1975041
    :cond_3
    check-cast p1, LX/DDA;

    .line 1975042
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1975043
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1975044
    if-eq v2, v3, :cond_0

    .line 1975045
    iget-object v2, p0, LX/DDA;->a:LX/DDZ;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/DDA;->a:LX/DDZ;

    iget-object v3, p1, LX/DDA;->a:LX/DDZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1975046
    goto :goto_0

    .line 1975047
    :cond_5
    iget-object v2, p1, LX/DDA;->a:LX/DDZ;

    if-nez v2, :cond_4

    .line 1975048
    :cond_6
    iget-object v2, p0, LX/DDA;->b:LX/DDY;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/DDA;->b:LX/DDY;

    iget-object v3, p1, LX/DDA;->b:LX/DDY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1975049
    goto :goto_0

    .line 1975050
    :cond_8
    iget-object v2, p1, LX/DDA;->b:LX/DDY;

    if-nez v2, :cond_7

    .line 1975051
    :cond_9
    iget-object v2, p0, LX/DDA;->c:LX/1Po;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/DDA;->c:LX/1Po;

    iget-object v3, p1, LX/DDA;->c:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1975052
    goto :goto_0

    .line 1975053
    :cond_a
    iget-object v2, p1, LX/DDA;->c:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
