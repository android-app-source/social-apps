.class public LX/DNp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Runnable;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/DNo;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/api/feedtype/FeedType;LX/B1W;LX/0fz;LX/DNe;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0pn;LX/9M3;LX/1Ck;)V
    .locals 12
    .param p1    # Lcom/facebook/api/feedtype/FeedType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/B1W;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0fz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/DNe;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1991991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1991992
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/DNp;->c:Z

    .line 1991993
    move-object/from16 v0, p9

    iput-object v0, p0, LX/DNp;->b:LX/1Ck;

    .line 1991994
    invoke-static/range {p6 .. p6}, LX/0TA;->a(Ljava/util/concurrent/ExecutorService;)LX/0TD;

    move-result-object v3

    .line 1991995
    new-instance v8, LX/DNj;

    move-object/from16 v0, p7

    invoke-direct {v8, p0, v0, p1}, LX/DNj;-><init>(LX/DNp;LX/0pn;Lcom/facebook/api/feedtype/FeedType;)V

    .line 1991996
    new-instance v1, LX/DNl;

    move-object v2, p0

    move-object v4, p2

    move-object/from16 v5, p8

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, LX/DNl;-><init>(LX/DNp;LX/0TD;LX/B1W;LX/9M3;LX/0tX;)V

    .line 1991997
    new-instance v4, Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;

    move-object v5, p0

    move-object v6, p3

    move-object v7, v3

    move-object v9, v1

    move-object v10, p2

    move-object/from16 v11, p4

    invoke-direct/range {v4 .. v11}, Lcom/facebook/groups/feed/data/GroupsFeedConsistencySync$3;-><init>(LX/DNp;LX/0fz;LX/0TD;Ljava/util/concurrent/Callable;LX/0Vj;LX/B1W;LX/DNe;)V

    iput-object v4, p0, LX/DNp;->a:Ljava/lang/Runnable;

    .line 1991998
    return-void
.end method

.method public static synthetic a(LX/DNp;LX/0pn;Lcom/facebook/api/feedtype/FeedType;LX/0Px;)LX/0Px;
    .locals 4

    .prologue
    .line 1991999
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1992000
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1992001
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p0, :cond_0

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    .line 1992002
    iget-object v0, v0, LX/44w;->a:Ljava/lang/Object;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1992003
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1992004
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [LX/0ux;

    const/4 v1, 0x0

    sget-object p0, LX/0pp;->a:LX/0U1;

    .line 1992005
    iget-object p3, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, p3

    .line 1992006
    invoke-virtual {p2}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p0, p3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object p0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object p0, LX/0pp;->d:LX/0U1;

    .line 1992007
    iget-object p3, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, p3

    .line 1992008
    invoke-static {p0, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object p0

    aput-object p0, v0, v1

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 1992009
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    const/4 p0, 0x0

    invoke-static {p1, v1, v0, p0}, LX/0pn;->a(LX/0pn;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1992010
    move-object v0, v0

    .line 1992011
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 1992012
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const p0, 0x42554c1c

    if-eq v3, p0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const p0, 0x65f211ca

    if-eq v3, p0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    invoke-interface {v3}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const p0, 0x655e82e2

    if-ne v3, p0, :cond_4

    :cond_2
    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 1992013
    if-eqz v3, :cond_1

    .line 1992014
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1992015
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1992016
    return-object v0

    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1992017
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/DNp;->c:Z

    .line 1992018
    iget-object v0, p0, LX/DNp;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1992019
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1992020
    iget-boolean v0, p0, LX/DNp;->c:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 1992021
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/DNp;->c:Z

    .line 1992022
    iget-object v0, p0, LX/DNp;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1992023
    :cond_1
    return-void
.end method
