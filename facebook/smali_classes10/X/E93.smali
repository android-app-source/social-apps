.class public LX/E93;
.super LX/E8m;
.source ""


# instance fields
.field public final A:LX/967;

.field private B:LX/1NP;

.field private g:Landroid/content/Context;

.field private h:LX/3U6;

.field private i:LX/E2N;

.field private j:LX/3Tp;

.field private k:LX/E1j;

.field private l:LX/0bH;

.field private m:LX/E92;

.field private n:LX/1PT;

.field private o:LX/1SX;

.field public p:LX/1Rq;

.field private q:LX/1Db;

.field private r:LX/AjP;

.field public s:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ">;>;"
        }
    .end annotation
.end field

.field public t:LX/1My;

.field private u:Z

.field private v:LX/1DZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final w:LX/1Db;

.field public x:LX/E1l;

.field public y:LX/CvY;

.field public final z:LX/3mH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;LX/0SG;LX/E2N;LX/3Tp;LX/E1j;LX/0bH;LX/1Db;LX/AjP;LX/1My;LX/1Db;LX/E1l;Lcom/facebook/reaction/ReactionUtil;LX/CvY;LX/Cfw;LX/3mH;LX/967;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1SX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0o8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2083737
    move-object/from16 v0, p15

    move-object/from16 v1, p17

    invoke-direct {p0, p4, p5, v0, v1}, LX/E8m;-><init>(LX/0o8;LX/0SG;Lcom/facebook/reaction/ReactionUtil;LX/Cfw;)V

    .line 2083738
    iput-object p1, p0, LX/E93;->g:Landroid/content/Context;

    .line 2083739
    iput-object p6, p0, LX/E93;->i:LX/E2N;

    .line 2083740
    iput-object p7, p0, LX/E93;->j:LX/3Tp;

    .line 2083741
    iput-object p8, p0, LX/E93;->k:LX/E1j;

    .line 2083742
    iput-object p9, p0, LX/E93;->l:LX/0bH;

    .line 2083743
    invoke-direct {p0}, LX/E93;->u()LX/0TF;

    move-result-object v2

    iput-object v2, p0, LX/E93;->s:LX/0TF;

    .line 2083744
    iput-object p12, p0, LX/E93;->t:LX/1My;

    .line 2083745
    iput-object p2, p0, LX/E93;->n:LX/1PT;

    .line 2083746
    iput-object p3, p0, LX/E93;->o:LX/1SX;

    .line 2083747
    iput-object p10, p0, LX/E93;->q:LX/1Db;

    .line 2083748
    iput-object p11, p0, LX/E93;->r:LX/AjP;

    .line 2083749
    move-object/from16 v0, p13

    iput-object v0, p0, LX/E93;->w:LX/1Db;

    .line 2083750
    move-object/from16 v0, p14

    iput-object v0, p0, LX/E93;->x:LX/E1l;

    .line 2083751
    move-object/from16 v0, p16

    iput-object v0, p0, LX/E93;->y:LX/CvY;

    .line 2083752
    move-object/from16 v0, p18

    iput-object v0, p0, LX/E93;->z:LX/3mH;

    .line 2083753
    move-object/from16 v0, p19

    iput-object v0, p0, LX/E93;->A:LX/967;

    .line 2083754
    return-void
.end method

.method public static a(LX/E93;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2083755
    iget-object v0, p0, LX/E93;->r:LX/AjP;

    invoke-virtual {v0, p1, v2}, LX/AjP;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    .line 2083756
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2083757
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2ja;->a(LX/9qX;)V

    .line 2083758
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->m()Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/2ja;->b(Ljava/lang/String;I)V

    .line 2083759
    :cond_0
    return-void
.end method

.method public static a(LX/E93;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;I)V
    .locals 8

    .prologue
    .line 2083760
    invoke-static {p1}, LX/E93;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2083761
    if-eqz v0, :cond_0

    .line 2083762
    iget-object v1, p0, LX/E8m;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 2083763
    iget-object v1, p0, LX/E93;->x:LX/E1l;

    new-instance v2, LX/E2O;

    invoke-direct {v2, v0, p1}, LX/E2O;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)V

    invoke-virtual {v1, p2, v2}, LX/E1l;->a(ILX/Cfo;)V

    .line 2083764
    invoke-static {p0, v0, p1}, LX/E93;->a(LX/E93;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)V

    .line 2083765
    :goto_0
    return-void

    .line 2083766
    :cond_0
    invoke-static {p1}, LX/E93;->c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)LX/E2P;

    move-result-object v0

    .line 2083767
    if-eqz v0, :cond_1

    .line 2083768
    invoke-virtual {v0}, LX/E2O;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    iget-object v2, p0, LX/E8m;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v3

    invoke-static {v1, v3, v4}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 2083769
    iget-object v1, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v1, p2, v0}, LX/E1l;->a(ILX/Cfo;)V

    .line 2083770
    invoke-virtual {v0}, LX/E2O;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {p0, v1, p1}, LX/E93;->a(LX/E93;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)V

    .line 2083771
    goto :goto_0

    .line 2083772
    :cond_1
    invoke-direct {p0, p1, p2}, LX/E93;->c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;I)I

    move-result v0

    .line 2083773
    if-gez v0, :cond_2

    .line 2083774
    :goto_1
    goto :goto_0

    .line 2083775
    :cond_2
    invoke-static {p1}, LX/CfY;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Ljava/util/Set;

    move-result-object v7

    .line 2083776
    new-instance v2, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v4, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v5, 0x0

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2083777
    iget-object v1, p0, LX/E93;->t:LX/1My;

    iget-object v3, p0, LX/E93;->s:LX/0TF;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4, v2}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_1
.end method

.method public static a$redex0(LX/E93;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 5

    .prologue
    .line 2083778
    iget-object v0, p0, LX/E93;->x:LX/E1l;

    .line 2083779
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2083780
    :cond_0
    iget-object v0, p0, LX/E93;->h:LX/3U6;

    if-eqz v0, :cond_1

    .line 2083781
    iget-object v0, p0, LX/E93;->h:LX/3U6;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, LX/1Qj;->a([Ljava/lang/Object;)V

    .line 2083782
    :goto_0
    return-void

    .line 2083783
    :cond_1
    invoke-virtual {p0}, LX/E8m;->k()V

    goto :goto_0

    .line 2083784
    :cond_2
    iget-object v1, v0, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cfo;

    .line 2083785
    invoke-interface {v1}, LX/Cfo;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2083786
    instance-of v4, v1, LX/E2O;

    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    .line 2083787
    check-cast v1, LX/E2O;

    .line 2083788
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2083789
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, LX/E2O;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2083790
    :cond_4
    :goto_2
    goto :goto_1

    .line 2083791
    :cond_5
    iput-object p1, v1, LX/E2O;->a:Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_2
.end method

.method private static b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083792
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    .line 2083793
    if-eqz v0, :cond_0

    .line 2083794
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->FEED_STORY_SINGLE:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object p0

    if-ne v1, p0, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2083795
    if-nez v1, :cond_1

    .line 2083796
    :cond_0
    const/4 v0, 0x0

    .line 2083797
    :goto_1
    return-object v0

    .line 2083798
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 2083799
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v1

    .line 2083800
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    if-eqz p0, :cond_3

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 2083801
    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2083802
    :goto_3
    move-object v0, v1

    .line 2083803
    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 2083804
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 2083805
    :cond_4
    const/4 v1, 0x0

    goto :goto_3
.end method

.method private b(LX/9qT;)V
    .locals 4

    .prologue
    .line 2083806
    invoke-interface {p1}, LX/9qT;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    .line 2083807
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    .line 2083808
    if-eqz v0, :cond_0

    .line 2083809
    iget-object p1, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {p1}, LX/E1l;->size()I

    move-result p1

    invoke-static {p0, v0, p1}, LX/E93;->a(LX/E93;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;I)V

    .line 2083810
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2083811
    :cond_1
    return-void
.end method

.method private b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 3
    .param p1    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2083812
    invoke-direct {p0, p2}, LX/E93;->c(Ljava/lang/String;)I

    move-result v0

    .line 2083813
    if-ltz v0, :cond_1

    .line 2083814
    iget-object v1, p0, LX/E93;->x:LX/E1l;

    .line 2083815
    iget-object v2, v1, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2083816
    iget-object v1, p0, LX/E93;->t:LX/1My;

    .line 2083817
    if-nez p2, :cond_2

    .line 2083818
    :goto_0
    if-eqz p1, :cond_0

    .line 2083819
    invoke-static {p0, p1, v0}, LX/E93;->a(LX/E93;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;I)V

    .line 2083820
    :cond_0
    invoke-virtual {p0}, LX/E8m;->k()V

    .line 2083821
    const/4 v0, 0x1

    .line 2083822
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2083823
    :cond_2
    iget-object v2, v1, LX/1My;->a:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2083824
    iget-object v2, v1, LX/1My;->f:LX/1N9;

    invoke-virtual {v2, p2}, LX/1N9;->a(Ljava/lang/String;)V

    .line 2083825
    iget-object v2, v1, LX/1My;->b:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;I)I
    .locals 3

    .prologue
    .line 2083826
    iget-object v0, p0, LX/E8m;->c:LX/Cfw;

    invoke-virtual {v0, p1}, LX/Cfw;->a(LX/9qX;)LX/Cfx;

    move-result-object v0

    .line 2083827
    const-string v1, "SUCCESS"

    .line 2083828
    iget-object v2, v0, LX/Cfx;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2083829
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2083830
    iget-object v1, p0, LX/E93;->x:LX/E1l;

    new-instance v2, Lcom/facebook/reaction/common/ReactionCardNode;

    invoke-direct {v2, p1, v0}, Lcom/facebook/reaction/common/ReactionCardNode;-><init>(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;LX/Cfx;)V

    invoke-virtual {v1, p2, v2}, LX/E1l;->a(ILX/Cfo;)V

    .line 2083831
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2083832
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2ja;->a(LX/9qX;)V

    .line 2083833
    :cond_0
    :goto_0
    return p2

    .line 2083834
    :cond_1
    iget-object v1, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v1}, LX/0o8;->m()LX/2ja;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2083835
    iget-object v1, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v1}, LX/0o8;->m()LX/2ja;

    move-result-object v1

    .line 2083836
    iget-object v2, v0, LX/Cfx;->d:Ljava/lang/String;

    move-object v0, v2

    .line 2083837
    invoke-interface {p1}, LX/9qX;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {p1}, LX/9qX;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2083838
    :cond_2
    iget-object v2, v1, LX/2ja;->c:LX/03V;

    sget-object p0, LX/2ja;->a:Ljava/lang/String;

    const-string p2, "Null unit id or unit type when adding invalid story"

    invoke-virtual {v2, p0, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2083839
    :cond_3
    :goto_1
    const/4 p2, -0x1

    goto :goto_0

    .line 2083840
    :cond_4
    iget-object v2, v1, LX/2ja;->t:Ljava/util/List;

    invoke-interface {p1}, LX/9qX;->m()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2083841
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2083842
    iget-object v2, v1, LX/2ja;->s:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private c(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v3, -0x1

    .line 2083843
    invoke-virtual {p0}, LX/E8m;->f()I

    move-result v1

    move v0, v1

    .line 2083844
    :goto_0
    invoke-virtual {p0}, LX/E8m;->g()I

    move-result v2

    if-gt v0, v2, :cond_3

    .line 2083845
    sub-int v2, v0, v1

    .line 2083846
    iget-object v4, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v4, v2}, LX/1Qr;->h_(I)I

    move-result v2

    .line 2083847
    if-ltz v2, :cond_0

    iget-object v4, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v4}, LX/E1l;->size()I

    move-result v4

    if-lt v2, v4, :cond_1

    :cond_0
    move v0, v3

    .line 2083848
    :goto_1
    return v0

    .line 2083849
    :cond_1
    iget-object v4, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v4, v2}, LX/E1l;->b(I)LX/Cfo;

    move-result-object v4

    invoke-interface {v4}, LX/Cfo;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v4

    .line 2083850
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v2

    .line 2083851
    goto :goto_1

    .line 2083852
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v3

    .line 2083853
    goto :goto_1
.end method

.method private static c(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)LX/E2P;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2083928
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->gV_()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;

    move-result-object v0

    .line 2083929
    if-eqz v0, :cond_0

    .line 2083930
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;->PAGE_POST_STORY:Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->d()Lcom/facebook/graphql/enums/GraphQLReactionStoryAttachmentsStyle;

    move-result-object v3

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2083931
    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 2083932
    :goto_1
    return-object v0

    .line 2083933
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;

    .line 2083934
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryFragmentModel$ReactionAttachmentsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;

    move-result-object v5

    .line 2083935
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->C()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->C()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2083936
    new-instance v0, LX/E2P;

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->C()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoryAttachmentFragmentModel;->u()Z

    move-result v2

    invoke-direct {v0, v1, p0, v2}, LX/E2P;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Z)V

    goto :goto_1

    .line 2083937
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 2083938
    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private f(I)V
    .locals 8

    .prologue
    .line 2083854
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2083855
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    iget-object v1, p0, LX/E8m;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2083856
    iget-object v4, v0, LX/2ja;->j:LX/1vC;

    const v5, 0x1e0004

    iget-object v6, v0, LX/2ja;->l:LX/2jY;

    .line 2083857
    iget-object v7, v6, LX/2jY;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2083858
    invoke-virtual {v4, v5, v6}, LX/1vC;->a(ILjava/lang/String;)V

    .line 2083859
    invoke-static {v0, p1, v2, v3}, LX/2ja;->b(LX/2ja;IJ)V

    .line 2083860
    iput-wide v2, v0, LX/2ja;->h:J

    .line 2083861
    const-wide/16 v4, 0x0

    iput-wide v4, v0, LX/2ja;->o:J

    .line 2083862
    iget v4, v0, LX/2ja;->i:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, LX/2ja;->i:I

    .line 2083863
    :cond_0
    return-void
.end method

.method private h(I)Z
    .locals 1

    .prologue
    .line 2083925
    invoke-virtual {p0}, LX/E8m;->g()I

    move-result v0

    if-le p1, v0, :cond_0

    iget-object v0, p0, LX/E8m;->f:LX/2jY;

    .line 2083926
    iget-boolean p0, v0, LX/2jY;->o:Z

    move v0, p0

    .line 2083927
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2083924
    new-instance v0, LX/E90;

    invoke-direct {v0, p0}, LX/E90;-><init>(LX/E93;)V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2083910
    sget v0, LX/Cfy;->d:I

    if-ne p2, v0, :cond_1

    .line 2083911
    new-instance v0, LX/E97;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/E8m;->b(Landroid/content/Context;)LX/E8s;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E97;-><init>(Landroid/view/View;)V

    .line 2083912
    :cond_0
    :goto_0
    return-object v0

    .line 2083913
    :cond_1
    sget v0, LX/Cfy;->c:I

    if-ne p2, v0, :cond_2

    .line 2083914
    new-instance v0, LX/E97;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/E8m;->a(Landroid/content/Context;)LX/E8q;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E97;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2083915
    :cond_2
    const/4 v0, 0x0

    .line 2083916
    sget v1, LX/Cfy;->b:I

    if-ne p2, v1, :cond_4

    .line 2083917
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f031171

    invoke-static {v1, v2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2083918
    new-instance v0, LX/E8u;

    invoke-direct {v0, v1}, LX/E8u;-><init>(Landroid/view/View;)V

    .line 2083919
    :cond_3
    :goto_1
    move-object v0, v0

    .line 2083920
    if-nez v0, :cond_0

    .line 2083921
    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v0, p1, p2}, LX/1OQ;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0

    .line 2083922
    :cond_4
    sget v1, LX/Cfy;->a:I

    if-ne p2, v1, :cond_3

    .line 2083923
    new-instance v0, LX/E8u;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8u;-><init>(Landroid/view/View;)V

    goto :goto_1
.end method

.method public final a(LX/1DZ;)V
    .locals 0

    .prologue
    .line 2083908
    iput-object p1, p0, LX/E93;->v:LX/1DZ;

    .line 2083909
    return-void
.end method

.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 2083905
    invoke-super {p0, p1}, LX/E8m;->a(LX/1a1;)V

    .line 2083906
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    .line 2083907
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 13

    .prologue
    .line 2083870
    instance-of v0, p1, LX/E97;

    if-eqz v0, :cond_0

    .line 2083871
    :goto_0
    return-void

    .line 2083872
    :cond_0
    instance-of v0, p1, LX/E8u;

    if-eqz v0, :cond_2

    .line 2083873
    iget-object v2, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v2}, LX/0o8;->m()LX/2ja;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2083874
    iget-object v2, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v2}, LX/0o8;->m()LX/2ja;

    move-result-object v2

    iget-object v3, p0, LX/E8m;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const v11, 0x1e000d

    const v10, 0x1e0004

    .line 2083875
    iget-wide v6, v2, LX/2ja;->o:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 2083876
    const-string v6, "%s:%d"

    const-string v7, "PAGE_NUMBER"

    iget v8, v2, LX/2ja;->i:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object v6, v6

    .line 2083877
    iget-object v7, v2, LX/2ja;->j:LX/1vC;

    iget-object v8, v2, LX/2ja;->l:LX/2jY;

    .line 2083878
    iget-object v9, v8, LX/2jY;->a:Ljava/lang/String;

    move-object v8, v9

    .line 2083879
    iget-object v9, v2, LX/2ja;->l:LX/2jY;

    .line 2083880
    iget-object v12, v9, LX/2jY;->b:Ljava/lang/String;

    move-object v9, v12

    .line 2083881
    invoke-virtual {v7, v11, v8, v9}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2083882
    iget-object v7, v2, LX/2ja;->j:LX/1vC;

    iget-object v8, v2, LX/2ja;->l:LX/2jY;

    .line 2083883
    iget-object v9, v8, LX/2jY;->a:Ljava/lang/String;

    move-object v8, v9

    .line 2083884
    invoke-virtual {v7, v11, v8, v6}, LX/1vC;->c(ILjava/lang/String;Ljava/lang/String;)V

    .line 2083885
    iget-object v7, v2, LX/2ja;->j:LX/1vC;

    iget-object v8, v2, LX/2ja;->l:LX/2jY;

    .line 2083886
    iget-object v9, v8, LX/2jY;->a:Ljava/lang/String;

    move-object v8, v9

    .line 2083887
    iget-object v9, v2, LX/2ja;->l:LX/2jY;

    .line 2083888
    iget-object v11, v9, LX/2jY;->b:Ljava/lang/String;

    move-object v9, v11

    .line 2083889
    invoke-virtual {v7, v10, v8, v9}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2083890
    iget-object v7, v2, LX/2ja;->j:LX/1vC;

    iget-object v8, v2, LX/2ja;->l:LX/2jY;

    .line 2083891
    iget-object v9, v8, LX/2jY;->a:Ljava/lang/String;

    move-object v8, v9

    .line 2083892
    invoke-virtual {v7, v10, v8, v6}, LX/1vC;->c(ILjava/lang/String;Ljava/lang/String;)V

    .line 2083893
    iput-wide v4, v2, LX/2ja;->o:J

    .line 2083894
    :cond_1
    goto :goto_0

    .line 2083895
    :cond_2
    invoke-virtual {p0}, LX/E8m;->f()I

    move-result v0

    sub-int v0, p2, v0

    .line 2083896
    iget-object v1, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v1, p1, v0}, LX/1OQ;->a(LX/1a1;I)V

    .line 2083897
    iget-object v1, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v1, v0}, LX/1Qr;->h_(I)I

    move-result v1

    .line 2083898
    if-ltz v1, :cond_3

    iget-object v2, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v2}, LX/E1l;->size()I

    move-result v2

    if-lt v1, v2, :cond_4

    .line 2083899
    :cond_3
    :goto_1
    goto/16 :goto_0

    .line 2083900
    :cond_4
    iget-object v2, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v2, v1}, LX/E1l;->b(I)LX/Cfo;

    move-result-object v2

    .line 2083901
    invoke-interface {v2}, LX/Cfo;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    .line 2083902
    iget-object v3, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v3}, LX/0o8;->m()LX/2ja;

    move-result-object v3

    .line 2083903
    if-eqz v3, :cond_3

    .line 2083904
    invoke-virtual {v3, v2, v1, p2}, LX/2ja;->a(LX/9qX;II)V

    goto :goto_1
.end method

.method public final a(LX/9qT;)V
    .locals 2

    .prologue
    .line 2083864
    iget-object v0, p0, LX/E8m;->f:LX/2jY;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2083865
    iget-object v0, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v0}, LX/E1l;->size()I

    move-result v0

    .line 2083866
    invoke-direct {p0, p1}, LX/E93;->b(LX/9qT;)V

    .line 2083867
    invoke-virtual {p0}, LX/E8m;->k()V

    .line 2083868
    iget-object v1, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v1}, LX/E1l;->size()I

    move-result v1

    sub-int v0, v1, v0

    invoke-direct {p0, v0}, LX/E93;->f(I)V

    .line 2083869
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2083731
    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    if-eqz v0, :cond_0

    .line 2083732
    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 2083733
    :cond_0
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2083734
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V
    .locals 0

    .prologue
    .line 2083735
    iput-object p1, p0, LX/E93;->d:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    .line 2083736
    return-void
.end method

.method public final a(LX/2jY;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2083666
    iput-object p1, p0, LX/E93;->f:LX/2jY;

    .line 2083667
    invoke-virtual {p0}, LX/E8m;->j()V

    .line 2083668
    invoke-virtual {p1}, LX/2jY;->o()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qT;

    .line 2083669
    invoke-direct {p0, v0}, LX/E93;->b(LX/9qT;)V

    .line 2083670
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2083671
    :cond_0
    invoke-virtual {p0}, LX/E93;->q()LX/1Rq;

    move-result-object v0

    iput-object v0, p0, LX/E93;->p:LX/1Rq;

    .line 2083672
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->kI_()Landroid/view/ViewGroup;

    move-result-object v0

    .line 2083673
    instance-of v2, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v2, :cond_3

    .line 2083674
    :goto_1
    iget-object v0, p0, LX/E93;->r:LX/AjP;

    new-instance v2, LX/E8v;

    invoke-direct {v2, p0}, LX/E8v;-><init>(LX/E93;)V

    .line 2083675
    iput-object v2, v0, LX/AjP;->c:LX/AjN;

    .line 2083676
    iget-object v0, p0, LX/E93;->x:LX/E1l;

    .line 2083677
    iget-object v2, v0, LX/E1l;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    move v0, v2

    .line 2083678
    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2083679
    :goto_2
    if-eqz v0, :cond_1

    .line 2083680
    iget-object v1, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v1}, LX/E1l;->size()I

    move-result v1

    invoke-direct {p0, v1}, LX/E93;->f(I)V

    .line 2083681
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 2083682
    goto :goto_2

    .line 2083683
    :cond_3
    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2083684
    new-instance v2, LX/E8z;

    invoke-direct {v2, p0}, LX/E8z;-><init>(LX/E93;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setRecyclerListener(LX/1OU;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2083648
    invoke-direct {p0, p1, p2}, LX/E93;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2083649
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/E93;->b(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 2083650
    invoke-super {p0, p1}, LX/E8m;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 2083651
    new-instance v0, LX/E92;

    invoke-direct {v0, p0}, LX/E92;-><init>(LX/E93;)V

    iput-object v0, p0, LX/E93;->m:LX/E92;

    .line 2083652
    iget-object v0, p0, LX/E93;->l:LX/0bH;

    iget-object v1, p0, LX/E93;->m:LX/E92;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2083653
    new-instance v0, LX/E8y;

    invoke-direct {v0, p0}, LX/E8y;-><init>(LX/E93;)V

    iput-object v0, p0, LX/E93;->B:LX/1NP;

    .line 2083654
    iget-object v0, p0, LX/E93;->l:LX/0bH;

    iget-object v1, p0, LX/E93;->B:LX/1NP;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2083655
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2083656
    invoke-direct {p0, p1}, LX/E93;->c(Ljava/lang/String;)I

    move-result v0

    .line 2083657
    if-ltz v0, :cond_0

    invoke-virtual {p0}, LX/E8m;->e()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2083658
    iget-object v1, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v1, v0}, LX/E1l;->b(I)LX/Cfo;

    move-result-object v1

    move-object v0, v1

    .line 2083659
    invoke-interface {v0}, LX/Cfo;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    .line 2083660
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/2jY;)V
    .locals 1

    .prologue
    .line 2083661
    iput-object p1, p0, LX/E93;->f:LX/2jY;

    .line 2083662
    invoke-virtual {p0}, LX/E93;->dispose()V

    .line 2083663
    invoke-virtual {p0}, LX/E93;->q()LX/1Rq;

    move-result-object v0

    iput-object v0, p0, LX/E93;->p:LX/1Rq;

    .line 2083664
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/E93;->u:Z

    .line 2083665
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 2083685
    invoke-super {p0, p1}, LX/E8m;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 2083686
    iget-object v0, p0, LX/E93;->l:LX/0bH;

    iget-object v1, p0, LX/E93;->m:LX/E92;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2083687
    iget-object v0, p0, LX/E93;->l:LX/0bH;

    iget-object v1, p0, LX/E93;->B:LX/1NP;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2083688
    iget-object v0, p0, LX/E93;->r:LX/AjP;

    invoke-virtual {v0}, LX/AjP;->a()V

    .line 2083689
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2083690
    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    goto :goto_0
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 2083691
    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    if-eqz v0, :cond_0

    .line 2083692
    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 2083693
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/E93;->u:Z

    .line 2083694
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2083695
    iget-object v0, p0, LX/E93;->x:LX/E1l;

    invoke-virtual {v0}, LX/E1l;->size()I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2083696
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    if-nez v0, :cond_1

    .line 2083697
    :cond_0
    const/4 v0, 0x0

    .line 2083698
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v0, p1}, LX/1Qr;->h_(I)I

    move-result v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 5

    .prologue
    .line 2083699
    invoke-direct {p0, p1}, LX/E93;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2083700
    sget v0, LX/Cfy;->b:I

    .line 2083701
    :goto_0
    return v0

    .line 2083702
    :cond_0
    invoke-virtual {p0}, LX/E8m;->g()I

    move-result v0

    if-le p1, v0, :cond_2

    .line 2083703
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2083704
    iget-object v0, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    .line 2083705
    iget-boolean v1, v0, LX/2ja;->n:Z

    if-eqz v1, :cond_1

    .line 2083706
    iget-object v1, v0, LX/2ja;->g:LX/2j3;

    iget-object v2, v0, LX/2ja;->l:LX/2jY;

    .line 2083707
    iget-object v3, v2, LX/2jY;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2083708
    iget-object v3, v0, LX/2ja;->l:LX/2jY;

    .line 2083709
    iget-object v4, v3, LX/2jY;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2083710
    iget-object v4, v1, LX/2j3;->a:LX/0Zb;

    sget-object p1, LX/Cff;->REACTION_SCROLLED_TO_BOTTOM:LX/Cff;

    const-string p0, "reaction_overlay"

    invoke-static {p1, v2, p0, v3}, LX/2j3;->a(LX/Cff;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v4, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2083711
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/2ja;->n:Z

    .line 2083712
    :cond_1
    sget v0, LX/Cfy;->a:I

    goto :goto_0

    .line 2083713
    :cond_2
    invoke-virtual {p0}, LX/E8m;->f()I

    move-result v0

    if-ge p1, v0, :cond_4

    .line 2083714
    invoke-virtual {p0}, LX/E8m;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2083715
    invoke-virtual {p0}, LX/E8m;->i()I

    move-result v0

    if-ne p1, v0, :cond_3

    sget v0, LX/Cfy;->c:I

    goto :goto_0

    :cond_3
    sget v0, LX/Cfy;->d:I

    goto :goto_0

    .line 2083716
    :cond_4
    invoke-virtual {p0}, LX/E8m;->f()I

    move-result v0

    sub-int v0, p1, v0

    .line 2083717
    iget-object v1, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v1, v0}, LX/1OP;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 2083718
    iget-boolean v0, p0, LX/E93;->u:Z

    return v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 2083644
    iget-object v0, p0, LX/E93;->t:LX/1My;

    invoke-virtual {v0}, LX/1My;->b()V

    .line 2083645
    iget-object v0, p0, LX/E93;->x:LX/E1l;

    .line 2083646
    iget-object p0, v0, LX/E1l;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 2083647
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2083719
    iget-object v0, p0, LX/E93;->p:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->notifyDataSetChanged()V

    .line 2083720
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2083721
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2083722
    iget-object v0, p0, LX/E93;->t:LX/1My;

    invoke-virtual {v0}, LX/1My;->d()V

    .line 2083723
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 2083724
    iget-object v0, p0, LX/E93;->t:LX/1My;

    invoke-virtual {v0}, LX/1My;->e()V

    .line 2083725
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 2083726
    iget-object v0, p0, LX/E93;->t:LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 2083727
    return-void
.end method

.method public q()LX/1Rq;
    .locals 10

    .prologue
    .line 2083728
    iget-object v0, p0, LX/E93;->i:LX/E2N;

    iget-object v1, p0, LX/E93;->g:Landroid/content/Context;

    iget-object v2, p0, LX/E93;->n:LX/1PT;

    iget-object v3, p0, LX/E93;->o:LX/1SX;

    new-instance v4, Lcom/facebook/reaction/ui/recyclerview/ReactionMixedRecyclerViewAdapter$2;

    invoke-direct {v4, p0}, Lcom/facebook/reaction/ui/recyclerview/ReactionMixedRecyclerViewAdapter$2;-><init>(LX/E93;)V

    iget-object v5, p0, LX/E8m;->d:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v6, p0, LX/E93;->j:LX/3Tp;

    iget-object v7, p0, LX/E93;->g:Landroid/content/Context;

    iget-object v8, p0, LX/E8m;->a:LX/0o8;

    invoke-virtual {v6, v7, v8}, LX/3Tp;->a(Landroid/content/Context;LX/0o8;)LX/3Tv;

    move-result-object v6

    iget-object v7, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v7}, LX/0o8;->m()LX/2ja;

    move-result-object v7

    iget-object v8, p0, LX/E8m;->f:LX/2jY;

    invoke-virtual {p0}, LX/E93;->r()LX/1PY;

    move-result-object v9

    invoke-virtual/range {v0 .. v9}, LX/E2N;->a(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/2ja;LX/2jY;LX/1PY;)LX/3U6;

    move-result-object v0

    iput-object v0, p0, LX/E93;->h:LX/3U6;

    .line 2083729
    iget-object v0, p0, LX/E93;->k:LX/E1j;

    iget-object v1, p0, LX/E93;->h:LX/3U6;

    iget-object v2, p0, LX/E93;->v:LX/1DZ;

    iget-object v3, p0, LX/E93;->x:LX/E1l;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/E1j;->a(LX/3U6;LX/1DZ;LX/E1l;LX/0g8;)LX/1Rq;

    move-result-object v0

    return-object v0
.end method

.method public final r()LX/1PY;
    .locals 1

    .prologue
    .line 2083730
    new-instance v0, LX/E8x;

    invoke-direct {v0, p0}, LX/E8x;-><init>(LX/E93;)V

    return-object v0
.end method
