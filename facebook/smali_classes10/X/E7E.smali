.class public final enum LX/E7E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/E7E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/E7E;

.field public static final enum ACCEPTED:LX/E7E;

.field public static final enum DECLINED:LX/E7E;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2081192
    new-instance v0, LX/E7E;

    const-string v1, "ACCEPTED"

    const-string v2, "accepted"

    invoke-direct {v0, v1, v3, v2}, LX/E7E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/E7E;->ACCEPTED:LX/E7E;

    .line 2081193
    new-instance v0, LX/E7E;

    const-string v1, "DECLINED"

    const-string v2, "declined"

    invoke-direct {v0, v1, v4, v2}, LX/E7E;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/E7E;->DECLINED:LX/E7E;

    .line 2081194
    const/4 v0, 0x2

    new-array v0, v0, [LX/E7E;

    sget-object v1, LX/E7E;->ACCEPTED:LX/E7E;

    aput-object v1, v0, v3

    sget-object v1, LX/E7E;->DECLINED:LX/E7E;

    aput-object v1, v0, v4

    sput-object v0, LX/E7E;->$VALUES:[LX/E7E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2081195
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, LX/E7E;->name:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/E7E;
    .locals 1

    .prologue
    .line 2081196
    const-class v0, LX/E7E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/E7E;

    return-object v0
.end method

.method public static values()[LX/E7E;
    .locals 1

    .prologue
    .line 2081197
    sget-object v0, LX/E7E;->$VALUES:[LX/E7E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/E7E;

    return-object v0
.end method
