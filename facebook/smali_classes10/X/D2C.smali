.class public final enum LX/D2C;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/D2C;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/D2C;

.field public static final enum CALL_TO_ACTION_BUTTON_CLICKED:LX/D2C;

.field public static final enum CALL_TO_ACTION_BUTTON_IMPRESSION:LX/D2C;

.field public static final enum CAMERA_CLOSED:LX/D2C;

.field public static final enum DEEP_LINK_BUTTON_CLICKED:LX/D2C;

.field public static final enum DEEP_LINK_FLOW_CANCELED:LX/D2C;

.field public static final enum DEEP_LINK_FLOW_SUCCESS:LX/D2C;

.field public static final enum EXISTING_VIDEO:LX/D2C;

.field public static final enum INVALID_VIDEO:LX/D2C;

.field public static final enum NUX_MAIN_BUTTON_CLICKED:LX/D2C;

.field public static final enum OPEN_GALLERY:LX/D2C;

.field public static final enum PREVIEW_CLOSED:LX/D2C;

.field public static final enum PREVIEW_OPENED:LX/D2C;

.field public static final enum SCRUBBER_CLOSED:LX/D2C;

.field public static final enum SCRUBBER_OPENED:LX/D2C;

.field public static final enum TAKE_VIDEO:LX/D2C;

.field public static final enum UPLOAD_STARTED:LX/D2C;

.field public static final enum USER_CREATION_FINISHED:LX/D2C;

.field public static final enum VIDEO_CAMERA_OPEN:LX/D2C;

.field public static final enum VIDEO_TOO_LONG:LX/D2C;

.field public static final enum VIDEO_TOO_SMALL:LX/D2C;


# instance fields
.field private mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1958300
    new-instance v0, LX/D2C;

    const-string v1, "CALL_TO_ACTION_BUTTON_IMPRESSION"

    const-string v2, "profile_video_android_call_to_action_button_impression"

    invoke-direct {v0, v1, v4, v2}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->CALL_TO_ACTION_BUTTON_IMPRESSION:LX/D2C;

    .line 1958301
    new-instance v0, LX/D2C;

    const-string v1, "CALL_TO_ACTION_BUTTON_CLICKED"

    const-string v2, "profile_video_android_call_to_action_button_clicked"

    invoke-direct {v0, v1, v5, v2}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->CALL_TO_ACTION_BUTTON_CLICKED:LX/D2C;

    .line 1958302
    new-instance v0, LX/D2C;

    const-string v1, "NUX_MAIN_BUTTON_CLICKED"

    const-string v2, "profile_video_android_nux_main_button_clicked"

    invoke-direct {v0, v1, v6, v2}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->NUX_MAIN_BUTTON_CLICKED:LX/D2C;

    .line 1958303
    new-instance v0, LX/D2C;

    const-string v1, "VIDEO_CAMERA_OPEN"

    const-string v2, "profile_video_android_video_camera_open"

    invoke-direct {v0, v1, v7, v2}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->VIDEO_CAMERA_OPEN:LX/D2C;

    .line 1958304
    new-instance v0, LX/D2C;

    const-string v1, "TAKE_VIDEO"

    const-string v2, "profile_video_android_take_video_with_camera"

    invoke-direct {v0, v1, v8, v2}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->TAKE_VIDEO:LX/D2C;

    .line 1958305
    new-instance v0, LX/D2C;

    const-string v1, "DEEP_LINK_BUTTON_CLICKED"

    const/4 v2, 0x5

    const-string v3, "profile_video_android_deep_link_button_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->DEEP_LINK_BUTTON_CLICKED:LX/D2C;

    .line 1958306
    new-instance v0, LX/D2C;

    const-string v1, "DEEP_LINK_FLOW_SUCCESS"

    const/4 v2, 0x6

    const-string v3, "profile_video_android_deep_link_flow_success"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->DEEP_LINK_FLOW_SUCCESS:LX/D2C;

    .line 1958307
    new-instance v0, LX/D2C;

    const-string v1, "DEEP_LINK_FLOW_CANCELED"

    const/4 v2, 0x7

    const-string v3, "profile_video_android_deep_link_flow_canceled"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->DEEP_LINK_FLOW_CANCELED:LX/D2C;

    .line 1958308
    new-instance v0, LX/D2C;

    const-string v1, "OPEN_GALLERY"

    const/16 v2, 0x8

    const-string v3, "profile_video_android_open_video_gallery"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->OPEN_GALLERY:LX/D2C;

    .line 1958309
    new-instance v0, LX/D2C;

    const-string v1, "EXISTING_VIDEO"

    const/16 v2, 0x9

    const-string v3, "profile_video_android_chose_existing_video"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->EXISTING_VIDEO:LX/D2C;

    .line 1958310
    new-instance v0, LX/D2C;

    const-string v1, "INVALID_VIDEO"

    const/16 v2, 0xa

    const-string v3, "profile_video_android_invalid_video"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->INVALID_VIDEO:LX/D2C;

    .line 1958311
    new-instance v0, LX/D2C;

    const-string v1, "VIDEO_TOO_LONG"

    const/16 v2, 0xb

    const-string v3, "profile_video_android_video_chosen_too_long"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->VIDEO_TOO_LONG:LX/D2C;

    .line 1958312
    new-instance v0, LX/D2C;

    const-string v1, "VIDEO_TOO_SMALL"

    const/16 v2, 0xc

    const-string v3, "profile_video_android_video_too_small"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->VIDEO_TOO_SMALL:LX/D2C;

    .line 1958313
    new-instance v0, LX/D2C;

    const-string v1, "PREVIEW_OPENED"

    const/16 v2, 0xd

    const-string v3, "profile_video_android_preview_opened"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->PREVIEW_OPENED:LX/D2C;

    .line 1958314
    new-instance v0, LX/D2C;

    const-string v1, "SCRUBBER_OPENED"

    const/16 v2, 0xe

    const-string v3, "profile_video_android_scrubber_opened"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->SCRUBBER_OPENED:LX/D2C;

    .line 1958315
    new-instance v0, LX/D2C;

    const-string v1, "CAMERA_CLOSED"

    const/16 v2, 0xf

    const-string v3, "profile_video_android_camera_closed"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->CAMERA_CLOSED:LX/D2C;

    .line 1958316
    new-instance v0, LX/D2C;

    const-string v1, "PREVIEW_CLOSED"

    const/16 v2, 0x10

    const-string v3, "profile_video_android_preview_closed"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->PREVIEW_CLOSED:LX/D2C;

    .line 1958317
    new-instance v0, LX/D2C;

    const-string v1, "SCRUBBER_CLOSED"

    const/16 v2, 0x11

    const-string v3, "profile_video_android_scrubber_closed"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->SCRUBBER_CLOSED:LX/D2C;

    .line 1958318
    new-instance v0, LX/D2C;

    const-string v1, "UPLOAD_STARTED"

    const/16 v2, 0x12

    const-string v3, "profile_video_android_upload_started"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->UPLOAD_STARTED:LX/D2C;

    .line 1958319
    new-instance v0, LX/D2C;

    const-string v1, "USER_CREATION_FINISHED"

    const/16 v2, 0x13

    const-string v3, "profile_video_android_user_creation_flow_finished"

    invoke-direct {v0, v1, v2, v3}, LX/D2C;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/D2C;->USER_CREATION_FINISHED:LX/D2C;

    .line 1958320
    const/16 v0, 0x14

    new-array v0, v0, [LX/D2C;

    sget-object v1, LX/D2C;->CALL_TO_ACTION_BUTTON_IMPRESSION:LX/D2C;

    aput-object v1, v0, v4

    sget-object v1, LX/D2C;->CALL_TO_ACTION_BUTTON_CLICKED:LX/D2C;

    aput-object v1, v0, v5

    sget-object v1, LX/D2C;->NUX_MAIN_BUTTON_CLICKED:LX/D2C;

    aput-object v1, v0, v6

    sget-object v1, LX/D2C;->VIDEO_CAMERA_OPEN:LX/D2C;

    aput-object v1, v0, v7

    sget-object v1, LX/D2C;->TAKE_VIDEO:LX/D2C;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/D2C;->DEEP_LINK_BUTTON_CLICKED:LX/D2C;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/D2C;->DEEP_LINK_FLOW_SUCCESS:LX/D2C;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/D2C;->DEEP_LINK_FLOW_CANCELED:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/D2C;->OPEN_GALLERY:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/D2C;->EXISTING_VIDEO:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/D2C;->INVALID_VIDEO:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/D2C;->VIDEO_TOO_LONG:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/D2C;->VIDEO_TOO_SMALL:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/D2C;->PREVIEW_OPENED:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/D2C;->SCRUBBER_OPENED:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/D2C;->CAMERA_CLOSED:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/D2C;->PREVIEW_CLOSED:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/D2C;->SCRUBBER_CLOSED:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/D2C;->UPLOAD_STARTED:LX/D2C;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/D2C;->USER_CREATION_FINISHED:LX/D2C;

    aput-object v2, v0, v1

    sput-object v0, LX/D2C;->$VALUES:[LX/D2C;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1958321
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1958322
    iput-object p3, p0, LX/D2C;->mEventName:Ljava/lang/String;

    .line 1958323
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/D2C;
    .locals 1

    .prologue
    .line 1958324
    const-class v0, LX/D2C;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/D2C;

    return-object v0
.end method

.method public static values()[LX/D2C;
    .locals 1

    .prologue
    .line 1958325
    sget-object v0, LX/D2C;->$VALUES:[LX/D2C;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/D2C;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1958326
    iget-object v0, p0, LX/D2C;->mEventName:Ljava/lang/String;

    return-object v0
.end method
