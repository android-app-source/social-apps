.class public final LX/EQk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;",
        ">;",
        "Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/EQl;


# direct methods
.method public constructor <init>(LX/EQl;)V
    .locals 0

    .prologue
    .line 2119080
    iput-object p1, p0, LX/EQk;->a:LX/EQl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2119081
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2119082
    if-nez p1, :cond_0

    .line 2119083
    const/4 v0, 0x0

    .line 2119084
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/EQk;->a:LX/EQl;

    .line 2119085
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2119086
    check-cast v0, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;

    .line 2119087
    invoke-static {v1}, LX/EQl;->c(LX/EQl;)Z

    move-result v2

    .line 2119088
    invoke-static {v0, v2}, LX/EQl;->a(Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;Z)Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    move-result-object v3

    .line 2119089
    const/4 v4, 0x0

    .line 2119090
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v5

    if-nez v5, :cond_4

    .line 2119091
    :cond_1
    :goto_1
    move-object v4, v4

    .line 2119092
    new-instance v5, LX/ER3;

    invoke-direct {v5}, LX/ER3;-><init>()V

    move-object v5, v5

    .line 2119093
    iput-boolean v2, v5, LX/ER3;->a:Z

    .line 2119094
    move-object v5, v5

    .line 2119095
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2119096
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v8

    if-eqz v8, :cond_10

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v8

    if-eqz v8, :cond_10

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->a()Z

    move-result v8

    if-eqz v8, :cond_10

    move p0, v7

    .line 2119097
    :goto_2
    if-eqz v3, :cond_11

    move v8, v7

    .line 2119098
    :goto_3
    if-nez v8, :cond_12

    .line 2119099
    if-eqz p0, :cond_2

    .line 2119100
    iget-object v7, v1, LX/EQl;->d:LX/03V;

    const-string v8, "MomentsUpsell"

    const-string p0, "No interstitial data returned, but always show is true"

    invoke-virtual {v7, v8, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2119101
    :cond_2
    :goto_4
    move v2, v6

    .line 2119102
    iput-boolean v2, v5, LX/ER3;->b:Z

    .line 2119103
    move-object v2, v5

    .line 2119104
    iput-object v3, v2, LX/ER3;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    .line 2119105
    move-object v3, v2

    .line 2119106
    if-eqz v4, :cond_3

    const/4 v2, 0x1

    .line 2119107
    :goto_5
    iput-boolean v2, v3, LX/ER3;->d:Z

    .line 2119108
    move-object v2, v3

    .line 2119109
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2119110
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v3

    if-eqz v3, :cond_15

    .line 2119111
    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->a()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2119112
    if-eqz v3, :cond_14

    move v3, v5

    :goto_6
    if-eqz v3, :cond_17

    .line 2119113
    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->a()LX/1vs;

    move-result-object v3

    iget-object v7, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2119114
    invoke-virtual {v7, v3, v6}, LX/15i;->j(II)I

    move-result v3

    if-nez v3, :cond_16

    move v3, v5

    :goto_7
    if-eqz v3, :cond_18

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v3

    if-nez v3, :cond_18

    :goto_8
    move v3, v5

    .line 2119115
    iput-boolean v3, v2, LX/ER3;->e:Z

    .line 2119116
    move-object v2, v2

    .line 2119117
    if-eqz v0, :cond_19

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v3

    if-eqz v3, :cond_19

    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->c()Z

    move-result v3

    if-eqz v3, :cond_19

    const/4 v3, 0x1

    :goto_9
    move v3, v3

    .line 2119118
    iput-boolean v3, v2, LX/ER3;->f:Z

    .line 2119119
    move-object v2, v2

    .line 2119120
    iput-object v4, v2, LX/ER3;->g:Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    .line 2119121
    move-object v2, v2

    .line 2119122
    new-instance v3, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-direct {v3, v2}, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;-><init>(LX/ER3;)V

    move-object v2, v3

    .line 2119123
    move-object v0, v2

    .line 2119124
    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_5

    .line 2119125
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->e()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2119126
    new-instance v4, LX/ER7;

    invoke-direct {v4}, LX/ER7;-><init>()V

    move-object v4, v4

    .line 2119127
    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel$VaultModel;->b()Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionFragmentModel;->b()I

    move-result v5

    .line 2119128
    iput v5, v4, LX/ER7;->a:I

    .line 2119129
    move-object v4, v4

    .line 2119130
    const p0, -0x6d1efd5d

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2119131
    if-nez v0, :cond_5

    move v6, v5

    :goto_a
    if-eqz v6, :cond_7

    move v6, v5

    :goto_b
    if-eqz v6, :cond_a

    :goto_c
    if-eqz v5, :cond_c

    .line 2119132
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 2119133
    :goto_d
    move-object v5, v5

    .line 2119134
    iput-object v5, v4, LX/ER7;->b:Ljava/util/List;

    .line 2119135
    move-object v4, v4

    .line 2119136
    iget-object v5, v1, LX/EQl;->c:Landroid/content/Context;

    const v6, 0x7f083139

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 2119137
    iput-object v5, v4, LX/ER7;->c:Ljava/lang/String;

    .line 2119138
    move-object v4, v4

    .line 2119139
    const-string v5, "moments://"

    move-object v5, v5

    .line 2119140
    iput-object v5, v4, LX/ER7;->d:Ljava/lang/String;

    .line 2119141
    move-object v4, v4

    .line 2119142
    new-instance v5, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;

    invoke-direct {v5, v4}, Lcom/facebook/vault/momentsupsell/model/MomentsAppTabInfo;-><init>(LX/ER7;)V

    move-object v4, v5

    .line 2119143
    goto/16 :goto_1

    .line 2119144
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->a()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    .line 2119145
    if-nez v6, :cond_6

    move v6, v5

    goto :goto_a

    :cond_6
    move v6, v7

    goto :goto_a

    .line 2119146
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->a()LX/1vs;

    move-result-object v6

    iget-object v8, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    invoke-static {v8, v6, v7, p0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v6

    .line 2119147
    if-eqz v6, :cond_8

    invoke-static {v6}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v6

    :goto_e
    if-nez v6, :cond_9

    move v6, v5

    goto :goto_b

    :cond_8
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v6

    goto :goto_e

    :cond_9
    move v6, v7

    goto :goto_b

    .line 2119148
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->a()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-static {v6, v5, v7, p0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v5

    .line 2119149
    if-eqz v5, :cond_b

    invoke-static {v5}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v5

    :goto_f
    invoke-virtual {v5}, LX/39O;->a()Z

    move-result v5

    goto :goto_c

    :cond_b
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v5

    goto :goto_f

    .line 2119150
    :cond_c
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2119151
    invoke-virtual {v0}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPromotionQueryModel;->a()LX/1vs;

    move-result-object v5

    iget-object v8, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-static {v8, v5, v7, p0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v5

    .line 2119152
    if-eqz v5, :cond_e

    invoke-static {v5}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v5

    :goto_10
    invoke-virtual {v5}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :cond_d
    :goto_11
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v5

    iget-object p0, v5, LX/1vs;->a:LX/15i;

    iget p1, v5, LX/1vs;->b:I

    .line 2119153
    if-eqz p1, :cond_d

    const-class v5, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    invoke-virtual {p0, p1, v7, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    if-eqz v5, :cond_d

    const-class v5, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    invoke-virtual {p0, p1, v7, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_d

    const-class v5, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    invoke-virtual {p0, p1, v7, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_d

    .line 2119154
    const-class v5, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    invoke-virtual {p0, p1, v7, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;

    invoke-virtual {v5}, Lcom/facebook/vault/momentsupsell/graphql/MomentsUpsellQueryModels$MomentsAppPhotoFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_11

    .line 2119155
    :cond_e
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v5

    goto :goto_10

    .line 2119156
    :cond_f
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    goto/16 :goto_d

    :cond_10
    move p0, v6

    .line 2119157
    goto/16 :goto_2

    :cond_11
    move v8, v6

    .line 2119158
    goto/16 :goto_3

    .line 2119159
    :cond_12
    if-eqz p0, :cond_13

    move v6, v7

    .line 2119160
    goto/16 :goto_4

    .line 2119161
    :cond_13
    if-nez v2, :cond_2

    move v6, v7

    goto/16 :goto_4

    :cond_14
    move v3, v6

    goto/16 :goto_6

    :cond_15
    move v3, v6

    goto/16 :goto_6

    :cond_16
    move v3, v6

    goto/16 :goto_7

    :cond_17
    move v3, v6

    goto/16 :goto_7

    :cond_18
    move v5, v6

    goto/16 :goto_8

    :cond_19
    const/4 v3, 0x0

    goto/16 :goto_9
.end method
