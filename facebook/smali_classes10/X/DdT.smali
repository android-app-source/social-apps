.class public LX/DdT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/user/model/UserKey;

.field private final b:LX/2Og;

.field private final c:LX/2Oj;

.field public final d:LX/2Oi;


# direct methods
.method public constructor <init>(Lcom/facebook/user/model/UserKey;LX/2Og;LX/2Oj;LX/2Oi;)V
    .locals 0
    .param p1    # Lcom/facebook/user/model/UserKey;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserKey;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2019537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2019538
    iput-object p1, p0, LX/DdT;->a:Lcom/facebook/user/model/UserKey;

    .line 2019539
    iput-object p2, p0, LX/DdT;->b:LX/2Og;

    .line 2019540
    iput-object p3, p0, LX/DdT;->c:LX/2Oj;

    .line 2019541
    iput-object p4, p0, LX/DdT;->d:LX/2Oi;

    .line 2019542
    return-void
.end method

.method public static b(LX/0QB;)LX/DdT;
    .locals 5

    .prologue
    .line 2019568
    new-instance v4, LX/DdT;

    invoke-static {p0}, LX/2Ot;->b(LX/0QB;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v1

    check-cast v1, LX/2Og;

    invoke-static {p0}, LX/2Oj;->a(LX/0QB;)LX/2Oj;

    move-result-object v2

    check-cast v2, LX/2Oj;

    invoke-static {p0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v3

    check-cast v3, LX/2Oi;

    invoke-direct {v4, v0, v1, v2, v3}, LX/DdT;-><init>(Lcom/facebook/user/model/UserKey;LX/2Og;LX/2Oj;LX/2Oi;)V

    .line 2019569
    return-object v4
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Px;
    .locals 8
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2019543
    if-nez p1, :cond_0

    .line 2019544
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2019545
    :goto_0
    return-object v0

    .line 2019546
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_3

    .line 2019547
    const/4 v0, 0x0

    .line 2019548
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2019549
    if-eqz p1, :cond_2

    iget-object v3, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v4, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v3, v4, :cond_2

    .line 2019550
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2019551
    iget-wide v4, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2019552
    :cond_1
    if-nez v0, :cond_2

    .line 2019553
    iget-object v3, p0, LX/DdT;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2019554
    :cond_2
    iget-object v3, p0, LX/DdT;->d:LX/2Oi;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/2Oi;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2019555
    goto :goto_0

    .line 2019556
    :cond_3
    iget-object v0, p0, LX/DdT;->b:LX/2Og;

    invoke-virtual {v0, p1}, LX/2Og;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2019557
    const/4 v1, 0x0

    .line 2019558
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2019559
    if-eqz v0, :cond_6

    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    if-eqz v2, :cond_6

    .line 2019560
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_6

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2019561
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v7

    .line 2019562
    if-eqz v7, :cond_5

    if-eqz v1, :cond_4

    iget-object p1, p0, LX/DdT;->a:Lcom/facebook/user/model/UserKey;

    invoke-static {p1, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2019563
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2019564
    :cond_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2019565
    :cond_6
    iget-object v2, p0, LX/DdT;->d:LX/2Oi;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2Oi;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 2019566
    move-object v0, v1

    .line 2019567
    goto/16 :goto_0
.end method
