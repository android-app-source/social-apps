.class public final LX/E6e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/E6f;


# direct methods
.method public constructor <init>(LX/E6f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2080437
    iput-object p1, p0, LX/E6e;->c:LX/E6f;

    iput-object p2, p0, LX/E6e;->a:Ljava/lang/String;

    iput-object p3, p0, LX/E6e;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const v0, 0x18328279

    invoke-static {v6, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2080438
    iget-object v1, p0, LX/E6e;->c:LX/E6f;

    iget-object v1, v1, LX/E6f;->a:LX/E1i;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/E6e;->a:Ljava/lang/String;

    iget-object v4, p0, LX/E6e;->b:Ljava/lang/String;

    sget-object v5, LX/Cfc;->PAGE_NEW_ACTIVITY_COUNT_TAP:LX/Cfc;

    invoke-virtual {v1, v2, v3, v4, v5}, LX/E1i;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)LX/Cfl;

    move-result-object v1

    .line 2080439
    iget-object v2, v1, LX/Cfl;->d:Landroid/content/Intent;

    if-eqz v2, :cond_0

    .line 2080440
    iget-object v2, v1, LX/Cfl;->d:Landroid/content/Intent;

    const-string v3, "extra_is_admin"

    invoke-virtual {v2, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2080441
    iget-object v2, v1, LX/Cfl;->d:Landroid/content/Intent;

    const-string v3, "extra_page_tab"

    sget-object v4, LX/BEQ;->ACTIVITY:LX/BEQ;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2080442
    :cond_0
    iget-object v2, p0, LX/E6e;->c:LX/E6f;

    iget-object v3, p0, LX/E6e;->a:Ljava/lang/String;

    .line 2080443
    invoke-virtual {v2, v3, v1, p1}, LX/Cfk;->a(Ljava/lang/String;LX/Cfl;Landroid/view/View;)V

    .line 2080444
    const v1, 0x7d2a5212

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
