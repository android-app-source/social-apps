.class public final LX/EW2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:LX/EWC;


# direct methods
.method public constructor <init>(LX/EWC;)V
    .locals 0

    .prologue
    .line 2129297
    iput-object p1, p0, LX/EW2;->a:LX/EWC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2129298
    iget-object v0, p0, LX/EW2;->a:LX/EWC;

    .line 2129299
    iget-boolean v1, v0, LX/EWC;->s:Z

    move v0, v1

    .line 2129300
    if-nez v0, :cond_0

    iget-object v0, p0, LX/EW2;->a:LX/EWC;

    invoke-virtual {v0}, LX/EWC;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/EW2;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->Q:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 2129301
    iget-object v0, p0, LX/EW2;->a:LX/EWC;

    iget-object v0, v0, LX/EWC;->Q:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 2129302
    :cond_0
    return-void
.end method
