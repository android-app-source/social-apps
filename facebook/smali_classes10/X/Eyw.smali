.class public LX/Eyw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0oz;

.field private final c:LX/Eyv;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0oz;LX/Eyv;Ljava/lang/String;)V
    .locals 0
    .param p3    # LX/Eyv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2186707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2186708
    iput-object p1, p0, LX/Eyw;->a:LX/0Zb;

    .line 2186709
    iput-object p2, p0, LX/Eyw;->b:LX/0oz;

    .line 2186710
    iput-object p3, p0, LX/Eyw;->c:LX/Eyv;

    .line 2186711
    iput-object p4, p0, LX/Eyw;->d:Ljava/lang/String;

    .line 2186712
    return-void
.end method

.method public static c(LX/Eyw;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 2186713
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "sprout"

    .line 2186714
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2186715
    move-object v0, v0

    .line 2186716
    iget-object v1, p0, LX/Eyw;->d:Ljava/lang/String;

    .line 2186717
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2186718
    move-object v0, v0

    .line 2186719
    const-string v1, "sprout_source"

    iget-object v2, p0, LX/Eyw;->c:LX/Eyv;

    invoke-virtual {v2}, LX/Eyv;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method
