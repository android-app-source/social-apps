.class public final LX/DJc;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# static fields
.field private static final a:LX/0jK;


# instance fields
.field private final b:LX/0Uh;

.field public final c:LX/0iA;

.field private final d:LX/3iT;

.field public final e:LX/0gd;

.field private final f:LX/DIw;

.field private final g:LX/DJM;

.field private final h:LX/34x;

.field public final i:Z

.field private final j:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public final k:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

.field public m:LX/DIv;

.field private final n:Landroid/text/TextWatcher;

.field private final o:LX/DJT;

.field private final p:LX/DJH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1985481
    const-class v0, LX/DJc;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/DJc;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Uh;LX/0iA;LX/3iT;LX/0gd;LX/DIw;LX/DJM;LX/34x;LX/B5j;LX/0Or;)V
    .locals 3
    .param p9    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0iA;",
            "LX/3iT;",
            "LX/0gd;",
            "LX/DIw;",
            "LX/DJM;",
            "LX/34x;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1985482
    invoke-direct {p0, p1, p9}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1985483
    new-instance v1, LX/DJS;

    invoke-direct {v1, p0}, LX/DJS;-><init>(LX/DJc;)V

    iput-object v1, p0, LX/DJc;->n:Landroid/text/TextWatcher;

    .line 1985484
    new-instance v1, LX/DJT;

    invoke-direct {v1, p0}, LX/DJT;-><init>(LX/DJc;)V

    iput-object v1, p0, LX/DJc;->o:LX/DJT;

    .line 1985485
    new-instance v1, LX/DJU;

    invoke-direct {v1, p0}, LX/DJU;-><init>(LX/DJc;)V

    iput-object v1, p0, LX/DJc;->p:LX/DJH;

    .line 1985486
    iput-object p2, p0, LX/DJc;->b:LX/0Uh;

    .line 1985487
    iput-object p3, p0, LX/DJc;->c:LX/0iA;

    .line 1985488
    iput-object p4, p0, LX/DJc;->d:LX/3iT;

    .line 1985489
    iput-object p5, p0, LX/DJc;->e:LX/0gd;

    .line 1985490
    iput-object p6, p0, LX/DJc;->f:LX/DIw;

    .line 1985491
    iput-object p7, p0, LX/DJc;->g:LX/DJM;

    .line 1985492
    iput-object p8, p0, LX/DJc;->h:LX/34x;

    .line 1985493
    iput-object p10, p0, LX/DJc;->k:LX/0Or;

    .line 1985494
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    .line 1985495
    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iput-object v2, p0, LX/DJc;->j:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1985496
    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/DJc;->j:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v1}, LX/88g;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/DJc;->b:LX/0Uh;

    sget v2, LX/7l1;->c:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, LX/DJc;->i:Z

    .line 1985497
    return-void
.end method

.method public static a$redex0(LX/DJc;J)V
    .locals 6

    .prologue
    .line 1985498
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/DJc;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 1985499
    iget-object v3, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->a()V

    .line 1985500
    iget-object v3, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getMarketplaceId()J

    move-result-wide v3

    cmp-long v3, v3, p1

    if-eqz v3, :cond_1

    .line 1985501
    iget-object v3, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v3, :cond_0

    .line 1985502
    iget-object v3, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v3

    iput-object v3, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 1985503
    :cond_0
    iget-object v3, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v3, p1, p2}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setMarketplaceId(J)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 1985504
    iget-object v3, v0, LX/0jL;->a:LX/0cA;

    sget-object v4, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v3, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1985505
    :cond_1
    move-object v0, v0

    .line 1985506
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1985507
    return-void
.end method

.method public static aN(LX/DJc;)V
    .locals 12

    .prologue
    .line 1985508
    invoke-static {p0}, LX/DJc;->aU(LX/DJc;)Ljava/util/List;

    move-result-object v0

    .line 1985509
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1985510
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    .line 1985511
    iget-object v2, p0, LX/DJc;->g:LX/DJM;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v1

    .line 1985512
    new-instance v4, LX/DJL;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v2}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    move-object v9, v3

    move-object v10, v0

    move-object v11, v1

    invoke-direct/range {v4 .. v11}, LX/DJL;-><init>(LX/0tX;Ljava/util/concurrent/Executor;LX/03V;LX/0Xl;Ljava/lang/String;Ljava/util/List;LX/21D;)V

    .line 1985513
    move-object v0, v4

    .line 1985514
    iget-object v1, v0, LX/DJL;->h:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 1985515
    :cond_0
    return-void
.end method

.method private aO()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1985516
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    .line 1985517
    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v5

    .line 1985518
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setCurrencyCode(Ljava/lang/String;)V

    .line 1985519
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getGroupCommerceCategories()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setCategories(LX/0Px;)V

    .line 1985520
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getIsCategoryOptional()Z

    move-result v3

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getPrefillCategoryId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v3, v6}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(ZLjava/lang/String;)V

    .line 1985521
    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getProductItemLocationPickerSettings()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    move-result-object v0

    .line 1985522
    iget-object v6, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->getUseZipCode()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v1

    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->getIsCompulsory()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v6, v3, v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(ZZ)V

    .line 1985523
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    new-instance v3, LX/DJP;

    invoke-direct {v3, p0}, LX/DJP;-><init>(LX/DJc;)V

    invoke-virtual {v0, v3}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setOnCategoryClickedListener(Landroid/view/View$OnClickListener;)V

    .line 1985524
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    new-instance v3, LX/DJQ;

    invoke-direct {v3, p0}, LX/DJQ;-><init>(LX/DJc;)V

    invoke-virtual {v0, v3}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setOnLocationClickedListener(Landroid/view/View$OnClickListener;)V

    .line 1985525
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v3, p0, LX/DJc;->n:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Landroid/text/TextWatcher;)V

    .line 1985526
    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getMarketplaceCrossPostSettingModel()Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    move-result-object v0

    .line 1985527
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->a()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v4}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v5, LX/2rw;->MARKETPLACE:LX/2rw;

    if-eq v3, v5, :cond_5

    .line 1985528
    :goto_2
    iget-boolean v3, p0, LX/DJc;->i:Z

    if-eqz v3, :cond_6

    .line 1985529
    iget-object v3, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v5, p0, LX/DJc;->p:LX/DJH;

    invoke-virtual {v3, v0, v1, v5}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;ZLX/DJH;)V

    .line 1985530
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    invoke-virtual {v3}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    invoke-static {v3}, LX/DJc;->b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2, v2, v2}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Ljava/lang/String;ZZI)V

    .line 1985531
    :cond_0
    :goto_3
    if-eqz v1, :cond_1

    .line 1985532
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldPostToMarketplaceByDefault()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setShouldCrossPostToMarketPlace(Z)V

    .line 1985533
    :cond_1
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0, v2}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setVisibility(I)V

    .line 1985534
    iget-boolean v0, p0, LX/DJc;->i:Z

    if-nez v0, :cond_2

    .line 1985535
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->b()V

    .line 1985536
    :cond_2
    return-void

    :cond_3
    move v3, v2

    .line 1985537
    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v1, v2

    .line 1985538
    goto :goto_2

    .line 1985539
    :cond_6
    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    .line 1985540
    iget-object v3, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-interface {v4}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v5

    iget-wide v6, v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;Ljava/lang/String;)V

    .line 1985541
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    new-instance v3, LX/DJR;

    invoke-direct {v3, p0}, LX/DJR;-><init>(LX/DJc;)V

    invoke-virtual {v0, v3}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setOnPostToMarketplaceCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_3
.end method

.method public static aS(LX/DJc;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1985542
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    .line 1985543
    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getProductItemLocationPickerSettings()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    move-result-object v0

    .line 1985544
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->getUseZipCode()Z

    move-result v1

    if-eqz v1, :cond_2

    move v4, v2

    .line 1985545
    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->getIsCompulsory()Z

    move-result v0

    if-eqz v0, :cond_3

    move v1, v2

    .line 1985546
    :goto_1
    new-instance v0, LX/5Rj;

    invoke-direct {v0}, LX/5Rj;-><init>()V

    iget-object v6, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v6}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1985547
    iput-object v6, v0, LX/5Rj;->a:Ljava/lang/String;

    .line 1985548
    move-object v0, v0

    .line 1985549
    iget-object v6, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v6}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getDescriptionText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1985550
    iput-object v6, v0, LX/5Rj;->d:Ljava/lang/String;

    .line 1985551
    move-object v6, v0

    .line 1985552
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getPrice()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1985553
    iput-object v0, v6, LX/5Rj;->e:Ljava/lang/Long;

    .line 1985554
    move-object v0, v6

    .line 1985555
    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v6

    .line 1985556
    iput-object v6, v0, LX/5Rj;->f:Ljava/lang/String;

    .line 1985557
    move-object v0, v0

    .line 1985558
    iget-object v6, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    .line 1985559
    iget-object v7, v6, Lcom/facebook/groupcommerce/composer/ComposerSellView;->q:Ljava/lang/String;

    move-object v6, v7

    .line 1985560
    iput-object v6, v0, LX/5Rj;->g:Ljava/lang/String;

    .line 1985561
    move-object v0, v0

    .line 1985562
    iget-object v6, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v6}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getIsCategoryOptional()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1985563
    :goto_2
    iput-boolean v2, v0, LX/5Rj;->k:Z

    .line 1985564
    move-object v0, v0

    .line 1985565
    iput-boolean v4, v0, LX/5Rj;->l:Z

    .line 1985566
    move-object v0, v0

    .line 1985567
    iput-boolean v1, v0, LX/5Rj;->m:Z

    .line 1985568
    move-object v1, v0

    .line 1985569
    if-nez v4, :cond_7

    .line 1985570
    iget-object v0, p0, LX/DJc;->m:LX/DIv;

    .line 1985571
    iget-object v2, v0, LX/DIv;->k:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    move-object v2, v2

    .line 1985572
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getStructuredLocationText()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 1985573
    :goto_3
    if-nez v2, :cond_6

    .line 1985574
    :goto_4
    iput-object v2, v1, LX/5Rj;->c:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    .line 1985575
    move-object v2, v1

    .line 1985576
    iput-object v0, v2, LX/5Rj;->b:Ljava/lang/String;

    .line 1985577
    :goto_5
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->c()LX/0jJ;

    move-result-object v0

    sget-object v2, LX/DJc;->a:LX/0jK;

    invoke-virtual {v0, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v1}, LX/5Rj;->a()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    .line 1985578
    iget-object v2, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 1985579
    iget-object v2, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v2

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1985580
    iget-object v2, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v2, :cond_0

    .line 1985581
    iget-object v2, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v2

    iput-object v2, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 1985582
    :cond_0
    iget-object v2, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v2, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 1985583
    iget-object v2, v0, LX/0jL;->a:LX/0cA;

    sget-object v3, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v2, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1985584
    :cond_1
    move-object v0, v0

    .line 1985585
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1985586
    return-void

    :cond_2
    move v4, v3

    .line 1985587
    goto/16 :goto_0

    :cond_3
    move v1, v3

    .line 1985588
    goto/16 :goto_1

    :cond_4
    move v2, v3

    .line 1985589
    goto :goto_2

    .line 1985590
    :cond_5
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getStructuredLocationText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1985591
    :cond_6
    iget-object v0, v2, Lcom/facebook/ipc/composer/model/ProductItemPlace;->name:Ljava/lang/String;

    goto :goto_4

    .line 1985592
    :cond_7
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getZipcodeText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1985593
    iput-object v0, v1, LX/5Rj;->b:Ljava/lang/String;

    .line 1985594
    goto :goto_5
.end method

.method public static aT(LX/DJc;)V
    .locals 6

    .prologue
    .line 1985595
    iget-object v0, p0, LX/DJc;->j:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v0}, LX/88g;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1985596
    :goto_0
    return-void

    .line 1985597
    :cond_0
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getSelectedTargets()Ljava/util/List;

    move-result-object v0

    .line 1985598
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1985599
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/DJc;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, LX/DJc;->j:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_0

    .line 1985600
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJh;

    .line 1985601
    iget-object v1, v0, LX/DJh;->a:Ljava/lang/String;

    iget-object v2, p0, LX/DJc;->k:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1985602
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->c()LX/0jJ;

    move-result-object v1

    sget-object v2, LX/DJc;->a:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    new-instance v2, LX/89I;

    iget-object v0, v0, LX/DJh;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    invoke-direct {v2, v4, v5, v0}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v2}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_0

    .line 1985603
    :cond_2
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->c()LX/0jJ;

    move-result-object v1

    sget-object v2, LX/DJc;->a:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    new-instance v2, LX/89I;

    iget-object v3, v0, LX/DJh;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v3, LX/2rw;->GROUP:LX/2rw;

    invoke-direct {v2, v4, v5, v3}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v3, v0, LX/DJh;->b:Ljava/lang/String;

    .line 1985604
    iput-object v3, v2, LX/89I;->c:Ljava/lang/String;

    .line 1985605
    move-object v2, v2

    .line 1985606
    iget-object v0, v0, LX/DJh;->c:Ljava/lang/String;

    .line 1985607
    iput-object v0, v2, LX/89I;->d:Ljava/lang/String;

    .line 1985608
    move-object v0, v2

    .line 1985609
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto/16 :goto_0
.end method

.method public static aU(LX/DJc;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/DJh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1985610
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getSelectedTargets()Ljava/util/List;

    move-result-object v0

    .line 1985611
    iget-object v1, p0, LX/DJc;->j:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v1}, LX/88g;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1985612
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1985613
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1985614
    :cond_0
    const/4 v0, 0x0

    .line 1985615
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final X()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985446
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final Z()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985616
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 7
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1985617
    packed-switch p1, :pswitch_data_0

    .line 1985618
    :goto_0
    return-void

    .line 1985619
    :pswitch_0
    const-string p1, "category_id"

    invoke-virtual {p3, p1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1985620
    const-string p1, "category_id"

    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1985621
    iget-object p2, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-virtual {p2, p1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setCategoryID(Ljava/lang/String;)V

    .line 1985622
    invoke-static {p0}, LX/DJc;->aS(LX/DJc;)V

    .line 1985623
    :cond_0
    goto :goto_0

    .line 1985624
    :pswitch_1
    const/4 v3, 0x0

    .line 1985625
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    .line 1985626
    if-nez p2, :cond_1

    .line 1985627
    iget-object v1, p0, LX/DJc;->e:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_CANCEL:LX/0ge;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 1985628
    :goto_1
    goto :goto_0

    .line 1985629
    :cond_1
    const/4 v1, -0x1

    if-eq p2, v1, :cond_2

    .line 1985630
    iget-object v1, p0, LX/DJc;->e:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_FAILURE:LX/0ge;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    goto :goto_1

    .line 1985631
    :cond_2
    iget-object v1, p0, LX/DJc;->e:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION:LX/0ge;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 1985632
    const-string v0, "extra_xed_location"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1985633
    if-eqz v0, :cond_4

    .line 1985634
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setStructuredLocationText(Ljava/lang/CharSequence;)V

    .line 1985635
    iget-object v0, p0, LX/DJc;->m:LX/DIv;

    .line 1985636
    iput-object v3, v0, LX/DIv;->k:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    .line 1985637
    :cond_3
    :goto_2
    invoke-static {p0}, LX/DJc;->aS(LX/DJc;)V

    goto :goto_1

    .line 1985638
    :cond_4
    const-string v0, "text_only_place"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1985639
    iget-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    const-string v1, "text_only_place"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setStructuredLocationText(Ljava/lang/CharSequence;)V

    .line 1985640
    iget-object v0, p0, LX/DJc;->m:LX/DIv;

    .line 1985641
    iput-object v3, v0, LX/DIv;->k:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    .line 1985642
    goto :goto_2

    .line 1985643
    :cond_5
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1985644
    if-eqz v0, :cond_3

    .line 1985645
    new-instance v1, LX/5Rm;

    invoke-direct {v1}, LX/5Rm;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 1985646
    iput-object v2, v1, LX/5Rm;->a:Ljava/lang/String;

    .line 1985647
    move-object v1, v1

    .line 1985648
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    .line 1985649
    iput-object v2, v1, LX/5Rm;->b:Ljava/lang/String;

    .line 1985650
    move-object v1, v1

    .line 1985651
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 1985652
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v2

    .line 1985653
    iput-wide v2, v1, LX/5Rm;->c:D

    .line 1985654
    move-object v2, v1

    .line 1985655
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v4

    .line 1985656
    iput-wide v4, v2, LX/5Rm;->d:D

    .line 1985657
    :cond_6
    invoke-virtual {v1}, LX/5Rm;->a()Lcom/facebook/ipc/composer/model/ProductItemPlace;

    move-result-object v0

    .line 1985658
    iget-object v1, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v2, v0, Lcom/facebook/ipc/composer/model/ProductItemPlace;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setStructuredLocationText(Ljava/lang/CharSequence;)V

    .line 1985659
    iget-object v1, p0, LX/DJc;->m:LX/DIv;

    .line 1985660
    iput-object v0, v1, LX/DIv;->k:Lcom/facebook/ipc/composer/model/ProductItemPlace;

    .line 1985661
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x51
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewStub;)V
    .locals 5

    .prologue
    .line 1985662
    iget-object v0, p0, LX/DJc;->b:LX/0Uh;

    sget v1, LX/7l1;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1985663
    const v0, 0x7f0312fa

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1985664
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 1985665
    sget-object v1, LX/DJf;->BLACK:LX/DJf;

    iget-object v2, p0, LX/DJc;->h:LX/34x;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082f13

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/34x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/DJg;->b(LX/DJf;Landroid/view/View;Ljava/lang/String;)V

    .line 1985666
    :cond_0
    return-void
.end method

.method public final aC()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985667
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aF()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985668
    new-instance v0, LX/DJa;

    invoke-direct {v0, p0}, LX/DJa;-><init>(LX/DJc;)V

    return-object v0
.end method

.method public final aG()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985479
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aH()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985480
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985440
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aK()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "LX/0Px",
            "<",
            "LX/3l4;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985441
    new-instance v0, LX/DJV;

    invoke-direct {v0, p0}, LX/DJV;-><init>(LX/DJc;)V

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985442
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ab()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985443
    new-instance v0, LX/DJW;

    invoke-direct {v0, p0}, LX/DJW;-><init>(LX/DJc;)V

    return-object v0
.end method

.method public final ac()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985444
    new-instance v0, LX/DJX;

    invoke-direct {v0, p0}, LX/DJX;-><init>(LX/DJc;)V

    return-object v0
.end method

.method public final ad()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985445
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ae()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985447
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final af()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985448
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final am()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985449
    new-instance v0, LX/DJY;

    invoke-direct {v0, p0}, LX/DJY;-><init>(LX/DJc;)V

    return-object v0
.end method

.method public final aq()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985450
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ar()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985451
    new-instance v0, LX/DJZ;

    invoke-direct {v0, p0}, LX/DJZ;-><init>(LX/DJc;)V

    return-object v0
.end method

.method public final as()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985452
    sget-object v0, LX/ARN;->a:LX/ARN;

    return-object v0
.end method

.method public final at()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985453
    sget-object v0, LX/ARN;->a:LX/ARN;

    return-object v0
.end method

.method public final av()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1985454
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final b(Landroid/view/ViewStub;)Z
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x1

    .line 1985455
    const v0, 0x7f0313be

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1985456
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iput-object v0, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    .line 1985457
    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v0

    invoke-virtual {v0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    .line 1985458
    invoke-interface {v3}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1985459
    invoke-interface {v3}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    .line 1985460
    iget-object v2, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v4, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1985461
    iget-object v2, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v4, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setZipcodeText(Ljava/lang/CharSequence;)V

    .line 1985462
    iget-object v2, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v4, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setStructuredLocationText(Ljava/lang/CharSequence;)V

    .line 1985463
    iget-object v2, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 1985464
    iget-object v2, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v4, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->currencyCode:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 1985465
    :cond_0
    iget-object v2, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1985466
    iget-object v2, p0, LX/DJc;->l:Lcom/facebook/groupcommerce/composer/ComposerSellView;

    iget-object v0, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->description:Ljava/lang/String;

    iget-object v4, p0, LX/DJc;->d:LX/3iT;

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, LX/8of;->a(Ljava/lang/CharSequence;LX/3iT;LX/8oi;)LX/8of;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setDescriptionText(Ljava/lang/CharSequence;)V

    .line 1985467
    :cond_1
    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getProductItemLocationPickerSettings()Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;

    move-result-object v0

    .line 1985468
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ProductItemLocationPickerSettings;->getUseZipCode()Z

    move-result v0

    if-eqz v0, :cond_3

    move v5, v10

    .line 1985469
    :goto_0
    if-nez v5, :cond_4

    invoke-interface {v3}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->pickupDeliveryInfo:Ljava/lang/String;

    if-nez v0, :cond_4

    :cond_2
    move v4, v10

    .line 1985470
    :goto_1
    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getMarketplaceCrossPostSettingModel()Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    move-result-object v0

    .line 1985471
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    if-nez v0, :cond_5

    move v6, v10

    .line 1985472
    :goto_2
    iget-object v1, p0, LX/DJc;->f:LX/DIw;

    iget-object v2, p0, LX/DJc;->o:LX/DJT;

    iget-boolean v7, p0, LX/DJc;->i:Z

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v8, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual/range {v1 .. v9}, LX/DIw;->a(LX/DJT;LX/0j7;ZZZZJ)LX/DIv;

    move-result-object v0

    iput-object v0, p0, LX/DJc;->m:LX/DIv;

    .line 1985473
    iget-object v0, p0, LX/DJc;->m:LX/DIv;

    invoke-virtual {v0}, LX/DIv;->a()V

    .line 1985474
    invoke-direct {p0}, LX/DJc;->aO()V

    .line 1985475
    return v10

    :cond_3
    move v5, v1

    .line 1985476
    goto :goto_0

    :cond_4
    move v4, v1

    .line 1985477
    goto :goto_1

    :cond_5
    move v6, v1

    .line 1985478
    goto :goto_2
.end method
