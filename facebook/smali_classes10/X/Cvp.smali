.class public LX/Cvp;
.super LX/CvH;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Zb;

.field private final c:LX/0kb;

.field private final d:LX/Cww;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Qc;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cwi;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Uh;

.field private final h:LX/2Sc;

.field private final i:LX/0TD;

.field private final j:LX/0TD;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Be;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

.field public m:Ljava/lang/String;

.field public n:I

.field public o:I

.field private p:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field private q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1949634
    const-class v0, LX/Cvp;

    sput-object v0, LX/Cvp;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0kb;LX/Cww;LX/0Ot;LX/0Ot;LX/0Uh;LX/2Sc;LX/0TD;LX/0TD;LX/0Ot;)V
    .locals 3
    .param p8    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p9    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0kb;",
            "LX/Cww;",
            "LX/0Ot",
            "<",
            "LX/3Qc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cwi;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2Sc;",
            "LX/0TD;",
            "LX/0TD;",
            "LX/0Ot",
            "<",
            "LX/7Be;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1949616
    invoke-direct {p0}, LX/CvH;-><init>()V

    .line 1949617
    sget-object v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iput-object v0, p0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 1949618
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cvp;->s:Ljava/util/List;

    .line 1949619
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cvp;->t:Ljava/util/List;

    .line 1949620
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Cvp;->u:Ljava/util/List;

    .line 1949621
    iput-object p1, p0, LX/Cvp;->b:LX/0Zb;

    .line 1949622
    iput-object p2, p0, LX/Cvp;->c:LX/0kb;

    .line 1949623
    iput-object p3, p0, LX/Cvp;->d:LX/Cww;

    .line 1949624
    iput-object p4, p0, LX/Cvp;->e:LX/0Ot;

    .line 1949625
    iput-object p5, p0, LX/Cvp;->f:LX/0Ot;

    .line 1949626
    iput-object p6, p0, LX/Cvp;->g:LX/0Uh;

    .line 1949627
    iput-object p7, p0, LX/Cvp;->h:LX/2Sc;

    .line 1949628
    iput-object p8, p0, LX/Cvp;->i:LX/0TD;

    .line 1949629
    iput-object p9, p0, LX/Cvp;->j:LX/0TD;

    .line 1949630
    iput-object p10, p0, LX/Cvp;->k:LX/0Ot;

    .line 1949631
    new-instance v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v1, p0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v1, v1, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/search/logging/api/SearchTypeaheadSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 1949632
    const-string v0, ""

    invoke-virtual {p0, v0}, LX/Cvp;->a(Ljava/lang/String;)V

    .line 1949633
    return-void
.end method

.method private static a(LX/Cvp;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;LX/0Px;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;LX/7B5;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3
    .param p5    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/CwU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "LX/CwU;",
            "LX/Cwd;",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7B5;",
            "Z)",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;"
        }
    .end annotation

    .prologue
    .line 1949601
    invoke-static {p0, p1}, LX/Cvp;->c(LX/Cvp;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1949602
    const-string v0, "selected_type"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949603
    const-string v2, "selected_text"

    iget-object v0, p0, LX/Cvp;->d:LX/Cww;

    invoke-virtual {p3, v0}, Lcom/facebook/search/model/TypeaheadUnit;->a(LX/Cwh;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949604
    const-string v0, "selected_input_query"

    invoke-virtual {v1, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949605
    const-string v0, "selected_position"

    invoke-static {p3, p5}, LX/CvH;->a(Lcom/facebook/search/model/TypeaheadUnit;LX/0Px;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949606
    const-string v0, "selected_is_bootstrap_entity"

    invoke-static {p3}, LX/Cvp;->a(Lcom/facebook/search/model/TypeaheadUnit;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949607
    const-string v0, "last_state"

    invoke-virtual {v1, v0, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949608
    invoke-static {v1, p8}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 1949609
    invoke-static {v1, p9}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7B5;)V

    .line 1949610
    iget-object v0, p8, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1949611
    invoke-static {v1, p7, v0}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/Cwd;LX/103;)V

    .line 1949612
    invoke-virtual {p3, v1}, Lcom/facebook/search/model/TypeaheadUnit;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949613
    if-eqz p10, :cond_0

    .line 1949614
    invoke-direct {p0, v1}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949615
    :cond_0
    return-object v1
.end method

.method private static a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7HZ;)V
    .locals 2

    .prologue
    .line 1949593
    const-string v0, "fetch_state"

    invoke-virtual {p2}, LX/7HZ;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949594
    sget-object v0, LX/7HZ;->ERROR:LX/7HZ;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, LX/Cvp;->h:LX/2Sc;

    .line 1949595
    iget-object v1, v0, LX/2Sc;->g:Ljava/lang/Throwable;

    move-object v0, v1

    .line 1949596
    if-eqz v0, :cond_0

    .line 1949597
    const-string v0, "error_message"

    iget-object v1, p0, LX/Cvp;->h:LX/2Sc;

    .line 1949598
    iget-object p0, v1, LX/2Sc;->g:Ljava/lang/Throwable;

    move-object v1, p0

    .line 1949599
    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949600
    :cond_0
    return-void
.end method

.method private static a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1949591
    iget-object v0, p0, LX/Cvp;->i:LX/0TD;

    new-instance v1, LX/Cvn;

    invoke-direct {v1, p0, p2}, LX/Cvn;-><init>(LX/Cvp;Ljava/util/List;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/Cvo;

    invoke-direct {v1, p0, p1, p2, p3}, LX/Cvo;-><init>(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;Z)V

    iget-object v2, p0, LX/Cvp;->j:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1949592
    return-void
.end method

.method private static a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;Z)V
    .locals 7

    .prologue
    .line 1949575
    iget-object v0, p0, LX/Cvp;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1949576
    const-string v1, "null_state_candidate_results"

    iget-object v3, p0, LX/Cvp;->s:Ljava/util/List;

    .line 1949577
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v5, v0

    .line 1949578
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 1949579
    move-object v0, p0

    move-object v2, p1

    move v4, p2

    invoke-static/range {v0 .. v6}, LX/Cvp;->a$redex0(LX/Cvp;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;ZLX/0Px;LX/0Px;)V

    .line 1949580
    :cond_0
    iget-object v0, p0, LX/Cvp;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1949581
    const-string v1, "single_state_candidate_results"

    iget-object v3, p0, LX/Cvp;->t:Ljava/util/List;

    .line 1949582
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v5, v0

    .line 1949583
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 1949584
    move-object v0, p0

    move-object v2, p1

    move v4, p2

    invoke-static/range {v0 .. v6}, LX/Cvp;->a$redex0(LX/Cvp;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;ZLX/0Px;LX/0Px;)V

    .line 1949585
    :cond_1
    iget-object v0, p0, LX/Cvp;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1949586
    const-string v1, "scoped_singlestate_candidate_results"

    iget-object v3, p0, LX/Cvp;->u:Ljava/util/List;

    .line 1949587
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v5, v0

    .line 1949588
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 1949589
    move-object v0, p0

    move-object v2, p1

    move v4, p2

    invoke-static/range {v0 .. v6}, LX/Cvp;->a$redex0(LX/Cvp;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;ZLX/0Px;LX/0Px;)V

    .line 1949590
    :cond_2
    return-void
.end method

.method private a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 8

    .prologue
    .line 1949563
    const-string v1, "was_bootstrap_loaded_at_click_time"

    iget-object v0, p0, LX/Cvp;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qc;

    invoke-virtual {v0}, LX/3Qc;->c()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949564
    iget-object v0, p0, LX/Cvp;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qc;

    invoke-virtual {v0}, LX/3Qc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1949565
    const-string v1, "bootstrap_load_time"

    iget-object v0, p0, LX/Cvp;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qc;

    .line 1949566
    invoke-static {v0}, LX/3Qc;->j(LX/3Qc;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v0, LX/3Qc;->k:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-static {v0}, LX/3Qc;->l(LX/3Qc;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    :goto_0
    move-wide v2, v4

    .line 1949567
    invoke-virtual {p1, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949568
    :cond_0
    iget-object v0, p0, LX/Cvp;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qc;

    .line 1949569
    iget-object v1, v0, LX/3Qc;->p:LX/3Ql;

    sget-object v2, LX/3Ql;->NO_ERROR:LX/3Ql;

    if-eq v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v0, v1

    .line 1949570
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1949571
    const-string v1, "bootstrap_load_error"

    iget-object v0, p0, LX/Cvp;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qc;

    .line 1949572
    iget-object v2, v0, LX/3Qc;->p:LX/3Ql;

    invoke-virtual {v2}, LX/3Ql;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 1949573
    invoke-virtual {p1, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949574
    :cond_1
    return-void

    :cond_2
    iget-object v4, v0, LX/3Qc;->k:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7B5;)V
    .locals 2
    .param p1    # LX/7B5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1949557
    if-eqz p1, :cond_0

    .line 1949558
    sget-object v0, LX/7B5;->TAB:LX/7B5;

    if-ne p1, v0, :cond_1

    .line 1949559
    const-string v0, "typeahead_mode"

    const-string v1, "scoped_tabbed"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949560
    :cond_0
    :goto_0
    return-void

    .line 1949561
    :cond_1
    sget-object v0, LX/7B5;->PILL:LX/7B5;

    if-ne p1, v0, :cond_0

    .line 1949562
    const-string v0, "typeahead_mode"

    const-string v1, "scoped_pill"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/Cwd;LX/103;)V
    .locals 2

    .prologue
    .line 1949553
    sget-object v0, LX/Cwd;->SCOPED:LX/Cwd;

    if-ne p1, v0, :cond_0

    .line 1949554
    const-string v0, "filter"

    invoke-virtual {p0, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949555
    :goto_0
    return-void

    .line 1949556
    :cond_0
    const-string v0, "filter"

    const-string v1, "global"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 5
    .param p1    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1949530
    if-eqz p1, :cond_0

    .line 1949531
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1949532
    if-nez v0, :cond_1

    .line 1949533
    :cond_0
    :goto_0
    return-void

    .line 1949534
    :cond_1
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 1949535
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/103;->name()Ljava/lang/String;

    move-result-object v0

    .line 1949536
    :goto_1
    new-instance v1, LX/162;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/162;-><init>(LX/0mC;)V

    .line 1949537
    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1949538
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 1949539
    iget-object v3, p1, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v3, v3

    .line 1949540
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1949541
    const-string v3, "entity_id"

    .line 1949542
    iget-object v4, p1, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v4, v4

    .line 1949543
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949544
    const-string v3, "entity_text"

    .line 1949545
    iget-object v4, p1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v4, v4

    .line 1949546
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949547
    const-string v3, "entity_type"

    invoke-virtual {v2, v3, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949548
    :cond_2
    new-instance v0, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 1949549
    const-string v3, "origin"

    invoke-virtual {v0, v3, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1949550
    const-string v1, "extras"

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1949551
    const-string v1, "search_origin_tracking"

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1949552
    :cond_3
    const-string v0, "unknown"

    goto :goto_1
.end method

.method private static a(Lcom/facebook/search/model/TypeaheadUnit;)Z
    .locals 1

    .prologue
    .line 1949635
    instance-of v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 1949636
    iget-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v0, v0

    .line 1949637
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/Cvp;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;ZLX/0Px;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1949324
    iput-object p2, p0, LX/Cvp;->p:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949325
    iput-object p5, p0, LX/Cvp;->q:LX/0Px;

    .line 1949326
    iput-object p6, p0, LX/Cvp;->r:LX/0Px;

    .line 1949327
    invoke-virtual {p0, p3}, LX/CvH;->a(Ljava/util/List;)LX/162;

    move-result-object v0

    .line 1949328
    const-string v1, "megaphone_visible"

    invoke-virtual {p2, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949329
    invoke-virtual {p2, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949330
    return-void
.end method

.method public static b(LX/0QB;)LX/Cvp;
    .locals 11

    .prologue
    .line 1949331
    new-instance v0, LX/Cvp;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-static {p0}, LX/Cww;->a(LX/0QB;)LX/Cww;

    move-result-object v3

    check-cast v3, LX/Cww;

    const/16 v4, 0x113c

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x32de

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {p0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v7

    check-cast v7, LX/2Sc;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, LX/0TD;

    const/16 v10, 0x32ba

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, LX/Cvp;-><init>(LX/0Zb;LX/0kb;LX/Cww;LX/0Ot;LX/0Ot;LX/0Uh;LX/2Sc;LX/0TD;LX/0TD;LX/0Ot;)V

    .line 1949332
    return-object v0
.end method

.method private static b(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1949333
    iget-object v0, p0, LX/Cvp;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1949334
    :goto_0
    return-void

    .line 1949335
    :cond_0
    const-string v0, "navigation_text"

    iget-object v1, p0, LX/Cvp;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static c(LX/Cvp;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 1949336
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "click"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "typeahead"

    .line 1949337
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1949338
    move-object v0, v0

    .line 1949339
    const-string v1, "typeahead_sid"

    iget-object v2, p0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v2, v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "candidate_result_sid"

    iget-object v2, p0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v2, v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "keypresses"

    iget v2, p0, LX/Cvp;->n:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1949340
    const-string v0, "network_type"

    iget-object v2, p0, LX/Cvp;->c:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949341
    const-string v0, "network_subtype"

    iget-object v2, p0, LX/Cvp;->c:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949342
    const-string v2, "search_type"

    iget-object v0, p0, LX/Cvp;->g:LX/0Uh;

    sget v3, LX/2SU;->e:I

    invoke-virtual {v0, v3}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "graph_search"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1949343
    return-object v1

    .line 1949344
    :cond_0
    const-string v0, "simple_search"

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1949345
    iget-object v0, p0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v0, v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0m9;Lcom/facebook/search/model/TypeaheadUnit;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    .line 1949346
    iget-object v0, p0, LX/Cvp;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwi;

    .line 1949347
    instance-of v1, p2, LX/CwQ;

    if-eqz v1, :cond_0

    move-object v1, p2

    .line 1949348
    check-cast v1, LX/CwQ;

    invoke-interface {v1}, LX/CwQ;->x()LX/3bj;

    move-result-object v1

    invoke-virtual {v1}, LX/3bj;->name()Ljava/lang/String;

    move-result-object v1

    .line 1949349
    const-string v2, "keyword_source"

    invoke-virtual {p1, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949350
    :cond_0
    instance-of v1, p2, LX/CwB;

    if-eqz v1, :cond_7

    move-object v1, p2

    .line 1949351
    check-cast v1, LX/CwB;

    .line 1949352
    const-string v2, "result_style_list"

    invoke-interface {v1}, LX/CwB;->jC_()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949353
    const-string v2, "semantic"

    invoke-interface {v1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949354
    const-string v2, "text"

    invoke-interface {v1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949355
    instance-of v1, p2, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v1, :cond_6

    move-object v1, p2

    .line 1949356
    check-cast v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 1949357
    const-string v2, "is_bootstrap_entity"

    .line 1949358
    iget-boolean v3, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v3, v3

    .line 1949359
    invoke-virtual {p1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1949360
    iget-boolean v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v2, v2

    .line 1949361
    if-eqz v2, :cond_1

    .line 1949362
    const-string v2, "keyword_type"

    .line 1949363
    iget-wide v7, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->o:D

    move-wide v4, v7

    .line 1949364
    invoke-virtual {p1, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 1949365
    :cond_1
    const-string v2, "is_mem_cached_entity"

    invoke-virtual {v0, p2}, LX/Cwi;->a(Lcom/facebook/search/model/TypeaheadUnit;)Z

    move-result v0

    invoke-virtual {p1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1949366
    const-string v0, "keyword_type"

    .line 1949367
    iget-object v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->D:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v2, :cond_f

    iget-object v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->g:LX/CwF;

    invoke-virtual {v2}, LX/CwF;->name()Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v2, v2

    .line 1949368
    invoke-virtual {p1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949369
    iget-object v0, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->E:Ljava/lang/String;

    move-object v0, v0

    .line 1949370
    if-eqz v0, :cond_2

    .line 1949371
    const-string v0, "entity_id"

    .line 1949372
    iget-object v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->E:Ljava/lang/String;

    move-object v2, v2

    .line 1949373
    invoke-virtual {p1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949374
    const-string v0, "connected_state"

    .line 1949375
    iget-boolean v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->F:Z

    move v2, v2

    .line 1949376
    invoke-virtual {p1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1949377
    :cond_2
    iget-object v0, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->v:LX/0P1;

    move-object v0, v0

    .line 1949378
    if-eqz v0, :cond_3

    .line 1949379
    iget-object v0, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->v:LX/0P1;

    move-object v0, v0

    .line 1949380
    const-string v2, "is_spell_correction"

    invoke-virtual {v0, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1949381
    const-string v2, "is_spell_correction"

    .line 1949382
    iget-object v0, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->v:LX/0P1;

    move-object v0, v0

    .line 1949383
    const-string v3, "is_spell_correction"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1949384
    :cond_3
    const-string v0, "fetch_source"

    .line 1949385
    iget-object v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v2, v2

    .line 1949386
    invoke-virtual {v2}, LX/CwI;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949387
    const-string v0, "fetch_mode"

    .line 1949388
    iget-object v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->m:Ljava/lang/String;

    move-object v2, v2

    .line 1949389
    invoke-virtual {p1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949390
    const-string v0, "type"

    const-string v2, "keyword"

    invoke-virtual {p1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949391
    const-string v2, "keyword_source"

    .line 1949392
    iget-object v0, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1949393
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1949394
    iget-object v0, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v0, v0

    .line 1949395
    invoke-virtual {v0}, LX/CwI;->name()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949396
    iget-object v0, p0, LX/Cvp;->r:LX/0Px;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1949397
    const-string v0, "is_failed_bootstrap_entity"

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949398
    :cond_4
    :goto_2
    return-void

    .line 1949399
    :cond_5
    iget-object v0, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1949400
    goto :goto_1

    .line 1949401
    :cond_6
    instance-of v0, p2, Lcom/facebook/search/model/TrendingTypeaheadUnit;

    if-eqz v0, :cond_4

    .line 1949402
    const-string v0, "type"

    const-string v1, "trending"

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949403
    const-string v1, "topic_id"

    move-object v0, p2

    check-cast v0, Lcom/facebook/search/model/TrendingTypeaheadUnit;

    .line 1949404
    iget-object v2, v0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, v2

    .line 1949405
    invoke-virtual {p1, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949406
    iget-object v0, p0, LX/Cvp;->p:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "trending_query_id"

    check-cast p2, Lcom/facebook/search/model/TrendingTypeaheadUnit;

    .line 1949407
    iget-object v2, p2, Lcom/facebook/search/model/TrendingTypeaheadUnit;->h:Ljava/lang/String;

    move-object v2, v2

    .line 1949408
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_2

    .line 1949409
    :cond_7
    instance-of v1, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    if-eqz v1, :cond_9

    .line 1949410
    check-cast p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    .line 1949411
    const-string v0, "semantic"

    .line 1949412
    iget-object v1, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1949413
    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949414
    const-string v0, "text"

    .line 1949415
    iget-object v1, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1949416
    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949417
    const-string v0, "tracking"

    .line 1949418
    iget-object v1, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->m:Ljava/lang/String;

    move-object v1, v1

    .line 1949419
    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949420
    const-string v1, "type"

    sget-object v0, LX/3bj;->ns_trending:LX/3bj;

    .line 1949421
    iget-object v2, p2, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    move-object v2, v2

    .line 1949422
    invoke-virtual {v0, v2}, LX/3bj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "trending"

    :goto_3
    invoke-virtual {p1, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_2

    :cond_8
    const-string v0, "keyword"

    goto :goto_3

    .line 1949423
    :cond_9
    instance-of v1, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v1, :cond_b

    .line 1949424
    check-cast p2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 1949425
    invoke-static {p1, p2}, LX/CvH;->a(LX/0m9;Lcom/facebook/search/model/EntityTypeaheadUnit;)V

    .line 1949426
    const-string v1, "is_bootstrap_entity"

    .line 1949427
    iget-boolean v2, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v2, v2

    .line 1949428
    invoke-virtual {p1, v1, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1949429
    const-string v1, "is_mem_cached_entity"

    invoke-virtual {v0, p2}, LX/Cwi;->a(Lcom/facebook/search/model/TypeaheadUnit;)Z

    move-result v0

    invoke-virtual {p1, v1, v0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1949430
    const-string v0, "is_phonetic_matched_entity"

    .line 1949431
    iget-boolean v1, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->q:Z

    move v1, v1

    .line 1949432
    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1949433
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->p:LX/0P1;

    move-object v0, v0

    .line 1949434
    if-eqz v0, :cond_a

    .line 1949435
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->p:LX/0P1;

    move-object v0, v0

    .line 1949436
    const-string v1, "is_spell_correction"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1949437
    const-string v1, "is_spell_correction"

    .line 1949438
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->p:LX/0P1;

    move-object v0, v0

    .line 1949439
    const-string v2, "is_spell_correction"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v1, v0}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1949440
    :cond_a
    const-string v0, "type"

    .line 1949441
    iget-object v1, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1949442
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949443
    iget-object v0, p0, LX/Cvp;->q:LX/0Px;

    .line 1949444
    iget-object v1, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1949445
    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1949446
    const-string v0, "is_failed_bootstrap_entity"

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto/16 :goto_2

    .line 1949447
    :cond_b
    instance-of v0, p2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    if-eqz v0, :cond_c

    move-object v0, p2

    .line 1949448
    check-cast v0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    .line 1949449
    const-string v1, "semantic"

    .line 1949450
    iget-object v2, v0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1949451
    invoke-virtual {p1, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949452
    const-string v1, "text"

    .line 1949453
    iget-object v2, v0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1949454
    invoke-virtual {p1, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949455
    const-string v1, "result_style_list"

    .line 1949456
    iget-object v2, v0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v2, v2

    .line 1949457
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949458
    const-string v1, "type"

    .line 1949459
    iget-object v2, v0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v2

    .line 1949460
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949461
    const-string v0, "is_recent_entity"

    invoke-virtual {p1, v0, v4}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1949462
    check-cast p2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    invoke-virtual {p2}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1949463
    const-string v0, "type"

    const-string v1, "keyword"

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto/16 :goto_2

    .line 1949464
    :cond_c
    instance-of v0, p2, Lcom/facebook/search/model/ShortcutTypeaheadUnit;

    if-eqz v0, :cond_d

    .line 1949465
    check-cast p2, Lcom/facebook/search/model/ShortcutTypeaheadUnit;

    .line 1949466
    const-string v0, "semantic"

    .line 1949467
    iget-object v1, p2, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1949468
    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949469
    const-string v0, "text"

    .line 1949470
    iget-object v1, p2, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1949471
    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949472
    const-string v0, "result_style_list"

    .line 1949473
    iget-object v1, p2, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1949474
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949475
    const-string v0, "type"

    .line 1949476
    iget-object v1, p2, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1949477
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949478
    const-string v0, "shortcut_path"

    .line 1949479
    iget-object v1, p2, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->g:Landroid/net/Uri;

    move-object v1, v1

    .line 1949480
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949481
    const-string v0, "shortcut_fallback_path"

    .line 1949482
    iget-object v1, p2, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->h:Landroid/net/Uri;

    move-object v1, v1

    .line 1949483
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto/16 :goto_2

    .line 1949484
    :cond_d
    instance-of v0, p2, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;

    if-eqz v0, :cond_e

    .line 1949485
    check-cast p2, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;

    .line 1949486
    const-string v0, "semantic"

    invoke-virtual {p2}, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949487
    const-string v0, "text"

    .line 1949488
    iget-object v1, p2, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->a:Lcom/facebook/placetips/bootstrap/PresenceDescription;

    if-nez v1, :cond_10

    const-string v1, ""

    :goto_4
    move-object v1, v1

    .line 1949489
    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1949490
    const-string v0, "type"

    const-string v1, "place_tip"

    invoke-virtual {p1, v0, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto/16 :goto_2

    .line 1949491
    :cond_e
    iget-object v0, p0, LX/Cvp;->h:LX/2Sc;

    sget-object v1, LX/3Ql;->LOGGING_UNIMPLEMENTED_RESULT_ROW_TYPE:LX/3Ql;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Candidate result logging not implemented for TypeaheadUnit of type %s."

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_f
    iget-object v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->D:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    :cond_10
    iget-object v1, p2, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->a:Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->h()Ljava/lang/String;

    move-result-object v1

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1949492
    new-instance v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v2, v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/facebook/search/logging/api/SearchTypeaheadSession;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/Cvp;->l:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 1949493
    const/4 v0, 0x0

    iput v0, p0, LX/Cvp;->n:I

    .line 1949494
    iget-object v0, p0, LX/Cvp;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1949495
    iget-object v0, p0, LX/Cvp;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1949496
    iget-object v0, p0, LX/Cvp;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1949497
    iput-object p1, p0, LX/Cvp;->m:Ljava/lang/String;

    .line 1949498
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;LX/0Px;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 11
    .param p5    # LX/CwU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/Cwd;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "LX/CwU;",
            "LX/Cwd;",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1949525
    const-string v1, "invalidated_suggestion"

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-static/range {v0 .. v10}, LX/Cvp;->a(LX/Cvp;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;LX/0Px;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;LX/7B5;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1949526
    iget-object v1, p0, LX/Cvp;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1949527
    const/4 v1, 0x3

    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1949528
    invoke-virtual {v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 1949529
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;LX/0Px;ZLX/7HZ;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;LX/7B5;J)V
    .locals 13
    .param p7    # LX/CwU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/Cwd;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/7B5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;Z",
            "LX/7HZ;",
            "LX/CwU;",
            "LX/Cwd;",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7B5;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 1949499
    const-string v3, "selection"

    const/4 v12, 0x1

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-static/range {v2 .. v12}, LX/Cvp;->a(LX/Cvp;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;LX/0Px;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;LX/7B5;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1949500
    invoke-static {p0, v2}, LX/Cvp;->b(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949501
    move-object/from16 v0, p6

    invoke-static {p0, v2, v0}, LX/Cvp;->a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7HZ;)V

    .line 1949502
    move/from16 v0, p5

    invoke-static {p0, v2, v0}, LX/Cvp;->a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;Z)V

    .line 1949503
    move-object/from16 v0, p3

    invoke-virtual {p0, v0}, LX/Cvp;->a(Ljava/lang/String;)V

    .line 1949504
    move-wide/from16 v0, p11

    invoke-virtual {v2, v0, v1}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->a(J)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    .line 1949505
    move-object/from16 v0, p4

    move/from16 v1, p5

    invoke-static {p0, v2, v0, v1}, LX/Cvp;->a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;Z)V

    .line 1949506
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZLX/7HZ;LX/CwU;)V
    .locals 3
    .param p6    # LX/CwU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "LX/7HZ;",
            "LX/CwU;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1949507
    const-string v0, "end_see_more"

    invoke-static {p0, v0}, LX/Cvp;->c(LX/Cvp;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "selected_input_query"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "session_end_reason"

    const-string v2, "selected_see_more"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "see_more_source"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "last_state"

    invoke-virtual {v0, v1, p6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1949508
    invoke-static {p0, v0}, LX/Cvp;->b(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949509
    invoke-direct {p0, v0}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949510
    invoke-static {p0, v0, p5}, LX/Cvp;->a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7HZ;)V

    .line 1949511
    invoke-static {p0, v0, p2, p4}, LX/Cvp;->a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;Z)V

    .line 1949512
    invoke-virtual {p0, p1}, LX/Cvp;->a(Ljava/lang/String;)V

    .line 1949513
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;ZLX/7HZ;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;LX/7B5;)V
    .locals 3
    .param p5    # LX/CwU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/Cwd;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/7B5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;Z",
            "LX/7HZ;",
            "LX/CwU;",
            "LX/Cwd;",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7B5;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1949514
    const-string v0, "end_back_button"

    invoke-static {p0, v0}, LX/Cvp;->c(LX/Cvp;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "selected_input_query"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "session_end_reason"

    const-string v2, "back_press"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "last_state"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1949515
    invoke-static {v0, p8}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7B5;)V

    .line 1949516
    iget-object v1, p7, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v1, v1

    .line 1949517
    invoke-static {v0, p6, v1}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/Cwd;LX/103;)V

    .line 1949518
    invoke-static {v0, p7}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 1949519
    invoke-static {p0, v0}, LX/Cvp;->b(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949520
    invoke-direct {p0, v0}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1949521
    invoke-static {p0, v0, p4}, LX/Cvp;->a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7HZ;)V

    .line 1949522
    invoke-static {p0, v0, p3}, LX/Cvp;->a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;Z)V

    .line 1949523
    invoke-static {p0, v0, p2, p3}, LX/Cvp;->a(LX/Cvp;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/util/List;Z)V

    .line 1949524
    return-void
.end method
