.class public final LX/DJP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/DJc;


# direct methods
.method public constructor <init>(LX/DJc;)V
    .locals 0

    .prologue
    .line 1985239
    iput-object p1, p0, LX/DJP;->a:LX/DJc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x70f198d

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1985240
    iget-object v1, p0, LX/DJP;->a:LX/DJc;

    .line 1985241
    new-instance v3, Landroid/content/Intent;

    .line 1985242
    iget-object p0, v1, LX/AQ9;->b:Landroid/content/Context;

    move-object p0, p0

    .line 1985243
    const-class p1, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;

    invoke-direct {v3, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1985244
    const-string p0, "categories"

    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object p1

    invoke-virtual {p1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object p1

    invoke-interface {p1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getGroupCommerceCategories()LX/0Px;

    move-result-object p1

    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1985245
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object p0

    invoke-virtual {p0}, LX/B5j;->f()LX/Ar6;

    move-result-object p0

    const/16 p1, 0x51

    invoke-interface {p0, v3, p1}, LX/Ar6;->a(Landroid/content/Intent;I)V

    .line 1985246
    const v1, 0x5bb5393a

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
