.class public LX/Dp6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static volatile c:LX/Dp6;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2043281
    sget-object v0, LX/DpF;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, LX/Dp6;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2043278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2043279
    iput-object p1, p0, LX/Dp6;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2043280
    return-void
.end method

.method public static a(LX/0QB;)LX/Dp6;
    .locals 4

    .prologue
    .line 2043265
    sget-object v0, LX/Dp6;->c:LX/Dp6;

    if-nez v0, :cond_1

    .line 2043266
    const-class v1, LX/Dp6;

    monitor-enter v1

    .line 2043267
    :try_start_0
    sget-object v0, LX/Dp6;->c:LX/Dp6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2043268
    if-eqz v2, :cond_0

    .line 2043269
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2043270
    new-instance p0, LX/Dp6;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/Dp6;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2043271
    move-object v0, p0

    .line 2043272
    sput-object v0, LX/Dp6;->c:LX/Dp6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2043273
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2043274
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2043275
    :cond_1
    sget-object v0, LX/Dp6;->c:LX/Dp6;

    return-object v0

    .line 2043276
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2043277
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/tincan/omnistore/TincanMessage;)LX/Dp5;
    .locals 7

    .prologue
    .line 2043247
    iget-object v0, p1, Lcom/facebook/messaging/tincan/omnistore/TincanMessage;->b:Ljava/nio/ByteBuffer;

    .line 2043248
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 2043249
    invoke-static {}, LX/Dpn;->a()Ljava/security/MessageDigest;

    move-result-object v2

    .line 2043250
    if-nez v2, :cond_1

    .line 2043251
    sget-object v2, LX/Dpn;->a:Ljava/lang/Class;

    const-string v3, "No SHA256 available"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2043252
    const/4 v2, 0x1

    new-array v2, v2, [B

    .line 2043253
    :goto_0
    move-object v1, v2

    .line 2043254
    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    .line 2043255
    new-instance v3, LX/DpD;

    invoke-direct {v3, v0}, LX/DpD;-><init>(Ljava/nio/ByteBuffer;)V

    .line 2043256
    new-instance v4, LX/1sr;

    invoke-direct {v4, v3}, LX/1sr;-><init>(Ljava/io/InputStream;)V

    .line 2043257
    invoke-interface {v2, v4}, LX/1sq;->a(LX/1ss;)LX/1su;

    move-result-object v0

    .line 2043258
    :try_start_0
    invoke-static {v0}, LX/DpN;->b(LX/1su;)LX/DpN;

    move-result-object v0

    .line 2043259
    sget v2, LX/Dp6;->a:I

    iget-object v3, p0, LX/Dp6;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/Dp9;->k:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    add-int/2addr v2, v3

    .line 2043260
    iget-object v3, v0, LX/DpN;->version:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v2, :cond_0

    .line 2043261
    new-instance v1, LX/Dpo;

    iget-object v3, v0, LX/DpN;->version:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v1, v2, v3, v0}, LX/Dpo;-><init>(IILX/DpN;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2043262
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, LX/1sr;->a()V

    throw v0

    .line 2043263
    :cond_0
    :try_start_1
    new-instance v2, LX/Dp5;

    iget-object v3, p1, Lcom/facebook/messaging/tincan/omnistore/TincanMessage;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v0, v1}, LX/Dp5;-><init>(Ljava/lang/String;LX/DpN;[B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2043264
    invoke-virtual {v4}, LX/1sr;->a()V

    return-object v2

    :cond_1
    invoke-virtual {v2, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    goto :goto_0
.end method
