.class public LX/Dor;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Dor;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dos;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dot;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dou;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dov;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Dos;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dot;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dou;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dov;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2042562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2042563
    iput-object p1, p0, LX/Dor;->a:LX/0Ot;

    .line 2042564
    iput-object p2, p0, LX/Dor;->b:LX/0Ot;

    .line 2042565
    iput-object p3, p0, LX/Dor;->c:LX/0Ot;

    .line 2042566
    iput-object p4, p0, LX/Dor;->d:LX/0Ot;

    .line 2042567
    return-void
.end method

.method public static a(LX/0QB;)LX/Dor;
    .locals 7

    .prologue
    .line 2042568
    sget-object v0, LX/Dor;->e:LX/Dor;

    if-nez v0, :cond_1

    .line 2042569
    const-class v1, LX/Dor;

    monitor-enter v1

    .line 2042570
    :try_start_0
    sget-object v0, LX/Dor;->e:LX/Dor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2042571
    if-eqz v2, :cond_0

    .line 2042572
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2042573
    new-instance v3, LX/Dor;

    const/16 v4, 0x2a19

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2a1a

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2a1b

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x2a1c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, LX/Dor;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2042574
    move-object v0, v3

    .line 2042575
    sput-object v0, LX/Dor;->e:LX/Dor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2042576
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2042577
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2042578
    :cond_1
    sget-object v0, LX/Dor;->e:LX/Dor;

    return-object v0

    .line 2042579
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2042580
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
