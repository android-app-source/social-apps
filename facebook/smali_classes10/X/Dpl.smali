.class public LX/Dpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final sender_key:[B

.field public final thread_fbid:Ljava/lang/Long;

.field public final thread_participants:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/DpM;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2047201
    new-instance v0, LX/1sv;

    const-string v1, "ThreadSenderKeyInfo"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Dpl;->b:LX/1sv;

    .line 2047202
    new-instance v0, LX/1sw;

    const-string v1, "thread_fbid"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpl;->c:LX/1sw;

    .line 2047203
    new-instance v0, LX/1sw;

    const-string v1, "sender_key"

    const/16 v2, 0xb

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpl;->d:LX/1sw;

    .line 2047204
    new-instance v0, LX/1sw;

    const-string v1, "thread_participants"

    const/16 v2, 0xd

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Dpl;->e:LX/1sw;

    .line 2047205
    const/4 v0, 0x1

    sput-boolean v0, LX/Dpl;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;[BLjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "[B",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/DpM;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2047078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2047079
    iput-object p1, p0, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    .line 2047080
    iput-object p2, p0, LX/Dpl;->sender_key:[B

    .line 2047081
    iput-object p3, p0, LX/Dpl;->thread_participants:Ljava/util/Map;

    .line 2047082
    return-void
.end method

.method public static b(LX/1su;)LX/Dpl;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2047170
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v4, v0

    move-object v5, v0

    .line 2047171
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2047172
    iget-byte v3, v2, LX/1sw;->b:B

    if-eqz v3, :cond_9

    .line 2047173
    iget-short v3, v2, LX/1sw;->c:S

    packed-switch v3, :pswitch_data_0

    .line 2047174
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047175
    :pswitch_0
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v6, 0xa

    if-ne v3, v6, :cond_0

    .line 2047176
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v5, v2

    goto :goto_0

    .line 2047177
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047178
    :pswitch_1
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v6, 0xb

    if-ne v3, v6, :cond_1

    .line 2047179
    invoke-virtual {p0}, LX/1su;->q()[B

    move-result-object v2

    move-object v4, v2

    goto :goto_0

    .line 2047180
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2047181
    :pswitch_2
    iget-byte v3, v2, LX/1sw;->b:B

    const/16 v6, 0xd

    if-ne v3, v6, :cond_8

    .line 2047182
    invoke-virtual {p0}, LX/1su;->g()LX/7H3;

    move-result-object v6

    .line 2047183
    new-instance v3, Ljava/util/HashMap;

    iget v0, v6, LX/7H3;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    move v0, v1

    .line 2047184
    :goto_1
    iget v2, v6, LX/7H3;->c:I

    if-gez v2, :cond_4

    invoke-static {}, LX/1su;->s()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2047185
    :cond_2
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 2047186
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v8

    .line 2047187
    new-instance v9, Ljava/util/ArrayList;

    iget v2, v8, LX/1u3;->b:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    .line 2047188
    :goto_2
    iget v10, v8, LX/1u3;->b:I

    if-gez v10, :cond_6

    invoke-static {}, LX/1su;->t()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 2047189
    :cond_3
    invoke-static {p0}, LX/DpM;->b(LX/1su;)LX/DpM;

    move-result-object v10

    .line 2047190
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2047191
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2047192
    :cond_4
    iget v2, v6, LX/7H3;->c:I

    if-lt v0, v2, :cond_2

    :cond_5
    move-object v0, v3

    .line 2047193
    goto/16 :goto_0

    .line 2047194
    :cond_6
    iget v10, v8, LX/1u3;->b:I

    if-lt v2, v10, :cond_3

    .line 2047195
    :cond_7
    invoke-interface {v3, v7, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2047196
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2047197
    :cond_8
    iget-byte v2, v2, LX/1sw;->b:B

    invoke-static {p0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2047198
    :cond_9
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2047199
    new-instance v1, LX/Dpl;

    invoke-direct {v1, v5, v4, v0}, LX/Dpl;-><init>(Ljava/lang/Long;[BLjava/util/Map;)V

    .line 2047200
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2047134
    if-eqz p2, :cond_0

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 2047135
    :goto_0
    if-eqz p2, :cond_1

    const-string v0, "\n"

    move-object v1, v0

    .line 2047136
    :goto_1
    if-eqz p2, :cond_2

    const-string v0, " "

    .line 2047137
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ThreadSenderKeyInfo"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2047138
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047139
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047140
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047141
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047142
    const-string v4, "thread_fbid"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047143
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047144
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047145
    iget-object v4, p0, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    if-nez v4, :cond_3

    .line 2047146
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047147
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047148
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047149
    const-string v4, "sender_key"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047150
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047151
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047152
    iget-object v4, p0, LX/Dpl;->sender_key:[B

    if-nez v4, :cond_4

    .line 2047153
    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047154
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047155
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047156
    const-string v4, "thread_participants"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047157
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047158
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047159
    iget-object v0, p0, LX/Dpl;->thread_participants:Ljava/util/Map;

    if-nez v0, :cond_5

    .line 2047160
    const-string v0, "null"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047161
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047162
    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2047163
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2047164
    :cond_0
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_0

    .line 2047165
    :cond_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2047166
    :cond_2
    const-string v0, ""

    goto/16 :goto_2

    .line 2047167
    :cond_3
    iget-object v4, p0, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2047168
    :cond_4
    iget-object v4, p0, LX/Dpl;->sender_key:[B

    add-int/lit8 v5, p1, 0x1

    invoke-static {v4, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 2047169
    :cond_5
    iget-object v0, p0, LX/Dpl;->thread_participants:Ljava/util/Map;

    add-int/lit8 v4, p1, 0x1

    invoke-static {v0, v4, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method

.method public final a(LX/1su;)V
    .locals 6

    .prologue
    .line 2047116
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2047117
    iget-object v0, p0, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2047118
    sget-object v0, LX/Dpl;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2047119
    iget-object v0, p0, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2047120
    :cond_0
    iget-object v0, p0, LX/Dpl;->sender_key:[B

    if-eqz v0, :cond_1

    .line 2047121
    sget-object v0, LX/Dpl;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2047122
    iget-object v0, p0, LX/Dpl;->sender_key:[B

    invoke-virtual {p1, v0}, LX/1su;->a([B)V

    .line 2047123
    :cond_1
    iget-object v0, p0, LX/Dpl;->thread_participants:Ljava/util/Map;

    if-eqz v0, :cond_3

    .line 2047124
    sget-object v0, LX/Dpl;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2047125
    new-instance v0, LX/7H3;

    const/16 v1, 0xa

    const/16 v2, 0xf

    iget-object v3, p0, LX/Dpl;->thread_participants:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/7H3;-><init>(BBI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/7H3;)V

    .line 2047126
    iget-object v0, p0, LX/Dpl;->thread_participants:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2047127
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, LX/1su;->a(J)V

    .line 2047128
    new-instance v3, LX/1u3;

    const/16 v4, 0xc

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v3, v4, v1}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v3}, LX/1su;->a(LX/1u3;)V

    .line 2047129
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DpM;

    .line 2047130
    invoke-virtual {v0, p1}, LX/DpM;->a(LX/1su;)V

    goto :goto_0

    .line 2047131
    :cond_3
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2047132
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2047133
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2047087
    if-nez p1, :cond_1

    .line 2047088
    :cond_0
    :goto_0
    return v0

    .line 2047089
    :cond_1
    instance-of v1, p1, LX/Dpl;

    if-eqz v1, :cond_0

    .line 2047090
    check-cast p1, LX/Dpl;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2047091
    if-nez p1, :cond_3

    .line 2047092
    :cond_2
    :goto_1
    move v0, v2

    .line 2047093
    goto :goto_0

    .line 2047094
    :cond_3
    iget-object v0, p0, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2047095
    :goto_2
    iget-object v3, p1, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2047096
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2047097
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2047098
    iget-object v0, p0, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    iget-object v3, p1, LX/Dpl;->thread_fbid:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2047099
    :cond_5
    iget-object v0, p0, LX/Dpl;->sender_key:[B

    if-eqz v0, :cond_c

    move v0, v1

    .line 2047100
    :goto_4
    iget-object v3, p1, LX/Dpl;->sender_key:[B

    if-eqz v3, :cond_d

    move v3, v1

    .line 2047101
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2047102
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2047103
    iget-object v0, p0, LX/Dpl;->sender_key:[B

    iget-object v3, p1, LX/Dpl;->sender_key:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2047104
    :cond_7
    iget-object v0, p0, LX/Dpl;->thread_participants:Ljava/util/Map;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2047105
    :goto_6
    iget-object v3, p1, LX/Dpl;->thread_participants:Ljava/util/Map;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2047106
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2047107
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2047108
    iget-object v0, p0, LX/Dpl;->thread_participants:Ljava/util/Map;

    iget-object v3, p1, LX/Dpl;->thread_participants:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 2047109
    goto :goto_1

    :cond_a
    move v0, v2

    .line 2047110
    goto :goto_2

    :cond_b
    move v3, v2

    .line 2047111
    goto :goto_3

    :cond_c
    move v0, v2

    .line 2047112
    goto :goto_4

    :cond_d
    move v3, v2

    .line 2047113
    goto :goto_5

    :cond_e
    move v0, v2

    .line 2047114
    goto :goto_6

    :cond_f
    move v3, v2

    .line 2047115
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2047086
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2047083
    sget-boolean v0, LX/Dpl;->a:Z

    .line 2047084
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Dpl;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2047085
    return-object v0
.end method
