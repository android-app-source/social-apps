.class public LX/Cmg;
.super LX/Cm9;
.source ""

# interfaces
.implements LX/Cm1;
.implements LX/Cm4;


# instance fields
.field public a:LX/1Lg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/8Ys;

.field private final c:LX/8Yr;

.field private final d:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field private final e:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

.field private final f:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

.field private final g:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

.field private final h:Z

.field private final i:Z

.field private final j:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field private k:Z

.field public l:LX/1M7;


# direct methods
.method public constructor <init>(LX/Cmf;)V
    .locals 1

    .prologue
    .line 1933526
    invoke-direct {p0, p1}, LX/Cm9;-><init>(LX/Cm8;)V

    .line 1933527
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Cmg;->k:Z

    .line 1933528
    iget-object v0, p1, LX/Cmf;->a:LX/8Ys;

    iput-object v0, p0, LX/Cmg;->b:LX/8Ys;

    .line 1933529
    iget-object v0, p1, LX/Cmf;->b:LX/8Yr;

    iput-object v0, p0, LX/Cmg;->c:LX/8Yr;

    .line 1933530
    iget-object v0, p1, LX/Cmf;->c:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, LX/Cmg;->d:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1933531
    iget-object v0, p1, LX/Cmf;->d:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    iput-object v0, p0, LX/Cmg;->e:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    .line 1933532
    iget-object v0, p1, LX/Cmf;->e:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    iput-object v0, p0, LX/Cmg;->f:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    .line 1933533
    iget-object v0, p1, LX/Cmf;->f:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    iput-object v0, p0, LX/Cmg;->g:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    .line 1933534
    iget-boolean v0, p1, LX/Cmf;->g:Z

    iput-boolean v0, p0, LX/Cmg;->h:Z

    .line 1933535
    iget-boolean v0, p1, LX/Cmf;->h:Z

    iput-boolean v0, p0, LX/Cmg;->i:Z

    .line 1933536
    iget-object v0, p1, LX/Cmf;->i:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, LX/Cmg;->j:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1933537
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Cmg;

    invoke-static {p0}, LX/1Lf;->a(LX/0QB;)LX/1Lg;

    move-result-object p0

    check-cast p0, LX/1Lg;

    iput-object p0, p1, LX/Cmg;->a:LX/1Lg;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1933542
    const-class v0, LX/Cmg;

    invoke-static {v0, p0, p1}, LX/Cmg;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1933543
    iget-object v0, p0, LX/Cmg;->a:LX/1Lg;

    sget-object v1, LX/1Li;->INSTANT_ARTICLE:LX/1Li;

    invoke-virtual {v0, v1}, LX/1Lg;->a(LX/1Li;)LX/1M7;

    move-result-object v0

    iput-object v0, p0, LX/Cmg;->l:LX/1M7;

    .line 1933544
    invoke-virtual {p0}, LX/Cmg;->r()LX/8Ys;

    move-result-object v0

    invoke-interface {v0}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object v0

    .line 1933545
    invoke-virtual {p0}, LX/Cmg;->r()LX/8Ys;

    move-result-object v1

    invoke-interface {v1}, LX/8Ys;->t()Ljava/lang/String;

    move-result-object v1

    .line 1933546
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1933547
    iget-object v2, p0, LX/Cmg;->l:LX/1M7;

    new-array v3, v6, [LX/36s;

    new-instance v4, LX/36s;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, LX/Cm5;->n()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, LX/36s;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    aput-object v4, v3, v7

    invoke-interface {v2, v3}, LX/1M7;->a([LX/36s;)V

    .line 1933548
    :cond_0
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1933549
    iget-object v0, p0, LX/Cmg;->l:LX/1M7;

    new-array v2, v6, [LX/36s;

    new-instance v3, LX/36s;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, LX/Cm5;->n()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v4}, LX/36s;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    aput-object v3, v2, v7

    invoke-interface {v0, v2}, LX/1M7;->a([LX/36s;)V

    .line 1933550
    :cond_1
    iget-object v0, p0, LX/Cmg;->l:LX/1M7;

    invoke-interface {v0, v6}, LX/1M7;->a(Z)V

    .line 1933551
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1933540
    iput-boolean p1, p0, LX/Cmg;->k:Z

    .line 1933541
    return-void
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 1933539
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method public final iY_()Z
    .locals 1

    .prologue
    .line 1933538
    iget-boolean v0, p0, LX/Cmg;->i:Z

    return v0
.end method

.method public final iZ_()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933517
    iget-object v0, p0, LX/Cmg;->j:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method

.method public final ja_()Z
    .locals 1

    .prologue
    .line 1933525
    iget-boolean v0, p0, LX/Cmg;->k:Z

    return v0
.end method

.method public final jb_()I
    .locals 1

    .prologue
    .line 1933552
    const/16 v0, 0xa

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1933524
    iget-boolean v0, p0, LX/Cmg;->h:Z

    return v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933523
    iget-object v0, p0, LX/Cmg;->d:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method

.method public final r()LX/8Ys;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933522
    iget-object v0, p0, LX/Cmg;->b:LX/8Ys;

    return-object v0
.end method

.method public final s()LX/8Yr;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933521
    iget-object v0, p0, LX/Cmg;->c:LX/8Yr;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933520
    iget-object v0, p0, LX/Cmg;->e:Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933519
    iget-object v0, p0, LX/Cmg;->f:Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1933518
    iget-object v0, p0, LX/Cmg;->g:Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    return-object v0
.end method
