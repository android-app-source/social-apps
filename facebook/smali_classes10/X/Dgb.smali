.class public final enum LX/Dgb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Dgb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Dgb;

.field public static final enum NEARBY_PLACE:LX/Dgb;

.field public static final enum PINNED_LOCATION:LX/Dgb;

.field public static final enum UNSET:LX/Dgb;

.field public static final enum USER_LOCATION:LX/Dgb;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2029639
    new-instance v0, LX/Dgb;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v2}, LX/Dgb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dgb;->UNSET:LX/Dgb;

    .line 2029640
    new-instance v0, LX/Dgb;

    const-string v1, "PINNED_LOCATION"

    invoke-direct {v0, v1, v3}, LX/Dgb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dgb;->PINNED_LOCATION:LX/Dgb;

    .line 2029641
    new-instance v0, LX/Dgb;

    const-string v1, "USER_LOCATION"

    invoke-direct {v0, v1, v4}, LX/Dgb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dgb;->USER_LOCATION:LX/Dgb;

    .line 2029642
    new-instance v0, LX/Dgb;

    const-string v1, "NEARBY_PLACE"

    invoke-direct {v0, v1, v5}, LX/Dgb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Dgb;->NEARBY_PLACE:LX/Dgb;

    .line 2029643
    const/4 v0, 0x4

    new-array v0, v0, [LX/Dgb;

    sget-object v1, LX/Dgb;->UNSET:LX/Dgb;

    aput-object v1, v0, v2

    sget-object v1, LX/Dgb;->PINNED_LOCATION:LX/Dgb;

    aput-object v1, v0, v3

    sget-object v1, LX/Dgb;->USER_LOCATION:LX/Dgb;

    aput-object v1, v0, v4

    sget-object v1, LX/Dgb;->NEARBY_PLACE:LX/Dgb;

    aput-object v1, v0, v5

    sput-object v0, LX/Dgb;->$VALUES:[LX/Dgb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2029644
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Dgb;
    .locals 1

    .prologue
    .line 2029645
    const-class v0, LX/Dgb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dgb;

    return-object v0
.end method

.method public static values()[LX/Dgb;
    .locals 1

    .prologue
    .line 2029646
    sget-object v0, LX/Dgb;->$VALUES:[LX/Dgb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Dgb;

    return-object v0
.end method
