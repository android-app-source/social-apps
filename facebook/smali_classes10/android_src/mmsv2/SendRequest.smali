.class public Landroid_src/mmsv2/SendRequest;
.super Landroid_src/mmsv2/MmsRequest;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid_src/mmsv2/SendRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private e:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2149238
    new-instance v0, LX/EdK;

    invoke-direct {v0}, LX/EdK;-><init>()V

    sput-object v0, Landroid_src/mmsv2/SendRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2149236
    invoke-direct {p0, p1}, Landroid_src/mmsv2/MmsRequest;-><init>(Landroid/os/Parcel;)V

    .line 2149237
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 2149234
    invoke-direct {p0, p1, p2, p3}, Landroid_src/mmsv2/MmsRequest;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;)V

    .line 2149235
    return-void
.end method


# virtual methods
.method public final a(LX/Ecx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2149215
    iget-object v0, p0, Landroid_src/mmsv2/MmsRequest;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid_src/mmsv2/MmsRequest;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, LX/Ecx;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/content/Intent;[B)Z
    .locals 2

    .prologue
    .line 2149228
    if-eqz p3, :cond_1

    if-eqz p2, :cond_1

    .line 2149229
    array-length v0, p3

    const v1, 0xfa000

    if-le v0, v1, :cond_0

    .line 2149230
    const/4 v0, 0x0

    .line 2149231
    :goto_0
    return v0

    .line 2149232
    :cond_0
    const-string v0, "android.telephony.extra.MMS_DATA"

    invoke-virtual {p2, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2149233
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Z
    .locals 9

    .prologue
    .line 2149218
    iget-object v0, p0, Landroid_src/mmsv2/MmsRequest;->b:Landroid/net/Uri;

    const-string v1, "maxMessageSize"

    const v2, 0x4b000

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v4, 0x0

    .line 2149219
    if-nez v0, :cond_1

    move-object v3, v4

    .line 2149220
    :goto_0
    move-object v0, v3

    .line 2149221
    iput-object v0, p0, Landroid_src/mmsv2/SendRequest;->e:[B

    .line 2149222
    iget-object v0, p0, Landroid_src/mmsv2/SendRequest;->e:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2149223
    :cond_1
    new-instance v3, LX/EdJ;

    invoke-direct {v3, p0, p1, v0, v1}, LX/EdJ;-><init>(Landroid_src/mmsv2/SendRequest;Landroid/content/Context;Landroid/net/Uri;I)V

    .line 2149224
    iget-object v5, p0, Landroid_src/mmsv2/MmsRequest;->d:Ljava/util/concurrent/ExecutorService;

    const v6, -0x5f913443

    invoke-static {v5, v3, v6}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v5

    .line 2149225
    const-wide/16 v7, 0x7530

    :try_start_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v6, 0x767630e5

    invoke-static {v5, v7, v8, v3, v6}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2149226
    :catch_0
    const/4 v3, 0x1

    invoke-interface {v5, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-object v3, v4

    .line 2149227
    goto :goto_0
.end method

.method public final a(LX/EdG;LX/Ecx;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 10

    .prologue
    .line 2149216
    iget-object v0, p1, LX/EdG;->i:LX/EdB;

    move-object v0, v0

    .line 2149217
    invoke-virtual {p0, p2}, Landroid_src/mmsv2/SendRequest;->a(LX/Ecx;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid_src/mmsv2/SendRequest;->e:[B

    const-string v3, "POST"

    invoke-interface {p2}, LX/Ecx;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-interface {p2}, LX/Ecx;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2}, LX/Ecx;->c()I

    move-result v6

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    invoke-virtual/range {v0 .. v9}, LX/EdB;->a(Ljava/lang/String;[BLjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
