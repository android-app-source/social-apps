.class public final Landroid_src/mmsv2/MmsService$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid_src/mmsv2/MmsRequest;

.field public final synthetic b:LX/EdH;


# direct methods
.method public constructor <init>(LX/EdH;Landroid_src/mmsv2/MmsRequest;)V
    .locals 0

    .prologue
    .line 2149057
    iput-object p1, p0, Landroid_src/mmsv2/MmsService$2;->b:LX/EdH;

    iput-object p2, p0, Landroid_src/mmsv2/MmsService$2;->a:Landroid_src/mmsv2/MmsRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2149034
    :try_start_0
    iget-object v0, p0, Landroid_src/mmsv2/MmsService$2;->a:Landroid_src/mmsv2/MmsRequest;

    iget-object v1, p0, Landroid_src/mmsv2/MmsService$2;->b:LX/EdH;

    iget-object v2, p0, Landroid_src/mmsv2/MmsService$2;->b:LX/EdH;

    iget-object v2, v2, LX/EdH;->l:LX/EdG;

    .line 2149035
    sget-object v3, LX/EdH;->f:LX/Ed5;

    move-object v3, v3

    .line 2149036
    sget-object v4, LX/EdH;->e:LX/Ed0;

    move-object v4, v4

    .line 2149037
    sget-object v5, LX/EdH;->g:LX/Ed8;

    move-object v5, v5

    .line 2149038
    invoke-virtual/range {v0 .. v5}, Landroid_src/mmsv2/MmsRequest;->a(Landroid/content/Context;LX/EdG;LX/Ed5;LX/Ed0;LX/Ed8;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2149039
    iget-object v0, p0, Landroid_src/mmsv2/MmsService$2;->a:Landroid_src/mmsv2/MmsRequest;

    .line 2149040
    iget-boolean v1, v0, Landroid_src/mmsv2/MmsRequest;->e:Z

    move v0, v1

    .line 2149041
    if-eqz v0, :cond_0

    .line 2149042
    invoke-static {}, LX/EdH;->e()V

    .line 2149043
    :cond_0
    iget-object v0, p0, Landroid_src/mmsv2/MmsService$2;->b:LX/EdH;

    invoke-static {v0}, LX/EdH;->h(LX/EdH;)V

    .line 2149044
    :goto_0
    return-void

    .line 2149045
    :catch_0
    move-exception v0

    .line 2149046
    :try_start_1
    const-string v1, "MmsLib"

    const-string v2, "Unexpected execution failure"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2149047
    iget-object v0, p0, Landroid_src/mmsv2/MmsService$2;->a:Landroid_src/mmsv2/MmsRequest;

    .line 2149048
    iget-boolean v1, v0, Landroid_src/mmsv2/MmsRequest;->e:Z

    move v0, v1

    .line 2149049
    if-eqz v0, :cond_1

    .line 2149050
    invoke-static {}, LX/EdH;->e()V

    .line 2149051
    :cond_1
    iget-object v0, p0, Landroid_src/mmsv2/MmsService$2;->b:LX/EdH;

    invoke-static {v0}, LX/EdH;->h(LX/EdH;)V

    goto :goto_0

    .line 2149052
    :catchall_0
    move-exception v0

    iget-object v1, p0, Landroid_src/mmsv2/MmsService$2;->a:Landroid_src/mmsv2/MmsRequest;

    .line 2149053
    iget-boolean v2, v1, Landroid_src/mmsv2/MmsRequest;->e:Z

    move v1, v2

    .line 2149054
    if-eqz v1, :cond_2

    .line 2149055
    invoke-static {}, LX/EdH;->e()V

    .line 2149056
    :cond_2
    iget-object v1, p0, Landroid_src/mmsv2/MmsService$2;->b:LX/EdH;

    invoke-static {v1}, LX/EdH;->h(LX/EdH;)V

    throw v0
.end method
