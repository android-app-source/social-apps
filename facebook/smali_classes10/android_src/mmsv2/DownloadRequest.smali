.class public Landroid_src/mmsv2/DownloadRequest;
.super Landroid_src/mmsv2/MmsRequest;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid_src/mmsv2/DownloadRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2148625
    new-instance v0, LX/EdA;

    invoke-direct {v0}, LX/EdA;-><init>()V

    sput-object v0, Landroid_src/mmsv2/DownloadRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 2148623
    invoke-direct {p0, p1}, Landroid_src/mmsv2/MmsRequest;-><init>(Landroid/os/Parcel;)V

    .line 2148624
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 2148621
    invoke-direct {p0, p1, p2, p3}, Landroid_src/mmsv2/MmsRequest;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;)V

    .line 2148622
    return-void
.end method


# virtual methods
.method public final a(LX/Ecx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2148620
    iget-object v0, p0, Landroid_src/mmsv2/MmsRequest;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/content/Intent;[B)Z
    .locals 7

    .prologue
    .line 2148611
    iget-object v0, p0, Landroid_src/mmsv2/MmsRequest;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    .line 2148612
    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v1, v2

    .line 2148613
    :goto_0
    move v0, v1

    .line 2148614
    return v0

    .line 2148615
    :cond_1
    new-instance v1, LX/Ed9;

    invoke-direct {v1, p0, p1, v0, p3}, LX/Ed9;-><init>(Landroid_src/mmsv2/DownloadRequest;Landroid/content/Context;Landroid/net/Uri;[B)V

    .line 2148616
    iget-object v3, p0, Landroid_src/mmsv2/MmsRequest;->d:Ljava/util/concurrent/ExecutorService;

    const v4, 0x66209d61

    invoke-static {v3, v1, v4}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    move-result-object v3

    .line 2148617
    const-wide/16 v5, 0x7530

    :try_start_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v4, 0x394e638c

    invoke-static {v3, v5, v6, v1, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 2148618
    :catch_0
    const/4 v1, 0x1

    invoke-interface {v3, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move v1, v2

    .line 2148619
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Z
    .locals 1

    .prologue
    .line 2148610
    const/4 v0, 0x1

    return v0
.end method

.method public final a(LX/EdG;LX/Ecx;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 10

    .prologue
    .line 2148608
    iget-object v0, p1, LX/EdG;->i:LX/EdB;

    move-object v0, v0

    .line 2148609
    invoke-virtual {p0, p2}, Landroid_src/mmsv2/DownloadRequest;->a(LX/Ecx;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "GET"

    invoke-interface {p2}, LX/Ecx;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-interface {p2}, LX/Ecx;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2}, LX/Ecx;->c()I

    move-result v6

    move-object v7, p3

    move-object v8, p4

    move-object v9, p5

    invoke-virtual/range {v0 .. v9}, LX/EdB;->a(Ljava/lang/String;[BLjava/lang/String;ZLjava/lang/String;ILandroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
