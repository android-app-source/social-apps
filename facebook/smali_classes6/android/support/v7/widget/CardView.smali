.class public Landroid/support/v7/widget/CardView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/54q;


# static fields
.field private static final a:LX/54r;


# instance fields
.field private b:Z

.field private c:Z

.field private final d:Landroid/graphics/Rect;

.field private final e:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 828554
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 828555
    new-instance v0, LX/54s;

    invoke-direct {v0}, LX/54s;-><init>()V

    sput-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    .line 828556
    :goto_0
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0}, LX/54r;->a()V

    .line 828557
    return-void

    .line 828558
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 828559
    new-instance v0, LX/54x;

    invoke-direct {v0}, LX/54x;-><init>()V

    sput-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    goto :goto_0

    .line 828560
    :cond_1
    new-instance v0, LX/54v;

    invoke-direct {v0}, LX/54v;-><init>()V

    sput-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 828523
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 828524
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    .line 828525
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/CardView;->e:Landroid/graphics/Rect;

    .line 828526
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v7/widget/CardView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 828527
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 828528
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 828529
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    .line 828530
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/CardView;->e:Landroid/graphics/Rect;

    .line 828531
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/CardView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 828532
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 828533
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 828534
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    .line 828535
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/CardView;->e:Landroid/graphics/Rect;

    .line 828536
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/CardView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 828537
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 828538
    sget-object v0, LX/03r;->CardView:[I

    const v1, 0x7f0e02be

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 828539
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 828540
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    .line 828541
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    .line 828542
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    .line 828543
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/widget/CardView;->b:Z

    .line 828544
    const/16 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v7/widget/CardView;->c:Z

    .line 828545
    const/16 v1, 0x6

    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 828546
    iget-object v2, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    const/16 v7, 0x7

    invoke-virtual {v0, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, v2, Landroid/graphics/Rect;->left:I

    .line 828547
    iget-object v2, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    const/16 v7, 0x9

    invoke-virtual {v0, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, v2, Landroid/graphics/Rect;->top:I

    .line 828548
    iget-object v2, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    const/16 v7, 0x8

    invoke-virtual {v0, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, v2, Landroid/graphics/Rect;->right:I

    .line 828549
    iget-object v2, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    const/16 v7, 0xa

    invoke-virtual {v0, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, v2, Landroid/graphics/Rect;->bottom:I

    .line 828550
    cmpl-float v1, v5, v6

    if-lez v1, :cond_0

    move v6, v5

    .line 828551
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 828552
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    move-object v1, p0

    move-object v2, p1

    invoke-interface/range {v0 .. v6}, LX/54r;->a(LX/54q;Landroid/content/Context;IFFF)V

    .line 828553
    return-void
.end method


# virtual methods
.method public final a(IIII)V
    .locals 4

    .prologue
    .line 828561
    iget-object v0, p0, Landroid/support/v7/widget/CardView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 828562
    iget-object v0, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, p1

    iget-object v1, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, p2

    iget-object v2, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, p3

    iget-object v3, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, p4

    invoke-super {p0, v0, v1, v2, v3}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 828563
    return-void
.end method

.method public getCardElevation()F
    .locals 1

    .prologue
    .line 828564
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0, p0}, LX/54r;->e(LX/54q;)F

    move-result v0

    return v0
.end method

.method public getContentPaddingBottom()I
    .locals 1

    .prologue
    .line 828565
    iget-object v0, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0
.end method

.method public getContentPaddingLeft()I
    .locals 1

    .prologue
    .line 828566
    iget-object v0, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public getContentPaddingRight()I
    .locals 1

    .prologue
    .line 828567
    iget-object v0, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    return v0
.end method

.method public getContentPaddingTop()I
    .locals 1

    .prologue
    .line 828521
    iget-object v0, p0, Landroid/support/v7/widget/CardView;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public getMaxCardElevation()F
    .locals 1

    .prologue
    .line 828522
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0, p0}, LX/54r;->a(LX/54q;)F

    move-result v0

    return v0
.end method

.method public getPreventCornerOverlap()Z
    .locals 1

    .prologue
    .line 828486
    iget-boolean v0, p0, Landroid/support/v7/widget/CardView;->c:Z

    return v0
.end method

.method public getRadius()F
    .locals 1

    .prologue
    .line 828488
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0, p0}, LX/54r;->d(LX/54q;)F

    move-result v0

    return v0
.end method

.method public getUseCompatPadding()Z
    .locals 1

    .prologue
    .line 828489
    iget-boolean v0, p0, Landroid/support/v7/widget/CardView;->b:Z

    return v0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 828490
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    instance-of v0, v0, LX/54s;

    if-nez v0, :cond_0

    .line 828491
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 828492
    sparse-switch v0, :sswitch_data_0

    .line 828493
    :goto_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 828494
    sparse-switch v0, :sswitch_data_1

    .line 828495
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 828496
    :goto_2
    return-void

    .line 828497
    :sswitch_0
    sget-object v1, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v1, p0}, LX/54r;->b(LX/54q;)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 828498
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 828499
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0

    .line 828500
    :sswitch_1
    sget-object v1, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v1, p0}, LX/54r;->c(LX/54q;)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 828501
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 828502
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    goto :goto_1

    .line 828503
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public setCardBackgroundColor(I)V
    .locals 1

    .prologue
    .line 828504
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0, p0, p1}, LX/54r;->a(LX/54q;I)V

    .line 828505
    return-void
.end method

.method public setCardElevation(F)V
    .locals 1

    .prologue
    .line 828506
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0, p0, p1}, LX/54r;->c(LX/54q;F)V

    .line 828507
    return-void
.end method

.method public setMaxCardElevation(F)V
    .locals 1

    .prologue
    .line 828508
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0, p0, p1}, LX/54r;->b(LX/54q;F)V

    .line 828509
    return-void
.end method

.method public final setPadding(IIII)V
    .locals 0

    .prologue
    .line 828487
    return-void
.end method

.method public final setPaddingRelative(IIII)V
    .locals 0

    .prologue
    .line 828510
    return-void
.end method

.method public setPreventCornerOverlap(Z)V
    .locals 1

    .prologue
    .line 828511
    iget-boolean v0, p0, Landroid/support/v7/widget/CardView;->c:Z

    if-ne p1, v0, :cond_0

    .line 828512
    :goto_0
    return-void

    .line 828513
    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/CardView;->c:Z

    .line 828514
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0, p0}, LX/54r;->g(LX/54q;)V

    goto :goto_0
.end method

.method public setRadius(F)V
    .locals 1

    .prologue
    .line 828515
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0, p0, p1}, LX/54r;->a(LX/54q;F)V

    .line 828516
    return-void
.end method

.method public setUseCompatPadding(Z)V
    .locals 1

    .prologue
    .line 828517
    iget-boolean v0, p0, Landroid/support/v7/widget/CardView;->b:Z

    if-ne v0, p1, :cond_0

    .line 828518
    :goto_0
    return-void

    .line 828519
    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/CardView;->b:Z

    .line 828520
    sget-object v0, Landroid/support/v7/widget/CardView;->a:LX/54r;

    invoke-interface {v0, p0}, LX/54r;->f(LX/54q;)V

    goto :goto_0
.end method
