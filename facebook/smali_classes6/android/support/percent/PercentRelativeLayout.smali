.class public Landroid/support/percent/PercentRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source ""


# instance fields
.field private final a:LX/2tQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 828480
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 828481
    new-instance v0, LX/2tQ;

    invoke-direct {v0, p0}, LX/2tQ;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Landroid/support/percent/PercentRelativeLayout;->a:LX/2tQ;

    .line 828482
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 828483
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 828484
    new-instance v0, LX/2tQ;

    invoke-direct {v0, p0}, LX/2tQ;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Landroid/support/percent/PercentRelativeLayout;->a:LX/2tQ;

    .line 828485
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 828474
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 828475
    new-instance v0, LX/2tQ;

    invoke-direct {v0, p0}, LX/2tQ;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Landroid/support/percent/PercentRelativeLayout;->a:LX/2tQ;

    .line 828476
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)LX/54p;
    .locals 2

    .prologue
    .line 828478
    new-instance v0, LX/54p;

    invoke-virtual {p0}, Landroid/support/percent/PercentRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/54p;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 828479
    invoke-direct {p0, p1}, Landroid/support/percent/PercentRelativeLayout;->a(Landroid/util/AttributeSet;)LX/54p;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 1

    .prologue
    .line 828477
    invoke-direct {p0, p1}, Landroid/support/percent/PercentRelativeLayout;->a(Landroid/util/AttributeSet;)LX/54p;

    move-result-object v0

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 828450
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 828451
    iget-object v0, p0, Landroid/support/percent/PercentRelativeLayout;->a:LX/2tQ;

    const/4 p5, 0x3

    .line 828452
    const/4 v1, 0x0

    iget-object p0, v0, LX/2tQ;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    move p1, v1

    :goto_0
    if-ge p1, p2, :cond_4

    .line 828453
    iget-object v1, v0, LX/2tQ;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 828454
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    .line 828455
    const-string p3, "PercentLayout"

    invoke-static {p3, p5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 828456
    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "should restore "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p3, " "

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 828457
    :cond_0
    instance-of v1, p0, LX/54o;

    if-eqz v1, :cond_2

    move-object v1, p0

    .line 828458
    check-cast v1, LX/54o;

    invoke-interface {v1}, LX/54o;->a()LX/2tR;

    move-result-object v1

    .line 828459
    const-string p3, "PercentLayout"

    invoke-static {p3, p5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 828460
    new-instance p3, Ljava/lang/StringBuilder;

    const-string p4, "using "

    invoke-direct {p3, p4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 828461
    :cond_1
    if-eqz v1, :cond_2

    .line 828462
    instance-of p3, p0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz p3, :cond_3

    .line 828463
    check-cast p0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 828464
    invoke-virtual {v1, p0}, LX/2tR;->a(Landroid/view/ViewGroup$LayoutParams;)V

    .line 828465
    iget-object p3, v1, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget p3, p3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput p3, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 828466
    iget-object p3, v1, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget p3, p3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput p3, p0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 828467
    iget-object p3, v1, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget p3, p3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput p3, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 828468
    iget-object p3, v1, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget p3, p3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput p3, p0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 828469
    iget-object p3, v1, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-static {p3}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result p3

    invoke-static {p0, p3}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 828470
    iget-object p3, v1, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-static {p3}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result p3

    invoke-static {p0, p3}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 828471
    :cond_2
    :goto_1
    add-int/lit8 v1, p1, 0x1

    move p1, v1

    goto :goto_0

    .line 828472
    :cond_3
    invoke-virtual {v1, p0}, LX/2tR;->a(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 828473
    :cond_4
    return-void
.end method

.method public final onMeasure(II)V
    .locals 12

    .prologue
    .line 828423
    iget-object v0, p0, Landroid/support/percent/PercentRelativeLayout;->a:LX/2tQ;

    invoke-virtual {v0, p1, p2}, LX/2tQ;->a(II)V

    .line 828424
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 828425
    iget-object v0, p0, Landroid/support/percent/PercentRelativeLayout;->a:LX/2tQ;

    const/4 v10, 0x3

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v9, -0x2

    .line 828426
    iget-object v2, v0, LX/2tQ;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    move v5, v1

    move v4, v1

    :goto_0
    if-ge v5, v6, :cond_2

    .line 828427
    iget-object v1, v0, LX/2tQ;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 828428
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 828429
    const-string v1, "PercentLayout"

    invoke-static {v1, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 828430
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v8, "should handle measured state too small "

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 828431
    :cond_0
    instance-of v1, v2, LX/54o;

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 828432
    check-cast v1, LX/54o;

    invoke-interface {v1}, LX/54o;->a()LX/2tR;

    move-result-object v8

    .line 828433
    if-eqz v8, :cond_1

    .line 828434
    invoke-static {v7}, LX/0vv;->k(Landroid/view/View;)I

    move-result v1

    const/high16 v11, -0x1000000

    and-int/2addr v1, v11

    .line 828435
    const/high16 v11, 0x1000000

    if-ne v1, v11, :cond_7

    iget v1, v8, LX/2tR;->a:F

    const/4 v11, 0x0

    cmpl-float v1, v1, v11

    if-ltz v1, :cond_7

    iget-object v1, v8, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v11, -0x2

    if-ne v1, v11, :cond_7

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 828436
    if-eqz v1, :cond_6

    .line 828437
    iput v9, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    move v1, v3

    .line 828438
    :goto_2
    sget-object v4, LX/0vv;->a:LX/0w2;

    invoke-interface {v4, v7}, LX/0w2;->l(Landroid/view/View;)I

    move-result v4

    move v4, v4

    .line 828439
    const/high16 v11, -0x1000000

    and-int/2addr v4, v11

    .line 828440
    const/high16 v11, 0x1000000

    if-ne v4, v11, :cond_8

    iget v4, v8, LX/2tR;->b:F

    const/4 v11, 0x0

    cmpl-float v4, v4, v11

    if-ltz v4, :cond_8

    iget-object v4, v8, LX/2tR;->i:Landroid/view/ViewGroup$MarginLayoutParams;

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v11, -0x2

    if-ne v4, v11, :cond_8

    const/4 v4, 0x1

    :goto_3
    move v4, v4

    .line 828441
    if-eqz v4, :cond_5

    .line 828442
    iput v9, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    move v4, v3

    .line 828443
    :cond_1
    :goto_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    .line 828444
    :cond_2
    const-string v1, "PercentLayout"

    invoke-static {v1, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 828445
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "should trigger second measure pass: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 828446
    :cond_3
    move v0, v4

    .line 828447
    if-eqz v0, :cond_4

    .line 828448
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 828449
    :cond_4
    return-void

    :cond_5
    move v4, v1

    goto :goto_4

    :cond_6
    move v1, v4

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    :cond_8
    const/4 v4, 0x0

    goto :goto_3
.end method
