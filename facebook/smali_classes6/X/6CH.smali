.class public LX/6CH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CF;
.implements LX/6CA;
.implements LX/6CG;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/03V;

.field private c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063710
    iput-object p1, p0, LX/6CH;->a:LX/0Zb;

    .line 1063711
    iput-object p2, p0, LX/6CH;->b:LX/03V;

    .line 1063712
    return-void
.end method

.method public static b(LX/0QB;)LX/6CH;
    .locals 3

    .prologue
    .line 1063615
    new-instance v2, LX/6CH;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/6CH;-><init>(LX/0Zb;LX/03V;)V

    .line 1063616
    return-object v2
.end method

.method public static d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1063702
    iget-object v0, p0, LX/6CH;->a:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1063703
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1063704
    const/4 v0, 0x0

    .line 1063705
    :cond_0
    :goto_0
    return-object v0

    .line 1063706
    :cond_1
    const-string v1, "browser_extensions"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1063707
    if-eqz p2, :cond_0

    .line 1063708
    const-string v1, "page_id"

    const-string v2, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "source"

    const-string v3, "JS_BRIDGE_LOG_SOURCE"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "trigger_surface"

    const-string v3, "JS_BRIDGE_TRIGGERING_SURFACE"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "extension_type"

    const-string v3, "JS_BRIDGE_EXTENSION_TYPE"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 3

    .prologue
    .line 1063692
    const-string v0, "browser_extensions_native_bridge_called"

    .line 1063693
    iget-object v1, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v1, v1

    .line 1063694
    invoke-static {p0, v0, v1}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v0

    .line 1063695
    if-nez v0, :cond_0

    .line 1063696
    :goto_0
    return-void

    .line 1063697
    :cond_0
    const-string v1, "website_url"

    .line 1063698
    iget-object v2, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1063699
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "api_endpoint"

    .line 1063700
    iget-object v2, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1063701
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;IZ)V
    .locals 3

    .prologue
    .line 1063682
    const-string v0, "browser_extensions_native_bridge_result"

    .line 1063683
    iget-object v1, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v1, v1

    .line 1063684
    invoke-static {p0, v0, v1}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v0

    .line 1063685
    if-nez v0, :cond_0

    .line 1063686
    :goto_0
    return-void

    .line 1063687
    :cond_0
    const-string v1, "website_url"

    .line 1063688
    iget-object v2, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1063689
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "api_endpoint"

    .line 1063690
    iget-object v2, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1063691
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "error_code"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v0

    const-string v1, "callback_result"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1063673
    iget-object v0, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1063674
    invoke-static {p0, p2, v0}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v0

    .line 1063675
    if-nez v0, :cond_0

    .line 1063676
    :goto_0
    return-void

    .line 1063677
    :cond_0
    const-string v1, "api_endpoint"

    .line 1063678
    iget-object v2, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1063679
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "website_url"

    .line 1063680
    iget-object v2, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1063681
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1063672
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1063666
    const-string v0, "browser_extensions_error"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v0

    .line 1063667
    if-nez v0, :cond_0

    .line 1063668
    :goto_0
    return-void

    .line 1063669
    :cond_0
    const-string v1, "error_tag"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "error_message"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "page_id"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "ad_id"

    if-eqz p4, :cond_1

    :goto_1
    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1063670
    iget-object v0, p0, LX/6CH;->b:LX/03V;

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1063671
    :cond_1
    const-string p4, ""

    goto :goto_1
.end method

.method public final a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1063656
    const-string v0, "browser_extensions_permission_dialog_shown"

    .line 1063657
    iget-object v1, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v1, v1

    .line 1063658
    invoke-static {p0, v0, v1}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v0

    .line 1063659
    if-nez v0, :cond_0

    .line 1063660
    :goto_0
    return-void

    .line 1063661
    :cond_0
    const-string v1, "website_url"

    .line 1063662
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1063663
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "api_endpoint"

    .line 1063664
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1063665
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "permission_requested"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1063647
    iget-object v0, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1063648
    invoke-static {p0, p3, v0}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v0

    .line 1063649
    if-nez v0, :cond_0

    .line 1063650
    :goto_0
    return-void

    .line 1063651
    :cond_0
    const-string v1, "api_endpoint"

    .line 1063652
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1063653
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "website_url"

    .line 1063654
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1063655
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "autofill_fields_requested"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 1063646
    invoke-static {p1}, LX/6D3;->a(Landroid/os/Bundle;)LX/6Cx;

    move-result-object v0

    sget-object v1, LX/6Cx;->MESSENGER_EXTENSION:LX/6Cx;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1063639
    const-string v0, "JS_BRIDGE_SESSION_ID"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1063640
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/6CH;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/6CH;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1063641
    :cond_0
    const-string v1, "browser_extensions_browser_open"

    invoke-static {p0, v1, p2}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v1

    .line 1063642
    if-eqz v1, :cond_1

    .line 1063643
    const-string v2, "website_url"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1063644
    :cond_1
    iput-object v0, p0, LX/6CH;->c:Ljava/lang/String;

    .line 1063645
    return-void
.end method

.method public final b(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1063630
    iget-object v0, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1063631
    invoke-static {p0, p3, v0}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v0

    .line 1063632
    if-nez v0, :cond_0

    .line 1063633
    :goto_0
    return-void

    .line 1063634
    :cond_0
    const-string v1, "api_endpoint"

    .line 1063635
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1063636
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "website_url"

    .line 1063637
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1063638
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "autofill_fields_requested"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1063626
    const-string v0, "browser_extensions_browser_closed"

    invoke-static {p0, v0, p2}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v0

    .line 1063627
    if-eqz v0, :cond_0

    .line 1063628
    const-string v1, "website_url"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1063629
    :cond_0
    return-void
.end method

.method public final c(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1063617
    iget-object v0, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v0, v0

    .line 1063618
    invoke-static {p0, p3, v0}, LX/6CH;->d(LX/6CH;Ljava/lang/String;Landroid/os/Bundle;)LX/0oG;

    move-result-object v0

    .line 1063619
    if-nez v0, :cond_0

    .line 1063620
    :goto_0
    return-void

    .line 1063621
    :cond_0
    const-string v1, "api_endpoint"

    .line 1063622
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1063623
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "website_url"

    .line 1063624
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1063625
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "autofill_fields_filled"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
