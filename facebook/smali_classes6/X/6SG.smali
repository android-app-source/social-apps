.class public final LX/6SG;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1091787
    const-class v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveNotableLikedCommentsQueryModel;

    const v0, -0x7010745d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "LiveNotableLikedCommentsQuery"

    const-string v6, "cf56f41b2d1f43b6adf014a644b62411"

    const-string v7, "video"

    const-string v8, "10155207368346729"

    const-string v9, "10155259089101729"

    .line 1091788
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1091789
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1091790
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1091791
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1091792
    sparse-switch v0, :sswitch_data_0

    .line 1091793
    :goto_0
    return-object p1

    .line 1091794
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1091795
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1091796
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1091797
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1091798
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1091799
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x65d81c4d -> :sswitch_5
        -0x1a57a594 -> :sswitch_4
        0x5a7510f -> :sswitch_2
        0xc765056 -> :sswitch_1
        0x1f588673 -> :sswitch_0
        0x69d55a61 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1091800
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 1091801
    :goto_1
    return v0

    .line 1091802
    :pswitch_1
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 1091803
    :pswitch_3
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1091804
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
