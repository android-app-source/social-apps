.class public LX/6Ks;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Jh;


# instance fields
.field private final a:I

.field private final b:I

.field private c:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1FJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1077545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077546
    const-string v0, "Non-null bitmap required to create BitmapInput."

    invoke-static {p1, v0}, LX/03g;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077547
    iput-object p1, p0, LX/6Ks;->c:LX/1FJ;

    .line 1077548
    iget-object v0, p0, LX/6Ks;->c:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, LX/6Ks;->a:I

    .line 1077549
    iget-object v0, p0, LX/6Ks;->c:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, LX/6Ks;->b:I

    .line 1077550
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/SurfaceTexture;LX/6Km;)V
    .locals 6

    .prologue
    const v5, 0x812f

    const/16 v4, 0x2601

    const/4 v3, 0x0

    const/16 v2, 0xde1

    .line 1077562
    new-instance v0, LX/5Pe;

    invoke-direct {v0}, LX/5Pe;-><init>()V

    .line 1077563
    iput v2, v0, LX/5Pe;->a:I

    .line 1077564
    move-object v0, v0

    .line 1077565
    const/16 v1, 0x2801

    invoke-virtual {v0, v1, v4}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2800

    invoke-virtual {v0, v1, v4}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2802

    invoke-virtual {v0, v1, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2803

    invoke-virtual {v0, v1, v5}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    invoke-virtual {v0}, LX/5Pe;->a()LX/5Pf;

    move-result-object v1

    .line 1077566
    iget v0, v1, LX/5Pf;->b:I

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1077567
    iget-object v0, p0, LX/6Ks;->c:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v2, v3, v0, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 1077568
    const/4 v0, 0x1

    invoke-static {p2, v0, v1}, LX/6Km;->a$redex0(LX/6Km;ILjava/lang/Object;)V

    .line 1077569
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1077559
    invoke-virtual {p0}, LX/6Ks;->dE_()V

    .line 1077560
    iget-object v0, p0, LX/6Ks;->c:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1077561
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1077558
    const/4 v0, 0x0

    return v0
.end method

.method public final dE_()V
    .locals 0

    .prologue
    .line 1077557
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1077556
    const/4 v0, 0x0

    return v0
.end method

.method public final getClock()LX/6Jg;
    .locals 1

    .prologue
    .line 1077554
    sget-object v0, LX/6KW;->a:LX/6KW;

    move-object v0, v0

    .line 1077555
    return-object v0
.end method

.method public final getInputHeight()I
    .locals 1

    .prologue
    .line 1077553
    iget v0, p0, LX/6Ks;->b:I

    return v0
.end method

.method public final getInputWidth()I
    .locals 1

    .prologue
    .line 1077552
    iget v0, p0, LX/6Ks;->a:I

    return v0
.end method

.method public final getRotationDegrees$134621()I
    .locals 1

    .prologue
    .line 1077551
    const/4 v0, 0x0

    return v0
.end method
