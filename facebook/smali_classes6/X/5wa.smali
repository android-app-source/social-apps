.class public final LX/5wa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderStructuredNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public C:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public D:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemsConnectionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:Z

.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:J

.field public y:Z

.field public z:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1025314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;
    .locals 20

    .prologue
    .line 1025315
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1025316
    move-object/from16 v0, p0

    iget-object v3, v0, LX/5wa;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1025317
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5wa;->h:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    invoke-static {v2, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1025318
    move-object/from16 v0, p0

    iget-object v5, v0, LX/5wa;->j:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1025319
    move-object/from16 v0, p0

    iget-object v6, v0, LX/5wa;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v2, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1025320
    move-object/from16 v0, p0

    iget-object v7, v0, LX/5wa;->l:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1025321
    move-object/from16 v0, p0

    iget-object v8, v0, LX/5wa;->s:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1025322
    move-object/from16 v0, p0

    iget-object v9, v0, LX/5wa;->t:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1025323
    move-object/from16 v0, p0

    iget-object v10, v0, LX/5wa;->u:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1025324
    move-object/from16 v0, p0

    iget-object v11, v0, LX/5wa;->v:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    invoke-static {v2, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1025325
    move-object/from16 v0, p0

    iget-object v12, v0, LX/5wa;->w:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-static {v2, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1025326
    move-object/from16 v0, p0

    iget-object v13, v0, LX/5wa;->z:Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileHeaderAssociatedVideoModel;

    invoke-static {v2, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1025327
    move-object/from16 v0, p0

    iget-object v14, v0, LX/5wa;->A:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v2, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    .line 1025328
    move-object/from16 v0, p0

    iget-object v15, v0, LX/5wa;->B:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderStructuredNameModel;

    invoke-static {v2, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1025329
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5wa;->C:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 1025330
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5wa;->D:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemsConnectionFieldsModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v2, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1025331
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5wa;->E:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1025332
    const/16 v19, 0x20

    move/from16 v0, v19

    invoke-virtual {v2, v0}, LX/186;->c(I)V

    .line 1025333
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v2, v0, v3}, LX/186;->b(II)V

    .line 1025334
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5wa;->b:Z

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->a(IZ)V

    .line 1025335
    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5wa;->c:Z

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->a(IZ)V

    .line 1025336
    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5wa;->d:Z

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->a(IZ)V

    .line 1025337
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5wa;->e:Z

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->a(IZ)V

    .line 1025338
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5wa;->f:Z

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->a(IZ)V

    .line 1025339
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/5wa;->g:Z

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->a(IZ)V

    .line 1025340
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1025341
    const/16 v3, 0x8

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5wa;->i:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1025342
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1025343
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1025344
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1025345
    const/16 v3, 0xc

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5wa;->m:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1025346
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5wa;->n:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1025347
    const/16 v3, 0xe

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5wa;->o:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1025348
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5wa;->p:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1025349
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5wa;->q:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1025350
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5wa;->r:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1025351
    const/16 v3, 0x12

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1025352
    const/16 v3, 0x13

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1025353
    const/16 v3, 0x14

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1025354
    const/16 v3, 0x15

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1025355
    const/16 v3, 0x16

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1025356
    const/16 v3, 0x17

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/5wa;->x:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1025357
    const/16 v3, 0x18

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5wa;->y:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1025358
    const/16 v3, 0x19

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1025359
    const/16 v3, 0x1a

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 1025360
    const/16 v3, 0x1b

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 1025361
    const/16 v3, 0x1c

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1025362
    const/16 v3, 0x1d

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1025363
    const/16 v3, 0x1e

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 1025364
    const/16 v3, 0x1f

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5wa;->F:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1025365
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1025366
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1025367
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1025368
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1025369
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1025370
    new-instance v3, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;-><init>(LX/15i;)V

    .line 1025371
    return-object v3
.end method
