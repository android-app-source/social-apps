.class public final LX/6UJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    .line 1101301
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1101302
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1101303
    if-eqz v0, :cond_0

    .line 1101304
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101305
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1101306
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1101307
    if-eqz v0, :cond_d

    .line 1101308
    const-string v1, "tip_jar_transactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101309
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1101310
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1101311
    if-eqz v2, :cond_7

    .line 1101312
    const-string v3, "edges"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101313
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1101314
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_6

    .line 1101315
    invoke-virtual {p0, v2, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1101316
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1101317
    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1101318
    if-eqz v6, :cond_5

    .line 1101319
    const-string v7, "node"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101320
    const-wide/16 v10, 0x0

    .line 1101321
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1101322
    const/4 v8, 0x0

    invoke-virtual {p0, v6, v8}, LX/15i;->g(II)I

    move-result v8

    .line 1101323
    if-eqz v8, :cond_1

    .line 1101324
    const-string v9, "amount_received"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101325
    invoke-static {p0, v8, p2}, LX/6UF;->a(LX/15i;ILX/0nX;)V

    .line 1101326
    :cond_1
    const/4 v8, 0x1

    invoke-virtual {p0, v6, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1101327
    if-eqz v8, :cond_2

    .line 1101328
    const-string v9, "comment"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101329
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1101330
    :cond_2
    const/4 v8, 0x2

    invoke-virtual {p0, v6, v8, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 1101331
    cmpl-double v10, v8, v10

    if-eqz v10, :cond_3

    .line 1101332
    const-string v10, "tip_amount"

    invoke-virtual {p2, v10}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101333
    invoke-virtual {p2, v8, v9}, LX/0nX;->a(D)V

    .line 1101334
    :cond_3
    const/4 v8, 0x3

    invoke-virtual {p0, v6, v8}, LX/15i;->g(II)I

    move-result v8

    .line 1101335
    if-eqz v8, :cond_4

    .line 1101336
    const-string v9, "tip_giver"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101337
    invoke-static {p0, v8, p2, p3}, LX/6UG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1101338
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1101339
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1101340
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1101341
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1101342
    :cond_7
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1101343
    if-eqz v2, :cond_c

    .line 1101344
    const-string v3, "page_info"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101345
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1101346
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1101347
    if-eqz v3, :cond_8

    .line 1101348
    const-string v4, "end_cursor"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101349
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1101350
    :cond_8
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1101351
    if-eqz v3, :cond_9

    .line 1101352
    const-string v4, "has_next_page"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101353
    invoke-virtual {p2, v3}, LX/0nX;->a(Z)V

    .line 1101354
    :cond_9
    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1101355
    if-eqz v3, :cond_a

    .line 1101356
    const-string v4, "has_previous_page"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101357
    invoke-virtual {p2, v3}, LX/0nX;->a(Z)V

    .line 1101358
    :cond_a
    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1101359
    if-eqz v3, :cond_b

    .line 1101360
    const-string v4, "start_cursor"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101361
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1101362
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1101363
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1101364
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1101365
    return-void
.end method
