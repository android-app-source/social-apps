.class public final LX/6ZV;
.super LX/0T0;
.source ""


# instance fields
.field public final synthetic a:LX/6Zb;


# direct methods
.method public constructor <init>(LX/6Zb;)V
    .locals 0

    .prologue
    .line 1111196
    iput-object p1, p0, LX/6ZV;->a:LX/6Zb;

    invoke-direct {p0}, LX/0T0;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1111197
    iget-object v0, p0, LX/6ZV;->a:LX/6Zb;

    .line 1111198
    iget-boolean p4, v0, LX/6Zb;->h:Z

    if-eqz p4, :cond_0

    const/16 p4, 0x136f

    if-ne p2, p4, :cond_0

    .line 1111199
    const/4 p4, 0x0

    iput-boolean p4, v0, LX/6Zb;->h:Z

    .line 1111200
    const/4 p4, -0x1

    if-ne p3, p4, :cond_1

    sget-object p4, LX/6ZY;->DIALOG_SUCCESS:LX/6ZY;

    :goto_0
    iput-object p4, v0, LX/6Zb;->i:LX/6ZY;

    .line 1111201
    :cond_0
    return-void

    .line 1111202
    :cond_1
    sget-object p4, LX/6ZY;->DIALOG_CANCEL:LX/6ZY;

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1111193
    iget-object v0, p0, LX/6ZV;->a:LX/6Zb;

    const/4 v1, 0x0

    .line 1111194
    if-eqz p1, :cond_0

    const-string p0, "GooglePlayServicesLocationUpsellDialogController:waiting_for_dialog_result"

    invoke-virtual {p1, p0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, v0, LX/6Zb;->h:Z

    .line 1111195
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1111189
    iget-object v0, p0, LX/6ZV;->a:LX/6Zb;

    .line 1111190
    iget-boolean v1, v0, LX/6Zb;->h:Z

    if-eqz v1, :cond_0

    .line 1111191
    const-string v1, "GooglePlayServicesLocationUpsellDialogController:waiting_for_dialog_result"

    const/4 p0, 0x1

    invoke-virtual {p1, v1, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1111192
    :cond_0
    return-void
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 1111187
    iget-object v0, p0, LX/6ZV;->a:LX/6Zb;

    iget-object v0, v0, LX/6Zb;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/location/gmsupsell/GooglePlayServicesLocationUpsellDialogController$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/location/gmsupsell/GooglePlayServicesLocationUpsellDialogController$1$1;-><init>(LX/6ZV;)V

    const v2, -0x2a888714

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1111188
    return-void
.end method
