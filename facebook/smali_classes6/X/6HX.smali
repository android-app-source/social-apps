.class public final enum LX/6HX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6HX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6HX;

.field public static final enum FACE_DETECTION_AUTOFOCUS:LX/6HX;

.field public static final enum LAST_SECOND_AUTOFOCUS:LX/6HX;

.field public static final enum TOUCH_TO_FOCUS:LX/6HX;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1072214
    new-instance v0, LX/6HX;

    const-string v1, "TOUCH_TO_FOCUS"

    invoke-direct {v0, v1, v2}, LX/6HX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6HX;->TOUCH_TO_FOCUS:LX/6HX;

    .line 1072215
    new-instance v0, LX/6HX;

    const-string v1, "LAST_SECOND_AUTOFOCUS"

    invoke-direct {v0, v1, v3}, LX/6HX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6HX;->LAST_SECOND_AUTOFOCUS:LX/6HX;

    .line 1072216
    new-instance v0, LX/6HX;

    const-string v1, "FACE_DETECTION_AUTOFOCUS"

    invoke-direct {v0, v1, v4}, LX/6HX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6HX;->FACE_DETECTION_AUTOFOCUS:LX/6HX;

    .line 1072217
    const/4 v0, 0x3

    new-array v0, v0, [LX/6HX;

    sget-object v1, LX/6HX;->TOUCH_TO_FOCUS:LX/6HX;

    aput-object v1, v0, v2

    sget-object v1, LX/6HX;->LAST_SECOND_AUTOFOCUS:LX/6HX;

    aput-object v1, v0, v3

    sget-object v1, LX/6HX;->FACE_DETECTION_AUTOFOCUS:LX/6HX;

    aput-object v1, v0, v4

    sput-object v0, LX/6HX;->$VALUES:[LX/6HX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1072219
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6HX;
    .locals 1

    .prologue
    .line 1072220
    const-class v0, LX/6HX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6HX;

    return-object v0
.end method

.method public static values()[LX/6HX;
    .locals 1

    .prologue
    .line 1072218
    sget-object v0, LX/6HX;->$VALUES:[LX/6HX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6HX;

    return-object v0
.end method
