.class public final enum LX/65k;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/65k;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/65k;

.field public static final enum HTTP_20_HEADERS:LX/65k;

.field public static final enum SPDY_HEADERS:LX/65k;

.field public static final enum SPDY_REPLY:LX/65k;

.field public static final enum SPDY_SYN_STREAM:LX/65k;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1048442
    new-instance v0, LX/65k;

    const-string v1, "SPDY_SYN_STREAM"

    invoke-direct {v0, v1, v2}, LX/65k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/65k;->SPDY_SYN_STREAM:LX/65k;

    .line 1048443
    new-instance v0, LX/65k;

    const-string v1, "SPDY_REPLY"

    invoke-direct {v0, v1, v3}, LX/65k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/65k;->SPDY_REPLY:LX/65k;

    .line 1048444
    new-instance v0, LX/65k;

    const-string v1, "SPDY_HEADERS"

    invoke-direct {v0, v1, v4}, LX/65k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/65k;->SPDY_HEADERS:LX/65k;

    .line 1048445
    new-instance v0, LX/65k;

    const-string v1, "HTTP_20_HEADERS"

    invoke-direct {v0, v1, v5}, LX/65k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/65k;->HTTP_20_HEADERS:LX/65k;

    .line 1048446
    const/4 v0, 0x4

    new-array v0, v0, [LX/65k;

    sget-object v1, LX/65k;->SPDY_SYN_STREAM:LX/65k;

    aput-object v1, v0, v2

    sget-object v1, LX/65k;->SPDY_REPLY:LX/65k;

    aput-object v1, v0, v3

    sget-object v1, LX/65k;->SPDY_HEADERS:LX/65k;

    aput-object v1, v0, v4

    sget-object v1, LX/65k;->HTTP_20_HEADERS:LX/65k;

    aput-object v1, v0, v5

    sput-object v0, LX/65k;->$VALUES:[LX/65k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1048441
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/65k;
    .locals 1

    .prologue
    .line 1048440
    const-class v0, LX/65k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/65k;

    return-object v0
.end method

.method public static values()[LX/65k;
    .locals 1

    .prologue
    .line 1048439
    sget-object v0, LX/65k;->$VALUES:[LX/65k;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/65k;

    return-object v0
.end method


# virtual methods
.method public final failIfHeadersAbsent()Z
    .locals 1

    .prologue
    .line 1048438
    sget-object v0, LX/65k;->SPDY_HEADERS:LX/65k;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final failIfHeadersPresent()Z
    .locals 1

    .prologue
    .line 1048435
    sget-object v0, LX/65k;->SPDY_REPLY:LX/65k;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final failIfStreamAbsent()Z
    .locals 1

    .prologue
    .line 1048437
    sget-object v0, LX/65k;->SPDY_REPLY:LX/65k;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/65k;->SPDY_HEADERS:LX/65k;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final failIfStreamPresent()Z
    .locals 1

    .prologue
    .line 1048436
    sget-object v0, LX/65k;->SPDY_SYN_STREAM:LX/65k;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
