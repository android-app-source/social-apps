.class public final LX/67F;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[B

.field public b:I

.field public c:I

.field public d:Z

.field public e:Z

.field public f:LX/67F;

.field public g:LX/67F;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1052637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052638
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, LX/67F;->a:[B

    .line 1052639
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67F;->e:Z

    .line 1052640
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/67F;->d:Z

    .line 1052641
    return-void
.end method

.method public constructor <init>(LX/67F;)V
    .locals 3

    .prologue
    .line 1052634
    iget-object v0, p1, LX/67F;->a:[B

    iget v1, p1, LX/67F;->b:I

    iget v2, p1, LX/67F;->c:I

    invoke-direct {p0, v0, v1, v2}, LX/67F;-><init>([BII)V

    .line 1052635
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/67F;->d:Z

    .line 1052636
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 1

    .prologue
    .line 1052627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052628
    iput-object p1, p0, LX/67F;->a:[B

    .line 1052629
    iput p2, p0, LX/67F;->b:I

    .line 1052630
    iput p3, p0, LX/67F;->c:I

    .line 1052631
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/67F;->e:Z

    .line 1052632
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67F;->d:Z

    .line 1052633
    return-void
.end method


# virtual methods
.method public final a()LX/67F;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1052585
    iget-object v0, p0, LX/67F;->f:LX/67F;

    if-eq v0, p0, :cond_0

    iget-object v0, p0, LX/67F;->f:LX/67F;

    .line 1052586
    :goto_0
    iget-object v2, p0, LX/67F;->g:LX/67F;

    iget-object v3, p0, LX/67F;->f:LX/67F;

    iput-object v3, v2, LX/67F;->f:LX/67F;

    .line 1052587
    iget-object v2, p0, LX/67F;->f:LX/67F;

    iget-object v3, p0, LX/67F;->g:LX/67F;

    iput-object v3, v2, LX/67F;->g:LX/67F;

    .line 1052588
    iput-object v1, p0, LX/67F;->f:LX/67F;

    .line 1052589
    iput-object v1, p0, LX/67F;->g:LX/67F;

    .line 1052590
    return-object v0

    :cond_0
    move-object v0, v1

    .line 1052591
    goto :goto_0
.end method

.method public final a(I)LX/67F;
    .locals 5

    .prologue
    .line 1052618
    if-lez p1, :cond_0

    iget v0, p0, LX/67F;->c:I

    iget v1, p0, LX/67F;->b:I

    sub-int/2addr v0, v1

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1052619
    :cond_1
    const/16 v0, 0x400

    if-lt p1, v0, :cond_2

    .line 1052620
    new-instance v0, LX/67F;

    invoke-direct {v0, p0}, LX/67F;-><init>(LX/67F;)V

    .line 1052621
    :goto_0
    iget v1, v0, LX/67F;->b:I

    add-int/2addr v1, p1

    iput v1, v0, LX/67F;->c:I

    .line 1052622
    iget v1, p0, LX/67F;->b:I

    add-int/2addr v1, p1

    iput v1, p0, LX/67F;->b:I

    .line 1052623
    iget-object v1, p0, LX/67F;->g:LX/67F;

    invoke-virtual {v1, v0}, LX/67F;->a(LX/67F;)LX/67F;

    .line 1052624
    return-object v0

    .line 1052625
    :cond_2
    invoke-static {}, LX/67G;->a()LX/67F;

    move-result-object v0

    .line 1052626
    iget-object v1, p0, LX/67F;->a:[B

    iget v2, p0, LX/67F;->b:I

    iget-object v3, v0, LX/67F;->a:[B

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public final a(LX/67F;)LX/67F;
    .locals 1

    .prologue
    .line 1052613
    iput-object p0, p1, LX/67F;->g:LX/67F;

    .line 1052614
    iget-object v0, p0, LX/67F;->f:LX/67F;

    iput-object v0, p1, LX/67F;->f:LX/67F;

    .line 1052615
    iget-object v0, p0, LX/67F;->f:LX/67F;

    iput-object p1, v0, LX/67F;->g:LX/67F;

    .line 1052616
    iput-object p1, p0, LX/67F;->f:LX/67F;

    .line 1052617
    return-object p1
.end method

.method public final a(LX/67F;I)V
    .locals 6

    .prologue
    const/16 v2, 0x2000

    const/4 v5, 0x0

    .line 1052602
    iget-boolean v0, p1, LX/67F;->e:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1052603
    :cond_0
    iget v0, p1, LX/67F;->c:I

    add-int/2addr v0, p2

    if-le v0, v2, :cond_3

    .line 1052604
    iget-boolean v0, p1, LX/67F;->d:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1052605
    :cond_1
    iget v0, p1, LX/67F;->c:I

    add-int/2addr v0, p2

    iget v1, p1, LX/67F;->b:I

    sub-int/2addr v0, v1

    if-le v0, v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1052606
    :cond_2
    iget-object v0, p1, LX/67F;->a:[B

    iget v1, p1, LX/67F;->b:I

    iget-object v2, p1, LX/67F;->a:[B

    iget v3, p1, LX/67F;->c:I

    iget v4, p1, LX/67F;->b:I

    sub-int/2addr v3, v4

    invoke-static {v0, v1, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1052607
    iget v0, p1, LX/67F;->c:I

    iget v1, p1, LX/67F;->b:I

    sub-int/2addr v0, v1

    iput v0, p1, LX/67F;->c:I

    .line 1052608
    iput v5, p1, LX/67F;->b:I

    .line 1052609
    :cond_3
    iget-object v0, p0, LX/67F;->a:[B

    iget v1, p0, LX/67F;->b:I

    iget-object v2, p1, LX/67F;->a:[B

    iget v3, p1, LX/67F;->c:I

    invoke-static {v0, v1, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1052610
    iget v0, p1, LX/67F;->c:I

    add-int/2addr v0, p2

    iput v0, p1, LX/67F;->c:I

    .line 1052611
    iget v0, p0, LX/67F;->b:I

    add-int/2addr v0, p2

    iput v0, p0, LX/67F;->b:I

    .line 1052612
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1052592
    iget-object v0, p0, LX/67F;->g:LX/67F;

    if-ne v0, p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1052593
    :cond_0
    iget-object v0, p0, LX/67F;->g:LX/67F;

    iget-boolean v0, v0, LX/67F;->e:Z

    if-nez v0, :cond_2

    .line 1052594
    :cond_1
    :goto_0
    return-void

    .line 1052595
    :cond_2
    iget v0, p0, LX/67F;->c:I

    iget v1, p0, LX/67F;->b:I

    sub-int v1, v0, v1

    .line 1052596
    iget-object v0, p0, LX/67F;->g:LX/67F;

    iget v0, v0, LX/67F;->c:I

    rsub-int v2, v0, 0x2000

    iget-object v0, p0, LX/67F;->g:LX/67F;

    iget-boolean v0, v0, LX/67F;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v2

    .line 1052597
    if-gt v1, v0, :cond_1

    .line 1052598
    iget-object v0, p0, LX/67F;->g:LX/67F;

    invoke-virtual {p0, v0, v1}, LX/67F;->a(LX/67F;I)V

    .line 1052599
    invoke-virtual {p0}, LX/67F;->a()LX/67F;

    .line 1052600
    invoke-static {p0}, LX/67G;->a(LX/67F;)V

    goto :goto_0

    .line 1052601
    :cond_3
    iget-object v0, p0, LX/67F;->g:LX/67F;

    iget v0, v0, LX/67F;->b:I

    goto :goto_1
.end method
