.class public LX/6WP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:[I

.field public b:I

.field public c:I

.field private d:I

.field public e:F


# direct methods
.method public constructor <init>(I[IF)V
    .locals 0

    .prologue
    .line 1105935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1105936
    iput p1, p0, LX/6WP;->d:I

    .line 1105937
    iput-object p2, p0, LX/6WP;->a:[I

    .line 1105938
    iput p3, p0, LX/6WP;->e:F

    .line 1105939
    invoke-direct {p0}, LX/6WP;->f()V

    .line 1105940
    return-void
.end method

.method public synthetic constructor <init>(I[IFB)V
    .locals 0

    .prologue
    .line 1105934
    invoke-direct {p0, p1, p2, p3}, LX/6WP;-><init>(I[IF)V

    return-void
.end method

.method private e(I)I
    .locals 2

    .prologue
    .line 1105933
    invoke-virtual {p0, p1}, LX/6WP;->a(I)I

    move-result v0

    invoke-virtual {p0, p1}, LX/6WP;->c(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private f(I)I
    .locals 2

    .prologue
    .line 1105932
    invoke-virtual {p0, p1}, LX/6WP;->b(I)I

    move-result v0

    invoke-virtual {p0, p1}, LX/6WP;->d(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 1105922
    invoke-virtual {p0}, LX/6WP;->e()I

    move-result v1

    .line 1105923
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 1105924
    invoke-direct {p0, v0}, LX/6WP;->e(I)I

    move-result v2

    .line 1105925
    invoke-direct {p0, v0}, LX/6WP;->f(I)I

    move-result v3

    .line 1105926
    iget v4, p0, LX/6WP;->b:I

    if-ge v4, v2, :cond_0

    .line 1105927
    iput v2, p0, LX/6WP;->b:I

    .line 1105928
    :cond_0
    iget v2, p0, LX/6WP;->c:I

    if-ge v2, v3, :cond_1

    .line 1105929
    iput v3, p0, LX/6WP;->c:I

    .line 1105930
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1105931
    :cond_2
    return-void
.end method

.method public static newBuilder()LX/6WO;
    .locals 2

    .prologue
    .line 1105921
    new-instance v0, LX/6WO;

    invoke-direct {v0}, LX/6WO;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1105920
    iget v0, p0, LX/6WP;->b:I

    return v0
.end method

.method public final a(I)I
    .locals 2

    .prologue
    .line 1105919
    iget-object v0, p0, LX/6WP;->a:[I

    shl-int/lit8 v1, p1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1105918
    iget v0, p0, LX/6WP;->c:I

    return v0
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 1105913
    iget-object v0, p0, LX/6WP;->a:[I

    shl-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1105917
    iget v0, p0, LX/6WP;->d:I

    if-lez v0, :cond_0

    iget v0, p0, LX/6WP;->d:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/6WP;->c:I

    goto :goto_0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 1105916
    iget-object v0, p0, LX/6WP;->a:[I

    shl-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public final d(I)I
    .locals 2

    .prologue
    .line 1105915
    iget-object v0, p0, LX/6WP;->a:[I

    shl-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x3

    aget v0, v0, v1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1105914
    iget-object v0, p0, LX/6WP;->a:[I

    array-length v0, v0

    shr-int/lit8 v0, v0, 0x2

    return v0
.end method
