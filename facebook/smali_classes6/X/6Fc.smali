.class public LX/6Fc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/6Fg;",
        "LX/6Fh;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0W9;

.field public final d:LX/0Zh;

.field public final e:LX/0W3;

.field public final f:LX/0tK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1068389
    const-class v0, LX/6Fc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Fc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0W9;LX/0W3;LX/0tK;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0W9;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0tK;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1068390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1068391
    iput-object p1, p0, LX/6Fc;->b:LX/0Or;

    .line 1068392
    iput-object p2, p0, LX/6Fc;->c:LX/0W9;

    .line 1068393
    invoke-static {}, LX/0Zh;->a()LX/0Zh;

    move-result-object v0

    iput-object v0, p0, LX/6Fc;->d:LX/0Zh;

    .line 1068394
    iput-object p3, p0, LX/6Fc;->e:LX/0W3;

    .line 1068395
    iput-object p4, p0, LX/6Fc;->f:LX/0tK;

    .line 1068396
    return-void
.end method

.method public static a(LX/6Fc;LX/6Fg;LX/0n9;)V
    .locals 12

    .prologue
    .line 1068397
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nA;->a(LX/0nD;)V

    .line 1068398
    const-string v0, "description"

    .line 1068399
    iget-object v1, p1, LX/6Fg;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1068400
    invoke-static {p2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068401
    const-string v0, "category_id"

    .line 1068402
    iget-object v1, p1, LX/6Fg;->h:Ljava/lang/String;

    move-object v1, v1

    .line 1068403
    invoke-static {p2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068404
    const-string v0, "network_type"

    .line 1068405
    iget-object v1, p1, LX/6Fg;->n:Ljava/lang/String;

    move-object v1, v1

    .line 1068406
    invoke-static {p2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068407
    const-string v0, "network_subtype"

    .line 1068408
    iget-object v1, p1, LX/6Fg;->o:Ljava/lang/String;

    move-object v1, v1

    .line 1068409
    invoke-static {p2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068410
    const-string v0, "build_num"

    .line 1068411
    iget-object v1, p1, LX/6Fg;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1068412
    invoke-static {p2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068413
    const-string v0, "source"

    .line 1068414
    iget-object v1, p1, LX/6Fg;->q:LX/6Fb;

    move-object v1, v1

    .line 1068415
    invoke-virtual {v1}, LX/6Fb;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1068416
    invoke-static {p2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068417
    iget-object v0, p1, LX/6Fg;->f:LX/0P1;

    move-object v2, v0

    .line 1068418
    if-eqz v2, :cond_1

    .line 1068419
    invoke-virtual {v2}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1068420
    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1068421
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1068422
    invoke-static {p2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068423
    goto :goto_0

    .line 1068424
    :cond_1
    const-string v0, "misc_info"

    const/4 v5, 0x0

    .line 1068425
    new-instance v2, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 1068426
    const-string v1, "Git_Hash"

    .line 1068427
    iget-object v3, p1, LX/6Fg;->j:Ljava/lang/String;

    move-object v3, v3

    .line 1068428
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068429
    iget-object v1, p1, LX/6Fg;->m:Ljava/lang/String;

    move-object v1, v1

    .line 1068430
    if-eqz v1, :cond_2

    .line 1068431
    const-string v3, "Git_Branch"

    invoke-virtual {v2, v3, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068432
    :cond_2
    iget-object v1, p1, LX/6Fg;->l:Ljava/lang/String;

    move-object v1, v1

    .line 1068433
    if-eqz v1, :cond_3

    .line 1068434
    const-string v3, "Build_Time"

    invoke-virtual {v2, v3, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068435
    :cond_3
    const-string v1, "Report_ID"

    .line 1068436
    iget-object v3, p1, LX/6Fg;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1068437
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068438
    const-string v1, "Build_Num"

    .line 1068439
    iget-object v3, p1, LX/6Fg;->k:Ljava/lang/String;

    move-object v3, v3

    .line 1068440
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068441
    const-string v1, "OS_Version"

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068442
    const-string v1, "Manufacturer"

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068443
    const-string v1, "Model"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068444
    const-string v1, "Device Locale"

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068445
    const-string v1, "App Locale"

    iget-object v3, p0, LX/6Fc;->c:LX/0W9;

    invoke-virtual {v3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068446
    const-string v1, "Zombie(s)"

    .line 1068447
    iget-object v3, p1, LX/6Fg;->p:Ljava/lang/String;

    move-object v3, v3

    .line 1068448
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068449
    const-string v3, "Sent_On_Retry"

    .line 1068450
    iget-boolean v1, p1, LX/6Fg;->r:Z

    move v1, v1

    .line 1068451
    if-eqz v1, :cond_7

    const-string v1, "True"

    :goto_1
    invoke-virtual {v2, v3, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068452
    const-string v1, "Creation_Time"

    .line 1068453
    iget-object v3, p1, LX/6Fg;->s:Ljava/lang/String;

    move-object v3, v3

    .line 1068454
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068455
    const-string v1, "Send_Time"

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068456
    const-string v1, "Timed_Out_Attachments"

    .line 1068457
    iget-object v3, p1, LX/6Fg;->t:Ljava/lang/String;

    move-object v3, v3

    .line 1068458
    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068459
    iget-object v1, p0, LX/6Fc;->f:LX/0tK;

    invoke-virtual {v1}, LX/0tK;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1068460
    iget-object v1, p0, LX/6Fc;->f:LX/0tK;

    invoke-virtual {v1, v5}, LX/0tK;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1068461
    iget-object v1, p0, LX/6Fc;->f:LX/0tK;

    invoke-virtual {v1, v5}, LX/0tK;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1068462
    const-string v1, "data_saver"

    const-string v3, "active"

    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1068463
    :cond_4
    :goto_2
    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1068464
    invoke-static {p2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068465
    const-string v0, "attachment_file_names"

    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 1068466
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 1068467
    iget-object v7, p0, LX/6Fc;->e:LX/0W3;

    sget-wide v9, LX/0X5;->aY:J

    invoke-interface {v7, v9, v10, v5}, LX/0W4;->a(JZ)Z

    move-result v7

    .line 1068468
    if-eqz v7, :cond_5

    .line 1068469
    iget-object v7, p1, LX/6Fg;->c:LX/0Px;

    move-object v7, v7

    .line 1068470
    if-eqz v7, :cond_5

    .line 1068471
    iget-object v7, p1, LX/6Fg;->c:LX/0Px;

    move-object v7, v7

    .line 1068472
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    :goto_3
    if-ge v5, v7, :cond_5

    .line 1068473
    const-string v8, "screenshot-%d.png"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1068474
    invoke-virtual {v6, v8, v11}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1068475
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 1068476
    :cond_5
    iget-object v5, p1, LX/6Fg;->e:LX/0P1;

    move-object v5, v5

    .line 1068477
    invoke-virtual {v5}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v5

    invoke-virtual {v5}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1068478
    invoke-virtual {v6, v5, v11}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    goto :goto_4

    .line 1068479
    :cond_6
    invoke-virtual {v6}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, v5

    .line 1068480
    invoke-static {p2, v0, v1}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068481
    return-void

    .line 1068482
    :cond_7
    const-string v1, "False"

    goto/16 :goto_1

    .line 1068483
    :cond_8
    const-string v1, "data_saver"

    const-string v3, "not_active"

    invoke-virtual {v2, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_2
.end method

.method private e(LX/6Fg;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6Fg;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/4cQ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1068484
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1068485
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1068486
    iget-object v0, p0, LX/6Fc;->e:LX/0W3;

    sget-wide v2, LX/0X5;->aY:J

    invoke-interface {v0, v2, v3, v8}, LX/0W4;->a(JZ)Z

    move-result v0

    .line 1068487
    if-nez v0, :cond_2

    .line 1068488
    iget-object v0, p1, LX/6Fg;->c:LX/0Px;

    move-object v0, v0

    .line 1068489
    if-eqz v0, :cond_2

    .line 1068490
    iget-object v0, p1, LX/6Fg;->c:LX/0Px;

    move-object v0, v0

    .line 1068491
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1068492
    iget-object v0, p1, LX/6Fg;->c:LX/0Px;

    move-object v3, v0

    .line 1068493
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v8

    move v1, v8

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1068494
    :try_start_0
    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/net/URI;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 1068495
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Ljava/io/File;->canRead()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1068496
    :cond_0
    sget-object v0, LX/6Fc;->a:Ljava/lang/String;

    const-string v5, "Ignoring invalid screen shot file"

    invoke-static {v0, v5}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 1068497
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1068498
    :cond_1
    const-string v0, "screenshot-%d.png"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1068499
    new-instance v6, LX/4ct;

    const-string v7, "image/png"

    invoke-direct {v6, v5, v7, v0}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 1068500
    new-instance v5, LX/4cQ;

    invoke-direct {v5, v0, v6}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1068501
    add-int/lit8 v0, v1, 0x1

    goto :goto_1

    .line 1068502
    :catch_0
    move-exception v0

    .line 1068503
    sget-object v5, LX/6Fc;->a:Ljava/lang/String;

    const-string v6, "Ignoring invalid screen shot"

    invoke-static {v5, v6, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068504
    invoke-static {v0}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    goto :goto_1

    .line 1068505
    :cond_2
    iget-object v0, p1, LX/6Fg;->d:LX/0P1;

    move-object v0, v0

    .line 1068506
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1068507
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1068508
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1068509
    :try_start_1
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/net/URI;)V

    .line 1068510
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1068511
    sget-object v0, LX/6Fc;->a:Ljava/lang/String;

    const-string v1, "Ignoring invalid debug attachment"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068512
    const-string v0, "Attachment file not found, skipping: "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 1068513
    :catch_1
    move-exception v0

    .line 1068514
    sget-object v1, LX/6Fc;->a:Ljava/lang/String;

    const-string v2, "Ignoring invalid debug attachment: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v3, v4, v8

    invoke-static {v1, v0, v2, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1068515
    invoke-static {v0}, LX/23D;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1068516
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/6Fc;->c:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 1068517
    const-string v2, ".jpg"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, ".jpeg"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const-string v2, "image/jpeg"

    .line 1068518
    :goto_3
    new-instance v0, LX/4cu;

    const-wide/16 v4, 0x0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, LX/4cu;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 1068519
    new-instance v1, LX/4cQ;

    invoke-direct {v1, v3, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1068520
    :cond_5
    const-string v2, "text/plain"
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 1068521
    :cond_6
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_7

    .line 1068522
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1068523
    new-instance v1, LX/4cq;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/String;)[B

    move-result-object v0

    const-string v2, "text/plain"

    const-string v3, "missing_attachment_report.txt"

    invoke-direct {v1, v0, v2, v3}, LX/4cq;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    .line 1068524
    new-instance v0, LX/4cQ;

    const-string v2, "missing_attachment_report.txt"

    invoke-direct {v0, v2, v1}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1068525
    :cond_7
    return-object v9
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 12

    .prologue
    .line 1068526
    check-cast p1, LX/6Fg;

    .line 1068527
    iget-object v5, p0, LX/6Fc;->d:LX/0Zh;

    invoke-virtual {v5}, LX/0Zh;->b()LX/0n9;

    move-result-object v6

    .line 1068528
    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0nA;->a(LX/0nD;)V

    .line 1068529
    const-string v7, "user_identifier"

    iget-object v5, p0, LX/6Fc;->b:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1068530
    invoke-static {v6, v7, v5}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068531
    const-string v5, "client_time"

    .line 1068532
    iget-object v7, p1, LX/6Fg;->s:Ljava/lang/String;

    move-object v7, v7

    .line 1068533
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v7}, Ljava/util/Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    move-object v7, v8

    .line 1068534
    invoke-static {v6, v5, v7}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068535
    const-string v5, "config_id"

    const-string v7, "624618737631578"

    .line 1068536
    invoke-static {v6, v5, v7}, LX/0n9;->a(LX/0n9;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1068537
    const-string v5, "metadata"

    invoke-virtual {v6, v5}, LX/0n9;->b(Ljava/lang/String;)LX/0n9;

    move-result-object v5

    invoke-static {p0, p1, v5}, LX/6Fc;->a(LX/6Fc;LX/6Fg;LX/0n9;)V

    .line 1068538
    move-object v1, v6

    .line 1068539
    invoke-direct {p0, p1}, LX/6Fc;->e(LX/6Fg;)Ljava/util/List;

    move-result-object v2

    .line 1068540
    iget-object v0, p0, LX/6Fc;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1068541
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v3

    const-string v4, "bugReportUpload"

    .line 1068542
    iput-object v4, v3, LX/14O;->b:Ljava/lang/String;

    .line 1068543
    move-object v3, v3

    .line 1068544
    const-string v4, "POST"

    .line 1068545
    iput-object v4, v3, LX/14O;->c:Ljava/lang/String;

    .line 1068546
    move-object v3, v3

    .line 1068547
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/bugs"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1068548
    iput-object v0, v3, LX/14O;->d:Ljava/lang/String;

    .line 1068549
    move-object v0, v3

    .line 1068550
    iput-object v1, v0, LX/14O;->h:LX/0n9;

    .line 1068551
    move-object v0, v0

    .line 1068552
    iput-object v2, v0, LX/14O;->l:Ljava/util/List;

    .line 1068553
    move-object v0, v0

    .line 1068554
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1068555
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1068556
    move-object v0, v0

    .line 1068557
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1068558
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1068559
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 1068560
    sget-object v0, LX/6Fc;->a:Ljava/lang/String;

    const-string v1, "Bug report upload failed, error code: %d, msg: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 1068561
    iget v4, p2, LX/1pN;->b:I

    move v4, v4

    .line 1068562
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 1068563
    iget-object v4, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 1068564
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1068565
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 1068566
    iget-object v1, p2, LX/1pN;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1068567
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/6Fh;->a(ILjava/lang/String;)LX/6Fh;

    move-result-object v0

    .line 1068568
    :goto_0
    return-object v0

    .line 1068569
    :cond_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 1068570
    invoke-static {v0}, LX/6Fh;->a(Ljava/lang/String;)LX/6Fh;

    move-result-object v0

    goto :goto_0
.end method
