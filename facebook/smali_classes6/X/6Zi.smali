.class public LX/6Zi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static b:Ljava/lang/String;

.field private static volatile g:LX/6Zi;


# instance fields
.field public final c:LX/6aJ;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/0iA;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1111419
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MAPS_LAUNCH_EXTERNAL_MAP_APP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/6Zi;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/6aJ;Lcom/facebook/content/SecureContextHelper;LX/0iA;LX/0Ot;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/6aJ;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0iA;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1111409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1111410
    iput-object p2, p0, LX/6Zi;->c:LX/6aJ;

    .line 1111411
    iput-object p3, p0, LX/6Zi;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1111412
    iput-object p4, p0, LX/6Zi;->e:LX/0iA;

    .line 1111413
    iput-object p5, p0, LX/6Zi;->f:LX/0Ot;

    .line 1111414
    sget-object v0, LX/6Zi;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1111415
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1111416
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, LX/6Zi;->b:Ljava/lang/String;

    .line 1111417
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/6Zi;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Zi;->b:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1111418
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/6Zi;
    .locals 9

    .prologue
    .line 1111396
    sget-object v0, LX/6Zi;->g:LX/6Zi;

    if-nez v0, :cond_1

    .line 1111397
    const-class v1, LX/6Zi;

    monitor-enter v1

    .line 1111398
    :try_start_0
    sget-object v0, LX/6Zi;->g:LX/6Zi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1111399
    if-eqz v2, :cond_0

    .line 1111400
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1111401
    new-instance v3, LX/6Zi;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/6aJ;->a(LX/0QB;)LX/6aJ;

    move-result-object v5

    check-cast v5, LX/6aJ;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v7

    check-cast v7, LX/0iA;

    const/16 v8, 0x2eb

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/6Zi;-><init>(Landroid/content/Context;LX/6aJ;Lcom/facebook/content/SecureContextHelper;LX/0iA;LX/0Ot;)V

    .line 1111402
    move-object v0, v3

    .line 1111403
    sput-object v0, LX/6Zi;->g:LX/6Zi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1111404
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1111405
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1111406
    :cond_1
    sget-object v0, LX/6Zi;->g:LX/6Zi;

    return-object v0

    .line 1111407
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1111408
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(DDLjava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1111392
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1111393
    :goto_0
    const-string v1, "%s?daddr=%s"

    const-string v2, "http://maps.google.com/maps"

    invoke-static {v1, v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1111394
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.REFERRER"

    sget-object v2, LX/6Zi;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.facebook.intent.extra.SKIP_IN_APP_BROWSER"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 1111395
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;DDLjava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1111385
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1111386
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "geo:0,0?q="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.REFERRER"

    sget-object v3, LX/6Zi;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.facebook.intent.extra.SKIP_IN_APP_BROWSER"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 1111387
    invoke-static {p0, v1}, LX/2A3;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1111388
    const-string v2, "%s?q=%s"

    const-string v3, "http://maps.google.com/maps"

    invoke-static {v2, v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1111389
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1111390
    :cond_0
    return-object v1

    .line 1111391
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1111346
    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v1 .. v10}, LX/6Zi;->a(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnDismissListener;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1111367
    const/4 v2, 0x0

    .line 1111368
    const-string v3, "after_party"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "native_page_profile"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "native_story"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1111369
    :cond_0
    const/4 v2, 0x1

    .line 1111370
    :cond_1
    if-eqz v2, :cond_3

    .line 1111371
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1111372
    const-string v2, "place_name"

    move-object/from16 v0, p7

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111373
    const-string v2, "address"

    move-object/from16 v0, p8

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111374
    const-string v2, "latitude"

    move-wide/from16 v0, p3

    invoke-virtual {v3, v2, v0, v1}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 1111375
    const-string v2, "longitude"

    move-wide/from16 v0, p5

    invoke-virtual {v3, v2, v0, v1}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 1111376
    const-string v2, "zoom"

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 1111377
    const-string v2, "curation_surface"

    invoke-virtual {v3, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111378
    iget-object v2, p0, LX/6Zi;->c:LX/6aJ;

    const-string v4, "latitude_longitude"

    invoke-virtual {v2, p2, v4}, LX/6aJ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111379
    iget-object v2, p0, LX/6Zi;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/17W;

    sget-object v4, LX/0ax;->gH:Ljava/lang/String;

    invoke-virtual {v2, p1, v4, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 1111380
    :cond_2
    :goto_0
    return-void

    .line 1111381
    :cond_3
    iget-object v2, p0, LX/6Zi;->c:LX/6aJ;

    const-string v3, "latitude_longitude"

    invoke-virtual {v2, p2, v3}, LX/6aJ;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, p1

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-object/from16 v8, p8

    .line 1111382
    invoke-static/range {v3 .. v8}, LX/6Zi;->a(Landroid/content/Context;DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 1111383
    const-string v2, "native_page_profile"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v3, p0

    move-object v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-direct/range {v3 .. v11}, LX/6Zi;->a(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1111384
    :cond_4
    iget-object v2, p0, LX/6Zi;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v5, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnDismissListener;)Z
    .locals 13
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Landroid/content/DialogInterface$OnDismissListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1111352
    iget-object v2, p0, LX/6Zi;->e:LX/0iA;

    sget-object v3, LX/6Zi;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/0i1;

    move-result-object v12

    .line 1111353
    instance-of v2, v12, LX/3kY;

    if-nez v2, :cond_0

    .line 1111354
    const/4 v2, 0x0

    .line 1111355
    :goto_0
    return v2

    .line 1111356
    :cond_0
    new-instance v3, LX/6a9;

    move-object v4, p1

    move-object v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-direct/range {v3 .. v11}, LX/6a9;-><init>(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;)V

    .line 1111357
    new-instance v2, LX/4me;

    invoke-direct {v2, p1}, LX/4me;-><init>(Landroid/content/Context;)V

    .line 1111358
    const v4, 0x7f0801c6

    invoke-virtual {v2, v4}, LX/4me;->setTitle(I)V

    .line 1111359
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, LX/2EJ;->a(Landroid/view/View;IIII)V

    .line 1111360
    new-instance v4, LX/6Zf;

    move-object/from16 v0, p9

    invoke-direct {v4, p0, v0}, LX/6Zf;-><init>(LX/6Zi;Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v2, v4}, LX/4me;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1111361
    new-instance v4, LX/6Zg;

    invoke-direct {v4, p0, v2}, LX/6Zg;-><init>(LX/6Zi;LX/4me;)V

    invoke-virtual {v3, v4}, LX/6a9;->setOnIntentClickListener(LX/6Zg;)V

    .line 1111362
    new-instance v4, LX/6Zh;

    invoke-direct {v4, p0}, LX/6Zh;-><init>(LX/6Zi;)V

    invoke-virtual {v3, v4}, LX/6a9;->setOnMapImageDownloadListener(LX/6Zh;)V

    .line 1111363
    invoke-virtual {v2}, LX/4me;->show()V

    .line 1111364
    iget-object v2, p0, LX/6Zi;->c:LX/6aJ;

    invoke-virtual {v2}, LX/6aJ;->a()V

    .line 1111365
    iget-object v2, p0, LX/6Zi;->e:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    invoke-interface {v12}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1111366
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V
    .locals 17
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1111347
    move-object/from16 v0, p0

    iget-object v6, v0, LX/6Zi;->c:LX/6aJ;

    const-string v7, "directions_latitude_longitude"

    move-object/from16 v0, p2

    invoke-virtual {v6, v0, v7}, LX/6aJ;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111348
    move-wide/from16 v0, p3

    move-wide/from16 v2, p5

    move-object/from16 v4, p8

    invoke-static {v0, v1, v2, v3, v4}, LX/6Zi;->a(DDLjava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-wide/from16 v10, p3

    move-wide/from16 v12, p5

    move-object/from16 v14, p7

    move-object/from16 v15, p8

    .line 1111349
    invoke-direct/range {v7 .. v15}, LX/6Zi;->a(Landroid/content/Context;Landroid/content/Intent;DDLjava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1111350
    move-object/from16 v0, p0

    iget-object v6, v0, LX/6Zi;->d:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p1

    invoke-interface {v6, v9, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1111351
    :cond_0
    return-void
.end method
