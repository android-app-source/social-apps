.class public LX/6Yf;
.super LX/6Ya;
.source ""


# instance fields
.field public final c:LX/7Y7;

.field public final d:LX/4g9;

.field public final e:LX/7YP;


# direct methods
.method public constructor <init>(LX/7Y7;LX/4g9;LX/7YP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110341
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1110342
    iput-object p1, p0, LX/6Yf;->c:LX/7Y7;

    .line 1110343
    iput-object p2, p0, LX/6Yf;->d:LX/4g9;

    .line 1110344
    iput-object p3, p0, LX/6Yf;->e:LX/7YP;

    .line 1110345
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1110346
    iget-object v0, p0, LX/6Yf;->d:LX/4g9;

    sget-object v1, LX/4g6;->d:LX/4g6;

    invoke-virtual {p0}, LX/6Ya;->e()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4g9;->a(LX/4g5;Ljava/util/Map;)V

    .line 1110347
    new-instance v0, LX/6YP;

    invoke-direct {v0, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1110348
    invoke-virtual {p0, v0}, LX/6Ya;->a(LX/6YP;)V

    .line 1110349
    return-object v0
.end method

.method public final a(LX/6YP;)V
    .locals 11

    .prologue
    .line 1110350
    invoke-virtual {p0}, LX/6Ya;->f()LX/6Y5;

    move-result-object v0

    iget-object v1, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110351
    iget-object v2, v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1110352
    iget-object v2, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110353
    iget-object v3, v2, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->f:Ljava/lang/String;

    move-object v2, v3

    .line 1110354
    iget-object v3, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110355
    iget-object v4, v3, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->e:Ljava/lang/String;

    move-object v3, v4

    .line 1110356
    iget-object v4, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110357
    iget-object v5, v4, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->h:Ljava/lang/String;

    move-object v4, v5

    .line 1110358
    iput-object v1, v0, LX/6Y5;->e:Ljava/lang/String;

    .line 1110359
    iput-object v2, v0, LX/6Y5;->f:Ljava/lang/String;

    .line 1110360
    iput-object v3, v0, LX/6Y5;->g:Ljava/lang/String;

    .line 1110361
    iput-object v4, v0, LX/6Y5;->h:Ljava/lang/String;

    .line 1110362
    move-object v0, v0

    .line 1110363
    iget-object v1, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110364
    iget-object v2, v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->g:Ljava/lang/String;

    move-object v1, v2

    .line 1110365
    new-instance v5, LX/6Ye;

    iget-object v7, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    iget-object v8, p0, LX/6Yf;->c:LX/7Y7;

    iget-object v9, p0, LX/6Yf;->d:LX/4g9;

    iget-object v10, p0, LX/6Yf;->e:LX/7YP;

    move-object v6, p0

    invoke-direct/range {v5 .. v10}, LX/6Ye;-><init>(LX/6Yf;Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;LX/7Y7;LX/4g9;LX/7YP;)V

    move-object v2, v5

    .line 1110366
    invoke-virtual {v0, v1, v2}, LX/6Y5;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v0

    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v2, 0x7f080e0a

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/6YN;->PROMOS_LIST:LX/6YN;

    invoke-virtual {p0, v2}, LX/6Ya;->a(LX/6YN;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Y5;->a(Ljava/lang/Boolean;)LX/6Y5;

    move-result-object v0

    .line 1110367
    invoke-virtual {p1, v0}, LX/6YP;->a(LX/6Y5;)V

    .line 1110368
    return-void
.end method
