.class public final LX/66D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65J;


# instance fields
.field public final synthetic a:LX/66H;

.field private final b:LX/675;

.field private c:Z

.field private d:J


# direct methods
.method public constructor <init>(LX/66H;J)V
    .locals 2

    .prologue
    .line 1049622
    iput-object p1, p0, LX/66D;->a:LX/66H;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049623
    new-instance v0, LX/675;

    iget-object v1, p0, LX/66D;->a:LX/66H;

    iget-object v1, v1, LX/66H;->d:LX/670;

    invoke-interface {v1}, LX/65J;->a()LX/65f;

    move-result-object v1

    invoke-direct {v0, v1}, LX/675;-><init>(LX/65f;)V

    iput-object v0, p0, LX/66D;->b:LX/675;

    .line 1049624
    iput-wide p2, p0, LX/66D;->d:J

    .line 1049625
    return-void
.end method


# virtual methods
.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1049626
    iget-object v0, p0, LX/66D;->b:LX/675;

    return-object v0
.end method

.method public final a_(LX/672;J)V
    .locals 8

    .prologue
    .line 1049627
    iget-boolean v0, p0, LX/66D;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049628
    :cond_0
    iget-wide v6, p1, LX/672;->b:J

    move-wide v0, v6

    .line 1049629
    const-wide/16 v2, 0x0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, LX/65A;->a(JJJ)V

    .line 1049630
    iget-wide v0, p0, LX/66D;->d:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    .line 1049631
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/66D;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes but received "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049632
    :cond_1
    iget-object v0, p0, LX/66D;->a:LX/66H;

    iget-object v0, v0, LX/66H;->d:LX/670;

    invoke-interface {v0, p1, p2, p3}, LX/65J;->a_(LX/672;J)V

    .line 1049633
    iget-wide v0, p0, LX/66D;->d:J

    sub-long/2addr v0, p2

    iput-wide v0, p0, LX/66D;->d:J

    .line 1049634
    return-void
.end method

.method public final close()V
    .locals 4

    .prologue
    .line 1049635
    iget-boolean v0, p0, LX/66D;->c:Z

    if-eqz v0, :cond_0

    .line 1049636
    :goto_0
    return-void

    .line 1049637
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/66D;->c:Z

    .line 1049638
    iget-wide v0, p0, LX/66D;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049639
    :cond_1
    iget-object v1, p0, LX/66D;->b:LX/675;

    .line 1049640
    iget-object v2, v1, LX/675;->a:LX/65f;

    move-object v2, v2

    .line 1049641
    sget-object v0, LX/65f;->b:LX/65f;

    invoke-virtual {v1, v0}, LX/675;->a(LX/65f;)LX/675;

    .line 1049642
    invoke-virtual {v2}, LX/65f;->f()LX/65f;

    .line 1049643
    invoke-virtual {v2}, LX/65f;->bL_()LX/65f;

    .line 1049644
    iget-object v0, p0, LX/66D;->a:LX/66H;

    const/4 v1, 0x3

    .line 1049645
    iput v1, v0, LX/66H;->e:I

    .line 1049646
    goto :goto_0
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 1049647
    iget-boolean v0, p0, LX/66D;->c:Z

    if-eqz v0, :cond_0

    .line 1049648
    :goto_0
    return-void

    .line 1049649
    :cond_0
    iget-object v0, p0, LX/66D;->a:LX/66H;

    iget-object v0, v0, LX/66H;->d:LX/670;

    invoke-interface {v0}, LX/670;->flush()V

    goto :goto_0
.end method
