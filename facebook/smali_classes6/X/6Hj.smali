.class public LX/6Hj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/camera/gating/CustomCameraShutterSoundGk;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/camera/gating/SoftCameraShutterSoundGk;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/camera/gating/FaceDetectionGk;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/camera/gating/RotatePortraitFrontFacingPicturesGk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1072784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072785
    iput-object p1, p0, LX/6Hj;->a:LX/0Or;

    .line 1072786
    iput-object p2, p0, LX/6Hj;->b:LX/0Or;

    .line 1072787
    iput-object p3, p0, LX/6Hj;->c:LX/0Or;

    .line 1072788
    iput-object p4, p0, LX/6Hj;->d:LX/0Or;

    .line 1072789
    return-void
.end method

.method public static a(LX/0QB;)LX/6Hj;
    .locals 1

    .prologue
    .line 1072790
    invoke-static {p0}, LX/6Hj;->b(LX/0QB;)LX/6Hj;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/6Hj;
    .locals 5

    .prologue
    .line 1072791
    new-instance v0, LX/6Hj;

    const/16 v1, 0x145a

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x304

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x145c

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x145e

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/6Hj;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 1072792
    return-object v0
.end method
