.class public LX/67a;
.super LX/1Lc;
.source ""


# instance fields
.field public a:LX/3u3;

.field public b:LX/67V;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1053219
    invoke-direct {p0}, LX/1Lc;-><init>()V

    .line 1053220
    return-void
.end method

.method public static a(LX/0QB;)LX/67a;
    .locals 1

    .prologue
    .line 1053216
    new-instance v0, LX/67a;

    invoke-direct {v0}, LX/67a;-><init>()V

    .line 1053217
    move-object v0, v0

    .line 1053218
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;Landroid/view/MenuItem;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Landroid/view/MenuItem;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1053196
    invoke-virtual {p0}, LX/67a;->f()LX/3u1;

    move-result-object v0

    .line 1053197
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/3u1;->b()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 1053198
    const/4 v1, 0x0

    .line 1053199
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1053200
    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    move v0, v1

    .line 1053201
    :goto_0
    move v0, v0

    .line 1053202
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 1053203
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_1

    .line 1053204
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1053205
    invoke-static {v0}, LX/3pR;->b(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v2

    .line 1053206
    if-eqz v2, :cond_3

    .line 1053207
    invoke-static {v0, v2}, LX/3pR;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1053208
    invoke-static {v0}, LX/3qU;->a(Landroid/content/Context;)LX/3qU;

    move-result-object v1

    .line 1053209
    invoke-virtual {v1, v0}, LX/3qU;->a(Landroid/app/Activity;)LX/3qU;

    .line 1053210
    invoke-virtual {v1}, LX/3qU;->a()V

    .line 1053211
    :try_start_0
    invoke-static {v0}, LX/3p6;->a(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1053212
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1053213
    :catch_0
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_2

    .line 1053214
    :cond_2
    invoke-static {v0, v2}, LX/3pR;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_2

    :cond_3
    move v0, v1

    .line 1053215
    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/LayoutInflater;",
            "Landroid/view/ViewGroup;",
            "Landroid/os/Bundle;",
            ")",
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1053192
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0, p1, p2, p3}, LX/3u3;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1053193
    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/67a;->c:Z

    .line 1053194
    return-object v1

    .line 1053195
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1053190
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->d()V

    .line 1053191
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1053188
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0, p1}, LX/3u3;->a(Landroid/os/Bundle;)V

    .line 1053189
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1053221
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1053222
    if-eqz v0, :cond_0

    .line 1053223
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->d()V

    .line 1053224
    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1053158
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0, p1}, LX/3u3;->b(I)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1053182
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1053183
    if-nez v0, :cond_0

    .line 1053184
    const/4 v0, 0x0

    .line 1053185
    :goto_0
    return v0

    .line 1053186
    :cond_0
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->b()Landroid/view/MenuInflater;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 1053187
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1053178
    invoke-super {p0, p1}, LX/1Lc;->b(Landroid/support/v4/app/Fragment;)V

    .line 1053179
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/67a;->c:Z

    .line 1053180
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->c()V

    .line 1053181
    return-void
.end method

.method public final e()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 1053177
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->b()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1053174
    invoke-super {p0, p1}, LX/1Lc;->e(Landroid/support/v4/app/Fragment;)V

    .line 1053175
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->e()V

    .line 1053176
    return-void
.end method

.method public final f()LX/3u1;
    .locals 1

    .prologue
    .line 1053171
    iget-boolean v0, p0, LX/67a;->c:Z

    if-nez v0, :cond_0

    .line 1053172
    const/4 v0, 0x0

    .line 1053173
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/67a;->a:LX/3u3;

    invoke-virtual {v0}, LX/3u3;->a()LX/3u1;

    move-result-object v0

    goto :goto_0
.end method

.method public final f(Landroid/support/v4/app/Fragment;)V
    .locals 4

    .prologue
    .line 1053159
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1053160
    if-eqz v0, :cond_0

    .line 1053161
    new-instance v1, LX/3uP;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {v1, v0}, LX/3uP;-><init>(Landroid/view/Window;)V

    move-object v0, v1

    .line 1053162
    :goto_0
    move-object v0, v0

    .line 1053163
    new-instance v1, LX/67Y;

    invoke-direct {v1, p0}, LX/67Y;-><init>(LX/67a;)V

    .line 1053164
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_1

    .line 1053165
    new-instance v2, LX/3uA;

    invoke-direct {v2, p1, v0, v1}, LX/3uA;-><init>(Landroid/support/v4/app/Fragment;LX/3uM;LX/3u2;)V

    .line 1053166
    :goto_1
    move-object v0, v2

    .line 1053167
    iput-object v0, p0, LX/67a;->a:LX/3u3;

    .line 1053168
    return-void

    :cond_0
    new-instance v0, LX/3uN;

    .line 1053169
    new-instance v1, LX/67Z;

    invoke-direct {v1, p0, p1}, LX/67Z;-><init>(LX/67a;Landroid/support/v4/app/Fragment;)V

    move-object v1, v1

    .line 1053170
    invoke-direct {v0, p1, v1}, LX/3uN;-><init>(Landroid/support/v4/app/Fragment;Landroid/view/Window$Callback;)V

    goto :goto_0

    :cond_1
    new-instance v2, LX/3u8;

    invoke-direct {v2, p1, v0, v1}, LX/3u8;-><init>(Landroid/support/v4/app/Fragment;LX/3uM;LX/3u2;)V

    goto :goto_1
.end method
