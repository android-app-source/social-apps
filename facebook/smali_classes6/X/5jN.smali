.class public final LX/5jN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5iE;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/net/Uri;

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:F

.field public h:I

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:Z


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/TextParams;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 986378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986379
    iput-object v2, p0, LX/5jN;->a:Ljava/lang/String;

    .line 986380
    iput-object v2, p0, LX/5jN;->b:Landroid/net/Uri;

    .line 986381
    iput v0, p0, LX/5jN;->c:F

    .line 986382
    iput v0, p0, LX/5jN;->d:F

    .line 986383
    iput v0, p0, LX/5jN;->e:F

    .line 986384
    iput v0, p0, LX/5jN;->f:F

    .line 986385
    iput v0, p0, LX/5jN;->g:F

    .line 986386
    iput v1, p0, LX/5jN;->h:I

    .line 986387
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5jN;->j:Z

    .line 986388
    iput-boolean v1, p0, LX/5jN;->k:Z

    .line 986389
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/TextParams;->textString:Ljava/lang/String;

    iput-object v0, p0, LX/5jN;->a:Ljava/lang/String;

    .line 986390
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->d()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/5jN;->b:Landroid/net/Uri;

    .line 986391
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->n()F

    move-result v0

    iput v0, p0, LX/5jN;->c:F

    .line 986392
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->o()F

    move-result v0

    iput v0, p0, LX/5jN;->d:F

    .line 986393
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->e()F

    move-result v0

    iput v0, p0, LX/5jN;->e:F

    .line 986394
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->f()F

    move-result v0

    iput v0, p0, LX/5jN;->f:F

    .line 986395
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->c()F

    move-result v0

    iput v0, p0, LX/5jN;->g:F

    .line 986396
    iget v0, p1, Lcom/facebook/photos/creativeediting/model/TextParams;->textColor:I

    iput v0, p0, LX/5jN;->h:I

    .line 986397
    iget-boolean v0, p1, Lcom/facebook/photos/creativeediting/model/TextParams;->isSelectable:Z

    iput-boolean v0, p0, LX/5jN;->j:Z

    .line 986398
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/TextParams;->j()Z

    move-result v0

    iput-boolean v0, p0, LX/5jN;->k:Z

    .line 986399
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 986417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986418
    iput-object v2, p0, LX/5jN;->a:Ljava/lang/String;

    .line 986419
    iput-object v2, p0, LX/5jN;->b:Landroid/net/Uri;

    .line 986420
    iput v0, p0, LX/5jN;->c:F

    .line 986421
    iput v0, p0, LX/5jN;->d:F

    .line 986422
    iput v0, p0, LX/5jN;->e:F

    .line 986423
    iput v0, p0, LX/5jN;->f:F

    .line 986424
    iput v0, p0, LX/5jN;->g:F

    .line 986425
    iput v1, p0, LX/5jN;->h:I

    .line 986426
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5jN;->j:Z

    .line 986427
    iput-boolean v1, p0, LX/5jN;->k:Z

    .line 986428
    iput-object p1, p0, LX/5jN;->a:Ljava/lang/String;

    .line 986429
    iput-object p2, p0, LX/5jN;->b:Landroid/net/Uri;

    .line 986430
    return-void
.end method


# virtual methods
.method public final a(Z)LX/5iE;
    .locals 1

    .prologue
    .line 986416
    return-object p0
.end method

.method public final a()Lcom/facebook/photos/creativeediting/model/TextParams;
    .locals 2

    .prologue
    .line 986413
    iget-object v0, p0, LX/5jN;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 986414
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5jN;->i:Ljava/lang/String;

    .line 986415
    :cond_0
    new-instance v0, Lcom/facebook/photos/creativeediting/model/TextParams;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/model/TextParams;-><init>(LX/5jN;)V

    return-object v0
.end method

.method public final synthetic b()LX/5i8;
    .locals 1

    .prologue
    .line 986412
    invoke-virtual {p0}, LX/5jN;->a()Lcom/facebook/photos/creativeediting/model/TextParams;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f(F)LX/5iE;
    .locals 1

    .prologue
    .line 986431
    iput p1, p0, LX/5jN;->f:F

    .line 986432
    move-object v0, p0

    .line 986433
    return-object v0
.end method

.method public final synthetic g(F)LX/5iE;
    .locals 1

    .prologue
    .line 986409
    iput p1, p0, LX/5jN;->e:F

    .line 986410
    move-object v0, p0

    .line 986411
    return-object v0
.end method

.method public final synthetic h(F)LX/5iE;
    .locals 1

    .prologue
    .line 986406
    iput p1, p0, LX/5jN;->d:F

    .line 986407
    move-object v0, p0

    .line 986408
    return-object v0
.end method

.method public final synthetic i(F)LX/5iE;
    .locals 1

    .prologue
    .line 986403
    iput p1, p0, LX/5jN;->c:F

    .line 986404
    move-object v0, p0

    .line 986405
    return-object v0
.end method

.method public final synthetic j(F)LX/5iE;
    .locals 1

    .prologue
    .line 986400
    iput p1, p0, LX/5jN;->g:F

    .line 986401
    move-object v0, p0

    .line 986402
    return-object v0
.end method
