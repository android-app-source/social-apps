.class public final LX/66F;
.super LX/66A;
.source ""


# instance fields
.field public final synthetic d:LX/66H;

.field private e:Z


# direct methods
.method public constructor <init>(LX/66H;)V
    .locals 1

    .prologue
    .line 1049672
    iput-object p1, p0, LX/66F;->d:LX/66H;

    invoke-direct {p0, p1}, LX/66A;-><init>(LX/66H;)V

    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 6

    .prologue
    const/4 v5, 0x1

    const-wide/16 v0, -0x1

    .line 1049673
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049674
    :cond_0
    iget-boolean v2, p0, LX/66A;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049675
    :cond_1
    iget-boolean v2, p0, LX/66F;->e:Z

    if-eqz v2, :cond_2

    .line 1049676
    :goto_0
    return-wide v0

    .line 1049677
    :cond_2
    iget-object v2, p0, LX/66F;->d:LX/66H;

    iget-object v2, v2, LX/66H;->c:LX/671;

    invoke-interface {v2, p1, p2, p3}, LX/65D;->a(LX/672;J)J

    move-result-wide v2

    .line 1049678
    cmp-long v4, v2, v0

    if-nez v4, :cond_3

    .line 1049679
    iput-boolean v5, p0, LX/66F;->e:Z

    .line 1049680
    invoke-virtual {p0, v5}, LX/66A;->a(Z)V

    goto :goto_0

    :cond_3
    move-wide v0, v2

    .line 1049681
    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1049682
    iget-boolean v0, p0, LX/66A;->b:Z

    if-eqz v0, :cond_0

    .line 1049683
    :goto_0
    return-void

    .line 1049684
    :cond_0
    iget-boolean v0, p0, LX/66F;->e:Z

    if-nez v0, :cond_1

    .line 1049685
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/66A;->a(Z)V

    .line 1049686
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/66F;->b:Z

    goto :goto_0
.end method
