.class public final LX/5AB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 857134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 857135
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 857136
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 857137
    invoke-static {p0, p1}, LX/5AB;->b(LX/15w;LX/186;)I

    move-result v1

    .line 857138
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 857139
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 857207
    const/16 v18, 0x0

    .line 857208
    const/16 v17, 0x0

    .line 857209
    const/16 v16, 0x0

    .line 857210
    const/4 v15, 0x0

    .line 857211
    const/4 v14, 0x0

    .line 857212
    const/4 v13, 0x0

    .line 857213
    const/4 v12, 0x0

    .line 857214
    const/4 v11, 0x0

    .line 857215
    const/4 v10, 0x0

    .line 857216
    const/4 v9, 0x0

    .line 857217
    const/4 v8, 0x0

    .line 857218
    const/4 v7, 0x0

    .line 857219
    const/4 v6, 0x0

    .line 857220
    const/4 v5, 0x0

    .line 857221
    const/4 v4, 0x0

    .line 857222
    const/4 v3, 0x0

    .line 857223
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 857224
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 857225
    const/4 v3, 0x0

    .line 857226
    :goto_0
    return v3

    .line 857227
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 857228
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_11

    .line 857229
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 857230
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 857231
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 857232
    const-string v20, "action_links"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 857233
    invoke-static/range {p0 .. p1}, LX/57A;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 857234
    :cond_2
    const-string v20, "associated_application"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 857235
    invoke-static/range {p0 .. p1}, LX/40v;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 857236
    :cond_3
    const-string v20, "attachment_properties"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 857237
    invoke-static/range {p0 .. p1}, LX/58U;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 857238
    :cond_4
    const-string v20, "deduplication_key"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 857239
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 857240
    :cond_5
    const-string v20, "description"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 857241
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 857242
    :cond_6
    const-string v20, "media"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 857243
    invoke-static/range {p0 .. p1}, LX/5AA;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 857244
    :cond_7
    const-string v20, "media_reference_token"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 857245
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 857246
    :cond_8
    const-string v20, "source"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 857247
    invoke-static/range {p0 .. p1}, LX/58V;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 857248
    :cond_9
    const-string v20, "style_infos"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 857249
    invoke-static/range {p0 .. p1}, LX/58k;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 857250
    :cond_a
    const-string v20, "style_list"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 857251
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 857252
    :cond_b
    const-string v20, "subtitle"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 857253
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 857254
    :cond_c
    const-string v20, "target"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 857255
    invoke-static/range {p0 .. p1}, LX/58i;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 857256
    :cond_d
    const-string v20, "title"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 857257
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 857258
    :cond_e
    const-string v20, "title_with_entities"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 857259
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 857260
    :cond_f
    const-string v20, "tracking"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 857261
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 857262
    :cond_10
    const-string v20, "url"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 857263
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 857264
    :cond_11
    const/16 v19, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 857265
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 857266
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 857267
    const/16 v17, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 857268
    const/16 v16, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 857269
    const/4 v15, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 857270
    const/4 v14, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 857271
    const/4 v13, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 857272
    const/4 v12, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 857273
    const/16 v11, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 857274
    const/16 v10, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 857275
    const/16 v9, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 857276
    const/16 v8, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 857277
    const/16 v7, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 857278
    const/16 v6, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 857279
    const/16 v5, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 857280
    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 857281
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0x9

    .line 857140
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 857141
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 857142
    if-eqz v0, :cond_0

    .line 857143
    const-string v1, "action_links"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857144
    invoke-static {p0, v0, p2, p3}, LX/57A;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 857145
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 857146
    if-eqz v0, :cond_1

    .line 857147
    const-string v1, "associated_application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857148
    invoke-static {p0, v0, p2, p3}, LX/40v;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 857149
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 857150
    if-eqz v0, :cond_2

    .line 857151
    const-string v1, "attachment_properties"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857152
    invoke-static {p0, v0, p2, p3}, LX/58U;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 857153
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 857154
    if-eqz v0, :cond_3

    .line 857155
    const-string v1, "deduplication_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857156
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 857157
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 857158
    if-eqz v0, :cond_4

    .line 857159
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857160
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 857161
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 857162
    if-eqz v0, :cond_5

    .line 857163
    const-string v1, "media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857164
    invoke-static {p0, v0, p2, p3}, LX/5AA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 857165
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 857166
    if-eqz v0, :cond_6

    .line 857167
    const-string v1, "media_reference_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857168
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 857169
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 857170
    if-eqz v0, :cond_7

    .line 857171
    const-string v1, "source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857172
    invoke-static {p0, v0, p2}, LX/58V;->a(LX/15i;ILX/0nX;)V

    .line 857173
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 857174
    if-eqz v0, :cond_8

    .line 857175
    const-string v1, "style_infos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857176
    invoke-static {p0, v0, p2, p3}, LX/58k;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 857177
    :cond_8
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 857178
    if-eqz v0, :cond_9

    .line 857179
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857180
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 857181
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 857182
    if-eqz v0, :cond_a

    .line 857183
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857184
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 857185
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 857186
    if-eqz v0, :cond_b

    .line 857187
    const-string v1, "target"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857188
    invoke-static {p0, v0, p2, p3}, LX/58i;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 857189
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 857190
    if-eqz v0, :cond_c

    .line 857191
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857192
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 857193
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 857194
    if-eqz v0, :cond_d

    .line 857195
    const-string v1, "title_with_entities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857196
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 857197
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 857198
    if-eqz v0, :cond_e

    .line 857199
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857200
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 857201
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 857202
    if-eqz v0, :cond_f

    .line 857203
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 857204
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 857205
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 857206
    return-void
.end method
