.class public LX/5QO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 913118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 913119
    return-void
.end method

.method public static b(LX/15i;I)Lcom/facebook/greetingcards/model/GreetingCard$Slide;
    .locals 12

    .prologue
    .line 913120
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 913121
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    invoke-virtual {p0, v0, v1, v2}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 913122
    :goto_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    .line 913123
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v6, :cond_7

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;

    .line 913124
    const/4 v2, 0x1

    if-ne v5, v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->l()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_5

    .line 913125
    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->l()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 913126
    const/4 v8, 0x0

    invoke-virtual {v7, v2, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-nez v2, :cond_4

    const/4 v0, 0x0

    :goto_3
    invoke-static {v7, v8, v0}, Lcom/facebook/greetingcards/model/CardPhoto;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/model/CardPhoto;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 913127
    :cond_0
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 913128
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 913129
    move-object v1, v0

    goto :goto_0

    .line 913130
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 913131
    :cond_4
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;->a()D

    move-result-wide v10

    double-to-float v9, v10

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;->b()D

    move-result-wide v10

    double-to-float v0, v10

    invoke-direct {v2, v9, v0}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v0, v2

    goto :goto_3

    .line 913132
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->m()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    .line 913133
    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->m()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 913134
    const/4 v8, 0x0

    invoke-virtual {v7, v2, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-nez v2, :cond_6

    const/4 v0, 0x0

    :goto_5
    invoke-static {v7, v8, v0}, Lcom/facebook/greetingcards/model/CardPhoto;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/model/CardPhoto;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    :cond_6
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;->a()D

    move-result-wide v10

    double-to-float v9, v10

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$SlidesModel$NodesModel$PhotosModel$PhotosNodesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;->b()D

    move-result-wide v10

    double-to-float v0, v10

    invoke-direct {v2, v9, v0}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v0, v2

    goto :goto_5

    .line 913135
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 913136
    :goto_6
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 913137
    :goto_7
    new-instance v2, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, Lcom/facebook/greetingcards/model/GreetingCard$Slide;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    return-object v2

    .line 913138
    :cond_8
    const-string v0, ""

    goto :goto_6

    .line 913139
    :cond_9
    const-string v1, ""

    goto :goto_7
.end method
