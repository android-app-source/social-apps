.class public final LX/5Nh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/drawingview/DrawingView$Stroke;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 906540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/drawingview/DrawingView$Stroke;
    .locals 2

    .prologue
    .line 906541
    new-instance v0, Lcom/facebook/drawingview/DrawingView$Stroke;

    invoke-direct {v0, p0}, Lcom/facebook/drawingview/DrawingView$Stroke;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 906542
    invoke-static {p1}, LX/5Nh;->a(Landroid/os/Parcel;)Lcom/facebook/drawingview/DrawingView$Stroke;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 906543
    new-array v0, p1, [Lcom/facebook/drawingview/DrawingView$Stroke;

    move-object v0, v0

    .line 906544
    return-object v0
.end method
