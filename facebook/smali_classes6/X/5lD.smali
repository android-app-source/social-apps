.class public final LX/5lD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 997097
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 997098
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 997099
    :goto_0
    return v1

    .line 997100
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 997101
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 997102
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 997103
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 997104
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 997105
    const-string v3, "keyframes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 997106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 997107
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 997108
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 997109
    invoke-static {p0, p1}, LX/5lC;->b(LX/15w;LX/186;)I

    move-result v2

    .line 997110
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 997111
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 997112
    goto :goto_1

    .line 997113
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 997114
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 997115
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 997116
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 997117
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 997118
    if-eqz v0, :cond_1

    .line 997119
    const-string v1, "keyframes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 997120
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 997121
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_0

    .line 997122
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2}, LX/5lC;->a(LX/15i;ILX/0nX;)V

    .line 997123
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 997124
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 997125
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 997126
    return-void
.end method
