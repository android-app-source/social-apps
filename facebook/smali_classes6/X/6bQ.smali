.class public final LX/6bQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/media/util/model/MediaModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/6bP;


# direct methods
.method public constructor <init>(LX/6bP;)V
    .locals 0

    .prologue
    .line 1113568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113569
    iput-object p1, p0, LX/6bQ;->a:LX/6bP;

    .line 1113570
    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1113571
    check-cast p1, Lcom/facebook/media/util/model/MediaModel;

    check-cast p2, Lcom/facebook/media/util/model/MediaModel;

    .line 1113572
    new-instance v0, Ljava/io/File;

    .line 1113573
    iget-object v1, p1, Lcom/facebook/media/util/model/MediaModel;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1113574
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1113575
    new-instance v1, Ljava/io/File;

    .line 1113576
    iget-object v2, p2, Lcom/facebook/media/util/model/MediaModel;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1113577
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1113578
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1113579
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Long;->compare(JJ)I

    move-result v0

    .line 1113580
    :goto_0
    iget-object v1, p0, LX/6bQ;->a:LX/6bP;

    sget-object v2, LX/6bP;->OLDEST_FIRST:LX/6bP;

    if-ne v1, v2, :cond_3

    :goto_1
    return v0

    .line 1113581
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1113582
    const/4 v0, -0x1

    goto :goto_0

    .line 1113583
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1113584
    const/4 v0, 0x1

    goto :goto_0

    .line 1113585
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1113586
    :cond_3
    neg-int v0, v0

    goto :goto_1
.end method
