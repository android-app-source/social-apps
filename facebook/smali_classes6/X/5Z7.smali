.class public final LX/5Z7;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 944096
    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$SearchGroupThreadQueryModel;

    const v0, 0x25581c50

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "SearchGroupThreadQuery"

    const-string v6, "0812ed6d1f59bd5d50c83a2f1c9352cf"

    const-string v7, "entities_named"

    const-string v8, "10155265581901729"

    const-string v9, "10155259696881729"

    const-string v0, "actor_id"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 944097
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 944108
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 944109
    sparse-switch v0, :sswitch_data_0

    .line 944110
    :goto_0
    return-object p1

    .line 944111
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 944112
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 944113
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 944114
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 944115
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 944116
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 944117
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 944118
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 944119
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 944120
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 944121
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 944122
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 944123
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 944124
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 944125
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 944126
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 944127
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 944128
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 944129
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 944130
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 944131
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 944132
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 944133
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 944134
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 944135
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 944136
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x753cab1d -> :sswitch_13
        -0x7226305f -> :sswitch_7
        -0x5f54881d -> :sswitch_9
        -0x5bcbf522 -> :sswitch_16
        -0x56855c4a -> :sswitch_b
        -0x4c47f2a9 -> :sswitch_a
        -0x4450092f -> :sswitch_6
        -0x39e54905 -> :sswitch_19
        -0x26451294 -> :sswitch_2
        -0x1b236af7 -> :sswitch_18
        -0x179abbec -> :sswitch_17
        -0xf820fe3 -> :sswitch_5
        -0xedb418c -> :sswitch_1
        -0x786d0bb -> :sswitch_c
        -0x3224078 -> :sswitch_d
        -0x132889c -> :sswitch_12
        -0x8d30fe -> :sswitch_f
        0x8da57ae -> :sswitch_15
        0xe0e2e5a -> :sswitch_4
        0x19ec4b2a -> :sswitch_14
        0x1b7d0371 -> :sswitch_0
        0x2f1911b0 -> :sswitch_10
        0x3349e8c0 -> :sswitch_11
        0x5af48aaa -> :sswitch_8
        0x5ba7488b -> :sswitch_e
        0x69308369 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 944098
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 944099
    :goto_1
    return v0

    .line 944100
    :sswitch_0
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "23"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    .line 944101
    :pswitch_0
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 944102
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 944103
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 944104
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 944105
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 944106
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 944107
    :pswitch_6
    const/16 v0, 0xa

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_6
        0x32 -> :sswitch_1
        0x33 -> :sswitch_4
        0x34 -> :sswitch_0
        0x35 -> :sswitch_5
        0x37 -> :sswitch_2
        0x641 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
