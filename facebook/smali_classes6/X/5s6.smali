.class public LX/5s6;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/5s6;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 1012420
    invoke-direct {p0, p1}, LX/5r0;-><init>(I)V

    .line 1012421
    iput p2, p0, LX/5s6;->a:I

    .line 1012422
    iput p3, p0, LX/5s6;->b:I

    .line 1012423
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 4

    .prologue
    .line 1012425
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 1012426
    const-string v1, "width"

    iget v2, p0, LX/5s6;->a:I

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1012427
    const-string v1, "height"

    iget v2, p0, LX/5s6;->b:I

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    float-to-double v2, v2

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1012428
    iget v1, p0, LX/5r0;->c:I

    move v1, v1

    .line 1012429
    const-string v2, "topContentSizeChange"

    invoke-interface {p1, v1, v2, v0}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 1012430
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1012424
    const-string v0, "topContentSizeChange"

    return-object v0
.end method
