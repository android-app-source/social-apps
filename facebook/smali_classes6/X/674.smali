.class public final LX/674;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65J;


# instance fields
.field private final a:LX/670;

.field public final b:Ljava/util/zip/Deflater;

.field private c:Z


# direct methods
.method public constructor <init>(LX/65J;Ljava/util/zip/Deflater;)V
    .locals 1

    .prologue
    .line 1052163
    invoke-static {p1}, LX/67B;->a(LX/65J;)LX/670;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LX/674;-><init>(LX/670;Ljava/util/zip/Deflater;)V

    .line 1052164
    return-void
.end method

.method private constructor <init>(LX/670;Ljava/util/zip/Deflater;)V
    .locals 2

    .prologue
    .line 1052109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052110
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052111
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "inflater == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052112
    :cond_1
    iput-object p1, p0, LX/674;->a:LX/670;

    .line 1052113
    iput-object p2, p0, LX/674;->b:Ljava/util/zip/Deflater;

    .line 1052114
    return-void
.end method

.method public static a(LX/674;Z)V
    .locals 7
    .annotation build Lorg/codehaus/mojo/animal_sniffer/IgnoreJRERequirement;
    .end annotation

    .prologue
    .line 1052148
    iget-object v0, p0, LX/674;->a:LX/670;

    invoke-interface {v0}, LX/670;->c()LX/672;

    move-result-object v1

    .line 1052149
    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/672;->e(I)LX/67F;

    move-result-object v2

    .line 1052150
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/674;->b:Ljava/util/zip/Deflater;

    iget-object v3, v2, LX/67F;->a:[B

    iget v4, v2, LX/67F;->c:I

    iget v5, v2, LX/67F;->c:I

    rsub-int v5, v5, 0x2000

    const/4 v6, 0x2

    .line 1052151
    invoke-virtual {v0, v3, v4, v5, v6}, Ljava/util/zip/Deflater;->deflate([BIII)I

    move-result v0

    .line 1052152
    :goto_1
    if-lez v0, :cond_2

    .line 1052153
    iget v3, v2, LX/67F;->c:I

    add-int/2addr v3, v0

    iput v3, v2, LX/67F;->c:I

    .line 1052154
    iget-wide v2, v1, LX/672;->b:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, v1, LX/672;->b:J

    .line 1052155
    iget-object v0, p0, LX/674;->a:LX/670;

    invoke-interface {v0}, LX/670;->t()LX/670;

    goto :goto_0

    .line 1052156
    :cond_1
    iget-object v0, p0, LX/674;->b:Ljava/util/zip/Deflater;

    iget-object v3, v2, LX/67F;->a:[B

    iget v4, v2, LX/67F;->c:I

    iget v5, v2, LX/67F;->c:I

    rsub-int v5, v5, 0x2000

    .line 1052157
    invoke-virtual {v0, v3, v4, v5}, Ljava/util/zip/Deflater;->deflate([BII)I

    move-result v0

    goto :goto_1

    .line 1052158
    :cond_2
    iget-object v0, p0, LX/674;->b:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->needsInput()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1052159
    iget v0, v2, LX/67F;->b:I

    iget v3, v2, LX/67F;->c:I

    if-ne v0, v3, :cond_3

    .line 1052160
    invoke-virtual {v2}, LX/67F;->a()LX/67F;

    move-result-object v0

    iput-object v0, v1, LX/672;->a:LX/67F;

    .line 1052161
    invoke-static {v2}, LX/67G;->a(LX/67F;)V

    .line 1052162
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1052147
    iget-object v0, p0, LX/674;->a:LX/670;

    invoke-interface {v0}, LX/65J;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final a_(LX/672;J)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 1052133
    iget-wide v0, p1, LX/672;->b:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1052134
    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_1

    .line 1052135
    iget-object v0, p1, LX/672;->a:LX/67F;

    .line 1052136
    iget v1, v0, LX/67F;->c:I

    iget v4, v0, LX/67F;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v1, v4

    .line 1052137
    iget-object v4, p0, LX/674;->b:Ljava/util/zip/Deflater;

    iget-object v5, v0, LX/67F;->a:[B

    iget v6, v0, LX/67F;->b:I

    invoke-virtual {v4, v5, v6, v1}, Ljava/util/zip/Deflater;->setInput([BII)V

    .line 1052138
    const/4 v4, 0x0

    invoke-static {p0, v4}, LX/674;->a(LX/674;Z)V

    .line 1052139
    iget-wide v4, p1, LX/672;->b:J

    int-to-long v6, v1

    sub-long/2addr v4, v6

    iput-wide v4, p1, LX/672;->b:J

    .line 1052140
    iget v4, v0, LX/67F;->b:I

    add-int/2addr v4, v1

    iput v4, v0, LX/67F;->b:I

    .line 1052141
    iget v4, v0, LX/67F;->b:I

    iget v5, v0, LX/67F;->c:I

    if-ne v4, v5, :cond_0

    .line 1052142
    invoke-virtual {v0}, LX/67F;->a()LX/67F;

    move-result-object v4

    iput-object v4, p1, LX/672;->a:LX/67F;

    .line 1052143
    invoke-static {v0}, LX/67G;->a(LX/67F;)V

    .line 1052144
    :cond_0
    int-to-long v0, v1

    sub-long/2addr p2, v0

    .line 1052145
    goto :goto_0

    .line 1052146
    :cond_1
    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 1052119
    iget-boolean v0, p0, LX/674;->c:Z

    if-eqz v0, :cond_1

    .line 1052120
    :cond_0
    :goto_0
    return-void

    .line 1052121
    :cond_1
    const/4 v1, 0x0

    .line 1052122
    :try_start_0
    iget-object v0, p0, LX/674;->b:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->finish()V

    .line 1052123
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/674;->a(LX/674;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 1052124
    :goto_1
    :try_start_1
    iget-object v0, p0, LX/674;->b:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->end()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v1

    .line 1052125
    :cond_2
    :goto_2
    :try_start_2
    iget-object v1, p0, LX/674;->a:LX/670;

    invoke-interface {v1}, LX/65J;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 1052126
    :cond_3
    :goto_3
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/674;->c:Z

    .line 1052127
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/67J;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1052128
    :catch_0
    move-exception v0

    .line 1052129
    if-eqz v1, :cond_2

    move-object v0, v1

    goto :goto_2

    .line 1052130
    :catch_1
    move-exception v1

    .line 1052131
    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_3

    .line 1052132
    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 1052116
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/674;->a(LX/674;Z)V

    .line 1052117
    iget-object v0, p0, LX/674;->a:LX/670;

    invoke-interface {v0}, LX/670;->flush()V

    .line 1052118
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1052115
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DeflaterSink("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/674;->a:LX/670;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
