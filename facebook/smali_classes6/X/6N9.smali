.class public LX/6N9;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rr",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/database/Cursor;

.field public final c:LX/3fx;

.field public d:I

.field public e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1082681
    const-class v0, LX/6N9;

    sput-object v0, LX/6N9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/3fx;LX/2J0;Landroid/database/Cursor;)V
    .locals 1
    .param p3    # Landroid/database/Cursor;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1082676
    invoke-direct {p0}, LX/0Rr;-><init>()V

    .line 1082677
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6N9;->e:Z

    .line 1082678
    iput-object p1, p0, LX/6N9;->c:LX/3fx;

    .line 1082679
    invoke-static {p3}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;

    move-result-object v0

    iput-object v0, p0, LX/6N9;->b:Landroid/database/Cursor;

    .line 1082680
    return-void
.end method

.method public static a(Ljava/lang/String;IILX/3fx;)Lcom/facebook/user/model/UserPhoneNumber;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1082669
    if-eqz p2, :cond_0

    if-eq p2, p1, :cond_0

    .line 1082670
    :goto_0
    return-object v0

    .line 1082671
    :cond_0
    const-string v1, "parseSmsAddress"

    const v2, -0x1f0301a6

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1082672
    :try_start_0
    new-instance v1, LX/7Hx;

    invoke-direct {v1, p3, p0}, LX/7Hx;-><init>(LX/3fx;Ljava/lang/String;)V

    move-object v1, v1

    .line 1082673
    invoke-virtual {v1}, LX/7Hx;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1082674
    invoke-virtual {p3, p0, p1}, LX/3fx;->a(Ljava/lang/String;I)Lcom/facebook/user/model/UserPhoneNumber;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1082675
    :cond_1
    const v1, -0x51c66287

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x26688f0d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static f(LX/6N9;)I
    .locals 5

    .prologue
    .line 1082657
    const/4 v0, 0x0

    .line 1082658
    :goto_0
    const/4 v1, 0x0

    .line 1082659
    iget-object v2, p0, LX/6N9;->b:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1082660
    :cond_0
    :goto_1
    move v1, v1

    .line 1082661
    if-eqz v1, :cond_1

    .line 1082662
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1082663
    :cond_1
    return v0

    .line 1082664
    :cond_2
    iget-object v2, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v3, "deleted"

    invoke-static {v2, v3}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 1082665
    if-eqz v2, :cond_3

    .line 1082666
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v4, "_id"

    invoke-static {v3, v4}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    .line 1082667
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 1082668
    :cond_3
    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1082595
    iget-object v0, p0, LX/6N9;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isBeforeFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1082596
    iget-object v0, p0, LX/6N9;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 1082597
    :cond_0
    invoke-static {p0}, LX/6N9;->f(LX/6N9;)I

    .line 1082598
    iget-object v0, p0, LX/6N9;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1082599
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1082600
    :goto_0
    return-object v0

    .line 1082601
    :cond_1
    iget-object v1, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v2, "contact_id"

    invoke-static {v1, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v1

    .line 1082602
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 1082603
    new-instance v2, LX/0XI;

    invoke-direct {v2}, LX/0XI;-><init>()V

    .line 1082604
    sget-object v3, LX/0XG;->ADDRESS_BOOK:LX/0XG;

    invoke-virtual {v2, v3, v1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 1082605
    :cond_2
    invoke-static {p0}, LX/6N9;->f(LX/6N9;)I

    .line 1082606
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1082607
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v4, "contact_id"

    invoke-static {v3, v4}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v3

    .line 1082608
    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 1082609
    invoke-static {v3, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1082610
    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 1082611
    :goto_1
    move-object v0, v1

    .line 1082612
    goto :goto_0

    .line 1082613
    :cond_3
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v4, "mimetype"

    invoke-static {v3, v4}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1082614
    const-string v4, "vnd.android.cursor.item/name"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1082615
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v4, "is_super_primary"

    invoke-static {v3, v4}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    .line 1082616
    iget-object v4, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v5, "data1"

    invoke-static {v4, v5}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082617
    iget-object v5, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v6, "data2"

    invoke-static {v5, v6}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1082618
    iget-object v6, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v7, "data3"

    invoke-static {v6, v7}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1082619
    new-instance v7, Lcom/facebook/user/model/Name;

    invoke-direct {v7, v5, v6, v4}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082620
    iget-boolean v4, p0, LX/6N9;->e:Z

    if-eqz v4, :cond_c

    .line 1082621
    iput-object v7, v2, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 1082622
    :cond_4
    :goto_2
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1082623
    :cond_5
    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    goto :goto_1

    .line 1082624
    :cond_6
    const-string v4, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1082625
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v4, "data1"

    invoke-static {v3, v4}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1082626
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v4, "data2"

    invoke-static {v3, v4}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v6

    .line 1082627
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1082628
    iget-object v3, v2, LX/0XI;->c:Ljava/util/List;

    move-object v3, v3

    .line 1082629
    if-nez v3, :cond_7

    .line 1082630
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1082631
    iput-object v3, v2, LX/0XI;->c:Ljava/util/List;

    .line 1082632
    :cond_7
    iget-boolean v4, p0, LX/6N9;->e:Z

    if-nez v4, :cond_f

    const/4 v4, 0x1

    .line 1082633
    :goto_3
    new-instance v7, Lcom/facebook/user/model/UserEmailAddress;

    invoke-direct {v7, v5, v6, v4}, Lcom/facebook/user/model/UserEmailAddress;-><init>(Ljava/lang/String;IZ)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1082634
    :cond_8
    goto :goto_2

    .line 1082635
    :cond_9
    const-string v4, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1082636
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v4, "data1"

    invoke-static {v3, v4}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082637
    iget-object v3, p0, LX/6N9;->b:Landroid/database/Cursor;

    const-string v5, "data2"

    invoke-static {v3, v5}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v5

    .line 1082638
    iget-boolean v3, p0, LX/6N9;->e:Z

    if-eqz v3, :cond_10

    new-instance v3, Lcom/facebook/user/model/UserPhoneNumber;

    invoke-direct {v3, v4, v4, v5}, Lcom/facebook/user/model/UserPhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    move-object v4, v3

    .line 1082639
    :goto_4
    if-eqz v4, :cond_b

    .line 1082640
    iget-object v3, v2, LX/0XI;->d:Ljava/util/List;

    move-object v3, v3

    .line 1082641
    if-nez v3, :cond_a

    .line 1082642
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1082643
    iput-object v3, v2, LX/0XI;->d:Ljava/util/List;

    .line 1082644
    :cond_a
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1082645
    :cond_b
    goto :goto_2

    .line 1082646
    :cond_c
    invoke-virtual {v7}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 1082647
    iget-object v4, v2, LX/0XI;->g:Lcom/facebook/user/model/Name;

    move-object v4, v4

    .line 1082648
    if-nez v4, :cond_e

    const/4 v4, 0x0

    .line 1082649
    :goto_5
    if-gt v5, v4, :cond_d

    if-eqz v3, :cond_4

    if-lez v5, :cond_4

    .line 1082650
    :cond_d
    iput-object v7, v2, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 1082651
    goto/16 :goto_2

    .line 1082652
    :cond_e
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    goto :goto_5

    .line 1082653
    :cond_f
    const/4 v4, 0x0

    goto :goto_3

    .line 1082654
    :cond_10
    iget v3, p0, LX/6N9;->d:I

    iget-object v6, p0, LX/6N9;->c:LX/3fx;

    invoke-static {v4, v5, v3, v6}, LX/6N9;->a(Ljava/lang/String;IILX/3fx;)Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v3

    move-object v4, v3

    goto :goto_4
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1082655
    iget-object v0, p0, LX/6N9;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1082656
    return-void
.end method
