.class public LX/5e6;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 966647
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    sput-object v0, LX/5e6;->a:LX/0tf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 966648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 966649
    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-static {p0, v0}, LX/5e6;->a(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/Set;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 966650
    invoke-static {p0, p1}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v0

    .line 966651
    sget-object v2, LX/5e6;->a:LX/0tf;

    monitor-enter v2

    .line 966652
    :try_start_0
    sget-object v3, LX/5e6;->a:LX/0tf;

    invoke-virtual {v3, v0, v1, p1}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 966653
    monitor-exit v2

    .line 966654
    return-wide v0

    .line 966655
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(J)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 966656
    sget-object v1, LX/5e6;->a:LX/0tf;

    monitor-enter v1

    .line 966657
    :try_start_0
    sget-object v0, LX/5e6;->a:LX/0tf;

    invoke-virtual {v0, p0, p1}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    monitor-exit v1

    return-object v0

    .line 966658
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
