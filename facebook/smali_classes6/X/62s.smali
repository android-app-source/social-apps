.class public LX/62s;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private b:J

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:J

.field public h:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;J)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1042698
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1042699
    iput v0, p0, LX/62s;->c:F

    .line 1042700
    iput v0, p0, LX/62s;->d:F

    .line 1042701
    iput v0, p0, LX/62s;->e:F

    .line 1042702
    iput v0, p0, LX/62s;->f:F

    .line 1042703
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/62s;->g:J

    .line 1042704
    iput-object p1, p0, LX/62s;->a:Landroid/graphics/drawable/Drawable;

    .line 1042705
    iput-wide p2, p0, LX/62s;->b:J

    .line 1042706
    iget-wide v0, p0, LX/62s;->b:J

    invoke-virtual {p0, v0, v1}, LX/62s;->setDuration(J)V

    .line 1042707
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5

    .prologue
    const/high16 v4, 0x43b40000    # 360.0f

    .line 1042708
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 1042709
    iget v0, p0, LX/62s;->d:F

    iget-wide v2, p0, LX/62s;->g:J

    long-to-float v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1042710
    iget-wide v0, p0, LX/62s;->b:J

    long-to-float v0, v0

    mul-float/2addr v0, p1

    .line 1042711
    iget v1, p0, LX/62s;->c:F

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    .line 1042712
    iget v1, p0, LX/62s;->d:F

    iget v2, p0, LX/62s;->c:F

    sub-float v2, v0, v2

    add-float/2addr v1, v2

    iput v1, p0, LX/62s;->d:F

    .line 1042713
    :goto_0
    iput v0, p0, LX/62s;->c:F

    .line 1042714
    iget-object v0, p0, LX/62s;->h:Landroid/view/animation/Interpolator;

    iget v1, p0, LX/62s;->d:F

    iget-wide v2, p0, LX/62s;->g:J

    long-to-float v2, v2

    div-float/2addr v1, v2

    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 1042715
    iget v1, p0, LX/62s;->f:F

    mul-float/2addr v0, v1

    rem-float/2addr v0, v4

    iput v0, p0, LX/62s;->e:F

    .line 1042716
    :cond_0
    iget-object v0, p0, LX/62s;->a:Landroid/graphics/drawable/Drawable;

    const v1, 0x461c4000    # 10000.0f

    iget v2, p0, LX/62s;->e:F

    div-float/2addr v2, v4

    add-float/2addr v2, p1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 1042717
    return-void

    .line 1042718
    :cond_1
    iget v1, p0, LX/62s;->d:F

    iget-wide v2, p0, LX/62s;->b:J

    long-to-float v2, v2

    add-float/2addr v2, v0

    iget v3, p0, LX/62s;->c:F

    sub-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, p0, LX/62s;->d:F

    goto :goto_0
.end method
