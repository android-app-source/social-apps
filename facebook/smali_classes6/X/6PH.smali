.class public final LX/6PH;
.super LX/3dG;
.source ""


# instance fields
.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic d:LX/3iV;


# direct methods
.method public varargs constructor <init>(LX/3iV;[Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1086105
    iput-object p1, p0, LX/6PH;->d:LX/3iV;

    iput-object p3, p0, LX/6PH;->b:Ljava/lang/String;

    iput-object p4, p0, LX/6PH;->c:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0, p2}, LX/3dG;-><init>([Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2

    .prologue
    .line 1086106
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/6PH;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1086107
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    iget-object v0, p0, LX/6PH;->d:LX/3iV;

    iget-object v0, v0, LX/3iV;->c:LX/20j;

    iget-object v1, p0, LX/6PH;->c:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, p1, v1}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086108
    const-string v0, "FeedbackGraphQLGenerator.editFeedbackComment"

    return-object v0
.end method
