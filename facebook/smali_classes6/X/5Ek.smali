.class public final LX/5Ek;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 883709
    const/4 v5, 0x0

    .line 883710
    const-wide/16 v6, 0x0

    .line 883711
    const/4 v4, 0x0

    .line 883712
    const/4 v3, 0x0

    .line 883713
    const/4 v2, 0x0

    .line 883714
    const/4 v1, 0x0

    .line 883715
    const/4 v0, 0x0

    .line 883716
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 883717
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 883718
    const/4 v0, 0x0

    .line 883719
    :goto_0
    return v0

    .line 883720
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v5, :cond_5

    .line 883721
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 883722
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 883723
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v10, :cond_0

    if-eqz v0, :cond_0

    .line 883724
    const-string v5, "height"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 883725
    const/4 v0, 0x1

    .line 883726
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v9, v4

    move v4, v0

    goto :goto_1

    .line 883727
    :cond_1
    const-string v5, "scale"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 883728
    const/4 v0, 0x1

    .line 883729
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 883730
    :cond_2
    const-string v5, "uri"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 883731
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v8, v0

    goto :goto_1

    .line 883732
    :cond_3
    const-string v5, "width"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 883733
    const/4 v0, 0x1

    .line 883734
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v6, v0

    move v7, v5

    goto :goto_1

    .line 883735
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 883736
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 883737
    if-eqz v4, :cond_6

    .line 883738
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v9, v4}, LX/186;->a(III)V

    .line 883739
    :cond_6
    if-eqz v1, :cond_7

    .line 883740
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 883741
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 883742
    if-eqz v6, :cond_8

    .line 883743
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v7, v1}, LX/186;->a(III)V

    .line 883744
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_9
    move v8, v4

    move v9, v5

    move v4, v2

    move-wide v11, v6

    move v6, v0

    move v7, v3

    move-wide v2, v11

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 883745
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 883746
    invoke-virtual {p0, p1, v3, v3}, LX/15i;->a(III)I

    move-result v0

    .line 883747
    if-eqz v0, :cond_0

    .line 883748
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883749
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 883750
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 883751
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 883752
    const-string v2, "scale"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883753
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 883754
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883755
    if-eqz v0, :cond_2

    .line 883756
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883757
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883758
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 883759
    if-eqz v0, :cond_3

    .line 883760
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883761
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 883762
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 883763
    return-void
.end method
