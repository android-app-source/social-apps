.class public final enum LX/6K9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6K9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6K9;

.field public static final enum PREPARED:LX/6K9;

.field public static final enum PREPARE_STARTED:LX/6K9;

.field public static final enum RECORDING:LX/6K9;

.field public static final enum RECORDING_STARTED:LX/6K9;

.field public static final enum STOPPED:LX/6K9;

.field public static final enum STOP_STARTED:LX/6K9;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1076460
    new-instance v0, LX/6K9;

    const-string v1, "PREPARE_STARTED"

    invoke-direct {v0, v1, v3}, LX/6K9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6K9;->PREPARE_STARTED:LX/6K9;

    .line 1076461
    new-instance v0, LX/6K9;

    const-string v1, "PREPARED"

    invoke-direct {v0, v1, v4}, LX/6K9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6K9;->PREPARED:LX/6K9;

    .line 1076462
    new-instance v0, LX/6K9;

    const-string v1, "RECORDING_STARTED"

    invoke-direct {v0, v1, v5}, LX/6K9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6K9;->RECORDING_STARTED:LX/6K9;

    .line 1076463
    new-instance v0, LX/6K9;

    const-string v1, "RECORDING"

    invoke-direct {v0, v1, v6}, LX/6K9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6K9;->RECORDING:LX/6K9;

    .line 1076464
    new-instance v0, LX/6K9;

    const-string v1, "STOP_STARTED"

    invoke-direct {v0, v1, v7}, LX/6K9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6K9;->STOP_STARTED:LX/6K9;

    .line 1076465
    new-instance v0, LX/6K9;

    const-string v1, "STOPPED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6K9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6K9;->STOPPED:LX/6K9;

    .line 1076466
    const/4 v0, 0x6

    new-array v0, v0, [LX/6K9;

    sget-object v1, LX/6K9;->PREPARE_STARTED:LX/6K9;

    aput-object v1, v0, v3

    sget-object v1, LX/6K9;->PREPARED:LX/6K9;

    aput-object v1, v0, v4

    sget-object v1, LX/6K9;->RECORDING_STARTED:LX/6K9;

    aput-object v1, v0, v5

    sget-object v1, LX/6K9;->RECORDING:LX/6K9;

    aput-object v1, v0, v6

    sget-object v1, LX/6K9;->STOP_STARTED:LX/6K9;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6K9;->STOPPED:LX/6K9;

    aput-object v2, v0, v1

    sput-object v0, LX/6K9;->$VALUES:[LX/6K9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1076467
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6K9;
    .locals 1

    .prologue
    .line 1076468
    const-class v0, LX/6K9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6K9;

    return-object v0
.end method

.method public static values()[LX/6K9;
    .locals 1

    .prologue
    .line 1076469
    sget-object v0, LX/6K9;->$VALUES:[LX/6K9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6K9;

    return-object v0
.end method
