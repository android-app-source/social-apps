.class public LX/5iG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            "LX/1aX;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1aX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Landroid/graphics/drawable/Drawable$Callback;

.field public f:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(LX/1aX;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # LX/1aX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 982496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982497
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/5iG;->c:Ljava/util/LinkedHashMap;

    .line 982498
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/5iG;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 982499
    iput-object p1, p0, LX/5iG;->d:LX/1aX;

    .line 982500
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/5iG;->a:Ljava/lang/String;

    .line 982501
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/5iG;->b:Ljava/lang/String;

    .line 982502
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/StickerParams;)LX/1aX;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 982503
    iget-object v0, p0, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    return-object v0
.end method

.method public final a(Landroid/graphics/drawable/Drawable$Callback;)V
    .locals 3

    .prologue
    .line 982504
    iput-object p1, p0, LX/5iG;->e:Landroid/graphics/drawable/Drawable$Callback;

    .line 982505
    iget-object v0, p0, LX/5iG;->d:LX/1aX;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5iG;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 982506
    iget-object v0, p0, LX/5iG;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 982507
    :cond_0
    iget-object v0, p0, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 982508
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 982509
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    goto :goto_0

    .line 982510
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/StickerParams;LX/1aX;)V
    .locals 3

    .prologue
    .line 982511
    iget-object v0, p0, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 982512
    invoke-virtual {p2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 982513
    invoke-virtual {p2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, LX/5iG;->e:Landroid/graphics/drawable/Drawable$Callback;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 982514
    :cond_0
    iget-object v0, p0, LX/5iG;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 982515
    invoke-virtual {p2}, LX/1aX;->d()V

    .line 982516
    :cond_1
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 982517
    iget-object v0, p0, LX/5iG;->d:LX/1aX;

    if-eqz v0, :cond_0

    .line 982518
    iget-object v0, p0, LX/5iG;->d:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 982519
    :goto_0
    return v0

    .line 982520
    :cond_0
    iget-object v0, p0, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1aX;

    .line 982521
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p1, :cond_1

    move v0, v1

    .line 982522
    goto :goto_0

    .line 982523
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 982524
    iget-object v0, p0, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/StickerParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 982525
    iget-object v0, p0, LX/5iG;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 982526
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Swipeable item name : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 982527
    iget-object v1, p0, LX/5iG;->a:Ljava/lang/String;

    move-object v1, v1

    .line 982528
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFrame : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/5iG;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
