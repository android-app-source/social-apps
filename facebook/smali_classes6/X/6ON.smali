.class public final enum LX/6ON;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6ON;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6ON;

.field public static final enum ADD:LX/6ON;

.field public static final enum DELETE:LX/6ON;

.field public static final enum MODIFY:LX/6ON;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1085078
    new-instance v0, LX/6ON;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v2}, LX/6ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ON;->ADD:LX/6ON;

    .line 1085079
    new-instance v0, LX/6ON;

    const-string v1, "MODIFY"

    invoke-direct {v0, v1, v3}, LX/6ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ON;->MODIFY:LX/6ON;

    .line 1085080
    new-instance v0, LX/6ON;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v4}, LX/6ON;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ON;->DELETE:LX/6ON;

    .line 1085081
    const/4 v0, 0x3

    new-array v0, v0, [LX/6ON;

    sget-object v1, LX/6ON;->ADD:LX/6ON;

    aput-object v1, v0, v2

    sget-object v1, LX/6ON;->MODIFY:LX/6ON;

    aput-object v1, v0, v3

    sget-object v1, LX/6ON;->DELETE:LX/6ON;

    aput-object v1, v0, v4

    sput-object v0, LX/6ON;->$VALUES:[LX/6ON;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1085082
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6ON;
    .locals 1

    .prologue
    .line 1085083
    const-class v0, LX/6ON;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ON;

    return-object v0
.end method

.method public static values()[LX/6ON;
    .locals 1

    .prologue
    .line 1085084
    sget-object v0, LX/6ON;->$VALUES:[LX/6ON;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6ON;

    return-object v0
.end method
