.class public final LX/5TD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 921332
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 921333
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 921334
    :goto_0
    return v1

    .line 921335
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_7

    .line 921336
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 921337
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 921338
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 921339
    const-string v10, "commerce_inventory"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 921340
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v8, v0

    move v0, v2

    goto :goto_1

    .line 921341
    :cond_1
    const-string v10, "current_price"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 921342
    invoke-static {p0, p1}, LX/5TF;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 921343
    :cond_2
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 921344
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 921345
    :cond_3
    const-string v10, "image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 921346
    const/4 v9, 0x0

    .line 921347
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v10, :cond_d

    .line 921348
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 921349
    :goto_2
    move v5, v9

    .line 921350
    goto :goto_1

    .line 921351
    :cond_4
    const-string v10, "item_price"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 921352
    invoke-static {p0, p1}, LX/5TF;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 921353
    :cond_5
    const-string v10, "variant_values"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 921354
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 921355
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 921356
    :cond_7
    const/4 v9, 0x6

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 921357
    if-eqz v0, :cond_8

    .line 921358
    invoke-virtual {p1, v1, v8, v1}, LX/186;->a(III)V

    .line 921359
    :cond_8
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 921360
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 921361
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 921362
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 921363
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 921364
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1

    .line 921365
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 921366
    :cond_b
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_c

    .line 921367
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 921368
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 921369
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_b

    if-eqz v10, :cond_b

    .line 921370
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 921371
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 921372
    :cond_c
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 921373
    invoke-virtual {p1, v9, v5}, LX/186;->b(II)V

    .line 921374
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto/16 :goto_2

    :cond_d
    move v5, v9

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    const/4 v0, 0x0

    .line 921375
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 921376
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 921377
    if-eqz v0, :cond_0

    .line 921378
    const-string v1, "commerce_inventory"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921379
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 921380
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 921381
    if-eqz v0, :cond_1

    .line 921382
    const-string v1, "current_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921383
    invoke-static {p0, v0, p2}, LX/5TF;->a(LX/15i;ILX/0nX;)V

    .line 921384
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 921385
    if-eqz v0, :cond_2

    .line 921386
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921387
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 921388
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 921389
    if-eqz v0, :cond_4

    .line 921390
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921391
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 921392
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 921393
    if-eqz v1, :cond_3

    .line 921394
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921395
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 921396
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 921397
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 921398
    if-eqz v0, :cond_5

    .line 921399
    const-string v1, "item_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921400
    invoke-static {p0, v0, p2}, LX/5TF;->a(LX/15i;ILX/0nX;)V

    .line 921401
    :cond_5
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 921402
    if-eqz v0, :cond_6

    .line 921403
    const-string v0, "variant_values"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921404
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 921405
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 921406
    return-void
.end method
