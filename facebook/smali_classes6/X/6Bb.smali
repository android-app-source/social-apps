.class public LX/6Bb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# instance fields
.field private final a:LX/6Bf;

.field private final b:LX/0W3;

.field private final c:LX/6Bj;


# direct methods
.method public constructor <init>(LX/6Bf;LX/0W3;LX/6Bj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1062834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1062835
    iput-object p1, p0, LX/6Bb;->a:LX/6Bf;

    .line 1062836
    iput-object p2, p0, LX/6Bb;->b:LX/0W3;

    .line 1062837
    iput-object p3, p0, LX/6Bb;->c:LX/6Bj;

    .line 1062838
    return-void
.end method

.method public static b(LX/0QB;)LX/6Bb;
    .locals 7

    .prologue
    .line 1062839
    new-instance v3, LX/6Bb;

    invoke-static {p0}, LX/6Bf;->b(LX/0QB;)LX/6Bf;

    move-result-object v0

    check-cast v0, LX/6Bf;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    .line 1062840
    new-instance v6, LX/6Bj;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0nI;->b(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v5

    check-cast v5, Landroid/net/ConnectivityManager;

    invoke-direct {v6, v2, v4, v5}, LX/6Bj;-><init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/net/ConnectivityManager;)V

    .line 1062841
    move-object v2, v6

    .line 1062842
    check-cast v2, LX/6Bj;

    invoke-direct {v3, v0, v1, v2}, LX/6Bb;-><init>(LX/6Bf;LX/0W3;LX/6Bj;)V

    .line 1062843
    return-object v3
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 13

    .prologue
    .line 1062844
    iget-object v0, p0, LX/6Bb;->a:LX/6Bf;

    invoke-virtual {v0}, LX/6Bf;->b()V

    .line 1062845
    iget-object v0, p0, LX/6Bb;->a:LX/6Bf;

    iget-object v1, p0, LX/6Bb;->b:LX/0W3;

    sget-wide v2, LX/0X5;->q:J

    const-wide/16 v4, 0x7530

    invoke-interface {v1, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v2

    .line 1062846
    iget-object v1, v0, LX/6Bf;->o:LX/6Be;

    if-eqz v1, :cond_0

    .line 1062847
    iget-object v1, v0, LX/6Bf;->n:Ljava/util/HashSet;

    iget-object v4, v0, LX/6Bf;->o:LX/6Be;

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1062848
    :cond_0
    new-instance v1, LX/6Be;

    iget-object v4, v0, LX/6Bf;->b:LX/0SG;

    invoke-direct {v1, v4, v2, v3}, LX/6Be;-><init>(LX/0SG;J)V

    iput-object v1, v0, LX/6Bf;->o:LX/6Be;

    .line 1062849
    iget-object v1, v0, LX/6Bf;->o:LX/6Be;

    invoke-virtual {v0, v1}, LX/6Bf;->a(LX/6BZ;)V

    .line 1062850
    iget-object v0, p0, LX/6Bb;->a:LX/6Bf;

    iget-object v1, p0, LX/6Bb;->b:LX/0W3;

    sget-wide v2, LX/0X5;->o:J

    const-wide/32 v4, 0x200000

    invoke-interface {v1, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v2

    .line 1062851
    iget-object v1, v0, LX/6Bf;->p:LX/6Bc;

    if-eqz v1, :cond_1

    .line 1062852
    iget-object v1, v0, LX/6Bf;->n:Ljava/util/HashSet;

    iget-object v4, v0, LX/6Bf;->p:LX/6Bc;

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1062853
    :cond_1
    new-instance v1, LX/6Bc;

    invoke-direct {v1, v2, v3}, LX/6Bc;-><init>(J)V

    iput-object v1, v0, LX/6Bf;->p:LX/6Bc;

    .line 1062854
    iget-object v1, v0, LX/6Bf;->p:LX/6Bc;

    invoke-virtual {v0, v1}, LX/6Bf;->a(LX/6BZ;)V

    .line 1062855
    iget-object v0, p0, LX/6Bb;->a:LX/6Bf;

    iget-object v1, p0, LX/6Bb;->b:LX/0W3;

    sget-wide v2, LX/0X5;->p:J

    const-wide/32 v4, 0x80000

    invoke-interface {v1, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v2

    .line 1062856
    iget-object v1, v0, LX/6Bf;->q:LX/6Bc;

    if-eqz v1, :cond_2

    .line 1062857
    iget-object v1, v0, LX/6Bf;->n:Ljava/util/HashSet;

    iget-object v4, v0, LX/6Bf;->q:LX/6Bc;

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1062858
    :cond_2
    new-instance v1, LX/6Bc;

    invoke-direct {v1, v2, v3}, LX/6Bc;-><init>(J)V

    iput-object v1, v0, LX/6Bf;->q:LX/6Bc;

    .line 1062859
    iget-object v1, v0, LX/6Bf;->q:LX/6Bc;

    invoke-virtual {v0, v1}, LX/6Bf;->a(LX/6BZ;)V

    .line 1062860
    iget-object v0, p0, LX/6Bb;->a:LX/6Bf;

    iget-object v1, p0, LX/6Bb;->b:LX/0W3;

    sget-wide v2, LX/0X5;->n:J

    const/4 v4, 0x2

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v1

    invoke-virtual {v0, v1}, LX/6Bf;->a(I)V

    .line 1062861
    iget-object v0, p0, LX/6Bb;->b:LX/0W3;

    sget-wide v2, LX/0X5;->r:J

    const-wide/32 v4, 0x2255100

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    .line 1062862
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_3

    .line 1062863
    iget-object v2, p0, LX/6Bb;->a:LX/6Bf;

    iget-object v3, p0, LX/6Bb;->c:LX/6Bj;

    .line 1062864
    new-instance v6, LX/6Bi;

    iget-object v7, v3, LX/6Bj;->b:LX/0SG;

    iget-object v8, v3, LX/6Bj;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v9, v3, LX/6Bj;->d:Landroid/net/ConnectivityManager;

    const/4 v12, 0x0

    move-wide v10, v0

    invoke-direct/range {v6 .. v12}, LX/6Bi;-><init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/net/ConnectivityManager;JB)V

    move-object v0, v6

    .line 1062865
    invoke-virtual {v2, v0}, LX/6Bf;->a(LX/6BZ;)V

    .line 1062866
    :cond_3
    if-eqz p1, :cond_4

    .line 1062867
    iget-object v0, p0, LX/6Bb;->a:LX/6Bf;

    new-instance v1, LX/6Ba;

    invoke-direct {v1, p1}, LX/6Ba;-><init>(LX/2Jy;)V

    invoke-virtual {v0, v1}, LX/6Bf;->a(LX/6BZ;)V

    .line 1062868
    :cond_4
    iget-object v0, p0, LX/6Bb;->a:LX/6Bf;

    invoke-virtual {v0}, LX/6Bf;->a()Z

    move-result v0

    return v0
.end method
