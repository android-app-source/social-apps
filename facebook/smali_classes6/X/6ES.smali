.class public final LX/6ES;
.super LX/4mf;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;Landroid/content/Context;I)V
    .locals 5

    .prologue
    .line 1066151
    iput-object p1, p0, LX/6ES;->b:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    .line 1066152
    invoke-direct {p0, p1, p2, p3}, LX/4mf;-><init>(Lcom/facebook/ui/dialogs/FbDialogFragment;Landroid/content/Context;I)V

    .line 1066153
    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 1066154
    invoke-virtual {p0}, LX/6ES;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0311cc

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1066155
    const v0, 0x7f0d29c7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1066156
    invoke-virtual {p0}, LX/6ES;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081d68

    new-array v4, p3, [Ljava/lang/Object;

    iget-object p1, p0, LX/6ES;->b:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    iget-object p1, p1, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->u:Ljava/lang/String;

    aput-object p1, v4, p2

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1066157
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1066158
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, p3}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 1066159
    iget-object v4, p0, LX/6ES;->b:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    iget-object v4, v4, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->u:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 p1, 0x12

    invoke-virtual {v3, v2, p2, v4, p1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1066160
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1066161
    iget-object v0, p0, LX/6ES;->b:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    iget-object v2, p0, LX/6ES;->b:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->a(Landroid/view/View;Ljava/util/ArrayList;)V

    .line 1066162
    const v0, 0x7f0d29ca

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1066163
    new-instance v2, LX/6EQ;

    invoke-direct {v2, p0}, LX/6EQ;-><init>(LX/6ES;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1066164
    const v0, 0x7f0d29c9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1066165
    new-instance v2, LX/6ER;

    invoke-direct {v2, p0}, LX/6ER;-><init>(LX/6ES;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1066166
    iget-object v2, p0, LX/6ES;->b:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    const v0, 0x7f0d29c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, v2, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1066167
    iget-object v2, p0, LX/6ES;->b:Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;

    const v0, 0x7f0d29c6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->s:Landroid/widget/LinearLayout;

    .line 1066168
    invoke-virtual {p0, v1}, LX/6ES;->setContentView(Landroid/view/View;)V

    .line 1066169
    return-void
.end method
