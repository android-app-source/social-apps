.class public final LX/5NW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cj",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;


# direct methods
.method public constructor <init>(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)V
    .locals 0

    .prologue
    .line 906388
    iput-object p1, p0, LX/5NW;->a:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 906383
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 906384
    iget-object v1, p0, LX/5NW;->a:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    iget-object v0, p0, LX/5NW;->a:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    invoke-static {v0}, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->a$redex0(Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/5NW;->a:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    iget-object v0, v0, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->d:Landroid/graphics/drawable/Drawable;

    .line 906385
    :goto_0
    invoke-virtual {v1, v0}, LX/1bp;->a(Landroid/graphics/drawable/Drawable;)V

    .line 906386
    :cond_0
    return-void

    .line 906387
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 906380
    iget-object v0, p0, LX/5NW;->a:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    iget-object v1, p0, LX/5NW;->a:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    iget-object v1, v1, Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;->d:Landroid/graphics/drawable/Drawable;

    .line 906381
    invoke-virtual {v0, v1}, LX/1bp;->a(Landroid/graphics/drawable/Drawable;)V

    .line 906382
    return-void
.end method

.method public final c(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 906376
    return-void
.end method

.method public final d(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 906377
    iget-object v0, p0, LX/5NW;->a:Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    const/4 v1, 0x0

    .line 906378
    invoke-virtual {v0, v1}, LX/1bp;->a(Landroid/graphics/drawable/Drawable;)V

    .line 906379
    return-void
.end method
