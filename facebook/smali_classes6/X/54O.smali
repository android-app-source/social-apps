.class public final LX/54O;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static d:LX/54O;


# instance fields
.field public final a:J

.field public final b:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "LX/54Q;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 827975
    new-instance v0, LX/54O;

    const-wide/16 v2, 0x3c

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, v2, v3, v1}, LX/54O;-><init>(JLjava/util/concurrent/TimeUnit;)V

    sput-object v0, LX/54O;->d:LX/54O;

    return-void
.end method

.method private constructor <init>(JLjava/util/concurrent/TimeUnit;)V
    .locals 7

    .prologue
    .line 827976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827977
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, LX/54O;->a:J

    .line 827978
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LX/54O;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 827979
    const/4 v0, 0x1

    sget-object v1, LX/54R;->b:LX/53o;

    invoke-static {v0, v1}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, LX/54O;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 827980
    iget-object v0, p0, LX/54O;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lrx/schedulers/CachedThreadScheduler$CachedWorkerPool$1;

    invoke-direct {v1, p0}, Lrx/schedulers/CachedThreadScheduler$CachedWorkerPool$1;-><init>(LX/54O;)V

    iget-wide v2, p0, LX/54O;->a:J

    iget-wide v4, p0, LX/54O;->a:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 827981
    return-void
.end method

.method public static d()J
    .locals 2

    .prologue
    .line 827982
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method
