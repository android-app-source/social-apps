.class public LX/6eE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I

.field private b:I


# direct methods
.method private constructor <init>(II)V
    .locals 0

    .prologue
    .line 1117676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117677
    iput p1, p0, LX/6eE;->a:I

    .line 1117678
    iput p2, p0, LX/6eE;->b:I

    .line 1117679
    return-void
.end method

.method public static a()LX/6eE;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1117680
    new-instance v0, LX/6eE;

    invoke-direct {v0, v1, v1}, LX/6eE;-><init>(II)V

    return-object v0
.end method

.method public static a(Landroid/support/v4/app/Fragment;)LX/6eE;
    .locals 5

    .prologue
    .line 1117681
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Service;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1117682
    :goto_0
    if-eqz v0, :cond_1

    .line 1117683
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1117684
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1117685
    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    const v3, 0x1020002

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1117686
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    const v4, 0x7f0b016c

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    .line 1117687
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result p0

    sub-int/2addr v4, p0

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    sub-int v2, v4, v2

    const v4, 0x7f0b0190

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v2, v4

    .line 1117688
    new-instance v4, LX/472;

    invoke-direct {v4, v3, v2}, LX/472;-><init>(II)V

    move-object v1, v4

    .line 1117689
    new-instance v0, LX/6eE;

    iget v2, v1, LX/472;->a:I

    iget v1, v1, LX/472;->b:I

    invoke-direct {v0, v2, v1}, LX/6eE;-><init>(II)V

    .line 1117690
    :goto_1
    return-object v0

    .line 1117691
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1117692
    :cond_1
    invoke-static {}, LX/6eE;->a()LX/6eE;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1117693
    const-string v0, "arg_dialog_width"

    iget v1, p0, LX/6eE;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1117694
    const-string v0, "arg_dialog_height"

    iget v1, p0, LX/6eE;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1117695
    return-void
.end method
