.class public final enum LX/6BV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6BV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6BV;

.field public static final enum CAN_BE_EXTERNAL:LX/6BV;

.field public static final enum MUST_BE_CUSTOM_LOCATION:LX/6BV;

.field public static final enum MUST_BE_INTERNAL:LX/6BV;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1062700
    new-instance v0, LX/6BV;

    const-string v1, "CAN_BE_EXTERNAL"

    invoke-direct {v0, v1, v2}, LX/6BV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6BV;->CAN_BE_EXTERNAL:LX/6BV;

    .line 1062701
    new-instance v0, LX/6BV;

    const-string v1, "MUST_BE_INTERNAL"

    invoke-direct {v0, v1, v3}, LX/6BV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6BV;->MUST_BE_INTERNAL:LX/6BV;

    .line 1062702
    new-instance v0, LX/6BV;

    const-string v1, "MUST_BE_CUSTOM_LOCATION"

    invoke-direct {v0, v1, v4}, LX/6BV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6BV;->MUST_BE_CUSTOM_LOCATION:LX/6BV;

    .line 1062703
    const/4 v0, 0x3

    new-array v0, v0, [LX/6BV;

    sget-object v1, LX/6BV;->CAN_BE_EXTERNAL:LX/6BV;

    aput-object v1, v0, v2

    sget-object v1, LX/6BV;->MUST_BE_INTERNAL:LX/6BV;

    aput-object v1, v0, v3

    sget-object v1, LX/6BV;->MUST_BE_CUSTOM_LOCATION:LX/6BV;

    aput-object v1, v0, v4

    sput-object v0, LX/6BV;->$VALUES:[LX/6BV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1062699
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6BV;
    .locals 1

    .prologue
    .line 1062697
    const-class v0, LX/6BV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6BV;

    return-object v0
.end method

.method public static values()[LX/6BV;
    .locals 1

    .prologue
    .line 1062698
    sget-object v0, LX/6BV;->$VALUES:[LX/6BV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6BV;

    return-object v0
.end method
