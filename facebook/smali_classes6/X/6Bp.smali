.class public LX/6Bp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/assetdownload/AssetDownloadConfiguration;

.field public final b:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/facebook/assetdownload/AssetDownloadConfiguration;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 1063187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063188
    const-string v0, "assetDownloadConfiguration must not be null"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1063189
    invoke-virtual {p1}, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->a()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "assetDownloadConfiguration.getSource() must not return null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1063190
    const-string v0, "destination must not be null"

    invoke-static {p2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1063191
    iput-object p1, p0, LX/6Bp;->a:Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    .line 1063192
    iput-object p2, p0, LX/6Bp;->b:Ljava/io/File;

    .line 1063193
    return-void
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1063194
    iget-object v0, p0, LX/6Bp;->a:Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    .line 1063195
    iget-object p0, v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mAnalyticsTag:Ljava/lang/String;

    move-object v0, p0

    .line 1063196
    return-object v0
.end method
