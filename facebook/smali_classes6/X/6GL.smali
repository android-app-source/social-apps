.class public final LX/6GL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1070261
    iput-object p1, p0, LX/6GL;->b:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iput-object p2, p0, LX/6GL;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1070262
    const/4 v1, 0x0

    .line 1070263
    :try_start_0
    invoke-static {}, LX/6G6;->a()LX/6G6;

    move-result-object v0

    .line 1070264
    iget-object v2, p0, LX/6GL;->b:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iget-object v2, v2, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->B:LX/0W3;

    sget-wide v4, LX/0X5;->aY:J

    const/4 v3, 0x0

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JZ)Z

    move-result v2

    .line 1070265
    if-eqz v2, :cond_1

    .line 1070266
    iget-object v2, v0, LX/6G6;->i:Ljava/io/File;

    move-object v0, v2

    .line 1070267
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bug_report_image_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/6GL;->b:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iget-object v3, v3, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->l:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;

    move-result-object v1

    .line 1070268
    iget-object v0, p0, LX/6GL;->b:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v2, p0, LX/6GL;->a:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0, v1}, LX/6G3;->a(Ljava/io/InputStream;LX/6FR;)V

    .line 1070269
    iget-object v0, v1, LX/6FR;->b:Landroid/net/Uri;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1070270
    if-eqz v1, :cond_0

    .line 1070271
    iget-object v2, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v2

    .line 1070272
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_0
    return-object v0

    .line 1070273
    :cond_1
    :try_start_1
    iget-object v2, v0, LX/6G6;->b:Ljava/io/File;

    move-object v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1070274
    goto :goto_0

    .line 1070275
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 1070276
    iget-object v2, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v2

    .line 1070277
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_2
    throw v0
.end method
