.class public final LX/5Ji;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5Ji;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5Jg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5Jk;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 897621
    const/4 v0, 0x0

    sput-object v0, LX/5Ji;->a:LX/5Ji;

    .line 897622
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5Ji;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 897618
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 897619
    new-instance v0, LX/5Jk;

    invoke-direct {v0}, LX/5Jk;-><init>()V

    iput-object v0, p0, LX/5Ji;->c:LX/5Jk;

    .line 897620
    return-void
.end method

.method public static declared-synchronized q()LX/5Ji;
    .locals 2

    .prologue
    .line 897614
    const-class v1, LX/5Ji;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5Ji;->a:LX/5Ji;

    if-nez v0, :cond_0

    .line 897615
    new-instance v0, LX/5Ji;

    invoke-direct {v0}, LX/5Ji;-><init>()V

    sput-object v0, LX/5Ji;->a:LX/5Ji;

    .line 897616
    :cond_0
    sget-object v0, LX/5Ji;->a:LX/5Ji;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 897617
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 897612
    invoke-static {}, LX/1dS;->b()V

    .line 897613
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/16 v0, 0x1e

    const v1, 0x23d3b7ed

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 897589
    check-cast p6, LX/5Jh;

    .line 897590
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 897591
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 897592
    iget-object v3, p6, LX/5Jh;->b:LX/1dV;

    move v0, p3

    move v1, p4

    move-object v2, p5

    const/4 p0, 0x0

    .line 897593
    invoke-static {}, LX/5Jk;->a()LX/1no;

    move-result-object p1

    .line 897594
    invoke-static {p0, p0}, LX/1mh;->a(II)I

    move-result p0

    invoke-virtual {v3, p0, v1, p1}, LX/1dV;->a(IILX/1no;)V

    .line 897595
    iget p0, p1, LX/1no;->a:I

    .line 897596
    iget p2, p1, LX/1no;->b:I

    .line 897597
    invoke-static {p1}, LX/5Jk;->a(LX/1no;)V

    .line 897598
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 897599
    iput-object p1, v4, LX/1np;->a:Ljava/lang/Object;

    .line 897600
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 897601
    iput-object p1, v5, LX/1np;->a:Ljava/lang/Object;

    .line 897602
    invoke-static {v0}, LX/1mh;->a(I)I

    move-result p1

    if-nez p1, :cond_0

    :goto_0
    iput p0, v2, LX/1no;->a:I

    .line 897603
    iput p2, v2, LX/1no;->b:I

    .line 897604
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 897605
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p6, LX/5Jh;->c:Ljava/lang/Integer;

    .line 897606
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 897607
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 897608
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p6, LX/5Jh;->d:Ljava/lang/Integer;

    .line 897609
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 897610
    const/16 v0, 0x1f

    const v1, 0x7be06414

    invoke-static {v7, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 897611
    :cond_0
    invoke-static {v0}, LX/1mh;->b(I)I

    move-result p0

    goto :goto_0
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 7

    .prologue
    .line 897565
    check-cast p3, LX/5Jh;

    .line 897566
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 897567
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 897568
    iget-object v1, p3, LX/5Jh;->b:LX/1dV;

    iget-object v2, p3, LX/5Jh;->c:Ljava/lang/Integer;

    iget-object v3, p3, LX/5Jh;->d:Ljava/lang/Integer;

    move-object v0, p2

    const/4 p0, 0x0

    .line 897569
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 897570
    iput-object v2, v4, LX/1np;->a:Ljava/lang/Object;

    .line 897571
    iput-object v3, v5, LX/1np;->a:Ljava/lang/Object;

    .line 897572
    :goto_0
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 897573
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p3, LX/5Jh;->e:Ljava/lang/Integer;

    .line 897574
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 897575
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 897576
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p3, LX/5Jh;->f:Ljava/lang/Integer;

    .line 897577
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 897578
    return-void

    .line 897579
    :cond_0
    invoke-static {}, LX/5Jk;->a()LX/1no;

    move-result-object v6

    .line 897580
    invoke-static {p0, p0}, LX/1mh;->a(II)I

    move-result p0

    invoke-virtual {v0}, LX/1Dg;->d()I

    move-result p1

    const/high16 p2, 0x40000000    # 2.0f

    invoke-static {p1, p2}, LX/1mh;->a(II)I

    move-result p1

    invoke-virtual {v1, p0, p1, v6}, LX/1dV;->a(IILX/1no;)V

    .line 897581
    iget p0, v6, LX/1no;->a:I

    .line 897582
    iget p1, v6, LX/1no;->b:I

    .line 897583
    invoke-static {v6}, LX/5Jk;->a(LX/1no;)V

    .line 897584
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 897585
    iput-object v6, v4, LX/1np;->a:Ljava/lang/Object;

    .line 897586
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 897587
    iput-object v6, v5, LX/1np;->a:Ljava/lang/Object;

    .line 897588
    goto :goto_0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 897563
    new-instance v0, LX/5Jj;

    invoke-direct {v0, p1}, LX/5Jj;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 897564
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 3

    .prologue
    .line 897623
    check-cast p2, LX/5Jh;

    .line 897624
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 897625
    iget-object v0, p2, LX/5Jh;->a:LX/1X1;

    .line 897626
    invoke-static {p1, v0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v2

    const/4 p0, 0x1

    .line 897627
    iput-boolean p0, v2, LX/1me;->c:Z

    .line 897628
    move-object v2, v2

    .line 897629
    invoke-virtual {v2}, LX/1me;->b()LX/1dV;

    move-result-object v2

    .line 897630
    iput-object v2, v1, LX/1np;->a:Ljava/lang/Object;

    .line 897631
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 897632
    check-cast v0, LX/1dV;

    iput-object v0, p2, LX/5Jh;->b:LX/1dV;

    .line 897633
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 897634
    return-void
.end method

.method public final c(LX/1De;LX/1X1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 897545
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 897546
    const/4 v2, 0x0

    .line 897547
    sget-object v0, LX/03r;->HorizontalScroll:[I

    invoke-virtual {p1, v0, v2}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 897548
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    .line 897549
    invoke-virtual {v4, v3}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v0

    .line 897550
    const/16 p0, 0x0

    if-ne v0, p0, :cond_0

    .line 897551
    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 897552
    iput-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 897553
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 897554
    goto :goto_1

    .line 897555
    :cond_2
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 897556
    check-cast p2, LX/5Jh;

    .line 897557
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 897558
    if-eqz v0, :cond_3

    .line 897559
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 897560
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/5Jh;->g:Z

    .line 897561
    :cond_3
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 897562
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 897544
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 897543
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 4

    .prologue
    .line 897536
    check-cast p3, LX/5Jh;

    .line 897537
    check-cast p2, LX/5Jj;

    iget-boolean v0, p3, LX/5Jh;->g:Z

    iget-object v1, p3, LX/5Jh;->b:LX/1dV;

    iget-object v2, p3, LX/5Jh;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p3, LX/5Jh;->f:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 897538
    invoke-virtual {p2, v0}, LX/5Jj;->setHorizontalScrollBarEnabled(Z)V

    .line 897539
    iget-object v0, p2, LX/5Jj;->a:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 897540
    iput v2, p2, LX/5Jj;->b:I

    .line 897541
    iput v3, p2, LX/5Jj;->c:I

    .line 897542
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 897527
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 897530
    check-cast p2, LX/5Jj;

    .line 897531
    const/4 p3, 0x0

    .line 897532
    iget-object p0, p2, LX/5Jj;->a:Lcom/facebook/components/ComponentView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 897533
    iput p3, p2, LX/5Jj;->b:I

    .line 897534
    iput p3, p2, LX/5Jj;->c:I

    .line 897535
    return-void
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 897529
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 897528
    const/16 v0, 0xf

    return v0
.end method
