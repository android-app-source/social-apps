.class public final LX/5Ru;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/net/Uri;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public e:Ljava/lang/String;

.field public f:Landroid/graphics/RectF;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 916715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;)LX/5Ru;
    .locals 2

    .prologue
    .line 916716
    new-instance v0, LX/5Ru;

    invoke-direct {v0}, LX/5Ru;-><init>()V

    iget-object v1, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    .line 916717
    iput-object v1, v0, LX/5Ru;->a:Landroid/net/Uri;

    .line 916718
    move-object v0, v0

    .line 916719
    iget v1, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->c:I

    .line 916720
    iput v1, v0, LX/5Ru;->b:I

    .line 916721
    move-object v0, v0

    .line 916722
    iget-object v1, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    .line 916723
    iput-object v1, v0, LX/5Ru;->c:Ljava/lang/String;

    .line 916724
    move-object v0, v0

    .line 916725
    iget-object v1, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 916726
    iput-object v1, v0, LX/5Ru;->d:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 916727
    move-object v0, v0

    .line 916728
    iget-object v1, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->f:Ljava/lang/String;

    .line 916729
    iput-object v1, v0, LX/5Ru;->e:Ljava/lang/String;

    .line 916730
    move-object v0, v0

    .line 916731
    iget-object v1, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    .line 916732
    iput-object v1, v0, LX/5Ru;->f:Landroid/graphics/RectF;

    .line 916733
    move-object v0, v0

    .line 916734
    iget-object v1, p0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->h:Ljava/lang/String;

    .line 916735
    iput-object v1, v0, LX/5Ru;->g:Ljava/lang/String;

    .line 916736
    move-object v0, v0

    .line 916737
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;
    .locals 9

    .prologue
    .line 916738
    iget-object v0, p0, LX/5Ru;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 916739
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5Ru;->c:Ljava/lang/String;

    .line 916740
    :cond_0
    new-instance v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    iget-object v1, p0, LX/5Ru;->a:Landroid/net/Uri;

    iget v2, p0, LX/5Ru;->b:I

    iget-object v3, p0, LX/5Ru;->c:Ljava/lang/String;

    iget-object v4, p0, LX/5Ru;->d:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v5, p0, LX/5Ru;->e:Ljava/lang/String;

    iget-object v6, p0, LX/5Ru;->f:Landroid/graphics/RectF;

    iget-object v7, p0, LX/5Ru;->g:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;-><init>(Landroid/net/Uri;ILjava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Ljava/lang/String;Landroid/graphics/RectF;Ljava/lang/String;B)V

    return-object v0
.end method
