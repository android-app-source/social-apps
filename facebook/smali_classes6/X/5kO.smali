.class public final LX/5kO;
.super LX/40T;
.source ""

# interfaces
.implements LX/40U;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/40T;",
        "LX/40U",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/4VK;)V
    .locals 0

    .prologue
    .line 993218
    invoke-direct {p0, p1}, LX/40T;-><init>(LX/4VK;)V

    .line 993219
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$LocationSuggestionModel$LocationTagSuggestionModel;)V
    .locals 2

    .prologue
    .line 993220
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "location_tag_suggestion"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 993221
    return-void
.end method

.method public final a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)V
    .locals 2

    .prologue
    .line 993222
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "explicit_place"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 993223
    return-void
.end method

.method public final a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;)V
    .locals 2

    .prologue
    .line 993224
    iget-object v0, p0, LX/40T;->a:LX/4VK;

    const-string v1, "tags"

    invoke-virtual {v0, v1, p1}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 993225
    return-void
.end method
