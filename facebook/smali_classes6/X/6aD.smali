.class public LX/6aD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/6a6;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/6a6;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/maps/annotation/MapApiKeyString;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1112192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112193
    const-string v0, "must provide a google map api key"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6aD;->a:Ljava/lang/String;

    .line 1112194
    iput-object p2, p0, LX/6aD;->b:LX/6a6;

    .line 1112195
    return-void
.end method
