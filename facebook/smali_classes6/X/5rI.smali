.class public LX/5rI;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[I


# instance fields
.field private final b:[F

.field private c:I

.field private d:F

.field private e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1011108
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/5rI;->a:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x4
        0x8
        0x10
        0x20
        0x40
        0x80
        0x100
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1011109
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/5rI;-><init>(F)V

    .line 1011110
    return-void
.end method

.method public constructor <init>(F)V
    .locals 1

    .prologue
    .line 1011111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1011112
    invoke-static {}, LX/5rI;->a()[F

    move-result-object v0

    iput-object v0, p0, LX/5rI;->b:[F

    .line 1011113
    const/4 v0, 0x0

    iput v0, p0, LX/5rI;->c:I

    .line 1011114
    iput p1, p0, LX/5rI;->d:F

    .line 1011115
    return-void
.end method

.method private static a()[F
    .locals 1

    .prologue
    .line 1011116
    const/16 v0, 0x9

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 4
        0x7fc00000    # NaNf
        0x7fc00000    # NaNf
        0x7fc00000    # NaNf
        0x7fc00000    # NaNf
        0x7fc00000    # NaNf
        0x7fc00000    # NaNf
        0x7fc00000    # NaNf
        0x7fc00000    # NaNf
        0x7fc00000    # NaNf
    .end array-data
.end method


# virtual methods
.method public final a(I)F
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 1011117
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-ne p1, v0, :cond_2

    :cond_0
    const/high16 v0, 0x7fc00000    # NaNf

    .line 1011118
    :goto_0
    iget v1, p0, LX/5rI;->c:I

    if-nez v1, :cond_3

    .line 1011119
    :cond_1
    :goto_1
    return v0

    .line 1011120
    :cond_2
    iget v0, p0, LX/5rI;->d:F

    goto :goto_0

    .line 1011121
    :cond_3
    iget v1, p0, LX/5rI;->c:I

    sget-object v2, LX/5rI;->a:[I

    aget v2, v2, p1

    and-int/2addr v1, v2

    if-eqz v1, :cond_4

    .line 1011122
    iget-object v0, p0, LX/5rI;->b:[F

    aget v0, v0, p1

    goto :goto_1

    .line 1011123
    :cond_4
    iget-boolean v1, p0, LX/5rI;->e:Z

    if-eqz v1, :cond_1

    .line 1011124
    const/4 v1, 0x1

    if-eq p1, v1, :cond_5

    const/4 v1, 0x3

    if-ne p1, v1, :cond_6

    :cond_5
    const/4 v1, 0x7

    .line 1011125
    :goto_2
    iget v2, p0, LX/5rI;->c:I

    sget-object v3, LX/5rI;->a:[I

    aget v3, v3, v1

    and-int/2addr v2, v3

    if-eqz v2, :cond_7

    .line 1011126
    iget-object v0, p0, LX/5rI;->b:[F

    aget v0, v0, v1

    goto :goto_1

    .line 1011127
    :cond_6
    const/4 v1, 0x6

    goto :goto_2

    .line 1011128
    :cond_7
    iget v1, p0, LX/5rI;->c:I

    sget-object v2, LX/5rI;->a:[I

    aget v2, v2, v4

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    .line 1011129
    iget-object v0, p0, LX/5rI;->b:[F

    aget v0, v0, v4

    goto :goto_1
.end method

.method public final a(IF)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1011130
    iget-object v2, p0, LX/5rI;->b:[F

    aget v2, v2, p1

    invoke-static {v2, p2}, LX/5qm;->a(FF)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1011131
    iget-object v2, p0, LX/5rI;->b:[F

    aput p2, v2, p1

    .line 1011132
    invoke-static {p2}, LX/1mo;->a(F)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1011133
    iget v2, p0, LX/5rI;->c:I

    sget-object v3, LX/5rI;->a:[I

    aget v3, v3, p1

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    iput v2, p0, LX/5rI;->c:I

    .line 1011134
    :goto_0
    iget v2, p0, LX/5rI;->c:I

    sget-object v3, LX/5rI;->a:[I

    const/16 v4, 0x8

    aget v3, v3, v4

    and-int/2addr v2, v3

    if-nez v2, :cond_0

    iget v2, p0, LX/5rI;->c:I

    sget-object v3, LX/5rI;->a:[I

    const/4 v4, 0x7

    aget v3, v3, v4

    and-int/2addr v2, v3

    if-nez v2, :cond_0

    iget v2, p0, LX/5rI;->c:I

    sget-object v3, LX/5rI;->a:[I

    const/4 v4, 0x6

    aget v3, v3, v4

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, LX/5rI;->e:Z

    .line 1011135
    :goto_1
    return v1

    .line 1011136
    :cond_2
    iget v2, p0, LX/5rI;->c:I

    sget-object v3, LX/5rI;->a:[I

    aget v3, v3, p1

    or-int/2addr v2, v3

    iput v2, p0, LX/5rI;->c:I

    goto :goto_0

    :cond_3
    move v1, v0

    .line 1011137
    goto :goto_1
.end method

.method public final b(I)F
    .locals 1

    .prologue
    .line 1011138
    iget-object v0, p0, LX/5rI;->b:[F

    aget v0, v0, p1

    return v0
.end method
