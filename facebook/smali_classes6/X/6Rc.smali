.class public LX/6Rc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method private constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 1090117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1090118
    iput-wide p1, p0, LX/6Rc;->a:J

    .line 1090119
    iput-wide p3, p0, LX/6Rc;->b:J

    .line 1090120
    return-void
.end method

.method public static a(J)LX/6Rc;
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 1090121
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1090122
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1090123
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1090124
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v4

    invoke-direct {v2, v1, v3, v4}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 1090125
    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->roll(II)V

    .line 1090126
    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v0

    invoke-direct {v3, v1, v4, v0}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 1090127
    new-instance v0, LX/6Rc;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-direct {v0, v4, v5, v2, v3}, LX/6Rc;-><init>(JJ)V

    return-object v0
.end method

.method public static b(J)LX/6Rc;
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 1090128
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1090129
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1090130
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v1

    .line 1090131
    invoke-virtual {v0, v6, v1}, Ljava/util/Calendar;->set(II)V

    .line 1090132
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v4

    invoke-direct {v2, v3, v1, v4}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 1090133
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1090134
    invoke-virtual {v0, v5, v5}, Ljava/util/Calendar;->roll(II)V

    .line 1090135
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v1

    .line 1090136
    invoke-virtual {v0, v6, v1}, Ljava/util/Calendar;->set(II)V

    .line 1090137
    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v0

    invoke-direct {v3, v4, v1, v0}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 1090138
    new-instance v0, LX/6Rc;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-direct {v0, v4, v5, v2, v3}, LX/6Rc;-><init>(JJ)V

    return-object v0
.end method


# virtual methods
.method public final c(J)Z
    .locals 3

    .prologue
    .line 1090139
    iget-wide v0, p0, LX/6Rc;->a:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, LX/6Rc;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
