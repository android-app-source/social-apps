.class public LX/60x;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:J

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:J

.field public final g:I

.field public final h:LX/60v;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>(JIIIIJILX/60v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1039473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039474
    iput-wide p1, p0, LX/60x;->a:J

    .line 1039475
    iput p3, p0, LX/60x;->b:I

    .line 1039476
    iput p4, p0, LX/60x;->c:I

    .line 1039477
    iput p5, p0, LX/60x;->d:I

    .line 1039478
    iput p6, p0, LX/60x;->e:I

    .line 1039479
    iput-wide p7, p0, LX/60x;->f:J

    .line 1039480
    iput p9, p0, LX/60x;->g:I

    .line 1039481
    iput-object p10, p0, LX/60x;->h:LX/60v;

    .line 1039482
    iput-object p11, p0, LX/60x;->i:Ljava/lang/String;

    .line 1039483
    iput-object p12, p0, LX/60x;->j:Ljava/lang/String;

    .line 1039484
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1039485
    iget-object v0, p0, LX/60x;->h:LX/60v;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/60x;->h:LX/60v;

    iget-boolean v0, v0, LX/60v;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
