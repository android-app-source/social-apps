.class public final LX/5i1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 981362
    const/4 v12, 0x0

    .line 981363
    const/4 v11, 0x0

    .line 981364
    const/4 v10, 0x0

    .line 981365
    const/4 v9, 0x0

    .line 981366
    const/4 v8, 0x0

    .line 981367
    const/4 v7, 0x0

    .line 981368
    const/4 v6, 0x0

    .line 981369
    const/4 v5, 0x0

    .line 981370
    const/4 v4, 0x0

    .line 981371
    const/4 v3, 0x0

    .line 981372
    const/4 v2, 0x0

    .line 981373
    const/4 v1, 0x0

    .line 981374
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 981375
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 981376
    const/4 v1, 0x0

    .line 981377
    :goto_0
    return v1

    .line 981378
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 981379
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_9

    .line 981380
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 981381
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 981382
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 981383
    const-string v14, "can_see_voice_switcher"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 981384
    const/4 v4, 0x1

    .line 981385
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 981386
    :cond_2
    const-string v14, "can_viewer_comment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 981387
    const/4 v3, 0x1

    .line 981388
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 981389
    :cond_3
    const-string v14, "can_viewer_like"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 981390
    const/4 v2, 0x1

    .line 981391
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 981392
    :cond_4
    const-string v14, "comments"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 981393
    invoke-static/range {p0 .. p1}, LX/5hz;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 981394
    :cond_5
    const-string v14, "does_viewer_like"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 981395
    const/4 v1, 0x1

    .line 981396
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 981397
    :cond_6
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 981398
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 981399
    :cond_7
    const-string v14, "legacy_api_post_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 981400
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 981401
    :cond_8
    const-string v14, "likers"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 981402
    invoke-static/range {p0 .. p1}, LX/5i0;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 981403
    :cond_9
    const/16 v13, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 981404
    if-eqz v4, :cond_a

    .line 981405
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 981406
    :cond_a
    if-eqz v3, :cond_b

    .line 981407
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 981408
    :cond_b
    if-eqz v2, :cond_c

    .line 981409
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 981410
    :cond_c
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 981411
    if-eqz v1, :cond_d

    .line 981412
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 981413
    :cond_d
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 981414
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 981415
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 981416
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 981417
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 981418
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981419
    if-eqz v0, :cond_0

    .line 981420
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981421
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981422
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981423
    if-eqz v0, :cond_1

    .line 981424
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981425
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981426
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981427
    if-eqz v0, :cond_2

    .line 981428
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981429
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981430
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981431
    if-eqz v0, :cond_3

    .line 981432
    const-string v1, "comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981433
    invoke-static {p0, v0, p2}, LX/5hz;->a(LX/15i;ILX/0nX;)V

    .line 981434
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981435
    if-eqz v0, :cond_4

    .line 981436
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981437
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981438
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981439
    if-eqz v0, :cond_5

    .line 981440
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981441
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981442
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981443
    if-eqz v0, :cond_6

    .line 981444
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981445
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981446
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981447
    if-eqz v0, :cond_7

    .line 981448
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981449
    invoke-static {p0, v0, p2}, LX/5i0;->a(LX/15i;ILX/0nX;)V

    .line 981450
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 981451
    return-void
.end method
