.class public LX/6bb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6bX;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1113737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113738
    iput-object p1, p0, LX/6bb;->a:Landroid/content/Context;

    .line 1113739
    iput-object p2, p0, LX/6bb;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1113740
    return-void
.end method

.method public static b(LX/0QB;)LX/6bb;
    .locals 3

    .prologue
    .line 1113735
    new-instance v2, LX/6bb;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/6bb;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 1113736
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Z
    .locals 3

    .prologue
    .line 1113732
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/3GK;->a:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1113733
    iget-object v1, p0, LX/6bb;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/6bb;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1113734
    const/4 v0, 0x1

    return v0
.end method
