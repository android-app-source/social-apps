.class public LX/5na;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1004412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004413
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1004414
    check-cast p1, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;

    .line 1004415
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1004416
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "event"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004417
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "client_time"

    iget-wide v2, p1, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004418
    iget-object v0, p1, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1004419
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "from_privacy"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004420
    :cond_0
    iget-object v0, p1, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1004421
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "to_privacy"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004422
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "product"

    const-string v2, "fb4a_composer"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004423
    iget-object v0, p1, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1004424
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "source"

    iget-object v2, p1, Lcom/facebook/privacy/protocol/ReportInlinePrivacySurveyActionParams;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004425
    :cond_2
    new-instance v0, LX/14N;

    const-string v1, "reportInlinePrivacySurveyAction"

    const-string v2, "POST"

    const-string v3, "me/inline_privacy_survey_events"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->CAN_WAIT:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1004426
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1004427
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
