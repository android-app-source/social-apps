.class public final LX/5cR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 962810
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_c

    .line 962811
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 962812
    :goto_0
    return v1

    .line 962813
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 962814
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 962815
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 962816
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 962817
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 962818
    const-string v12, "airline_logo_image_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 962819
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 962820
    :cond_2
    const-string v12, "auxiliary_fields"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 962821
    invoke-static {p0, p1}, LX/5cb;->b(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 962822
    :cond_3
    const-string v12, "flight_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 962823
    invoke-static {p0, p1}, LX/5cX;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 962824
    :cond_4
    const-string v12, "header_image_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 962825
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 962826
    :cond_5
    const-string v12, "header_text_field"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 962827
    invoke-static {p0, p1}, LX/5cb;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 962828
    :cond_6
    const-string v12, "passenger"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 962829
    invoke-static {p0, p1}, LX/5ca;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 962830
    :cond_7
    const-string v12, "qr_code"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 962831
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 962832
    :cond_8
    const-string v12, "qr_code_header_image_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 962833
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 962834
    :cond_9
    const-string v12, "qr_code_image_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 962835
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 962836
    :cond_a
    const-string v12, "secondary_fields"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 962837
    invoke-static {p0, p1}, LX/5cb;->b(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 962838
    :cond_b
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 962839
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 962840
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 962841
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 962842
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 962843
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 962844
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 962845
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 962846
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 962847
    const/16 v1, 0x8

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 962848
    const/16 v1, 0x9

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 962849
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 962850
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 962851
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 962852
    if-eqz v0, :cond_0

    .line 962853
    const-string v1, "airline_logo_image_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962854
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962855
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 962856
    if-eqz v0, :cond_1

    .line 962857
    const-string v1, "auxiliary_fields"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962858
    invoke-static {p0, v0, p2, p3}, LX/5cb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 962859
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 962860
    if-eqz v0, :cond_2

    .line 962861
    const-string v1, "flight_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962862
    invoke-static {p0, v0, p2, p3}, LX/5cX;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 962863
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 962864
    if-eqz v0, :cond_3

    .line 962865
    const-string v1, "header_image_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962866
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962867
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 962868
    if-eqz v0, :cond_4

    .line 962869
    const-string v1, "header_text_field"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962870
    invoke-static {p0, v0, p2}, LX/5cb;->a(LX/15i;ILX/0nX;)V

    .line 962871
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 962872
    if-eqz v0, :cond_5

    .line 962873
    const-string v1, "passenger"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962874
    invoke-static {p0, v0, p2, p3}, LX/5ca;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 962875
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 962876
    if-eqz v0, :cond_6

    .line 962877
    const-string v1, "qr_code"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962878
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962879
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 962880
    if-eqz v0, :cond_7

    .line 962881
    const-string v1, "qr_code_header_image_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962882
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962883
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 962884
    if-eqz v0, :cond_8

    .line 962885
    const-string v1, "qr_code_image_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962886
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 962887
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 962888
    if-eqz v0, :cond_9

    .line 962889
    const-string v1, "secondary_fields"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 962890
    invoke-static {p0, v0, p2, p3}, LX/5cb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 962891
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 962892
    return-void
.end method
