.class public final LX/5zT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1034798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v13, 0x0

    const/4 v2, 0x0

    .line 1034799
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1034800
    iget-object v1, p0, LX/5zT;->g:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1034801
    iget-object v3, p0, LX/5zT;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1034802
    iget-object v5, p0, LX/5zT;->i:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1034803
    iget-object v6, p0, LX/5zT;->l:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1034804
    iget-object v7, p0, LX/5zT;->m:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1034805
    iget-object v8, p0, LX/5zT;->n:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1034806
    iget-object v9, p0, LX/5zT;->o:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 1034807
    iget-object v10, p0, LX/5zT;->p:Ljava/lang/String;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1034808
    const/16 v11, 0x11

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1034809
    iget-boolean v11, p0, LX/5zT;->a:Z

    invoke-virtual {v0, v13, v11}, LX/186;->a(IZ)V

    .line 1034810
    iget-boolean v11, p0, LX/5zT;->b:Z

    invoke-virtual {v0, v4, v11}, LX/186;->a(IZ)V

    .line 1034811
    const/4 v11, 0x2

    iget-boolean v12, p0, LX/5zT;->c:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 1034812
    const/4 v11, 0x3

    iget-boolean v12, p0, LX/5zT;->d:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 1034813
    const/4 v11, 0x4

    iget-boolean v12, p0, LX/5zT;->e:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 1034814
    const/4 v11, 0x5

    iget-boolean v12, p0, LX/5zT;->f:Z

    invoke-virtual {v0, v11, v12}, LX/186;->a(IZ)V

    .line 1034815
    const/4 v11, 0x6

    invoke-virtual {v0, v11, v1}, LX/186;->b(II)V

    .line 1034816
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1034817
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1034818
    const/16 v1, 0x9

    iget-boolean v3, p0, LX/5zT;->j:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1034819
    const/16 v1, 0xa

    iget-boolean v3, p0, LX/5zT;->k:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1034820
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1034821
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1034822
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1034823
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1034824
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1034825
    const/16 v1, 0x10

    iget-boolean v3, p0, LX/5zT;->q:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1034826
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1034827
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1034828
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1034829
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1034830
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1034831
    new-instance v1, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;-><init>(LX/15i;)V

    .line 1034832
    return-object v1
.end method
