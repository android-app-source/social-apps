.class public LX/6KT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0uf;


# direct methods
.method public constructor <init>(LX/0uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1076742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076743
    iput-object p1, p0, LX/6KT;->a:LX/0uf;

    .line 1076744
    return-void
.end method

.method public static a(LX/0QB;)LX/6KT;
    .locals 1

    .prologue
    .line 1076741
    invoke-static {p0}, LX/6KT;->b(LX/0QB;)LX/6KT;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/6KT;
    .locals 2

    .prologue
    .line 1076739
    new-instance v1, LX/6KT;

    invoke-static {p0}, LX/0ts;->a(LX/0QB;)LX/0uf;

    move-result-object v0

    check-cast v0, LX/0uf;

    invoke-direct {v1, v0}, LX/6KT;-><init>(LX/0uf;)V

    .line 1076740
    return-object v1
.end method


# virtual methods
.method public final a(LX/6KC;)V
    .locals 6

    .prologue
    .line 1076710
    iget-object v0, p0, LX/6KT;->a:LX/0uf;

    sget-wide v2, LX/0X5;->iz:J

    invoke-virtual {v0, v2, v3}, LX/0uf;->a(J)LX/1jr;

    move-result-object v0

    .line 1076711
    invoke-virtual {v0}, LX/1jr;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1076712
    :goto_0
    return-void

    .line 1076713
    :cond_0
    const-string v1, "encodingProfile"

    const-string v2, "high"

    .line 1076714
    invoke-virtual {v0, v1}, LX/1jr;->a(Ljava/lang/String;)I

    move-result v3

    .line 1076715
    if-gez v3, :cond_1

    .line 1076716
    iget-object v3, v0, LX/1jr;->h:LX/0uc;

    iget-object v4, v0, LX/1jr;->a:LX/1jp;

    invoke-interface {v4}, LX/1jp;->a()LX/1jk;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p0, "Param not found: "

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object p0, v0, LX/1jr;->a:LX/1jp;

    invoke-interface {p0}, LX/1jp;->b()I

    move-result p0

    invoke-interface {v3, v4, v5, p0}, LX/0uc;->a(LX/1jk;Ljava/lang/String;I)V

    .line 1076717
    :goto_1
    move-object v1, v2

    .line 1076718
    const-string v2, "useCamera1"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/1jr;->a(Ljava/lang/String;Z)Z

    move-result v2

    .line 1076719
    const-string v3, "useCustomVideoRecorder"

    .line 1076720
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_2

    const/4 v4, 0x1

    :goto_2
    move v4, v4

    .line 1076721
    invoke-virtual {v0, v3, v4}, LX/1jr;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 1076722
    new-instance v3, LX/6KA;

    invoke-direct {v3}, LX/6KA;-><init>()V

    new-instance v4, LX/6KE;

    invoke-direct {v4}, LX/6KE;-><init>()V

    .line 1076723
    iput-boolean v2, v4, LX/6KE;->a:Z

    .line 1076724
    move-object v2, v4

    .line 1076725
    invoke-virtual {v2}, LX/6KE;->a()LX/6KF;

    move-result-object v2

    .line 1076726
    iput-object v2, v3, LX/6KA;->a:LX/6KF;

    .line 1076727
    move-object v2, v3

    .line 1076728
    new-instance v3, LX/6KJ;

    invoke-direct {v3}, LX/6KJ;-><init>()V

    .line 1076729
    iput-object v1, v3, LX/6KJ;->a:Ljava/lang/String;

    .line 1076730
    move-object v1, v3

    .line 1076731
    iput-boolean v0, v1, LX/6KJ;->b:Z

    .line 1076732
    move-object v0, v1

    .line 1076733
    invoke-virtual {v0}, LX/6KJ;->a()LX/6KK;

    move-result-object v0

    .line 1076734
    iput-object v0, v2, LX/6KA;->b:LX/6KK;

    .line 1076735
    move-object v0, v2

    .line 1076736
    invoke-virtual {v0}, LX/6KA;->a()LX/6KB;

    move-result-object v0

    .line 1076737
    sput-object v0, LX/6KD;->b:LX/6KB;

    .line 1076738
    goto :goto_0

    :cond_1
    invoke-virtual {v0, v3, v2}, LX/1jr;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_2
.end method
