.class public final LX/6Q6;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/directinstall/appdetails/AppDetailsFragment;)V
    .locals 0

    .prologue
    .line 1086837
    iput-object p1, p0, LX/6Q6;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1086838
    iget-object v0, p0, LX/6Q6;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    iget-object v1, p0, LX/6Q6;->b:Ljava/lang/String;

    iget-object v2, p0, LX/6Q6;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1086839
    :try_start_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1086840
    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1086841
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1086842
    iget-object v4, v0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->g:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1086843
    :goto_0
    return-void

    .line 1086844
    :catch_0
    move-exception v3

    .line 1086845
    iget-object v4, v0, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->e:LX/03V;

    sget-object v5, Lcom/facebook/directinstall/appdetails/AppDetailsFragment;->a:Ljava/lang/String;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Unable to openURL: "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1086846
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1086847
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1086848
    iget-object v0, p0, LX/6Q6;->a:Lcom/facebook/directinstall/appdetails/AppDetailsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a068a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1086849
    return-void
.end method
