.class public final LX/682;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/67g;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/android/maps/ClusterItem;",
        ">",
        "Ljava/lang/Object;",
        "LX/67g;",
        "Ljava/lang/Comparable",
        "<",
        "LX/682",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public a:D

.field public b:D

.field private final c:Ljava/util/Comparator;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final d:LX/67m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public e:LX/67h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/67h",
            "<TT;>;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/android/maps/model/LatLng;


# direct methods
.method private c()V
    .locals 2

    .prologue
    .line 1054158
    iget-object v0, p0, LX/682;->d:LX/67m;

    invoke-virtual {v0}, LX/67m;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    .line 1054159
    iget-object v1, p0, LX/682;->f:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1054160
    iput-object v0, p0, LX/682;->f:Lcom/facebook/android/maps/model/LatLng;

    .line 1054161
    iget-object v0, p0, LX/682;->f:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v0, v1}, LX/31h;->d(D)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, LX/67h;->a(D)D

    move-result-wide v0

    iput-wide v0, p0, LX/682;->a:D

    .line 1054162
    iget-object v0, p0, LX/682;->f:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v0, v1}, LX/31h;->b(D)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LX/682;->b:D

    .line 1054163
    :cond_0
    return-void
.end method


# virtual methods
.method public final a([D)V
    .locals 4

    .prologue
    .line 1054164
    invoke-direct {p0}, LX/682;->c()V

    .line 1054165
    const/4 v0, 0x0

    iget-wide v2, p0, LX/682;->a:D

    aput-wide v2, p1, v0

    .line 1054166
    const/4 v0, 0x1

    iget-wide v2, p0, LX/682;->b:D

    aput-wide v2, p1, v0

    .line 1054167
    return-void
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 6

    .prologue
    .line 1054168
    check-cast p1, LX/682;

    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 1054169
    iget-object v2, p0, LX/682;->c:Ljava/util/Comparator;

    if-eqz v2, :cond_1

    .line 1054170
    iget-object v0, p0, LX/682;->c:Ljava/util/Comparator;

    iget-object v1, p0, LX/682;->d:LX/67m;

    iget-object v2, p1, LX/682;->d:LX/67m;

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 1054171
    :cond_0
    :goto_0
    return v0

    .line 1054172
    :cond_1
    iget-object v2, p0, LX/682;->d:LX/67m;

    instance-of v2, v2, Ljava/lang/Comparable;

    if-eqz v2, :cond_2

    .line 1054173
    iget-object v0, p0, LX/682;->d:LX/67m;

    check-cast v0, Ljava/lang/Comparable;

    iget-object v1, p1, LX/682;->d:LX/67m;

    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    .line 1054174
    :cond_2
    invoke-direct {p0}, LX/682;->c()V

    .line 1054175
    invoke-direct {p1}, LX/682;->c()V

    .line 1054176
    iget-wide v2, p0, LX/682;->a:D

    iget-wide v4, p1, LX/682;->a:D

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_3

    .line 1054177
    iget-wide v2, p0, LX/682;->a:D

    iget-wide v4, p1, LX/682;->a:D

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1054178
    :cond_3
    iget-wide v2, p0, LX/682;->b:D

    iget-wide v4, p1, LX/682;->b:D

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_4

    .line 1054179
    iget-wide v2, p0, LX/682;->b:D

    iget-wide v4, p1, LX/682;->b:D

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1054180
    :cond_4
    invoke-virtual {p0}, LX/682;->hashCode()I

    move-result v2

    invoke-virtual {p1}, LX/682;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_5

    .line 1054181
    invoke-virtual {p0}, LX/682;->hashCode()I

    move-result v2

    invoke-virtual {p1}, LX/682;->hashCode()I

    move-result v3

    if-gt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 1054182
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1054183
    if-ne p0, p1, :cond_0

    .line 1054184
    const/4 v0, 0x1

    .line 1054185
    :goto_0
    return v0

    .line 1054186
    :cond_0
    instance-of v0, p1, LX/682;

    if-nez v0, :cond_1

    .line 1054187
    const/4 v0, 0x0

    goto :goto_0

    .line 1054188
    :cond_1
    check-cast p1, LX/682;

    .line 1054189
    iget-object v0, p0, LX/682;->d:LX/67m;

    iget-object v1, p1, LX/682;->d:LX/67m;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1054190
    iget-object v0, p0, LX/682;->d:LX/67m;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
