.class public LX/6L3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:LX/6K0;

.field public final b:Landroid/os/Handler;

.field private final c:LX/6L5;

.field public d:I

.field public volatile e:LX/6L2;

.field public f:Landroid/media/AudioRecord;

.field private final g:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/6L5;Landroid/os/Handler;LX/6K0;)V
    .locals 3

    .prologue
    .line 1077910
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077911
    new-instance v0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$1;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$1;-><init>(LX/6L3;)V

    iput-object v0, p0, LX/6L3;->g:Ljava/lang/Runnable;

    .line 1077912
    iput-object p1, p0, LX/6L3;->c:LX/6L5;

    .line 1077913
    iput-object p2, p0, LX/6L3;->b:Landroid/os/Handler;

    .line 1077914
    iput-object p3, p0, LX/6L3;->a:LX/6K0;

    .line 1077915
    sget-object v0, LX/6L2;->STOPPED:LX/6L2;

    iput-object v0, p0, LX/6L3;->e:LX/6L2;

    .line 1077916
    iget-object v0, p0, LX/6L3;->c:LX/6L5;

    iget v0, v0, LX/6L5;->b:I

    iget-object v1, p0, LX/6L3;->c:LX/6L5;

    iget v1, v1, LX/6L5;->c:I

    iget-object v2, p0, LX/6L3;->c:LX/6L5;

    iget v2, v2, LX/6L5;->d:I

    invoke-static {v0, v1, v2}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, LX/6L3;->d:I

    .line 1077917
    iget v0, p0, LX/6L3;->d:I

    if-gtz v0, :cond_0

    .line 1077918
    const/16 v0, 0x1000

    iput v0, p0, LX/6L3;->d:I

    .line 1077919
    :goto_0
    return-void

    .line 1077920
    :cond_0
    iget v0, p0, LX/6L3;->d:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/6L3;->d:I

    goto :goto_0
.end method

.method public static a(LX/6L3;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 1077921
    if-nez p1, :cond_0

    .line 1077922
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The handler cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1077923
    :cond_0
    iget-object v0, p0, LX/6L3;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 1077924
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The handler must be on a separate thread then the recording one"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1077925
    :cond_1
    return-void
.end method

.method public static d(LX/6L3;LX/6JU;Landroid/os/Handler;)V
    .locals 6

    .prologue
    .line 1077926
    iget-object v0, p0, LX/6L3;->e:LX/6L2;

    sget-object v1, LX/6L2;->STOPPED:LX/6L2;

    if-eq v0, v1, :cond_0

    .line 1077927
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Must only call prepare() on a stopped AudioRecorder. Current state is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6L3;->e:LX/6L2;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2, v0}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    .line 1077928
    :goto_0
    return-void

    .line 1077929
    :cond_0
    :try_start_0
    new-instance v0, Landroid/media/AudioRecord;

    iget-object v1, p0, LX/6L3;->c:LX/6L5;

    iget v1, v1, LX/6L5;->a:I

    iget-object v2, p0, LX/6L3;->c:LX/6L5;

    iget v2, v2, LX/6L5;->b:I

    iget-object v3, p0, LX/6L3;->c:LX/6L5;

    iget v3, v3, LX/6L5;->c:I

    iget-object v4, p0, LX/6L3;->c:LX/6L5;

    iget v4, v4, LX/6L5;->d:I

    iget-object v5, p0, LX/6L3;->c:LX/6L5;

    iget v5, v5, LX/6L5;->e:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, LX/6L3;->f:Landroid/media/AudioRecord;

    .line 1077930
    iget-object v0, p0, LX/6L3;->f:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-nez v0, :cond_1

    .line 1077931
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Could not prepare audio recording"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077932
    :catch_0
    move-exception v0

    .line 1077933
    invoke-static {p1, p2, v0}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1077934
    :cond_1
    sget-object v0, LX/6L2;->PREPARED:LX/6L2;

    iput-object v0, p0, LX/6L3;->e:LX/6L2;

    .line 1077935
    invoke-static {p1, p2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public static declared-synchronized e(LX/6L3;LX/6JU;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 1077936
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6L3;->e:LX/6L2;

    sget-object v1, LX/6L2;->PREPARED:LX/6L2;

    if-eq v0, v1, :cond_0

    .line 1077937
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "prepare() must be called before starting audio recording. Current state is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6L3;->e:LX/6L2;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2, v0}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1077938
    :goto_0
    monitor-exit p0

    return-void

    .line 1077939
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/6L3;->f:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1077940
    :try_start_2
    sget-object v0, LX/6L2;->STARTED:LX/6L2;

    iput-object v0, p0, LX/6L3;->e:LX/6L2;

    .line 1077941
    iget-object v0, p0, LX/6L3;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/6L3;->g:Ljava/lang/Runnable;

    const v2, -0x678b629b

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1077942
    invoke-static {p1, p2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1077943
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1077944
    :catch_0
    move-exception v0

    .line 1077945
    :try_start_3
    invoke-static {p1, p2, v0}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized f(LX/6L3;LX/6JU;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 1077946
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6L3;->f:Landroid/media/AudioRecord;

    if-eqz v0, :cond_0

    .line 1077947
    iget-object v0, p0, LX/6L3;->f:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 1077948
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/6L3;->f:Landroid/media/AudioRecord;

    .line 1077949
    invoke-static {p1, p2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1077950
    monitor-exit p0

    return-void

    .line 1077951
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized c(LX/6JU;Landroid/os/Handler;)V
    .locals 3

    .prologue
    .line 1077952
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p2}, LX/6L3;->a(LX/6L3;Landroid/os/Handler;)V

    .line 1077953
    sget-object v0, LX/6L2;->STOPPED:LX/6L2;

    iput-object v0, p0, LX/6L3;->e:LX/6L2;

    .line 1077954
    iget-object v0, p0, LX/6L3;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/cameracore/mediapipeline/recorder/AudioRecorder$4;-><init>(LX/6L3;LX/6JU;Landroid/os/Handler;)V

    const v2, 0x12af6ba1

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1077955
    monitor-exit p0

    return-void

    .line 1077956
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
