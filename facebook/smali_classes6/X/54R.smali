.class public final LX/54R;
.super LX/52s;
.source ""


# static fields
.field public static final a:LX/53o;

.field public static final b:LX/53o;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 828006
    new-instance v0, LX/53o;

    const-string v1, "RxCachedThreadScheduler-"

    invoke-direct {v0, v1}, LX/53o;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/54R;->a:LX/53o;

    .line 828007
    new-instance v0, LX/53o;

    const-string v1, "RxCachedWorkerPoolEvictor-"

    invoke-direct {v0, v1}, LX/53o;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/54R;->b:LX/53o;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 828008
    invoke-direct {p0}, LX/52s;-><init>()V

    .line 828009
    return-void
.end method


# virtual methods
.method public final a()LX/52r;
    .locals 3

    .prologue
    .line 828010
    new-instance v0, LX/54P;

    sget-object v1, LX/54O;->d:LX/54O;

    .line 828011
    :cond_0
    iget-object v2, v1, LX/54O;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 828012
    iget-object v2, v1, LX/54O;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/54Q;

    .line 828013
    if-eqz v2, :cond_0

    .line 828014
    :goto_0
    move-object v1, v2

    .line 828015
    invoke-direct {v0, v1}, LX/54P;-><init>(LX/54Q;)V

    return-object v0

    :cond_1
    new-instance v2, LX/54Q;

    sget-object p0, LX/54R;->a:LX/53o;

    invoke-direct {v2, p0}, LX/54Q;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    goto :goto_0
.end method
