.class public final LX/6Go;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;)V
    .locals 0

    .prologue
    .line 1070947
    iput-object p1, p0, LX/6Go;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1070948
    iget-object v0, p0, LX/6Go;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->x:Landroid/widget/FrameLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setDrawingCacheEnabled(Z)V

    .line 1070949
    iget-object v0, p0, LX/6Go;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->x:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->buildDrawingCache()V

    .line 1070950
    iget-object v0, p0, LX/6Go;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->x:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1070951
    iget-object v1, p0, LX/6Go;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->x:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setDrawingCacheEnabled(Z)V

    .line 1070952
    iget-object v1, p0, LX/6Go;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerDoodleFragment;->o:LX/1Er;

    const-string v2, "bugreporter-doodle-"

    const-string v3, ".jpg"

    sget-object v4, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v1, v2, v3, v4}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v1

    .line 1070953
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1070954
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1070955
    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1070956
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 1070957
    return-object v1

    .line 1070958
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method
