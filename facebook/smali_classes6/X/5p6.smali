.class public final LX/5p6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/bridge/Callback;


# instance fields
.field private final a:Lcom/facebook/react/bridge/CatalystInstance;

.field private final b:Lcom/facebook/react/bridge/ExecutorToken;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/CatalystInstance;Lcom/facebook/react/bridge/ExecutorToken;I)V
    .locals 0

    .prologue
    .line 1007851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007852
    iput-object p1, p0, LX/5p6;->a:Lcom/facebook/react/bridge/CatalystInstance;

    .line 1007853
    iput-object p2, p0, LX/5p6;->b:Lcom/facebook/react/bridge/ExecutorToken;

    .line 1007854
    iput p3, p0, LX/5p6;->c:I

    .line 1007855
    return-void
.end method


# virtual methods
.method public final varargs a([Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1007856
    iget-object v0, p0, LX/5p6;->a:Lcom/facebook/react/bridge/CatalystInstance;

    iget-object v1, p0, LX/5p6;->b:Lcom/facebook/react/bridge/ExecutorToken;

    iget v2, p0, LX/5p6;->c:I

    invoke-static {p1}, LX/5op;->a([Ljava/lang/Object;)Lcom/facebook/react/bridge/WritableNativeArray;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/react/bridge/CatalystInstance;->invokeCallback(Lcom/facebook/react/bridge/ExecutorToken;ILcom/facebook/react/bridge/NativeArray;)V

    .line 1007857
    return-void
.end method
