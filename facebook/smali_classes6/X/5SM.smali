.class public final LX/5SM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/5SK;

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:I

.field public l:I

.field public m:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public n:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public o:I

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 917825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917826
    sget-object v0, LX/5SK;->TRIM:LX/5SK;

    iput-object v0, p0, LX/5SM;->a:LX/5SK;

    .line 917827
    iput-object v3, p0, LX/5SM;->b:Ljava/lang/String;

    .line 917828
    iput-object v3, p0, LX/5SM;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 917829
    iput-boolean v1, p0, LX/5SM;->d:Z

    .line 917830
    iput-boolean v1, p0, LX/5SM;->e:Z

    .line 917831
    iput-boolean v1, p0, LX/5SM;->f:Z

    .line 917832
    iput-boolean v1, p0, LX/5SM;->g:Z

    .line 917833
    iput-boolean v1, p0, LX/5SM;->h:Z

    .line 917834
    iput-boolean v1, p0, LX/5SM;->i:Z

    .line 917835
    iput-boolean v1, p0, LX/5SM;->j:Z

    .line 917836
    const/16 v0, 0x3e8

    iput v0, p0, LX/5SM;->k:I

    .line 917837
    iput v2, p0, LX/5SM;->l:I

    .line 917838
    iput v2, p0, LX/5SM;->m:I

    .line 917839
    iput v2, p0, LX/5SM;->n:I

    .line 917840
    iput v1, p0, LX/5SM;->o:I

    .line 917841
    const-string v0, "standard"

    iput-object v0, p0, LX/5SM;->q:Ljava/lang/String;

    .line 917842
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 917843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917844
    sget-object v0, LX/5SK;->TRIM:LX/5SK;

    iput-object v0, p0, LX/5SM;->a:LX/5SK;

    .line 917845
    iput-object v3, p0, LX/5SM;->b:Ljava/lang/String;

    .line 917846
    iput-object v3, p0, LX/5SM;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 917847
    iput-boolean v1, p0, LX/5SM;->d:Z

    .line 917848
    iput-boolean v1, p0, LX/5SM;->e:Z

    .line 917849
    iput-boolean v1, p0, LX/5SM;->f:Z

    .line 917850
    iput-boolean v1, p0, LX/5SM;->g:Z

    .line 917851
    iput-boolean v1, p0, LX/5SM;->h:Z

    .line 917852
    iput-boolean v1, p0, LX/5SM;->i:Z

    .line 917853
    iput-boolean v1, p0, LX/5SM;->j:Z

    .line 917854
    const/16 v0, 0x3e8

    iput v0, p0, LX/5SM;->k:I

    .line 917855
    iput v2, p0, LX/5SM;->l:I

    .line 917856
    iput v2, p0, LX/5SM;->m:I

    .line 917857
    iput v2, p0, LX/5SM;->n:I

    .line 917858
    iput v1, p0, LX/5SM;->o:I

    .line 917859
    const-string v0, "standard"

    iput-object v0, p0, LX/5SM;->q:Ljava/lang/String;

    .line 917860
    iget-object v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->a:LX/5SK;

    move-object v0, v0

    .line 917861
    iput-object v0, p0, LX/5SM;->a:LX/5SK;

    .line 917862
    iget-object v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    move-object v0, v0

    .line 917863
    iput-object v0, p0, LX/5SM;->b:Ljava/lang/String;

    .line 917864
    iget-object v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v0, v0

    .line 917865
    iput-object v0, p0, LX/5SM;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 917866
    iget-boolean v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->f:Z

    move v0, v0

    .line 917867
    iput-boolean v0, p0, LX/5SM;->d:Z

    .line 917868
    iget-boolean v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->g:Z

    move v0, v0

    .line 917869
    iput-boolean v0, p0, LX/5SM;->e:Z

    .line 917870
    iget-boolean v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->d:Z

    move v0, v0

    .line 917871
    iput-boolean v0, p0, LX/5SM;->f:Z

    .line 917872
    iget-boolean v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->e:Z

    move v0, v0

    .line 917873
    iput-boolean v0, p0, LX/5SM;->g:Z

    .line 917874
    iget-boolean v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->h:Z

    move v0, v0

    .line 917875
    iput-boolean v0, p0, LX/5SM;->h:Z

    .line 917876
    iget-boolean v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->i:Z

    move v0, v0

    .line 917877
    iput-boolean v0, p0, LX/5SM;->i:Z

    .line 917878
    iget-boolean v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->j:Z

    move v0, v0

    .line 917879
    iput-boolean v0, p0, LX/5SM;->j:Z

    .line 917880
    iget v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->k:I

    move v0, v0

    .line 917881
    iput v0, p0, LX/5SM;->k:I

    .line 917882
    iget v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->l:I

    move v0, v0

    .line 917883
    iput v0, p0, LX/5SM;->l:I

    .line 917884
    iget v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->n:I

    move v0, v0

    .line 917885
    iput v0, p0, LX/5SM;->m:I

    .line 917886
    iget v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->o:I

    move v0, v0

    .line 917887
    iput v0, p0, LX/5SM;->n:I

    .line 917888
    iget v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->m:I

    move v0, v0

    .line 917889
    iput v0, p0, LX/5SM;->o:I

    .line 917890
    iget-object v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->p:Ljava/lang/String;

    move-object v0, v0

    .line 917891
    iput-object v0, p0, LX/5SM;->p:Ljava/lang/String;

    .line 917892
    iget-object v0, p1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->q:Ljava/lang/String;

    move-object v0, v0

    .line 917893
    iput-object v0, p0, LX/5SM;->q:Ljava/lang/String;

    .line 917894
    return-void
.end method


# virtual methods
.method public final a(LX/5SK;)LX/5SM;
    .locals 0

    .prologue
    .line 917895
    if-eqz p1, :cond_0

    .line 917896
    iput-object p1, p0, LX/5SM;->a:LX/5SK;

    .line 917897
    :cond_0
    return-object p0
.end method

.method public final a()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;
    .locals 2

    .prologue
    .line 917898
    new-instance v0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;-><init>(LX/5SM;)V

    return-object v0
.end method
