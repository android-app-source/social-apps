.class public final LX/6DL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6DJ;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/6DM;


# direct methods
.method public constructor <init>(LX/6DM;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1064835
    iput-object p1, p0, LX/6DL;->c:LX/6DM;

    iput-object p2, p0, LX/6DL;->a:Ljava/lang/String;

    iput-object p3, p0, LX/6DL;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/NameAutofillData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1064836
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1064837
    iget-object v0, p0, LX/6DL;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/browserextensions/ipc/autofill/NameAutofillData;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1064838
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1064839
    :cond_0
    :goto_0
    iget-object v0, p0, LX/6DL;->b:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, 0x3d50cc5b

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1064840
    return-void

    .line 1064841
    :cond_1
    iget-object v0, p0, LX/6DL;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/browserextensions/ipc/autofill/TelephoneAutofillData;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1064842
    invoke-interface {v1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1064843
    :cond_2
    iget-object v0, p0, LX/6DL;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/browserextensions/ipc/autofill/AddressAutofillData;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1064844
    invoke-interface {v1, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1064845
    :cond_3
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;

    .line 1064846
    invoke-virtual {v0}, Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;->f()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/6DL;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1064847
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
