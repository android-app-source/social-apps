.class public final LX/5ap;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 956216
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 956217
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956218
    :goto_0
    return v1

    .line 956219
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956220
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 956221
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 956222
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956223
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 956224
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 956225
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 956226
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 956227
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 956228
    invoke-static {p0, p1}, LX/5ak;->a(LX/15w;LX/186;)I

    move-result v3

    .line 956229
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 956230
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 956231
    goto :goto_1

    .line 956232
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 956233
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 956234
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_a

    .line 956235
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956236
    :goto_3
    move v0, v3

    .line 956237
    goto :goto_1

    .line 956238
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 956239
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 956240
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 956241
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 956242
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_8

    .line 956243
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 956244
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956245
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_6

    if-eqz v6, :cond_6

    .line 956246
    const-string v7, "has_previous_page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 956247
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    goto :goto_4

    .line 956248
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 956249
    :cond_8
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 956250
    if-eqz v0, :cond_9

    .line 956251
    invoke-virtual {p1, v3, v5}, LX/186;->a(IZ)V

    .line 956252
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_a
    move v0, v3

    move v5, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 956253
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956254
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956255
    if-eqz v0, :cond_1

    .line 956256
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956257
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 956258
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 956259
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5ak;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 956260
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 956261
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 956262
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 956263
    if-eqz v0, :cond_3

    .line 956264
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956265
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956266
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 956267
    if-eqz v1, :cond_2

    .line 956268
    const-string v2, "has_previous_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956269
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 956270
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956271
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956272
    return-void
.end method
