.class public final LX/6Eb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6D6;

.field public final synthetic b:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

.field public final synthetic c:LX/6Ee;


# direct methods
.method public constructor <init>(LX/6Ee;LX/6D6;Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;)V
    .locals 0

    .prologue
    .line 1066351
    iput-object p1, p0, LX/6Eb;->c:LX/6Ee;

    iput-object p2, p0, LX/6Eb;->a:LX/6D6;

    iput-object p3, p0, LX/6Eb;->b:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1066349
    iget-object v0, p0, LX/6Eb;->c:LX/6Ee;

    invoke-static {v0, p1}, LX/6Ee;->a$redex0(LX/6Ee;Ljava/lang/Throwable;)V

    .line 1066350
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 1066289
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1066290
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Result;

    .line 1066291
    if-eqz v0, :cond_1

    .line 1066292
    iget-object v1, v0, Lcom/facebook/browserextensions/common/identity/QueryPermissionsMethod$Result;->a:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 1066293
    :goto_0
    iget-object v1, p0, LX/6Eb;->a:LX/6D6;

    invoke-virtual {v1}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1066294
    :try_start_0
    iget-object v0, p0, LX/6Eb;->c:LX/6Ee;

    iget-object v1, p0, LX/6Eb;->b:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    iget-object v2, p0, LX/6Eb;->a:LX/6D6;

    .line 1066295
    iget-object v4, v0, LX/6Ee;->g:LX/6EV;

    invoke-virtual {v1}, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;->h()Ljava/lang/String;

    move-result-object v5

    .line 1066296
    iget-object v7, v4, LX/6EV;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/6D7;->c:LX/0Tn;

    invoke-virtual {v6, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v6

    check-cast v6, LX/0Tn;

    const/4 v8, 0x0

    invoke-interface {v7, v6, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v4, v6

    .line 1066297
    iget-object v5, v0, LX/6Ee;->g:LX/6EV;

    invoke-virtual {v1}, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;->h()Ljava/lang/String;

    move-result-object v6

    .line 1066298
    iget-object v8, v5, LX/6EV;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/6D7;->d:LX/0Tn;

    invoke-virtual {v7, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v7

    check-cast v7, LX/0Tn;

    const-string v9, "0"

    invoke-interface {v8, v7, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    move v5, v7

    .line 1066299
    new-instance v6, LX/6Ec;

    invoke-direct {v6, v0, v2}, LX/6Ec;-><init>(LX/6Ee;LX/6D6;)V

    .line 1066300
    sget-object v7, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    if-eq v2, v7, :cond_0

    .line 1066301
    sget-object v7, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    invoke-virtual {v7}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1066302
    :cond_0
    if-eqz v4, :cond_8

    int-to-long v8, v5

    iget-object v5, v0, LX/6Ee;->c:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    cmp-long v5, v8, v10

    if-lez v5, :cond_8

    .line 1066303
    iget-object v5, v0, LX/6Ee;->g:LX/6EV;

    invoke-virtual {v5, v4, v6}, LX/6EV;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1066304
    :goto_1
    move-object v0, v4

    .line 1066305
    new-instance v1, LX/6Ea;

    invoke-direct {v1, p0}, LX/6Ea;-><init>(LX/6Eb;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1066306
    :goto_2
    return-void

    .line 1066307
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1066308
    :catch_0
    move-exception v0

    .line 1066309
    iget-object v1, p0, LX/6Eb;->c:LX/6Ee;

    invoke-static {v1, v0}, LX/6Ee;->a$redex0(LX/6Ee;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1066310
    :cond_2
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_3
    const/4 v0, 0x1

    .line 1066311
    :goto_3
    iget-object v1, p0, LX/6Eb;->c:LX/6Ee;

    iget-object v2, p0, LX/6Eb;->a:LX/6D6;

    .line 1066312
    new-instance v3, LX/6Ed;

    invoke-direct {v3, v1, v2}, LX/6Ed;-><init>(LX/6Ee;LX/6D6;)V

    .line 1066313
    if-eqz v0, :cond_4

    .line 1066314
    sget-object v4, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    invoke-virtual {v4}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1066315
    sget-object v4, LX/6D6;->PUBLIC_PROFILE:LX/6D6;

    invoke-virtual {v4}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1066316
    :cond_4
    move-object v1, v3

    .line 1066317
    iget-object v2, p0, LX/6Eb;->c:LX/6Ee;

    iget-object v3, p0, LX/6Eb;->b:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    .line 1066318
    if-eqz v0, :cond_9

    .line 1066319
    new-instance v4, LX/6EL;

    iget-object v5, v2, LX/6Ee;->b:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/6EL;-><init>(Landroid/content/Context;)V

    .line 1066320
    :goto_4
    invoke-virtual {v3}, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;->h()Ljava/lang/String;

    move-result-object v5

    .line 1066321
    iput-object v5, v4, LX/6EK;->c:Ljava/lang/String;

    .line 1066322
    move-object v5, v4

    .line 1066323
    const-string v6, "JS_BRIDGE_APP_NAME"

    invoke-virtual {v3, v6}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object v6, v6

    .line 1066324
    iput-object v6, v5, LX/6EK;->d:Ljava/lang/String;

    .line 1066325
    move-object v5, v5

    .line 1066326
    iput-object v1, v5, LX/6EK;->e:Ljava/util/ArrayList;

    .line 1066327
    move-object v5, v5

    .line 1066328
    iget-object v6, v3, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v6, v6

    .line 1066329
    iput-object v6, v5, LX/6EK;->f:Ljava/lang/String;

    .line 1066330
    invoke-virtual {v4}, LX/6EK;->b()Landroid/content/Intent;

    move-result-object v4

    .line 1066331
    const/high16 v5, 0x10000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1066332
    move-object v0, v4

    .line 1066333
    iget-object v2, p0, LX/6Eb;->c:LX/6Ee;

    iget-object v2, v2, LX/6Ee;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/6Eb;->c:LX/6Ee;

    iget-object v3, v3, LX/6Ee;->b:Landroid/content/Context;

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1066334
    iget-object v0, p0, LX/6Eb;->c:LX/6Ee;

    iget-object v2, p0, LX/6Eb;->c:LX/6Ee;

    iget-object v2, v2, LX/6Ee;->j:LX/6D0;

    iget-object v3, p0, LX/6Eb;->b:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    .line 1066335
    iget-object v4, v3, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v3, v4

    .line 1066336
    invoke-virtual {v2, v3}, LX/6D0;->a(Landroid/os/Bundle;)LX/6Cz;

    move-result-object v2

    .line 1066337
    iput-object v2, v0, LX/6Ee;->l:LX/6Cz;

    .line 1066338
    iget-object v0, p0, LX/6Eb;->c:LX/6Ee;

    iget-object v0, v0, LX/6Ee;->l:LX/6Cz;

    iget-object v2, p0, LX/6Eb;->b:Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;

    .line 1066339
    iget-object v3, v0, LX/6Cz;->g:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6CG;

    .line 1066340
    iget-object v5, v2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v5, v5

    .line 1066341
    invoke-interface {v3, v5}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1066342
    invoke-interface {v3, v1, v2}, LX/6CG;->a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    goto :goto_5

    .line 1066343
    :cond_6
    goto/16 :goto_2

    .line 1066344
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 1066345
    :cond_8
    :try_start_1
    iget-object v4, v0, LX/6Ee;->e:LX/6EF;

    invoke-virtual {v1}, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;->h()Ljava/lang/String;

    move-result-object v5

    .line 1066346
    iget-object v7, v1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v7, v7

    .line 1066347
    invoke-virtual {v4, v5, v6, v7}, LX/6EF;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto/16 :goto_1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1066348
    :cond_9
    new-instance v4, LX/6EZ;

    iget-object v5, v2, LX/6Ee;->b:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/6EZ;-><init>(Landroid/content/Context;)V

    goto/16 :goto_4
.end method
