.class public LX/6dC;
.super LX/2Rg;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:LX/6cp;

.field private static final c:Ljava/lang/Object;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1115516
    invoke-static {}, LX/6cp;->newBuilder()LX/6cn;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "messages"

    const-string v3, "_ROWID_"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "thread_key"

    const-string v2, "messages"

    const-string v3, "thread_key"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "msg_id"

    const-string v2, "messages"

    const-string v3, "msg_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "action_id"

    const-string v2, "messages"

    const-string v3, "action_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "text"

    const-string v2, "messages"

    const-string v3, "text"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "sender"

    const-string v2, "messages"

    const-string v3, "sender"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "is_not_forwardable"

    const-string v2, "messages"

    const-string v3, "is_not_forwardable"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "timestamp_ms"

    const-string v2, "messages"

    const-string v3, "timestamp_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "timestamp_sent_ms"

    const-string v2, "messages"

    const-string v3, "timestamp_sent_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "msg_type"

    const-string v2, "messages"

    const-string v3, "msg_type"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "affected_users"

    const-string v2, "messages"

    const-string v3, "affected_users"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "attachments"

    const-string v2, "messages"

    const-string v3, "attachments"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "shares"

    const-string v2, "messages"

    const-string v3, "shares"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "sticker_id"

    const-string v2, "messages"

    const-string v3, "sticker_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "offline_threading_id"

    const-string v2, "messages"

    const-string v3, "offline_threading_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "source"

    const-string v2, "messages"

    const-string v3, "source"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "channel_source"

    const-string v2, "messages"

    const-string v3, "channel_source"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "is_non_authoritative"

    const-string v2, "messages"

    const-string v3, "is_non_authoritative"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "pending_send_media_attachment"

    const-string v2, "messages"

    const-string v3, "pending_send_media_attachment"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "sent_share_attachment"

    const-string v2, "messages"

    const-string v3, "sent_share_attachment"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "client_tags"

    const-string v2, "messages"

    const-string v3, "client_tags"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "send_error"

    const-string v2, "messages"

    const-string v3, "send_error"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "send_error_message"

    const-string v2, "messages"

    const-string v3, "send_error_message"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "send_error_number"

    const-string v2, "messages"

    const-string v3, "send_error_number"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "send_error_timestamp_ms"

    const-string v2, "messages"

    const-string v3, "send_error_timestamp_ms"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "send_error_error_url"

    const-string v2, "messages"

    const-string v3, "send_error_error_url"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "send_channel"

    const-string v2, "messages"

    const-string v3, "send_channel"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "publicity"

    const-string v2, "messages"

    const-string v3, "publicity"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "send_queue_type"

    const-string v2, "messages"

    const-string v3, "send_queue_type"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "payment_transaction"

    const-string v2, "messages"

    const-string v3, "payment_transaction"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "payment_request"

    const-string v2, "messages"

    const-string v3, "payment_request"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "has_unavailable_attachment"

    const-string v2, "messages"

    const-string v3, "has_unavailable_attachment"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "app_attribution"

    const-string v2, "messages"

    const-string v3, "app_attribution"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "content_app_attribution"

    const-string v2, "messages"

    const-string v3, "content_app_attribution"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "xma"

    const-string v2, "messages"

    const-string v3, "xma"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_type"

    const-string v2, "messages"

    const-string v3, "admin_text_type"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_theme_color"

    const-string v2, "messages"

    const-string v3, "admin_text_theme_color"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_icon_emoji"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_icon_emoji"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_nickname"

    const-string v2, "messages"

    const-string v3, "admin_text_nickname"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_target_id"

    const-string v2, "messages"

    const-string v3, "admin_text_target_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_message_lifetime"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_message_lifetime"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_journey_color_choices"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_journey_color_choices"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_journey_emoji_choices"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_journey_emoji_choices"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_journey_nickname_choices"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_journey_nickname_choices"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_journey_bot_choices"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_journey_bot_choices"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "message_lifetime"

    const-string v2, "messages"

    const-string v3, "message_lifetime"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_rtc_event"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_rtc_event"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_rtc_server_info_data"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_rtc_server_info_data"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_rtc_is_video_call"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_rtc_is_video_call"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_ride_provider_name"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_ride_provider_name"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "is_sponsored"

    const-string v2, "messages"

    const-string v3, "is_sponsored"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_ad_properties"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_ad_properties"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_game_score_data"

    const-string v2, "messages"

    const-string v3, "admin_text_game_score_data"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_thread_event_reminder_properties"

    const-string v2, "messages"

    const-string v3, "admin_text_thread_event_reminder_properties"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "commerce_message_type"

    const-string v2, "messages"

    const-string v3, "commerce_message_type"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "customizations"

    const-string v2, "messages"

    const-string v3, "customizations"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_joinable_event_type"

    const-string v2, "messages"

    sget-object v3, LX/6df;->ae:LX/0U1;

    .line 1115517
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1115518
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "metadata_at_text_ranges"

    const-string v2, "messages"

    const-string v3, "metadata_at_text_ranges"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "platform_metadata"

    const-string v2, "messages"

    const-string v3, "platform_metadata"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_is_joinable_promo"

    const-string v2, "messages"

    sget-object v3, LX/6df;->ah:LX/0U1;

    .line 1115519
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1115520
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_agent_intent_id"

    const-string v2, "messages"

    const-string v3, "admin_text_agent_intent_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "montage_reply_message_id"

    const-string v2, "messages"

    const-string v3, "montage_reply_message_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "admin_text_booking_request_id"

    const-string v2, "messages"

    const-string v3, "admin_text_booking_request_id"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "generic_admin_message_extensible_data"

    const-string v2, "messages"

    const-string v3, "generic_admin_message_extensible_data"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "folder"

    const-string v2, "threads"

    sget-object v3, LX/6dq;->w:LX/0U1;

    .line 1115521
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 1115522
    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "reactions"

    const-string v2, "message_reactions"

    const-string v3, "reactions"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    const-string v1, "profile_ranges"

    const-string v2, "messages"

    const-string v3, "profile_ranges"

    invoke-virtual {v0, v1, v2, v3}, LX/6cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/6cn;

    move-result-object v0

    invoke-virtual {v0}, LX/6cn;->a()LX/6cp;

    move-result-object v0

    sput-object v0, LX/6dC;->a:LX/6cp;

    .line 1115523
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6dC;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1115524
    invoke-direct {p0}, LX/2Rg;-><init>()V

    .line 1115525
    iput-object p1, p0, LX/6dC;->b:LX/0Or;

    .line 1115526
    return-void
.end method

.method public static a(LX/0QB;)LX/6dC;
    .locals 7

    .prologue
    .line 1115527
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1115528
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1115529
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1115530
    if-nez v1, :cond_0

    .line 1115531
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1115532
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1115533
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1115534
    sget-object v1, LX/6dC;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1115535
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1115536
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1115537
    :cond_1
    if-nez v1, :cond_4

    .line 1115538
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1115539
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1115540
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1115541
    new-instance v1, LX/6dC;

    const/16 p0, 0x274b

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v1, p0}, LX/6dC;-><init>(LX/0Or;)V

    .line 1115542
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1115543
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1115544
    if-nez v1, :cond_2

    .line 1115545
    sget-object v0, LX/6dC;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dC;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1115546
    :goto_1
    if-eqz v0, :cond_3

    .line 1115547
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1115548
    :goto_3
    check-cast v0, LX/6dC;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1115549
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1115550
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1115551
    :catchall_1
    move-exception v0

    .line 1115552
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1115553
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1115554
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1115555
    :cond_2
    :try_start_8
    sget-object v0, LX/6dC;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dC;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1115556
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0RA;->c(Ljava/lang/Iterable;)Ljava/util/LinkedHashSet;

    move-result-object v2

    .line 1115557
    invoke-static {p1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1115558
    invoke-static {p2}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1115559
    sget-object v0, LX/6dC;->a:LX/6cp;

    invoke-virtual {v0}, LX/6cp;->a()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1115560
    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1115561
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1115562
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1115563
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1115564
    const-string v0, "m._ROWID_ AS _id"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115565
    const-string v0, "messages AS m"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115566
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1115567
    const-string v7, "_id"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 1115568
    sget-object v7, LX/6dC;->a:LX/6cp;

    invoke-virtual {v7, v0}, LX/6cp;->a(Ljava/lang/String;)LX/6co;

    move-result-object v7

    .line 1115569
    if-nez v7, :cond_3

    .line 1115570
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown field: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1115571
    :cond_3
    const-string v0, "messages"

    iget-object v8, v7, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1115572
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, ", m."

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v7, LX/6co;->c:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " AS "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v7, v7, LX/6co;->a:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1115573
    :cond_4
    const-string v0, "threads"

    iget-object v8, v7, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1115574
    if-nez v2, :cond_9

    .line 1115575
    const-string v0, " INNER JOIN "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115576
    const-string v0, "threads"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115577
    const-string v0, " AS t ON m."

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115578
    sget-object v0, LX/6df;->b:LX/0U1;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1115579
    const-string v0, "= t."

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115580
    sget-object v0, LX/6dq;->a:LX/0U1;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v0, v3

    .line 1115581
    :goto_2
    const-string v2, ", t."

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115582
    iget-object v2, v7, LX/6co;->c:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115583
    const-string v2, " AS "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115584
    iget-object v2, v7, LX/6co;->a:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v0

    goto/16 :goto_1

    .line 1115585
    :cond_5
    const-string v0, "message_reactions"

    iget-object v8, v7, LX/6co;->b:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1115586
    if-nez v1, :cond_6

    .line 1115587
    const-string v0, " LEFT JOIN (SELECT "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115588
    sget-object v0, LX/6cs;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115589
    const-string v0, ", GROUP_CONCAT(("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115590
    sget-object v0, LX/6cs;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115591
    const-string v0, " || \'"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115592
    const-string v0, "="

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115593
    const-string v0, "\' || "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115594
    sget-object v0, LX/6cs;->c:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115595
    const-string v0, "), \'"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115596
    const-string v0, ";"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115597
    const-string v0, "\') AS "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115598
    const-string v0, "reactions"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115599
    const-string v0, " FROM "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115600
    const-string v0, "message_reactions"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115601
    const-string v0, " GROUP BY "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115602
    sget-object v0, LX/6cs;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115603
    const-string v0, ") AS mr ON m."

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115604
    sget-object v0, LX/6df;->a:LX/0U1;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1115605
    const-string v0, "= mr."

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115606
    sget-object v0, LX/6cs;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v3

    .line 1115607
    :cond_6
    const-string v0, ", mr."

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115608
    iget-object v0, v7, LX/6co;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115609
    const-string v0, " AS "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1115610
    iget-object v0, v7, LX/6co;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    move v0, v1

    move v1, v0

    .line 1115611
    goto/16 :goto_1

    .line 1115612
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(SELECT "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_9
    move v0, v2

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 1115613
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1115614
    invoke-static {p2, p3, p5}, LX/6dC;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1115615
    iget-object v1, p0, LX/6dC;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dQ;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
