.class public final LX/5Gr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/model/GraphQLComment;

.field public e:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public k:Z

.field public l:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 889303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889304
    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5Gr;->h:Ljava/lang/String;

    .line 889305
    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5Gr;->i:Ljava/lang/String;

    .line 889306
    iput-boolean v1, p0, LX/5Gr;->k:Z

    .line 889307
    iput v1, p0, LX/5Gr;->l:I

    return-void
.end method


# virtual methods
.method public final a(I)LX/5Gr;
    .locals 0

    .prologue
    .line 889301
    iput p1, p0, LX/5Gr;->l:I

    .line 889302
    return-object p0
.end method

.method public final a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/5Gr;
    .locals 0

    .prologue
    .line 889293
    iput-object p1, p0, LX/5Gr;->e:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 889294
    return-object p0
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/5Gr;
    .locals 0
    .param p1    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 889299
    iput-object p1, p0, LX/5Gr;->j:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 889300
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)LX/5Gr;
    .locals 0

    .prologue
    .line 889297
    iput-object p1, p0, LX/5Gr;->d:Lcom/facebook/graphql/model/GraphQLComment;

    .line 889298
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/5Gr;
    .locals 0

    .prologue
    .line 889295
    iput-object p1, p0, LX/5Gr;->a:Ljava/lang/String;

    .line 889296
    return-object p0
.end method

.method public final a(Z)LX/5Gr;
    .locals 0

    .prologue
    .line 889308
    iput-boolean p1, p0, LX/5Gr;->k:Z

    .line 889309
    return-object p0
.end method

.method public final a()Lcom/facebook/api/ufiservices/common/AddCommentParams;
    .locals 1

    .prologue
    .line 889282
    iget-object v0, p0, LX/5Gr;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 889283
    iget-object v0, p0, LX/5Gr;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 889284
    iget-object v0, p0, LX/5Gr;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 889285
    iget-object v0, p0, LX/5Gr;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 889286
    new-instance v0, Lcom/facebook/api/ufiservices/common/AddCommentParams;

    invoke-direct {v0, p0}, Lcom/facebook/api/ufiservices/common/AddCommentParams;-><init>(LX/5Gr;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/5Gr;
    .locals 0

    .prologue
    .line 889278
    iput-object p1, p0, LX/5Gr;->b:Ljava/lang/String;

    .line 889279
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/5Gr;
    .locals 0

    .prologue
    .line 889280
    iput-object p1, p0, LX/5Gr;->c:Ljava/lang/String;

    .line 889281
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/5Gr;
    .locals 0

    .prologue
    .line 889287
    iput-object p1, p0, LX/5Gr;->f:Ljava/lang/String;

    .line 889288
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/5Gr;
    .locals 0

    .prologue
    .line 889289
    iput-object p1, p0, LX/5Gr;->i:Ljava/lang/String;

    .line 889290
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/5Gr;
    .locals 0

    .prologue
    .line 889291
    iput-object p1, p0, LX/5Gr;->h:Ljava/lang/String;

    .line 889292
    return-object p0
.end method
