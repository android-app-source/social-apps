.class public LX/6LN;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/6LJ;

.field private final c:LX/508;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/508",
            "<",
            "Lcom/facebook/common/banner/BannerNotification;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/common/banner/BannerNotification;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/6LO;

.field private f:LX/6LM;

.field private g:LX/6LL;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Landroid/view/ViewGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1078369
    const-class v0, LX/6LN;

    sput-object v0, LX/6LN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/6LJ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1078372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078373
    iput-object p1, p0, LX/6LN;->b:LX/6LJ;

    .line 1078374
    new-instance v0, LX/6LK;

    invoke-direct {v0, p0}, LX/6LK;-><init>(LX/6LN;)V

    invoke-static {v0}, LX/508;->a(Ljava/util/Comparator;)LX/504;

    move-result-object v0

    invoke-virtual {v0}, LX/504;->a()LX/508;

    move-result-object v0

    iput-object v0, p0, LX/6LN;->c:LX/508;

    .line 1078375
    sget-object v0, LX/6LM;->UNKNOWN:LX/6LM;

    iput-object v0, p0, LX/6LN;->f:LX/6LM;

    .line 1078376
    return-void
.end method

.method public static b(LX/0QB;)LX/6LN;
    .locals 2

    .prologue
    .line 1078370
    new-instance v1, LX/6LN;

    invoke-static {p0}, LX/6LJ;->b(LX/0QB;)LX/6LJ;

    move-result-object v0

    check-cast v0, LX/6LJ;

    invoke-direct {v1, v0}, LX/6LN;-><init>(LX/6LJ;)V

    .line 1078371
    return-object v1
.end method

.method private c(LX/6LI;)V
    .locals 1

    .prologue
    .line 1078365
    iget-object v0, p0, LX/6LN;->c:LX/508;

    invoke-virtual {v0, p1}, LX/508;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1078366
    iget-object v0, p0, LX/6LN;->c:LX/508;

    invoke-virtual {v0, p1}, LX/508;->add(Ljava/lang/Object;)Z

    .line 1078367
    :cond_0
    return-void
.end method

.method private d(LX/6LI;)V
    .locals 4

    .prologue
    .line 1078349
    iget-object v0, p0, LX/6LN;->h:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, LX/6LI;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1078350
    iget-object v1, p0, LX/6LN;->h:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    .line 1078351
    :goto_0
    iget-object v0, p0, LX/6LN;->b:LX/6LJ;

    const-string v1, "view"

    const-string v2, "view"

    invoke-virtual {p1}, LX/6LI;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/6LJ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1078352
    return-void

    .line 1078353
    :cond_0
    iget-object v1, p0, LX/6LN;->i:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1078354
    iget-object v1, p0, LX/6LN;->h:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/6LN;->i:Landroid/view/View;

    invoke-static {v1, v2, v0}, LX/478;->b(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    .line 1078355
    :cond_1
    :goto_1
    iput-object v0, p0, LX/6LN;->i:Landroid/view/View;

    goto :goto_0

    .line 1078356
    :cond_2
    iget-object v1, p0, LX/6LN;->h:Landroid/view/ViewGroup;

    const v2, 0x7f0d017c

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1078357
    if-eqz v1, :cond_1

    .line 1078358
    iget-object v1, p0, LX/6LN;->h:Landroid/view/ViewGroup;

    const v2, 0x7f0d017c

    .line 1078359
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1078360
    invoke-static {v1, v3, v0}, LX/478;->b(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    .line 1078361
    goto :goto_1
.end method

.method private e(LX/6LI;)I
    .locals 2

    .prologue
    .line 1078368
    iget-object v0, p0, LX/6LN;->e:LX/6LO;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, LX/6LO;->a(Ljava/lang/Class;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1078362
    iget-object v0, p0, LX/6LN;->d:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6LI;

    .line 1078363
    invoke-virtual {v0}, LX/6LI;->b()V

    goto :goto_0

    .line 1078364
    :cond_0
    return-void
.end method

.method public final a(LX/6LI;)V
    .locals 5

    .prologue
    .line 1078342
    iget-object v0, p0, LX/6LN;->d:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "%s must be registered before being shown."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1078343
    iget-object v0, p0, LX/6LN;->c:LX/508;

    invoke-virtual {v0}, LX/508;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, LX/6LN;->e(LX/6LI;)I

    move-result v1

    iget-object v0, p0, LX/6LN;->c:LX/508;

    invoke-virtual {v0}, LX/508;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6LI;

    invoke-direct {p0, v0}, LX/6LN;->e(LX/6LI;)I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 1078344
    :cond_0
    iget-object v0, p0, LX/6LN;->h:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 1078345
    :goto_0
    return-void

    .line 1078346
    :cond_1
    invoke-direct {p0, p1}, LX/6LN;->c(LX/6LI;)V

    .line 1078347
    invoke-direct {p0, p1}, LX/6LN;->d(LX/6LI;)V

    goto :goto_0

    .line 1078348
    :cond_2
    invoke-direct {p0, p1}, LX/6LN;->c(LX/6LI;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;LX/6LO;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/common/banner/BannerNotification;",
            ">;",
            "LX/6LO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1078336
    iput-object p2, p0, LX/6LN;->e:LX/6LO;

    .line 1078337
    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/6LN;->d:LX/0Rf;

    .line 1078338
    iget-object v0, p0, LX/6LN;->d:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6LI;

    .line 1078339
    iput-object p0, v0, LX/6LI;->a:LX/6LN;

    .line 1078340
    goto :goto_0

    .line 1078341
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1078333
    iget-object v0, p0, LX/6LN;->d:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6LI;

    .line 1078334
    invoke-virtual {v0}, LX/6LI;->c()V

    goto :goto_0

    .line 1078335
    :cond_0
    return-void
.end method

.method public final b(LX/6LI;)V
    .locals 2

    .prologue
    .line 1078325
    iget-object v0, p0, LX/6LN;->c:LX/508;

    invoke-virtual {v0}, LX/508;->b()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_2

    .line 1078326
    iget-object v0, p0, LX/6LN;->c:LX/508;

    invoke-virtual {v0, p1}, LX/508;->remove(Ljava/lang/Object;)Z

    .line 1078327
    iget-object v0, p0, LX/6LN;->c:LX/508;

    invoke-virtual {v0}, LX/508;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1078328
    iget-object v0, p0, LX/6LN;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1078329
    iget-object v0, p0, LX/6LN;->g:LX/6LL;

    if-eqz v0, :cond_0

    .line 1078330
    :cond_0
    :goto_0
    return-void

    .line 1078331
    :cond_1
    iget-object v0, p0, LX/6LN;->c:LX/508;

    invoke-virtual {v0}, LX/508;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6LI;

    invoke-direct {p0, v0}, LX/6LN;->d(LX/6LI;)V

    goto :goto_0

    .line 1078332
    :cond_2
    iget-object v0, p0, LX/6LN;->c:LX/508;

    invoke-virtual {v0, p1}, LX/508;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
