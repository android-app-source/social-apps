.class public final LX/64y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/650;

.field public final b:LX/64w;

.field public final c:LX/66R;

.field private d:Z


# direct methods
.method public constructor <init>(LX/64w;LX/650;)V
    .locals 1

    .prologue
    .line 1046265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046266
    iput-object p1, p0, LX/64y;->b:LX/64w;

    .line 1046267
    iput-object p2, p0, LX/64y;->a:LX/650;

    .line 1046268
    new-instance v0, LX/66R;

    invoke-direct {v0, p1}, LX/66R;-><init>(LX/64w;)V

    iput-object v0, p0, LX/64y;->c:LX/66R;

    .line 1046269
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1046270
    iget-object v0, p0, LX/64y;->c:LX/66R;

    .line 1046271
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/66R;->d:Z

    .line 1046272
    iget-object p0, v0, LX/66R;->b:LX/65W;

    .line 1046273
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/65W;->e()V

    .line 1046274
    :cond_0
    return-void
.end method

.method public final a(LX/64X;)V
    .locals 3

    .prologue
    .line 1046275
    monitor-enter p0

    .line 1046276
    :try_start_0
    iget-boolean v0, p0, LX/64y;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already Executed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046277
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1046278
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/64y;->d:Z

    .line 1046279
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1046280
    iget-object v0, p0, LX/64y;->b:LX/64w;

    .line 1046281
    iget-object v1, v0, LX/64w;->a:LX/64i;

    move-object v0, v1

    .line 1046282
    new-instance v1, Lokhttp3/RealCall$AsyncCall;

    invoke-direct {v1, p0, p1}, Lokhttp3/RealCall$AsyncCall;-><init>(LX/64y;LX/64X;)V

    invoke-virtual {v0, v1}, LX/64i;->a(Lokhttp3/RealCall$AsyncCall;)V

    .line 1046283
    return-void
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 1046284
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/64y;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already Executed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1046285
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/64y;->c:LX/66R;

    const/4 v1, 0x1

    .line 1046286
    iput-boolean v1, v0, LX/66R;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1046287
    monitor-exit p0

    return-void
.end method

.method public final e()LX/64q;
    .locals 2

    .prologue
    .line 1046288
    iget-object v0, p0, LX/64y;->a:LX/650;

    .line 1046289
    iget-object v1, v0, LX/650;->a:LX/64q;

    move-object v0, v1

    .line 1046290
    const-string v1, "/..."

    invoke-virtual {v0, v1}, LX/64q;->c(Ljava/lang/String;)LX/64q;

    move-result-object v0

    return-object v0
.end method
