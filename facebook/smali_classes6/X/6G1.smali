.class public final LX/6G1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6G0;


# instance fields
.field private final a:LX/6G0;

.field private final b:J


# direct methods
.method public constructor <init>(LX/6G0;J)V
    .locals 0

    .prologue
    .line 1069525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1069526
    iput-object p1, p0, LX/6G1;->a:LX/6G0;

    .line 1069527
    iput-wide p2, p0, LX/6G1;->b:J

    .line 1069528
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1069529
    iget-object v0, p0, LX/6G1;->a:LX/6G0;

    invoke-interface {v0}, LX/6G0;->a()LX/0Px;

    move-result-object v1

    .line 1069530
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    .line 1069531
    iget-wide v8, v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->c:J

    move-wide v4, v8

    .line 1069532
    iget-wide v6, p0, LX/6G1;->b:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 1069533
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1069534
    :goto_1
    return-object v0

    .line 1069535
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 1069536
    goto :goto_1
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/bugreporter/activity/chooser/ChooserOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1069537
    iget-object v0, p0, LX/6G1;->a:LX/6G0;

    invoke-interface {v0}, LX/6G0;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
