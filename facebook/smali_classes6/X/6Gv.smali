.class public final LX/6Gv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Landroid/graphics/Bitmap;",
        "LX/6H0;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1071043
    iput-object p1, p0, LX/6Gv;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    iput-object p2, p0, LX/6Gv;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)LX/6H0;
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1071044
    if-nez p1, :cond_0

    .line 1071045
    iget-object v0, p0, LX/6Gv;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    const v1, 0x7f081930

    .line 1071046
    invoke-static {v0, v1}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a$redex0(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;I)V

    .line 1071047
    const/4 v0, 0x0

    .line 1071048
    :goto_0
    return-object v0

    .line 1071049
    :cond_0
    new-instance v0, LX/6H0;

    iget-object v1, p0, LX/6Gv;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6H0;-><init>(Landroid/content/Context;)V

    .line 1071050
    iget-object v1, v0, LX/6H0;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1071051
    new-instance v1, LX/6Gt;

    invoke-direct {v1, p0}, LX/6Gt;-><init>(LX/6Gv;)V

    .line 1071052
    iget-object p1, v0, LX/6H0;->b:Landroid/widget/ImageView;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1071053
    new-instance v1, LX/6Gu;

    invoke-direct {v1, p0}, LX/6Gu;-><init>(LX/6Gv;)V

    invoke-virtual {v0, v1}, LX/6H0;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1071054
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-direct {p0, p1}, LX/6Gv;->a(Landroid/graphics/Bitmap;)LX/6H0;

    move-result-object v0

    return-object v0
.end method
