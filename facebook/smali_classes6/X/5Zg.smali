.class public final LX/5Zg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$AttachmentPropertiesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$SourceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 952504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;
    .locals 17

    .prologue
    .line 952505
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 952506
    move-object/from16 v0, p0

    iget-object v2, v0, LX/5Zg;->a:LX/0Px;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 952507
    move-object/from16 v0, p0

    iget-object v3, v0, LX/5Zg;->b:LX/0Px;

    invoke-static {v1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 952508
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5Zg;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 952509
    move-object/from16 v0, p0

    iget-object v5, v0, LX/5Zg;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;

    invoke-static {v1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 952510
    move-object/from16 v0, p0

    iget-object v6, v0, LX/5Zg;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    invoke-static {v1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 952511
    move-object/from16 v0, p0

    iget-object v7, v0, LX/5Zg;->f:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 952512
    move-object/from16 v0, p0

    iget-object v8, v0, LX/5Zg;->g:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$SourceModel;

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 952513
    move-object/from16 v0, p0

    iget-object v9, v0, LX/5Zg;->h:LX/0Px;

    invoke-virtual {v1, v9}, LX/186;->c(Ljava/util/List;)I

    move-result v9

    .line 952514
    move-object/from16 v0, p0

    iget-object v10, v0, LX/5Zg;->i:LX/0Px;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 952515
    move-object/from16 v0, p0

    iget-object v11, v0, LX/5Zg;->j:Ljava/lang/String;

    invoke-virtual {v1, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 952516
    move-object/from16 v0, p0

    iget-object v12, v0, LX/5Zg;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 952517
    move-object/from16 v0, p0

    iget-object v13, v0, LX/5Zg;->l:Ljava/lang/String;

    invoke-virtual {v1, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 952518
    move-object/from16 v0, p0

    iget-object v14, v0, LX/5Zg;->m:Ljava/lang/String;

    invoke-virtual {v1, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 952519
    move-object/from16 v0, p0

    iget-object v15, v0, LX/5Zg;->n:Ljava/lang/String;

    invoke-virtual {v1, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 952520
    const/16 v16, 0xe

    move/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 952521
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 952522
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 952523
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 952524
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 952525
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 952526
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 952527
    const/4 v2, 0x6

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 952528
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 952529
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 952530
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 952531
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 952532
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 952533
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 952534
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 952535
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 952536
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 952537
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 952538
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 952539
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 952540
    new-instance v2, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    invoke-direct {v2, v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;-><init>(LX/15i;)V

    .line 952541
    return-object v2
.end method
