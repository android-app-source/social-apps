.class public final LX/6Ci;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteMutationFragmentsModels$MessengerExtensionFavoriteMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Cj;

.field public final synthetic b:LX/6Ck;


# direct methods
.method public constructor <init>(LX/6Ck;LX/6Cj;)V
    .locals 0

    .prologue
    .line 1064287
    iput-object p1, p0, LX/6Ci;->b:LX/6Ck;

    iput-object p2, p0, LX/6Ci;->a:LX/6Cj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1064288
    iget-object v0, p0, LX/6Ci;->a:LX/6Cj;

    invoke-interface {v0}, LX/6Cj;->b()V

    .line 1064289
    iget-object v0, p0, LX/6Ci;->b:LX/6Ck;

    iget-object v0, v0, LX/6Ck;->a:LX/03V;

    const-string v1, "MessengerExtensionFavoriteManager"

    const-string v2, "Extension favorite mutation failed."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064290
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1064285
    iget-object v0, p0, LX/6Ci;->a:LX/6Cj;

    invoke-interface {v0}, LX/6Cj;->a()V

    .line 1064286
    return-void
.end method
