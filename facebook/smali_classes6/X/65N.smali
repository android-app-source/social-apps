.class public final LX/65N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/io/Flushable;


# static fields
.field public static final a:Ljava/util/regex/Pattern;

.field public static final synthetic b:Z

.field private static final p:LX/65J;


# instance fields
.field public final c:LX/66U;

.field private d:J

.field public final e:I

.field public f:J

.field public g:LX/670;

.field public final h:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/65M;",
            ">;"
        }
    .end annotation
.end field

.field public i:I

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:J

.field public final n:Ljava/util/concurrent/Executor;

.field public final o:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1047056
    const-class v0, LX/65N;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/65N;->b:Z

    .line 1047057
    const-string v0, "[a-z0-9_-]{1,120}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/65N;->a:Ljava/util/regex/Pattern;

    .line 1047058
    new-instance v0, LX/65K;

    invoke-direct {v0}, LX/65K;-><init>()V

    sput-object v0, LX/65N;->p:LX/65J;

    return-void

    .line 1047059
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/65N;)Z
    .locals 2

    .prologue
    .line 1047053
    iget v0, p0, LX/65N;->i:I

    const/16 v1, 0x7d0

    if-lt v0, v1, :cond_0

    iget v0, p0, LX/65N;->i:I

    iget-object v1, p0, LX/65N;->h:Ljava/util/LinkedHashMap;

    .line 1047054
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 1047055
    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(LX/65N;LX/65L;Z)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 1047008
    monitor-enter p0

    :try_start_0
    iget-object v2, p1, LX/65L;->b:LX/65M;

    .line 1047009
    iget-object v1, v2, LX/65M;->f:LX/65L;

    if-eq v1, p1, :cond_0

    .line 1047010
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047011
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1047012
    :cond_0
    if-eqz p2, :cond_4

    :try_start_1
    iget-boolean v1, v2, LX/65M;->e:Z

    if-nez v1, :cond_4

    move v1, v0

    .line 1047013
    :goto_0
    iget v3, p0, LX/65N;->e:I

    if-ge v1, v3, :cond_4

    .line 1047014
    iget-object v3, p1, LX/65L;->c:[Z

    aget-boolean v3, v3, v1

    if-nez v3, :cond_1

    .line 1047015
    invoke-virtual {p1}, LX/65L;->b()V

    .line 1047016
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Newly created entry didn\'t create value for index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047017
    :cond_1
    iget-object v3, p0, LX/65N;->c:LX/66U;

    iget-object v4, v2, LX/65M;->d:[Ljava/io/File;

    aget-object v4, v4, v1

    invoke-interface {v3, v4}, LX/66U;->b(Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1047018
    invoke-virtual {p1}, LX/65L;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1047019
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    .line 1047020
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1047021
    :cond_4
    :goto_2
    :try_start_2
    iget v1, p0, LX/65N;->e:I

    if-ge v0, v1, :cond_7

    .line 1047022
    iget-object v1, v2, LX/65M;->d:[Ljava/io/File;

    aget-object v1, v1, v0

    .line 1047023
    if-eqz p2, :cond_6

    .line 1047024
    iget-object v3, p0, LX/65N;->c:LX/66U;

    invoke-interface {v3, v1}, LX/66U;->b(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1047025
    iget-object v3, v2, LX/65M;->c:[Ljava/io/File;

    aget-object v3, v3, v0

    .line 1047026
    iget-object v4, p0, LX/65N;->c:LX/66U;

    invoke-interface {v4, v1, v3}, LX/66U;->a(Ljava/io/File;Ljava/io/File;)V

    .line 1047027
    iget-object v1, v2, LX/65M;->b:[J

    aget-wide v4, v1, v0

    .line 1047028
    iget-object v1, p0, LX/65N;->c:LX/66U;

    invoke-interface {v1, v3}, LX/66U;->c(Ljava/io/File;)J

    move-result-wide v6

    .line 1047029
    iget-object v1, v2, LX/65M;->b:[J

    aput-wide v6, v1, v0

    .line 1047030
    iget-wide v8, p0, LX/65N;->f:J

    sub-long v4, v8, v4

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/65N;->f:J

    .line 1047031
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1047032
    :cond_6
    iget-object v3, p0, LX/65N;->c:LX/66U;

    invoke-interface {v3, v1}, LX/66U;->a(Ljava/io/File;)V

    goto :goto_3

    .line 1047033
    :cond_7
    iget v0, p0, LX/65N;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/65N;->i:I

    .line 1047034
    const/4 v0, 0x0

    .line 1047035
    iput-object v0, v2, LX/65M;->f:LX/65L;

    .line 1047036
    iget-boolean v0, v2, LX/65M;->e:Z

    or-int/2addr v0, p2

    if-eqz v0, :cond_a

    .line 1047037
    const/4 v0, 0x1

    .line 1047038
    iput-boolean v0, v2, LX/65M;->e:Z

    .line 1047039
    iget-object v0, p0, LX/65N;->g:LX/670;

    const-string v1, "CLEAN"

    invoke-interface {v0, v1}, LX/670;->b(Ljava/lang/String;)LX/670;

    move-result-object v0

    const/16 v1, 0x20

    invoke-interface {v0, v1}, LX/670;->h(I)LX/670;

    .line 1047040
    iget-object v0, p0, LX/65N;->g:LX/670;

    iget-object v1, v2, LX/65M;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/670;->b(Ljava/lang/String;)LX/670;

    .line 1047041
    iget-object v0, p0, LX/65N;->g:LX/670;

    invoke-virtual {v2, v0}, LX/65M;->a(LX/670;)V

    .line 1047042
    iget-object v0, p0, LX/65N;->g:LX/670;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, LX/670;->h(I)LX/670;

    .line 1047043
    if-eqz p2, :cond_8

    .line 1047044
    iget-wide v0, p0, LX/65N;->m:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    iput-wide v4, p0, LX/65N;->m:J

    .line 1047045
    iput-wide v0, v2, LX/65M;->g:J

    .line 1047046
    :cond_8
    :goto_4
    iget-object v0, p0, LX/65N;->g:LX/670;

    invoke-interface {v0}, LX/670;->flush()V

    .line 1047047
    iget-wide v0, p0, LX/65N;->f:J

    iget-wide v2, p0, LX/65N;->d:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_9

    invoke-static {p0}, LX/65N;->a(LX/65N;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1047048
    :cond_9
    iget-object v0, p0, LX/65N;->n:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/65N;->o:Ljava/lang/Runnable;

    const v2, 0x4588a3ec

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_1

    .line 1047049
    :cond_a
    iget-object v0, p0, LX/65N;->h:Ljava/util/LinkedHashMap;

    iget-object v1, v2, LX/65M;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1047050
    iget-object v0, p0, LX/65N;->g:LX/670;

    const-string v1, "REMOVE"

    invoke-interface {v0, v1}, LX/670;->b(Ljava/lang/String;)LX/670;

    move-result-object v0

    const/16 v1, 0x20

    invoke-interface {v0, v1}, LX/670;->h(I)LX/670;

    .line 1047051
    iget-object v0, p0, LX/65N;->g:LX/670;

    iget-object v1, v2, LX/65M;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/670;->b(Ljava/lang/String;)LX/670;

    .line 1047052
    iget-object v0, p0, LX/65N;->g:LX/670;

    const/16 v1, 0xa

    invoke-interface {v0, v1}, LX/670;->h(I)LX/670;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method private static declared-synchronized b(LX/65N;)Z
    .locals 1

    .prologue
    .line 1047007
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65N;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    .line 1046962
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/65N;->b(LX/65N;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1046963
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cache is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1046964
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1046965
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private d()V
    .locals 10

    .prologue
    .line 1046984
    :goto_0
    iget-wide v0, p0, LX/65N;->f:J

    iget-wide v2, p0, LX/65N;->d:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    .line 1046985
    iget-object v0, p0, LX/65N;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65M;

    .line 1046986
    iget-object v4, v0, LX/65M;->f:LX/65L;

    if-eqz v4, :cond_1

    .line 1046987
    iget-object v4, v0, LX/65M;->f:LX/65L;

    .line 1046988
    iget-object v5, v4, LX/65L;->b:LX/65M;

    iget-object v5, v5, LX/65M;->f:LX/65L;

    if-ne v5, v4, :cond_1

    .line 1046989
    const/4 v5, 0x0

    :goto_1
    iget-object v6, v4, LX/65L;->a:LX/65N;

    iget v6, v6, LX/65N;->e:I

    if-ge v5, v6, :cond_0

    .line 1046990
    :try_start_0
    iget-object v6, v4, LX/65L;->a:LX/65N;

    iget-object v6, v6, LX/65N;->c:LX/66U;

    iget-object v7, v4, LX/65L;->b:LX/65M;

    iget-object v7, v7, LX/65M;->d:[Ljava/io/File;

    aget-object v7, v7, v5

    invoke-interface {v6, v7}, LX/66U;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1046991
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1046992
    :cond_0
    iget-object v5, v4, LX/65L;->b:LX/65M;

    const/4 v6, 0x0

    .line 1046993
    iput-object v6, v5, LX/65M;->f:LX/65L;

    .line 1046994
    :cond_1
    const/4 v4, 0x0

    :goto_3
    iget v5, p0, LX/65N;->e:I

    if-ge v4, v5, :cond_2

    .line 1046995
    iget-object v5, p0, LX/65N;->c:LX/66U;

    iget-object v6, v0, LX/65M;->c:[Ljava/io/File;

    aget-object v6, v6, v4

    invoke-interface {v5, v6}, LX/66U;->a(Ljava/io/File;)V

    .line 1046996
    iget-wide v6, p0, LX/65N;->f:J

    iget-object v5, v0, LX/65M;->b:[J

    aget-wide v8, v5, v4

    sub-long/2addr v6, v8

    iput-wide v6, p0, LX/65N;->f:J

    .line 1046997
    iget-object v5, v0, LX/65M;->b:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v5, v4

    .line 1046998
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1046999
    :cond_2
    iget v4, p0, LX/65N;->i:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, LX/65N;->i:I

    .line 1047000
    iget-object v4, p0, LX/65N;->g:LX/670;

    const-string v5, "REMOVE"

    invoke-interface {v4, v5}, LX/670;->b(Ljava/lang/String;)LX/670;

    move-result-object v4

    const/16 v5, 0x20

    invoke-interface {v4, v5}, LX/670;->h(I)LX/670;

    move-result-object v4

    iget-object v5, v0, LX/65M;->a:Ljava/lang/String;

    invoke-interface {v4, v5}, LX/670;->b(Ljava/lang/String;)LX/670;

    move-result-object v4

    const/16 v5, 0xa

    invoke-interface {v4, v5}, LX/670;->h(I)LX/670;

    .line 1047001
    iget-object v4, p0, LX/65N;->h:Ljava/util/LinkedHashMap;

    iget-object v5, v0, LX/65M;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1047002
    invoke-static {p0}, LX/65N;->a(LX/65N;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1047003
    iget-object v4, p0, LX/65N;->n:Ljava/util/concurrent/Executor;

    iget-object v5, p0, LX/65N;->o:Ljava/lang/Runnable;

    const v6, 0x27d46337

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1047004
    :cond_3
    goto/16 :goto_0

    .line 1047005
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/65N;->l:Z

    .line 1047006
    return-void

    :catch_0
    goto :goto_2
.end method


# virtual methods
.method public final declared-synchronized close()V
    .locals 5

    .prologue
    .line 1046972
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65N;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/65N;->k:Z

    if-eqz v0, :cond_1

    .line 1046973
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/65N;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1046974
    :goto_0
    monitor-exit p0

    return-void

    .line 1046975
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/65N;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, LX/65N;->h:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    new-array v1, v1, [LX/65M;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/65M;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 1046976
    iget-object v4, v3, LX/65M;->f:LX/65L;

    if-eqz v4, :cond_2

    .line 1046977
    iget-object v3, v3, LX/65M;->f:LX/65L;

    invoke-virtual {v3}, LX/65L;->b()V

    .line 1046978
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1046979
    :cond_3
    invoke-direct {p0}, LX/65N;->d()V

    .line 1046980
    iget-object v0, p0, LX/65N;->g:LX/670;

    invoke-interface {v0}, LX/65J;->close()V

    .line 1046981
    const/4 v0, 0x0

    iput-object v0, p0, LX/65N;->g:LX/670;

    .line 1046982
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/65N;->k:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1046983
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized flush()V
    .locals 1

    .prologue
    .line 1046966
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/65N;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1046967
    :goto_0
    monitor-exit p0

    return-void

    .line 1046968
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/65N;->c()V

    .line 1046969
    invoke-direct {p0}, LX/65N;->d()V

    .line 1046970
    iget-object v0, p0, LX/65N;->g:LX/670;

    invoke-interface {v0}, LX/670;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1046971
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
