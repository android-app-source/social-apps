.class public final LX/54i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0za;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 828271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828272
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/54i;->b:Z

    .line 828273
    return-void
.end method

.method private static a(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/0za;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 828233
    if-nez p0, :cond_1

    .line 828234
    :cond_0
    return-void

    .line 828235
    :cond_1
    const/4 v1, 0x0

    .line 828236
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0za;

    .line 828237
    :try_start_0
    invoke-interface {v0}, LX/0za;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 828238
    :catch_0
    move-exception v3

    .line 828239
    if-nez v1, :cond_5

    .line 828240
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 828241
    :goto_1
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 828242
    goto :goto_0

    .line 828243
    :cond_2
    if-eqz v1, :cond_0

    .line 828244
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    .line 828245
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 828246
    instance-of v2, v0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_3

    .line 828247
    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 828248
    :cond_3
    new-instance v0, LX/52x;

    invoke-direct {v0, v1}, LX/52x;-><init>(Ljava/util/Collection;)V

    throw v0

    .line 828249
    :cond_4
    new-instance v0, LX/52x;

    invoke-direct {v0, v1}, LX/52x;-><init>(Ljava/util/Collection;)V

    throw v0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0za;)V
    .locals 3

    .prologue
    .line 828260
    const/4 v0, 0x0

    .line 828261
    monitor-enter p0

    .line 828262
    :try_start_0
    iget-boolean v1, p0, LX/54i;->b:Z

    if-eqz v1, :cond_1

    .line 828263
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 828264
    if-eqz p1, :cond_0

    .line 828265
    invoke-interface {p1}, LX/0za;->b()V

    .line 828266
    :cond_0
    return-void

    .line 828267
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/54i;->a:Ljava/util/Set;

    if-nez v1, :cond_2

    .line 828268
    new-instance v1, Ljava/util/HashSet;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, LX/54i;->a:Ljava/util/Set;

    .line 828269
    :cond_2
    iget-object v1, p0, LX/54i;->a:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object p1, v0

    goto :goto_0

    .line 828270
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 828274
    monitor-enter p0

    .line 828275
    :try_start_0
    iget-boolean v0, p0, LX/54i;->b:Z

    if-eqz v0, :cond_0

    .line 828276
    monitor-exit p0

    .line 828277
    :goto_0
    return-void

    .line 828278
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/54i;->b:Z

    .line 828279
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 828280
    iget-object v0, p0, LX/54i;->a:Ljava/util/Set;

    invoke-static {v0}, LX/54i;->a(Ljava/util/Collection;)V

    goto :goto_0

    .line 828281
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(LX/0za;)V
    .locals 1

    .prologue
    .line 828251
    monitor-enter p0

    .line 828252
    :try_start_0
    iget-boolean v0, p0, LX/54i;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/54i;->a:Ljava/util/Set;

    if-nez v0, :cond_2

    .line 828253
    :cond_0
    monitor-exit p0

    .line 828254
    :cond_1
    :goto_0
    return-void

    .line 828255
    :cond_2
    iget-object v0, p0, LX/54i;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 828256
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 828257
    if-eqz v0, :cond_1

    .line 828258
    invoke-interface {p1}, LX/0za;->b()V

    goto :goto_0

    .line 828259
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 828250
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/54i;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
