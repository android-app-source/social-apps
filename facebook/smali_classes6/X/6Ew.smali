.class public LX/6Ew;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ev;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ev",
        "<",
        "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
        ">;"
    }
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field private final a:LX/6rL;

.field public final b:LX/6r9;

.field public final c:LX/6F6;

.field private final d:LX/6Et;

.field public final e:Ljava/util/concurrent/Executor;

.field private final f:LX/0Tf;

.field private final g:Landroid/content/Context;

.field private final h:LX/0W3;

.field public i:Lcom/facebook/payments/checkout/model/CheckoutData;

.field public j:LX/6qd;

.field public k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:LX/6qh;

.field public m:LX/0YG;


# direct methods
.method public constructor <init>(LX/6rL;LX/6r9;LX/6F6;LX/6Et;Ljava/util/concurrent/Executor;LX/0Tf;Landroid/content/Context;LX/0W3;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1066814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066815
    iput-object p1, p0, LX/6Ew;->a:LX/6rL;

    .line 1066816
    iput-object p2, p0, LX/6Ew;->b:LX/6r9;

    .line 1066817
    iput-object p3, p0, LX/6Ew;->c:LX/6F6;

    .line 1066818
    iput-object p4, p0, LX/6Ew;->d:LX/6Et;

    .line 1066819
    iput-object p5, p0, LX/6Ew;->e:Ljava/util/concurrent/Executor;

    .line 1066820
    iput-object p6, p0, LX/6Ew;->f:LX/0Tf;

    .line 1066821
    iput-object p7, p0, LX/6Ew;->g:Landroid/content/Context;

    .line 1066822
    iput-object p8, p0, LX/6Ew;->h:LX/0W3;

    .line 1066823
    return-void
.end method

.method public static a(LX/0QB;)LX/6Ew;
    .locals 12

    .prologue
    .line 1066803
    const-class v1, LX/6Ew;

    monitor-enter v1

    .line 1066804
    :try_start_0
    sget-object v0, LX/6Ew;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1066805
    sput-object v2, LX/6Ew;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1066806
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1066807
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1066808
    new-instance v3, LX/6Ew;

    invoke-static {v0}, LX/6rL;->b(LX/0QB;)LX/6rL;

    move-result-object v4

    check-cast v4, LX/6rL;

    invoke-static {v0}, LX/6r9;->a(LX/0QB;)LX/6r9;

    move-result-object v5

    check-cast v5, LX/6r9;

    invoke-static {v0}, LX/6F6;->a(LX/0QB;)LX/6F6;

    move-result-object v6

    check-cast v6, LX/6F6;

    invoke-static {v0}, LX/6Et;->b(LX/0QB;)LX/6Et;

    move-result-object v7

    check-cast v7, LX/6Et;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, LX/0Tf;

    const-class v10, Landroid/content/Context;

    invoke-interface {v0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v11

    check-cast v11, LX/0W3;

    invoke-direct/range {v3 .. v11}, LX/6Ew;-><init>(LX/6rL;LX/6r9;LX/6F6;LX/6Et;Ljava/util/concurrent/Executor;LX/0Tf;Landroid/content/Context;LX/0W3;)V

    .line 1066809
    move-object v0, v3

    .line 1066810
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1066811
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6Ew;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1066812
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1066813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/6Ew;Ljava/lang/String;)V
    .locals 6
    .param p0    # LX/6Ew;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1066824
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1066825
    iget-object v0, p0, LX/6Ew;->g:Landroid/content/Context;

    const v1, 0x7f081d7f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/6Ew;->g:Landroid/content/Context;

    iget-object v5, p0, LX/6Ew;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-static {v4, v5}, LX/6Ez;->a(Landroid/content/Context;Lcom/facebook/payments/checkout/model/CheckoutData;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 1066826
    :cond_0
    iget-object v0, p0, LX/6Ew;->g:Landroid/content/Context;

    iget-object v1, p0, LX/6Ew;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->E:Z

    iget-object v2, p0, LX/6Ew;->l:LX/6qh;

    invoke-static {v0, p1, v1, v2}, LX/6rI;->a(Landroid/content/Context;Ljava/lang/String;ZLX/6qh;)V

    .line 1066827
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;)V
    .locals 1

    .prologue
    .line 1066800
    iget-object v0, p0, LX/6Ew;->a:LX/6rL;

    invoke-virtual {v0, p1}, LX/6rL;->a(LX/6qh;)V

    .line 1066801
    iput-object p1, p0, LX/6Ew;->l:LX/6qh;

    .line 1066802
    return-void
.end method

.method public final a(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 1

    .prologue
    .line 1066797
    check-cast p2, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 1066798
    iget-object v0, p0, LX/6Ew;->a:LX/6rL;

    invoke-virtual {v0, p1}, LX/6rL;->a(Landroid/os/Bundle;)V

    .line 1066799
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 7

    .prologue
    .line 1066778
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    const/4 v6, 0x1

    .line 1066779
    iput-object p1, p0, LX/6Ew;->i:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1066780
    iget-object v0, p0, LX/6Ew;->k:LX/0am;

    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->h()LX/0am;

    move-result-object v1

    if-eq v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1066781
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->d:LX/6re;

    sget-object v1, LX/6re;->JS_UPDATE_CHECKOUT:LX/6re;

    if-eq v0, v1, :cond_1

    .line 1066782
    :cond_0
    :goto_1
    return-void

    .line 1066783
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->h()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/6Ew;->k:LX/0am;

    .line 1066784
    iget-object v0, p0, LX/6Ew;->d:LX/6Et;

    const/4 v1, 0x0

    .line 1066785
    invoke-static {p1}, LX/6Et;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/browserextensions/common/payments/model/ShippingAddress;

    move-result-object v3

    .line 1066786
    if-nez v3, :cond_4

    .line 1066787
    :goto_2
    move-object v0, v1

    .line 1066788
    if-eqz v0, :cond_0

    .line 1066789
    iget-object v1, p0, LX/6Ew;->m:LX/0YG;

    invoke-static {v1}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1066790
    iget-object v1, p0, LX/6Ew;->m:LX/0YG;

    invoke-interface {v1, v6}, LX/0YG;->cancel(Z)Z

    .line 1066791
    :cond_2
    iget-object v1, p0, LX/6Ew;->c:LX/6F6;

    invoke-virtual {v1, p0, v0}, LX/6F6;->a(LX/6Ew;Lorg/json/JSONObject;)V

    .line 1066792
    iget-object v0, p0, LX/6Ew;->f:LX/0Tf;

    new-instance v1, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$1;

    invoke-direct {v1, p0}, Lcom/facebook/browserextensions/common/payments/JSBasedCheckoutOrderStatusHandler$1;-><init>(LX/6Ew;)V

    iget-object v2, p0, LX/6Ew;->h:LX/0W3;

    sget-wide v4, LX/0X5;->fv:J

    const/4 v3, 0x5

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v2

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    iput-object v0, p0, LX/6Ew;->m:LX/0YG;

    .line 1066793
    iget-object v0, p0, LX/6Ew;->l:LX/6qh;

    iget-object v1, p0, LX/6Ew;->m:LX/0YG;

    invoke-virtual {v0, v1, v6}, LX/6qh;->a(Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1066794
    :cond_4
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    iget-object v4, v0, LX/6Et;->b:LX/0lB;

    invoke-virtual {v4, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    goto :goto_2

    .line 1066795
    :catch_0
    move-exception v2

    .line 1066796
    sget-object v3, LX/6Et;->a:Ljava/lang/String;

    const-string v4, "Exception serializing getShippingAddressJSON!"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final b(Landroid/os/Bundle;Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 1

    .prologue
    .line 1066775
    check-cast p2, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 1066776
    iget-object v0, p0, LX/6Ew;->a:LX/6rL;

    invoke-virtual {v0, p1}, LX/6rL;->b(Landroid/os/Bundle;)V

    .line 1066777
    return-void
.end method
