.class public final LX/5Bx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 865982
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 865983
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 865984
    :goto_0
    return v1

    .line 865985
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_8

    .line 865986
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 865987
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 865988
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 865989
    const-string v11, "does_viewer_like"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 865990
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v9, v0

    move v0, v2

    goto :goto_1

    .line 865991
    :cond_1
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 865992
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 865993
    :cond_2
    const-string v11, "legacy_api_post_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 865994
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 865995
    :cond_3
    const-string v11, "like_sentence"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 865996
    invoke-static {p0, p1}, LX/5C0;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 865997
    :cond_4
    const-string v11, "likers"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 865998
    invoke-static {p0, p1}, LX/5Bw;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 865999
    :cond_5
    const-string v11, "viewer_does_not_like_sentence"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 866000
    invoke-static {p0, p1}, LX/5C0;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 866001
    :cond_6
    const-string v11, "viewer_likes_sentence"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 866002
    invoke-static {p0, p1}, LX/5C0;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 866003
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 866004
    :cond_8
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 866005
    if-eqz v0, :cond_9

    .line 866006
    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 866007
    :cond_9
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 866008
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 866009
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 866010
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 866011
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 866012
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 866013
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 866014
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 866015
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 866016
    if-eqz v0, :cond_0

    .line 866017
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866018
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 866019
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 866020
    if-eqz v0, :cond_1

    .line 866021
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866022
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 866023
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 866024
    if-eqz v0, :cond_2

    .line 866025
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866026
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 866027
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 866028
    if-eqz v0, :cond_3

    .line 866029
    const-string v1, "like_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866030
    invoke-static {p0, v0, p2, p3}, LX/5C0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 866031
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 866032
    if-eqz v0, :cond_4

    .line 866033
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866034
    invoke-static {p0, v0, p2}, LX/5Bw;->a(LX/15i;ILX/0nX;)V

    .line 866035
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 866036
    if-eqz v0, :cond_5

    .line 866037
    const-string v1, "viewer_does_not_like_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866038
    invoke-static {p0, v0, p2, p3}, LX/5C0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 866039
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 866040
    if-eqz v0, :cond_6

    .line 866041
    const-string v1, "viewer_likes_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 866042
    invoke-static {p0, v0, p2, p3}, LX/5C0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 866043
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 866044
    return-void
.end method
