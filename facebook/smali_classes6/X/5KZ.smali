.class public final LX/5KZ;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5KZ;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5KX;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5Ka;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 898791
    const/4 v0, 0x0

    sput-object v0, LX/5KZ;->a:LX/5KZ;

    .line 898792
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5KZ;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 898776
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 898777
    new-instance v0, LX/5Ka;

    invoke-direct {v0}, LX/5Ka;-><init>()V

    iput-object v0, p0, LX/5KZ;->c:LX/5Ka;

    .line 898778
    return-void
.end method

.method public static c(LX/1De;)LX/5KX;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 898783
    new-instance v1, LX/5KY;

    invoke-direct {v1}, LX/5KY;-><init>()V

    .line 898784
    sget-object v2, LX/5KZ;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5KX;

    .line 898785
    if-nez v2, :cond_0

    .line 898786
    new-instance v2, LX/5KX;

    invoke-direct {v2}, LX/5KX;-><init>()V

    .line 898787
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/5KX;->a$redex0(LX/5KX;LX/1De;IILX/5KY;)V

    .line 898788
    move-object v1, v2

    .line 898789
    move-object v0, v1

    .line 898790
    return-object v0
.end method

.method public static declared-synchronized q()LX/5KZ;
    .locals 2

    .prologue
    .line 898793
    const-class v1, LX/5KZ;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5KZ;->a:LX/5KZ;

    if-nez v0, :cond_0

    .line 898794
    new-instance v0, LX/5KZ;

    invoke-direct {v0}, LX/5KZ;-><init>()V

    sput-object v0, LX/5KZ;->a:LX/5KZ;

    .line 898795
    :cond_0
    sget-object v0, LX/5KZ;->a:LX/5KZ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 898796
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 898779
    check-cast p2, LX/5KY;

    .line 898780
    iget-object v0, p2, LX/5KY;->a:Lcom/facebook/java2js/JSContext;

    iget-object v1, p2, LX/5KY;->b:Ljava/lang/Object;

    iget-object v2, p2, LX/5KY;->c:Ljava/lang/String;

    iget-object v3, p2, LX/5KY;->d:Ljava/lang/Object;

    .line 898781
    const-string v4, "ComponentScript"

    const-string v5, "render"

    const/4 p0, 0x4

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v2, p0, p1

    const/4 p1, 0x1

    invoke-static {v0, v3}, Lcom/facebook/java2js/JSValue;->make(Lcom/facebook/java2js/JSContext;Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object p2

    aput-object p2, p0, p1

    const/4 p1, 0x2

    const/4 p2, 0x0

    aput-object p2, p0, p1

    const/4 p1, 0x3

    aput-object v1, p0, p1

    invoke-virtual {v0, v4, v5, p0}, Lcom/facebook/java2js/JSContext;->callModuleMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object v4

    const-class v5, LX/1X5;

    invoke-virtual {v4, v5}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1X5;

    invoke-virtual {v4}, LX/1X5;->b()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 898782
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 898774
    invoke-static {}, LX/1dS;->b()V

    .line 898775
    const/4 v0, 0x0

    return-object v0
.end method
