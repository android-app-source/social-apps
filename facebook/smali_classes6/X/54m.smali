.class public final LX/54m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# static fields
.field public static final a:LX/54l;

.field public static final c:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater",
            "<",
            "LX/54m;",
            "LX/54l;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile b:LX/54l;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 828331
    new-instance v0, LX/54l;

    const/4 v1, 0x0

    .line 828332
    sget-object v2, LX/0ze;->a:LX/0zf;

    move-object v2, v2

    .line 828333
    invoke-direct {v0, v1, v2}, LX/54l;-><init>(ZLX/0za;)V

    sput-object v0, LX/54m;->a:LX/54l;

    .line 828334
    const-class v0, LX/54m;

    const-class v1, LX/54l;

    const-string v2, "b"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, LX/54m;->c:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 828328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828329
    sget-object v0, LX/54m;->a:LX/54l;

    iput-object v0, p0, LX/54m;->b:LX/54l;

    .line 828330
    return-void
.end method


# virtual methods
.method public final a(LX/0za;)V
    .locals 3

    .prologue
    .line 828319
    if-nez p1, :cond_0

    .line 828320
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Subscription can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 828321
    :cond_0
    iget-object v0, p0, LX/54m;->b:LX/54l;

    .line 828322
    iget-boolean v1, v0, LX/54l;->a:Z

    if-eqz v1, :cond_1

    .line 828323
    invoke-interface {p1}, LX/0za;->b()V

    .line 828324
    :goto_0
    return-void

    .line 828325
    :cond_1
    new-instance v1, LX/54l;

    iget-boolean v2, v0, LX/54l;->a:Z

    invoke-direct {v1, v2, p1}, LX/54l;-><init>(ZLX/0za;)V

    move-object v1, v1

    .line 828326
    sget-object v2, LX/54m;->c:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v2, p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 828327
    iget-object v0, v0, LX/54l;->b:LX/0za;

    invoke-interface {v0}, LX/0za;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 828312
    :cond_0
    iget-object v0, p0, LX/54m;->b:LX/54l;

    .line 828313
    iget-boolean v1, v0, LX/54l;->a:Z

    if-eqz v1, :cond_1

    .line 828314
    :goto_0
    return-void

    .line 828315
    :cond_1
    new-instance v1, LX/54l;

    const/4 v2, 0x1

    iget-object v3, v0, LX/54l;->b:LX/0za;

    invoke-direct {v1, v2, v3}, LX/54l;-><init>(ZLX/0za;)V

    move-object v1, v1

    .line 828316
    sget-object v2, LX/54m;->c:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v2, p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 828317
    iget-object v0, v0, LX/54l;->b:LX/0za;

    invoke-interface {v0}, LX/0za;->b()V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 828318
    iget-object v0, p0, LX/54m;->b:LX/54l;

    iget-boolean v0, v0, LX/54l;->a:Z

    return v0
.end method
