.class public LX/63V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1043276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1043277
    iput-object p1, p0, LX/63V;->a:Landroid/content/Context;

    .line 1043278
    return-void
.end method

.method public static a(LX/0QB;)LX/63V;
    .locals 4

    .prologue
    .line 1043279
    const-class v1, LX/63V;

    monitor-enter v1

    .line 1043280
    :try_start_0
    sget-object v0, LX/63V;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1043281
    sput-object v2, LX/63V;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1043282
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1043283
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1043284
    new-instance p0, LX/63V;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/63V;-><init>(Landroid/content/Context;)V

    .line 1043285
    move-object v0, p0

    .line 1043286
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1043287
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/63V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1043288
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1043289
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 1043290
    iget-object v0, p0, LX/63V;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->TitleBarViewStub:[I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1043291
    const/16 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1043292
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1043293
    return v1
.end method
