.class public LX/6cV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static volatile f:LX/6cV;


# instance fields
.field public final a:LX/6cW;

.field private final b:LX/2N6;

.field private final c:LX/2Ud;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1114402
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, LX/6cS;->b:LX/0U1;

    .line 1114403
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 1114404
    aput-object v1, v0, v2

    sput-object v0, LX/6cV;->d:[Ljava/lang/String;

    .line 1114405
    new-array v0, v3, [Ljava/lang/String;

    sget-object v1, LX/6cS;->c:LX/0U1;

    .line 1114406
    iget-object v3, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v3

    .line 1114407
    aput-object v1, v0, v2

    sput-object v0, LX/6cV;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/6cW;LX/2N6;LX/2Ud;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1114408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114409
    iput-object p1, p0, LX/6cV;->a:LX/6cW;

    .line 1114410
    iput-object p2, p0, LX/6cV;->b:LX/2N6;

    .line 1114411
    iput-object p3, p0, LX/6cV;->c:LX/2Ud;

    .line 1114412
    return-void
.end method

.method public static a(LX/0QB;)LX/6cV;
    .locals 6

    .prologue
    .line 1114413
    sget-object v0, LX/6cV;->f:LX/6cV;

    if-nez v0, :cond_1

    .line 1114414
    const-class v1, LX/6cV;

    monitor-enter v1

    .line 1114415
    :try_start_0
    sget-object v0, LX/6cV;->f:LX/6cV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1114416
    if-eqz v2, :cond_0

    .line 1114417
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1114418
    new-instance p0, LX/6cV;

    invoke-static {v0}, LX/6cW;->a(LX/0QB;)LX/6cW;

    move-result-object v3

    check-cast v3, LX/6cW;

    invoke-static {v0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v4

    check-cast v4, LX/2N6;

    invoke-static {v0}, LX/2Ud;->a(LX/0QB;)LX/2Ud;

    move-result-object v5

    check-cast v5, LX/2Ud;

    invoke-direct {p0, v3, v4, v5}, LX/6cV;-><init>(LX/6cW;LX/2N6;LX/2Ud;)V

    .line 1114419
    move-object v0, p0

    .line 1114420
    sput-object v0, LX/6cV;->f:LX/6cV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1114421
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1114422
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1114423
    :cond_1
    sget-object v0, LX/6cV;->f:LX/6cV;

    return-object v0

    .line 1114424
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1114425
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(JJ)Z
    .locals 3

    .prologue
    .line 1114426
    invoke-static {p0, p1, p2}, LX/6cV;->d(LX/6cV;J)J

    move-result-wide v0

    cmp-long v0, v0, p3

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(J)J
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 1114427
    sget-object v0, LX/6cS;->a:LX/0U1;

    .line 1114428
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114429
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 1114430
    iget-object v0, p0, LX/6cV;->a:LX/6cW;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "time_skew"

    sget-object v2, LX/6cV;->d:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1114431
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1114432
    sget-object v0, LX/6cS;->b:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->c(Landroid/database/Cursor;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1114433
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1114434
    :goto_0
    return-wide v0

    .line 1114435
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1114436
    :goto_1
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1114437
    :catch_0
    move-exception v0

    .line 1114438
    :try_start_1
    const-string v1, "SkewedTimestampHandler"

    const-string v3, "Error checking time skew"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1114439
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static d(LX/6cV;J)J
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 1114440
    sget-object v0, LX/6cS;->a:LX/0U1;

    .line 1114441
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114442
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 1114443
    iget-object v0, p0, LX/6cV;->a:LX/6cW;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "time_skew"

    sget-object v2, LX/6cV;->e:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1114444
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1114445
    sget-object v0, LX/6cS;->c:LX/0U1;

    invoke-virtual {v0, v2}, LX/0U1;->c(Landroid/database/Cursor;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1114446
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1114447
    :goto_0
    return-wide v0

    .line 1114448
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1114449
    :goto_1
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1114450
    :catch_0
    move-exception v0

    .line 1114451
    :try_start_1
    const-string v1, "SkewedTimestampHandler"

    const-string v3, "Error checking time skew"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1114452
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(JJ)J
    .locals 9

    .prologue
    .line 1114453
    iget-object v0, p0, LX/6cV;->c:LX/2Ud;

    .line 1114454
    iget-object v1, v0, LX/2Ud;->g:LX/2Ue;

    move-object v0, v1

    .line 1114455
    sget-object v1, LX/2Ue;->UNKNOWN:LX/2Ue;

    if-eq v0, v1, :cond_1

    .line 1114456
    iget-object v0, p0, LX/6cV;->c:LX/2Ud;

    invoke-virtual {v0}, LX/2Ud;->a()J

    move-result-wide v4

    .line 1114457
    :goto_0
    cmp-long v0, v4, p3

    if-lez v0, :cond_2

    .line 1114458
    :cond_0
    :goto_1
    return-wide p3

    .line 1114459
    :cond_1
    iget-object v0, p0, LX/6cV;->b:LX/2N6;

    invoke-virtual {v0}, LX/2N6;->a()J

    move-result-wide v4

    goto :goto_0

    .line 1114460
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, LX/6cV;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v1, p0

    move-wide v2, p1

    move-wide v6, p3

    .line 1114461
    new-instance v0, Landroid/content/ContentValues;

    const/4 v8, 0x3

    invoke-direct {v0, v8}, Landroid/content/ContentValues;-><init>(I)V

    .line 1114462
    sget-object v8, LX/6cS;->a:LX/0U1;

    .line 1114463
    iget-object p0, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, p0

    .line 1114464
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, v8, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1114465
    sget-object v8, LX/6cS;->b:LX/0U1;

    .line 1114466
    iget-object p0, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, p0

    .line 1114467
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, v8, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1114468
    sget-object v8, LX/6cS;->c:LX/0U1;

    .line 1114469
    iget-object p0, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, p0

    .line 1114470
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, v8, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1114471
    iget-object v8, v1, LX/6cV;->a:LX/6cW;

    invoke-virtual {v8}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string p0, "time_skew"

    const/4 p1, 0x0

    const/4 p2, 0x5

    const p3, 0x3f4cb685

    invoke-static {p3}, LX/03h;->a(I)V

    invoke-virtual {v8, p0, p1, v0, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    const v0, -0x1ad4d00d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1114472
    move-wide p3, v4

    .line 1114473
    goto :goto_1

    .line 1114474
    :cond_3
    invoke-direct {p0, p1, p2}, LX/6cV;->c(J)J

    move-result-wide v4

    .line 1114475
    const-wide/16 v0, -0x1

    cmp-long v0, v4, v0

    if-eqz v0, :cond_0

    move-wide p3, v4

    goto :goto_1
.end method
