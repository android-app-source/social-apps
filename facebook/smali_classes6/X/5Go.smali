.class public final enum LX/5Go;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Go;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Go;

.field public static final enum GRAPHQL_DEFAULT:LX/5Go;

.field public static final enum GRAPHQL_FEEDBACK_DETAILS:LX/5Go;

.field public static final enum GRAPHQL_PHOTO_CREATION_STORY:LX/5Go;

.field public static final enum GRAPHQL_VIDEO_CREATION_STORY:LX/5Go;

.field public static final enum NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

.field public static final enum PLATFORM_DEFAULT:LX/5Go;

.field public static final enum PLATFORM_FEEDBACK_DETAILS:LX/5Go;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 889204
    new-instance v0, LX/5Go;

    const-string v1, "GRAPHQL_DEFAULT"

    invoke-direct {v0, v1, v3}, LX/5Go;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    .line 889205
    new-instance v0, LX/5Go;

    const-string v1, "GRAPHQL_PHOTO_CREATION_STORY"

    invoke-direct {v0, v1, v4}, LX/5Go;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Go;->GRAPHQL_PHOTO_CREATION_STORY:LX/5Go;

    .line 889206
    new-instance v0, LX/5Go;

    const-string v1, "GRAPHQL_VIDEO_CREATION_STORY"

    invoke-direct {v0, v1, v5}, LX/5Go;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Go;->GRAPHQL_VIDEO_CREATION_STORY:LX/5Go;

    .line 889207
    new-instance v0, LX/5Go;

    const-string v1, "GRAPHQL_FEEDBACK_DETAILS"

    invoke-direct {v0, v1, v6}, LX/5Go;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Go;->GRAPHQL_FEEDBACK_DETAILS:LX/5Go;

    .line 889208
    new-instance v0, LX/5Go;

    const-string v1, "NOTIFICATION_FEEDBACK_DETAILS"

    invoke-direct {v0, v1, v7}, LX/5Go;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Go;->NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

    .line 889209
    new-instance v0, LX/5Go;

    const-string v1, "PLATFORM_FEEDBACK_DETAILS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5Go;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Go;->PLATFORM_FEEDBACK_DETAILS:LX/5Go;

    .line 889210
    new-instance v0, LX/5Go;

    const-string v1, "PLATFORM_DEFAULT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/5Go;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Go;->PLATFORM_DEFAULT:LX/5Go;

    .line 889211
    const/4 v0, 0x7

    new-array v0, v0, [LX/5Go;

    sget-object v1, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    aput-object v1, v0, v3

    sget-object v1, LX/5Go;->GRAPHQL_PHOTO_CREATION_STORY:LX/5Go;

    aput-object v1, v0, v4

    sget-object v1, LX/5Go;->GRAPHQL_VIDEO_CREATION_STORY:LX/5Go;

    aput-object v1, v0, v5

    sget-object v1, LX/5Go;->GRAPHQL_FEEDBACK_DETAILS:LX/5Go;

    aput-object v1, v0, v6

    sget-object v1, LX/5Go;->NOTIFICATION_FEEDBACK_DETAILS:LX/5Go;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5Go;->PLATFORM_FEEDBACK_DETAILS:LX/5Go;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5Go;->PLATFORM_DEFAULT:LX/5Go;

    aput-object v2, v0, v1

    sput-object v0, LX/5Go;->$VALUES:[LX/5Go;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 889213
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Go;
    .locals 1

    .prologue
    .line 889214
    const-class v0, LX/5Go;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Go;

    return-object v0
.end method

.method public static values()[LX/5Go;
    .locals 1

    .prologue
    .line 889212
    sget-object v0, LX/5Go;->$VALUES:[LX/5Go;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Go;

    return-object v0
.end method
