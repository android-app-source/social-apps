.class public LX/62a;
.super LX/3x6;
.source ""


# instance fields
.field private final a:Z

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1041779
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/62a;-><init>(II)V

    .line 1041780
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 1041781
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/62a;-><init>(IIZ)V

    .line 1041782
    return-void
.end method

.method private constructor <init>(IIZ)V
    .locals 0

    .prologue
    .line 1041783
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1041784
    iput-boolean p3, p0, LX/62a;->a:Z

    .line 1041785
    iput p1, p0, LX/62a;->c:I

    .line 1041786
    iput p2, p0, LX/62a;->b:I

    .line 1041787
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 4

    .prologue
    .line 1041788
    invoke-super {p0, p1, p2, p3, p4}, LX/3x6;->a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 1041789
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/view/View;)I

    move-result v2

    .line 1041790
    iget v0, p0, LX/62a;->c:I

    .line 1041791
    const/4 v1, 0x0

    .line 1041792
    if-nez v2, :cond_0

    .line 1041793
    iget v0, p0, LX/62a;->b:I

    .line 1041794
    :cond_0
    invoke-virtual {p4}, LX/1Ok;->e()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_1

    .line 1041795
    iget v1, p0, LX/62a;->b:I

    .line 1041796
    :cond_1
    iget-boolean v2, p0, LX/62a;->a:Z

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 1041797
    iget-boolean v2, p0, LX/62a;->a:Z

    if-eqz v2, :cond_3

    :goto_1
    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 1041798
    return-void

    :cond_2
    move v2, v0

    .line 1041799
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1041800
    goto :goto_1
.end method
