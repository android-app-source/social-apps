.class public final LX/5KE;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/5KE;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5KC;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/5KF;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 898366
    const/4 v0, 0x0

    sput-object v0, LX/5KE;->a:LX/5KE;

    .line 898367
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5KE;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 898363
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 898364
    new-instance v0, LX/5KF;

    invoke-direct {v0}, LX/5KF;-><init>()V

    iput-object v0, p0, LX/5KE;->c:LX/5KF;

    .line 898365
    return-void
.end method

.method public static c(LX/1De;)LX/5KC;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 898447
    new-instance v1, LX/5KD;

    invoke-direct {v1}, LX/5KD;-><init>()V

    .line 898448
    sget-object v2, LX/5KE;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5KC;

    .line 898449
    if-nez v2, :cond_0

    .line 898450
    new-instance v2, LX/5KC;

    invoke-direct {v2}, LX/5KC;-><init>()V

    .line 898451
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/5KC;->a$redex0(LX/5KC;LX/1De;IILX/5KD;)V

    .line 898452
    move-object v1, v2

    .line 898453
    move-object v0, v1

    .line 898454
    return-object v0
.end method

.method public static declared-synchronized q()LX/5KE;
    .locals 2

    .prologue
    .line 898455
    const-class v1, LX/5KE;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5KE;->a:LX/5KE;

    if-nez v0, :cond_0

    .line 898456
    new-instance v0, LX/5KE;

    invoke-direct {v0}, LX/5KE;-><init>()V

    sput-object v0, LX/5KE;->a:LX/5KE;

    .line 898457
    :cond_0
    sget-object v0, LX/5KE;->a:LX/5KE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 898458
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 898459
    invoke-static {}, LX/1dS;->b()V

    .line 898460
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2b9ceae5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 898461
    invoke-static {}, LX/5KF;->a()V

    .line 898462
    const/16 v1, 0x1f

    const v2, -0x6e571b3b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 1

    .prologue
    .line 898463
    check-cast p3, LX/5KD;

    .line 898464
    iget-object v0, p3, LX/5KD;->a:LX/5Je;

    .line 898465
    invoke-virtual {p2}, LX/1Dg;->c()I

    move-result p0

    invoke-virtual {p2}, LX/1Dg;->d()I

    move-result p1

    invoke-virtual {v0, p0, p1}, LX/3mY;->b(II)V

    .line 898466
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 898467
    new-instance v0, LX/25R;

    invoke-direct {v0, p1}, LX/25R;-><init>(Landroid/content/Context;)V

    .line 898468
    const/4 p0, 0x1

    invoke-virtual {v0, p0}, LX/25R;->setSnappingEnabled(Z)V

    .line 898469
    move-object v0, v0

    .line 898470
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 898471
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/1X1;LX/1X1;)Z
    .locals 5

    .prologue
    .line 898414
    check-cast p1, LX/5KD;

    .line 898415
    check-cast p2, LX/5KD;

    .line 898416
    iget-object v0, p1, LX/5KD;->a:LX/5Je;

    iget-object v1, p2, LX/5KD;->a:LX/5Je;

    invoke-static {v0, v1}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 898417
    iget-boolean v1, p1, LX/5KD;->h:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p2, LX/5KD;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v1

    .line 898418
    iget-boolean v2, p1, LX/5KD;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p2, LX/5KD;->i:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v2

    .line 898419
    iget v3, p1, LX/5KD;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, p2, LX/5KD;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, LX/1S3;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v3

    .line 898420
    const/4 p0, 0x1

    .line 898421
    iget-object v4, v0, LX/3lz;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 898422
    iget-object p1, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object p1, p1

    .line 898423
    if-eq v4, p1, :cond_0

    move v4, p0

    .line 898424
    :goto_0
    move v4, v4

    .line 898425
    invoke-static {v0}, LX/1cy;->a(LX/3lz;)V

    .line 898426
    invoke-static {v1}, LX/1cy;->a(LX/3lz;)V

    .line 898427
    invoke-static {v2}, LX/1cy;->a(LX/3lz;)V

    .line 898428
    invoke-static {v3}, LX/1cy;->a(LX/3lz;)V

    .line 898429
    return v4

    .line 898430
    :cond_0
    iget-object v4, v1, LX/3lz;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 898431
    check-cast v4, Ljava/lang/Boolean;

    .line 898432
    iget-object p1, v1, LX/3lz;->b:Ljava/lang/Object;

    move-object p1, p1

    .line 898433
    invoke-virtual {v4, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    move v4, p0

    .line 898434
    goto :goto_0

    .line 898435
    :cond_1
    iget-object v4, v2, LX/3lz;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 898436
    check-cast v4, Ljava/lang/Boolean;

    .line 898437
    iget-object p1, v2, LX/3lz;->b:Ljava/lang/Object;

    move-object p1, p1

    .line 898438
    invoke-virtual {v4, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move v4, p0

    .line 898439
    goto :goto_0

    .line 898440
    :cond_2
    iget-object v4, v3, LX/3lz;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 898441
    check-cast v4, Ljava/lang/Integer;

    .line 898442
    iget-object p1, v3, LX/3lz;->b:Ljava/lang/Object;

    move-object p1, p1

    .line 898443
    invoke-virtual {v4, p1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    move v4, p0

    .line 898444
    goto :goto_0

    .line 898445
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 898446
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 4

    .prologue
    .line 898407
    check-cast p3, LX/5KD;

    .line 898408
    check-cast p2, LX/25R;

    iget-object v0, p3, LX/5KD;->a:LX/5Je;

    iget-boolean v1, p3, LX/5KD;->h:Z

    iget-boolean v2, p3, LX/5KD;->i:Z

    iget v3, p3, LX/5KD;->j:I

    .line 898409
    iput-boolean v1, p2, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 898410
    invoke-virtual {p2, v2}, LX/25R;->setClipToPadding(Z)V

    .line 898411
    invoke-virtual {p2, v3}, LX/25R;->setScrollBarStyle(I)V

    .line 898412
    invoke-virtual {v0, p2}, LX/3mY;->a(Landroid/view/ViewGroup;)V

    .line 898413
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 898406
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 898402
    check-cast p3, LX/5KD;

    .line 898403
    check-cast p2, LX/25R;

    iget-object v0, p3, LX/5KD;->a:LX/5Je;

    .line 898404
    invoke-virtual {v0, p2}, LX/3mY;->d(Landroid/view/ViewGroup;)V

    .line 898405
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 8

    .prologue
    .line 898383
    check-cast p3, LX/5KD;

    .line 898384
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    move-object v0, p2

    .line 898385
    check-cast v0, LX/25R;

    iget-object v1, p3, LX/5KD;->b:LX/1Of;

    iget-object v2, p3, LX/5KD;->a:LX/5Je;

    iget-object v3, p3, LX/5KD;->c:LX/25S;

    iget-object v4, p3, LX/5KD;->d:LX/5K7;

    iget-object v5, p3, LX/5KD;->e:LX/3x6;

    iget-object v6, p3, LX/5KD;->f:LX/1OX;

    .line 898386
    iget-object p0, v0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object p0, p0

    .line 898387
    iput-object p0, v7, LX/1np;->a:Ljava/lang/Object;

    .line 898388
    sget-object p0, LX/5K9;->a:LX/1Of;

    if-eq v1, p0, :cond_0

    .line 898389
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 898390
    :cond_0
    if-eqz v5, :cond_1

    .line 898391
    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 898392
    :cond_1
    if-eqz v6, :cond_2

    .line 898393
    invoke-virtual {v0, v6}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 898394
    :cond_2
    invoke-virtual {v2, v0}, LX/3mY;->b(Landroid/view/ViewGroup;)V

    .line 898395
    if-eqz v4, :cond_3

    .line 898396
    iput-object v0, v4, LX/5K7;->a:Landroid/support/v7/widget/RecyclerView;

    .line 898397
    :cond_3
    iput-object v3, v0, LX/25R;->p:LX/25S;

    .line 898398
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 898399
    check-cast v0, LX/1Of;

    iput-object v0, p3, LX/5KD;->g:LX/1Of;

    .line 898400
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 898401
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 6

    .prologue
    .line 898371
    check-cast p3, LX/5KD;

    move-object v0, p2

    .line 898372
    check-cast v0, LX/25R;

    iget-object v1, p3, LX/5KD;->a:LX/5Je;

    iget-object v2, p3, LX/5KD;->d:LX/5K7;

    iget-object v3, p3, LX/5KD;->e:LX/3x6;

    iget-object v4, p3, LX/5KD;->f:LX/1OX;

    iget-object v5, p3, LX/5KD;->g:LX/1Of;

    const/4 p0, 0x0

    .line 898373
    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 898374
    invoke-virtual {v1, v0}, LX/3mY;->c(Landroid/view/ViewGroup;)V

    .line 898375
    if-eqz v2, :cond_0

    .line 898376
    iput-object p0, v2, LX/5K7;->a:Landroid/support/v7/widget/RecyclerView;

    .line 898377
    :cond_0
    if-eqz v3, :cond_1

    .line 898378
    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->b(LX/3x6;)V

    .line 898379
    :cond_1
    if-eqz v4, :cond_2

    .line 898380
    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 898381
    :cond_2
    iput-object p0, v0, LX/25R;->p:LX/25S;

    .line 898382
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 898370
    const/4 v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 898369
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 898368
    const/16 v0, 0xf

    return v0
.end method
