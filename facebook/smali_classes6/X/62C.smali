.class public LX/62C;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements LX/0Vf;
.implements LX/1OP;
.implements LX/1Cw;


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/62B;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/61t;

.field public final c:LX/61s;


# direct methods
.method public constructor <init>(ZLjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<+",
            "LX/1Cw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1041288
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1041289
    new-instance v0, LX/629;

    invoke-direct {v0, p0}, LX/629;-><init>(LX/62C;)V

    iput-object v0, p0, LX/62C;->c:LX/61s;

    .line 1041290
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1041291
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1041292
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cw;

    .line 1041293
    new-instance v4, LX/62B;

    invoke-direct {v4, p0, v0}, LX/62B;-><init>(LX/62C;LX/1Cw;)V

    .line 1041294
    invoke-interface {v0, v4}, LX/1Cw;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1041295
    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1041296
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1041297
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/62C;->a:LX/0Px;

    .line 1041298
    new-instance v0, LX/61t;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/61t;-><init>(ZLX/0Px;)V

    iput-object v0, p0, LX/62C;->b:LX/61t;

    .line 1041299
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/62C;->a(LX/62C;LX/61s;)V

    .line 1041300
    return-void
.end method

.method public varargs constructor <init>(Z[LX/1Cw;)V
    .locals 1

    .prologue
    .line 1041301
    invoke-static {p2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/62C;-><init>(ZLjava/util/List;)V

    .line 1041302
    return-void
.end method

.method public static a(Ljava/util/List;)LX/62C;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1Cw;",
            ">;)",
            "LX/62C;"
        }
    .end annotation

    .prologue
    .line 1041303
    new-instance v0, LX/62C;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, LX/62C;-><init>(ZLjava/util/List;)V

    return-object v0
.end method

.method public static varargs a([LX/1Cw;)LX/62C;
    .locals 2

    .prologue
    .line 1041286
    new-instance v0, LX/62C;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, LX/62C;-><init>(Z[LX/1Cw;)V

    return-object v0
.end method

.method public static a(LX/62C;LX/61s;)V
    .locals 1
    .param p0    # LX/62C;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1041304
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->a(LX/61s;)V

    .line 1041305
    return-void
.end method

.method public static b(Ljava/util/List;)LX/62C;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/1Cw;",
            ">;)",
            "LX/62C;"
        }
    .end annotation

    .prologue
    .line 1041306
    new-instance v0, LX/62C;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, LX/62C;-><init>(ZLjava/util/List;)V

    return-object v0
.end method

.method public static varargs b([LX/1Cw;)LX/62C;
    .locals 2

    .prologue
    .line 1041307
    new-instance v0, LX/62C;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, LX/62C;-><init>(Z[LX/1Cw;)V

    return-object v0
.end method

.method public static b(LX/62C;LX/1Cw;)V
    .locals 1

    .prologue
    .line 1041308
    new-instance v0, LX/62A;

    invoke-direct {v0, p0, p1}, LX/62A;-><init>(LX/62C;LX/1Cw;)V

    invoke-static {p0, v0}, LX/62C;->a(LX/62C;LX/61s;)V

    .line 1041309
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 1041310
    return-void
.end method


# virtual methods
.method public final a(LX/1Cw;)I
    .locals 1

    .prologue
    .line 1041311
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->a(LX/1Cw;)V

    .line 1041312
    iget-object v0, p0, LX/62C;->b:LX/61t;

    .line 1041313
    iget p0, v0, LX/61t;->c:I

    move v0, p0

    .line 1041314
    return v0
.end method

.method public a(I)LX/1Cw;
    .locals 1

    .prologue
    .line 1041315
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->a(I)V

    .line 1041316
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->d()LX/1Cw;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1041317
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->b(I)V

    .line 1041318
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->c(I)I

    move-result v0

    .line 1041319
    iget-object v1, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v1}, LX/61t;->d()LX/1Cw;

    move-result-object v1

    invoke-interface {v1, v0, p2}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 1041320
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->a(I)V

    .line 1041321
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p4}, LX/61t;->c(I)I

    move-result v4

    .line 1041322
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->d()LX/1Cw;

    move-result-object v0

    iget-object v1, p0, LX/62C;->b:LX/61t;

    .line 1041323
    iget v2, v1, LX/61t;->d:I

    move v1, v2

    .line 1041324
    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 1041325
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1041287
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->g()Z

    move-result v0

    return v0
.end method

.method public final dispose()V
    .locals 1

    .prologue
    .line 1041252
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->dispose()V

    .line 1041253
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1041254
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->e()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1041255
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->a(I)V

    .line 1041256
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->d()LX/1Cw;

    move-result-object v0

    iget-object v1, p0, LX/62C;->b:LX/61t;

    .line 1041257
    iget p0, v1, LX/61t;->d:I

    move v1, p0

    .line 1041258
    invoke-interface {v0, v1}, LX/1Cw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 1041259
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->a(I)V

    .line 1041260
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->d()LX/1Cw;

    move-result-object v0

    iget-object v1, p0, LX/62C;->b:LX/61t;

    .line 1041261
    iget p0, v1, LX/61t;->d:I

    move v1, p0

    .line 1041262
    invoke-interface {v0, v1}, LX/1Cw;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1041263
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->a(I)V

    .line 1041264
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->d()LX/1Cw;

    move-result-object v0

    iget-object v1, p0, LX/62C;->b:LX/61t;

    .line 1041265
    iget p1, v1, LX/61t;->d:I

    move v1, p1

    .line 1041266
    invoke-interface {v0, v1}, LX/1Cw;->getItemViewType(I)I

    move-result v0

    iget-object v1, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v1}, LX/61t;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1041267
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->a(I)V

    .line 1041268
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->d()LX/1Cw;

    move-result-object v0

    iget-object v1, p0, LX/62C;->b:LX/61t;

    .line 1041269
    iget p0, v1, LX/61t;->d:I

    move v1, p0

    .line 1041270
    invoke-interface {v0, v1, p2, p3}, LX/1Cw;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1041271
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->f()I

    move-result v0

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1041272
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->g()Z

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1041273
    invoke-virtual {p0}, LX/62C;->getCount()I

    move-result v0

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 1041274
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 1041275
    invoke-virtual {p0}, LX/62C;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 1041276
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0, p1}, LX/61t;->a(I)V

    .line 1041277
    iget-object v0, p0, LX/62C;->b:LX/61t;

    invoke-virtual {v0}, LX/61t;->d()LX/1Cw;

    move-result-object v0

    iget-object v1, p0, LX/62C;->b:LX/61t;

    .line 1041278
    iget p0, v1, LX/61t;->d:I

    move v1, p0

    .line 1041279
    invoke-interface {v0, v1}, LX/1Cw;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 1041280
    const-string v0, "MultiAdapterListAdapter.notifyDataSetChanged"

    const v1, -0xaa79ae7

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1041281
    :try_start_0
    iget-object v0, p0, LX/62C;->c:LX/61s;

    invoke-static {p0, v0}, LX/62C;->a(LX/62C;LX/61s;)V

    .line 1041282
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041283
    const v0, 0x1d7f0f5b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1041284
    return-void

    .line 1041285
    :catchall_0
    move-exception v0

    const v1, -0x431e134c

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
