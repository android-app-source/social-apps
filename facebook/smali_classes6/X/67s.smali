.class public LX/67s;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1053749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(DDD)D
    .locals 8

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    .line 1053750
    const-wide v0, 0x401921fb54442d18L    # 6.283185307179586

    sub-double v2, p0, v4

    mul-double/2addr v0, v2

    sub-double/2addr v0, p4

    .line 1053751
    invoke-static {p2, p3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {p2, p3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    .line 1053752
    add-double v2, v6, v0

    sub-double v0, v6, v0

    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x402921fb54442d18L    # 12.566370614359172

    div-double/2addr v0, v2

    add-double/2addr v0, v4

    return-wide v0
.end method

.method public static a(Ljava/util/List;)[D
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;)[D"
        }
    .end annotation

    .prologue
    .line 1053723
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v6

    .line 1053724
    mul-int/lit8 v2, v6, 0x2

    new-array v7, v2, [D

    .line 1053725
    const/4 v3, 0x0

    .line 1053726
    const/4 v2, 0x0

    move v4, v2

    move v5, v3

    :goto_0
    if-ge v4, v6, :cond_2

    .line 1053727
    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/android/maps/model/LatLng;

    .line 1053728
    add-int/lit8 v3, v4, 0x1

    if-ne v3, v6, :cond_0

    const/4 v3, 0x0

    :goto_1
    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/android/maps/model/LatLng;

    .line 1053729
    iget-wide v8, v2, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-wide v10, v3, Lcom/facebook/android/maps/model/LatLng;->b:D

    cmpg-double v8, v8, v10

    if-gez v8, :cond_3

    .line 1053730
    :goto_2
    iget-wide v8, v3, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    .line 1053731
    iget-wide v10, v3, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    .line 1053732
    iget-wide v12, v2, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v12, v13}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    .line 1053733
    iget-wide v2, v2, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    .line 1053734
    sub-double/2addr v2, v10

    .line 1053735
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    .line 1053736
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    .line 1053737
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    .line 1053738
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {v12, v13}, Ljava/lang/Math;->tan(D)D

    move-result-wide v12

    mul-double/2addr v12, v14

    mul-double v18, v18, v16

    sub-double v12, v12, v18

    invoke-static {v2, v3, v12, v13}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 1053739
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    mul-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->asin(D)D

    move-result-wide v12

    .line 1053740
    const-wide v18, 0x3ff921fb54442d18L    # 1.5707963267948966

    move-wide/from16 v0, v18

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v18

    if-nez v18, :cond_1

    const-wide/16 v18, 0x0

    cmpl-double v8, v8, v18

    if-nez v8, :cond_1

    const-wide/16 v2, 0x0

    .line 1053741
    :goto_3
    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v8, v14

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 1053742
    sub-double v8, v10, v2

    .line 1053743
    add-int/lit8 v2, v5, 0x1

    aput-wide v12, v7, v5

    .line 1053744
    add-int/lit8 v3, v2, 0x1

    aput-wide v8, v7, v2

    .line 1053745
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v5, v3

    goto/16 :goto_0

    .line 1053746
    :cond_0
    add-int/lit8 v3, v4, 0x1

    goto/16 :goto_1

    .line 1053747
    :cond_1
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v2, v14

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    goto :goto_3

    .line 1053748
    :cond_2
    return-object v7

    :cond_3
    move-object/from16 v20, v3

    move-object v3, v2

    move-object/from16 v2, v20

    goto/16 :goto_2
.end method

.method public static a(Ljava/util/List;Z)[D
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;Z)[D"
        }
    .end annotation

    .prologue
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    const/4 v2, 0x0

    .line 1053694
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 1053695
    mul-int/lit8 v0, v3, 0x4

    .line 1053696
    if-nez p1, :cond_6

    .line 1053697
    add-int/lit8 v0, v0, -0x4

    move v1, v0

    .line 1053698
    :goto_0
    new-array v5, v1, [D

    .line 1053699
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    .line 1053700
    add-int/lit8 v4, v1, -0x2

    iget-wide v6, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v6, v7}, LX/31h;->d(D)F

    move-result v6

    float-to-double v6, v6

    aput-wide v6, v5, v4

    .line 1053701
    add-int/lit8 v4, v1, -0x1

    iget-wide v6, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v6, v7}, LX/31h;->b(D)F

    move-result v0

    float-to-double v6, v0

    aput-wide v6, v5, v4

    .line 1053702
    add-int/lit8 v6, v3, -0x1

    move v3, v2

    move v4, v2

    :goto_1
    if-ge v3, v6, :cond_1

    .line 1053703
    if-nez v4, :cond_0

    add-int/lit8 v0, v1, -0x2

    .line 1053704
    :goto_2
    add-int/lit8 v7, v4, 0x1

    add-int/lit8 v8, v0, 0x1

    aget-wide v10, v5, v0

    aput-wide v10, v5, v4

    .line 1053705
    add-int/lit8 v4, v7, 0x1

    aget-wide v8, v5, v8

    aput-wide v8, v5, v7

    .line 1053706
    add-int/lit8 v0, v3, 0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/model/LatLng;

    .line 1053707
    add-int/lit8 v7, v4, 0x1

    iget-wide v8, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v8, v9}, LX/31h;->d(D)F

    move-result v8

    float-to-double v8, v8

    aput-wide v8, v5, v4

    .line 1053708
    add-int/lit8 v4, v7, 0x1

    iget-wide v8, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v8, v9}, LX/31h;->b(D)F

    move-result v0

    float-to-double v8, v0

    aput-wide v8, v5, v7

    .line 1053709
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1053710
    :cond_0
    add-int/lit8 v0, v4, -0x2

    goto :goto_2

    .line 1053711
    :cond_1
    if-eqz p1, :cond_2

    .line 1053712
    add-int/lit8 v0, v4, -0x2

    .line 1053713
    add-int/lit8 v3, v4, 0x1

    add-int/lit8 v6, v0, 0x1

    aget-wide v8, v5, v0

    aput-wide v8, v5, v4

    .line 1053714
    aget-wide v6, v5, v6

    aput-wide v6, v5, v3

    :cond_2
    move v0, v2

    .line 1053715
    :goto_3
    if-ge v0, v1, :cond_5

    .line 1053716
    add-int/lit8 v2, v0, 0x2

    aget-wide v2, v5, v2

    aget-wide v6, v5, v0

    sub-double/2addr v2, v6

    .line 1053717
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpl-double v4, v2, v6

    if-lez v4, :cond_4

    .line 1053718
    aget-wide v2, v5, v0

    add-double/2addr v2, v12

    aput-wide v2, v5, v0

    .line 1053719
    :cond_3
    :goto_4
    add-int/lit8 v0, v0, 0x4

    goto :goto_3

    .line 1053720
    :cond_4
    const-wide/high16 v6, -0x4020000000000000L    # -0.5

    cmpg-double v2, v2, v6

    if-gez v2, :cond_3

    .line 1053721
    add-int/lit8 v2, v0, 0x2

    aget-wide v6, v5, v2

    add-double/2addr v6, v12

    aput-wide v6, v5, v2

    goto :goto_4

    .line 1053722
    :cond_5
    return-object v5

    :cond_6
    move v1, v0

    goto/16 :goto_0
.end method

.method public static a([D[D)[LX/31i;
    .locals 18

    .prologue
    .line 1053667
    move-object/from16 v0, p0

    array-length v5, v0

    .line 1053668
    div-int/lit8 v2, v5, 0x4

    new-array v6, v2, [LX/31i;

    .line 1053669
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1053670
    :goto_0
    if-ge v4, v5, :cond_4

    .line 1053671
    aget-wide v8, p1, v2

    .line 1053672
    add-int/lit8 v7, v2, 0x1

    aget-wide v10, p1, v7

    .line 1053673
    const-wide v12, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v10

    invoke-static {v10, v11}, LX/31h;->d(D)F

    move-result v7

    float-to-double v10, v7

    .line 1053674
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->asin(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v8

    invoke-static {v8, v9}, LX/31h;->b(D)F

    move-result v7

    float-to-double v8, v7

    .line 1053675
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    add-double/2addr v12, v10

    .line 1053676
    new-instance v7, LX/31i;

    invoke-direct {v7}, LX/31i;-><init>()V

    .line 1053677
    aget-wide v14, p0, v4

    add-int/lit8 v16, v4, 0x2

    aget-wide v16, p0, v16

    cmpg-double v14, v14, v16

    if-gez v14, :cond_1

    .line 1053678
    aget-wide v14, p0, v4

    iput-wide v14, v7, LX/31i;->c:D

    .line 1053679
    add-int/lit8 v14, v4, 0x2

    aget-wide v14, p0, v14

    iput-wide v14, v7, LX/31i;->d:D

    .line 1053680
    :goto_1
    add-int/lit8 v14, v4, 0x1

    aget-wide v14, p0, v14

    add-int/lit8 v16, v4, 0x3

    aget-wide v16, p0, v16

    cmpg-double v14, v14, v16

    if-gez v14, :cond_2

    .line 1053681
    add-int/lit8 v14, v4, 0x1

    aget-wide v14, p0, v14

    iput-wide v14, v7, LX/31i;->a:D

    .line 1053682
    add-int/lit8 v14, v4, 0x3

    aget-wide v14, p0, v14

    iput-wide v14, v7, LX/31i;->b:D

    .line 1053683
    :goto_2
    iget-wide v14, v7, LX/31i;->c:D

    sub-double/2addr v14, v10

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v14

    iget-wide v0, v7, LX/31i;->d:D

    move-wide/from16 v16, v0

    sub-double v10, v16, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    cmpg-double v10, v14, v10

    if-gtz v10, :cond_3

    .line 1053684
    iput-wide v8, v7, LX/31i;->a:D

    .line 1053685
    :cond_0
    :goto_3
    aput-object v7, v6, v3

    .line 1053686
    add-int/lit8 v2, v2, 0x2

    add-int/lit8 v4, v4, 0x4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1053687
    :cond_1
    add-int/lit8 v14, v4, 0x2

    aget-wide v14, p0, v14

    iput-wide v14, v7, LX/31i;->c:D

    .line 1053688
    aget-wide v14, p0, v4

    iput-wide v14, v7, LX/31i;->d:D

    goto :goto_1

    .line 1053689
    :cond_2
    add-int/lit8 v14, v4, 0x3

    aget-wide v14, p0, v14

    iput-wide v14, v7, LX/31i;->a:D

    .line 1053690
    add-int/lit8 v14, v4, 0x1

    aget-wide v14, p0, v14

    iput-wide v14, v7, LX/31i;->b:D

    goto :goto_2

    .line 1053691
    :cond_3
    iget-wide v10, v7, LX/31i;->c:D

    sub-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    iget-wide v14, v7, LX/31i;->d:D

    sub-double v12, v14, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->floor(D)D

    move-result-wide v12

    cmpg-double v10, v10, v12

    if-gtz v10, :cond_0

    .line 1053692
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    sub-double v8, v10, v8

    iput-wide v8, v7, LX/31i;->b:D

    goto :goto_3

    .line 1053693
    :cond_4
    return-object v6
.end method
