.class public final LX/5un;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/5uo;

.field public g:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation
.end field

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5uo;Ljava/lang/String;Ljava/lang/String;LX/0Px;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5uo;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1020142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1020143
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/5un;->a:LX/0am;

    .line 1020144
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/5un;->b:LX/0am;

    .line 1020145
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/5un;->c:LX/0am;

    .line 1020146
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/5un;->d:LX/0am;

    .line 1020147
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/5un;->e:LX/0am;

    .line 1020148
    iput-object p1, p0, LX/5un;->f:LX/5uo;

    .line 1020149
    iput-object p2, p0, LX/5un;->g:Ljava/lang/String;

    .line 1020150
    iput-object p3, p0, LX/5un;->h:Ljava/lang/String;

    .line 1020151
    iput-object p4, p0, LX/5un;->i:LX/0Px;

    .line 1020152
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/saved/server/UpdateSavedStateParams;
    .locals 2

    .prologue
    .line 1020153
    new-instance v0, Lcom/facebook/saved/server/UpdateSavedStateParams;

    invoke-direct {v0, p0}, Lcom/facebook/saved/server/UpdateSavedStateParams;-><init>(LX/5un;)V

    return-object v0
.end method
