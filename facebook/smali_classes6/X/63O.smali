.class public LX/63O;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements LX/0h5;


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;

.field private b:Landroid/widget/ImageButton;

.field private c:Lcom/facebook/resources/ui/FbButton;

.field public d:LX/63W;

.field public e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1043233
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1043234
    invoke-direct {p0}, LX/63O;->a()V

    .line 1043235
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1043230
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1043231
    invoke-direct {p0}, LX/63O;->a()V

    .line 1043232
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1043228
    invoke-direct {p0, p1, p2}, LX/63O;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1043229
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1043220
    invoke-virtual {p0}, LX/63O;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0303f8

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1043221
    const v0, 0x7f0d0841

    invoke-virtual {p0, v0}, LX/63O;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/63O;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1043222
    const v0, 0x7f0d0c3e

    invoke-virtual {p0, v0}, LX/63O;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    .line 1043223
    const v0, 0x7f0d0012

    invoke-virtual {p0, v0}, LX/63O;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    .line 1043224
    new-instance v0, LX/63N;

    invoke-direct {v0, p0}, LX/63N;-><init>(LX/63O;)V

    .line 1043225
    iget-object v1, p0, LX/63O;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1043226
    iget-object v1, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1043227
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1043219
    return-void
.end method

.method public final d_(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1043218
    const/4 v0, 0x0

    return-object v0
.end method

.method public setButtonSpecs(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1043155
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043156
    iget-object v0, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    sget-object v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-ne v0, v1, :cond_4

    .line 1043157
    :cond_1
    iget-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1043158
    iget-object v0, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1043159
    :cond_2
    :goto_1
    return-void

    .line 1043160
    :cond_3
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    goto :goto_0

    .line 1043161
    :cond_4
    iget-object v0, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043162
    iget v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v0, v1

    .line 1043163
    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    .line 1043164
    iget-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043165
    iget p1, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v1, p1

    .line 1043166
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1043167
    iget-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1043168
    iget-object v0, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1043169
    :cond_5
    :goto_2
    iget-object v0, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043170
    iget-boolean v2, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    move v1, v2

    .line 1043171
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setSelected(Z)V

    .line 1043172
    iget-object v0, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043173
    iget-boolean v2, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    move v1, v2

    .line 1043174
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1043175
    iget-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043176
    iget-boolean v2, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    move v1, v2

    .line 1043177
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 1043178
    iget-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043179
    iget-boolean v2, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    move v1, v2

    .line 1043180
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1043181
    iget-object v0, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043182
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    move-object v0, v1

    .line 1043183
    if-eqz v0, :cond_2

    .line 1043184
    iget-object v0, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043185
    iget-object v2, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1043186
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1043187
    iget-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043188
    iget-object v2, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    move-object v1, v2

    .line 1043189
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1043190
    :cond_6
    iget-object v0, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043191
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v1

    .line 1043192
    if-eqz v0, :cond_7

    .line 1043193
    iget-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043194
    iget-object p1, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    move-object v1, p1

    .line 1043195
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1043196
    iget-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1043197
    iget-object v0, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_2

    .line 1043198
    :cond_7
    iget-object v0, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043199
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v0, v1

    .line 1043200
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1043201
    iget-object v0, p0, LX/63O;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1043202
    iget-object v0, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/63O;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043203
    iget-object v2, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v1, v2

    .line 1043204
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1043205
    iget-object v0, p0, LX/63O;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_2
.end method

.method public setCustomTitleView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1043217
    return-void
.end method

.method public setHasBackButton(Z)V
    .locals 0

    .prologue
    .line 1043216
    return-void
.end method

.method public setHasFbLogo(Z)V
    .locals 0

    .prologue
    .line 1043215
    return-void
.end method

.method public setOnBackPressedListener(LX/63J;)V
    .locals 0

    .prologue
    .line 1043214
    return-void
.end method

.method public setOnToolbarButtonListener(LX/63W;)V
    .locals 0

    .prologue
    .line 1043212
    iput-object p1, p0, LX/63O;->d:LX/63W;

    .line 1043213
    return-void
.end method

.method public setShowDividers(Z)V
    .locals 0

    .prologue
    .line 1043211
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 1043209
    iget-object v0, p0, LX/63O;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1043210
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1043207
    iget-object v0, p0, LX/63O;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1043208
    return-void
.end method

.method public setTitlebarAsModal(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1043206
    return-void
.end method
