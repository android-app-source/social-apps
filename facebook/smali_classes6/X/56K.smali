.class public final LX/56K;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 837566
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 837567
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 837568
    :goto_0
    return v1

    .line 837569
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 837570
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 837571
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 837572
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 837573
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 837574
    const-string v7, "description"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 837575
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 837576
    :cond_2
    const-string v7, "icon_image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 837577
    invoke-static {p0, p1}, LX/56I;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 837578
    :cond_3
    const-string v7, "label"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 837579
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 837580
    :cond_4
    const-string v7, "legacy_graph_api_privacy_json"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 837581
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 837582
    :cond_5
    const-string v7, "privacy_options"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 837583
    invoke-static {p0, p1}, LX/56J;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 837584
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 837585
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 837586
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 837587
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 837588
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 837589
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 837590
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 837591
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 837592
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 837593
    if-eqz v0, :cond_0

    .line 837594
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837595
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837596
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837597
    if-eqz v0, :cond_1

    .line 837598
    const-string v1, "icon_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837599
    invoke-static {p0, v0, p2}, LX/56I;->a(LX/15i;ILX/0nX;)V

    .line 837600
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 837601
    if-eqz v0, :cond_2

    .line 837602
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837603
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837604
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 837605
    if-eqz v0, :cond_3

    .line 837606
    const-string v1, "legacy_graph_api_privacy_json"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837607
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837608
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837609
    if-eqz v0, :cond_4

    .line 837610
    const-string v1, "privacy_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837611
    invoke-static {p0, v0, p2, p3}, LX/63w;->a$redex0(LX/15i;ILX/0nX;LX/0my;)V

    .line 837612
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 837613
    return-void
.end method
