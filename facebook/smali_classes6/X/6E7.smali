.class public LX/6E7;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/6E6;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:LX/6qh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1065818
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1065819
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1065816
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1065817
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1065814
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1065815
    return-void
.end method


# virtual methods
.method public final a(LX/73T;)V
    .locals 1

    .prologue
    .line 1065812
    iget-object v0, p0, LX/6E7;->a:LX/6qh;

    invoke-virtual {v0, p1}, LX/6qh;->a(LX/73T;)V

    .line 1065813
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1065810
    iget-object v0, p0, LX/6E7;->a:LX/6qh;

    invoke-virtual {v0, p1}, LX/6qh;->b(Landroid/content/Intent;)V

    .line 1065811
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 1065806
    iget-object v0, p0, LX/6E7;->a:LX/6qh;

    invoke-virtual {v0, p1, p2}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 1065807
    return-void
.end method

.method public setPaymentsComponentCallback(LX/6qh;)V
    .locals 0

    .prologue
    .line 1065808
    iput-object p1, p0, LX/6E7;->a:LX/6qh;

    .line 1065809
    return-void
.end method
