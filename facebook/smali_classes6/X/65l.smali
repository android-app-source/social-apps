.class public final LX/65l;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[LX/65j;

.field public b:I

.field public c:I

.field public d:I

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/671;

.field private final g:I

.field public h:I


# direct methods
.method private constructor <init>(IILX/65D;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1048585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048586
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/65l;->e:Ljava/util/List;

    .line 1048587
    const/16 v0, 0x8

    new-array v0, v0, [LX/65j;

    iput-object v0, p0, LX/65l;->a:[LX/65j;

    .line 1048588
    iget-object v0, p0, LX/65l;->a:[LX/65j;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/65l;->b:I

    .line 1048589
    iput v1, p0, LX/65l;->c:I

    .line 1048590
    iput v1, p0, LX/65l;->d:I

    .line 1048591
    iput p1, p0, LX/65l;->g:I

    .line 1048592
    iput p2, p0, LX/65l;->h:I

    .line 1048593
    invoke-static {p3}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v0

    iput-object v0, p0, LX/65l;->f:LX/671;

    .line 1048594
    return-void
.end method

.method public constructor <init>(ILX/65D;)V
    .locals 0

    .prologue
    .line 1048583
    invoke-direct {p0, p1, p1, p2}, LX/65l;-><init>(IILX/65D;)V

    .line 1048584
    return-void
.end method

.method private a(II)I
    .locals 3

    .prologue
    .line 1048573
    and-int v0, p1, p2

    .line 1048574
    if-ge v0, p2, :cond_0

    .line 1048575
    :goto_0
    return v0

    .line 1048576
    :cond_0
    const/4 v0, 0x0

    .line 1048577
    :goto_1
    invoke-direct {p0}, LX/65l;->g()I

    move-result v1

    .line 1048578
    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_1

    .line 1048579
    and-int/lit8 v1, v1, 0x7f

    shl-int/2addr v1, v0

    add-int/2addr p2, v1

    .line 1048580
    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 1048581
    :cond_1
    shl-int v0, v1, v0

    add-int/2addr v0, p2

    .line 1048582
    goto :goto_0
.end method

.method public static a(LX/65l;I)I
    .locals 6

    .prologue
    .line 1048562
    const/4 v1, 0x0

    .line 1048563
    if-lez p1, :cond_1

    .line 1048564
    iget-object v0, p0, LX/65l;->a:[LX/65j;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iget v2, p0, LX/65l;->b:I

    if-lt v0, v2, :cond_0

    if-lez p1, :cond_0

    .line 1048565
    iget-object v2, p0, LX/65l;->a:[LX/65j;

    aget-object v2, v2, v0

    iget v2, v2, LX/65j;->j:I

    sub-int/2addr p1, v2

    .line 1048566
    iget v2, p0, LX/65l;->d:I

    iget-object v3, p0, LX/65l;->a:[LX/65j;

    aget-object v3, v3, v0

    iget v3, v3, LX/65j;->j:I

    sub-int/2addr v2, v3

    iput v2, p0, LX/65l;->d:I

    .line 1048567
    iget v2, p0, LX/65l;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/65l;->c:I

    .line 1048568
    add-int/lit8 v1, v1, 0x1

    .line 1048569
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1048570
    :cond_0
    iget-object v0, p0, LX/65l;->a:[LX/65j;

    iget v2, p0, LX/65l;->b:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, LX/65l;->a:[LX/65j;

    iget v4, p0, LX/65l;->b:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v1

    iget v5, p0, LX/65l;->c:I

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1048571
    iget v0, p0, LX/65l;->b:I

    add-int/2addr v0, v1

    iput v0, p0, LX/65l;->b:I

    .line 1048572
    :cond_1
    return v1
.end method

.method public static a(LX/65l;ILX/65j;)V
    .locals 6

    .prologue
    const/4 v3, -0x1

    .line 1048541
    iget-object v0, p0, LX/65l;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1048542
    iget v0, p2, LX/65j;->j:I

    .line 1048543
    if-eq p1, v3, :cond_0

    .line 1048544
    iget-object v1, p0, LX/65l;->a:[LX/65j;

    invoke-static {p0, p1}, LX/65l;->c(LX/65l;I)I

    move-result v2

    aget-object v1, v1, v2

    iget v1, v1, LX/65j;->j:I

    sub-int/2addr v0, v1

    .line 1048545
    :cond_0
    iget v1, p0, LX/65l;->h:I

    if-le v0, v1, :cond_1

    .line 1048546
    invoke-static {p0}, LX/65l;->d(LX/65l;)V

    .line 1048547
    :goto_0
    return-void

    .line 1048548
    :cond_1
    iget v1, p0, LX/65l;->d:I

    add-int/2addr v1, v0

    iget v2, p0, LX/65l;->h:I

    sub-int/2addr v1, v2

    .line 1048549
    invoke-static {p0, v1}, LX/65l;->a(LX/65l;I)I

    move-result v1

    .line 1048550
    if-ne p1, v3, :cond_3

    .line 1048551
    iget v1, p0, LX/65l;->c:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, LX/65l;->a:[LX/65j;

    array-length v2, v2

    if-le v1, v2, :cond_2

    .line 1048552
    iget-object v1, p0, LX/65l;->a:[LX/65j;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [LX/65j;

    .line 1048553
    iget-object v2, p0, LX/65l;->a:[LX/65j;

    const/4 v3, 0x0

    iget-object v4, p0, LX/65l;->a:[LX/65j;

    array-length v4, v4

    iget-object v5, p0, LX/65l;->a:[LX/65j;

    array-length v5, v5

    invoke-static {v2, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1048554
    iget-object v2, p0, LX/65l;->a:[LX/65j;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, LX/65l;->b:I

    .line 1048555
    iput-object v1, p0, LX/65l;->a:[LX/65j;

    .line 1048556
    :cond_2
    iget v1, p0, LX/65l;->b:I

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, LX/65l;->b:I

    .line 1048557
    iget-object v2, p0, LX/65l;->a:[LX/65j;

    aput-object p2, v2, v1

    .line 1048558
    iget v1, p0, LX/65l;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/65l;->c:I

    .line 1048559
    :goto_1
    iget v1, p0, LX/65l;->d:I

    add-int/2addr v0, v1

    iput v0, p0, LX/65l;->d:I

    goto :goto_0

    .line 1048560
    :cond_3
    invoke-static {p0, p1}, LX/65l;->c(LX/65l;I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, p1

    .line 1048561
    iget-object v2, p0, LX/65l;->a:[LX/65j;

    aput-object p2, v2, v1

    goto :goto_1
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 1048447
    invoke-static {p1}, LX/65l;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1048448
    sget-object v0, LX/65n;->a:[LX/65j;

    aget-object v0, v0, p1

    .line 1048449
    iget-object v1, p0, LX/65l;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1048450
    :goto_0
    return-void

    .line 1048451
    :cond_0
    sget-object v0, LX/65n;->a:[LX/65j;

    array-length v0, v0

    sub-int v0, p1, v0

    invoke-static {p0, v0}, LX/65l;->c(LX/65l;I)I

    move-result v0

    .line 1048452
    if-ltz v0, :cond_1

    iget-object v1, p0, LX/65l;->a:[LX/65j;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_2

    .line 1048453
    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Header index too large "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048454
    :cond_2
    iget-object v1, p0, LX/65l;->e:Ljava/util/List;

    iget-object v2, p0, LX/65l;->a:[LX/65j;

    aget-object v0, v2, v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static c(LX/65l;I)I
    .locals 1

    .prologue
    .line 1048540
    iget v0, p0, LX/65l;->b:I

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, p1

    return v0
.end method

.method public static d(LX/65l;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1048534
    iget-object v0, p0, LX/65l;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1048535
    iget-object v0, p0, LX/65l;->a:[LX/65j;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1048536
    iget-object v0, p0, LX/65l;->a:[LX/65j;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/65l;->b:I

    .line 1048537
    iput v2, p0, LX/65l;->c:I

    .line 1048538
    iput v2, p0, LX/65l;->d:I

    .line 1048539
    return-void
.end method

.method public static f(LX/65l;I)LX/673;
    .locals 2

    .prologue
    .line 1048531
    invoke-static {p1}, LX/65l;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1048532
    sget-object v0, LX/65n;->a:[LX/65j;

    aget-object v0, v0, p1

    iget-object v0, v0, LX/65j;->h:LX/673;

    .line 1048533
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/65l;->a:[LX/65j;

    sget-object v1, LX/65n;->a:[LX/65j;

    array-length v1, v1

    sub-int v1, p1, v1

    invoke-static {p0, v1}, LX/65l;->c(LX/65l;I)I

    move-result v1

    aget-object v0, v0, v1

    iget-object v0, v0, LX/65j;->h:LX/673;

    goto :goto_0
.end method

.method private g()I
    .locals 1

    .prologue
    .line 1048530
    iget-object v0, p0, LX/65l;->f:LX/671;

    invoke-interface {v0}, LX/671;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private static g(I)Z
    .locals 1

    .prologue
    .line 1048529
    if-ltz p0, :cond_0

    sget-object v0, LX/65n;->a:[LX/65j;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/65l;)LX/673;
    .locals 7

    .prologue
    .line 1048495
    invoke-direct {p0}, LX/65l;->g()I

    move-result v1

    .line 1048496
    and-int/lit16 v0, v1, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    .line 1048497
    :goto_0
    const/16 v2, 0x7f

    invoke-direct {p0, v1, v2}, LX/65l;->a(II)I

    move-result v1

    .line 1048498
    if-eqz v0, :cond_5

    .line 1048499
    sget-object v0, LX/65v;->c:LX/65v;

    move-object v0, v0

    .line 1048500
    iget-object v2, p0, LX/65l;->f:LX/671;

    int-to-long v4, v1

    invoke-interface {v2, v4, v5}, LX/671;->e(J)[B

    move-result-object v1

    const/4 v2, 0x0

    .line 1048501
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1048502
    iget-object v3, v0, LX/65v;->d:LX/65u;

    move v4, v2

    move-object v5, v3

    move v3, v2

    .line 1048503
    :goto_1
    array-length p0, v1

    if-ge v2, p0, :cond_2

    .line 1048504
    aget-byte p0, v1, v2

    and-int/lit16 p0, p0, 0xff

    .line 1048505
    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v4, p0

    .line 1048506
    add-int/lit8 v3, v3, 0x8

    .line 1048507
    :goto_2
    const/16 p0, 0x8

    if-lt v3, p0, :cond_1

    .line 1048508
    add-int/lit8 p0, v3, -0x8

    ushr-int p0, v4, p0

    and-int/lit16 p0, p0, 0xff

    .line 1048509
    iget-object v5, v5, LX/65u;->a:[LX/65u;

    aget-object v5, v5, p0

    .line 1048510
    iget-object p0, v5, LX/65u;->a:[LX/65u;

    if-nez p0, :cond_0

    .line 1048511
    iget p0, v5, LX/65u;->b:I

    invoke-virtual {v6, p0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 1048512
    iget v5, v5, LX/65u;->c:I

    sub-int/2addr v3, v5

    .line 1048513
    iget-object v5, v0, LX/65v;->d:LX/65u;

    goto :goto_2

    .line 1048514
    :cond_0
    add-int/lit8 v3, v3, -0x8

    .line 1048515
    goto :goto_2

    .line 1048516
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1048517
    :cond_2
    :goto_3
    if-lez v3, :cond_3

    .line 1048518
    rsub-int/lit8 v2, v3, 0x8

    shl-int v2, v4, v2

    and-int/lit16 v2, v2, 0xff

    .line 1048519
    iget-object v5, v5, LX/65u;->a:[LX/65u;

    aget-object v2, v5, v2

    .line 1048520
    iget-object v5, v2, LX/65u;->a:[LX/65u;

    if-nez v5, :cond_3

    iget v5, v2, LX/65u;->c:I

    if-gt v5, v3, :cond_3

    .line 1048521
    iget v5, v2, LX/65u;->b:I

    invoke-virtual {v6, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 1048522
    iget v2, v2, LX/65u;->c:I

    sub-int/2addr v3, v2

    .line 1048523
    iget-object v5, v0, LX/65v;->d:LX/65u;

    goto :goto_3

    .line 1048524
    :cond_3
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    move-object v0, v2

    .line 1048525
    invoke-static {v0}, LX/673;->a([B)LX/673;

    move-result-object v0

    .line 1048526
    :goto_4
    return-object v0

    .line 1048527
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 1048528
    :cond_5
    iget-object v0, p0, LX/65l;->f:LX/671;

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, LX/671;->c(J)LX/673;

    move-result-object v0

    goto :goto_4
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/16 v4, 0x80

    const/16 v3, 0x40

    .line 1048455
    :goto_0
    iget-object v0, p0, LX/65l;->f:LX/671;

    invoke-interface {v0}, LX/671;->e()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1048456
    iget-object v0, p0, LX/65l;->f:LX/671;

    invoke-interface {v0}, LX/671;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 1048457
    if-ne v0, v4, :cond_0

    .line 1048458
    new-instance v0, Ljava/io/IOException;

    const-string v1, "index == 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048459
    :cond_0
    and-int/lit16 v1, v0, 0x80

    if-ne v1, v4, :cond_1

    .line 1048460
    const/16 v1, 0x7f

    invoke-direct {p0, v0, v1}, LX/65l;->a(II)I

    move-result v0

    .line 1048461
    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, LX/65l;->b(I)V

    goto :goto_0

    .line 1048462
    :cond_1
    if-ne v0, v3, :cond_2

    .line 1048463
    invoke-static {p0}, LX/65l;->h(LX/65l;)LX/673;

    move-result-object v0

    invoke-static {v0}, LX/65n;->b(LX/673;)LX/673;

    move-result-object v0

    .line 1048464
    invoke-static {p0}, LX/65l;->h(LX/65l;)LX/673;

    move-result-object v1

    .line 1048465
    const/4 v2, -0x1

    new-instance v5, LX/65j;

    invoke-direct {v5, v0, v1}, LX/65j;-><init>(LX/673;LX/673;)V

    invoke-static {p0, v2, v5}, LX/65l;->a(LX/65l;ILX/65j;)V

    .line 1048466
    goto :goto_0

    .line 1048467
    :cond_2
    and-int/lit8 v1, v0, 0x40

    if-ne v1, v3, :cond_3

    .line 1048468
    const/16 v1, 0x3f

    invoke-direct {p0, v0, v1}, LX/65l;->a(II)I

    move-result v0

    .line 1048469
    add-int/lit8 v0, v0, -0x1

    .line 1048470
    invoke-static {p0, v0}, LX/65l;->f(LX/65l;I)LX/673;

    move-result-object v1

    .line 1048471
    invoke-static {p0}, LX/65l;->h(LX/65l;)LX/673;

    move-result-object v2

    .line 1048472
    const/4 v5, -0x1

    new-instance v6, LX/65j;

    invoke-direct {v6, v1, v2}, LX/65j;-><init>(LX/673;LX/673;)V

    invoke-static {p0, v5, v6}, LX/65l;->a(LX/65l;ILX/65j;)V

    .line 1048473
    goto :goto_0

    .line 1048474
    :cond_3
    and-int/lit8 v1, v0, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    .line 1048475
    const/16 v1, 0x1f

    invoke-direct {p0, v0, v1}, LX/65l;->a(II)I

    move-result v0

    iput v0, p0, LX/65l;->h:I

    .line 1048476
    iget v0, p0, LX/65l;->h:I

    if-ltz v0, :cond_4

    iget v0, p0, LX/65l;->h:I

    iget v1, p0, LX/65l;->g:I

    if-le v0, v1, :cond_5

    .line 1048477
    :cond_4
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid dynamic table size update "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/65l;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048478
    :cond_5
    iget v0, p0, LX/65l;->h:I

    iget v1, p0, LX/65l;->d:I

    if-ge v0, v1, :cond_6

    .line 1048479
    iget v0, p0, LX/65l;->h:I

    if-nez v0, :cond_b

    .line 1048480
    invoke-static {p0}, LX/65l;->d(LX/65l;)V

    .line 1048481
    :cond_6
    :goto_1
    goto/16 :goto_0

    .line 1048482
    :cond_7
    const/16 v1, 0x10

    if-eq v0, v1, :cond_8

    if-nez v0, :cond_9

    .line 1048483
    :cond_8
    invoke-static {p0}, LX/65l;->h(LX/65l;)LX/673;

    move-result-object v0

    invoke-static {v0}, LX/65n;->b(LX/673;)LX/673;

    move-result-object v0

    .line 1048484
    invoke-static {p0}, LX/65l;->h(LX/65l;)LX/673;

    move-result-object v1

    .line 1048485
    iget-object v2, p0, LX/65l;->e:Ljava/util/List;

    new-instance v5, LX/65j;

    invoke-direct {v5, v0, v1}, LX/65j;-><init>(LX/673;LX/673;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1048486
    goto/16 :goto_0

    .line 1048487
    :cond_9
    const/16 v1, 0xf

    invoke-direct {p0, v0, v1}, LX/65l;->a(II)I

    move-result v0

    .line 1048488
    add-int/lit8 v0, v0, -0x1

    .line 1048489
    invoke-static {p0, v0}, LX/65l;->f(LX/65l;I)LX/673;

    move-result-object v1

    .line 1048490
    invoke-static {p0}, LX/65l;->h(LX/65l;)LX/673;

    move-result-object v2

    .line 1048491
    iget-object v5, p0, LX/65l;->e:Ljava/util/List;

    new-instance v6, LX/65j;

    invoke-direct {v6, v1, v2}, LX/65j;-><init>(LX/673;LX/673;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1048492
    goto/16 :goto_0

    .line 1048493
    :cond_a
    return-void

    .line 1048494
    :cond_b
    iget v0, p0, LX/65l;->d:I

    iget v1, p0, LX/65l;->h:I

    sub-int/2addr v0, v1

    invoke-static {p0, v0}, LX/65l;->a(LX/65l;I)I

    goto :goto_1
.end method
