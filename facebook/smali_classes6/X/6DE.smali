.class public final LX/6DE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/6CA;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/6CA;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1064756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064757
    iput-object p1, p0, LX/6DE;->a:LX/0QB;

    .line 1064758
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1064760
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6DE;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1064761
    packed-switch p2, :pswitch_data_0

    .line 1064762
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1064763
    :pswitch_0
    new-instance v2, LX/6CB;

    .line 1064764
    new-instance p2, LX/6Cw;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/6CH;->b(LX/0QB;)LX/6CH;

    move-result-object v1

    check-cast v1, LX/6CH;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p0

    check-cast p0, LX/0tX;

    invoke-direct {p2, v0, v1, p0}, LX/6Cw;-><init>(Landroid/content/Context;LX/6CH;LX/0tX;)V

    .line 1064765
    move-object v0, p2

    .line 1064766
    check-cast v0, LX/6Cw;

    invoke-static {p1}, LX/6CH;->b(LX/0QB;)LX/6CH;

    move-result-object v1

    check-cast v1, LX/6CH;

    invoke-direct {v2, v0, v1}, LX/6CB;-><init>(LX/6Cw;LX/6CH;)V

    .line 1064767
    move-object v0, v2

    .line 1064768
    :goto_0
    return-object v0

    .line 1064769
    :pswitch_1
    invoke-static {p1}, LX/6CH;->b(LX/0QB;)LX/6CH;

    move-result-object v0

    goto :goto_0

    .line 1064770
    :pswitch_2
    invoke-static {p1}, LX/6Ef;->a(LX/0QB;)LX/6Ef;

    move-result-object v0

    goto :goto_0

    .line 1064771
    :pswitch_3
    new-instance v1, LX/6FD;

    invoke-static {p1}, LX/6FE;->a(LX/0QB;)LX/6FE;

    move-result-object v0

    check-cast v0, LX/6FE;

    invoke-direct {v1, v0}, LX/6FD;-><init>(LX/6FE;)V

    .line 1064772
    move-object v0, v1

    .line 1064773
    goto :goto_0

    .line 1064774
    :pswitch_4
    invoke-static {p1}, LX/7i5;->a(LX/0QB;)LX/7i5;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1064759
    const/4 v0, 0x5

    return v0
.end method
