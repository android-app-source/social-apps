.class public final LX/5p2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5p1;


# instance fields
.field public final synthetic a:LX/5p5;

.field public b:Ljava/lang/reflect/Method;

.field private final c:[LX/5or;

.field public final d:Ljava/lang/String;

.field private final e:[Ljava/lang/Object;

.field private f:Ljava/lang/String;

.field private final g:I

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/5p5;Ljava/lang/reflect/Method;)V
    .locals 2

    .prologue
    .line 1007737
    iput-object p1, p0, LX/5p2;->a:LX/5p5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007738
    const-string v0, "async"

    iput-object v0, p0, LX/5p2;->f:Ljava/lang/String;

    .line 1007739
    iput-object p2, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    .line 1007740
    iget-object v0, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1007741
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    .line 1007742
    invoke-direct {p0, v0}, LX/5p2;->b([Ljava/lang/Class;)[LX/5or;

    move-result-object v1

    iput-object v1, p0, LX/5p2;->c:[LX/5or;

    .line 1007743
    invoke-direct {p0, v0}, LX/5p2;->a([Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/5p2;->d:Ljava/lang/String;

    .line 1007744
    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/5p2;->e:[Ljava/lang/Object;

    .line 1007745
    invoke-direct {p0}, LX/5p2;->c()I

    move-result v0

    iput v0, p0, LX/5p2;->g:I

    .line 1007746
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LX/5p5;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5p2;->h:Ljava/lang/String;

    .line 1007747
    return-void
.end method

.method private static a(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1007736
    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int v1, p0, p1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a([Ljava/lang/Class;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1007719
    new-instance v3, Ljava/lang/StringBuilder;

    array-length v0, p1

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1007720
    const-string v0, "v."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 1007721
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_3

    .line 1007722
    aget-object v4, p1, v0

    .line 1007723
    const-class v2, Lcom/facebook/react/bridge/ExecutorToken;

    if-ne v4, v2, :cond_0

    .line 1007724
    iget-object v2, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v2}, LX/5p5;->bP_()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1007725
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Module "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " doesn\'t support web workers, but "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " takes an ExecutorToken."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007726
    :cond_0
    const-class v2, LX/5pW;

    if-ne v4, v2, :cond_1

    .line 1007727
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string v5, "Promise must be used as last parameter only"

    invoke-static {v2, v5}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1007728
    const-string v2, "promise"

    iput-object v2, p0, LX/5p2;->f:Ljava/lang/String;

    .line 1007729
    :cond_1
    invoke-static {v4}, LX/5p5;->c(Ljava/lang/Class;)C

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1007730
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v2, v1

    .line 1007731
    goto :goto_1

    .line 1007732
    :cond_3
    iget-object v0, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v0}, LX/5p5;->bP_()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1007733
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x54

    if-eq v0, v1, :cond_4

    .line 1007734
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Module "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " supports web workers, but "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "does not take an ExecutorToken as its first parameter."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007735
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b([Ljava/lang/Class;)[LX/5or;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1007688
    iget-object v0, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v0}, LX/5p5;->bP_()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1007689
    aget-object v0, p1, v2

    const-class v3, Lcom/facebook/react/bridge/ExecutorToken;

    if-eq v0, v3, :cond_0

    .line 1007690
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Module "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " supports web workers, but "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "does not take an ExecutorToken as its first parameter."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    .line 1007691
    :goto_0
    array-length v3, p1

    sub-int/2addr v3, v0

    new-array v5, v3, [LX/5or;

    move v3, v2

    .line 1007692
    :goto_1
    array-length v4, p1

    sub-int/2addr v4, v0

    if-ge v3, v4, :cond_f

    .line 1007693
    add-int v4, v3, v0

    .line 1007694
    aget-object v6, p1, v4

    .line 1007695
    const-class v7, Ljava/lang/Boolean;

    if-eq v6, v7, :cond_1

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v6, v7, :cond_2

    .line 1007696
    :cond_1
    sget-object v4, LX/5p5;->a:LX/5or;

    aput-object v4, v5, v3

    .line 1007697
    :goto_2
    aget-object v4, v5, v3

    invoke-virtual {v4}, LX/5or;->a()I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_1

    .line 1007698
    :cond_2
    const-class v7, Ljava/lang/Integer;

    if-eq v6, v7, :cond_3

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v6, v7, :cond_4

    .line 1007699
    :cond_3
    sget-object v4, LX/5p5;->d:LX/5or;

    aput-object v4, v5, v3

    goto :goto_2

    .line 1007700
    :cond_4
    const-class v7, Ljava/lang/Double;

    if-eq v6, v7, :cond_5

    sget-object v7, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne v6, v7, :cond_6

    .line 1007701
    :cond_5
    sget-object v4, LX/5p5;->b:LX/5or;

    aput-object v4, v5, v3

    goto :goto_2

    .line 1007702
    :cond_6
    const-class v7, Ljava/lang/Float;

    if-eq v6, v7, :cond_7

    sget-object v7, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne v6, v7, :cond_8

    .line 1007703
    :cond_7
    sget-object v4, LX/5p5;->c:LX/5or;

    aput-object v4, v5, v3

    goto :goto_2

    .line 1007704
    :cond_8
    const-class v7, Ljava/lang/String;

    if-ne v6, v7, :cond_9

    .line 1007705
    sget-object v4, LX/5p5;->e:LX/5or;

    aput-object v4, v5, v3

    goto :goto_2

    .line 1007706
    :cond_9
    const-class v7, Lcom/facebook/react/bridge/Callback;

    if-ne v6, v7, :cond_a

    .line 1007707
    sget-object v4, LX/5p5;->h:LX/5or;

    aput-object v4, v5, v3

    goto :goto_2

    .line 1007708
    :cond_a
    const-class v7, LX/5pW;

    if-ne v6, v7, :cond_c

    .line 1007709
    sget-object v6, LX/5p5;->i:LX/5or;

    aput-object v6, v5, v3

    .line 1007710
    array-length v6, p1

    add-int/lit8 v6, v6, -0x1

    if-ne v4, v6, :cond_b

    move v4, v1

    :goto_3
    const-string v6, "Promise must be used as last parameter only"

    invoke-static {v4, v6}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1007711
    const-string v4, "promise"

    iput-object v4, p0, LX/5p2;->f:Ljava/lang/String;

    goto :goto_2

    :cond_b
    move v4, v2

    .line 1007712
    goto :goto_3

    .line 1007713
    :cond_c
    const-class v4, LX/5pG;

    if-ne v6, v4, :cond_d

    .line 1007714
    sget-object v4, LX/5p5;->g:LX/5or;

    aput-object v4, v5, v3

    goto :goto_2

    .line 1007715
    :cond_d
    const-class v4, LX/5pC;

    if-ne v6, v4, :cond_e

    .line 1007716
    sget-object v4, LX/5p5;->f:LX/5or;

    aput-object v4, v5, v3

    goto :goto_2

    .line 1007717
    :cond_e
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got unknown argument class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007718
    :cond_f
    return-object v5

    :cond_10
    move v0, v2

    goto/16 :goto_0
.end method

.method private c()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1007684
    iget-object v2, p0, LX/5p2;->c:[LX/5or;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1007685
    invoke-virtual {v4}, LX/5or;->a()I

    move-result v4

    add-int/2addr v1, v4

    .line 1007686
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1007687
    :cond_0
    return v1
.end method


# virtual methods
.method public final a(Lcom/facebook/react/bridge/CatalystInstance;Lcom/facebook/react/bridge/ExecutorToken;Lcom/facebook/react/bridge/ReadableNativeArray;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x2000

    const/4 v1, 0x0

    .line 1007659
    const-string v0, "callJavaModuleMethod"

    invoke-static {v6, v7, v0}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v0

    const-string v2, "method"

    iget-object v3, p0, LX/5p2;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v0

    invoke-virtual {v0}, LX/0BN;->a()V

    .line 1007660
    :try_start_0
    iget v0, p0, LX/5p2;->g:I

    invoke-virtual {p3}, Lcom/facebook/react/bridge/ReadableNativeArray;->size()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 1007661
    new-instance v0, LX/5pT;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v2}, LX/5p5;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/react/bridge/ReadableNativeArray;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arguments, expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/5p2;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1007662
    :catchall_0
    move-exception v0

    invoke-static {v6, v7}, LX/018;->a(J)V

    throw v0

    .line 1007663
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v0}, LX/5p5;->bP_()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1007664
    iget-object v0, p0, LX/5p2;->e:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1007665
    const/4 v0, 0x1

    move v2, v1

    .line 1007666
    :goto_0
    :try_start_2
    iget-object v3, p0, LX/5p2;->c:[LX/5or;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 1007667
    iget-object v3, p0, LX/5p2;->e:[Ljava/lang/Object;

    add-int v4, v2, v0

    iget-object v5, p0, LX/5p2;->c:[LX/5or;

    aget-object v5, v5, v2

    invoke-virtual {v5, p1, p2, p3, v1}, LX/5or;->a(Lcom/facebook/react/bridge/CatalystInstance;Lcom/facebook/react/bridge/ExecutorToken;Lcom/facebook/react/bridge/ReadableNativeArray;I)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1007668
    iget-object v3, p0, LX/5p2;->c:[LX/5or;

    aget-object v3, v3, v2

    invoke-virtual {v3}, LX/5or;->a()I
    :try_end_2
    .catch Lcom/facebook/react/bridge/UnexpectedNativeTypeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    add-int/2addr v1, v3

    .line 1007669
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1007670
    :catch_0
    move-exception v0

    .line 1007671
    :try_start_3
    new-instance v3, LX/5pT;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/react/bridge/UnexpectedNativeTypeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (constructing arguments for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v5}, LX/5p5;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at argument index "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, LX/5p2;->c:[LX/5or;

    aget-object v2, v5, v2

    invoke-virtual {v2}, LX/5or;->a()I

    move-result v2

    invoke-static {v1, v2}, LX/5p2;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1, v0}, LX/5pT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1007672
    :cond_1
    :try_start_4
    iget-object v0, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    iget-object v1, p0, LX/5p2;->a:LX/5p5;

    iget-object v2, p0, LX/5p2;->e:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1007673
    invoke-static {v6, v7}, LX/018;->a(J)V

    .line 1007674
    return-void

    .line 1007675
    :catch_1
    move-exception v0

    .line 1007676
    :try_start_5
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not invoke "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v3}, LX/5p5;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1007677
    :catch_2
    move-exception v0

    .line 1007678
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not invoke "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v3}, LX/5p5;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 1007679
    :catch_3
    move-exception v0

    .line 1007680
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_2

    .line 1007681
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 1007682
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not invoke "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/5p2;->a:LX/5p5;

    invoke-virtual {v3}, LX/5p5;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/5p2;->b:Ljava/lang/reflect/Method;

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    move v0, v1

    move v2, v1

    goto/16 :goto_0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007683
    iget-object v0, p0, LX/5p2;->f:Ljava/lang/String;

    return-object v0
.end method
