.class public abstract LX/5SB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Landroid/os/ParcelUuid;

.field public final e:LX/5SA;


# direct methods
.method public constructor <init>(JJLjava/lang/String;LX/5SA;Landroid/os/ParcelUuid;)V
    .locals 1

    .prologue
    .line 917594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 917595
    iput-wide p1, p0, LX/5SB;->a:J

    .line 917596
    iput-wide p3, p0, LX/5SB;->b:J

    .line 917597
    iput-object p5, p0, LX/5SB;->c:Ljava/lang/String;

    .line 917598
    iput-object p6, p0, LX/5SB;->e:LX/5SA;

    .line 917599
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    iput-object v0, p0, LX/5SB;->d:Landroid/os/ParcelUuid;

    .line 917600
    return-void
.end method


# virtual methods
.method public abstract a()Z
.end method

.method public abstract b()Z
.end method

.method public abstract c()Z
.end method

.method public abstract d()Z
.end method

.method public abstract e()LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 917593
    iget-wide v0, p0, LX/5SB;->b:J

    return-wide v0
.end method

.method public final i()Z
    .locals 4

    .prologue
    .line 917592
    iget-wide v0, p0, LX/5SB;->b:J

    iget-wide v2, p0, LX/5SB;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
