.class public LX/6Lf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/Ivg;

.field private final b:LX/3ID;

.field public c:Z

.field private d:F

.field private e:F

.field private f:I

.field public final g:Landroid/view/VelocityTracker;

.field public h:F

.field public i:F

.field private j:F

.field private k:F


# direct methods
.method public constructor <init>(LX/Ivg;LX/3ID;)V
    .locals 1

    .prologue
    .line 1078627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078628
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, LX/6Lf;->g:Landroid/view/VelocityTracker;

    .line 1078629
    iput-object p2, p0, LX/6Lf;->b:LX/3ID;

    .line 1078630
    iput-object p1, p0, LX/6Lf;->a:LX/Ivg;

    .line 1078631
    return-void
.end method

.method private static b(LX/6Lf;Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 1078632
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1078633
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1078634
    iget-object v1, p0, LX/6Lf;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1078635
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1078636
    iget-object v2, p0, LX/6Lf;->a:LX/Ivg;

    if-nez v2, :cond_0

    .line 1078637
    :goto_0
    return v1

    .line 1078638
    :cond_0
    iget-boolean v2, p0, LX/6Lf;->c:Z

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1078639
    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_1

    .line 1078640
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p1

    .line 1078641
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1078642
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 1078643
    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 1078644
    :cond_2
    :goto_2
    :pswitch_0
    iget-object v0, p0, LX/6Lf;->b:LX/3ID;

    invoke-virtual {v0}, LX/3ID;->a()Z

    move-result v1

    goto :goto_0

    .line 1078645
    :pswitch_1
    iget-object v2, p0, LX/6Lf;->b:LX/3ID;

    invoke-virtual {v2, p1}, LX/3ID;->a(Landroid/view/MotionEvent;)Z

    .line 1078646
    iput-boolean v0, p0, LX/6Lf;->c:Z

    .line 1078647
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LX/6Lf;->j:F

    iput v0, p0, LX/6Lf;->d:F

    .line 1078648
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/6Lf;->k:F

    iput v0, p0, LX/6Lf;->e:F

    .line 1078649
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/6Lf;->f:I

    .line 1078650
    iget-object v0, p0, LX/6Lf;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 1078651
    invoke-static {p0, p1}, LX/6Lf;->b(LX/6Lf;Landroid/view/MotionEvent;)V

    goto :goto_2

    .line 1078652
    :pswitch_2
    iget-object v0, p0, LX/6Lf;->b:LX/3ID;

    invoke-virtual {v0, p1}, LX/3ID;->a(Landroid/view/MotionEvent;)Z

    .line 1078653
    invoke-static {p0, p1}, LX/6Lf;->b(LX/6Lf;Landroid/view/MotionEvent;)V

    .line 1078654
    iget v0, p0, LX/6Lf;->f:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 1078655
    if-ltz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1078656
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 1078657
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 1078658
    iget v2, p0, LX/6Lf;->j:F

    sub-float/2addr v2, v1

    .line 1078659
    iget v3, p0, LX/6Lf;->k:F

    sub-float/2addr v3, v0

    .line 1078660
    iget-object v4, p0, LX/6Lf;->b:LX/3ID;

    .line 1078661
    iget-boolean p1, v4, LX/3ID;->e:Z

    move v4, p1

    .line 1078662
    if-eqz v4, :cond_3

    .line 1078663
    iget-object v4, p0, LX/6Lf;->a:LX/Ivg;

    invoke-virtual {v4, v2}, LX/Ivg;->a(F)V

    .line 1078664
    :cond_3
    iget-object v2, p0, LX/6Lf;->b:LX/3ID;

    .line 1078665
    iget-boolean v4, v2, LX/3ID;->f:Z

    move v2, v4

    .line 1078666
    if-eqz v2, :cond_4

    .line 1078667
    iget-object v2, p0, LX/6Lf;->a:LX/Ivg;

    invoke-virtual {v2, v3}, LX/Ivg;->b(F)V

    .line 1078668
    :cond_4
    iput v1, p0, LX/6Lf;->j:F

    .line 1078669
    iput v0, p0, LX/6Lf;->k:F

    goto :goto_2

    .line 1078670
    :pswitch_3
    iput-boolean v1, p0, LX/6Lf;->c:Z

    .line 1078671
    invoke-static {p0, p1}, LX/6Lf;->b(LX/6Lf;Landroid/view/MotionEvent;)V

    .line 1078672
    iget-object v0, p0, LX/6Lf;->g:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 1078673
    iget-object v0, p0, LX/6Lf;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    iput v0, p0, LX/6Lf;->h:F

    .line 1078674
    iget-object v0, p0, LX/6Lf;->g:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    iput v0, p0, LX/6Lf;->i:F

    .line 1078675
    const/4 v0, -0x1

    iput v0, p0, LX/6Lf;->f:I

    .line 1078676
    iget-object v0, p0, LX/6Lf;->b:LX/3ID;

    .line 1078677
    iget-boolean v1, v0, LX/3ID;->e:Z

    move v0, v1

    .line 1078678
    if-eqz v0, :cond_5

    .line 1078679
    iget-object v0, p0, LX/6Lf;->a:LX/Ivg;

    iget v1, p0, LX/6Lf;->h:F

    invoke-virtual {v0, v1}, LX/Ivg;->c(F)V

    .line 1078680
    :cond_5
    iget-object v0, p0, LX/6Lf;->b:LX/3ID;

    .line 1078681
    iget-boolean v1, v0, LX/3ID;->f:Z

    move v0, v1

    .line 1078682
    if-eqz v0, :cond_6

    .line 1078683
    iget-object v0, p0, LX/6Lf;->a:LX/Ivg;

    iget v1, p0, LX/6Lf;->i:F

    invoke-virtual {v0, v1}, LX/Ivg;->d(F)V

    .line 1078684
    :cond_6
    iget-object v0, p0, LX/6Lf;->b:LX/3ID;

    invoke-virtual {v0, p1}, LX/3ID;->a(Landroid/view/MotionEvent;)Z

    goto/16 :goto_2

    .line 1078685
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v2

    .line 1078686
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 1078687
    iget v4, p0, LX/6Lf;->f:I

    if-ne v3, v4, :cond_2

    .line 1078688
    if-nez v2, :cond_7

    .line 1078689
    :goto_3
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, LX/6Lf;->d:F

    .line 1078690
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iput v1, p0, LX/6Lf;->e:F

    .line 1078691
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, LX/6Lf;->j:F

    .line 1078692
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iput v1, p0, LX/6Lf;->k:F

    .line 1078693
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, LX/6Lf;->f:I

    goto/16 :goto_2

    :cond_7
    move v0, v1

    .line 1078694
    goto :goto_3

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
