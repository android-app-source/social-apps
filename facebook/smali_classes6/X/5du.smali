.class public final LX/5du;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5dg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5dg",
        "<",
        "Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 966317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/PlatformMetadata;
    .locals 6

    .prologue
    .line 966318
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 966319
    invoke-virtual {p1}, LX/0lF;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 966320
    invoke-virtual {p1}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 966321
    const-string v3, "title"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "content_type"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "payload"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    const-string p0, "image_url"

    invoke-virtual {v0, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "data"

    invoke-virtual {v0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p1

    invoke-static {v3, v4, v5, p0, p1}, Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/QuickReplyItem;

    move-result-object v3

    move-object v0, v3

    .line 966322
    if-eqz v0, :cond_0

    .line 966323
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 966324
    :cond_1
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;-><init>(LX/0Px;)V

    return-object v0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966325
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966326
    new-array v0, p1, [Lcom/facebook/messaging/model/messagemetadata/QuickRepliesPlatformMetadata;

    return-object v0
.end method
