.class public final LX/5CU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 26

    .prologue
    .line 867905
    const/16 v22, 0x0

    .line 867906
    const/16 v21, 0x0

    .line 867907
    const/16 v20, 0x0

    .line 867908
    const/16 v19, 0x0

    .line 867909
    const/16 v18, 0x0

    .line 867910
    const/16 v17, 0x0

    .line 867911
    const/16 v16, 0x0

    .line 867912
    const/4 v15, 0x0

    .line 867913
    const/4 v14, 0x0

    .line 867914
    const/4 v13, 0x0

    .line 867915
    const/4 v12, 0x0

    .line 867916
    const/4 v11, 0x0

    .line 867917
    const/4 v10, 0x0

    .line 867918
    const/4 v9, 0x0

    .line 867919
    const/4 v8, 0x0

    .line 867920
    const/4 v7, 0x0

    .line 867921
    const/4 v6, 0x0

    .line 867922
    const/4 v5, 0x0

    .line 867923
    const/4 v4, 0x0

    .line 867924
    const/4 v3, 0x0

    .line 867925
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    .line 867926
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 867927
    const/4 v3, 0x0

    .line 867928
    :goto_0
    return v3

    .line 867929
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 867930
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_13

    .line 867931
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v23

    .line 867932
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 867933
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    if-eqz v23, :cond_1

    .line 867934
    const-string v24, "__type__"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_2

    const-string v24, "__typename"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 867935
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v22

    goto :goto_1

    .line 867936
    :cond_3
    const-string v24, "category_names"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 867937
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 867938
    :cond_4
    const-string v24, "city"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 867939
    invoke-static/range {p0 .. p1}, LX/5CP;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 867940
    :cond_5
    const-string v24, "id"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 867941
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto :goto_1

    .line 867942
    :cond_6
    const-string v24, "is_owned"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 867943
    const/4 v5, 0x1

    .line 867944
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto :goto_1

    .line 867945
    :cond_7
    const-string v24, "location"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 867946
    invoke-static/range {p0 .. p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 867947
    :cond_8
    const-string v24, "name"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 867948
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 867949
    :cond_9
    const-string v24, "overall_star_rating"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 867950
    invoke-static/range {p0 .. p1}, LX/5CQ;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 867951
    :cond_a
    const-string v24, "page_visits"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 867952
    invoke-static/range {p0 .. p1}, LX/5CR;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 867953
    :cond_b
    const-string v24, "profile_picture"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 867954
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 867955
    :cond_c
    const-string v24, "profile_picture_is_silhouette"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 867956
    const/4 v4, 0x1

    .line 867957
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 867958
    :cond_d
    const-string v24, "saved_collection"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 867959
    invoke-static/range {p0 .. p1}, LX/40i;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 867960
    :cond_e
    const-string v24, "should_show_reviews_on_profile"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 867961
    const/4 v3, 0x1

    .line 867962
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 867963
    :cond_f
    const-string v24, "super_category_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_10

    .line 867964
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 867965
    :cond_10
    const-string v24, "url"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 867966
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 867967
    :cond_11
    const-string v24, "viewer_saved_state"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_12

    .line 867968
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto/16 :goto_1

    .line 867969
    :cond_12
    const-string v24, "viewer_visits"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 867970
    invoke-static/range {p0 .. p1}, LX/5CS;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 867971
    :cond_13
    const/16 v23, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 867972
    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 867973
    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 867974
    const/16 v21, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 867975
    const/16 v20, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 867976
    if-eqz v5, :cond_14

    .line 867977
    const/4 v5, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 867978
    :cond_14
    const/4 v5, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 867979
    const/4 v5, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 867980
    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v15}, LX/186;->b(II)V

    .line 867981
    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->b(II)V

    .line 867982
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13}, LX/186;->b(II)V

    .line 867983
    if-eqz v4, :cond_15

    .line 867984
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 867985
    :cond_15
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 867986
    if-eqz v3, :cond_16

    .line 867987
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 867988
    :cond_16
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 867989
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 867990
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 867991
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 867992
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
