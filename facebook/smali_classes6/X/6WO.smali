.class public final LX/6WO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public b:F

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1105908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1105909
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x18

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/6WO;->a:Ljava/util/ArrayList;

    .line 1105910
    const/4 v0, 0x0

    iput v0, p0, LX/6WO;->b:F

    .line 1105911
    const/4 v0, -0x1

    iput v0, p0, LX/6WO;->c:I

    .line 1105912
    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 1105907
    invoke-direct {p0}, LX/6WO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IIII)LX/6WO;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/IntRange;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/IntRange;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/IntRange;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/IntRange;
        .end annotation
    .end param

    .prologue
    .line 1105894
    iget-object v0, p0, LX/6WO;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105895
    iget-object v0, p0, LX/6WO;->a:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105896
    iget-object v0, p0, LX/6WO;->a:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105897
    iget-object v0, p0, LX/6WO;->a:Ljava/util/ArrayList;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105898
    return-object p0
.end method

.method public final a()LX/6WP;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1105899
    iget v0, p0, LX/6WO;->c:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/6WO;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 1105900
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRows & setFirstItemAspectRatio are mutually exclusive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1105901
    :cond_0
    iget-object v0, p0, LX/6WO;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1105902
    new-array v4, v3, [I

    move v1, v2

    .line 1105903
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1105904
    iget-object v0, p0, LX/6WO;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v4, v1

    .line 1105905
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1105906
    :cond_1
    new-instance v0, LX/6WP;

    iget v1, p0, LX/6WO;->c:I

    iget v3, p0, LX/6WO;->b:F

    invoke-direct {v0, v1, v4, v3}, LX/6WP;-><init>(I[IF)V

    return-object v0
.end method
