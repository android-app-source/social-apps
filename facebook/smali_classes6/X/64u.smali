.class public final LX/64u;
.super LX/64t;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1046014
    invoke-direct {p0}, LX/64t;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/64c;LX/64R;LX/65W;)LX/65S;
    .locals 1

    .prologue
    .line 1045994
    invoke-virtual {p1, p2, p3}, LX/64c;->a(LX/64R;LX/65W;)LX/65S;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/64c;)LX/65T;
    .locals 1

    .prologue
    .line 1046013
    iget-object v0, p1, LX/64c;->a:LX/65T;

    return-object v0
.end method

.method public final a(LX/64y;)LX/65W;
    .locals 1

    .prologue
    .line 1046008
    check-cast p1, LX/64y;

    .line 1046009
    iget-object v0, p1, LX/64y;->c:LX/66R;

    .line 1046010
    iget-object p1, v0, LX/66R;->b:LX/65W;

    move-object v0, p1

    .line 1046011
    move-object v0, v0

    .line 1046012
    return-object v0
.end method

.method public final a(LX/64e;Ljavax/net/ssl/SSLSocket;Z)V
    .locals 1

    .prologue
    .line 1046002
    invoke-static {p1, p2, p3}, LX/64e;->b(LX/64e;Ljavax/net/ssl/SSLSocket;Z)LX/64e;

    move-result-object v0

    .line 1046003
    iget-object p0, v0, LX/64e;->h:[Ljava/lang/String;

    if-eqz p0, :cond_0

    .line 1046004
    iget-object p0, v0, LX/64e;->h:[Ljava/lang/String;

    invoke-virtual {p2, p0}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    .line 1046005
    :cond_0
    iget-object p0, v0, LX/64e;->g:[Ljava/lang/String;

    if-eqz p0, :cond_1

    .line 1046006
    iget-object v0, v0, LX/64e;->g:[Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljavax/net/ssl/SSLSocket;->setEnabledCipherSuites([Ljava/lang/String;)V

    .line 1046007
    :cond_1
    return-void
.end method

.method public final a(LX/64m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1046015
    const/4 p0, 0x1

    .line 1046016
    const-string v0, ":"

    invoke-virtual {p2, v0, p0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 1046017
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1046018
    const/4 v1, 0x0

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/64m;->b(Ljava/lang/String;Ljava/lang/String;)LX/64m;

    .line 1046019
    :goto_0
    return-void

    .line 1046020
    :cond_0
    const-string v0, ":"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1046021
    const-string v0, ""

    invoke-virtual {p2, p0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/64m;->b(Ljava/lang/String;Ljava/lang/String;)LX/64m;

    goto :goto_0

    .line 1046022
    :cond_1
    const-string v0, ""

    invoke-virtual {p1, v0, p2}, LX/64m;->b(Ljava/lang/String;Ljava/lang/String;)LX/64m;

    goto :goto_0
.end method

.method public final a(LX/64m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1046000
    invoke-virtual {p1, p2, p3}, LX/64m;->b(Ljava/lang/String;Ljava/lang/String;)LX/64m;

    .line 1046001
    return-void
.end method

.method public final a(LX/64c;LX/65S;)Z
    .locals 1

    .prologue
    .line 1045999
    invoke-virtual {p1, p2}, LX/64c;->b(LX/65S;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/64c;LX/65S;)V
    .locals 0

    .prologue
    .line 1045997
    invoke-virtual {p1, p2}, LX/64c;->a(LX/65S;)V

    .line 1045998
    return-void
.end method

.method public final b(LX/64y;)V
    .locals 0

    .prologue
    .line 1045995
    check-cast p1, LX/64y;

    invoke-virtual {p1}, LX/64y;->c()V

    .line 1045996
    return-void
.end method
