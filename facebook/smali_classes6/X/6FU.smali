.class public LX/6FU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Landroid/net/Uri;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:LX/6Fb;

.field public s:Ljava/lang/String;

.field public t:I

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1068036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1068037
    const/4 v0, 0x0

    iput-object v0, p0, LX/6FU;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(LX/0P1;)LX/6FU;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/6FU;"
        }
    .end annotation

    .prologue
    .line 1068038
    iput-object p1, p0, LX/6FU;->e:LX/0P1;

    .line 1068039
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/6FU;
    .locals 0

    .prologue
    .line 1068040
    iput-object p1, p0, LX/6FU;->a:Landroid/net/Uri;

    .line 1068041
    return-object p0
.end method

.method public final a(Lcom/facebook/bugreporter/BugReport;)LX/6FU;
    .locals 1

    .prologue
    .line 1068042
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 1068043
    iput-object v0, p0, LX/6FU;->a:Landroid/net/Uri;

    .line 1068044
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1068045
    iput-object v0, p0, LX/6FU;->b:Ljava/lang/String;

    .line 1068046
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->c:Landroid/net/Uri;

    move-object v0, v0

    .line 1068047
    iput-object v0, p0, LX/6FU;->c:Landroid/net/Uri;

    .line 1068048
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    move-object v0, v0

    .line 1068049
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/6FU;->d:Ljava/util/List;

    .line 1068050
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->e:LX/0P1;

    move-object v0, v0

    .line 1068051
    iput-object v0, p0, LX/6FU;->e:LX/0P1;

    .line 1068052
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    move-object v0, v0

    .line 1068053
    iput-object v0, p0, LX/6FU;->f:LX/0P1;

    .line 1068054
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1068055
    iput-object v0, p0, LX/6FU;->h:Ljava/lang/String;

    .line 1068056
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1068057
    iput-object v0, p0, LX/6FU;->i:Ljava/lang/String;

    .line 1068058
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1068059
    iput-object v0, p0, LX/6FU;->j:Ljava/lang/String;

    .line 1068060
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1068061
    iput-object v0, p0, LX/6FU;->k:Ljava/lang/String;

    .line 1068062
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1068063
    iput-object v0, p0, LX/6FU;->l:Ljava/lang/String;

    .line 1068064
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->l:Ljava/lang/String;

    move-object v0, v0

    .line 1068065
    iput-object v0, p0, LX/6FU;->m:Ljava/lang/String;

    .line 1068066
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1068067
    iput-object v0, p0, LX/6FU;->n:Ljava/lang/String;

    .line 1068068
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->n:Ljava/lang/String;

    move-object v0, v0

    .line 1068069
    iput-object v0, p0, LX/6FU;->o:Ljava/lang/String;

    .line 1068070
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->o:Ljava/lang/String;

    move-object v0, v0

    .line 1068071
    iput-object v0, p0, LX/6FU;->p:Ljava/lang/String;

    .line 1068072
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->p:Ljava/lang/String;

    move-object v0, v0

    .line 1068073
    iput-object v0, p0, LX/6FU;->q:Ljava/lang/String;

    .line 1068074
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->q:LX/6Fb;

    move-object v0, v0

    .line 1068075
    iput-object v0, p0, LX/6FU;->r:LX/6Fb;

    .line 1068076
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->f:LX/0P1;

    move-object v0, v0

    .line 1068077
    iput-object v0, p0, LX/6FU;->g:LX/0P1;

    .line 1068078
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->r:Ljava/lang/String;

    move-object v0, v0

    .line 1068079
    iput-object v0, p0, LX/6FU;->s:Ljava/lang/String;

    .line 1068080
    iget v0, p1, Lcom/facebook/bugreporter/BugReport;->s:I

    move v0, v0

    .line 1068081
    iput v0, p0, LX/6FU;->t:I

    .line 1068082
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->u:Ljava/lang/String;

    move-object v0, v0

    .line 1068083
    iput-object v0, p0, LX/6FU;->u:Ljava/lang/String;

    .line 1068084
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->v:Ljava/lang/String;

    move-object v0, v0

    .line 1068085
    iput-object v0, p0, LX/6FU;->v:Ljava/lang/String;

    .line 1068086
    iget-object v0, p1, Lcom/facebook/bugreporter/BugReport;->w:Ljava/lang/String;

    move-object v0, v0

    .line 1068087
    iput-object v0, p0, LX/6FU;->w:Ljava/lang/String;

    .line 1068088
    iget-boolean v0, p1, Lcom/facebook/bugreporter/BugReport;->x:Z

    move v0, v0

    .line 1068089
    iput-boolean v0, p0, LX/6FU;->x:Z

    .line 1068090
    return-object p0
.end method

.method public final b(LX/0P1;)LX/6FU;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/6FU;"
        }
    .end annotation

    .prologue
    .line 1068091
    iput-object p1, p0, LX/6FU;->f:LX/0P1;

    .line 1068092
    return-object p0
.end method

.method public final b(Landroid/net/Uri;)LX/6FU;
    .locals 0

    .prologue
    .line 1068093
    iput-object p1, p0, LX/6FU;->c:Landroid/net/Uri;

    .line 1068094
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/6FU;
    .locals 0

    .prologue
    .line 1068095
    iput-object p1, p0, LX/6FU;->s:Ljava/lang/String;

    .line 1068096
    return-object p0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1068097
    iget-object v0, p0, LX/6FU;->d:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ljava/lang/String;)LX/6FU;
    .locals 0

    .prologue
    .line 1068098
    iput-object p1, p0, LX/6FU;->w:Ljava/lang/String;

    .line 1068099
    return-object p0
.end method

.method public final y()Lcom/facebook/bugreporter/BugReport;
    .locals 1

    .prologue
    .line 1068100
    new-instance v0, Lcom/facebook/bugreporter/BugReport;

    invoke-direct {v0, p0}, Lcom/facebook/bugreporter/BugReport;-><init>(LX/6FU;)V

    return-object v0
.end method
