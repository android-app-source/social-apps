.class public final LX/5aV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 955064
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 955065
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 955066
    :goto_0
    return v1

    .line 955067
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 955068
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 955069
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 955070
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 955071
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 955072
    const-string v4, "__type__"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "__typename"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 955073
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    goto :goto_1

    .line 955074
    :cond_3
    const-string v4, "name"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 955075
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 955076
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 955077
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 955078
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 955079
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 955080
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 955081
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 955082
    if-eqz v0, :cond_0

    .line 955083
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955084
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 955085
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955086
    if-eqz v0, :cond_1

    .line 955087
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955088
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955089
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 955090
    return-void
.end method
