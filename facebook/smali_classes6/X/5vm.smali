.class public LX/5vm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/17Y;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/17Y;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1022961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1022962
    iput-object p1, p0, LX/5vm;->a:Landroid/content/Context;

    .line 1022963
    iput-object p2, p0, LX/5vm;->b:LX/0Or;

    .line 1022964
    iput-object p3, p0, LX/5vm;->c:LX/17Y;

    .line 1022965
    return-void
.end method

.method public static a(LX/0QB;)LX/5vm;
    .locals 1

    .prologue
    .line 1022945
    invoke-static {p0}, LX/5vm;->b(LX/0QB;)LX/5vm;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/5vm;
    .locals 4

    .prologue
    .line 1022959
    new-instance v2, LX/5vm;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v1, 0x15e7

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-direct {v2, v0, v3, v1}, LX/5vm;-><init>(Landroid/content/Context;LX/0Or;LX/17Y;)V

    .line 1022960
    return-object v2
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1022957
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/5vm;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1022958
    iget-object v1, p0, LX/5vm;->c:LX/17Y;

    iget-object v2, p0, LX/5vm;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 1022956
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v7, v6

    invoke-virtual/range {v0 .. v7}, LX/5vm;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1022946
    invoke-virtual {p0}, LX/5vm;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1022947
    if-nez v0, :cond_0

    .line 1022948
    const/4 v0, 0x0

    .line 1022949
    :goto_0
    return-object v0

    .line 1022950
    :cond_0
    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1022951
    const-string v1, "extra_profile_pic_expiration"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1022952
    const-string v1, "extra_profile_pic_caption"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1022953
    const-string v1, "profile_photo_method_extra"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1022954
    const-string v1, "extra_app_attribution"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1022955
    const-string v1, "extra_msqrd_mask_id"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
