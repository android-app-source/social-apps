.class public final enum LX/6ZE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6ZE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6ZE;

.field public static final enum LOCATION_UNAVAILABLE:LX/6ZE;

.field public static final enum LOCATION_UNSUPPORTED:LX/6ZE;

.field public static final enum PERMISSION_DENIED:LX/6ZE;

.field public static final enum TEMPORARY_ERROR:LX/6ZE;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1111028
    new-instance v0, LX/6ZE;

    const-string v1, "LOCATION_UNAVAILABLE"

    invoke-direct {v0, v1, v2}, LX/6ZE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZE;->LOCATION_UNAVAILABLE:LX/6ZE;

    .line 1111029
    new-instance v0, LX/6ZE;

    const-string v1, "TEMPORARY_ERROR"

    invoke-direct {v0, v1, v3}, LX/6ZE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZE;->TEMPORARY_ERROR:LX/6ZE;

    .line 1111030
    new-instance v0, LX/6ZE;

    const-string v1, "PERMISSION_DENIED"

    invoke-direct {v0, v1, v4}, LX/6ZE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZE;->PERMISSION_DENIED:LX/6ZE;

    .line 1111031
    new-instance v0, LX/6ZE;

    const-string v1, "LOCATION_UNSUPPORTED"

    invoke-direct {v0, v1, v5}, LX/6ZE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZE;->LOCATION_UNSUPPORTED:LX/6ZE;

    .line 1111032
    const/4 v0, 0x4

    new-array v0, v0, [LX/6ZE;

    sget-object v1, LX/6ZE;->LOCATION_UNAVAILABLE:LX/6ZE;

    aput-object v1, v0, v2

    sget-object v1, LX/6ZE;->TEMPORARY_ERROR:LX/6ZE;

    aput-object v1, v0, v3

    sget-object v1, LX/6ZE;->PERMISSION_DENIED:LX/6ZE;

    aput-object v1, v0, v4

    sget-object v1, LX/6ZE;->LOCATION_UNSUPPORTED:LX/6ZE;

    aput-object v1, v0, v5

    sput-object v0, LX/6ZE;->$VALUES:[LX/6ZE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1111033
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6ZE;
    .locals 1

    .prologue
    .line 1111034
    const-class v0, LX/6ZE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ZE;

    return-object v0
.end method

.method public static values()[LX/6ZE;
    .locals 1

    .prologue
    .line 1111027
    sget-object v0, LX/6ZE;->$VALUES:[LX/6ZE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6ZE;

    return-object v0
.end method
