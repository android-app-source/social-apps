.class public final LX/64W;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/64W;

.field public static final b:LX/64W;


# instance fields
.field public c:Ljava/lang/String;

.field public final d:Z

.field public final e:Z

.field public final f:I

.field public final g:I

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:I

.field public final l:I

.field public final m:Z

.field public final n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1044638
    new-instance v0, LX/64V;

    invoke-direct {v0}, LX/64V;-><init>()V

    .line 1044639
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/64V;->a:Z

    .line 1044640
    move-object v0, v0

    .line 1044641
    invoke-virtual {v0}, LX/64V;->d()LX/64W;

    move-result-object v0

    sput-object v0, LX/64W;->a:LX/64W;

    .line 1044642
    new-instance v0, LX/64V;

    invoke-direct {v0}, LX/64V;-><init>()V

    .line 1044643
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/64V;->f:Z

    .line 1044644
    move-object v0, v0

    .line 1044645
    const v1, 0x7fffffff

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1044646
    invoke-virtual {v0, v1, v2}, LX/64V;->a(ILjava/util/concurrent/TimeUnit;)LX/64V;

    move-result-object v0

    .line 1044647
    invoke-virtual {v0}, LX/64V;->d()LX/64W;

    move-result-object v0

    sput-object v0, LX/64W;->b:LX/64W;

    .line 1044648
    return-void
.end method

.method public constructor <init>(LX/64V;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1044649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044650
    iget-boolean v0, p1, LX/64V;->a:Z

    iput-boolean v0, p0, LX/64W;->d:Z

    .line 1044651
    iget-boolean v0, p1, LX/64V;->b:Z

    iput-boolean v0, p0, LX/64W;->e:Z

    .line 1044652
    iget v0, p1, LX/64V;->c:I

    iput v0, p0, LX/64W;->f:I

    .line 1044653
    const/4 v0, -0x1

    iput v0, p0, LX/64W;->g:I

    .line 1044654
    iput-boolean v1, p0, LX/64W;->h:Z

    .line 1044655
    iput-boolean v1, p0, LX/64W;->i:Z

    .line 1044656
    iput-boolean v1, p0, LX/64W;->j:Z

    .line 1044657
    iget v0, p1, LX/64V;->d:I

    iput v0, p0, LX/64W;->k:I

    .line 1044658
    iget v0, p1, LX/64V;->e:I

    iput v0, p0, LX/64W;->l:I

    .line 1044659
    iget-boolean v0, p1, LX/64V;->f:Z

    iput-boolean v0, p0, LX/64W;->m:Z

    .line 1044660
    iget-boolean v0, p1, LX/64V;->g:Z

    iput-boolean v0, p0, LX/64W;->n:Z

    .line 1044661
    return-void
.end method

.method private constructor <init>(ZZIIZZZIIZZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1044662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044663
    iput-boolean p1, p0, LX/64W;->d:Z

    .line 1044664
    iput-boolean p2, p0, LX/64W;->e:Z

    .line 1044665
    iput p3, p0, LX/64W;->f:I

    .line 1044666
    iput p4, p0, LX/64W;->g:I

    .line 1044667
    iput-boolean p5, p0, LX/64W;->h:Z

    .line 1044668
    iput-boolean p6, p0, LX/64W;->i:Z

    .line 1044669
    iput-boolean p7, p0, LX/64W;->j:Z

    .line 1044670
    iput p8, p0, LX/64W;->k:I

    .line 1044671
    iput p9, p0, LX/64W;->l:I

    .line 1044672
    iput-boolean p10, p0, LX/64W;->m:Z

    .line 1044673
    iput-boolean p11, p0, LX/64W;->n:Z

    .line 1044674
    iput-object p12, p0, LX/64W;->c:Ljava/lang/String;

    .line 1044675
    return-void
.end method

.method public static a(LX/64n;)LX/64W;
    .locals 23

    .prologue
    .line 1044676
    const/16 v16, 0x0

    .line 1044677
    const/4 v4, 0x0

    .line 1044678
    const/4 v5, -0x1

    .line 1044679
    const/4 v6, -0x1

    .line 1044680
    const/4 v7, 0x0

    .line 1044681
    const/4 v8, 0x0

    .line 1044682
    const/4 v9, 0x0

    .line 1044683
    const/4 v10, -0x1

    .line 1044684
    const/4 v11, -0x1

    .line 1044685
    const/4 v12, 0x0

    .line 1044686
    const/4 v13, 0x0

    .line 1044687
    const/4 v15, 0x1

    .line 1044688
    const/4 v3, 0x0

    .line 1044689
    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, LX/64n;->a()I

    move-result v19

    move/from16 v18, v2

    move-object v2, v3

    move/from16 v3, v16

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_12

    .line 1044690
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/64n;->a(I)Ljava/lang/String;

    move-result-object v16

    .line 1044691
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/64n;->b(I)Ljava/lang/String;

    move-result-object v14

    .line 1044692
    const-string v17, "Cache-Control"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1044693
    if-eqz v2, :cond_2

    .line 1044694
    const/4 v15, 0x0

    .line 1044695
    :goto_1
    const/16 v16, 0x0

    move/from16 v22, v16

    move/from16 v16, v3

    move/from16 v3, v22

    .line 1044696
    :cond_0
    :goto_2
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v3, v0, :cond_11

    .line 1044697
    const-string v17, "=,;"

    move-object/from16 v0, v17

    invoke-static {v14, v3, v0}, LX/66M;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v17

    .line 1044698
    move/from16 v0, v17

    invoke-virtual {v14, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    .line 1044699
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v3

    move/from16 v0, v17

    if-eq v0, v3, :cond_1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v21, 0x2c

    move/from16 v0, v21

    if-eq v3, v0, :cond_1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v21, 0x3b

    move/from16 v0, v21

    if-ne v3, v0, :cond_4

    .line 1044700
    :cond_1
    add-int/lit8 v17, v17, 0x1

    .line 1044701
    const/4 v3, 0x0

    move-object/from16 v22, v3

    move/from16 v3, v17

    move-object/from16 v17, v22

    .line 1044702
    :goto_3
    const-string v21, "no-cache"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 1044703
    const/16 v16, 0x1

    goto :goto_2

    :cond_2
    move-object v2, v14

    .line 1044704
    goto :goto_1

    .line 1044705
    :cond_3
    const-string v17, "Pragma"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_10

    .line 1044706
    const/4 v15, 0x0

    goto :goto_1

    .line 1044707
    :cond_4
    add-int/lit8 v3, v17, 0x1

    .line 1044708
    invoke-static {v14, v3}, LX/66M;->a(Ljava/lang/String;I)I

    move-result v3

    .line 1044709
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v3, v0, :cond_5

    invoke-virtual {v14, v3}, Ljava/lang/String;->charAt(I)C

    move-result v17

    const/16 v21, 0x22

    move/from16 v0, v17

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 1044710
    add-int/lit8 v3, v3, 0x1

    .line 1044711
    const-string v17, "\""

    move-object/from16 v0, v17

    invoke-static {v14, v3, v0}, LX/66M;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v17

    .line 1044712
    move/from16 v0, v17

    invoke-virtual {v14, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1044713
    add-int/lit8 v17, v17, 0x1

    move-object/from16 v22, v3

    move/from16 v3, v17

    move-object/from16 v17, v22

    .line 1044714
    goto :goto_3

    .line 1044715
    :cond_5
    const-string v17, ",;"

    move-object/from16 v0, v17

    invoke-static {v14, v3, v0}, LX/66M;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v17

    .line 1044716
    move/from16 v0, v17

    invoke-virtual {v14, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v22, v3

    move/from16 v3, v17

    move-object/from16 v17, v22

    goto :goto_3

    .line 1044717
    :cond_6
    const-string v21, "no-store"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 1044718
    const/4 v4, 0x1

    goto/16 :goto_2

    .line 1044719
    :cond_7
    const-string v21, "max-age"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 1044720
    const/4 v5, -0x1

    move-object/from16 v0, v17

    invoke-static {v0, v5}, LX/66M;->b(Ljava/lang/String;I)I

    move-result v5

    goto/16 :goto_2

    .line 1044721
    :cond_8
    const-string v21, "s-maxage"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 1044722
    const/4 v6, -0x1

    move-object/from16 v0, v17

    invoke-static {v0, v6}, LX/66M;->b(Ljava/lang/String;I)I

    move-result v6

    goto/16 :goto_2

    .line 1044723
    :cond_9
    const-string v21, "private"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 1044724
    const/4 v7, 0x1

    goto/16 :goto_2

    .line 1044725
    :cond_a
    const-string v21, "public"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 1044726
    const/4 v8, 0x1

    goto/16 :goto_2

    .line 1044727
    :cond_b
    const-string v21, "must-revalidate"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 1044728
    const/4 v9, 0x1

    goto/16 :goto_2

    .line 1044729
    :cond_c
    const-string v21, "max-stale"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 1044730
    const v10, 0x7fffffff

    move-object/from16 v0, v17

    invoke-static {v0, v10}, LX/66M;->b(Ljava/lang/String;I)I

    move-result v10

    goto/16 :goto_2

    .line 1044731
    :cond_d
    const-string v21, "min-fresh"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 1044732
    const/4 v11, -0x1

    move-object/from16 v0, v17

    invoke-static {v0, v11}, LX/66M;->b(Ljava/lang/String;I)I

    move-result v11

    goto/16 :goto_2

    .line 1044733
    :cond_e
    const-string v17, "only-if-cached"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 1044734
    const/4 v12, 0x1

    goto/16 :goto_2

    .line 1044735
    :cond_f
    const-string v17, "no-transform"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 1044736
    const/4 v13, 0x1

    goto/16 :goto_2

    :cond_10
    move/from16 v16, v3

    .line 1044737
    :cond_11
    add-int/lit8 v3, v18, 0x1

    move/from16 v18, v3

    move/from16 v3, v16

    goto/16 :goto_0

    .line 1044738
    :cond_12
    if-nez v15, :cond_13

    .line 1044739
    const/4 v14, 0x0

    .line 1044740
    :goto_4
    new-instance v2, LX/64W;

    invoke-direct/range {v2 .. v14}, LX/64W;-><init>(ZZIIZZZIIZZLjava/lang/String;)V

    return-object v2

    :cond_13
    move-object v14, v2

    goto :goto_4
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1044741
    iget-object v0, p0, LX/64W;->c:Ljava/lang/String;

    .line 1044742
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v3, -0x1

    .line 1044743
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1044744
    iget-boolean v1, p0, LX/64W;->d:Z

    if-eqz v1, :cond_1

    const-string v1, "no-cache, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044745
    :cond_1
    iget-boolean v1, p0, LX/64W;->e:Z

    if-eqz v1, :cond_2

    const-string v1, "no-store, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044746
    :cond_2
    iget v1, p0, LX/64W;->f:I

    if-eq v1, v3, :cond_3

    const-string v1, "max-age="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/64W;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044747
    :cond_3
    iget v1, p0, LX/64W;->g:I

    if-eq v1, v3, :cond_4

    const-string v1, "s-maxage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/64W;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044748
    :cond_4
    iget-boolean v1, p0, LX/64W;->h:Z

    if-eqz v1, :cond_5

    const-string v1, "private, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044749
    :cond_5
    iget-boolean v1, p0, LX/64W;->i:Z

    if-eqz v1, :cond_6

    const-string v1, "public, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044750
    :cond_6
    iget-boolean v1, p0, LX/64W;->j:Z

    if-eqz v1, :cond_7

    const-string v1, "must-revalidate, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044751
    :cond_7
    iget v1, p0, LX/64W;->k:I

    if-eq v1, v3, :cond_8

    const-string v1, "max-stale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/64W;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044752
    :cond_8
    iget v1, p0, LX/64W;->l:I

    if-eq v1, v3, :cond_9

    const-string v1, "min-fresh="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LX/64W;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044753
    :cond_9
    iget-boolean v1, p0, LX/64W;->m:Z

    if-eqz v1, :cond_a

    const-string v1, "only-if-cached, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044754
    :cond_a
    iget-boolean v1, p0, LX/64W;->n:Z

    if-eqz v1, :cond_b

    const-string v1, "no-transform, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1044755
    :cond_b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-nez v1, :cond_c

    const-string v0, ""

    .line 1044756
    :goto_1
    move-object v0, v0

    .line 1044757
    iput-object v0, p0, LX/64W;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 1044758
    :cond_c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1044759
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
