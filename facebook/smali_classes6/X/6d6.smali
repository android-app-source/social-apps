.class public final LX/6d6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final A:I

.field private final B:I

.field private final C:I

.field private final D:I

.field private final E:I

.field private final F:I

.field private final G:I

.field private final H:I

.field private final I:I

.field private final J:I

.field private final K:I

.field private final L:I

.field private final M:I

.field private final N:I

.field private final O:I

.field private final P:I

.field private final Q:I

.field private final R:I

.field private final S:I

.field private final T:I

.field private final U:I

.field private final V:I

.field private final W:I

.field private final X:I

.field private final Y:I

.field private final Z:I

.field public final synthetic a:LX/2NH;

.field private final aa:I

.field private final ab:I

.field private final ac:I

.field private final ad:I

.field private final ae:I

.field private final af:I

.field private final ag:I

.field private final ah:I

.field private final ai:I

.field private final aj:I

.field private final ak:I

.field private final al:I

.field private final am:I

.field private final an:I

.field private final ao:I

.field private final ap:I

.field public final b:Landroid/database/Cursor;

.field public final c:LX/2Lz;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field public final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:I

.field private final s:I

.field private final t:I

.field private final u:I

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:I

.field private final z:I


# direct methods
.method public constructor <init>(LX/2NH;Landroid/database/Cursor;LX/2Lz;)V
    .locals 1

    .prologue
    .line 1115111
    iput-object p1, p0, LX/6d6;->a:LX/2NH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1115112
    iput-object p2, p0, LX/6d6;->b:Landroid/database/Cursor;

    .line 1115113
    iput-object p3, p0, LX/6d6;->c:LX/2Lz;

    .line 1115114
    const-string v0, "thread_key"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->d:I

    .line 1115115
    const-string v0, "msg_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->e:I

    .line 1115116
    const-string v0, "action_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->f:I

    .line 1115117
    const-string v0, "text"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->g:I

    .line 1115118
    const-string v0, "sender"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->h:I

    .line 1115119
    const-string v0, "is_not_forwardable"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->i:I

    .line 1115120
    const-string v0, "timestamp_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->j:I

    .line 1115121
    const-string v0, "timestamp_sent_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->k:I

    .line 1115122
    const-string v0, "msg_type"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->m:I

    .line 1115123
    const-string v0, "affected_users"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->n:I

    .line 1115124
    const-string v0, "attachments"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->o:I

    .line 1115125
    const-string v0, "shares"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->p:I

    .line 1115126
    const-string v0, "sticker_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->q:I

    .line 1115127
    const-string v0, "client_tags"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->r:I

    .line 1115128
    const-string v0, "offline_threading_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->s:I

    .line 1115129
    const-string v0, "source"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->t:I

    .line 1115130
    const-string v0, "channel_source"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->u:I

    .line 1115131
    const-string v0, "send_channel"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->C:I

    .line 1115132
    const-string v0, "is_non_authoritative"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->v:I

    .line 1115133
    const-string v0, "pending_send_media_attachment"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->w:I

    .line 1115134
    const-string v0, "sent_share_attachment"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->x:I

    .line 1115135
    const-string v0, "send_error"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->y:I

    .line 1115136
    const-string v0, "send_error_message"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->z:I

    .line 1115137
    const-string v0, "send_error_number"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->A:I

    .line 1115138
    const-string v0, "send_error_timestamp_ms"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->l:I

    .line 1115139
    const-string v0, "send_error_error_url"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->B:I

    .line 1115140
    const-string v0, "publicity"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->D:I

    .line 1115141
    const-string v0, "send_queue_type"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->E:I

    .line 1115142
    const-string v0, "payment_transaction"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->F:I

    .line 1115143
    const-string v0, "payment_request"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->G:I

    .line 1115144
    const-string v0, "has_unavailable_attachment"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->H:I

    .line 1115145
    const-string v0, "app_attribution"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->I:I

    .line 1115146
    const-string v0, "content_app_attribution"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->J:I

    .line 1115147
    const-string v0, "xma"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->K:I

    .line 1115148
    const-string v0, "admin_text_type"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->L:I

    .line 1115149
    const-string v0, "admin_text_theme_color"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->M:I

    .line 1115150
    const-string v0, "admin_text_thread_icon_emoji"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->N:I

    .line 1115151
    const-string v0, "admin_text_nickname"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->O:I

    .line 1115152
    const-string v0, "admin_text_target_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->P:I

    .line 1115153
    const-string v0, "admin_text_thread_message_lifetime"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->Q:I

    .line 1115154
    const-string v0, "admin_text_thread_journey_color_choices"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->R:I

    .line 1115155
    const-string v0, "admin_text_thread_journey_emoji_choices"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->S:I

    .line 1115156
    const-string v0, "admin_text_thread_journey_nickname_choices"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->T:I

    .line 1115157
    const-string v0, "admin_text_thread_journey_bot_choices"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->U:I

    .line 1115158
    const-string v0, "message_lifetime"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->Z:I

    .line 1115159
    const-string v0, "admin_text_thread_rtc_event"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->V:I

    .line 1115160
    const-string v0, "admin_text_thread_rtc_server_info_data"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->W:I

    .line 1115161
    const-string v0, "admin_text_thread_rtc_is_video_call"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->X:I

    .line 1115162
    const-string v0, "admin_text_thread_ride_provider_name"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->Y:I

    .line 1115163
    const-string v0, "is_sponsored"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->aa:I

    .line 1115164
    const-string v0, "admin_text_thread_ad_properties"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ab:I

    .line 1115165
    const-string v0, "admin_text_game_score_data"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ac:I

    .line 1115166
    const-string v0, "admin_text_thread_event_reminder_properties"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ad:I

    .line 1115167
    const-string v0, "commerce_message_type"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->af:I

    .line 1115168
    const-string v0, "customizations"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ag:I

    .line 1115169
    const-string v0, "admin_text_joinable_event_type"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ah:I

    .line 1115170
    const-string v0, "metadata_at_text_ranges"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ai:I

    .line 1115171
    const-string v0, "platform_metadata"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->aj:I

    .line 1115172
    const-string v0, "admin_text_is_joinable_promo"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ak:I

    .line 1115173
    const-string v0, "admin_text_agent_intent_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ae:I

    .line 1115174
    const-string v0, "montage_reply_message_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->al:I

    .line 1115175
    const-string v0, "admin_text_booking_request_id"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->am:I

    .line 1115176
    const-string v0, "generic_admin_message_extensible_data"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->an:I

    .line 1115177
    const-string v0, "reactions"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ao:I

    .line 1115178
    const-string v0, "profile_ranges"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/6d6;->ap:I

    .line 1115179
    return-void
.end method

.method private static c(LX/6d6;)Lcom/facebook/messaging/model/send/SendError;
    .locals 5

    .prologue
    .line 1115180
    invoke-static {}, Lcom/facebook/messaging/model/send/SendError;->newBuilder()LX/6fO;

    move-result-object v0

    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->y:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/6fP;->fromSerializedString(Ljava/lang/String;)LX/6fP;

    move-result-object v1

    .line 1115181
    iput-object v1, v0, LX/6fO;->a:LX/6fP;

    .line 1115182
    move-object v0, v0

    .line 1115183
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->z:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1115184
    iput-object v1, v0, LX/6fO;->b:Ljava/lang/String;

    .line 1115185
    move-object v0, v0

    .line 1115186
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->A:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1115187
    iput v1, v0, LX/6fO;->e:I

    .line 1115188
    move-object v0, v0

    .line 1115189
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->l:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1115190
    iput-wide v2, v0, LX/6fO;->c:J

    .line 1115191
    move-object v0, v0

    .line 1115192
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->B:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1115193
    iput-object v1, v0, LX/6fO;->d:Ljava/lang/String;

    .line 1115194
    move-object v0, v0

    .line 1115195
    invoke-virtual {v0}, LX/6fO;->g()Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/messaging/model/messages/Message;
    .locals 15

    .prologue
    .line 1115196
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1115197
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->e:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1115198
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->v:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->v:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    .line 1115199
    :goto_0
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->H:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_11

    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->H:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_11

    const/4 v1, 0x1

    .line 1115200
    :goto_1
    iget-object v2, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v4, p0, LX/6d6;->aa:I

    invoke-interface {v2, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_12

    iget-object v2, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v4, p0, LX/6d6;->aa:I

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_12

    const/4 v2, 0x1

    .line 1115201
    :goto_2
    iget-object v4, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v5, p0, LX/6d6;->d:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v4

    .line 1115202
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    move-result-object v5

    .line 1115203
    iput-object v4, v5, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1115204
    move-object v5, v5

    .line 1115205
    iget-object v6, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6d6;->f:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115206
    iput-wide v6, v5, LX/6f7;->g:J

    .line 1115207
    move-object v5, v5

    .line 1115208
    iget-object v6, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6d6;->g:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1115209
    iput-object v6, v5, LX/6f7;->f:Ljava/lang/String;

    .line 1115210
    move-object v5, v5

    .line 1115211
    iget-object v6, p0, LX/6d6;->a:LX/2NH;

    iget-object v6, v6, LX/2NH;->b:LX/2N7;

    iget-object v7, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v8, p0, LX/6d6;->h:I

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2N7;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v6

    .line 1115212
    iput-object v6, v5, LX/6f7;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 1115213
    move-object v5, v5

    .line 1115214
    iget-object v6, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6d6;->j:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115215
    iput-wide v6, v5, LX/6f7;->c:J

    .line 1115216
    move-object v5, v5

    .line 1115217
    iget-object v6, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6d6;->k:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 1115218
    iput-wide v6, v5, LX/6f7;->d:J

    .line 1115219
    move-object v5, v5

    .line 1115220
    iget-object v6, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6d6;->m:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, LX/2uW;->fromDbKeyValue(I)LX/2uW;

    move-result-object v6

    .line 1115221
    iput-object v6, v5, LX/6f7;->l:LX/2uW;

    .line 1115222
    move-object v5, v5

    .line 1115223
    iget-object v6, p0, LX/6d6;->a:LX/2NH;

    iget-object v6, v6, LX/2NH;->b:LX/2N7;

    iget-object v7, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v8, p0, LX/6d6;->n:I

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2N7;->b(Ljava/lang/String;)LX/0Px;

    move-result-object v6

    .line 1115224
    iput-object v6, v5, LX/6f7;->m:Ljava/util/List;

    .line 1115225
    move-object v5, v5

    .line 1115226
    iget-object v6, p0, LX/6d6;->a:LX/2NH;

    iget-object v6, v6, LX/2NH;->d:LX/2NI;

    iget-object v7, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v8, p0, LX/6d6;->o:I

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v3}, LX/2NI;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 1115227
    iput-object v3, v5, LX/6f7;->i:Ljava/util/List;

    .line 1115228
    move-object v3, v5

    .line 1115229
    iget-object v5, p0, LX/6d6;->a:LX/2NH;

    iget-object v5, v5, LX/2NH;->e:LX/2NJ;

    iget-object v6, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6d6;->p:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1115230
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_18

    .line 1115231
    sget-object v7, LX/0Q7;->a:LX/0Px;

    move-object v7, v7

    .line 1115232
    :goto_3
    move-object v5, v7

    .line 1115233
    iput-object v5, v3, LX/6f7;->j:Ljava/util/List;

    .line 1115234
    move-object v3, v3

    .line 1115235
    iget-object v5, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6d6;->q:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1115236
    iput-object v5, v3, LX/6f7;->k:Ljava/lang/String;

    .line 1115237
    move-object v3, v3

    .line 1115238
    iget-object v5, p0, LX/6d6;->a:LX/2NH;

    iget-object v5, v5, LX/2NH;->g:LX/2No;

    iget-object v6, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v7, p0, LX/6d6;->r:I

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1115239
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v8

    .line 1115240
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1115241
    iget-object v7, v5, LX/2No;->a:LX/2N8;

    invoke-virtual {v7, v6}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 1115242
    invoke-virtual {v7}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v9

    .line 1115243
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1115244
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 1115245
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0lF;

    invoke-virtual {v7}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v10, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_4

    .line 1115246
    :cond_0
    invoke-virtual {v8}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    move-object v5, v7

    .line 1115247
    invoke-virtual {v3, v5}, LX/6f7;->a(Ljava/util/Map;)LX/6f7;

    move-result-object v3

    iget-object v5, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6d6;->s:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1115248
    iput-object v5, v3, LX/6f7;->n:Ljava/lang/String;

    .line 1115249
    move-object v3, v3

    .line 1115250
    iget-object v5, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6d6;->t:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1115251
    iput-object v5, v3, LX/6f7;->p:Ljava/lang/String;

    .line 1115252
    move-object v3, v3

    .line 1115253
    iget-object v5, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6d6;->u:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/2NH;->e(Ljava/lang/String;)LX/6f2;

    move-result-object v5

    .line 1115254
    iput-object v5, v3, LX/6f7;->q:LX/6f2;

    .line 1115255
    move-object v3, v3

    .line 1115256
    iput-boolean v0, v3, LX/6f7;->o:Z

    .line 1115257
    move-object v0, v3

    .line 1115258
    iget-object v3, p0, LX/6d6;->a:LX/2NH;

    iget-object v3, v3, LX/2NH;->c:LX/2NB;

    iget-object v5, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6d6;->w:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/2NB;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 1115259
    iput-object v3, v0, LX/6f7;->r:Ljava/util/List;

    .line 1115260
    move-object v0, v0

    .line 1115261
    iget-object v3, p0, LX/6d6;->a:LX/2NH;

    iget-object v3, v3, LX/2NH;->f:LX/2Nm;

    iget-object v5, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v6, p0, LX/6d6;->x:I

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/2Nm;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/share/SentShareAttachment;

    move-result-object v3

    .line 1115262
    iput-object v3, v0, LX/6f7;->s:Lcom/facebook/messaging/model/share/SentShareAttachment;

    .line 1115263
    move-object v0, v0

    .line 1115264
    invoke-static {p0}, LX/6d6;->c(LX/6d6;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v3

    .line 1115265
    iput-object v3, v0, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 1115266
    move-object v0, v0

    .line 1115267
    iget-object v3, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v5, p0, LX/6d6;->C:I

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1115268
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1115269
    sget-object v5, LX/6f3;->UNKNOWN:LX/6f3;

    .line 1115270
    :goto_5
    move-object v3, v5

    .line 1115271
    invoke-virtual {v0, v3}, LX/6f7;->a(LX/6f3;)LX/6f7;

    move-result-object v0

    iget-object v3, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v5, p0, LX/6d6;->D:I

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1115272
    sget-object v5, Lcom/facebook/messaging/model/messages/Publicity;->d:LX/0Rf;

    invoke-virtual {v5}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/model/messages/Publicity;

    .line 1115273
    iget-object v7, v5, Lcom/facebook/messaging/model/messages/Publicity;->e:Ljava/lang/String;

    move-object v7, v7

    .line 1115274
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1115275
    :goto_6
    move-object v3, v5

    .line 1115276
    iput-object v3, v0, LX/6f7;->v:Lcom/facebook/messaging/model/messages/Publicity;

    .line 1115277
    move-object v0, v0

    .line 1115278
    iget-object v3, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v5, p0, LX/6d6;->E:I

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    .line 1115279
    if-nez v3, :cond_1c

    .line 1115280
    :cond_2
    :goto_7
    move-object v3, v5

    .line 1115281
    iput-object v3, v0, LX/6f7;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 1115282
    move-object v0, v0

    .line 1115283
    iget-object v3, p0, LX/6d6;->a:LX/2NH;

    iget-object v3, v3, LX/2NH;->i:LX/2Nq;

    iget-object v4, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v5, p0, LX/6d6;->F:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2Nq;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    move-result-object v3

    .line 1115284
    iput-object v3, v0, LX/6f7;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 1115285
    move-object v0, v0

    .line 1115286
    iget-object v3, p0, LX/6d6;->a:LX/2NH;

    iget-object v3, v3, LX/2NH;->h:LX/2Np;

    iget-object v4, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v5, p0, LX/6d6;->G:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2Np;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/payment/PaymentRequestData;

    move-result-object v3

    .line 1115287
    iput-object v3, v0, LX/6f7;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 1115288
    move-object v0, v0

    .line 1115289
    iput-boolean v1, v0, LX/6f7;->D:Z

    .line 1115290
    move-object v0, v0

    .line 1115291
    iget-object v1, p0, LX/6d6;->a:LX/2NH;

    iget-object v1, v1, LX/2NH;->j:LX/2NC;

    iget-object v3, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v4, p0, LX/6d6;->I:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    .line 1115292
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 1115293
    :goto_8
    move-object v1, v5

    .line 1115294
    iput-object v1, v0, LX/6f7;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1115295
    move-object v0, v0

    .line 1115296
    iget-object v1, p0, LX/6d6;->a:LX/2NH;

    iget-object v1, v1, LX/2NH;->j:LX/2NC;

    iget-object v3, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v4, p0, LX/6d6;->J:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/2NC;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    move-result-object v1

    .line 1115297
    iput-object v1, v0, LX/6f7;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1115298
    move-object v0, v0

    .line 1115299
    iget-object v1, p0, LX/6d6;->a:LX/2NH;

    iget-object v1, v1, LX/2NH;->p:LX/2Ns;

    iget-object v3, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v4, p0, LX/6d6;->K:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/2Ns;->a(Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v1

    .line 1115300
    iput-object v1, v0, LX/6f7;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 1115301
    move-object v0, v0

    .line 1115302
    iput-boolean v2, v0, LX/6f7;->M:Z

    .line 1115303
    move-object v0, v0

    .line 1115304
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->ap:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1115305
    iget-object v2, p0, LX/6d6;->a:LX/2NH;

    new-instance v3, LX/6d5;

    invoke-direct {v3, p0}, LX/6d5;-><init>(LX/6d6;)V

    invoke-static {v2, v1, v3}, LX/2NH;->a$redex0(LX/2NH;Ljava/lang/String;LX/266;)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 1115306
    iput-object v1, v0, LX/6f7;->V:LX/0Px;

    .line 1115307
    move-object v7, v0

    .line 1115308
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->Z:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1115309
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->Z:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1115310
    iput-object v0, v7, LX/6f7;->J:Ljava/lang/Integer;

    .line 1115311
    :cond_3
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->af:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1115312
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->af:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1115313
    iput-object v0, v7, LX/6f7;->N:Ljava/lang/String;

    .line 1115314
    :cond_4
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->ag:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1115315
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->ag:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1115316
    if-eqz v0, :cond_20

    const-string v1, "{\"border\":\"flowers\"}"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    const/4 v1, 0x1

    :goto_9
    move v0, v1

    .line 1115317
    iput-boolean v0, v7, LX/6f7;->O:Z

    .line 1115318
    :cond_5
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->ai:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1115319
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->ai:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1115320
    iget-object v0, p0, LX/6d6;->a:LX/2NH;

    iget-object v0, v0, LX/2NH;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ci;

    .line 1115321
    iget-object v2, v0, LX/6ci;->b:LX/0lC;

    iget-object v3, v0, LX/6ci;->a:LX/03V;

    invoke-static {v2, v3, v1}, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a(LX/0lC;LX/03V;Ljava/lang/String;)LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 1115322
    invoke-virtual {v7, v0}, LX/6f7;->e(Ljava/util/List;)LX/6f7;

    .line 1115323
    :cond_6
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->aj:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1115324
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->aj:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1115325
    iget-object v0, p0, LX/6d6;->a:LX/2NH;

    iget-object v0, v0, LX/2NH;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cj;

    .line 1115326
    iget-object v2, v0, LX/6cj;->a:LX/03V;

    iget-object v3, v0, LX/6cj;->b:LX/0lC;

    invoke-static {v2, v3, v1}, LX/5dt;->a(LX/03V;LX/0lC;Ljava/lang/String;)LX/0P1;

    move-result-object v2

    move-object v0, v2

    .line 1115327
    invoke-virtual {v7, v0}, LX/6f7;->b(Ljava/util/Map;)LX/6f7;

    .line 1115328
    :cond_7
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->al:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1115329
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->al:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1115330
    iput-object v0, v7, LX/6f7;->P:Ljava/lang/String;

    .line 1115331
    :cond_8
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->ao:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1115332
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->ao:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1115333
    iget-object v1, p0, LX/6d6;->a:LX/2NH;

    iget-object v1, v1, LX/2NH;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v2, 0x0

    .line 1115334
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v3

    .line 1115335
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_a
    if-ge v1, v5, :cond_9

    aget-object v6, v4, v1

    .line 1115336
    const-string v8, "="

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1115337
    const/4 v8, 0x1

    aget-object v8, v6, v8

    aget-object v6, v6, v2

    invoke-static {v6}, Lcom/facebook/user/model/UserKey;->a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v6

    invoke-interface {v3, v8, v6}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1115338
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 1115339
    :cond_9
    move-object v0, v3

    .line 1115340
    invoke-virtual {v7, v0}, LX/6f7;->a(LX/0Xu;)LX/6f7;

    .line 1115341
    :cond_a
    iget-object v0, v7, LX/6f7;->j:Ljava/util/List;

    move-object v0, v0

    .line 1115342
    if-eqz v0, :cond_b

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    .line 1115343
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/share/Share;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/Share;->l:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 1115344
    iput-object v0, v7, LX/6f7;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 1115345
    :cond_b
    iget-object v0, v7, LX/6f7;->l:LX/2uW;

    move-object v0, v0

    .line 1115346
    sget-object v1, LX/2uW;->ADMIN:LX/2uW;

    if-ne v0, v1, :cond_c

    .line 1115347
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->R:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2NH;->d(Ljava/lang/String;)LX/0Px;

    move-result-object v6

    .line 1115348
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v1, p0, LX/6d6;->S:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2NH;->d(Ljava/lang/String;)LX/0Px;

    move-result-object v8

    .line 1115349
    iget-object v0, p0, LX/6d6;->a:LX/2NH;

    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->T:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/6d1;

    invoke-direct {v2, p0}, LX/6d1;-><init>(LX/6d6;)V

    invoke-static {v0, v1, v2}, LX/2NH;->a$redex0(LX/2NH;Ljava/lang/String;LX/266;)LX/0Px;

    move-result-object v9

    .line 1115350
    iget-object v0, p0, LX/6d6;->a:LX/2NH;

    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->U:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/6d2;

    invoke-direct {v2, p0}, LX/6d2;-><init>(LX/6d6;)V

    invoke-static {v0, v1, v2}, LX/2NH;->a$redex0(LX/2NH;Ljava/lang/String;LX/266;)LX/0Px;

    move-result-object v10

    .line 1115351
    iget-object v0, p0, LX/6d6;->a:LX/2NH;

    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->ab:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/6d3;

    invoke-direct {v2, p0}, LX/6d3;-><init>(LX/6d6;)V

    invoke-static {v0, v1, v2}, LX/2NH;->b$redex0(LX/2NH;Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    .line 1115352
    const/4 v3, 0x0

    .line 1115353
    const/4 v2, 0x0

    .line 1115354
    const/4 v1, 0x0

    .line 1115355
    iget-object v4, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v5, p0, LX/6d6;->ac:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1115356
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_17

    .line 1115357
    :try_start_0
    iget-object v5, p0, LX/6d6;->a:LX/2NH;

    iget-object v5, v5, LX/2NH;->o:LX/0lB;

    invoke-virtual {v5, v4}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 1115358
    const-string v5, "game_type"

    invoke-virtual {v4, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v3

    .line 1115359
    const-string v5, "score"

    invoke-virtual {v4, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    const/4 v11, 0x0

    invoke-virtual {v5, v11}, LX/0lF;->b(I)I

    move-result v2

    .line 1115360
    const-string v5, "is_new_high_score"

    invoke-virtual {v4, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0lF;->a(Z)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move-object v4, v3

    move v3, v2

    move v2, v1

    .line 1115361
    :goto_b
    const/4 v1, 0x0

    .line 1115362
    iget-object v5, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v11, p0, LX/6d6;->L:I

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 1115363
    iget-object v5, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v12, p0, LX/6d6;->an:I

    invoke-interface {v5, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1115364
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_13

    .line 1115365
    :try_start_1
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1115366
    invoke-static {v11}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->a(I)Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    move-result-object v5

    .line 1115367
    invoke-static {v5}, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;->a(Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;)LX/6eo;

    move-result-object v13

    .line 1115368
    if-nez v13, :cond_21

    .line 1115369
    const/4 v13, 0x0

    .line 1115370
    :goto_c
    move-object v1, v13
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1115371
    move-object v5, v1

    .line 1115372
    :goto_d
    iget-object v1, p0, LX/6d6;->a:LX/2NH;

    iget-object v12, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v13, p0, LX/6d6;->ad:I

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v13, LX/6d4;

    invoke-direct {v13, p0}, LX/6d4;-><init>(LX/6d6;)V

    invoke-static {v1, v12, v13}, LX/2NH;->b$redex0(LX/2NH;Ljava/lang/String;LX/266;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    .line 1115373
    invoke-static {}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->newBuilder()LX/6ev;

    move-result-object v12

    .line 1115374
    invoke-static {v11}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->a(I)Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    move-result-object v13

    iput-object v13, v12, LX/6ev;->a:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    .line 1115375
    move-object v11, v12

    .line 1115376
    iget-object v12, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v13, p0, LX/6d6;->M:I

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 1115377
    iput v12, v11, LX/6ev;->b:I

    .line 1115378
    move-object v11, v11

    .line 1115379
    iget-object v12, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v13, p0, LX/6d6;->N:I

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1115380
    iput-object v12, v11, LX/6ev;->c:Ljava/lang/String;

    .line 1115381
    move-object v11, v11

    .line 1115382
    iget-object v12, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v13, p0, LX/6d6;->O:I

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1115383
    iput-object v12, v11, LX/6ev;->d:Ljava/lang/String;

    .line 1115384
    move-object v11, v11

    .line 1115385
    iget-object v12, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v13, p0, LX/6d6;->P:I

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1115386
    iput-object v12, v11, LX/6ev;->e:Ljava/lang/String;

    .line 1115387
    move-object v11, v11

    .line 1115388
    iget-object v12, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v13, p0, LX/6d6;->Q:I

    invoke-interface {v12, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 1115389
    iput v12, v11, LX/6ev;->f:I

    .line 1115390
    move-object v11, v11

    .line 1115391
    iput-object v6, v11, LX/6ev;->g:LX/0Px;

    .line 1115392
    move-object v6, v11

    .line 1115393
    iput-object v8, v6, LX/6ev;->h:LX/0Px;

    .line 1115394
    move-object v6, v6

    .line 1115395
    iput-object v9, v6, LX/6ev;->i:LX/0Px;

    .line 1115396
    move-object v6, v6

    .line 1115397
    iput-object v10, v6, LX/6ev;->j:LX/0Px;

    .line 1115398
    move-object v6, v6

    .line 1115399
    iget-object v8, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v9, p0, LX/6d6;->V:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1115400
    iput-object v8, v6, LX/6ev;->k:Ljava/lang/String;

    .line 1115401
    move-object v6, v6

    .line 1115402
    iget-object v8, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v9, p0, LX/6d6;->W:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1115403
    iput-object v8, v6, LX/6ev;->l:Ljava/lang/String;

    .line 1115404
    move-object v8, v6

    .line 1115405
    iget-object v6, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v9, p0, LX/6d6;->X:I

    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-eqz v6, :cond_14

    const/4 v6, 0x1

    .line 1115406
    :goto_e
    iput-boolean v6, v8, LX/6ev;->m:Z

    .line 1115407
    move-object v6, v8

    .line 1115408
    iget-object v8, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v9, p0, LX/6d6;->Y:I

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1115409
    iput-object v8, v6, LX/6ev;->n:Ljava/lang/String;

    .line 1115410
    move-object v6, v6

    .line 1115411
    iput-object v0, v6, LX/6ev;->o:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    .line 1115412
    move-object v0, v6

    .line 1115413
    iput-object v4, v0, LX/6ev;->p:Ljava/lang/String;

    .line 1115414
    move-object v0, v0

    .line 1115415
    iput v3, v0, LX/6ev;->q:I

    .line 1115416
    move-object v0, v0

    .line 1115417
    iput-boolean v2, v0, LX/6ev;->r:Z

    .line 1115418
    move-object v0, v0

    .line 1115419
    iput-object v1, v0, LX/6ev;->s:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    .line 1115420
    move-object v0, v0

    .line 1115421
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->ah:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6ev;->i(Ljava/lang/String;)LX/6ev;

    move-result-object v1

    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->ak:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    .line 1115422
    :goto_f
    iput-boolean v0, v1, LX/6ev;->u:Z

    .line 1115423
    move-object v0, v1

    .line 1115424
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->ae:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1115425
    iput-object v1, v0, LX/6ev;->v:Ljava/lang/String;

    .line 1115426
    move-object v0, v0

    .line 1115427
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v2, p0, LX/6d6;->am:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1115428
    iput-object v1, v0, LX/6ev;->w:Ljava/lang/String;

    .line 1115429
    move-object v0, v0

    .line 1115430
    iput-object v5, v0, LX/6ev;->x:Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    .line 1115431
    move-object v0, v0

    .line 1115432
    invoke-virtual {v0}, LX/6ev;->a()Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    move-result-object v0

    .line 1115433
    iput-object v0, v7, LX/6f7;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 1115434
    :cond_c
    invoke-virtual {v7}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 1115435
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1115436
    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v4, p0, LX/6d6;->i:I

    invoke-interface {v1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_22

    iget-object v1, p0, LX/6d6;->b:Landroid/database/Cursor;

    iget v4, p0, LX/6d6;->i:I

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_22

    move v1, v2

    .line 1115437
    :goto_10
    iget-object v4, p0, LX/6d6;->c:LX/2Lz;

    invoke-virtual {v4, v0}, LX/2Lz;->e(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-nez v4, :cond_d

    iget-object v4, p0, LX/6d6;->c:LX/2Lz;

    invoke-virtual {v4, v0}, LX/2Lz;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-nez v4, :cond_d

    invoke-static {v0}, LX/2Lz;->g(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-eqz v4, :cond_23

    :cond_d
    const/4 v4, 0x1

    :goto_11
    move v4, v4

    .line 1115438
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v5

    if-nez v1, :cond_e

    if-eqz v4, :cond_f

    :cond_e
    move v3, v2

    .line 1115439
    :cond_f
    iput-boolean v3, v5, LX/6f7;->h:Z

    .line 1115440
    move-object v1, v5

    .line 1115441
    invoke-virtual {v1}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    move-object v0, v1

    .line 1115442
    :goto_12
    return-object v0

    .line 1115443
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1115444
    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1115445
    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_2

    :catch_0
    move-object v4, v3

    move v3, v2

    move v2, v1

    goto/16 :goto_b

    :catch_1
    :cond_13
    move-object v5, v1

    goto/16 :goto_d

    .line 1115446
    :cond_14
    const/4 v6, 0x0

    goto/16 :goto_e

    :cond_15
    const/4 v0, 0x0

    goto :goto_f

    .line 1115447
    :cond_16
    const/4 v0, 0x0

    goto :goto_12

    :cond_17
    move-object v4, v3

    move v3, v2

    move v2, v1

    goto/16 :goto_b

    .line 1115448
    :cond_18
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1115449
    iget-object v7, v5, LX/2NJ;->b:LX/2N8;

    invoke-virtual {v7, v6}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 1115450
    invoke-virtual {v7}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_13
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_19

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0lF;

    .line 1115451
    iget-object v10, v5, LX/2NJ;->a:LX/2NK;

    invoke-virtual {v10, v7}, LX/2NK;->a(LX/0lF;)Lcom/facebook/messaging/model/share/Share;

    move-result-object v7

    invoke-virtual {v8, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_13

    .line 1115452
    :cond_19
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    goto/16 :goto_3

    .line 1115453
    :cond_1a
    :try_start_2
    const-class v5, LX/6f3;

    invoke-static {v5, v3}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v5

    check-cast v5, LX/6f3;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_5

    .line 1115454
    :catch_2
    sget-object v5, LX/6f3;->UNKNOWN:LX/6f3;

    goto/16 :goto_5

    :cond_1b
    sget-object v5, Lcom/facebook/messaging/model/messages/Publicity;->a:Lcom/facebook/messaging/model/messages/Publicity;

    goto/16 :goto_6

    .line 1115455
    :cond_1c
    invoke-static {}, LX/6fM;->values()[LX/6fM;

    move-result-object v7

    array-length v8, v7

    const/4 v6, 0x0

    :goto_14
    if-ge v6, v8, :cond_2

    aget-object v9, v7, v6

    .line 1115456
    iget-object v10, v9, LX/6fM;->serializedValue:Ljava/lang/String;

    invoke-static {v10, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1d

    .line 1115457
    new-instance v5, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    invoke-direct {v5, v4, v9}, Lcom/facebook/messaging/model/send/PendingSendQueueKey;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/6fM;)V

    goto/16 :goto_7

    .line 1115458
    :cond_1d
    add-int/lit8 v6, v6, 0x1

    goto :goto_14

    .line 1115459
    :cond_1e
    iget-object v4, v1, LX/2NC;->a:LX/2N8;

    invoke-virtual {v4, v3}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 1115460
    const-string v4, "app_id"

    invoke-virtual {v6, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v7

    .line 1115461
    const-string v4, "app_name"

    invoke-virtual {v6, v4}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1f

    const-string v4, "app_name"

    invoke-virtual {v6, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v4

    .line 1115462
    :goto_15
    const-string v8, "app_key_hash"

    invoke-virtual {v6, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-virtual {v6}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v8

    .line 1115463
    new-instance v6, Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-direct {v6, v7, v4, v8, v5}, Lcom/facebook/share/model/ComposerAppAttribution;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v6

    goto/16 :goto_8

    :cond_1f
    move-object v4, v5

    .line 1115464
    goto :goto_15

    :cond_20
    const/4 v1, 0x0

    goto/16 :goto_9

    :cond_21
    :try_start_3
    invoke-interface {v13, v12}, LX/6eo;->a(Lorg/json/JSONObject;)Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    move-result-object v13

    goto/16 :goto_c
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_22
    move v1, v3

    .line 1115465
    goto/16 :goto_10

    :cond_23
    const/4 v4, 0x0

    goto/16 :goto_11
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1115466
    iget-object v0, p0, LX/6d6;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1115467
    return-void
.end method
