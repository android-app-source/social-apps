.class public final LX/56N;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 837808
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 837809
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 837810
    :goto_0
    return v1

    .line 837811
    :cond_0
    const-string v9, "page_rating"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 837812
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    .line 837813
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 837814
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 837815
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 837816
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 837817
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 837818
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 837819
    :cond_2
    const-string v9, "privacy_scope"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 837820
    invoke-static {p0, p1}, LX/5uE;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 837821
    :cond_3
    const-string v9, "represented_profile"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 837822
    invoke-static {p0, p1}, LX/56M;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 837823
    :cond_4
    const-string v9, "value"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 837824
    invoke-static {p0, p1}, LX/5u1;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 837825
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 837826
    :cond_6
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 837827
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 837828
    if-eqz v0, :cond_7

    .line 837829
    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 837830
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 837831
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 837832
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 837833
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 837834
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 837835
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 837836
    if-eqz v0, :cond_0

    .line 837837
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837838
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837839
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 837840
    if-eqz v0, :cond_1

    .line 837841
    const-string v1, "page_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837842
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 837843
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837844
    if-eqz v0, :cond_2

    .line 837845
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837846
    invoke-static {p0, v0, p2, p3}, LX/5uE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 837847
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837848
    if-eqz v0, :cond_3

    .line 837849
    const-string v1, "represented_profile"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837850
    invoke-static {p0, v0, p2}, LX/56M;->a(LX/15i;ILX/0nX;)V

    .line 837851
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837852
    if-eqz v0, :cond_4

    .line 837853
    const-string v1, "value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837854
    invoke-static {p0, v0, p2}, LX/5u1;->a(LX/15i;ILX/0nX;)V

    .line 837855
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 837856
    return-void
.end method
