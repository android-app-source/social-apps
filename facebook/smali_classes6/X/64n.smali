.class public final LX/64n;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:[Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/64m;)V
    .locals 2

    .prologue
    .line 1045487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045488
    iget-object v0, p1, LX/64m;->a:Ljava/util/List;

    iget-object v1, p1, LX/64m;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, LX/64n;->a:[Ljava/lang/String;

    .line 1045489
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1045462
    iget-object v0, p0, LX/64n;->a:[Ljava/lang/String;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1045463
    iget-object v0, p0, LX/64n;->a:[Ljava/lang/String;

    mul-int/lit8 v1, p1, 0x2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1045464
    iget-object v0, p0, LX/64n;->a:[Ljava/lang/String;

    .line 1045465
    array-length v1, v0

    add-int/lit8 v1, v1, -0x2

    :goto_0
    if-ltz v1, :cond_1

    .line 1045466
    aget-object p0, v0, v1

    invoke-virtual {p1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1045467
    add-int/lit8 v1, v1, 0x1

    aget-object v1, v0, v1

    .line 1045468
    :goto_1
    move-object v0, v1

    .line 1045469
    return-object v0

    .line 1045470
    :cond_0
    add-int/lit8 v1, v1, -0x2

    goto :goto_0

    .line 1045471
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1045472
    iget-object v0, p0, LX/64n;->a:[Ljava/lang/String;

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/util/Date;
    .locals 1

    .prologue
    .line 1045473
    invoke-virtual {p0, p1}, LX/64n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1045474
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/66L;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1045475
    instance-of v0, p1, LX/64n;

    if-eqz v0, :cond_0

    check-cast p1, LX/64n;

    iget-object v0, p1, LX/64n;->a:[Ljava/lang/String;

    iget-object v1, p0, LX/64n;->a:[Ljava/lang/String;

    .line 1045476
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 1045477
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1045478
    iget-object v0, p0, LX/64n;->a:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final newBuilder()LX/64m;
    .locals 3

    .prologue
    .line 1045479
    new-instance v0, LX/64m;

    invoke-direct {v0}, LX/64m;-><init>()V

    .line 1045480
    iget-object v1, v0, LX/64m;->a:Ljava/util/List;

    iget-object v2, p0, LX/64n;->a:[Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 1045481
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1045482
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1045483
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/64n;->a()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 1045484
    invoke-virtual {p0, v0}, LX/64n;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v0}, LX/64n;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045485
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1045486
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
