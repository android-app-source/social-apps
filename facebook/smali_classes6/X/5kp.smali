.class public final LX/5kp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 996358
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 996359
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 996360
    :goto_0
    return v1

    .line 996361
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 996362
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 996363
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 996364
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 996365
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 996366
    const-string v6, "facebox_center"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 996367
    invoke-static {p0, p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 996368
    :cond_2
    const-string v6, "facebox_size"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 996369
    invoke-static {p0, p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 996370
    :cond_3
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 996371
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 996372
    :cond_4
    const-string v6, "tag_suggestions"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 996373
    invoke-static {p0, p1}, LX/5kr;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 996374
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 996375
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 996376
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 996377
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 996378
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 996379
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 996380
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 996381
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 996382
    if-eqz v0, :cond_0

    .line 996383
    const-string v1, "facebox_center"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996384
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 996385
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 996386
    if-eqz v0, :cond_1

    .line 996387
    const-string v1, "facebox_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996388
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 996389
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 996390
    if-eqz v0, :cond_2

    .line 996391
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996392
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 996393
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 996394
    if-eqz v0, :cond_3

    .line 996395
    const-string v1, "tag_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996396
    invoke-static {p0, v0, p2, p3}, LX/5kr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 996397
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 996398
    return-void
.end method
