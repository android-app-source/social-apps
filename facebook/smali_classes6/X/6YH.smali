.class public final synthetic LX/6YH;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1109664
    invoke-static {}, LX/2nT;->values()[LX/2nT;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6YH;->d:[I

    :try_start_0
    sget-object v0, LX/6YH;->d:[I

    sget-object v1, LX/2nT;->SPINNER:LX/2nT;

    invoke-virtual {v1}, LX/2nT;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_b

    :goto_0
    :try_start_1
    sget-object v0, LX/6YH;->d:[I

    sget-object v1, LX/2nT;->DATA_CONTROL_WITHOUT_UPSELL:LX/2nT;

    invoke-virtual {v1}, LX/2nT;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_a

    :goto_1
    :try_start_2
    sget-object v0, LX/6YH;->d:[I

    sget-object v1, LX/2nT;->DEFAULT:LX/2nT;

    invoke-virtual {v1}, LX/2nT;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_9

    .line 1109665
    :goto_2
    invoke-static {}, LX/4g1;->values()[LX/4g1;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6YH;->c:[I

    :try_start_3
    sget-object v0, LX/6YH;->c:[I

    sget-object v1, LX/4g1;->UPSELL_WITH_DATA_CONTROL:LX/4g1;

    invoke-virtual {v1}, LX/4g1;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_8

    :goto_3
    :try_start_4
    sget-object v0, LX/6YH;->c:[I

    sget-object v1, LX/4g1;->UPSELL_WITHOUT_DATA_CONTROL:LX/4g1;

    invoke-virtual {v1}, LX/4g1;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_7

    :goto_4
    :try_start_5
    sget-object v0, LX/6YH;->c:[I

    sget-object v1, LX/4g1;->DATA_CONTROL_WITHOUT_UPSELL:LX/4g1;

    invoke-virtual {v1}, LX/4g1;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_6

    :goto_5
    :try_start_6
    sget-object v0, LX/6YH;->c:[I

    sget-object v1, LX/4g1;->NO_DATA_CONTROL_NO_UPSELL:LX/4g1;

    invoke-virtual {v1}, LX/4g1;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_5

    .line 1109666
    :goto_6
    invoke-static {}, LX/4g0;->values()[LX/4g0;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6YH;->b:[I

    :try_start_7
    sget-object v0, LX/6YH;->b:[I

    sget-object v1, LX/4g0;->DATA_CONTROL_FAILURE:LX/4g0;

    invoke-virtual {v1}, LX/4g0;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_4

    :goto_7
    :try_start_8
    sget-object v0, LX/6YH;->b:[I

    sget-object v1, LX/4g0;->UPSELL_FAILURE:LX/4g0;

    invoke-virtual {v1}, LX/4g0;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_3

    .line 1109667
    :goto_8
    invoke-static {}, LX/4g3;->values()[LX/4g3;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/6YH;->a:[I

    :try_start_9
    sget-object v0, LX/6YH;->a:[I

    sget-object v1, LX/4g3;->CONFIRM:LX/4g3;

    invoke-virtual {v1}, LX/4g3;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2

    :goto_9
    :try_start_a
    sget-object v0, LX/6YH;->a:[I

    sget-object v1, LX/4g3;->CANCEL:LX/4g3;

    invoke-virtual {v1}, LX/4g3;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1

    :goto_a
    :try_start_b
    sget-object v0, LX/6YH;->a:[I

    sget-object v1, LX/4g3;->FAILURE:LX/4g3;

    invoke-virtual {v1}, LX/4g3;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_0

    :goto_b
    return-void

    :catch_0
    goto :goto_b

    :catch_1
    goto :goto_a

    :catch_2
    goto :goto_9

    :catch_3
    goto :goto_8

    :catch_4
    goto :goto_7

    :catch_5
    goto :goto_6

    :catch_6
    goto :goto_5

    :catch_7
    goto :goto_4

    :catch_8
    goto :goto_3

    :catch_9
    goto/16 :goto_2

    :catch_a
    goto/16 :goto_1

    :catch_b
    goto/16 :goto_0
.end method
