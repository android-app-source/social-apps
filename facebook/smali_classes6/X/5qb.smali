.class public LX/5qb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1009471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1009472
    invoke-static {}, LX/5qb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1009473
    const-string v0, "10.0.3.2:8081"

    .line 1009474
    :goto_0
    return-object v0

    .line 1009475
    :cond_0
    invoke-static {}, LX/5qb;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1009476
    const-string v0, "10.0.2.2:8081"

    goto :goto_0

    .line 1009477
    :cond_1
    const-string v0, "localhost:8081"

    goto :goto_0
.end method

.method private static b()Z
    .locals 2

    .prologue
    .line 1009478
    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    const-string v1, "vbox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private static c()Z
    .locals 2

    .prologue
    .line 1009479
    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    const-string v1, "generic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
