.class public final LX/5i4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 72

    .prologue
    .line 981568
    const/16 v65, 0x0

    .line 981569
    const/16 v64, 0x0

    .line 981570
    const/16 v61, 0x0

    .line 981571
    const-wide/16 v62, 0x0

    .line 981572
    const/16 v60, 0x0

    .line 981573
    const/16 v57, 0x0

    .line 981574
    const-wide/16 v58, 0x0

    .line 981575
    const/16 v56, 0x0

    .line 981576
    const/16 v55, 0x0

    .line 981577
    const/16 v54, 0x0

    .line 981578
    const/16 v53, 0x0

    .line 981579
    const/16 v52, 0x0

    .line 981580
    const/16 v51, 0x0

    .line 981581
    const/16 v50, 0x0

    .line 981582
    const/16 v49, 0x0

    .line 981583
    const/16 v48, 0x0

    .line 981584
    const/16 v47, 0x0

    .line 981585
    const/16 v46, 0x0

    .line 981586
    const/16 v43, 0x0

    .line 981587
    const-wide/16 v44, 0x0

    .line 981588
    const/16 v42, 0x0

    .line 981589
    const/16 v41, 0x0

    .line 981590
    const/16 v40, 0x0

    .line 981591
    const/16 v39, 0x0

    .line 981592
    const/16 v38, 0x0

    .line 981593
    const/16 v37, 0x0

    .line 981594
    const/16 v36, 0x0

    .line 981595
    const-wide/16 v34, 0x0

    .line 981596
    const-wide/16 v32, 0x0

    .line 981597
    const/16 v31, 0x0

    .line 981598
    const/16 v30, 0x0

    .line 981599
    const/16 v29, 0x0

    .line 981600
    const/16 v28, 0x0

    .line 981601
    const/16 v27, 0x0

    .line 981602
    const/16 v26, 0x0

    .line 981603
    const/16 v25, 0x0

    .line 981604
    const/16 v24, 0x0

    .line 981605
    const/16 v23, 0x0

    .line 981606
    const/16 v22, 0x0

    .line 981607
    const/16 v21, 0x0

    .line 981608
    const/16 v20, 0x0

    .line 981609
    const/16 v19, 0x0

    .line 981610
    const/16 v18, 0x0

    .line 981611
    const/16 v17, 0x0

    .line 981612
    const/16 v16, 0x0

    .line 981613
    const/4 v15, 0x0

    .line 981614
    const/4 v14, 0x0

    .line 981615
    const/4 v13, 0x0

    .line 981616
    const/4 v12, 0x0

    .line 981617
    const/4 v11, 0x0

    .line 981618
    const/4 v10, 0x0

    .line 981619
    const/4 v9, 0x0

    .line 981620
    const/4 v8, 0x0

    .line 981621
    const/4 v7, 0x0

    .line 981622
    const/4 v6, 0x0

    .line 981623
    const/4 v5, 0x0

    .line 981624
    const/4 v4, 0x0

    .line 981625
    const/4 v3, 0x0

    .line 981626
    const/4 v2, 0x0

    .line 981627
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v66

    sget-object v67, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v66

    move-object/from16 v1, v67

    if-eq v0, v1, :cond_3d

    .line 981628
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 981629
    const/4 v2, 0x0

    .line 981630
    :goto_0
    return v2

    .line 981631
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v67, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v67

    if-eq v2, v0, :cond_26

    .line 981632
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 981633
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 981634
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v67

    sget-object v68, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v67

    move-object/from16 v1, v68

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 981635
    const-string v67, "can_viewer_delete"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_1

    .line 981636
    const/4 v2, 0x1

    .line 981637
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v66, v7

    move v7, v2

    goto :goto_1

    .line 981638
    :cond_1
    const-string v67, "can_viewer_report"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_2

    .line 981639
    const/4 v2, 0x1

    .line 981640
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v65, v6

    move v6, v2

    goto :goto_1

    .line 981641
    :cond_2
    const-string v67, "captions_url"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_3

    .line 981642
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v64, v2

    goto :goto_1

    .line 981643
    :cond_3
    const-string v67, "created_time"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_4

    .line 981644
    const/4 v2, 0x1

    .line 981645
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 981646
    :cond_4
    const-string v67, "creation_story"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_5

    .line 981647
    invoke-static/range {p0 .. p1}, LX/5i3;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v63, v2

    goto :goto_1

    .line 981648
    :cond_5
    const-string v67, "enable_focus"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_6

    .line 981649
    const/4 v2, 0x1

    .line 981650
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    move/from16 v62, v26

    move/from16 v26, v2

    goto/16 :goto_1

    .line 981651
    :cond_6
    const-string v67, "focus_width_degrees"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_7

    .line 981652
    const/4 v2, 0x1

    .line 981653
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v60

    move/from16 v25, v2

    goto/16 :goto_1

    .line 981654
    :cond_7
    const-string v67, "guided_tour"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_8

    .line 981655
    invoke-static/range {p0 .. p1}, LX/5CB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v59, v2

    goto/16 :goto_1

    .line 981656
    :cond_8
    const-string v67, "height"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_9

    .line 981657
    const/4 v2, 0x1

    .line 981658
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v24

    move/from16 v58, v24

    move/from16 v24, v2

    goto/16 :goto_1

    .line 981659
    :cond_9
    const-string v67, "id"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_a

    .line 981660
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v57, v2

    goto/16 :goto_1

    .line 981661
    :cond_a
    const-string v67, "initial_view_heading_degrees"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_b

    .line 981662
    const/4 v2, 0x1

    .line 981663
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v23

    move/from16 v56, v23

    move/from16 v23, v2

    goto/16 :goto_1

    .line 981664
    :cond_b
    const-string v67, "initial_view_pitch_degrees"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_c

    .line 981665
    const/4 v2, 0x1

    .line 981666
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v22

    move/from16 v55, v22

    move/from16 v22, v2

    goto/16 :goto_1

    .line 981667
    :cond_c
    const-string v67, "initial_view_roll_degrees"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_d

    .line 981668
    const/4 v2, 0x1

    .line 981669
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v21

    move/from16 v54, v21

    move/from16 v21, v2

    goto/16 :goto_1

    .line 981670
    :cond_d
    const-string v67, "is_looping"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_e

    .line 981671
    const/4 v2, 0x1

    .line 981672
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    move/from16 v53, v20

    move/from16 v20, v2

    goto/16 :goto_1

    .line 981673
    :cond_e
    const-string v67, "is_playable"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_f

    .line 981674
    const/4 v2, 0x1

    .line 981675
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    move/from16 v52, v19

    move/from16 v19, v2

    goto/16 :goto_1

    .line 981676
    :cond_f
    const-string v67, "is_spherical"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_10

    .line 981677
    const/4 v2, 0x1

    .line 981678
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    move/from16 v51, v18

    move/from16 v18, v2

    goto/16 :goto_1

    .line 981679
    :cond_10
    const-string v67, "is_video_broadcast"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_11

    .line 981680
    const/4 v2, 0x1

    .line 981681
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    move/from16 v50, v17

    move/from16 v17, v2

    goto/16 :goto_1

    .line 981682
    :cond_11
    const-string v67, "loop_count"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_12

    .line 981683
    const/4 v2, 0x1

    .line 981684
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    move/from16 v49, v16

    move/from16 v16, v2

    goto/16 :goto_1

    .line 981685
    :cond_12
    const-string v67, "message"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_13

    .line 981686
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v48, v2

    goto/16 :goto_1

    .line 981687
    :cond_13
    const-string v67, "off_focus_level"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_14

    .line 981688
    const/4 v2, 0x1

    .line 981689
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v46

    move v15, v2

    goto/16 :goto_1

    .line 981690
    :cond_14
    const-string v67, "play_count"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_15

    .line 981691
    const/4 v2, 0x1

    .line 981692
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v45, v14

    move v14, v2

    goto/16 :goto_1

    .line 981693
    :cond_15
    const-string v67, "playable_duration_in_ms"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_16

    .line 981694
    const/4 v2, 0x1

    .line 981695
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    move/from16 v44, v13

    move v13, v2

    goto/16 :goto_1

    .line 981696
    :cond_16
    const-string v67, "playable_url"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_17

    .line 981697
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v43, v2

    goto/16 :goto_1

    .line 981698
    :cond_17
    const-string v67, "playable_url_hd"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_18

    .line 981699
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 981700
    :cond_18
    const-string v67, "playlist"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_19

    .line 981701
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 981702
    :cond_19
    const-string v67, "preferredPlayableUrlString"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_1a

    .line 981703
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 981704
    :cond_1a
    const-string v67, "projection_type"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_1b

    .line 981705
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 981706
    :cond_1b
    const-string v67, "sphericalFullscreenAspectRatio"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_1c

    .line 981707
    const/4 v2, 0x1

    .line 981708
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v38

    move v12, v2

    goto/16 :goto_1

    .line 981709
    :cond_1c
    const-string v67, "sphericalInlineAspectRatio"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_1d

    .line 981710
    const/4 v2, 0x1

    .line 981711
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v36

    move v11, v2

    goto/16 :goto_1

    .line 981712
    :cond_1d
    const-string v67, "sphericalPlayableUrlHdString"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_1e

    .line 981713
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 981714
    :cond_1e
    const-string v67, "sphericalPlayableUrlSdString"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_1f

    .line 981715
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 981716
    :cond_1f
    const-string v67, "sphericalPreferredFov"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_20

    .line 981717
    const/4 v2, 0x1

    .line 981718
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    move/from16 v32, v10

    move v10, v2

    goto/16 :goto_1

    .line 981719
    :cond_20
    const-string v67, "supports_time_slices"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_21

    .line 981720
    const/4 v2, 0x1

    .line 981721
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v31, v9

    move v9, v2

    goto/16 :goto_1

    .line 981722
    :cond_21
    const-string v67, "title"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_22

    .line 981723
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 981724
    :cond_22
    const-string v67, "videoThumbnail"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_23

    .line 981725
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 981726
    :cond_23
    const-string v67, "video_captions_locales"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v67

    if-eqz v67, :cond_24

    .line 981727
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto/16 :goto_1

    .line 981728
    :cond_24
    const-string v67, "width"

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 981729
    const/4 v2, 0x1

    .line 981730
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move/from16 v27, v8

    move v8, v2

    goto/16 :goto_1

    .line 981731
    :cond_25
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 981732
    :cond_26
    const/16 v2, 0x25

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 981733
    if-eqz v7, :cond_27

    .line 981734
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v66

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 981735
    :cond_27
    if-eqz v6, :cond_28

    .line 981736
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v65

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 981737
    :cond_28
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v64

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981738
    if-eqz v3, :cond_29

    .line 981739
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 981740
    :cond_29
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v63

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981741
    if-eqz v26, :cond_2a

    .line 981742
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v62

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 981743
    :cond_2a
    if-eqz v25, :cond_2b

    .line 981744
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v60

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 981745
    :cond_2b
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981746
    if-eqz v24, :cond_2c

    .line 981747
    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 981748
    :cond_2c
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981749
    if-eqz v23, :cond_2d

    .line 981750
    const/16 v2, 0xa

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 981751
    :cond_2d
    if-eqz v22, :cond_2e

    .line 981752
    const/16 v2, 0xb

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 981753
    :cond_2e
    if-eqz v21, :cond_2f

    .line 981754
    const/16 v2, 0xc

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 981755
    :cond_2f
    if-eqz v20, :cond_30

    .line 981756
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 981757
    :cond_30
    if-eqz v19, :cond_31

    .line 981758
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 981759
    :cond_31
    if-eqz v18, :cond_32

    .line 981760
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 981761
    :cond_32
    if-eqz v17, :cond_33

    .line 981762
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 981763
    :cond_33
    if-eqz v16, :cond_34

    .line 981764
    const/16 v2, 0x11

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 981765
    :cond_34
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981766
    if-eqz v15, :cond_35

    .line 981767
    const/16 v3, 0x13

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v46

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 981768
    :cond_35
    if-eqz v14, :cond_36

    .line 981769
    const/16 v2, 0x14

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 981770
    :cond_36
    if-eqz v13, :cond_37

    .line 981771
    const/16 v2, 0x15

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 981772
    :cond_37
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981773
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981774
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981775
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981776
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981777
    if-eqz v12, :cond_38

    .line 981778
    const/16 v3, 0x1b

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v38

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 981779
    :cond_38
    if-eqz v11, :cond_39

    .line 981780
    const/16 v3, 0x1c

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v36

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 981781
    :cond_39
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981782
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981783
    if-eqz v10, :cond_3a

    .line 981784
    const/16 v2, 0x1f

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 981785
    :cond_3a
    if-eqz v9, :cond_3b

    .line 981786
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 981787
    :cond_3b
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981788
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981789
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 981790
    if-eqz v8, :cond_3c

    .line 981791
    const/16 v2, 0x24

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 981792
    :cond_3c
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_3d
    move/from16 v66, v65

    move/from16 v65, v64

    move/from16 v64, v61

    move/from16 v69, v5

    move/from16 v70, v6

    move/from16 v6, v22

    move/from16 v22, v16

    move/from16 v16, v10

    move v10, v4

    move-wide/from16 v4, v62

    move/from16 v62, v57

    move/from16 v63, v60

    move/from16 v57, v54

    move-wide/from16 v60, v58

    move/from16 v54, v51

    move/from16 v58, v55

    move/from16 v59, v56

    move/from16 v51, v48

    move/from16 v55, v52

    move/from16 v56, v53

    move/from16 v48, v43

    move/from16 v52, v49

    move/from16 v53, v50

    move/from16 v43, v40

    move/from16 v49, v46

    move/from16 v50, v47

    move/from16 v40, v37

    move-wide/from16 v46, v44

    move/from16 v44, v41

    move/from16 v45, v42

    move/from16 v41, v38

    move/from16 v42, v39

    move-wide/from16 v38, v34

    move/from16 v35, v36

    move/from16 v34, v31

    move-wide/from16 v36, v32

    move/from16 v31, v28

    move/from16 v33, v30

    move/from16 v32, v29

    move/from16 v28, v25

    move/from16 v30, v27

    move/from16 v29, v26

    move/from16 v25, v19

    move/from16 v27, v24

    move/from16 v26, v20

    move/from16 v19, v13

    move/from16 v20, v14

    move/from16 v24, v18

    move v13, v7

    move/from16 v7, v23

    move/from16 v18, v12

    move v14, v8

    move/from16 v23, v17

    move/from16 v12, v70

    move v8, v2

    move/from16 v17, v11

    move/from16 v11, v69

    move/from16 v71, v9

    move v9, v3

    move/from16 v3, v21

    move/from16 v21, v15

    move/from16 v15, v71

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 981793
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 981794
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 981795
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5i4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 981796
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 981797
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 981798
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 981799
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 981800
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 981801
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 981802
    invoke-static {p0, p1}, LX/5i4;->a(LX/15w;LX/186;)I

    move-result v1

    .line 981803
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 981804
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/16 v6, 0x23

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 981805
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 981806
    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v0

    .line 981807
    if-eqz v0, :cond_0

    .line 981808
    const-string v1, "can_viewer_delete"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981809
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981810
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981811
    if-eqz v0, :cond_1

    .line 981812
    const-string v1, "can_viewer_report"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981813
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981814
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981815
    if-eqz v0, :cond_2

    .line 981816
    const-string v1, "captions_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981817
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981818
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v8, v9}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 981819
    cmp-long v2, v0, v8

    if-eqz v2, :cond_3

    .line 981820
    const-string v2, "created_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981821
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 981822
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981823
    if-eqz v0, :cond_4

    .line 981824
    const-string v1, "creation_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981825
    invoke-static {p0, v0, p2, p3}, LX/5i3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 981826
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981827
    if-eqz v0, :cond_5

    .line 981828
    const-string v1, "enable_focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981829
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981830
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 981831
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_6

    .line 981832
    const-string v2, "focus_width_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981833
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 981834
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981835
    if-eqz v0, :cond_7

    .line 981836
    const-string v1, "guided_tour"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981837
    invoke-static {p0, v0, p2, p3}, LX/5CB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 981838
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 981839
    if-eqz v0, :cond_8

    .line 981840
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981841
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981842
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981843
    if-eqz v0, :cond_9

    .line 981844
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981845
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981846
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 981847
    if-eqz v0, :cond_a

    .line 981848
    const-string v1, "initial_view_heading_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981849
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981850
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 981851
    if-eqz v0, :cond_b

    .line 981852
    const-string v1, "initial_view_pitch_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981853
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981854
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 981855
    if-eqz v0, :cond_c

    .line 981856
    const-string v1, "initial_view_roll_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981857
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981858
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981859
    if-eqz v0, :cond_d

    .line 981860
    const-string v1, "is_looping"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981861
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981862
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981863
    if-eqz v0, :cond_e

    .line 981864
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981865
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981866
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981867
    if-eqz v0, :cond_f

    .line 981868
    const-string v1, "is_spherical"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981869
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981870
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981871
    if-eqz v0, :cond_10

    .line 981872
    const-string v1, "is_video_broadcast"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981873
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981874
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 981875
    if-eqz v0, :cond_11

    .line 981876
    const-string v1, "loop_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981877
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981878
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981879
    if-eqz v0, :cond_12

    .line 981880
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981881
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 981882
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 981883
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_13

    .line 981884
    const-string v2, "off_focus_level"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981885
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 981886
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 981887
    if-eqz v0, :cond_14

    .line 981888
    const-string v1, "play_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981889
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981890
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 981891
    if-eqz v0, :cond_15

    .line 981892
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981893
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981894
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981895
    if-eqz v0, :cond_16

    .line 981896
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981897
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981898
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981899
    if-eqz v0, :cond_17

    .line 981900
    const-string v1, "playable_url_hd"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981901
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981902
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981903
    if-eqz v0, :cond_18

    .line 981904
    const-string v1, "playlist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981905
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981906
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981907
    if-eqz v0, :cond_19

    .line 981908
    const-string v1, "preferredPlayableUrlString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981909
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981910
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981911
    if-eqz v0, :cond_1a

    .line 981912
    const-string v1, "projection_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981913
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981914
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 981915
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1b

    .line 981916
    const-string v2, "sphericalFullscreenAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981917
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 981918
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 981919
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1c

    .line 981920
    const-string v2, "sphericalInlineAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981921
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 981922
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981923
    if-eqz v0, :cond_1d

    .line 981924
    const-string v1, "sphericalPlayableUrlHdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981925
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981926
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981927
    if-eqz v0, :cond_1e

    .line 981928
    const-string v1, "sphericalPlayableUrlSdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981929
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981930
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 981931
    if-eqz v0, :cond_1f

    .line 981932
    const-string v1, "sphericalPreferredFov"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981933
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981934
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 981935
    if-eqz v0, :cond_20

    .line 981936
    const-string v1, "supports_time_slices"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981937
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 981938
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981939
    if-eqz v0, :cond_21

    .line 981940
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981941
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 981942
    :cond_21
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981943
    if-eqz v0, :cond_22

    .line 981944
    const-string v1, "videoThumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981945
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 981946
    :cond_22
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 981947
    if-eqz v0, :cond_23

    .line 981948
    const-string v0, "video_captions_locales"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981949
    invoke-virtual {p0, p1, v6}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 981950
    :cond_23
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 981951
    if-eqz v0, :cond_24

    .line 981952
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981953
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 981954
    :cond_24
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 981955
    return-void
.end method
