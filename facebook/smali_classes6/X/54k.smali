.class public final LX/54k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# static fields
.field public static final a:LX/54j;

.field public static final c:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater",
            "<",
            "LX/54k;",
            "LX/54j;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile b:LX/54j;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 828286
    new-instance v0, LX/54j;

    const/4 v1, 0x0

    .line 828287
    sget-object v2, LX/0ze;->a:LX/0zf;

    move-object v2, v2

    .line 828288
    invoke-direct {v0, v1, v2}, LX/54j;-><init>(ZLX/0za;)V

    sput-object v0, LX/54k;->a:LX/54j;

    .line 828289
    const-class v0, LX/54k;

    const-class v1, LX/54j;

    const-string v2, "b"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, LX/54k;->c:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 828290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828291
    sget-object v0, LX/54k;->a:LX/54j;

    iput-object v0, p0, LX/54k;->b:LX/54j;

    .line 828292
    return-void
.end method


# virtual methods
.method public final a(LX/0za;)V
    .locals 3

    .prologue
    .line 828293
    if-nez p1, :cond_0

    .line 828294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Subscription can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 828295
    :cond_0
    iget-object v0, p0, LX/54k;->b:LX/54j;

    .line 828296
    iget-boolean v1, v0, LX/54j;->a:Z

    if-eqz v1, :cond_1

    .line 828297
    invoke-interface {p1}, LX/0za;->b()V

    .line 828298
    :goto_0
    return-void

    .line 828299
    :cond_1
    new-instance v1, LX/54j;

    iget-boolean v2, v0, LX/54j;->a:Z

    invoke-direct {v1, v2, p1}, LX/54j;-><init>(ZLX/0za;)V

    move-object v1, v1

    .line 828300
    sget-object v2, LX/54k;->c:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v2, p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 828301
    :cond_0
    iget-object v0, p0, LX/54k;->b:LX/54j;

    .line 828302
    iget-boolean v1, v0, LX/54j;->a:Z

    if-eqz v1, :cond_1

    .line 828303
    :goto_0
    return-void

    .line 828304
    :cond_1
    new-instance v1, LX/54j;

    const/4 v2, 0x1

    iget-object v3, v0, LX/54j;->b:LX/0za;

    invoke-direct {v1, v2, v3}, LX/54j;-><init>(ZLX/0za;)V

    move-object v1, v1

    .line 828305
    sget-object v2, LX/54k;->c:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v2, p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 828306
    iget-object v0, v0, LX/54j;->b:LX/0za;

    invoke-interface {v0}, LX/0za;->b()V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 828307
    iget-object v0, p0, LX/54k;->b:LX/54j;

    iget-boolean v0, v0, LX/54j;->a:Z

    return v0
.end method
