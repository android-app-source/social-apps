.class public LX/6Wd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(LX/6Wd;)V
    .locals 2

    .prologue
    .line 1106434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106435
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/6Wd;->b:Ljava/lang/StringBuilder;

    .line 1106436
    iget-object v0, p1, LX/6Wd;->a:Ljava/lang/String;

    iput-object v0, p0, LX/6Wd;->a:Ljava/lang/String;

    .line 1106437
    iget-object v0, p0, LX/6Wd;->b:Ljava/lang/StringBuilder;

    iget-object v1, p1, LX/6Wd;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1106438
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1106439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106440
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/6Wd;->b:Ljava/lang/StringBuilder;

    .line 1106441
    iput-object p1, p0, LX/6Wd;->a:Ljava/lang/String;

    .line 1106442
    return-void
.end method

.method public static final a(LX/6Wd;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1106443
    iget-object v0, p0, LX/6Wd;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1106444
    return-void
.end method


# virtual methods
.method public a(LX/6Wn;)LX/6Wi;
    .locals 2

    .prologue
    .line 1106445
    new-instance v0, LX/6Wi;

    invoke-direct {v0, p0}, LX/6Wi;-><init>(LX/6Wd;)V

    .line 1106446
    const-string v1, " FROM "

    invoke-static {v0, v1}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1106447
    invoke-virtual {p1}, LX/6Wn;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1106448
    return-object v0
.end method

.method public varargs a([LX/6Wc;)LX/6Wk;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1106449
    new-instance v3, LX/6Wk;

    invoke-direct {v3, p0}, LX/6Wk;-><init>(LX/6Wd;)V

    .line 1106450
    const-string v0, "SELECT"

    invoke-static {v3, v0}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1106451
    const/4 v0, 0x1

    .line 1106452
    array-length v4, p1

    move v2, v0

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, p1, v0

    .line 1106453
    if-nez v2, :cond_0

    .line 1106454
    const-string v2, ","

    invoke-static {v3, v2}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1106455
    :cond_0
    const-string v2, " "

    invoke-static {v3, v2}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1106456
    invoke-virtual {v5}, LX/6Wc;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1106457
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    goto :goto_0

    .line 1106458
    :cond_1
    return-object v3
.end method

.method public a(LX/6WX;)LX/6Wl;
    .locals 2

    .prologue
    .line 1106459
    new-instance v0, LX/6Wl;

    invoke-direct {v0, p0}, LX/6Wl;-><init>(LX/6Wd;)V

    .line 1106460
    const-string v1, " WHERE "

    invoke-static {v0, v1}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1106461
    invoke-virtual {p1}, LX/6WX;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/6Wd;->a(LX/6Wd;Ljava/lang/CharSequence;)V

    .line 1106462
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1106463
    iget-object v0, p0, LX/6Wd;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
