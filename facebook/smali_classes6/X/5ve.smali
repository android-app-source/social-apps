.class public LX/5ve;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1022613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/Bundle;)LX/36O;
    .locals 1

    .prologue
    .line 1022612
    const-string v0, "graphql_profile"

    invoke-static {p0, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/36O;

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1022598
    sparse-switch p1, :sswitch_data_0

    .line 1022599
    :goto_0
    return-void

    .line 1022600
    :sswitch_0
    invoke-static {p0, p2, p3, p4}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1022601
    :sswitch_1
    invoke-static {p0, p2, p4, p3}, LX/5ve;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1022602
    :sswitch_2
    new-instance v0, LX/5QQ;

    invoke-direct {v0}, LX/5QQ;-><init>()V

    invoke-virtual {v0, p2}, LX/5QQ;->a(Ljava/lang/String;)LX/5QQ;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/5QQ;->b(Ljava/lang/String;)LX/5QQ;

    move-result-object v0

    .line 1022603
    iget-object p1, v0, LX/5QQ;->a:Landroid/os/Bundle;

    move-object v0, p1

    .line 1022604
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x41e065f -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(Landroid/os/Bundle;LX/36O;)V
    .locals 3

    .prologue
    .line 1022605
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1022606
    invoke-interface {p1}, LX/36O;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/36O;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x25d6af

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1022607
    if-eqz v0, :cond_1

    .line 1022608
    invoke-interface {p1}, LX/36O;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/36O;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/36O;->cp_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/36O;->cp_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {p0, v1, v2, v0}, LX/5ve;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1022609
    :goto_2
    return-void

    .line 1022610
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1022611
    :cond_1
    const-string v0, "graphql_profile"

    invoke-static {p0, v0, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1022596
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 1022597
    return-void
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/1f8;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1022559
    new-instance v0, LX/5vg;

    invoke-direct {v0}, LX/5vg;-><init>()V

    .line 1022560
    iput-object p1, v0, LX/5vg;->d:Ljava/lang/String;

    .line 1022561
    move-object v0, v0

    .line 1022562
    new-instance v1, LX/4aM;

    invoke-direct {v1}, LX/4aM;-><init>()V

    .line 1022563
    iput-object p2, v1, LX/4aM;->b:Ljava/lang/String;

    .line 1022564
    move-object v1, v1

    .line 1022565
    invoke-virtual {v1}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    move-object v1, v1

    .line 1022566
    iput-object v1, v0, LX/5vg;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1022567
    move-object v0, v0

    .line 1022568
    iput-object p3, v0, LX/5vg;->e:Ljava/lang/String;

    .line 1022569
    move-object v1, v0

    .line 1022570
    if-nez p4, :cond_0

    const/4 v0, 0x0

    .line 1022571
    :goto_0
    iput-object v0, v1, LX/5vg;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    .line 1022572
    move-object v0, v1

    .line 1022573
    invoke-virtual {v0}, LX/5vg;->a()Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleExtendedGraphQLModel;

    move-result-object v0

    .line 1022574
    invoke-static {p0, v0}, LX/5ve;->a(Landroid/os/Bundle;LX/36O;)V

    .line 1022575
    return-void

    .line 1022576
    :cond_0
    new-instance v0, LX/5wY;

    invoke-direct {v0}, LX/5wY;-><init>()V

    new-instance p1, LX/4aM;

    invoke-direct {p1}, LX/4aM;-><init>()V

    .line 1022577
    iput-object p4, p1, LX/4aM;->b:Ljava/lang/String;

    .line 1022578
    move-object p1, p1

    .line 1022579
    invoke-virtual {p1}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    .line 1022580
    iput-object p1, v0, LX/5wY;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1022581
    move-object v0, v0

    .line 1022582
    invoke-virtual {v0}, LX/5wY;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v0

    .line 1022583
    new-instance p1, LX/5wZ;

    invoke-direct {p1}, LX/5wZ;-><init>()V

    .line 1022584
    iput-object v0, p1, LX/5wZ;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    .line 1022585
    move-object v0, p1

    .line 1022586
    invoke-static {p5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;->a(LX/1f8;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object p1

    .line 1022587
    iput-object p1, v0, LX/5wZ;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1022588
    move-object v0, v0

    .line 1022589
    invoke-virtual {v0}, LX/5wZ;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v0

    move-object v0, v0

    .line 1022590
    goto :goto_0
.end method

.method public static b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1022591
    if-eqz p1, :cond_0

    .line 1022592
    const-string v0, "model_bundle_page_id"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022593
    :cond_0
    const-string v0, "model_bundle_page_name"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022594
    const-string v0, "model_bundle_page_profile_pic_uri"

    invoke-virtual {p0, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022595
    return-void
.end method
