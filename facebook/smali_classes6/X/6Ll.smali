.class public LX/6Ll;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0w3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1078810
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1078811
    invoke-direct {p0}, LX/6Ll;->a()V

    .line 1078812
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1078807
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1078808
    invoke-direct {p0}, LX/6Ll;->a()V

    .line 1078809
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1078805
    const-class v0, LX/6Ll;

    invoke-static {v0, p0}, LX/6Ll;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1078806
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/6Ll;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/6Ll;

    invoke-static {v0}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v0

    check-cast v0, LX/0w3;

    iput-object v0, p0, LX/6Ll;->a:LX/0w3;

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 1078802
    iget-object v0, p0, LX/6Ll;->a:LX/0w3;

    invoke-virtual {v0, p0, p2}, LX/0w3;->a(Landroid/view/View;I)V

    .line 1078803
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1078804
    return-void
.end method
