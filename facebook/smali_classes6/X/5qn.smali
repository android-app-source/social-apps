.class public abstract LX/5qn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field private final a:LX/5pX;


# direct methods
.method public constructor <init>(LX/5pX;)V
    .locals 0

    .prologue
    .line 1009665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1009666
    iput-object p1, p0, LX/5qn;->a:LX/5pX;

    .line 1009667
    return-void
.end method


# virtual methods
.method public abstract a(J)V
.end method

.method public final doFrame(J)V
    .locals 3

    .prologue
    .line 1009668
    :try_start_0
    invoke-virtual {p0, p1, p2}, LX/5qn;->a(J)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1009669
    :goto_0
    return-void

    .line 1009670
    :catch_0
    move-exception v0

    .line 1009671
    iget-object v1, p0, LX/5qn;->a:LX/5pX;

    invoke-virtual {v1, v0}, LX/5pX;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method
