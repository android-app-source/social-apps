.class public final enum LX/5SD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5SD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5SD;

.field public static final enum EDIT_COVER_PHOTO:LX/5SD;

.field public static final enum EDIT_PROFILE_PIC:LX/5SD;

.field public static final enum VIEWING_MODE:LX/5SD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 917630
    new-instance v0, LX/5SD;

    const-string v1, "EDIT_PROFILE_PIC"

    invoke-direct {v0, v1, v2}, LX/5SD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SD;->EDIT_PROFILE_PIC:LX/5SD;

    .line 917631
    new-instance v0, LX/5SD;

    const-string v1, "EDIT_COVER_PHOTO"

    invoke-direct {v0, v1, v3}, LX/5SD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SD;->EDIT_COVER_PHOTO:LX/5SD;

    .line 917632
    new-instance v0, LX/5SD;

    const-string v1, "VIEWING_MODE"

    invoke-direct {v0, v1, v4}, LX/5SD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SD;->VIEWING_MODE:LX/5SD;

    .line 917633
    const/4 v0, 0x3

    new-array v0, v0, [LX/5SD;

    sget-object v1, LX/5SD;->EDIT_PROFILE_PIC:LX/5SD;

    aput-object v1, v0, v2

    sget-object v1, LX/5SD;->EDIT_COVER_PHOTO:LX/5SD;

    aput-object v1, v0, v3

    sget-object v1, LX/5SD;->VIEWING_MODE:LX/5SD;

    aput-object v1, v0, v4

    sput-object v0, LX/5SD;->$VALUES:[LX/5SD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 917636
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5SD;
    .locals 1

    .prologue
    .line 917635
    const-class v0, LX/5SD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5SD;

    return-object v0
.end method

.method public static values()[LX/5SD;
    .locals 1

    .prologue
    .line 917634
    sget-object v0, LX/5SD;->$VALUES:[LX/5SD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5SD;

    return-object v0
.end method
