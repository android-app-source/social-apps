.class public LX/6W5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6W5;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1104550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1104551
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1104552
    iput-object v0, p0, LX/6W5;->a:LX/0Ot;

    .line 1104553
    return-void
.end method

.method public static a(LX/0QB;)LX/6W5;
    .locals 4

    .prologue
    .line 1104554
    sget-object v0, LX/6W5;->b:LX/6W5;

    if-nez v0, :cond_1

    .line 1104555
    const-class v1, LX/6W5;

    monitor-enter v1

    .line 1104556
    :try_start_0
    sget-object v0, LX/6W5;->b:LX/6W5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1104557
    if-eqz v2, :cond_0

    .line 1104558
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1104559
    new-instance v3, LX/6W5;

    invoke-direct {v3}, LX/6W5;-><init>()V

    .line 1104560
    const/16 p0, 0xbc

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1104561
    iput-object p0, v3, LX/6W5;->a:LX/0Ot;

    .line 1104562
    move-object v0, v3

    .line 1104563
    sput-object v0, LX/6W5;->b:LX/6W5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1104564
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1104565
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1104566
    :cond_1
    sget-object v0, LX/6W5;->b:LX/6W5;

    return-object v0

    .line 1104567
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1104568
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
