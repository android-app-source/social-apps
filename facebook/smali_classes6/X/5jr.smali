.class public final LX/5jr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 989001
    const/4 v10, 0x0

    .line 989002
    const/4 v9, 0x0

    .line 989003
    const/4 v8, 0x0

    .line 989004
    const/4 v7, 0x0

    .line 989005
    const/4 v6, 0x0

    .line 989006
    const/4 v5, 0x0

    .line 989007
    const/4 v4, 0x0

    .line 989008
    const/4 v3, 0x0

    .line 989009
    const/4 v2, 0x0

    .line 989010
    const/4 v1, 0x0

    .line 989011
    const/4 v0, 0x0

    .line 989012
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 989013
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 989014
    const/4 v0, 0x0

    .line 989015
    :goto_0
    return v0

    .line 989016
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 989017
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 989018
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 989019
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 989020
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 989021
    const-string v12, "attribution_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 989022
    invoke-static {p0, p1}, LX/5jo;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 989023
    :cond_2
    const-string v12, "attribution_thumbnail"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 989024
    invoke-static {p0, p1}, LX/5jp;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 989025
    :cond_3
    const-string v12, "has_location_constraints"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 989026
    const/4 v1, 0x1

    .line 989027
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 989028
    :cond_4
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 989029
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 989030
    :cond_5
    const-string v12, "instructions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 989031
    invoke-static {p0, p1}, LX/5jq;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 989032
    :cond_6
    const-string v12, "is_logging_disabled"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 989033
    const/4 v0, 0x1

    .line 989034
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    goto :goto_1

    .line 989035
    :cond_7
    const-string v12, "model_uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 989036
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 989037
    :cond_8
    const-string v12, "model_weights_uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 989038
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 989039
    :cond_9
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 989040
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 989041
    :cond_a
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 989042
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 989043
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 989044
    if-eqz v1, :cond_b

    .line 989045
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 989046
    :cond_b
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 989047
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 989048
    if-eqz v0, :cond_c

    .line 989049
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 989050
    :cond_c
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 989051
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 989052
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 989053
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 988957
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 988958
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988959
    if-eqz v0, :cond_0

    .line 988960
    const-string v1, "attribution_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988961
    invoke-static {p0, v0, p2}, LX/5jo;->a(LX/15i;ILX/0nX;)V

    .line 988962
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988963
    if-eqz v0, :cond_1

    .line 988964
    const-string v1, "attribution_thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988965
    invoke-static {p0, v0, p2}, LX/5jp;->a(LX/15i;ILX/0nX;)V

    .line 988966
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 988967
    if-eqz v0, :cond_2

    .line 988968
    const-string v1, "has_location_constraints"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988969
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 988970
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 988971
    if-eqz v0, :cond_3

    .line 988972
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988973
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 988974
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988975
    if-eqz v0, :cond_5

    .line 988976
    const-string v1, "instructions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988977
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 988978
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 988979
    if-eqz v1, :cond_4

    .line 988980
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988981
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 988982
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 988983
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 988984
    if-eqz v0, :cond_6

    .line 988985
    const-string v1, "is_logging_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988986
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 988987
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 988988
    if-eqz v0, :cond_7

    .line 988989
    const-string v1, "model_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988990
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 988991
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 988992
    if-eqz v0, :cond_8

    .line 988993
    const-string v1, "model_weights_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988994
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 988995
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 988996
    if-eqz v0, :cond_9

    .line 988997
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988998
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 988999
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 989000
    return-void
.end method
