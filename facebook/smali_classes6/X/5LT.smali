.class public final LX/5LT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 901257
    const/4 v10, 0x0

    .line 901258
    const/4 v9, 0x0

    .line 901259
    const/4 v8, 0x0

    .line 901260
    const/4 v7, 0x0

    .line 901261
    const/4 v6, 0x0

    .line 901262
    const/4 v5, 0x0

    .line 901263
    const/4 v4, 0x0

    .line 901264
    const/4 v3, 0x0

    .line 901265
    const/4 v2, 0x0

    .line 901266
    const/4 v1, 0x0

    .line 901267
    const/4 v0, 0x0

    .line 901268
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 901269
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 901270
    const/4 v0, 0x0

    .line 901271
    :goto_0
    return v0

    .line 901272
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 901273
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 901274
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 901275
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 901276
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 901277
    const-string v12, "associated_places_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 901278
    invoke-static {p0, p1}, LX/5LQ;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 901279
    :cond_2
    const-string v12, "cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 901280
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 901281
    :cond_3
    const-string v12, "custom_icon_suggestions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 901282
    invoke-static {p0, p1}, LX/5Lg;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 901283
    :cond_4
    const-string v12, "display_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 901284
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 901285
    :cond_5
    const-string v12, "icon"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 901286
    invoke-static {p0, p1}, LX/5LR;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 901287
    :cond_6
    const-string v12, "iconImageLarge"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 901288
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 901289
    :cond_7
    const-string v12, "node"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 901290
    invoke-static {p0, p1}, LX/5Lr;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 901291
    :cond_8
    const-string v12, "show_attachment_preview"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 901292
    const/4 v0, 0x1

    .line 901293
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    goto/16 :goto_1

    .line 901294
    :cond_9
    const-string v12, "subtext"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 901295
    invoke-static {p0, p1}, LX/5LS;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 901296
    :cond_a
    const-string v12, "tracking"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 901297
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 901298
    :cond_b
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 901299
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 901300
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 901301
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 901302
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 901303
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 901304
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 901305
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 901306
    if-eqz v0, :cond_c

    .line 901307
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 901308
    :cond_c
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 901309
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 901310
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 901311
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 901312
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 901313
    if-eqz v0, :cond_0

    .line 901314
    const-string v1, "associated_places_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901315
    invoke-static {p0, v0, p2}, LX/5LQ;->a(LX/15i;ILX/0nX;)V

    .line 901316
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 901317
    if-eqz v0, :cond_1

    .line 901318
    const-string v1, "cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901319
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 901320
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 901321
    if-eqz v0, :cond_2

    .line 901322
    const-string v1, "custom_icon_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901323
    invoke-static {p0, v0, p2, p3}, LX/5Lg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 901324
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 901325
    if-eqz v0, :cond_3

    .line 901326
    const-string v1, "display_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901327
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 901328
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 901329
    if-eqz v0, :cond_4

    .line 901330
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901331
    invoke-static {p0, v0, p2}, LX/5LR;->a(LX/15i;ILX/0nX;)V

    .line 901332
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 901333
    if-eqz v0, :cond_5

    .line 901334
    const-string v1, "iconImageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901335
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 901336
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 901337
    if-eqz v0, :cond_6

    .line 901338
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901339
    invoke-static {p0, v0, p2, p3}, LX/5Lr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 901340
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 901341
    if-eqz v0, :cond_7

    .line 901342
    const-string v1, "show_attachment_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901343
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 901344
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 901345
    if-eqz v0, :cond_8

    .line 901346
    const-string v1, "subtext"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901347
    invoke-static {p0, v0, p2}, LX/5LS;->a(LX/15i;ILX/0nX;)V

    .line 901348
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 901349
    if-eqz v0, :cond_9

    .line 901350
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 901351
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 901352
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 901353
    return-void
.end method
