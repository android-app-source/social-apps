.class public LX/62Y;
.super LX/3x6;
.source ""


# instance fields
.field private final a:I

.field private final b:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1041735
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/62Y;-><init>(IZ)V

    .line 1041736
    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 1041756
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1041757
    iput p1, p0, LX/62Y;->a:I

    .line 1041758
    iput-boolean p2, p0, LX/62Y;->b:Z

    .line 1041759
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 5

    .prologue
    .line 1041737
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    .line 1041738
    instance-of v1, v0, LX/3wu;

    if-eqz v1, :cond_2

    .line 1041739
    check-cast v0, LX/3wu;

    .line 1041740
    iget v1, v0, LX/3wu;->c:I

    move v0, v1

    .line 1041741
    :goto_0
    move v0, v0

    .line 1041742
    if-gtz v0, :cond_0

    .line 1041743
    :goto_1
    return-void

    .line 1041744
    :cond_0
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v1

    .line 1041745
    rem-int v2, v1, v0

    .line 1041746
    iget v3, p0, LX/62Y;->a:I

    iget v4, p0, LX/62Y;->a:I

    mul-int/2addr v4, v2

    div-int/2addr v4, v0

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->left:I

    .line 1041747
    add-int/lit8 v2, v2, 0x1

    iget v3, p0, LX/62Y;->a:I

    mul-int/2addr v2, v3

    div-int/2addr v2, v0

    iput v2, p1, Landroid/graphics/Rect;->right:I

    .line 1041748
    iget-boolean v2, p0, LX/62Y;->b:Z

    if-eqz v2, :cond_1

    if-ge v1, v0, :cond_1

    .line 1041749
    iget v0, p0, LX/62Y;->a:I

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 1041750
    :cond_1
    iget v0, p0, LX/62Y;->a:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 1041751
    :cond_2
    instance-of v1, v0, LX/3xT;

    if-eqz v1, :cond_3

    .line 1041752
    check-cast v0, LX/3xT;

    .line 1041753
    iget v1, v0, LX/3xT;->g:I

    move v0, v1

    .line 1041754
    goto :goto_0

    .line 1041755
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method
