.class public final LX/5Z4;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 943999
    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;

    const v0, -0x6300304d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "MoreMessagesQuery"

    const-string v6, "8ec85214ea79f69d130439337e833c08"

    const-string v7, "message_thread"

    const-string v8, "10155265581931729"

    const-string v9, "10155259696856729"

    .line 944000
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 944001
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 944002
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 944003
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 944004
    sparse-switch v0, :sswitch_data_0

    .line 944005
    :goto_0
    return-object p1

    .line 944006
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 944007
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 944008
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 944009
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 944010
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 944011
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 944012
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 944013
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 944014
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 944015
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 944016
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 944017
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 944018
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 944019
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 944020
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 944021
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 944022
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x753cab1d -> :sswitch_5
        -0x5f54881d -> :sswitch_3
        -0x5d1dd090 -> :sswitch_e
        -0x5bcbf522 -> :sswitch_f
        -0x56855c4a -> :sswitch_c
        -0x4c47f2a9 -> :sswitch_b
        -0x4982f868 -> :sswitch_0
        -0x4450092f -> :sswitch_a
        -0x39e54905 -> :sswitch_10
        -0x786d0bb -> :sswitch_8
        -0x3224078 -> :sswitch_9
        -0x8d30fe -> :sswitch_7
        0x8da57ae -> :sswitch_4
        0x19ec4b2a -> :sswitch_1
        0x3349e8c0 -> :sswitch_d
        0x5af48aaa -> :sswitch_2
        0x5ba7488b -> :sswitch_6
    .end sparse-switch
.end method
