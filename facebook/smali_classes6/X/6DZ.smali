.class public final LX/6DZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/browserextensions/common/autofill/SaveAutofillDataDialogFragment;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1065283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065284
    const-string v0, "app_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6DZ;->a:Ljava/lang/String;

    .line 1065285
    const-string v0, "autofill_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/6DZ;->b:Ljava/util/ArrayList;

    .line 1065286
    const-string v0, "js_bridge_call"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    iput-object v0, p0, LX/6DZ;->c:Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    .line 1065287
    return-void
.end method
