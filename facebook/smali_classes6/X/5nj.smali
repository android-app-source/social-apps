.class public final enum LX/5nj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5nj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5nj;

.field public static final enum APP_STEP_EXPOSED:LX/5nj;

.field public static final enum COMPOSER_STEP_EXPOSED:LX/5nj;

.field public static final enum INTRO_STEP_CLOSED:LX/5nj;

.field public static final enum INTRO_STEP_EXPOSED:LX/5nj;

.field public static final enum MUTATION:LX/5nj;

.field public static final enum PROFILE_STEP_EXPOSED:LX/5nj;

.field public static final enum REVIEW_STEP_CLOSED:LX/5nj;

.field public static final enum REVIEW_STEP_EXPOSED:LX/5nj;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1004568
    new-instance v0, LX/5nj;

    const-string v1, "MUTATION"

    const-string v2, "mutation"

    invoke-direct {v0, v1, v4, v2}, LX/5nj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nj;->MUTATION:LX/5nj;

    .line 1004569
    new-instance v0, LX/5nj;

    const-string v1, "INTRO_STEP_EXPOSED"

    const-string v2, "intro_step_exposed"

    invoke-direct {v0, v1, v5, v2}, LX/5nj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nj;->INTRO_STEP_EXPOSED:LX/5nj;

    .line 1004570
    new-instance v0, LX/5nj;

    const-string v1, "COMPOSER_STEP_EXPOSED"

    const-string v2, "composer_step_exposed"

    invoke-direct {v0, v1, v6, v2}, LX/5nj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nj;->COMPOSER_STEP_EXPOSED:LX/5nj;

    .line 1004571
    new-instance v0, LX/5nj;

    const-string v1, "PROFILE_STEP_EXPOSED"

    const-string v2, "profile_step_exposed"

    invoke-direct {v0, v1, v7, v2}, LX/5nj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nj;->PROFILE_STEP_EXPOSED:LX/5nj;

    .line 1004572
    new-instance v0, LX/5nj;

    const-string v1, "APP_STEP_EXPOSED"

    const-string v2, "app_step_exposed"

    invoke-direct {v0, v1, v8, v2}, LX/5nj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nj;->APP_STEP_EXPOSED:LX/5nj;

    .line 1004573
    new-instance v0, LX/5nj;

    const-string v1, "REVIEW_STEP_EXPOSED"

    const/4 v2, 0x5

    const-string v3, "review_step_exposed"

    invoke-direct {v0, v1, v2, v3}, LX/5nj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nj;->REVIEW_STEP_EXPOSED:LX/5nj;

    .line 1004574
    new-instance v0, LX/5nj;

    const-string v1, "INTRO_STEP_CLOSED"

    const/4 v2, 0x6

    const-string v3, "intro_step_closed"

    invoke-direct {v0, v1, v2, v3}, LX/5nj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nj;->INTRO_STEP_CLOSED:LX/5nj;

    .line 1004575
    new-instance v0, LX/5nj;

    const-string v1, "REVIEW_STEP_CLOSED"

    const/4 v2, 0x7

    const-string v3, "review_step_closed"

    invoke-direct {v0, v1, v2, v3}, LX/5nj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nj;->REVIEW_STEP_CLOSED:LX/5nj;

    .line 1004576
    const/16 v0, 0x8

    new-array v0, v0, [LX/5nj;

    sget-object v1, LX/5nj;->MUTATION:LX/5nj;

    aput-object v1, v0, v4

    sget-object v1, LX/5nj;->INTRO_STEP_EXPOSED:LX/5nj;

    aput-object v1, v0, v5

    sget-object v1, LX/5nj;->COMPOSER_STEP_EXPOSED:LX/5nj;

    aput-object v1, v0, v6

    sget-object v1, LX/5nj;->PROFILE_STEP_EXPOSED:LX/5nj;

    aput-object v1, v0, v7

    sget-object v1, LX/5nj;->APP_STEP_EXPOSED:LX/5nj;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5nj;->REVIEW_STEP_EXPOSED:LX/5nj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5nj;->INTRO_STEP_CLOSED:LX/5nj;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5nj;->REVIEW_STEP_CLOSED:LX/5nj;

    aput-object v2, v0, v1

    sput-object v0, LX/5nj;->$VALUES:[LX/5nj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004577
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004578
    iput-object p3, p0, LX/5nj;->eventName:Ljava/lang/String;

    .line 1004579
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5nj;
    .locals 1

    .prologue
    .line 1004580
    const-class v0, LX/5nj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5nj;

    return-object v0
.end method

.method public static values()[LX/5nj;
    .locals 1

    .prologue
    .line 1004581
    sget-object v0, LX/5nj;->$VALUES:[LX/5nj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5nj;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004582
    iget-object v0, p0, LX/5nj;->eventName:Ljava/lang/String;

    return-object v0
.end method
