.class public LX/6Kt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Kh;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:LX/5Pf;

.field private c:Landroid/graphics/SurfaceTexture;

.field private d:Landroid/view/Surface;

.field public e:Ljava/util/concurrent/ExecutorService;

.field public final f:[F

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:Z

.field public l:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/nio/Buffer;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/io/File;

.field public n:LX/6Jm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1077668
    const-class v0, LX/6Kt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Kt;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1077595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077596
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/6Kt;->f:[F

    .line 1077597
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Kt;->k:Z

    .line 1077598
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/6Kt;->l:Ljava/lang/ref/WeakReference;

    .line 1077599
    return-void
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 1077660
    iget v0, p0, LX/6Kt;->i:I

    if-ne v0, p1, :cond_0

    iget v0, p0, LX/6Kt;->j:I

    if-eq v0, p2, :cond_1

    .line 1077661
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/6Kt;->l:Ljava/lang/ref/WeakReference;

    .line 1077662
    :cond_1
    iput p1, p0, LX/6Kt;->i:I

    .line 1077663
    iput p2, p0, LX/6Kt;->j:I

    .line 1077664
    iget-object v0, p0, LX/6Kt;->c:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_2

    .line 1077665
    iget-object v0, p0, LX/6Kt;->c:Landroid/graphics/SurfaceTexture;

    iget v1, p0, LX/6Kt;->i:I

    iget v2, p0, LX/6Kt;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 1077666
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6Kt;->k:Z

    .line 1077667
    return-void
.end method


# virtual methods
.method public final a(LX/6Kl;)V
    .locals 4

    .prologue
    .line 1077647
    const v3, 0x812f

    const/16 v2, 0x2601

    .line 1077648
    new-instance v0, LX/5Pe;

    invoke-direct {v0}, LX/5Pe;-><init>()V

    const/16 v1, 0xde1

    .line 1077649
    iput v1, v0, LX/5Pe;->a:I

    .line 1077650
    move-object v0, v0

    .line 1077651
    const/16 v1, 0x2801

    invoke-virtual {v0, v1, v2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2800

    invoke-virtual {v0, v1, v2}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2802

    invoke-virtual {v0, v1, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    const/16 v1, 0x2803

    invoke-virtual {v0, v1, v3}, LX/5Pe;->a(II)LX/5Pe;

    move-result-object v0

    invoke-virtual {v0}, LX/5Pe;->a()LX/5Pf;

    move-result-object v0

    move-object v0, v0

    .line 1077652
    iput-object v0, p0, LX/6Kt;->b:LX/5Pf;

    .line 1077653
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, LX/6Kt;->b:LX/5Pf;

    iget v1, v1, LX/5Pf;->b:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, LX/6Kt;->c:Landroid/graphics/SurfaceTexture;

    .line 1077654
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, LX/6Kt;->c:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/6Kt;->d:Landroid/view/Surface;

    .line 1077655
    iget-object v0, p0, LX/6Kt;->d:Landroid/view/Surface;

    invoke-virtual {p1, p0, v0}, LX/6Kl;->a(LX/6Kd;Landroid/view/Surface;)V

    .line 1077656
    iget v0, p0, LX/6Kt;->g:I

    if-lez v0, :cond_0

    iget v0, p0, LX/6Kt;->h:I

    if-lez v0, :cond_0

    .line 1077657
    iget v0, p0, LX/6Kt;->g:I

    iget v1, p0, LX/6Kt;->h:I

    invoke-direct {p0, v0, v1}, LX/6Kt;->a(II)V

    .line 1077658
    :cond_0
    iget-object v0, p0, LX/6Kt;->c:Landroid/graphics/SurfaceTexture;

    iget v1, p0, LX/6Kt;->i:I

    iget v2, p0, LX/6Kt;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 1077659
    return-void
.end method

.method public final a()[F
    .locals 1

    .prologue
    .line 1077646
    iget-object v0, p0, LX/6Kt;->f:[F

    return-object v0
.end method

.method public final b()LX/61D;
    .locals 1

    .prologue
    .line 1077645
    sget-object v0, LX/61D;->DEFAULT:LX/61D;

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1077643
    invoke-virtual {p0}, LX/6Kt;->dE_()V

    .line 1077644
    return-void
.end method

.method public final dE_()V
    .locals 1

    .prologue
    .line 1077639
    iget-object v0, p0, LX/6Kt;->d:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1077640
    iget-object v0, p0, LX/6Kt;->c:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 1077641
    iget-object v0, p0, LX/6Kt;->b:LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    .line 1077642
    return-void
.end method

.method public final f()V
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 1077603
    iget-boolean v0, p0, LX/6Kt;->k:Z

    if-nez v0, :cond_1

    iget v0, p0, LX/6Kt;->g:I

    if-lez v0, :cond_1

    iget v0, p0, LX/6Kt;->h:I

    if-lez v0, :cond_1

    .line 1077604
    iget v0, p0, LX/6Kt;->g:I

    iget v1, p0, LX/6Kt;->h:I

    invoke-direct {p0, v0, v1}, LX/6Kt;->a(II)V

    .line 1077605
    :cond_0
    :goto_0
    return-void

    .line 1077606
    :cond_1
    iget-object v0, p0, LX/6Kt;->m:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 1077607
    const/4 v6, 0x0

    .line 1077608
    iget-object v7, p0, LX/6Kt;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/nio/Buffer;

    .line 1077609
    if-nez v7, :cond_4

    .line 1077610
    iget v7, p0, LX/6Kt;->i:I

    iget v8, p0, LX/6Kt;->j:I

    mul-int/2addr v7, v8

    mul-int/lit8 v7, v7, 0x4

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 1077611
    new-instance v7, Ljava/lang/ref/WeakReference;

    invoke-direct {v7, v12}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v7, p0, LX/6Kt;->l:Ljava/lang/ref/WeakReference;

    .line 1077612
    :goto_1
    invoke-virtual {v12}, Ljava/nio/Buffer;->rewind()Ljava/nio/Buffer;

    .line 1077613
    iget v8, p0, LX/6Kt;->i:I

    iget v9, p0, LX/6Kt;->j:I

    const/16 v10, 0x1908

    const/16 v11, 0x1401

    move v7, v6

    invoke-static/range {v6 .. v12}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 1077614
    const-string v6, "glReadPixels"

    invoke-static {v6}, LX/5PP;->a(Ljava/lang/String;)V

    .line 1077615
    move-object v0, v12

    .line 1077616
    iget-object v1, p0, LX/6Kt;->m:Ljava/io/File;

    .line 1077617
    iget-object v2, p0, LX/6Kt;->n:LX/6Jm;

    .line 1077618
    :try_start_0
    iget v3, p0, LX/6Kt;->i:I

    iget v4, p0, LX/6Kt;->j:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1077619
    if-nez v3, :cond_2

    .line 1077620
    sget-object v4, LX/6Kt;->a:Ljava/lang/String;

    const-string v6, "Unable to create bitmap of given size %d x %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, p0, LX/6Kt;->i:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget v9, p0, LX/6Kt;->j:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v4, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1077621
    iget-object v4, p0, LX/6Kt;->n:LX/6Jm;

    if-eqz v4, :cond_2

    .line 1077622
    iget-object v4, p0, LX/6Kt;->n:LX/6Jm;

    new-instance v6, Ljava/lang/NullPointerException;

    const-string v7, "Bitmap null"

    invoke-direct {v6, v7}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, LX/6Jm;->a(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1077623
    :cond_2
    :goto_2
    move-object v3, v3

    .line 1077624
    if-eqz v3, :cond_0

    .line 1077625
    invoke-virtual {v3, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 1077626
    if-eqz v2, :cond_3

    .line 1077627
    invoke-static {v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1077628
    iget-object v4, v2, LX/6Jm;->c:LX/6Jt;

    const/16 v6, 0xa

    invoke-virtual {v4, v6}, LX/6Jt;->c(I)V

    .line 1077629
    iget-object v4, v2, LX/6Jm;->c:LX/6Jt;

    const/16 v6, 0x10

    invoke-virtual {v4, v6}, LX/6Jt;->b(I)V

    .line 1077630
    iget-object v4, v2, LX/6Jm;->c:LX/6Jt;

    iget-object v4, v4, LX/6Jt;->b:Landroid/os/Handler;

    new-instance v6, Lcom/facebook/cameracore/capturecoordinator/CaptureCoordinator$2$2;

    invoke-direct {v6, v2, v0}, Lcom/facebook/cameracore/capturecoordinator/CaptureCoordinator$2$2;-><init>(LX/6Jm;Landroid/graphics/Bitmap;)V

    const v7, -0x6cc95410

    invoke-static {v4, v6, v7}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1077631
    :cond_3
    iget-object v0, p0, LX/6Kt;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;

    invoke-direct {v4, p0, v1, v3, v2}, Lcom/facebook/cameracore/mediapipeline/outputs/PhotoOutput$1;-><init>(LX/6Kt;Ljava/io/File;Landroid/graphics/Bitmap;LX/6Jm;)V

    const v1, -0x7e92e87b

    invoke-static {v0, v4, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1077632
    iput-object v5, p0, LX/6Kt;->n:LX/6Jm;

    .line 1077633
    iput-object v5, p0, LX/6Kt;->m:Ljava/io/File;

    goto/16 :goto_0

    :cond_4
    move-object v12, v7

    goto/16 :goto_1

    .line 1077634
    :catch_0
    move-exception v3

    .line 1077635
    sget-object v4, LX/6Kt;->a:Ljava/lang/String;

    const-string v6, "Unable to create Bitmap due to OOM"

    invoke-static {v4, v6, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1077636
    iget-object v4, p0, LX/6Kt;->n:LX/6Jm;

    if-eqz v4, :cond_5

    .line 1077637
    iget-object v4, p0, LX/6Kt;->n:LX/6Jm;

    invoke-virtual {v4, v3}, LX/6Jm;->a(Ljava/lang/Throwable;)V

    .line 1077638
    :cond_5
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final getHeight()I
    .locals 1

    .prologue
    .line 1077602
    iget v0, p0, LX/6Kt;->j:I

    return v0
.end method

.method public final getWidth()I
    .locals 1

    .prologue
    .line 1077601
    iget v0, p0, LX/6Kt;->i:I

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 1077600
    const/4 v0, 0x1

    return v0
.end method
