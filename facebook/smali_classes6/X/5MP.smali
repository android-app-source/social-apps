.class public LX/5MP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5MG;


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 905479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905480
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 905481
    new-instance v0, LX/5MH;

    const-string v1, "Mismatching number of values"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905482
    :cond_0
    iput-object p1, p0, LX/5MP;->a:Ljava/lang/String;

    .line 905483
    return-void
.end method

.method private a(D)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 905484
    :try_start_0
    iget-object v1, p0, LX/5MP;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 905485
    cmpl-double v1, v2, p1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 905486
    :cond_0
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 905487
    iget-object v0, p0, LX/5MP;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0uE;)Z
    .locals 2

    .prologue
    .line 905488
    sget-object v0, LX/5MO;->a:[I

    .line 905489
    iget-object v1, p1, LX/0uE;->b:LX/0uN;

    move-object v1, v1

    .line 905490
    invoke-virtual {v1}, LX/0uN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 905491
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 905492
    :pswitch_0
    invoke-virtual {p1}, LX/0uE;->c()J

    move-result-wide v0

    long-to-double v0, v0

    invoke-direct {p0, v0, v1}, LX/5MP;->a(D)Z

    move-result v0

    goto :goto_0

    .line 905493
    :pswitch_1
    invoke-virtual {p1}, LX/0uE;->d()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/5MP;->a(D)Z

    move-result v0

    goto :goto_0

    .line 905494
    :pswitch_2
    invoke-virtual {p1}, LX/0uE;->b()Z

    move-result v0

    .line 905495
    iget-object v1, p0, LX/5MP;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 905496
    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 905497
    goto :goto_0

    .line 905498
    :pswitch_3
    invoke-virtual {p1}, LX/0uE;->toString()Ljava/lang/String;

    move-result-object v0

    .line 905499
    iget-object v1, p0, LX/5MP;->a:Ljava/lang/String;

    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 905500
    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
