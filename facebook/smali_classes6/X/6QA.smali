.class public LX/6QA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Q9;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1087046
    const-class v0, LX/6QA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6QA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1087043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1087044
    iput-object p1, p0, LX/6QA;->b:LX/03V;

    .line 1087045
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/directinstall/intent/DirectInstallAppData;Ljava/util/Map;Z)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/directinstall/intent/DirectInstallAppData;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 1087041
    iget-object v0, p0, LX/6QA;->b:LX/03V;

    sget-object v1, LX/6QA;->a:Ljava/lang/String;

    const-string v2, "DefaultDirectInstaller should not handle installs. Modules should provide their own implementations of DirectInstaller"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087042
    const/4 v0, 0x0

    return v0
.end method
