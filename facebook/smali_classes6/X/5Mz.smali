.class public final LX/5Mz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/39A;


# instance fields
.field public final synthetic a:LX/0yb;


# direct methods
.method public constructor <init>(LX/0yb;)V
    .locals 0

    .prologue
    .line 905819
    iput-object p1, p0, LX/5Mz;->a:LX/0yb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 905809
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 905810
    iget-object v0, p0, LX/5Mz;->a:LX/0yb;

    iget-object v0, v0, LX/0yb;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/5Mz;->a:LX/0yb;

    iget-object v0, v0, LX/0yb;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/121;

    sget-object v1, LX/0yY;->ZERO_BALANCE_WEBVIEW:LX/0yY;

    invoke-virtual {v0, v1}, LX/121;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 905811
    :cond_0
    iget-object v0, p0, LX/5Mz;->a:LX/0yb;

    const-string v1, "dialtone_upgrade_dialog"

    invoke-virtual {v0, v1}, LX/0yc;->b(Ljava/lang/String;)Z

    .line 905812
    :goto_0
    return-void

    .line 905813
    :cond_1
    iget-object v0, p0, LX/5Mz;->a:LX/0yb;

    invoke-virtual {v0}, LX/0yc;->e()Landroid/app/Activity;

    move-result-object v1

    .line 905814
    if-nez v1, :cond_2

    .line 905815
    iget-object v0, p0, LX/5Mz;->a:LX/0yb;

    invoke-static {v0}, LX/0yb;->D(LX/0yb;)V

    goto :goto_0

    .line 905816
    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/facebook/dialtone/activity/DialtoneModeTransitionInterstitialActivity;

    invoke-direct {v2, v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 905817
    const/high16 v0, 0x10000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 905818
    iget-object v0, p0, LX/5Mz;->a:LX/0yb;

    iget-object v0, v0, LX/0yb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
