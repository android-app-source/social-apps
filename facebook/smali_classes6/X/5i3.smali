.class public final LX/5i3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 981479
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_c

    .line 981480
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 981481
    :goto_0
    return v1

    .line 981482
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 981483
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 981484
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 981485
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 981486
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 981487
    const-string v10, "actors"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 981488
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 981489
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_2

    .line 981490
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_2

    .line 981491
    invoke-static {p0, p1}, LX/5hs;->b(LX/15w;LX/186;)I

    move-result v9

    .line 981492
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 981493
    :cond_2
    invoke-static {v8, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 981494
    goto :goto_1

    .line 981495
    :cond_3
    const-string v10, "attachments"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 981496
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 981497
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_4

    .line 981498
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_4

    .line 981499
    invoke-static {p0, p1}, LX/5hy;->b(LX/15w;LX/186;)I

    move-result v9

    .line 981500
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 981501
    :cond_4
    invoke-static {v7, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 981502
    goto :goto_1

    .line 981503
    :cond_5
    const-string v10, "feedback"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 981504
    invoke-static {p0, p1}, LX/5i1;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 981505
    :cond_6
    const-string v10, "id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 981506
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 981507
    :cond_7
    const-string v10, "message"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 981508
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 981509
    :cond_8
    const-string v10, "shareable"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 981510
    invoke-static {p0, p1}, LX/5i2;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 981511
    :cond_9
    const-string v10, "summary"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 981512
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 981513
    :cond_a
    const-string v10, "title"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 981514
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 981515
    :cond_b
    const/16 v9, 0x8

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 981516
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 981517
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 981518
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 981519
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 981520
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 981521
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 981522
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 981523
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 981524
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 981525
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 981526
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981527
    if-eqz v0, :cond_1

    .line 981528
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981529
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 981530
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 981531
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/5hs;->a(LX/15i;ILX/0nX;)V

    .line 981532
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 981533
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 981534
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981535
    if-eqz v0, :cond_3

    .line 981536
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981537
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 981538
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 981539
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5hy;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 981540
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 981541
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 981542
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981543
    if-eqz v0, :cond_4

    .line 981544
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981545
    invoke-static {p0, v0, p2, p3}, LX/5i1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 981546
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 981547
    if-eqz v0, :cond_5

    .line 981548
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981549
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 981550
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981551
    if-eqz v0, :cond_6

    .line 981552
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981553
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 981554
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981555
    if-eqz v0, :cond_7

    .line 981556
    const-string v1, "shareable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981557
    invoke-static {p0, v0, p2}, LX/5i2;->a(LX/15i;ILX/0nX;)V

    .line 981558
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981559
    if-eqz v0, :cond_8

    .line 981560
    const-string v1, "summary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981561
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 981562
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 981563
    if-eqz v0, :cond_9

    .line 981564
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 981565
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 981566
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 981567
    return-void
.end method
