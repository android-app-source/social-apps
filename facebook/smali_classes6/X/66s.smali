.class public final LX/66s;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:Z


# instance fields
.field private final b:Z

.field private final c:Ljava/util/Random;

.field public final d:LX/670;

.field private e:Z

.field public final f:LX/672;

.field private final g:LX/66r;

.field public h:Z

.field private final i:[B

.field private final j:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1051163
    const-class v0, LX/66s;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/66s;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(ZLX/670;Ljava/util/Random;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1051164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1051165
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    iput-object v0, p0, LX/66s;->f:LX/672;

    .line 1051166
    new-instance v0, LX/66r;

    invoke-direct {v0, p0}, LX/66r;-><init>(LX/66s;)V

    iput-object v0, p0, LX/66s;->g:LX/66r;

    .line 1051167
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051168
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "random == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051169
    :cond_1
    iput-boolean p1, p0, LX/66s;->b:Z

    .line 1051170
    iput-object p2, p0, LX/66s;->d:LX/670;

    .line 1051171
    iput-object p3, p0, LX/66s;->c:Ljava/util/Random;

    .line 1051172
    if-eqz p1, :cond_3

    const/4 v0, 0x4

    new-array v0, v0, [B

    :goto_0
    iput-object v0, p0, LX/66s;->i:[B

    .line 1051173
    if-eqz p1, :cond_2

    const/16 v0, 0x2000

    new-array v1, v0, [B

    :cond_2
    iput-object v1, p0, LX/66s;->j:[B

    .line 1051174
    return-void

    :cond_3
    move-object v0, v1

    .line 1051175
    goto :goto_0
.end method

.method private static a(LX/66s;ILX/672;)V
    .locals 8

    .prologue
    .line 1051176
    sget-boolean v0, LX/66s;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1051177
    :cond_0
    iget-boolean v0, p0, LX/66s;->e:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051178
    :cond_1
    const/4 v0, 0x0

    .line 1051179
    if-eqz p2, :cond_2

    .line 1051180
    iget-wide v6, p2, LX/672;->b:J

    move-wide v0, v6

    .line 1051181
    long-to-int v0, v0

    .line 1051182
    int-to-long v2, v0

    const-wide/16 v4, 0x7d

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 1051183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Payload size must be less than or equal to 125"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051184
    :cond_2
    or-int/lit16 v1, p1, 0x80

    .line 1051185
    iget-object v2, p0, LX/66s;->d:LX/670;

    invoke-interface {v2, v1}, LX/670;->h(I)LX/670;

    .line 1051186
    iget-boolean v1, p0, LX/66s;->b:Z

    if-eqz v1, :cond_4

    .line 1051187
    or-int/lit16 v1, v0, 0x80

    .line 1051188
    iget-object v2, p0, LX/66s;->d:LX/670;

    invoke-interface {v2, v1}, LX/670;->h(I)LX/670;

    .line 1051189
    iget-object v1, p0, LX/66s;->c:Ljava/util/Random;

    iget-object v2, p0, LX/66s;->i:[B

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextBytes([B)V

    .line 1051190
    iget-object v1, p0, LX/66s;->d:LX/670;

    iget-object v2, p0, LX/66s;->i:[B

    invoke-interface {v1, v2}, LX/670;->c([B)LX/670;

    .line 1051191
    if-eqz p2, :cond_3

    .line 1051192
    int-to-long v0, v0

    invoke-direct {p0, p2, v0, v1}, LX/66s;->a(LX/671;J)V

    .line 1051193
    :cond_3
    :goto_0
    iget-object v0, p0, LX/66s;->d:LX/670;

    invoke-interface {v0}, LX/670;->d()LX/670;

    .line 1051194
    return-void

    .line 1051195
    :cond_4
    iget-object v1, p0, LX/66s;->d:LX/670;

    invoke-interface {v1, v0}, LX/670;->h(I)LX/670;

    .line 1051196
    if-eqz p2, :cond_3

    .line 1051197
    iget-object v0, p0, LX/66s;->d:LX/670;

    invoke-interface {v0, p2}, LX/670;->a(LX/65D;)J

    goto :goto_0
.end method

.method private a(LX/671;J)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1051198
    sget-boolean v0, LX/66s;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1051199
    :cond_0
    const-wide/16 v4, 0x0

    .line 1051200
    :goto_0
    cmp-long v0, v4, p2

    if-gez v0, :cond_2

    .line 1051201
    iget-object v0, p0, LX/66s;->j:[B

    array-length v0, v0

    int-to-long v0, v0

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 1051202
    iget-object v1, p0, LX/66s;->j:[B

    invoke-interface {p1, v1, v7, v0}, LX/671;->a([BII)I

    move-result v6

    .line 1051203
    const/4 v0, -0x1

    if-ne v6, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1051204
    :cond_1
    iget-object v0, p0, LX/66s;->j:[B

    int-to-long v1, v6

    iget-object v3, p0, LX/66s;->i:[B

    invoke-static/range {v0 .. v5}, LX/66n;->a([BJ[BJ)V

    .line 1051205
    iget-object v0, p0, LX/66s;->d:LX/670;

    iget-object v1, p0, LX/66s;->j:[B

    invoke-interface {v0, v1, v7, v6}, LX/670;->c([BII)LX/670;

    .line 1051206
    int-to-long v0, v6

    add-long/2addr v4, v0

    .line 1051207
    goto :goto_0

    .line 1051208
    :cond_2
    return-void
.end method

.method public static a$redex0(LX/66s;IJZZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1051209
    sget-boolean v0, LX/66s;->a:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1051210
    :cond_0
    iget-boolean v0, p0, LX/66s;->e:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051211
    :cond_1
    if-eqz p4, :cond_3

    move v0, p1

    .line 1051212
    :goto_0
    if-eqz p5, :cond_2

    .line 1051213
    or-int/lit16 v0, v0, 0x80

    .line 1051214
    :cond_2
    iget-object v2, p0, LX/66s;->d:LX/670;

    invoke-interface {v2, v0}, LX/670;->h(I)LX/670;

    .line 1051215
    iget-boolean v0, p0, LX/66s;->b:Z

    if-eqz v0, :cond_7

    .line 1051216
    const/16 v0, 0x80

    .line 1051217
    iget-object v1, p0, LX/66s;->c:Ljava/util/Random;

    iget-object v2, p0, LX/66s;->i:[B

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextBytes([B)V

    .line 1051218
    :goto_1
    const-wide/16 v2, 0x7d

    cmp-long v1, p2, v2

    if-gtz v1, :cond_4

    .line 1051219
    long-to-int v1, p2

    or-int/2addr v0, v1

    .line 1051220
    iget-object v1, p0, LX/66s;->d:LX/670;

    invoke-interface {v1, v0}, LX/670;->h(I)LX/670;

    .line 1051221
    :goto_2
    iget-boolean v0, p0, LX/66s;->b:Z

    if-eqz v0, :cond_6

    .line 1051222
    iget-object v0, p0, LX/66s;->d:LX/670;

    iget-object v1, p0, LX/66s;->i:[B

    invoke-interface {v0, v1}, LX/670;->c([B)LX/670;

    .line 1051223
    iget-object v0, p0, LX/66s;->f:LX/672;

    invoke-direct {p0, v0, p2, p3}, LX/66s;->a(LX/671;J)V

    .line 1051224
    :goto_3
    iget-object v0, p0, LX/66s;->d:LX/670;

    invoke-interface {v0}, LX/670;->d()LX/670;

    .line 1051225
    return-void

    :cond_3
    move v0, v1

    .line 1051226
    goto :goto_0

    .line 1051227
    :cond_4
    const-wide/32 v2, 0xffff

    cmp-long v1, p2, v2

    if-gtz v1, :cond_5

    .line 1051228
    or-int/lit8 v0, v0, 0x7e

    .line 1051229
    iget-object v1, p0, LX/66s;->d:LX/670;

    invoke-interface {v1, v0}, LX/670;->h(I)LX/670;

    .line 1051230
    iget-object v0, p0, LX/66s;->d:LX/670;

    long-to-int v1, p2

    invoke-interface {v0, v1}, LX/670;->g(I)LX/670;

    goto :goto_2

    .line 1051231
    :cond_5
    or-int/lit8 v0, v0, 0x7f

    .line 1051232
    iget-object v1, p0, LX/66s;->d:LX/670;

    invoke-interface {v1, v0}, LX/670;->h(I)LX/670;

    .line 1051233
    iget-object v0, p0, LX/66s;->d:LX/670;

    invoke-interface {v0, p2, p3}, LX/670;->l(J)LX/670;

    goto :goto_2

    .line 1051234
    :cond_6
    iget-object v0, p0, LX/66s;->d:LX/670;

    iget-object v1, p0, LX/66s;->f:LX/672;

    invoke-interface {v0, v1, p2, p3}, LX/65J;->a_(LX/672;J)V

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(IJ)LX/65J;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1051235
    iget-boolean v0, p0, LX/66s;->h:Z

    if-eqz v0, :cond_0

    .line 1051236
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Another message writer is active. Did you call close()?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051237
    :cond_0
    iput-boolean v1, p0, LX/66s;->h:Z

    .line 1051238
    iget-object v0, p0, LX/66s;->g:LX/66r;

    .line 1051239
    iput p1, v0, LX/66r;->b:I

    .line 1051240
    iget-object v0, p0, LX/66s;->g:LX/66r;

    .line 1051241
    iput-wide p2, v0, LX/66r;->c:J

    .line 1051242
    iget-object v0, p0, LX/66s;->g:LX/66r;

    .line 1051243
    iput-boolean v1, v0, LX/66r;->d:Z

    .line 1051244
    iget-object v0, p0, LX/66s;->g:LX/66r;

    const/4 v1, 0x0

    .line 1051245
    iput-boolean v1, v0, LX/66r;->e:Z

    .line 1051246
    iget-object v0, p0, LX/66s;->g:LX/66r;

    return-object v0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1051247
    const/4 v0, 0x0

    .line 1051248
    if-nez p1, :cond_0

    if-eqz p2, :cond_2

    .line 1051249
    :cond_0
    if-eqz p1, :cond_1

    .line 1051250
    invoke-static {p1, v1}, LX/66n;->a(IZ)V

    .line 1051251
    :cond_1
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    .line 1051252
    invoke-virtual {v0, p1}, LX/672;->c(I)LX/672;

    .line 1051253
    if-eqz p2, :cond_2

    .line 1051254
    invoke-virtual {v0, p2}, LX/672;->a(Ljava/lang/String;)LX/672;

    .line 1051255
    :cond_2
    monitor-enter p0

    .line 1051256
    const/16 v1, 0x8

    :try_start_0
    invoke-static {p0, v1, v0}, LX/66s;->a(LX/66s;ILX/672;)V

    .line 1051257
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/66s;->e:Z

    .line 1051258
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/672;)V
    .locals 1

    .prologue
    .line 1051259
    monitor-enter p0

    .line 1051260
    const/16 v0, 0x9

    :try_start_0
    invoke-static {p0, v0, p1}, LX/66s;->a(LX/66s;ILX/672;)V

    .line 1051261
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(LX/672;)V
    .locals 1

    .prologue
    .line 1051262
    monitor-enter p0

    .line 1051263
    const/16 v0, 0xa

    :try_start_0
    invoke-static {p0, v0, p1}, LX/66s;->a(LX/66s;ILX/672;)V

    .line 1051264
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
