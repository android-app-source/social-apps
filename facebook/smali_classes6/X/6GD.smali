.class public final LX/6GD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:Lcom/facebook/bugreporter/RageShakeDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/RageShakeDialogFragment;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 1069975
    iput-object p1, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    iput p2, p0, LX/6GD;->a:I

    iput-object p3, p0, LX/6GD;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9

    .prologue
    .line 1069976
    iget-object v0, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1069977
    iget-object v0, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    .line 1069978
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v2

    .line 1069979
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    .line 1069980
    iget-object v3, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    invoke-virtual {v2, v3}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 1069981
    invoke-virtual {v2}, LX/0hH;->c()I

    .line 1069982
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1069983
    packed-switch p2, :pswitch_data_0

    .line 1069984
    iget v0, p0, LX/6GD;->a:I

    sub-int v0, p2, v0

    .line 1069985
    if-ltz v0, :cond_0

    iget-object v2, p0, LX/6GD;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1069986
    iget-object v2, p0, LX/6GD;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E1Z;

    .line 1069987
    iget-object v2, v0, LX/E1Z;->d:LX/17Y;

    sget-object v3, LX/0ax;->eh:Ljava/lang/String;

    invoke-interface {v2, v1, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1069988
    iget-object v3, v0, LX/E1Z;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1069989
    :cond_0
    :goto_0
    return-void

    .line 1069990
    :pswitch_0
    iget-object v0, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/RageShakeDialogFragment;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6G2;

    invoke-static {}, LX/6FY;->newBuilder()LX/6FX;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/6FX;->a(Landroid/content/Context;)LX/6FX;

    move-result-object v1

    sget-object v2, LX/6Fb;->RAGE_SHAKE:LX/6Fb;

    invoke-virtual {v1, v2}, LX/6FX;->a(LX/6Fb;)LX/6FX;

    move-result-object v1

    invoke-virtual {v1}, LX/6FX;->a()LX/6FY;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6G2;->a(LX/6FY;)V

    goto :goto_0

    .line 1069991
    :pswitch_1
    iget-object v0, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/RageShakeDialogFragment;->r:LX/6Uq;

    const/16 v1, 0x1f4

    .line 1069992
    iget-object v4, v0, LX/6Uq;->f:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03R;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/03R;->asBoolean(Z)Z

    move-result v4

    move v4, v4

    .line 1069993
    if-eqz v4, :cond_1

    iget-object v4, v0, LX/6Uq;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    .line 1069994
    :cond_1
    :goto_1
    goto :goto_0

    .line 1069995
    :pswitch_2
    iget-object v0, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/RageShakeDialogFragment;->s:LX/6V3;

    .line 1069996
    invoke-static {v1}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    .line 1069997
    invoke-static {v0, v2}, LX/6V3;->b(LX/6V3;Landroid/view/View;)Ljava/io/File;

    move-result-object v3

    .line 1069998
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.SEND"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1069999
    const-string v5, "android.intent.extra.SUBJECT"

    const-string v1, "View Hierarchy Debug Info"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1070000
    const-string v5, "android.intent.extra.TEXT"

    const-string v1, "View Hierarchy json attached."

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1070001
    const-string v5, "android.intent.extra.STREAM"

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1070002
    const-string v3, "text/plain"

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1070003
    const-string v3, "Share via..."

    invoke-static {v4, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    .line 1070004
    iget-object v4, v0, LX/6V3;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1070005
    goto :goto_0

    .line 1070006
    :pswitch_3
    iget-object v0, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/RageShakeDialogFragment;->o:Landroid/content/ComponentName;

    if-eqz v0, :cond_0

    .line 1070007
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    iget-object v2, v2, Lcom/facebook/bugreporter/RageShakeDialogFragment;->o:Landroid/content/ComponentName;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1070008
    iget-object v2, p0, LX/6GD;->c:Lcom/facebook/bugreporter/RageShakeDialogFragment;

    iget-object v2, v2, Lcom/facebook/bugreporter/RageShakeDialogFragment;->m:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1070009
    :cond_2
    iget-object v4, v0, LX/6Uq;->i:LX/0Sh;

    invoke-virtual {v4}, LX/0Sh;->a()V

    .line 1070010
    new-instance v4, Lcom/facebook/fbui/runtimelinter/UIRuntimeLinter$2;

    invoke-direct {v4, v0}, Lcom/facebook/fbui/runtimelinter/UIRuntimeLinter$2;-><init>(LX/6Uq;)V

    .line 1070011
    iget-object v5, v0, LX/6Uq;->c:Landroid/os/Handler;

    int-to-long v6, v1

    const v8, 0xc3304ab

    invoke-static {v5, v4, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
