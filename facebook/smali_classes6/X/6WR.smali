.class public final LX/6WR;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/6WS;

.field public b:Landroid/view/View;

.field public c:I


# direct methods
.method public constructor <init>(LX/6WS;)V
    .locals 0

    .prologue
    .line 1106017
    iput-object p1, p0, LX/6WR;->a:LX/6WS;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 13

    .prologue
    .line 1106018
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1106019
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    invoke-virtual {p1}, LX/0wd;->e()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-nez v1, :cond_0

    .line 1106020
    invoke-virtual {p1}, LX/0wd;->j()LX/0wd;

    .line 1106021
    :goto_0
    return-void

    .line 1106022
    :cond_0
    float-to-double v2, v0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 1106023
    float-to-double v0, v0

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    const-wide/16 v6, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v10, v0

    .line 1106024
    iget-object v6, p0, LX/6WR;->b:Landroid/view/View;

    float-to-double v0, v10

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {v6, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1106025
    iget-object v11, p0, LX/6WR;->b:Landroid/view/View;

    float-to-double v0, v10

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide v6, 0x3fe3333333333333L    # 0.6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {v11, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1106026
    iget-object v0, p0, LX/6WR;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    iget v0, p0, LX/6WR;->c:I

    int-to-float v12, v0

    float-to-double v0, v10

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide v6, 0x3fc999999999999aL    # 0.2

    const-wide v8, 0x3fe4cccccccccccdL    # 0.65

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v12

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v11, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1106027
    :goto_1
    iget-object v0, p0, LX/6WR;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_0

    .line 1106028
    :cond_1
    iget-object v1, p0, LX/6WR;->b:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 1106029
    iget-object v1, p0, LX/6WR;->b:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleX(F)V

    .line 1106030
    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide v6, 0x3fe4cccccccccccdL    # 0.65

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1106031
    iget-object v1, p0, LX/6WR;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, LX/6WR;->c:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1106012
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1106013
    iget-object v0, p0, LX/6WR;->a:LX/6WS;

    iget-object v0, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    iget-object v1, p0, LX/6WR;->a:LX/6WS;

    iget-object v1, v1, LX/6WS;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1106014
    :goto_0
    iget-object v0, p0, LX/6WR;->a:LX/6WS;

    iget-object v0, v0, LX/0ht;->g:LX/5OY;

    iget-object v1, p0, LX/6WR;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/5OY;->removeView(Landroid/view/View;)V

    .line 1106015
    return-void

    .line 1106016
    :cond_0
    iget-object v0, p0, LX/6WR;->a:LX/6WS;

    iget-object v0, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    iget-object v1, p0, LX/6WR;->a:LX/6WS;

    iget-object v1, v1, LX/6WS;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final c(LX/0wd;)V
    .locals 4

    .prologue
    .line 1105996
    iget-object v0, p0, LX/6WR;->a:LX/6WS;

    iget-object v0, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, LX/6WR;->c:I

    .line 1105997
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, LX/6WR;->a:LX/6WS;

    invoke-virtual {v1}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6WR;->b:Landroid/view/View;

    .line 1105998
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1105999
    iget-object v0, p0, LX/6WR;->b:Landroid/view/View;

    iget-object v1, p0, LX/6WR;->a:LX/6WS;

    iget-object v1, v1, LX/6WS;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1106000
    :goto_0
    iget-object v0, p0, LX/6WR;->b:Landroid/view/View;

    iget-object v1, p0, LX/6WR;->a:LX/6WS;

    iget-object v1, v1, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, LX/6WR;->a:LX/6WS;

    iget-object v2, v2, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, LX/6WR;->a:LX/6WS;

    iget-object v3, v3, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v3}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingRight()I

    move-result v3

    iget-object p1, p0, LX/6WR;->a:LX/6WS;

    iget-object p1, p1, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {p1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingBottom()I

    move-result p1

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 1106001
    iget-object v0, p0, LX/6WR;->b:Landroid/view/View;

    iget-object v1, p0, LX/6WR;->a:LX/6WS;

    iget-object v1, v1, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPivotX()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotX(F)V

    .line 1106002
    iget-object v0, p0, LX/6WR;->b:Landroid/view/View;

    iget-object v1, p0, LX/6WR;->a:LX/6WS;

    iget-object v1, v1, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPivotY()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotY(F)V

    .line 1106003
    iget-object v0, p0, LX/6WR;->a:LX/6WS;

    iget-object v0, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1106004
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, LX/6WR;->a:LX/6WS;

    iget-object v2, v2, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, LX/6WR;->c:I

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1106005
    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1106006
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1106007
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 1106008
    iget-object v0, p0, LX/6WR;->a:LX/6WS;

    iget-object v0, v0, LX/0ht;->g:LX/5OY;

    iget-object v2, p0, LX/6WR;->b:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, LX/5OY;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1106009
    iget-object v0, p0, LX/6WR;->a:LX/6WS;

    iget-object v0, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1106010
    return-void

    .line 1106011
    :cond_0
    iget-object v0, p0, LX/6WR;->b:Landroid/view/View;

    iget-object v1, p0, LX/6WR;->a:LX/6WS;

    iget-object v1, v1, LX/6WS;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
