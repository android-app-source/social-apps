.class public LX/5op;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1007618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007619
    return-void
.end method

.method public static a()LX/5pD;
    .locals 1

    .prologue
    .line 1007617
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    return-object v0
.end method

.method private static a(Ljava/lang/Object;)LX/5pD;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1007590
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v1

    .line 1007591
    instance-of v2, p0, [Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1007592
    check-cast p0, [Ljava/lang/String;

    check-cast p0, [Ljava/lang/String;

    array-length v2, p0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, p0, v0

    .line 1007593
    invoke-interface {v1, v3}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 1007594
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1007595
    :cond_0
    instance-of v2, p0, [Landroid/os/Bundle;

    if-eqz v2, :cond_1

    .line 1007596
    check-cast p0, [Landroid/os/Bundle;

    check-cast p0, [Landroid/os/Bundle;

    array-length v2, p0

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, p0, v0

    .line 1007597
    invoke-static {v3}, LX/5op;->a(Landroid/os/Bundle;)LX/5pH;

    move-result-object v3

    invoke-interface {v1, v3}, LX/5pD;->a(LX/5pH;)V

    .line 1007598
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1007599
    :cond_1
    instance-of v2, p0, [I

    if-eqz v2, :cond_2

    .line 1007600
    check-cast p0, [I

    check-cast p0, [I

    array-length v2, p0

    :goto_2
    if-ge v0, v2, :cond_6

    aget v3, p0, v0

    .line 1007601
    invoke-interface {v1, v3}, LX/5pD;->pushInt(I)V

    .line 1007602
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1007603
    :cond_2
    instance-of v2, p0, [F

    if-eqz v2, :cond_3

    .line 1007604
    check-cast p0, [F

    check-cast p0, [F

    array-length v2, p0

    :goto_3
    if-ge v0, v2, :cond_6

    aget v3, p0, v0

    .line 1007605
    float-to-double v4, v3

    invoke-interface {v1, v4, v5}, LX/5pD;->pushDouble(D)V

    .line 1007606
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1007607
    :cond_3
    instance-of v2, p0, [D

    if-eqz v2, :cond_4

    .line 1007608
    check-cast p0, [D

    check-cast p0, [D

    array-length v2, p0

    :goto_4
    if-ge v0, v2, :cond_6

    aget-wide v4, p0, v0

    .line 1007609
    invoke-interface {v1, v4, v5}, LX/5pD;->pushDouble(D)V

    .line 1007610
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1007611
    :cond_4
    instance-of v2, p0, [Z

    if-eqz v2, :cond_5

    .line 1007612
    check-cast p0, [Z

    check-cast p0, [Z

    array-length v2, p0

    :goto_5
    if-ge v0, v2, :cond_6

    aget-boolean v3, p0, v0

    .line 1007613
    invoke-interface {v1, v3}, LX/5pD;->pushBoolean(Z)V

    .line 1007614
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1007615
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown array type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007616
    :cond_6
    return-object v1
.end method

.method public static a(Landroid/os/Bundle;)LX/5pH;
    .locals 6

    .prologue
    .line 1007571
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v2

    .line 1007572
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1007573
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 1007574
    if-nez v1, :cond_0

    .line 1007575
    invoke-interface {v2, v0}, LX/5pH;->putNull(Ljava/lang/String;)V

    goto :goto_0

    .line 1007576
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1007577
    invoke-static {v1}, LX/5op;->a(Ljava/lang/Object;)LX/5pD;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pD;)V

    goto :goto_0

    .line 1007578
    :cond_1
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 1007579
    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1007580
    :cond_2
    instance-of v4, v1, Ljava/lang/Number;

    if-eqz v4, :cond_4

    .line 1007581
    instance-of v4, v1, Ljava/lang/Integer;

    if-eqz v4, :cond_3

    .line 1007582
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v2, v0, v1}, LX/5pH;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 1007583
    :cond_3
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v4

    invoke-interface {v2, v0, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    goto :goto_0

    .line 1007584
    :cond_4
    instance-of v4, v1, Ljava/lang/Boolean;

    if-eqz v4, :cond_5

    .line 1007585
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v2, v0, v1}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1007586
    :cond_5
    instance-of v4, v1, Landroid/os/Bundle;

    if-eqz v4, :cond_6

    .line 1007587
    check-cast v1, Landroid/os/Bundle;

    invoke-static {v1}, LX/5op;->a(Landroid/os/Bundle;)LX/5pH;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    goto :goto_0

    .line 1007588
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not convert "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007589
    :cond_7
    return-object v2
.end method

.method public static a(LX/5pG;)Landroid/os/Bundle;
    .locals 6
    .param p0    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1007531
    if-nez p0, :cond_0

    .line 1007532
    :goto_0
    return-object v0

    .line 1007533
    :cond_0
    invoke-interface {p0}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v2

    .line 1007534
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1007535
    :goto_1
    invoke-interface {v2}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1007536
    invoke-interface {v2}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v3

    .line 1007537
    invoke-interface {p0, v3}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v4

    .line 1007538
    sget-object v5, LX/5oo;->a:[I

    invoke-virtual {v4}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    .line 1007539
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not convert object with key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007540
    :pswitch_0
    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1007541
    :pswitch_1
    invoke-interface {p0, v3}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 1007542
    :pswitch_2
    invoke-interface {p0, v3}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    goto :goto_1

    .line 1007543
    :pswitch_3
    invoke-interface {p0, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1007544
    :pswitch_4
    invoke-interface {p0, v3}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v4

    invoke-static {v4}, LX/5op;->a(LX/5pG;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 1007545
    :pswitch_5
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Arrays aren\'t supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v0, v1

    .line 1007546
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a([Ljava/lang/Object;)Lcom/facebook/react/bridge/WritableNativeArray;
    .locals 6

    .prologue
    .line 1007548
    new-instance v2, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v2}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    .line 1007549
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p0

    if-ge v1, v0, :cond_8

    .line 1007550
    aget-object v0, p0, v1

    .line 1007551
    if-nez v0, :cond_0

    .line 1007552
    invoke-virtual {v2}, Lcom/facebook/react/bridge/WritableNativeArray;->pushNull()V

    .line 1007553
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1007554
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 1007555
    const-class v4, Ljava/lang/Boolean;

    if-ne v3, v4, :cond_1

    .line 1007556
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/facebook/react/bridge/WritableNativeArray;->pushBoolean(Z)V

    goto :goto_1

    .line 1007557
    :cond_1
    const-class v4, Ljava/lang/Integer;

    if-ne v3, v4, :cond_2

    .line 1007558
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/facebook/react/bridge/WritableNativeArray;->pushDouble(D)V

    goto :goto_1

    .line 1007559
    :cond_2
    const-class v4, Ljava/lang/Double;

    if-ne v3, v4, :cond_3

    .line 1007560
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/facebook/react/bridge/WritableNativeArray;->pushDouble(D)V

    goto :goto_1

    .line 1007561
    :cond_3
    const-class v4, Ljava/lang/Float;

    if-ne v3, v4, :cond_4

    .line 1007562
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/facebook/react/bridge/WritableNativeArray;->pushDouble(D)V

    goto :goto_1

    .line 1007563
    :cond_4
    const-class v4, Ljava/lang/String;

    if-ne v3, v4, :cond_5

    .line 1007564
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/react/bridge/WritableNativeArray;->pushString(Ljava/lang/String;)V

    goto :goto_1

    .line 1007565
    :cond_5
    const-class v4, Lcom/facebook/react/bridge/WritableNativeMap;

    if-ne v3, v4, :cond_6

    .line 1007566
    check-cast v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-virtual {v2, v0}, Lcom/facebook/react/bridge/WritableNativeArray;->a(LX/5pH;)V

    goto :goto_1

    .line 1007567
    :cond_6
    const-class v4, Lcom/facebook/react/bridge/WritableNativeArray;

    if-ne v3, v4, :cond_7

    .line 1007568
    check-cast v0, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-virtual {v2, v0}, Lcom/facebook/react/bridge/WritableNativeArray;->a(LX/5pD;)V

    goto :goto_1

    .line 1007569
    :cond_7
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot convert argument of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007570
    :cond_8
    return-object v2
.end method

.method public static b()LX/5pH;
    .locals 1

    .prologue
    .line 1007547
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    return-object v0
.end method
