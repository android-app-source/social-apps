.class public final LX/5GA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 887237
    const-wide/16 v12, 0x0

    .line 887238
    const-wide/16 v10, 0x0

    .line 887239
    const-wide/16 v8, 0x0

    .line 887240
    const-wide/16 v6, 0x0

    .line 887241
    const/4 v5, 0x0

    .line 887242
    const/4 v4, 0x0

    .line 887243
    const/4 v3, 0x0

    .line 887244
    const/4 v2, 0x0

    .line 887245
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 887246
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 887247
    const/4 v2, 0x0

    .line 887248
    :goto_0
    return v2

    .line 887249
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_5

    .line 887250
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 887251
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 887252
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 887253
    const-string v6, "east"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 887254
    const/4 v2, 0x1

    .line 887255
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 887256
    :cond_1
    const-string v6, "north"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 887257
    const/4 v2, 0x1

    .line 887258
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto :goto_1

    .line 887259
    :cond_2
    const-string v6, "south"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 887260
    const/4 v2, 0x1

    .line 887261
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide v14, v6

    goto :goto_1

    .line 887262
    :cond_3
    const-string v6, "west"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 887263
    const/4 v2, 0x1

    .line 887264
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto :goto_1

    .line 887265
    :cond_4
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 887266
    :cond_5
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 887267
    if-eqz v3, :cond_6

    .line 887268
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 887269
    :cond_6
    if-eqz v10, :cond_7

    .line 887270
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 887271
    :cond_7
    if-eqz v9, :cond_8

    .line 887272
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 887273
    :cond_8
    if-eqz v8, :cond_9

    .line 887274
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 887275
    :cond_9
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move-wide v14, v8

    move-wide/from16 v16, v10

    move v10, v4

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide v4, v12

    move-wide v12, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 887218
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 887219
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 887220
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 887221
    const-string v2, "east"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887222
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 887223
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 887224
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 887225
    const-string v2, "north"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887226
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 887227
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 887228
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 887229
    const-string v2, "south"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887230
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 887231
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 887232
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 887233
    const-string v2, "west"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887234
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 887235
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 887236
    return-void
.end method
