.class public LX/5Sa;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 918462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/5UY;)Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;
    .locals 6
    .param p0    # LX/5UY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 918463
    if-nez p0, :cond_0

    .line 918464
    :goto_0
    return-object v1

    .line 918465
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 918466
    invoke-interface {p0}, LX/5UY;->d()LX/0Px;

    move-result-object v4

    .line 918467
    if-eqz v4, :cond_1

    .line 918468
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    .line 918469
    invoke-static {v0}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 918470
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 918471
    :cond_1
    invoke-interface {p0}, LX/5UY;->b()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, LX/5UY;->b()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel;->a()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel$AppCenterCoverImageModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 918472
    invoke-interface {p0}, LX/5UY;->b()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel;->a()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel$AppCenterCoverImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel$AppCenterCoverImageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 918473
    :goto_2
    new-instance v1, LX/5Sw;

    invoke-direct {v1}, LX/5Sw;-><init>()V

    invoke-interface {p0}, LX/5UY;->c()Ljava/lang/String;

    move-result-object v2

    .line 918474
    iput-object v2, v1, LX/5Sw;->a:Ljava/lang/String;

    .line 918475
    move-object v1, v1

    .line 918476
    invoke-interface {p0}, LX/5UY;->cC_()Ljava/lang/String;

    move-result-object v2

    .line 918477
    iput-object v2, v1, LX/5Sw;->h:Ljava/lang/String;

    .line 918478
    move-object v1, v1

    .line 918479
    invoke-interface {p0}, LX/5UY;->l()Ljava/lang/String;

    move-result-object v2

    .line 918480
    iput-object v2, v1, LX/5Sw;->i:Ljava/lang/String;

    .line 918481
    move-object v1, v1

    .line 918482
    invoke-interface {p0}, LX/5UY;->p()Ljava/lang/String;

    move-result-object v2

    .line 918483
    iput-object v2, v1, LX/5Sw;->j:Ljava/lang/String;

    .line 918484
    move-object v1, v1

    .line 918485
    invoke-interface {p0}, LX/5UY;->m()Ljava/lang/String;

    move-result-object v2

    .line 918486
    iput-object v2, v1, LX/5Sw;->k:Ljava/lang/String;

    .line 918487
    move-object v1, v1

    .line 918488
    invoke-interface {p0}, LX/5UY;->cD_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5Sw;->d(Ljava/lang/String;)LX/5Sw;

    move-result-object v1

    invoke-interface {p0}, LX/5UY;->j()Ljava/lang/String;

    move-result-object v2

    .line 918489
    iput-object v2, v1, LX/5Sw;->b:Ljava/lang/String;

    .line 918490
    move-object v1, v1

    .line 918491
    iput-object v3, v1, LX/5Sw;->m:Ljava/util/List;

    .line 918492
    move-object v1, v1

    .line 918493
    invoke-interface {p0}, LX/5UY;->cN_()Ljava/lang/String;

    move-result-object v2

    .line 918494
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_3
    iput-object v3, v1, LX/5Sw;->n:Landroid/net/Uri;

    .line 918495
    move-object v1, v1

    .line 918496
    invoke-interface {p0}, LX/5UY;->k()Ljava/lang/String;

    move-result-object v2

    .line 918497
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_4
    iput-object v3, v1, LX/5Sw;->o:Landroid/net/Uri;

    .line 918498
    move-object v1, v1

    .line 918499
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    :goto_5
    move-object v0, v1

    .line 918500
    invoke-interface {p0}, LX/5UY;->e()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 918501
    invoke-interface {p0}, LX/5UY;->e()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v1

    invoke-static {v1}, LX/5Ta;->a(Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;)Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    move-result-object v1

    .line 918502
    iput-object v1, v0, LX/5Sw;->l:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    .line 918503
    :cond_2
    invoke-virtual {v0}, LX/5Sw;->p()Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;

    move-result-object v1

    goto/16 :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2

    .line 918504
    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    .line 918505
    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    :cond_6
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 918506
    iput-object v2, v1, LX/5Sw;->e:Landroid/net/Uri;

    .line 918507
    move-object v1, v1

    .line 918508
    goto :goto_5
.end method
