.class public final enum LX/6Cx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Cx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Cx;

.field public static final enum INSTANT_EXPERIENCE:LX/6Cx;

.field public static final enum MESSENGER_EXTENSION:LX/6Cx;

.field public static final enum NONE:LX/6Cx;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1064460
    new-instance v0, LX/6Cx;

    const-string v1, "MESSENGER_EXTENSION"

    const-string v2, "messenger_extension"

    invoke-direct {v0, v1, v3, v2}, LX/6Cx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Cx;->MESSENGER_EXTENSION:LX/6Cx;

    .line 1064461
    new-instance v0, LX/6Cx;

    const-string v1, "INSTANT_EXPERIENCE"

    const-string v2, "instant_experience"

    invoke-direct {v0, v1, v4, v2}, LX/6Cx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Cx;->INSTANT_EXPERIENCE:LX/6Cx;

    .line 1064462
    new-instance v0, LX/6Cx;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v5, v2}, LX/6Cx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Cx;->NONE:LX/6Cx;

    .line 1064463
    const/4 v0, 0x3

    new-array v0, v0, [LX/6Cx;

    sget-object v1, LX/6Cx;->MESSENGER_EXTENSION:LX/6Cx;

    aput-object v1, v0, v3

    sget-object v1, LX/6Cx;->INSTANT_EXPERIENCE:LX/6Cx;

    aput-object v1, v0, v4

    sget-object v1, LX/6Cx;->NONE:LX/6Cx;

    aput-object v1, v0, v5

    sput-object v0, LX/6Cx;->$VALUES:[LX/6Cx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1064464
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1064465
    iput-object p3, p0, LX/6Cx;->value:Ljava/lang/String;

    .line 1064466
    return-void
.end method

.method public static fromRawValue(Ljava/lang/String;)LX/6Cx;
    .locals 5

    .prologue
    .line 1064453
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1064454
    sget-object v0, LX/6Cx;->NONE:LX/6Cx;

    .line 1064455
    :cond_0
    :goto_0
    return-object v0

    .line 1064456
    :cond_1
    invoke-static {}, LX/6Cx;->values()[LX/6Cx;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 1064457
    iget-object v4, v0, LX/6Cx;->value:Ljava/lang/String;

    invoke-static {v4, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1064458
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1064459
    :cond_2
    sget-object v0, LX/6Cx;->NONE:LX/6Cx;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Cx;
    .locals 1

    .prologue
    .line 1064452
    const-class v0, LX/6Cx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Cx;

    return-object v0
.end method

.method public static values()[LX/6Cx;
    .locals 1

    .prologue
    .line 1064451
    sget-object v0, LX/6Cx;->$VALUES:[LX/6Cx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Cx;

    return-object v0
.end method
