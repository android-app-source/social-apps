.class public LX/63h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0h5;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:Landroid/support/v7/widget/Toolbar;

.field private final d:LX/63Y;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/63J;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/63W;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Landroid/view/View$OnClickListener;

.field private final j:LX/3xb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1043539
    const v0, 0x7f020014

    sput v0, LX/63h;->a:I

    .line 1043540
    const v0, 0x7f020015

    sput v0, LX/63h;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/support/v7/widget/Toolbar;)V
    .locals 2

    .prologue
    .line 1043528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1043529
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1043530
    iput-object v0, p0, LX/63h;->e:LX/0Px;

    .line 1043531
    new-instance v0, LX/63e;

    invoke-direct {v0, p0}, LX/63e;-><init>(LX/63h;)V

    iput-object v0, p0, LX/63h;->i:Landroid/view/View$OnClickListener;

    .line 1043532
    new-instance v0, LX/63f;

    invoke-direct {v0, p0}, LX/63f;-><init>(LX/63h;)V

    iput-object v0, p0, LX/63h;->j:LX/3xb;

    .line 1043533
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    .line 1043534
    new-instance v0, LX/63Y;

    invoke-direct {v0}, LX/63Y;-><init>()V

    iput-object v0, p0, LX/63h;->d:LX/63Y;

    .line 1043535
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/63h;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1043536
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/63h;->j:LX/3xb;

    .line 1043537
    iput-object v1, v0, Landroid/support/v7/widget/Toolbar;->D:LX/3xb;

    .line 1043538
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1043525
    iget-object v0, p0, LX/63h;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1043526
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/63h;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 1043527
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1043524
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported. Try setHasBackButton()."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d_(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1043521
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1043522
    invoke-virtual {p0, v0}, LX/63h;->setCustomTitleView(Landroid/view/View;)V

    .line 1043523
    return-object v0
.end method

.method public final setButtonSpecs(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1043515
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/63h;->e:LX/0Px;

    .line 1043516
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v0

    .line 1043517
    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    .line 1043518
    iget-object v1, p0, LX/63h;->e:LX/0Px;

    invoke-static {v0, v1}, LX/63Y;->a(Landroid/view/Menu;Ljava/util/List;)V

    .line 1043519
    iget-object v1, p0, LX/63h;->d:LX/63Y;

    iget-object v2, p0, LX/63h;->e:LX/0Px;

    iget-object v3, p0, LX/63h;->h:LX/63W;

    invoke-virtual {v1, v0, v2, v3}, LX/63Y;->a(Landroid/view/Menu;Ljava/util/List;LX/63W;)V

    .line 1043520
    return-void
.end method

.method public final setCustomTitleView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1043510
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1043511
    invoke-direct {p0}, LX/63h;->a()V

    .line 1043512
    iput-object p1, p0, LX/63h;->f:Landroid/view/View;

    .line 1043513
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, LX/63h;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 1043514
    return-void
.end method

.method public final setHasBackButton(Z)V
    .locals 2

    .prologue
    .line 1043506
    if-eqz p1, :cond_0

    .line 1043507
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    sget v1, LX/63h;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 1043508
    :goto_0
    return-void

    .line 1043509
    :cond_0
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final setHasFbLogo(Z)V
    .locals 2

    .prologue
    .line 1043505
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setOnBackPressedListener(LX/63J;)V
    .locals 0

    .prologue
    .line 1043491
    iput-object p1, p0, LX/63h;->g:LX/63J;

    .line 1043492
    return-void
.end method

.method public final setOnToolbarButtonListener(LX/63W;)V
    .locals 0

    .prologue
    .line 1043503
    iput-object p1, p0, LX/63h;->h:LX/63W;

    .line 1043504
    return-void
.end method

.method public final setShowDividers(Z)V
    .locals 2

    .prologue
    .line 1043502
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setTitle(I)V
    .locals 1

    .prologue
    .line 1043500
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/63h;->setTitle(Ljava/lang/String;)V

    .line 1043501
    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1043497
    invoke-direct {p0}, LX/63h;->a()V

    .line 1043498
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1043499
    return-void
.end method

.method public final setTitlebarAsModal(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1043493
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/63h;->setHasBackButton(Z)V

    .line 1043494
    iget-object v0, p0, LX/63h;->c:Landroid/support/v7/widget/Toolbar;

    sget v1, LX/63h;->b:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 1043495
    new-instance v0, LX/63g;

    invoke-direct {v0, p0, p1}, LX/63g;-><init>(LX/63h;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, LX/63h;->setOnBackPressedListener(LX/63J;)V

    .line 1043496
    return-void
.end method
