.class public final LX/6Fk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:LX/6Ft;


# direct methods
.method public constructor <init>(LX/6Ft;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1068918
    iput-object p1, p0, LX/6Fk;->b:LX/6Ft;

    iput-object p2, p0, LX/6Fk;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1068911
    iget-object v0, p0, LX/6Fk;->b:LX/6Ft;

    iget-object v1, p0, LX/6Fk;->a:Ljava/io/File;

    .line 1068912
    :try_start_0
    const-string v2, "report.txt"

    invoke-static {v1, v2}, LX/6G3;->b(Ljava/io/File;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1068913
    :goto_0
    move-object v0, v2

    .line 1068914
    return-object v0

    .line 1068915
    :catch_0
    move-exception v2

    .line 1068916
    iget-object v3, v0, LX/6Ft;->c:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p0, LX/6Ft;->u:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p0, "generateAcraReport"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068917
    const/4 v2, 0x0

    goto :goto_0
.end method
