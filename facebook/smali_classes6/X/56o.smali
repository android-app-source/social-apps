.class public final LX/56o;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 838953
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 838954
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 838955
    :goto_0
    return v1

    .line 838956
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 838957
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 838958
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 838959
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 838960
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 838961
    const-string v4, "ranges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 838962
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 838963
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 838964
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 838965
    invoke-static {p0, p1}, LX/56n;->b(LX/15w;LX/186;)I

    move-result v3

    .line 838966
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 838967
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 838968
    goto :goto_1

    .line 838969
    :cond_3
    const-string v4, "text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 838970
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 838971
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 838972
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 838973
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 838974
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 838975
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 838976
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 838977
    if-eqz v0, :cond_1

    .line 838978
    const-string v1, "ranges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838979
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 838980
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 838981
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/56n;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 838982
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 838983
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 838984
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 838985
    if-eqz v0, :cond_2

    .line 838986
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838987
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 838988
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 838989
    return-void
.end method
