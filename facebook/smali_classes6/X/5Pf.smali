.class public LX/5Pf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5Pe;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 910999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 911000
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move v0, v9

    .line 911001
    :goto_0
    iget-object v2, p1, LX/5Pe;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 911002
    iget-object v2, p1, LX/5Pe;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p1, LX/5Pe;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 911003
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 911004
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/5Pf;->c:Ljava/util/Map;

    .line 911005
    iget v0, p1, LX/5Pe;->a:I

    iput v0, p0, LX/5Pf;->a:I

    .line 911006
    iget-object v2, p1, LX/5Pe;->c:Landroid/graphics/Bitmap;

    .line 911007
    iget-object v8, p1, LX/5Pe;->f:Ljava/nio/ByteBuffer;

    .line 911008
    new-array v0, v4, [I

    .line 911009
    invoke-static {v4, v0, v9}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 911010
    aget v0, v0, v9

    iput v0, p0, LX/5Pf;->b:I

    .line 911011
    :try_start_0
    iget v0, p0, LX/5Pf;->a:I

    iget v1, p0, LX/5Pf;->b:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 911012
    iget-object v0, p0, LX/5Pf;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 911013
    iget v4, p0, LX/5Pf;->a:I

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v4, v1, v0}, Landroid/opengl/GLES20;->glTexParameteri(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 911014
    :catchall_0
    move-exception v0

    iget v1, p0, LX/5Pf;->a:I

    invoke-static {v1, v9}, Landroid/opengl/GLES20;->glBindTexture(II)V

    throw v0

    .line 911015
    :cond_1
    if-eqz v2, :cond_3

    .line 911016
    :try_start_1
    iget v0, p0, LX/5Pf;->a:I

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 911017
    :cond_2
    :goto_2
    iget v0, p0, LX/5Pf;->a:I

    invoke-static {v0, v9}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 911018
    return-void

    .line 911019
    :cond_3
    if-eqz v8, :cond_2

    .line 911020
    :try_start_2
    iget v0, p0, LX/5Pf;->a:I

    const/4 v1, 0x0

    iget v2, p1, LX/5Pe;->g:I

    iget v3, p1, LX/5Pe;->d:I

    iget v4, p1, LX/5Pe;->e:I

    const/4 v5, 0x0

    iget v6, p1, LX/5Pe;->g:I

    const/16 v7, 0x1401

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 910995
    new-array v0, v3, [I

    .line 910996
    iget v1, p0, LX/5Pf;->b:I

    aput v1, v0, v2

    .line 910997
    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 910998
    return-void
.end method
