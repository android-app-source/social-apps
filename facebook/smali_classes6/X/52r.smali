.class public abstract LX/52r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 826447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()J
    .locals 2

    .prologue
    .line 826448
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public abstract a(LX/0vR;)LX/0za;
.end method

.method public final a(LX/0vR;JJLjava/util/concurrent/TimeUnit;)LX/0za;
    .locals 8

    .prologue
    .line 826449
    invoke-virtual {p6, p4, p5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v6

    .line 826450
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, LX/52r;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    invoke-virtual {p6, p2, p3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    add-long v4, v0, v2

    .line 826451
    new-instance v2, LX/54k;

    invoke-direct {v2}, LX/54k;-><init>()V

    .line 826452
    new-instance v0, LX/52q;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, LX/52q;-><init>(LX/52r;LX/54k;LX/0vR;JJ)V

    .line 826453
    invoke-virtual {p0, v0, p2, p3, p6}, LX/52r;->a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/54k;->a(LX/0za;)V

    .line 826454
    return-object v2
.end method

.method public abstract a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;
.end method
