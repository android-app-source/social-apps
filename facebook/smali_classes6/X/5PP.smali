.class public LX/5PP;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:I

.field private static final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 910671
    const/16 v0, 0x3142

    sput v0, LX/5PP;->a:I

    .line 910672
    const-class v0, LX/5PP;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/5PP;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 910673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 910674
    move v0, v1

    move v3, v1

    .line 910675
    :goto_0
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v2

    move v3, v4

    .line 910676
    goto :goto_0

    .line 910677
    :cond_0
    if-nez v3, :cond_1

    .line 910678
    return-void

    .line 910679
    :cond_1
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "%s: GL error 0x%04x %s occurred, see logcat output"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v4

    const/4 v1, 0x2

    .line 910680
    packed-switch v0, :pswitch_data_0

    .line 910681
    :pswitch_0
    const-string v4, "UNKNOWN"

    .line 910682
    :goto_1
    move-object v0, v4

    .line 910683
    aput-object v0, v5, v1

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 910684
    :pswitch_1
    const-string v4, "GL_INVALID_ENUM"

    goto :goto_1

    .line 910685
    :pswitch_2
    const-string v4, "GL_INVALID_FRAMEBUFFER_OPERATION"

    goto :goto_1

    .line 910686
    :pswitch_3
    const-string v4, "GL_INVALID_OPERATION"

    goto :goto_1

    .line 910687
    :pswitch_4
    const-string v4, "GL_INVALID_VALUE"

    goto :goto_1

    .line 910688
    :pswitch_5
    const-string v4, "GL_OUT_OF_MEMORY"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x500
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 910689
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "%s: EGL error 0x%04x %s occurred, see logcat output"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 910690
    packed-switch p1, :pswitch_data_0

    .line 910691
    const-string v4, "UNKNOWN"

    .line 910692
    :goto_0
    move-object v4, v4

    .line 910693
    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910694
    :pswitch_0
    const-string v4, "EGL_NOT_INITIALIZED"

    goto :goto_0

    .line 910695
    :pswitch_1
    const-string v4, "EGL_BAD_ACCESS"

    goto :goto_0

    .line 910696
    :pswitch_2
    const-string v4, "EGL_BAD_ALLOC"

    goto :goto_0

    .line 910697
    :pswitch_3
    const-string v4, "EGL_BAD_ATTRIBUTE"

    goto :goto_0

    .line 910698
    :pswitch_4
    const-string v4, "EGL_BAD_CONTEXT"

    goto :goto_0

    .line 910699
    :pswitch_5
    const-string v4, "EGL_BAD_CONFIG"

    goto :goto_0

    .line 910700
    :pswitch_6
    const-string v4, "EGL_BAD_CURRENT_SURFACE"

    goto :goto_0

    .line 910701
    :pswitch_7
    const-string v4, "EGL_BAD_DISPLAY"

    goto :goto_0

    .line 910702
    :pswitch_8
    const-string v4, "EGL_BAD_SURFACE"

    goto :goto_0

    .line 910703
    :pswitch_9
    const-string v4, "EGL_BAD_MATCH"

    goto :goto_0

    .line 910704
    :pswitch_a
    const-string v4, "EGL_BAD_PARAMETER"

    goto :goto_0

    .line 910705
    :pswitch_b
    const-string v4, "EGL_BAD_NATIVE_PIXMAP"

    goto :goto_0

    .line 910706
    :pswitch_c
    const-string v4, "EGL_BAD_NATIVE_WINDOW"

    goto :goto_0

    .line 910707
    :pswitch_d
    const-string v4, "EGL_CONTEXT_LOST"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3001
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_a
        :pswitch_8
        :pswitch_d
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 910708
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 910709
    const/16 v1, 0x3000

    .line 910710
    const/4 v0, 0x0

    move v3, v0

    move v0, v1

    .line 910711
    :goto_0
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 910712
    const/4 v0, 0x1

    move v3, v0

    move v0, v2

    goto :goto_0

    .line 910713
    :cond_0
    if-nez v3, :cond_3

    .line 910714
    :goto_1
    return-void

    .line 910715
    :cond_1
    const/16 v2, 0x3000

    .line 910716
    const/4 v1, 0x0

    .line 910717
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    move v4, v1

    move v1, v2

    .line 910718
    :goto_2
    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v3

    if-eq v3, v2, :cond_2

    .line 910719
    const/4 v1, 0x1

    move v4, v1

    move v1, v3

    goto :goto_2

    .line 910720
    :cond_2
    if-nez v4, :cond_4

    .line 910721
    :goto_3
    goto :goto_1

    .line 910722
    :cond_3
    invoke-static {p0, v0}, LX/5PP;->a(Ljava/lang/String;I)V

    goto :goto_1

    .line 910723
    :cond_4
    invoke-static {p0, v1}, LX/5PP;->a(Ljava/lang/String;I)V

    goto :goto_3
.end method
