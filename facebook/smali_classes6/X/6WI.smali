.class public final LX/6WI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/31a;

.field public b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1105321
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/2EJ;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/6WI;-><init>(Landroid/content/Context;I)V

    .line 1105322
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 1105317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1105318
    new-instance v0, LX/31a;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-static {p1, p2}, LX/2EJ;->a(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, p1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, LX/31a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6WI;->a:LX/31a;

    .line 1105319
    iput p2, p0, LX/6WI;->b:I

    .line 1105320
    return-void
.end method


# virtual methods
.method public final a(I)LX/6WI;
    .locals 2

    .prologue
    .line 1105315
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/31a;->f:Ljava/lang/CharSequence;

    .line 1105316
    return-object p0
.end method

.method public final a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;
    .locals 2

    .prologue
    .line 1105312
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/31a;->k:Ljava/lang/CharSequence;

    .line 1105313
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iput-object p2, v0, LX/31a;->l:Landroid/content/DialogInterface$OnClickListener;

    .line 1105314
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/6WI;
    .locals 1

    .prologue
    .line 1105280
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iput-object p1, v0, LX/31a;->f:Ljava/lang/CharSequence;

    .line 1105281
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;
    .locals 1

    .prologue
    .line 1105309
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iput-object p1, v0, LX/31a;->k:Ljava/lang/CharSequence;

    .line 1105310
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iput-object p2, v0, LX/31a;->l:Landroid/content/DialogInterface$OnClickListener;

    .line 1105311
    return-object p0
.end method

.method public final a(Z)LX/6WI;
    .locals 1

    .prologue
    .line 1105307
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iput-boolean p1, v0, LX/31a;->q:Z

    .line 1105308
    return-object p0
.end method

.method public final a()LX/6WJ;
    .locals 3

    .prologue
    .line 1105297
    new-instance v0, LX/6WJ;

    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    iget v2, p0, LX/6WI;->b:I

    invoke-direct {v0, v1, v2}, LX/6WJ;-><init>(Landroid/content/Context;I)V

    .line 1105298
    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v2, v0, LX/2EJ;->a:LX/4BW;

    invoke-virtual {v1, v2}, LX/31a;->a(LX/4BW;)V

    .line 1105299
    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-boolean v1, v1, LX/31a;->q:Z

    invoke-virtual {v0, v1}, LX/6WJ;->setCancelable(Z)V

    .line 1105300
    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-boolean v1, v1, LX/31a;->q:Z

    if-eqz v1, :cond_0

    .line 1105301
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/6WJ;->setCanceledOnTouchOutside(Z)V

    .line 1105302
    :cond_0
    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->r:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, LX/6WJ;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1105303
    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->s:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, LX/6WJ;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1105304
    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->t:Landroid/content/DialogInterface$OnKeyListener;

    if-eqz v1, :cond_1

    .line 1105305
    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->t:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, LX/6WJ;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1105306
    :cond_1
    return-object v0
.end method

.method public final b(I)LX/6WI;
    .locals 2

    .prologue
    .line 1105295
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/31a;->h:Ljava/lang/CharSequence;

    .line 1105296
    return-object p0
.end method

.method public final b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;
    .locals 2

    .prologue
    .line 1105292
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, LX/31a;->m:Ljava/lang/CharSequence;

    .line 1105293
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iput-object p2, v0, LX/31a;->n:Landroid/content/DialogInterface$OnClickListener;

    .line 1105294
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)LX/6WI;
    .locals 1

    .prologue
    .line 1105290
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iput-object p1, v0, LX/31a;->h:Ljava/lang/CharSequence;

    .line 1105291
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;
    .locals 1

    .prologue
    .line 1105287
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iput-object p1, v0, LX/31a;->m:Ljava/lang/CharSequence;

    .line 1105288
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iput-object p2, v0, LX/31a;->n:Landroid/content/DialogInterface$OnClickListener;

    .line 1105289
    return-object p0
.end method

.method public final b()LX/6WJ;
    .locals 2

    .prologue
    .line 1105284
    invoke-virtual {p0}, LX/6WI;->a()LX/6WJ;

    move-result-object v0

    .line 1105285
    :try_start_0
    invoke-virtual {v0}, LX/6WJ;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1105286
    :goto_0
    return-object v0

    :catch_0
    goto :goto_0
.end method

.method public final c(I)LX/6WI;
    .locals 2

    .prologue
    .line 1105282
    iget-object v0, p0, LX/6WI;->a:LX/31a;

    iget-object v1, p0, LX/6WI;->a:LX/31a;

    iget-object v1, v1, LX/31a;->a:Landroid/content/Context;

    invoke-static {v1, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, LX/31a;->i:Landroid/graphics/drawable/Drawable;

    .line 1105283
    return-object p0
.end method
