.class public LX/6VU;
.super LX/1BL;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6VU;


# instance fields
.field private final b:LX/0So;


# direct methods
.method public constructor <init>(LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1104095
    invoke-direct {p0}, LX/1BL;-><init>()V

    .line 1104096
    iput-object p1, p0, LX/6VU;->b:LX/0So;

    .line 1104097
    return-void
.end method

.method public static a(LX/0QB;)LX/6VU;
    .locals 4

    .prologue
    .line 1104063
    sget-object v0, LX/6VU;->c:LX/6VU;

    if-nez v0, :cond_1

    .line 1104064
    const-class v1, LX/6VU;

    monitor-enter v1

    .line 1104065
    :try_start_0
    sget-object v0, LX/6VU;->c:LX/6VU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1104066
    if-eqz v2, :cond_0

    .line 1104067
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1104068
    new-instance p0, LX/6VU;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-direct {p0, v3}, LX/6VU;-><init>(LX/0So;)V

    .line 1104069
    move-object v0, p0

    .line 1104070
    sput-object v0, LX/6VU;->c:LX/6VU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1104071
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1104072
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1104073
    :cond_1
    sget-object v0, LX/6VU;->c:LX/6VU;

    return-object v0

    .line 1104074
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1104075
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)LX/1fG;
    .locals 2

    .prologue
    .line 1104091
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/1fG;

    iget-object v1, p0, LX/6VU;->b:LX/0So;

    invoke-direct {v0, p3, p1, v1}, LX/1fG;-><init>(Landroid/net/Uri;Ljava/lang/String;LX/0So;)V

    .line 1104092
    iget-object v1, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v1, p1, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1104093
    monitor-exit p0

    return-object v0

    .line 1104094
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/1bf;)LX/2xm;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1104098
    const/4 v0, 0x0

    return-object v0
.end method

.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 1104083
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v0}, LX/0vX;->t()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1104084
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fG;

    .line 1104085
    sget-boolean v2, LX/1fG;->i:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    const-string v3, "Attempting to clear image load times while they are being tracked"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1104086
    iget-object v2, v0, LX/1fG;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1104087
    goto :goto_0

    .line 1104088
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1104089
    :cond_0
    monitor-exit p0

    return-void

    .line 1104090
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized c(Ljava/lang/String;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "LX/1fG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1104076
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid feed unit id"

    invoke-static {v0, v1}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 1104077
    iget-object v0, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v0, p1}, LX/0Xu;->f(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 1104078
    const/4 v0, 0x0

    .line 1104079
    :goto_1
    monitor-exit p0

    return-object v0

    .line 1104080
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1104081
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/1BL;->a:LX/0vX;

    invoke-interface {v0, p1}, LX/0vX;->a(Ljava/lang/Object;)Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 1104082
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
