.class public LX/6AE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/4Yr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

.field private final b:LX/6Ou;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;LX/6Ou;)V
    .locals 0
    .param p1    # Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058747
    iput-object p1, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    .line 1058748
    iput-object p2, p0, LX/6AE;->b:LX/6Ou;

    .line 1058749
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1058752
    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel$TargetModel;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1058753
    :cond_0
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1058754
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel$TargetModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 6

    .prologue
    .line 1058755
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, LX/4Yr;

    const/4 v2, 0x0

    .line 1058756
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;->j()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;

    move-result-object v0

    const/4 v3, 0x0

    .line 1058757
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    .line 1058758
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel;->j()LX/0Px;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel;

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$StoryModel$AttachmentsModel$TargetModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1058759
    if-eqz v4, :cond_4

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    .line 1058760
    :goto_0
    move v0, v1

    .line 1058761
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1058762
    :cond_0
    :goto_1
    return-void

    .line 1058763
    :cond_1
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1058764
    iget-object v0, p0, LX/6AE;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;

    .line 1058765
    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLDeletePlaceRecommendationFromPlaceListMutationCallModel$ChangedPagesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1058766
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1058767
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1058768
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v1

    if-nez v1, :cond_5

    .line 1058769
    :cond_3
    :goto_3
    move-object v0, v0

    .line 1058770
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/4Yr;->a(LX/0Px;)V

    goto :goto_1

    :cond_4
    move v1, v3

    .line 1058771
    goto/16 :goto_0

    .line 1058772
    :catch_0
    move v1, v3

    goto/16 :goto_0

    .line 1058773
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fe()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v1

    .line 1058774
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1058775
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_4
    if-ge v2, p0, :cond_7

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceListItem;

    .line 1058776
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceListItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object p1

    .line 1058777
    invoke-interface {v3, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 1058778
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1058779
    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 1058780
    :cond_7
    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-static {v2}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v2

    new-instance v5, LX/4YB;

    invoke-direct {v5}, LX/4YB;-><init>()V

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 1058781
    iput-object v4, v5, LX/4YB;->b:LX/0Px;

    .line 1058782
    move-object v4, v5

    .line 1058783
    invoke-virtual {v4}, LX/4YB;->a()Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    move-result-object v4

    .line 1058784
    iput-object v4, v2, LX/4XR;->hJ:Lcom/facebook/graphql/model/GraphQLPlaceListItemsFromPlaceListConnection;

    .line 1058785
    move-object v2, v2

    .line 1058786
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 1058787
    iput-object v2, v1, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1058788
    move-object v1, v1

    .line 1058789
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    goto :goto_3
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058751
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058750
    const-string v0, "SocialSearchDeletePlaceRecommendationsMutatingVisitor"

    return-object v0
.end method
