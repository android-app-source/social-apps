.class public final LX/5RO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 915044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915045
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 915046
    iput-object v0, p0, LX/5RO;->g:LX/0Px;

    .line 915047
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5RO;->h:Ljava/lang/String;

    .line 915048
    const-string v0, ""

    iput-object v0, p0, LX/5RO;->i:Ljava/lang/String;

    .line 915049
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)V
    .locals 1

    .prologue
    .line 915050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915051
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 915052
    iput-object v0, p0, LX/5RO;->g:LX/0Px;

    .line 915053
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5RO;->h:Ljava/lang/String;

    .line 915054
    const-string v0, ""

    iput-object v0, p0, LX/5RO;->i:Ljava/lang/String;

    .line 915055
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTaggedPlace:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, LX/5RO;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 915056
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mPlaceAttachmentRemoved:Z

    iput-boolean v0, p0, LX/5RO;->b:Z

    .line 915057
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mTextOnlyPlace:Ljava/lang/String;

    iput-object v0, p0, LX/5RO;->c:Ljava/lang/String;

    .line 915058
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mIsCheckin:Z

    iput-boolean v0, p0, LX/5RO;->d:Z

    .line 915059
    iget-boolean v0, p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mXedLocation:Z

    iput-boolean v0, p0, LX/5RO;->e:Z

    .line 915060
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mImplicitLoc:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    iput-object v0, p0, LX/5RO;->f:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 915061
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerPlaces:LX/0Px;

    iput-object v0, p0, LX/5RO;->g:LX/0Px;

    .line 915062
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSessionId:Ljava/lang/String;

    iput-object v0, p0, LX/5RO;->h:Ljava/lang/String;

    .line 915063
    iget-object v0, p1, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->mLightweightPlacePickerSearchResultsId:Ljava/lang/String;

    iput-object v0, p0, LX/5RO;->i:Ljava/lang/String;

    .line 915064
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 915065
    iput-object v0, p0, LX/5RO;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 915066
    iput-object v0, p0, LX/5RO;->c:Ljava/lang/String;

    .line 915067
    return-void
.end method


# virtual methods
.method public final a()LX/5RO;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 915068
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5RO;->e:Z

    .line 915069
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5RO;->b:Z

    .line 915070
    iput-object v1, p0, LX/5RO;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 915071
    iput-object v1, p0, LX/5RO;->f:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 915072
    iput-object v1, p0, LX/5RO;->c:Ljava/lang/String;

    .line 915073
    return-object p0
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;
    .locals 1
    .param p1    # Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 915074
    invoke-direct {p0}, LX/5RO;->c()V

    .line 915075
    invoke-static {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    iput-object v0, p0, LX/5RO;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 915076
    return-object p0
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;)LX/5RO;
    .locals 1

    .prologue
    .line 915077
    invoke-direct {p0}, LX/5RO;->c()V

    .line 915078
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5RO;->e:Z

    .line 915079
    if-eqz p1, :cond_0

    .line 915080
    invoke-static {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    iput-object v0, p0, LX/5RO;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 915081
    :cond_0
    if-eqz p2, :cond_1

    .line 915082
    iput-object p2, p0, LX/5RO;->f:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 915083
    :cond_1
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/5RO;
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 915084
    invoke-direct {p0}, LX/5RO;->c()V

    .line 915085
    iput-object p1, p0, LX/5RO;->c:Ljava/lang/String;

    .line 915086
    return-object p0
.end method

.method public final b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;
    .locals 1

    .prologue
    .line 915087
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/5RO;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;)LX/5RO;

    .line 915088
    return-object p0
.end method

.method public final b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
    .locals 2

    .prologue
    .line 915089
    new-instance v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-direct {v0, p0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;-><init>(LX/5RO;)V

    return-object v0
.end method
