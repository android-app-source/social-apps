.class public LX/69x;
.super LX/3dG;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final b:LX/20j;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/facebook/graphql/model/GraphQLComment;


# direct methods
.method public constructor <init>(LX/20j;Lcom/facebook/graphql/model/GraphQLComment;Ljava/lang/String;)V
    .locals 2
    .param p2    # Lcom/facebook/graphql/model/GraphQLComment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058443
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    invoke-direct {p0, v0}, LX/3dG;-><init>([Ljava/lang/String;)V

    .line 1058444
    iput-object p1, p0, LX/69x;->b:LX/20j;

    .line 1058445
    iput-object p3, p0, LX/69x;->c:Ljava/lang/String;

    .line 1058446
    iput-object p2, p0, LX/69x;->d:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1058447
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2

    .prologue
    .line 1058448
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/69x;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/16z;->d(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1058449
    :cond_0
    :goto_0
    return-object p1

    .line 1058450
    :cond_1
    iget-object v0, p0, LX/69x;->b:LX/20j;

    iget-object v1, p0, LX/69x;->d:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0, p1, v1}, LX/20j;->c(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058451
    const-string v0, "AddCommentCacheVisitor"

    return-object v0
.end method
