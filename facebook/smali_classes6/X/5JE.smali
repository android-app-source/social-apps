.class public abstract LX/5JE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 896801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 896802
    iput-object p1, p0, LX/5JE;->a:Ljava/lang/String;

    .line 896803
    iput-object p2, p0, LX/5JE;->c:Ljava/lang/String;

    .line 896804
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 896800
    return-void
.end method

.method public abstract a(Landroid/content/Context;[B)Z
.end method

.method public final b(Landroid/content/Context;[B)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 896784
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 896785
    iget-object v1, p0, LX/5JE;->a:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v3

    .line 896786
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v0, p0, LX/5JE;->b:Ljava/io/File;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 896787
    const v0, 0x7fffffff

    :try_start_1
    invoke-static {v4, v3, p2, v0}, LX/5JH;->a(Ljava/io/OutputStream;Ljava/io/InputStream;[BI)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 896788
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 896789
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 896790
    :cond_0
    return-void

    .line 896791
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 896792
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_1
    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 896793
    :catch_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 896794
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    if-eqz v3, :cond_1

    if-eqz v2, :cond_3

    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    :cond_1
    :goto_3
    throw v0

    .line 896795
    :catch_2
    move-exception v4

    :try_start_8
    invoke-static {v1, v4}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 896796
    :catchall_2
    move-exception v0

    goto :goto_2

    .line 896797
    :cond_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_1

    .line 896798
    :catch_3
    move-exception v1

    invoke-static {v2, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_3

    .line 896799
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method
