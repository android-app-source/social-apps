.class public final LX/6Ur;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Uo;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/6Ur;


# instance fields
.field public final b:LX/6V0;

.field public final c:LX/0lC;

.field private final d:LX/03V;


# direct methods
.method public constructor <init>(LX/6V0;LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103046
    iput-object p1, p0, LX/6Ur;->b:LX/6V0;

    .line 1103047
    iput-object p2, p0, LX/6Ur;->c:LX/0lC;

    .line 1103048
    iput-object p3, p0, LX/6Ur;->d:LX/03V;

    .line 1103049
    return-void
.end method

.method public static a(LX/0QB;)LX/6Ur;
    .locals 6

    .prologue
    .line 1102996
    sget-object v0, LX/6Ur;->e:LX/6Ur;

    if-nez v0, :cond_1

    .line 1102997
    const-class v1, LX/6Ur;

    monitor-enter v1

    .line 1102998
    :try_start_0
    sget-object v0, LX/6Ur;->e:LX/6Ur;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1102999
    if-eqz v2, :cond_0

    .line 1103000
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1103001
    new-instance p0, LX/6Ur;

    invoke-static {v0}, LX/6V0;->b(LX/0QB;)LX/6V0;

    move-result-object v3

    check-cast v3, LX/6V0;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/6Ur;-><init>(LX/6V0;LX/0lC;LX/03V;)V

    .line 1103002
    move-object v0, p0

    .line 1103003
    sput-object v0, LX/6Ur;->e:LX/6Ur;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1103004
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1103005
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1103006
    :cond_1
    sget-object v0, LX/6Ur;->e:LX/6Ur;

    return-object v0

    .line 1103007
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1103008
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/ViewGroup;LX/162;Landroid/widget/RelativeLayout;I)V
    .locals 4

    .prologue
    .line 1103025
    if-eqz p3, :cond_4

    .line 1103026
    instance-of v0, p1, Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 1103027
    check-cast p1, Landroid/widget/ListView;

    .line 1103028
    iget-object v0, p0, LX/6Ur;->c:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v0

    .line 1103029
    const-string v1, "top_level_offending_relative_layout"

    iget-object v2, p0, LX/6Ur;->b:LX/6V0;

    sget-object v3, LX/6Uz;->NONE:LX/6Uz;

    invoke-virtual {v2, p3, v3}, LX/6V0;->b(Landroid/view/View;LX/6Uz;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1103030
    const-string v1, "offending_list_view"

    iget-object v2, p0, LX/6Ur;->b:LX/6V0;

    sget-object v3, LX/6Uz;->NONE:LX/6Uz;

    invoke-virtual {v2, p1, v3}, LX/6V0;->b(Landroid/view/View;LX/6Uz;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1103031
    const-string v1, "num_relative_layouts_in_listview_path"

    invoke-virtual {v0, v1, p4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1103032
    move-object v0, v0

    .line 1103033
    invoke-virtual {p2, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 1103034
    :cond_0
    return-void

    .line 1103035
    :cond_1
    instance-of v0, p1, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 1103036
    add-int/lit8 p4, p4, 0x1

    .line 1103037
    :cond_2
    :goto_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1103038
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1103039
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    .line 1103040
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, p2, p3, p4}, LX/6Ur;->a(Landroid/view/ViewGroup;LX/162;Landroid/widget/RelativeLayout;I)V

    .line 1103041
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1103042
    :cond_4
    instance-of v0, p1, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 1103043
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1103044
    const/4 p4, 0x1

    move-object p3, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103024
    const-string v0, "If you host a ListView inside a RelativeLayout, perf is gonna have a bad time."

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1103010
    iget-object v1, p0, LX/6Ur;->c:LX/0lC;

    invoke-virtual {v1}, LX/0lC;->f()LX/162;

    move-result-object v1

    .line 1103011
    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2, v0}, LX/6Ur;->a(Landroid/view/ViewGroup;LX/162;Landroid/widget/RelativeLayout;I)V

    .line 1103012
    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v2

    .line 1103013
    if-nez v2, :cond_0

    .line 1103014
    :goto_0
    return v0

    .line 1103015
    :cond_0
    const-string v0, "num_rule_breakers"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103016
    :try_start_0
    iget-object v0, p0, LX/6Ur;->c:LX/0lC;

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1103017
    :goto_1
    const-string v1, "rule_breakers"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103018
    const/4 v0, 0x1

    goto :goto_0

    .line 1103019
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1103020
    const-string v2, "Error serializing rule breakers: "

    .line 1103021
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1103022
    iget-object v3, p0, LX/6Ur;->d:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1103023
    sget-object v3, LX/6Ur;->a:Ljava/lang/Class;

    invoke-static {v3, v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103009
    const-string v0, "ListView child of RelativeLayout"

    return-object v0
.end method
