.class public final LX/5PJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/SurfaceHolder;


# instance fields
.field private final a:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Landroid/view/Surface;)V
    .locals 0

    .prologue
    .line 910540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 910541
    iput-object p1, p0, LX/5PJ;->a:Landroid/view/Surface;

    .line 910542
    return-void
.end method


# virtual methods
.method public final addCallback(Landroid/view/SurfaceHolder$Callback;)V
    .locals 0

    .prologue
    .line 910539
    return-void
.end method

.method public final getSurface()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 910538
    iget-object v0, p0, LX/5PJ;->a:Landroid/view/Surface;

    return-object v0
.end method

.method public final getSurfaceFrame()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 910527
    const/4 v0, 0x0

    return-object v0
.end method

.method public final isCreating()Z
    .locals 1

    .prologue
    .line 910537
    const/4 v0, 0x0

    return v0
.end method

.method public final lockCanvas()Landroid/graphics/Canvas;
    .locals 1

    .prologue
    .line 910536
    const/4 v0, 0x0

    return-object v0
.end method

.method public final lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    .locals 1

    .prologue
    .line 910535
    const/4 v0, 0x0

    return-object v0
.end method

.method public final removeCallback(Landroid/view/SurfaceHolder$Callback;)V
    .locals 0

    .prologue
    .line 910534
    return-void
.end method

.method public final setFixedSize(II)V
    .locals 0

    .prologue
    .line 910533
    return-void
.end method

.method public final setFormat(I)V
    .locals 0

    .prologue
    .line 910532
    return-void
.end method

.method public final setKeepScreenOn(Z)V
    .locals 0

    .prologue
    .line 910531
    return-void
.end method

.method public final setSizeFromLayout()V
    .locals 0

    .prologue
    .line 910530
    return-void
.end method

.method public final setType(I)V
    .locals 0

    .prologue
    .line 910529
    return-void
.end method

.method public final unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 910528
    return-void
.end method
