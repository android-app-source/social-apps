.class public final LX/6Fn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:Ljava/util/Map;

.field public final synthetic c:Ljava/io/File;

.field public final synthetic d:Ljava/io/File;

.field public final synthetic e:LX/6Ft;


# direct methods
.method public constructor <init>(LX/6Ft;Ljava/util/Map;Ljava/util/Map;Ljava/io/File;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1068937
    iput-object p1, p0, LX/6Fn;->e:LX/6Ft;

    iput-object p2, p0, LX/6Fn;->a:Ljava/util/Map;

    iput-object p3, p0, LX/6Fn;->b:Ljava/util/Map;

    iput-object p4, p0, LX/6Fn;->c:Ljava/io/File;

    iput-object p5, p0, LX/6Fn;->d:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 1068938
    iget-object v0, p0, LX/6Fn;->e:LX/6Ft;

    iget-object v0, v0, LX/6Ft;->o:LX/0W3;

    sget-wide v2, LX/0X5;->aN:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v1

    .line 1068939
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/6Fn;->a:Ljava/util/Map;

    :goto_0
    iget-object v2, p0, LX/6Fn;->e:LX/6Ft;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/6Fn;->c:Ljava/io/File;

    .line 1068940
    :goto_1
    new-instance v6, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyyMMdd-HHmmss"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v6, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1068941
    const-string v4, "Etc/UTC"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1068942
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v7

    .line 1068943
    :try_start_0
    iget-object v4, v2, LX/6Ft;->f:LX/44D;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, LX/44D;->a(I)LX/0Px;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 1068944
    :goto_2
    move-object v8, v4

    .line 1068945
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v4, 0x0

    move v5, v4

    :goto_3
    if-ge v5, v9, :cond_0

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/File;

    .line 1068946
    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    .line 1068947
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "debuglog-"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v13, Ljava/util/Date;

    invoke-direct {v13, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v13}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".txt"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1068948
    :try_start_1
    invoke-static {v1, v10}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;

    move-result-object v11

    .line 1068949
    invoke-static {v4, v11}, LX/6G3;->a(Ljava/io/File;LX/6FR;)V

    .line 1068950
    iget-object v4, v11, LX/6FR;->b:Landroid/net/Uri;

    move-object v4, v4

    .line 1068951
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7, v10, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1068952
    :goto_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 1068953
    :catch_0
    move-exception v4

    .line 1068954
    const-string v10, "BugReportWriter"

    const-string v11, "Unable to copy debug log file"

    invoke-static {v10, v11, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068955
    iget-object v4, v2, LX/6Ft;->q:LX/6GZ;

    sget-object v10, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v4, v10}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_4

    .line 1068956
    :cond_0
    move-object v1, v7

    .line 1068957
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1068958
    const/4 v0, 0x0

    return-object v0

    .line 1068959
    :cond_1
    iget-object v0, p0, LX/6Fn;->b:Ljava/util/Map;

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, LX/6Fn;->d:Ljava/io/File;

    goto :goto_1

    .line 1068960
    :catch_1
    move-exception v4

    .line 1068961
    iget-object v5, v2, LX/6Ft;->c:LX/03V;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, LX/6Ft;->u:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "getRecentLogFiles"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1068962
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1068963
    goto/16 :goto_2
.end method
