.class public LX/5r2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1010840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(D)F
    .locals 2

    .prologue
    .line 1010841
    double-to-float v0, p0

    invoke-static {v0}, LX/5r2;->a(F)F

    move-result v0

    return v0
.end method

.method public static a(F)F
    .locals 2

    .prologue
    .line 1010837
    const/4 v0, 0x1

    .line 1010838
    sget-object v1, LX/5ql;->a:Landroid/util/DisplayMetrics;

    move-object v1, v1

    .line 1010839
    invoke-static {v0, p0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public static b(F)F
    .locals 2

    .prologue
    .line 1010832
    const/4 v0, 0x2

    .line 1010833
    sget-object v1, LX/5ql;->a:Landroid/util/DisplayMetrics;

    move-object v1, v1

    .line 1010834
    invoke-static {v0, p0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public static c(F)F
    .locals 1

    .prologue
    .line 1010835
    sget-object v0, LX/5ql;->a:Landroid/util/DisplayMetrics;

    move-object v0, v0

    .line 1010836
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v0, p0, v0

    return v0
.end method
