.class public final LX/5bK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 957915
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 957916
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 957917
    :goto_0
    return v6

    .line 957918
    :cond_0
    const-string v9, "request_time"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 957919
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v1

    .line 957920
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_3

    .line 957921
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 957922
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 957923
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 957924
    const-string v9, "node"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 957925
    invoke-static {p0, p1}, LX/5bJ;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 957926
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 957927
    :cond_3
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 957928
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 957929
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 957930
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 957931
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_5
    move v0, v6

    move-wide v2, v4

    move v7, v6

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 957932
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 957933
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957934
    if-eqz v0, :cond_0

    .line 957935
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957936
    invoke-static {p0, v0, p2}, LX/5bJ;->a(LX/15i;ILX/0nX;)V

    .line 957937
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 957938
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 957939
    const-string v2, "request_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957940
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 957941
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 957942
    return-void
.end method
