.class public LX/6FH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6FJ;

.field private final b:LX/6FE;


# direct methods
.method public constructor <init>(LX/6FJ;LX/6FE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1067573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067574
    iput-object p1, p0, LX/6FH;->a:LX/6FJ;

    .line 1067575
    iput-object p2, p0, LX/6FH;->b:LX/6FE;

    .line 1067576
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067577
    const-string v0, "processPayment"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 7

    .prologue
    .line 1067578
    check-cast p1, Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;

    .line 1067579
    iget-object v0, p0, LX/6FH;->b:LX/6FE;

    .line 1067580
    iget-object v1, v0, LX/6FE;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    move-object v2, v1

    .line 1067581
    iget-object v0, p0, LX/6FH;->b:LX/6FE;

    invoke-virtual {v0}, LX/6FE;->c()V

    .line 1067582
    if-eqz v2, :cond_0

    invoke-static {v2}, LX/6FJ;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1067583
    :cond_0
    sget-object v0, LX/6Cy;->BROWSER_EXTENSION_MISSING_PAYMENT_METHOD:LX/6Cy;

    invoke-virtual {v0}, LX/6Cy;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1067584
    :goto_0
    return-void

    .line 1067585
    :cond_1
    iget-object v0, p0, LX/6FH;->a:LX/6FJ;

    sget-object v1, LX/6xg;->NMOR_BUSINESS_PLATFORM_COMMERCE:LX/6xg;

    .line 1067586
    const-string v3, "amount"

    invoke-virtual {p1, v3}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v3, v3

    .line 1067587
    const-string v4, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p1, v4}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v4, v4

    .line 1067588
    const/4 v5, 0x0

    new-instance v6, LX/6FG;

    invoke-direct {v6, p0, p1}, LX/6FG;-><init>(LX/6FH;Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;)V

    invoke-virtual/range {v0 .. v6}, LX/6FJ;->a(LX/6xg;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;Ljava/lang/String;LX/0m9;LX/6FF;)V

    goto :goto_0
.end method
