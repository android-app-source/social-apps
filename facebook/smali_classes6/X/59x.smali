.class public final LX/59x;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 33

    .prologue
    .line 855244
    const/16 v25, 0x0

    .line 855245
    const/16 v24, 0x0

    .line 855246
    const/16 v23, 0x0

    .line 855247
    const/16 v22, 0x0

    .line 855248
    const/16 v21, 0x0

    .line 855249
    const/16 v20, 0x0

    .line 855250
    const-wide/16 v18, 0x0

    .line 855251
    const-wide/16 v16, 0x0

    .line 855252
    const-wide/16 v14, 0x0

    .line 855253
    const-wide/16 v12, 0x0

    .line 855254
    const/4 v11, 0x0

    .line 855255
    const/4 v10, 0x0

    .line 855256
    const/4 v9, 0x0

    .line 855257
    const/4 v8, 0x0

    .line 855258
    const/4 v7, 0x0

    .line 855259
    const/4 v6, 0x0

    .line 855260
    const/4 v5, 0x0

    .line 855261
    const/4 v4, 0x0

    .line 855262
    const/4 v3, 0x0

    .line 855263
    const/4 v2, 0x0

    .line 855264
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_16

    .line 855265
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 855266
    const/4 v2, 0x0

    .line 855267
    :goto_0
    return v2

    .line 855268
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    if-eq v2, v0, :cond_b

    .line 855269
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 855270
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 855271
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 855272
    const-string v27, "cropped_area_image_height_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1

    .line 855273
    const/4 v2, 0x1

    .line 855274
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v26, v14

    move v14, v2

    goto :goto_1

    .line 855275
    :cond_1
    const-string v27, "cropped_area_image_width_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 855276
    const/4 v2, 0x1

    .line 855277
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    move/from16 v25, v13

    move v13, v2

    goto :goto_1

    .line 855278
    :cond_2
    const-string v27, "cropped_area_left_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 855279
    const/4 v2, 0x1

    .line 855280
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    move/from16 v24, v12

    move v12, v2

    goto :goto_1

    .line 855281
    :cond_3
    const-string v27, "cropped_area_top_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 855282
    const/4 v2, 0x1

    .line 855283
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v23, v11

    move v11, v2

    goto :goto_1

    .line 855284
    :cond_4
    const-string v27, "full_pano_height_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 855285
    const/4 v2, 0x1

    .line 855286
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move/from16 v22, v7

    move v7, v2

    goto :goto_1

    .line 855287
    :cond_5
    const-string v27, "full_pano_width_pixels"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 855288
    const/4 v2, 0x1

    .line 855289
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v15, v6

    move v6, v2

    goto/16 :goto_1

    .line 855290
    :cond_6
    const-string v27, "initial_view_heading_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 855291
    const/4 v2, 0x1

    .line 855292
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 855293
    :cond_7
    const-string v27, "initial_view_pitch_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 855294
    const/4 v2, 0x1

    .line 855295
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v20

    move v10, v2

    goto/16 :goto_1

    .line 855296
    :cond_8
    const-string v27, "initial_view_roll_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 855297
    const/4 v2, 0x1

    .line 855298
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v18

    move v9, v2

    goto/16 :goto_1

    .line 855299
    :cond_9
    const-string v27, "initial_view_vertical_fov_degrees"

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 855300
    const/4 v2, 0x1

    .line 855301
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v16

    move v8, v2

    goto/16 :goto_1

    .line 855302
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 855303
    :cond_b
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 855304
    if-eqz v14, :cond_c

    .line 855305
    const/4 v2, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1, v14}, LX/186;->a(III)V

    .line 855306
    :cond_c
    if-eqz v13, :cond_d

    .line 855307
    const/4 v2, 0x1

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1, v13}, LX/186;->a(III)V

    .line 855308
    :cond_d
    if-eqz v12, :cond_e

    .line 855309
    const/4 v2, 0x2

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1, v12}, LX/186;->a(III)V

    .line 855310
    :cond_e
    if-eqz v11, :cond_f

    .line 855311
    const/4 v2, 0x3

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1, v11}, LX/186;->a(III)V

    .line 855312
    :cond_f
    if-eqz v7, :cond_10

    .line 855313
    const/4 v2, 0x4

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1, v7}, LX/186;->a(III)V

    .line 855314
    :cond_10
    if-eqz v6, :cond_11

    .line 855315
    const/4 v2, 0x5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v6}, LX/186;->a(III)V

    .line 855316
    :cond_11
    if-eqz v3, :cond_12

    .line 855317
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 855318
    :cond_12
    if-eqz v10, :cond_13

    .line 855319
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 855320
    :cond_13
    if-eqz v9, :cond_14

    .line 855321
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 855322
    :cond_14
    if-eqz v8, :cond_15

    .line 855323
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 855324
    :cond_15
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_16
    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v29, v9

    move v9, v3

    move v3, v5

    move/from16 v30, v11

    move v11, v8

    move v8, v2

    move-wide/from16 v31, v14

    move/from16 v15, v20

    move/from16 v14, v30

    move-wide/from16 v20, v16

    move-wide/from16 v16, v12

    move v13, v10

    move/from16 v12, v29

    move v10, v4

    move-wide/from16 v4, v18

    move-wide/from16 v18, v31

    goto/16 :goto_1
.end method
