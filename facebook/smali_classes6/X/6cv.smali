.class public final enum LX/6cv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6cv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6cv;

.field public static final enum BOT:LX/6cv;

.field public static final enum PARTICIPANT:LX/6cv;

.field public static final enum REQUEST:LX/6cv;


# instance fields
.field public final dbValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1114831
    new-instance v0, LX/6cv;

    const-string v1, "PARTICIPANT"

    const-string v2, "PARTICIPANT"

    invoke-direct {v0, v1, v3, v2}, LX/6cv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6cv;->PARTICIPANT:LX/6cv;

    .line 1114832
    new-instance v0, LX/6cv;

    const-string v1, "BOT"

    const-string v2, "BOT"

    invoke-direct {v0, v1, v4, v2}, LX/6cv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6cv;->BOT:LX/6cv;

    .line 1114833
    new-instance v0, LX/6cv;

    const-string v1, "REQUEST"

    const-string v2, "REQUEST"

    invoke-direct {v0, v1, v5, v2}, LX/6cv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6cv;->REQUEST:LX/6cv;

    .line 1114834
    const/4 v0, 0x3

    new-array v0, v0, [LX/6cv;

    sget-object v1, LX/6cv;->PARTICIPANT:LX/6cv;

    aput-object v1, v0, v3

    sget-object v1, LX/6cv;->BOT:LX/6cv;

    aput-object v1, v0, v4

    sget-object v1, LX/6cv;->REQUEST:LX/6cv;

    aput-object v1, v0, v5

    sput-object v0, LX/6cv;->$VALUES:[LX/6cv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1114835
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1114836
    iput-object p3, p0, LX/6cv;->dbValue:Ljava/lang/String;

    .line 1114837
    return-void
.end method

.method public static fromDbValue(Ljava/lang/String;)LX/6cv;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1114838
    invoke-static {}, LX/6cv;->values()[LX/6cv;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1114839
    iget-object v4, v0, LX/6cv;->dbValue:Ljava/lang/String;

    invoke-static {v4, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1114840
    :goto_1
    return-object v0

    .line 1114841
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1114842
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/6cv;
    .locals 1

    .prologue
    .line 1114843
    const-class v0, LX/6cv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6cv;

    return-object v0
.end method

.method public static values()[LX/6cv;
    .locals 1

    .prologue
    .line 1114844
    sget-object v0, LX/6cv;->$VALUES:[LX/6cv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6cv;

    return-object v0
.end method
