.class public final LX/5ii;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 984549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v13, 0x1

    const/4 v1, 0x0

    const/4 v12, 0x0

    .line 984550
    new-instance v0, LX/186;

    const/16 v2, 0x80

    invoke-direct {v0, v2}, LX/186;-><init>(I)V

    .line 984551
    iget-object v2, p0, LX/5ii;->b:LX/0Px;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 984552
    iget-object v2, p0, LX/5ii;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 984553
    iget-object v2, p0, LX/5ii;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 984554
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 984555
    iget-wide v2, p0, LX/5ii;->a:J

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 984556
    invoke-virtual {v0, v13, v6}, LX/186;->b(II)V

    .line 984557
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 984558
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 984559
    const/4 v7, 0x4

    iget-wide v8, p0, LX/5ii;->e:J

    move-object v6, v0

    move-wide v10, v4

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IJJ)V

    .line 984560
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    .line 984561
    invoke-virtual {v0, v2}, LX/186;->d(I)V

    .line 984562
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 984563
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 984564
    new-instance v0, LX/15i;

    move-object v1, v2

    move-object v2, v12

    move-object v3, v12

    move v4, v13

    move-object v5, v12

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 984565
    new-instance v1, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-direct {v1, v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;-><init>(LX/15i;)V

    .line 984566
    return-object v1
.end method
