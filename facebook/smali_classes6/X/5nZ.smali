.class public final enum LX/5nZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5nZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5nZ;

.field public static final enum DISMISSED:LX/5nZ;

.field public static final enum EXPOSED:LX/5nZ;

.field public static final enum FRIENDS_PRIVACY:LX/5nZ;

.field public static final enum LEARN_MORE:LX/5nZ;

.field public static final enum MORE_OPTIONS:LX/5nZ;

.field public static final enum NAVIGATED_BACK:LX/5nZ;

.field public static final enum PUBLIC_PRIVACY:LX/5nZ;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1004381
    new-instance v0, LX/5nZ;

    const-string v1, "EXPOSED"

    const-string v2, "exposed"

    invoke-direct {v0, v1, v4, v2}, LX/5nZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nZ;->EXPOSED:LX/5nZ;

    .line 1004382
    new-instance v0, LX/5nZ;

    const-string v1, "DISMISSED"

    const-string v2, "dismissal"

    invoke-direct {v0, v1, v5, v2}, LX/5nZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nZ;->DISMISSED:LX/5nZ;

    .line 1004383
    new-instance v0, LX/5nZ;

    const-string v1, "LEARN_MORE"

    const-string v2, "learn_more"

    invoke-direct {v0, v1, v6, v2}, LX/5nZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nZ;->LEARN_MORE:LX/5nZ;

    .line 1004384
    new-instance v0, LX/5nZ;

    const-string v1, "PUBLIC_PRIVACY"

    const-string v2, "public_sticky"

    invoke-direct {v0, v1, v7, v2}, LX/5nZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nZ;->PUBLIC_PRIVACY:LX/5nZ;

    .line 1004385
    new-instance v0, LX/5nZ;

    const-string v1, "FRIENDS_PRIVACY"

    const-string v2, "friends_sticky"

    invoke-direct {v0, v1, v8, v2}, LX/5nZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nZ;->FRIENDS_PRIVACY:LX/5nZ;

    .line 1004386
    new-instance v0, LX/5nZ;

    const-string v1, "MORE_OPTIONS"

    const/4 v2, 0x5

    const-string v3, "selector"

    invoke-direct {v0, v1, v2, v3}, LX/5nZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nZ;->MORE_OPTIONS:LX/5nZ;

    .line 1004387
    new-instance v0, LX/5nZ;

    const-string v1, "NAVIGATED_BACK"

    const/4 v2, 0x6

    const-string v3, "back"

    invoke-direct {v0, v1, v2, v3}, LX/5nZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nZ;->NAVIGATED_BACK:LX/5nZ;

    .line 1004388
    const/4 v0, 0x7

    new-array v0, v0, [LX/5nZ;

    sget-object v1, LX/5nZ;->EXPOSED:LX/5nZ;

    aput-object v1, v0, v4

    sget-object v1, LX/5nZ;->DISMISSED:LX/5nZ;

    aput-object v1, v0, v5

    sget-object v1, LX/5nZ;->LEARN_MORE:LX/5nZ;

    aput-object v1, v0, v6

    sget-object v1, LX/5nZ;->PUBLIC_PRIVACY:LX/5nZ;

    aput-object v1, v0, v7

    sget-object v1, LX/5nZ;->FRIENDS_PRIVACY:LX/5nZ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5nZ;->MORE_OPTIONS:LX/5nZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5nZ;->NAVIGATED_BACK:LX/5nZ;

    aput-object v2, v0, v1

    sput-object v0, LX/5nZ;->$VALUES:[LX/5nZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004389
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004390
    iput-object p3, p0, LX/5nZ;->eventName:Ljava/lang/String;

    .line 1004391
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5nZ;
    .locals 1

    .prologue
    .line 1004392
    const-class v0, LX/5nZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5nZ;

    return-object v0
.end method

.method public static values()[LX/5nZ;
    .locals 1

    .prologue
    .line 1004393
    sget-object v0, LX/5nZ;->$VALUES:[LX/5nZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5nZ;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004394
    iget-object v0, p0, LX/5nZ;->eventName:Ljava/lang/String;

    return-object v0
.end method
