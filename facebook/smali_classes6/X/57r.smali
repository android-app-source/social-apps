.class public final LX/57r;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 842669
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 842670
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 842671
    :goto_0
    return v1

    .line 842672
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 842673
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 842674
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 842675
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 842676
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 842677
    const-string v3, "associated_video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 842678
    const/4 v2, 0x0

    .line 842679
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 842680
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 842681
    :goto_2
    move v0, v2

    .line 842682
    goto :goto_1

    .line 842683
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 842684
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 842685
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 842686
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 842687
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 842688
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 842689
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 842690
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_5

    if-eqz v4, :cond_5

    .line 842691
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 842692
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    .line 842693
    :cond_6
    const-string v5, "playable_url"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 842694
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 842695
    :cond_7
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 842696
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 842697
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 842698
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_8
    move v0, v2

    move v3, v2

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 842699
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 842700
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 842701
    if-eqz v0, :cond_2

    .line 842702
    const-string v1, "associated_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842703
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 842704
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 842705
    if-eqz v1, :cond_0

    .line 842706
    const-string p1, "id"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842707
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 842708
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 842709
    if-eqz v1, :cond_1

    .line 842710
    const-string p1, "playable_url"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 842711
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 842712
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 842713
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 842714
    return-void
.end method
