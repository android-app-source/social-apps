.class public final enum LX/6IG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6IG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6IG;

.field public static final enum LANDSCAPE:LX/6IG;

.field public static final enum PORTRAIT:LX/6IG;

.field public static final enum REVERSE_LANDSCAPE:LX/6IG;

.field public static final enum REVERSE_PORTRAIT:LX/6IG;


# instance fields
.field public final mReverseRotation:I

.field public final mRotation:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v2, 0x5a

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1073742
    new-instance v0, LX/6IG;

    const-string v1, "LANDSCAPE"

    invoke-direct {v0, v1, v4, v2, v4}, LX/6IG;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/6IG;->LANDSCAPE:LX/6IG;

    .line 1073743
    new-instance v0, LX/6IG;

    const-string v1, "PORTRAIT"

    invoke-direct {v0, v1, v5, v4, v2}, LX/6IG;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/6IG;->PORTRAIT:LX/6IG;

    .line 1073744
    new-instance v0, LX/6IG;

    const-string v1, "REVERSE_LANDSCAPE"

    const/16 v2, 0x10e

    const/16 v3, 0xb4

    invoke-direct {v0, v1, v6, v2, v3}, LX/6IG;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    .line 1073745
    new-instance v0, LX/6IG;

    const-string v1, "REVERSE_PORTRAIT"

    const/16 v2, 0xb4

    const/16 v3, 0x10e

    invoke-direct {v0, v1, v7, v2, v3}, LX/6IG;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/6IG;->REVERSE_PORTRAIT:LX/6IG;

    .line 1073746
    const/4 v0, 0x4

    new-array v0, v0, [LX/6IG;

    sget-object v1, LX/6IG;->LANDSCAPE:LX/6IG;

    aput-object v1, v0, v4

    sget-object v1, LX/6IG;->PORTRAIT:LX/6IG;

    aput-object v1, v0, v5

    sget-object v1, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    aput-object v1, v0, v6

    sget-object v1, LX/6IG;->REVERSE_PORTRAIT:LX/6IG;

    aput-object v1, v0, v7

    sput-object v0, LX/6IG;->$VALUES:[LX/6IG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1073747
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1073748
    iput p3, p0, LX/6IG;->mRotation:I

    .line 1073749
    iput p4, p0, LX/6IG;->mReverseRotation:I

    .line 1073750
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6IG;
    .locals 1

    .prologue
    .line 1073751
    const-class v0, LX/6IG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6IG;

    return-object v0
.end method

.method public static values()[LX/6IG;
    .locals 1

    .prologue
    .line 1073752
    sget-object v0, LX/6IG;->$VALUES:[LX/6IG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6IG;

    return-object v0
.end method
