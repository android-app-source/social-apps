.class public final LX/67O;
.super LX/389;
.source ""


# instance fields
.field public final synthetic a:LX/3Fm;

.field private final b:Ljava/lang/String;

.field private c:Z

.field private d:I

.field private e:I

.field private f:LX/67N;

.field private g:I


# direct methods
.method public constructor <init>(LX/3Fm;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1052908
    iput-object p1, p0, LX/67O;->a:LX/3Fm;

    invoke-direct {p0}, LX/389;-><init>()V

    .line 1052909
    const/4 v0, -0x1

    iput v0, p0, LX/67O;->d:I

    .line 1052910
    iput-object p2, p0, LX/67O;->b:Ljava/lang/String;

    .line 1052911
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1052903
    iget-object v0, p0, LX/67O;->a:LX/3Fm;

    .line 1052904
    iget-object v1, v0, LX/3Fm;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1052905
    invoke-virtual {p0}, LX/67O;->d()V

    .line 1052906
    invoke-static {v0}, LX/3Fm;->j(LX/3Fm;)V

    .line 1052907
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1052898
    iget-object v0, p0, LX/67O;->f:LX/67N;

    if-eqz v0, :cond_0

    .line 1052899
    iget-object v0, p0, LX/67O;->f:LX/67N;

    iget v1, p0, LX/67O;->g:I

    invoke-virtual {v0, v1, p1}, LX/67N;->a(II)V

    .line 1052900
    :goto_0
    return-void

    .line 1052901
    :cond_0
    iput p1, p0, LX/67O;->d:I

    .line 1052902
    const/4 v0, 0x0

    iput v0, p0, LX/67O;->e:I

    goto :goto_0
.end method

.method public final a(LX/67N;)V
    .locals 8

    .prologue
    .line 1052881
    iput-object p1, p0, LX/67O;->f:LX/67N;

    .line 1052882
    iget-object v0, p0, LX/67O;->b:Ljava/lang/String;

    .line 1052883
    iget v5, p1, LX/67N;->f:I

    add-int/lit8 v2, v5, 0x1

    iput v2, p1, LX/67N;->f:I

    .line 1052884
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1052885
    const-string v2, "routeId"

    invoke-virtual {v7, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052886
    const/4 v3, 0x3

    iget v4, p1, LX/67N;->e:I

    add-int/lit8 v2, v4, 0x1

    iput v2, p1, LX/67N;->e:I

    const/4 v6, 0x0

    move-object v2, p1

    invoke-static/range {v2 .. v7}, LX/67N;->a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 1052887
    move v0, v5

    .line 1052888
    iput v0, p0, LX/67O;->g:I

    .line 1052889
    iget-boolean v0, p0, LX/67O;->c:Z

    if-eqz v0, :cond_1

    .line 1052890
    iget v0, p0, LX/67O;->g:I

    invoke-virtual {p1, v0}, LX/67N;->c(I)V

    .line 1052891
    iget v0, p0, LX/67O;->d:I

    if-ltz v0, :cond_0

    .line 1052892
    iget v0, p0, LX/67O;->g:I

    iget v1, p0, LX/67O;->d:I

    invoke-virtual {p1, v0, v1}, LX/67N;->a(II)V

    .line 1052893
    const/4 v0, -0x1

    iput v0, p0, LX/67O;->d:I

    .line 1052894
    :cond_0
    iget v0, p0, LX/67O;->e:I

    if-eqz v0, :cond_1

    .line 1052895
    iget v0, p0, LX/67O;->g:I

    iget v1, p0, LX/67O;->e:I

    invoke-virtual {p1, v0, v1}, LX/67N;->b(II)V

    .line 1052896
    const/4 v0, 0x0

    iput v0, p0, LX/67O;->e:I

    .line 1052897
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1052862
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67O;->c:Z

    .line 1052863
    iget-object v0, p0, LX/67O;->f:LX/67N;

    if-eqz v0, :cond_0

    .line 1052864
    iget-object v0, p0, LX/67O;->f:LX/67N;

    iget v1, p0, LX/67O;->g:I

    invoke-virtual {v0, v1}, LX/67N;->c(I)V

    .line 1052865
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1052877
    iget-object v0, p0, LX/67O;->f:LX/67N;

    if-eqz v0, :cond_0

    .line 1052878
    iget-object v0, p0, LX/67O;->f:LX/67N;

    iget v1, p0, LX/67O;->g:I

    invoke-virtual {v0, v1, p1}, LX/67N;->b(II)V

    .line 1052879
    :goto_0
    return-void

    .line 1052880
    :cond_0
    iget v0, p0, LX/67O;->e:I

    add-int/2addr v0, p1

    iput v0, p0, LX/67O;->e:I

    goto :goto_0
.end method

.method public final c()V
    .locals 8

    .prologue
    .line 1052872
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/67O;->c:Z

    .line 1052873
    iget-object v0, p0, LX/67O;->f:LX/67N;

    if-eqz v0, :cond_0

    .line 1052874
    iget-object v0, p0, LX/67O;->f:LX/67N;

    iget v1, p0, LX/67O;->g:I

    const/4 v6, 0x0

    .line 1052875
    const/4 v3, 0x6

    iget v4, v0, LX/67N;->e:I

    add-int/lit8 v2, v4, 0x1

    iput v2, v0, LX/67N;->e:I

    move-object v2, v0

    move v5, v1

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/67N;->a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 1052876
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 8

    .prologue
    .line 1052866
    iget-object v0, p0, LX/67O;->f:LX/67N;

    if-eqz v0, :cond_0

    .line 1052867
    iget-object v0, p0, LX/67O;->f:LX/67N;

    iget v1, p0, LX/67O;->g:I

    const/4 v6, 0x0

    .line 1052868
    const/4 v3, 0x4

    iget v4, v0, LX/67N;->e:I

    add-int/lit8 v2, v4, 0x1

    iput v2, v0, LX/67N;->e:I

    move-object v2, v0

    move v5, v1

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/67N;->a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 1052869
    const/4 v0, 0x0

    iput-object v0, p0, LX/67O;->f:LX/67N;

    .line 1052870
    const/4 v0, 0x0

    iput v0, p0, LX/67O;->g:I

    .line 1052871
    :cond_0
    return-void
.end method
