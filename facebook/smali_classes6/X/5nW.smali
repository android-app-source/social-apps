.class public final enum LX/5nW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5nW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5nW;

.field public static final enum CUSTOM_SELECTION:LX/5nW;

.field public static final enum DISMISSED:LX/5nW;

.field public static final enum EXPOSED:LX/5nW;

.field public static final enum FRIENDS_PRIVACY:LX/5nW;

.field public static final enum HOLDOUT:LX/5nW;

.field public static final enum LEARN_MORE:LX/5nW;

.field public static final enum MORE_OPTIONS:LX/5nW;

.field public static final enum NAVIGATED_BACK:LX/5nW;

.field public static final enum ONLY_ME_PRIVACY:LX/5nW;

.field public static final enum POSTED:LX/5nW;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1004314
    new-instance v0, LX/5nW;

    const-string v1, "EXPOSED"

    const-string v2, "exposed"

    invoke-direct {v0, v1, v4, v2}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->EXPOSED:LX/5nW;

    .line 1004315
    new-instance v0, LX/5nW;

    const-string v1, "DISMISSED"

    const-string v2, "dismissal"

    invoke-direct {v0, v1, v5, v2}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->DISMISSED:LX/5nW;

    .line 1004316
    new-instance v0, LX/5nW;

    const-string v1, "LEARN_MORE"

    const-string v2, "learn_more"

    invoke-direct {v0, v1, v6, v2}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->LEARN_MORE:LX/5nW;

    .line 1004317
    new-instance v0, LX/5nW;

    const-string v1, "FRIENDS_PRIVACY"

    const-string v2, "friends_sticky"

    invoke-direct {v0, v1, v7, v2}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->FRIENDS_PRIVACY:LX/5nW;

    .line 1004318
    new-instance v0, LX/5nW;

    const-string v1, "ONLY_ME_PRIVACY"

    const-string v2, "only_me_sticky"

    invoke-direct {v0, v1, v8, v2}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->ONLY_ME_PRIVACY:LX/5nW;

    .line 1004319
    new-instance v0, LX/5nW;

    const-string v1, "MORE_OPTIONS"

    const/4 v2, 0x5

    const-string v3, "selector"

    invoke-direct {v0, v1, v2, v3}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->MORE_OPTIONS:LX/5nW;

    .line 1004320
    new-instance v0, LX/5nW;

    const-string v1, "NAVIGATED_BACK"

    const/4 v2, 0x6

    const-string v3, "back"

    invoke-direct {v0, v1, v2, v3}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->NAVIGATED_BACK:LX/5nW;

    .line 1004321
    new-instance v0, LX/5nW;

    const-string v1, "POSTED"

    const/4 v2, 0x7

    const-string v3, "posted"

    invoke-direct {v0, v1, v2, v3}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->POSTED:LX/5nW;

    .line 1004322
    new-instance v0, LX/5nW;

    const-string v1, "CUSTOM_SELECTION"

    const/16 v2, 0x8

    const-string v3, "custom_selection"

    invoke-direct {v0, v1, v2, v3}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->CUSTOM_SELECTION:LX/5nW;

    .line 1004323
    new-instance v0, LX/5nW;

    const-string v1, "HOLDOUT"

    const/16 v2, 0x9

    const-string v3, "holdout"

    invoke-direct {v0, v1, v2, v3}, LX/5nW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nW;->HOLDOUT:LX/5nW;

    .line 1004324
    const/16 v0, 0xa

    new-array v0, v0, [LX/5nW;

    sget-object v1, LX/5nW;->EXPOSED:LX/5nW;

    aput-object v1, v0, v4

    sget-object v1, LX/5nW;->DISMISSED:LX/5nW;

    aput-object v1, v0, v5

    sget-object v1, LX/5nW;->LEARN_MORE:LX/5nW;

    aput-object v1, v0, v6

    sget-object v1, LX/5nW;->FRIENDS_PRIVACY:LX/5nW;

    aput-object v1, v0, v7

    sget-object v1, LX/5nW;->ONLY_ME_PRIVACY:LX/5nW;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5nW;->MORE_OPTIONS:LX/5nW;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5nW;->NAVIGATED_BACK:LX/5nW;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5nW;->POSTED:LX/5nW;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5nW;->CUSTOM_SELECTION:LX/5nW;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5nW;->HOLDOUT:LX/5nW;

    aput-object v2, v0, v1

    sput-object v0, LX/5nW;->$VALUES:[LX/5nW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004325
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004326
    iput-object p3, p0, LX/5nW;->eventName:Ljava/lang/String;

    .line 1004327
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5nW;
    .locals 1

    .prologue
    .line 1004328
    const-class v0, LX/5nW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5nW;

    return-object v0
.end method

.method public static values()[LX/5nW;
    .locals 1

    .prologue
    .line 1004329
    sget-object v0, LX/5nW;->$VALUES:[LX/5nW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5nW;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004330
    iget-object v0, p0, LX/5nW;->eventName:Ljava/lang/String;

    return-object v0
.end method
