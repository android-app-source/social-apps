.class public LX/5OU;
.super LX/0xh;
.source ""


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/View;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public final synthetic g:Lcom/facebook/fbui/popover/PopoverViewFlipper;


# direct methods
.method public constructor <init>(Lcom/facebook/fbui/popover/PopoverViewFlipper;)V
    .locals 0

    .prologue
    .line 908893
    iput-object p1, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 908855
    iget-object v0, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 908856
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 908857
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 908858
    iget-object v2, p0, LX/5OU;->a:Landroid/view/View;

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    .line 908859
    iget-object v2, p0, LX/5OU;->b:Landroid/view/View;

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    .line 908860
    iget-object v0, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 908861
    iget-object v1, p0, LX/5OU;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v0

    iput v1, p0, LX/5OU;->e:I

    .line 908862
    iget-object v1, p0, LX/5OU;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/5OU;->f:I

    .line 908863
    iget-object v0, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 908864
    iget-object v1, p0, LX/5OU;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    iput v1, p0, LX/5OU;->c:I

    .line 908865
    iget-object v1, p0, LX/5OU;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/5OU;->d:I

    .line 908866
    return-void
.end method

.method public final a(LX/0wd;)V
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 908867
    iget-object v0, p0, LX/5OU;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5OU;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 908868
    :cond_0
    :goto_0
    return-void

    .line 908869
    :cond_1
    iget-object v6, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    .line 908870
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v7, v0

    .line 908871
    iget v0, p0, LX/5OU;->c:I

    iget v1, p0, LX/5OU;->d:I

    add-int/2addr v0, v1

    .line 908872
    sget-object v1, LX/5OR;->a:[I

    iget-object v2, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    iget-object v2, v2, Lcom/facebook/fbui/popover/PopoverViewFlipper;->n:LX/5OV;

    invoke-virtual {v2}, LX/5OV;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 908873
    :goto_1
    iget v0, p0, LX/5OU;->f:I

    iget v1, p0, LX/5OU;->e:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, v7

    .line 908874
    iget v1, p0, LX/5OU;->e:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 908875
    invoke-virtual {v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 908876
    iget v0, p0, LX/5OU;->d:I

    iget v1, p0, LX/5OU;->c:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    mul-float/2addr v0, v7

    .line 908877
    iget v1, p0, LX/5OU;->c:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 908878
    float-to-int v0, v0

    .line 908879
    invoke-virtual {v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 908880
    iget-object v1, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    .line 908881
    invoke-static {v1, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a$redex0(Lcom/facebook/fbui/popover/PopoverViewFlipper;I)V

    .line 908882
    invoke-virtual {v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->requestLayout()V

    goto :goto_0

    .line 908883
    :pswitch_0
    iget-object v8, p0, LX/5OU;->a:Landroid/view/View;

    float-to-double v0, v7

    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setAlpha(F)V

    .line 908884
    iget-object v0, p0, LX/5OU;->b:Landroid/view/View;

    sub-float v1, v9, v7

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1

    .line 908885
    :pswitch_1
    iget-object v0, p0, LX/5OU;->a:Landroid/view/View;

    neg-float v1, v7

    iget v2, p0, LX/5OU;->e:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 908886
    iget-object v0, p0, LX/5OU;->b:Landroid/view/View;

    sub-float v1, v9, v7

    iget v2, p0, LX/5OU;->f:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1

    .line 908887
    :pswitch_2
    iget-object v0, p0, LX/5OU;->a:Landroid/view/View;

    iget v1, p0, LX/5OU;->e:I

    int-to-float v1, v1

    mul-float/2addr v1, v7

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 908888
    iget-object v0, p0, LX/5OU;->b:Landroid/view/View;

    sub-float v1, v7, v9

    iget v2, p0, LX/5OU;->f:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1

    .line 908889
    :pswitch_3
    iget-object v1, p0, LX/5OU;->a:Landroid/view/View;

    neg-float v2, v7

    int-to-float v3, v0

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 908890
    iget-object v1, p0, LX/5OU;->b:Landroid/view/View;

    sub-float v2, v9, v7

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_1

    .line 908891
    :pswitch_4
    iget-object v1, p0, LX/5OU;->a:Landroid/view/View;

    int-to-float v2, v0

    mul-float/2addr v2, v7

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 908892
    iget-object v1, p0, LX/5OU;->b:Landroid/view/View;

    sub-float v2, v7, v9

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 908852
    iput-object p1, p0, LX/5OU;->a:Landroid/view/View;

    .line 908853
    iput-object p2, p0, LX/5OU;->b:Landroid/view/View;

    .line 908854
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 908839
    iget-object v1, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 908840
    iget-object v0, p0, LX/5OU;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5OU;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 908841
    :cond_0
    :goto_0
    return-void

    .line 908842
    :cond_1
    iget-object v0, p0, LX/5OU;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 908843
    iget-object v0, p0, LX/5OU;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 908844
    iput-object v2, p0, LX/5OU;->a:Landroid/view/View;

    .line 908845
    iput-object v2, p0, LX/5OU;->b:Landroid/view/View;

    goto :goto_0
.end method

.method public final c(LX/0wd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 908846
    iget-object v1, p0, LX/5OU;->g:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-static {v1}, LX/0wc;->a(Landroid/view/View;)V

    .line 908847
    iget-object v0, p0, LX/5OU;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5OU;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 908848
    :cond_0
    :goto_0
    return-void

    .line 908849
    :cond_1
    iget-object v0, p0, LX/5OU;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 908850
    iget-object v0, p0, LX/5OU;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 908851
    invoke-virtual {p0}, LX/5OU;->a()V

    goto :goto_0
.end method
