.class public final LX/5s8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# instance fields
.field public final synthetic a:LX/5s9;

.field private volatile b:Z

.field private c:Z


# direct methods
.method public constructor <init>(LX/5s9;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1012502
    iput-object p1, p0, LX/5s8;->a:LX/5s9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1012503
    iput-boolean v0, p0, LX/5s8;->b:Z

    .line 1012504
    iput-boolean v0, p0, LX/5s8;->c:Z

    return-void
.end method

.method public synthetic constructor <init>(LX/5s9;B)V
    .locals 0

    .prologue
    .line 1012501
    invoke-direct {p0, p1}, LX/5s8;-><init>(LX/5s9;)V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 1012499
    invoke-static {}, LX/5r6;->a()LX/5r6;

    move-result-object v0

    sget-object v1, LX/5r4;->TIMERS_EVENTS:LX/5r4;

    iget-object v2, p0, LX/5s8;->a:LX/5s9;

    iget-object v2, v2, LX/5s9;->m:LX/5s8;

    invoke-virtual {v0, v1, v2}, LX/5r6;->a(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 1012500
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1012505
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5s8;->c:Z

    .line 1012506
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1012476
    iget-boolean v0, p0, LX/5s8;->b:Z

    if-nez v0, :cond_0

    .line 1012477
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5s8;->b:Z

    .line 1012478
    invoke-direct {p0}, LX/5s8;->d()V

    .line 1012479
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1012494
    iget-boolean v0, p0, LX/5s8;->b:Z

    if-eqz v0, :cond_0

    .line 1012495
    :goto_0
    return-void

    .line 1012496
    :cond_0
    iget-object v0, p0, LX/5s8;->a:LX/5s9;

    iget-object v0, v0, LX/5s9;->d:LX/5pY;

    invoke-virtual {v0}, LX/5pX;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1012497
    invoke-virtual {p0}, LX/5s8;->b()V

    goto :goto_0

    .line 1012498
    :cond_1
    iget-object v0, p0, LX/5s8;->a:LX/5s9;

    iget-object v0, v0, LX/5s9;->d:LX/5pY;

    new-instance v1, Lcom/facebook/react/uimanager/events/EventDispatcher$ScheduleDispatchFrameCallback$1;

    invoke-direct {v1, p0}, Lcom/facebook/react/uimanager/events/EventDispatcher$ScheduleDispatchFrameCallback$1;-><init>(LX/5s8;)V

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final doFrame(J)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x2000

    .line 1012480
    invoke-static {}, LX/5pe;->b()V

    .line 1012481
    iget-boolean v0, p0, LX/5s8;->c:Z

    if-eqz v0, :cond_1

    .line 1012482
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5s8;->b:Z

    .line 1012483
    :goto_0
    const-string v0, "ScheduleDispatchFrameCallback"

    invoke-static {v4, v5, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1012484
    :try_start_0
    iget-object v0, p0, LX/5s8;->a:LX/5s9;

    invoke-static {v0}, LX/5s9;->g(LX/5s9;)V

    .line 1012485
    iget-object v0, p0, LX/5s8;->a:LX/5s9;

    iget v0, v0, LX/5s9;->k:I

    if-lez v0, :cond_0

    iget-object v0, p0, LX/5s8;->a:LX/5s9;

    iget-boolean v0, v0, LX/5s9;->o:Z

    if-nez v0, :cond_0

    .line 1012486
    iget-object v0, p0, LX/5s8;->a:LX/5s9;

    const/4 v1, 0x1

    .line 1012487
    iput-boolean v1, v0, LX/5s9;->o:Z

    .line 1012488
    const-wide/16 v0, 0x2000

    const-string v2, "ScheduleDispatchFrameCallback"

    iget-object v3, p0, LX/5s8;->a:LX/5s9;

    iget v3, v3, LX/5s9;->p:I

    invoke-static {v0, v1, v2, v3}, LX/018;->d(JLjava/lang/String;I)V

    .line 1012489
    iget-object v0, p0, LX/5s8;->a:LX/5s9;

    iget-object v0, v0, LX/5s9;->d:LX/5pY;

    iget-object v1, p0, LX/5s8;->a:LX/5s9;

    iget-object v1, v1, LX/5s9;->g:Lcom/facebook/react/uimanager/events/EventDispatcher$DispatchEventsRunnable;

    invoke-virtual {v0, v1}, LX/5pX;->c(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1012490
    :cond_0
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1012491
    return-void

    .line 1012492
    :cond_1
    invoke-direct {p0}, LX/5s8;->d()V

    goto :goto_0

    .line 1012493
    :catchall_0
    move-exception v0

    invoke-static {v4, v5}, LX/018;->a(J)V

    throw v0
.end method
