.class public final LX/5GU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 45

    .prologue
    .line 887982
    const/16 v38, 0x0

    .line 887983
    const/16 v37, 0x0

    .line 887984
    const/16 v36, 0x0

    .line 887985
    const-wide/16 v34, 0x0

    .line 887986
    const/16 v33, 0x0

    .line 887987
    const/16 v32, 0x0

    .line 887988
    const-wide/16 v30, 0x0

    .line 887989
    const/16 v29, 0x0

    .line 887990
    const/16 v28, 0x0

    .line 887991
    const/16 v27, 0x0

    .line 887992
    const/16 v26, 0x0

    .line 887993
    const/16 v25, 0x0

    .line 887994
    const/16 v24, 0x0

    .line 887995
    const-wide/16 v22, 0x0

    .line 887996
    const/16 v21, 0x0

    .line 887997
    const/16 v20, 0x0

    .line 887998
    const/16 v19, 0x0

    .line 887999
    const/16 v18, 0x0

    .line 888000
    const-wide/16 v16, 0x0

    .line 888001
    const/4 v15, 0x0

    .line 888002
    const/4 v14, 0x0

    .line 888003
    const/4 v13, 0x0

    .line 888004
    const/4 v12, 0x0

    .line 888005
    const-wide/16 v10, 0x0

    .line 888006
    const/4 v9, 0x0

    .line 888007
    const/4 v8, 0x0

    .line 888008
    const/4 v7, 0x0

    .line 888009
    const/4 v6, 0x0

    .line 888010
    const/4 v5, 0x0

    .line 888011
    const/4 v4, 0x0

    .line 888012
    const/4 v3, 0x0

    .line 888013
    const/4 v2, 0x0

    .line 888014
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v39

    sget-object v40, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    if-eq v0, v1, :cond_22

    .line 888015
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 888016
    const/4 v2, 0x0

    .line 888017
    :goto_0
    return v2

    .line 888018
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_19

    .line 888019
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 888020
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 888021
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 888022
    const-string v6, "canceled_endscreen_body"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 888023
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v41, v2

    goto :goto_1

    .line 888024
    :cond_1
    const-string v6, "canceled_endscreen_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 888025
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v40, v2

    goto :goto_1

    .line 888026
    :cond_2
    const-string v6, "canceled_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 888027
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v39, v2

    goto :goto_1

    .line 888028
    :cond_3
    const-string v6, "client_latency_buffer"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 888029
    const/4 v2, 0x1

    .line 888030
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 888031
    :cond_4
    const-string v6, "expiration_endscreen_body"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 888032
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v38, v2

    goto :goto_1

    .line 888033
    :cond_5
    const-string v6, "expiration_endscreen_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 888034
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 888035
    :cond_6
    const-string v6, "expiration_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 888036
    const/4 v2, 0x1

    .line 888037
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v14, v2

    move-wide/from16 v36, v6

    goto/16 :goto_1

    .line 888038
    :cond_7
    const-string v6, "expiration_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 888039
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v34, v2

    goto/16 :goto_1

    .line 888040
    :cond_8
    const-string v6, "formatted_start_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 888041
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 888042
    :cond_9
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 888043
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 888044
    :cond_a
    const-string v6, "is_rescheduled"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 888045
    const/4 v2, 0x1

    .line 888046
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v13, v2

    move/from16 v31, v6

    goto/16 :goto_1

    .line 888047
    :cond_b
    const-string v6, "is_scheduled"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 888048
    const/4 v2, 0x1

    .line 888049
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v12, v2

    move/from16 v30, v6

    goto/16 :goto_1

    .line 888050
    :cond_c
    const-string v6, "is_viewer_subscribed"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 888051
    const/4 v2, 0x1

    .line 888052
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v2

    move/from16 v27, v6

    goto/16 :goto_1

    .line 888053
    :cond_d
    const-string v6, "lobby_open_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 888054
    const/4 v2, 0x1

    .line 888055
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v10, v2

    move-wide/from16 v28, v6

    goto/16 :goto_1

    .line 888056
    :cond_e
    const-string v6, "rescheduled_endscreen_body"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 888057
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 888058
    :cond_f
    const-string v6, "rescheduled_endscreen_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 888059
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 888060
    :cond_10
    const-string v6, "rescheduled_heading"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 888061
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 888062
    :cond_11
    const-string v6, "running_late_lobby_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 888063
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 888064
    :cond_12
    const-string v6, "running_late_start_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 888065
    const/4 v2, 0x1

    .line 888066
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v9, v2

    move-wide/from16 v22, v6

    goto/16 :goto_1

    .line 888067
    :cond_13
    const-string v6, "running_late_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 888068
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 888069
    :cond_14
    const-string v6, "schedule_background_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 888070
    invoke-static/range {p0 .. p1}, LX/5GR;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 888071
    :cond_15
    const-string v6, "schedule_custom_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 888072
    invoke-static/range {p0 .. p1}, LX/5GS;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 888073
    :cond_16
    const-string v6, "schedule_profile_image"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 888074
    invoke-static/range {p0 .. p1}, LX/5GT;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 888075
    :cond_17
    const-string v6, "start_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 888076
    const/4 v2, 0x1

    .line 888077
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide/from16 v16, v6

    goto/16 :goto_1

    .line 888078
    :cond_18
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 888079
    :cond_19
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 888080
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888081
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888082
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888083
    if-eqz v3, :cond_1a

    .line 888084
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 888085
    :cond_1a
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888086
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888087
    if-eqz v14, :cond_1b

    .line 888088
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v36

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 888089
    :cond_1b
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888090
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888091
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888092
    if-eqz v13, :cond_1c

    .line 888093
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 888094
    :cond_1c
    if-eqz v12, :cond_1d

    .line 888095
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 888096
    :cond_1d
    if-eqz v11, :cond_1e

    .line 888097
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 888098
    :cond_1e
    if-eqz v10, :cond_1f

    .line 888099
    const/16 v3, 0xd

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v28

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 888100
    :cond_1f
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888101
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888102
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888103
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888104
    if-eqz v9, :cond_20

    .line 888105
    const/16 v3, 0x12

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v22

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 888106
    :cond_20
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888107
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888108
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 888109
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 888110
    if-eqz v8, :cond_21

    .line 888111
    const/16 v3, 0x17

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 888112
    :cond_21
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_22
    move/from16 v39, v36

    move/from16 v40, v37

    move/from16 v41, v38

    move-wide/from16 v36, v30

    move/from16 v38, v33

    move/from16 v30, v25

    move/from16 v31, v26

    move/from16 v33, v28

    move/from16 v25, v20

    move/from16 v26, v21

    move/from16 v20, v15

    move/from16 v21, v18

    move/from16 v18, v13

    move v15, v12

    move v13, v7

    move v12, v6

    move-wide/from16 v42, v10

    move v11, v5

    move v10, v4

    move-wide/from16 v4, v34

    move/from16 v34, v29

    move/from16 v35, v32

    move/from16 v32, v27

    move-wide/from16 v28, v22

    move/from16 v27, v24

    move-wide/from16 v22, v16

    move-wide/from16 v16, v42

    move/from16 v24, v19

    move/from16 v19, v14

    move v14, v8

    move v8, v2

    move/from16 v44, v3

    move v3, v9

    move/from16 v9, v44

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 888113
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 888114
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888115
    if-eqz v0, :cond_0

    .line 888116
    const-string v1, "canceled_endscreen_body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888117
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888118
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888119
    if-eqz v0, :cond_1

    .line 888120
    const-string v1, "canceled_endscreen_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888121
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888122
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888123
    if-eqz v0, :cond_2

    .line 888124
    const-string v1, "canceled_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888125
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888126
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 888127
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    .line 888128
    const-string v2, "client_latency_buffer"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888129
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 888130
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888131
    if-eqz v0, :cond_4

    .line 888132
    const-string v1, "expiration_endscreen_body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888133
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888134
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888135
    if-eqz v0, :cond_5

    .line 888136
    const-string v1, "expiration_endscreen_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888137
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888138
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 888139
    cmp-long v2, v0, v4

    if-eqz v2, :cond_6

    .line 888140
    const-string v2, "expiration_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888141
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 888142
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888143
    if-eqz v0, :cond_7

    .line 888144
    const-string v1, "expiration_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888145
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888146
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888147
    if-eqz v0, :cond_8

    .line 888148
    const-string v1, "formatted_start_time"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888149
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888150
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888151
    if-eqz v0, :cond_9

    .line 888152
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888153
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888154
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 888155
    if-eqz v0, :cond_a

    .line 888156
    const-string v1, "is_rescheduled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888157
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 888158
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 888159
    if-eqz v0, :cond_b

    .line 888160
    const-string v1, "is_scheduled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888161
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 888162
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 888163
    if-eqz v0, :cond_c

    .line 888164
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888165
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 888166
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 888167
    cmp-long v2, v0, v4

    if-eqz v2, :cond_d

    .line 888168
    const-string v2, "lobby_open_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888169
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 888170
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888171
    if-eqz v0, :cond_e

    .line 888172
    const-string v1, "rescheduled_endscreen_body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888173
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888174
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888175
    if-eqz v0, :cond_f

    .line 888176
    const-string v1, "rescheduled_endscreen_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888177
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888178
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888179
    if-eqz v0, :cond_10

    .line 888180
    const-string v1, "rescheduled_heading"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888181
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888182
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888183
    if-eqz v0, :cond_11

    .line 888184
    const-string v1, "running_late_lobby_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888185
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888186
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 888187
    cmp-long v2, v0, v4

    if-eqz v2, :cond_12

    .line 888188
    const-string v2, "running_late_start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888189
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 888190
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888191
    if-eqz v0, :cond_13

    .line 888192
    const-string v1, "running_late_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888193
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888194
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888195
    if-eqz v0, :cond_15

    .line 888196
    const-string v1, "schedule_background_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888197
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 888198
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 888199
    if-eqz v1, :cond_14

    .line 888200
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888201
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888202
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 888203
    :cond_15
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888204
    if-eqz v0, :cond_16

    .line 888205
    const-string v1, "schedule_custom_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888206
    invoke-static {p0, v0, p2}, LX/5GS;->a(LX/15i;ILX/0nX;)V

    .line 888207
    :cond_16
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888208
    if-eqz v0, :cond_18

    .line 888209
    const-string v1, "schedule_profile_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888210
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 888211
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 888212
    if-eqz v1, :cond_17

    .line 888213
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888214
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888215
    :cond_17
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 888216
    :cond_18
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 888217
    cmp-long v2, v0, v4

    if-eqz v2, :cond_19

    .line 888218
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888219
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 888220
    :cond_19
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 888221
    return-void
.end method
