.class public final LX/5au;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 956376
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 956377
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 956378
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 956379
    invoke-static {p0, p1}, LX/5au;->b(LX/15w;LX/186;)I

    move-result v1

    .line 956380
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 956381
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 956382
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 956383
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 956384
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5au;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 956385
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 956386
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 956387
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 956388
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 956389
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 956390
    :goto_0
    return v1

    .line 956391
    :cond_0
    const-string v8, "length"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 956392
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v5, v3

    move v3, v2

    .line 956393
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 956394
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 956395
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 956396
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 956397
    const-string v8, "entity"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 956398
    invoke-static {p0, p1}, LX/5at;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 956399
    :cond_2
    const-string v8, "offset"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 956400
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 956401
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 956402
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 956403
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 956404
    if-eqz v3, :cond_5

    .line 956405
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 956406
    :cond_5
    if-eqz v0, :cond_6

    .line 956407
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 956408
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 956409
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 956410
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 956411
    if-eqz v0, :cond_0

    .line 956412
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956413
    invoke-static {p0, v0, p2}, LX/5at;->a(LX/15i;ILX/0nX;)V

    .line 956414
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 956415
    if-eqz v0, :cond_1

    .line 956416
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956417
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 956418
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 956419
    if-eqz v0, :cond_2

    .line 956420
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 956421
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 956422
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 956423
    return-void
.end method
