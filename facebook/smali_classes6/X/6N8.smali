.class public LX/6N8;
.super LX/6Mz;
.source ""


# instance fields
.field private final d:LX/6N3;

.field private final e:LX/6Op;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2J0;LX/6N3;LX/6Op;LX/0Or;Landroid/database/Cursor;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/annotations/IsPhoneEmailSourcesUploadEnabled;
        .end annotation
    .end param
    .param p5    # Landroid/database/Cursor;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2J0;",
            "LX/6N3;",
            "LX/6Op;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1082452
    invoke-direct {p0, p1, p5}, LX/6Mz;-><init>(LX/2J0;Landroid/database/Cursor;)V

    .line 1082453
    iput-object p2, p0, LX/6N8;->d:LX/6N3;

    .line 1082454
    iput-object p3, p0, LX/6N8;->e:LX/6Op;

    .line 1082455
    iput-object p4, p0, LX/6N8;->f:LX/0Or;

    .line 1082456
    return-void
.end method


# virtual methods
.method public final a(LX/6NG;)V
    .locals 6

    .prologue
    .line 1082575
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082576
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1082577
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data2"

    invoke-static {v0, v1}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    .line 1082578
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data3"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082579
    const/4 v1, 0x0

    .line 1082580
    iget-object v0, p0, LX/6N8;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1082581
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "account_type"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082582
    :goto_0
    new-instance v1, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/facebook/contacts/model/PhonebookPhoneNumber;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, LX/6NG;->a(Lcom/facebook/contacts/model/PhonebookPhoneNumber;)LX/6NG;

    .line 1082583
    :goto_1
    return-void

    .line 1082584
    :cond_0
    iget-object v0, p0, LX/6N8;->d:LX/6N3;

    .line 1082585
    iget-object v1, p1, LX/6NG;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1082586
    const-string v2, "phone"

    invoke-virtual {v0, v1, v2}, LX/6N3;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(LX/6NG;)V
    .locals 6

    .prologue
    .line 1082563
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082564
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1082565
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data2"

    invoke-static {v0, v1}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    .line 1082566
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data3"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082567
    const/4 v1, 0x0

    .line 1082568
    iget-object v0, p0, LX/6N8;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1082569
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "account_type"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082570
    :goto_0
    new-instance v1, Lcom/facebook/contacts/model/PhonebookEmailAddress;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/facebook/contacts/model/PhonebookEmailAddress;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, LX/6NG;->a(Lcom/facebook/contacts/model/PhonebookEmailAddress;)LX/6NG;

    .line 1082571
    :goto_1
    return-void

    .line 1082572
    :cond_0
    iget-object v0, p0, LX/6N8;->d:LX/6N3;

    .line 1082573
    iget-object v1, p1, LX/6NG;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1082574
    const-string v2, "email"

    invoke-virtual {v0, v1, v2}, LX/6N3;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final c(LX/6NG;)V
    .locals 10

    .prologue
    .line 1082544
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082545
    iget-object v1, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v1, v2}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1082546
    iget-object v2, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data3"

    invoke-static {v2, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082547
    iget-object v3, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v4, "data4"

    invoke-static {v3, v4}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1082548
    iget-object v4, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v5, "data5"

    invoke-static {v4, v5}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082549
    iget-object v5, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v6, "data6"

    invoke-static {v5, v6}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1082550
    iget-object v6, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v7, "data7"

    invoke-static {v6, v7}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1082551
    iget-object v7, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v8, "data8"

    invoke-static {v7, v8}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1082552
    iget-object v8, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v9, "data9"

    invoke-static {v8, v9}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1082553
    iput-object v0, p1, LX/6NG;->b:Ljava/lang/String;

    .line 1082554
    iput-object v1, p1, LX/6NG;->c:Ljava/lang/String;

    .line 1082555
    iput-object v2, p1, LX/6NG;->d:Ljava/lang/String;

    .line 1082556
    iput-object v3, p1, LX/6NG;->e:Ljava/lang/String;

    .line 1082557
    iput-object v4, p1, LX/6NG;->f:Ljava/lang/String;

    .line 1082558
    iput-object v5, p1, LX/6NG;->g:Ljava/lang/String;

    .line 1082559
    iput-object v6, p1, LX/6NG;->h:Ljava/lang/String;

    .line 1082560
    iput-object v7, p1, LX/6NG;->i:Ljava/lang/String;

    .line 1082561
    iput-object v8, p1, LX/6NG;->j:Ljava/lang/String;

    .line 1082562
    return-void
.end method

.method public final d(LX/6NG;)V
    .locals 2

    .prologue
    .line 1082539
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data14"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082540
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1082541
    :goto_0
    iput-boolean v0, p1, LX/6NG;->k:Z

    .line 1082542
    return-void

    .line 1082543
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(LX/6NG;)V
    .locals 2

    .prologue
    .line 1082534
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082535
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1082536
    :goto_0
    iput-boolean v0, p1, LX/6NG;->l:Z

    .line 1082537
    return-void

    .line 1082538
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(LX/6NG;)V
    .locals 6

    .prologue
    .line 1082524
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1082525
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1082526
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v0, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 1082527
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data3"

    invoke-static {v0, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1082528
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v4, "data5"

    invoke-static {v0, v4}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082529
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v5, "data6"

    invoke-static {v0, v5}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1082530
    new-instance v0, Lcom/facebook/contacts/model/PhonebookInstantMessaging;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/contacts/model/PhonebookInstantMessaging;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082531
    if-eqz v0, :cond_0

    .line 1082532
    iget-object v1, p1, LX/6NG;->o:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1082533
    :cond_0
    return-void
.end method

.method public final g(LX/6NG;)V
    .locals 4

    .prologue
    .line 1082587
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082588
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1082589
    iget-object v1, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v1, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    .line 1082590
    iget-object v2, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data3"

    invoke-static {v2, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082591
    new-instance v3, Lcom/facebook/contacts/model/PhonebookNickname;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/contacts/model/PhonebookNickname;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1082592
    if-eqz v3, :cond_0

    .line 1082593
    iget-object v0, p1, LX/6NG;->p:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1082594
    :cond_0
    return-void
.end method

.method public final h(LX/6NG;)V
    .locals 11

    .prologue
    .line 1082509
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1082510
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1082511
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v0, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 1082512
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data3"

    invoke-static {v0, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1082513
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v4, "data4"

    invoke-static {v0, v4}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082514
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v5, "data5"

    invoke-static {v0, v5}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1082515
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v6, "data6"

    invoke-static {v0, v6}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1082516
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v7, "data7"

    invoke-static {v0, v7}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1082517
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v8, "data8"

    invoke-static {v0, v8}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1082518
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v9, "data9"

    invoke-static {v0, v9}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1082519
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v10, "data10"

    invoke-static {v0, v10}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1082520
    new-instance v0, Lcom/facebook/contacts/model/PhonebookAddress;

    invoke-direct/range {v0 .. v10}, Lcom/facebook/contacts/model/PhonebookAddress;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082521
    if-eqz v0, :cond_0

    .line 1082522
    iget-object v1, p1, LX/6NG;->q:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1082523
    :cond_0
    return-void
.end method

.method public final i(LX/6NG;)V
    .locals 4

    .prologue
    .line 1082501
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082502
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1082503
    iget-object v1, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v1, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    .line 1082504
    iget-object v2, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data3"

    invoke-static {v2, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082505
    new-instance v3, Lcom/facebook/contacts/model/PhonebookWebsite;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/contacts/model/PhonebookWebsite;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1082506
    if-eqz v3, :cond_0

    .line 1082507
    iget-object v0, p1, LX/6NG;->r:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1082508
    :cond_0
    return-void
.end method

.method public final j(LX/6NG;)V
    .locals 4

    .prologue
    .line 1082493
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082494
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1082495
    iget-object v1, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v1, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    .line 1082496
    iget-object v2, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data3"

    invoke-static {v2, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082497
    new-instance v3, Lcom/facebook/contacts/model/PhonebookRelation;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/contacts/model/PhonebookRelation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1082498
    if-eqz v3, :cond_0

    .line 1082499
    iget-object v0, p1, LX/6NG;->s:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1082500
    :cond_0
    return-void
.end method

.method public final k(LX/6NG;)V
    .locals 10

    .prologue
    .line 1082479
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1082480
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data4"

    invoke-static {v0, v2}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082481
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1082482
    :cond_0
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v0, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    .line 1082483
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data3"

    invoke-static {v0, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1082484
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v5, "data5"

    invoke-static {v0, v5}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1082485
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v6, "data6"

    invoke-static {v0, v6}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1082486
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v7, "data7"

    invoke-static {v0, v7}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1082487
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v8, "data8"

    invoke-static {v0, v8}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1082488
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v9, "data9"

    invoke-static {v0, v9}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1082489
    new-instance v0, Lcom/facebook/contacts/model/PhonebookOrganization;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/contacts/model/PhonebookOrganization;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082490
    if-eqz v0, :cond_1

    .line 1082491
    iget-object v1, p1, LX/6NG;->t:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1082492
    :cond_1
    return-void
.end method

.method public final l(LX/6NG;)V
    .locals 4

    .prologue
    .line 1082471
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082472
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1082473
    iget-object v1, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data2"

    invoke-static {v1, v2}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v1

    .line 1082474
    iget-object v2, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v3, "data3"

    invoke-static {v2, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082475
    new-instance v3, Lcom/facebook/contacts/model/PhonebookEvent;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/contacts/model/PhonebookEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1082476
    if-eqz v3, :cond_0

    .line 1082477
    iget-object v0, p1, LX/6NG;->u:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1082478
    :cond_0
    return-void
.end method

.method public final m(LX/6NG;)V
    .locals 2

    .prologue
    .line 1082464
    iget-object v0, p0, LX/6N8;->e:LX/6Op;

    .line 1082465
    iget-object v1, p1, LX/6NG;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1082466
    invoke-virtual {v0, v1}, LX/6Op;->a(Ljava/lang/String;)Lcom/facebook/contacts/model/PhonebookContactMetadata;

    move-result-object v0

    .line 1082467
    if-eqz v0, :cond_0

    .line 1082468
    if-eqz v0, :cond_0

    .line 1082469
    iget-object v1, p1, LX/6NG;->v:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1082470
    :cond_0
    return-void
.end method

.method public final n(LX/6NG;)V
    .locals 5

    .prologue
    .line 1082457
    iget-object v0, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v1, "data1"

    invoke-static {v0, v1}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1082458
    iget-object v1, p0, LX/6Mz;->c:Landroid/database/Cursor;

    const-string v2, "data3"

    invoke-static {v1, v2}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1082459
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1082460
    new-instance v2, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;

    const-string v3, "Message"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/facebook/contacts/model/PhonebookWhatsappProfile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1082461
    if-eqz v2, :cond_0

    .line 1082462
    iget-object v0, p1, LX/6NG;->w:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1082463
    :cond_0
    return-void
.end method
