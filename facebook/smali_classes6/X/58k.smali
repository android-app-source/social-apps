.class public final LX/58k;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 848983
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 848984
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 848985
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 848986
    invoke-static {p0, p1}, LX/58k;->b(LX/15w;LX/186;)I

    move-result v1

    .line 848987
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 848988
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 848977
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 848978
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 848979
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/58k;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 848980
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 848981
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 848982
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 50

    .prologue
    .line 848631
    const/16 v46, 0x0

    .line 848632
    const/16 v45, 0x0

    .line 848633
    const/16 v44, 0x0

    .line 848634
    const/16 v43, 0x0

    .line 848635
    const/16 v42, 0x0

    .line 848636
    const/16 v41, 0x0

    .line 848637
    const/16 v40, 0x0

    .line 848638
    const/16 v39, 0x0

    .line 848639
    const/16 v38, 0x0

    .line 848640
    const/16 v37, 0x0

    .line 848641
    const/16 v36, 0x0

    .line 848642
    const/16 v35, 0x0

    .line 848643
    const/16 v34, 0x0

    .line 848644
    const/16 v33, 0x0

    .line 848645
    const/16 v32, 0x0

    .line 848646
    const/16 v31, 0x0

    .line 848647
    const/16 v30, 0x0

    .line 848648
    const/16 v29, 0x0

    .line 848649
    const/16 v28, 0x0

    .line 848650
    const/16 v27, 0x0

    .line 848651
    const/16 v26, 0x0

    .line 848652
    const/16 v25, 0x0

    .line 848653
    const/16 v24, 0x0

    .line 848654
    const/16 v23, 0x0

    .line 848655
    const/16 v22, 0x0

    .line 848656
    const/16 v21, 0x0

    .line 848657
    const/16 v20, 0x0

    .line 848658
    const/16 v19, 0x0

    .line 848659
    const/16 v18, 0x0

    .line 848660
    const/16 v17, 0x0

    .line 848661
    const/16 v16, 0x0

    .line 848662
    const/4 v15, 0x0

    .line 848663
    const/4 v14, 0x0

    .line 848664
    const/4 v13, 0x0

    .line 848665
    const/4 v12, 0x0

    .line 848666
    const/4 v11, 0x0

    .line 848667
    const/4 v10, 0x0

    .line 848668
    const/4 v9, 0x0

    .line 848669
    const/4 v8, 0x0

    .line 848670
    const/4 v7, 0x0

    .line 848671
    const/4 v6, 0x0

    .line 848672
    const/4 v5, 0x0

    .line 848673
    const/4 v4, 0x0

    .line 848674
    const/4 v3, 0x0

    .line 848675
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v47

    sget-object v48, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    if-eq v0, v1, :cond_1

    .line 848676
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 848677
    const/4 v3, 0x0

    .line 848678
    :goto_0
    return v3

    .line 848679
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 848680
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v47

    sget-object v48, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    if-eq v0, v1, :cond_2a

    .line 848681
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v47

    .line 848682
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 848683
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v48

    sget-object v49, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    if-eq v0, v1, :cond_1

    if-eqz v47, :cond_1

    .line 848684
    const-string v48, "__type__"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-nez v48, :cond_2

    const-string v48, "__typename"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_3

    .line 848685
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v46

    goto :goto_1

    .line 848686
    :cond_3
    const-string v48, "attributes"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 848687
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v45

    goto :goto_1

    .line 848688
    :cond_4
    const-string v48, "availability_indicator_label"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_5

    .line 848689
    invoke-static/range {p0 .. p1}, LX/4at;->a(LX/15w;LX/186;)I

    move-result v44

    goto :goto_1

    .line 848690
    :cond_5
    const-string v48, "bounding_box"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_6

    .line 848691
    invoke-static/range {p0 .. p1}, LX/5EZ;->a(LX/15w;LX/186;)I

    move-result v43

    goto :goto_1

    .line 848692
    :cond_6
    const-string v48, "broadcaster"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_7

    .line 848693
    invoke-static/range {p0 .. p1}, LX/5GQ;->a(LX/15w;LX/186;)I

    move-result v42

    goto :goto_1

    .line 848694
    :cond_7
    const-string v48, "contextual_title"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_8

    .line 848695
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v41

    goto :goto_1

    .line 848696
    :cond_8
    const-string v48, "destination_id"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_9

    .line 848697
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    goto/16 :goto_1

    .line 848698
    :cond_9
    const-string v48, "destination_type"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_a

    .line 848699
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v39 .. v39}, Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v39

    goto/16 :goto_1

    .line 848700
    :cond_a
    const-string v48, "game_description"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_b

    .line 848701
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    goto/16 :goto_1

    .line 848702
    :cond_b
    const-string v48, "game_name"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_c

    .line 848703
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    goto/16 :goto_1

    .line 848704
    :cond_c
    const-string v48, "game_orientation"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_d

    .line 848705
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    goto/16 :goto_1

    .line 848706
    :cond_d
    const-string v48, "game_uri"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_e

    .line 848707
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    goto/16 :goto_1

    .line 848708
    :cond_e
    const-string v48, "group"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_f

    .line 848709
    invoke-static/range {p0 .. p1}, LX/5Fc;->a(LX/15w;LX/186;)I

    move-result v34

    goto/16 :goto_1

    .line 848710
    :cond_f
    const-string v48, "icon_uri"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_10

    .line 848711
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto/16 :goto_1

    .line 848712
    :cond_10
    const-string v48, "instant_experience_ad_id"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_11

    .line 848713
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 848714
    :cond_11
    const-string v48, "instant_experience_app"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_12

    .line 848715
    invoke-static/range {p0 .. p1}, LX/5GG;->a(LX/15w;LX/186;)I

    move-result v31

    goto/16 :goto_1

    .line 848716
    :cond_12
    const-string v48, "instant_experience_app_id"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_13

    .line 848717
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 848718
    :cond_13
    const-string v48, "instant_experience_domain_whitelist"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_14

    .line 848719
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v29

    goto/16 :goto_1

    .line 848720
    :cond_14
    const-string v48, "instant_experience_feature_enabled_list"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_15

    .line 848721
    invoke-static/range {p0 .. p1}, LX/5GH;->a(LX/15w;LX/186;)I

    move-result v28

    goto/16 :goto_1

    .line 848722
    :cond_15
    const-string v48, "instant_experience_link_uri"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_16

    .line 848723
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 848724
    :cond_16
    const-string v48, "instant_experience_page"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_17

    .line 848725
    invoke-static/range {p0 .. p1}, LX/5GI;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 848726
    :cond_17
    const-string v48, "instant_experience_user_app_scoped_id"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_18

    .line 848727
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    goto/16 :goto_1

    .line 848728
    :cond_18
    const-string v48, "instant_experience_user_page_scoped_id"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_19

    .line 848729
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 848730
    :cond_19
    const-string v48, "instant_game_id"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1a

    .line 848731
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto/16 :goto_1

    .line 848732
    :cond_1a
    const-string v48, "label"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1b

    .line 848733
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 848734
    :cond_1b
    const-string v48, "lat_long_list"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1c

    .line 848735
    invoke-static/range {p0 .. p1}, LX/5Fi;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 848736
    :cond_1c
    const-string v48, "layout_height"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1d

    .line 848737
    const/4 v6, 0x1

    .line 848738
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    goto/16 :goto_1

    .line 848739
    :cond_1d
    const-string v48, "layout_width"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1e

    .line 848740
    const/4 v5, 0x1

    .line 848741
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto/16 :goto_1

    .line 848742
    :cond_1e
    const-string v48, "layout_x"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1f

    .line 848743
    const/4 v4, 0x1

    .line 848744
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    goto/16 :goto_1

    .line 848745
    :cond_1f
    const-string v48, "layout_y"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_20

    .line 848746
    const/4 v3, 0x1

    .line 848747
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto/16 :goto_1

    .line 848748
    :cond_20
    const-string v48, "location"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_21

    .line 848749
    invoke-static/range {p0 .. p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 848750
    :cond_21
    const-string v48, "logo"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_22

    .line 848751
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 848752
    :cond_22
    const-string v48, "mobile_game_uri"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_23

    .line 848753
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 848754
    :cond_23
    const-string v48, "native_template_view"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_24

    .line 848755
    invoke-static/range {p0 .. p1}, LX/5ez;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 848756
    :cond_24
    const-string v48, "nearby_locations"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_25

    .line 848757
    invoke-static/range {p0 .. p1}, LX/4aX;->b(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 848758
    :cond_25
    const-string v48, "parent_story"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_26

    .line 848759
    invoke-static/range {p0 .. p1}, LX/5Ee;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 848760
    :cond_26
    const-string v48, "place_rec_info"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_27

    .line 848761
    invoke-static/range {p0 .. p1}, LX/5GC;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 848762
    :cond_27
    const-string v48, "splash_uri"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_28

    .line 848763
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 848764
    :cond_28
    const-string v48, "video_broadcast_schedule"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_29

    .line 848765
    invoke-static/range {p0 .. p1}, LX/5GU;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 848766
    :cond_29
    const-string v48, "video_uri"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_0

    .line 848767
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 848768
    :cond_2a
    const/16 v47, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 848769
    const/16 v47, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v47

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848770
    const/16 v46, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v46

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848771
    const/16 v45, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v45

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848772
    const/16 v44, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v44

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848773
    const/16 v43, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v43

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848774
    const/16 v42, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v42

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848775
    const/16 v41, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848776
    const/16 v40, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v40

    move/from16 v2, v39

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848777
    const/16 v39, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v39

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848778
    const/16 v38, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v38

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848779
    const/16 v37, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v37

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848780
    const/16 v36, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848781
    const/16 v35, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848782
    const/16 v34, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848783
    const/16 v33, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848784
    const/16 v32, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848785
    const/16 v31, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848786
    const/16 v30, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848787
    const/16 v29, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848788
    const/16 v28, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848789
    const/16 v27, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848790
    const/16 v26, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848791
    const/16 v25, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848792
    const/16 v24, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848793
    const/16 v23, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848794
    const/16 v22, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848795
    if-eqz v6, :cond_2b

    .line 848796
    const/16 v6, 0x1a

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v6, v1, v2}, LX/186;->a(III)V

    .line 848797
    :cond_2b
    if-eqz v5, :cond_2c

    .line 848798
    const/16 v5, 0x1b

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 848799
    :cond_2c
    if-eqz v4, :cond_2d

    .line 848800
    const/16 v4, 0x1c

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 848801
    :cond_2d
    if-eqz v3, :cond_2e

    .line 848802
    const/16 v3, 0x1d

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1, v4}, LX/186;->a(III)V

    .line 848803
    :cond_2e
    const/16 v3, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 848804
    const/16 v3, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 848805
    const/16 v3, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 848806
    const/16 v3, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 848807
    const/16 v3, 0x22

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 848808
    const/16 v3, 0x23

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 848809
    const/16 v3, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 848810
    const/16 v3, 0x25

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 848811
    const/16 v3, 0x26

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 848812
    const/16 v3, 0x27

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 848813
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v5, 0x11

    const/16 v4, 0xa

    const/4 v3, 0x7

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 848814
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 848815
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 848816
    if-eqz v0, :cond_0

    .line 848817
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848818
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 848819
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 848820
    if-eqz v0, :cond_1

    .line 848821
    const-string v0, "attributes"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848822
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 848823
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848824
    if-eqz v0, :cond_2

    .line 848825
    const-string v1, "availability_indicator_label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848826
    invoke-static {p0, v0, p2, p3}, LX/4at;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 848827
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848828
    if-eqz v0, :cond_3

    .line 848829
    const-string v1, "bounding_box"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848830
    invoke-static {p0, v0, p2}, LX/5EZ;->a(LX/15i;ILX/0nX;)V

    .line 848831
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848832
    if-eqz v0, :cond_4

    .line 848833
    const-string v1, "broadcaster"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848834
    invoke-static {p0, v0, p2}, LX/5GQ;->a(LX/15i;ILX/0nX;)V

    .line 848835
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848836
    if-eqz v0, :cond_5

    .line 848837
    const-string v1, "contextual_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848838
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 848839
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848840
    if-eqz v0, :cond_6

    .line 848841
    const-string v1, "destination_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848842
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848843
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 848844
    if-eqz v0, :cond_7

    .line 848845
    const-string v0, "destination_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848846
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848847
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848848
    if-eqz v0, :cond_8

    .line 848849
    const-string v1, "game_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848850
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848851
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848852
    if-eqz v0, :cond_9

    .line 848853
    const-string v1, "game_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848854
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848855
    :cond_9
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 848856
    if-eqz v0, :cond_a

    .line 848857
    const-string v0, "game_orientation"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848858
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848859
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848860
    if-eqz v0, :cond_b

    .line 848861
    const-string v1, "game_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848862
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848863
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848864
    if-eqz v0, :cond_c

    .line 848865
    const-string v1, "group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848866
    invoke-static {p0, v0, p2, p3}, LX/5Fc;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 848867
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848868
    if-eqz v0, :cond_d

    .line 848869
    const-string v1, "icon_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848870
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848871
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848872
    if-eqz v0, :cond_e

    .line 848873
    const-string v1, "instant_experience_ad_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848874
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848875
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848876
    if-eqz v0, :cond_f

    .line 848877
    const-string v1, "instant_experience_app"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848878
    invoke-static {p0, v0, p2, p3}, LX/5GG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 848879
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848880
    if-eqz v0, :cond_10

    .line 848881
    const-string v1, "instant_experience_app_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848882
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848883
    :cond_10
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 848884
    if-eqz v0, :cond_11

    .line 848885
    const-string v0, "instant_experience_domain_whitelist"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848886
    invoke-virtual {p0, p1, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 848887
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848888
    if-eqz v0, :cond_12

    .line 848889
    const-string v1, "instant_experience_feature_enabled_list"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848890
    invoke-static {p0, v0, p2}, LX/5GH;->a(LX/15i;ILX/0nX;)V

    .line 848891
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848892
    if-eqz v0, :cond_13

    .line 848893
    const-string v1, "instant_experience_link_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848894
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848895
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848896
    if-eqz v0, :cond_14

    .line 848897
    const-string v1, "instant_experience_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848898
    invoke-static {p0, v0, p2}, LX/5GI;->a(LX/15i;ILX/0nX;)V

    .line 848899
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848900
    if-eqz v0, :cond_15

    .line 848901
    const-string v1, "instant_experience_user_app_scoped_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848902
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848903
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848904
    if-eqz v0, :cond_16

    .line 848905
    const-string v1, "instant_experience_user_page_scoped_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848906
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848907
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848908
    if-eqz v0, :cond_17

    .line 848909
    const-string v1, "instant_game_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848910
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848911
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848912
    if-eqz v0, :cond_18

    .line 848913
    const-string v1, "label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848914
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848915
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848916
    if-eqz v0, :cond_19

    .line 848917
    const-string v1, "lat_long_list"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848918
    invoke-static {p0, v0, p2, p3}, LX/5Fi;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 848919
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 848920
    if-eqz v0, :cond_1a

    .line 848921
    const-string v1, "layout_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848922
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 848923
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 848924
    if-eqz v0, :cond_1b

    .line 848925
    const-string v1, "layout_width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848926
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 848927
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 848928
    if-eqz v0, :cond_1c

    .line 848929
    const-string v1, "layout_x"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848930
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 848931
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 848932
    if-eqz v0, :cond_1d

    .line 848933
    const-string v1, "layout_y"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848934
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 848935
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848936
    if-eqz v0, :cond_1e

    .line 848937
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848938
    invoke-static {p0, v0, p2}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 848939
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848940
    if-eqz v0, :cond_1f

    .line 848941
    const-string v1, "logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848942
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 848943
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848944
    if-eqz v0, :cond_20

    .line 848945
    const-string v1, "mobile_game_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848946
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848947
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848948
    if-eqz v0, :cond_21

    .line 848949
    const-string v1, "native_template_view"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848950
    invoke-static {p0, v0, p2, p3}, LX/5ez;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 848951
    :cond_21
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848952
    if-eqz v0, :cond_22

    .line 848953
    const-string v1, "nearby_locations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848954
    invoke-static {p0, v0, p2, p3}, LX/4aX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 848955
    :cond_22
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848956
    if-eqz v0, :cond_23

    .line 848957
    const-string v1, "parent_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848958
    invoke-static {p0, v0, p2, p3}, LX/5Ee;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 848959
    :cond_23
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848960
    if-eqz v0, :cond_24

    .line 848961
    const-string v1, "place_rec_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848962
    invoke-static {p0, v0, p2, p3}, LX/5GC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 848963
    :cond_24
    const/16 v0, 0x25

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848964
    if-eqz v0, :cond_25

    .line 848965
    const-string v1, "splash_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848966
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848967
    :cond_25
    const/16 v0, 0x26

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 848968
    if-eqz v0, :cond_26

    .line 848969
    const-string v1, "video_broadcast_schedule"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848970
    invoke-static {p0, v0, p2, p3}, LX/5GU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 848971
    :cond_26
    const/16 v0, 0x27

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 848972
    if-eqz v0, :cond_27

    .line 848973
    const-string v1, "video_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 848974
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 848975
    :cond_27
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 848976
    return-void
.end method
