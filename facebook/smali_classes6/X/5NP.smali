.class public LX/5NP;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/17Y;

.field public final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 906265
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 906266
    iput-object p1, p0, LX/5NP;->c:Landroid/content/Context;

    .line 906267
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 906268
    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, LX/5NP;->a:Lcom/facebook/content/SecureContextHelper;

    .line 906269
    invoke-static {v1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    iput-object v0, p0, LX/5NP;->b:LX/17Y;

    .line 906270
    new-instance v0, LX/5NO;

    invoke-direct {v0, p0}, LX/5NO;-><init>(LX/5NP;)V

    invoke-virtual {p0, v0}, LX/5NP;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 906271
    const v0, 0x7f080652

    invoke-virtual {p0, v0}, LX/5NP;->setTitle(I)V

    .line 906272
    return-void
.end method
