.class public LX/6Tv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1099546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1099547
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1099521
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;

    .line 1099522
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1099523
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1099524
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "recipient_ids"

    .line 1099525
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 1099526
    iget-object v3, p1, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result p0

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, p0, :cond_0

    iget-object v3, p1, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->b:LX/0Px;

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1099527
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1099528
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1099529
    :cond_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 1099530
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1099531
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "live_video_subscriptions"

    .line 1099532
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1099533
    move-object v1, v1

    .line 1099534
    const-string v2, "POST"

    .line 1099535
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1099536
    move-object v1, v1

    .line 1099537
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Lcom/facebook/facecastdisplay/protocol/LiveVideoInviteFriendsParams;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/invite_notifications"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1099538
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1099539
    move-object v1, v1

    .line 1099540
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1099541
    move-object v0, v1

    .line 1099542
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1099543
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1099544
    move-object v0, v0

    .line 1099545
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1099519
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1099520
    const/4 v0, 0x0

    return-object v0
.end method
