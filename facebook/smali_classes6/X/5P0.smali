.class public final enum LX/5P0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5P0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5P0;

.field public static final enum CONTACTS:LX/5P0;

.field public static final enum FRIENDS:LX/5P0;

.field public static final enum INVITES:LX/5P0;

.field public static final enum REQUESTS:LX/5P0;

.field public static final enum SEARCH:LX/5P0;

.field public static final enum SUGGESTIONS:LX/5P0;


# instance fields
.field public final analyticsTag:Ljava/lang/String;

.field public final titleResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 909822
    new-instance v0, LX/5P0;

    const-string v1, "SUGGESTIONS"

    const v2, 0x7f0818a0

    const-string v3, "friends_center_suggestions_tab"

    invoke-direct {v0, v1, v5, v2, v3}, LX/5P0;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5P0;->SUGGESTIONS:LX/5P0;

    .line 909823
    new-instance v0, LX/5P0;

    const-string v1, "SEARCH"

    const v2, 0x7f0818a1

    const-string v3, "friends_center_search_tab"

    invoke-direct {v0, v1, v6, v2, v3}, LX/5P0;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5P0;->SEARCH:LX/5P0;

    .line 909824
    new-instance v0, LX/5P0;

    const-string v1, "REQUESTS"

    const v2, 0x7f0818a2

    const-string v3, "friends_center_requests_tab"

    invoke-direct {v0, v1, v7, v2, v3}, LX/5P0;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5P0;->REQUESTS:LX/5P0;

    .line 909825
    new-instance v0, LX/5P0;

    const-string v1, "CONTACTS"

    const v2, 0x7f0818a3

    const-string v3, "friends_center_contacts_tab"

    invoke-direct {v0, v1, v8, v2, v3}, LX/5P0;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5P0;->CONTACTS:LX/5P0;

    .line 909826
    new-instance v0, LX/5P0;

    const-string v1, "INVITES"

    const v2, 0x7f0818a4

    const-string v3, "friends_center_invites_tab"

    invoke-direct {v0, v1, v9, v2, v3}, LX/5P0;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5P0;->INVITES:LX/5P0;

    .line 909827
    new-instance v0, LX/5P0;

    const-string v1, "FRIENDS"

    const/4 v2, 0x5

    const v3, 0x7f0818a7

    const-string v4, "friends_center_friends_tab"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5P0;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/5P0;->FRIENDS:LX/5P0;

    .line 909828
    const/4 v0, 0x6

    new-array v0, v0, [LX/5P0;

    sget-object v1, LX/5P0;->SUGGESTIONS:LX/5P0;

    aput-object v1, v0, v5

    sget-object v1, LX/5P0;->SEARCH:LX/5P0;

    aput-object v1, v0, v6

    sget-object v1, LX/5P0;->REQUESTS:LX/5P0;

    aput-object v1, v0, v7

    sget-object v1, LX/5P0;->CONTACTS:LX/5P0;

    aput-object v1, v0, v8

    sget-object v1, LX/5P0;->INVITES:LX/5P0;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/5P0;->FRIENDS:LX/5P0;

    aput-object v2, v0, v1

    sput-object v0, LX/5P0;->$VALUES:[LX/5P0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 909817
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 909818
    iput p3, p0, LX/5P0;->titleResId:I

    .line 909819
    iput-object p4, p0, LX/5P0;->analyticsTag:Ljava/lang/String;

    .line 909820
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/5P0;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 909829
    invoke-static {}, LX/5P0;->values()[LX/5P0;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 909830
    invoke-virtual {v0}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 909831
    :goto_1
    return-object v0

    .line 909832
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 909833
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/5P0;
    .locals 1

    .prologue
    .line 909821
    const-class v0, LX/5P0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5P0;

    return-object v0
.end method

.method public static values()[LX/5P0;
    .locals 1

    .prologue
    .line 909816
    sget-object v0, LX/5P0;->$VALUES:[LX/5P0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5P0;

    return-object v0
.end method
