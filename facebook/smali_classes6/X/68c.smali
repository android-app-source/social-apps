.class public final LX/68c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/68h;

.field public final b:LX/68d;

.field public final c:[Z

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(LX/68h;LX/68d;)V
    .locals 1

    .prologue
    .line 1055734
    iput-object p1, p0, LX/68c;->a:LX/68h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1055735
    iput-object p2, p0, LX/68c;->b:LX/68d;

    .line 1055736
    iget-boolean v0, p2, LX/68d;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/68c;->c:[Z

    .line 1055737
    return-void

    .line 1055738
    :cond_0
    iget v0, p1, LX/68h;->k:I

    new-array v0, v0, [Z

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 1055739
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/68c;->a:LX/68h;

    iget v0, v0, LX/68h;->k:I

    if-lt p1, v0, :cond_1

    .line 1055740
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to be greater than 0 and less than the maximum value count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/68c;->a:LX/68h;

    iget v2, v2, LX/68h;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055741
    :cond_1
    iget-object v2, p0, LX/68c;->a:LX/68h;

    monitor-enter v2

    .line 1055742
    :try_start_0
    iget-object v0, p0, LX/68c;->b:LX/68d;

    iget-object v0, v0, LX/68d;->e:LX/68c;

    if-eq v0, p0, :cond_2

    .line 1055743
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1055744
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1055745
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/68c;->b:LX/68d;

    iget-boolean v0, v0, LX/68d;->d:Z

    if-nez v0, :cond_3

    .line 1055746
    iget-object v0, p0, LX/68c;->c:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    .line 1055747
    :cond_3
    iget-object v0, p0, LX/68c;->b:LX/68d;

    invoke-virtual {v0, p1}, LX/68d;->b(I)Ljava/io/File;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 1055748
    :try_start_2
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v0

    .line 1055749
    :goto_0
    :try_start_3
    new-instance v0, LX/68b;

    invoke-direct {v0, p0, v1}, LX/68b;-><init>(LX/68c;Ljava/io/OutputStream;)V

    monitor-exit v2

    :goto_1
    return-object v0

    .line 1055750
    :catch_0
    iget-object v0, p0, LX/68c;->a:LX/68h;

    iget-object v0, v0, LX/68h;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1055751
    :try_start_4
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v1, v0

    .line 1055752
    goto :goto_0

    .line 1055753
    :catch_1
    :try_start_5
    sget-object v0, LX/68h;->r:Ljava/io/OutputStream;

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1055754
    iget-object v0, p0, LX/68c;->a:LX/68h;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, LX/68h;->a$redex0(LX/68h;LX/68c;Z)V

    .line 1055755
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1055756
    iget-boolean v0, p0, LX/68c;->e:Z

    if-nez v0, :cond_0

    .line 1055757
    :try_start_0
    invoke-virtual {p0}, LX/68c;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1055758
    :cond_0
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
