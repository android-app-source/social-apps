.class public interface abstract LX/5Vt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Vs;
.implements LX/5Vp;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "ThreadInfo"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract C()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract X()LX/2uF;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadGames"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract ac()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadQueueInfoModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThreadQueueMetadata"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ae()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$RequestedBookingRequestModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRequestedBookingRequest"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract af()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$PendingBookingRequestModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPendingBookingRequest"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ah()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$BookingRequestsModel$ConfirmedBookingRequestModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getConfirmedBookingRequest"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ai()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$AllParticipantsModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllParticipants"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActiveBots"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAllParticipantIds"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract u()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCustomizationInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
