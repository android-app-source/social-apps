.class public final LX/6Tu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1099395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 10

    .prologue
    .line 1099396
    if-nez p0, :cond_0

    .line 1099397
    const/4 v0, 0x0

    .line 1099398
    :goto_0
    return-object v0

    .line 1099399
    :cond_0
    new-instance v2, LX/3dM;

    invoke-direct {v2}, LX/3dM;-><init>()V

    .line 1099400
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->a()Z

    move-result v0

    .line 1099401
    iput-boolean v0, v2, LX/3dM;->k:Z

    .line 1099402
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->b()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;

    move-result-object v0

    .line 1099403
    if-nez v0, :cond_3

    .line 1099404
    const/4 v1, 0x0

    .line 1099405
    :goto_1
    move-object v0, v1

    .line 1099406
    iput-object v0, v2, LX/3dM;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 1099407
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->c()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;

    move-result-object v0

    .line 1099408
    if-nez v0, :cond_4

    .line 1099409
    const/4 v1, 0x0

    .line 1099410
    :goto_2
    move-object v0, v1

    .line 1099411
    iput-object v0, v2, LX/3dM;->z:Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    .line 1099412
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->d()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;

    move-result-object v0

    .line 1099413
    if-nez v0, :cond_8

    .line 1099414
    const/4 v1, 0x0

    .line 1099415
    :goto_3
    move-object v0, v1

    .line 1099416
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1099417
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->e()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;

    move-result-object v0

    .line 1099418
    if-nez v0, :cond_9

    .line 1099419
    const/4 v1, 0x0

    .line 1099420
    :goto_4
    move-object v0, v1

    .line 1099421
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;)LX/3dM;

    .line 1099422
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->du_()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;

    move-result-object v0

    .line 1099423
    if-nez v0, :cond_a

    .line 1099424
    const/4 v1, 0x0

    .line 1099425
    :goto_5
    move-object v0, v1

    .line 1099426
    iput-object v0, v2, LX/3dM;->K:Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    .line 1099427
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->dt_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1099428
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1099429
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->dt_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1099430
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->dt_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;

    .line 1099431
    if-nez v0, :cond_b

    .line 1099432
    const/4 v4, 0x0

    .line 1099433
    :goto_7
    move-object v0, v4

    .line 1099434
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1099435
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1099436
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1099437
    iput-object v0, v2, LX/3dM;->N:LX/0Px;

    .line 1099438
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->j()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;

    move-result-object v0

    .line 1099439
    if-nez v0, :cond_c

    .line 1099440
    const/4 v1, 0x0

    .line 1099441
    :goto_8
    move-object v0, v1

    .line 1099442
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;)LX/3dM;

    .line 1099443
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->k()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;

    move-result-object v0

    .line 1099444
    if-nez v0, :cond_11

    .line 1099445
    const/4 v1, 0x0

    .line 1099446
    :goto_9
    move-object v0, v1

    .line 1099447
    iput-object v0, v2, LX/3dM;->T:Lcom/facebook/graphql/model/GraphQLUser;

    .line 1099448
    invoke-virtual {p0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;->l()I

    move-result v0

    invoke-virtual {v2, v0}, LX/3dM;->b(I)LX/3dM;

    .line 1099449
    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto/16 :goto_0

    .line 1099450
    :cond_3
    new-instance v1, LX/4Vv;

    invoke-direct {v1}, LX/4Vv;-><init>()V

    .line 1099451
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$CommentsModel;->a()I

    move-result v3

    .line 1099452
    iput v3, v1, LX/4Vv;->b:I

    .line 1099453
    invoke-virtual {v1}, LX/4Vv;->a()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v1

    goto/16 :goto_1

    .line 1099454
    :cond_4
    new-instance v4, LX/4Wx;

    invoke-direct {v4}, LX/4Wx;-><init>()V

    .line 1099455
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1099456
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1099457
    const/4 v1, 0x0

    move v3, v1

    :goto_a
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_5

    .line 1099458
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;

    .line 1099459
    if-nez v1, :cond_7

    .line 1099460
    const/4 v6, 0x0

    .line 1099461
    :goto_b
    move-object v1, v6

    .line 1099462
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1099463
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_a

    .line 1099464
    :cond_5
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1099465
    iput-object v1, v4, LX/4Wx;->b:LX/0Px;

    .line 1099466
    :cond_6
    invoke-virtual {v4}, LX/4Wx;->a()Lcom/facebook/graphql/model/GraphQLImportantReactorsConnection;

    move-result-object v1

    goto/16 :goto_2

    .line 1099467
    :cond_7
    new-instance v6, LX/3dL;

    invoke-direct {v6}, LX/3dL;-><init>()V

    .line 1099468
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 1099469
    iput-object v7, v6, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1099470
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ImportantReactorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 1099471
    iput-object v7, v6, LX/3dL;->ag:Ljava/lang/String;

    .line 1099472
    invoke-virtual {v6}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    goto :goto_b

    .line 1099473
    :cond_8
    new-instance v1, LX/3dN;

    invoke-direct {v1}, LX/3dN;-><init>()V

    .line 1099474
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$LikersModel;->a()I

    move-result v3

    .line 1099475
    iput v3, v1, LX/3dN;->b:I

    .line 1099476
    invoke-virtual {v1}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v1

    goto/16 :goto_3

    .line 1099477
    :cond_9
    new-instance v1, LX/3dO;

    invoke-direct {v1}, LX/3dO;-><init>()V

    .line 1099478
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$SimpleReactionsFeedbackFieldsModel$ReactorsModel;->a()I

    move-result v3

    .line 1099479
    iput v3, v1, LX/3dO;->b:I

    .line 1099480
    invoke-virtual {v1}, LX/3dO;->a()Lcom/facebook/graphql/model/GraphQLReactorsOfContentConnection;

    move-result-object v1

    goto/16 :goto_4

    .line 1099481
    :cond_a
    new-instance v1, LX/4Ya;

    invoke-direct {v1}, LX/4Ya;-><init>()V

    .line 1099482
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel$ResharesModel;->a()I

    move-result v3

    .line 1099483
    iput v3, v1, LX/4Ya;->b:I

    .line 1099484
    invoke-virtual {v1}, LX/4Ya;->a()Lcom/facebook/graphql/model/GraphQLResharesOfContentConnection;

    move-result-object v1

    goto/16 :goto_5

    .line 1099485
    :cond_b
    new-instance v4, LX/4WL;

    invoke-direct {v4}, LX/4WL;-><init>()V

    .line 1099486
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsFeedbackFieldsModel$SupportedReactionsModel;->a()I

    move-result v5

    .line 1099487
    iput v5, v4, LX/4WL;->b:I

    .line 1099488
    invoke-virtual {v4}, LX/4WL;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReaction;

    move-result-object v4

    goto/16 :goto_7

    .line 1099489
    :cond_c
    new-instance v4, LX/3dQ;

    invoke-direct {v4}, LX/3dQ;-><init>()V

    .line 1099490
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 1099491
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1099492
    const/4 v1, 0x0

    move v3, v1

    :goto_c
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_d

    .line 1099493
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;

    .line 1099494
    if-nez v1, :cond_f

    .line 1099495
    const/4 v6, 0x0

    .line 1099496
    :goto_d
    move-object v1, v6

    .line 1099497
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1099498
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_c

    .line 1099499
    :cond_d
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1099500
    iput-object v1, v4, LX/3dQ;->b:LX/0Px;

    .line 1099501
    :cond_e
    invoke-virtual {v4}, LX/3dQ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsConnection;

    move-result-object v1

    goto/16 :goto_8

    .line 1099502
    :cond_f
    new-instance v6, LX/4ZJ;

    invoke-direct {v6}, LX/4ZJ;-><init>()V

    .line 1099503
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->a()Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;

    move-result-object v7

    .line 1099504
    if-nez v7, :cond_10

    .line 1099505
    const/4 v8, 0x0

    .line 1099506
    :goto_e
    move-object v7, v8

    .line 1099507
    iput-object v7, v6, LX/4ZJ;->b:Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    .line 1099508
    invoke-virtual {v1}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel;->b()I

    move-result v7

    .line 1099509
    iput v7, v6, LX/4ZJ;->c:I

    .line 1099510
    invoke-virtual {v6}, LX/4ZJ;->a()Lcom/facebook/graphql/model/GraphQLTopReactionsEdge;

    move-result-object v6

    goto :goto_d

    .line 1099511
    :cond_10
    new-instance v8, LX/4WM;

    invoke-direct {v8}, LX/4WM;-><init>()V

    .line 1099512
    invoke-virtual {v7}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ReactionsCountFieldsModel$TopReactionsModel$EdgesModel$NodeModel;->a()I

    move-result v9

    .line 1099513
    iput v9, v8, LX/4WM;->f:I

    .line 1099514
    invoke-virtual {v8}, LX/4WM;->a()Lcom/facebook/graphql/model/GraphQLFeedbackReactionInfo;

    move-result-object v8

    goto :goto_e

    .line 1099515
    :cond_11
    new-instance v1, LX/33O;

    invoke-direct {v1}, LX/33O;-><init>()V

    .line 1099516
    invoke-virtual {v0}, Lcom/facebook/api/graphql/reactions/ReactionsGraphQLModels$ViewerReactionsSocialFeedbackFieldsModel$ViewerActsAsPersonModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 1099517
    iput-object v3, v1, LX/33O;->aI:Ljava/lang/String;

    .line 1099518
    invoke-virtual {v1}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    goto/16 :goto_9
.end method
