.class public final enum LX/6Ig;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Ig;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Ig;

.field public static final enum NONE:LX/6Ig;

.field public static final enum OPTICAL:LX/6Ig;

.field public static final enum SOFTWARE:LX/6Ig;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1074383
    new-instance v0, LX/6Ig;

    const-string v1, "OPTICAL"

    invoke-direct {v0, v1, v2}, LX/6Ig;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Ig;->OPTICAL:LX/6Ig;

    .line 1074384
    new-instance v0, LX/6Ig;

    const-string v1, "SOFTWARE"

    invoke-direct {v0, v1, v3}, LX/6Ig;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Ig;->SOFTWARE:LX/6Ig;

    .line 1074385
    new-instance v0, LX/6Ig;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/6Ig;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Ig;->NONE:LX/6Ig;

    .line 1074386
    const/4 v0, 0x3

    new-array v0, v0, [LX/6Ig;

    sget-object v1, LX/6Ig;->OPTICAL:LX/6Ig;

    aput-object v1, v0, v2

    sget-object v1, LX/6Ig;->SOFTWARE:LX/6Ig;

    aput-object v1, v0, v3

    sget-object v1, LX/6Ig;->NONE:LX/6Ig;

    aput-object v1, v0, v4

    sput-object v0, LX/6Ig;->$VALUES:[LX/6Ig;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1074380
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Ig;
    .locals 1

    .prologue
    .line 1074382
    const-class v0, LX/6Ig;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Ig;

    return-object v0
.end method

.method public static values()[LX/6Ig;
    .locals 1

    .prologue
    .line 1074381
    sget-object v0, LX/6Ig;->$VALUES:[LX/6Ig;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Ig;

    return-object v0
.end method
