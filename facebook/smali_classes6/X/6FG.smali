.class public final LX/6FG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6FF;


# instance fields
.field public final synthetic a:Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;

.field public final synthetic b:LX/6FH;


# direct methods
.method public constructor <init>(LX/6FH;Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;)V
    .locals 0

    .prologue
    .line 1067561
    iput-object p1, p0, LX/6FG;->b:LX/6FH;

    iput-object p2, p0, LX/6FG;->a:Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1067562
    iget-object v0, p0, LX/6FG;->a:Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;

    sget-object v1, LX/6Cy;->BROWSER_EXTENSION_PROCESS_PAYMENT_FAILED:LX/6Cy;

    invoke-virtual {v1}, LX/6Cy;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1067563
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;)V
    .locals 3

    .prologue
    .line 1067564
    iget-object v0, p1, Lcom/facebook/payments/checkout/protocol/model/CheckoutChargeResult;->b:LX/0lF;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1067565
    const-string v1, "payment_result"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1067566
    iget-object v1, p0, LX/6FG;->a:Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;

    iget-object v2, p0, LX/6FG;->a:Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;

    invoke-virtual {v2}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v2

    .line 1067567
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 1067568
    const-string p1, "callbackID"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067569
    const-string p1, "payment_result"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067570
    move-object v0, p0

    .line 1067571
    invoke-virtual {v1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    .line 1067572
    return-void
.end method
