.class public final LX/65o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# instance fields
.field public a:I

.field public b:B

.field public c:I

.field public d:I

.field public e:S

.field private final f:LX/671;


# direct methods
.method public constructor <init>(LX/671;)V
    .locals 0

    .prologue
    .line 1048691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048692
    iput-object p1, p0, LX/65o;->f:LX/671;

    .line 1048693
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1048694
    iget v0, p0, LX/65o;->c:I

    .line 1048695
    iget-object v1, p0, LX/65o;->f:LX/671;

    invoke-static {v1}, LX/65t;->b(LX/671;)I

    move-result v1

    iput v1, p0, LX/65o;->d:I

    iput v1, p0, LX/65o;->a:I

    .line 1048696
    iget-object v1, p0, LX/65o;->f:LX/671;

    invoke-interface {v1}, LX/671;->h()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    .line 1048697
    iget-object v2, p0, LX/65o;->f:LX/671;

    invoke-interface {v2}, LX/671;->h()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    iput-byte v2, p0, LX/65o;->b:B

    .line 1048698
    sget-object v2, LX/65t;->a:Ljava/util/logging/Logger;

    sget-object v3, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, LX/65t;->a:Ljava/util/logging/Logger;

    iget v3, p0, LX/65o;->c:I

    iget v4, p0, LX/65o;->a:I

    iget-byte v5, p0, LX/65o;->b:B

    invoke-static {v7, v3, v4, v1, v5}, LX/65p;->a(ZIIBB)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 1048699
    :cond_0
    iget-object v2, p0, LX/65o;->f:LX/671;

    invoke-interface {v2}, LX/671;->j()I

    move-result v2

    const v3, 0x7fffffff

    and-int/2addr v2, v3

    iput v2, p0, LX/65o;->c:I

    .line 1048700
    const/16 v2, 0x9

    if-eq v1, v2, :cond_1

    const-string v0, "%s != TYPE_CONTINUATION"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    aput-object v1, v2, v6

    invoke-static {v0, v2}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048701
    :cond_1
    iget v1, p0, LX/65o;->c:I

    if-eq v1, v0, :cond_2

    const-string v0, "TYPE_CONTINUATION streamId changed"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, LX/65t;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048702
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 1048703
    :goto_0
    iget v2, p0, LX/65o;->d:I

    if-nez v2, :cond_2

    .line 1048704
    iget-object v2, p0, LX/65o;->f:LX/671;

    iget-short v3, p0, LX/65o;->e:S

    int-to-long v4, v3

    invoke-interface {v2, v4, v5}, LX/671;->f(J)V

    .line 1048705
    const/4 v2, 0x0

    iput-short v2, p0, LX/65o;->e:S

    .line 1048706
    iget-byte v2, p0, LX/65o;->b:B

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 1048707
    :cond_0
    :goto_1
    return-wide v0

    .line 1048708
    :cond_1
    invoke-direct {p0}, LX/65o;->b()V

    goto :goto_0

    .line 1048709
    :cond_2
    iget-object v2, p0, LX/65o;->f:LX/671;

    iget v3, p0, LX/65o;->d:I

    int-to-long v4, v3

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, LX/65D;->a(LX/672;J)J

    move-result-wide v2

    .line 1048710
    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    .line 1048711
    iget v0, p0, LX/65o;->d:I

    int-to-long v0, v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, LX/65o;->d:I

    move-wide v0, v2

    .line 1048712
    goto :goto_1
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1048713
    iget-object v0, p0, LX/65o;->f:LX/671;

    invoke-interface {v0}, LX/65D;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 1048714
    return-void
.end method
