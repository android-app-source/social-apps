.class public final LX/5LA;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 899761
    const-class v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchSingleTaggableSuggestionQueryModel;

    const v0, 0x57e541f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchSingleTaggableSuggestionQuery"

    const-string v6, "441a5449c3e2975f128b1cb926302e8c"

    const-string v7, "taggable_activity"

    const-string v8, "10155069965986729"

    const-string v9, "10155259088281729"

    .line 899762
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 899763
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 899764
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 899765
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 899766
    sparse-switch v0, :sswitch_data_0

    .line 899767
    :goto_0
    return-object p1

    .line 899768
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 899769
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 899770
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 899771
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 899772
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 899773
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 899774
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 899775
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 899776
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 899777
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x69f19a9a -> :sswitch_2
        -0x58c96de5 -> :sswitch_5
        -0x51484e72 -> :sswitch_0
        -0x50cab1c8 -> :sswitch_8
        -0x4ae70342 -> :sswitch_7
        -0x444a93e6 -> :sswitch_3
        -0x41a91745 -> :sswitch_6
        -0x2f1f8925 -> :sswitch_1
        0x291d8de0 -> :sswitch_9
        0x54df6484 -> :sswitch_4
    .end sparse-switch
.end method
