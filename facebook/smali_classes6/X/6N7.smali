.class public LX/6N7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/0PH;",
            "LX/6NY;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile g:LX/6N7;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6NX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2Re;

.field public final e:LX/2Rd;

.field public final f:LX/6Nf;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1082337
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/0PH;->PHONE_E164:LX/0PH;

    sget-object v2, LX/6NY;->PHONE_E164:LX/6NY;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0PH;->PHONE_NATIONAL:LX/0PH;

    sget-object v2, LX/6NY;->PHONE_NATIONAL:LX/6NY;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0PH;->PHONE_LOCAL:LX/0PH;

    sget-object v2, LX/6NY;->PHONE_LOCAL:LX/6NY;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/0PH;->PHONE_VERIFIED:LX/0PH;

    sget-object v2, LX/6NY;->PHONE_VERIFIED:LX/6NY;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/6N7;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/2Re;LX/2Rd;LX/6Nf;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6NX;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2Re;",
            "Lcom/facebook/user/names/Normalizer;",
            "LX/6Nf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1082338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1082339
    iput-object p1, p0, LX/6N7;->b:LX/0Or;

    .line 1082340
    iput-object p2, p0, LX/6N7;->c:LX/0Or;

    .line 1082341
    iput-object p3, p0, LX/6N7;->d:LX/2Re;

    .line 1082342
    iput-object p4, p0, LX/6N7;->e:LX/2Rd;

    .line 1082343
    iput-object p5, p0, LX/6N7;->f:LX/6Nf;

    .line 1082344
    return-void
.end method

.method public static a(LX/0QB;)LX/6N7;
    .locals 9

    .prologue
    .line 1082345
    sget-object v0, LX/6N7;->g:LX/6N7;

    if-nez v0, :cond_1

    .line 1082346
    const-class v1, LX/6N7;

    monitor-enter v1

    .line 1082347
    :try_start_0
    sget-object v0, LX/6N7;->g:LX/6N7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1082348
    if-eqz v2, :cond_0

    .line 1082349
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1082350
    new-instance v3, LX/6N7;

    const/16 v4, 0x1a14

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/2Re;->a(LX/0QB;)LX/2Re;

    move-result-object v6

    check-cast v6, LX/2Re;

    invoke-static {v0}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v7

    check-cast v7, LX/2Rd;

    invoke-static {v0}, LX/6Nf;->a(LX/0QB;)LX/6Nf;

    move-result-object v8

    check-cast v8, LX/6Nf;

    invoke-direct/range {v3 .. v8}, LX/6N7;-><init>(LX/0Or;LX/0Or;LX/2Re;LX/2Rd;LX/6Nf;)V

    .line 1082351
    move-object v0, v3

    .line 1082352
    sput-object v0, LX/6N7;->g:LX/6N7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1082353
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1082354
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1082355
    :cond_1
    sget-object v0, LX/6N7;->g:LX/6N7;

    return-object v0

    .line 1082356
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1082357
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6N7;LX/2RR;Ljava/util/Set;Lcom/facebook/omnistore/Collection;)Lcom/facebook/omnistore/Cursor;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2RR;",
            "Ljava/util/Set",
            "<",
            "LX/0PH;",
            ">;",
            "Lcom/facebook/omnistore/Collection;",
            ")",
            "Lcom/facebook/omnistore/Cursor;"
        }
    .end annotation

    .prologue
    .line 1082358
    iget-object v0, p1, LX/2RR;->b:Ljava/util/Collection;

    move-object v0, v0

    .line 1082359
    iget-object v1, p1, LX/2RR;->c:Ljava/util/Collection;

    move-object v1, v1

    .line 1082360
    iget-object v2, p1, LX/2RR;->d:Ljava/util/Collection;

    move-object v2, v2

    .line 1082361
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    if-eqz v2, :cond_1d

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1d

    :cond_2
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1082362
    if-eqz v0, :cond_3

    .line 1082363
    const-string v0, ""

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/omnistore/Collection$SortDirection;->DESCENDING:Lcom/facebook/omnistore/Collection$SortDirection;

    invoke-virtual {p3, v0, v1, v2}, Lcom/facebook/omnistore/Collection;->query(Ljava/lang/String;ILcom/facebook/omnistore/Collection$SortDirection;)Lcom/facebook/omnistore/Cursor;

    move-result-object v0

    .line 1082364
    :goto_1
    return-object v0

    .line 1082365
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1082366
    iget-object v0, p1, LX/2RR;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1082367
    if-eqz v0, :cond_b

    .line 1082368
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1082369
    sget-object v2, LX/0PH;->NAME:LX/0PH;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1082370
    const/4 v4, 0x0

    .line 1082371
    iget-object v2, p0, LX/6N7;->d:LX/2Re;

    invoke-virtual {v2, v0}, LX/2Re;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v6

    .line 1082372
    new-instance v7, Ljava/util/ArrayList;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v2

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1082373
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_2
    if-ge v5, v8, :cond_5

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1082374
    iget-object v9, p0, LX/6N7;->e:LX/2Rd;

    invoke-virtual {v9, v2}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1082375
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1082376
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082377
    :cond_4
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 1082378
    :cond_5
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v5, 0x4

    if-le v2, v5, :cond_7

    .line 1082379
    :cond_6
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 1082380
    iget-object v2, p0, LX/6N7;->e:LX/2Rd;

    invoke-virtual {v2, v0}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082381
    :cond_7
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1082382
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    :goto_3
    if-ge v4, v6, :cond_8

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1082383
    sget-object v8, LX/6NY;->NAME:LX/6NY;

    invoke-virtual {v8}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->GLOB:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, "*"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v9, v2}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082384
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 1082385
    :cond_8
    invoke-static {v5}, Lcom/facebook/omnistore/IndexQuery;->and(Ljava/util/List;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v2

    move-object v2, v2

    .line 1082386
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082387
    :cond_9
    sget-object v2, LX/0PH;->USERNAME:LX/0PH;

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1082388
    sget-object v2, LX/6NY;->USERNAME_KEY:LX/6NY;

    invoke-virtual {v2}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->GLOB:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, LX/6N7;->e:LX/2Rd;

    invoke-virtual {v6, v0}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "*"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v2

    move-object v2, v2

    .line 1082389
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082390
    :cond_a
    invoke-static {v0}, LX/6My;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1082391
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1e

    move-object v2, v3

    .line 1082392
    :goto_4
    move-object v0, v2

    .line 1082393
    invoke-static {v0}, Lcom/facebook/omnistore/IndexQuery;->or(Ljava/util/List;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082394
    :cond_b
    iget-object v0, p1, LX/2RR;->b:Ljava/util/Collection;

    move-object v0, v0

    .line 1082395
    if-eqz v0, :cond_d

    .line 1082396
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1082397
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2RU;

    .line 1082398
    sget-object v4, LX/6NY;->PROFILE_TYPE:LX/6NY;

    invoke-virtual {v4}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    invoke-virtual {v0}, LX/2RU;->getDbValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1082399
    :cond_c
    invoke-static {v2}, Lcom/facebook/omnistore/IndexQuery;->or(Ljava/util/List;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082400
    :cond_d
    iget-object v0, p1, LX/2RR;->c:Ljava/util/Collection;

    move-object v0, v0

    .line 1082401
    if-eqz v0, :cond_f

    .line 1082402
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1082403
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Oq;

    .line 1082404
    sget-object v4, LX/6NY;->LINK_TYPE:LX/6NY;

    invoke-virtual {v4}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    invoke-virtual {v0}, LX/3Oq;->getDbValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1082405
    :cond_e
    invoke-static {v2}, Lcom/facebook/omnistore/IndexQuery;->or(Ljava/util/List;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082406
    :cond_f
    iget-object v0, p1, LX/2RR;->d:Ljava/util/Collection;

    move-object v0, v0

    .line 1082407
    if-eqz v0, :cond_11

    .line 1082408
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1082409
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 1082410
    sget-object v4, LX/6NY;->FBID:LX/6NY;

    invoke-virtual {v4}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 1082411
    :cond_10
    invoke-static {v2}, Lcom/facebook/omnistore/IndexQuery;->or(Ljava/util/List;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082412
    :cond_11
    iget-boolean v0, p1, LX/2RR;->f:Z

    move v0, v0

    .line 1082413
    if-eqz v0, :cond_12

    .line 1082414
    sget-object v0, LX/6NY;->FBID:LX/6NY;

    invoke-virtual {v0}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->NOT_EQUAL:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    iget-object v0, p0, LX/6N7;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082415
    :cond_12
    iget-boolean v0, p1, LX/2RR;->h:Z

    move v0, v0

    .line 1082416
    if-eqz v0, :cond_13

    .line 1082417
    sget-object v0, LX/6NY;->IS_MESSENGER_USER:LX/6NY;

    invoke-virtual {v0}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    const-string v3, "0"

    invoke-static {v0, v2, v3}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082418
    :cond_13
    iget-boolean v0, p1, LX/2RR;->i:Z

    move v0, v0

    .line 1082419
    if-eqz v0, :cond_14

    .line 1082420
    sget-object v0, LX/6NY;->IS_MESSENGER_USER:LX/6NY;

    invoke-virtual {v0}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    const-string v3, "1"

    invoke-static {v0, v2, v3}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082421
    :cond_14
    iget-boolean v0, p1, LX/2RR;->g:Z

    move v0, v0

    .line 1082422
    if-eqz v0, :cond_15

    .line 1082423
    sget-object v0, LX/6NY;->IS_PUSHABLE_TRISTATE:LX/6NY;

    invoke-virtual {v0}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    sget-object v3, LX/03R;->YES:LX/03R;

    invoke-virtual {v3}, LX/03R;->getDbValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082424
    :cond_15
    iget-boolean v0, p1, LX/2RR;->j:Z

    move v0, v0

    .line 1082425
    if-eqz v0, :cond_16

    .line 1082426
    sget-object v0, LX/6NY;->IS_IN_CONTACT_LIST:LX/6NY;

    invoke-virtual {v0}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    const-string v3, "1"

    invoke-static {v0, v2, v3}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082427
    :cond_16
    iget-object v0, p1, LX/2RR;->m:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-object v0, v0

    .line 1082428
    if-eqz v0, :cond_17

    .line 1082429
    iget-object v0, p1, LX/2RR;->m:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-object v0, v0

    .line 1082430
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    if-eq v0, v2, :cond_21

    .line 1082431
    sget-object v2, LX/6NY;->VIEWER_CONNECTION_STATUS:LX/6NY;

    invoke-virtual {v2}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1082432
    :cond_17
    :goto_8
    iget-boolean v0, p1, LX/2RR;->l:Z

    move v0, v0

    .line 1082433
    if-nez v0, :cond_18

    .line 1082434
    sget-object v0, LX/6NY;->IS_MEMORIALIZED:LX/6NY;

    invoke-virtual {v0}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->NOT_EQUAL:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    const-string v3, "1"

    invoke-static {v0, v2, v3}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082435
    :cond_18
    iget-boolean v0, p1, LX/2RR;->k:Z

    move v0, v0

    .line 1082436
    if-eqz v0, :cond_19

    .line 1082437
    sget-object v0, LX/6NY;->IS_ZERO_COMMUNICATION_RANK:LX/6NY;

    invoke-virtual {v0}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    const-string v3, "1"

    invoke-static {v0, v2, v3}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1082438
    :cond_19
    invoke-static {v1}, Lcom/facebook/omnistore/IndexQuery;->and(Ljava/util/List;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v0

    move-object v1, v0

    .line 1082439
    iget-object v0, p1, LX/2RR;->n:LX/2RS;

    move-object v0, v0

    .line 1082440
    iget v2, p1, LX/2RR;->p:I

    move v2, v2

    .line 1082441
    invoke-virtual {p3}, Lcom/facebook/omnistore/Collection;->getSnapshotState()Lcom/facebook/omnistore/Omnistore$SnapshotState;

    .line 1082442
    sget-object v3, LX/2RS;->NO_SORT_ORDER:LX/2RS;

    if-eq v0, v3, :cond_1a

    sget-object v3, LX/2RS;->ID:LX/2RS;

    if-ne v0, v3, :cond_1b

    :cond_1a
    invoke-virtual {p3, v1, v2}, Lcom/facebook/omnistore/Collection;->queryWithIndex(Lcom/facebook/omnistore/IndexQuery;I)Lcom/facebook/omnistore/Cursor;

    move-result-object v0

    goto/16 :goto_1

    :cond_1b
    iget-object v3, v0, LX/2RS;->mOmnistoreIndexColumnName:Ljava/lang/String;

    .line 1082443
    iget-boolean v0, p1, LX/2RR;->o:Z

    move v0, v0

    .line 1082444
    if-eqz v0, :cond_1c

    sget-object v0, Lcom/facebook/omnistore/Collection$SortDirection;->DESCENDING:Lcom/facebook/omnistore/Collection$SortDirection;

    :goto_9
    invoke-virtual {p3, v1, v3, v0, v2}, Lcom/facebook/omnistore/Collection;->queryWithIndexSorted(Lcom/facebook/omnistore/IndexQuery;Ljava/lang/String;Lcom/facebook/omnistore/Collection$SortDirection;I)Lcom/facebook/omnistore/Cursor;

    move-result-object v0

    goto/16 :goto_1

    :cond_1c
    sget-object v0, Lcom/facebook/omnistore/Collection$SortDirection;->ASCENDING:Lcom/facebook/omnistore/Collection$SortDirection;

    goto :goto_9

    :cond_1d
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1082445
    :cond_1e
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1f
    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0PH;

    .line 1082446
    sget-object v6, LX/6N7;->a:LX/0P1;

    invoke-virtual {v6, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6NY;

    .line 1082447
    if-eqz v2, :cond_1f

    .line 1082448
    invoke-virtual {v2}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v2

    sget-object v6, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->GLOB:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v6, v7}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_20
    move-object v2, v3

    .line 1082449
    goto/16 :goto_4

    .line 1082450
    :cond_21
    sget-object v2, LX/6NY;->VIEWER_CONNECTION_STATUS:LX/6NY;

    invoke-virtual {v2}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v2

    sget-object v3, LX/6NY;->VIEWER_CONNECTION_STATUS:LX/6NY;

    invoke-virtual {v3}, LX/6NY;->getDbValue()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/omnistore/IndexQuery$QueryOperator;->EQUAL_TO:Lcom/facebook/omnistore/IndexQuery$QueryOperator;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/facebook/omnistore/IndexQuery;->predicate(Ljava/lang/String;Lcom/facebook/omnistore/IndexQuery$QueryOperator;Ljava/lang/String;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/omnistore/IndexQuery;->or(Ljava/util/List;)Lcom/facebook/omnistore/IndexQuery;

    move-result-object v2

    .line 1082451
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8
.end method
