.class public final enum LX/6eh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6eh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6eh;

.field public static final enum ADMIN:LX/6eh;

.field public static final enum AUDIO_CLIP:LX/6eh;

.field public static final enum CALL_LOG:LX/6eh;

.field public static final enum COMMERCE:LX/6eh;

.field public static final enum GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/6eh;

.field public static final enum GROUP_IMAGE_CHANGE:LX/6eh;

.field public static final enum GROUP_MEMBERSHIP_CHANGE:LX/6eh;

.field public static final enum GROUP_NAME_CHANGE:LX/6eh;

.field public static final enum MOMENTS_INVITE:LX/6eh;

.field public static final enum NORMAL:LX/6eh;

.field public static final enum PAYMENT:LX/6eh;

.field public static final enum PHOTOS:LX/6eh;

.field public static final enum STICKER:LX/6eh;

.field public static final enum TELEPHONE_COMMUNICATION_LOG:LX/6eh;

.field public static final enum VIDEO_CALL:LX/6eh;

.field public static final enum VIDEO_CLIP:LX/6eh;

.field public static final enum VOIP_CALL:LX/6eh;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1118083
    new-instance v0, LX/6eh;

    const-string v1, "GROUP_MEMBERSHIP_CHANGE"

    invoke-direct {v0, v1, v3}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->GROUP_MEMBERSHIP_CHANGE:LX/6eh;

    .line 1118084
    new-instance v0, LX/6eh;

    const-string v1, "GROUP_NAME_CHANGE"

    invoke-direct {v0, v1, v4}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->GROUP_NAME_CHANGE:LX/6eh;

    .line 1118085
    new-instance v0, LX/6eh;

    const-string v1, "GROUP_IMAGE_CHANGE"

    invoke-direct {v0, v1, v5}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->GROUP_IMAGE_CHANGE:LX/6eh;

    .line 1118086
    new-instance v0, LX/6eh;

    const-string v1, "VOIP_CALL"

    invoke-direct {v0, v1, v6}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->VOIP_CALL:LX/6eh;

    .line 1118087
    new-instance v0, LX/6eh;

    const-string v1, "VIDEO_CALL"

    invoke-direct {v0, v1, v7}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->VIDEO_CALL:LX/6eh;

    .line 1118088
    new-instance v0, LX/6eh;

    const-string v1, "STICKER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->STICKER:LX/6eh;

    .line 1118089
    new-instance v0, LX/6eh;

    const-string v1, "PHOTOS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->PHOTOS:LX/6eh;

    .line 1118090
    new-instance v0, LX/6eh;

    const-string v1, "AUDIO_CLIP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->AUDIO_CLIP:LX/6eh;

    .line 1118091
    new-instance v0, LX/6eh;

    const-string v1, "VIDEO_CLIP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->VIDEO_CLIP:LX/6eh;

    .line 1118092
    new-instance v0, LX/6eh;

    const-string v1, "PAYMENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->PAYMENT:LX/6eh;

    .line 1118093
    new-instance v0, LX/6eh;

    const-string v1, "MOMENTS_INVITE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->MOMENTS_INVITE:LX/6eh;

    .line 1118094
    new-instance v0, LX/6eh;

    const-string v1, "NORMAL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->NORMAL:LX/6eh;

    .line 1118095
    new-instance v0, LX/6eh;

    const-string v1, "ADMIN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->ADMIN:LX/6eh;

    .line 1118096
    new-instance v0, LX/6eh;

    const-string v1, "COMMERCE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->COMMERCE:LX/6eh;

    .line 1118097
    new-instance v0, LX/6eh;

    const-string v1, "CALL_LOG"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->CALL_LOG:LX/6eh;

    .line 1118098
    new-instance v0, LX/6eh;

    const-string v1, "GLOBALLY_DELETED_MESSAGE_PLACEHOLDER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/6eh;

    .line 1118099
    new-instance v0, LX/6eh;

    const-string v1, "TELEPHONE_COMMUNICATION_LOG"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/6eh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6eh;->TELEPHONE_COMMUNICATION_LOG:LX/6eh;

    .line 1118100
    const/16 v0, 0x11

    new-array v0, v0, [LX/6eh;

    sget-object v1, LX/6eh;->GROUP_MEMBERSHIP_CHANGE:LX/6eh;

    aput-object v1, v0, v3

    sget-object v1, LX/6eh;->GROUP_NAME_CHANGE:LX/6eh;

    aput-object v1, v0, v4

    sget-object v1, LX/6eh;->GROUP_IMAGE_CHANGE:LX/6eh;

    aput-object v1, v0, v5

    sget-object v1, LX/6eh;->VOIP_CALL:LX/6eh;

    aput-object v1, v0, v6

    sget-object v1, LX/6eh;->VIDEO_CALL:LX/6eh;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6eh;->STICKER:LX/6eh;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6eh;->PHOTOS:LX/6eh;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6eh;->AUDIO_CLIP:LX/6eh;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6eh;->VIDEO_CLIP:LX/6eh;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6eh;->PAYMENT:LX/6eh;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6eh;->MOMENTS_INVITE:LX/6eh;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6eh;->NORMAL:LX/6eh;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/6eh;->ADMIN:LX/6eh;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/6eh;->COMMERCE:LX/6eh;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/6eh;->CALL_LOG:LX/6eh;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/6eh;->GLOBALLY_DELETED_MESSAGE_PLACEHOLDER:LX/6eh;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/6eh;->TELEPHONE_COMMUNICATION_LOG:LX/6eh;

    aput-object v2, v0, v1

    sput-object v0, LX/6eh;->$VALUES:[LX/6eh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1118102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6eh;
    .locals 1

    .prologue
    .line 1118103
    const-class v0, LX/6eh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6eh;

    return-object v0
.end method

.method public static values()[LX/6eh;
    .locals 1

    .prologue
    .line 1118101
    sget-object v0, LX/6eh;->$VALUES:[LX/6eh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6eh;

    return-object v0
.end method
