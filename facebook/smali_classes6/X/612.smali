.class public final enum LX/612;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/612;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/612;

.field public static final enum CODEC_AUDIO_AAC:LX/612;

.field public static final enum CODEC_AUDIO_AMR_NB:LX/612;

.field public static final enum CODEC_AUDIO_AMR_WB:LX/612;

.field public static final enum CODEC_AUDIO_MP3:LX/612;

.field public static final enum CODEC_AUDIO_VORBIS:LX/612;

.field public static final enum CODEC_VIDEO_H263:LX/612;

.field public static final enum CODEC_VIDEO_H264:LX/612;

.field public static final enum CODEC_VIDEO_MPEG4:LX/612;

.field public static final enum CODEC_VIDEO_VP8:LX/612;

.field public static final enum CODEC_VIDEO_VP9:LX/612;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1039502
    new-instance v0, LX/612;

    const-string v1, "CODEC_VIDEO_VP8"

    const-string v2, "video/x-vnd.on2.vp8"

    invoke-direct {v0, v1, v4, v2}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_VIDEO_VP8:LX/612;

    .line 1039503
    new-instance v0, LX/612;

    const-string v1, "CODEC_VIDEO_VP9"

    const-string v2, "video/x-vnd.on2.vp9"

    invoke-direct {v0, v1, v5, v2}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_VIDEO_VP9:LX/612;

    .line 1039504
    new-instance v0, LX/612;

    const-string v1, "CODEC_VIDEO_H264"

    const-string v2, "video/avc"

    invoke-direct {v0, v1, v6, v2}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_VIDEO_H264:LX/612;

    .line 1039505
    new-instance v0, LX/612;

    const-string v1, "CODEC_VIDEO_MPEG4"

    const-string v2, "video/mp4v-es"

    invoke-direct {v0, v1, v7, v2}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_VIDEO_MPEG4:LX/612;

    .line 1039506
    new-instance v0, LX/612;

    const-string v1, "CODEC_VIDEO_H263"

    const-string v2, "video/3gpp"

    invoke-direct {v0, v1, v8, v2}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_VIDEO_H263:LX/612;

    .line 1039507
    new-instance v0, LX/612;

    const-string v1, "CODEC_AUDIO_AMR_NB"

    const/4 v2, 0x5

    const-string v3, "audio/3gpp"

    invoke-direct {v0, v1, v2, v3}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_AUDIO_AMR_NB:LX/612;

    .line 1039508
    new-instance v0, LX/612;

    const-string v1, "CODEC_AUDIO_AMR_WB"

    const/4 v2, 0x6

    const-string v3, "audio/amr-wb"

    invoke-direct {v0, v1, v2, v3}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_AUDIO_AMR_WB:LX/612;

    .line 1039509
    new-instance v0, LX/612;

    const-string v1, "CODEC_AUDIO_MP3"

    const/4 v2, 0x7

    const-string v3, "audio/mpeg"

    invoke-direct {v0, v1, v2, v3}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_AUDIO_MP3:LX/612;

    .line 1039510
    new-instance v0, LX/612;

    const-string v1, "CODEC_AUDIO_AAC"

    const/16 v2, 0x8

    const-string v3, "audio/mp4a-latm"

    invoke-direct {v0, v1, v2, v3}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_AUDIO_AAC:LX/612;

    .line 1039511
    new-instance v0, LX/612;

    const-string v1, "CODEC_AUDIO_VORBIS"

    const/16 v2, 0x9

    const-string v3, "audio/vorbis"

    invoke-direct {v0, v1, v2, v3}, LX/612;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/612;->CODEC_AUDIO_VORBIS:LX/612;

    .line 1039512
    const/16 v0, 0xa

    new-array v0, v0, [LX/612;

    sget-object v1, LX/612;->CODEC_VIDEO_VP8:LX/612;

    aput-object v1, v0, v4

    sget-object v1, LX/612;->CODEC_VIDEO_VP9:LX/612;

    aput-object v1, v0, v5

    sget-object v1, LX/612;->CODEC_VIDEO_H264:LX/612;

    aput-object v1, v0, v6

    sget-object v1, LX/612;->CODEC_VIDEO_MPEG4:LX/612;

    aput-object v1, v0, v7

    sget-object v1, LX/612;->CODEC_VIDEO_H263:LX/612;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/612;->CODEC_AUDIO_AMR_NB:LX/612;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/612;->CODEC_AUDIO_AMR_WB:LX/612;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/612;->CODEC_AUDIO_MP3:LX/612;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/612;->CODEC_AUDIO_AAC:LX/612;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/612;->CODEC_AUDIO_VORBIS:LX/612;

    aput-object v2, v0, v1

    sput-object v0, LX/612;->$VALUES:[LX/612;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1039513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1039514
    iput-object p3, p0, LX/612;->value:Ljava/lang/String;

    .line 1039515
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/612;
    .locals 1

    .prologue
    .line 1039516
    const-class v0, LX/612;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/612;

    return-object v0
.end method

.method public static values()[LX/612;
    .locals 1

    .prologue
    .line 1039517
    sget-object v0, LX/612;->$VALUES:[LX/612;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/612;

    return-object v0
.end method
