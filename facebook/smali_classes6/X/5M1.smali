.class public final LX/5M1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

.field public b:LX/4Zi;

.field public c:I


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;)V
    .locals 1
    .param p1    # Lcom/facebook/composer/publish/common/PendingStoryPersistentData;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 904567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 904568
    new-instance v0, LX/4Zi;

    invoke-direct {v0}, LX/4Zi;-><init>()V

    iput-object v0, p0, LX/5M1;->b:LX/4Zi;

    .line 904569
    const/16 v0, 0x64

    iput v0, p0, LX/5M1;->c:I

    .line 904570
    iput-object p1, p0, LX/5M1;->a:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    .line 904571
    return-void
.end method

.method public static a(Lcom/facebook/composer/publish/common/PendingStory;)LX/5M1;
    .locals 2

    .prologue
    .line 904572
    new-instance v0, LX/5M1;

    iget-object v1, p0, Lcom/facebook/composer/publish/common/PendingStory;->dbRepresentation:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    invoke-direct {v0, v1}, LX/5M1;-><init>(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;)V

    .line 904573
    iget-object v1, p0, Lcom/facebook/composer/publish/common/PendingStory;->b:LX/4Zi;

    iput-object v1, v0, LX/5M1;->b:LX/4Zi;

    .line 904574
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/publish/common/PendingStory;
    .locals 11

    .prologue
    .line 904575
    iget-object v0, p0, LX/5M1;->a:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 904576
    iget-object v0, p0, LX/5M1;->a:Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;->story:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v5, 0x0

    .line 904577
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    .line 904578
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    .line 904579
    :cond_0
    :goto_0
    new-instance v0, Lcom/facebook/composer/publish/common/PendingStory;

    invoke-direct {v0, p0}, Lcom/facebook/composer/publish/common/PendingStory;-><init>(LX/5M1;)V

    return-object v0

    .line 904580
    :cond_1
    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 904581
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 904582
    if-eqz v2, :cond_0

    .line 904583
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 904584
    if-eqz v2, :cond_0

    .line 904585
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x4ed245b

    if-ne v2, v3, :cond_0

    .line 904586
    iput v5, p0, LX/5M1;->c:I

    .line 904587
    iget-object v3, p0, LX/5M1;->b:LX/4Zi;

    const-wide/16 v4, 0x2710

    const-wide/16 v6, 0x1388

    const-wide/32 v8, 0x493e0

    .line 904588
    invoke-virtual {v3}, LX/4Zi;->b()Z

    move-result v10

    if-nez v10, :cond_2

    .line 904589
    iput-wide v4, v3, LX/4Zi;->g:J

    .line 904590
    iput-wide v6, v3, LX/4Zi;->i:J

    .line 904591
    iput-wide v8, v3, LX/4Zi;->h:J

    .line 904592
    :cond_2
    goto :goto_0
.end method
