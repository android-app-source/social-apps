.class public LX/6c4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/6c4;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/pm/PackageManager;

.field private final d:LX/0Sg;

.field public final e:LX/01T;

.field public f:LX/01U;

.field public g:Z

.field public h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/01U;Landroid/content/pm/PackageManager;LX/0Sg;LX/01T;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/01U;",
            "Landroid/content/pm/PackageManager;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1114145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114146
    iput-object p1, p0, LX/6c4;->a:Landroid/content/Context;

    .line 1114147
    iput-object p2, p0, LX/6c4;->b:LX/0Or;

    .line 1114148
    iput-object p3, p0, LX/6c4;->f:LX/01U;

    .line 1114149
    iput-object p4, p0, LX/6c4;->c:Landroid/content/pm/PackageManager;

    .line 1114150
    iput-object p5, p0, LX/6c4;->d:LX/0Sg;

    .line 1114151
    iput-object p6, p0, LX/6c4;->e:LX/01T;

    .line 1114152
    return-void
.end method

.method public static a(LX/0QB;)LX/6c4;
    .locals 10

    .prologue
    .line 1114153
    sget-object v0, LX/6c4;->i:LX/6c4;

    if-nez v0, :cond_1

    .line 1114154
    const-class v1, LX/6c4;

    monitor-enter v1

    .line 1114155
    :try_start_0
    sget-object v0, LX/6c4;->i:LX/6c4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1114156
    if-eqz v2, :cond_0

    .line 1114157
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1114158
    new-instance v3, LX/6c4;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0XV;->b(LX/0QB;)LX/01U;

    move-result-object v6

    check-cast v6, LX/01U;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v7

    check-cast v7, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v8

    check-cast v8, LX/0Sg;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v9

    check-cast v9, LX/01T;

    invoke-direct/range {v3 .. v9}, LX/6c4;-><init>(Landroid/content/Context;LX/0Or;LX/01U;Landroid/content/pm/PackageManager;LX/0Sg;LX/01T;)V

    .line 1114159
    move-object v0, v3

    .line 1114160
    sput-object v0, LX/6c4;->i:LX/6c4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1114161
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1114162
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1114163
    :cond_1
    sget-object v0, LX/6c4;->i:LX/6c4;

    return-object v0

    .line 1114164
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1114165
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6c4;Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1114166
    sget-object v1, LX/3RB;->l:Ljava/lang/String;

    iget-object v0, p0, LX/6c4;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1114167
    const/4 v0, 0x0

    .line 1114168
    iget-object v1, p0, LX/6c4;->c:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 1114169
    if-nez v0, :cond_1

    .line 1114170
    invoke-static {}, LX/0Vg;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1114171
    :goto_0
    return-object v0

    .line 1114172
    :cond_1
    iget-object v0, p0, LX/6c4;->e:LX/01T;

    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1114173
    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/6c4;->g:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1114174
    if-eqz v0, :cond_2

    .line 1114175
    iget-object v0, p0, LX/6c4;->d:LX/0Sg;

    const-string v1, "ChatHeadsInitializer initAfterUiIdle"

    new-instance v2, Lcom/facebook/messaging/chatheads/ipc/ChatHeadsBroadcaster$1;

    invoke-direct {v2, p0, p1}, Lcom/facebook/messaging/chatheads/ipc/ChatHeadsBroadcaster$1;-><init>(LX/6c4;Landroid/content/Intent;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    sget-object v4, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    move-result-object v0

    goto :goto_0

    .line 1114176
    :cond_2
    iget-object v0, p0, LX/6c4;->a:Landroid/content/Context;

    iget-object v1, p0, LX/6c4;->f:LX/01U;

    invoke-virtual {v1}, LX/01U;->getPermission()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1114177
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/6c4;ZLjava/lang/String;)V
    .locals 2
    .param p1    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1114178
    iget-object v0, p0, LX/6c4;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1114179
    iget-object v0, p0, LX/6c4;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1114180
    const/4 v0, 0x0

    iput-object v0, p0, LX/6c4;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1114181
    :cond_0
    new-instance v1, Landroid/content/Intent;

    if-eqz p1, :cond_2

    sget-object v0, LX/3RB;->k:Ljava/lang/String;

    :goto_0
    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1114182
    if-eqz p2, :cond_1

    .line 1114183
    sget-object v0, LX/3RB;->n:Ljava/lang/String;

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1114184
    :cond_1
    invoke-static {p0, v1}, LX/6c4;->a(LX/6c4;Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1114185
    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1114186
    :goto_1
    return-void

    .line 1114187
    :cond_2
    sget-object v0, LX/3RB;->i:Ljava/lang/String;

    goto :goto_0

    .line 1114188
    :cond_3
    iput-object v0, p0, LX/6c4;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1114189
    new-instance v1, LX/6c3;

    invoke-direct {v1, p0}, LX/6c3;-><init>(LX/6c4;)V

    .line 1114190
    sget-object p1, LX/131;->INSTANCE:LX/131;

    move-object p1, p1

    .line 1114191
    invoke-static {v0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1114192
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/6c4;->a(LX/6c4;ZLjava/lang/String;)V

    .line 1114193
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1114194
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/3RB;->j:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1114195
    invoke-static {p0, v0}, LX/6c4;->a(LX/6c4;Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1114196
    return-void
.end method
