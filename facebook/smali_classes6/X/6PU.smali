.class public abstract LX/6PU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/4Yr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1086182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086183
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086184
    iput-object p1, p0, LX/6PU;->a:Ljava/lang/String;

    .line 1086185
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086186
    iget-object v0, p0, LX/6PU;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1086187
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, LX/4Yr;

    .line 1086188
    iget-object v0, p0, LX/6PU;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6PU;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6PU;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1086189
    :goto_0
    return-void

    .line 1086190
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/6PU;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V

    goto :goto_0
.end method

.method public abstract a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086191
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method
