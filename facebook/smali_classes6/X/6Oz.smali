.class public final LX/6Oz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Ow;

.field public final synthetic b:LX/3ic;


# direct methods
.method public constructor <init>(LX/3ic;LX/6Ow;)V
    .locals 0

    .prologue
    .line 1085968
    iput-object p1, p0, LX/6Oz;->b:LX/3ic;

    iput-object p2, p0, LX/6Oz;->a:LX/6Ow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1085969
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1085970
    if-eqz p1, :cond_0

    .line 1085971
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1085972
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/6Oz;->a:LX/6Ow;

    .line 1085973
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1085974
    invoke-interface {v0, v1}, LX/6Ow;->a(Ljava/lang/Object;)Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentHideUnhideFragmentModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1085975
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
