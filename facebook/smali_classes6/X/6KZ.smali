.class public LX/6KZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:[F

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6KY;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/5Pc;

.field public d:LX/6KY;

.field public e:Z

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>(LX/5Pc;LX/7Se;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1076795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076796
    iput-boolean v2, p0, LX/6KZ;->e:Z

    .line 1076797
    iput v2, p0, LX/6KZ;->f:I

    .line 1076798
    iput v2, p0, LX/6KZ;->g:I

    .line 1076799
    iput-object p1, p0, LX/6KZ;->c:LX/5Pc;

    .line 1076800
    new-instance v0, LX/6KY;

    invoke-direct {v0, p2}, LX/6KY;-><init>(LX/61B;)V

    iput-object v0, p0, LX/6KZ;->d:LX/6KY;

    .line 1076801
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6KZ;->b:Ljava/util/List;

    .line 1076802
    iget-object v0, p0, LX/6KZ;->b:Ljava/util/List;

    iget-object v1, p0, LX/6KZ;->d:LX/6KY;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1076803
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, LX/6KZ;->a:[F

    .line 1076804
    iget-object v0, p0, LX/6KZ;->a:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1076805
    return-void
.end method

.method private static a(LX/6KZ;LX/6Jg;LX/6KY;LX/5Pf;LX/5PO;[F[F[F)V
    .locals 8

    .prologue
    const v7, 0x8d40

    const/4 v6, 0x0

    .line 1076806
    if-eqz p3, :cond_0

    .line 1076807
    iget v0, p3, LX/5Pf;->a:I

    iget v1, p3, LX/5Pf;->b:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1076808
    :cond_0
    iget v0, p4, LX/5PO;->c:I

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 1076809
    invoke-static {v6}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1076810
    iget v0, p4, LX/5PO;->a:I

    iget v1, p4, LX/5PO;->b:I

    invoke-static {v6, v6, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1076811
    invoke-direct {p0, p5}, LX/6KZ;->a([F)[F

    move-result-object v1

    invoke-direct {p0, p6}, LX/6KZ;->a([F)[F

    move-result-object v2

    invoke-direct {p0, p7}, LX/6KZ;->a([F)[F

    move-result-object v3

    invoke-interface {p1}, LX/6Jg;->b()J

    move-result-wide v4

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, LX/6KY;->a([F[F[FJ)V

    .line 1076812
    invoke-static {v7, v6}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 1076813
    const/16 v0, 0xde1

    invoke-static {v0, v6}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1076814
    return-void
.end method

.method private static a(LX/6KZ;LX/6KY;LX/5Pf;)V
    .locals 2

    .prologue
    .line 1076815
    iget v0, p2, LX/5Pf;->a:I

    const v1, 0x8d65

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, LX/6KY;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1076816
    const/4 v0, 0x1

    iget-object v1, p0, LX/6KZ;->c:LX/5Pc;

    invoke-virtual {p1, v0, v1}, LX/6KY;->a(ZLX/5Pc;)V

    .line 1076817
    :cond_0
    iget v0, p2, LX/5Pf;->a:I

    const/16 v1, 0xde1

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, LX/6KY;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1076818
    const/4 v0, 0x0

    iget-object v1, p0, LX/6KZ;->c:LX/5Pc;

    invoke-virtual {p1, v0, v1}, LX/6KY;->a(ZLX/5Pc;)V

    .line 1076819
    :cond_1
    return-void
.end method

.method private a([F)[F
    .locals 0

    .prologue
    .line 1076820
    if-eqz p1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    iget-object p1, p0, LX/6KZ;->a:[F

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6Jg;LX/5Pf;LX/6Kc;[F[F[F)LX/5PO;
    .locals 15

    .prologue
    .line 1076821
    const/4 v13, 0x0

    .line 1076822
    const/4 v5, 0x0

    .line 1076823
    const/4 v1, 0x0

    move v14, v1

    move-object/from16 v4, p2

    :goto_0
    :try_start_0
    iget-object v1, p0, LX/6KZ;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v14, v1, :cond_8

    .line 1076824
    iget-object v1, p0, LX/6KZ;->b:Ljava/util/List;

    invoke-interface {v1, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6KY;

    .line 1076825
    if-nez v14, :cond_9

    .line 1076826
    invoke-virtual {v3}, LX/6KY;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1076827
    move-object/from16 v0, p2

    invoke-static {p0, v3, v0}, LX/6KZ;->a(LX/6KZ;LX/6KY;LX/5Pf;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-object v9, v5

    .line 1076828
    :goto_1
    :try_start_1
    invoke-virtual {v3}, LX/6KY;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1076829
    invoke-virtual {v3}, LX/6KY;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1076830
    invoke-virtual/range {p3 .. p3}, LX/6Kc;->e()LX/5PO;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    .line 1076831
    if-nez v14, :cond_1

    move-object/from16 v6, p4

    :goto_2
    if-nez v14, :cond_2

    move-object/from16 v7, p5

    :goto_3
    if-nez v14, :cond_3

    move-object/from16 v8, p6

    :goto_4
    move-object v1, p0

    move-object/from16 v2, p1

    :try_start_2
    invoke-static/range {v1 .. v8}, LX/6KZ;->a(LX/6KZ;LX/6Jg;LX/6KY;LX/5Pf;LX/5PO;[F[F[F)V

    .line 1076832
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, LX/6Kc;->a(LX/5PO;)V

    .line 1076833
    iget-object v4, v5, LX/5PO;->d:LX/5Pf;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1076834
    :try_start_3
    const-string v1, "effectmanager::onDrawFrame - rendering chainable"

    invoke-static {v1}, LX/5PP;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-object v9, v5

    .line 1076835
    :goto_5
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    move-object v13, v5

    move-object v5, v9

    goto :goto_0

    .line 1076836
    :cond_0
    :try_start_4
    iget-object v1, p0, LX/6KZ;->d:LX/6KY;

    move-object/from16 v0, p2

    invoke-static {p0, v1, v0}, LX/6KZ;->a(LX/6KZ;LX/6KY;LX/5Pf;)V

    .line 1076837
    invoke-virtual/range {p3 .. p3}, LX/6Kc;->e()LX/5PO;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result-object v8

    .line 1076838
    :try_start_5
    iget-object v6, p0, LX/6KZ;->d:LX/6KY;

    move-object v4, p0

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    invoke-static/range {v4 .. v11}, LX/6KZ;->a(LX/6KZ;LX/6Jg;LX/6KY;LX/5Pf;LX/5PO;[F[F[F)V

    .line 1076839
    iget-object v4, v8, LX/5PO;->d:LX/5Pf;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 1076840
    :try_start_6
    const-string v1, "effectmanager::onDrawFrame - preparing fbt"

    invoke-static {v1}, LX/5PP;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    move-object v9, v8

    move-object v13, v8

    goto :goto_1

    .line 1076841
    :cond_1
    :try_start_7
    iget-object v6, p0, LX/6KZ;->a:[F

    goto :goto_2

    :cond_2
    iget-object v7, p0, LX/6KZ;->a:[F

    goto :goto_3

    :cond_3
    iget-object v8, p0, LX/6KZ;->a:[F
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_4

    .line 1076842
    :cond_4
    const/4 v8, 0x0

    move-object v5, p0

    move-object/from16 v6, p1

    move-object v7, v3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    :try_start_8
    invoke-static/range {v5 .. v12}, LX/6KZ;->a(LX/6KZ;LX/6Jg;LX/6KY;LX/5Pf;LX/5PO;[F[F[F)V

    .line 1076843
    const-string v1, "effectmanager::onDrawFrame - rendering non chainable"

    invoke-static {v1}, LX/5PP;->a(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    :cond_5
    move-object v5, v13

    goto :goto_5

    .line 1076844
    :catch_0
    move-exception v1

    move-object v5, v8

    move-object v13, v8

    .line 1076845
    :goto_6
    if-eqz v5, :cond_6

    .line 1076846
    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, LX/6Kc;->a(LX/5PO;)V

    .line 1076847
    :cond_6
    if-eqz v13, :cond_7

    if-eq v13, v5, :cond_7

    .line 1076848
    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, LX/6Kc;->a(LX/5PO;)V

    .line 1076849
    :cond_7
    invoke-static {v1}, LX/0V9;->b(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1

    .line 1076850
    :cond_8
    return-object v5

    .line 1076851
    :catch_1
    move-exception v1

    move-object v5, v9

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    move-object v13, v5

    goto :goto_6

    :catch_4
    move-exception v1

    move-object v5, v8

    goto :goto_6

    :cond_9
    move-object v9, v5

    goto/16 :goto_1
.end method
