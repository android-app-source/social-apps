.class public LX/5Oq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 909467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 909468
    iput-object p1, p0, LX/5Oq;->a:LX/0ad;

    .line 909469
    return-void
.end method

.method public static a(LX/0QB;)LX/5Oq;
    .locals 4

    .prologue
    .line 909456
    const-class v1, LX/5Oq;

    monitor-enter v1

    .line 909457
    :try_start_0
    sget-object v0, LX/5Oq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 909458
    sput-object v2, LX/5Oq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 909459
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 909460
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 909461
    new-instance p0, LX/5Oq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/5Oq;-><init>(LX/0ad;)V

    .line 909462
    move-object v0, p0

    .line 909463
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 909464
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/5Oq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 909465
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 909466
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
