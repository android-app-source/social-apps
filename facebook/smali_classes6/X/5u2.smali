.class public final LX/5u2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1017815
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1017816
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1017817
    :goto_0
    return v1

    .line 1017818
    :cond_0
    const-string v8, "page_rating"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1017819
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    .line 1017820
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 1017821
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1017822
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1017823
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 1017824
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1017825
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1017826
    :cond_2
    const-string v8, "privacy_scope"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1017827
    invoke-static {p0, p1}, LX/5uE;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1017828
    :cond_3
    const-string v8, "value"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1017829
    invoke-static {p0, p1}, LX/5u1;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1017830
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1017831
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1017832
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1017833
    if-eqz v0, :cond_6

    .line 1017834
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 1017835
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1017836
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1017837
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1017838
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1017839
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1017840
    if-eqz v0, :cond_0

    .line 1017841
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1017842
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1017843
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1017844
    if-eqz v0, :cond_1

    .line 1017845
    const-string v1, "page_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1017846
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1017847
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1017848
    if-eqz v0, :cond_2

    .line 1017849
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1017850
    invoke-static {p0, v0, p2, p3}, LX/5uE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1017851
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1017852
    if-eqz v0, :cond_3

    .line 1017853
    const-string v1, "value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1017854
    invoke-static {p0, v0, p2}, LX/5u1;->a(LX/15i;ILX/0nX;)V

    .line 1017855
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1017856
    return-void
.end method
