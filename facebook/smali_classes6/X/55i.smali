.class public final LX/55i;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 829626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPageInfo;)LX/1vs;
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAppendOnlyPageInfoGraphQL"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 829627
    if-nez p0, :cond_0

    .line 829628
    invoke-static {v2, v3}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    .line 829629
    :goto_0
    return-object v0

    .line 829630
    :cond_0
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 829631
    const/4 v1, 0x0

    .line 829632
    if-nez p0, :cond_3

    .line 829633
    :goto_1
    move v1, v1

    .line 829634
    if-nez v1, :cond_1

    .line 829635
    invoke-static {v2, v3}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 829636
    :cond_1
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 829637
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 829638
    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 829639
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 829640
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 829641
    const-string v1, "AppendOnlyGraphQLObjectCollectionConverter.getAppendOnlyPageInfoGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 829642
    :cond_2
    const v1, -0x4f3779d1

    .line 829643
    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {v2}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v2

    .line 829644
    invoke-static {v0, v2, v1}, LX/1vs;->a(LX/15i;II)LX/1vs;

    move-result-object v2

    move-object v0, v2

    .line 829645
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 829646
    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 829647
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 829648
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 829649
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->p_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 829650
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 829651
    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 829652
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v4

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 829653
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->c()Z

    move-result v4

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 829654
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 829655
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 829656
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto :goto_1
.end method
