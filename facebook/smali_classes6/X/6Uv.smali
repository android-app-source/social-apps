.class public LX/6Uv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile e:LX/6Uv;


# instance fields
.field private final c:LX/6V4;

.field public final d:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1103187
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "LEFT_OF"

    aput-object v1, v0, v3

    const-string v1, "RIGHT_OF"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "ABOVE"

    aput-object v2, v0, v1

    const-string v1, "BELOW"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "ALIGN_BASELINE"

    aput-object v2, v0, v1

    const-string v1, "ALIGN_LEFT"

    aput-object v1, v0, v6

    const/4 v1, 0x6

    const-string v2, "ALIGN_TOP"

    aput-object v2, v0, v1

    const-string v1, "ALIGN_RIGHT"

    aput-object v1, v0, v7

    const/16 v1, 0x8

    const-string v2, "ALIGN_BOTTOM"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ALIGN_PARENT_LEFT"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "ALIGN_PARENT_TOP"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "ALIGN_PARENT_RIGHT"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ALIGN_PARENT_BOTTOM"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CENTER_IN_PARENT"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CENTER_HORIZONTAL"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "CENTER_VERTICAL"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "START_OF"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "END_OF"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "ALIGN_START"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ALIGN_END"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "ALIGN_PARENT_START"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ALIGN_PARENT_END"

    aput-object v2, v0, v1

    sput-object v0, LX/6Uv;->a:[Ljava/lang/String;

    .line 1103188
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "NO_GRAVITY"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/16 v1, 0x30

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "TOP"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/16 v1, 0x50

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "BOTTOM"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "LEFT"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "RIGHT"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x800003

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "START"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const v1, 0x800005

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "END"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CENTER_VERTICAL"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/16 v1, 0x70

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "FILL_VERTICAL"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CENTER_HORIZONTAL"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "FILL_HORIZONTAL"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CENTER"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const/16 v1, 0x77

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "FILL"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/6Uv;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/6V4;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103190
    iput-object p1, p0, LX/6Uv;->c:LX/6V4;

    .line 1103191
    iput-object p2, p0, LX/6Uv;->d:Landroid/content/Context;

    .line 1103192
    return-void
.end method

.method public static a(LX/0QB;)LX/6Uv;
    .locals 5

    .prologue
    .line 1103193
    sget-object v0, LX/6Uv;->e:LX/6Uv;

    if-nez v0, :cond_1

    .line 1103194
    const-class v1, LX/6Uv;

    monitor-enter v1

    .line 1103195
    :try_start_0
    sget-object v0, LX/6Uv;->e:LX/6Uv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1103196
    if-eqz v2, :cond_0

    .line 1103197
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1103198
    new-instance p0, LX/6Uv;

    invoke-static {v0}, LX/6V4;->a(LX/0QB;)LX/6V4;

    move-result-object v3

    check-cast v3, LX/6V4;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {p0, v3, v4}, LX/6Uv;-><init>(LX/6V4;Landroid/content/Context;)V

    .line 1103199
    move-object v0, p0

    .line 1103200
    sput-object v0, LX/6Uv;->e:LX/6Uv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1103201
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1103202
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1103203
    :cond_1
    sget-object v0, LX/6Uv;->e:LX/6Uv;

    return-object v0

    .line 1103204
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1103205
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103206
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 1103207
    const-string v0, "MATCH_PARENT"

    .line 1103208
    :goto_0
    return-object v0

    .line 1103209
    :cond_0
    const/4 v0, -0x2

    if-ne p0, v0, :cond_1

    .line 1103210
    const-string v0, "WRAP_CONTENT"

    goto :goto_0

    .line 1103211
    :cond_1
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1103212
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 1103213
    const-string v0, "NONE"

    .line 1103214
    :goto_0
    return-object v0

    .line 1103215
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1103216
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1103217
    sget-object v0, LX/6Uv;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1103218
    and-int v4, v0, p0

    if-ne v4, v0, :cond_1

    .line 1103219
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1103220
    :cond_2
    const/16 v0, 0x11

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1103221
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1103222
    const/16 v0, 0x10

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1103223
    :cond_3
    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1103224
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1103225
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1103226
    :cond_4
    const/16 v0, 0x70

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1103227
    const/16 v0, 0x30

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1103228
    const/16 v0, 0x50

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1103229
    :cond_5
    const-string v0, "|"

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0PO;->join([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1103230
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1103231
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
