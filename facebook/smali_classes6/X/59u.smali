.class public final LX/59u;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 854653
    const/4 v15, 0x0

    .line 854654
    const/4 v14, 0x0

    .line 854655
    const/4 v13, 0x0

    .line 854656
    const/4 v12, 0x0

    .line 854657
    const/4 v11, 0x0

    .line 854658
    const/4 v10, 0x0

    .line 854659
    const/4 v9, 0x0

    .line 854660
    const/4 v8, 0x0

    .line 854661
    const/4 v7, 0x0

    .line 854662
    const/4 v6, 0x0

    .line 854663
    const/4 v5, 0x0

    .line 854664
    const/4 v4, 0x0

    .line 854665
    const/4 v3, 0x0

    .line 854666
    const/4 v2, 0x0

    .line 854667
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 854668
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 854669
    const/4 v2, 0x0

    .line 854670
    :goto_0
    return v2

    .line 854671
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 854672
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_b

    .line 854673
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 854674
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 854675
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 854676
    const-string v17, "__type__"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    const-string v17, "__typename"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 854677
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v15

    goto :goto_1

    .line 854678
    :cond_3
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 854679
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 854680
    :cond_4
    const-string v17, "is_verified"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 854681
    const/4 v6, 0x1

    .line 854682
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 854683
    :cond_5
    const-string v17, "live_video_subscription_status"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 854684
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLiveVideoSubscriptionStatus;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto :goto_1

    .line 854685
    :cond_6
    const-string v17, "name"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 854686
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 854687
    :cond_7
    const-string v17, "video_channel_can_viewer_follow"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 854688
    const/4 v5, 0x1

    .line 854689
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 854690
    :cond_8
    const-string v17, "video_channel_can_viewer_subscribe"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 854691
    const/4 v4, 0x1

    .line 854692
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 854693
    :cond_9
    const-string v17, "video_channel_has_viewer_subscribed"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 854694
    const/4 v3, 0x1

    .line 854695
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 854696
    :cond_a
    const-string v17, "video_channel_is_viewer_following"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 854697
    const/4 v2, 0x1

    .line 854698
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 854699
    :cond_b
    const/16 v16, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 854700
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 854701
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 854702
    if-eqz v6, :cond_c

    .line 854703
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->a(IZ)V

    .line 854704
    :cond_c
    const/4 v6, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 854705
    const/4 v6, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11}, LX/186;->b(II)V

    .line 854706
    if-eqz v5, :cond_d

    .line 854707
    const/4 v5, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10}, LX/186;->a(IZ)V

    .line 854708
    :cond_d
    if-eqz v4, :cond_e

    .line 854709
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->a(IZ)V

    .line 854710
    :cond_e
    if-eqz v3, :cond_f

    .line 854711
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 854712
    :cond_f
    if-eqz v2, :cond_10

    .line 854713
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->a(IZ)V

    .line 854714
    :cond_10
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 854715
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 854716
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 854717
    if-eqz v0, :cond_0

    .line 854718
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854719
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 854720
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 854721
    if-eqz v0, :cond_1

    .line 854722
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854723
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 854724
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 854725
    if-eqz v0, :cond_2

    .line 854726
    const-string v1, "is_verified"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854727
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 854728
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 854729
    if-eqz v0, :cond_3

    .line 854730
    const-string v0, "live_video_subscription_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854731
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 854732
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 854733
    if-eqz v0, :cond_4

    .line 854734
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854735
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 854736
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 854737
    if-eqz v0, :cond_5

    .line 854738
    const-string v1, "video_channel_can_viewer_follow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854739
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 854740
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 854741
    if-eqz v0, :cond_6

    .line 854742
    const-string v1, "video_channel_can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854743
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 854744
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 854745
    if-eqz v0, :cond_7

    .line 854746
    const-string v1, "video_channel_has_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854747
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 854748
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 854749
    if-eqz v0, :cond_8

    .line 854750
    const-string v1, "video_channel_is_viewer_following"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854751
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 854752
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 854753
    return-void
.end method
