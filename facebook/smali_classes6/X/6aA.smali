.class public LX/6aA;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:D


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1112184
    const-wide v0, 0x412e848000000000L    # 1000000.0

    sput-wide v0, LX/6aA;->a:D

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1112185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(DD)Landroid/location/Location;
    .locals 2

    .prologue
    .line 1112186
    new-instance v0, Landroid/location/Location;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1112187
    invoke-virtual {v0, p0, p1}, Landroid/location/Location;->setLatitude(D)V

    .line 1112188
    invoke-virtual {v0, p2, p3}, Landroid/location/Location;->setLongitude(D)V

    .line 1112189
    return-object v0
.end method
