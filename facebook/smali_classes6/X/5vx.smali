.class public final LX/5vx;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1023320
    const-class v1, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    const v0, 0xe809495

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "ProfileVideoDetailQuery"

    const-string v6, "2f58aa2ab62cdc7af76c8d2eff4eed1d"

    const-string v7, "video"

    const-string v8, "10155072967176729"

    const-string v9, "10155259089876729"

    .line 1023321
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1023322
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1023323
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1023324
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1023325
    sparse-switch v0, :sswitch_data_0

    .line 1023326
    :goto_0
    return-object p1

    .line 1023327
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1023328
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1023329
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1023330
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1023331
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1023332
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1023333
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1023334
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x69b6761e -> :sswitch_3
        -0x25a646c8 -> :sswitch_0
        -0x2177e47b -> :sswitch_1
        0x1918b88b -> :sswitch_2
        0x2292beef -> :sswitch_6
        0x26d0c0ff -> :sswitch_5
        0x44a0c75f -> :sswitch_7
        0x73a026b5 -> :sswitch_4
    .end sparse-switch
.end method
