.class public abstract LX/53w;
.super LX/53v;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/53v",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final f:J

.field private static final g:I


# instance fields
.field public final e:[J


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 827630
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    const-class v1, [J

    invoke-virtual {v0, v1}, Lsun/misc/Unsafe;->arrayIndexScale(Ljava/lang/Class;)I

    move-result v0

    .line 827631
    const/16 v1, 0x8

    if-ne v1, v0, :cond_0

    .line 827632
    sget v0, LX/53v;->a:I

    add-int/lit8 v0, v0, 0x3

    sput v0, LX/53w;->g:I

    .line 827633
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    const-class v1, [J

    invoke-virtual {v0, v1}, Lsun/misc/Unsafe;->arrayBaseOffset(Ljava/lang/Class;)I

    move-result v0

    const/16 v1, 0x20

    sget v2, LX/53w;->g:I

    sget v3, LX/53v;->a:I

    sub-int/2addr v2, v3

    shl-int/2addr v1, v2

    add-int/2addr v0, v1

    int-to-long v0, v0

    sput-wide v0, LX/53w;->f:J

    .line 827634
    return-void

    .line 827635
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected long[] element size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(I)V
    .locals 6

    .prologue
    .line 827636
    invoke-direct {p0, p1}, LX/53v;-><init>(I)V

    .line 827637
    iget v0, p0, LX/53v;->b:I

    sget v1, LX/53v;->a:I

    shl-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x40

    new-array v0, v0, [J

    iput-object v0, p0, LX/53w;->e:[J

    .line 827638
    const-wide/16 v0, 0x0

    :goto_0
    iget v2, p0, LX/53v;->b:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 827639
    iget-object v2, p0, LX/53w;->e:[J

    invoke-virtual {p0, v0, v1}, LX/53w;->d(J)J

    move-result-wide v4

    invoke-static {v2, v4, v5, v0, v1}, LX/53w;->a([JJJ)V

    .line 827640
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    goto :goto_0

    .line 827641
    :cond_0
    return-void
.end method

.method public static a([JJ)J
    .locals 3

    .prologue
    .line 827642
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    invoke-virtual {v0, p0, p1, p2}, Lsun/misc/Unsafe;->getLongVolatile(Ljava/lang/Object;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a([JJJ)V
    .locals 7

    .prologue
    .line 827643
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->putOrderedLong(Ljava/lang/Object;JJ)V

    .line 827644
    return-void
.end method


# virtual methods
.method public final d(J)J
    .locals 5

    .prologue
    .line 827645
    sget-wide v0, LX/53w;->f:J

    iget-wide v2, p0, LX/53v;->c:J

    and-long/2addr v2, p1

    sget v4, LX/53w;->g:I

    shl-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method
