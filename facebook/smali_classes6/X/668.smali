.class public final LX/668;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/64r;


# instance fields
.field private final a:LX/64g;


# direct methods
.method public constructor <init>(LX/64g;)V
    .locals 0

    .prologue
    .line 1049417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049418
    iput-object p1, p0, LX/668;->a:LX/64g;

    .line 1049419
    return-void
.end method


# virtual methods
.method public final a(LX/66O;)LX/655;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 1049420
    iget-object v1, p1, LX/66O;->f:LX/650;

    move-object v1, v1

    .line 1049421
    invoke-virtual {v1}, LX/650;->newBuilder()LX/64z;

    move-result-object v2

    .line 1049422
    iget-object v3, v1, LX/650;->d:LX/651;

    move-object v3, v3

    .line 1049423
    if-eqz v3, :cond_1

    .line 1049424
    invoke-virtual {v3}, LX/651;->a()LX/64s;

    move-result-object v4

    .line 1049425
    if-eqz v4, :cond_0

    .line 1049426
    const-string v5, "Content-Type"

    invoke-virtual {v4}, LX/64s;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 1049427
    :cond_0
    invoke-virtual {v3}, LX/651;->b()J

    move-result-wide v4

    .line 1049428
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_a

    .line 1049429
    const-string v3, "Content-Length"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 1049430
    const-string v3, "Transfer-Encoding"

    invoke-virtual {v2, v3}, LX/64z;->b(Ljava/lang/String;)LX/64z;

    .line 1049431
    :cond_1
    :goto_0
    const-string v3, "Host"

    invoke-virtual {v1, v3}, LX/650;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 1049432
    const-string v3, "Host"

    .line 1049433
    iget-object v4, v1, LX/650;->a:LX/64q;

    move-object v4, v4

    .line 1049434
    invoke-static {v4, v0}, LX/65A;->a(LX/64q;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 1049435
    :cond_2
    const-string v3, "Connection"

    invoke-virtual {v1, v3}, LX/650;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 1049436
    const-string v3, "Connection"

    const-string v4, "Keep-Alive"

    invoke-virtual {v2, v3, v4}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 1049437
    :cond_3
    const-string v3, "Accept-Encoding"

    invoke-virtual {v1, v3}, LX/650;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    .line 1049438
    const/4 v0, 0x1

    .line 1049439
    const-string v3, "Accept-Encoding"

    const-string v4, "gzip"

    invoke-virtual {v2, v3, v4}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 1049440
    :cond_4
    iget-object v3, p0, LX/668;->a:LX/64g;

    .line 1049441
    iget-object v4, v1, LX/650;->a:LX/64q;

    move-object v4, v4

    .line 1049442
    invoke-interface {v3, v4}, LX/64g;->a(LX/64q;)Ljava/util/List;

    move-result-object v3

    .line 1049443
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    .line 1049444
    const-string v4, "Cookie"

    .line 1049445
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1049446
    const/4 v5, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    move v6, v5

    :goto_1
    if-ge v6, v8, :cond_6

    .line 1049447
    if-lez v6, :cond_5

    .line 1049448
    const-string v5, "; "

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1049449
    :cond_5
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/64f;

    .line 1049450
    iget-object v9, v5, LX/64f;->e:Ljava/lang/String;

    move-object v9, v9

    .line 1049451
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x3d

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1049452
    iget-object v10, v5, LX/64f;->f:Ljava/lang/String;

    move-object v5, v10

    .line 1049453
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1049454
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 1049455
    :cond_6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v3, v5

    .line 1049456
    invoke-virtual {v2, v4, v3}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 1049457
    :cond_7
    const-string v3, "User-Agent"

    invoke-virtual {v1, v3}, LX/650;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_8

    .line 1049458
    const-string v3, "User-Agent"

    .line 1049459
    const-string v4, "okhttp/3.4.1"

    move-object v4, v4

    .line 1049460
    invoke-virtual {v2, v3, v4}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 1049461
    :cond_8
    invoke-virtual {v2}, LX/64z;->b()LX/650;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/66O;->a(LX/650;)LX/655;

    move-result-object v2

    .line 1049462
    iget-object v3, p0, LX/668;->a:LX/64g;

    .line 1049463
    iget-object v4, v1, LX/650;->a:LX/64q;

    move-object v4, v4

    .line 1049464
    iget-object v5, v2, LX/655;->f:LX/64n;

    move-object v5, v5

    .line 1049465
    invoke-static {v3, v4, v5}, LX/66M;->a(LX/64g;LX/64q;LX/64n;)V

    .line 1049466
    invoke-virtual {v2}, LX/655;->newBuilder()LX/654;

    move-result-object v3

    .line 1049467
    iput-object v1, v3, LX/654;->a:LX/650;

    .line 1049468
    move-object v1, v3

    .line 1049469
    if-eqz v0, :cond_9

    const-string v0, "gzip"

    const-string v3, "Content-Encoding"

    .line 1049470
    invoke-virtual {v2, v3}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1049471
    invoke-static {v2}, LX/66M;->b(LX/655;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1049472
    new-instance v0, LX/676;

    .line 1049473
    iget-object v3, v2, LX/655;->g:LX/656;

    move-object v3, v3

    .line 1049474
    invoke-virtual {v3}, LX/656;->d()LX/671;

    move-result-object v3

    invoke-direct {v0, v3}, LX/676;-><init>(LX/65D;)V

    .line 1049475
    iget-object v3, v2, LX/655;->f:LX/64n;

    move-object v2, v3

    .line 1049476
    invoke-virtual {v2}, LX/64n;->newBuilder()LX/64m;

    move-result-object v2

    const-string v3, "Content-Encoding"

    .line 1049477
    invoke-virtual {v2, v3}, LX/64m;->b(Ljava/lang/String;)LX/64m;

    move-result-object v2

    const-string v3, "Content-Length"

    .line 1049478
    invoke-virtual {v2, v3}, LX/64m;->b(Ljava/lang/String;)LX/64m;

    move-result-object v2

    .line 1049479
    invoke-virtual {v2}, LX/64m;->a()LX/64n;

    move-result-object v2

    .line 1049480
    invoke-virtual {v1, v2}, LX/654;->a(LX/64n;)LX/654;

    .line 1049481
    new-instance v3, LX/66P;

    invoke-static {v0}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v0

    invoke-direct {v3, v2, v0}, LX/66P;-><init>(LX/64n;LX/671;)V

    .line 1049482
    iput-object v3, v1, LX/654;->g:LX/656;

    .line 1049483
    :cond_9
    invoke-virtual {v1}, LX/654;->a()LX/655;

    move-result-object v0

    return-object v0

    .line 1049484
    :cond_a
    const-string v3, "Transfer-Encoding"

    const-string v4, "chunked"

    invoke-virtual {v2, v3, v4}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    .line 1049485
    const-string v3, "Content-Length"

    invoke-virtual {v2, v3}, LX/64z;->b(Ljava/lang/String;)LX/64z;

    goto/16 :goto_0
.end method
