.class public LX/6Yw;
.super LX/6Ya;
.source ""


# instance fields
.field public final c:LX/4g9;

.field public d:LX/6YV;

.field public e:Z


# direct methods
.method public constructor <init>(LX/6YV;LX/4g9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110709
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1110710
    iput-object p1, p0, LX/6Yw;->d:LX/6YV;

    .line 1110711
    iput-object p2, p0, LX/6Yw;->c:LX/4g9;

    .line 1110712
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Yw;->e:Z

    .line 1110713
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 7

    .prologue
    .line 1110714
    new-instance v2, LX/6YP;

    invoke-direct {v2, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1110715
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110716
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->u:Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    move-object v3, v1

    .line 1110717
    if-nez v3, :cond_3

    const-string v0, ""

    .line 1110718
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, LX/6Ys;

    invoke-direct {v1, p0, v0}, LX/6Ys;-><init>(LX/6Yw;Ljava/lang/String;)V

    move-object v0, v1

    .line 1110719
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v4, 0x0

    .line 1110720
    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 1110721
    iget-object v5, v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1110722
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1110723
    iget-object v5, v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1110724
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1110725
    :cond_0
    :goto_2
    move-object v1, v4

    .line 1110726
    new-instance v4, LX/6Y5;

    invoke-direct {v4}, LX/6Y5;-><init>()V

    iget-object v5, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v5}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->q()I

    move-result v5

    .line 1110727
    iput v5, v4, LX/6Y5;->s:I

    .line 1110728
    move-object v4, v4

    .line 1110729
    if-eqz v1, :cond_5

    .line 1110730
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v5, 0x7f080e11

    invoke-virtual {v0, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LX/6Yw;->d()Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, LX/6Y5;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v0

    iget-object v5, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v6, 0x7f080e0b

    invoke-virtual {v5, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, LX/6Ya;->c()Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v0

    .line 1110731
    iput-object v1, v0, LX/6Y5;->d:Landroid/text/Spannable;

    .line 1110732
    move-object v0, v0

    .line 1110733
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Y5;->a(Ljava/lang/Boolean;)LX/6Y5;

    .line 1110734
    :goto_3
    iget-object v0, p0, LX/6Yw;->d:LX/6YV;

    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110735
    iget-object v5, v1, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    move-object v1, v5

    .line 1110736
    invoke-interface {v0, v1}, LX/6YV;->b(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1110737
    new-instance v0, LX/6Yt;

    invoke-direct {v0, p0}, LX/6Yt;-><init>(LX/6Yw;)V

    .line 1110738
    iput-object v0, v4, LX/6Y5;->p:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 1110739
    iget-object v0, p0, LX/6Yw;->d:LX/6YV;

    invoke-interface {v0}, LX/6YV;->a()Z

    move-result v0

    .line 1110740
    iput-boolean v0, v4, LX/6Y5;->q:Z

    .line 1110741
    :cond_1
    if-eqz v3, :cond_2

    .line 1110742
    iget-object v0, v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1110743
    iput-object v0, v4, LX/6Y5;->t:Ljava/lang/String;

    .line 1110744
    move-object v0, v4

    .line 1110745
    iget-object v1, v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1110746
    invoke-virtual {v0, v1}, LX/6Y5;->a(Ljava/lang/String;)LX/6Y5;

    move-result-object v0

    .line 1110747
    iget-object v1, v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1110748
    iput-object v1, v0, LX/6Y5;->c:Ljava/lang/String;

    .line 1110749
    :cond_2
    invoke-virtual {v2, v4}, LX/6YP;->a(LX/6Y5;)V

    .line 1110750
    return-object v2

    .line 1110751
    :cond_3
    iget-object v0, v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->D:Ljava/lang/String;

    move-object v0, v0

    .line 1110752
    goto/16 :goto_0

    .line 1110753
    :cond_4
    sget-object v0, LX/6YN;->PROMOS_LIST:LX/6YN;

    invoke-virtual {p0, v0}, LX/6Ya;->a(LX/6YN;)Landroid/view/View$OnClickListener;

    move-result-object v0

    goto/16 :goto_1

    .line 1110754
    :cond_5
    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v5, 0x7f080e11

    invoke-virtual {v1, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/6Yw;->d()Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, LX/6Y5;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v1

    iget-object v5, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v6, 0x7f080e0b

    invoke-virtual {v5, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, LX/6Ya;->c()Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v1

    iget-object v5, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v6, 0x7f080e10

    invoke-virtual {v5, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v0}, LX/6Y5;->c(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    goto :goto_3

    .line 1110755
    :cond_6
    iget-object v5, v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1110756
    iget-object v6, v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->e:Ljava/lang/String;

    move-object v6, v6

    .line 1110757
    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1110758
    array-length v6, v5

    const/4 p1, 0x2

    if-ne v6, p1, :cond_0

    .line 1110759
    const v4, 0x7f0a03f2

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 1110760
    new-instance v6, LX/6Yv;

    invoke-direct {v6, p0, v0, v4}, LX/6Yv;-><init>(LX/6Yw;Landroid/view/View$OnClickListener;I)V

    .line 1110761
    new-instance v4, LX/47x;

    invoke-direct {v4, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const/4 p1, 0x0

    aget-object p1, v5, p1

    invoke-virtual {v4, p1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v4

    const/16 p1, 0x21

    invoke-virtual {v4, v6, p1}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v4

    .line 1110762
    iget-object v6, v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->e:Ljava/lang/String;

    move-object v6, v6

    .line 1110763
    invoke-virtual {v4, v6}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v4

    invoke-virtual {v4}, LX/47x;->a()LX/47x;

    move-result-object v4

    const/4 v6, 0x1

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v4

    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v4

    goto/16 :goto_2
.end method

.method public final d()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1110764
    new-instance v0, LX/6Yu;

    invoke-direct {v0, p0}, LX/6Yu;-><init>(LX/6Yw;)V

    return-object v0
.end method
