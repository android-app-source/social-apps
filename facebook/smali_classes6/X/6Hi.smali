.class public LX/6Hi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:F

.field public b:F

.field public c:Landroid/graphics/Rect;

.field public d:F

.field public e:F

.field public f:Landroid/graphics/Rect;

.field public g:D

.field public h:Z

.field public i:F

.field public j:F

.field public k:I

.field public l:J


# direct methods
.method private constructor <init>(J)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1072723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072724
    iput v2, p0, LX/6Hi;->a:F

    .line 1072725
    iput v2, p0, LX/6Hi;->b:F

    .line 1072726
    iput-object v0, p0, LX/6Hi;->c:Landroid/graphics/Rect;

    .line 1072727
    iput v2, p0, LX/6Hi;->d:F

    .line 1072728
    iput v2, p0, LX/6Hi;->e:F

    .line 1072729
    iput-object v0, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    .line 1072730
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, LX/6Hi;->g:D

    .line 1072731
    iput-boolean v3, p0, LX/6Hi;->h:Z

    .line 1072732
    iput v2, p0, LX/6Hi;->i:F

    .line 1072733
    iput v2, p0, LX/6Hi;->j:F

    .line 1072734
    iput v3, p0, LX/6Hi;->k:I

    .line 1072735
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6Hi;->l:J

    .line 1072736
    iput-wide p1, p0, LX/6Hi;->l:J

    .line 1072737
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Rect;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1072738
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072739
    iput v2, p0, LX/6Hi;->a:F

    .line 1072740
    iput v2, p0, LX/6Hi;->b:F

    .line 1072741
    iput-object v0, p0, LX/6Hi;->c:Landroid/graphics/Rect;

    .line 1072742
    iput v2, p0, LX/6Hi;->d:F

    .line 1072743
    iput v2, p0, LX/6Hi;->e:F

    .line 1072744
    iput-object v0, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    .line 1072745
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, LX/6Hi;->g:D

    .line 1072746
    iput-boolean v3, p0, LX/6Hi;->h:Z

    .line 1072747
    iput v2, p0, LX/6Hi;->i:F

    .line 1072748
    iput v2, p0, LX/6Hi;->j:F

    .line 1072749
    iput v3, p0, LX/6Hi;->k:I

    .line 1072750
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6Hi;->l:J

    .line 1072751
    iput-object p1, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    .line 1072752
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v0

    iput v0, p0, LX/6Hi;->d:F

    .line 1072753
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v0

    iput v0, p0, LX/6Hi;->e:F

    .line 1072754
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-wide v0, v4

    .line 1072755
    iput-wide v0, p0, LX/6Hi;->l:J

    .line 1072756
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1072757
    iget-object v0, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 1072758
    iget-object v0, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    iput-object v0, p0, LX/6Hi;->c:Landroid/graphics/Rect;

    .line 1072759
    iget v0, p0, LX/6Hi;->d:F

    iput v0, p0, LX/6Hi;->a:F

    .line 1072760
    iget v0, p0, LX/6Hi;->e:F

    iput v0, p0, LX/6Hi;->b:F

    .line 1072761
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v1

    iget v2, p0, LX/6Hi;->d:F

    iget v3, p0, LX/6Hi;->e:F

    invoke-static {v0, v1, v2, v3}, LX/6Hh;->a(FFFF)D

    move-result-wide v0

    iput-wide v0, p0, LX/6Hi;->g:D

    .line 1072762
    iput-object p1, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    .line 1072763
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v0

    iput v0, p0, LX/6Hi;->d:F

    .line 1072764
    invoke-virtual {p1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v0

    iput v0, p0, LX/6Hi;->e:F

    .line 1072765
    return-void
.end method

.method public final b()LX/6Hi;
    .locals 4

    .prologue
    .line 1072766
    new-instance v0, LX/6Hi;

    iget-wide v2, p0, LX/6Hi;->l:J

    invoke-direct {v0, v2, v3}, LX/6Hi;-><init>(J)V

    .line 1072767
    iget-object v1, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    .line 1072768
    iget-object v1, p0, LX/6Hi;->f:Landroid/graphics/Rect;

    .line 1072769
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v2, v0, LX/6Hi;->f:Landroid/graphics/Rect;

    .line 1072770
    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v2

    iput v2, v0, LX/6Hi;->d:F

    .line 1072771
    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    iput v2, v0, LX/6Hi;->e:F

    .line 1072772
    :cond_0
    iget-object v1, p0, LX/6Hi;->c:Landroid/graphics/Rect;

    if-eqz v1, :cond_1

    .line 1072773
    iget-object v1, p0, LX/6Hi;->c:Landroid/graphics/Rect;

    .line 1072774
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v2, v0, LX/6Hi;->c:Landroid/graphics/Rect;

    .line 1072775
    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v2

    iput v2, v0, LX/6Hi;->a:F

    .line 1072776
    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    iput v2, v0, LX/6Hi;->b:F

    .line 1072777
    :cond_1
    iget-wide v2, p0, LX/6Hi;->g:D

    iput-wide v2, v0, LX/6Hi;->g:D

    .line 1072778
    iget-boolean v1, p0, LX/6Hi;->h:Z

    iput-boolean v1, v0, LX/6Hi;->h:Z

    .line 1072779
    iget v1, p0, LX/6Hi;->i:F

    iput v1, v0, LX/6Hi;->i:F

    .line 1072780
    iget v1, p0, LX/6Hi;->j:F

    iput v1, v0, LX/6Hi;->j:F

    .line 1072781
    iget v1, p0, LX/6Hi;->k:I

    iput v1, v0, LX/6Hi;->k:I

    .line 1072782
    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1072783
    invoke-virtual {p0}, LX/6Hi;->b()LX/6Hi;

    move-result-object v0

    return-object v0
.end method
