.class public final LX/5DY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "overallStarRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "overallStarRating"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "pageVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "pageVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Z

.field public s:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z

.field public u:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "viewerVisits"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 872161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 872162
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;
    .locals 24

    .prologue
    .line 872163
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 872164
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5DY;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 872165
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, LX/5DY;->b:LX/15i;

    move-object/from16 v0, p0

    iget v7, v0, LX/5DY;->c:I

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v5, 0x7601011

    invoke-static {v6, v7, v5}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v3, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 872166
    move-object/from16 v0, p0

    iget-object v6, v0, LX/5DY;->d:Ljava/lang/String;

    invoke-virtual {v3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 872167
    move-object/from16 v0, p0

    iget-object v7, v0, LX/5DY;->e:LX/0Px;

    invoke-virtual {v3, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v7

    .line 872168
    move-object/from16 v0, p0

    iget-object v8, v0, LX/5DY;->f:Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$NewsFeedDefaultsPlaceFieldsWithoutMediaModel$CityModel;

    invoke-static {v3, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 872169
    move-object/from16 v0, p0

    iget-object v9, v0, LX/5DY;->g:Ljava/lang/String;

    invoke-virtual {v3, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 872170
    move-object/from16 v0, p0

    iget-object v10, v0, LX/5DY;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-static {v3, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 872171
    move-object/from16 v0, p0

    iget-object v11, v0, LX/5DY;->j:Ljava/lang/String;

    invoke-virtual {v3, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 872172
    sget-object v12, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v12

    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, LX/5DY;->k:LX/15i;

    move-object/from16 v0, p0

    iget v14, v0, LX/5DY;->l:I

    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v12, 0x34c84503

    invoke-static {v13, v14, v12}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v12

    invoke-static {v3, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 872173
    sget-object v13, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v13

    :try_start_2
    move-object/from16 v0, p0

    iget-object v14, v0, LX/5DY;->m:LX/15i;

    move-object/from16 v0, p0

    iget v15, v0, LX/5DY;->n:I

    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const v13, -0x33df0b5b    # -4.2193556E7f

    invoke-static {v14, v15, v13}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v13

    invoke-static {v3, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 872174
    move-object/from16 v0, p0

    iget-object v14, v0, LX/5DY;->o:Ljava/lang/String;

    invoke-virtual {v3, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 872175
    move-object/from16 v0, p0

    iget-object v15, v0, LX/5DY;->p:Ljava/lang/String;

    invoke-virtual {v3, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 872176
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5DY;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 872177
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5DY;->s:Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 872178
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5DY;->u:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 872179
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5DY;->v:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    .line 872180
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5DY;->w:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    .line 872181
    sget-object v21, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v21

    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, LX/5DY;->x:LX/15i;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/5DY;->y:I

    move/from16 v23, v0

    monitor-exit v21
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const v21, 0x471b9538

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/api/graphql/place/NewsFeedExplicitPlaceFieldsGraphQLModels$DraculaImplementation;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 872182
    const/16 v22, 0x15

    move/from16 v0, v22

    invoke-virtual {v3, v0}, LX/186;->c(I)V

    .line 872183
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v3, v0, v4}, LX/186;->b(II)V

    .line 872184
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v5}, LX/186;->b(II)V

    .line 872185
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 872186
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 872187
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 872188
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 872189
    const/4 v4, 0x6

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/5DY;->h:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 872190
    const/4 v4, 0x7

    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 872191
    const/16 v4, 0x8

    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 872192
    const/16 v4, 0x9

    invoke-virtual {v3, v4, v12}, LX/186;->b(II)V

    .line 872193
    const/16 v4, 0xa

    invoke-virtual {v3, v4, v13}, LX/186;->b(II)V

    .line 872194
    const/16 v4, 0xb

    invoke-virtual {v3, v4, v14}, LX/186;->b(II)V

    .line 872195
    const/16 v4, 0xc

    invoke-virtual {v3, v4, v15}, LX/186;->b(II)V

    .line 872196
    const/16 v4, 0xd

    move/from16 v0, v16

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 872197
    const/16 v4, 0xe

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/5DY;->r:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 872198
    const/16 v4, 0xf

    move/from16 v0, v17

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 872199
    const/16 v4, 0x10

    move-object/from16 v0, p0

    iget-boolean v5, v0, LX/5DY;->t:Z

    invoke-virtual {v3, v4, v5}, LX/186;->a(IZ)V

    .line 872200
    const/16 v4, 0x11

    move/from16 v0, v18

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 872201
    const/16 v4, 0x12

    move/from16 v0, v19

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 872202
    const/16 v4, 0x13

    move/from16 v0, v20

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 872203
    const/16 v4, 0x14

    move/from16 v0, v21

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 872204
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 872205
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 872206
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 872207
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 872208
    new-instance v3, LX/15i;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 872209
    new-instance v4, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;

    invoke-direct {v4, v3}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;-><init>(LX/15i;)V

    .line 872210
    return-object v4

    .line 872211
    :catchall_0
    move-exception v3

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3

    .line 872212
    :catchall_1
    move-exception v3

    :try_start_5
    monitor-exit v12
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v3

    .line 872213
    :catchall_2
    move-exception v3

    :try_start_6
    monitor-exit v13
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v3

    .line 872214
    :catchall_3
    move-exception v3

    :try_start_7
    monitor-exit v21
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v3
.end method
