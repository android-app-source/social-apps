.class public final LX/6T6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1097351
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 1097352
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1097353
    :goto_0
    return v1

    .line 1097354
    :cond_0
    const-string v6, "video_time_offset"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1097355
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 1097356
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 1097357
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1097358
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1097359
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1097360
    const-string v6, "pinned_comment"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1097361
    invoke-static {p0, p1}, LX/6Sl;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1097362
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1097363
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1097364
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1097365
    if-eqz v0, :cond_4

    .line 1097366
    invoke-virtual {p1, v2, v3, v1}, LX/186;->a(III)V

    .line 1097367
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1097368
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1097369
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1097370
    if-eqz v0, :cond_0

    .line 1097371
    const-string v1, "pinned_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097372
    invoke-static {p0, v0, p2, p3}, LX/6Sl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1097373
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1097374
    if-eqz v0, :cond_1

    .line 1097375
    const-string v1, "video_time_offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097376
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1097377
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1097378
    return-void
.end method
