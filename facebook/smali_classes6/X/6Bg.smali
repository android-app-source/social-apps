.class public final LX/6Bg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/assetdownload/AssetDownloadConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1063025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1063026
    check-cast p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    check-cast p2, Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    .line 1063027
    iget v0, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mPriority:I

    move v0, v0

    .line 1063028
    iget v1, p2, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mPriority:I

    move v1, v1

    .line 1063029
    if-eq v0, v1, :cond_0

    .line 1063030
    sub-int/2addr v0, v1

    .line 1063031
    :goto_0
    return v0

    .line 1063032
    :cond_0
    iget-object v0, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    move-object v0, v0

    .line 1063033
    iget-object v1, p2, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    move-object v1, v1

    .line 1063034
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
