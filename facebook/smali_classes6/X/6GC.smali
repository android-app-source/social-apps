.class public final LX/6GC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1fX;

.field public final c:Landroid/hardware/SensorEventListener;

.field public final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private e:Landroid/hardware/SensorManager;

.field private f:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1fX;Landroid/hardware/SensorEventListener;)V
    .locals 2
    .param p2    # LX/1fX;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param

    .prologue
    .line 1069954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1069955
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/6GC;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1069956
    new-instance v0, Lcom/facebook/bugreporter/RageShakeDetector$AsyncSensorRegistration$1;

    invoke-direct {v0, p0}, Lcom/facebook/bugreporter/RageShakeDetector$AsyncSensorRegistration$1;-><init>(LX/6GC;)V

    iput-object v0, p0, LX/6GC;->g:Ljava/lang/Runnable;

    .line 1069957
    iput-object p1, p0, LX/6GC;->a:Landroid/content/Context;

    .line 1069958
    iput-object p2, p0, LX/6GC;->b:LX/1fX;

    .line 1069959
    new-instance v0, LX/6GB;

    invoke-direct {v0, p0, p3}, LX/6GB;-><init>(LX/6GC;Landroid/hardware/SensorEventListener;)V

    iput-object v0, p0, LX/6GC;->c:Landroid/hardware/SensorEventListener;

    .line 1069960
    return-void
.end method

.method public static c(LX/6GC;)Landroid/hardware/SensorManager;
    .locals 2

    .prologue
    .line 1069961
    iget-object v0, p0, LX/6GC;->e:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 1069962
    iget-object v0, p0, LX/6GC;->a:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, LX/6GC;->e:Landroid/hardware/SensorManager;

    .line 1069963
    :cond_0
    iget-object v0, p0, LX/6GC;->e:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method public static d(LX/6GC;)V
    .locals 2

    .prologue
    .line 1069964
    iget-object v0, p0, LX/6GC;->f:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 1069965
    iget-object v0, p0, LX/6GC;->f:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1069966
    const/4 v0, 0x0

    iput-object v0, p0, LX/6GC;->f:Ljava/util/concurrent/Future;

    .line 1069967
    :cond_0
    iget-object v0, p0, LX/6GC;->b:LX/1fX;

    iget-object v1, p0, LX/6GC;->g:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6GC;->f:Ljava/util/concurrent/Future;

    .line 1069968
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 4
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 1069969
    iget-object v0, p0, LX/6GC;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 1069970
    if-nez v0, :cond_1

    .line 1069971
    invoke-static {p0}, LX/6GC;->d(LX/6GC;)V

    .line 1069972
    :cond_0
    return-void

    .line 1069973
    :cond_1
    if-gez v0, :cond_0

    .line 1069974
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Imbalanced start/stopReceiving calls: count="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
