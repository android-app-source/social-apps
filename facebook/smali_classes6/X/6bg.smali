.class public LX/6bg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/6bg;


# instance fields
.field private volatile b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1113779
    const-class v0, LX/6bg;

    sput-object v0, LX/6bg;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1113780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113781
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/6bg;->b:Ljava/util/ArrayList;

    .line 1113782
    return-void
.end method

.method public static a(LX/0QB;)LX/6bg;
    .locals 3

    .prologue
    .line 1113783
    sget-object v0, LX/6bg;->d:LX/6bg;

    if-nez v0, :cond_1

    .line 1113784
    const-class v1, LX/6bg;

    monitor-enter v1

    .line 1113785
    :try_start_0
    sget-object v0, LX/6bg;->d:LX/6bg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1113786
    if-eqz v2, :cond_0

    .line 1113787
    :try_start_1
    new-instance v0, LX/6bg;

    invoke-direct {v0}, LX/6bg;-><init>()V

    .line 1113788
    move-object v0, v0

    .line 1113789
    sput-object v0, LX/6bg;->d:LX/6bg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1113790
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1113791
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1113792
    :cond_1
    sget-object v0, LX/6bg;->d:LX/6bg;

    return-object v0

    .line 1113793
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1113794
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1113795
    iget-object v0, p0, LX/6bg;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1113796
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6bg;->c:Z

    .line 1113797
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;)V
    .locals 1

    .prologue
    .line 1113798
    iget-boolean v0, p0, LX/6bg;->c:Z

    if-eqz v0, :cond_0

    .line 1113799
    iget-object v0, p0, LX/6bg;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1113800
    :cond_0
    return-void
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1113801
    iget-boolean v0, p0, LX/6bg;->c:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1113802
    iget-object v0, p0, LX/6bg;->b:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1113803
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/6bg;->c:Z

    .line 1113804
    return-object v0
.end method
