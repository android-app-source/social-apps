.class public interface abstract LX/5Vr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Vq;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Forwarder;
    processor = "com.facebook.dracula.transformer.Transformer"
    to = "UserInfo$"
.end annotation


# virtual methods
.method public abstract B()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract D()Z
.end method

.method public abstract E()Z
.end method

.method public abstract G()Z
.end method

.method public abstract H()Z
.end method

.method public abstract I()Z
.end method

.method public abstract J()Z
.end method

.method public abstract K()Z
.end method

.method public abstract L()Z
.end method

.method public abstract M()Z
.end method

.method public abstract Q()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsInterfaces$PlatformCallToAction;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract R()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsInterfaces$PlatformCallToAction;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract S()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract U()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ab()Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract am()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract an()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ao()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract ap()Lcom/facebook/messaging/graphql/threads/UserInfoModels$SmsMessagingParticipantFieldsModel$PhoneNumberModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract aq()Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel$MessengerContactModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract au()Lcom/facebook/messaging/graphql/threads/UserInfoModels$CustomerDataFragModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()Z
.end method

.method public abstract n()Z
.end method

.method public abstract o()Z
.end method

.method public abstract r()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract s()Lcom/facebook/graphql/enums/GraphQLCommercePageType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract t()D
.end method

.method public abstract w()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method
