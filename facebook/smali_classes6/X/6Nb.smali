.class public final enum LX/6Nb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Nb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Nb;

.field public static final enum CONTACT_QUERY:LX/6Nb;

.field public static final enum DELETE_CONTACT:LX/6Nb;

.field public static final enum REINDEX_COLLECION:LX/6Nb;

.field public static final enum SAVE_CONTACT:LX/6Nb;

.field public static final enum USER_QUERY:LX/6Nb;


# instance fields
.field public final mCaller:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1083676
    new-instance v0, LX/6Nb;

    const-string v1, "CONTACT_QUERY"

    const-string v2, "contact_query"

    invoke-direct {v0, v1, v3, v2}, LX/6Nb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Nb;->CONTACT_QUERY:LX/6Nb;

    .line 1083677
    new-instance v0, LX/6Nb;

    const-string v1, "USER_QUERY"

    const-string v2, "user_query"

    invoke-direct {v0, v1, v4, v2}, LX/6Nb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Nb;->USER_QUERY:LX/6Nb;

    .line 1083678
    new-instance v0, LX/6Nb;

    const-string v1, "DELETE_CONTACT"

    const-string v2, "delete_contact"

    invoke-direct {v0, v1, v5, v2}, LX/6Nb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Nb;->DELETE_CONTACT:LX/6Nb;

    .line 1083679
    new-instance v0, LX/6Nb;

    const-string v1, "SAVE_CONTACT"

    const-string v2, "save_contact"

    invoke-direct {v0, v1, v6, v2}, LX/6Nb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Nb;->SAVE_CONTACT:LX/6Nb;

    .line 1083680
    new-instance v0, LX/6Nb;

    const-string v1, "REINDEX_COLLECION"

    const-string v2, "reindex_collection"

    invoke-direct {v0, v1, v7, v2}, LX/6Nb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Nb;->REINDEX_COLLECION:LX/6Nb;

    .line 1083681
    const/4 v0, 0x5

    new-array v0, v0, [LX/6Nb;

    sget-object v1, LX/6Nb;->CONTACT_QUERY:LX/6Nb;

    aput-object v1, v0, v3

    sget-object v1, LX/6Nb;->USER_QUERY:LX/6Nb;

    aput-object v1, v0, v4

    sget-object v1, LX/6Nb;->DELETE_CONTACT:LX/6Nb;

    aput-object v1, v0, v5

    sget-object v1, LX/6Nb;->SAVE_CONTACT:LX/6Nb;

    aput-object v1, v0, v6

    sget-object v1, LX/6Nb;->REINDEX_COLLECION:LX/6Nb;

    aput-object v1, v0, v7

    sput-object v0, LX/6Nb;->$VALUES:[LX/6Nb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1083682
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1083683
    iput-object p3, p0, LX/6Nb;->mCaller:Ljava/lang/String;

    .line 1083684
    return-void
.end method

.method public static synthetic access$000(LX/6Nb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1083685
    iget-object v0, p0, LX/6Nb;->mCaller:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Nb;
    .locals 1

    .prologue
    .line 1083686
    const-class v0, LX/6Nb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Nb;

    return-object v0
.end method

.method public static values()[LX/6Nb;
    .locals 1

    .prologue
    .line 1083687
    sget-object v0, LX/6Nb;->$VALUES:[LX/6Nb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Nb;

    return-object v0
.end method
