.class public final LX/6Fx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/bugreporter/BugReport;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/0am;

.field public final synthetic c:LX/6G2;


# direct methods
.method public constructor <init>(LX/6G2;Landroid/content/Context;LX/0am;)V
    .locals 0

    .prologue
    .line 1069497
    iput-object p1, p0, LX/6Fx;->c:LX/6G2;

    iput-object p2, p0, LX/6Fx;->a:Landroid/content/Context;

    iput-object p3, p0, LX/6Fx;->b:LX/0am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1069498
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1069499
    check-cast p1, Lcom/facebook/bugreporter/BugReport;

    .line 1069500
    iget-object v0, p0, LX/6Fx;->c:LX/6G2;

    iget-object v1, p0, LX/6Fx;->a:Landroid/content/Context;

    iget-object v2, p0, LX/6Fx;->b:LX/0am;

    .line 1069501
    iget-object v3, v0, LX/6G2;->e:LX/6G0;

    .line 1069502
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1069503
    new-instance v4, LX/6G1;

    iget-object v5, v0, LX/6G2;->e:LX/6G0;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-direct {v4, v5, v7, v8}, LX/6G1;-><init>(LX/6G0;J)V

    move-object v3, v4

    .line 1069504
    :cond_0
    invoke-static {v1, p1, v3}, Lcom/facebook/bugreporter/activity/BugReportActivity;->a(Landroid/content/Context;Lcom/facebook/bugreporter/BugReport;LX/6G0;)Landroid/content/Intent;

    move-result-object v3

    .line 1069505
    const-string v4, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-static {p1}, LX/6Fi;->a(Lcom/facebook/bugreporter/BugReport;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1069506
    const-class v4, Landroid/app/Service;

    invoke-static {v1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1069507
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1069508
    :cond_1
    iget-object v4, v0, LX/6G2;->i:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1069509
    return-void
.end method
