.class public final LX/5GW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 888256
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 888257
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 888258
    :goto_0
    return v1

    .line 888259
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 888260
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 888261
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 888262
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 888263
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 888264
    const-string v7, "external_url"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 888265
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 888266
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 888267
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 888268
    :cond_3
    const-string v7, "open_graph_node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 888269
    invoke-static {p0, p1}, LX/5GV;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 888270
    :cond_4
    const-string v7, "title"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 888271
    const/4 v6, 0x0

    .line 888272
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_b

    .line 888273
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 888274
    :goto_2
    move v2, v6

    .line 888275
    goto :goto_1

    .line 888276
    :cond_5
    const-string v7, "user_url"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 888277
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 888278
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 888279
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 888280
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 888281
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 888282
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 888283
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 888284
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 888285
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 888286
    :cond_9
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 888287
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 888288
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 888289
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 888290
    const-string v8, "text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 888291
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_3

    .line 888292
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 888293
    invoke-virtual {p1, v6, v2}, LX/186;->b(II)V

    .line 888294
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_2

    :cond_b
    move v2, v6

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 888295
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 888296
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888297
    if-eqz v0, :cond_0

    .line 888298
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888299
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888300
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888301
    if-eqz v0, :cond_1

    .line 888302
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888303
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888304
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888305
    if-eqz v0, :cond_2

    .line 888306
    const-string v1, "open_graph_node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888307
    invoke-static {p0, v0, p2}, LX/5GV;->a(LX/15i;ILX/0nX;)V

    .line 888308
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 888309
    if-eqz v0, :cond_4

    .line 888310
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888311
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 888312
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 888313
    if-eqz v1, :cond_3

    .line 888314
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888315
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888316
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 888317
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 888318
    if-eqz v0, :cond_5

    .line 888319
    const-string v1, "user_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 888320
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 888321
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 888322
    return-void
.end method
