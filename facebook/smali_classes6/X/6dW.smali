.class public final LX/6dW;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0sv;

.field private static final b:LX/0sv;

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 1116221
    new-instance v0, LX/0su;

    sget-object v1, LX/6dV;->c:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/6dW;->a:LX/0sv;

    .line 1116222
    new-instance v0, LX/2Qn;

    sget-object v1, LX/6dV;->b:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const-string v2, "threads"

    sget-object v3, LX/6dq;->a:LX/0U1;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    const-string v4, "ON DELETE CASCADE"

    invoke-direct {v0, v1, v2, v3, v4}, LX/2Qn;-><init>(LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    sput-object v0, LX/6dW;->b:LX/0sv;

    .line 1116223
    sget-object v0, LX/6dV;->a:LX/0U1;

    sget-object v1, LX/6dV;->b:LX/0U1;

    sget-object v2, LX/6dV;->c:LX/0U1;

    sget-object v3, LX/6dV;->d:LX/0U1;

    sget-object v4, LX/6dV;->e:LX/0U1;

    sget-object v5, LX/6dV;->f:LX/0U1;

    sget-object v6, LX/6dV;->g:LX/0U1;

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/6dW;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 1116224
    const-string v0, "event_reminders"

    sget-object v1, LX/6dW;->c:LX/0Px;

    sget-object v2, LX/6dW;->a:LX/0sv;

    sget-object v3, LX/6dW;->b:LX/0sv;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 1116225
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 1116226
    return-void
.end method
