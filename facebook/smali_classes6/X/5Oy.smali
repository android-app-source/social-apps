.class public final enum LX/5Oy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Oy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Oy;

.field public static final enum HIGHLIGHT_IMPROVEMENT:LX/5Oy;

.field public static final enum NO_ATTACHMENT:LX/5Oy;

.field public static final enum REQUEST_ACCEPTED_NOTICE:LX/5Oy;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 909789
    new-instance v0, LX/5Oy;

    const-string v1, "NO_ATTACHMENT"

    invoke-direct {v0, v1, v2}, LX/5Oy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oy;->NO_ATTACHMENT:LX/5Oy;

    .line 909790
    new-instance v0, LX/5Oy;

    const-string v1, "REQUEST_ACCEPTED_NOTICE"

    invoke-direct {v0, v1, v3}, LX/5Oy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oy;->REQUEST_ACCEPTED_NOTICE:LX/5Oy;

    .line 909791
    new-instance v0, LX/5Oy;

    const-string v1, "HIGHLIGHT_IMPROVEMENT"

    invoke-direct {v0, v1, v4}, LX/5Oy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oy;->HIGHLIGHT_IMPROVEMENT:LX/5Oy;

    .line 909792
    const/4 v0, 0x3

    new-array v0, v0, [LX/5Oy;

    sget-object v1, LX/5Oy;->NO_ATTACHMENT:LX/5Oy;

    aput-object v1, v0, v2

    sget-object v1, LX/5Oy;->REQUEST_ACCEPTED_NOTICE:LX/5Oy;

    aput-object v1, v0, v3

    sget-object v1, LX/5Oy;->HIGHLIGHT_IMPROVEMENT:LX/5Oy;

    aput-object v1, v0, v4

    sput-object v0, LX/5Oy;->$VALUES:[LX/5Oy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 909786
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Oy;
    .locals 1

    .prologue
    .line 909788
    const-class v0, LX/5Oy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Oy;

    return-object v0
.end method

.method public static values()[LX/5Oy;
    .locals 1

    .prologue
    .line 909787
    sget-object v0, LX/5Oy;->$VALUES:[LX/5Oy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Oy;

    return-object v0
.end method
