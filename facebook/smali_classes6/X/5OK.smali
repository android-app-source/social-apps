.class public LX/5OK;
.super Landroid/widget/ListView;
.source ""


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I


# instance fields
.field private d:F

.field public e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Z

.field private k:Z

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 908523
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/5OK;->a:[I

    .line 908524
    new-array v0, v3, [I

    const v1, 0x10100a4

    aput v1, v0, v2

    sput-object v0, LX/5OK;->b:[I

    .line 908525
    new-array v0, v3, [I

    const v1, 0x10100a6

    aput v1, v0, v2

    sput-object v0, LX/5OK;->c:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x10100a4
        0x10100a6
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 908547
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/5OK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 908548
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 908545
    const v0, 0x7f01024b

    invoke-direct {p0, p1, p2, v0}, LX/5OK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 908546
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 908531
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 908532
    iput-boolean v3, p0, LX/5OK;->j:Z

    .line 908533
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5OK;->k:Z

    .line 908534
    invoke-virtual {p0}, LX/5OK;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/5OK;->g:I

    .line 908535
    invoke-virtual {p0}, LX/5OK;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/5OK;->e:I

    .line 908536
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, LX/5OK;->d:F

    .line 908537
    sget-object v0, LX/03r;->PopoverListView:[I

    const v1, 0x7f0e01d7

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 908538
    const/16 v1, 0x0

    iget v2, p0, LX/5OK;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/5OK;->g:I

    .line 908539
    const/16 v1, 0x1

    iget v2, p0, LX/5OK;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, LX/5OK;->e:I

    .line 908540
    const/16 v1, 0x2

    iget v2, p0, LX/5OK;->d:F

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, LX/5OK;->d:F

    .line 908541
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 908542
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 908543
    const v1, 0x7f030610

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/5OK;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 908544
    return-void
.end method

.method public static a(LX/5OK;)V
    .locals 2

    .prologue
    .line 908526
    iget-object v0, p0, LX/5OK;->m:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5OK;->m:Landroid/view/View;

    .line 908527
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, LX/5OK;->m:Landroid/view/View;

    .line 908528
    invoke-virtual {p0, v0}, LX/5OK;->removeHeaderView(Landroid/view/View;)Z

    .line 908529
    return-void

    .line 908530
    :cond_0
    iget-object v0, p0, LX/5OK;->l:Lcom/facebook/resources/ui/FbTextView;

    goto :goto_0
.end method

.method public static setTitleView(LX/5OK;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 908519
    invoke-virtual {p0}, LX/5OK;->getHeaderViewsCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 908520
    invoke-static {p0}, LX/5OK;->a(LX/5OK;)V

    .line 908521
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/5OK;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 908522
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 908549
    iget-boolean v0, p0, LX/5OK;->j:Z

    if-eq v0, p1, :cond_0

    .line 908550
    iput-boolean p1, p0, LX/5OK;->j:Z

    .line 908551
    invoke-virtual {p0}, LX/5OK;->requestLayout()V

    .line 908552
    invoke-virtual {p0}, LX/5OK;->invalidate()V

    .line 908553
    :cond_0
    return-void
.end method

.method public final onCreateDrawableState(I)[I
    .locals 5

    .prologue
    .line 908506
    invoke-virtual {p0}, LX/5OK;->isPressed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 908507
    invoke-super {p0, p1}, Landroid/widget/ListView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 908508
    :cond_0
    :goto_0
    return-object v0

    .line 908509
    :cond_1
    add-int/lit8 v0, p1, 0x2

    invoke-super {p0, v0}, Landroid/widget/ListView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 908510
    invoke-virtual {p0}, LX/5OK;->getChildCount()I

    move-result v1

    .line 908511
    invoke-virtual {p0}, LX/5OK;->getFirstVisiblePosition()I

    move-result v2

    .line 908512
    iget v3, p0, LX/5OK;->h:I

    iget v4, p0, LX/5OK;->i:I

    invoke-virtual {p0, v3, v4}, LX/5OK;->pointToPosition(II)I

    move-result v3

    sub-int v2, v3, v2

    .line 908513
    if-nez v2, :cond_2

    add-int/lit8 v3, v1, -0x1

    if-ne v2, v3, :cond_2

    .line 908514
    sget-object v1, LX/5OK;->a:[I

    invoke-static {v0, v1}, LX/5OK;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 908515
    :cond_2
    if-nez v2, :cond_3

    .line 908516
    sget-object v1, LX/5OK;->b:[I

    invoke-static {v0, v1}, LX/5OK;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 908517
    :cond_3
    add-int/lit8 v1, v1, -0x1

    if-ne v2, v1, :cond_0

    .line 908518
    sget-object v1, LX/5OK;->c:[I

    invoke-static {v0, v1}, LX/5OK;->mergeDrawableStates([I[I)[I

    goto :goto_0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 908422
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    .line 908423
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/5OK;->h:I

    .line 908424
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/5OK;->i:I

    .line 908425
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 908426
    :cond_0
    iput v1, p0, LX/5OK;->h:I

    .line 908427
    iput v1, p0, LX/5OK;->i:I

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 17

    .prologue
    .line 908443
    invoke-virtual/range {p0 .. p0}, LX/5OK;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 908444
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/5OK;->k:Z

    if-eqz v1, :cond_0

    .line 908445
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 908446
    invoke-super/range {p0 .. p2}, Landroid/widget/ListView;->onMeasure(II)V

    .line 908447
    :cond_0
    const/4 v1, 0x0

    .line 908448
    invoke-virtual/range {p0 .. p0}, LX/5OK;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    .line 908449
    instance-of v3, v2, Landroid/widget/HeaderViewListAdapter;

    if-eqz v3, :cond_c

    move-object v1, v2

    .line 908450
    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 908451
    move-object/from16 v0, p0

    iget-object v1, v0, LX/5OK;->m:Landroid/view/View;

    if-eqz v1, :cond_b

    .line 908452
    move-object/from16 v0, p0

    iget-object v1, v0, LX/5OK;->m:Landroid/view/View;

    const/high16 v4, -0x80000000

    move/from16 v0, p2

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    move/from16 v0, p1

    invoke-virtual {v1, v0, v4}, Landroid/view/View;->measure(II)V

    .line 908453
    move-object/from16 v0, p0

    iget-object v1, v0, LX/5OK;->m:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v4, v1

    .line 908454
    :goto_0
    instance-of v1, v3, LX/5OG;

    if-eqz v1, :cond_1

    move-object v1, v3

    .line 908455
    check-cast v1, LX/5OG;

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/5OK;->j:Z

    invoke-virtual {v1, v6}, LX/5OG;->a(Z)V

    .line 908456
    :cond_1
    if-nez v3, :cond_d

    const/4 v1, 0x0

    .line 908457
    :goto_1
    if-lez v4, :cond_2

    .line 908458
    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v1, v6

    .line 908459
    :cond_2
    move-object/from16 v0, p0

    iget v6, v0, LX/5OK;->d:F

    cmpl-float v6, v1, v6

    if-lez v6, :cond_3

    .line 908460
    move-object/from16 v0, p0

    iget v1, v0, LX/5OK;->d:F

    const/high16 v6, 0x3f000000    # 0.5f

    sub-float/2addr v1, v6

    .line 908461
    :cond_3
    if-lez v4, :cond_4

    .line 908462
    const/high16 v6, 0x3f800000    # 1.0f

    sub-float/2addr v1, v6

    .line 908463
    :cond_4
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 908464
    invoke-virtual/range {p0 .. p0}, LX/5OK;->getPaddingTop()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, LX/5OK;->getPaddingBottom()I

    move-result v7

    add-int/2addr v6, v7

    add-int/2addr v4, v6

    .line 908465
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    .line 908466
    const/4 v6, 0x0

    invoke-static {v9, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 908467
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    .line 908468
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v5, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 908469
    const/4 v6, 0x0

    .line 908470
    invoke-virtual/range {p0 .. p0}, LX/5OK;->getDividerHeight()I

    move-result v13

    .line 908471
    const/4 v7, 0x0

    .line 908472
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/5OK;->j:Z

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/5OK;->k:Z

    if-nez v2, :cond_14

    .line 908473
    :cond_5
    const/4 v2, 0x0

    move/from16 v16, v2

    move v2, v4

    move-object v4, v7

    move/from16 v7, v16

    :goto_2
    if-ge v7, v12, :cond_11

    .line 908474
    move-object/from16 v0, p0

    invoke-interface {v3, v7, v4, v0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 908475
    invoke-virtual {v8, v10, v11}, Landroid/view/View;->measure(II)V

    .line 908476
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, LX/5OK;->e:I

    .line 908477
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/5OK;->j:Z

    if-eqz v4, :cond_6

    int-to-float v4, v7

    cmpg-float v4, v4, v1

    if-gez v4, :cond_6

    .line 908478
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v2, v4

    .line 908479
    if-lez v7, :cond_6

    .line 908480
    add-int/2addr v2, v13

    .line 908481
    :cond_6
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 908482
    move-object/from16 v0, p0

    iget v14, v0, LX/5OK;->g:I

    if-eqz v14, :cond_7

    move-object/from16 v0, p0

    iget v14, v0, LX/5OK;->g:I

    rem-int v14, v4, v14

    if-eqz v14, :cond_7

    .line 908483
    move-object/from16 v0, p0

    iget v14, v0, LX/5OK;->g:I

    move-object/from16 v0, p0

    iget v15, v0, LX/5OK;->g:I

    div-int/2addr v4, v15

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v4, v14

    .line 908484
    :cond_7
    if-lt v4, v5, :cond_e

    .line 908485
    int-to-float v4, v7

    cmpl-float v4, v4, v1

    if-ltz v4, :cond_13

    .line 908486
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/5OK;->j:Z

    if-nez v3, :cond_8

    .line 908487
    move-object/from16 v0, p0

    iget v3, v0, LX/5OK;->e:I

    int-to-float v3, v3

    mul-float/2addr v3, v1

    int-to-float v4, v13

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float/2addr v1, v6

    mul-float/2addr v1, v4

    add-float/2addr v1, v3

    float-to-int v1, v1

    add-int/2addr v2, v1

    .line 908488
    :cond_8
    invoke-static {v2, v9}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 908489
    invoke-virtual/range {p0 .. p0}, LX/5OK;->getSuggestedMinimumWidth()I

    move-result v1

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 908490
    move-object/from16 v0, p0

    iget v3, v0, LX/5OK;->f:I

    if-lez v3, :cond_10

    .line 908491
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/5OK;->k:Z

    if-eqz v3, :cond_f

    .line 908492
    move-object/from16 v0, p0

    iget v1, v0, LX/5OK;->f:I

    invoke-virtual/range {p0 .. p0}, LX/5OK;->getMeasuredWidth()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 908493
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/5OK;->k:Z

    if-nez v3, :cond_a

    .line 908494
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 908495
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 908496
    move-object/from16 v0, p0

    invoke-super {v0, v3, v4}, Landroid/widget/ListView;->onMeasure(II)V

    .line 908497
    :cond_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, LX/5OK;->setMeasuredDimension(II)V

    .line 908498
    return-void

    .line 908499
    :cond_b
    const v1, 0x7f0b01da

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    move v4, v1

    goto/16 :goto_0

    :cond_c
    move-object v3, v2

    move v4, v1

    .line 908500
    goto/16 :goto_0

    .line 908501
    :cond_d
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    int-to-float v1, v1

    goto/16 :goto_1

    .line 908502
    :cond_e
    if-le v4, v6, :cond_12

    .line 908503
    :goto_5
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v6, v4

    move-object v4, v8

    goto/16 :goto_2

    .line 908504
    :cond_f
    move-object/from16 v0, p0

    iget v3, v0, LX/5OK;->f:I

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_4

    .line 908505
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/5OK;->k:Z

    if-eqz v3, :cond_9

    invoke-virtual/range {p0 .. p0}, LX/5OK;->getMeasuredWidth()I

    move-result v1

    goto :goto_4

    :cond_11
    move v5, v6

    goto/16 :goto_3

    :cond_12
    move v4, v6

    goto :goto_5

    :cond_13
    move v4, v5

    goto :goto_5

    :cond_14
    move v5, v6

    move v2, v4

    goto/16 :goto_3
.end method

.method public setMaxRows(F)V
    .locals 1

    .prologue
    .line 908438
    iget v0, p0, LX/5OK;->d:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 908439
    iput p1, p0, LX/5OK;->d:F

    .line 908440
    invoke-virtual {p0}, LX/5OK;->requestLayout()V

    .line 908441
    invoke-virtual {p0}, LX/5OK;->invalidate()V

    .line 908442
    :cond_0
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 1

    .prologue
    .line 908433
    iget v0, p0, LX/5OK;->f:I

    if-eq v0, p1, :cond_0

    .line 908434
    iput p1, p0, LX/5OK;->f:I

    .line 908435
    invoke-virtual {p0}, LX/5OK;->requestLayout()V

    .line 908436
    invoke-virtual {p0}, LX/5OK;->invalidate()V

    .line 908437
    :cond_0
    return-void
.end method

.method public setShowFullWidth(Z)V
    .locals 1

    .prologue
    .line 908428
    iget-boolean v0, p0, LX/5OK;->k:Z

    if-eq v0, p1, :cond_0

    .line 908429
    iput-boolean p1, p0, LX/5OK;->k:Z

    .line 908430
    invoke-virtual {p0}, LX/5OK;->requestLayout()V

    .line 908431
    invoke-virtual {p0}, LX/5OK;->invalidate()V

    .line 908432
    :cond_0
    return-void
.end method
