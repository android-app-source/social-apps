.class public LX/6bh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/crypto/keychain/KeyChain;


# instance fields
.field private final a:LX/1Hy;

.field private final b:Ljava/security/SecureRandom;

.field private final c:[B


# direct methods
.method public constructor <init>(LX/1Hy;Ljava/security/SecureRandom;[B)V
    .locals 0

    .prologue
    .line 1113805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113806
    iput-object p1, p0, LX/6bh;->a:LX/1Hy;

    .line 1113807
    iput-object p2, p0, LX/6bh;->b:Ljava/security/SecureRandom;

    .line 1113808
    iput-object p3, p0, LX/6bh;->c:[B

    .line 1113809
    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 1

    .prologue
    .line 1113810
    iget-object v0, p0, LX/6bh;->c:[B

    return-object v0
.end method

.method public final b()[B
    .locals 2

    .prologue
    .line 1113811
    iget-object v0, p0, LX/6bh;->a:LX/1Hy;

    iget v0, v0, LX/1Hy;->ivLength:I

    new-array v0, v0, [B

    .line 1113812
    iget-object v1, p0, LX/6bh;->b:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 1113813
    return-object v0
.end method
