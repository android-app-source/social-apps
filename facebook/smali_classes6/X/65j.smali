.class public final LX/65j;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/673;

.field public static final b:LX/673;

.field public static final c:LX/673;

.field public static final d:LX/673;

.field public static final e:LX/673;

.field public static final f:LX/673;

.field public static final g:LX/673;


# instance fields
.field public final h:LX/673;

.field public final i:LX/673;

.field public final j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1048426
    const-string v0, ":status"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/65j;->a:LX/673;

    .line 1048427
    const-string v0, ":method"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/65j;->b:LX/673;

    .line 1048428
    const-string v0, ":path"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/65j;->c:LX/673;

    .line 1048429
    const-string v0, ":scheme"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/65j;->d:LX/673;

    .line 1048430
    const-string v0, ":authority"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/65j;->e:LX/673;

    .line 1048431
    const-string v0, ":host"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/65j;->f:LX/673;

    .line 1048432
    const-string v0, ":version"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/65j;->g:LX/673;

    return-void
.end method

.method public constructor <init>(LX/673;LX/673;)V
    .locals 2

    .prologue
    .line 1048421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048422
    iput-object p1, p0, LX/65j;->h:LX/673;

    .line 1048423
    iput-object p2, p0, LX/65j;->i:LX/673;

    .line 1048424
    invoke-virtual {p1}, LX/673;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x20

    invoke-virtual {p2}, LX/673;->e()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/65j;->j:I

    .line 1048425
    return-void
.end method

.method public constructor <init>(LX/673;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1048419
    invoke-static {p2}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/65j;-><init>(LX/673;LX/673;)V

    .line 1048420
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1048433
    invoke-static {p1}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    invoke-static {p2}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/65j;-><init>(LX/673;LX/673;)V

    .line 1048434
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1048410
    instance-of v1, p1, LX/65j;

    if-eqz v1, :cond_0

    .line 1048411
    check-cast p1, LX/65j;

    .line 1048412
    iget-object v1, p0, LX/65j;->h:LX/673;

    iget-object v2, p1, LX/65j;->h:LX/673;

    invoke-virtual {v1, v2}, LX/673;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/65j;->i:LX/673;

    iget-object v2, p1, LX/65j;->i:LX/673;

    .line 1048413
    invoke-virtual {v1, v2}, LX/673;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 1048414
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1048415
    iget-object v0, p0, LX/65j;->h:LX/673;

    invoke-virtual {v0}, LX/673;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1048416
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/65j;->i:LX/673;

    invoke-virtual {v1}, LX/673;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1048417
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1048418
    const-string v0, "%s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/65j;->h:LX/673;

    invoke-virtual {v3}, LX/673;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/65j;->i:LX/673;

    invoke-virtual {v3}, LX/673;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/65A;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
