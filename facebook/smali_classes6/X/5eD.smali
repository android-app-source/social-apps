.class public LX/5eD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 966836
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Ljava/nio/ByteBuffer;
    .locals 4

    .prologue
    .line 966837
    const/4 v1, 0x0

    .line 966838
    :try_start_0
    const-class v0, LX/5eD;

    const-string v2, "/assets/qe_lookup.bin"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 966839
    invoke-static {v1}, LX/0aP;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 966840
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    return-object v0

    .line 966841
    :catch_0
    move-exception v0

    .line 966842
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "IOException encountered while reading resource"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 966843
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;)Ljava/nio/ByteBuffer;
    .locals 7

    .prologue
    .line 966844
    const/4 v0, 0x0

    .line 966845
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "qe_lookup.bin"

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 966846
    :try_start_1
    const/16 v0, -0x1778

    int-to-long v2, v0

    .line 966847
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 966848
    long-to-int v0, v2

    invoke-static {v1, v0}, LX/0aP;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 966849
    :goto_0
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    :goto_1
    return-object v0

    .line 966850
    :cond_0
    :try_start_2
    invoke-static {v1}, LX/0aP;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 966851
    :catch_0
    move-object v1, v0

    :goto_2
    :try_start_3
    invoke-static {}, LX/5eD;->a()Ljava/nio/ByteBuffer;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 966852
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    goto :goto_1

    .line 966853
    :catch_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 966854
    :goto_3
    :try_start_4
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "IOException encountered while reading asset"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 966855
    :catchall_0
    move-exception v0

    :goto_4
    invoke-static {v1}, LX/0VN;->a(Ljava/io/InputStream;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_4

    .line 966856
    :catch_2
    move-exception v0

    goto :goto_3

    .line 966857
    :catch_3
    goto :goto_2
.end method
