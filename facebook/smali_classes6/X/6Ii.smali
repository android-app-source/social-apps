.class public final LX/6Ii;
.super Landroid/hardware/camera2/CameraCaptureSession$StateCallback;
.source ""


# instance fields
.field public final synthetic a:LX/6Iu;


# direct methods
.method public constructor <init>(LX/6Iu;)V
    .locals 0

    .prologue
    .line 1074468
    iput-object p1, p0, LX/6Ii;->a:LX/6Iu;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraCaptureSession$StateCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfigureFailed(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 2

    .prologue
    .line 1074469
    iget-object v0, p0, LX/6Ii;->a:LX/6Iu;

    const/4 v1, 0x2

    .line 1074470
    invoke-static {v0, v1}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074471
    iget-object v0, p0, LX/6Ii;->a:LX/6Iu;

    iget-object v0, v0, LX/6Iu;->e:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$12$3;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$12$3;-><init>(LX/6Ii;)V

    invoke-virtual {v0, v1}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    .line 1074472
    return-void
.end method

.method public final onConfigured(Landroid/hardware/camera2/CameraCaptureSession;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1074473
    iget-object v0, p0, LX/6Ii;->a:LX/6Iu;

    .line 1074474
    iput-object p1, v0, LX/6Iu;->B:Landroid/hardware/camera2/CameraCaptureSession;

    .line 1074475
    :try_start_0
    iget-object v0, p0, LX/6Ii;->a:LX/6Iu;

    const/4 v1, 0x2

    .line 1074476
    invoke-static {v0, v1}, LX/6Iu;->c$redex0(LX/6Iu;I)V

    .line 1074477
    iget-object v0, p0, LX/6Ii;->a:LX/6Iu;

    invoke-static {v0}, LX/6Iu;->s(LX/6Iu;)V

    .line 1074478
    iget-object v0, p0, LX/6Ii;->a:LX/6Iu;

    const/4 v1, 0x1

    .line 1074479
    iput-boolean v1, v0, LX/6Iu;->r:Z

    .line 1074480
    iget-object v0, p0, LX/6Ii;->a:LX/6Iu;

    iget-object v0, v0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_0

    .line 1074481
    iget-object v0, p0, LX/6Ii;->a:LX/6Iu;

    iget-object v0, v0, LX/6Iu;->p:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->start()V

    .line 1074482
    :cond_0
    iget-object v0, p0, LX/6Ii;->a:LX/6Iu;

    iget-object v0, v0, LX/6Iu;->e:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$12$1;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$12$1;-><init>(LX/6Ii;)V

    invoke-virtual {v0, v1}, LX/6Iv;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074483
    :goto_0
    return-void

    .line 1074484
    :catch_0
    move-exception v0

    .line 1074485
    iget-object v1, p0, LX/6Ii;->a:LX/6Iu;

    .line 1074486
    invoke-static {v1, v2}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074487
    iget-object v1, p0, LX/6Ii;->a:LX/6Iu;

    iget-object v1, v1, LX/6Iu;->e:LX/6Iv;

    new-instance v2, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$12$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$12$2;-><init>(LX/6Ii;Ljava/lang/Exception;)V

    invoke-virtual {v1, v2}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
