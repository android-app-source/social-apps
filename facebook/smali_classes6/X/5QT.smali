.class public final enum LX/5QT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5QT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5QT;

.field public static final enum WITHOUT_TABS_LEGACY:LX/5QT;

.field public static final enum WITH_TABS:LX/5QT;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 913218
    new-instance v0, LX/5QT;

    const-string v1, "WITH_TABS"

    invoke-direct {v0, v1, v2}, LX/5QT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QT;->WITH_TABS:LX/5QT;

    .line 913219
    new-instance v0, LX/5QT;

    const-string v1, "WITHOUT_TABS_LEGACY"

    invoke-direct {v0, v1, v3}, LX/5QT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5QT;->WITHOUT_TABS_LEGACY:LX/5QT;

    .line 913220
    const/4 v0, 0x2

    new-array v0, v0, [LX/5QT;

    sget-object v1, LX/5QT;->WITH_TABS:LX/5QT;

    aput-object v1, v0, v2

    sget-object v1, LX/5QT;->WITHOUT_TABS_LEGACY:LX/5QT;

    aput-object v1, v0, v3

    sput-object v0, LX/5QT;->$VALUES:[LX/5QT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 913221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5QT;
    .locals 1

    .prologue
    .line 913222
    const-class v0, LX/5QT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5QT;

    return-object v0
.end method

.method public static values()[LX/5QT;
    .locals 1

    .prologue
    .line 913217
    sget-object v0, LX/5QT;->$VALUES:[LX/5QT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5QT;

    return-object v0
.end method
