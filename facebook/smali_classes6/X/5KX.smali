.class public final LX/5KX;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/5KZ;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/5KY;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 898717
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "jsContext"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "target"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "componentName"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "props"

    aput-object v2, v0, v1

    sput-object v0, LX/5KX;->b:[Ljava/lang/String;

    .line 898718
    sput v3, LX/5KX;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 898749
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 898750
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/5KX;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/5KX;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/5KX;LX/1De;IILX/5KY;)V
    .locals 1

    .prologue
    .line 898745
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 898746
    iput-object p4, p0, LX/5KX;->a:LX/5KY;

    .line 898747
    iget-object v0, p0, LX/5KX;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 898748
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/java2js/JSContext;)LX/5KX;
    .locals 2

    .prologue
    .line 898742
    iget-object v0, p0, LX/5KX;->a:LX/5KY;

    iput-object p1, v0, LX/5KY;->a:Lcom/facebook/java2js/JSContext;

    .line 898743
    iget-object v0, p0, LX/5KX;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 898744
    return-object p0
.end method

.method public final a(Ljava/lang/Object;)LX/5KX;
    .locals 2

    .prologue
    .line 898739
    iget-object v0, p0, LX/5KX;->a:LX/5KY;

    iput-object p1, v0, LX/5KY;->b:Ljava/lang/Object;

    .line 898740
    iget-object v0, p0, LX/5KX;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 898741
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 898735
    invoke-super {p0}, LX/1X5;->a()V

    .line 898736
    const/4 v0, 0x0

    iput-object v0, p0, LX/5KX;->a:LX/5KY;

    .line 898737
    sget-object v0, LX/5KZ;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 898738
    return-void
.end method

.method public final b(Ljava/lang/Object;)LX/5KX;
    .locals 2

    .prologue
    .line 898732
    iget-object v0, p0, LX/5KX;->a:LX/5KY;

    iput-object p1, v0, LX/5KY;->d:Ljava/lang/Object;

    .line 898733
    iget-object v0, p0, LX/5KX;->d:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 898734
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/5KX;
    .locals 2

    .prologue
    .line 898729
    iget-object v0, p0, LX/5KX;->a:LX/5KY;

    iput-object p1, v0, LX/5KY;->c:Ljava/lang/String;

    .line 898730
    iget-object v0, p0, LX/5KX;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 898731
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/5KZ;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 898719
    iget-object v1, p0, LX/5KX;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/5KX;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/5KX;->c:I

    if-ge v1, v2, :cond_2

    .line 898720
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 898721
    :goto_0
    sget v2, LX/5KX;->c:I

    if-ge v0, v2, :cond_1

    .line 898722
    iget-object v2, p0, LX/5KX;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 898723
    sget-object v2, LX/5KX;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 898724
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 898725
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898726
    :cond_2
    iget-object v0, p0, LX/5KX;->a:LX/5KY;

    .line 898727
    invoke-virtual {p0}, LX/5KX;->a()V

    .line 898728
    return-object v0
.end method
