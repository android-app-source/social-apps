.class public LX/65g;
.super LX/65f;
.source ""


# static fields
.field private static a:LX/65g;


# instance fields
.field private c:Z

.field private d:LX/65g;

.field private e:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1048257
    invoke-direct {p0}, LX/65f;-><init>()V

    return-void
.end method

.method private static declared-synchronized a(LX/65g;JZ)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    .line 1048236
    const-class v1, LX/65g;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/65g;->a:LX/65g;

    if-nez v0, :cond_0

    .line 1048237
    new-instance v0, LX/65g;

    invoke-direct {v0}, LX/65g;-><init>()V

    sput-object v0, LX/65g;->a:LX/65g;

    .line 1048238
    new-instance v0, Lokio/AsyncTimeout$Watchdog;

    invoke-direct {v0}, Lokio/AsyncTimeout$Watchdog;-><init>()V

    invoke-virtual {v0}, Lokio/AsyncTimeout$Watchdog;->start()V

    .line 1048239
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 1048240
    cmp-long v0, p1, v4

    if-eqz v0, :cond_3

    if-eqz p3, :cond_3

    .line 1048241
    invoke-virtual {p0}, LX/65f;->d()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-static {p1, p2, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    add-long/2addr v4, v2

    iput-wide v4, p0, LX/65g;->e:J

    .line 1048242
    :goto_0
    invoke-static {p0, v2, v3}, LX/65g;->b(LX/65g;J)J

    move-result-wide v4

    .line 1048243
    sget-object v0, LX/65g;->a:LX/65g;

    .line 1048244
    :goto_1
    iget-object v6, v0, LX/65g;->d:LX/65g;

    if-eqz v6, :cond_1

    iget-object v6, v0, LX/65g;->d:LX/65g;

    invoke-static {v6, v2, v3}, LX/65g;->b(LX/65g;J)J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-gez v6, :cond_6

    .line 1048245
    :cond_1
    iget-object v2, v0, LX/65g;->d:LX/65g;

    iput-object v2, p0, LX/65g;->d:LX/65g;

    .line 1048246
    iput-object p0, v0, LX/65g;->d:LX/65g;

    .line 1048247
    sget-object v2, LX/65g;->a:LX/65g;

    if-ne v0, v2, :cond_2

    .line 1048248
    const-class v0, LX/65g;

    const v2, -0x72c2db49

    invoke-static {v0, v2}, LX/02L;->b(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048249
    :cond_2
    monitor-exit v1

    return-void

    .line 1048250
    :cond_3
    cmp-long v0, p1, v4

    if-eqz v0, :cond_4

    .line 1048251
    add-long v4, v2, p1

    :try_start_1
    iput-wide v4, p0, LX/65g;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1048252
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1048253
    :cond_4
    if-eqz p3, :cond_5

    .line 1048254
    :try_start_2
    invoke-virtual {p0}, LX/65f;->d()J

    move-result-wide v4

    iput-wide v4, p0, LX/65g;->e:J

    goto :goto_0

    .line 1048255
    :cond_5
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048256
    :cond_6
    iget-object v0, v0, LX/65g;->d:LX/65g;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private static declared-synchronized a(LX/65g;)Z
    .locals 3

    .prologue
    .line 1048227
    const-class v1, LX/65g;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/65g;->a:LX/65g;

    :goto_0
    if-eqz v0, :cond_1

    .line 1048228
    iget-object v2, v0, LX/65g;->d:LX/65g;

    if-ne v2, p0, :cond_0

    .line 1048229
    iget-object v2, p0, LX/65g;->d:LX/65g;

    iput-object v2, v0, LX/65g;->d:LX/65g;

    .line 1048230
    const/4 v0, 0x0

    iput-object v0, p0, LX/65g;->d:LX/65g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048231
    const/4 v0, 0x0

    .line 1048232
    :goto_1
    monitor-exit v1

    return v0

    .line 1048233
    :cond_0
    :try_start_1
    iget-object v0, v0, LX/65g;->d:LX/65g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1048234
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 1048235
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(LX/65g;J)J
    .locals 3

    .prologue
    .line 1048226
    iget-wide v0, p0, LX/65g;->e:J

    sub-long/2addr v0, p1

    return-wide v0
.end method

.method public static declared-synchronized e()LX/65g;
    .locals 10

    .prologue
    const-wide/32 v8, 0xf4240

    const/4 v0, 0x0

    .line 1048258
    const-class v2, LX/65g;

    monitor-enter v2

    :try_start_0
    sget-object v1, LX/65g;->a:LX/65g;

    iget-object v1, v1, LX/65g;->d:LX/65g;

    .line 1048259
    if-nez v1, :cond_0

    .line 1048260
    const-class v1, LX/65g;

    const v3, -0x2b540318

    invoke-static {v1, v3}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048261
    :goto_0
    monitor-exit v2

    return-object v0

    .line 1048262
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, LX/65g;->b(LX/65g;J)J

    move-result-wide v4

    .line 1048263
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 1048264
    const-wide/32 v6, 0xf4240

    div-long v6, v4, v6

    .line 1048265
    mul-long/2addr v8, v6

    sub-long/2addr v4, v8

    .line 1048266
    const-class v1, LX/65g;

    long-to-int v3, v4

    const v4, -0x2416b7fe

    invoke-static {v1, v6, v7, v3, v4}, LX/02L;->a(Ljava/lang/Object;JII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1048267
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 1048268
    :cond_1
    :try_start_2
    sget-object v0, LX/65g;->a:LX/65g;

    iget-object v3, v1, LX/65g;->d:LX/65g;

    iput-object v3, v0, LX/65g;->d:LX/65g;

    .line 1048269
    const/4 v0, 0x0

    iput-object v0, v1, LX/65g;->d:LX/65g;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v1

    .line 1048270
    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 1048222
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    .line 1048223
    if-eqz p1, :cond_0

    .line 1048224
    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1048225
    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 1048221
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1048218
    invoke-virtual {p0}, LX/65g;->bI_()Z

    move-result v0

    .line 1048219
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/65g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1048220
    :cond_0
    return-void
.end method

.method public final b(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 1048216
    invoke-virtual {p0}, LX/65g;->bI_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1048217
    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0, p1}, LX/65g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    goto :goto_0
.end method

.method public final bI_()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1048212
    iget-boolean v1, p0, LX/65g;->c:Z

    if-nez v1, :cond_0

    .line 1048213
    :goto_0
    return v0

    .line 1048214
    :cond_0
    iput-boolean v0, p0, LX/65g;->c:Z

    .line 1048215
    invoke-static {p0}, LX/65g;->a(LX/65g;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 1048205
    iget-boolean v0, p0, LX/65g;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unbalanced enter/exit"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048206
    :cond_0
    invoke-virtual {p0}, LX/65f;->bJ_()J

    move-result-wide v0

    .line 1048207
    invoke-virtual {p0}, LX/65f;->bK_()Z

    move-result v2

    .line 1048208
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_1

    if-nez v2, :cond_1

    .line 1048209
    :goto_0
    return-void

    .line 1048210
    :cond_1
    const/4 v3, 0x1

    iput-boolean v3, p0, LX/65g;->c:Z

    .line 1048211
    invoke-static {p0, v0, v1, v2}, LX/65g;->a(LX/65g;JZ)V

    goto :goto_0
.end method
