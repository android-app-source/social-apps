.class public LX/6Kb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:J

.field private e:J

.field private volatile f:J

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 1076879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076880
    iput-wide v0, p0, LX/6Kb;->a:J

    .line 1076881
    iput-wide v0, p0, LX/6Kb;->b:J

    .line 1076882
    iput-wide v0, p0, LX/6Kb;->c:J

    .line 1076883
    iput-wide v0, p0, LX/6Kb;->d:J

    .line 1076884
    iput-wide v0, p0, LX/6Kb;->e:J

    .line 1076885
    iput-wide v0, p0, LX/6Kb;->f:J

    .line 1076886
    const/4 v0, 0x0

    iput v0, p0, LX/6Kb;->g:I

    .line 1076887
    const/16 v0, 0x21

    iput v0, p0, LX/6Kb;->g:I

    .line 1076888
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()LX/6Ka;
    .locals 10

    .prologue
    .line 1076859
    monitor-enter p0

    :try_start_0
    new-instance v0, LX/6Ka;

    iget-wide v2, p0, LX/6Kb;->e:J

    iget-wide v4, p0, LX/6Kb;->d:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    const v2, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v1, v2

    iget-wide v2, p0, LX/6Kb;->f:J

    iget-wide v4, p0, LX/6Kb;->a:J

    iget-wide v6, p0, LX/6Kb;->c:J

    iget-wide v8, p0, LX/6Kb;->b:J

    invoke-direct/range {v0 .. v9}, LX/6Ka;-><init>(FJJJJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 7

    .prologue
    .line 1076860
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, LX/6Kb;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1076861
    iput-wide p1, p0, LX/6Kb;->d:J

    .line 1076862
    :goto_0
    iput-wide p1, p0, LX/6Kb;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076863
    monitor-exit p0

    return-void

    .line 1076864
    :cond_0
    :try_start_1
    iget-wide v0, p0, LX/6Kb;->e:J

    sub-long v0, p1, v0

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    .line 1076865
    iget v2, p0, LX/6Kb;->g:I

    mul-int/lit8 v2, v2, 0x8

    add-int/lit8 v2, v2, 0x5

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 1076866
    iget-wide v2, p0, LX/6Kb;->c:J

    iget v4, p0, LX/6Kb;->g:I

    mul-int/lit8 v4, v4, 0x8

    int-to-long v4, v4

    div-long/2addr v0, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/6Kb;->c:J

    .line 1076867
    :cond_1
    :goto_1
    iget-wide v0, p0, LX/6Kb;->f:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/6Kb;->f:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1076868
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1076869
    :cond_2
    :try_start_2
    iget v2, p0, LX/6Kb;->g:I

    mul-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x5

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 1076870
    iget-wide v2, p0, LX/6Kb;->b:J

    iget v4, p0, LX/6Kb;->g:I

    mul-int/lit8 v4, v4, 0x1

    int-to-long v4, v4

    div-long v4, v0, v4

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/6Kb;->b:J

    .line 1076871
    iget-wide v2, p0, LX/6Kb;->a:J

    iget v4, p0, LX/6Kb;->g:I

    mul-int/lit8 v4, v4, 0x2

    int-to-long v4, v4

    div-long/2addr v0, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/6Kb;->a:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 1076872
    iput-wide v0, p0, LX/6Kb;->a:J

    .line 1076873
    iput-wide v0, p0, LX/6Kb;->b:J

    .line 1076874
    iput-wide v0, p0, LX/6Kb;->c:J

    .line 1076875
    iput-wide v0, p0, LX/6Kb;->d:J

    .line 1076876
    iput-wide v0, p0, LX/6Kb;->e:J

    .line 1076877
    iput-wide v0, p0, LX/6Kb;->f:J

    .line 1076878
    return-void
.end method
