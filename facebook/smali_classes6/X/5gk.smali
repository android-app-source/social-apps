.class public final LX/5gk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 976634
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 976635
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 976636
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 976637
    invoke-static {p0, p1}, LX/5gk;->b(LX/15w;LX/186;)I

    move-result v1

    .line 976638
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 976639
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 976717
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 976718
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 976719
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5gk;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 976720
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 976721
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 976722
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 976673
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 976674
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 976675
    :goto_0
    return v1

    .line 976676
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 976677
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 976678
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 976679
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 976680
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 976681
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 976682
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 976683
    :cond_3
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 976684
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 976685
    :cond_4
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 976686
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 976687
    :cond_5
    const-string v6, "profile_picture"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 976688
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 976689
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v7, :cond_f

    .line 976690
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 976691
    :goto_2
    move v0, v5

    .line 976692
    goto :goto_1

    .line 976693
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 976694
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 976695
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 976696
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 976697
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 976698
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 976699
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 976700
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 976701
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 976702
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_8

    if-eqz v11, :cond_8

    .line 976703
    const-string v12, "height"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 976704
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v10, v7

    move v7, v6

    goto :goto_3

    .line 976705
    :cond_9
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 976706
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_3

    .line 976707
    :cond_a
    const-string v12, "width"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 976708
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v8, v0

    move v0, v6

    goto :goto_3

    .line 976709
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 976710
    :cond_c
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 976711
    if-eqz v7, :cond_d

    .line 976712
    invoke-virtual {p1, v5, v10, v5}, LX/186;->a(III)V

    .line 976713
    :cond_d
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 976714
    if-eqz v0, :cond_e

    .line 976715
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8, v5}, LX/186;->a(III)V

    .line 976716
    :cond_e
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_f
    move v0, v5

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 976640
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 976641
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 976642
    if-eqz v0, :cond_0

    .line 976643
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976644
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 976645
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 976646
    if-eqz v0, :cond_1

    .line 976647
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976648
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 976649
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 976650
    if-eqz v0, :cond_2

    .line 976651
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976652
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 976653
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 976654
    if-eqz v0, :cond_6

    .line 976655
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976656
    const/4 p3, 0x0

    .line 976657
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 976658
    invoke-virtual {p0, v0, p3, p3}, LX/15i;->a(III)I

    move-result v1

    .line 976659
    if-eqz v1, :cond_3

    .line 976660
    const-string p1, "height"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976661
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 976662
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 976663
    if-eqz v1, :cond_4

    .line 976664
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976665
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 976666
    :cond_4
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, p3}, LX/15i;->a(III)I

    move-result v1

    .line 976667
    if-eqz v1, :cond_5

    .line 976668
    const-string p1, "width"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 976669
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 976670
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 976671
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 976672
    return-void
.end method
