.class public LX/5Oo;
.super Ljava/lang/RuntimeException;
.source ""


# instance fields
.field public final originalException:Ljava/lang/Throwable;

.field public final parts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final stage:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 909427
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 909428
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5Oo;->parts:Ljava/util/List;

    .line 909429
    iput-object p3, p0, LX/5Oo;->originalException:Ljava/lang/Throwable;

    .line 909430
    iput-object p1, p0, LX/5Oo;->stage:Ljava/lang/String;

    .line 909431
    iget-object v0, p0, LX/5Oo;->parts:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 909432
    invoke-virtual {p0, p3}, LX/5Oo;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 909433
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 909434
    instance-of v0, p2, LX/5Oo;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 909435
    check-cast v0, LX/5Oo;

    .line 909436
    iget-object v0, v0, LX/5Oo;->parts:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 909437
    :cond_0
    const-class v0, LX/5Oo;

    invoke-static {p2, v0}, LX/1Bz;->propagateIfInstanceOf(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 909438
    new-instance v0, LX/5Oo;

    invoke-direct {v0, p1, p0, p2}, LX/5Oo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
