.class public final enum LX/5jJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5jJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5jJ;

.field public static final enum FINISHING:LX/5jJ;

.field public static final enum NOT_SWIPING:LX/5jJ;

.field public static final enum ON_DOWN:LX/5jJ;

.field public static final enum SWIPING:LX/5jJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 986334
    new-instance v0, LX/5jJ;

    const-string v1, "NOT_SWIPING"

    invoke-direct {v0, v1, v2}, LX/5jJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5jJ;->NOT_SWIPING:LX/5jJ;

    .line 986335
    new-instance v0, LX/5jJ;

    const-string v1, "ON_DOWN"

    invoke-direct {v0, v1, v3}, LX/5jJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5jJ;->ON_DOWN:LX/5jJ;

    .line 986336
    new-instance v0, LX/5jJ;

    const-string v1, "FINISHING"

    invoke-direct {v0, v1, v4}, LX/5jJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5jJ;->FINISHING:LX/5jJ;

    .line 986337
    new-instance v0, LX/5jJ;

    const-string v1, "SWIPING"

    invoke-direct {v0, v1, v5}, LX/5jJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5jJ;->SWIPING:LX/5jJ;

    .line 986338
    const/4 v0, 0x4

    new-array v0, v0, [LX/5jJ;

    sget-object v1, LX/5jJ;->NOT_SWIPING:LX/5jJ;

    aput-object v1, v0, v2

    sget-object v1, LX/5jJ;->ON_DOWN:LX/5jJ;

    aput-object v1, v0, v3

    sget-object v1, LX/5jJ;->FINISHING:LX/5jJ;

    aput-object v1, v0, v4

    sget-object v1, LX/5jJ;->SWIPING:LX/5jJ;

    aput-object v1, v0, v5

    sput-object v0, LX/5jJ;->$VALUES:[LX/5jJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 986341
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5jJ;
    .locals 1

    .prologue
    .line 986340
    const-class v0, LX/5jJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5jJ;

    return-object v0
.end method

.method public static values()[LX/5jJ;
    .locals 1

    .prologue
    .line 986339
    sget-object v0, LX/5jJ;->$VALUES:[LX/5jJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5jJ;

    return-object v0
.end method
