.class public final LX/65T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/657;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1047370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1047371
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LX/65T;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/657;)V
    .locals 1

    .prologue
    .line 1047373
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65T;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047374
    monitor-exit p0

    return-void

    .line 1047375
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/657;)V
    .locals 1

    .prologue
    .line 1047376
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65T;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047377
    monitor-exit p0

    return-void

    .line 1047378
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/657;)Z
    .locals 1

    .prologue
    .line 1047372
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65T;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
