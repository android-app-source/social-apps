.class public LX/6Pm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;",
        "LX/4ZP;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1086364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086365
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6Pm;->a:Ljava/lang/String;

    .line 1086366
    iput-wide p2, p0, LX/6Pm;->b:J

    .line 1086367
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6Pm;->c:Ljava/lang/String;

    .line 1086368
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6Pm;->d:Ljava/lang/String;

    .line 1086369
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6Pm;->e:Ljava/lang/String;

    .line 1086370
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6Pm;->f:Ljava/lang/String;

    .line 1086371
    iput-boolean p8, p0, LX/6Pm;->g:Z

    .line 1086372
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086373
    iget-object v0, p0, LX/6Pm;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 4

    .prologue
    .line 1086374
    check-cast p1, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    check-cast p2, LX/4ZP;

    .line 1086375
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Pm;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1086376
    :cond_0
    :goto_0
    return-void

    .line 1086377
    :cond_1
    iget-wide v0, p0, LX/6Pm;->b:J

    .line 1086378
    iget-object v2, p2, LX/40T;->a:LX/4VK;

    const-string v3, "start_time"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086379
    iget-object v0, p0, LX/6Pm;->c:Ljava/lang/String;

    .line 1086380
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v2, "formatted_start_time"

    invoke-virtual {v1, v2, v0}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086381
    iget-object v0, p0, LX/6Pm;->d:Ljava/lang/String;

    .line 1086382
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v2, "rescheduled_heading"

    invoke-virtual {v1, v2, v0}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086383
    iget-object v0, p0, LX/6Pm;->e:Ljava/lang/String;

    .line 1086384
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v2, "rescheduled_endscreen_title"

    invoke-virtual {v1, v2, v0}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086385
    iget-object v0, p0, LX/6Pm;->f:Ljava/lang/String;

    .line 1086386
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v2, "rescheduled_endscreen_body"

    invoke-virtual {v1, v2, v0}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086387
    iget-boolean v0, p0, LX/6Pm;->g:Z

    .line 1086388
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v2, "is_rescheduled"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086389
    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086390
    const-class v0, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086391
    const-string v0, "VideoBroadcastScheduleMutatingVisitor"

    return-object v0
.end method
