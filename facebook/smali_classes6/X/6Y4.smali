.class public final enum LX/6Y4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Y4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Y4;

.field public static final enum CARRIER_MANAGER:LX/6Y4;

.field public static final enum INTERSTITIAL:LX/6Y4;

.field public static final enum MEGAPHONE:LX/6Y4;

.field public static final enum UNKNOWN:LX/6Y4;


# instance fields
.field private final mLocation:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1109206
    new-instance v0, LX/6Y4;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v3, v2}, LX/6Y4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Y4;->UNKNOWN:LX/6Y4;

    .line 1109207
    new-instance v0, LX/6Y4;

    const-string v1, "MEGAPHONE"

    const-string v2, "megaphone"

    invoke-direct {v0, v1, v4, v2}, LX/6Y4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Y4;->MEGAPHONE:LX/6Y4;

    .line 1109208
    new-instance v0, LX/6Y4;

    const-string v1, "INTERSTITIAL"

    const-string v2, "interstitial"

    invoke-direct {v0, v1, v5, v2}, LX/6Y4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Y4;->INTERSTITIAL:LX/6Y4;

    .line 1109209
    new-instance v0, LX/6Y4;

    const-string v1, "CARRIER_MANAGER"

    const-string v2, "carrier_manager"

    invoke-direct {v0, v1, v6, v2}, LX/6Y4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6Y4;->CARRIER_MANAGER:LX/6Y4;

    .line 1109210
    const/4 v0, 0x4

    new-array v0, v0, [LX/6Y4;

    sget-object v1, LX/6Y4;->UNKNOWN:LX/6Y4;

    aput-object v1, v0, v3

    sget-object v1, LX/6Y4;->MEGAPHONE:LX/6Y4;

    aput-object v1, v0, v4

    sget-object v1, LX/6Y4;->INTERSTITIAL:LX/6Y4;

    aput-object v1, v0, v5

    sget-object v1, LX/6Y4;->CARRIER_MANAGER:LX/6Y4;

    aput-object v1, v0, v6

    sput-object v0, LX/6Y4;->$VALUES:[LX/6Y4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1109197
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1109198
    iput-object p3, p0, LX/6Y4;->mLocation:Ljava/lang/String;

    .line 1109199
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/6Y4;
    .locals 5

    .prologue
    .line 1109200
    if-eqz p0, :cond_1

    .line 1109201
    invoke-static {}, LX/6Y4;->values()[LX/6Y4;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1109202
    iget-object v4, v0, LX/6Y4;->mLocation:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1109203
    :goto_1
    return-object v0

    .line 1109204
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1109205
    :cond_1
    sget-object v0, LX/6Y4;->UNKNOWN:LX/6Y4;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Y4;
    .locals 1

    .prologue
    .line 1109194
    const-class v0, LX/6Y4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Y4;

    return-object v0
.end method

.method public static values()[LX/6Y4;
    .locals 1

    .prologue
    .line 1109196
    sget-object v0, LX/6Y4;->$VALUES:[LX/6Y4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Y4;

    return-object v0
.end method


# virtual methods
.method public final getParamName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109195
    iget-object v0, p0, LX/6Y4;->mLocation:Ljava/lang/String;

    return-object v0
.end method
