.class public final LX/67Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Window$Callback;


# instance fields
.field public final synthetic a:Landroid/support/v4/app/Fragment;

.field public final synthetic b:LX/67a;


# direct methods
.method public constructor <init>(LX/67a;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 1053111
    iput-object p1, p0, LX/67Z;->b:LX/67a;

    iput-object p2, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1053126
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053127
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 1053128
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1053129
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053130
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 1053131
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1053112
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053113
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 1053114
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchKeyShortcutEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1053132
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053133
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 1053134
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1053135
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053136
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 1053137
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1053138
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053139
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 1053140
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 0

    .prologue
    .line 1053141
    return-void
.end method

.method public final onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 0

    .prologue
    .line 1053142
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 0

    .prologue
    .line 1053143
    return-void
.end method

.method public final onContentChanged()V
    .locals 0

    .prologue
    .line 1053144
    return-void
.end method

.method public final onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 1053145
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053146
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    move v0, v1

    .line 1053147
    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 1053148
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053149
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v1

    .line 1053150
    instance-of v0, v0, LX/3qV;

    if-eqz v0, :cond_0

    .line 1053151
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053152
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v1

    .line 1053153
    check-cast v0, LX/3qV;

    .line 1053154
    iget-object v1, p0, LX/67Z;->b:LX/67a;

    iget-object v1, v1, LX/67a;->a:LX/3u3;

    invoke-virtual {v1}, LX/3u3;->b()Landroid/view/MenuInflater;

    move-result-object v1

    .line 1053155
    iget-object p0, v0, LX/3p4;->b:LX/0jz;

    invoke-virtual {p0, p2, v1}, LX/0jz;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    .line 1053156
    const/4 v0, 0x1

    .line 1053157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreatePanelView(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 1053115
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053116
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mHidden:Z

    move v0, v1

    .line 1053117
    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 1053118
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053119
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v1

    .line 1053120
    instance-of v0, v0, LX/3qV;

    if-eqz v0, :cond_0

    .line 1053121
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053122
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v1

    .line 1053123
    check-cast v0, LX/3qV;

    .line 1053124
    iget-object v1, v0, LX/3p4;->b:LX/0jz;

    invoke-virtual {v1}, LX/0jz;->w()Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1053125
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 1053077
    return-void
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1053078
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053079
    iget-object p1, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, p1

    .line 1053080
    instance-of v0, v0, LX/3qV;

    if-eqz v0, :cond_0

    .line 1053081
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053082
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, p0

    .line 1053083
    check-cast v0, LX/3qV;

    .line 1053084
    iget-object p0, v0, LX/3p4;->b:LX/0jz;

    invoke-virtual {p0, p2}, LX/0jz;->a(Landroid/view/MenuItem;)Z

    move-result p0

    move v0, p0

    .line 1053085
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1053086
    const/4 v0, 0x0

    return v0
.end method

.method public final onPanelClosed(ILandroid/view/Menu;)V
    .locals 0

    .prologue
    .line 1053087
    return-void
.end method

.method public final onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1053088
    if-nez p1, :cond_0

    .line 1053089
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053090
    iget-object p1, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, p1

    .line 1053091
    instance-of v0, v0, LX/3qV;

    if-eqz v0, :cond_0

    .line 1053092
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053093
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, p0

    .line 1053094
    check-cast v0, LX/3qV;

    .line 1053095
    iget-object p0, v0, LX/3p4;->b:LX/0jz;

    invoke-virtual {p0, p3}, LX/0jz;->a(Landroid/view/Menu;)Z

    .line 1053096
    const/4 v0, 0x1

    .line 1053097
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSearchRequested()Z
    .locals 1

    .prologue
    .line 1053098
    const/4 v0, 0x0

    return v0
.end method

.method public final onSearchRequested(Landroid/view/SearchEvent;)Z
    .locals 1

    .prologue
    .line 1053099
    const/4 v0, 0x0

    return v0
.end method

.method public final onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
    .locals 0

    .prologue
    .line 1053100
    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 1053101
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053102
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 1053103
    invoke-virtual {v0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    .line 1053104
    return-void
.end method

.method public final onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 1053105
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053106
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 1053107
    invoke-virtual {v0, p1}, Landroid/view/View;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    return-object v0
.end method

.method public final onWindowStartingActionMode(Landroid/view/ActionMode$Callback;I)Landroid/view/ActionMode;
    .locals 1

    .prologue
    .line 1053108
    iget-object v0, p0, LX/67Z;->a:Landroid/support/v4/app/Fragment;

    .line 1053109
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 1053110
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->startActionMode(Landroid/view/ActionMode$Callback;I)Landroid/view/ActionMode;

    move-result-object v0

    return-object v0
.end method
