.class public LX/63B;
.super Landroid/text/method/LinkMovementMethod;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;

.field public static final b:LX/63B;


# instance fields
.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1042912
    const-class v0, LX/63B;

    sput-object v0, LX/63B;->a:Ljava/lang/Class;

    .line 1042913
    new-instance v0, LX/63B;

    invoke-direct {v0}, LX/63B;-><init>()V

    sput-object v0, LX/63B;->b:LX/63B;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1042910
    invoke-direct {p0}, Landroid/text/method/LinkMovementMethod;-><init>()V

    .line 1042911
    return-void
.end method

.method private static a(IILandroid/text/Layout;II)I
    .locals 2

    .prologue
    .line 1042914
    :goto_0
    if-gt p0, p1, :cond_2

    .line 1042915
    add-int v0, p0, p1

    div-int/lit8 v0, v0, 0x2

    .line 1042916
    int-to-float v1, v0

    invoke-virtual {p2, p4, v1}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v1

    .line 1042917
    if-ne v1, p3, :cond_0

    .line 1042918
    :goto_1
    return v0

    .line 1042919
    :cond_0
    if-ge v1, p3, :cond_1

    .line 1042920
    add-int/lit8 p0, v0, 0x1

    goto :goto_0

    .line 1042921
    :cond_1
    add-int/lit8 p1, v0, -0x1

    .line 1042922
    goto :goto_0

    .line 1042923
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static a(IIIII)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 1042909
    new-instance v0, Landroid/graphics/Rect;

    sub-int v1, p0, p4

    sub-int v2, p1, p4

    add-int v3, p2, p4

    add-int v4, p3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method


# virtual methods
.method public final onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 16

    .prologue
    .line 1042858
    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 1042859
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    if-nez v1, :cond_8

    .line 1042860
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 1042861
    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 1042862
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v4

    add-int/2addr v4, v2

    .line 1042863
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v2

    sub-int v2, v3, v2

    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v3

    add-int/2addr v3, v2

    .line 1042864
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    .line 1042865
    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 1042866
    move-object/from16 v0, p0

    iget v1, v0, LX/63B;->c:I

    if-nez v1, :cond_1

    .line 1042867
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41f00000    # 30.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move-object/from16 v0, p0

    iput v1, v0, LX/63B;->c:I

    .line 1042868
    :cond_1
    const/4 v1, 0x0

    .line 1042869
    :goto_0
    invoke-interface/range {p2 .. p2}, Landroid/text/Spannable;->length()I

    move-result v2

    if-ge v1, v2, :cond_7

    .line 1042870
    invoke-interface/range {p2 .. p2}, Landroid/text/Spannable;->length()I

    move-result v2

    const-class v6, Landroid/text/style/ClickableSpan;

    move-object/from16 v0, p2

    invoke-interface {v0, v1, v2, v6}, Landroid/text/Spannable;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v6

    .line 1042871
    const-class v1, Landroid/text/style/ClickableSpan;

    move-object/from16 v0, p2

    invoke-interface {v0, v6, v6, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ClickableSpan;

    .line 1042872
    array-length v2, v1

    if-eqz v2, :cond_7

    .line 1042873
    const/4 v2, 0x0

    aget-object v7, v1, v2

    .line 1042874
    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    .line 1042875
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    .line 1042876
    invoke-virtual {v5, v8}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v9

    .line 1042877
    add-int/lit8 v2, v8, 0x1

    .line 1042878
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1042879
    if-ne v9, v1, :cond_4

    .line 1042880
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 1042881
    invoke-virtual {v5, v1, v9}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 1042882
    iget v11, v9, Landroid/graphics/Rect;->left:I

    iget v12, v9, Landroid/graphics/Rect;->right:I

    invoke-static {v11, v12, v5, v6, v1}, LX/63B;->a(IILandroid/text/Layout;II)I

    move-result v6

    iget v11, v9, Landroid/graphics/Rect;->top:I

    iget v12, v9, Landroid/graphics/Rect;->left:I

    iget v13, v9, Landroid/graphics/Rect;->right:I

    invoke-static {v12, v13, v5, v8, v1}, LX/63B;->a(IILandroid/text/Layout;II)I

    move-result v1

    iget v8, v9, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v9, v0, LX/63B;->c:I

    invoke-static {v6, v11, v1, v8, v9}, LX/63B;->a(IIIII)Landroid/graphics/Rect;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1042883
    :cond_2
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 1042884
    invoke-virtual {v1, v4, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1042885
    const v1, 0x7f0d002b

    new-instance v2, Landroid/graphics/Point;

    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 1042886
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    .line 1042887
    const v1, 0x7f0d002b

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 1042888
    const/4 v1, 0x1

    .line 1042889
    :goto_1
    return v1

    .line 1042890
    :cond_4
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    .line 1042891
    invoke-virtual {v5, v1, v11}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 1042892
    iget v12, v11, Landroid/graphics/Rect;->left:I

    iget v13, v11, Landroid/graphics/Rect;->right:I

    invoke-static {v12, v13, v5, v6, v1}, LX/63B;->a(IILandroid/text/Layout;II)I

    move-result v6

    iget v12, v11, Landroid/graphics/Rect;->top:I

    iget v13, v11, Landroid/graphics/Rect;->right:I

    iget v14, v11, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v15, v0, LX/63B;->c:I

    invoke-static {v6, v12, v13, v14, v15}, LX/63B;->a(IIIII)Landroid/graphics/Rect;

    move-result-object v6

    invoke-interface {v10, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1042893
    invoke-virtual {v5, v9, v11}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 1042894
    iget v6, v11, Landroid/graphics/Rect;->left:I

    iget v12, v11, Landroid/graphics/Rect;->top:I

    iget v13, v11, Landroid/graphics/Rect;->left:I

    iget v14, v11, Landroid/graphics/Rect;->right:I

    invoke-static {v13, v14, v5, v8, v9}, LX/63B;->a(IILandroid/text/Layout;II)I

    move-result v8

    iget v13, v11, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v14, v0, LX/63B;->c:I

    invoke-static {v6, v12, v8, v13, v14}, LX/63B;->a(IIIII)Landroid/graphics/Rect;

    move-result-object v6

    invoke-interface {v10, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1042895
    sub-int v6, v9, v1

    const/4 v8, 0x1

    if-le v6, v8, :cond_2

    .line 1042896
    add-int/lit8 v1, v1, 0x1

    :goto_2
    if-ge v1, v9, :cond_2

    .line 1042897
    invoke-virtual {v5, v1, v11}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 1042898
    iget v6, v11, Landroid/graphics/Rect;->left:I

    iget v8, v11, Landroid/graphics/Rect;->top:I

    iget v12, v11, Landroid/graphics/Rect;->right:I

    iget v13, v11, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v14, v0, LX/63B;->c:I

    invoke-static {v6, v8, v12, v13, v14}, LX/63B;->a(IIIII)Landroid/graphics/Rect;

    move-result-object v6

    invoke-interface {v10, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1042899
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    move v1, v2

    .line 1042900
    goto/16 :goto_0

    .line 1042901
    :cond_6
    invoke-virtual {v5, v3}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v1

    .line 1042902
    int-to-float v2, v4

    invoke-virtual {v5, v1, v2}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v1

    .line 1042903
    const-class v2, Landroid/text/style/ClickableSpan;

    move-object/from16 v0, p2

    invoke-interface {v0, v1, v1, v2}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ClickableSpan;

    .line 1042904
    array-length v2, v1

    if-eqz v2, :cond_7

    .line 1042905
    const/4 v2, 0x0

    aget-object v2, v1, v2

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    const/4 v3, 0x0

    aget-object v1, v1, v3

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    move-object/from16 v0, p2

    invoke-static {v0, v2, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 1042906
    const/4 v1, 0x1

    goto/16 :goto_1

    .line 1042907
    :cond_7
    invoke-static/range {p2 .. p2}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    .line 1042908
    :cond_8
    invoke-super/range {p0 .. p3}, Landroid/text/method/LinkMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v1

    goto/16 :goto_1
.end method
