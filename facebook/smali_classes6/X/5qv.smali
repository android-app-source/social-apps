.class public final LX/5qv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/PopupMenu$OnDismissListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# instance fields
.field public final a:Lcom/facebook/react/bridge/Callback;

.field public b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 1010332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1010333
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5qv;->b:Z

    .line 1010334
    iput-object p1, p0, LX/5qv;->a:Lcom/facebook/react/bridge/Callback;

    .line 1010335
    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/react/bridge/Callback;B)V
    .locals 0

    .prologue
    .line 1010336
    invoke-direct {p0, p1}, LX/5qv;-><init>(Lcom/facebook/react/bridge/Callback;)V

    return-void
.end method


# virtual methods
.method public final onDismiss(Landroid/widget/PopupMenu;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1010337
    iget-boolean v0, p0, LX/5qv;->b:Z

    if-nez v0, :cond_0

    .line 1010338
    iget-object v0, p0, LX/5qv;->a:Lcom/facebook/react/bridge/Callback;

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "dismissed"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1010339
    iput-boolean v4, p0, LX/5qv;->b:Z

    .line 1010340
    :cond_0
    return-void
.end method

.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1010341
    iget-boolean v2, p0, LX/5qv;->b:Z

    if-nez v2, :cond_0

    .line 1010342
    iget-object v2, p0, LX/5qv;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "itemSelected"

    aput-object v4, v3, v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getOrder()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-interface {v2, v3}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1010343
    iput-boolean v0, p0, LX/5qv;->b:Z

    .line 1010344
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
