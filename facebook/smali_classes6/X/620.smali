.class public abstract LX/620;
.super Lcom/facebook/widget/listview/BetterListView;
.source ""


# instance fields
.field private A:LX/61z;

.field private final B:LX/03V;

.field public final C:LX/0So;

.field public a:I

.field public b:I

.field public c:I

.field public d:Landroid/widget/ImageView;

.field public e:Landroid/widget/LinearLayout;

.field public f:Landroid/view/WindowManager;

.field public g:Landroid/view/WindowManager$LayoutParams;

.field private h:I

.field private i:I

.field public j:I

.field public k:I

.field private l:LX/61w;

.field public m:LX/61y;

.field public n:I

.field public o:I

.field public p:I

.field private q:Landroid/view/GestureDetector;

.field public r:Landroid/graphics/Rect;

.field public s:Landroid/graphics/Bitmap;

.field private final t:I

.field private u:I

.field public v:I

.field public w:I

.field public x:Landroid/graphics/drawable/Drawable;

.field public y:Landroid/graphics/drawable/Drawable;

.field public z:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1041102
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/620;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1041103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 1041104
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1041105
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/620;->r:Landroid/graphics/Rect;

    .line 1041106
    iput v4, p0, LX/620;->a:I

    .line 1041107
    iput v4, p0, LX/620;->b:I

    .line 1041108
    iput v4, p0, LX/620;->c:I

    .line 1041109
    iput v4, p0, LX/620;->u:I

    .line 1041110
    iput-object v3, p0, LX/620;->x:Landroid/graphics/drawable/Drawable;

    .line 1041111
    iput-object v3, p0, LX/620;->y:Landroid/graphics/drawable/Drawable;

    .line 1041112
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, LX/620;->t:I

    .line 1041113
    new-instance v0, LX/61z;

    invoke-direct {v0, p0}, LX/61z;-><init>(LX/620;)V

    iput-object v0, p0, LX/620;->A:LX/61z;

    .line 1041114
    if-eqz p2, :cond_0

    .line 1041115
    invoke-virtual {p0}, LX/620;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v3, LX/03r;->DragSortListView:[I

    invoke-virtual {v0, p2, v3, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1041116
    const/16 v3, 0x0

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, LX/620;->a:I

    .line 1041117
    iget v3, p0, LX/620;->a:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, LX/620;->b:I

    .line 1041118
    const/16 v3, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, LX/620;->c:I

    .line 1041119
    const/16 v3, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, p0, LX/620;->u:I

    .line 1041120
    const/16 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, LX/620;->x:Landroid/graphics/drawable/Drawable;

    .line 1041121
    const/16 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iput-object v3, p0, LX/620;->y:Landroid/graphics/drawable/Drawable;

    .line 1041122
    invoke-virtual {p0}, LX/620;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b06c9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, LX/620;->w:I

    .line 1041123
    invoke-virtual {p0}, LX/620;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b06ca

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, LX/620;->v:I

    .line 1041124
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1041125
    :cond_0
    iget v0, p0, LX/620;->a:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Item height must be > 0"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1041126
    iget v0, p0, LX/620;->c:I

    if-lez v0, :cond_2

    :goto_1
    const-string v0, "Grabber id must be valid"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1041127
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 1041128
    invoke-static {v1}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v0

    check-cast v0, LX/0So;

    iput-object v0, p0, LX/620;->C:LX/0So;

    .line 1041129
    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, LX/620;->B:LX/03V;

    .line 1041130
    return-void

    :cond_1
    move v0, v2

    .line 1041131
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1041132
    goto :goto_1
.end method

.method public static a(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 1041133
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 1041134
    :cond_0
    :goto_0
    return-void

    .line 1041135
    :cond_1
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1041136
    if-eqz v0, :cond_0

    .line 1041137
    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1040945
    iget v0, p0, LX/620;->j:I

    sub-int v0, p1, v0

    iget v2, p0, LX/620;->a:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    .line 1040946
    iget-object v3, p0, LX/620;->r:Landroid/graphics/Rect;

    .line 1040947
    invoke-virtual {p0}, LX/620;->getChildCount()I

    move-result v0

    .line 1040948
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_3

    .line 1040949
    invoke-virtual {p0, v0}, LX/620;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    .line 1040950
    invoke-virtual {p1, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 1040951
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1040952
    invoke-virtual {p0}, LX/620;->getFirstVisiblePosition()I

    move-result v3

    add-int/2addr v0, v3

    .line 1040953
    :goto_1
    move v0, v0

    .line 1040954
    if-ltz v0, :cond_1

    .line 1040955
    iget v1, p0, LX/620;->i:I

    if-gt v0, v1, :cond_0

    .line 1040956
    add-int/lit8 v0, v0, 0x1

    .line 1040957
    :cond_0
    :goto_2
    return v0

    .line 1040958
    :cond_1
    if-gez v2, :cond_0

    move v0, v1

    .line 1040959
    goto :goto_2

    .line 1040960
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1040961
    :cond_3
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static f(LX/620;)V
    .locals 12

    .prologue
    const/16 v1, 0x50

    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 1041138
    iget-object v0, p0, LX/620;->C:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v6

    iget-wide v8, p0, LX/620;->z:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x19

    cmp-long v0, v6, v8

    if-gez v0, :cond_0

    .line 1041139
    :goto_0
    return-void

    .line 1041140
    :cond_0
    invoke-static {p0}, LX/620;->getDragSortListAdapter(LX/620;)LX/61x;

    move-result-object v0

    invoke-interface {v0}, LX/61x;->b()I

    move-result v2

    .line 1041141
    invoke-static {p0}, LX/620;->getDragSortListAdapter(LX/620;)LX/61x;

    move-result-object v0

    invoke-interface {v0}, LX/61x;->c()I

    move-result v8

    .line 1041142
    invoke-direct {p0}, LX/620;->getBoundedDragPosition()I

    move-result v0

    .line 1041143
    invoke-virtual {p0}, LX/620;->getFirstVisiblePosition()I

    move-result v5

    sub-int/2addr v0, v5

    .line 1041144
    iget v5, p0, LX/620;->h:I

    iget v6, p0, LX/620;->i:I

    if-lt v5, v6, :cond_1

    iget v5, p0, LX/620;->h:I

    if-ge v5, v8, :cond_1

    .line 1041145
    add-int/lit8 v0, v0, 0x1

    .line 1041146
    :cond_1
    invoke-virtual {p0}, LX/620;->getLastVisiblePosition()I

    move-result v5

    invoke-virtual {p0}, LX/620;->getFirstVisiblePosition()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v9, v5, 0x1

    .line 1041147
    iget v5, p0, LX/620;->i:I

    invoke-virtual {p0}, LX/620;->getFirstVisiblePosition()I

    move-result v6

    sub-int v10, v5, v6

    .line 1041148
    iget v5, p0, LX/620;->i:I

    if-ne v5, v8, :cond_2

    iget v5, p0, LX/620;->h:I

    if-lt v5, v8, :cond_2

    .line 1041149
    add-int/lit8 v0, v0, -0x1

    move v7, v0

    .line 1041150
    :goto_1
    if-ne v2, v8, :cond_3

    .line 1041151
    invoke-virtual {p0, v10}, LX/620;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v2, p0, LX/620;->a:I

    invoke-virtual {p0, v0, v2, v1, v3}, LX/620;->a(Landroid/view/View;III)V

    goto :goto_0

    .line 1041152
    :cond_2
    iget v5, p0, LX/620;->i:I

    if-ne v5, v2, :cond_8

    iget v5, p0, LX/620;->h:I

    if-ge v5, v2, :cond_8

    .line 1041153
    add-int/lit8 v0, v0, 0x1

    move v7, v0

    goto :goto_1

    :cond_3
    move v6, v4

    .line 1041154
    :goto_2
    if-ge v6, v9, :cond_5

    .line 1041155
    invoke-virtual {p0, v6}, LX/620;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 1041156
    if-eqz v11, :cond_5

    .line 1041157
    iget v0, p0, LX/620;->a:I

    .line 1041158
    if-ne v6, v10, :cond_4

    .line 1041159
    const/4 v0, 0x1

    move v2, v3

    move v5, v0

    move v0, v1

    .line 1041160
    :goto_3
    invoke-virtual {p0, v11, v5, v0, v2}, LX/620;->a(Landroid/view/View;III)V

    .line 1041161
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    .line 1041162
    :cond_4
    if-ne v6, v7, :cond_7

    .line 1041163
    iget v2, p0, LX/620;->b:I

    .line 1041164
    iget v0, p0, LX/620;->h:I

    if-lt v0, v8, :cond_6

    .line 1041165
    const/16 v0, 0x30

    move v5, v2

    move v2, v4

    goto :goto_3

    .line 1041166
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->layoutChildren()V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v5, v2

    move v2, v4

    goto :goto_3

    :cond_7
    move v2, v4

    move v5, v0

    move v0, v1

    goto :goto_3

    :cond_8
    move v7, v0

    goto :goto_1
.end method

.method private getBoundedDragPosition()I
    .locals 3

    .prologue
    .line 1041167
    invoke-static {p0}, LX/620;->getDragSortListAdapter(LX/620;)LX/61x;

    move-result-object v0

    invoke-interface {v0}, LX/61x;->b()I

    move-result v1

    .line 1041168
    invoke-static {p0}, LX/620;->getDragSortListAdapter(LX/620;)LX/61x;

    move-result-object v0

    invoke-interface {v0}, LX/61x;->c()I

    move-result v0

    .line 1041169
    iget v2, p0, LX/620;->h:I

    .line 1041170
    if-le v2, v0, :cond_0

    .line 1041171
    :goto_0
    return v0

    .line 1041172
    :cond_0
    if-ge v2, v1, :cond_1

    move v0, v1

    .line 1041173
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public static getDragSortListAdapter(LX/620;)LX/61x;
    .locals 1

    .prologue
    .line 1041174
    invoke-virtual {p0}, LX/620;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, LX/61x;

    return-object v0
.end method

.method public static h(LX/620;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1041175
    iget-object v0, p0, LX/620;->e:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 1041176
    invoke-virtual {p0}, LX/620;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1041177
    iget-object v1, p0, LX/620;->e:Landroid/widget/LinearLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1041178
    iget-object v0, p0, LX/620;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1041179
    iput-object v2, p0, LX/620;->d:Landroid/widget/ImageView;

    .line 1041180
    iput-object v2, p0, LX/620;->e:Landroid/widget/LinearLayout;

    .line 1041181
    :cond_0
    iget-object v0, p0, LX/620;->s:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 1041182
    iget-object v0, p0, LX/620;->s:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1041183
    iput-object v2, p0, LX/620;->s:Landroid/graphics/Bitmap;

    .line 1041184
    :cond_1
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a(Landroid/view/View;III)V
.end method

.method public final addFooterView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1041185
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Footers are not supported with DragSortListView in conjunction with remove_mode"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1041101
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Footers are not supported with DragSortListView in conjunction with remove_mode"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addHeaderView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1041100
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Headers are not supported with DragSortListView"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1041099
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Headers are not supported with DragSortListView"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract b(Landroid/view/View;)Z
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x685d8c74

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1041096
    invoke-super {p0}, Lcom/facebook/widget/listview/BetterListView;->onDetachedFromWindow()V

    .line 1041097
    invoke-static {p0}, LX/620;->h(LX/620;)V

    .line 1041098
    const/16 v1, 0x2d

    const v2, -0x3d046f12

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x4

    const/4 v0, 0x0

    .line 1041022
    iget-object v1, p0, LX/620;->l:LX/61w;

    if-nez v1, :cond_0

    iget-object v1, p0, LX/620;->m:LX/61y;

    if-eqz v1, :cond_1

    .line 1041023
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1041024
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/facebook/widget/listview/BetterListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 1041025
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 1041026
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 1041027
    invoke-virtual {p0, v1, v3}, LX/620;->pointToPosition(II)I

    move-result v4

    .line 1041028
    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 1041029
    invoke-virtual {p0}, LX/620;->getFirstVisiblePosition()I

    move-result v5

    sub-int v5, v4, v5

    invoke-virtual {p0, v5}, LX/620;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1041030
    invoke-virtual {p0, v5}, LX/620;->b(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1041031
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int v6, v3, v6

    iput v6, p0, LX/620;->j:I

    .line 1041032
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v6, v3

    iput v6, p0, LX/620;->k:I

    .line 1041033
    iget v6, p0, LX/620;->c:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1041034
    iget-object v7, p0, LX/620;->r:Landroid/graphics/Rect;

    .line 1041035
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v8

    iput v8, v7, Landroid/graphics/Rect;->left:I

    .line 1041036
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v8

    iput v8, v7, Landroid/graphics/Rect;->right:I

    .line 1041037
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v8

    iput v8, v7, Landroid/graphics/Rect;->top:I

    .line 1041038
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v6

    iput v6, v7, Landroid/graphics/Rect;->bottom:I

    .line 1041039
    iget v6, v7, Landroid/graphics/Rect;->left:I

    if-gt v6, v1, :cond_9

    iget v6, v7, Landroid/graphics/Rect;->right:I

    if-gt v1, v6, :cond_9

    .line 1041040
    iget v1, p0, LX/620;->u:I

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1041041
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v1

    .line 1041042
    :goto_2
    if-nez v1, :cond_2

    .line 1041043
    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1041044
    :cond_2
    invoke-virtual {v5}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v2

    .line 1041045
    invoke-virtual {v5}, Landroid/view/View;->willNotCacheDrawing()Z

    move-result v7

    .line 1041046
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1041047
    invoke-virtual {v5, v0}, Landroid/view/View;->setWillNotCacheDrawing(Z)V

    .line 1041048
    :try_start_0
    invoke-virtual {v5}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1041049
    if-nez v8, :cond_4

    .line 1041050
    iget-object v0, p0, LX/620;->B:LX/03V;

    const-string v1, "bookmark"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The drawing cache is null while dragging the item in the list! "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041051
    invoke-virtual {v5, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1041052
    invoke-virtual {v5, v7}, Landroid/view/View;->setWillNotCacheDrawing(Z)V

    goto/16 :goto_0

    :cond_3
    move v1, v2

    .line 1041053
    goto :goto_2

    .line 1041054
    :cond_4
    :try_start_1
    invoke-virtual {v5}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 1041055
    invoke-virtual {v5, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1041056
    invoke-virtual {v5, v7}, Landroid/view/View;->setWillNotCacheDrawing(Z)V

    .line 1041057
    if-nez v1, :cond_5

    .line 1041058
    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1041059
    :cond_5
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1041060
    invoke-virtual {p0, v1, v9}, LX/620;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    .line 1041061
    iget v1, v1, Landroid/graphics/Rect;->left:I

    const/4 v13, -0x2

    .line 1041062
    invoke-static {p0}, LX/620;->h(LX/620;)V

    .line 1041063
    iget-object v10, p0, LX/620;->C:LX/0So;

    invoke-interface {v10}, LX/0So;->now()J

    move-result-wide v10

    iput-wide v10, p0, LX/620;->z:J

    .line 1041064
    new-instance v10, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v10}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    .line 1041065
    iget-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    const/16 v11, 0x33

    iput v11, v10, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1041066
    iget-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    iput v1, v10, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1041067
    iget-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    iget v11, p0, LX/620;->j:I

    sub-int v11, v3, v11

    iget v12, p0, LX/620;->k:I

    add-int/2addr v11, v12

    iput v11, v10, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1041068
    invoke-virtual {p0}, LX/620;->getContext()Landroid/content/Context;

    move-result-object v10

    const-class v11, Landroid/app/Activity;

    invoke-static {v10, v11}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    if-nez v10, :cond_6

    .line 1041069
    iget-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    const/16 v11, 0x7d7

    iput v11, v10, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 1041070
    :cond_6
    iget-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    iput v13, v10, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1041071
    iget-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    iput v13, v10, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1041072
    iget-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    const/16 v11, 0x198

    iput v11, v10, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1041073
    iget-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    const/4 v11, -0x3

    iput v11, v10, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 1041074
    iget-object v10, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    const/4 v11, 0x0

    iput v11, v10, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1041075
    new-instance v10, Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/620;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v10, p0, LX/620;->d:Landroid/widget/ImageView;

    .line 1041076
    iget-object v10, p0, LX/620;->x:Landroid/graphics/drawable/Drawable;

    if-eqz v10, :cond_7

    .line 1041077
    iget-object v10, p0, LX/620;->d:Landroid/widget/ImageView;

    iget-object v11, p0, LX/620;->x:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1041078
    :cond_7
    iget-object v10, p0, LX/620;->d:Landroid/widget/ImageView;

    invoke-virtual {v10, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1041079
    new-instance v11, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, LX/620;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v11, v10}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1041080
    iget-object v10, p0, LX/620;->d:Landroid/widget/ImageView;

    invoke-virtual {v11, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1041081
    iget-object v10, p0, LX/620;->y:Landroid/graphics/drawable/Drawable;

    if-eqz v10, :cond_8

    .line 1041082
    iget-object v10, p0, LX/620;->y:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11, v10}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1041083
    :cond_8
    iput-object v8, p0, LX/620;->s:Landroid/graphics/Bitmap;

    .line 1041084
    invoke-virtual {p0}, LX/620;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    invoke-static {v10}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v10

    check-cast v10, Landroid/view/WindowManager;

    iput-object v10, p0, LX/620;->f:Landroid/view/WindowManager;

    .line 1041085
    iget-object v10, p0, LX/620;->f:Landroid/view/WindowManager;

    iget-object v12, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v10, v11, v12}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1041086
    iput-object v11, p0, LX/620;->e:Landroid/widget/LinearLayout;

    .line 1041087
    iput v4, p0, LX/620;->h:I

    .line 1041088
    iget v1, p0, LX/620;->h:I

    iput v1, p0, LX/620;->i:I

    .line 1041089
    invoke-virtual {p0}, LX/620;->getHeight()I

    move-result v1

    iput v1, p0, LX/620;->p:I

    .line 1041090
    iget v1, p0, LX/620;->t:I

    .line 1041091
    sub-int v2, v3, v1

    iget v4, p0, LX/620;->p:I

    div-int/lit8 v4, v4, 0x3

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, LX/620;->n:I

    .line 1041092
    add-int/2addr v1, v3

    iget v2, p0, LX/620;->p:I

    mul-int/lit8 v2, v2, 0x2

    div-int/lit8 v2, v2, 0x3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, LX/620;->o:I

    goto/16 :goto_1

    .line 1041093
    :catchall_0
    move-exception v0

    invoke-virtual {v5, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1041094
    invoke-virtual {v5, v7}, Landroid/view/View;->setWillNotCacheDrawing(Z)V

    throw v0

    .line 1041095
    :cond_9
    iput-object v9, p0, LX/620;->e:Landroid/widget/LinearLayout;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x1a767e7a

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1040962
    iget-object v2, p0, LX/620;->q:Landroid/view/GestureDetector;

    if-eqz v2, :cond_0

    .line 1040963
    iget-object v2, p0, LX/620;->q:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1040964
    :cond_0
    iget-object v2, p0, LX/620;->l:LX/61w;

    if-nez v2, :cond_1

    iget-object v2, p0, LX/620;->m:LX/61y;

    if-eqz v2, :cond_9

    :cond_1
    iget-object v2, p0, LX/620;->e:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_9

    .line 1040965
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 1040966
    packed-switch v2, :pswitch_data_0

    .line 1040967
    :cond_2
    :goto_0
    const v2, 0x8807caa

    invoke-static {v2, v1}, LX/02F;->a(II)V

    .line 1040968
    :goto_1
    return v0

    .line 1040969
    :pswitch_0
    iget-object v2, p0, LX/620;->r:Landroid/graphics/Rect;

    .line 1040970
    iget-object v3, p0, LX/620;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 1040971
    iget-object v2, p0, LX/620;->A:LX/61z;

    .line 1040972
    const/4 v3, 0x0

    iput v3, v2, LX/61z;->d:F

    .line 1040973
    const/4 v2, 0x0

    .line 1040974
    :goto_2
    invoke-virtual {p0, v2}, LX/620;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1040975
    if-nez v3, :cond_3

    .line 1040976
    invoke-virtual {p0}, Lcom/facebook/widget/listview/BetterListView;->layoutChildren()V

    .line 1040977
    invoke-virtual {p0, v2}, LX/620;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1040978
    if-eqz v3, :cond_4

    .line 1040979
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1040980
    :cond_4
    const/4 v2, 0x0

    :goto_3
    invoke-virtual {p0}, LX/620;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 1040981
    invoke-virtual {p0, v2}, LX/620;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1040982
    invoke-virtual {p0, v3}, LX/620;->a(Landroid/view/View;)V

    .line 1040983
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1040984
    :cond_5
    invoke-static {p0}, LX/620;->h(LX/620;)V

    .line 1040985
    iget-object v2, p0, LX/620;->m:LX/61y;

    if-eqz v2, :cond_2

    iget v2, p0, LX/620;->h:I

    iget v3, p0, LX/620;->i:I

    if-eq v2, v3, :cond_2

    .line 1040986
    iget-object v2, p0, LX/620;->m:LX/61y;

    iget v3, p0, LX/620;->i:I

    invoke-direct {p0}, LX/620;->getBoundedDragPosition()I

    move-result v4

    invoke-interface {v2, v3, v4}, LX/61y;->a(II)V

    goto :goto_0

    .line 1040987
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 1040988
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 1040989
    iget-object v3, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    iget v4, p0, LX/620;->j:I

    sub-int v4, v2, v4

    iget p1, p0, LX/620;->k:I

    add-int/2addr v4, p1

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1040990
    iget-object v3, p0, LX/620;->f:Landroid/view/WindowManager;

    iget-object v4, p0, LX/620;->e:Landroid/widget/LinearLayout;

    iget-object p1, p0, LX/620;->g:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v3, v4, p1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1040991
    invoke-direct {p0, v2}, LX/620;->b(I)I

    move-result v3

    .line 1040992
    if-ltz v3, :cond_2

    .line 1040993
    iput v3, p0, LX/620;->h:I

    .line 1040994
    invoke-static {p0}, LX/620;->f(LX/620;)V

    .line 1040995
    iget v3, p0, LX/620;->p:I

    div-int/lit8 v3, v3, 0x3

    if-lt v2, v3, :cond_6

    .line 1040996
    iget v3, p0, LX/620;->p:I

    div-int/lit8 v3, v3, 0x3

    iput v3, p0, LX/620;->n:I

    .line 1040997
    :cond_6
    iget v3, p0, LX/620;->p:I

    mul-int/lit8 v3, v3, 0x2

    div-int/lit8 v3, v3, 0x3

    if-gt v2, v3, :cond_7

    .line 1040998
    iget v3, p0, LX/620;->p:I

    mul-int/lit8 v3, v3, 0x2

    div-int/lit8 v3, v3, 0x3

    iput v3, p0, LX/620;->o:I

    .line 1040999
    :cond_7
    iget-object v3, p0, LX/620;->A:LX/61z;

    .line 1041000
    iget-object v5, v3, LX/61z;->a:LX/620;

    iget v5, v5, LX/620;->o:I

    if-le v2, v5, :cond_c

    iget-object v5, v3, LX/61z;->a:LX/620;

    invoke-virtual {v5}, LX/620;->getLastVisiblePosition()I

    move-result v5

    iget-object v6, v3, LX/61z;->a:LX/620;

    invoke-static {v6}, LX/620;->getDragSortListAdapter(LX/620;)LX/61x;

    move-result-object v6

    invoke-interface {v6}, LX/61x;->c()I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    if-ge v5, v6, :cond_c

    .line 1041001
    iget-object v5, v3, LX/61z;->a:LX/620;

    iget v5, v5, LX/620;->p:I

    iget-object v6, v3, LX/61z;->a:LX/620;

    iget v6, v6, LX/620;->o:I

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    .line 1041002
    if-le v2, v5, :cond_b

    .line 1041003
    iget-object v6, v3, LX/61z;->a:LX/620;

    iget v6, v6, LX/620;->p:I

    sub-int/2addr v6, v5

    .line 1041004
    sub-int v5, v2, v5

    div-int/2addr v5, v6

    int-to-float v5, v5

    .line 1041005
    iget-object v6, v3, LX/61z;->a:LX/620;

    iget v6, v6, LX/620;->v:I

    int-to-float v6, v6

    iget-object v7, v3, LX/61z;->a:LX/620;

    iget v7, v7, LX/620;->w:I

    iget-object v8, v3, LX/61z;->a:LX/620;

    iget v8, v8, LX/620;->v:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v5, v7

    add-float/2addr v5, v6

    float-to-int v5, v5

    .line 1041006
    :goto_4
    move v5, v5

    .line 1041007
    int-to-float v5, v5

    iput v5, v3, LX/61z;->d:F

    .line 1041008
    iget-boolean v5, v3, LX/61z;->c:Z

    if-nez v5, :cond_8

    iget v5, v3, LX/61z;->d:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-nez v5, :cond_a

    .line 1041009
    :cond_8
    :goto_5
    goto/16 :goto_0

    .line 1041010
    :cond_9
    invoke-super {p0, p1}, Lcom/facebook/widget/listview/BetterListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0xfd996b5

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto/16 :goto_1

    .line 1041011
    :cond_a
    const/4 v5, 0x1

    iput-boolean v5, v3, LX/61z;->c:Z

    .line 1041012
    iget-object v5, v3, LX/61z;->a:LX/620;

    iget-object v5, v5, LX/620;->C:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v5

    iput-wide v5, v3, LX/61z;->b:J

    .line 1041013
    iget-object v5, v3, LX/61z;->a:LX/620;

    iget-object v6, v3, LX/61z;->e:Ljava/lang/Runnable;

    invoke-static {v5, v6}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_5

    .line 1041014
    :cond_b
    iget-object v5, v3, LX/61z;->a:LX/620;

    iget v5, v5, LX/620;->v:I

    goto :goto_4

    .line 1041015
    :cond_c
    iget-object v5, v3, LX/61z;->a:LX/620;

    iget v5, v5, LX/620;->n:I

    if-ge v2, v5, :cond_e

    iget-object v5, v3, LX/61z;->a:LX/620;

    invoke-virtual {v5}, LX/620;->getFirstVisiblePosition()I

    move-result v5

    iget-object v6, v3, LX/61z;->a:LX/620;

    invoke-static {v6}, LX/620;->getDragSortListAdapter(LX/620;)LX/61x;

    move-result-object v6

    invoke-interface {v6}, LX/61x;->b()I

    move-result v6

    add-int/lit8 v6, v6, -0x2

    if-le v5, v6, :cond_e

    .line 1041016
    iget-object v5, v3, LX/61z;->a:LX/620;

    iget v5, v5, LX/620;->n:I

    div-int/lit8 v5, v5, 0x2

    if-ge v2, v5, :cond_d

    .line 1041017
    iget-object v5, v3, LX/61z;->a:LX/620;

    iget v5, v5, LX/620;->n:I

    div-int/lit8 v5, v5, 0x2

    .line 1041018
    sub-int v6, v5, v2

    div-int v5, v6, v5

    int-to-float v5, v5

    .line 1041019
    iget-object v6, v3, LX/61z;->a:LX/620;

    iget v6, v6, LX/620;->v:I

    int-to-float v6, v6

    iget-object v7, v3, LX/61z;->a:LX/620;

    iget v7, v7, LX/620;->w:I

    iget-object v8, v3, LX/61z;->a:LX/620;

    iget v8, v8, LX/620;->v:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v5, v7

    add-float/2addr v5, v6

    float-to-int v5, v5

    neg-int v5, v5

    goto :goto_4

    .line 1041020
    :cond_d
    iget-object v5, v3, LX/61z;->a:LX/620;

    iget v5, v5, LX/620;->v:I

    neg-int v5, v5

    goto :goto_4

    .line 1041021
    :cond_e
    const/4 v5, 0x0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 1040944
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, LX/620;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 1040940
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/61x;

    if-nez v0, :cond_0

    .line 1040941
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "DragSortListView expects a DragSortListAdapter based adapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1040942
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1040943
    return-void
.end method

.method public setDragListener(LX/61w;)V
    .locals 0

    .prologue
    .line 1040938
    iput-object p1, p0, LX/620;->l:LX/61w;

    .line 1040939
    return-void
.end method
