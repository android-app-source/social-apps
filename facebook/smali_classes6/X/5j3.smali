.class public final LX/5j3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 985477
    const/4 v13, 0x0

    .line 985478
    const/4 v12, 0x0

    .line 985479
    const/4 v11, 0x0

    .line 985480
    const/4 v10, 0x0

    .line 985481
    const/4 v7, 0x0

    .line 985482
    const-wide/16 v8, 0x0

    .line 985483
    const/4 v6, 0x0

    .line 985484
    const/4 v5, 0x0

    .line 985485
    const/4 v4, 0x0

    .line 985486
    const/4 v3, 0x0

    .line 985487
    const/4 v2, 0x0

    .line 985488
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 985489
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 985490
    const/4 v2, 0x0

    .line 985491
    :goto_0
    return v2

    .line 985492
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v15, :cond_b

    .line 985493
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 985494
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 985495
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_0

    if-eqz v3, :cond_0

    .line 985496
    const-string v15, "client_generated_text_info"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 985497
    invoke-static/range {p0 .. p1}, LX/5iw;->a(LX/15w;LX/186;)I

    move-result v3

    move v14, v3

    goto :goto_1

    .line 985498
    :cond_1
    const-string v15, "custom_font"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 985499
    invoke-static/range {p0 .. p1}, LX/5iy;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 985500
    :cond_2
    const-string v15, "font_name"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 985501
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 985502
    :cond_3
    const-string v15, "landscape_anchoring"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 985503
    invoke-static/range {p0 .. p1}, LX/5iz;->a(LX/15w;LX/186;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 985504
    :cond_4
    const-string v15, "portrait_anchoring"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 985505
    invoke-static/range {p0 .. p1}, LX/5j0;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 985506
    :cond_5
    const-string v15, "rotation_degree"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 985507
    const/4 v2, 0x1

    .line 985508
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto :goto_1

    .line 985509
    :cond_6
    const-string v15, "text_color"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 985510
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 985511
    :cond_7
    const-string v15, "text_content"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 985512
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 985513
    :cond_8
    const-string v15, "text_landscape_size"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 985514
    invoke-static/range {p0 .. p1}, LX/5j1;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 985515
    :cond_9
    const-string v15, "text_portrait_size"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 985516
    invoke-static/range {p0 .. p1}, LX/5j2;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 985517
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 985518
    :cond_b
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 985519
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 985520
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 985521
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 985522
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 985523
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 985524
    if-eqz v2, :cond_c

    .line 985525
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 985526
    :cond_c
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 985527
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 985528
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 985529
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 985530
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v6

    move v6, v7

    move v7, v10

    move v10, v5

    move-wide/from16 v17, v8

    move v9, v4

    move v8, v3

    move-wide/from16 v4, v17

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 985531
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 985532
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985533
    if-eqz v0, :cond_1

    .line 985534
    const-string v1, "client_generated_text_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985535
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 985536
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 985537
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2}, LX/5iw;->a(LX/15i;ILX/0nX;)V

    .line 985538
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 985539
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 985540
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985541
    if-eqz v0, :cond_2

    .line 985542
    const-string v1, "custom_font"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985543
    invoke-static {p0, v0, p2, p3}, LX/5iy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 985544
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 985545
    if-eqz v0, :cond_3

    .line 985546
    const-string v1, "font_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985547
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 985548
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985549
    if-eqz v0, :cond_4

    .line 985550
    const-string v1, "landscape_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985551
    invoke-static {p0, v0, p2}, LX/5iz;->a(LX/15i;ILX/0nX;)V

    .line 985552
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985553
    if-eqz v0, :cond_5

    .line 985554
    const-string v1, "portrait_anchoring"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985555
    invoke-static {p0, v0, p2}, LX/5j0;->a(LX/15i;ILX/0nX;)V

    .line 985556
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 985557
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_6

    .line 985558
    const-string v2, "rotation_degree"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985559
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 985560
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 985561
    if-eqz v0, :cond_7

    .line 985562
    const-string v1, "text_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985563
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 985564
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 985565
    if-eqz v0, :cond_8

    .line 985566
    const-string v1, "text_content"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985567
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 985568
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985569
    if-eqz v0, :cond_9

    .line 985570
    const-string v1, "text_landscape_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985571
    invoke-static {p0, v0, p2}, LX/5j1;->a(LX/15i;ILX/0nX;)V

    .line 985572
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 985573
    if-eqz v0, :cond_a

    .line 985574
    const-string v1, "text_portrait_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985575
    invoke-static {p0, v0, p2}, LX/5j2;->a(LX/15i;ILX/0nX;)V

    .line 985576
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 985577
    return-void
.end method
