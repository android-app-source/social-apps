.class public LX/5NU;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/4CM;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/4CM;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 906357
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/4CM;
    .locals 7

    .prologue
    .line 906358
    sget-object v0, LX/5NU;->a:LX/4CM;

    if-nez v0, :cond_1

    .line 906359
    const-class v1, LX/5NU;

    monitor-enter v1

    .line 906360
    :try_start_0
    sget-object v0, LX/5NU;->a:LX/4CM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 906361
    if-eqz v2, :cond_0

    .line 906362
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 906363
    invoke-static {v0}, LX/4eT;->a(LX/0QB;)LX/1G9;

    move-result-object v3

    check-cast v3, LX/1G9;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v5

    check-cast v5, LX/1FZ;

    invoke-static {v0}, LX/1Fp;->a(LX/0QB;)LX/1FE;

    move-result-object v6

    check-cast v6, LX/1FE;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    invoke-static {v3, v4, v5, v6, p0}, LX/2uL;->a(LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/1FZ;LX/1FE;LX/0W3;)LX/4CM;

    move-result-object v3

    move-object v0, v3

    .line 906364
    sput-object v0, LX/5NU;->a:LX/4CM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 906365
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 906366
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 906367
    :cond_1
    sget-object v0, LX/5NU;->a:LX/4CM;

    return-object v0

    .line 906368
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 906369
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 906370
    invoke-static {p0}, LX/4eT;->a(LX/0QB;)LX/1G9;

    move-result-object v0

    check-cast v0, LX/1G9;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v2

    check-cast v2, LX/1FZ;

    invoke-static {p0}, LX/1Fp;->a(LX/0QB;)LX/1FE;

    move-result-object v3

    check-cast v3, LX/1FE;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v4

    check-cast v4, LX/0W3;

    invoke-static {v0, v1, v2, v3, v4}, LX/2uL;->a(LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/1FZ;LX/1FE;LX/0W3;)LX/4CM;

    move-result-object v0

    return-object v0
.end method
