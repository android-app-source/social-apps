.class public LX/5eI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/1Nt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nt",
            "<TP;*TE;TV;>;"
        }
    .end annotation
.end field

.field public final b:LX/1PW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field public d:LX/5eH;

.field public e:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Nt;LX/1PW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Nt",
            "<TP;*TE;TV;>;TE;)V"
        }
    .end annotation

    .prologue
    .line 966880
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966881
    iput-object p1, p0, LX/5eI;->a:LX/1Nt;

    .line 966882
    iput-object p2, p0, LX/5eI;->b:LX/1PW;

    .line 966883
    return-void
.end method

.method public static a(LX/5eI;LX/5eH;)V
    .locals 2

    .prologue
    .line 966884
    iget-object v0, p0, LX/5eI;->b:LX/1PW;

    iget-object v1, p0, LX/5eI;->e:Landroid/view/View;

    invoke-static {v0, p1, v1}, LX/1aP;->b(LX/1PW;LX/5eH;Landroid/view/View;)V

    .line 966885
    return-void
.end method

.method private a(Ljava/lang/Object;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;TV;)V"
        }
    .end annotation

    .prologue
    .line 966886
    iget-object v0, p0, LX/5eI;->d:LX/5eH;

    if-nez v0, :cond_0

    .line 966887
    new-instance v0, LX/5eH;

    iget-object v1, p0, LX/5eI;->a:LX/1Nt;

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2, p1}, LX/5eH;-><init>(LX/1Nt;ILjava/lang/Object;)V

    iput-object v0, p0, LX/5eI;->d:LX/5eH;

    .line 966888
    iget-object v0, p0, LX/5eI;->b:LX/1PW;

    iget-object v1, p0, LX/5eI;->d:LX/5eH;

    invoke-static {v0, v1}, LX/1aP;->a(LX/1PW;LX/5eH;)V

    .line 966889
    :cond_0
    iget-object v0, p0, LX/5eI;->d:LX/5eH;

    move-object v0, v0

    .line 966890
    iget-object v1, p0, LX/5eI;->b:LX/1PW;

    invoke-static {v1, v0, p2}, LX/1aP;->a(LX/1PW;LX/5eH;Landroid/view/View;)V

    .line 966891
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 966892
    iget-object v0, p0, LX/5eI;->c:Ljava/lang/Object;

    invoke-direct {p0, v0, p1}, LX/5eI;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 966893
    iput-object p1, p0, LX/5eI;->e:Landroid/view/View;

    .line 966894
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)V"
        }
    .end annotation

    .prologue
    .line 966895
    iget-object v0, p0, LX/5eI;->c:Ljava/lang/Object;

    if-ne v0, p1, :cond_1

    .line 966896
    :cond_0
    :goto_0
    return-void

    .line 966897
    :cond_1
    iget-object v0, p0, LX/5eI;->d:LX/5eH;

    .line 966898
    iput-object p1, p0, LX/5eI;->c:Ljava/lang/Object;

    .line 966899
    const/4 v1, 0x0

    iput-object v1, p0, LX/5eI;->d:LX/5eH;

    .line 966900
    iget-object v1, p0, LX/5eI;->e:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 966901
    invoke-static {p0, v0}, LX/5eI;->a(LX/5eI;LX/5eH;)V

    .line 966902
    iget-object v0, p0, LX/5eI;->c:Ljava/lang/Object;

    iget-object v1, p0, LX/5eI;->e:Landroid/view/View;

    invoke-direct {p0, v0, v1}, LX/5eI;->a(Ljava/lang/Object;Landroid/view/View;)V

    goto :goto_0
.end method
