.class public LX/5Mj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/io/FileDescriptor;

.field private final c:Ljava/io/PrintWriter;

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/io/PrintWriter;

.field public final f:LX/5Mk;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;LX/5Mk;)V
    .locals 2

    .prologue
    .line 905671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905672
    iput-object p1, p0, LX/5Mj;->a:Ljava/lang/String;

    .line 905673
    iput-object p2, p0, LX/5Mj;->b:Ljava/io/FileDescriptor;

    .line 905674
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/PrintWriter;

    iput-object v0, p0, LX/5Mj;->c:Ljava/io/PrintWriter;

    .line 905675
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/5Mj;->d:LX/0Px;

    .line 905676
    new-instance v0, Ljava/io/PrintWriter;

    new-instance v1, LX/45h;

    invoke-direct {v1, p3, p1}, LX/45h;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    iput-object v0, p0, LX/5Mj;->e:Ljava/io/PrintWriter;

    .line 905677
    iput-object p5, p0, LX/5Mj;->f:LX/5Mk;

    .line 905678
    return-void
.end method
