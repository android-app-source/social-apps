.class public LX/5MS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5MG;


# static fields
.field public static final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/5MR;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:LX/5MR;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/regex/Pattern;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 905510
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "in"

    sget-object v2, LX/5MR;->IN:LX/5MR;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "nin"

    sget-object v2, LX/5MR;->NIN:LX/5MR;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "contains"

    sget-object v2, LX/5MR;->CONTAINS:LX/5MR;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "regex"

    sget-object v2, LX/5MR;->REGEX:LX/5MR;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/5MS;->e:LX/0P1;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/5MR;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/5MR;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 905511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905512
    iput-object p1, p0, LX/5MS;->a:Ljava/lang/String;

    .line 905513
    iput-object p2, p0, LX/5MS;->b:LX/5MR;

    .line 905514
    iput-object p3, p0, LX/5MS;->c:Ljava/util/List;

    .line 905515
    sget-object v0, LX/5MQ;->a:[I

    iget-object v1, p0, LX/5MS;->b:LX/5MR;

    invoke-virtual {v1}, LX/5MR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 905516
    new-instance v0, LX/5MH;

    const-string v1, "Illegal string strategy"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905517
    :pswitch_0
    iget-object v0, p0, LX/5MS;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/5MS;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 905518
    :cond_0
    new-instance v0, LX/5MH;

    const-string v1, "Mismatching number of values"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905519
    :pswitch_1
    iget-object v0, p0, LX/5MS;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/5MS;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v2, :cond_4

    .line 905520
    :cond_1
    new-instance v0, LX/5MH;

    const-string v1, "Mismatching number of values"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905521
    :pswitch_2
    iget-object v0, p0, LX/5MS;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/5MS;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v2, :cond_3

    .line 905522
    :cond_2
    new-instance v0, LX/5MH;

    const-string v1, "Mismatching number of values"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 905523
    :cond_3
    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/5MS;->d:Ljava/util/regex/Pattern;
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 905524
    :cond_4
    return-void

    .line 905525
    :catch_0
    new-instance v0, LX/5MH;

    const-string v1, "Regex syntax error"

    invoke-direct {v0, v1}, LX/5MH;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 905526
    iget-object v0, p0, LX/5MS;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0uE;)Z
    .locals 3

    .prologue
    .line 905527
    invoke-virtual {p1}, LX/0uE;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 905528
    sget-object v2, LX/5MQ;->a:[I

    iget-object p1, p0, LX/5MS;->b:LX/5MR;

    invoke-virtual {p1}, LX/5MR;->ordinal()I

    move-result p1

    aget v2, v2, p1

    packed-switch v2, :pswitch_data_0

    .line 905529
    :cond_0
    :goto_0
    move v0, v1

    .line 905530
    return v0

    .line 905531
    :pswitch_0
    iget-object v1, p0, LX/5MS;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 905532
    :pswitch_1
    iget-object v2, p0, LX/5MS;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 905533
    :pswitch_2
    iget-object v2, p0, LX/5MS;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    goto :goto_0

    .line 905534
    :pswitch_3
    iget-object v1, p0, LX/5MS;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 905535
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
