.class public LX/6J8;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1075349
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1075350
    sput-object v0, LX/6J8;->a:Ljava/util/List;

    sget-object v1, LX/6JO;->CONTINUOUS_VIDEO:LX/6JO;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075351
    sget-object v0, LX/6J8;->a:Ljava/util/List;

    sget-object v1, LX/6JO;->CONTINUOUS_PICTURE:LX/6JO;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075352
    sget-object v0, LX/6J8;->a:Ljava/util/List;

    sget-object v1, LX/6JO;->EXTENDED_DOF:LX/6JO;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075353
    sget-object v0, LX/6J8;->a:Ljava/util/List;

    sget-object v1, LX/6JO;->AUTO:LX/6JO;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075354
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1075355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075356
    return-void
.end method

.method public static a(Ljava/util/List;)LX/6JO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6JO;",
            ">;)",
            "LX/6JO;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1075357
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1075358
    :goto_0
    return-object v0

    .line 1075359
    :cond_0
    const/4 v0, 0x0

    :goto_1
    sget-object v2, LX/6J8;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1075360
    sget-object v2, LX/6J8;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1075361
    sget-object v1, LX/6J8;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JO;

    goto :goto_0

    .line 1075362
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 1075363
    goto :goto_0
.end method
