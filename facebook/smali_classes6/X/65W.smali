.class public final LX/65W;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/64R;

.field public b:LX/657;

.field private final c:LX/64c;

.field public final d:LX/65V;

.field private e:I

.field private f:LX/65S;

.field private g:Z

.field private h:Z

.field private i:LX/66G;


# direct methods
.method public constructor <init>(LX/64c;LX/64R;)V
    .locals 2

    .prologue
    .line 1047602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1047603
    iput-object p1, p0, LX/65W;->c:LX/64c;

    .line 1047604
    iput-object p2, p0, LX/65W;->a:LX/64R;

    .line 1047605
    new-instance v0, LX/65V;

    invoke-direct {p0}, LX/65W;->g()LX/65T;

    move-result-object v1

    invoke-direct {v0, p2, v1}, LX/65V;-><init>(LX/64R;LX/65T;)V

    iput-object v0, p0, LX/65W;->d:LX/65V;

    .line 1047606
    return-void
.end method

.method private static a(LX/65W;IIIZ)LX/65S;
    .locals 6

    .prologue
    .line 1047566
    iget-object v1, p0, LX/65W;->c:LX/64c;

    monitor-enter v1

    .line 1047567
    :try_start_0
    iget-boolean v0, p0, LX/65W;->g:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "released"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047568
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1047569
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/65W;->i:LX/66G;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "stream != null"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047570
    :cond_1
    iget-boolean v0, p0, LX/65W;->h:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Canceled"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047571
    :cond_2
    iget-object v0, p0, LX/65W;->f:LX/65S;

    .line 1047572
    if-eqz v0, :cond_3

    iget-boolean v2, v0, LX/65S;->h:Z

    if-nez v2, :cond_3

    .line 1047573
    monitor-exit v1

    .line 1047574
    :goto_0
    return-object v0

    .line 1047575
    :cond_3
    sget-object v0, LX/64t;->a:LX/64t;

    iget-object v2, p0, LX/65W;->c:LX/64c;

    iget-object v3, p0, LX/65W;->a:LX/64R;

    invoke-virtual {v0, v2, v3, p0}, LX/64t;->a(LX/64c;LX/64R;LX/65W;)LX/65S;

    move-result-object v0

    .line 1047576
    if-eqz v0, :cond_4

    .line 1047577
    iput-object v0, p0, LX/65W;->f:LX/65S;

    .line 1047578
    monitor-exit v1

    goto :goto_0

    .line 1047579
    :cond_4
    iget-object v0, p0, LX/65W;->b:LX/657;

    .line 1047580
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1047581
    if-nez v0, :cond_6

    .line 1047582
    iget-object v0, p0, LX/65W;->d:LX/65V;

    invoke-virtual {v0}, LX/65V;->b()LX/657;

    move-result-object v0

    .line 1047583
    iget-object v1, p0, LX/65W;->c:LX/64c;

    monitor-enter v1

    .line 1047584
    :try_start_2
    iput-object v0, p0, LX/65W;->b:LX/657;

    .line 1047585
    const/4 v2, 0x0

    iput v2, p0, LX/65W;->e:I

    .line 1047586
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v1, v0

    .line 1047587
    :goto_1
    new-instance v0, LX/65S;

    invoke-direct {v0, v1}, LX/65S;-><init>(LX/657;)V

    .line 1047588
    invoke-virtual {p0, v0}, LX/65W;->a(LX/65S;)V

    .line 1047589
    iget-object v1, p0, LX/65W;->c:LX/64c;

    monitor-enter v1

    .line 1047590
    :try_start_3
    sget-object v2, LX/64t;->a:LX/64t;

    iget-object v3, p0, LX/65W;->c:LX/64c;

    invoke-virtual {v2, v3, v0}, LX/64t;->b(LX/64c;LX/65S;)V

    .line 1047591
    iput-object v0, p0, LX/65W;->f:LX/65S;

    .line 1047592
    iget-boolean v2, p0, LX/65W;->h:Z

    if-eqz v2, :cond_5

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Canceled"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047593
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 1047594
    :catchall_2
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    .line 1047595
    :cond_5
    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1047596
    iget-object v1, p0, LX/65W;->a:LX/64R;

    .line 1047597
    iget-object v2, v1, LX/64R;->f:Ljava/util/List;

    move-object v4, v2

    .line 1047598
    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/65S;->a(IIILjava/util/List;Z)V

    .line 1047599
    invoke-direct {p0}, LX/65W;->g()LX/65T;

    move-result-object v1

    .line 1047600
    iget-object v2, v0, LX/65S;->k:LX/657;

    move-object v2, v2

    .line 1047601
    invoke-virtual {v1, v2}, LX/65T;->b(LX/657;)V

    goto :goto_0

    :cond_6
    move-object v1, v0

    goto :goto_1
.end method

.method private static a(LX/65W;IIIZZ)LX/65S;
    .locals 3

    .prologue
    .line 1047557
    :goto_0
    invoke-static {p0, p1, p2, p3, p4}, LX/65W;->a(LX/65W;IIIZ)LX/65S;

    move-result-object v0

    .line 1047558
    iget-object v1, p0, LX/65W;->c:LX/64c;

    monitor-enter v1

    .line 1047559
    :try_start_0
    iget v2, v0, LX/65S;->c:I

    if-nez v2, :cond_1

    .line 1047560
    monitor-exit v1

    .line 1047561
    :cond_0
    return-object v0

    .line 1047562
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047563
    invoke-virtual {v0, p5}, LX/65S;->a(Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1047564
    invoke-virtual {p0}, LX/65W;->d()V

    goto :goto_0

    .line 1047565
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(ZZZ)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1047536
    iget-object v1, p0, LX/65W;->c:LX/64c;

    monitor-enter v1

    .line 1047537
    if-eqz p3, :cond_0

    .line 1047538
    const/4 v2, 0x0

    :try_start_0
    iput-object v2, p0, LX/65W;->i:LX/66G;

    .line 1047539
    :cond_0
    if-eqz p2, :cond_1

    .line 1047540
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/65W;->g:Z

    .line 1047541
    :cond_1
    iget-object v2, p0, LX/65W;->f:LX/65S;

    if-eqz v2, :cond_5

    .line 1047542
    if-eqz p1, :cond_2

    .line 1047543
    iget-object v2, p0, LX/65W;->f:LX/65S;

    const/4 v3, 0x1

    iput-boolean v3, v2, LX/65S;->h:Z

    .line 1047544
    :cond_2
    iget-object v2, p0, LX/65W;->i:LX/66G;

    if-nez v2, :cond_5

    iget-boolean v2, p0, LX/65W;->g:Z

    if-nez v2, :cond_3

    iget-object v2, p0, LX/65W;->f:LX/65S;

    iget-boolean v2, v2, LX/65S;->h:Z

    if-eqz v2, :cond_5

    .line 1047545
    :cond_3
    iget-object v2, p0, LX/65W;->f:LX/65S;

    invoke-direct {p0, v2}, LX/65W;->b(LX/65S;)V

    .line 1047546
    iget-object v2, p0, LX/65W;->f:LX/65S;

    iget-object v2, v2, LX/65S;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1047547
    iget-object v2, p0, LX/65W;->f:LX/65S;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    iput-wide v4, v2, LX/65S;->i:J

    .line 1047548
    sget-object v2, LX/64t;->a:LX/64t;

    iget-object v3, p0, LX/65W;->c:LX/64c;

    iget-object v4, p0, LX/65W;->f:LX/65S;

    invoke-virtual {v2, v3, v4}, LX/64t;->a(LX/64c;LX/65S;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1047549
    iget-object v0, p0, LX/65W;->f:LX/65S;

    .line 1047550
    :cond_4
    const/4 v2, 0x0

    iput-object v2, p0, LX/65W;->f:LX/65S;

    .line 1047551
    :cond_5
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047552
    if-eqz v0, :cond_6

    .line 1047553
    iget-object v1, v0, LX/65S;->a:Ljava/net/Socket;

    move-object v0, v1

    .line 1047554
    invoke-static {v0}, LX/65A;->a(Ljava/net/Socket;)V

    .line 1047555
    :cond_6
    return-void

    .line 1047556
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private b(LX/65S;)V
    .locals 3

    .prologue
    .line 1047529
    const/4 v0, 0x0

    iget-object v1, p1, LX/65S;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1047530
    iget-object v0, p1, LX/65S;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/Reference;

    .line 1047531
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 1047532
    iget-object v0, p1, LX/65S;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1047533
    return-void

    .line 1047534
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1047535
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private g()LX/65T;
    .locals 2

    .prologue
    .line 1047528
    sget-object v0, LX/64t;->a:LX/64t;

    iget-object v1, p0, LX/65W;->c:LX/64c;

    invoke-virtual {v0, v1}, LX/64t;->a(LX/64c;)LX/65T;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/66G;
    .locals 2

    .prologue
    .line 1047525
    iget-object v1, p0, LX/65W;->c:LX/64c;

    monitor-enter v1

    .line 1047526
    :try_start_0
    iget-object v0, p0, LX/65W;->i:LX/66G;

    monitor-exit v1

    return-object v0

    .line 1047527
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/64w;Z)LX/66G;
    .locals 6

    .prologue
    .line 1047607
    iget v0, p1, LX/64w;->w:I

    move v1, v0

    .line 1047608
    iget v0, p1, LX/64w;->x:I

    move v2, v0

    .line 1047609
    iget v0, p1, LX/64w;->y:I

    move v3, v0

    .line 1047610
    iget-boolean v0, p1, LX/64w;->v:Z

    move v4, v0

    .line 1047611
    move-object v0, p0

    move v5, p2

    .line 1047612
    :try_start_0
    invoke-static/range {v0 .. v5}, LX/65W;->a(LX/65W;IIIZZ)LX/65S;

    move-result-object v1

    .line 1047613
    iget-object v0, v1, LX/65S;->b:LX/65c;

    if-eqz v0, :cond_0

    .line 1047614
    new-instance v0, LX/66J;

    iget-object v1, v1, LX/65S;->b:LX/65c;

    invoke-direct {v0, p1, p0, v1}, LX/66J;-><init>(LX/64w;LX/65W;LX/65c;)V

    .line 1047615
    :goto_0
    iget-object v1, p0, LX/65W;->c:LX/64c;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1047616
    :try_start_1
    iput-object v0, p0, LX/65W;->i:LX/66G;

    .line 1047617
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    .line 1047618
    :cond_0
    :try_start_2
    iget-object v0, v1, LX/65S;->a:Ljava/net/Socket;

    move-object v0, v0

    .line 1047619
    invoke-virtual {v0, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 1047620
    iget-object v0, v1, LX/65S;->d:LX/671;

    invoke-interface {v0}, LX/65D;->a()LX/65f;

    move-result-object v0

    int-to-long v4, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v2}, LX/65f;->a(JLjava/util/concurrent/TimeUnit;)LX/65f;

    .line 1047621
    iget-object v0, v1, LX/65S;->e:LX/670;

    invoke-interface {v0}, LX/65J;->a()LX/65f;

    move-result-object v0

    int-to-long v2, v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, LX/65f;->a(JLjava/util/concurrent/TimeUnit;)LX/65f;

    .line 1047622
    new-instance v0, LX/66H;

    iget-object v2, v1, LX/65S;->d:LX/671;

    iget-object v1, v1, LX/65S;->e:LX/670;

    invoke-direct {v0, p1, p0, v2, v1}, LX/66H;-><init>(LX/64w;LX/65W;LX/671;LX/670;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1047623
    :catch_0
    move-exception v0

    .line 1047624
    new-instance v1, LX/65U;

    invoke-direct {v1, v0}, LX/65U;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 1047625
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method public final a(LX/65S;)V
    .locals 2

    .prologue
    .line 1047523
    iget-object v0, p1, LX/65S;->g:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1047524
    return-void
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1047492
    iget-object v3, p0, LX/65W;->c:LX/64c;

    monitor-enter v3

    .line 1047493
    :try_start_0
    instance-of v0, p1, LX/667;

    if-eqz v0, :cond_3

    .line 1047494
    check-cast p1, LX/667;

    .line 1047495
    iget-object v0, p1, LX/667;->errorCode:LX/65X;

    sget-object v4, LX/65X;->REFUSED_STREAM:LX/65X;

    if-ne v0, v4, :cond_0

    .line 1047496
    iget v0, p0, LX/65W;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/65W;->e:I

    .line 1047497
    :cond_0
    iget-object v0, p1, LX/667;->errorCode:LX/65X;

    sget-object v4, LX/65X;->REFUSED_STREAM:LX/65X;

    if-ne v0, v4, :cond_1

    iget v0, p0, LX/65W;->e:I

    if-le v0, v2, :cond_6

    .line 1047498
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/65W;->b:LX/657;

    :cond_2
    :goto_0
    move v0, v2

    .line 1047499
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047500
    invoke-direct {p0, v0, v1, v2}, LX/65W;->a(ZZZ)V

    .line 1047501
    return-void

    .line 1047502
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/65W;->f:LX/65S;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/65W;->f:LX/65S;

    .line 1047503
    iget-object v4, v0, LX/65S;->b:LX/65c;

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    :goto_2
    move v0, v4

    .line 1047504
    if-nez v0, :cond_6

    .line 1047505
    iget-object v0, p0, LX/65W;->f:LX/65S;

    iget v0, v0, LX/65S;->c:I

    if-nez v0, :cond_2

    .line 1047506
    iget-object v0, p0, LX/65W;->b:LX/657;

    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    .line 1047507
    iget-object v0, p0, LX/65W;->d:LX/65V;

    iget-object v4, p0, LX/65W;->b:LX/657;

    .line 1047508
    iget-object v5, v4, LX/657;->b:Ljava/net/Proxy;

    move-object v5, v5

    .line 1047509
    invoke-virtual {v5}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v5

    sget-object v6, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v5, v6, :cond_4

    iget-object v5, v0, LX/65V;->a:LX/64R;

    .line 1047510
    iget-object v6, v5, LX/64R;->g:Ljava/net/ProxySelector;

    move-object v5, v6

    .line 1047511
    if-eqz v5, :cond_4

    .line 1047512
    iget-object v5, v0, LX/65V;->a:LX/64R;

    .line 1047513
    iget-object v6, v5, LX/64R;->g:Ljava/net/ProxySelector;

    move-object v5, v6

    .line 1047514
    iget-object v6, v0, LX/65V;->a:LX/64R;

    .line 1047515
    iget-object v7, v6, LX/64R;->a:LX/64q;

    move-object v6, v7

    .line 1047516
    invoke-virtual {v6}, LX/64q;->a()Ljava/net/URI;

    move-result-object v6

    .line 1047517
    iget-object v7, v4, LX/657;->b:Ljava/net/Proxy;

    move-object v7, v7

    .line 1047518
    invoke-virtual {v7}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v7

    .line 1047519
    invoke-virtual {v5, v6, v7, p1}, Ljava/net/ProxySelector;->connectFailed(Ljava/net/URI;Ljava/net/SocketAddress;Ljava/io/IOException;)V

    .line 1047520
    :cond_4
    iget-object v5, v0, LX/65V;->b:LX/65T;

    invoke-virtual {v5, v4}, LX/65T;->a(LX/657;)V

    .line 1047521
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, LX/65W;->b:LX/657;

    goto :goto_0

    .line 1047522
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final a(ZLX/66G;)V
    .locals 4

    .prologue
    .line 1047483
    iget-object v1, p0, LX/65W;->c:LX/64c;

    monitor-enter v1

    .line 1047484
    if-eqz p2, :cond_0

    :try_start_0
    iget-object v0, p0, LX/65W;->i:LX/66G;

    if-eq p2, v0, :cond_1

    .line 1047485
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/65W;->i:LX/66G;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047486
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1047487
    :cond_1
    if-nez p1, :cond_2

    .line 1047488
    :try_start_1
    iget-object v0, p0, LX/65W;->f:LX/65S;

    iget v2, v0, LX/65S;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/65S;->c:I

    .line 1047489
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1047490
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, LX/65W;->a(ZZZ)V

    .line 1047491
    return-void
.end method

.method public final declared-synchronized b()LX/65S;
    .locals 1

    .prologue
    .line 1047482
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65W;->f:LX/65S;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1047480
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0, v1}, LX/65W;->a(ZZZ)V

    .line 1047481
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1047478
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1}, LX/65W;->a(ZZZ)V

    .line 1047479
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1047466
    iget-object v1, p0, LX/65W;->c:LX/64c;

    monitor-enter v1

    .line 1047467
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/65W;->h:Z

    .line 1047468
    iget-object v0, p0, LX/65W;->i:LX/66G;

    .line 1047469
    iget-object v2, p0, LX/65W;->f:LX/65S;

    .line 1047470
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047471
    if-eqz v0, :cond_1

    .line 1047472
    invoke-interface {v0}, LX/66G;->a()V

    .line 1047473
    :cond_0
    :goto_0
    return-void

    .line 1047474
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1047475
    :cond_1
    if-eqz v2, :cond_0

    .line 1047476
    invoke-virtual {v2}, LX/65S;->b()V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1047477
    iget-object v0, p0, LX/65W;->a:LX/64R;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
