.class public final LX/69A;
.super LX/67m;
.source ""


# static fields
.field private static final o:Landroid/graphics/Paint;


# instance fields
.field public A:F

.field private B:D

.field private p:Z

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;"
        }
    .end annotation
.end field

.field private r:[D

.field private s:[D

.field private t:[LX/31i;

.field public final u:Landroid/graphics/Paint;

.field private final v:LX/31i;

.field private final w:LX/31i;

.field private final x:LX/31i;

.field private final y:Landroid/graphics/RectF;

.field public z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1057836
    const/4 v0, 0x0

    sput-object v0, LX/69A;->o:Landroid/graphics/Paint;

    .line 1057837
    return-void
.end method

.method public constructor <init>(LX/680;LX/69B;)V
    .locals 2

    .prologue
    .line 1057699
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1057700
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/69A;->u:Landroid/graphics/Paint;

    .line 1057701
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/69A;->v:LX/31i;

    .line 1057702
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/69A;->w:LX/31i;

    .line 1057703
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/69A;->x:LX/31i;

    .line 1057704
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/69A;->y:Landroid/graphics/RectF;

    .line 1057705
    iget v0, p2, LX/69B;->f:F

    move v0, v0

    .line 1057706
    iput v0, p0, LX/69A;->k:F

    .line 1057707
    iget-boolean v0, p2, LX/69B;->b:Z

    move v0, v0

    .line 1057708
    iput-boolean v0, p0, LX/69A;->p:Z

    .line 1057709
    iget-boolean v0, p2, LX/69B;->d:Z

    move v0, v0

    .line 1057710
    iput-boolean v0, p0, LX/69A;->i:Z

    .line 1057711
    iget-object v0, p2, LX/69B;->c:Ljava/util/List;

    move-object v0, v0

    .line 1057712
    iput-object v0, p0, LX/69A;->q:Ljava/util/List;

    .line 1057713
    iget-object v0, p0, LX/69A;->u:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 1057714
    iget-object v0, p0, LX/69A;->u:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1057715
    iget v0, p2, LX/69B;->a:I

    move v0, v0

    .line 1057716
    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    iput v1, p0, LX/69A;->z:I

    .line 1057717
    iget-object v1, p0, LX/69A;->u:Landroid/graphics/Paint;

    const/high16 p1, -0x1000000

    or-int/2addr p1, v0

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1057718
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057719
    iget v0, p2, LX/69B;->e:F

    move v0, v0

    .line 1057720
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    iput v1, p0, LX/69A;->A:F

    .line 1057721
    iget-object v1, p0, LX/69A;->u:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1057722
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057723
    invoke-direct {p0}, LX/69A;->c()V

    .line 1057724
    return-void
.end method

.method private c()V
    .locals 14

    .prologue
    const/4 v2, 0x0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 1057797
    iget-object v0, p0, LX/69A;->q:Ljava/util/List;

    invoke-static {v0, v2}, LX/67s;->a(Ljava/util/List;Z)[D

    move-result-object v0

    iput-object v0, p0, LX/69A;->r:[D

    .line 1057798
    iget-boolean v0, p0, LX/69A;->p:Z

    if-nez v0, :cond_5

    .line 1057799
    iget-object v0, p0, LX/69A;->r:[D

    iget-object v1, p0, LX/69A;->w:LX/31i;

    const-wide/high16 v8, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    const-wide/high16 v6, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    .line 1057800
    iput-wide v8, v1, LX/31i;->a:D

    .line 1057801
    iput-wide v6, v1, LX/31i;->b:D

    .line 1057802
    iput-wide v8, v1, LX/31i;->c:D

    .line 1057803
    iput-wide v6, v1, LX/31i;->d:D

    .line 1057804
    const/4 v6, 0x0

    array-length v7, v0

    :goto_0
    if-ge v6, v7, :cond_4

    .line 1057805
    aget-wide v8, v0, v6

    .line 1057806
    add-int/lit8 v10, v6, 0x1

    aget-wide v10, v0, v10

    .line 1057807
    iget-wide v12, v1, LX/31i;->a:D

    cmpg-double v12, v10, v12

    if-gez v12, :cond_0

    .line 1057808
    iput-wide v10, v1, LX/31i;->a:D

    .line 1057809
    :cond_0
    iget-wide v12, v1, LX/31i;->b:D

    cmpg-double v12, v12, v10

    if-gez v12, :cond_1

    .line 1057810
    iput-wide v10, v1, LX/31i;->b:D

    .line 1057811
    :cond_1
    iget-wide v10, v1, LX/31i;->c:D

    cmpg-double v10, v8, v10

    if-gez v10, :cond_2

    .line 1057812
    iput-wide v8, v1, LX/31i;->c:D

    .line 1057813
    :cond_2
    iget-wide v10, v1, LX/31i;->d:D

    cmpg-double v10, v10, v8

    if-gez v10, :cond_3

    .line 1057814
    iput-wide v8, v1, LX/31i;->d:D

    .line 1057815
    :cond_3
    add-int/lit8 v6, v6, 0x2

    goto :goto_0

    .line 1057816
    :cond_4
    iget-object v0, p0, LX/69A;->w:LX/31i;

    iget-wide v0, v0, LX/31i;->c:D

    iget-object v2, p0, LX/69A;->w:LX/31i;

    iget-wide v2, v2, LX/31i;->d:D

    add-double/2addr v0, v2

    div-double/2addr v0, v4

    iput-wide v0, p0, LX/69A;->m:D

    .line 1057817
    iget-object v0, p0, LX/69A;->w:LX/31i;

    iget-wide v0, v0, LX/31i;->a:D

    iget-object v2, p0, LX/69A;->w:LX/31i;

    iget-wide v2, v2, LX/31i;->b:D

    add-double/2addr v0, v2

    div-double/2addr v0, v4

    iput-wide v0, p0, LX/69A;->n:D

    .line 1057818
    return-void

    .line 1057819
    :cond_5
    iget-object v0, p0, LX/69A;->q:Ljava/util/List;

    invoke-static {v0}, LX/67s;->a(Ljava/util/List;)[D

    move-result-object v0

    iput-object v0, p0, LX/69A;->s:[D

    .line 1057820
    iget-object v0, p0, LX/69A;->r:[D

    iget-object v1, p0, LX/69A;->s:[D

    invoke-static {v0, v1}, LX/67s;->a([D[D)[LX/31i;

    move-result-object v0

    iput-object v0, p0, LX/69A;->t:[LX/31i;

    .line 1057821
    iget-object v0, p0, LX/69A;->w:LX/31i;

    iget-object v1, p0, LX/69A;->t:[LX/31i;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, LX/31i;->a(LX/31i;)V

    .line 1057822
    const/4 v0, 0x1

    iget-object v1, p0, LX/69A;->t:[LX/31i;

    array-length v1, v1

    :goto_1
    if-ge v0, v1, :cond_4

    .line 1057823
    iget-object v2, p0, LX/69A;->w:LX/31i;

    iget-object v3, p0, LX/69A;->t:[LX/31i;

    aget-object v3, v3, v0

    .line 1057824
    iget-wide v6, v3, LX/31i;->c:D

    iget-wide v8, v3, LX/31i;->d:D

    cmpl-double v6, v6, v8

    if-gez v6, :cond_6

    iget-wide v6, v3, LX/31i;->a:D

    iget-wide v8, v3, LX/31i;->b:D

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_7

    .line 1057825
    :cond_6
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1057826
    :cond_7
    iget-wide v6, v2, LX/31i;->c:D

    iget-wide v8, v2, LX/31i;->d:D

    cmpl-double v6, v6, v8

    if-gez v6, :cond_8

    iget-wide v6, v2, LX/31i;->a:D

    iget-wide v8, v2, LX/31i;->b:D

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_9

    .line 1057827
    :cond_8
    invoke-virtual {v2, v3}, LX/31i;->a(LX/31i;)V

    goto :goto_2

    .line 1057828
    :cond_9
    iget-wide v6, v2, LX/31i;->c:D

    iget-wide v8, v3, LX/31i;->c:D

    cmpl-double v6, v6, v8

    if-lez v6, :cond_a

    .line 1057829
    iget-wide v6, v3, LX/31i;->c:D

    iput-wide v6, v2, LX/31i;->c:D

    .line 1057830
    :cond_a
    iget-wide v6, v2, LX/31i;->a:D

    iget-wide v8, v3, LX/31i;->a:D

    cmpl-double v6, v6, v8

    if-lez v6, :cond_b

    .line 1057831
    iget-wide v6, v3, LX/31i;->a:D

    iput-wide v6, v2, LX/31i;->a:D

    .line 1057832
    :cond_b
    iget-wide v6, v2, LX/31i;->d:D

    iget-wide v8, v3, LX/31i;->d:D

    cmpg-double v6, v6, v8

    if-gez v6, :cond_c

    .line 1057833
    iget-wide v6, v3, LX/31i;->d:D

    iput-wide v6, v2, LX/31i;->d:D

    .line 1057834
    :cond_c
    iget-wide v6, v2, LX/31i;->b:D

    iget-wide v8, v3, LX/31i;->b:D

    cmpg-double v6, v6, v8

    if-gez v6, :cond_6

    .line 1057835
    iget-wide v6, v3, LX/31i;->b:D

    iput-wide v6, v2, LX/31i;->b:D

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 24

    .prologue
    .line 1057727
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->r:[D

    array-length v2, v2

    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    .line 1057728
    :cond_0
    :goto_0
    return-void

    .line 1057729
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->w:LX/31i;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->c:[F

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/67m;->a(LX/31i;[F)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1057730
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->c:[F

    const/4 v3, 0x0

    aget v9, v2, v3

    .line 1057731
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->c:[F

    const/4 v3, 0x1

    aget v22, v2, v3

    .line 1057732
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->f:LX/31h;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->v:LX/31i;

    invoke-virtual {v2, v3}, LX/31h;->a(LX/31i;)V

    .line 1057733
    move-object/from16 v0, p0

    iget v2, v0, LX/69A;->z:I

    const/16 v3, 0xff

    if-eq v2, v3, :cond_3

    const/4 v2, 0x1

    move v11, v2

    .line 1057734
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->r:[D

    array-length v2, v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/69A;->p:Z

    if-nez v2, :cond_4

    const/4 v2, 0x1

    move/from16 v21, v2

    .line 1057735
    :goto_2
    if-eqz v11, :cond_2

    if-nez v21, :cond_2

    .line 1057736
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->u:Landroid/graphics/Paint;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1057737
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->w:LX/31i;

    iget-wide v4, v3, LX/31i;->a:D

    iput-wide v4, v2, LX/31i;->a:D

    .line 1057738
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->w:LX/31i;

    iget-wide v4, v3, LX/31i;->b:D

    iput-wide v4, v2, LX/31i;->b:D

    .line 1057739
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->w:LX/31i;

    iget-wide v4, v3, LX/31i;->c:D

    float-to-double v6, v9

    add-double/2addr v4, v6

    iput-wide v4, v2, LX/31i;->c:D

    .line 1057740
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->w:LX/31i;

    iget-wide v4, v3, LX/31i;->d:D

    move/from16 v0, v22

    float-to-double v6, v0

    add-double/2addr v4, v6

    iput-wide v4, v2, LX/31i;->d:D

    .line 1057741
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->v:LX/31i;

    invoke-virtual {v2, v3}, LX/31i;->c(LX/31i;)Z

    .line 1057742
    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->f:LX/31h;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v4, v2, LX/31i;->c:D

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v6, v2, LX/31i;->a:D

    move-object/from16 v0, p0

    iget-object v8, v0, LX/67m;->c:[F

    invoke-virtual/range {v3 .. v8}, LX/31h;->a(DD[F)V

    .line 1057743
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->y:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->c:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/67m;->c:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, LX/67m;->c:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, LX/67m;->c:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1057744
    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->f:LX/31h;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v4, v2, LX/31i;->c:D

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v6, v2, LX/31i;->b:D

    move-object/from16 v0, p0

    iget-object v8, v0, LX/67m;->c:[F

    invoke-virtual/range {v3 .. v8}, LX/31h;->a(DD[F)V

    .line 1057745
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->y:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->c:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/67m;->c:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->union(FF)V

    .line 1057746
    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->f:LX/31h;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v4, v2, LX/31i;->d:D

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v6, v2, LX/31i;->a:D

    move-object/from16 v0, p0

    iget-object v8, v0, LX/67m;->c:[F

    invoke-virtual/range {v3 .. v8}, LX/31h;->a(DD[F)V

    .line 1057747
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->y:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->c:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/67m;->c:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->union(FF)V

    .line 1057748
    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->f:LX/31h;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v4, v2, LX/31i;->d:D

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v6, v2, LX/31i;->b:D

    move-object/from16 v0, p0

    iget-object v8, v0, LX/67m;->c:[F

    invoke-virtual/range {v3 .. v8}, LX/31h;->a(DD[F)V

    .line 1057749
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->y:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->c:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/67m;->c:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->union(FF)V

    .line 1057750
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->y:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->y:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget v4, v0, LX/69A;->A:F

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 1057751
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->y:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->y:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget v4, v0, LX/69A;->A:F

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 1057752
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->y:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->y:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget v4, v0, LX/69A;->A:F

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 1057753
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->y:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->y:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iget v4, v0, LX/69A;->A:F

    add-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    .line 1057754
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->y:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v3, v0, LX/69A;->z:I

    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->saveLayerAlpha(Landroid/graphics/RectF;II)I

    .line 1057755
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->u:Landroid/graphics/Paint;

    if-eqz v21, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, LX/69A;->z:I

    :goto_3
    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    move/from16 v20, v9

    .line 1057756
    :goto_4
    cmpg-float v2, v20, v22

    if-gtz v2, :cond_e

    .line 1057757
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/69A;->p:Z

    if-nez v2, :cond_6

    .line 1057758
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->r:[D

    array-length v10, v3

    move v9, v2

    :goto_5
    if-ge v9, v10, :cond_d

    .line 1057759
    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->f:LX/31h;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->r:[D

    aget-wide v4, v2, v9

    move/from16 v0, v20

    float-to-double v6, v0

    add-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->r:[D

    add-int/lit8 v6, v9, 0x1

    aget-wide v6, v2, v6

    move-object/from16 v0, p0

    iget-object v8, v0, LX/67m;->c:[F

    invoke-virtual/range {v3 .. v8}, LX/31h;->a(DD[F)V

    .line 1057760
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->c:[F

    const/4 v3, 0x0

    aget v12, v2, v3

    .line 1057761
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->c:[F

    const/4 v3, 0x1

    aget v13, v2, v3

    .line 1057762
    move-object/from16 v0, p0

    iget-object v3, v0, LX/67m;->f:LX/31h;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->r:[D

    add-int/lit8 v4, v9, 0x2

    aget-wide v4, v2, v4

    move/from16 v0, v20

    float-to-double v6, v0

    add-double/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->r:[D

    add-int/lit8 v6, v9, 0x3

    aget-wide v6, v2, v6

    move-object/from16 v0, p0

    iget-object v8, v0, LX/67m;->c:[F

    invoke-virtual/range {v3 .. v8}, LX/31h;->a(DD[F)V

    .line 1057763
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->c:[F

    const/4 v3, 0x0

    aget v5, v2, v3

    .line 1057764
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->c:[F

    const/4 v3, 0x1

    aget v6, v2, v3

    .line 1057765
    move-object/from16 v0, p0

    iget-object v7, v0, LX/69A;->u:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v3, v12

    move v4, v13

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1057766
    add-int/lit8 v2, v9, 0x4

    move v9, v2

    goto :goto_5

    .line 1057767
    :cond_3
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_1

    .line 1057768
    :cond_4
    const/4 v2, 0x0

    move/from16 v21, v2

    goto/16 :goto_2

    .line 1057769
    :cond_5
    const/16 v2, 0xff

    goto/16 :goto_3

    .line 1057770
    :cond_6
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, LX/69A;->t:[LX/31i;

    array-length v0, v5

    move/from16 v23, v0

    move/from16 v17, v2

    move/from16 v18, v3

    move/from16 v19, v4

    .line 1057771
    :goto_6
    move/from16 v0, v19

    move/from16 v1, v23

    if-ge v0, v1, :cond_d

    .line 1057772
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->t:[LX/31i;

    aget-object v3, v3, v19

    invoke-virtual {v2, v3}, LX/31i;->a(LX/31i;)V

    .line 1057773
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v4, v2, LX/31i;->c:D

    move/from16 v0, v20

    float-to-double v6, v0

    add-double/2addr v4, v6

    iput-wide v4, v2, LX/31i;->c:D

    .line 1057774
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v4, v2, LX/31i;->d:D

    move/from16 v0, v20

    float-to-double v6, v0

    add-double/2addr v4, v6

    iput-wide v4, v2, LX/31i;->d:D

    .line 1057775
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/69A;->v:LX/31i;

    invoke-virtual {v2, v3}, LX/31i;->c(LX/31i;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1057776
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->r:[D

    aget-wide v2, v2, v18

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->r:[D

    add-int/lit8 v3, v18, 0x3

    aget-wide v2, v2, v3

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_a

    :cond_7
    const/4 v2, 0x1

    move v12, v2

    .line 1057777
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, LX/69A;->x:LX/31i;

    iget-wide v2, v2, LX/31i;->c:D

    .line 1057778
    move-object/from16 v0, p0

    iget-object v4, v0, LX/69A;->x:LX/31i;

    iget-wide v14, v4, LX/31i;->d:D

    .line 1057779
    move-object/from16 v0, p0

    iget-object v4, v0, LX/69A;->s:[D

    aget-wide v4, v4, v17

    move-object/from16 v0, p0

    iget-object v6, v0, LX/69A;->s:[D

    add-int/lit8 v7, v17, 0x1

    aget-wide v6, v6, v7

    invoke-static/range {v2 .. v7}, LX/67s;->a(DDD)D

    move-result-wide v8

    .line 1057780
    move-object/from16 v0, p0

    iget-object v5, v0, LX/67m;->f:LX/31h;

    if-eqz v12, :cond_8

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v8, v6, v8

    :cond_8
    move-object/from16 v0, p0

    iget-object v10, v0, LX/67m;->c:[F

    move-wide v6, v2

    invoke-virtual/range {v5 .. v10}, LX/31h;->a(DD[F)V

    .line 1057781
    move-object/from16 v0, p0

    iget-object v4, v0, LX/67m;->c:[F

    const/4 v5, 0x0

    aget v7, v4, v5

    .line 1057782
    move-object/from16 v0, p0

    iget-object v4, v0, LX/67m;->c:[F

    const/4 v5, 0x1

    aget v8, v4, v5

    move v13, v8

    move/from16 v16, v7

    .line 1057783
    :goto_8
    cmpg-double v4, v2, v14

    if-gez v4, :cond_c

    .line 1057784
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/69A;->B:D

    add-double/2addr v4, v2

    cmpl-double v4, v4, v14

    if-lez v4, :cond_b

    move-wide v2, v14

    .line 1057785
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, LX/69A;->s:[D

    aget-wide v4, v4, v17

    move-object/from16 v0, p0

    iget-object v6, v0, LX/69A;->s:[D

    add-int/lit8 v7, v17, 0x1

    aget-wide v6, v6, v7

    invoke-static/range {v2 .. v7}, LX/67s;->a(DDD)D

    move-result-wide v8

    .line 1057786
    move-object/from16 v0, p0

    iget-object v5, v0, LX/67m;->f:LX/31h;

    if-eqz v12, :cond_9

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double v8, v6, v8

    :cond_9
    move-object/from16 v0, p0

    iget-object v10, v0, LX/67m;->c:[F

    move-wide v6, v2

    invoke-virtual/range {v5 .. v10}, LX/31h;->a(DD[F)V

    .line 1057787
    move-object/from16 v0, p0

    iget-object v4, v0, LX/67m;->c:[F

    const/4 v5, 0x0

    aget v7, v4, v5

    .line 1057788
    move-object/from16 v0, p0

    iget-object v4, v0, LX/67m;->c:[F

    const/4 v5, 0x1

    aget v8, v4, v5

    .line 1057789
    move-object/from16 v0, p0

    iget-object v9, v0, LX/69A;->u:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    move/from16 v5, v16

    move v6, v13

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move v13, v8

    move/from16 v16, v7

    .line 1057790
    goto :goto_8

    .line 1057791
    :cond_a
    const/4 v2, 0x0

    move v12, v2

    goto/16 :goto_7

    .line 1057792
    :cond_b
    move-object/from16 v0, p0

    iget-wide v4, v0, LX/69A;->B:D

    add-double/2addr v2, v4

    goto :goto_9

    .line 1057793
    :cond_c
    add-int/lit8 v4, v19, 0x1

    add-int/lit8 v3, v18, 0x4

    add-int/lit8 v2, v17, 0x2

    move/from16 v17, v2

    move/from16 v18, v3

    move/from16 v19, v4

    goto/16 :goto_6

    .line 1057794
    :cond_d
    const/high16 v2, 0x3f800000    # 1.0f

    add-float v2, v2, v20

    move/from16 v20, v2

    goto/16 :goto_4

    .line 1057795
    :cond_e
    if-eqz v11, :cond_0

    if-nez v21, :cond_0

    .line 1057796
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1057725
    iget-object v0, p0, LX/67m;->f:LX/31h;

    iget v1, p0, LX/67m;->d:F

    const/high16 v2, 0x41200000    # 10.0f

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, LX/31h;->a(F)D

    move-result-wide v0

    iput-wide v0, p0, LX/69A;->B:D

    .line 1057726
    return-void
.end method
