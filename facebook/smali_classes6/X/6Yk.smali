.class public LX/6Yk;
.super LX/6Ya;
.source ""


# instance fields
.field public final c:LX/7Y7;

.field private final d:LX/4g9;

.field private final e:LX/0yH;

.field public final f:LX/7YP;

.field public final g:LX/0Or;
    .annotation runtime Lcom/facebook/iorg/common/upsell/annotations/IsInZeroUpsellShowUseDataOrStayFreeScreenGateKeeper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7Y7;LX/4g9;LX/0yH;LX/7YP;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/upsell/annotations/IsInZeroUpsellShowUseDataOrStayFreeScreenGateKeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/iorg/common/zero/interfaces/IorgAndroidThreadUtil;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroAnalyticsLogger;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "Lcom/facebook/iorg/common/upsell/server/UpsellApiRequestManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110480
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1110481
    iput-object p1, p0, LX/6Yk;->c:LX/7Y7;

    .line 1110482
    iput-object p2, p0, LX/6Yk;->d:LX/4g9;

    .line 1110483
    iput-object p3, p0, LX/6Yk;->e:LX/0yH;

    .line 1110484
    iput-object p4, p0, LX/6Yk;->f:LX/7YP;

    .line 1110485
    iput-object p5, p0, LX/6Yk;->g:LX/0Or;

    .line 1110486
    return-void
.end method

.method public static h(LX/6Yk;)V
    .locals 9

    .prologue
    .line 1110487
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    if-nez v0, :cond_0

    .line 1110488
    :goto_0
    return-void

    .line 1110489
    :cond_0
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/4g0;->UPSELL_FAILURE:LX/4g0;

    .line 1110490
    iget-object v8, v0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->m:LX/12M;

    new-instance v2, LX/4g4;

    iget-object v3, v0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    sget-object v4, LX/4g3;->FAILURE:LX/4g3;

    iget-object v5, v0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->r:Ljava/lang/Object;

    iget-object v6, v0, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->q:LX/4g1;

    move-object v7, v1

    invoke-direct/range {v2 .. v7}, LX/4g4;-><init>(LX/0yY;LX/4g3;Ljava/lang/Object;LX/4g1;LX/4g0;)V

    invoke-virtual {v8, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1110491
    invoke-virtual {v0}, Lcom/facebook/iorg/common/zero/ui/IorgDialogFragment;->i()V

    .line 1110492
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 11

    .prologue
    .line 1110493
    iget-object v0, p0, LX/6Yk;->d:LX/4g9;

    sget-object v1, LX/4g6;->h:LX/4g6;

    invoke-virtual {p0}, LX/6Ya;->e()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4g9;->a(LX/4g5;Ljava/util/Map;)V

    .line 1110494
    new-instance v0, LX/6YP;

    invoke-direct {v0, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1110495
    invoke-virtual {v0}, LX/6YP;->a()V

    .line 1110496
    iget-object v1, p0, LX/6Yk;->e:LX/0yH;

    sget-object v2, LX/0yY;->NATIVE_UPSELL_INTERSTITIAL:LX/0yY;

    invoke-virtual {v1, v2}, LX/0yH;->a(LX/0yY;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1110497
    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v2, LX/6YN;->STANDARD_DATA_CHARGES_APPLY:LX/6YN;

    invoke-virtual {v1, v2}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    .line 1110498
    :goto_0
    return-object v0

    .line 1110499
    :cond_0
    iget-object v9, p0, LX/6Yk;->f:LX/7YP;

    new-instance v3, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;

    const/4 v4, 0x2

    iget-object v5, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, LX/0tP;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, LX/6Y4;->INTERSTITIAL:LX/6Y4;

    iget-object v8, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110500
    iget-object v10, v8, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    move-object v8, v10

    .line 1110501
    invoke-direct/range {v3 .. v8}, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;-><init>(ILjava/lang/String;ZLX/6Y4;LX/0yY;)V

    invoke-virtual {v9, v3}, LX/7YP;->a(Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1110502
    iget-object v4, p0, LX/6Yk;->c:LX/7Y7;

    new-instance v5, LX/6Yj;

    invoke-direct {v5, p0}, LX/6Yj;-><init>(LX/6Yk;)V

    invoke-virtual {v4, v3, v5}, LX/7Y7;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1110503
    goto :goto_0
.end method
