.class public LX/6Vl;
.super LX/1RF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1PW;",
        ">",
        "LX/1RF",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1PW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final b:Ljava/io/PrintWriter;

.field private c:I


# direct methods
.method public constructor <init>(Ljava/io/PrintWriter;LX/1PW;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/PrintWriter;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 1104397
    invoke-direct {p0}, LX/1RF;-><init>()V

    .line 1104398
    const/4 v0, 0x0

    iput v0, p0, LX/6Vl;->c:I

    .line 1104399
    iput-object p1, p0, LX/6Vl;->b:Ljava/io/PrintWriter;

    .line 1104400
    iput-object p2, p0, LX/6Vl;->a:LX/1PW;

    .line 1104401
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1104402
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/6Vl;->c:I

    if-ge v0, v1, :cond_0

    .line 1104403
    iget-object v1, p0, LX/6Vl;->b:Ljava/io/PrintWriter;

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1104404
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1104405
    :cond_0
    iget-object v0, p0, LX/6Vl;->b:Ljava/io/PrintWriter;

    invoke-virtual {v0, p1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1104406
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/SinglePartDefinitionWithViewTypeAndIsNeeded",
            "<TP;*-TE;*>;TP;)Z"
        }
    .end annotation

    .prologue
    .line 1104407
    invoke-interface {p1, p2}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1104408
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "N "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/6Vl;->a(Ljava/lang/String;)V

    .line 1104409
    const/4 v0, 0x0

    .line 1104410
    :goto_0
    return v0

    .line 1104411
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Y "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/6Vl;->a(Ljava/lang/String;)V

    .line 1104412
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<TP;*-TE;>;TP;)Z"
        }
    .end annotation

    .prologue
    .line 1104413
    invoke-interface {p1, p2}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1104414
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "N "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/6Vl;->a(Ljava/lang/String;)V

    .line 1104415
    const/4 v0, 0x0

    .line 1104416
    :goto_0
    return v0

    .line 1104417
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "+ "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/6Vl;->a(Ljava/lang/String;)V

    .line 1104418
    iget v0, p0, LX/6Vl;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/6Vl;->c:I

    .line 1104419
    iget-object v0, p0, LX/6Vl;->a:LX/1PW;

    invoke-interface {p1, p0, p2, v0}, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;->a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;

    .line 1104420
    iget v0, p0, LX/6Vl;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/6Vl;->c:I

    .line 1104421
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<TP;-TE;>;TP;)Z"
        }
    .end annotation

    .prologue
    .line 1104422
    instance-of v0, p1, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    if-eqz v0, :cond_0

    .line 1104423
    check-cast p1, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    invoke-virtual {p0, p1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)Z

    move-result v0

    .line 1104424
    :goto_0
    return v0

    .line 1104425
    :cond_0
    instance-of v0, p1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v0, :cond_1

    .line 1104426
    check-cast p1, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {p0, p1, p2}, LX/1RF;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1104427
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
