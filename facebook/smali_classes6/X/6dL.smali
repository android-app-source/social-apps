.class public final LX/6dL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:LX/6cv;

.field public final c:Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lcom/facebook/user/model/UserKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:J


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadParticipant;LX/6cv;)V
    .locals 2

    .prologue
    .line 1115781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1115782
    iput-object p1, p0, LX/6dL;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1115783
    iput-object p2, p0, LX/6dL;->c:Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 1115784
    iput-object p3, p0, LX/6dL;->b:LX/6cv;

    .line 1115785
    const/4 v0, 0x0

    iput-object v0, p0, LX/6dL;->d:Lcom/facebook/user/model/UserKey;

    .line 1115786
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6dL;->e:J

    .line 1115787
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;J)V
    .locals 1

    .prologue
    .line 1115788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1115789
    iput-object p1, p0, LX/6dL;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1115790
    iput-object p2, p0, LX/6dL;->d:Lcom/facebook/user/model/UserKey;

    .line 1115791
    sget-object v0, LX/6cv;->REQUEST:LX/6cv;

    iput-object v0, p0, LX/6dL;->b:LX/6cv;

    .line 1115792
    const/4 v0, 0x0

    iput-object v0, p0, LX/6dL;->c:Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 1115793
    iput-wide p3, p0, LX/6dL;->e:J

    .line 1115794
    return-void
.end method
