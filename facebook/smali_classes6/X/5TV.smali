.class public LX/5TV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/messaging/business/commerce/model/retail/RetailCarrier;

.field public e:Landroid/net/Uri;

.field public f:J

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

.field public i:Lcom/facebook/messaging/business/commerce/model/retail/RetailAddress;

.field public j:J

.field public k:Ljava/lang/String;

.field public l:J

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Lcom/facebook/messaging/business/attachments/model/LogoImage;

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/attachments/model/PlatformGenericAttachmentItem;",
            ">;"
        }
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/business/commerce/model/retail/ShipmentTrackingEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 921963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 921964
    return-void
.end method


# virtual methods
.method public final d(Ljava/lang/String;)LX/5TV;
    .locals 1

    .prologue
    .line 921965
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/5TV;->e:Landroid/net/Uri;

    .line 921966
    return-object p0

    .line 921967
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Lcom/facebook/messaging/business/commerce/model/retail/Shipment;
    .locals 1

    .prologue
    .line 921968
    new-instance v0, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/business/commerce/model/retail/Shipment;-><init>(LX/5TV;)V

    return-object v0
.end method
