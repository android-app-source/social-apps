.class public final LX/67c;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/168;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/67c;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/168;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1053233
    invoke-direct {p0, p1}, LX/0aT;-><init>(LX/0Ot;)V

    .line 1053234
    return-void
.end method

.method public static a(LX/0QB;)LX/67c;
    .locals 4

    .prologue
    .line 1053235
    sget-object v0, LX/67c;->a:LX/67c;

    if-nez v0, :cond_1

    .line 1053236
    const-class v1, LX/67c;

    monitor-enter v1

    .line 1053237
    :try_start_0
    sget-object v0, LX/67c;->a:LX/67c;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1053238
    if-eqz v2, :cond_0

    .line 1053239
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1053240
    new-instance v3, LX/67c;

    const/16 p0, 0xce

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/67c;-><init>(LX/0Ot;)V

    .line 1053241
    move-object v0, v3

    .line 1053242
    sput-object v0, LX/67c;->a:LX/67c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1053243
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1053244
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1053245
    :cond_1
    sget-object v0, LX/67c;->a:LX/67c;

    return-object v0

    .line 1053246
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1053247
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1053248
    check-cast p3, LX/168;

    .line 1053249
    sget-object v0, LX/1rF;->CLOCK_CHANGE:LX/1rF;

    iget-object v1, p3, LX/168;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {p3, v0, v2, v3}, LX/168;->a$redex0(LX/168;LX/1rF;J)V

    .line 1053250
    return-void
.end method
