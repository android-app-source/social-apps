.class public LX/5KG;
.super LX/1OX;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field public final a:LX/5Je;

.field private final b:LX/1OR;

.field private final c:LX/5KB;

.field private final d:LX/1De;

.field private e:Landroid/view/View;

.field private f:I


# direct methods
.method public constructor <init>(LX/1De;LX/5Je;LX/5KB;)V
    .locals 1

    .prologue
    .line 898474
    invoke-direct {p0}, LX/1OX;-><init>()V

    .line 898475
    const/4 v0, -0x1

    iput v0, p0, LX/5KG;->f:I

    .line 898476
    iput-object p1, p0, LX/5KG;->d:LX/1De;

    .line 898477
    iput-object p2, p0, LX/5KG;->a:LX/5Je;

    .line 898478
    invoke-virtual {p2}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    iput-object v0, p0, LX/5KG;->b:LX/1OR;

    .line 898479
    iput-object p3, p0, LX/5KG;->c:LX/5KB;

    .line 898480
    iget-object v0, p0, LX/5KG;->c:LX/5KB;

    invoke-virtual {v0}, LX/5KB;->d()V

    .line 898481
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 898482
    iget-object v0, p0, LX/5KG;->c:LX/5KB;

    iget-object v1, p0, LX/5KG;->a:LX/5Je;

    invoke-virtual {v1, p1}, LX/3mY;->d(I)LX/1dV;

    move-result-object v1

    .line 898483
    invoke-virtual {v1}, LX/1dV;->getComponentView()Lcom/facebook/components/ComponentView;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 898484
    invoke-virtual {v1}, LX/1dV;->getComponentView()Lcom/facebook/components/ComponentView;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/components/ComponentView;->e()V

    .line 898485
    :cond_0
    iget-object p0, v0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    invoke-virtual {p0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 898486
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x1

    .line 898487
    iget-object v0, p0, LX/5KG;->a:LX/5Je;

    check-cast v0, LX/5Jl;

    invoke-virtual {v0}, LX/5Jl;->a()I

    move-result v2

    .line 898488
    move v3, v2

    :goto_0
    if-ltz v3, :cond_9

    .line 898489
    iget-object v0, p0, LX/5KG;->a:LX/5Je;

    check-cast v0, LX/5Jl;

    invoke-virtual {v0, v3}, LX/5Jl;->a(I)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v3

    .line 898490
    :goto_1
    move v3, v0

    .line 898491
    iget-object v0, p0, LX/5KG;->a:LX/5Je;

    invoke-virtual {v0, v2}, LX/3mY;->d(I)LX/1dV;

    move-result-object v0

    .line 898492
    iget-object v4, p0, LX/5KG;->e:Landroid/view/View;

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    iget-object v4, p0, LX/5KG;->e:Landroid/view/View;

    invoke-virtual {v0}, LX/1dV;->getComponentView()Lcom/facebook/components/ComponentView;

    move-result-object v5

    if-eq v4, v5, :cond_0

    .line 898493
    iget-object v4, p0, LX/5KG;->e:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 898494
    const/4 v4, 0x0

    iput-object v4, p0, LX/5KG;->e:Landroid/view/View;

    .line 898495
    :cond_0
    if-eq v3, v6, :cond_1

    if-nez v0, :cond_2

    .line 898496
    :cond_1
    iget-object v0, p0, LX/5KG;->c:LX/5KB;

    invoke-virtual {v0}, LX/5KB;->d()V

    .line 898497
    iput v6, p0, LX/5KG;->f:I

    .line 898498
    :goto_2
    return-void

    .line 898499
    :cond_2
    invoke-virtual {v0}, LX/1dV;->getComponentView()Lcom/facebook/components/ComponentView;

    move-result-object v0

    .line 898500
    if-ne v2, v3, :cond_3

    .line 898501
    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->getTop()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setTranslationY(F)V

    .line 898502
    iput-object v0, p0, LX/5KG;->e:Landroid/view/View;

    .line 898503
    iget-object v0, p0, LX/5KG;->c:LX/5KB;

    invoke-virtual {v0}, LX/5KB;->d()V

    .line 898504
    iput v6, p0, LX/5KG;->f:I

    goto :goto_2

    .line 898505
    :cond_3
    iget-object v0, p0, LX/5KG;->c:LX/5KB;

    .line 898506
    iget-object v4, v0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    invoke-virtual {v4}, Lcom/facebook/components/ComponentView;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_a

    const/4 v4, 0x1

    :goto_3
    move v0, v4

    .line 898507
    if-nez v0, :cond_4

    iget v0, p0, LX/5KG;->f:I

    if-eq v3, v0, :cond_5

    .line 898508
    :cond_4
    invoke-direct {p0, v3}, LX/5KG;->a(I)V

    .line 898509
    iget-object v0, p0, LX/5KG;->c:LX/5KB;

    .line 898510
    iget-object v4, v0, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/components/ComponentView;->setVisibility(I)V

    .line 898511
    :cond_5
    iget-object v0, p0, LX/5KG;->a:LX/5Je;

    check-cast v0, LX/5Jl;

    .line 898512
    invoke-virtual {v0}, LX/5Je;->k()LX/1OR;

    move-result-object v4

    check-cast v4, LX/1P1;

    invoke-virtual {v4}, LX/1P1;->n()I

    move-result v4

    move v4, v4

    .line 898513
    :goto_4
    if-gt v2, v4, :cond_7

    .line 898514
    iget-object v0, p0, LX/5KG;->a:LX/5Je;

    check-cast v0, LX/5Jl;

    invoke-virtual {v0, v2}, LX/5Jl;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 898515
    iget-object v0, p0, LX/5KG;->b:LX/1OR;

    invoke-virtual {v0, v2}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    .line 898516
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget-object v2, p0, LX/5KG;->c:LX/5KB;

    .line 898517
    iget-object v4, v2, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    move-object v2, v4

    .line 898518
    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->getBottom()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, LX/5KG;->c:LX/5KB;

    invoke-virtual {v2}, LX/5KB;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    .line 898519
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 898520
    :goto_5
    iget-object v1, p0, LX/5KG;->c:LX/5KB;

    .line 898521
    iget-object v2, v1, LX/5KB;->c:Lcom/facebook/components/ComponentView;

    int-to-float v4, v0

    invoke-virtual {v2, v4}, Lcom/facebook/components/ComponentView;->setTranslationY(F)V

    .line 898522
    iput v3, p0, LX/5KG;->f:I

    goto :goto_2

    .line 898523
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_5

    .line 898524
    :cond_8
    add-int/lit8 v3, v3, -0x1

    goto/16 :goto_0

    .line 898525
    :cond_9
    const/4 v0, -0x1

    goto/16 :goto_1

    :cond_a
    const/4 v4, 0x0

    goto :goto_3
.end method
