.class public LX/6QD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0So;

.field private b:Ljava/util/concurrent/ScheduledExecutorService;

.field public final c:Landroid/content/Context;

.field public d:Landroid/widget/ProgressBar;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Landroid/widget/ImageButton;

.field private h:LX/6QC;

.field public i:J

.field private j:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Landroid/view/animation/Interpolator;

.field public l:J

.field public m:I

.field private final n:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0So;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1087126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1087127
    const-wide/16 v0, 0x1

    iput-wide v0, p0, LX/6QD;->i:J

    .line 1087128
    const/4 v0, 0x0

    iput v0, p0, LX/6QD;->m:I

    .line 1087129
    new-instance v0, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;

    invoke-direct {v0, p0}, Lcom/facebook/directinstall/appdetails/InstallProgressDisplayHelper$2;-><init>(LX/6QD;)V

    iput-object v0, p0, LX/6QD;->n:Ljava/lang/Runnable;

    .line 1087130
    iput-object p1, p0, LX/6QD;->c:Landroid/content/Context;

    .line 1087131
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, LX/6QD;->k:Landroid/view/animation/Interpolator;

    .line 1087132
    iput-object p2, p0, LX/6QD;->a:LX/0So;

    .line 1087133
    iput-object p3, p0, LX/6QD;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1087134
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 1087124
    iget-object v0, p0, LX/6QD;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1087125
    return-void
.end method

.method public static b(LX/6QD;)V
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1087096
    invoke-direct {p0}, LX/6QD;->c()V

    .line 1087097
    iget-object v0, p0, LX/6QD;->h:LX/6QC;

    sget-object v1, LX/6QC;->DOWNLOADING:LX/6QC;

    if-ne v0, v1, :cond_1

    .line 1087098
    invoke-direct {p0, v3}, LX/6QD;->e(I)V

    .line 1087099
    iget-object v0, p0, LX/6QD;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1087100
    iget v0, p0, LX/6QD;->m:I

    .line 1087101
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v6

    .line 1087102
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 1087103
    iget-object v7, p0, LX/6QD;->f:Lcom/facebook/resources/ui/FbTextView;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, " "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-float v9, v0

    const/high16 v10, 0x42c80000    # 100.0f

    div-float/2addr v9, v10

    float-to-double v10, v9

    invoke-virtual {v6, v10, v11}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1087104
    invoke-direct {p0, v3}, LX/6QD;->b(I)V

    .line 1087105
    const v0, 0x7f081e84

    .line 1087106
    iget-object v1, p0, LX/6QD;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, LX/6QD;->c:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1087107
    iget v0, p0, LX/6QD;->m:I

    if-nez v0, :cond_0

    .line 1087108
    iget-object v0, p0, LX/6QD;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 1087109
    :goto_0
    iget-object v0, p0, LX/6QD;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/6QD;->l:J

    .line 1087110
    invoke-static {p0, v3}, LX/6QD;->d(LX/6QD;I)V

    .line 1087111
    :goto_1
    return-void

    .line 1087112
    :cond_0
    iget-object v0, p0, LX/6QD;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0

    .line 1087113
    :cond_1
    iget-object v0, p0, LX/6QD;->h:LX/6QC;

    sget-object v1, LX/6QC;->INSTALLING:LX/6QC;

    if-ne v0, v1, :cond_2

    .line 1087114
    invoke-direct {p0, v2}, LX/6QD;->e(I)V

    .line 1087115
    iget-object v0, p0, LX/6QD;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1087116
    invoke-direct {p0, v4}, LX/6QD;->b(I)V

    .line 1087117
    iget-object v0, p0, LX/6QD;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_1

    .line 1087118
    :cond_2
    iget-object v0, p0, LX/6QD;->h:LX/6QC;

    sget-object v1, LX/6QC;->COMPLETED:LX/6QC;

    if-ne v0, v1, :cond_3

    .line 1087119
    invoke-direct {p0, v2}, LX/6QD;->e(I)V

    .line 1087120
    iget-object v0, p0, LX/6QD;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1087121
    invoke-direct {p0, v4}, LX/6QD;->b(I)V

    .line 1087122
    iget-object v0, p0, LX/6QD;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 1087123
    :cond_3
    iget-object v0, p0, LX/6QD;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1087092
    iget-object v0, p0, LX/6QD;->j:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 1087093
    iget-object v0, p0, LX/6QD;->j:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1087094
    const/4 v0, 0x0

    iput-object v0, p0, LX/6QD;->j:Ljava/util/concurrent/Future;

    .line 1087095
    :cond_0
    return-void
.end method

.method public static d(LX/6QD;I)V
    .locals 5

    .prologue
    .line 1087089
    invoke-direct {p0}, LX/6QD;->c()V

    .line 1087090
    iget-object v0, p0, LX/6QD;->b:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, LX/6QD;->n:Ljava/lang/Runnable;

    int-to-long v2, p1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/6QD;->j:Ljava/util/concurrent/Future;

    .line 1087091
    return-void
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 1087073
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 1087074
    iget-object v0, p0, LX/6QD;->f:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1087075
    :cond_0
    iget-object v0, p0, LX/6QD;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1087076
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1087081
    sget-object v0, LX/6QC;->UNKNOWN:LX/6QC;

    iput-object v0, p0, LX/6QD;->h:LX/6QC;

    .line 1087082
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/6QD;->i:J

    .line 1087083
    iget-object v0, p0, LX/6QD;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1087084
    iget-object v0, p0, LX/6QD;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1087085
    invoke-direct {p0, v3}, LX/6QD;->b(I)V

    .line 1087086
    invoke-direct {p0, v3}, LX/6QD;->e(I)V

    .line 1087087
    iput v2, p0, LX/6QD;->m:I

    .line 1087088
    return-void
.end method

.method public final a(LX/6QC;)V
    .locals 1

    .prologue
    .line 1087077
    iget-object v0, p0, LX/6QD;->h:LX/6QC;

    if-eq v0, p1, :cond_0

    .line 1087078
    iput-object p1, p0, LX/6QD;->h:LX/6QC;

    .line 1087079
    invoke-static {p0}, LX/6QD;->b(LX/6QD;)V

    .line 1087080
    :cond_0
    return-void
.end method
