.class public final LX/681;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/692;

.field public b:Z

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:F

.field public l:F

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1054124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1054125
    iput v0, p0, LX/681;->c:I

    .line 1054126
    iput-boolean v0, p0, LX/681;->d:Z

    .line 1054127
    iput-boolean v0, p0, LX/681;->e:Z

    .line 1054128
    iput-boolean v0, p0, LX/681;->j:Z

    .line 1054129
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, LX/681;->k:F

    .line 1054130
    const/high16 v0, 0x41980000    # 19.0f

    iput v0, p0, LX/681;->l:F

    .line 1054131
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/681;
    .locals 4

    .prologue
    .line 1054132
    new-instance v0, LX/681;

    invoke-direct {v0}, LX/681;-><init>()V

    .line 1054133
    if-nez p1, :cond_0

    .line 1054134
    :goto_0
    return-object v0

    .line 1054135
    :cond_0
    invoke-static {p1}, LX/692;->a(Landroid/util/AttributeSet;)LX/692;

    move-result-object v1

    iput-object v1, v0, LX/681;->a:LX/692;

    .line 1054136
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "uiCompass"

    iget-boolean v3, v0, LX/681;->b:Z

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/681;->b:Z

    .line 1054137
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "mapType"

    invoke-interface {p1, v1, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1054138
    const-string v2, "satellite"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1054139
    const/4 v1, 0x2

    iput v1, v0, LX/681;->c:I

    .line 1054140
    :cond_1
    :goto_1
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "uiRotateGestures"

    iget-boolean v3, v0, LX/681;->d:Z

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/681;->d:Z

    .line 1054141
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "uiScrollGestures"

    iget-boolean v3, v0, LX/681;->e:Z

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/681;->e:Z

    .line 1054142
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "uiTiltGestures"

    iget-boolean v3, v0, LX/681;->f:Z

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/681;->f:Z

    .line 1054143
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "mUseViewLifecycleInFragment"

    iget-boolean v3, v0, LX/681;->g:Z

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/681;->g:Z

    .line 1054144
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "zOrderOnTop"

    iget-boolean v3, v0, LX/681;->h:Z

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/681;->h:Z

    .line 1054145
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "uiZoomControls"

    iget-boolean v3, v0, LX/681;->i:Z

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/681;->i:Z

    .line 1054146
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "uiZoomGestures"

    iget-boolean v3, v0, LX/681;->j:Z

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, LX/681;->j:Z

    .line 1054147
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "maxZoomLevel"

    iget v3, v0, LX/681;->l:F

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeFloatValue(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v1

    iput v1, v0, LX/681;->l:F

    .line 1054148
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "minZoomLevel"

    iget v3, v0, LX/681;->k:F

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeFloatValue(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v1

    iput v1, v0, LX/681;->k:F

    .line 1054149
    const-string v1, "http://schemas.android.com/apk/facebook"

    const-string v2, "surface"

    invoke-interface {p1, v1, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/681;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 1054150
    :cond_2
    const-string v2, "terrain"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1054151
    const/4 v1, 0x3

    iput v1, v0, LX/681;->c:I

    goto/16 :goto_1

    .line 1054152
    :cond_3
    const-string v2, "hybrid"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1054153
    const/4 v1, 0x4

    iput v1, v0, LX/681;->c:I

    goto/16 :goto_1

    .line 1054154
    :cond_4
    const-string v2, "live"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1054155
    const/4 v1, 0x5

    iput v1, v0, LX/681;->c:I

    goto/16 :goto_1

    .line 1054156
    :cond_5
    const-string v2, "crowdsourcing_osm"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1054157
    const/4 v1, 0x6

    iput v1, v0, LX/681;->c:I

    goto/16 :goto_1
.end method
