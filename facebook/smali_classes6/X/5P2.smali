.class public final enum LX/5P2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5P2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5P2;

.field public static final enum CI_PYMK:LX/5P2;

.field public static final enum CONTACT_IMPORTER:LX/5P2;

.field public static final enum FRIENDING_CARD:LX/5P2;

.field public static final enum FRIENDING_RADAR:LX/5P2;

.field public static final enum FRIENDS_CENTER:LX/5P2;

.field public static final enum FRIENDS_TAB:LX/5P2;

.field public static final enum JEWEL:LX/5P2;

.field public static final enum NEARBY_FRIENDS:LX/5P2;

.field public static final enum NEWSFEED:LX/5P2;

.field public static final enum PROFILE_BROWSER_LIKES:LX/5P2;

.field public static final enum PYMK_FEED:LX/5P2;

.field public static final enum PYMK_JEWEL:LX/5P2;

.field public static final enum QR_CODE:LX/5P2;

.field public static final enum SEARCH:LX/5P2;

.field public static final enum SHORTCUT:LX/5P2;

.field public static final enum TIMELINE_FRIENDS_BOX:LX/5P2;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 909844
    new-instance v0, LX/5P2;

    const-string v1, "QR_CODE"

    const-string v2, "qr_code"

    invoke-direct {v0, v1, v4, v2}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->QR_CODE:LX/5P2;

    .line 909845
    new-instance v0, LX/5P2;

    const-string v1, "JEWEL"

    const-string v2, "jewel"

    invoke-direct {v0, v1, v5, v2}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->JEWEL:LX/5P2;

    .line 909846
    new-instance v0, LX/5P2;

    const-string v1, "PYMK_FEED"

    const-string v2, "pymk_feed"

    invoke-direct {v0, v1, v6, v2}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->PYMK_FEED:LX/5P2;

    .line 909847
    new-instance v0, LX/5P2;

    const-string v1, "PYMK_JEWEL"

    const-string v2, "pymk_jewel"

    invoke-direct {v0, v1, v7, v2}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->PYMK_JEWEL:LX/5P2;

    .line 909848
    new-instance v0, LX/5P2;

    const-string v1, "SEARCH"

    const-string v2, "search"

    invoke-direct {v0, v1, v8, v2}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->SEARCH:LX/5P2;

    .line 909849
    new-instance v0, LX/5P2;

    const-string v1, "SHORTCUT"

    const/4 v2, 0x5

    const-string v3, "shortcut"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->SHORTCUT:LX/5P2;

    .line 909850
    new-instance v0, LX/5P2;

    const-string v1, "PROFILE_BROWSER_LIKES"

    const/4 v2, 0x6

    const-string v3, "pb_likes"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->PROFILE_BROWSER_LIKES:LX/5P2;

    .line 909851
    new-instance v0, LX/5P2;

    const-string v1, "FRIENDING_CARD"

    const/4 v2, 0x7

    const-string v3, "friending_card"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->FRIENDING_CARD:LX/5P2;

    .line 909852
    new-instance v0, LX/5P2;

    const-string v1, "NEWSFEED"

    const/16 v2, 0x8

    const-string v3, "nf"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->NEWSFEED:LX/5P2;

    .line 909853
    new-instance v0, LX/5P2;

    const-string v1, "FRIENDS_CENTER"

    const/16 v2, 0x9

    const-string v3, "fc_pymk"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->FRIENDS_CENTER:LX/5P2;

    .line 909854
    new-instance v0, LX/5P2;

    const-string v1, "FRIENDING_RADAR"

    const/16 v2, 0xa

    const-string v3, "friending_radar"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->FRIENDING_RADAR:LX/5P2;

    .line 909855
    new-instance v0, LX/5P2;

    const-string v1, "CI_PYMK"

    const/16 v2, 0xb

    const-string v3, "ci_pymk"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->CI_PYMK:LX/5P2;

    .line 909856
    new-instance v0, LX/5P2;

    const-string v1, "CONTACT_IMPORTER"

    const/16 v2, 0xc

    const-string v3, "friend_finder"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->CONTACT_IMPORTER:LX/5P2;

    .line 909857
    new-instance v0, LX/5P2;

    const-string v1, "NEARBY_FRIENDS"

    const/16 v2, 0xd

    const-string v3, "nearby_friends"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->NEARBY_FRIENDS:LX/5P2;

    .line 909858
    new-instance v0, LX/5P2;

    const-string v1, "FRIENDS_TAB"

    const/16 v2, 0xe

    const-string v3, "fr_tab"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->FRIENDS_TAB:LX/5P2;

    .line 909859
    new-instance v0, LX/5P2;

    const-string v1, "TIMELINE_FRIENDS_BOX"

    const/16 v2, 0xf

    const-string v3, "tl_fr_box"

    invoke-direct {v0, v1, v2, v3}, LX/5P2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5P2;->TIMELINE_FRIENDS_BOX:LX/5P2;

    .line 909860
    const/16 v0, 0x10

    new-array v0, v0, [LX/5P2;

    sget-object v1, LX/5P2;->QR_CODE:LX/5P2;

    aput-object v1, v0, v4

    sget-object v1, LX/5P2;->JEWEL:LX/5P2;

    aput-object v1, v0, v5

    sget-object v1, LX/5P2;->PYMK_FEED:LX/5P2;

    aput-object v1, v0, v6

    sget-object v1, LX/5P2;->PYMK_JEWEL:LX/5P2;

    aput-object v1, v0, v7

    sget-object v1, LX/5P2;->SEARCH:LX/5P2;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5P2;->SHORTCUT:LX/5P2;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5P2;->PROFILE_BROWSER_LIKES:LX/5P2;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5P2;->FRIENDING_CARD:LX/5P2;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5P2;->NEWSFEED:LX/5P2;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5P2;->FRIENDS_CENTER:LX/5P2;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/5P2;->FRIENDING_RADAR:LX/5P2;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/5P2;->CI_PYMK:LX/5P2;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/5P2;->CONTACT_IMPORTER:LX/5P2;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/5P2;->NEARBY_FRIENDS:LX/5P2;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/5P2;->FRIENDS_TAB:LX/5P2;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/5P2;->TIMELINE_FRIENDS_BOX:LX/5P2;

    aput-object v2, v0, v1

    sput-object v0, LX/5P2;->$VALUES:[LX/5P2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 909861
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 909862
    iput-object p3, p0, LX/5P2;->value:Ljava/lang/String;

    .line 909863
    return-void
.end method

.method public static from(Ljava/io/Serializable;)LX/5P2;
    .locals 5
    .param p0    # Ljava/io/Serializable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 909864
    instance-of v0, p0, LX/5P2;

    if-eqz v0, :cond_0

    .line 909865
    check-cast p0, LX/5P2;

    .line 909866
    :goto_0
    return-object p0

    .line 909867
    :cond_0
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 909868
    invoke-static {}, LX/5P2;->values()[LX/5P2;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 909869
    iget-object v4, v0, LX/5P2;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object p0, v0

    .line 909870
    goto :goto_0

    .line 909871
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 909872
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5P2;
    .locals 1

    .prologue
    .line 909873
    const-class v0, LX/5P2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5P2;

    return-object v0
.end method

.method public static values()[LX/5P2;
    .locals 1

    .prologue
    .line 909874
    sget-object v0, LX/5P2;->$VALUES:[LX/5P2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5P2;

    return-object v0
.end method
