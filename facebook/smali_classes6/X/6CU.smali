.class public LX/6CU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/commerce/ResetCartJSBridgeCall;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6CP;


# direct methods
.method public constructor <init>(LX/6CP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063858
    iput-object p1, p0, LX/6CU;->a:LX/6CP;

    .line 1063859
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1063860
    const-string v0, "resetCart"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 3

    .prologue
    .line 1063861
    check-cast p1, Lcom/facebook/browserextensions/ipc/commerce/ResetCartJSBridgeCall;

    .line 1063862
    iget-object v0, p0, LX/6CU;->a:LX/6CP;

    .line 1063863
    const-string v1, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p1, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v1, v1

    .line 1063864
    new-instance v2, LX/6CT;

    invoke-direct {v2, p0, p1}, LX/6CT;-><init>(LX/6CU;Lcom/facebook/browserextensions/ipc/commerce/ResetCartJSBridgeCall;)V

    invoke-virtual {v0, v1, v2}, LX/6CP;->a(Ljava/lang/String;LX/6CO;)V

    .line 1063865
    return-void
.end method
