.class public final LX/69p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/graphql/executor/iface/ModelProcessor",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3PE;


# direct methods
.method public constructor <init>(LX/3PE;)V
    .locals 0

    .prologue
    .line 1058334
    iput-object p1, p0, LX/69p;->a:LX/3PE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0jT;LX/0jT;)LX/0jT;
    .locals 2

    .prologue
    .line 1058335
    check-cast p1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    check-cast p2, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    .line 1058336
    invoke-virtual {p1}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->H()Ljava/lang/String;

    move-result-object v0

    .line 1058337
    invoke-virtual {p2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    invoke-static {v1}, LX/4Vu;->a(Lcom/facebook/graphql/model/GraphQLComment;)LX/4Vu;

    move-result-object v1

    .line 1058338
    iput-object v0, v1, LX/4Vu;->B:Ljava/lang/String;

    .line 1058339
    move-object v0, v1

    .line 1058340
    new-instance v1, LX/58u;

    invoke-direct {v1}, LX/58u;-><init>()V

    invoke-virtual {v0}, LX/4Vu;->a()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v0

    .line 1058341
    iput-object v0, v1, LX/58u;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1058342
    move-object v0, v1

    .line 1058343
    invoke-virtual {p2}, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;->j()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    move-result-object v1

    .line 1058344
    iput-object v1, v0, LX/58u;->b:Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentMutateFeedbackFieldsModel;

    .line 1058345
    move-object v0, v0

    .line 1058346
    invoke-virtual {v0}, LX/58u;->a()Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentCreateMutationFragmentModel;

    move-result-object v0

    return-object v0
.end method
