.class public LX/6Ku;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Kd;


# instance fields
.field private final a:I

.field private final b:I

.field public c:Landroid/view/Surface;

.field public d:LX/6Kl;


# direct methods
.method public constructor <init>(Landroid/view/Surface;II)V
    .locals 2

    .prologue
    .line 1077669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077670
    if-nez p1, :cond_0

    .line 1077671
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "surface cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1077672
    :cond_0
    iput-object p1, p0, LX/6Ku;->c:Landroid/view/Surface;

    .line 1077673
    iput p2, p0, LX/6Ku;->a:I

    .line 1077674
    iput p3, p0, LX/6Ku;->b:I

    .line 1077675
    return-void
.end method


# virtual methods
.method public final a(LX/6Kl;)V
    .locals 1

    .prologue
    .line 1077683
    iput-object p1, p0, LX/6Ku;->d:LX/6Kl;

    .line 1077684
    iget-object v0, p0, LX/6Ku;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 1077685
    iget-object v0, p0, LX/6Ku;->c:Landroid/view/Surface;

    invoke-virtual {p1, p0, v0}, LX/6Kl;->a(LX/6Kd;Landroid/view/Surface;)V

    .line 1077686
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1077681
    invoke-virtual {p0}, LX/6Ku;->dE_()V

    .line 1077682
    return-void
.end method

.method public final dE_()V
    .locals 1

    .prologue
    .line 1077679
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Ku;->c:Landroid/view/Surface;

    .line 1077680
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1077687
    return-void
.end method

.method public final getHeight()I
    .locals 1

    .prologue
    .line 1077678
    iget v0, p0, LX/6Ku;->b:I

    return v0
.end method

.method public final getWidth()I
    .locals 1

    .prologue
    .line 1077677
    iget v0, p0, LX/6Ku;->a:I

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 1077676
    iget-object v0, p0, LX/6Ku;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Ku;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
