.class public LX/64A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "LX/1pN;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1044223
    const-class v0, LX/64A;

    sput-object v0, LX/64A;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1044224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044225
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 1044226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1044227
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/64A;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 1044228
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1044229
    move-object v1, v1

    .line 1044230
    const-string v2, "GET"

    .line 1044231
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1044232
    move-object v1, v1

    .line 1044233
    const-string v2, "method/mobile.zeroBalanceRedirect"

    .line 1044234
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1044235
    move-object v1, v1

    .line 1044236
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1044237
    move-object v0, v1

    .line 1044238
    const/4 v1, 0x1

    .line 1044239
    iput-boolean v1, v0, LX/14O;->B:Z

    .line 1044240
    move-object v0, v0

    .line 1044241
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 1044242
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1044243
    move-object v0, v0

    .line 1044244
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->DEFAULT_PRIORITY:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1044245
    return-object p2
.end method
