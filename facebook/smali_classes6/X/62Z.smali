.class public LX/62Z;
.super LX/3x6;
.source ""


# instance fields
.field private a:I

.field private final b:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 1041760
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1041761
    iput p2, p0, LX/62Z;->a:I

    .line 1041762
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/62Z;->b:Landroid/graphics/Paint;

    .line 1041763
    iget-object v0, p0, LX/62Z;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1041764
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 10

    .prologue
    .line 1041765
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v7

    .line 1041766
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v1

    sub-int v8, v0, v1

    .line 1041767
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v9

    .line 1041768
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    add-int/lit8 v0, v9, -0x1

    if-ge v6, v0, :cond_0

    .line 1041769
    invoke-virtual {p2, v6}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1041770
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 1041771
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    .line 1041772
    iget v1, p0, LX/62Z;->a:I

    add-int v3, v0, v1

    .line 1041773
    int-to-float v1, v0

    int-to-float v2, v7

    int-to-float v3, v3

    int-to-float v4, v8

    iget-object v5, p0, LX/62Z;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1041774
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 1041775
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 1

    .prologue
    .line 1041776
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 1041777
    return-void

    .line 1041778
    :cond_0
    iget v0, p0, LX/62Z;->a:I

    goto :goto_0
.end method
