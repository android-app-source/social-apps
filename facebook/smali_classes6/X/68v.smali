.class public LX/68v;
.super LX/67m;
.source ""


# instance fields
.field private final o:Landroid/graphics/Paint;

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:I


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1057128
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1057129
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/68v;->o:Landroid/graphics/Paint;

    .line 1057130
    const/high16 v0, 0x41400000    # 12.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68v;->p:F

    .line 1057131
    const/high16 v0, 0x42140000    # 37.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68v;->q:F

    .line 1057132
    const/high16 v0, 0x3f000000    # 0.5f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68v;->r:F

    .line 1057133
    iget v0, p0, LX/67m;->d:F

    mul-float/2addr v0, v2

    iput v0, p0, LX/68v;->s:F

    .line 1057134
    const/4 v0, 0x3

    iput v0, p0, LX/68v;->j:I

    .line 1057135
    iput v2, p0, LX/68v;->k:F

    .line 1057136
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68v;->l:Z

    .line 1057137
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    .line 1057075
    iget v2, p0, LX/68v;->t:F

    iget v3, p0, LX/68v;->q:F

    sub-float/2addr v2, v3

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_1

    iget v2, p0, LX/68v;->t:F

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_1

    iget v2, p0, LX/68v;->u:F

    iget v3, p0, LX/68v;->q:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_1

    iget v2, p0, LX/68v;->u:F

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_1

    .line 1057076
    iget v2, p0, LX/68v;->u:F

    iget v3, p0, LX/68v;->q:F

    sub-float/2addr v2, v3

    cmpg-float v2, p2, v2

    if-gez v2, :cond_0

    .line 1057077
    const/4 v1, 0x1

    iput v1, p0, LX/68v;->v:I

    .line 1057078
    :goto_0
    return v0

    .line 1057079
    :cond_0
    iget v2, p0, LX/68v;->u:F

    iget v3, p0, LX/68v;->q:F

    sub-float/2addr v2, v3

    cmpl-float v2, p2, v2

    if-lez v2, :cond_1

    .line 1057080
    iput v0, p0, LX/68v;->v:I

    goto :goto_0

    .line 1057081
    :cond_1
    iput v1, p0, LX/68v;->v:I

    move v0, v1

    .line 1057082
    goto :goto_0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v9, 0x3fc00000    # 1.5f

    const/high16 v8, 0x3f400000    # 0.75f

    const/high16 v7, 0x3e800000    # 0.25f

    const/high16 v6, 0x3f000000    # 0.5f

    .line 1057108
    iget-object v0, p0, LX/68v;->o:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1057109
    iget-object v1, p0, LX/68v;->o:Landroid/graphics/Paint;

    iget v0, p0, LX/68v;->v:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const v0, -0x222223

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1057110
    iget-object v0, p0, LX/68v;->o:Landroid/graphics/Paint;

    const/16 v1, 0xe6

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1057111
    iget v0, p0, LX/68v;->t:F

    iget v1, p0, LX/68v;->q:F

    sub-float v1, v0, v1

    iget v0, p0, LX/68v;->u:F

    iget v2, p0, LX/68v;->q:F

    mul-float/2addr v2, v10

    sub-float v2, v0, v2

    iget v3, p0, LX/68v;->t:F

    iget v0, p0, LX/68v;->u:F

    iget v4, p0, LX/68v;->q:F

    sub-float v4, v0, v4

    iget-object v5, p0, LX/68v;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1057112
    iget-object v1, p0, LX/68v;->o:Landroid/graphics/Paint;

    iget v0, p0, LX/68v;->v:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const v0, -0x222223

    :goto_1
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1057113
    iget-object v0, p0, LX/68v;->o:Landroid/graphics/Paint;

    const/16 v1, 0xe6

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1057114
    iget v0, p0, LX/68v;->t:F

    iget v1, p0, LX/68v;->q:F

    sub-float v1, v0, v1

    iget v0, p0, LX/68v;->u:F

    iget v2, p0, LX/68v;->q:F

    sub-float v2, v0, v2

    iget v3, p0, LX/68v;->t:F

    iget v4, p0, LX/68v;->u:F

    iget-object v5, p0, LX/68v;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1057115
    iget-object v0, p0, LX/68v;->o:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1057116
    iget-object v0, p0, LX/68v;->o:Landroid/graphics/Paint;

    iget v1, p0, LX/68v;->r:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1057117
    iget-object v0, p0, LX/68v;->o:Landroid/graphics/Paint;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1057118
    iget v0, p0, LX/68v;->t:F

    iget v1, p0, LX/68v;->q:F

    sub-float v1, v0, v1

    iget v0, p0, LX/68v;->u:F

    iget v2, p0, LX/68v;->q:F

    mul-float/2addr v2, v10

    sub-float v2, v0, v2

    iget v3, p0, LX/68v;->t:F

    iget v0, p0, LX/68v;->u:F

    iget v4, p0, LX/68v;->q:F

    sub-float v4, v0, v4

    iget-object v5, p0, LX/68v;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1057119
    iget v0, p0, LX/68v;->t:F

    iget v1, p0, LX/68v;->q:F

    sub-float v1, v0, v1

    iget v0, p0, LX/68v;->u:F

    iget v2, p0, LX/68v;->q:F

    sub-float v2, v0, v2

    iget v3, p0, LX/68v;->t:F

    iget v4, p0, LX/68v;->u:F

    iget-object v5, p0, LX/68v;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1057120
    iget-object v0, p0, LX/68v;->o:Landroid/graphics/Paint;

    iget v1, p0, LX/68v;->s:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1057121
    iget-object v0, p0, LX/68v;->o:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1057122
    iget v0, p0, LX/68v;->t:F

    iget v1, p0, LX/68v;->q:F

    mul-float/2addr v1, v8

    sub-float v1, v0, v1

    iget v0, p0, LX/68v;->u:F

    iget v2, p0, LX/68v;->q:F

    mul-float/2addr v2, v9

    sub-float v2, v0, v2

    iget v0, p0, LX/68v;->t:F

    iget v3, p0, LX/68v;->q:F

    mul-float/2addr v3, v7

    sub-float v3, v0, v3

    iget v0, p0, LX/68v;->u:F

    iget v4, p0, LX/68v;->q:F

    mul-float/2addr v4, v9

    sub-float v4, v0, v4

    iget-object v5, p0, LX/68v;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1057123
    iget v0, p0, LX/68v;->t:F

    iget v1, p0, LX/68v;->q:F

    mul-float/2addr v1, v6

    sub-float v1, v0, v1

    iget v0, p0, LX/68v;->u:F

    iget v2, p0, LX/68v;->q:F

    const/high16 v3, 0x3fe00000    # 1.75f

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, LX/68v;->t:F

    iget v3, p0, LX/68v;->q:F

    mul-float/2addr v3, v6

    sub-float v3, v0, v3

    iget v0, p0, LX/68v;->u:F

    iget v4, p0, LX/68v;->q:F

    const/high16 v5, 0x3fa00000    # 1.25f

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, LX/68v;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1057124
    iget v0, p0, LX/68v;->t:F

    iget v1, p0, LX/68v;->q:F

    mul-float/2addr v1, v8

    sub-float v1, v0, v1

    iget v0, p0, LX/68v;->u:F

    iget v2, p0, LX/68v;->q:F

    mul-float/2addr v2, v6

    sub-float v2, v0, v2

    iget v0, p0, LX/68v;->t:F

    iget v3, p0, LX/68v;->q:F

    mul-float/2addr v3, v7

    sub-float v3, v0, v3

    iget v0, p0, LX/68v;->u:F

    iget v4, p0, LX/68v;->q:F

    mul-float/2addr v4, v6

    sub-float v4, v0, v4

    iget-object v5, p0, LX/68v;->o:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1057125
    return-void

    .line 1057126
    :cond_0
    const/4 v0, -0x1

    goto/16 :goto_0

    .line 1057127
    :cond_1
    const/4 v0, -0x1

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1057103
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1057104
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 1057105
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, LX/68v;->p:F

    sub-float/2addr v1, v2

    iget-object v2, p0, LX/67m;->e:LX/680;

    iget v2, v2, LX/680;->e:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, p0, LX/68v;->t:F

    .line 1057106
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/68v;->p:F

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->f:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/68v;->u:F

    .line 1057107
    return-void
.end method

.method public final d(FF)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1057093
    iget v2, p0, LX/68v;->v:I

    if-ne v2, v1, :cond_3

    .line 1057094
    invoke-virtual {p0, p1, p2}, LX/67m;->a(FF)I

    .line 1057095
    iget v2, p0, LX/68v;->v:I

    if-eq v2, v1, :cond_0

    .line 1057096
    iput v0, p0, LX/68v;->v:I

    .line 1057097
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057098
    iget v2, p0, LX/68v;->v:I

    if-eq v2, v1, :cond_1

    iget v2, p0, LX/68v;->v:I

    if-ne v2, v3, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0

    .line 1057099
    :cond_3
    iget v2, p0, LX/68v;->v:I

    if-ne v2, v3, :cond_0

    .line 1057100
    invoke-virtual {p0, p1, p2}, LX/67m;->a(FF)I

    .line 1057101
    iget v2, p0, LX/68v;->v:I

    if-eq v2, v3, :cond_0

    .line 1057102
    iput v0, p0, LX/68v;->v:I

    goto :goto_0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 1057090
    iget v0, p0, LX/68v;->v:I

    if-eqz v0, :cond_0

    .line 1057091
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057092
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0xc8

    .line 1057083
    iget v0, p0, LX/68v;->v:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1057084
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-static {}, LX/67e;->a()LX/67d;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, LX/680;->a(LX/67d;ILX/6aX;)V

    .line 1057085
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, LX/68v;->v:I

    .line 1057086
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1057087
    return-void

    .line 1057088
    :cond_1
    iget v0, p0, LX/68v;->v:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1057089
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-static {}, LX/67e;->b()LX/67d;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, LX/680;->a(LX/67d;ILX/6aX;)V

    goto :goto_0
.end method
