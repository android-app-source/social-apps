.class public final LX/6GK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/6FU;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/4BY;

.field public final synthetic d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;ZLjava/lang/String;LX/4BY;)V
    .locals 0

    .prologue
    .line 1070216
    iput-object p1, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iput-boolean p2, p0, LX/6GK;->a:Z

    iput-object p3, p0, LX/6GK;->b:Ljava/lang/String;

    iput-object p4, p0, LX/6GK;->c:LX/4BY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1070217
    iget-object v0, p0, LX/6GK;->c:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1070218
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1070219
    const-string v1, "isSendClickedFlag"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1070220
    iget-object v1, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->n:LX/42n;

    iget-object v2, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    invoke-interface {v1, v2, v0}, LX/42n;->a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    .line 1070221
    iget-object v0, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    .line 1070222
    iput-boolean v3, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->r:Z

    .line 1070223
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1070224
    check-cast p1, LX/6FU;

    const/4 v3, 0x1

    .line 1070225
    iget-boolean v0, p0, LX/6GK;->a:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1070226
    iget-object v0, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    invoke-static {v0}, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->e$redex0(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;)V

    .line 1070227
    iget-object v0, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iget-object v1, p0, LX/6GK;->b:Ljava/lang/String;

    const/4 v8, 0x0

    .line 1070228
    iget-object v4, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    invoke-virtual {v4}, LX/6FU;->d()LX/0Px;

    move-result-object v4

    .line 1070229
    iput-object v4, p1, LX/6FU;->d:Ljava/util/List;

    .line 1070230
    iput-object v1, p1, LX/6FU;->b:Ljava/lang/String;

    .line 1070231
    iget-object v4, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->b:LX/0WV;

    invoke-virtual {v4}, LX/0WV;->b()I

    move-result v4

    int-to-long v4, v4

    .line 1070232
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_2

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 1070233
    :goto_0
    iget-object v5, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->e:LX/1gy;

    iget-object v5, v5, LX/1gy;->a:Ljava/lang/String;

    .line 1070234
    iput-object v5, p1, LX/6FU;->k:Ljava/lang/String;

    .line 1070235
    iput-object v4, p1, LX/6FU;->l:Ljava/lang/String;

    .line 1070236
    iget-object v4, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->f:LX/01U;

    sget-object v5, LX/01U;->PROD:LX/01U;

    if-ne v4, v5, :cond_3

    .line 1070237
    iput-object v8, p1, LX/6FU;->m:Ljava/lang/String;

    .line 1070238
    iput-object v8, p1, LX/6FU;->n:Ljava/lang/String;

    .line 1070239
    :goto_1
    invoke-virtual {p1}, LX/6FU;->y()Lcom/facebook/bugreporter/BugReport;

    move-result-object v0

    .line 1070240
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 1070241
    if-eqz v1, :cond_0

    .line 1070242
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 1070243
    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1070244
    invoke-virtual {v2}, Landroid/os/Parcel;->dataSize()I

    .line 1070245
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 1070246
    :cond_0
    iget-object v1, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->c:LX/0TD;

    new-instance v2, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment$4$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment$4$1;-><init>(LX/6GK;Lcom/facebook/bugreporter/BugReport;)V

    const v0, 0x634bb074

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1070247
    iget-object v0, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->i:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_DID_COMPLETE:LX/6GY;

    invoke-virtual {v0, v1}, LX/6GZ;->a(LX/6GY;)V

    .line 1070248
    :cond_1
    iget-object v0, p0, LX/6GK;->c:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1070249
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1070250
    const-string v1, "isSendClickedFlag"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1070251
    iget-object v1, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->n:LX/42n;

    iget-object v2, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    invoke-interface {v1, v2, v0}, LX/42n;->a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    .line 1070252
    iget-object v0, p0, LX/6GK;->d:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    .line 1070253
    iput-boolean v3, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->r:Z

    .line 1070254
    return-void

    .line 1070255
    :cond_2
    const-string v4, ""

    goto :goto_0

    .line 1070256
    :cond_3
    iget-object v4, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->e:LX/1gy;

    iget-object v4, v4, LX/1gy;->d:Ljava/lang/String;

    .line 1070257
    iput-object v4, p1, LX/6FU;->m:Ljava/lang/String;

    .line 1070258
    iget-object v4, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->e:LX/1gy;

    iget-object v4, v4, LX/1gy;->b:Ljava/lang/String;

    .line 1070259
    iput-object v4, p1, LX/6FU;->n:Ljava/lang/String;

    .line 1070260
    goto :goto_1
.end method
