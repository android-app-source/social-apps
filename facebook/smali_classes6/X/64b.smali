.class public final LX/64b;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/64b;

.field public static final B:LX/64b;

.field public static final C:LX/64b;

.field public static final D:LX/64b;

.field public static final E:LX/64b;

.field public static final F:LX/64b;

.field public static final G:LX/64b;

.field public static final H:LX/64b;

.field public static final I:LX/64b;

.field public static final J:LX/64b;

.field public static final K:LX/64b;

.field public static final L:LX/64b;

.field public static final M:LX/64b;

.field public static final N:LX/64b;

.field public static final O:LX/64b;

.field public static final P:LX/64b;

.field public static final Q:LX/64b;

.field public static final R:LX/64b;

.field public static final S:LX/64b;

.field public static final T:LX/64b;

.field public static final U:LX/64b;

.field public static final V:LX/64b;

.field public static final W:LX/64b;

.field public static final X:LX/64b;

.field public static final Y:LX/64b;

.field public static final Z:LX/64b;

.field public static final a:LX/64b;

.field public static final aA:LX/64b;

.field public static final aB:LX/64b;

.field public static final aC:LX/64b;

.field public static final aD:LX/64b;

.field public static final aE:LX/64b;

.field public static final aF:LX/64b;

.field public static final aG:LX/64b;

.field public static final aH:LX/64b;

.field public static final aI:LX/64b;

.field public static final aJ:LX/64b;

.field public static final aK:LX/64b;

.field public static final aL:LX/64b;

.field public static final aM:LX/64b;

.field public static final aN:LX/64b;

.field public static final aO:LX/64b;

.field public static final aP:LX/64b;

.field public static final aQ:LX/64b;

.field public static final aR:LX/64b;

.field private static final aT:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "LX/64b;",
            ">;"
        }
    .end annotation
.end field

.field public static final aa:LX/64b;

.field public static final ab:LX/64b;

.field public static final ac:LX/64b;

.field public static final ad:LX/64b;

.field public static final ae:LX/64b;

.field public static final af:LX/64b;

.field public static final ag:LX/64b;

.field public static final ah:LX/64b;

.field public static final ai:LX/64b;

.field public static final aj:LX/64b;

.field public static final ak:LX/64b;

.field public static final al:LX/64b;

.field public static final am:LX/64b;

.field public static final an:LX/64b;

.field public static final ao:LX/64b;

.field public static final ap:LX/64b;

.field public static final aq:LX/64b;

.field public static final ar:LX/64b;

.field public static final as:LX/64b;

.field public static final at:LX/64b;

.field public static final au:LX/64b;

.field public static final av:LX/64b;

.field public static final aw:LX/64b;

.field public static final ax:LX/64b;

.field public static final ay:LX/64b;

.field public static final az:LX/64b;

.field public static final b:LX/64b;

.field public static final c:LX/64b;

.field public static final d:LX/64b;

.field public static final e:LX/64b;

.field public static final f:LX/64b;

.field public static final g:LX/64b;

.field public static final h:LX/64b;

.field public static final i:LX/64b;

.field public static final j:LX/64b;

.field public static final k:LX/64b;

.field public static final l:LX/64b;

.field public static final m:LX/64b;

.field public static final n:LX/64b;

.field public static final o:LX/64b;

.field public static final p:LX/64b;

.field public static final q:LX/64b;

.field public static final r:LX/64b;

.field public static final s:LX/64b;

.field public static final t:LX/64b;

.field public static final u:LX/64b;

.field public static final v:LX/64b;

.field public static final w:LX/64b;

.field public static final x:LX/64b;

.field public static final y:LX/64b;

.field public static final z:LX/64b;


# instance fields
.field public final aS:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1044829
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, LX/64b;->aT:Ljava/util/concurrent/ConcurrentMap;

    .line 1044830
    const-string v0, "SSL_RSA_WITH_NULL_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->a:LX/64b;

    .line 1044831
    const-string v0, "SSL_RSA_WITH_NULL_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->b:LX/64b;

    .line 1044832
    const-string v0, "SSL_RSA_EXPORT_WITH_RC4_40_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->c:LX/64b;

    .line 1044833
    const-string v0, "SSL_RSA_WITH_RC4_128_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->d:LX/64b;

    .line 1044834
    const-string v0, "SSL_RSA_WITH_RC4_128_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->e:LX/64b;

    .line 1044835
    const-string v0, "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->f:LX/64b;

    .line 1044836
    const-string v0, "SSL_RSA_WITH_DES_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->g:LX/64b;

    .line 1044837
    const-string v0, "SSL_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->h:LX/64b;

    .line 1044838
    const-string v0, "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->i:LX/64b;

    .line 1044839
    const-string v0, "SSL_DHE_DSS_WITH_DES_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->j:LX/64b;

    .line 1044840
    const-string v0, "SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->k:LX/64b;

    .line 1044841
    const-string v0, "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->l:LX/64b;

    .line 1044842
    const-string v0, "SSL_DHE_RSA_WITH_DES_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->m:LX/64b;

    .line 1044843
    const-string v0, "SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->n:LX/64b;

    .line 1044844
    const-string v0, "SSL_DH_anon_EXPORT_WITH_RC4_40_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->o:LX/64b;

    .line 1044845
    const-string v0, "SSL_DH_anon_WITH_RC4_128_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->p:LX/64b;

    .line 1044846
    const-string v0, "SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->q:LX/64b;

    .line 1044847
    const-string v0, "SSL_DH_anon_WITH_DES_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->r:LX/64b;

    .line 1044848
    const-string v0, "SSL_DH_anon_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->s:LX/64b;

    .line 1044849
    const-string v0, "TLS_KRB5_WITH_DES_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->t:LX/64b;

    .line 1044850
    const-string v0, "TLS_KRB5_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->u:LX/64b;

    .line 1044851
    const-string v0, "TLS_KRB5_WITH_RC4_128_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->v:LX/64b;

    .line 1044852
    const-string v0, "TLS_KRB5_WITH_DES_CBC_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->w:LX/64b;

    .line 1044853
    const-string v0, "TLS_KRB5_WITH_3DES_EDE_CBC_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->x:LX/64b;

    .line 1044854
    const-string v0, "TLS_KRB5_WITH_RC4_128_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->y:LX/64b;

    .line 1044855
    const-string v0, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->z:LX/64b;

    .line 1044856
    const-string v0, "TLS_KRB5_EXPORT_WITH_RC4_40_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->A:LX/64b;

    .line 1044857
    const-string v0, "TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->B:LX/64b;

    .line 1044858
    const-string v0, "TLS_KRB5_EXPORT_WITH_RC4_40_MD5"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->C:LX/64b;

    .line 1044859
    const-string v0, "TLS_RSA_WITH_AES_128_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->D:LX/64b;

    .line 1044860
    const-string v0, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->E:LX/64b;

    .line 1044861
    const-string v0, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->F:LX/64b;

    .line 1044862
    const-string v0, "TLS_DH_anon_WITH_AES_128_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->G:LX/64b;

    .line 1044863
    const-string v0, "TLS_RSA_WITH_AES_256_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->H:LX/64b;

    .line 1044864
    const-string v0, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->I:LX/64b;

    .line 1044865
    const-string v0, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->J:LX/64b;

    .line 1044866
    const-string v0, "TLS_DH_anon_WITH_AES_256_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->K:LX/64b;

    .line 1044867
    const-string v0, "TLS_RSA_WITH_NULL_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->L:LX/64b;

    .line 1044868
    const-string v0, "TLS_RSA_WITH_AES_128_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->M:LX/64b;

    .line 1044869
    const-string v0, "TLS_RSA_WITH_AES_256_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->N:LX/64b;

    .line 1044870
    const-string v0, "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->O:LX/64b;

    .line 1044871
    const-string v0, "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->P:LX/64b;

    .line 1044872
    const-string v0, "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->Q:LX/64b;

    .line 1044873
    const-string v0, "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->R:LX/64b;

    .line 1044874
    const-string v0, "TLS_DH_anon_WITH_AES_128_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->S:LX/64b;

    .line 1044875
    const-string v0, "TLS_DH_anon_WITH_AES_256_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->T:LX/64b;

    .line 1044876
    const-string v0, "TLS_RSA_WITH_AES_128_GCM_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->U:LX/64b;

    .line 1044877
    const-string v0, "TLS_RSA_WITH_AES_256_GCM_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->V:LX/64b;

    .line 1044878
    const-string v0, "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->W:LX/64b;

    .line 1044879
    const-string v0, "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->X:LX/64b;

    .line 1044880
    const-string v0, "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->Y:LX/64b;

    .line 1044881
    const-string v0, "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->Z:LX/64b;

    .line 1044882
    const-string v0, "TLS_DH_anon_WITH_AES_128_GCM_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aa:LX/64b;

    .line 1044883
    const-string v0, "TLS_DH_anon_WITH_AES_256_GCM_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ab:LX/64b;

    .line 1044884
    const-string v0, "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ac:LX/64b;

    .line 1044885
    const-string v0, "TLS_ECDH_ECDSA_WITH_NULL_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ad:LX/64b;

    .line 1044886
    const-string v0, "TLS_ECDH_ECDSA_WITH_RC4_128_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ae:LX/64b;

    .line 1044887
    const-string v0, "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->af:LX/64b;

    .line 1044888
    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ag:LX/64b;

    .line 1044889
    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ah:LX/64b;

    .line 1044890
    const-string v0, "TLS_ECDHE_ECDSA_WITH_NULL_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ai:LX/64b;

    .line 1044891
    const-string v0, "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aj:LX/64b;

    .line 1044892
    const-string v0, "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ak:LX/64b;

    .line 1044893
    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->al:LX/64b;

    .line 1044894
    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->am:LX/64b;

    .line 1044895
    const-string v0, "TLS_ECDH_RSA_WITH_NULL_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->an:LX/64b;

    .line 1044896
    const-string v0, "TLS_ECDH_RSA_WITH_RC4_128_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ao:LX/64b;

    .line 1044897
    const-string v0, "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ap:LX/64b;

    .line 1044898
    const-string v0, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aq:LX/64b;

    .line 1044899
    const-string v0, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ar:LX/64b;

    .line 1044900
    const-string v0, "TLS_ECDHE_RSA_WITH_NULL_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->as:LX/64b;

    .line 1044901
    const-string v0, "TLS_ECDHE_RSA_WITH_RC4_128_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->at:LX/64b;

    .line 1044902
    const-string v0, "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->au:LX/64b;

    .line 1044903
    const-string v0, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->av:LX/64b;

    .line 1044904
    const-string v0, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aw:LX/64b;

    .line 1044905
    const-string v0, "TLS_ECDH_anon_WITH_NULL_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ax:LX/64b;

    .line 1044906
    const-string v0, "TLS_ECDH_anon_WITH_RC4_128_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->ay:LX/64b;

    .line 1044907
    const-string v0, "TLS_ECDH_anon_WITH_3DES_EDE_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->az:LX/64b;

    .line 1044908
    const-string v0, "TLS_ECDH_anon_WITH_AES_128_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aA:LX/64b;

    .line 1044909
    const-string v0, "TLS_ECDH_anon_WITH_AES_256_CBC_SHA"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aB:LX/64b;

    .line 1044910
    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aC:LX/64b;

    .line 1044911
    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aD:LX/64b;

    .line 1044912
    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aE:LX/64b;

    .line 1044913
    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aF:LX/64b;

    .line 1044914
    const-string v0, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aG:LX/64b;

    .line 1044915
    const-string v0, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aH:LX/64b;

    .line 1044916
    const-string v0, "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aI:LX/64b;

    .line 1044917
    const-string v0, "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aJ:LX/64b;

    .line 1044918
    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aK:LX/64b;

    .line 1044919
    const-string v0, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aL:LX/64b;

    .line 1044920
    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aM:LX/64b;

    .line 1044921
    const-string v0, "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aN:LX/64b;

    .line 1044922
    const-string v0, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aO:LX/64b;

    .line 1044923
    const-string v0, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aP:LX/64b;

    .line 1044924
    const-string v0, "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aQ:LX/64b;

    .line 1044925
    const-string v0, "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384"

    invoke-static {v0}, LX/64b;->a(Ljava/lang/String;)LX/64b;

    move-result-object v0

    sput-object v0, LX/64b;->aR:LX/64b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1044926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044927
    if-nez p1, :cond_0

    .line 1044928
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1044929
    :cond_0
    iput-object p1, p0, LX/64b;->aS:Ljava/lang/String;

    .line 1044930
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/64b;
    .locals 2

    .prologue
    .line 1044931
    sget-object v0, LX/64b;->aT:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/64b;

    .line 1044932
    if-nez v0, :cond_0

    .line 1044933
    new-instance v1, LX/64b;

    invoke-direct {v1, p0}, LX/64b;-><init>(Ljava/lang/String;)V

    .line 1044934
    sget-object v0, LX/64b;->aT:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/64b;

    .line 1044935
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1044936
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1044937
    iget-object v0, p0, LX/64b;->aS:Ljava/lang/String;

    return-object v0
.end method
