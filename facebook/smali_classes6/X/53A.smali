.class public final LX/53A;
.super LX/0zZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0zZ",
        "<",
        "LX/0v9",
        "<+TT;>;>;"
    }
.end annotation


# static fields
.field public static final e:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "LX/53A;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicLongFieldUpdater",
            "<",
            "LX/53A;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0vH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vH",
            "<",
            "LX/0v9",
            "<+TT;>;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/537;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/537",
            "<TT;>;"
        }
    .end annotation
.end field

.field public volatile d:I

.field private final f:LX/0zZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zZ",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final g:LX/54m;

.field private volatile h:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 826681
    const-class v0, LX/53A;

    const-string v1, "d"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, LX/53A;->e:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    .line 826682
    const-class v0, LX/53A;

    const-string v1, "h"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, LX/53A;->i:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    return-void
.end method

.method public constructor <init>(LX/0zZ;LX/54m;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zZ",
            "<TT;>;",
            "LX/54m;",
            ")V"
        }
    .end annotation

    .prologue
    .line 826683
    invoke-direct {p0, p1}, LX/0zZ;-><init>(LX/0zZ;)V

    .line 826684
    sget-object v0, LX/0vH;->a:LX/0vH;

    move-object v0, v0

    .line 826685
    iput-object v0, p0, LX/53A;->a:LX/0vH;

    .line 826686
    iput-object p1, p0, LX/53A;->f:LX/0zZ;

    .line 826687
    iput-object p2, p0, LX/53A;->g:LX/54m;

    .line 826688
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LX/53A;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 826689
    new-instance v0, LX/539;

    invoke-direct {v0, p0}, LX/539;-><init>(LX/53A;)V

    invoke-static {v0}, LX/0ze;->a(LX/0vR;)LX/0za;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0zZ;->a(LX/0za;)V

    .line 826690
    return-void
.end method

.method public static g(LX/53A;)V
    .locals 6

    .prologue
    .line 826691
    iget-wide v0, p0, LX/53A;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 826692
    iget-object v0, p0, LX/53A;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    .line 826693
    invoke-static {v0}, LX/0vH;->b(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 826694
    iget-object v0, p0, LX/53A;->f:LX/0zZ;

    invoke-virtual {v0}, LX/0zZ;->R_()V

    .line 826695
    :cond_0
    :goto_0
    return-void

    .line 826696
    :cond_1
    if-eqz v0, :cond_0

    .line 826697
    sget-object v1, LX/0vH;->c:Ljava/lang/Object;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    move-object v0, v0

    .line 826698
    check-cast v0, LX/0v9;

    .line 826699
    new-instance v1, LX/537;

    iget-object v2, p0, LX/53A;->f:LX/0zZ;

    iget-wide v4, p0, LX/53A;->h:J

    invoke-direct {v1, p0, v2, v4, v5}, LX/537;-><init>(LX/53A;LX/0zZ;J)V

    iput-object v1, p0, LX/53A;->c:LX/537;

    .line 826700
    iget-object v1, p0, LX/53A;->g:LX/54m;

    iget-object v2, p0, LX/53A;->c:LX/537;

    invoke-virtual {v1, v2}, LX/54m;->a(LX/0za;)V

    .line 826701
    iget-object v1, p0, LX/53A;->c:LX/537;

    invoke-virtual {v0, v1}, LX/0v9;->a(LX/0zZ;)LX/0za;

    goto :goto_0

    .line 826702
    :cond_3
    iget-object v0, p0, LX/53A;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    .line 826703
    invoke-static {v0}, LX/0vH;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826704
    iget-object v0, p0, LX/53A;->f:LX/0zZ;

    invoke-virtual {v0}, LX/0zZ;->R_()V

    goto :goto_0
.end method


# virtual methods
.method public final R_()V
    .locals 2

    .prologue
    .line 826705
    iget-object v0, p0, LX/53A;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 826706
    sget-object v1, LX/0vH;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 826707
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 826708
    sget-object v0, LX/53A;->e:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->getAndIncrement(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    .line 826709
    invoke-static {p0}, LX/53A;->g(LX/53A;)V

    .line 826710
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 826711
    check-cast p1, LX/0v9;

    .line 826712
    iget-object v0, p0, LX/53A;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {p1}, LX/0vH;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 826713
    sget-object v0, LX/53A;->e:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->getAndIncrement(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    .line 826714
    invoke-static {p0}, LX/53A;->g(LX/53A;)V

    .line 826715
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 826716
    iget-object v0, p0, LX/53A;->f:LX/0zZ;

    invoke-virtual {v0, p1}, LX/0zZ;->a(Ljava/lang/Throwable;)V

    .line 826717
    invoke-virtual {p0}, LX/0zZ;->b()V

    .line 826718
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 826719
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, LX/0zZ;->a(J)V

    .line 826720
    return-void
.end method
