.class public LX/6N6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;
.implements LX/1MT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static final b:Ljava/lang/String;

.field private static volatile h:LX/6N6;


# instance fields
.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/3fr;

.field private final e:LX/03V;

.field private final f:LX/0W3;

.field public final g:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1082335
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "contacts_db_in_bug_report"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6N6;->a:LX/0Tn;

    .line 1082336
    const-class v0, LX/6N6;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6N6;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3fr;LX/03V;LX/0W3;LX/2RQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1082328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1082329
    iput-object p1, p0, LX/6N6;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1082330
    iput-object p2, p0, LX/6N6;->d:LX/3fr;

    .line 1082331
    iput-object p3, p0, LX/6N6;->e:LX/03V;

    .line 1082332
    iput-object p4, p0, LX/6N6;->f:LX/0W3;

    .line 1082333
    iput-object p5, p0, LX/6N6;->g:LX/2RQ;

    .line 1082334
    return-void
.end method

.method public static a(LX/0QB;)LX/6N6;
    .locals 9

    .prologue
    .line 1082315
    sget-object v0, LX/6N6;->h:LX/6N6;

    if-nez v0, :cond_1

    .line 1082316
    const-class v1, LX/6N6;

    monitor-enter v1

    .line 1082317
    :try_start_0
    sget-object v0, LX/6N6;->h:LX/6N6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1082318
    if-eqz v2, :cond_0

    .line 1082319
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1082320
    new-instance v3, LX/6N6;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v5

    check-cast v5, LX/3fr;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v7

    check-cast v7, LX/0W3;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v8

    check-cast v8, LX/2RQ;

    invoke-direct/range {v3 .. v8}, LX/6N6;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/3fr;LX/03V;LX/0W3;LX/2RQ;)V

    .line 1082321
    move-object v0, v3

    .line 1082322
    sput-object v0, LX/6N6;->h:LX/6N6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1082323
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1082324
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1082325
    :cond_1
    sget-object v0, LX/6N6;->h:LX/6N6;

    return-object v0

    .line 1082326
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1082327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/io/File;)Landroid/net/Uri;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1082304
    new-instance v0, Ljava/io/File;

    const-string v1, "contacts_db.txt"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1082305
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1082306
    :try_start_0
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 1082307
    iget-object v4, p0, LX/6N6;->d:LX/3fr;

    iget-object v5, p0, LX/6N6;->g:LX/2RQ;

    invoke-virtual {v5}, LX/2RQ;->a()LX/2RR;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v5

    .line 1082308
    :goto_0
    invoke-interface {v5}, LX/6N1;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1082309
    invoke-interface {v5}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/contacts/graphql/Contact;

    .line 1082310
    invoke-static {v4}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v6

    const-string v7, "name"

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v6

    const-string v7, "fbid"

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v6

    const-string v7, "pushable"

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/Contact;->r()LX/03R;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v6

    const-string v7, "inContactList"

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/Contact;->v()Z

    move-result p1

    invoke-virtual {v6, v7, p1}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v6

    const-string v7, "type"

    invoke-virtual {v4}, Lcom/facebook/contacts/graphql/Contact;->A()LX/2RU;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v4

    invoke-virtual {v4}, LX/237;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 1082311
    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 1082312
    :cond_0
    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    .line 1082313
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1082314
    invoke-static {v1, v3}, LX/1md;->a(Ljava/io/Closeable;Z)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1, v3}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1082289
    iget-object v0, p0, LX/6N6;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6N6;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1082290
    const/4 v0, 0x0

    .line 1082291
    :goto_0
    return-object v0

    .line 1082292
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, LX/6N6;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1082293
    const-string v1, "contacts_db.txt"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1082294
    :catch_0
    move-exception v0

    .line 1082295
    iget-object v1, p0, LX/6N6;->e:LX/03V;

    sget-object v2, LX/6N6;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1082296
    throw v0
.end method

.method public final getFilesFromWorkerThread(Ljava/io/File;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1082299
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1082300
    iget-object v1, p0, LX/6N6;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/6N6;->a:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1082301
    invoke-direct {p0, p1}, LX/6N6;->a(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1082302
    new-instance v2, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;

    const-string v3, "contacts_db.txt"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "text/plain"

    invoke-direct {v2, v3, v1, v4}, Lcom/facebook/reportaproblem/base/bugreport/file/BugReportFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1082303
    :cond_0
    return-object v0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 1082298
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 1082297
    iget-object v0, p0, LX/6N6;->f:LX/0W3;

    sget-wide v2, LX/0X5;->aZ:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
