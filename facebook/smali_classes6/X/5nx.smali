.class public final enum LX/5nx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5nx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5nx;

.field public static final enum Dismissed:LX/5nx;

.field public static final enum Seen:LX/5nx;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1004828
    new-instance v0, LX/5nx;

    const-string v1, "Seen"

    const-string v2, "seen"

    invoke-direct {v0, v1, v3, v2}, LX/5nx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nx;->Seen:LX/5nx;

    .line 1004829
    new-instance v0, LX/5nx;

    const-string v1, "Dismissed"

    const-string v2, "dismissed"

    invoke-direct {v0, v1, v4, v2}, LX/5nx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nx;->Dismissed:LX/5nx;

    .line 1004830
    const/4 v0, 0x2

    new-array v0, v0, [LX/5nx;

    sget-object v1, LX/5nx;->Seen:LX/5nx;

    aput-object v1, v0, v3

    sget-object v1, LX/5nx;->Dismissed:LX/5nx;

    aput-object v1, v0, v4

    sput-object v0, LX/5nx;->$VALUES:[LX/5nx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004831
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004832
    iput-object p3, p0, LX/5nx;->name:Ljava/lang/String;

    .line 1004833
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5nx;
    .locals 1

    .prologue
    .line 1004834
    const-class v0, LX/5nx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5nx;

    return-object v0
.end method

.method public static values()[LX/5nx;
    .locals 1

    .prologue
    .line 1004835
    sget-object v0, LX/5nx;->$VALUES:[LX/5nx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5nx;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004836
    iget-object v0, p0, LX/5nx;->name:Ljava/lang/String;

    return-object v0
.end method
