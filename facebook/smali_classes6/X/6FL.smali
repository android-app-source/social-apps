.class public LX/6FL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6FJ;

.field private final b:LX/6FE;


# direct methods
.method public constructor <init>(LX/6FJ;LX/6FE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1067678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067679
    iput-object p1, p0, LX/6FL;->a:LX/6FJ;

    .line 1067680
    iput-object p2, p0, LX/6FL;->b:LX/6FE;

    .line 1067681
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1067682
    const-string v0, "requestAuthorizedCredentials"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 7

    .prologue
    .line 1067683
    check-cast p1, Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;

    .line 1067684
    iget-object v0, p0, LX/6FL;->b:LX/6FE;

    .line 1067685
    iget-object v1, v0, LX/6FE;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    move-object v2, v1

    .line 1067686
    iget-object v0, p0, LX/6FL;->b:LX/6FE;

    invoke-virtual {v0}, LX/6FE;->c()V

    .line 1067687
    if-eqz v2, :cond_0

    invoke-static {v2}, LX/6FJ;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1067688
    :cond_0
    sget-object v0, LX/6Cy;->BROWSER_EXTENSION_MISSING_PAYMENT_METHOD:LX/6Cy;

    invoke-virtual {v0}, LX/6Cy;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1067689
    :goto_0
    return-void

    .line 1067690
    :cond_1
    iget-object v0, p0, LX/6FL;->a:LX/6FJ;

    sget-object v1, LX/6xg;->NMOR_BUSINESS_PLATFORM_COMMERCE:LX/6xg;

    .line 1067691
    const-string v3, "amount"

    invoke-virtual {p1, v3}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v3, v3

    .line 1067692
    const-string v4, "JS_BRIDGE_PAGE_ID"

    invoke-virtual {p1, v4}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v4, v4

    .line 1067693
    const/4 v5, 0x0

    new-instance v6, LX/6FK;

    invoke-direct {v6, p0, v2, p1}, LX/6FK;-><init>(LX/6FL;Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;)V

    invoke-virtual/range {v0 .. v6}, LX/6FJ;->a(LX/6xg;Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;Ljava/lang/String;LX/0m9;LX/6FF;)V

    goto :goto_0
.end method
