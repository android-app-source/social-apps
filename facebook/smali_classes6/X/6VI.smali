.class public LX/6VI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103931
    iput-object p1, p0, LX/6VI;->a:LX/0Zb;

    .line 1103932
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6VI;->b:Ljava/lang/String;

    .line 1103933
    return-void
.end method

.method public static a(LX/6VI;LX/6VH;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1103934
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "birthday_entry_in_feeds"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/6VI;->b:Ljava/lang/String;

    .line 1103935
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1103936
    move-object v0, v0

    .line 1103937
    const-string v1, "action"

    invoke-virtual {p1}, LX/6VH;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/6VI;
    .locals 2

    .prologue
    .line 1103938
    new-instance v1, LX/6VI;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/6VI;-><init>(LX/0Zb;)V

    .line 1103939
    return-object v1
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 1103940
    iget-object v0, p0, LX/6VI;->a:LX/0Zb;

    sget-object v1, LX/6VH;->CLICK_TO_COMPOSER:LX/6VH;

    invoke-static {p0, v1}, LX/6VI;->a(LX/6VI;LX/6VH;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1103941
    return-void
.end method
