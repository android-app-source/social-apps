.class public final LX/5My;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DialtonePhotoUnblockMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0yL;

.field public final synthetic b:Landroid/net/Uri;

.field public final synthetic c:LX/0yb;


# direct methods
.method public constructor <init>(LX/0yb;LX/0yL;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 905783
    iput-object p1, p0, LX/5My;->c:LX/0yb;

    iput-object p2, p0, LX/5My;->a:LX/0yL;

    iput-object p3, p0, LX/5My;->b:Landroid/net/Uri;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 905784
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    invoke-static {v0}, LX/0yb;->C(LX/0yb;)V

    .line 905785
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 905786
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x1

    .line 905787
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    iget-object v0, v0, LX/0yb;->A:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    .line 905788
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    iget-object v0, v0, LX/0yb;->A:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 905789
    :cond_0
    if-nez p1, :cond_1

    .line 905790
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    invoke-static {v0}, LX/0yb;->C(LX/0yb;)V

    .line 905791
    :goto_0
    return-void

    .line 905792
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 905793
    check-cast v0, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DialtonePhotoUnblockMutationModel;

    invoke-virtual {v0}, Lcom/facebook/dialtone/protocol/DialtoneGraphQLModels$DialtonePhotoUnblockMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 905794
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 905795
    if-nez v2, :cond_2

    .line 905796
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    invoke-static {v0}, LX/0yb;->C(LX/0yb;)V

    goto :goto_0

    .line 905797
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 905798
    :cond_2
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    iget-object v0, v0, LX/0yb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pl;

    invoke-virtual {v0, v1, v2}, LX/1pl;->a(LX/15i;I)V

    .line 905799
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    iget-object v0, v0, LX/0yb;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1pl;

    iget-object v1, p0, LX/5My;->a:LX/0yL;

    iget-object v2, p0, LX/5My;->b:Landroid/net/Uri;

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 905800
    invoke-virtual {v0, v2}, LX/1pl;->a(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 905801
    invoke-interface {v1, v5}, LX/0yL;->e_(Z)V

    .line 905802
    iget-object v6, v0, LX/1pl;->d:LX/0kL;

    new-instance v7, LX/27k;

    iget-object v8, v0, LX/1pl;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0f002b

    iget v10, v0, LX/1pl;->g:I

    new-array v11, v3, [Ljava/lang/Object;

    iget p1, v0, LX/1pl;->g:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v11, v5

    invoke-virtual {v8, v9, v10, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v7, v5}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v7}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 905803
    :goto_1
    move v0, v3

    .line 905804
    if-nez v0, :cond_3

    .line 905805
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    const-string v1, "dialtone_photocapping_upgrade_dialog_impression"

    invoke-static {v0, v1}, LX/0yb;->f(LX/0yb;Ljava/lang/String;)V

    .line 905806
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    invoke-virtual {v0, v4}, LX/0yc;->a(Z)Z

    goto :goto_0

    .line 905807
    :cond_3
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    invoke-static {v0, v4}, LX/0yb;->d(LX/0yb;Z)V

    .line 905808
    iget-object v0, p0, LX/5My;->c:LX/0yb;

    const-string v1, "dialtone_photocapping_image_reveal"

    invoke-static {v0, v1}, LX/0yb;->f(LX/0yb;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    move v3, v5

    goto :goto_1
.end method
