.class public final enum LX/6bI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6bI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6bI;

.field public static final enum Audio:LX/6bI;

.field public static final enum Mixed:LX/6bI;

.field public static final enum Video:LX/6bI;


# instance fields
.field private mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1113214
    new-instance v0, LX/6bI;

    const-string v1, "Audio"

    invoke-direct {v0, v1, v4, v2}, LX/6bI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6bI;->Audio:LX/6bI;

    .line 1113215
    new-instance v0, LX/6bI;

    const-string v1, "Video"

    invoke-direct {v0, v1, v2, v3}, LX/6bI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6bI;->Video:LX/6bI;

    .line 1113216
    new-instance v0, LX/6bI;

    const-string v1, "Mixed"

    invoke-direct {v0, v1, v3, v5}, LX/6bI;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/6bI;->Mixed:LX/6bI;

    .line 1113217
    new-array v0, v5, [LX/6bI;

    sget-object v1, LX/6bI;->Audio:LX/6bI;

    aput-object v1, v0, v4

    sget-object v1, LX/6bI;->Video:LX/6bI;

    aput-object v1, v0, v2

    sget-object v1, LX/6bI;->Mixed:LX/6bI;

    aput-object v1, v0, v3

    sput-object v0, LX/6bI;->$VALUES:[LX/6bI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1113218
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1113219
    iput p3, p0, LX/6bI;->mValue:I

    .line 1113220
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6bI;
    .locals 1

    .prologue
    .line 1113221
    const-class v0, LX/6bI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6bI;

    return-object v0
.end method

.method public static values()[LX/6bI;
    .locals 1

    .prologue
    .line 1113222
    sget-object v0, LX/6bI;->$VALUES:[LX/6bI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6bI;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 1113223
    iget v0, p0, LX/6bI;->mValue:I

    return v0
.end method
