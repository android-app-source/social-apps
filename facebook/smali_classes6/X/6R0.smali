.class public LX/6R0;
.super Lcom/facebook/directinstall/ui/InstallDialog;
.source ""


# instance fields
.field public a:Landroid/widget/ProgressBar;

.field public b:Landroid/widget/LinearLayout;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/resources/ui/FbButton;

.field public e:Landroid/widget/LinearLayout;

.field public f:Landroid/widget/ExpandableListView;

.field public g:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

.field private h:LX/6Qz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1088368
    invoke-direct {p0, p1}, Lcom/facebook/directinstall/ui/InstallDialog;-><init>(Landroid/content/Context;)V

    .line 1088369
    const/4 p1, 0x0

    const/4 v2, 0x0

    .line 1088370
    invoke-virtual {p0}, LX/6R0;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030428

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1088371
    invoke-virtual {p0, v1}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/view/View;)V

    .line 1088372
    const v0, 0x7f0d0cc5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, LX/6R0;->f:Landroid/widget/ExpandableListView;

    .line 1088373
    iget-object v0, p0, LX/6R0;->f:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 1088374
    iget-object v0, p0, LX/6R0;->f:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setChildIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 1088375
    iget-object v0, p0, LX/6R0;->f:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->setDividerHeight(I)V

    .line 1088376
    invoke-virtual {p0}, LX/6R0;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030429

    iget-object v3, p0, LX/6R0;->f:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2, v3, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/6R0;->e:Landroid/widget/LinearLayout;

    .line 1088377
    const v0, 0x7f0d0550

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    iput-object v0, p0, LX/6R0;->g:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    .line 1088378
    const v0, 0x7f0d0cc0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/6R0;->a:Landroid/widget/ProgressBar;

    .line 1088379
    const v0, 0x7f0d0cc1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/6R0;->b:Landroid/widget/LinearLayout;

    .line 1088380
    const v0, 0x7f0d0cc2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/6R0;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1088381
    const v0, 0x7f0d0cc3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/6R0;->d:Lcom/facebook/resources/ui/FbButton;

    .line 1088382
    sget-object v0, LX/6Qz;->LOADING:LX/6Qz;

    invoke-virtual {p0, v0}, LX/6R0;->a(LX/6Qz;)V

    .line 1088383
    return-void
.end method


# virtual methods
.method public final a(LX/6Qz;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1088390
    iget-object v0, p0, LX/6R0;->f:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    .line 1088391
    iget-object v0, p0, LX/6R0;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1088392
    iget-object v0, p0, LX/6R0;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1088393
    iput-object p1, p0, LX/6R0;->h:LX/6Qz;

    .line 1088394
    iget-object v0, p0, LX/6R0;->h:LX/6Qz;

    sget-object v1, LX/6Qz;->LOADING:LX/6Qz;

    if-ne v0, v1, :cond_1

    .line 1088395
    iget-object v0, p0, LX/6R0;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1088396
    :cond_0
    :goto_0
    return-void

    .line 1088397
    :cond_1
    iget-object v0, p0, LX/6R0;->h:LX/6Qz;

    sget-object v1, LX/6Qz;->LOADED:LX/6Qz;

    if-ne v0, v1, :cond_2

    .line 1088398
    iget-object v0, p0, LX/6R0;->f:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    goto :goto_0

    .line 1088399
    :cond_2
    iget-object v0, p0, LX/6R0;->h:LX/6Qz;

    sget-object v1, LX/6Qz;->FAILED:LX/6Qz;

    if-ne v0, v1, :cond_0

    .line 1088400
    iget-object v0, p0, LX/6R0;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1088384
    iget-object v0, p0, LX/6R0;->g:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    .line 1088385
    iget-object v1, v0, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->b:LX/4lJ;

    move-object v0, v1

    .line 1088386
    sget-object v1, LX/4lJ;->EXPANDED:LX/4lJ;

    if-ne v0, v1, :cond_0

    .line 1088387
    iget-object v0, p0, LX/6R0;->g:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    sget-object v1, LX/4lJ;->COLLAPSED:LX/4lJ;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setExpandState(LX/4lJ;)V

    .line 1088388
    :goto_0
    return-void

    .line 1088389
    :cond_0
    invoke-super {p0}, Lcom/facebook/directinstall/ui/InstallDialog;->onBackPressed()V

    goto :goto_0
.end method
