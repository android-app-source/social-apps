.class public LX/6IJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field public a:Landroid/hardware/SensorManager;

.field public b:Landroid/hardware/Sensor;

.field public c:Landroid/hardware/Sensor;

.field public d:Z

.field private e:[F

.field private f:[F

.field private final g:[F

.field private final h:[F

.field private final i:[F

.field public j:F

.field public k:F

.field public l:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v4, 0x9

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1073773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1073774
    iput-boolean v2, p0, LX/6IJ;->d:Z

    .line 1073775
    iput-object v0, p0, LX/6IJ;->e:[F

    .line 1073776
    iput-object v0, p0, LX/6IJ;->f:[F

    .line 1073777
    new-array v0, v4, [F

    iput-object v0, p0, LX/6IJ;->g:[F

    .line 1073778
    new-array v0, v4, [F

    iput-object v0, p0, LX/6IJ;->h:[F

    .line 1073779
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, LX/6IJ;->i:[F

    .line 1073780
    iput v3, p0, LX/6IJ;->j:F

    .line 1073781
    iput v3, p0, LX/6IJ;->k:F

    .line 1073782
    const-wide/16 v4, 0x0

    iput-wide v4, p0, LX/6IJ;->l:J

    .line 1073783
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, LX/6IJ;->a:Landroid/hardware/SensorManager;

    .line 1073784
    iget-object v0, p0, LX/6IJ;->a:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, LX/6IJ;->b:Landroid/hardware/Sensor;

    .line 1073785
    iget-object v0, p0, LX/6IJ;->a:Landroid/hardware/SensorManager;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, LX/6IJ;->c:Landroid/hardware/Sensor;

    .line 1073786
    iget-object v0, p0, LX/6IJ;->b:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6IJ;->c:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/6IJ;->d:Z

    .line 1073787
    return-void

    :cond_0
    move v0, v2

    .line 1073788
    goto :goto_0
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 1073789
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/high16 v6, 0x43340000    # 180.0f

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    .line 1073761
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    if-ne v0, v7, :cond_0

    .line 1073762
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iput-object v0, p0, LX/6IJ;->e:[F

    .line 1073763
    :cond_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    if-ne v0, v8, :cond_1

    .line 1073764
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    iput-object v0, p0, LX/6IJ;->f:[F

    .line 1073765
    :cond_1
    iget-object v0, p0, LX/6IJ;->e:[F

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/6IJ;->f:[F

    if-eqz v0, :cond_2

    .line 1073766
    iget-object v0, p0, LX/6IJ;->g:[F

    iget-object v1, p0, LX/6IJ;->h:[F

    iget-object v2, p0, LX/6IJ;->e:[F

    iget-object v3, p0, LX/6IJ;->f:[F

    invoke-static {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z

    move-result v0

    .line 1073767
    if-eqz v0, :cond_2

    .line 1073768
    iget-object v0, p0, LX/6IJ;->g:[F

    iget-object v1, p0, LX/6IJ;->i:[F

    invoke-static {v0, v1}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    .line 1073769
    iget-object v0, p0, LX/6IJ;->i:[F

    aget v0, v0, v7

    mul-float/2addr v0, v6

    float-to-double v0, v0

    div-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, LX/6IJ;->j:F

    .line 1073770
    iget-object v0, p0, LX/6IJ;->i:[F

    aget v0, v0, v8

    mul-float/2addr v0, v6

    float-to-double v0, v0

    div-double/2addr v0, v4

    double-to-float v0, v0

    iput v0, p0, LX/6IJ;->k:F

    .line 1073771
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/6IJ;->l:J

    .line 1073772
    :cond_2
    return-void
.end method
