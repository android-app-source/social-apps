.class public final LX/6M8;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/contacts/graphql/ContactGraphQLModels$AddContactModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1079479
    const-class v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$AddContactModel;

    const v0, 0x606959bd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "AddContact"

    const-string v6, "59442565a175226641e329c5cf3a87dc"

    const-string v7, "contact_create_noshim"

    const-string v8, "0"

    const-string v9, "10155069963476729"

    const/4 v10, 0x0

    .line 1079480
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1079481
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1079482
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1079483
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1079484
    sparse-switch v0, :sswitch_data_0

    .line 1079485
    :goto_0
    return-object p1

    .line 1079486
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1079487
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1079488
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1079489
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1079490
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x719ba5ef -> :sswitch_2
        -0x55d248cb -> :sswitch_3
        -0x4e92d738 -> :sswitch_4
        0x5fb57ca -> :sswitch_0
        0x2956b75c -> :sswitch_1
    .end sparse-switch
.end method
