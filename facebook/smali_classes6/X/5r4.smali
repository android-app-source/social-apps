.class public final enum LX/5r4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5r4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5r4;

.field public static final enum DISPATCH_UI:LX/5r4;

.field public static final enum IDLE_EVENT:LX/5r4;

.field public static final enum NATIVE_ANIMATED_MODULE:LX/5r4;

.field public static final enum PERF_MARKERS:LX/5r4;

.field public static final enum TIMERS_EVENTS:LX/5r4;


# instance fields
.field private final mOrder:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1010850
    new-instance v0, LX/5r4;

    const-string v1, "PERF_MARKERS"

    invoke-direct {v0, v1, v2, v2}, LX/5r4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5r4;->PERF_MARKERS:LX/5r4;

    .line 1010851
    new-instance v0, LX/5r4;

    const-string v1, "DISPATCH_UI"

    invoke-direct {v0, v1, v3, v3}, LX/5r4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5r4;->DISPATCH_UI:LX/5r4;

    .line 1010852
    new-instance v0, LX/5r4;

    const-string v1, "NATIVE_ANIMATED_MODULE"

    invoke-direct {v0, v1, v4, v4}, LX/5r4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5r4;->NATIVE_ANIMATED_MODULE:LX/5r4;

    .line 1010853
    new-instance v0, LX/5r4;

    const-string v1, "TIMERS_EVENTS"

    invoke-direct {v0, v1, v5, v5}, LX/5r4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5r4;->TIMERS_EVENTS:LX/5r4;

    .line 1010854
    new-instance v0, LX/5r4;

    const-string v1, "IDLE_EVENT"

    invoke-direct {v0, v1, v6, v6}, LX/5r4;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5r4;->IDLE_EVENT:LX/5r4;

    .line 1010855
    const/4 v0, 0x5

    new-array v0, v0, [LX/5r4;

    sget-object v1, LX/5r4;->PERF_MARKERS:LX/5r4;

    aput-object v1, v0, v2

    sget-object v1, LX/5r4;->DISPATCH_UI:LX/5r4;

    aput-object v1, v0, v3

    sget-object v1, LX/5r4;->NATIVE_ANIMATED_MODULE:LX/5r4;

    aput-object v1, v0, v4

    sget-object v1, LX/5r4;->TIMERS_EVENTS:LX/5r4;

    aput-object v1, v0, v5

    sget-object v1, LX/5r4;->IDLE_EVENT:LX/5r4;

    aput-object v1, v0, v6

    sput-object v0, LX/5r4;->$VALUES:[LX/5r4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1010856
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1010857
    iput p3, p0, LX/5r4;->mOrder:I

    .line 1010858
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5r4;
    .locals 1

    .prologue
    .line 1010859
    const-class v0, LX/5r4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5r4;

    return-object v0
.end method

.method public static values()[LX/5r4;
    .locals 1

    .prologue
    .line 1010860
    sget-object v0, LX/5r4;->$VALUES:[LX/5r4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5r4;

    return-object v0
.end method


# virtual methods
.method public final getOrder()I
    .locals 1

    .prologue
    .line 1010861
    iget v0, p0, LX/5r4;->mOrder:I

    return v0
.end method
