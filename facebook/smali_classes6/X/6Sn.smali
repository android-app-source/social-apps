.class public final LX/6Sn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 1096629
    const/16 v28, 0x0

    .line 1096630
    const/16 v27, 0x0

    .line 1096631
    const/16 v26, 0x0

    .line 1096632
    const/16 v25, 0x0

    .line 1096633
    const/16 v24, 0x0

    .line 1096634
    const/16 v23, 0x0

    .line 1096635
    const/16 v22, 0x0

    .line 1096636
    const/16 v21, 0x0

    .line 1096637
    const/16 v20, 0x0

    .line 1096638
    const/16 v19, 0x0

    .line 1096639
    const/16 v18, 0x0

    .line 1096640
    const/16 v17, 0x0

    .line 1096641
    const/16 v16, 0x0

    .line 1096642
    const/4 v15, 0x0

    .line 1096643
    const/4 v14, 0x0

    .line 1096644
    const/4 v13, 0x0

    .line 1096645
    const/4 v12, 0x0

    .line 1096646
    const/4 v11, 0x0

    .line 1096647
    const/4 v10, 0x0

    .line 1096648
    const/4 v9, 0x0

    .line 1096649
    const/4 v8, 0x0

    .line 1096650
    const/4 v7, 0x0

    .line 1096651
    const/4 v6, 0x0

    .line 1096652
    const/4 v5, 0x0

    .line 1096653
    const/4 v4, 0x0

    .line 1096654
    const/4 v3, 0x0

    .line 1096655
    const/4 v2, 0x0

    .line 1096656
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1

    .line 1096657
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1096658
    const/4 v2, 0x0

    .line 1096659
    :goto_0
    return v2

    .line 1096660
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1096661
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_12

    .line 1096662
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v29

    .line 1096663
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1096664
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1

    if-eqz v29, :cond_1

    .line 1096665
    const-string v30, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    .line 1096666
    const/4 v11, 0x1

    .line 1096667
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto :goto_1

    .line 1096668
    :cond_2
    const-string v30, "can_see_voice_switcher"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_3

    .line 1096669
    const/4 v10, 0x1

    .line 1096670
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto :goto_1

    .line 1096671
    :cond_3
    const-string v30, "can_viewer_comment"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 1096672
    const/4 v9, 0x1

    .line 1096673
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto :goto_1

    .line 1096674
    :cond_4
    const-string v30, "can_viewer_comment_with_photo"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_5

    .line 1096675
    const/4 v8, 0x1

    .line 1096676
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto :goto_1

    .line 1096677
    :cond_5
    const-string v30, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_6

    .line 1096678
    const/4 v7, 0x1

    .line 1096679
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto :goto_1

    .line 1096680
    :cond_6
    const-string v30, "can_viewer_comment_with_video"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_7

    .line 1096681
    const/4 v6, 0x1

    .line 1096682
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto :goto_1

    .line 1096683
    :cond_7
    const-string v30, "can_viewer_like"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_8

    .line 1096684
    const/4 v5, 0x1

    .line 1096685
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 1096686
    :cond_8
    const-string v30, "can_viewer_subscribe"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 1096687
    const/4 v4, 0x1

    .line 1096688
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 1096689
    :cond_9
    const-string v30, "comments_mirroring_domain"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_a

    .line 1096690
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 1096691
    :cond_a
    const-string v30, "does_viewer_like"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 1096692
    const/4 v3, 0x1

    .line 1096693
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto/16 :goto_1

    .line 1096694
    :cond_b
    const-string v30, "id"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_c

    .line 1096695
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 1096696
    :cond_c
    const-string v30, "is_viewer_subscribed"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_d

    .line 1096697
    const/4 v2, 0x1

    .line 1096698
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 1096699
    :cond_d
    const-string v30, "legacy_api_post_id"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_e

    .line 1096700
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 1096701
    :cond_e
    const-string v30, "pinned_comment_events"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_f

    .line 1096702
    invoke-static/range {p0 .. p1}, LX/6TK;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1096703
    :cond_f
    const-string v30, "remixable_photo_uri"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_10

    .line 1096704
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 1096705
    :cond_10
    const-string v30, "video_timestamped_comments"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_11

    .line 1096706
    invoke-static/range {p0 .. p1}, LX/6TD;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1096707
    :cond_11
    const-string v30, "viewer_acts_as_page"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_0

    .line 1096708
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1096709
    :cond_12
    const/16 v29, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1096710
    if-eqz v11, :cond_13

    .line 1096711
    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 1096712
    :cond_13
    if-eqz v10, :cond_14

    .line 1096713
    const/4 v10, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1096714
    :cond_14
    if-eqz v9, :cond_15

    .line 1096715
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1096716
    :cond_15
    if-eqz v8, :cond_16

    .line 1096717
    const/4 v8, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1096718
    :cond_16
    if-eqz v7, :cond_17

    .line 1096719
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1096720
    :cond_17
    if-eqz v6, :cond_18

    .line 1096721
    const/4 v6, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1096722
    :cond_18
    if-eqz v5, :cond_19

    .line 1096723
    const/4 v5, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1096724
    :cond_19
    if-eqz v4, :cond_1a

    .line 1096725
    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1096726
    :cond_1a
    const/16 v4, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1096727
    if-eqz v3, :cond_1b

    .line 1096728
    const/16 v3, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1096729
    :cond_1b
    const/16 v3, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1096730
    if-eqz v2, :cond_1c

    .line 1096731
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1096732
    :cond_1c
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1096733
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1096734
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1096735
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1096736
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1096737
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1096558
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1096559
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096560
    if-eqz v0, :cond_0

    .line 1096561
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096562
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096563
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096564
    if-eqz v0, :cond_1

    .line 1096565
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096566
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096567
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096568
    if-eqz v0, :cond_2

    .line 1096569
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096570
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096571
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096572
    if-eqz v0, :cond_3

    .line 1096573
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096574
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096575
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096576
    if-eqz v0, :cond_4

    .line 1096577
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096578
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096579
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096580
    if-eqz v0, :cond_5

    .line 1096581
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096582
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096583
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096584
    if-eqz v0, :cond_6

    .line 1096585
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096586
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096587
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096588
    if-eqz v0, :cond_7

    .line 1096589
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096590
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096591
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096592
    if-eqz v0, :cond_8

    .line 1096593
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096594
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096595
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096596
    if-eqz v0, :cond_9

    .line 1096597
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096598
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096599
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096600
    if-eqz v0, :cond_a

    .line 1096601
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096602
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096603
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096604
    if-eqz v0, :cond_b

    .line 1096605
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096606
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096607
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096608
    if-eqz v0, :cond_c

    .line 1096609
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096610
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096611
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096612
    if-eqz v0, :cond_d

    .line 1096613
    const-string v1, "pinned_comment_events"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096614
    invoke-static {p0, v0, p2, p3}, LX/6TK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096615
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096616
    if-eqz v0, :cond_e

    .line 1096617
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096618
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096619
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096620
    if-eqz v0, :cond_f

    .line 1096621
    const-string v1, "video_timestamped_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096622
    invoke-static {p0, v0, p2, p3}, LX/6TD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096623
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096624
    if-eqz v0, :cond_10

    .line 1096625
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096626
    invoke-static {p0, v0, p2, p3}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096627
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1096628
    return-void
.end method
