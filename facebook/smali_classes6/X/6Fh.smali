.class public LX/6Fh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/0am;LX/0am;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1068769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1068770
    iput-object p1, p0, LX/6Fh;->a:LX/0am;

    .line 1068771
    iput-object p2, p0, LX/6Fh;->b:LX/0am;

    .line 1068772
    iput-object p3, p0, LX/6Fh;->c:LX/0am;

    .line 1068773
    return-void
.end method

.method public static a(ILjava/lang/String;)LX/6Fh;
    .locals 4

    .prologue
    .line 1068774
    new-instance v0, LX/6Fh;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/6Fh;-><init>(LX/0am;LX/0am;LX/0am;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/6Fh;
    .locals 4

    .prologue
    .line 1068775
    new-instance v0, LX/6Fh;

    invoke-static {p0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/6Fh;-><init>(LX/0am;LX/0am;LX/0am;)V

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1068776
    iget-object v0, p0, LX/6Fh;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1068777
    invoke-virtual {p0}, LX/6Fh;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1068778
    iget-object v0, p0, LX/6Fh;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1068779
    invoke-virtual {p0}, LX/6Fh;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1068780
    iget-object v0, p0, LX/6Fh;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 1068781
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1068782
    invoke-virtual {p0}, LX/6Fh;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1068783
    iget-object v0, p0, LX/6Fh;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 1068784
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
