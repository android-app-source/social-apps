.class public final enum LX/5TJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5TJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5TJ;

.field public static final enum AGENT_ITEM_SUGGESTION:LX/5TJ;

.field public static final enum CANCELLATION:LX/5TJ;

.field public static final enum PRODUCT_SUBSCRIPTION:LX/5TJ;

.field public static final enum RECEIPT:LX/5TJ;

.field public static final enum SHIPMENT:LX/5TJ;

.field public static final enum SHIPMENT_ETA:LX/5TJ;

.field public static final enum SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

.field public static final enum SHIPMENT_TRACKING_DELAYED:LX/5TJ;

.field public static final enum SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

.field public static final enum SHIPMENT_TRACKING_ETA:LX/5TJ;

.field public static final enum SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

.field public static final enum SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

.field public static final enum UNKNOWN:LX/5TJ;


# instance fields
.field private final mType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 921597
    new-instance v0, LX/5TJ;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->UNKNOWN:LX/5TJ;

    .line 921598
    new-instance v0, LX/5TJ;

    const-string v1, "RECEIPT"

    invoke-direct {v0, v1, v5, v5}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->RECEIPT:LX/5TJ;

    .line 921599
    new-instance v0, LX/5TJ;

    const-string v1, "CANCELLATION"

    invoke-direct {v0, v1, v6, v6}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->CANCELLATION:LX/5TJ;

    .line 921600
    new-instance v0, LX/5TJ;

    const-string v1, "SHIPMENT"

    invoke-direct {v0, v1, v7, v7}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->SHIPMENT:LX/5TJ;

    .line 921601
    new-instance v0, LX/5TJ;

    const-string v1, "SHIPMENT_TRACKING_ETA"

    invoke-direct {v0, v1, v8, v8}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    .line 921602
    new-instance v0, LX/5TJ;

    const-string v1, "SHIPMENT_TRACKING_IN_TRANSIT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    .line 921603
    new-instance v0, LX/5TJ;

    const-string v1, "SHIPMENT_TRACKING_OUT_FOR_DELIVERY"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    .line 921604
    new-instance v0, LX/5TJ;

    const-string v1, "SHIPMENT_TRACKING_DELAYED"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    .line 921605
    new-instance v0, LX/5TJ;

    const-string v1, "SHIPMENT_TRACKING_DELIVERED"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    .line 921606
    new-instance v0, LX/5TJ;

    const-string v1, "SHIPMENT_FOR_UNSUPPORTED_CARRIER"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    .line 921607
    new-instance v0, LX/5TJ;

    const-string v1, "SHIPMENT_ETA"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    .line 921608
    new-instance v0, LX/5TJ;

    const-string v1, "AGENT_ITEM_SUGGESTION"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->AGENT_ITEM_SUGGESTION:LX/5TJ;

    .line 921609
    new-instance v0, LX/5TJ;

    const-string v1, "PRODUCT_SUBSCRIPTION"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, LX/5TJ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/5TJ;->PRODUCT_SUBSCRIPTION:LX/5TJ;

    .line 921610
    const/16 v0, 0xd

    new-array v0, v0, [LX/5TJ;

    sget-object v1, LX/5TJ;->UNKNOWN:LX/5TJ;

    aput-object v1, v0, v4

    sget-object v1, LX/5TJ;->RECEIPT:LX/5TJ;

    aput-object v1, v0, v5

    sget-object v1, LX/5TJ;->CANCELLATION:LX/5TJ;

    aput-object v1, v0, v6

    sget-object v1, LX/5TJ;->SHIPMENT:LX/5TJ;

    aput-object v1, v0, v7

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/5TJ;->AGENT_ITEM_SUGGESTION:LX/5TJ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/5TJ;->PRODUCT_SUBSCRIPTION:LX/5TJ;

    aput-object v2, v0, v1

    sput-object v0, LX/5TJ;->$VALUES:[LX/5TJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 921594
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 921595
    iput p3, p0, LX/5TJ;->mType:I

    .line 921596
    return-void
.end method

.method public static getModelType(I)LX/5TJ;
    .locals 1

    .prologue
    .line 921537
    sget-object v0, LX/5TJ;->RECEIPT:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_0

    .line 921538
    sget-object v0, LX/5TJ;->RECEIPT:LX/5TJ;

    .line 921539
    :goto_0
    return-object v0

    .line 921540
    :cond_0
    sget-object v0, LX/5TJ;->CANCELLATION:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_1

    .line 921541
    sget-object v0, LX/5TJ;->CANCELLATION:LX/5TJ;

    goto :goto_0

    .line 921542
    :cond_1
    sget-object v0, LX/5TJ;->SHIPMENT:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_2

    .line 921543
    sget-object v0, LX/5TJ;->SHIPMENT:LX/5TJ;

    goto :goto_0

    .line 921544
    :cond_2
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_3

    .line 921545
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    goto :goto_0

    .line 921546
    :cond_3
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_4

    .line 921547
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    goto :goto_0

    .line 921548
    :cond_4
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_5

    .line 921549
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    goto :goto_0

    .line 921550
    :cond_5
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_6

    .line 921551
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    goto :goto_0

    .line 921552
    :cond_6
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_7

    .line 921553
    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    goto :goto_0

    .line 921554
    :cond_7
    sget-object v0, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_8

    .line 921555
    sget-object v0, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    goto :goto_0

    .line 921556
    :cond_8
    sget-object v0, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_9

    .line 921557
    sget-object v0, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    goto :goto_0

    .line 921558
    :cond_9
    sget-object v0, LX/5TJ;->AGENT_ITEM_SUGGESTION:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_a

    .line 921559
    sget-object v0, LX/5TJ;->AGENT_ITEM_SUGGESTION:LX/5TJ;

    goto :goto_0

    .line 921560
    :cond_a
    sget-object v0, LX/5TJ;->PRODUCT_SUBSCRIPTION:LX/5TJ;

    invoke-virtual {v0}, LX/5TJ;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_b

    .line 921561
    sget-object v0, LX/5TJ;->PRODUCT_SUBSCRIPTION:LX/5TJ;

    goto :goto_0

    .line 921562
    :cond_b
    sget-object v0, LX/5TJ;->UNKNOWN:LX/5TJ;

    goto :goto_0
.end method

.method public static isReceiptBubble(LX/5TJ;)Z
    .locals 1

    .prologue
    .line 921593
    sget-object v0, LX/5TJ;->RECEIPT:LX/5TJ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5TJ;->CANCELLATION:LX/5TJ;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isShippingBubble(LX/5TJ;)Z
    .locals 1

    .prologue
    .line 921592
    sget-object v0, LX/5TJ;->SHIPMENT:LX/5TJ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5TJ;
    .locals 1

    .prologue
    .line 921591
    const-class v0, LX/5TJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5TJ;

    return-object v0
.end method

.method public static values()[LX/5TJ;
    .locals 1

    .prologue
    .line 921590
    sget-object v0, LX/5TJ;->$VALUES:[LX/5TJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5TJ;

    return-object v0
.end method


# virtual methods
.method public final getTypeName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 921564
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->RECEIPT:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 921565
    const-string v0, "retail_receipt"

    .line 921566
    :goto_0
    return-object v0

    .line 921567
    :cond_0
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->CANCELLATION:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 921568
    const-string v0, "retail_cancellation"

    goto :goto_0

    .line 921569
    :cond_1
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->SHIPMENT:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 921570
    const-string v0, "retail_shipment"

    goto :goto_0

    .line 921571
    :cond_2
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_ETA:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 921572
    const-string v0, "retail_shipment_tracking_event"

    goto :goto_0

    .line 921573
    :cond_3
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_IN_TRANSIT:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 921574
    const-string v0, "retail_shipment_tracking_event"

    goto :goto_0

    .line 921575
    :cond_4
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_OUT_FOR_DELIVERY:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 921576
    const-string v0, "retail_shipment_tracking_event"

    goto :goto_0

    .line 921577
    :cond_5
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_DELAYED:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 921578
    const-string v0, "retail_shipment_tracking_event"

    goto :goto_0

    .line 921579
    :cond_6
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->SHIPMENT_TRACKING_DELIVERED:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_7

    .line 921580
    const-string v0, "retail_shipment_tracking_event"

    goto :goto_0

    .line 921581
    :cond_7
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->SHIPMENT_FOR_UNSUPPORTED_CARRIER:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_8

    .line 921582
    const-string v0, "retail_shipment"

    goto :goto_0

    .line 921583
    :cond_8
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->SHIPMENT_ETA:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_9

    .line 921584
    const-string v0, "retail_shipment"

    goto :goto_0

    .line 921585
    :cond_9
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->AGENT_ITEM_SUGGESTION:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_a

    .line 921586
    const-string v0, "retail_agent_item_suggestion"

    goto/16 :goto_0

    .line 921587
    :cond_a
    iget v0, p0, LX/5TJ;->mType:I

    sget-object v1, LX/5TJ;->PRODUCT_SUBSCRIPTION:LX/5TJ;

    invoke-virtual {v1}, LX/5TJ;->getValue()I

    move-result v1

    if-ne v0, v1, :cond_b

    .line 921588
    const-string v0, "retail_now_in_stock"

    goto/16 :goto_0

    .line 921589
    :cond_b
    const-string v0, "unknown"

    goto/16 :goto_0
.end method

.method public final getValue()I
    .locals 1

    .prologue
    .line 921563
    iget v0, p0, LX/5TJ;->mType:I

    return v0
.end method
