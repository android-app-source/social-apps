.class public final LX/5FG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 885007
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 885008
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885009
    :goto_0
    return v1

    .line 885010
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885011
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 885012
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 885013
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885014
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 885015
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 885016
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 885017
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 885018
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 885019
    const/4 v3, 0x0

    .line 885020
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 885021
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885022
    :goto_3
    move v2, v3

    .line 885023
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 885024
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 885025
    goto :goto_1

    .line 885026
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 885027
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 885028
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 885029
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885030
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 885031
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 885032
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885033
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 885034
    const-string v6, "icon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 885035
    const/4 v5, 0x0

    .line 885036
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 885037
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885038
    :goto_5
    move v4, v5

    .line 885039
    goto :goto_4

    .line 885040
    :cond_7
    const-string v6, "text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 885041
    const/4 v5, 0x0

    .line 885042
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_11

    .line 885043
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885044
    :goto_6
    move v2, v5

    .line 885045
    goto :goto_4

    .line 885046
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 885047
    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 885048
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 885049
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_9
    move v2, v3

    move v4, v3

    goto :goto_4

    .line 885050
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885051
    :cond_b
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 885052
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 885053
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885054
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_b

    if-eqz v6, :cond_b

    .line 885055
    const-string v7, "uri"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 885056
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_7

    .line 885057
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 885058
    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 885059
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_7

    .line 885060
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885061
    :cond_f
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_10

    .line 885062
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 885063
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885064
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_f

    if-eqz v6, :cond_f

    .line 885065
    const-string v7, "text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 885066
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_8

    .line 885067
    :cond_10
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 885068
    invoke-virtual {p1, v5, v2}, LX/186;->b(II)V

    .line 885069
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_6

    :cond_11
    move v2, v5

    goto :goto_8
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 885070
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885071
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885072
    if-eqz v0, :cond_1

    .line 885073
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885074
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 885075
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_0

    .line 885076
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2, p3}, LX/5FF;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 885077
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 885078
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 885079
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885080
    return-void
.end method
