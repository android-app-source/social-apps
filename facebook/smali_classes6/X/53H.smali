.class public final LX/53H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/52p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/52p;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicLongFieldUpdater",
            "<",
            "LX/53H;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/53J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53J",
            "<TT;>;"
        }
    .end annotation
.end field

.field public volatile c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 826847
    const-class v0, LX/53H;

    const-string v1, "c"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, LX/53H;->a:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    return-void
.end method

.method public constructor <init>(LX/53J;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/53J",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 826848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 826849
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/53H;->c:J

    .line 826850
    iput-object p1, p0, LX/53H;->b:LX/53J;

    .line 826851
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 5

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 826852
    iget-wide v0, p0, LX/53H;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 826853
    :cond_0
    :goto_0
    return-void

    .line 826854
    :cond_1
    cmp-long v0, p1, v2

    if-nez v0, :cond_2

    .line 826855
    iput-wide v2, p0, LX/53H;->c:J

    goto :goto_0

    .line 826856
    :cond_2
    sget-object v0, LX/53H;->a:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndAdd(Ljava/lang/Object;J)J

    .line 826857
    iget-object v0, p0, LX/53H;->b:LX/53J;

    invoke-static {v0}, LX/53J;->h(LX/53J;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826858
    const/4 v0, 0x0

    .line 826859
    iget-object v1, p0, LX/53H;->b:LX/53J;

    monitor-enter v1

    .line 826860
    :try_start_0
    iget-object v2, p0, LX/53H;->b:LX/53J;

    iget v2, v2, LX/53J;->f:I

    if-nez v2, :cond_3

    iget-object v2, p0, LX/53H;->b:LX/53J;

    iget-object v2, v2, LX/53J;->k:LX/53n;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/53H;->b:LX/53J;

    iget-object v2, v2, LX/53J;->k:LX/53n;

    invoke-virtual {v2}, LX/53n;->h()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 826861
    const/4 v0, 0x1

    .line 826862
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 826863
    if-eqz v0, :cond_0

    .line 826864
    iget-object v0, p0, LX/53H;->b:LX/53J;

    invoke-static {v0}, LX/53J;->k(LX/53J;)V

    goto :goto_0

    .line 826865
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
