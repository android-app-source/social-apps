.class public final LX/678;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65J;


# instance fields
.field public final synthetic a:LX/65f;

.field public final synthetic b:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(LX/65f;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 1052309
    iput-object p1, p0, LX/678;->a:LX/65f;

    iput-object p2, p0, LX/678;->b:Ljava/io/OutputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1052308
    iget-object v0, p0, LX/678;->a:LX/65f;

    return-object v0
.end method

.method public final a_(LX/672;J)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 1052310
    iget-wide v0, p1, LX/672;->b:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1052311
    :cond_0
    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_1

    .line 1052312
    iget-object v0, p0, LX/678;->a:LX/65f;

    invoke-virtual {v0}, LX/65f;->g()V

    .line 1052313
    iget-object v0, p1, LX/672;->a:LX/67F;

    .line 1052314
    iget v1, v0, LX/67F;->c:I

    iget v4, v0, LX/67F;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v1, v4

    .line 1052315
    iget-object v4, p0, LX/678;->b:Ljava/io/OutputStream;

    iget-object v5, v0, LX/67F;->a:[B

    iget v6, v0, LX/67F;->b:I

    invoke-virtual {v4, v5, v6, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 1052316
    iget v4, v0, LX/67F;->b:I

    add-int/2addr v4, v1

    iput v4, v0, LX/67F;->b:I

    .line 1052317
    int-to-long v4, v1

    sub-long/2addr p2, v4

    .line 1052318
    iget-wide v4, p1, LX/672;->b:J

    int-to-long v6, v1

    sub-long/2addr v4, v6

    iput-wide v4, p1, LX/672;->b:J

    .line 1052319
    iget v1, v0, LX/67F;->b:I

    iget v4, v0, LX/67F;->c:I

    if-ne v1, v4, :cond_0

    .line 1052320
    invoke-virtual {v0}, LX/67F;->a()LX/67F;

    move-result-object v1

    iput-object v1, p1, LX/672;->a:LX/67F;

    .line 1052321
    invoke-static {v0}, LX/67G;->a(LX/67F;)V

    goto :goto_0

    .line 1052322
    :cond_1
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1052306
    iget-object v0, p0, LX/678;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 1052307
    return-void
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 1052304
    iget-object v0, p0, LX/678;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 1052305
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1052303
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sink("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/678;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
