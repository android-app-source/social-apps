.class public abstract LX/68R;
.super LX/67m;
.source ""


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 0

    .prologue
    .line 1055201
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1055202
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 1055203
    invoke-virtual {p0}, LX/68R;->c()LX/31i;

    move-result-object v0

    .line 1055204
    iget-object v2, p0, LX/67m;->c:[F

    invoke-virtual {p0, v0, v2}, LX/67m;->a(LX/31i;[F)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1055205
    :cond_0
    :goto_0
    return-void

    .line 1055206
    :cond_1
    iget-object v0, p0, LX/67m;->c:[F

    aget v2, v0, v1

    .line 1055207
    iget-object v0, p0, LX/67m;->c:[F

    aget v0, v0, v8

    .line 1055208
    invoke-virtual {p0}, LX/68R;->p()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1055209
    iget-object v3, p0, LX/67m;->e:LX/680;

    .line 1055210
    iget-object v4, v3, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v3, v4

    .line 1055211
    invoke-virtual {p1, v8}, Landroid/graphics/Canvas;->save(I)I

    .line 1055212
    sub-float v4, v0, v2

    move v0, v1

    .line 1055213
    :goto_1
    int-to-float v5, v0

    cmpg-float v5, v5, v4

    if-gtz v5, :cond_4

    .line 1055214
    if-nez v0, :cond_3

    .line 1055215
    iget-object v5, p0, LX/67m;->c:[F

    iget-wide v6, v3, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v6, v6

    mul-float/2addr v6, v2

    aput v6, v5, v1

    .line 1055216
    iget-object v5, p0, LX/67m;->c:[F

    aput v9, v5, v8

    .line 1055217
    iget-object v5, v3, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget-object v6, p0, LX/67m;->c:[F

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->mapVectors([F)V

    .line 1055218
    :cond_2
    :goto_2
    iget-object v5, p0, LX/67m;->c:[F

    aget v5, v5, v1

    iget-object v6, p0, LX/67m;->c:[F

    aget v6, v6, v8

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1055219
    invoke-virtual {p0, p1}, LX/68R;->b(Landroid/graphics/Canvas;)V

    .line 1055220
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1055221
    :cond_3
    if-ne v0, v8, :cond_2

    .line 1055222
    iget-object v5, p0, LX/67m;->c:[F

    iget-wide v6, v3, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v6, v6

    aput v6, v5, v1

    .line 1055223
    iget-object v5, p0, LX/67m;->c:[F

    aput v9, v5, v8

    .line 1055224
    iget-object v5, v3, Lcom/facebook/android/maps/MapView;->k:Landroid/graphics/Matrix;

    iget-object v6, p0, LX/67m;->c:[F

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->mapVectors([F)V

    goto :goto_2

    .line 1055225
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public abstract b(Landroid/graphics/Canvas;)V
.end method

.method public abstract c()LX/31i;
.end method

.method public abstract p()Z
.end method
