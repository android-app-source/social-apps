.class public LX/69K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/69K;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0pn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058001
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1058002
    iput-object v0, p0, LX/69K;->a:LX/0Ot;

    .line 1058003
    return-void
.end method

.method public static a(LX/0QB;)LX/69K;
    .locals 4

    .prologue
    .line 1058004
    sget-object v0, LX/69K;->b:LX/69K;

    if-nez v0, :cond_1

    .line 1058005
    const-class v1, LX/69K;

    monitor-enter v1

    .line 1058006
    :try_start_0
    sget-object v0, LX/69K;->b:LX/69K;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1058007
    if-eqz v2, :cond_0

    .line 1058008
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1058009
    new-instance v3, LX/69K;

    invoke-direct {v3}, LX/69K;-><init>()V

    .line 1058010
    const/16 p0, 0xe8

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1058011
    iput-object p0, v3, LX/69K;->a:LX/0Ot;

    .line 1058012
    move-object v0, v3

    .line 1058013
    sput-object v0, LX/69K;->b:LX/69K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058014
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1058015
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1058016
    :cond_1
    sget-object v0, LX/69K;->b:LX/69K;

    return-object v0

    .line 1058017
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1058018
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
