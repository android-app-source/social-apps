.class public final LX/6GR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;)V
    .locals 0

    .prologue
    .line 1070486
    iput-object p1, p0, LX/6GR;->a:Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1070475
    iget-object v0, p0, LX/6GR;->a:Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;

    .line 1070476
    iget-object v1, v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->c:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_DID_SELECT_PRODUCT:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    .line 1070477
    iget-object v1, v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->d:LX/42n;

    if-eqz v1, :cond_1

    .line 1070478
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1070479
    iget-object v2, v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->b:LX/6GQ;

    invoke-virtual {v2, p3}, LX/6GQ;->a(I)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v2

    .line 1070480
    if-eqz v2, :cond_0

    .line 1070481
    const-string v3, "category_id"

    .line 1070482
    iget-wide v7, v2, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->c:J

    move-wide v5, v7

    .line 1070483
    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1070484
    :cond_0
    iget-object v2, v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryListFragment;->d:LX/42n;

    invoke-interface {v2, v0, v1}, LX/42n;->a(Lcom/facebook/base/fragment/NavigableFragment;Landroid/content/Intent;)V

    .line 1070485
    :cond_1
    return-void
.end method
