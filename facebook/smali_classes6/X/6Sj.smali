.class public final LX/6Sj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1096220
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1096221
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096222
    :goto_0
    return v1

    .line 1096223
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096224
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1096225
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1096226
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1096227
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1096228
    const-string v4, "auto_translated_message"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1096229
    const/4 v3, 0x0

    .line 1096230
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 1096231
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096232
    :goto_2
    move v2, v3

    .line 1096233
    goto :goto_1

    .line 1096234
    :cond_2
    const-string v4, "translation_type"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1096235
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTranslatabilityType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 1096236
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1096237
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1096238
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1096239
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1096240
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096241
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1096242
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1096243
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1096244
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1096245
    const-string v5, "text"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1096246
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_3

    .line 1096247
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1096248
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1096249
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v2, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1096250
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1096251
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096252
    if-eqz v0, :cond_1

    .line 1096253
    const-string v1, "auto_translated_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096254
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1096255
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1096256
    if-eqz v1, :cond_0

    .line 1096257
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096258
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096259
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1096260
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1096261
    if-eqz v0, :cond_2

    .line 1096262
    const-string v0, "translation_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096263
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096264
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1096265
    return-void
.end method
