.class public final LX/5bO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 958086
    const/16 v20, 0x0

    .line 958087
    const/16 v19, 0x0

    .line 958088
    const/16 v18, 0x0

    .line 958089
    const/16 v17, 0x0

    .line 958090
    const/16 v16, 0x0

    .line 958091
    const/4 v15, 0x0

    .line 958092
    const/4 v14, 0x0

    .line 958093
    const/4 v13, 0x0

    .line 958094
    const/4 v12, 0x0

    .line 958095
    const/4 v11, 0x0

    .line 958096
    const/4 v10, 0x0

    .line 958097
    const/4 v9, 0x0

    .line 958098
    const/4 v8, 0x0

    .line 958099
    const/4 v7, 0x0

    .line 958100
    const/4 v6, 0x0

    .line 958101
    const/4 v5, 0x0

    .line 958102
    const/4 v4, 0x0

    .line 958103
    const/4 v3, 0x0

    .line 958104
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    .line 958105
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 958106
    const/4 v3, 0x0

    .line 958107
    :goto_0
    return v3

    .line 958108
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 958109
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_e

    .line 958110
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v21

    .line 958111
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 958112
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    if-eqz v21, :cond_1

    .line 958113
    const-string v22, "__type__"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_2

    const-string v22, "__typename"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 958114
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v20

    goto :goto_1

    .line 958115
    :cond_3
    const-string v22, "height"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 958116
    const/4 v8, 0x1

    .line 958117
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto :goto_1

    .line 958118
    :cond_4
    const-string v22, "id"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 958119
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto :goto_1

    .line 958120
    :cond_5
    const-string v22, "image"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 958121
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 958122
    :cond_6
    const-string v22, "imageLarge"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 958123
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 958124
    :cond_7
    const-string v22, "imageNatural"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 958125
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 958126
    :cond_8
    const-string v22, "is_looping"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 958127
    const/4 v7, 0x1

    .line 958128
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 958129
    :cond_9
    const-string v22, "is_playable"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 958130
    const/4 v6, 0x1

    .line 958131
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 958132
    :cond_a
    const-string v22, "loop_count"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 958133
    const/4 v5, 0x1

    .line 958134
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto/16 :goto_1

    .line 958135
    :cond_b
    const-string v22, "playable_duration_in_ms"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 958136
    const/4 v4, 0x1

    .line 958137
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto/16 :goto_1

    .line 958138
    :cond_c
    const-string v22, "playable_url"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 958139
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 958140
    :cond_d
    const-string v22, "width"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 958141
    const/4 v3, 0x1

    .line 958142
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 958143
    :cond_e
    const/16 v21, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 958144
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 958145
    if-eqz v8, :cond_f

    .line 958146
    const/4 v8, 0x1

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v8, v1, v2}, LX/186;->a(III)V

    .line 958147
    :cond_f
    const/4 v8, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 958148
    const/4 v8, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 958149
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 958150
    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->b(II)V

    .line 958151
    if-eqz v7, :cond_10

    .line 958152
    const/4 v7, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->a(IZ)V

    .line 958153
    :cond_10
    if-eqz v6, :cond_11

    .line 958154
    const/4 v6, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->a(IZ)V

    .line 958155
    :cond_11
    if-eqz v5, :cond_12

    .line 958156
    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12, v6}, LX/186;->a(III)V

    .line 958157
    :cond_12
    if-eqz v4, :cond_13

    .line 958158
    const/16 v4, 0x9

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v5}, LX/186;->a(III)V

    .line 958159
    :cond_13
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 958160
    if-eqz v3, :cond_14

    .line 958161
    const/16 v3, 0xb

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 958162
    :cond_14
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 958163
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 958164
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 958165
    if-eqz v0, :cond_0

    .line 958166
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958167
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 958168
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 958169
    if-eqz v0, :cond_1

    .line 958170
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958171
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 958172
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958173
    if-eqz v0, :cond_2

    .line 958174
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958175
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958176
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958177
    if-eqz v0, :cond_3

    .line 958178
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958179
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 958180
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958181
    if-eqz v0, :cond_4

    .line 958182
    const-string v1, "imageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958183
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 958184
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958185
    if-eqz v0, :cond_5

    .line 958186
    const-string v1, "imageNatural"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958187
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 958188
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 958189
    if-eqz v0, :cond_6

    .line 958190
    const-string v1, "is_looping"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958191
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 958192
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 958193
    if-eqz v0, :cond_7

    .line 958194
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958195
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 958196
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 958197
    if-eqz v0, :cond_8

    .line 958198
    const-string v1, "loop_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958199
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 958200
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 958201
    if-eqz v0, :cond_9

    .line 958202
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958203
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 958204
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958205
    if-eqz v0, :cond_a

    .line 958206
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958207
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958208
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 958209
    if-eqz v0, :cond_b

    .line 958210
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958211
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 958212
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 958213
    return-void
.end method
