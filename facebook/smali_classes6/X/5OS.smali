.class public final enum LX/5OS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5OS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5OS;

.field public static final enum ABOVE:LX/5OS;

.field public static final enum BELOW:LX/5OS;

.field public static final enum NONE:LX/5OS;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 908819
    new-instance v0, LX/5OS;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/5OS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5OS;->NONE:LX/5OS;

    .line 908820
    new-instance v0, LX/5OS;

    const-string v1, "ABOVE"

    invoke-direct {v0, v1, v3}, LX/5OS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5OS;->ABOVE:LX/5OS;

    .line 908821
    new-instance v0, LX/5OS;

    const-string v1, "BELOW"

    invoke-direct {v0, v1, v4}, LX/5OS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5OS;->BELOW:LX/5OS;

    .line 908822
    const/4 v0, 0x3

    new-array v0, v0, [LX/5OS;

    sget-object v1, LX/5OS;->NONE:LX/5OS;

    aput-object v1, v0, v2

    sget-object v1, LX/5OS;->ABOVE:LX/5OS;

    aput-object v1, v0, v3

    sget-object v1, LX/5OS;->BELOW:LX/5OS;

    aput-object v1, v0, v4

    sput-object v0, LX/5OS;->$VALUES:[LX/5OS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 908816
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5OS;
    .locals 1

    .prologue
    .line 908818
    const-class v0, LX/5OS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5OS;

    return-object v0
.end method

.method public static values()[LX/5OS;
    .locals 1

    .prologue
    .line 908817
    sget-object v0, LX/5OS;->$VALUES:[LX/5OS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5OS;

    return-object v0
.end method
