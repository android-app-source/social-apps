.class public LX/6IF;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1073740
    const-class v0, LX/6IF;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6IF;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1073618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1073619
    return-void
.end method

.method public static a(FF)F
    .locals 3

    .prologue
    const/high16 v2, 0x43b40000    # 360.0f

    .line 1073735
    sub-float v0, p1, p0

    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 1073736
    sub-float/2addr p1, v2

    .line 1073737
    :cond_0
    :goto_0
    return p1

    .line 1073738
    :cond_1
    sub-float v0, p1, p0

    const/high16 v1, -0x3c790000    # -270.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 1073739
    add-float/2addr p1, v2

    goto :goto_0
.end method

.method public static a(III)I
    .locals 0

    .prologue
    .line 1073731
    if-le p0, p2, :cond_0

    .line 1073732
    :goto_0
    return p2

    .line 1073733
    :cond_0
    if-ge p0, p1, :cond_1

    move p2, p1

    goto :goto_0

    :cond_1
    move p2, p0

    .line 1073734
    goto :goto_0
.end method

.method public static a(Ljava/util/List;IILX/6ID;)Landroid/hardware/Camera$Size;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;II",
            "LX/6ID;",
            ")",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1073741
    sget-object v0, LX/6IE;->HIGH:LX/6IE;

    invoke-static {p0, p1, p2, p3, v0}, LX/6IF;->a(Ljava/util/List;IILX/6ID;LX/6IE;)Landroid/hardware/Camera$Size;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;IILX/6ID;LX/6IE;)Landroid/hardware/Camera$Size;
    .locals 13
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;II",
            "LX/6ID;",
            "LX/6IE;",
            ")",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1073701
    if-nez p0, :cond_1

    .line 1073702
    const/4 v5, 0x0

    .line 1073703
    :cond_0
    :goto_0
    return-object v5

    .line 1073704
    :cond_1
    sget-object v1, LX/6IE;->LOW:LX/6IE;

    move-object/from16 v0, p4

    if-ne v0, v1, :cond_5

    const/4 v1, 0x1

    move v9, v1

    .line 1073705
    :goto_1
    int-to-float v1, p2

    int-to-float v2, p1

    div-float v10, v1, v2

    .line 1073706
    if-eqz v9, :cond_6

    const v4, 0x7fffffff

    .line 1073707
    :goto_2
    if-eqz v9, :cond_7

    const v1, 0x7fffffff

    .line 1073708
    :goto_3
    const/4 v3, 0x0

    .line 1073709
    const/4 v2, 0x0

    .line 1073710
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move-object v5, v3

    move v6, v4

    move v4, v1

    :cond_2
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    .line 1073711
    iget v3, v1, Landroid/hardware/Camera$Size;->height:I

    int-to-float v3, v3

    iget v7, v1, Landroid/hardware/Camera$Size;->width:I

    int-to-float v7, v7

    div-float v8, v3, v7

    .line 1073712
    iget v3, v1, Landroid/hardware/Camera$Size;->width:I

    iget v7, v1, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v3, v7

    .line 1073713
    sget-object v7, LX/6ID;->SMALLER_THAN_OR_EQUAL_TO:LX/6ID;

    move-object/from16 v0, p3

    if-ne v0, v7, :cond_3

    iget v7, v1, Landroid/hardware/Camera$Size;->width:I

    if-gt v7, p1, :cond_2

    iget v7, v1, Landroid/hardware/Camera$Size;->height:I

    if-gt v7, p2, :cond_2

    .line 1073714
    :cond_3
    if-eqz v9, :cond_9

    if-ge v3, v6, :cond_8

    const/4 v7, 0x1

    .line 1073715
    :goto_5
    sub-float/2addr v8, v10

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    const v12, 0x3c23d70a    # 0.01f

    cmpg-float v8, v8, v12

    if-gtz v8, :cond_b

    const/4 v8, 0x1

    .line 1073716
    :goto_6
    if-eqz v7, :cond_4

    if-eqz v8, :cond_4

    move-object v5, v1

    move v6, v3

    .line 1073717
    :cond_4
    if-eqz v9, :cond_d

    if-ge v3, v4, :cond_c

    const/4 v7, 0x1

    .line 1073718
    :goto_7
    if-eqz v7, :cond_11

    move v2, v3

    :goto_8
    move v4, v2

    move-object v2, v1

    .line 1073719
    goto :goto_4

    .line 1073720
    :cond_5
    const/4 v1, 0x0

    move v9, v1

    goto :goto_1

    .line 1073721
    :cond_6
    const/4 v4, 0x0

    goto :goto_2

    .line 1073722
    :cond_7
    const/4 v1, 0x0

    goto :goto_3

    .line 1073723
    :cond_8
    const/4 v7, 0x0

    goto :goto_5

    :cond_9
    if-le v3, v6, :cond_a

    const/4 v7, 0x1

    goto :goto_5

    :cond_a
    const/4 v7, 0x0

    goto :goto_5

    .line 1073724
    :cond_b
    const/4 v8, 0x0

    goto :goto_6

    .line 1073725
    :cond_c
    const/4 v7, 0x0

    goto :goto_7

    :cond_d
    if-le v3, v4, :cond_e

    const/4 v7, 0x1

    goto :goto_7

    :cond_e
    const/4 v7, 0x0

    goto :goto_7

    .line 1073726
    :cond_f
    if-nez v5, :cond_0

    .line 1073727
    if-eqz v2, :cond_10

    .line 1073728
    sget-object v1, LX/6IF;->a:Ljava/lang/String;

    const-string v3, "Can not find a size that respects the desired proportions"

    invoke-static {v1, v3}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v2

    .line 1073729
    goto/16 :goto_0

    .line 1073730
    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_11
    move-object v1, v2

    move v2, v4

    goto :goto_8
.end method

.method public static a(Ljava/util/List;Landroid/graphics/Point;)Landroid/hardware/Camera$Size;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;",
            "Landroid/graphics/Point;",
            ")",
            "Landroid/hardware/Camera$Size;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1073698
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p1, Landroid/graphics/Point;->y:I

    .line 1073699
    sget-object p1, LX/6ID;->NO_CONSTRAINTS:LX/6ID;

    invoke-static {p0, v0, v1, p1}, LX/6IF;->a(Ljava/util/List;IILX/6ID;)Landroid/hardware/Camera$Size;

    move-result-object p1

    move-object v0, p1

    .line 1073700
    return-object v0
.end method

.method public static a(LX/6IG;LX/6IG;LX/6IL;LX/6IL;)V
    .locals 2

    .prologue
    .line 1073620
    sget-object v0, LX/6IB;->a:[I

    invoke-virtual {p0}, LX/6IG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1073621
    sget-object v0, LX/6IG;->LANDSCAPE:LX/6IG;

    if-ne p1, v0, :cond_0

    .line 1073622
    sget-object v0, LX/6IC;->TOP_RIGHT:LX/6IC;

    .line 1073623
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073624
    sget-object v0, LX/6IC;->BOTTOM_RIGHT:LX/6IC;

    .line 1073625
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073626
    :goto_0
    return-void

    .line 1073627
    :cond_0
    sget-object v0, LX/6IG;->PORTRAIT:LX/6IG;

    if-eq p1, v0, :cond_8

    .line 1073628
    sget-object v0, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    if-ne p1, v0, :cond_1

    .line 1073629
    sget-object v0, LX/6IC;->BOTTOM_LEFT:LX/6IC;

    .line 1073630
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073631
    sget-object v0, LX/6IC;->TOP_LEFT:LX/6IC;

    .line 1073632
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073633
    goto :goto_0

    .line 1073634
    :cond_1
    sget-object v0, LX/6IC;->TOP_RIGHT:LX/6IC;

    .line 1073635
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073636
    sget-object v0, LX/6IC;->TOP_LEFT:LX/6IC;

    .line 1073637
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073638
    goto :goto_0

    .line 1073639
    :pswitch_0
    sget-object v0, LX/6IG;->LANDSCAPE:LX/6IG;

    if-eq p1, v0, :cond_8

    .line 1073640
    sget-object v0, LX/6IG;->PORTRAIT:LX/6IG;

    if-ne p1, v0, :cond_2

    .line 1073641
    sget-object v0, LX/6IC;->BOTTOM_LEFT:LX/6IC;

    .line 1073642
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073643
    sget-object v0, LX/6IC;->TOP_LEFT:LX/6IC;

    .line 1073644
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073645
    goto :goto_0

    .line 1073646
    :cond_2
    sget-object v0, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    if-ne p1, v0, :cond_3

    .line 1073647
    sget-object v0, LX/6IC;->BOTTOM_RIGHT:LX/6IC;

    .line 1073648
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073649
    sget-object v0, LX/6IC;->BOTTOM_LEFT:LX/6IC;

    .line 1073650
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073651
    goto :goto_0

    .line 1073652
    :cond_3
    sget-object v0, LX/6IC;->TOP_LEFT:LX/6IC;

    .line 1073653
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073654
    sget-object v0, LX/6IC;->BOTTOM_LEFT:LX/6IC;

    .line 1073655
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073656
    goto :goto_0

    .line 1073657
    :pswitch_1
    sget-object v0, LX/6IG;->LANDSCAPE:LX/6IG;

    if-ne p1, v0, :cond_4

    .line 1073658
    sget-object v0, LX/6IC;->BOTTOM_RIGHT:LX/6IC;

    .line 1073659
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073660
    sget-object v0, LX/6IC;->BOTTOM_LEFT:LX/6IC;

    .line 1073661
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073662
    goto :goto_0

    .line 1073663
    :cond_4
    sget-object v0, LX/6IG;->PORTRAIT:LX/6IG;

    if-ne p1, v0, :cond_5

    .line 1073664
    sget-object v0, LX/6IC;->TOP_RIGHT:LX/6IC;

    .line 1073665
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073666
    sget-object v0, LX/6IC;->BOTTOM_RIGHT:LX/6IC;

    .line 1073667
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073668
    goto :goto_0

    .line 1073669
    :cond_5
    sget-object v0, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    if-eq p1, v0, :cond_8

    .line 1073670
    sget-object v0, LX/6IC;->BOTTOM_RIGHT:LX/6IC;

    .line 1073671
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073672
    sget-object v0, LX/6IC;->TOP_RIGHT:LX/6IC;

    .line 1073673
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073674
    goto :goto_0

    .line 1073675
    :pswitch_2
    sget-object v0, LX/6IG;->LANDSCAPE:LX/6IG;

    if-ne p1, v0, :cond_6

    .line 1073676
    sget-object v0, LX/6IC;->BOTTOM_LEFT:LX/6IC;

    .line 1073677
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073678
    sget-object v0, LX/6IC;->TOP_LEFT:LX/6IC;

    .line 1073679
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073680
    goto :goto_0

    .line 1073681
    :cond_6
    sget-object v0, LX/6IG;->PORTRAIT:LX/6IG;

    if-ne p1, v0, :cond_7

    .line 1073682
    sget-object v0, LX/6IC;->TOP_RIGHT:LX/6IC;

    .line 1073683
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073684
    sget-object v0, LX/6IC;->TOP_LEFT:LX/6IC;

    .line 1073685
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073686
    goto/16 :goto_0

    .line 1073687
    :cond_7
    sget-object v0, LX/6IG;->REVERSE_LANDSCAPE:LX/6IG;

    if-ne p1, v0, :cond_8

    .line 1073688
    sget-object v0, LX/6IC;->TOP_RIGHT:LX/6IC;

    .line 1073689
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073690
    sget-object v0, LX/6IC;->BOTTOM_RIGHT:LX/6IC;

    .line 1073691
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073692
    goto/16 :goto_0

    .line 1073693
    :cond_8
    sget-object v0, LX/6IC;->TOP_LEFT:LX/6IC;

    .line 1073694
    iput-object v0, p3, LX/6IL;->c:LX/6IC;

    .line 1073695
    sget-object v0, LX/6IC;->TOP_RIGHT:LX/6IC;

    .line 1073696
    iput-object v0, p2, LX/6IL;->c:LX/6IC;

    .line 1073697
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/app/Activity;Landroid/view/View;Landroid/widget/RelativeLayout;Landroid/widget/RelativeLayout;Landroid/graphics/Point;Landroid/graphics/Rect;III)Z
    .locals 12

    .prologue
    .line 1073533
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 1073534
    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v6

    .line 1073535
    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v7

    .line 1073536
    invoke-virtual {p3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073537
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v2, :cond_5

    .line 1073538
    if-ge v7, v6, :cond_0

    .line 1073539
    const-string v1, "CameraActivity"

    const-string v2, "resizePreview: vertical layout, wide screen. Aborting resize."

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073540
    const/4 v1, 0x0

    .line 1073541
    :goto_0
    return v1

    .line 1073542
    :cond_0
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v2, v6

    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Point;->y:I

    div-int/2addr v2, v3

    .line 1073543
    sub-int v5, v7, v2

    .line 1073544
    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1073545
    rsub-int/lit8 v2, p6, 0x64

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    .line 1073546
    if-ge v5, v2, :cond_2

    .line 1073547
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1073548
    sub-int v1, v7, v2

    .line 1073549
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v1, v2

    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Point;->x:I

    div-int v3, v1, v2

    .line 1073550
    const v1, 0x7f0d0869

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1073551
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073552
    sub-int v2, v6, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1073553
    const v2, 0x7f0d086a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1073554
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073555
    sub-int v3, v6, v3

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    sub-int v1, v3, v1

    iput v1, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1073556
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 1073557
    const/4 v1, 0x1

    goto :goto_0

    .line 1073558
    :cond_2
    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1073559
    add-int/lit8 v2, p8, 0x64

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    .line 1073560
    if-le v4, v2, :cond_b

    .line 1073561
    sub-int v4, v7, v2

    .line 1073562
    move-object/from16 v0, p4

    iget v5, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v5, v6

    add-int/lit8 v8, p7, 0x64

    mul-int/2addr v5, v8

    move-object/from16 v0, p4

    iget v8, v0, Landroid/graphics/Point;->y:I

    mul-int/lit8 v8, v8, 0x64

    div-int/2addr v5, v8

    .line 1073563
    if-le v5, v4, :cond_3

    .line 1073564
    sub-int v3, v7, v4

    move v11, v2

    move v2, v4

    move v4, v3

    move v3, v11

    .line 1073565
    :goto_2
    move-object/from16 v0, p4

    iget v5, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v2, v5

    move-object/from16 v0, p4

    iget v5, v0, Landroid/graphics/Point;->x:I

    div-int v5, v2, v5

    .line 1073566
    invoke-virtual {p2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073567
    sub-int v7, v5, v6

    div-int/lit8 v7, v7, 0x2

    .line 1073568
    sub-int/2addr v5, v6

    sub-int/2addr v5, v7

    .line 1073569
    neg-int v6, v7

    const/4 v8, 0x0

    neg-int v9, v5

    const/4 v10, 0x0

    invoke-virtual {v2, v6, v8, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1073570
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v7

    move-object/from16 v0, p5

    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 1073571
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v5

    move-object/from16 v0, p5

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 1073572
    :goto_3
    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1073573
    if-ge v3, v4, :cond_1

    .line 1073574
    const v1, 0x7f0d0868

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1073575
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073576
    sub-int v2, v4, v3

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    goto :goto_1

    .line 1073577
    :cond_3
    sub-int v4, v7, v5

    .line 1073578
    mul-int/lit8 v2, v3, 0x2

    if-le v4, v2, :cond_4

    div-int/lit8 v2, v4, 0x2

    move v3, v2

    move v2, v5

    goto :goto_2

    :cond_4
    move v2, v5

    goto :goto_2

    .line 1073579
    :cond_5
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lez v2, :cond_1

    .line 1073580
    if-le v7, v6, :cond_6

    .line 1073581
    const-string v1, "CameraActivity"

    const-string v2, "resizePreview: horizontal layout, tall screen. Aborting resize."

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1073582
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1073583
    :cond_6
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v2, v7

    move-object/from16 v0, p4

    iget v3, v0, Landroid/graphics/Point;->y:I

    div-int/2addr v2, v3

    .line 1073584
    sub-int v5, v6, v2

    .line 1073585
    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1073586
    rsub-int/lit8 v2, p6, 0x64

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    .line 1073587
    if-ge v5, v2, :cond_7

    .line 1073588
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1073589
    sub-int v1, v6, v2

    .line 1073590
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v1, v2

    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Point;->x:I

    div-int v3, v1, v2

    .line 1073591
    const v1, 0x7f0d0869

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1073592
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073593
    sub-int v2, v7, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1073594
    const v2, 0x7f0d086a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1073595
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073596
    sub-int v3, v7, v3

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    sub-int v1, v3, v1

    iput v1, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    goto/16 :goto_1

    .line 1073597
    :cond_7
    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1073598
    add-int/lit8 v2, p8, 0x64

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    .line 1073599
    if-le v4, v2, :cond_a

    .line 1073600
    sub-int v4, v6, v2

    .line 1073601
    move-object/from16 v0, p4

    iget v5, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v5, v7

    add-int/lit8 v8, p7, 0x64

    mul-int/2addr v5, v8

    move-object/from16 v0, p4

    iget v8, v0, Landroid/graphics/Point;->y:I

    mul-int/lit8 v8, v8, 0x64

    div-int/2addr v5, v8

    .line 1073602
    if-le v5, v4, :cond_8

    .line 1073603
    sub-int v3, v6, v4

    move v11, v2

    move v2, v4

    move v4, v3

    move v3, v11

    .line 1073604
    :goto_4
    move-object/from16 v0, p4

    iget v5, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v2, v5

    move-object/from16 v0, p4

    iget v5, v0, Landroid/graphics/Point;->x:I

    div-int v5, v2, v5

    .line 1073605
    invoke-virtual {p2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073606
    sub-int v6, v5, v7

    div-int/lit8 v6, v6, 0x2

    .line 1073607
    sub-int/2addr v5, v7

    sub-int/2addr v5, v6

    .line 1073608
    const/4 v7, 0x0

    neg-int v8, v6

    const/4 v9, 0x0

    neg-int v10, v5

    invoke-virtual {v2, v7, v8, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1073609
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v6

    move-object/from16 v0, p5

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 1073610
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v5

    move-object/from16 v0, p5

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1073611
    :goto_5
    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1073612
    if-ge v3, v4, :cond_1

    .line 1073613
    const v1, 0x7f0d0868

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1073614
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073615
    sub-int v2, v4, v3

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    goto/16 :goto_1

    .line 1073616
    :cond_8
    sub-int v4, v6, v5

    .line 1073617
    mul-int/lit8 v2, v3, 0x2

    if-le v4, v2, :cond_9

    div-int/lit8 v2, v4, 0x2

    move v3, v2

    move v2, v5

    goto :goto_4

    :cond_9
    move v2, v5

    goto :goto_4

    :cond_a
    move v3, v4

    move v4, v5

    goto :goto_5

    :cond_b
    move v3, v4

    move v4, v5

    goto/16 :goto_3
.end method
