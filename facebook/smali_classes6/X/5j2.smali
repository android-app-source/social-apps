.class public final LX/5j2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 985456
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v3, :cond_7

    .line 985457
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 985458
    :goto_0
    return v0

    .line 985459
    :cond_0
    const-string v12, "text_box_height"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 985460
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v7, v1

    .line 985461
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_4

    .line 985462
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 985463
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 985464
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 985465
    const-string v12, "horizontal_alignment_within_box"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 985466
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 985467
    :cond_2
    const-string v12, "text_box_width"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 985468
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v8

    move v6, v1

    goto :goto_1

    .line 985469
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 985470
    :cond_4
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 985471
    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 985472
    if-eqz v7, :cond_5

    move-object v0, p1

    .line 985473
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 985474
    :cond_5
    if-eqz v6, :cond_6

    .line 985475
    const/4 v1, 0x2

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 985476
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :cond_7
    move v6, v0

    move v7, v0

    move-wide v8, v4

    move-wide v2, v4

    move v10, v0

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 985441
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 985442
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 985443
    if-eqz v0, :cond_0

    .line 985444
    const-string v0, "horizontal_alignment_within_box"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985445
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 985446
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 985447
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 985448
    const-string v2, "text_box_height"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985449
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 985450
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 985451
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 985452
    const-string v2, "text_box_width"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985453
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 985454
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 985455
    return-void
.end method
