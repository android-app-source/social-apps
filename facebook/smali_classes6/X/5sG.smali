.class public LX/5sG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1012669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(ILX/5sD;)LX/5pD;
    .locals 14

    .prologue
    .line 1012670
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v1

    .line 1012671
    invoke-virtual {p1}, LX/5sD;->j()Landroid/view/MotionEvent;

    move-result-object v2

    .line 1012672
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1012673
    iget v3, p1, LX/5sD;->e:F

    move v3, v3

    .line 1012674
    sub-float v3, v0, v3

    .line 1012675
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 1012676
    iget v4, p1, LX/5sD;->f:F

    move v4, v4

    .line 1012677
    sub-float v4, v0, v4

    .line 1012678
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 1012679
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v5

    .line 1012680
    const-string v6, "pageX"

    invoke-virtual {v2, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    invoke-static {v7}, LX/5r2;->c(F)F

    move-result v7

    float-to-double v8, v7

    invoke-interface {v5, v6, v8, v9}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1012681
    const-string v6, "pageY"

    invoke-virtual {v2, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    invoke-static {v7}, LX/5r2;->c(F)F

    move-result v7

    float-to-double v8, v7

    invoke-interface {v5, v6, v8, v9}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1012682
    invoke-virtual {v2, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    sub-float/2addr v6, v3

    .line 1012683
    invoke-virtual {v2, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    sub-float/2addr v7, v4

    .line 1012684
    const-string v8, "locationX"

    invoke-static {v6}, LX/5r2;->c(F)F

    move-result v6

    float-to-double v10, v6

    invoke-interface {v5, v8, v10, v11}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1012685
    const-string v6, "locationY"

    invoke-static {v7}, LX/5r2;->c(F)F

    move-result v7

    float-to-double v8, v7

    invoke-interface {v5, v6, v8, v9}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1012686
    const-string v6, "target"

    invoke-interface {v5, v6, p0}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 1012687
    const-string v6, "timestamp"

    .line 1012688
    iget-wide v12, p1, LX/5r0;->d:J

    move-wide v8, v12

    .line 1012689
    long-to-double v8, v8

    invoke-interface {v5, v6, v8, v9}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1012690
    const-string v6, "identifier"

    invoke-virtual {v2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    int-to-double v8, v7

    invoke-interface {v5, v6, v8, v9}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 1012691
    invoke-interface {v1, v5}, LX/5pD;->a(LX/5pH;)V

    .line 1012692
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1012693
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;LX/5sF;ILX/5sD;)V
    .locals 5

    .prologue
    .line 1012694
    invoke-static {p2, p3}, LX/5sG;->a(ILX/5sD;)LX/5pD;

    move-result-object v1

    .line 1012695
    invoke-virtual {p3}, LX/5sD;->j()Landroid/view/MotionEvent;

    move-result-object v2

    .line 1012696
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v3

    .line 1012697
    sget-object v0, LX/5sF;->MOVE:LX/5sF;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/5sF;->CANCEL:LX/5sF;

    if-ne p1, v0, :cond_1

    .line 1012698
    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 1012699
    invoke-interface {v3, v0}, LX/5pD;->pushInt(I)V

    .line 1012700
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1012701
    :cond_1
    sget-object v0, LX/5sF;->START:LX/5sF;

    if-eq p1, v0, :cond_2

    sget-object v0, LX/5sF;->END:LX/5sF;

    if-ne p1, v0, :cond_4

    .line 1012702
    :cond_2
    invoke-virtual {v2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-interface {v3, v0}, LX/5pD;->pushInt(I)V

    .line 1012703
    :cond_3
    invoke-virtual {p1}, LX/5sF;->getJSEventName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0, v1, v3}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveTouches(Ljava/lang/String;LX/5pD;LX/5pD;)V

    .line 1012704
    return-void

    .line 1012705
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown touch type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
