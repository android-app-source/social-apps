.class public final enum LX/6JF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6JF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6JF;

.field public static final enum BACK:LX/6JF;

.field public static final enum FRONT:LX/6JF;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1075405
    new-instance v0, LX/6JF;

    const-string v1, "FRONT"

    invoke-direct {v0, v1, v2}, LX/6JF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6JF;->FRONT:LX/6JF;

    .line 1075406
    new-instance v0, LX/6JF;

    const-string v1, "BACK"

    invoke-direct {v0, v1, v3}, LX/6JF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6JF;->BACK:LX/6JF;

    .line 1075407
    const/4 v0, 0x2

    new-array v0, v0, [LX/6JF;

    sget-object v1, LX/6JF;->FRONT:LX/6JF;

    aput-object v1, v0, v2

    sget-object v1, LX/6JF;->BACK:LX/6JF;

    aput-object v1, v0, v3

    sput-object v0, LX/6JF;->$VALUES:[LX/6JF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1075408
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6JF;
    .locals 1

    .prologue
    .line 1075409
    const-class v0, LX/6JF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6JF;

    return-object v0
.end method

.method public static values()[LX/6JF;
    .locals 1

    .prologue
    .line 1075410
    sget-object v0, LX/6JF;->$VALUES:[LX/6JF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6JF;

    return-object v0
.end method


# virtual methods
.method public final flip()LX/6JF;
    .locals 1

    .prologue
    .line 1075411
    sget-object v0, LX/6JF;->FRONT:LX/6JF;

    if-ne p0, v0, :cond_0

    sget-object v0, LX/6JF;->BACK:LX/6JF;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/6JF;->FRONT:LX/6JF;

    goto :goto_0
.end method
