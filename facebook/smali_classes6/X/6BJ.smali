.class public LX/6BJ;
.super LX/0uk;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0uk",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0sZ;


# direct methods
.method public constructor <init>(LX/0sZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1061834
    invoke-direct {p0}, LX/0uk;-><init>()V

    .line 1061835
    iput-object p1, p0, LX/6BJ;->a:LX/0sZ;

    .line 1061836
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0zO;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1061837
    new-instance v0, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;

    const/16 v2, 0x19

    sget-object v3, LX/21x;->BASE:LX/21x;

    sget-object v4, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v5, LX/21y;->DEFAULT_ORDER:LX/21y;

    move-object v1, p1

    move-object v7, v6

    move v10, v9

    move v11, v8

    invoke-direct/range {v0 .. v11}, Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;-><init>(Ljava/lang/String;ILX/21x;LX/0rS;LX/21y;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    .line 1061838
    iget-object v1, p0, LX/6BJ;->a:LX/0sZ;

    invoke-virtual {v1, v0}, LX/0sZ;->a(Lcom/facebook/api/ufiservices/common/FetchFeedbackParams;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1061839
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1061840
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1061841
    const-string v0, "feedback_background_sync"

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1061842
    const/4 v0, 0x1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1061843
    const/16 v0, 0xa

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1061844
    const/4 v0, 0x1

    return v0
.end method
