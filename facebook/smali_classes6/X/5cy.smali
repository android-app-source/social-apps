.class public final LX/5cy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 964419
    const/4 v13, 0x0

    .line 964420
    const/4 v12, 0x0

    .line 964421
    const/4 v11, 0x0

    .line 964422
    const/4 v10, 0x0

    .line 964423
    const/4 v9, 0x0

    .line 964424
    const/4 v8, 0x0

    .line 964425
    const/4 v7, 0x0

    .line 964426
    const/4 v6, 0x0

    .line 964427
    const/4 v5, 0x0

    .line 964428
    const/4 v4, 0x0

    .line 964429
    const/4 v3, 0x0

    .line 964430
    const/4 v2, 0x0

    .line 964431
    const/4 v1, 0x0

    .line 964432
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 964433
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 964434
    const/4 v1, 0x0

    .line 964435
    :goto_0
    return v1

    .line 964436
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 964437
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_c

    .line 964438
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 964439
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 964440
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 964441
    const-string v15, "approval_mode"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 964442
    const/4 v2, 0x1

    .line 964443
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto :goto_1

    .line 964444
    :cond_2
    const-string v15, "approval_requests"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 964445
    invoke-static/range {p0 .. p1}, LX/5cv;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 964446
    :cond_3
    const-string v15, "customization_info"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 964447
    invoke-static/range {p0 .. p1}, LX/5dD;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 964448
    :cond_4
    const-string v15, "description"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 964449
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 964450
    :cond_5
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 964451
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 964452
    :cond_6
    const-string v15, "image"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 964453
    invoke-static/range {p0 .. p1}, LX/5cw;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 964454
    :cond_7
    const-string v15, "is_viewer_subscribed"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 964455
    const/4 v1, 0x1

    .line 964456
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 964457
    :cond_8
    const-string v15, "participant_count"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 964458
    invoke-static/range {p0 .. p1}, LX/5dI;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 964459
    :cond_9
    const-string v15, "thread_admins"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 964460
    invoke-static/range {p0 .. p1}, LX/5cx;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 964461
    :cond_a
    const-string v15, "thread_name"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 964462
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 964463
    :cond_b
    const-string v15, "thread_queue_participants"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 964464
    invoke-static/range {p0 .. p1}, LX/5dH;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 964465
    :cond_c
    const/16 v14, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 964466
    if-eqz v2, :cond_d

    .line 964467
    const/4 v2, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v14}, LX/186;->a(III)V

    .line 964468
    :cond_d
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 964469
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 964470
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 964471
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 964472
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 964473
    if-eqz v1, :cond_e

    .line 964474
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 964475
    :cond_e
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 964476
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 964477
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 964478
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 964479
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
