.class public final LX/6QO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/47G;

.field public final synthetic b:LX/6Qa;


# direct methods
.method public constructor <init>(LX/6Qa;LX/47G;)V
    .locals 0

    .prologue
    .line 1087372
    iput-object p1, p0, LX/6QO;->b:LX/6Qa;

    iput-object p2, p0, LX/6QO;->a:LX/47G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1087373
    iget-object v0, p0, LX/6QO;->b:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->h:LX/03V;

    sget-object v1, LX/6Qa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1087374
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1087375
    check-cast p1, Ljava/lang/Boolean;

    const/4 v2, 0x0

    .line 1087376
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1087377
    iget-object v0, p0, LX/6QO;->b:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->n:LX/2v6;

    iget-object v1, p0, LX/6QO;->a:LX/47G;

    iget-object v1, v1, LX/47G;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2v6;->b(Ljava/lang/String;)V

    .line 1087378
    iget-object v0, p0, LX/6QO;->b:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->b:LX/0s6;

    iget-object v1, p0, LX/6QO;->a:LX/47G;

    iget-object v1, v1, LX/47G;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/01H;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1087379
    iget-object v0, p0, LX/6QO;->b:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->n:LX/2v6;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->CANCELED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    iget-object v3, p0, LX/6QO;->a:LX/47G;

    iget-object v3, v3, LX/47G;->i:Ljava/lang/String;

    iget-object v4, p0, LX/6QO;->a:LX/47G;

    iget-object v4, v4, LX/47G;->e:Ljava/lang/String;

    iget-object v5, p0, LX/6QO;->a:LX/47G;

    iget-object v5, v5, LX/47G;->l:Ljava/lang/String;

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2v6;->a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6VZ;)V

    .line 1087380
    :cond_0
    :goto_0
    return-void

    .line 1087381
    :cond_1
    iget-object v0, p0, LX/6QO;->b:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->h:LX/03V;

    sget-object v1, LX/6Qa;->a:Ljava/lang/String;

    const-string v2, "could not cancel update"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
