.class public final LX/63X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/63W;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:I

.field public final synthetic d:LX/63Y;


# direct methods
.method public constructor <init>(LX/63Y;LX/63W;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 1043295
    iput-object p1, p0, LX/63X;->d:LX/63Y;

    iput-object p2, p0, LX/63X;->a:LX/63W;

    iput-object p3, p0, LX/63X;->b:Ljava/util/List;

    iput p4, p0, LX/63X;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x21bdc024

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1043296
    iget-object v0, p0, LX/63X;->a:LX/63W;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/63X;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/63X;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v2, p0, LX/63X;->c:I

    if-le v0, v2, :cond_0

    .line 1043297
    iget-object v0, p0, LX/63X;->b:Ljava/util/List;

    iget v2, p0, LX/63X;->c:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043298
    if-eqz v0, :cond_0

    .line 1043299
    iget-boolean v2, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    move v2, v2

    .line 1043300
    if-eqz v2, :cond_0

    .line 1043301
    iget-object v2, p0, LX/63X;->a:LX/63W;

    invoke-virtual {v2, p1, v0}, LX/63W;->a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1043302
    :cond_0
    const v0, 0x5744e119

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
