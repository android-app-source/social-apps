.class public final enum LX/5Rq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Rq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Rq;

.field public static final enum DEFAULT_CROP:LX/5Rq;

.field public static final enum ZOOM_CROP:LX/5Rq;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 916691
    new-instance v0, LX/5Rq;

    const-string v1, "DEFAULT_CROP"

    invoke-direct {v0, v1, v2}, LX/5Rq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rq;->DEFAULT_CROP:LX/5Rq;

    .line 916692
    new-instance v0, LX/5Rq;

    const-string v1, "ZOOM_CROP"

    invoke-direct {v0, v1, v3}, LX/5Rq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    .line 916693
    const/4 v0, 0x2

    new-array v0, v0, [LX/5Rq;

    sget-object v1, LX/5Rq;->DEFAULT_CROP:LX/5Rq;

    aput-object v1, v0, v2

    sget-object v1, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    aput-object v1, v0, v3

    sput-object v0, LX/5Rq;->$VALUES:[LX/5Rq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 916696
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Rq;
    .locals 1

    .prologue
    .line 916695
    const-class v0, LX/5Rq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rq;

    return-object v0
.end method

.method public static values()[LX/5Rq;
    .locals 1

    .prologue
    .line 916694
    sget-object v0, LX/5Rq;->$VALUES:[LX/5Rq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Rq;

    return-object v0
.end method
