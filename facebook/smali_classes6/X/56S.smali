.class public final LX/56S;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 837958
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 837959
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 837960
    :goto_0
    return v1

    .line 837961
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_4

    .line 837962
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 837963
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 837964
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 837965
    const-string v10, "start_time"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 837966
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 837967
    :cond_1
    const-string v10, "theme"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 837968
    invoke-static {p0, p1}, LX/56R;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 837969
    :cond_2
    const-string v10, "title"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 837970
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 837971
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 837972
    :cond_4
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 837973
    if-eqz v0, :cond_5

    move-object v0, p1

    .line 837974
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 837975
    :cond_5
    invoke-virtual {p1, v6, v8}, LX/186;->b(II)V

    .line 837976
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 837977
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 837978
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 837979
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 837980
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 837981
    const-string v2, "start_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837982
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 837983
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 837984
    if-eqz v0, :cond_1

    .line 837985
    const-string v1, "theme"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837986
    invoke-static {p0, v0, p2, p3}, LX/56R;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 837987
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 837988
    if-eqz v0, :cond_2

    .line 837989
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 837990
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 837991
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 837992
    return-void
.end method
