.class public LX/6Pa;
.super LX/6PU;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/model/FeedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 0
    .param p2    # Lcom/facebook/graphql/model/FeedUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1086248
    invoke-direct {p0, p1}, LX/6PU;-><init>(Ljava/lang/String;)V

    .line 1086249
    iput-object p2, p0, LX/6Pa;->a:Lcom/facebook/graphql/model/FeedUnit;

    .line 1086250
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V
    .locals 1

    .prologue
    .line 1086241
    iget-object v0, p0, LX/6Pa;->a:Lcom/facebook/graphql/model/FeedUnit;

    .line 1086242
    iget-object p0, p2, LX/40T;->a:LX/4VK;

    const-string p1, "local_follow_up_feed_unit"

    invoke-virtual {p0, p1, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086243
    iget-object v0, p2, LX/40T;->a:LX/4VK;

    .line 1086244
    invoke-static {v0}, LX/4VK;->d(LX/4VK;)LX/16f;

    move-result-object p0

    .line 1086245
    instance-of p2, p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p2}, LX/0PB;->checkState(Z)V

    .line 1086246
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 p2, 0x0

    invoke-static {p0, p2}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1086247
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086240
    const-string v0, "SetFollowUpFeedUnitMutatingVisitor"

    return-object v0
.end method
