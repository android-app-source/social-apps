.class public final LX/67N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field public final synthetic a:LX/3Fm;

.field public final b:Landroid/os/Messenger;

.field public final c:LX/67P;

.field private final d:Landroid/os/Messenger;

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public final i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/67L;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3Fm;Landroid/os/Messenger;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1052822
    iput-object p1, p0, LX/67N;->a:LX/3Fm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052823
    iput v0, p0, LX/67N;->e:I

    .line 1052824
    iput v0, p0, LX/67N;->f:I

    .line 1052825
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/67N;->i:Landroid/util/SparseArray;

    .line 1052826
    iput-object p2, p0, LX/67N;->b:Landroid/os/Messenger;

    .line 1052827
    new-instance v0, LX/67P;

    invoke-direct {v0, p0}, LX/67P;-><init>(LX/67N;)V

    iput-object v0, p0, LX/67N;->c:LX/67P;

    .line 1052828
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, LX/67N;->c:LX/67P;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, LX/67N;->d:Landroid/os/Messenger;

    .line 1052829
    return-void
.end method

.method public static a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z
    .locals 3

    .prologue
    .line 1052830
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 1052831
    iput p1, v0, Landroid/os/Message;->what:I

    .line 1052832
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 1052833
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 1052834
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1052835
    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1052836
    iget-object v1, p0, LX/67N;->d:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 1052837
    :try_start_0
    iget-object v1, p0, LX/67N;->b:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1052838
    const/4 v0, 0x1

    .line 1052839
    :goto_0
    return v0

    .line 1052840
    :catch_0
    move-exception v0

    .line 1052841
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    .line 1052842
    const-string v1, "MediaRouteProviderProxy"

    const-string v2, "Could not send message to service."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1052843
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1052844
    :catch_1
    goto :goto_1
.end method


# virtual methods
.method public final a(II)V
    .locals 6

    .prologue
    .line 1052845
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1052846
    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1052847
    const/4 v1, 0x7

    iget v2, p0, LX/67N;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LX/67N;->e:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-static/range {v0 .. v5}, LX/67N;->a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 1052848
    return-void
.end method

.method public final a(LX/38A;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1052849
    const/16 v1, 0xa

    iget v2, p0, LX/67N;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LX/67N;->e:I

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    .line 1052850
    iget-object v0, p1, LX/38A;->a:Landroid/os/Bundle;

    move-object v4, v0

    .line 1052851
    :goto_0
    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/67N;->a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 1052852
    return-void

    :cond_0
    move-object v4, v5

    .line 1052853
    goto :goto_0
.end method

.method public final b(II)V
    .locals 6

    .prologue
    .line 1052854
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1052855
    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1052856
    const/16 v1, 0x8

    iget v2, p0, LX/67N;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LX/67N;->e:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-static/range {v0 .. v5}, LX/67N;->a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 1052857
    return-void
.end method

.method public final binderDied()V
    .locals 3

    .prologue
    .line 1052858
    iget-object v0, p0, LX/67N;->a:LX/3Fm;

    iget-object v0, v0, LX/3Fm;->c:LX/3Fn;

    new-instance v1, Landroid/support/v7/media/RegisteredMediaRouteProvider$Connection$2;

    invoke-direct {v1, p0}, Landroid/support/v7/media/RegisteredMediaRouteProvider$Connection$2;-><init>(LX/67N;)V

    const v2, -0x1bdfc9ee

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1052859
    return-void
.end method

.method public final c(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1052860
    const/4 v1, 0x5

    iget v2, p0, LX/67N;->e:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, LX/67N;->e:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/67N;->a(LX/67N;IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 1052861
    return-void
.end method
