.class public LX/6A6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "LX/4XS;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;)V
    .locals 0
    .param p1    # Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058583
    iput-object p1, p0, LX/6A6;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;

    .line 1058584
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058585
    iget-object v0, p0, LX/6A6;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 9

    .prologue
    .line 1058586
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p2, LX/4XS;

    .line 1058587
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/6A6;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6A6;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6A6;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1058588
    :cond_0
    :goto_0
    return-void

    .line 1058589
    :cond_1
    iget-object v0, p0, LX/6A6;->a:Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel;->a()Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/graphql/PlaceListMutationsModels$GraphQLWithinPageEditPlaceListMutationModel$PlacelistModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;

    move-result-object v0

    .line 1058590
    if-nez v0, :cond_2

    .line 1058591
    const/4 v1, 0x0

    .line 1058592
    :goto_1
    move-object v0, v1

    .line 1058593
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string v2, "confirmed_location"

    invoke-virtual {v1, v2, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1058594
    goto :goto_0

    .line 1058595
    :cond_2
    new-instance v1, LX/4XY;

    invoke-direct {v1}, LX/4XY;-><init>()V

    .line 1058596
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1058597
    iput-object v2, v1, LX/4XY;->N:Ljava/lang/String;

    .line 1058598
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 1058599
    iput-object v2, v1, LX/4XY;->ag:Ljava/lang/String;

    .line 1058600
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->d()LX/1k1;

    move-result-object v2

    invoke-static {v2}, LX/5I9;->a(LX/1k1;)Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    .line 1058601
    iput-object v2, v1, LX/4XY;->aL:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 1058602
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->aQ_()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1058603
    if-nez v2, :cond_3

    .line 1058604
    const/4 v4, 0x0

    .line 1058605
    :goto_2
    move-object v2, v4

    .line 1058606
    iput-object v2, v1, LX/4XY;->aM:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 1058607
    invoke-virtual {v0}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceToSearchFieldsModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 1058608
    iput-object v2, v1, LX/4XY;->aT:Ljava/lang/String;

    .line 1058609
    invoke-virtual {v1}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    goto :goto_1

    .line 1058610
    :cond_3
    new-instance v4, LX/4WT;

    invoke-direct {v4}, LX/4WT;-><init>()V

    .line 1058611
    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, LX/15i;->l(II)D

    move-result-wide v6

    .line 1058612
    iput-wide v6, v4, LX/4WT;->b:D

    .line 1058613
    const/4 v5, 0x1

    invoke-virtual {v3, v2, v5}, LX/15i;->l(II)D

    move-result-wide v6

    .line 1058614
    iput-wide v6, v4, LX/4WT;->c:D

    .line 1058615
    const/4 v5, 0x2

    invoke-virtual {v3, v2, v5}, LX/15i;->l(II)D

    move-result-wide v6

    .line 1058616
    iput-wide v6, v4, LX/4WT;->d:D

    .line 1058617
    const/4 v5, 0x3

    invoke-virtual {v3, v2, v5}, LX/15i;->l(II)D

    move-result-wide v6

    .line 1058618
    iput-wide v6, v4, LX/4WT;->e:D

    .line 1058619
    invoke-virtual {v4}, LX/4WT;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v4

    goto :goto_2
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058620
    const-class v0, Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058621
    const-string v0, "SocialSearchAddEditLocationMutatingVisitor"

    return-object v0
.end method
