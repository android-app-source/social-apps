.class public final LX/5I3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 892386
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 892387
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 892388
    :goto_0
    return v1

    .line 892389
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 892390
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 892391
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 892392
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 892393
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 892394
    const-string v4, "style_list"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 892395
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 892396
    :cond_2
    const-string v4, "target"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 892397
    invoke-static {p0, p1}, LX/5G3;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 892398
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 892399
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 892400
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 892401
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 892375
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 892376
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 892377
    if-eqz v0, :cond_0

    .line 892378
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 892379
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 892380
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 892381
    if-eqz v0, :cond_1

    .line 892382
    const-string v1, "target"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 892383
    invoke-static {p0, v0, p2, p3}, LX/5G3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 892384
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 892385
    return-void
.end method
