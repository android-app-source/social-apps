.class public LX/6IQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6IP;


# instance fields
.field public final a:LX/5fS;

.field private final b:Landroid/hardware/Camera$CameraInfo;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JN;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JO;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5fS;Landroid/hardware/Camera$CameraInfo;)V
    .locals 0

    .prologue
    .line 1074026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1074027
    iput-object p1, p0, LX/6IQ;->a:LX/5fS;

    .line 1074028
    iput-object p2, p0, LX/6IQ;->b:Landroid/hardware/Camera$CameraInfo;

    .line 1074029
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;)",
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1074017
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1074018
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1074019
    :goto_0
    return-object v0

    .line 1074020
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1074021
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1074022
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1074023
    new-instance v3, LX/6JR;

    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-direct {v3, v4, v0}, LX/6JR;-><init>(II)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1074024
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 1074025
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/6JN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1074005
    iget-object v0, p0, LX/6IQ;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1074006
    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->b()Ljava/util/List;

    move-result-object v2

    .line 1074007
    if-nez v2, :cond_1

    .line 1074008
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6IQ;->c:Ljava/util/List;

    .line 1074009
    :cond_0
    iget-object v0, p0, LX/6IQ;->c:Ljava/util/List;

    return-object v0

    .line 1074010
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/6IQ;->c:Ljava/util/List;

    .line 1074011
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1074012
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1074013
    sget-object v3, LX/6IO;->c:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6JN;

    move-object v0, v3

    .line 1074014
    if-eqz v0, :cond_2

    .line 1074015
    iget-object v3, p0, LX/6IQ;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1074016
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/6JO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1073993
    iget-object v0, p0, LX/6IQ;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1073994
    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->t()Ljava/util/List;

    move-result-object v2

    .line 1073995
    if-nez v2, :cond_1

    .line 1073996
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6IQ;->d:Ljava/util/List;

    .line 1073997
    :cond_0
    iget-object v0, p0, LX/6IQ;->d:Ljava/util/List;

    return-object v0

    .line 1073998
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/6IQ;->d:Ljava/util/List;

    .line 1073999
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1074000
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1074001
    sget-object v3, LX/6IO;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6JO;

    move-object v0, v3

    .line 1074002
    if-eqz v0, :cond_2

    .line 1074003
    iget-object v3, p0, LX/6IQ;->d:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1074004
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1073990
    iget-object v0, p0, LX/6IQ;->e:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1073991
    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->C()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/6IQ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/6IQ;->e:Ljava/util/List;

    .line 1073992
    :cond_0
    iget-object v0, p0, LX/6IQ;->e:Ljava/util/List;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1073987
    iget-object v0, p0, LX/6IQ;->f:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1073988
    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->D()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/6IQ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/6IQ;->f:Ljava/util/List;

    .line 1073989
    :cond_0
    iget-object v0, p0, LX/6IQ;->f:Ljava/util/List;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/6JR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1073984
    iget-object v0, p0, LX/6IQ;->g:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1073985
    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->B()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/6IQ;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/6IQ;->g:Ljava/util/List;

    .line 1073986
    :cond_0
    iget-object v0, p0, LX/6IQ;->g:Ljava/util/List;

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1073979
    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1073983
    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->p()I

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1073982
    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->m()Z

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 1073981
    iget-object v0, p0, LX/6IQ;->b:Landroid/hardware/Camera$CameraInfo;

    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->orientation:I

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1073980
    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6IQ;->a:LX/5fS;

    invoke-virtual {v0}, LX/5fS;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
