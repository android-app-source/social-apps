.class public final LX/60G;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ActorsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$AttachmentsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1037036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1037037
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1037038
    iget-object v1, p0, LX/60G;->a:LX/0Px;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1037039
    iget-object v2, p0, LX/60G;->b:LX/0Px;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1037040
    iget-object v3, p0, LX/60G;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1037041
    iget-object v4, p0, LX/60G;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1037042
    iget-object v4, p0, LX/60G;->f:Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel$ShareableModel;

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1037043
    iget-object v4, p0, LX/60G;->g:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1037044
    const/4 v4, 0x7

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1037045
    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 1037046
    invoke-virtual {v0, v11, v2}, LX/186;->b(II)V

    .line 1037047
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1037048
    const/4 v1, 0x3

    iget-wide v2, p0, LX/60G;->d:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1037049
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1037050
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1037051
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1037052
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1037053
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1037054
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1037055
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1037056
    new-instance v0, LX/15i;

    move-object v2, v9

    move-object v3, v9

    move v4, v11

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1037057
    new-instance v1, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/video/protocol/core/VideoFragmentsModels$VideoStoryCreationStoryFragmentModel;-><init>(LX/15i;)V

    .line 1037058
    return-object v1
.end method
