.class public LX/6Z6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2D5;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0y3;

.field private final c:Landroid/location/LocationManager;

.field public final d:LX/0yH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1110895
    const-class v0, LX/6Z6;

    sput-object v0, LX/6Z6;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0y3;Landroid/location/LocationManager;LX/0yH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1110934
    iput-object p1, p0, LX/6Z6;->b:LX/0y3;

    .line 1110935
    iput-object p2, p0, LX/6Z6;->c:Landroid/location/LocationManager;

    .line 1110936
    iput-object p3, p0, LX/6Z6;->d:LX/0yH;

    .line 1110937
    return-void
.end method

.method public static a(LX/2Cv;)I
    .locals 2

    .prologue
    .line 1110927
    sget-object v0, LX/6Z5;->a:[I

    invoke-virtual {p0}, LX/2Cv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1110928
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "NO_POWER handled by passive location directly"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1110929
    :pswitch_0
    const/4 v0, 0x1

    .line 1110930
    :goto_0
    return v0

    .line 1110931
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1110932
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/2Cv;)I
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 1110924
    sget-object v1, LX/6Z5;->a:[I

    invoke-virtual {p0}, LX/2Cv;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1110925
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "NO_POWER handled by passive location directly"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1110926
    :pswitch_0
    const/4 v0, 0x1

    :pswitch_1
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/6Z6;
    .locals 4

    .prologue
    .line 1110922
    new-instance v3, LX/6Z6;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-static {p0}, LX/0y4;->b(LX/0QB;)Landroid/location/LocationManager;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    invoke-static {p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v2

    check-cast v2, LX/0yH;

    invoke-direct {v3, v0, v1, v2}, LX/6Z6;-><init>(LX/0y3;Landroid/location/LocationManager;LX/0yH;)V

    .line 1110923
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/facebook/location/ImmutableLocation;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1110920
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 1110921
    invoke-static {v0}, Lcom/facebook/location/ImmutableLocation;->c(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 1110917
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110918
    :try_start_0
    iget-object v0, p0, LX/6Z6;->c:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->removeUpdates(Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1110919
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final a(Landroid/app/PendingIntent;LX/2Cu;)V
    .locals 7

    .prologue
    .line 1110896
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110897
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110898
    iget-object v0, p0, LX/6Z6;->b:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    .line 1110899
    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-eq v0, v1, :cond_0

    .line 1110900
    new-instance v0, LX/6ZF;

    sget-object v1, LX/6ZE;->LOCATION_UNAVAILABLE:LX/6ZE;

    invoke-direct {v0, v1}, LX/6ZF;-><init>(LX/6ZE;)V

    throw v0

    .line 1110901
    :cond_0
    iget-object v0, p2, LX/2Cu;->a:LX/2Cv;

    sget-object v1, LX/2Cv;->NO_POWER:LX/2Cv;

    if-ne v0, v1, :cond_1

    .line 1110902
    :try_start_0
    iget-object v0, p0, LX/6Z6;->c:Landroid/location/LocationManager;

    const-string v1, "passive"

    iget-wide v2, p2, LX/2Cu;->c:J

    iget v4, p2, LX/2Cu;->d:F

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1110903
    :goto_0
    return-void

    .line 1110904
    :catch_0
    move-exception v0

    .line 1110905
    sget-object v1, LX/6Z6;->a:Ljava/lang/Class;

    const-string v2, "Could not start passive listening"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1110906
    new-instance v1, LX/6ZF;

    sget-object v2, LX/6ZE;->TEMPORARY_ERROR:LX/6ZE;

    invoke-direct {v1, v2, v0}, LX/6ZF;-><init>(LX/6ZE;Ljava/lang/Throwable;)V

    throw v1

    .line 1110907
    :cond_1
    new-instance v1, Landroid/location/Criteria;

    invoke-direct {v1}, Landroid/location/Criteria;-><init>()V

    .line 1110908
    iget-object v0, p0, LX/6Z6;->d:LX/0yH;

    sget-object v2, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v2}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/location/Criteria;->setCostAllowed(Z)V

    .line 1110909
    iget-object v0, p2, LX/2Cu;->a:LX/2Cv;

    invoke-static {v0}, LX/6Z6;->a(LX/2Cv;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/location/Criteria;->setPowerRequirement(I)V

    .line 1110910
    iget-object v0, p2, LX/2Cu;->a:LX/2Cv;

    invoke-static {v0}, LX/6Z6;->b(LX/2Cv;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 1110911
    move-object v5, v1

    .line 1110912
    :try_start_1
    iget-object v1, p0, LX/6Z6;->c:Landroid/location/LocationManager;

    iget-wide v2, p2, LX/2Cu;->b:J

    iget v4, p2, LX/2Cu;->d:F

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(JFLandroid/location/Criteria;Landroid/app/PendingIntent;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1110913
    :catch_1
    move-exception v0

    .line 1110914
    sget-object v1, LX/6Z6;->a:Ljava/lang/Class;

    const-string v2, "Could not start continuous listening"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1110915
    new-instance v1, LX/6ZF;

    sget-object v2, LX/6ZE;->TEMPORARY_ERROR:LX/6ZE;

    invoke-direct {v1, v2, v0}, LX/6ZF;-><init>(LX/6ZE;Ljava/lang/Throwable;)V

    throw v1

    .line 1110916
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
