.class public final LX/6VD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/view/View;

.field public b:LX/1nq;

.field public c:Landroid/text/Layout;

.field public d:I

.field public e:I

.field public f:I

.field public final synthetic g:Lcom/facebook/fbui/widget/contentview/ContentView;


# direct methods
.method public constructor <init>(Lcom/facebook/fbui/widget/contentview/ContentView;)V
    .locals 1

    .prologue
    .line 1103763
    iput-object p1, p0, LX/6VD;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103764
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    iput-object v0, p0, LX/6VD;->b:LX/1nq;

    .line 1103765
    const/4 v0, 0x0

    iput-object v0, p0, LX/6VD;->c:Landroid/text/Layout;

    .line 1103766
    const/4 v0, 0x0

    iput v0, p0, LX/6VD;->d:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/fbui/widget/contentview/ContentView;B)V
    .locals 0

    .prologue
    .line 1103649
    invoke-direct {p0, p1}, LX/6VD;-><init>(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    return-void
.end method

.method private a(Landroid/view/View;III)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x1

    .line 1103741
    iget-object v0, p0, LX/6VD;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-static {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->b(Lcom/facebook/fbui/widget/contentview/ContentView;)Z

    move-result v2

    .line 1103742
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/6VC;

    .line 1103743
    iget v1, v0, LX/1au;->d:I

    if-gez v1, :cond_0

    const v1, 0x800003

    .line 1103744
    :goto_0
    and-int/lit8 v1, v1, 0x7

    .line 1103745
    invoke-static {v0}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v3

    .line 1103746
    invoke-static {v0}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    .line 1103747
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 1103748
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 1103749
    if-eqz v2, :cond_3

    .line 1103750
    if-ne v1, v7, :cond_1

    .line 1103751
    sub-int v0, p4, v0

    sub-int/2addr v0, v4

    .line 1103752
    :goto_1
    add-int v1, v0, v4

    add-int v2, p3, v5

    invoke-virtual {p1, v0, p3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 1103753
    return-void

    .line 1103754
    :cond_0
    iget v1, v0, LX/1au;->d:I

    goto :goto_0

    .line 1103755
    :cond_1
    if-ne v1, v6, :cond_2

    .line 1103756
    sub-int v1, p4, p2

    sub-int/2addr v1, v3

    sub-int/2addr v1, v4

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p2

    add-int/2addr v0, v3

    goto :goto_1

    .line 1103757
    :cond_2
    add-int v0, p2, v3

    goto :goto_1

    .line 1103758
    :cond_3
    if-ne v1, v7, :cond_4

    .line 1103759
    add-int/2addr v0, p2

    goto :goto_1

    .line 1103760
    :cond_4
    if-ne v1, v6, :cond_5

    .line 1103761
    sub-int v1, p4, p2

    sub-int/2addr v1, v3

    sub-int/2addr v1, v4

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p2

    add-int/2addr v0, v1

    goto :goto_1

    .line 1103762
    :cond_5
    sub-int v0, p4, v3

    sub-int/2addr v0, v4

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/6VE;
    .locals 1

    .prologue
    .line 1103735
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1103736
    sget-object v0, LX/6VE;->VIEW:LX/6VE;

    .line 1103737
    :goto_0
    return-object v0

    .line 1103738
    :cond_0
    iget-object v0, p0, LX/6VD;->b:LX/1nq;

    if-eqz v0, :cond_1

    .line 1103739
    sget-object v0, LX/6VE;->LAYOUT:LX/6VE;

    goto :goto_0

    .line 1103740
    :cond_1
    sget-object v0, LX/6VE;->NONE:LX/6VE;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1103730
    sget-object v0, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v1

    invoke-virtual {v1}, LX/6VE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1103731
    :cond_0
    :goto_0
    return-void

    .line 1103732
    :pswitch_0
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103733
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0

    .line 1103734
    :pswitch_1
    iget-object v1, p0, LX/6VD;->b:LX/1nq;

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    sget-object v0, LX/0zo;->a:LX/0zr;

    :goto_1
    invoke-virtual {v1, v0}, LX/1nq;->a(LX/0zr;)LX/1nq;

    goto :goto_0

    :cond_1
    sget-object v0, LX/0zo;->c:LX/0zr;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(II)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    .line 1103719
    sget-object v0, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v1

    invoke-virtual {v1}, LX/6VE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1103720
    :cond_0
    :goto_0
    return-void

    .line 1103721
    :pswitch_0
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 1103722
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v2, v0

    check-cast v2, LX/6VC;

    .line 1103723
    iget-object v0, p0, LX/6VD;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, LX/6VD;->a:Landroid/view/View;

    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v5, v4, v2

    move v2, p1

    move v4, p2

    .line 1103724
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 1103725
    goto :goto_0

    .line 1103726
    :pswitch_1
    iget v0, p0, LX/6VD;->d:I

    if-eq v0, v2, :cond_1

    .line 1103727
    iget-object v0, p0, LX/6VD;->b:LX/1nq;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1nq;->a(I)LX/1nq;

    .line 1103728
    iget-object v0, p0, LX/6VD;->b:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, LX/6VD;->c:Landroid/text/Layout;

    goto :goto_0

    .line 1103729
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/6VD;->c:Landroid/text/Layout;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(III)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1103711
    sget-object v0, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v1

    invoke-virtual {v1}, LX/6VE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1103712
    :cond_0
    :goto_0
    return-void

    .line 1103713
    :pswitch_0
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 1103714
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    invoke-direct {p0, v0, p1, p2, p3}, LX/6VD;->a(Landroid/view/View;III)V

    goto :goto_0

    .line 1103715
    :pswitch_1
    iget v0, p0, LX/6VD;->d:I

    if-eq v0, v2, :cond_0

    .line 1103716
    iget-object v0, p0, LX/6VD;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-static {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->a(Lcom/facebook/fbui/widget/contentview/ContentView;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput p1, p0, LX/6VD;->e:I

    .line 1103717
    iput p2, p0, LX/6VD;->f:I

    goto :goto_0

    .line 1103718
    :cond_1
    invoke-virtual {p0}, LX/6VD;->d()I

    move-result v0

    sub-int p1, p3, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1103705
    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v0

    sget-object v1, LX/6VE;->LAYOUT:LX/6VE;

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/6VD;->d:I

    if-nez v0, :cond_0

    .line 1103706
    iget-object v0, p0, LX/6VD;->c:Landroid/text/Layout;

    if-eqz v0, :cond_0

    .line 1103707
    iget v0, p0, LX/6VD;->e:I

    int-to-float v0, v0

    iget v1, p0, LX/6VD;->f:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1103708
    iget-object v0, p0, LX/6VD;->c:Landroid/text/Layout;

    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1103709
    iget v0, p0, LX/6VD;->e:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, LX/6VD;->f:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1103710
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1103700
    sget-object v0, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v1

    invoke-virtual {v1}, LX/6VE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1103701
    :cond_0
    :goto_0
    return-void

    .line 1103702
    :pswitch_0
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103703
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1103704
    :pswitch_1
    iget-object v0, p0, LX/6VD;->b:LX/1nq;

    invoke-virtual {v0, p1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1103695
    sget-object v0, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v1

    invoke-virtual {v1}, LX/6VE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1103696
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1103697
    :pswitch_0
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103698
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 1103699
    :pswitch_1
    iget-object v0, p0, LX/6VD;->b:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->a()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1103691
    iput p1, p0, LX/6VD;->d:I

    .line 1103692
    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v0

    sget-object v1, LX/6VE;->VIEW:LX/6VE;

    if-ne v0, v1, :cond_0

    .line 1103693
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1103694
    :cond_0
    return-void
.end method

.method public final c()F
    .locals 2

    .prologue
    .line 1103686
    sget-object v0, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v1

    invoke-virtual {v1}, LX/6VE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1103687
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1103688
    :pswitch_0
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103689
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    goto :goto_0

    .line 1103690
    :pswitch_1
    iget-object v0, p0, LX/6VD;->b:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->b()F

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1103676
    sget-object v0, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v3

    invoke-virtual {v3}, LX/6VE;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 1103677
    :cond_0
    :goto_0
    return-void

    .line 1103678
    :pswitch_0
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103679
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    .line 1103680
    if-ne p1, v1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1103681
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 1103682
    goto :goto_1

    .line 1103683
    :pswitch_1
    iget-object v0, p0, LX/6VD;->b:LX/1nq;

    if-ne p1, v1, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, LX/1nq;->b(Z)LX/1nq;

    .line 1103684
    iget-object v0, p0, LX/6VD;->b:LX/1nq;

    invoke-virtual {v0, p1}, LX/1nq;->f(I)LX/1nq;

    goto :goto_0

    :cond_2
    move v1, v2

    .line 1103685
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 1103672
    sget-object v0, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v1

    invoke-virtual {v1}, LX/6VE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1103673
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1103674
    :pswitch_0
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    goto :goto_0

    .line 1103675
    :pswitch_1
    iget-object v0, p0, LX/6VD;->c:Landroid/text/Layout;

    invoke-static {v0}, LX/1nt;->a(Landroid/text/Layout;)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 1103667
    sget-object v0, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v1

    invoke-virtual {v1}, LX/6VE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1103668
    :cond_0
    :goto_0
    return-void

    .line 1103669
    :pswitch_0
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1103670
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 1103671
    :pswitch_1
    iget-object v0, p0, LX/6VD;->b:LX/1nq;

    iget-object v1, p0, LX/6VD;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1, p1}, LX/5OZ;->b(LX/1nq;Landroid/content/Context;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()I
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 1103662
    sget-object v1, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v2

    invoke-virtual {v2}, LX/6VE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1103663
    :cond_0
    :goto_0
    return v0

    .line 1103664
    :pswitch_0
    iget-object v1, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_0

    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0

    .line 1103665
    :pswitch_1
    iget v1, p0, LX/6VD;->d:I

    if-eq v1, v3, :cond_0

    .line 1103666
    iget-object v0, p0, LX/6VD;->c:Landroid/text/Layout;

    invoke-static {v0}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 1103658
    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v0

    sget-object v1, LX/6VE;->VIEW:LX/6VE;

    if-ne v0, v1, :cond_0

    .line 1103659
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/6VC;

    .line 1103660
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1103661
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 1103654
    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v0

    sget-object v1, LX/6VE;->VIEW:LX/6VE;

    if-ne v0, v1, :cond_0

    .line 1103655
    iget-object v0, p0, LX/6VD;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/6VC;

    .line 1103656
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    .line 1103657
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1103650
    sget-object v2, LX/6VA;->b:[I

    invoke-virtual {p0}, LX/6VD;->a()LX/6VE;

    move-result-object v3

    invoke-virtual {v3}, LX/6VE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1103651
    :cond_0
    :goto_0
    return v0

    .line 1103652
    :pswitch_0
    iget-object v2, p0, LX/6VD;->a:Landroid/view/View;

    instance-of v2, v2, Landroid/widget/TextView;

    if-eqz v2, :cond_1

    invoke-virtual {p0}, LX/6VD;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 1103653
    :pswitch_1
    invoke-virtual {p0}, LX/6VD;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
