.class public final LX/6Fz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

.field public final synthetic c:LX/6G2;


# direct methods
.method public constructor <init>(LX/6G2;Landroid/content/Context;Lcom/facebook/ui/dialogs/ProgressDialogFragment;)V
    .locals 0

    .prologue
    .line 1069515
    iput-object p1, p0, LX/6Fz;->c:LX/6G2;

    iput-object p2, p0, LX/6Fz;->a:Landroid/content/Context;

    iput-object p3, p0, LX/6Fz;->b:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1069516
    iget-object v0, p0, LX/6Fz;->a:Landroid/content/Context;

    instance-of v0, v0, LX/0ew;

    if-eqz v0, :cond_0

    .line 1069517
    iget-object v0, p0, LX/6Fz;->a:Landroid/content/Context;

    check-cast v0, LX/0ew;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 1069518
    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Fz;->b:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1069519
    iget-object v0, p0, LX/6Fz;->b:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1069520
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1069521
    invoke-direct {p0}, LX/6Fz;->a()V

    .line 1069522
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1069523
    invoke-direct {p0}, LX/6Fz;->a()V

    .line 1069524
    return-void
.end method
