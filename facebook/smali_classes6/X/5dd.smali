.class public LX/5dd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:LX/5dc;

.field public h:Ljava/lang/String;

.field public i:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/messaging/model/attribution/AttributionVisibility;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 965947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965948
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 965949
    iput-object v0, p0, LX/5dd;->i:LX/0P1;

    .line 965950
    sget-object v0, Lcom/facebook/messaging/model/attribution/AttributionVisibility;->b:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    iput-object v0, p0, LX/5dd;->j:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 965951
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)LX/5dd;
    .locals 2

    .prologue
    .line 965952
    iget-object v0, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->a:Ljava/lang/String;

    .line 965953
    iput-object v0, p0, LX/5dd;->a:Ljava/lang/String;

    .line 965954
    move-object v0, p0

    .line 965955
    iget-object v1, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    .line 965956
    iput-object v1, v0, LX/5dd;->b:Ljava/lang/String;

    .line 965957
    move-object v0, v0

    .line 965958
    iget-object v1, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->c:Ljava/lang/String;

    .line 965959
    iput-object v1, v0, LX/5dd;->c:Ljava/lang/String;

    .line 965960
    move-object v0, v0

    .line 965961
    iget-object v1, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    .line 965962
    iput-object v1, v0, LX/5dd;->d:Ljava/lang/String;

    .line 965963
    move-object v0, v0

    .line 965964
    iget-object v1, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->e:Ljava/lang/String;

    .line 965965
    iput-object v1, v0, LX/5dd;->e:Ljava/lang/String;

    .line 965966
    move-object v0, v0

    .line 965967
    iget-object v1, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    .line 965968
    iput-object v1, v0, LX/5dd;->f:Ljava/lang/String;

    .line 965969
    move-object v0, v0

    .line 965970
    iget-object v1, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->g:LX/0P1;

    invoke-virtual {v0, v1}, LX/5dd;->a(Ljava/util/Map;)LX/5dd;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->i:LX/5dc;

    .line 965971
    iput-object v1, v0, LX/5dd;->g:LX/5dc;

    .line 965972
    move-object v0, v0

    .line 965973
    iget-object v1, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->h:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 965974
    iput-object v1, v0, LX/5dd;->j:Lcom/facebook/messaging/model/attribution/AttributionVisibility;

    .line 965975
    move-object v0, v0

    .line 965976
    iget-object v1, p1, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->j:Ljava/lang/String;

    .line 965977
    iput-object v1, v0, LX/5dd;->h:Ljava/lang/String;

    .line 965978
    move-object v0, v0

    .line 965979
    return-object v0
.end method

.method public final a(Ljava/util/Map;)LX/5dd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/5dd;"
        }
    .end annotation

    .prologue
    .line 965980
    invoke-static {p1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/5dd;->i:LX/0P1;

    .line 965981
    return-object p0
.end method

.method public final k()Lcom/facebook/messaging/model/attribution/ContentAppAttribution;
    .locals 1

    .prologue
    .line 965982
    new-instance v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;-><init>(LX/5dd;)V

    return-object v0
.end method
