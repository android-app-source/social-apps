.class public LX/6Z2;
.super LX/0e8;
.source ""

# interfaces
.implements LX/0Up;
.implements LX/0dN;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final b:LX/0Vt;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0W9;LX/0Vt;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110832
    invoke-direct {p0, p1, p2}, LX/0e8;-><init>(Landroid/content/Context;LX/0W9;)V

    .line 1110833
    iput-object p3, p0, LX/6Z2;->b:LX/0Vt;

    .line 1110834
    sget-object v0, LX/0eC;->b:LX/0Tn;

    invoke-interface {p4, v0, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 1110835
    return-void
.end method

.method private d()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1110819
    iget-object v0, p0, LX/0e8;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1110820
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1110821
    :goto_0
    return-void

    .line 1110822
    :cond_0
    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 1110823
    iget-object v2, p0, LX/6Z2;->b:LX/0Vt;

    invoke-virtual {v2, v1}, LX/0Vt;->a(Ljava/util/Locale;)V

    .line 1110824
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1110825
    :goto_1
    goto :goto_0

    .line 1110826
    :cond_1
    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    .line 1110827
    invoke-virtual {p0, v1}, LX/0e8;->a(Ljava/util/Locale;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 0

    .prologue
    .line 1110830
    invoke-direct {p0}, LX/6Z2;->d()V

    .line 1110831
    return-void
.end method

.method public final init()V
    .locals 0

    .prologue
    .line 1110828
    invoke-direct {p0}, LX/6Z2;->d()V

    .line 1110829
    return-void
.end method
