.class public final LX/6Us;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Uo;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6Us;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103051
    return-void
.end method

.method private a(Landroid/view/View;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1103052
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 1103053
    :goto_0
    return v0

    .line 1103054
    :cond_0
    const/4 v1, 0x1

    .line 1103055
    instance-of v2, p1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 1103056
    check-cast p1, Landroid/view/ViewGroup;

    .line 1103057
    :goto_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1103058
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, LX/6Us;->a(Landroid/view/View;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1103059
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 1103060
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/6Us;
    .locals 3

    .prologue
    .line 1103061
    sget-object v0, LX/6Us;->b:LX/6Us;

    if-nez v0, :cond_1

    .line 1103062
    const-class v1, LX/6Us;

    monitor-enter v1

    .line 1103063
    :try_start_0
    sget-object v0, LX/6Us;->b:LX/6Us;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1103064
    if-eqz v2, :cond_0

    .line 1103065
    :try_start_1
    new-instance v0, LX/6Us;

    invoke-direct {v0}, LX/6Us;-><init>()V

    .line 1103066
    move-object v0, v0

    .line 1103067
    sput-object v0, LX/6Us;->b:LX/6Us;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1103068
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1103069
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1103070
    :cond_1
    sget-object v0, LX/6Us;->b:LX/6Us;

    return-object v0

    .line 1103071
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1103072
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103073
    const-string v0, "Reduce the number of views in use by flattening view hierarchies."

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/util/Map;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1103074
    invoke-direct {p0, p1}, LX/6Us;->a(Landroid/view/View;)I

    move-result v0

    .line 1103075
    const/16 v1, 0x1f4

    if-le v0, v1, :cond_0

    .line 1103076
    const-string v1, "num_non_gone_views"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103077
    const/4 v0, 0x1

    .line 1103078
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103079
    const-string v0, "Too many views in view hierarchy"

    return-object v0
.end method
