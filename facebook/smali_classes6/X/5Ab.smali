.class public final LX/5Ab;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 858952
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 858953
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 858954
    :goto_0
    return v1

    .line 858955
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 858956
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 858957
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 858958
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 858959
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 858960
    const-string v5, "real_time_activity_actors"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 858961
    invoke-static {p0, p1}, LX/5Aa;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 858962
    :cond_2
    const-string v5, "real_time_activity_sentence"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 858963
    invoke-static {p0, p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 858964
    :cond_3
    const-string v5, "real_time_activity_type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 858965
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedbackRealTimeActivityType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 858966
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 858967
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 858968
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 858969
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 858970
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 858971
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 858972
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 858973
    if-eqz v0, :cond_0

    .line 858974
    const-string v1, "real_time_activity_actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858975
    invoke-static {p0, v0, p2, p3}, LX/5Aa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 858976
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 858977
    if-eqz v0, :cond_1

    .line 858978
    const-string v1, "real_time_activity_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858979
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 858980
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 858981
    if-eqz v0, :cond_2

    .line 858982
    const-string v0, "real_time_activity_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858983
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 858984
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 858985
    return-void
.end method
