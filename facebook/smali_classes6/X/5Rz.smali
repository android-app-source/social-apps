.class public final enum LX/5Rz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Rz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Rz;

.field public static final enum ADS:LX/5Rz;

.field public static final enum COMPOSER:LX/5Rz;

.field public static final enum FUNDRAISER_CREATION:LX/5Rz;

.field public static final enum GENERIC:LX/5Rz;

.field public static final enum LIB_FB4A:LX/5Rz;

.field public static final enum NATIVE_SHARE_SHEET:LX/5Rz;

.field public static final enum PRODUCT_ITEM:LX/5Rz;

.field public static final enum PROFILE:LX/5Rz;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 916964
    new-instance v0, LX/5Rz;

    const-string v1, "COMPOSER"

    invoke-direct {v0, v1, v3}, LX/5Rz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rz;->COMPOSER:LX/5Rz;

    .line 916965
    new-instance v0, LX/5Rz;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v4}, LX/5Rz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rz;->PROFILE:LX/5Rz;

    .line 916966
    new-instance v0, LX/5Rz;

    const-string v1, "PRODUCT_ITEM"

    invoke-direct {v0, v1, v5}, LX/5Rz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rz;->PRODUCT_ITEM:LX/5Rz;

    .line 916967
    new-instance v0, LX/5Rz;

    const-string v1, "NATIVE_SHARE_SHEET"

    invoke-direct {v0, v1, v6}, LX/5Rz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rz;->NATIVE_SHARE_SHEET:LX/5Rz;

    .line 916968
    new-instance v0, LX/5Rz;

    const-string v1, "GENERIC"

    invoke-direct {v0, v1, v7}, LX/5Rz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rz;->GENERIC:LX/5Rz;

    .line 916969
    new-instance v0, LX/5Rz;

    const-string v1, "ADS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5Rz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rz;->ADS:LX/5Rz;

    .line 916970
    new-instance v0, LX/5Rz;

    const-string v1, "LIB_FB4A"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/5Rz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rz;->LIB_FB4A:LX/5Rz;

    .line 916971
    new-instance v0, LX/5Rz;

    const-string v1, "FUNDRAISER_CREATION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/5Rz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Rz;->FUNDRAISER_CREATION:LX/5Rz;

    .line 916972
    const/16 v0, 0x8

    new-array v0, v0, [LX/5Rz;

    sget-object v1, LX/5Rz;->COMPOSER:LX/5Rz;

    aput-object v1, v0, v3

    sget-object v1, LX/5Rz;->PROFILE:LX/5Rz;

    aput-object v1, v0, v4

    sget-object v1, LX/5Rz;->PRODUCT_ITEM:LX/5Rz;

    aput-object v1, v0, v5

    sget-object v1, LX/5Rz;->NATIVE_SHARE_SHEET:LX/5Rz;

    aput-object v1, v0, v6

    sget-object v1, LX/5Rz;->GENERIC:LX/5Rz;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5Rz;->ADS:LX/5Rz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5Rz;->LIB_FB4A:LX/5Rz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5Rz;->FUNDRAISER_CREATION:LX/5Rz;

    aput-object v2, v0, v1

    sput-object v0, LX/5Rz;->$VALUES:[LX/5Rz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 916973
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Rz;
    .locals 1

    .prologue
    .line 916974
    const-class v0, LX/5Rz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rz;

    return-object v0
.end method

.method public static values()[LX/5Rz;
    .locals 1

    .prologue
    .line 916975
    sget-object v0, LX/5Rz;->$VALUES:[LX/5Rz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Rz;

    return-object v0
.end method
