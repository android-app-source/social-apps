.class public final enum LX/5vL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5vL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5vL;

.field public static final enum ADD:LX/5vL;

.field public static final enum REMOVE:LX/5vL;


# instance fields
.field private final mAction:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1021792
    new-instance v0, LX/5vL;

    const-string v1, "ADD"

    const-string v2, "ADD"

    invoke-direct {v0, v1, v3, v2}, LX/5vL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5vL;->ADD:LX/5vL;

    .line 1021793
    new-instance v0, LX/5vL;

    const-string v1, "REMOVE"

    const-string v2, "REMOVE"

    invoke-direct {v0, v1, v4, v2}, LX/5vL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5vL;->REMOVE:LX/5vL;

    .line 1021794
    const/4 v0, 0x2

    new-array v0, v0, [LX/5vL;

    sget-object v1, LX/5vL;->ADD:LX/5vL;

    aput-object v1, v0, v3

    sget-object v1, LX/5vL;->REMOVE:LX/5vL;

    aput-object v1, v0, v4

    sput-object v0, LX/5vL;->$VALUES:[LX/5vL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1021788
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1021789
    iput-object p3, p0, LX/5vL;->mAction:Ljava/lang/String;

    .line 1021790
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5vL;
    .locals 1

    .prologue
    .line 1021791
    const-class v0, LX/5vL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5vL;

    return-object v0
.end method

.method public static values()[LX/5vL;
    .locals 1

    .prologue
    .line 1021787
    sget-object v0, LX/5vL;->$VALUES:[LX/5vL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5vL;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1021786
    iget-object v0, p0, LX/5vL;->mAction:Ljava/lang/String;

    return-object v0
.end method
