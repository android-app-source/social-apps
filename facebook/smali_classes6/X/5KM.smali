.class public LX/5KM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/17W;

.field private final c:LX/0Zb;

.field private final d:LX/0gh;

.field private e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/componentscript/CSNavigationIntentFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17W;LX/0gh;LX/0Zb;Ljava/util/Set;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/17W;",
            "LX/0gh;",
            "LX/0Zb;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/componentscript/CSNavigationIntentFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 898547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 898548
    iput-object p1, p0, LX/5KM;->a:Landroid/content/Context;

    .line 898549
    iput-object p2, p0, LX/5KM;->b:LX/17W;

    .line 898550
    iput-object p3, p0, LX/5KM;->d:LX/0gh;

    .line 898551
    iput-object p4, p0, LX/5KM;->c:LX/0Zb;

    .line 898552
    iput-object p5, p0, LX/5KM;->e:Ljava/util/Set;

    .line 898553
    iput-object p6, p0, LX/5KM;->f:LX/0Ot;

    .line 898554
    return-void
.end method


# virtual methods
.method public navigate(Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;)V
    .locals 9
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        mode = .enum LX/5SV;->METHOD:LX/5SV;
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 898555
    :try_start_0
    iget-object v0, p0, LX/5KM;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gdl;

    .line 898556
    const-string v2, "type"

    invoke-virtual {p1, v2}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 898557
    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_1
    :goto_1
    packed-switch v2, :pswitch_data_1

    .line 898558
    const/4 v2, 0x0

    :goto_2
    move-object v2, v2

    .line 898559
    if-eqz v2, :cond_0

    .line 898560
    iget-object v0, v2, LX/Gdj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-boolean v0, v2, LX/Gdj;->e:Z

    iget-object v3, v2, LX/Gdj;->d:LX/162;

    invoke-static {v0, v3}, LX/17Q;->a(ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 898561
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 898562
    iget-object v0, v2, LX/Gdj;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->f()I

    move-result v5

    iget-object v0, v2, LX/Gdj;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v7

    iget-object v0, v2, LX/Gdj;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v2, LX/Gdj;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_3
    iget-object v8, v2, LX/Gdj;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v5, v7, v0, v8}, LX/5ve;->a(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 898563
    const-string v0, "cs_intent_target_event"

    invoke-static {v4, v0, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 898564
    const-string v5, "cs_intent_target_last_tap_point"

    if-eqz v3, :cond_4

    .line 898565
    iget-boolean v0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->j:Z

    move v0, v0

    .line 898566
    if-eqz v0, :cond_4

    const-string v0, "tap_profile_pic_sponsored"

    :goto_4
    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 898567
    const-string v0, "cs_intent_target_tracking_codes"

    iget-object v3, v2, LX/Gdj;->d:LX/162;

    invoke-virtual {v3}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 898568
    move-object v3, v4

    .line 898569
    const-string v0, "cs_intent_target_event"

    invoke-static {v3, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 898570
    const-string v4, "cs_intent_target_tracking_codes"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 898571
    const-string v5, "cs_intent_target_last_tap_point"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 898572
    iget-object v5, p0, LX/5KM;->c:LX/0Zb;

    invoke-interface {v5, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 898573
    iget-object v0, p0, LX/5KM;->d:LX/0gh;

    .line 898574
    iput-object v4, v0, LX/0gh;->y:Ljava/lang/String;

    .line 898575
    iget-object v0, p0, LX/5KM;->d:LX/0gh;

    invoke-virtual {v0, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 898576
    iget-object v0, p0, LX/5KM;->b:LX/17W;

    iget-object v3, p0, LX/5KM;->a:Landroid/content/Context;

    .line 898577
    iget-object v4, v2, LX/Gdj;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->f()I

    move-result v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v2, LX/Gdj;->c:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-static {v4, v5}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 898578
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v2, v4, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 898579
    :catch_0
    move-exception v0

    .line 898580
    const-string v1, "Component_Script_Navigation"

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 898581
    iput-boolean v6, v0, LX/0VK;->d:Z

    .line 898582
    move-object v0, v0

    .line 898583
    iput v6, v0, LX/0VK;->e:I

    .line 898584
    move-object v0, v0

    .line 898585
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 898586
    iget-object v0, p0, LX/5KM;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 898587
    :cond_2
    return-void

    .line 898588
    :pswitch_0
    :try_start_1
    const-string v4, "Page"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x0

    goto/16 :goto_1

    .line 898589
    :pswitch_1
    iget-object v2, v0, LX/Gdl;->a:LX/Gdk;

    .line 898590
    new-instance v4, LX/Gdj;

    invoke-static {v2}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v3

    check-cast v3, LX/1nG;

    const/16 v0, 0x5d3

    invoke-static {v2, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-direct {v4, p1, p2, v3, v0}, LX/Gdj;-><init>(Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;LX/1nG;LX/0Ot;)V

    .line 898591
    move-object v2, v4

    .line 898592
    goto/16 :goto_2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 898593
    :cond_3
    const-string v0, ""

    goto/16 :goto_3

    .line 898594
    :cond_4
    const-string v0, "tap_profile_pic"

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x25d6af
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
