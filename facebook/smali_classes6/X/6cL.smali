.class public LX/6cL;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Ljava/lang/Boolean;

.field public final d:LX/00H;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1114301
    const-class v0, LX/6cL;

    sput-object v0, LX/6cL;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;LX/00H;Landroid/content/res/Resources;)V
    .locals 0
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/messaging/chatheads/annotations/IsChatHeadsPermittedGk;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1114302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114303
    iput-object p1, p0, LX/6cL;->b:Landroid/content/Context;

    .line 1114304
    iput-object p2, p0, LX/6cL;->c:Ljava/lang/Boolean;

    .line 1114305
    iput-object p3, p0, LX/6cL;->d:LX/00H;

    .line 1114306
    iput-object p4, p0, LX/6cL;->e:Landroid/content/res/Resources;

    .line 1114307
    return-void
.end method

.method public static b(LX/0QB;)LX/6cL;
    .locals 5

    .prologue
    .line 1114308
    new-instance v4, LX/6cL;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1114309
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v2, 0x540

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v1, v1

    .line 1114310
    check-cast v1, Ljava/lang/Boolean;

    const-class v2, LX/00H;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/00H;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v4, v0, v1, v2, v3}, LX/6cL;-><init>(Landroid/content/Context;Ljava/lang/Boolean;LX/00H;Landroid/content/res/Resources;)V

    .line 1114311
    return-object v4
.end method
