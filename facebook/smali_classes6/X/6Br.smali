.class public final LX/6Br;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "LX/6Bq;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6Bp;


# direct methods
.method public constructor <init>(LX/6Bp;)V
    .locals 0

    .prologue
    .line 1063216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063217
    iput-object p1, p0, LX/6Br;->a:LX/6Bp;

    .line 1063218
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1063219
    if-nez p1, :cond_0

    .line 1063220
    new-instance v0, Ljava/io/IOException;

    const-string v1, "responseData is not available"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1063221
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/6Br;->a:LX/6Bp;

    .line 1063222
    iget-object v1, v0, LX/6Bp;->b:Ljava/io/File;

    move-object v0, v1

    .line 1063223
    const/4 v1, 0x0

    new-array v1, v1, [LX/3AS;

    invoke-static {v0, v1}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v0

    .line 1063224
    invoke-virtual {v0, p1}, LX/3AU;->a(Ljava/io/InputStream;)J

    move-result-wide v0

    .line 1063225
    new-instance v2, LX/6Bq;

    iget-object v3, p0, LX/6Br;->a:LX/6Bp;

    invoke-direct {v2, v3, v0, v1}, LX/6Bq;-><init>(LX/6Bp;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1063226
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object v2

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0
.end method
