.class public abstract LX/6Wb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1106424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/6Wb;LX/6Wb;)LX/6Wa;
    .locals 1

    .prologue
    .line 1106425
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/6Wb;->c(Ljava/lang/String;)LX/6Wa;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)LX/6Wa;
    .locals 3

    .prologue
    .line 1106422
    new-instance v0, LX/6Wa;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6Wa;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(D)LX/6Wa;
    .locals 3

    .prologue
    .line 1106423
    new-instance v0, LX/6Wm;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6Wm;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0}, LX/6Wb;->a(LX/6Wb;LX/6Wb;)LX/6Wa;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6Wd;)LX/6Wa;
    .locals 3

    .prologue
    .line 1106421
    new-instance v0, LX/6Wa;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IN ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LX/6Wd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6Wa;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/6Wa;
    .locals 1

    .prologue
    .line 1106420
    new-instance v0, LX/6Wm;

    invoke-direct {v0, p1}, LX/6Wm;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/6Wm;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/6Wb;->c(Ljava/lang/String;)LX/6Wa;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/6Wa;
    .locals 3

    .prologue
    .line 1106419
    new-instance v0, LX/6Wa;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "strpos(lower("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "),"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, LX/6Wm;

    invoke-direct {v2, p1}, LX/6Wm;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") == 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6Wa;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
