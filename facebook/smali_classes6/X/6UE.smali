.class public final LX/6UE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1101075
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 1101076
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1101077
    :goto_0
    return v1

    .line 1101078
    :cond_0
    const-string v12, "tip_amount"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1101079
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    .line 1101080
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 1101081
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1101082
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1101083
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1101084
    const-string v12, "amount_received"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1101085
    invoke-static {p0, p1}, LX/6UB;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1101086
    :cond_2
    const-string v12, "comment"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1101087
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1101088
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1101089
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1101090
    :cond_4
    const-string v12, "tip_giver"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1101091
    invoke-static {p0, p1}, LX/6UD;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1101092
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1101093
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1101094
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 1101095
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1101096
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1101097
    if-eqz v0, :cond_7

    .line 1101098
    const/4 v1, 0x3

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1101099
    :cond_7
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1101100
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move-wide v2, v4

    move v8, v1

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1101101
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1101102
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1101103
    if-eqz v0, :cond_0

    .line 1101104
    const-string v1, "amount_received"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101105
    invoke-static {p0, v0, p2}, LX/6UB;->a(LX/15i;ILX/0nX;)V

    .line 1101106
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1101107
    if-eqz v0, :cond_1

    .line 1101108
    const-string v1, "comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101109
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1101110
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1101111
    if-eqz v0, :cond_2

    .line 1101112
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101113
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1101114
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1101115
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_3

    .line 1101116
    const-string v2, "tip_amount"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101117
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1101118
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1101119
    if-eqz v0, :cond_4

    .line 1101120
    const-string v1, "tip_giver"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1101121
    invoke-static {p0, v0, p2, p3}, LX/6UD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1101122
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1101123
    return-void
.end method
