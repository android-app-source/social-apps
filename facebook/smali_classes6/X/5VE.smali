.class public final LX/5VE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 928365
    const/4 v15, 0x0

    .line 928366
    const/4 v14, 0x0

    .line 928367
    const/4 v13, 0x0

    .line 928368
    const/4 v12, 0x0

    .line 928369
    const/4 v11, 0x0

    .line 928370
    const/4 v10, 0x0

    .line 928371
    const/4 v9, 0x0

    .line 928372
    const/4 v8, 0x0

    .line 928373
    const/4 v7, 0x0

    .line 928374
    const/4 v6, 0x0

    .line 928375
    const/4 v5, 0x0

    .line 928376
    const/4 v4, 0x0

    .line 928377
    const/4 v3, 0x0

    .line 928378
    const/4 v2, 0x0

    .line 928379
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 928380
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 928381
    const/4 v2, 0x0

    .line 928382
    :goto_0
    return v2

    .line 928383
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 928384
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 928385
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 928386
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 928387
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 928388
    const-string v17, "bubble_type"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 928389
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    goto :goto_1

    .line 928390
    :cond_2
    const-string v17, "carrier_tracking_url"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 928391
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 928392
    :cond_3
    const-string v17, "commerce_destination"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 928393
    invoke-static/range {p0 .. p1}, LX/5V0;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 928394
    :cond_4
    const-string v17, "commerce_origin"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 928395
    invoke-static/range {p0 .. p1}, LX/5V0;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 928396
    :cond_5
    const-string v17, "delayed_delivery_time_for_display"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 928397
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 928398
    :cond_6
    const-string v17, "estimated_delivery_time_for_display"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 928399
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 928400
    :cond_7
    const-string v17, "id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 928401
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 928402
    :cond_8
    const-string v17, "order_id"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 928403
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 928404
    :cond_9
    const-string v17, "retail_carrier"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 928405
    invoke-static/range {p0 .. p1}, LX/5VB;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 928406
    :cond_a
    const-string v17, "retail_shipment_items"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 928407
    invoke-static/range {p0 .. p1}, LX/5VC;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 928408
    :cond_b
    const-string v17, "service_type_description"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 928409
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 928410
    :cond_c
    const-string v17, "shipdate_for_display"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 928411
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 928412
    :cond_d
    const-string v17, "shipment_tracking_events"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 928413
    invoke-static/range {p0 .. p1}, LX/5VD;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 928414
    :cond_e
    const-string v17, "tracking_number"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 928415
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 928416
    :cond_f
    const/16 v16, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 928417
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 928418
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 928419
    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 928420
    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 928421
    const/4 v12, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 928422
    const/4 v11, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 928423
    const/4 v10, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 928424
    const/4 v9, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 928425
    const/16 v8, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 928426
    const/16 v7, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 928427
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 928428
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 928429
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 928430
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 928431
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 928432
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 928433
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 928434
    if-eqz v0, :cond_0

    .line 928435
    const-string v0, "bubble_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928436
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928437
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928438
    if-eqz v0, :cond_1

    .line 928439
    const-string v1, "carrier_tracking_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928440
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928441
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928442
    if-eqz v0, :cond_2

    .line 928443
    const-string v1, "commerce_destination"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928444
    invoke-static {p0, v0, p2}, LX/5V0;->a(LX/15i;ILX/0nX;)V

    .line 928445
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928446
    if-eqz v0, :cond_3

    .line 928447
    const-string v1, "commerce_origin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928448
    invoke-static {p0, v0, p2}, LX/5V0;->a(LX/15i;ILX/0nX;)V

    .line 928449
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928450
    if-eqz v0, :cond_4

    .line 928451
    const-string v1, "delayed_delivery_time_for_display"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928452
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928453
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928454
    if-eqz v0, :cond_5

    .line 928455
    const-string v1, "estimated_delivery_time_for_display"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928456
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928457
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928458
    if-eqz v0, :cond_6

    .line 928459
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928460
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928461
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928462
    if-eqz v0, :cond_7

    .line 928463
    const-string v1, "order_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928464
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928465
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928466
    if-eqz v0, :cond_8

    .line 928467
    const-string v1, "retail_carrier"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928468
    invoke-static {p0, v0, p2, p3}, LX/5VB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 928469
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928470
    if-eqz v0, :cond_9

    .line 928471
    const-string v1, "retail_shipment_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928472
    invoke-static {p0, v0, p2, p3}, LX/5VC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 928473
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928474
    if-eqz v0, :cond_a

    .line 928475
    const-string v1, "service_type_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928476
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928477
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928478
    if-eqz v0, :cond_b

    .line 928479
    const-string v1, "shipdate_for_display"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928480
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928481
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928482
    if-eqz v0, :cond_c

    .line 928483
    const-string v1, "shipment_tracking_events"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928484
    invoke-static {p0, v0, p2, p3}, LX/5VD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 928485
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928486
    if-eqz v0, :cond_d

    .line 928487
    const-string v1, "tracking_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928488
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928489
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 928490
    return-void
.end method
