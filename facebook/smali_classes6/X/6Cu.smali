.class public final LX/6Cu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/browserextensions/commerce/util/ResumeUrlMutationModels$CommerceExtensionResumeURLMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Cv;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/6Cw;


# direct methods
.method public constructor <init>(LX/6Cw;LX/6Cv;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1064442
    iput-object p1, p0, LX/6Cu;->c:LX/6Cw;

    iput-object p2, p0, LX/6Cu;->a:LX/6Cv;

    iput-object p3, p0, LX/6Cu;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1064443
    iget-object v0, p0, LX/6Cu;->c:LX/6Cw;

    iget-object v0, v0, LX/6Cw;->b:LX/6CH;

    const-string v1, "CommerceResumeUrlMutator"

    const-string v2, "Browser commerce save url mutation fails."

    iget-object v3, p0, LX/6Cu;->b:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, LX/6CH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1064444
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1064445
    return-void
.end method
