.class public final enum LX/6YN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6YN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6YN;

.field public static final enum BORROW_LOAN_CONFIRM:LX/6YN;

.field public static final enum BUY_CONFIRM:LX/6YN;

.field public static final enum BUY_FAILURE:LX/6YN;

.field public static final enum BUY_MAYBE:LX/6YN;

.field public static final enum BUY_SUCCESS:LX/6YN;

.field public static final enum FETCH_UPSELL:LX/6YN;

.field public static final enum PROMOS_LIST:LX/6YN;

.field public static final enum SHOW_LOAN:LX/6YN;

.field public static final enum STANDARD_DATA_CHARGES_APPLY:LX/6YN;

.field public static final enum USE_DATA_OR_STAY_IN_FREE:LX/6YN;

.field public static final enum VPN_CALL_TO_HANDLE:LX/6YN;

.field public static final enum ZERO_BALANCE_SPINNER:LX/6YN;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1109908
    new-instance v0, LX/6YN;

    const-string v1, "USE_DATA_OR_STAY_IN_FREE"

    invoke-direct {v0, v1, v3}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->USE_DATA_OR_STAY_IN_FREE:LX/6YN;

    .line 1109909
    new-instance v0, LX/6YN;

    const-string v1, "FETCH_UPSELL"

    invoke-direct {v0, v1, v4}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->FETCH_UPSELL:LX/6YN;

    .line 1109910
    new-instance v0, LX/6YN;

    const-string v1, "STANDARD_DATA_CHARGES_APPLY"

    invoke-direct {v0, v1, v5}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->STANDARD_DATA_CHARGES_APPLY:LX/6YN;

    .line 1109911
    new-instance v0, LX/6YN;

    const-string v1, "PROMOS_LIST"

    invoke-direct {v0, v1, v6}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->PROMOS_LIST:LX/6YN;

    .line 1109912
    new-instance v0, LX/6YN;

    const-string v1, "BUY_CONFIRM"

    invoke-direct {v0, v1, v7}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->BUY_CONFIRM:LX/6YN;

    .line 1109913
    new-instance v0, LX/6YN;

    const-string v1, "BUY_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->BUY_SUCCESS:LX/6YN;

    .line 1109914
    new-instance v0, LX/6YN;

    const-string v1, "BUY_MAYBE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->BUY_MAYBE:LX/6YN;

    .line 1109915
    new-instance v0, LX/6YN;

    const-string v1, "BUY_FAILURE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->BUY_FAILURE:LX/6YN;

    .line 1109916
    new-instance v0, LX/6YN;

    const-string v1, "SHOW_LOAN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->SHOW_LOAN:LX/6YN;

    .line 1109917
    new-instance v0, LX/6YN;

    const-string v1, "BORROW_LOAN_CONFIRM"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->BORROW_LOAN_CONFIRM:LX/6YN;

    .line 1109918
    new-instance v0, LX/6YN;

    const-string v1, "VPN_CALL_TO_HANDLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->VPN_CALL_TO_HANDLE:LX/6YN;

    .line 1109919
    new-instance v0, LX/6YN;

    const-string v1, "ZERO_BALANCE_SPINNER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/6YN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6YN;->ZERO_BALANCE_SPINNER:LX/6YN;

    .line 1109920
    const/16 v0, 0xc

    new-array v0, v0, [LX/6YN;

    sget-object v1, LX/6YN;->USE_DATA_OR_STAY_IN_FREE:LX/6YN;

    aput-object v1, v0, v3

    sget-object v1, LX/6YN;->FETCH_UPSELL:LX/6YN;

    aput-object v1, v0, v4

    sget-object v1, LX/6YN;->STANDARD_DATA_CHARGES_APPLY:LX/6YN;

    aput-object v1, v0, v5

    sget-object v1, LX/6YN;->PROMOS_LIST:LX/6YN;

    aput-object v1, v0, v6

    sget-object v1, LX/6YN;->BUY_CONFIRM:LX/6YN;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/6YN;->BUY_SUCCESS:LX/6YN;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6YN;->BUY_MAYBE:LX/6YN;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6YN;->BUY_FAILURE:LX/6YN;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6YN;->SHOW_LOAN:LX/6YN;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/6YN;->BORROW_LOAN_CONFIRM:LX/6YN;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/6YN;->VPN_CALL_TO_HANDLE:LX/6YN;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/6YN;->ZERO_BALANCE_SPINNER:LX/6YN;

    aput-object v2, v0, v1

    sput-object v0, LX/6YN;->$VALUES:[LX/6YN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1109921
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromInt(I)LX/6YN;
    .locals 2

    .prologue
    .line 1109922
    invoke-static {}, LX/6YN;->values()[LX/6YN;

    move-result-object v0

    .line 1109923
    if-ltz p0, :cond_0

    array-length v1, v0

    if-lt p0, v1, :cond_1

    .line 1109924
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unrecognized int value for Screen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1109925
    :cond_1
    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6YN;
    .locals 1

    .prologue
    .line 1109926
    const-class v0, LX/6YN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6YN;

    return-object v0
.end method

.method public static values()[LX/6YN;
    .locals 1

    .prologue
    .line 1109927
    sget-object v0, LX/6YN;->$VALUES:[LX/6YN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6YN;

    return-object v0
.end method
