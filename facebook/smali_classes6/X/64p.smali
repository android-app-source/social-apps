.class public final LX/64p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1045763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045764
    const-string v0, ""

    iput-object v0, p0, LX/64p;->b:Ljava/lang/String;

    .line 1045765
    const-string v0, ""

    iput-object v0, p0, LX/64p;->c:Ljava/lang/String;

    .line 1045766
    const/4 v0, -0x1

    iput v0, p0, LX/64p;->e:I

    .line 1045767
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/64p;->f:Ljava/util/List;

    .line 1045768
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045769
    return-void
.end method

.method private static a(LX/64p;Ljava/lang/String;IIZZ)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1045743
    const-string v3, " \"<>^`{}|/\\?#"

    const/4 v7, 0x1

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v4, p5

    move v6, v5

    invoke-static/range {v0 .. v7}, LX/64q;->a(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v1

    .line 1045744
    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "%2e"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1045745
    if-eqz v0, :cond_2

    .line 1045746
    :cond_1
    :goto_1
    return-void

    .line 1045747
    :cond_2
    const-string v0, ".."

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "%2e."

    .line 1045748
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, ".%2e"

    .line 1045749
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "%2e%2e"

    .line 1045750
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_3
    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1045751
    if-eqz v0, :cond_4

    .line 1045752
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    iget-object v1, p0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1045753
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1045754
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    iget-object v1, p0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1045755
    :goto_3
    goto :goto_1

    .line 1045756
    :cond_4
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    iget-object v2, p0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1045757
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    iget-object v2, p0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1045758
    :goto_4
    if-eqz p4, :cond_1

    .line 1045759
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1045760
    :cond_5
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    .line 1045761
    goto :goto_2

    .line 1045762
    :cond_8
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method private a(Ljava/lang/String;II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1045728
    if-ne p2, p3, :cond_1

    .line 1045729
    :cond_0
    return-void

    .line 1045730
    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1045731
    const/16 v1, 0x2f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5c

    if-ne v0, v1, :cond_4

    .line 1045732
    :cond_2
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1045733
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1045734
    add-int/lit8 p2, p2, 0x1

    :goto_0
    move v2, p2

    .line 1045735
    :goto_1
    if-ge v2, p3, :cond_0

    .line 1045736
    const-string v0, "/\\"

    invoke-static {p1, v2, p3, v0}, LX/65A;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v3

    .line 1045737
    if-ge v3, p3, :cond_5

    move v4, v5

    :goto_2
    move-object v0, p0

    move-object v1, p1

    .line 1045738
    invoke-static/range {v0 .. v5}, LX/64p;->a(LX/64p;Ljava/lang/String;IIZZ)V

    .line 1045739
    if-eqz v4, :cond_3

    add-int/lit8 v3, v3, 0x1

    :cond_3
    move v2, v3

    .line 1045740
    goto :goto_1

    .line 1045741
    :cond_4
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    iget-object v1, p0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1045742
    :cond_5
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;II[BI)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1045708
    move v0, p1

    move v4, p4

    .line 1045709
    :goto_0
    if-ge v0, p2, :cond_7

    .line 1045710
    array-length v2, p3

    if-ne v4, v2, :cond_0

    move v0, v1

    .line 1045711
    :goto_1
    return v0

    .line 1045712
    :cond_0
    if-eq v4, p4, :cond_2

    .line 1045713
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2e

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_1

    .line 1045714
    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    move v2, v1

    move v3, v0

    .line 1045715
    :goto_2
    if-ge v3, p2, :cond_5

    .line 1045716
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 1045717
    const/16 v6, 0x30

    if-lt v5, v6, :cond_5

    const/16 v6, 0x39

    if-gt v5, v6, :cond_5

    .line 1045718
    if-nez v2, :cond_3

    if-eq v0, v3, :cond_3

    move v0, v1

    goto :goto_1

    .line 1045719
    :cond_3
    mul-int/lit8 v2, v2, 0xa

    add-int/2addr v2, v5

    add-int/lit8 v2, v2, -0x30

    .line 1045720
    const/16 v5, 0xff

    if-le v2, v5, :cond_4

    move v0, v1

    goto :goto_1

    .line 1045721
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1045722
    :cond_5
    sub-int v0, v3, v0

    .line 1045723
    if-nez v0, :cond_6

    move v0, v1

    goto :goto_1

    .line 1045724
    :cond_6
    add-int/lit8 v0, v4, 0x1

    int-to-byte v2, v2

    aput-byte v2, p3, v4

    move v4, v0

    move v0, v3

    .line 1045725
    goto :goto_0

    .line 1045726
    :cond_7
    add-int/lit8 v0, p4, 0x4

    if-eq v4, v0, :cond_8

    move v0, v1

    goto :goto_1

    .line 1045727
    :cond_8
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private static c(Ljava/lang/String;II)I
    .locals 3

    .prologue
    .line 1045700
    const/4 v0, 0x0

    .line 1045701
    :goto_0
    if-ge p1, p2, :cond_1

    .line 1045702
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1045703
    const/16 v2, 0x5c

    if-eq v1, v2, :cond_0

    const/16 v2, 0x2f

    if-ne v1, v2, :cond_1

    .line 1045704
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 1045705
    add-int/lit8 p1, p1, 0x1

    .line 1045706
    goto :goto_0

    .line 1045707
    :cond_1
    return v0
.end method

.method private static e(Ljava/lang/String;II)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1045669
    invoke-static {p0, p1, p2, v2}, LX/64q;->a(Ljava/lang/String;IIZ)Ljava/lang/String;

    move-result-object v0

    .line 1045670
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1045671
    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 1045672
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, LX/64p;->f(Ljava/lang/String;II)Ljava/net/InetAddress;

    move-result-object v0

    .line 1045673
    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1045674
    :goto_1
    return-object v0

    .line 1045675
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0, v2, v1}, LX/64p;->f(Ljava/lang/String;II)Ljava/net/InetAddress;

    move-result-object v0

    goto :goto_0

    .line 1045676
    :cond_1
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    .line 1045677
    array-length v1, v0

    const/16 v2, 0x10

    if-ne v1, v2, :cond_9

    const/16 v10, 0x10

    const/16 v9, 0x3a

    const/4 v4, 0x0

    .line 1045678
    const/4 v3, -0x1

    move v5, v4

    move v6, v3

    move v3, v4

    .line 1045679
    :goto_2
    array-length v7, v0

    if-ge v3, v7, :cond_4

    move v8, v3

    .line 1045680
    :goto_3
    if-ge v8, v10, :cond_2

    aget-byte v7, v0, v8

    if-nez v7, :cond_2

    add-int/lit8 v7, v8, 0x1

    aget-byte v7, v0, v7

    if-nez v7, :cond_2

    .line 1045681
    add-int/lit8 v7, v8, 0x2

    move v8, v7

    goto :goto_3

    .line 1045682
    :cond_2
    sub-int v7, v8, v3

    .line 1045683
    if-le v7, v5, :cond_3

    move v5, v7

    move v6, v3

    .line 1045684
    :cond_3
    add-int/lit8 v3, v8, 0x2

    goto :goto_2

    .line 1045685
    :cond_4
    new-instance v3, LX/672;

    invoke-direct {v3}, LX/672;-><init>()V

    .line 1045686
    :cond_5
    :goto_4
    array-length v7, v0

    if-ge v4, v7, :cond_8

    .line 1045687
    if-ne v4, v6, :cond_6

    .line 1045688
    invoke-virtual {v3, v9}, LX/672;->b(I)LX/672;

    .line 1045689
    add-int/2addr v4, v5

    .line 1045690
    if-ne v4, v10, :cond_5

    invoke-virtual {v3, v9}, LX/672;->b(I)LX/672;

    goto :goto_4

    .line 1045691
    :cond_6
    if-lez v4, :cond_7

    invoke-virtual {v3, v9}, LX/672;->b(I)LX/672;

    .line 1045692
    :cond_7
    aget-byte v7, v0, v4

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    add-int/lit8 v8, v4, 0x1

    aget-byte v8, v0, v8

    and-int/lit16 v8, v8, 0xff

    or-int/2addr v7, v8

    .line 1045693
    int-to-long v7, v7

    invoke-virtual {v3, v7, v8}, LX/672;->i(J)LX/672;

    .line 1045694
    add-int/lit8 v4, v4, 0x2

    .line 1045695
    goto :goto_4

    .line 1045696
    :cond_8
    invoke-virtual {v3}, LX/672;->p()Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1045697
    goto :goto_1

    .line 1045698
    :cond_9
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1045699
    :cond_a
    invoke-static {v0}, LX/65A;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static f(Ljava/lang/String;II)Ljava/net/InetAddress;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/16 v11, 0x10

    const/4 v7, -0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 1045635
    new-array v8, v11, [B

    move v0, p1

    move v4, v7

    move v1, v7

    move v2, v5

    .line 1045636
    :goto_0
    if-ge v0, p2, :cond_2

    .line 1045637
    if-ne v2, v11, :cond_0

    move-object v0, v3

    .line 1045638
    :goto_1
    return-object v0

    .line 1045639
    :cond_0
    add-int/lit8 v6, v0, 0x2

    if-gt v6, p2, :cond_3

    const-string v6, "::"

    const/4 v9, 0x2

    invoke-virtual {p0, v0, v6, v5, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1045640
    if-eq v1, v7, :cond_1

    move-object v0, v3

    goto :goto_1

    .line 1045641
    :cond_1
    add-int/lit8 v0, v0, 0x2

    .line 1045642
    add-int/lit8 v1, v2, 0x2

    .line 1045643
    if-ne v0, p2, :cond_d

    move v2, v1

    .line 1045644
    :cond_2
    :goto_2
    if-eq v2, v11, :cond_c

    .line 1045645
    if-ne v1, v7, :cond_b

    move-object v0, v3

    goto :goto_1

    .line 1045646
    :cond_3
    if-eqz v2, :cond_4

    .line 1045647
    const-string v6, ":"

    invoke-virtual {p0, v0, v6, v5, v12}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1045648
    add-int/lit8 v0, v0, 0x1

    :cond_4
    :goto_3
    move v4, v5

    move v6, v0

    .line 1045649
    :goto_4
    if-ge v6, p2, :cond_8

    .line 1045650
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 1045651
    invoke-static {v9}, LX/64q;->a(C)I

    move-result v9

    .line 1045652
    if-eq v9, v7, :cond_8

    .line 1045653
    shl-int/lit8 v4, v4, 0x4

    add-int/2addr v4, v9

    .line 1045654
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 1045655
    :cond_5
    const-string v6, "."

    invoke-virtual {p0, v0, v6, v5, v12}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1045656
    add-int/lit8 v0, v2, -0x2

    invoke-static {p0, v4, p2, v8, v0}, LX/64p;->a(Ljava/lang/String;II[BI)Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v3

    goto :goto_1

    .line 1045657
    :cond_6
    add-int/lit8 v2, v2, 0x2

    .line 1045658
    goto :goto_2

    :cond_7
    move-object v0, v3

    .line 1045659
    goto :goto_1

    .line 1045660
    :cond_8
    sub-int v9, v6, v0

    .line 1045661
    if-eqz v9, :cond_9

    const/4 v10, 0x4

    if-le v9, v10, :cond_a

    :cond_9
    move-object v0, v3

    goto :goto_1

    .line 1045662
    :cond_a
    add-int/lit8 v9, v2, 0x1

    ushr-int/lit8 v10, v4, 0x8

    and-int/lit16 v10, v10, 0xff

    int-to-byte v10, v10

    aput-byte v10, v8, v2

    .line 1045663
    add-int/lit8 v2, v9, 0x1

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v8, v9

    move v4, v0

    move v0, v6

    .line 1045664
    goto :goto_0

    .line 1045665
    :cond_b
    sub-int v0, v2, v1

    rsub-int/lit8 v0, v0, 0x10

    sub-int v3, v2, v1

    invoke-static {v8, v1, v8, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1045666
    rsub-int/lit8 v0, v2, 0x10

    add-int/2addr v0, v1

    invoke-static {v8, v1, v0, v5}, Ljava/util/Arrays;->fill([BIIB)V

    .line 1045667
    :cond_c
    :try_start_0
    invoke-static {v8}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_1

    .line 1045668
    :catch_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_d
    move v2, v1

    goto :goto_3
.end method

.method private static g(Ljava/lang/String;II)I
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 1045499
    :try_start_0
    const-string v3, ""

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-static/range {v0 .. v7}, LX/64q;->a(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    .line 1045500
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1045501
    if-lez v0, :cond_0

    const v1, 0xffff

    if-gt v0, v1, :cond_0

    .line 1045502
    :goto_0
    return v0

    :cond_0
    move v0, v8

    .line 1045503
    goto :goto_0

    .line 1045504
    :catch_0
    move v0, v8

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1045634
    iget v0, p0, LX/64p;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/64p;->e:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/64p;->a:Ljava/lang/String;

    invoke-static {v0}, LX/64q;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(LX/64q;Ljava/lang/String;)LX/64o;
    .locals 13

    .prologue
    .line 1045559
    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p2, v0, v1}, LX/65A;->a(Ljava/lang/String;II)I

    move-result v2

    .line 1045560
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-static {p2, v2, v0}, LX/65A;->b(Ljava/lang/String;II)I

    move-result v11

    .line 1045561
    const/16 v8, 0x7a

    const/16 v7, 0x61

    const/16 v6, 0x5a

    const/16 v5, 0x41

    const/4 v0, -0x1

    .line 1045562
    sub-int v1, v11, v2

    const/4 v3, 0x2

    if-ge v1, v3, :cond_15

    .line 1045563
    :cond_0
    :goto_0
    move v0, v0

    .line 1045564
    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    .line 1045565
    const/4 v1, 0x1

    const-string v3, "https:"

    const/4 v4, 0x0

    const/4 v5, 0x6

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1045566
    const-string v0, "https"

    iput-object v0, p0, LX/64p;->a:Ljava/lang/String;

    .line 1045567
    add-int/lit8 v2, v2, 0x6

    .line 1045568
    :goto_1
    const/4 v1, 0x0

    .line 1045569
    const/4 v0, 0x0

    .line 1045570
    invoke-static {p2, v2, v11}, LX/64p;->c(Ljava/lang/String;II)I

    move-result v3

    .line 1045571
    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    if-eqz p1, :cond_1

    iget-object v4, p1, LX/64q;->b:Ljava/lang/String;

    iget-object v5, p0, LX/64p;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 1045572
    :cond_1
    add-int/2addr v2, v3

    move v8, v0

    move v9, v1

    move v1, v2

    .line 1045573
    :goto_2
    const-string v0, "@/\\?#"

    invoke-static {p2, v1, v11, v0}, LX/65A;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v10

    .line 1045574
    if-eq v10, v11, :cond_9

    .line 1045575
    invoke-virtual {p2, v10}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1045576
    :goto_3
    sparse-switch v0, :sswitch_data_0

    goto :goto_2

    .line 1045577
    :sswitch_0
    move v0, v1

    :goto_4
    if-ge v0, v10, :cond_4

    .line 1045578
    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    sparse-switch v2, :sswitch_data_1

    .line 1045579
    :cond_2
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1045580
    :cond_3
    :sswitch_1
    add-int/lit8 v0, v0, 0x1

    if-ge v0, v10, :cond_2

    .line 1045581
    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x5d

    if-ne v2, v3, :cond_3

    goto :goto_5

    :cond_4
    move v0, v10

    .line 1045582
    :sswitch_2
    move v0, v0

    .line 1045583
    add-int/lit8 v2, v0, 0x1

    if-ge v2, v10, :cond_d

    .line 1045584
    invoke-static {p2, v1, v0}, LX/64p;->e(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/64p;->d:Ljava/lang/String;

    .line 1045585
    add-int/lit8 v0, v0, 0x1

    invoke-static {p2, v0, v10}, LX/64p;->g(Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, LX/64p;->e:I

    .line 1045586
    iget v0, p0, LX/64p;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_e

    sget-object v0, LX/64o;->INVALID_PORT:LX/64o;

    .line 1045587
    :goto_6
    return-object v0

    .line 1045588
    :cond_5
    const/4 v1, 0x1

    const-string v3, "http:"

    const/4 v4, 0x0

    const/4 v5, 0x5

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1045589
    const-string v0, "http"

    iput-object v0, p0, LX/64p;->a:Ljava/lang/String;

    .line 1045590
    add-int/lit8 v2, v2, 0x5

    goto :goto_1

    .line 1045591
    :cond_6
    sget-object v0, LX/64o;->UNSUPPORTED_SCHEME:LX/64o;

    goto :goto_6

    .line 1045592
    :cond_7
    if-eqz p1, :cond_8

    .line 1045593
    iget-object v0, p1, LX/64q;->b:Ljava/lang/String;

    iput-object v0, p0, LX/64p;->a:Ljava/lang/String;

    goto :goto_1

    .line 1045594
    :cond_8
    sget-object v0, LX/64o;->MISSING_SCHEME:LX/64o;

    goto :goto_6

    .line 1045595
    :cond_9
    const/4 v0, -0x1

    goto :goto_3

    .line 1045596
    :sswitch_3
    if-nez v8, :cond_c

    .line 1045597
    const/16 v0, 0x3a

    invoke-static {p2, v1, v10, v0}, LX/65A;->a(Ljava/lang/String;IIC)I

    move-result v2

    .line 1045598
    const-string v3, " \"\':;<=>@[]^`{}|/\\?#"

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p2

    invoke-static/range {v0 .. v7}, LX/64q;->a(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    .line 1045599
    if-eqz v9, :cond_a

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/64p;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "%40"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_a
    iput-object v0, p0, LX/64p;->b:Ljava/lang/String;

    .line 1045600
    if-eq v2, v10, :cond_b

    .line 1045601
    const/4 v8, 0x1

    .line 1045602
    add-int/lit8 v1, v2, 0x1

    const-string v3, " \"\':;<=>@[]^`{}|/\\?#"

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p2

    move v2, v10

    invoke-static/range {v0 .. v7}, LX/64q;->a(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64p;->c:Ljava/lang/String;

    .line 1045603
    :cond_b
    const/4 v1, 0x1

    move v0, v8

    .line 1045604
    :goto_7
    add-int/lit8 v2, v10, 0x1

    move v8, v0

    move v9, v1

    move v1, v2

    .line 1045605
    goto/16 :goto_2

    .line 1045606
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/64p;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "%40"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v3, " \"\':;<=>@[]^`{}|/\\?#"

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v0, p2

    move v2, v10

    invoke-static/range {v0 .. v7}, LX/64q;->a(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64p;->c:Ljava/lang/String;

    move v0, v8

    move v1, v9

    goto :goto_7

    .line 1045607
    :cond_d
    invoke-static {p2, v1, v0}, LX/64p;->e(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64p;->d:Ljava/lang/String;

    .line 1045608
    iget-object v0, p0, LX/64p;->a:Ljava/lang/String;

    invoke-static {v0}, LX/64q;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/64p;->e:I

    .line 1045609
    :cond_e
    iget-object v0, p0, LX/64p;->d:Ljava/lang/String;

    if-nez v0, :cond_f

    sget-object v0, LX/64o;->INVALID_HOST:LX/64o;

    goto/16 :goto_6

    :cond_f
    move v2, v10

    .line 1045610
    :cond_10
    :goto_8
    const-string v0, "?#"

    invoke-static {p2, v2, v11, v0}, LX/65A;->a(Ljava/lang/String;IILjava/lang/String;)I

    move-result v0

    .line 1045611
    invoke-direct {p0, p2, v2, v0}, LX/64p;->a(Ljava/lang/String;II)V

    .line 1045612
    if-ge v0, v11, :cond_14

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x3f

    if-ne v1, v2, :cond_14

    .line 1045613
    const/16 v1, 0x23

    invoke-static {p2, v0, v11, v1}, LX/65A;->a(Ljava/lang/String;IIC)I

    move-result v2

    .line 1045614
    add-int/lit8 v1, v0, 0x1

    const-string v3, " \"\'<>#"

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v0, p2

    invoke-static/range {v0 .. v7}, LX/64q;->a(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/64q;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/64p;->g:Ljava/util/List;

    .line 1045615
    :goto_9
    if-ge v2, v11, :cond_11

    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_11

    .line 1045616
    add-int/lit8 v1, v2, 0x1

    const-string v3, ""

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p2

    move v2, v11

    invoke-static/range {v0 .. v7}, LX/64q;->a(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64p;->h:Ljava/lang/String;

    .line 1045617
    :cond_11
    sget-object v0, LX/64o;->SUCCESS:LX/64o;

    goto/16 :goto_6

    .line 1045618
    :cond_12
    invoke-virtual {p1}, LX/64q;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64p;->b:Ljava/lang/String;

    .line 1045619
    invoke-virtual {p1}, LX/64q;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/64p;->c:Ljava/lang/String;

    .line 1045620
    iget-object v0, p1, LX/64q;->e:Ljava/lang/String;

    iput-object v0, p0, LX/64p;->d:Ljava/lang/String;

    .line 1045621
    iget v0, p1, LX/64q;->f:I

    iput v0, p0, LX/64p;->e:I

    .line 1045622
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1045623
    iget-object v0, p0, LX/64p;->f:Ljava/util/List;

    invoke-virtual {p1}, LX/64q;->i()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1045624
    if-eq v2, v11, :cond_13

    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_10

    .line 1045625
    :cond_13
    invoke-virtual {p1}, LX/64q;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/64p;->c(Ljava/lang/String;)LX/64p;

    goto :goto_8

    :cond_14
    move v2, v0

    goto :goto_9

    .line 1045626
    :cond_15
    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1045627
    if-lt v1, v7, :cond_16

    if-le v1, v8, :cond_17

    :cond_16
    if-lt v1, v5, :cond_0

    if-gt v1, v6, :cond_0

    .line 1045628
    :cond_17
    add-int/lit8 v1, v2, 0x1

    :goto_a
    if-ge v1, v11, :cond_0

    .line 1045629
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 1045630
    if-lt v3, v7, :cond_18

    if-le v3, v8, :cond_1b

    :cond_18
    if-lt v3, v5, :cond_19

    if-le v3, v6, :cond_1b

    :cond_19
    const/16 v4, 0x30

    if-lt v3, v4, :cond_1a

    const/16 v4, 0x39

    if-le v3, v4, :cond_1b

    :cond_1a
    const/16 v4, 0x2b

    if-eq v3, v4, :cond_1b

    const/16 v4, 0x2d

    if-eq v3, v4, :cond_1b

    const/16 v4, 0x2e

    if-eq v3, v4, :cond_1b

    .line 1045631
    const/16 v4, 0x3a

    if-ne v3, v4, :cond_0

    move v0, v1

    .line 1045632
    goto/16 :goto_0

    .line 1045633
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x23 -> :sswitch_0
        0x2f -> :sswitch_0
        0x3f -> :sswitch_0
        0x40 -> :sswitch_3
        0x5c -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x3a -> :sswitch_2
        0x5b -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(I)LX/64p;
    .locals 3

    .prologue
    .line 1045556
    if-lez p1, :cond_0

    const v0, 0xffff

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected port: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045557
    :cond_1
    iput p1, p0, LX/64p;->e:I

    .line 1045558
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/64p;
    .locals 3

    .prologue
    .line 1045548
    if-nez p1, :cond_0

    .line 1045549
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "scheme == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045550
    :cond_0
    const-string v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1045551
    const-string v0, "http"

    iput-object v0, p0, LX/64p;->a:Ljava/lang/String;

    .line 1045552
    :goto_0
    return-object p0

    .line 1045553
    :cond_1
    const-string v0, "https"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1045554
    const-string v0, "https"

    iput-object v0, p0, LX/64p;->a:Ljava/lang/String;

    goto :goto_0

    .line 1045555
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected scheme: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/String;)LX/64p;
    .locals 3

    .prologue
    .line 1045543
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "host == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045544
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p1, v0, v1}, LX/64p;->e(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    .line 1045545
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected host: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045546
    :cond_1
    iput-object v0, p0, LX/64p;->d:Ljava/lang/String;

    .line 1045547
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/64p;
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1045538
    if-eqz p1, :cond_0

    const-string v1, " \"\'<>#"

    const/4 v3, 0x0

    move-object v0, p1

    move v4, v2

    move v5, v2

    .line 1045539
    invoke-static/range {v0 .. v5}, LX/64q;->a(Ljava/lang/String;Ljava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    .line 1045540
    invoke-static {v0}, LX/64q;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/64p;->g:Ljava/util/List;

    .line 1045541
    return-object p0

    .line 1045542
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/64q;
    .locals 2

    .prologue
    .line 1045535
    iget-object v0, p0, LX/64p;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "scheme == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045536
    :cond_0
    iget-object v0, p0, LX/64p;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "host == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1045537
    :cond_1
    new-instance v0, LX/64q;

    invoke-direct {v0, p0}, LX/64q;-><init>(LX/64p;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v3, 0x3a

    .line 1045505
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1045506
    iget-object v1, p0, LX/64p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045507
    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045508
    iget-object v1, p0, LX/64p;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64p;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1045509
    :cond_0
    iget-object v1, p0, LX/64p;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045510
    iget-object v1, p0, LX/64p;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1045511
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045512
    iget-object v1, p0, LX/64p;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045513
    :cond_1
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045514
    :cond_2
    iget-object v1, p0, LX/64p;->d:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    .line 1045515
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045516
    iget-object v1, p0, LX/64p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045517
    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045518
    :goto_0
    invoke-virtual {p0}, LX/64p;->a()I

    move-result v1

    .line 1045519
    iget-object v2, p0, LX/64p;->a:Ljava/lang/String;

    invoke-static {v2}, LX/64q;->a(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 1045520
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045521
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1045522
    :cond_3
    iget-object v1, p0, LX/64p;->f:Ljava/util/List;

    .line 1045523
    const/4 v2, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_4

    .line 1045524
    const/16 v2, 0x2f

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045525
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045526
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1045527
    :cond_4
    iget-object v1, p0, LX/64p;->g:Ljava/util/List;

    if-eqz v1, :cond_5

    .line 1045528
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045529
    iget-object v1, p0, LX/64p;->g:Ljava/util/List;

    invoke-static {v0, v1}, LX/64q;->b(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 1045530
    :cond_5
    iget-object v1, p0, LX/64p;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1045531
    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1045532
    iget-object v1, p0, LX/64p;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045533
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1045534
    :cond_7
    iget-object v1, p0, LX/64p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
