.class public final LX/6IV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Ib;


# direct methods
.method public constructor <init>(LX/6Ib;)V
    .locals 0

    .prologue
    .line 1074072
    iput-object p1, p0, LX/6IV;->a:LX/6Ib;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 1074073
    iget-object v0, p0, LX/6IV;->a:LX/6Ib;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/6Ib;->d(LX/6Ib;I)V

    .line 1074074
    iget-object v0, p0, LX/6IV;->a:LX/6Ib;

    iget-object v0, v0, LX/6Ib;->b:LX/6Ik;

    new-instance v1, LX/6JK;

    const/4 v2, 0x4

    const-string v3, "Couldn\'t open camera"

    invoke-direct {v1, v2, v3, p1}, LX/6JK;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, LX/6Ik;->a(Ljava/lang/Throwable;)V

    .line 1074075
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1074076
    const/4 v1, 0x1

    .line 1074077
    iget-object v0, p0, LX/6IV;->a:LX/6Ib;

    invoke-static {v0, v1}, LX/6Ib;->c(LX/6Ib;I)V

    .line 1074078
    iget-object v0, p0, LX/6IV;->a:LX/6Ib;

    .line 1074079
    iput-boolean v1, v0, LX/6Ib;->h:Z

    .line 1074080
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074081
    iget-object v1, p0, LX/6IV;->a:LX/6Ib;

    new-instance v2, LX/6IQ;

    invoke-virtual {v0}, LX/5fQ;->b()LX/5fS;

    move-result-object v3

    .line 1074082
    new-instance v4, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v4}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1074083
    iget-object p1, v0, LX/5fQ;->i:LX/5fM;

    invoke-static {p1}, LX/5fQ;->b(LX/5fM;)I

    move-result p1

    invoke-static {p1, v4}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1074084
    move-object v0, v4

    .line 1074085
    invoke-direct {v2, v3, v0}, LX/6IQ;-><init>(LX/5fS;Landroid/hardware/Camera$CameraInfo;)V

    .line 1074086
    iput-object v2, v1, LX/6Ib;->o:LX/6IP;

    .line 1074087
    iget-object v0, p0, LX/6IV;->a:LX/6Ib;

    .line 1074088
    iget-object v1, v0, LX/6Ib;->g:LX/6KN;

    if-eqz v1, :cond_0

    .line 1074089
    iget-object v1, v0, LX/6Ib;->g:LX/6KN;

    iget-object v2, v0, LX/6Ib;->a:LX/6JF;

    iget-object v3, v0, LX/6Ib;->o:LX/6IP;

    invoke-virtual {v1, v2, v3}, LX/6KN;->a(LX/6JF;LX/6IP;)V

    .line 1074090
    :cond_0
    iget-object v0, p0, LX/6IV;->a:LX/6Ib;

    iget-object v0, v0, LX/6Ib;->b:LX/6Ik;

    invoke-interface {v0}, LX/6Ik;->b()V

    .line 1074091
    return-void
.end method
