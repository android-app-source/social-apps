.class public final enum LX/62w;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/62w;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/62w;

.field public static final enum NEXT:LX/62w;

.field public static final enum PREVIOUS:LX/62w;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1042740
    new-instance v0, LX/62w;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v2}, LX/62w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62w;->NEXT:LX/62w;

    new-instance v0, LX/62w;

    const-string v1, "PREVIOUS"

    invoke-direct {v0, v1, v3}, LX/62w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/62w;->PREVIOUS:LX/62w;

    const/4 v0, 0x2

    new-array v0, v0, [LX/62w;

    sget-object v1, LX/62w;->NEXT:LX/62w;

    aput-object v1, v0, v2

    sget-object v1, LX/62w;->PREVIOUS:LX/62w;

    aput-object v1, v0, v3

    sput-object v0, LX/62w;->$VALUES:[LX/62w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1042741
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/62w;
    .locals 1

    .prologue
    .line 1042742
    const-class v0, LX/62w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/62w;

    return-object v0
.end method

.method public static values()[LX/62w;
    .locals 1

    .prologue
    .line 1042743
    sget-object v0, LX/62w;->$VALUES:[LX/62w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/62w;

    return-object v0
.end method
