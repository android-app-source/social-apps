.class public LX/67T;
.super LX/0kI;
.source ""


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1053007
    invoke-direct {p0}, LX/0kI;-><init>()V

    .line 1053008
    iput-object p1, p0, LX/67T;->a:LX/0Uh;

    .line 1053009
    return-void
.end method


# virtual methods
.method public final b(Landroid/app/Activity;ILandroid/view/KeyEvent;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Landroid/view/KeyEvent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1053010
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x52

    if-eq v0, v1, :cond_0

    .line 1053011
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1053012
    :goto_0
    return-object v0

    .line 1053013
    :cond_0
    iget-object v0, p0, LX/67T;->a:LX/0Uh;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1053014
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 1053015
    invoke-virtual {v0, v2}, Landroid/view/Window;->closePanel(I)V

    .line 1053016
    invoke-virtual {v0, v2, p3}, Landroid/view/Window;->openPanel(ILandroid/view/KeyEvent;)V

    .line 1053017
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 1053018
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method
