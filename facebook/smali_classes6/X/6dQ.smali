.class public LX/6dQ;
.super LX/1qS;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1116126
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6dQ;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/6ds;LX/6d0;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1116127
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 1116128
    const-string v0, "threads_db2"

    move-object v5, v0

    .line 1116129
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 1116130
    return-void
.end method

.method public static a(LX/0QB;)LX/6dQ;
    .locals 13

    .prologue
    .line 1116131
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1116132
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1116133
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1116134
    if-nez v1, :cond_0

    .line 1116135
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1116136
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1116137
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1116138
    sget-object v1, LX/6dQ;->a:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1116139
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1116140
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1116141
    :cond_1
    if-nez v1, :cond_4

    .line 1116142
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1116143
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1116144
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1116145
    new-instance v7, LX/6dQ;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v9

    check-cast v9, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v10

    check-cast v10, LX/1qU;

    invoke-static {v0}, LX/6ds;->a(LX/0QB;)LX/6ds;

    move-result-object v11

    check-cast v11, LX/6ds;

    invoke-static {v0}, LX/6d0;->a(LX/0QB;)LX/6d0;

    move-result-object v12

    check-cast v12, LX/6d0;

    invoke-direct/range {v7 .. v12}, LX/6dQ;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/6ds;LX/6d0;)V

    .line 1116146
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1116147
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1116148
    if-nez v1, :cond_2

    .line 1116149
    sget-object v0, LX/6dQ;->a:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1116150
    :goto_1
    if-eqz v0, :cond_3

    .line 1116151
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1116152
    :goto_3
    check-cast v0, LX/6dQ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1116153
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1116154
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1116155
    :catchall_1
    move-exception v0

    .line 1116156
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1116157
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1116158
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1116159
    :cond_2
    :try_start_8
    sget-object v0, LX/6dQ;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final f()V
    .locals 0

    .prologue
    .line 1116160
    invoke-super {p0}, LX/1qS;->h()V

    .line 1116161
    return-void
.end method
