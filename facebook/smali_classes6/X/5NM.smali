.class public final LX/5NM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:LX/5NN;


# direct methods
.method public constructor <init>(LX/5NN;)V
    .locals 0

    .prologue
    .line 906238
    iput-object p1, p0, LX/5NM;->a:LX/5NN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 906239
    iget-object v0, p0, LX/5NM;->a:LX/5NN;

    .line 906240
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 906241
    const-string v1, "ref"

    const-string p0, "dialtone_internal_settings"

    invoke-virtual {v2, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 906242
    iget-object v1, v0, LX/5NN;->b:LX/17Y;

    invoke-virtual {v0}, LX/5NN;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string p1, "dialtone://switch_to_dialtone"

    invoke-interface {v1, p0, p1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 906243
    if-nez v1, :cond_0

    .line 906244
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 906245
    const-string p0, "dialtone://switch_to_dialtone"

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 906246
    :cond_0
    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 906247
    iget-object v2, v0, LX/5NN;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object p0, v0, LX/5NN;->c:Landroid/content/Context;

    invoke-interface {v2, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 906248
    const/4 v0, 0x1

    return v0
.end method
