.class public LX/6Dx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Dw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Dw",
        "<",
        "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1065758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065759
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;
    .locals 5

    .prologue
    .line 1065760
    sget-object v0, LX/6tr;->PREPARE_CHECKOUT:LX/6tr;

    sget-object v1, LX/6tr;->VERIFY_PAYMENT_METHOD:LX/6tr;

    sget-object v2, LX/6tr;->PROCESSING_VERIFY_PAYMENT_METHOD:LX/6tr;

    sget-object v3, LX/6tr;->SEND_PAYMENT:LX/6tr;

    sget-object v4, LX/6tr;->FINISH:LX/6tr;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
