.class public LX/6Jc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field public final a:LX/6Kz;

.field public final b:LX/6LG;

.field public final c:Landroid/os/Handler;

.field public final d:Landroid/os/Handler;

.field public final e:Landroid/os/Handler;

.field public f:LX/6L8;

.field public g:LX/6L1;

.field public h:LX/6LC;

.field public i:Ljava/io/File;

.field public j:LX/6JU;

.field public k:Landroid/os/Handler;

.field public volatile l:Z

.field public final m:LX/6JS;

.field public final n:LX/6JT;


# direct methods
.method public constructor <init>(LX/6Kz;LX/6LG;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 1075587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075588
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/6Jc;->e:Landroid/os/Handler;

    .line 1075589
    new-instance v0, LX/6JS;

    invoke-direct {v0, p0}, LX/6JS;-><init>(LX/6Jc;)V

    iput-object v0, p0, LX/6Jc;->m:LX/6JS;

    .line 1075590
    new-instance v0, LX/6JT;

    invoke-direct {v0, p0}, LX/6JT;-><init>(LX/6Jc;)V

    iput-object v0, p0, LX/6Jc;->n:LX/6JT;

    .line 1075591
    iput-object p1, p0, LX/6Jc;->a:LX/6Kz;

    .line 1075592
    iput-object p2, p0, LX/6Jc;->b:LX/6LG;

    .line 1075593
    iput-object p3, p0, LX/6Jc;->c:Landroid/os/Handler;

    .line 1075594
    iput-object p4, p0, LX/6Jc;->d:Landroid/os/Handler;

    .line 1075595
    return-void
.end method

.method public static d(LX/6Jc;LX/6JU;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 1075596
    :try_start_0
    iget-object v0, p0, LX/6Jc;->f:LX/6L8;

    if-eqz v0, :cond_0

    .line 1075597
    iget-object v0, p0, LX/6Jc;->f:LX/6L8;

    invoke-virtual {v0}, LX/6L8;->a()V

    .line 1075598
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Jc;->f:LX/6L8;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075599
    invoke-static {p1, p2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    .line 1075600
    :goto_0
    return-void

    .line 1075601
    :catch_0
    move-exception v0

    .line 1075602
    invoke-static {p1, p2, v0}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
