.class public LX/5rK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[F

.field private static final b:Landroid/graphics/PointF;

.field private static final c:[F

.field private static final d:Landroid/graphics/Matrix;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1011232
    new-array v0, v1, [F

    sput-object v0, LX/5rK;->a:[F

    .line 1011233
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    sput-object v0, LX/5rK;->b:Landroid/graphics/PointF;

    .line 1011234
    new-array v0, v1, [F

    sput-object v0, LX/5rK;->c:[F

    .line 1011235
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, LX/5rK;->d:Landroid/graphics/Matrix;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1011231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(FFLandroid/view/ViewGroup;)I
    .locals 2

    .prologue
    .line 1011230
    sget-object v0, LX/5rK;->a:[F

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, LX/5rK;->a(FFLandroid/view/ViewGroup;[F[I)I

    move-result v0

    return v0
.end method

.method public static a(FFLandroid/view/ViewGroup;[F[I)I
    .locals 4
    .param p4    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1011218
    invoke-static {}, LX/5pe;->b()V

    .line 1011219
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    .line 1011220
    aput p0, p3, v2

    .line 1011221
    aput p1, p3, v3

    .line 1011222
    invoke-static {p3, p2}, LX/5rK;->a([FLandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1011223
    if-eqz v1, :cond_1

    .line 1011224
    invoke-static {v1}, LX/5rK;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 1011225
    if-eqz v1, :cond_1

    .line 1011226
    if-eqz p4, :cond_0

    .line 1011227
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    aput v0, p4, v2

    .line 1011228
    :cond_0
    aget v0, p3, v2

    aget v2, p3, v3

    invoke-static {v1, v0, v2}, LX/5rK;->a(Landroid/view/View;FF)I

    move-result v0

    .line 1011229
    :cond_1
    return v0
.end method

.method public static a(FFLandroid/view/ViewGroup;[I)I
    .locals 1
    .param p3    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1011217
    sget-object v0, LX/5rK;->a:[F

    invoke-static {p0, p1, p2, v0, p3}, LX/5rK;->a(FFLandroid/view/ViewGroup;[F[I)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/View;FF)I
    .locals 1

    .prologue
    .line 1011214
    instance-of v0, p0, LX/5r9;

    if-eqz v0, :cond_0

    .line 1011215
    check-cast p0, LX/5r9;

    invoke-interface {p0, p1, p2}, LX/5r9;->a(FF)I

    move-result v0

    .line 1011216
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1011211
    :goto_0
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v0

    if-gtz v0, :cond_0

    .line 1011212
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p0, v0

    goto :goto_0

    .line 1011213
    :cond_0
    return-object p0
.end method

.method private static a([FLandroid/view/View;)Landroid/view/View;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1011186
    instance-of v0, p1, LX/5rB;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, LX/5rB;

    invoke-interface {v0}, LX/5rB;->getPointerEvents()LX/5r3;

    move-result-object v0

    .line 1011187
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1011188
    sget-object v2, LX/5r3;->AUTO:LX/5r3;

    if-ne v0, v2, :cond_3

    .line 1011189
    sget-object v0, LX/5r3;->BOX_NONE:LX/5r3;

    .line 1011190
    :cond_0
    :goto_1
    sget-object v2, LX/5r3;->NONE:LX/5r3;

    if-ne v0, v2, :cond_4

    move-object p1, v1

    .line 1011191
    :cond_1
    :goto_2
    return-object p1

    .line 1011192
    :cond_2
    sget-object v0, LX/5r3;->AUTO:LX/5r3;

    goto :goto_0

    .line 1011193
    :cond_3
    sget-object v2, LX/5r3;->BOX_ONLY:LX/5r3;

    if-ne v0, v2, :cond_0

    .line 1011194
    sget-object v0, LX/5r3;->NONE:LX/5r3;

    goto :goto_1

    .line 1011195
    :cond_4
    sget-object v2, LX/5r3;->BOX_ONLY:LX/5r3;

    if-eq v0, v2, :cond_1

    .line 1011196
    sget-object v2, LX/5r3;->BOX_NONE:LX/5r3;

    if-ne v0, v2, :cond_7

    .line 1011197
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1011198
    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p0, v0}, LX/5rK;->a([FLandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1011199
    if-eq v0, p1, :cond_5

    move-object p1, v0

    .line 1011200
    goto :goto_2

    .line 1011201
    :cond_5
    instance-of v0, p1, LX/5r9;

    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1011202
    check-cast v0, LX/5r9;

    aget v2, p0, v3

    aget v3, p0, v4

    invoke-interface {v0, v2, v3}, LX/5r9;->a(FF)I

    move-result v0

    .line 1011203
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v0, v2, :cond_1

    :cond_6
    move-object p1, v1

    .line 1011204
    goto :goto_2

    .line 1011205
    :cond_7
    sget-object v1, LX/5r3;->AUTO:LX/5r3;

    if-ne v0, v1, :cond_9

    .line 1011206
    instance-of v0, p1, LX/5rA;

    if-eqz v0, :cond_8

    move-object v0, p1

    .line 1011207
    check-cast v0, LX/5rA;

    aget v1, p0, v3

    aget v2, p0, v4

    invoke-interface {v0, v1, v2}, LX/5rA;->b(FF)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1011208
    :cond_8
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 1011209
    check-cast p1, Landroid/view/ViewGroup;

    invoke-static {p0, p1}, LX/5rK;->a([FLandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    goto :goto_2

    .line 1011210
    :cond_9
    new-instance v1, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown pointer event type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/5r3;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static a([FLandroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1011171
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 1011172
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1011173
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1011174
    sget-object v2, LX/5rK;->b:Landroid/graphics/PointF;

    .line 1011175
    aget v3, p0, v6

    aget v4, p0, v7

    invoke-static {v3, v4, p1, v0, v2}, LX/5rK;->a(FFLandroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/PointF;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1011176
    aget v3, p0, v6

    .line 1011177
    aget v4, p0, v7

    .line 1011178
    iget v5, v2, Landroid/graphics/PointF;->x:F

    aput v5, p0, v6

    .line 1011179
    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, p0, v7

    .line 1011180
    invoke-static {p0, v0}, LX/5rK;->a([FLandroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 1011181
    if-eqz v0, :cond_1

    move-object p1, v0

    .line 1011182
    :cond_0
    return-object p1

    .line 1011183
    :cond_1
    aput v3, p0, v6

    .line 1011184
    aput v4, p0, v7

    .line 1011185
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(FFLandroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/PointF;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1011149
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p0

    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float v1, v0, v1

    .line 1011150
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, p1

    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v0, v2

    .line 1011151
    invoke-virtual {p3}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    .line 1011152
    invoke-virtual {v2}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1011153
    sget-object v5, LX/5rK;->c:[F

    .line 1011154
    aput v1, v5, v4

    .line 1011155
    aput v0, v5, v3

    .line 1011156
    sget-object v0, LX/5rK;->d:Landroid/graphics/Matrix;

    .line 1011157
    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1011158
    invoke-virtual {v0, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1011159
    aget v1, v5, v4

    .line 1011160
    aget v0, v5, v3

    move v2, v1

    move v1, v0

    .line 1011161
    :goto_0
    instance-of v0, p3, LX/5qf;

    if-eqz v0, :cond_1

    move-object v0, p3

    check-cast v0, LX/5qf;

    invoke-interface {v0}, LX/5qf;->getHitSlopRect()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, p3

    .line 1011162
    check-cast v0, LX/5qf;

    invoke-interface {v0}, LX/5qf;->getHitSlopRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 1011163
    iget v5, v0, Landroid/graphics/Rect;->left:I

    neg-int v5, v5

    int-to-float v5, v5

    cmpl-float v5, v2, v5

    if-ltz v5, :cond_0

    invoke-virtual {p3}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    move-result v6

    sub-int/2addr v5, v6

    iget v6, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    cmpg-float v5, v2, v5

    if-gez v5, :cond_0

    iget v5, v0, Landroid/graphics/Rect;->top:I

    neg-int v5, v5

    int-to-float v5, v5

    cmpl-float v5, v1, v5

    if-ltz v5, :cond_0

    invoke-virtual {p3}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v5

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    .line 1011164
    invoke-virtual {p4, v2, v1}, Landroid/graphics/PointF;->set(FF)V

    move v0, v3

    .line 1011165
    :goto_1
    return v0

    :cond_0
    move v0, v4

    .line 1011166
    goto :goto_1

    .line 1011167
    :cond_1
    cmpl-float v0, v2, v6

    if-ltz v0, :cond_2

    invoke-virtual {p3}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int/2addr v0, v5

    int-to-float v0, v0

    cmpg-float v0, v2, v0

    if-gez v0, :cond_2

    cmpl-float v0, v1, v6

    if-ltz v0, :cond_2

    invoke-virtual {p3}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    move-result v5

    sub-int/2addr v0, v5

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_2

    .line 1011168
    invoke-virtual {p4, v2, v1}, Landroid/graphics/PointF;->set(FF)V

    move v0, v3

    .line 1011169
    goto :goto_1

    :cond_2
    move v0, v4

    .line 1011170
    goto :goto_1

    :cond_3
    move v2, v1

    move v1, v0

    goto :goto_0
.end method
