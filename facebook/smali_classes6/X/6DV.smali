.class public LX/6DV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/RequestOfferCodeJSBridgeCall;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/1Bf;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Bf;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1065232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065233
    iput-object p1, p0, LX/6DV;->a:Landroid/content/Context;

    .line 1065234
    iput-object p2, p0, LX/6DV;->b:LX/1Bf;

    .line 1065235
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1065231
    const-string v0, "requestFillOfferCode"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 3

    .prologue
    .line 1065225
    check-cast p1, Lcom/facebook/browserextensions/ipc/RequestOfferCodeJSBridgeCall;

    .line 1065226
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1065227
    const-string v1, "EXTRA_OFFER_CODE_JS_BRIDGE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1065228
    iget-object v1, p0, LX/6DV;->b:LX/1Bf;

    iget-object v2, p0, LX/6DV;->a:Landroid/content/Context;

    .line 1065229
    invoke-static {v1}, LX/1Bf;->c(LX/1Bf;)Z

    move-result p0

    invoke-static {v2, p0, v0}, LX/049;->c(Landroid/content/Context;ZLandroid/os/Bundle;)V

    .line 1065230
    return-void
.end method
