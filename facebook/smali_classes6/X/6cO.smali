.class public final LX/6cO;
.super LX/0aT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0aT",
        "<",
        "LX/2UZ;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/6cO;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2UY;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2Ug;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Uc;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ud;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/2Uf;


# direct methods
.method public constructor <init>(LX/2Ug;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2Uf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ug;",
            "LX/0Ot",
            "<",
            "LX/2UY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2UZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Uc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ud;",
            ">;",
            "LX/2Uf;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1114333
    invoke-direct {p0, p3}, LX/0aT;-><init>(LX/0Ot;)V

    .line 1114334
    iput-object p2, p0, LX/6cO;->a:LX/0Ot;

    .line 1114335
    iput-object p1, p0, LX/6cO;->b:LX/2Ug;

    .line 1114336
    iput-object p4, p0, LX/6cO;->c:LX/0Ot;

    .line 1114337
    iput-object p5, p0, LX/6cO;->d:LX/0Ot;

    .line 1114338
    iput-object p6, p0, LX/6cO;->e:LX/2Uf;

    .line 1114339
    return-void
.end method

.method public static a(LX/0QB;)LX/6cO;
    .locals 10

    .prologue
    .line 1114340
    sget-object v0, LX/6cO;->f:LX/6cO;

    if-nez v0, :cond_1

    .line 1114341
    const-class v1, LX/6cO;

    monitor-enter v1

    .line 1114342
    :try_start_0
    sget-object v0, LX/6cO;->f:LX/6cO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1114343
    if-eqz v2, :cond_0

    .line 1114344
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1114345
    new-instance v3, LX/6cO;

    invoke-static {v0}, LX/2Ug;->b(LX/0QB;)LX/2Ug;

    move-result-object v4

    check-cast v4, LX/2Ug;

    const/16 v5, 0xcf8

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xcf9

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xcf7

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xcfc

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/2Uf;->b(LX/0QB;)LX/2Uf;

    move-result-object v9

    check-cast v9, LX/2Uf;

    invoke-direct/range {v3 .. v9}, LX/6cO;-><init>(LX/2Ug;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2Uf;)V

    .line 1114346
    move-object v0, v3

    .line 1114347
    sput-object v0, LX/6cO;->f:LX/6cO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1114348
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1114349
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1114350
    :cond_1
    sget-object v0, LX/6cO;->f:LX/6cO;

    return-object v0

    .line 1114351
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1114352
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1114353
    iget-object v0, p0, LX/6cO;->b:LX/2Ug;

    invoke-virtual {v0}, LX/2Ug;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1114354
    iget-object v0, p0, LX/6cO;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uc;

    invoke-virtual {v0}, LX/2Uc;->a()Z

    move-result v0

    .line 1114355
    iget-object v1, p0, LX/6cO;->e:LX/2Uf;

    .line 1114356
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "time_changed_event"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "auto_time"

    invoke-virtual {p1, p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    .line 1114357
    iget-object p2, v1, LX/2Uf;->a:LX/0Zb;

    invoke-interface {p2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1114358
    if-eqz v0, :cond_1

    .line 1114359
    iget-object v0, p0, LX/6cO;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ud;

    invoke-virtual {v0}, LX/2Ud;->d()V

    .line 1114360
    :cond_0
    :goto_0
    return-void

    .line 1114361
    :cond_1
    iget-object v0, p0, LX/6cO;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ud;

    .line 1114362
    iget-object v2, v0, LX/2Ud;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v4, v0, LX/2Ud;->d:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 1114363
    iget-wide v4, v0, LX/2Ud;->f:J

    iget-wide v6, v0, LX/2Ud;->h:J

    sub-long v6, v2, v6

    add-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, LX/2Ud;->b(J)V

    .line 1114364
    iput-wide v2, v0, LX/2Ud;->h:J

    .line 1114365
    iget-object v0, p0, LX/6cO;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2UY;

    const-wide/16 v4, 0x0

    .line 1114366
    iget-object v2, v0, LX/2UY;->d:LX/2Ug;

    invoke-virtual {v2}, LX/2Ug;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1114367
    iput-wide v4, v0, LX/2UY;->f:J

    .line 1114368
    iput-wide v4, v0, LX/2UY;->g:J

    .line 1114369
    invoke-virtual {v0}, LX/1Eg;->g()V

    .line 1114370
    :cond_2
    goto :goto_0
.end method
