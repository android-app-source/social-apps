.class public final LX/5fz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 972528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 972529
    new-instance v0, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    invoke-direct {v0, p1}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    .line 972530
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    .line 972531
    iput-object p2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 972532
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;
    .locals 3

    .prologue
    .line 972533
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string v1, "payment_value"

    .line 972534
    iget-object v2, p1, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v2, v2

    .line 972535
    invoke-virtual {v2}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 972536
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string v1, "currency"

    .line 972537
    iget-object v2, p1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v2, v2

    .line 972538
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 972539
    return-object p0
.end method

.method public final a(Ljava/lang/Long;)LX/5fz;
    .locals 2

    .prologue
    .line 972540
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string v1, "iris_seq_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 972541
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/5fz;
    .locals 2

    .prologue
    .line 972542
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string v1, "offline_threading_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 972543
    return-object p0
.end method

.method public final a(Z)LX/5fz;
    .locals 2

    .prologue
    .line 972544
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string v1, "from_payment_trigger"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 972545
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/5fz;
    .locals 2

    .prologue
    .line 972546
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string v1, "request_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 972547
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/5fz;
    .locals 2

    .prologue
    .line 972548
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 972549
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/5fz;
    .locals 2

    .prologue
    .line 972550
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string v1, "error_domain"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 972551
    return-object p0
.end method

.method public final i(Ljava/lang/String;)LX/5fz;
    .locals 1

    .prologue
    .line 972552
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    .line 972553
    iput-object p1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 972554
    return-object p0
.end method

.method public final j(Ljava/lang/String;)LX/5fz;
    .locals 2

    .prologue
    .line 972555
    iget-object v0, p0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string v1, "delta_name"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 972556
    return-object p0
.end method
