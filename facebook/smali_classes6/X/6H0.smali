.class public LX/6H0;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:Landroid/widget/ImageView;

.field public b:Landroid/widget/ImageView;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1071184
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/6H0;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1071185
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1071186
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1071187
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6H0;->c:Z

    .line 1071188
    invoke-virtual {p0}, LX/6H0;->getContext()Landroid/content/Context;

    move-result-object v0

    const p1, 0x7f0308d3

    invoke-static {v0, p1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1071189
    const v0, 0x7f0d16e6

    invoke-virtual {p0, v0}, LX/6H0;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/6H0;->a:Landroid/widget/ImageView;

    .line 1071190
    const v0, 0x7f0d16e7

    invoke-virtual {p0, v0}, LX/6H0;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/6H0;->b:Landroid/widget/ImageView;

    .line 1071191
    iget-object v0, p0, LX/6H0;->b:Landroid/widget/ImageView;

    new-instance p1, LX/6Gz;

    invoke-direct {p1, p0}, LX/6Gz;-><init>(LX/6H0;)V

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1071192
    const/4 p3, 0x0

    const/4 p2, -0x2

    .line 1071193
    invoke-virtual {p0}, LX/6H0;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b0fab

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1071194
    new-instance p1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {p1, p2, p2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1071195
    const/16 p2, 0x30

    iput p2, p1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1071196
    invoke-virtual {p1, v0, p3, v0, p3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 1071197
    invoke-virtual {p0, p1}, LX/6H0;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1071198
    return-void
.end method
