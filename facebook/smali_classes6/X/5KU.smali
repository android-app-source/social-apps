.class public final LX/5KU;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5KV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/java2js/JSValue;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 898660
    invoke-static {}, LX/5KV;->q()LX/5KV;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 898661
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 898662
    const-string v0, "CSButton"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 898663
    if-ne p0, p1, :cond_1

    .line 898664
    :cond_0
    :goto_0
    return v0

    .line 898665
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 898666
    goto :goto_0

    .line 898667
    :cond_3
    check-cast p1, LX/5KU;

    .line 898668
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 898669
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 898670
    if-eq v2, v3, :cond_0

    .line 898671
    iget-object v2, p0, LX/5KU;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/5KU;->a:Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, LX/5KU;->a:Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 898672
    goto :goto_0

    .line 898673
    :cond_4
    iget-object v2, p1, LX/5KU;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
