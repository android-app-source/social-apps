.class public LX/6Zk;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static d:Z


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/3BR;

.field private final c:LX/31d;

.field private final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1111576
    const/4 v0, 0x0

    sput-boolean v0, LX/6Zk;->d:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;LX/3BR;LX/31d;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1111569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1111570
    iput-object p1, p0, LX/6Zk;->a:Landroid/content/Context;

    .line 1111571
    iget-object v0, p2, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1111572
    iput-object v0, p0, LX/6Zk;->e:Ljava/lang/String;

    .line 1111573
    iput-object p3, p0, LX/6Zk;->b:LX/3BR;

    .line 1111574
    iput-object p4, p0, LX/6Zk;->c:LX/31d;

    .line 1111575
    return-void
.end method

.method public static b(LX/0QB;)LX/6Zk;
    .locals 5

    .prologue
    .line 1111567
    new-instance v4, LX/6Zk;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, LX/3BR;->a(LX/0QB;)LX/3BR;

    move-result-object v2

    check-cast v2, LX/3BR;

    invoke-static {p0}, LX/31d;->a(LX/0QB;)LX/31d;

    move-result-object v3

    check-cast v3, LX/31d;

    invoke-direct {v4, v0, v1, v2, v3}, LX/6Zk;-><init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;LX/3BR;LX/31d;)V

    .line 1111568
    return-object v4
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1111562
    sget-boolean v0, LX/6Zk;->d:Z

    if-nez v0, :cond_0

    .line 1111563
    iget-object v0, p0, LX/6Zk;->b:LX/3BR;

    invoke-static {v0}, LX/31U;->a(LX/3BR;)V

    .line 1111564
    iget-object v0, p0, LX/6Zk;->a:Landroid/content/Context;

    iget-object v1, p0, LX/6Zk;->e:Ljava/lang/String;

    iget-object v2, p0, LX/6Zk;->c:LX/31d;

    invoke-static {v0, v1, v2}, LX/3BU;->a(Landroid/content/Context;Ljava/lang/String;LX/31d;)V

    .line 1111565
    const/4 v0, 0x1

    sput-boolean v0, LX/6Zk;->d:Z

    .line 1111566
    :cond_0
    return-void
.end method
