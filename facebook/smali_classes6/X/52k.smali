.class public final enum LX/52k;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/52k;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/52k;

.field public static final enum CLOSE_FAILED:LX/52k;

.field public static final enum DATA_TOO_BIG:LX/52k;

.field public static final enum EOF_TOO_SOON:LX/52k;

.field public static final enum IMAGE_DEFECT:LX/52k;

.field public static final enum IMG_NOT_CONFINED:LX/52k;

.field public static final enum INVALID_IMG_DIMS:LX/52k;

.field public static final enum INVALID_SCR_DIMS:LX/52k;

.field public static final enum NOT_ENOUGH_MEM:LX/52k;

.field public static final enum NOT_GIF_FILE:LX/52k;

.field public static final enum NOT_READABLE:LX/52k;

.field public static final enum NO_COLOR_MAP:LX/52k;

.field public static final enum NO_ERROR:LX/52k;

.field public static final enum NO_FRAMES:LX/52k;

.field public static final enum NO_IMAG_DSCR:LX/52k;

.field public static final enum NO_SCRN_DSCR:LX/52k;

.field public static final enum OPEN_FAILED:LX/52k;

.field public static final enum READ_FAILED:LX/52k;

.field public static final enum UNKNOWN:LX/52k;

.field public static final enum WRONG_RECORD:LX/52k;


# instance fields
.field public final description:Ljava/lang/String;

.field private errorCode:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 826373
    new-instance v0, LX/52k;

    const-string v1, "NO_ERROR"

    const-string v2, "No error"

    invoke-direct {v0, v1, v5, v5, v2}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->NO_ERROR:LX/52k;

    .line 826374
    new-instance v0, LX/52k;

    const-string v1, "OPEN_FAILED"

    const/16 v2, 0x65

    const-string v3, "Failed to open given input"

    invoke-direct {v0, v1, v6, v2, v3}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->OPEN_FAILED:LX/52k;

    .line 826375
    new-instance v0, LX/52k;

    const-string v1, "READ_FAILED"

    const/16 v2, 0x66

    const-string v3, "Failed to read from given input"

    invoke-direct {v0, v1, v7, v2, v3}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->READ_FAILED:LX/52k;

    .line 826376
    new-instance v0, LX/52k;

    const-string v1, "NOT_GIF_FILE"

    const/16 v2, 0x67

    const-string v3, "Data is not in GIF format"

    invoke-direct {v0, v1, v8, v2, v3}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->NOT_GIF_FILE:LX/52k;

    .line 826377
    new-instance v0, LX/52k;

    const-string v1, "NO_SCRN_DSCR"

    const/16 v2, 0x68

    const-string v3, "No screen descriptor detected"

    invoke-direct {v0, v1, v9, v2, v3}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->NO_SCRN_DSCR:LX/52k;

    .line 826378
    new-instance v0, LX/52k;

    const-string v1, "NO_IMAG_DSCR"

    const/4 v2, 0x5

    const/16 v3, 0x69

    const-string v4, "No image descriptor detected"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->NO_IMAG_DSCR:LX/52k;

    .line 826379
    new-instance v0, LX/52k;

    const-string v1, "NO_COLOR_MAP"

    const/4 v2, 0x6

    const/16 v3, 0x6a

    const-string v4, "Neither global nor local color map found"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->NO_COLOR_MAP:LX/52k;

    .line 826380
    new-instance v0, LX/52k;

    const-string v1, "WRONG_RECORD"

    const/4 v2, 0x7

    const/16 v3, 0x6b

    const-string v4, "Wrong record type detected"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->WRONG_RECORD:LX/52k;

    .line 826381
    new-instance v0, LX/52k;

    const-string v1, "DATA_TOO_BIG"

    const/16 v2, 0x8

    const/16 v3, 0x6c

    const-string v4, "Number of pixels bigger than width * height"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->DATA_TOO_BIG:LX/52k;

    .line 826382
    new-instance v0, LX/52k;

    const-string v1, "NOT_ENOUGH_MEM"

    const/16 v2, 0x9

    const/16 v3, 0x6d

    const-string v4, "Failed to allocate required memory"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->NOT_ENOUGH_MEM:LX/52k;

    .line 826383
    new-instance v0, LX/52k;

    const-string v1, "CLOSE_FAILED"

    const/16 v2, 0xa

    const/16 v3, 0x6e

    const-string v4, "Failed to close given input"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->CLOSE_FAILED:LX/52k;

    .line 826384
    new-instance v0, LX/52k;

    const-string v1, "NOT_READABLE"

    const/16 v2, 0xb

    const/16 v3, 0x6f

    const-string v4, "Given file was not opened for read"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->NOT_READABLE:LX/52k;

    .line 826385
    new-instance v0, LX/52k;

    const-string v1, "IMAGE_DEFECT"

    const/16 v2, 0xc

    const/16 v3, 0x70

    const-string v4, "Image is defective, decoding aborted"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->IMAGE_DEFECT:LX/52k;

    .line 826386
    new-instance v0, LX/52k;

    const-string v1, "EOF_TOO_SOON"

    const/16 v2, 0xd

    const/16 v3, 0x71

    const-string v4, "Image EOF detected before image complete"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->EOF_TOO_SOON:LX/52k;

    .line 826387
    new-instance v0, LX/52k;

    const-string v1, "NO_FRAMES"

    const/16 v2, 0xe

    const/16 v3, 0x3e8

    const-string v4, "No frames found, at least one frame required"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->NO_FRAMES:LX/52k;

    .line 826388
    new-instance v0, LX/52k;

    const-string v1, "INVALID_SCR_DIMS"

    const/16 v2, 0xf

    const/16 v3, 0x3e9

    const-string v4, "Invalid screen size, dimensions must be positive"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->INVALID_SCR_DIMS:LX/52k;

    .line 826389
    new-instance v0, LX/52k;

    const-string v1, "INVALID_IMG_DIMS"

    const/16 v2, 0x10

    const/16 v3, 0x3ea

    const-string v4, "Invalid image size, dimensions must be positive"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->INVALID_IMG_DIMS:LX/52k;

    .line 826390
    new-instance v0, LX/52k;

    const-string v1, "IMG_NOT_CONFINED"

    const/16 v2, 0x11

    const/16 v3, 0x3eb

    const-string v4, "Image size exceeds screen size"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->IMG_NOT_CONFINED:LX/52k;

    .line 826391
    new-instance v0, LX/52k;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x12

    const/4 v3, -0x1

    const-string v4, "Unknown error"

    invoke-direct {v0, v1, v2, v3, v4}, LX/52k;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/52k;->UNKNOWN:LX/52k;

    .line 826392
    const/16 v0, 0x13

    new-array v0, v0, [LX/52k;

    sget-object v1, LX/52k;->NO_ERROR:LX/52k;

    aput-object v1, v0, v5

    sget-object v1, LX/52k;->OPEN_FAILED:LX/52k;

    aput-object v1, v0, v6

    sget-object v1, LX/52k;->READ_FAILED:LX/52k;

    aput-object v1, v0, v7

    sget-object v1, LX/52k;->NOT_GIF_FILE:LX/52k;

    aput-object v1, v0, v8

    sget-object v1, LX/52k;->NO_SCRN_DSCR:LX/52k;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/52k;->NO_IMAG_DSCR:LX/52k;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/52k;->NO_COLOR_MAP:LX/52k;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/52k;->WRONG_RECORD:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/52k;->DATA_TOO_BIG:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/52k;->NOT_ENOUGH_MEM:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/52k;->CLOSE_FAILED:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/52k;->NOT_READABLE:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/52k;->IMAGE_DEFECT:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/52k;->EOF_TOO_SOON:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/52k;->NO_FRAMES:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/52k;->INVALID_SCR_DIMS:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/52k;->INVALID_IMG_DIMS:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/52k;->IMG_NOT_CONFINED:LX/52k;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/52k;->UNKNOWN:LX/52k;

    aput-object v2, v0, v1

    sput-object v0, LX/52k;->$VALUES:[LX/52k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 826369
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 826370
    iput p3, p0, LX/52k;->errorCode:I

    .line 826371
    iput-object p4, p0, LX/52k;->description:Ljava/lang/String;

    .line 826372
    return-void
.end method

.method public static fromCode(I)LX/52k;
    .locals 5

    .prologue
    .line 826397
    invoke-static {}, LX/52k;->values()[LX/52k;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 826398
    iget v4, v0, LX/52k;->errorCode:I

    if-ne v4, p0, :cond_0

    .line 826399
    :goto_1
    return-object v0

    .line 826400
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 826401
    :cond_1
    sget-object v0, LX/52k;->UNKNOWN:LX/52k;

    .line 826402
    iput p0, v0, LX/52k;->errorCode:I

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/52k;
    .locals 1

    .prologue
    .line 826395
    const-class v0, LX/52k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/52k;

    return-object v0
.end method

.method public static values()[LX/52k;
    .locals 1

    .prologue
    .line 826396
    sget-object v0, LX/52k;->$VALUES:[LX/52k;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/52k;

    return-object v0
.end method


# virtual methods
.method public final getErrorCode()I
    .locals 1

    .prologue
    .line 826394
    iget v0, p0, LX/52k;->errorCode:I

    return v0
.end method

.method public final getFormattedDescription()Ljava/lang/String;
    .locals 5

    .prologue
    .line 826393
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "GifError %d: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, LX/52k;->errorCode:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/52k;->description:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
