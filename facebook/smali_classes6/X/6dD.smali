.class public LX/6dD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/01T;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0WJ;

.field private final e:LX/03V;


# direct methods
.method public constructor <init>(LX/01T;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0WJ;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/01T;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/0WJ;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1115641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1115642
    iput-object p1, p0, LX/6dD;->a:LX/01T;

    .line 1115643
    iput-object p2, p0, LX/6dD;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1115644
    iput-object p3, p0, LX/6dD;->c:LX/0Or;

    .line 1115645
    iput-object p4, p0, LX/6dD;->d:LX/0WJ;

    .line 1115646
    iput-object p5, p0, LX/6dD;->e:LX/03V;

    .line 1115647
    return-void
.end method

.method private a()V
    .locals 12

    .prologue
    const/4 v10, 0x0

    .line 1115616
    iget-object v0, p0, LX/6dD;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1115617
    sget-object v1, LX/2Iu;->a:LX/0U1;

    .line 1115618
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1115619
    const-string v2, "server_muted_until"

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v11

    .line 1115620
    const-wide/16 v8, 0x0

    .line 1115621
    :try_start_0
    const-string v1, "properties"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v4, LX/2Iu;->b:LX/0U1;

    .line 1115622
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1115623
    aput-object v4, v2, v3

    invoke-virtual {v11}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 1115624
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1115625
    const/4 v1, 0x0

    invoke-interface {v4, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    .line 1115626
    :try_start_2
    const-string v1, "properties"

    invoke-virtual {v11}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-wide v0, v2

    :goto_0
    move-object v10, v4

    .line 1115627
    :goto_1
    :try_start_3
    iget-object v2, p0, LX/6dD;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0db;->aD:LX/0Tn;

    invoke-interface {v2, v3, v0, v1}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1115628
    if-eqz v10, :cond_0

    .line 1115629
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1115630
    :cond_0
    return-void

    .line 1115631
    :catch_0
    move-exception v0

    move-object v1, v10

    .line 1115632
    :goto_2
    :try_start_4
    iget-object v2, p0, LX/6dD;->e:LX/03V;

    const-string v3, "ServerMutedUntilPostUpgradeInit"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Caught exception when migrating server mute settings. Migrating with: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object v10, v1

    move-wide v0, v8

    goto :goto_1

    .line 1115633
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v10, :cond_1

    .line 1115634
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 1115635
    :catchall_1
    move-exception v0

    move-object v10, v4

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v10, v1

    goto :goto_3

    .line 1115636
    :catch_1
    move-exception v0

    move-object v1, v4

    goto :goto_2

    :catch_2
    move-exception v0

    move-wide v8, v2

    move-object v1, v4

    goto :goto_2

    :cond_2
    move-wide v0, v8

    goto :goto_0
.end method


# virtual methods
.method public final init()V
    .locals 2

    .prologue
    .line 1115637
    iget-object v0, p0, LX/6dD;->a:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/6dD;->d:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1115638
    :cond_0
    :goto_0
    return-void

    .line 1115639
    :cond_1
    iget-object v0, p0, LX/6dD;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->aD:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1115640
    invoke-direct {p0}, LX/6dD;->a()V

    goto :goto_0
.end method
