.class public final LX/59Z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 852589
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 852590
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 852591
    :goto_0
    return v1

    .line 852592
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 852593
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 852594
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 852595
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 852596
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 852597
    const-string v5, "message"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 852598
    invoke-static {p0, p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 852599
    :cond_2
    const-string v5, "request_id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 852600
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 852601
    :cond_3
    const-string v5, "translation_id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 852602
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 852603
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 852604
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 852605
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 852606
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 852607
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 852608
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 852609
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852610
    if-eqz v0, :cond_0

    .line 852611
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852612
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 852613
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 852614
    if-eqz v0, :cond_1

    .line 852615
    const-string v1, "request_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852616
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 852617
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 852618
    if-eqz v0, :cond_2

    .line 852619
    const-string v1, "translation_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852620
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 852621
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 852622
    return-void
.end method
