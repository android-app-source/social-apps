.class public LX/6Es;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/RequestCloseBrowserJSBridgeCall;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1066723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066724
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1066725
    const-string v0, "requestCloseBrowser"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 2

    .prologue
    .line 1066726
    check-cast p1, Lcom/facebook/browserextensions/ipc/RequestCloseBrowserJSBridgeCall;

    .line 1066727
    invoke-virtual {p1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v0

    .line 1066728
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1066729
    const-string p0, "callbackID"

    invoke-virtual {v1, p0, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066730
    move-object v0, v1

    .line 1066731
    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    .line 1066732
    return-void
.end method
