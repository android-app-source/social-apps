.class public final LX/54A;
.super LX/549;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/549",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 827757
    invoke-direct {p0, p1}, LX/549;-><init>(I)V

    .line 827758
    return-void
.end method


# virtual methods
.method public final isEmpty()Z
    .locals 6

    .prologue
    .line 827754
    iget-wide v4, p0, LX/546;->g:J

    move-wide v0, v4

    .line 827755
    iget-wide v4, p0, LX/544;->e:J

    move-wide v2, v4

    .line 827756
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 827759
    if-nez p1, :cond_0

    .line 827760
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null is not a valid element"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 827761
    :cond_0
    iget-object v0, p0, LX/53v;->d:[Ljava/lang/Object;

    .line 827762
    iget-wide v8, p0, LX/544;->e:J

    move-wide v2, v8

    .line 827763
    invoke-virtual {p0, v2, v3}, LX/53v;->a(J)J

    move-result-wide v4

    .line 827764
    invoke-static {v0, v4, v5}, LX/53v;->b([Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 827765
    iget-wide v8, p0, LX/546;->g:J

    move-wide v6, v8

    .line 827766
    sub-long v6, v2, v6

    long-to-int v1, v6

    .line 827767
    iget v6, p0, LX/53v;->b:I

    if-ne v1, v6, :cond_1

    .line 827768
    const/4 v0, 0x0

    .line 827769
    :goto_0
    return v0

    .line 827770
    :cond_1
    invoke-static {v0, v4, v5}, LX/53v;->b([Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 827771
    :cond_2
    invoke-static {v0, v4, v5, p1}, LX/53v;->a([Ljava/lang/Object;JLjava/lang/Object;)V

    .line 827772
    const-wide/16 v0, 0x1

    add-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, LX/544;->d(J)V

    .line 827773
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final peek()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 827752
    iget-wide v2, p0, LX/546;->g:J

    move-wide v0, v2

    .line 827753
    invoke-virtual {p0, v0, v1}, LX/53v;->a(J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/53v;->c(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final poll()Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 827739
    iget-wide v8, p0, LX/548;->g:J

    move-wide v2, v8

    .line 827740
    :cond_0
    iget-wide v8, p0, LX/546;->g:J

    move-wide v4, v8

    .line 827741
    cmp-long v1, v4, v2

    if-ltz v1, :cond_2

    .line 827742
    iget-wide v8, p0, LX/544;->e:J

    move-wide v6, v8

    .line 827743
    cmp-long v1, v4, v6

    if-ltz v1, :cond_1

    .line 827744
    :goto_0
    return-object v0

    .line 827745
    :cond_1
    iput-wide v6, p0, LX/548;->g:J

    .line 827746
    :cond_2
    const-wide/16 v6, 0x1

    add-long/2addr v6, v4

    invoke-virtual {p0, v4, v5, v6, v7}, LX/546;->a(JJ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 827747
    invoke-virtual {p0, v4, v5}, LX/53v;->a(J)J

    move-result-wide v2

    .line 827748
    iget-object v4, p0, LX/53v;->d:[Ljava/lang/Object;

    .line 827749
    invoke-static {v4, v2, v3}, LX/53v;->a([Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v1

    .line 827750
    invoke-static {v4, v2, v3, v0}, LX/53v;->b([Ljava/lang/Object;JLjava/lang/Object;)V

    move-object v0, v1

    .line 827751
    goto :goto_0
.end method

.method public final size()I
    .locals 8

    .prologue
    .line 827733
    iget-wide v6, p0, LX/546;->g:J

    move-wide v0, v6

    .line 827734
    :goto_0
    iget-wide v6, p0, LX/544;->e:J

    move-wide v4, v6

    .line 827735
    iget-wide v6, p0, LX/546;->g:J

    move-wide v2, v6

    .line 827736
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 827737
    sub-long v0, v4, v2

    long-to-int v0, v0

    return v0

    :cond_0
    move-wide v0, v2

    .line 827738
    goto :goto_0
.end method
