.class public LX/6FE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6FE;


# instance fields
.field public a:Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;

.field public b:Lcom/facebook/payments/checkout/model/CheckoutData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1067559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1067560
    return-void
.end method

.method public static a(LX/0QB;)LX/6FE;
    .locals 3

    .prologue
    .line 1067547
    sget-object v0, LX/6FE;->c:LX/6FE;

    if-nez v0, :cond_1

    .line 1067548
    const-class v1, LX/6FE;

    monitor-enter v1

    .line 1067549
    :try_start_0
    sget-object v0, LX/6FE;->c:LX/6FE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1067550
    if-eqz v2, :cond_0

    .line 1067551
    :try_start_1
    new-instance v0, LX/6FE;

    invoke-direct {v0}, LX/6FE;-><init>()V

    .line 1067552
    move-object v0, v0

    .line 1067553
    sput-object v0, LX/6FE;->c:LX/6FE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1067554
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1067555
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1067556
    :cond_1
    sget-object v0, LX/6FE;->c:LX/6FE;

    return-object v0

    .line 1067557
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1067558
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c()V
    .locals 1

    .prologue
    .line 1067545
    const/4 v0, 0x0

    iput-object v0, p0, LX/6FE;->b:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1067546
    return-void
.end method
