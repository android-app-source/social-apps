.class public LX/6QL;
.super Landroid/widget/BaseExpandableListAdapter;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[F

.field private static final c:Landroid/graphics/ColorMatrixColorFilter;

.field public static final d:Z


# instance fields
.field public final e:Landroid/content/pm/PackageManager;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6QK;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6QM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1087344
    const-class v0, LX/6QL;

    sput-object v0, LX/6QL;->a:Ljava/lang/Class;

    .line 1087345
    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, LX/6QL;->b:[F

    .line 1087346
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    sget-object v1, LX/6QL;->b:[F

    invoke-direct {v0, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>([F)V

    sput-object v0, LX/6QL;->c:Landroid/graphics/ColorMatrixColorFilter;

    .line 1087347
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/6QL;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1087348
    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f000000    # 0.5f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f000000    # 0.5f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/pm/PackageManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1087349
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 1087350
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6QL;->f:Ljava/util/List;

    .line 1087351
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/6QL;->g:Ljava/util/List;

    .line 1087352
    iput-object p1, p0, LX/6QL;->e:Landroid/content/pm/PackageManager;

    .line 1087353
    return-void
.end method

.method private a(I)LX/6QK;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1087354
    iget-object v0, p0, LX/6QL;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6QK;

    return-object v0
.end method

.method public static a(LX/0QB;)LX/6QL;
    .locals 2

    .prologue
    .line 1087355
    new-instance v1, LX/6QL;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    invoke-direct {v1, v0}, LX/6QL;-><init>(Landroid/content/pm/PackageManager;)V

    .line 1087356
    move-object v0, v1

    .line 1087357
    return-object v0
.end method

.method private a(II)Landroid/content/pm/PermissionInfo;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1087358
    iget-object v0, p0, LX/6QL;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6QK;

    iget-object v0, v0, LX/6QK;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PermissionInfo;

    return-object v0
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 1087359
    iget-object v0, p0, LX/6QL;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1087360
    iget-object v0, p0, LX/6QL;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    return v0

    .line 1087361
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Z
    .locals 1

    .prologue
    .line 1087362
    iget-object v0, p0, LX/6QL;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic getChild(II)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1087342
    invoke-direct {p0, p1, p2}, LX/6QL;->a(II)Landroid/content/pm/PermissionInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getChildId(II)J
    .locals 2

    .prologue
    .line 1087343
    int-to-long v0, p2

    return-wide v0
.end method

.method public final getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1087327
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1087328
    if-nez p4, :cond_0

    .line 1087329
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1087330
    const v1, 0x7f0300ee

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 1087331
    :cond_0
    const v0, 0x7f0d055a

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1087332
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1087333
    invoke-direct {p0, p1}, LX/6QL;->c(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1087334
    invoke-direct {p0, p1, p2}, LX/6QL;->a(II)Landroid/content/pm/PermissionInfo;

    move-result-object v1

    .line 1087335
    iget-object v2, p0, LX/6QL;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v2}, Landroid/content/pm/PermissionInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1087336
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1087337
    return-object v0

    .line 1087338
    :cond_1
    iget-object v1, p0, LX/6QL;->g:Ljava/util/List;

    invoke-direct {p0, p1}, LX/6QL;->b(I)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6QM;

    .line 1087339
    iget-object v2, v1, LX/6QM;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1087340
    iget-object p0, v1, LX/6QM;->d:Landroid/view/View$OnClickListener;

    move-object v1, p0

    .line 1087341
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v1, v2

    goto :goto_0
.end method

.method public final getChildrenCount(I)I
    .locals 1

    .prologue
    .line 1087324
    invoke-direct {p0, p1}, LX/6QL;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1087325
    iget-object v0, p0, LX/6QL;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6QK;

    iget-object v0, v0, LX/6QK;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1087326
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final synthetic getGroup(I)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1087323
    invoke-direct {p0, p1}, LX/6QL;->a(I)LX/6QK;

    move-result-object v0

    return-object v0
.end method

.method public final getGroupCount()I
    .locals 2

    .prologue
    .line 1087322
    iget-object v0, p0, LX/6QL;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/6QL;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final getGroupId(I)J
    .locals 2

    .prologue
    .line 1087321
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 1087287
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 1087288
    if-nez p3, :cond_0

    .line 1087289
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1087290
    const v1, 0x7f0300ed

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 1087291
    :cond_0
    const v0, 0x7f0d0559

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1087292
    const v1, 0x7f0d0558

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1087293
    invoke-direct {p0, p1}, LX/6QL;->c(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1087294
    invoke-direct {p0, p1}, LX/6QL;->a(I)LX/6QK;

    move-result-object v2

    .line 1087295
    iget-object v3, v2, LX/6QK;->a:Landroid/content/pm/PermissionGroupInfo;

    if-eqz v3, :cond_2

    .line 1087296
    iget-object v3, v2, LX/6QK;->a:Landroid/content/pm/PermissionGroupInfo;

    iget-object v4, p0, LX/6QL;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v4}, Landroid/content/pm/PermissionGroupInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1087297
    iget-object v2, v2, LX/6QK;->a:Landroid/content/pm/PermissionGroupInfo;

    iget-object v4, p0, LX/6QL;->e:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v4}, Landroid/content/pm/PermissionGroupInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1087298
    :goto_0
    if-eqz v2, :cond_1

    .line 1087299
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1087300
    sget-object v4, LX/6QL;->c:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1087301
    :cond_1
    const/4 p4, 0x3

    const/4 p1, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1087302
    sget-boolean v4, LX/6QL;->d:Z

    if-eqz v4, :cond_4

    .line 1087303
    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1087304
    :goto_1
    aget-object p0, v4, p1

    if-eqz p2, :cond_5

    move v5, v6

    :goto_2
    invoke-virtual {p0, v5}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 1087305
    sget-boolean v5, LX/6QL;->d:Z

    if-eqz v5, :cond_6

    .line 1087306
    aget-object v5, v4, v7

    aget-object v6, v4, v6

    aget-object v7, v4, p1

    aget-object v4, v4, p4

    invoke-virtual {v0, v5, v6, v7, v4}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1087307
    :goto_3
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1087308
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1087309
    return-object p3

    .line 1087310
    :cond_2
    const v2, 0x7f081ea7

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1087311
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0219e0

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_0

    .line 1087312
    :cond_3
    iget-object v2, p0, LX/6QL;->g:Ljava/util/List;

    invoke-direct {p0, p1}, LX/6QL;->b(I)I

    move-result v3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6QM;

    .line 1087313
    iget-object v3, v2, LX/6QM;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1087314
    :try_start_0
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 1087315
    iget v6, v2, LX/6QM;->c:I

    move v2, v6

    .line 1087316
    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1087317
    :catch_0
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0219e0

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_0

    .line 1087318
    :cond_4
    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v4

    goto :goto_1

    :cond_5
    move v5, v7

    .line 1087319
    goto :goto_2

    .line 1087320
    :cond_6
    aget-object v5, v4, v7

    aget-object v6, v4, v6

    aget-object v7, v4, p1

    aget-object v4, v4, p4

    invoke-virtual {v0, v5, v6, v7, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_3
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1087286
    const/4 v0, 0x0

    return v0
.end method

.method public final isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 1087285
    const/4 v0, 0x0

    return v0
.end method
