.class public final LX/5Py;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 912016
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_a

    .line 912017
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 912018
    :goto_0
    return v1

    .line 912019
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 912020
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 912021
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 912022
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 912023
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 912024
    const-string v9, "__type__"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "__typename"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 912025
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 912026
    :cond_3
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 912027
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 912028
    :cond_4
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 912029
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 912030
    :cond_5
    const-string v9, "page"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 912031
    invoke-static {p0, p1}, LX/5Px;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 912032
    :cond_6
    const-string v9, "redirection_info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 912033
    invoke-static {p0, p1}, LX/5Q3;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 912034
    :cond_7
    const-string v9, "tag"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 912035
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 912036
    :cond_8
    const-string v9, "url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 912037
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 912038
    :cond_9
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 912039
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 912040
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 912041
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 912042
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 912043
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 912044
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 912045
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 912046
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 912047
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 912048
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 912049
    if-eqz v0, :cond_0

    .line 912050
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912051
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 912052
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 912053
    if-eqz v0, :cond_1

    .line 912054
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912055
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 912056
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 912057
    if-eqz v0, :cond_2

    .line 912058
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912059
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 912060
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 912061
    if-eqz v0, :cond_3

    .line 912062
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912063
    invoke-static {p0, v0, p2}, LX/5Px;->a(LX/15i;ILX/0nX;)V

    .line 912064
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 912065
    if-eqz v0, :cond_4

    .line 912066
    const-string v1, "redirection_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912067
    invoke-static {p0, v0, p2, p3}, LX/5Q3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 912068
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 912069
    if-eqz v0, :cond_5

    .line 912070
    const-string v1, "tag"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912071
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 912072
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 912073
    if-eqz v0, :cond_6

    .line 912074
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 912075
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 912076
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 912077
    return-void
.end method
