.class public LX/6Wr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/6Wr;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/6Wq;

.field private final c:LX/0lC;


# direct methods
.method public constructor <init>(LX/0Or;LX/6Wq;LX/0lC;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/6Wq;",
            "LX/0lC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1106589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106590
    iput-object p1, p0, LX/6Wr;->a:LX/0Or;

    .line 1106591
    iput-object p2, p0, LX/6Wr;->b:LX/6Wq;

    .line 1106592
    iput-object p3, p0, LX/6Wr;->c:LX/0lC;

    .line 1106593
    return-void
.end method

.method public static a(LX/0QB;)LX/6Wr;
    .locals 6

    .prologue
    .line 1106576
    sget-object v0, LX/6Wr;->d:LX/6Wr;

    if-nez v0, :cond_1

    .line 1106577
    const-class v1, LX/6Wr;

    monitor-enter v1

    .line 1106578
    :try_start_0
    sget-object v0, LX/6Wr;->d:LX/6Wr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1106579
    if-eqz v2, :cond_0

    .line 1106580
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1106581
    new-instance v5, LX/6Wr;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/6Wq;->a(LX/0QB;)LX/6Wq;

    move-result-object v3

    check-cast v3, LX/6Wq;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-direct {v5, p0, v3, v4}, LX/6Wr;-><init>(LX/0Or;LX/6Wq;LX/0lC;)V

    .line 1106582
    move-object v0, v5

    .line 1106583
    sput-object v0, LX/6Wr;->d:LX/6Wr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1106584
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1106585
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1106586
    :cond_1
    sget-object v0, LX/6Wr;->d:LX/6Wr;

    return-object v0

    .line 1106587
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1106588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6Wd;Ljava/lang/Class;Lcom/facebook/common/callercontext/CallerContext;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/6Wd;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1106567
    iget-object v0, p0, LX/6Wr;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/6Wr;->b:LX/6Wq;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, p3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1106568
    invoke-virtual {v0}, LX/0lF;->c()LX/15w;

    move-result-object v1

    .line 1106569
    :try_start_0
    iget-object v0, p0, LX/6Wr;->c:LX/0lC;

    invoke-virtual {v1, v0}, LX/15w;->a(LX/0lD;)V

    .line 1106570
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    .line 1106571
    invoke-virtual {v1}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v0, v2, :cond_0

    .line 1106572
    invoke-virtual {v1}, LX/15w;->c()LX/15z;

    .line 1106573
    :cond_0
    invoke-virtual {v1, p2}, LX/15w;->b(Ljava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Iterator;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1106574
    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1pX;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public final a(LX/6Wd;LX/0QK;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/6Wd;",
            "LX/0QK",
            "<",
            "LX/0lF;",
            "TT;>;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 1106575
    iget-object v0, p0, LX/6Wr;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v1, p0, LX/6Wr;->b:LX/6Wq;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, p3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
