.class public final LX/5it;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 985070
    const/4 v5, 0x0

    .line 985071
    const-wide/16 v6, 0x0

    .line 985072
    const/4 v4, 0x0

    .line 985073
    const-wide/16 v2, 0x0

    .line 985074
    const/4 v1, 0x0

    .line 985075
    const/4 v0, 0x0

    .line 985076
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 985077
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 985078
    const/4 v0, 0x0

    .line 985079
    :goto_0
    return v0

    .line 985080
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v4, :cond_5

    .line 985081
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 985082
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 985083
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_0

    if-eqz v0, :cond_0

    .line 985084
    const-string v4, "horizontal_alignment"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 985085
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    move v10, v0

    goto :goto_1

    .line 985086
    :cond_1
    const-string v4, "horizontal_margin"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 985087
    const/4 v0, 0x1

    .line 985088
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 985089
    :cond_2
    const-string v4, "vertical_alignment"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 985090
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 985091
    :cond_3
    const-string v4, "vertical_margin"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 985092
    const/4 v0, 0x1

    .line 985093
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v6, v0

    move-wide v8, v4

    goto :goto_1

    .line 985094
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 985095
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 985096
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 985097
    if-eqz v1, :cond_6

    .line 985098
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 985099
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 985100
    if-eqz v6, :cond_7

    .line 985101
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 985102
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_8
    move-wide v8, v2

    move v10, v5

    move-wide v2, v6

    move v7, v4

    move v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 985103
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 985104
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 985105
    if-eqz v0, :cond_0

    .line 985106
    const-string v0, "horizontal_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985107
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 985108
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 985109
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 985110
    const-string v2, "horizontal_margin"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985111
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 985112
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 985113
    if-eqz v0, :cond_2

    .line 985114
    const-string v0, "vertical_alignment"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985115
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 985116
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 985117
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 985118
    const-string v2, "vertical_margin"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 985119
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 985120
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 985121
    return-void
.end method
