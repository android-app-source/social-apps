.class public LX/6Qa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Q9;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile s:LX/6Qa;


# instance fields
.field public final b:LX/0s6;

.field public final c:LX/0Uh;

.field public final d:LX/6nZ;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/1sd;

.field public final g:LX/6QL;

.field public final h:LX/03V;

.field public final i:Ljava/util/concurrent/ExecutorService;

.field public final j:LX/17W;

.field public final k:LX/0kb;

.field private final l:LX/2y8;

.field public final m:LX/6Qw;

.field public final n:LX/2v6;

.field private final o:LX/0gh;

.field public final p:LX/0W3;

.field public final q:LX/6VP;

.field public r:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1087450
    const-class v0, LX/6Qa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Qa;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0s6;LX/0Uh;LX/6nZ;Lcom/facebook/content/SecureContextHelper;LX/1sd;LX/03V;Ljava/util/concurrent/ExecutorService;LX/6QL;LX/17W;LX/0kb;LX/2y8;LX/6Qw;LX/2v6;LX/0gh;LX/0W3;LX/6VP;)V
    .locals 1
    .param p7    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1087674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1087675
    iput-object p1, p0, LX/6Qa;->b:LX/0s6;

    .line 1087676
    iput-object p2, p0, LX/6Qa;->c:LX/0Uh;

    .line 1087677
    iput-object p3, p0, LX/6Qa;->d:LX/6nZ;

    .line 1087678
    iput-object p4, p0, LX/6Qa;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1087679
    iput-object p5, p0, LX/6Qa;->f:LX/1sd;

    .line 1087680
    iput-object p6, p0, LX/6Qa;->h:LX/03V;

    .line 1087681
    iput-object p7, p0, LX/6Qa;->i:Ljava/util/concurrent/ExecutorService;

    .line 1087682
    iput-object p8, p0, LX/6Qa;->g:LX/6QL;

    .line 1087683
    iput-object p9, p0, LX/6Qa;->j:LX/17W;

    .line 1087684
    iput-object p10, p0, LX/6Qa;->k:LX/0kb;

    .line 1087685
    iput-object p11, p0, LX/6Qa;->l:LX/2y8;

    .line 1087686
    iput-object p12, p0, LX/6Qa;->m:LX/6Qw;

    .line 1087687
    iput-object p13, p0, LX/6Qa;->n:LX/2v6;

    .line 1087688
    iput-object p14, p0, LX/6Qa;->o:LX/0gh;

    .line 1087689
    move-object/from16 v0, p15

    iput-object v0, p0, LX/6Qa;->p:LX/0W3;

    .line 1087690
    move-object/from16 v0, p16

    iput-object v0, p0, LX/6Qa;->q:LX/6VP;

    .line 1087691
    return-void
.end method

.method public static a(LX/0QB;)LX/6Qa;
    .locals 3

    .prologue
    .line 1087664
    sget-object v0, LX/6Qa;->s:LX/6Qa;

    if-nez v0, :cond_1

    .line 1087665
    const-class v1, LX/6Qa;

    monitor-enter v1

    .line 1087666
    :try_start_0
    sget-object v0, LX/6Qa;->s:LX/6Qa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1087667
    if-eqz v2, :cond_0

    .line 1087668
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/6Qa;->b(LX/0QB;)LX/6Qa;

    move-result-object v0

    sput-object v0, LX/6Qa;->s:LX/6Qa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1087669
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1087670
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1087671
    :cond_1
    sget-object v0, LX/6Qa;->s:LX/6Qa;

    return-object v0

    .line 1087672
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1087673
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6Qa;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1087663
    new-instance v0, LX/6QT;

    invoke-direct {v0, p0, p1}, LX/6QT;-><init>(LX/6Qa;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(LX/47G;Ljava/lang/String;IILjava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/47G;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1087654
    invoke-direct {p0, p1}, LX/6Qa;->f(LX/47G;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1087655
    iget-object v0, p0, LX/6Qa;->f:LX/1sd;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/1sd;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1087656
    iget-object v0, p0, LX/6Qa;->d:LX/6nZ;

    const/4 v5, 0x0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move-object v4, p5

    invoke-virtual/range {v0 .. v5}, LX/6nZ;->a(Ljava/lang/String;IILjava/util/Set;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1087657
    :goto_0
    return-object v0

    .line 1087658
    :cond_0
    iget-object v0, p0, LX/6Qa;->m:LX/6Qw;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sdk_check_failed  AppManager version:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6Qa;->f:LX/1sd;

    invoke-virtual {v2}, LX/1sd;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Installer version:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/6Qa;->f:LX/1sd;

    .line 1087659
    sget-object v3, LX/1ZP;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/1sd;->a(LX/1sd;Ljava/lang/String;)I

    move-result v3

    move v2, v3

    .line 1087660
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/6Qw;->a(LX/47G;Ljava/lang/String;)V

    .line 1087661
    :cond_1
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 1087662
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "neko_di_install_failure_gated"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method

.method public static synthetic a(LX/6Qa;Landroid/content/Context;LX/47G;)V
    .locals 12

    .prologue
    .line 1087636
    iget-object v0, p0, LX/6Qa;->n:LX/2v6;

    .line 1087637
    iget-object v5, p2, LX/47G;->g:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 1087638
    iget-object v5, p2, LX/47G;->g:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 1087639
    :goto_0
    move-object v0, v5

    .line 1087640
    if-nez v0, :cond_0

    .line 1087641
    iget-object v0, p0, LX/6Qa;->h:LX/03V;

    sget-object v1, LX/6Qa;->a:Ljava/lang/String;

    const-string v2, "Could not find install id for story, cannot cancel."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087642
    :goto_1
    return-void

    .line 1087643
    :cond_0
    iget-object v1, p0, LX/6Qa;->d:LX/6nZ;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    new-instance v0, Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-direct {v0, v4}, Landroid/os/Bundle;-><init>(I)V

    .line 1087644
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v8

    .line 1087645
    new-instance v6, LX/6nX;

    move-object v7, v1

    move-wide v9, v2

    move-object v11, v0

    invoke-direct/range {v6 .. v11}, LX/6nX;-><init>(LX/6nZ;Lcom/google/common/util/concurrent/SettableFuture;JLandroid/os/Bundle;)V

    .line 1087646
    invoke-virtual {v1, v8, v6}, LX/6nY;->a(Lcom/google/common/util/concurrent/SettableFuture;Landroid/content/ServiceConnection;)V

    .line 1087647
    new-instance v5, LX/6nb;

    invoke-direct {v5, v8}, LX/6nb;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;)V

    move-object v0, v5

    .line 1087648
    new-instance v1, LX/6QO;

    invoke-direct {v1, p0, p2}, LX/6QO;-><init>(LX/6Qa;LX/47G;)V

    iget-object v2, p0, LX/6Qa;->i:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 1087649
    :cond_1
    iget-object v5, p2, LX/47G;->i:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/2v6;->a(Ljava/lang/String;)LX/6Qc;

    move-result-object v5

    .line 1087650
    if-eqz v5, :cond_2

    .line 1087651
    iget-wide v7, v5, LX/6Qc;->h:J

    move-wide v5, v7

    .line 1087652
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 1087653
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/6Qa;Landroid/app/Dialog;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1087692
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1087693
    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    .line 1087694
    :cond_0
    iget-object v0, p0, LX/6Qa;->r:Landroid/content/Context;

    instance-of v0, v0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    if-eqz v0, :cond_1

    .line 1087695
    iget-object v1, p0, LX/6Qa;->r:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    .line 1087696
    iget-object v0, p0, LX/6Qa;->o:LX/0gh;

    const-string v2, "neko_di_dialog"

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, LX/0gh;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1087697
    :cond_1
    iget-object v0, p0, LX/6Qa;->r:Landroid/content/Context;

    instance-of v0, v0, Lcom/facebook/directinstall/feed/InstallDialogActivity;

    if-eqz v0, :cond_2

    .line 1087698
    iget-object v0, p0, LX/6Qa;->r:Landroid/content/Context;

    check-cast v0, Lcom/facebook/directinstall/feed/InstallDialogActivity;

    invoke-virtual {v0}, Lcom/facebook/directinstall/feed/InstallDialogActivity;->finish()V

    .line 1087699
    :cond_2
    return-void
.end method

.method public static a$redex0(LX/6Qa;Landroid/content/Context;LX/47G;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/47G;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1087629
    iget-object v0, p0, LX/6Qa;->n:LX/2v6;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->PENDING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    iget-object v3, p2, LX/47G;->i:Ljava/lang/String;

    iget-object v4, p2, LX/47G;->e:Ljava/lang/String;

    iget-object v5, p2, LX/47G;->l:Ljava/lang/String;

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2v6;->a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6VZ;)V

    .line 1087630
    instance-of v0, p1, Lcom/facebook/directinstall/feed/InstallDialogActivity;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1087631
    check-cast v0, Lcom/facebook/directinstall/feed/InstallDialogActivity;

    invoke-virtual {v0}, Lcom/facebook/directinstall/feed/InstallDialogActivity;->finish()V

    .line 1087632
    :cond_0
    iget-object v0, p0, LX/6Qa;->n:LX/2v6;

    invoke-virtual {v0, p2, p5}, LX/2v6;->a(LX/47G;Ljava/util/Map;)LX/6Qc;

    move-result-object v6

    .line 1087633
    iget-object v2, p2, LX/47G;->e:Ljava/lang/String;

    iget v3, p2, LX/47G;->k:I

    new-instance v5, Ljava/util/HashSet;

    iget-object v0, p2, LX/47G;->c:LX/0Px;

    invoke-direct {v5, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object v0, p0

    move-object v1, p2

    move v4, p6

    invoke-direct/range {v0 .. v5}, LX/6Qa;->a(LX/47G;Ljava/lang/String;IILjava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 1087634
    new-instance v0, LX/6QP;

    move-object v1, p0

    move-object v2, v6

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, LX/6QP;-><init>(LX/6Qa;LX/6Qc;Landroid/content/Context;LX/47G;Ljava/util/Map;Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v1, p0, LX/6Qa;->i:Ljava/util/concurrent/ExecutorService;

    invoke-static {v8, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1087635
    return-void
.end method

.method private static b(LX/0QB;)LX/6Qa;
    .locals 17

    .prologue
    .line 1087627
    new-instance v0, LX/6Qa;

    invoke-static/range {p0 .. p0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v1

    check-cast v1, LX/0s6;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/6nZ;->a(LX/0QB;)LX/6nZ;

    move-result-object v3

    check-cast v3, LX/6nZ;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/1sb;->a(LX/0QB;)LX/1sd;

    move-result-object v5

    check-cast v5, LX/1sd;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/6QL;->a(LX/0QB;)LX/6QL;

    move-result-object v8

    check-cast v8, LX/6QL;

    invoke-static/range {p0 .. p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v10

    check-cast v10, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/2y8;->a(LX/0QB;)LX/2y8;

    move-result-object v11

    check-cast v11, LX/2y8;

    invoke-static/range {p0 .. p0}, LX/6Qw;->a(LX/0QB;)LX/6Qw;

    move-result-object v12

    check-cast v12, LX/6Qw;

    invoke-static/range {p0 .. p0}, LX/2v6;->a(LX/0QB;)LX/2v6;

    move-result-object v13

    check-cast v13, LX/2v6;

    invoke-static/range {p0 .. p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v14

    check-cast v14, LX/0gh;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v15

    check-cast v15, LX/0W3;

    invoke-static/range {p0 .. p0}, LX/6VP;->a(LX/0QB;)LX/6VP;

    move-result-object v16

    check-cast v16, LX/6VP;

    invoke-direct/range {v0 .. v16}, LX/6Qa;-><init>(LX/0s6;LX/0Uh;LX/6nZ;Lcom/facebook/content/SecureContextHelper;LX/1sd;LX/03V;Ljava/util/concurrent/ExecutorService;LX/6QL;LX/17W;LX/0kb;LX/2y8;LX/6Qw;LX/2v6;LX/0gh;LX/0W3;LX/6VP;)V

    .line 1087628
    return-object v0
.end method

.method public static c(LX/6Qa;LX/47G;)V
    .locals 7

    .prologue
    .line 1087571
    const/4 v0, 0x0

    .line 1087572
    iget-object v1, p0, LX/6Qa;->k:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1087573
    iget-object v1, p1, LX/47G;->t:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    .line 1087574
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->ANY:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/6Qa;->k:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->v()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1087575
    :cond_1
    move v0, v0

    .line 1087576
    if-eqz v0, :cond_2

    .line 1087577
    iget-object v1, p0, LX/6Qa;->r:Landroid/content/Context;

    iget-object v3, p1, LX/47G;->o:Ljava/lang/String;

    iget-object v4, p1, LX/47G;->r:Landroid/os/Bundle;

    iget-object v5, p1, LX/47G;->s:Ljava/util/Map;

    invoke-static {p1}, LX/6Qa;->g(LX/47G;)I

    move-result v6

    move-object v0, p0

    move-object v2, p1

    invoke-static/range {v0 .. v6}, LX/6Qa;->a$redex0(LX/6Qa;Landroid/content/Context;LX/47G;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;I)V

    .line 1087578
    iget-object v0, p0, LX/6Qa;->m:LX/6Qw;

    const-string v1, "neko_di_accept_dialog_event"

    const-string v2, "permissions"

    invoke-virtual {v0, v1, v2, p1}, LX/6Qw;->a(Ljava/lang/String;Ljava/lang/String;LX/47G;)V

    .line 1087579
    :goto_0
    return-void

    .line 1087580
    :cond_2
    iget-object v0, p0, LX/6Qa;->k:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1087581
    iget-object v0, p0, LX/6Qa;->r:Landroid/content/Context;

    .line 1087582
    new-instance v1, LX/6Qy;

    invoke-direct {v1, v0}, LX/6Qy;-><init>(Landroid/content/Context;)V

    .line 1087583
    iget-object v2, p1, LX/47G;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/net/Uri;)V

    .line 1087584
    iget-object v2, p1, LX/47G;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/6Qy;->setTitle(Ljava/lang/CharSequence;)V

    .line 1087585
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/6Qy;->setCancelable(Z)V

    .line 1087586
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/6Qy;->setCanceledOnTouchOutside(Z)V

    .line 1087587
    iget-object v2, p1, LX/47G;->t:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    .line 1087588
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->WIFI_FORCE:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    if-ne v2, v3, :cond_4

    .line 1087589
    const/4 v4, 0x1

    .line 1087590
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081e8c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6Qy;->c(Ljava/lang/CharSequence;)V

    .line 1087591
    iget-object v2, p1, LX/47G;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Ljava/lang/CharSequence;)V

    .line 1087592
    const v2, 0x7f081e91

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(I)V

    .line 1087593
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Z)V

    .line 1087594
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Z)V

    .line 1087595
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->c(Z)V

    .line 1087596
    new-instance v2, LX/6QX;

    invoke-direct {v2, p0, v0, p1}, LX/6QX;-><init>(LX/6Qa;Landroid/content/Context;LX/47G;)V

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/view/View$OnClickListener;)V

    .line 1087597
    :goto_1
    invoke-virtual {v1}, LX/6Qy;->show()V

    .line 1087598
    iget-object v1, p0, LX/6Qa;->m:LX/6Qw;

    const-string v2, "neko_di_show_dialog"

    const-string v3, "network"

    invoke-virtual {v1, v2, v3, p1}, LX/6Qw;->a(Ljava/lang/String;Ljava/lang/String;LX/47G;)V

    .line 1087599
    goto :goto_0

    .line 1087600
    :cond_3
    iget-object v0, p0, LX/6Qa;->r:Landroid/content/Context;

    const/4 v4, 0x1

    .line 1087601
    new-instance v1, LX/6Qy;

    invoke-direct {v1, v0}, LX/6Qy;-><init>(Landroid/content/Context;)V

    .line 1087602
    iget-object v2, p1, LX/47G;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/net/Uri;)V

    .line 1087603
    iget-object v2, p1, LX/47G;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/6Qy;->setTitle(Ljava/lang/CharSequence;)V

    .line 1087604
    invoke-virtual {v1, v4}, LX/6Qy;->setCancelable(Z)V

    .line 1087605
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/6Qy;->setCanceledOnTouchOutside(Z)V

    .line 1087606
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081e8e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6Qy;->c(Ljava/lang/CharSequence;)V

    .line 1087607
    const v2, 0x7f081e8f

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(I)V

    .line 1087608
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Z)V

    .line 1087609
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Z)V

    .line 1087610
    iget-object v2, p1, LX/47G;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Ljava/lang/CharSequence;)V

    .line 1087611
    new-instance v2, LX/6QW;

    invoke-direct {v2, p0, v1, p1}, LX/6QW;-><init>(LX/6Qa;LX/6Qy;LX/47G;)V

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/view/View$OnClickListener;)V

    .line 1087612
    invoke-virtual {v1}, LX/6Qy;->show()V

    .line 1087613
    iget-object v1, p0, LX/6Qa;->m:LX/6Qw;

    const-string v2, "neko_di_show_dialog"

    const-string v3, "network"

    invoke-virtual {v1, v2, v3, p1}, LX/6Qw;->a(Ljava/lang/String;Ljava/lang/String;LX/47G;)V

    .line 1087614
    goto/16 :goto_0

    .line 1087615
    :cond_4
    const/4 v4, 0x1

    .line 1087616
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081e8d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6Qy;->c(Ljava/lang/CharSequence;)V

    .line 1087617
    const v2, 0x7f081e90

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(I)V

    .line 1087618
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Z)V

    .line 1087619
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Z)V

    .line 1087620
    const v2, 0x7f081e91

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->b(I)V

    .line 1087621
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->c(Z)V

    .line 1087622
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->d(Z)V

    .line 1087623
    iget-object v2, p1, LX/47G;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Ljava/lang/CharSequence;)V

    .line 1087624
    new-instance v2, LX/6QY;

    invoke-direct {v2, p0, v0, p1}, LX/6QY;-><init>(LX/6Qa;Landroid/content/Context;LX/47G;)V

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/view/View$OnClickListener;)V

    .line 1087625
    new-instance v2, LX/6QZ;

    invoke-direct {v2, p0, v0, p1}, LX/6QZ;-><init>(LX/6Qa;Landroid/content/Context;LX/47G;)V

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Landroid/view/View$OnClickListener;)V

    .line 1087626
    goto/16 :goto_1
.end method

.method private f(LX/47G;)Z
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1087570
    invoke-virtual {p1}, LX/47G;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/47G;->d:LX/0Px;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;->FB_ANDROID_STORE:Lcom/facebook/graphql/enums/GraphQLDigitalGoodStoreType;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Qa;->l:LX/2y8;

    invoke-virtual {v0}, LX/2y8;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/47G;)I
    .locals 2

    .prologue
    .line 1087567
    sget-object v0, LX/6QQ;->b:[I

    iget-object v1, p0, LX/47G;->t:Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLAppStoreDownloadConnectivityPolicy;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1087568
    const/4 v0, 0x2

    :goto_0
    return v0

    .line 1087569
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/directinstall/intent/DirectInstallAppData;Ljava/util/Map;Z)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/directinstall/intent/DirectInstallAppData;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1087451
    iget-object v0, p0, LX/6Qa;->r:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/6Qa;->r:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    move-object v1, v0

    .line 1087452
    :goto_0
    iget-object v0, p0, LX/6Qa;->o:LX/0gh;

    const-string v3, "neko_di_dialog"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, LX/0gh;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 1087453
    invoke-static {p2, p3}, LX/2yB;->a(Lcom/facebook/directinstall/intent/DirectInstallAppData;Ljava/util/Map;)LX/47G;

    move-result-object v0

    .line 1087454
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/47G;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, LX/6Qa;->f(LX/47G;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/6Qa;->f:LX/1sd;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, LX/1sd;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move v0, v6

    .line 1087455
    :goto_1
    return v0

    :cond_1
    move-object v1, v2

    .line 1087456
    goto :goto_0

    .line 1087457
    :cond_2
    iget-object v1, v0, LX/47G;->i:Ljava/lang/String;

    if-nez v1, :cond_3

    move v0, v6

    .line 1087458
    goto :goto_1

    .line 1087459
    :cond_3
    iput-object p1, p0, LX/6Qa;->r:Landroid/content/Context;

    .line 1087460
    iget-object v1, p0, LX/6Qa;->n:LX/2v6;

    iget-object v2, v0, LX/47G;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2v6;->a(Ljava/lang/String;)LX/6Qc;

    move-result-object v1

    .line 1087461
    iget-object v2, p0, LX/6Qa;->n:LX/2v6;

    iget-object v3, v0, LX/47G;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2v6;->c(Ljava/lang/String;)LX/6Qf;

    move-result-object v2

    .line 1087462
    if-eqz v1, :cond_4

    .line 1087463
    iget-boolean v3, v1, LX/6Qc;->j:Z

    move v3, v3

    .line 1087464
    if-nez v3, :cond_5

    :cond_4
    if-eqz v2, :cond_6

    iget-boolean v3, v2, LX/6Qf;->d:Z

    if-eqz v3, :cond_6

    .line 1087465
    :cond_5
    iget-wide v2, v2, LX/6Qf;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/47G;->a(LX/47G;Ljava/lang/String;)LX/47G;

    move-result-object v0

    const/4 v4, 0x1

    .line 1087466
    new-instance v1, LX/6Qy;

    invoke-direct {v1, p1}, LX/6Qy;-><init>(Landroid/content/Context;)V

    .line 1087467
    iget-object v2, v0, LX/47G;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/net/Uri;)V

    .line 1087468
    iget-object v2, v0, LX/47G;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/6Qy;->setTitle(Ljava/lang/CharSequence;)V

    .line 1087469
    invoke-virtual {v1, v4}, LX/6Qy;->setCancelable(Z)V

    .line 1087470
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/6Qy;->setCanceledOnTouchOutside(Z)V

    .line 1087471
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081e93

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/47G;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6Qy;->c(Ljava/lang/CharSequence;)V

    .line 1087472
    const v2, 0x7f081e94

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(I)V

    .line 1087473
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Z)V

    .line 1087474
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Z)V

    .line 1087475
    const v2, 0x7f081e95

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->b(I)V

    .line 1087476
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->c(Z)V

    .line 1087477
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->d(Z)V

    .line 1087478
    iget-object v2, v0, LX/47G;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Ljava/lang/CharSequence;)V

    .line 1087479
    new-instance v2, LX/6QU;

    invoke-direct {v2, p0, v1, v0}, LX/6QU;-><init>(LX/6Qa;LX/6Qy;LX/47G;)V

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/view/View$OnClickListener;)V

    .line 1087480
    new-instance v2, LX/6QV;

    invoke-direct {v2, p0, v1, p1, v0}, LX/6QV;-><init>(LX/6Qa;LX/6Qy;Landroid/content/Context;LX/47G;)V

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Landroid/view/View$OnClickListener;)V

    .line 1087481
    invoke-virtual {v1}, LX/6Qy;->show()V

    .line 1087482
    iget-object v1, p0, LX/6Qa;->m:LX/6Qw;

    const-string v2, "neko_di_show_dialog"

    const-string v3, "cancel"

    invoke-virtual {v1, v2, v3, v0}, LX/6Qw;->a(Ljava/lang/String;Ljava/lang/String;LX/47G;)V

    .line 1087483
    move v0, v7

    .line 1087484
    goto/16 :goto_1

    .line 1087485
    :cond_6
    const/4 v2, 0x1

    .line 1087486
    sget-object v3, LX/6QQ;->a:[I

    iget-object v4, v0, LX/47G;->n:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1087487
    if-eqz v1, :cond_17

    :goto_2
    :pswitch_0
    move v1, v2

    .line 1087488
    if-nez v1, :cond_15

    .line 1087489
    if-eqz p4, :cond_7

    .line 1087490
    invoke-static {p0, v0}, LX/6Qa;->c(LX/6Qa;LX/47G;)V

    move v0, v7

    .line 1087491
    goto/16 :goto_1

    .line 1087492
    :cond_7
    const/4 v4, 0x1

    .line 1087493
    iget-object v1, p0, LX/6Qa;->g:LX/6QL;

    if-eqz v1, :cond_13

    .line 1087494
    const/4 v8, 0x0

    .line 1087495
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1087496
    if-eqz v0, :cond_9

    .line 1087497
    iget-object v2, v0, LX/47G;->p:Ljava/lang/String;

    .line 1087498
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1087499
    new-instance v3, LX/6QM;

    iget-object v5, p0, LX/6Qa;->r:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081e98

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v2}, LX/6Qa;->a(LX/6Qa;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-direct {v3, v5, v2, v8, v6}, LX/6QM;-><init>(Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1087500
    :cond_8
    iget-object v2, v0, LX/47G;->q:Ljava/lang/String;

    .line 1087501
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 1087502
    new-instance v3, LX/6QM;

    iget-object v5, p0, LX/6Qa;->r:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f081e97

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v2}, LX/6Qa;->a(LX/6Qa;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-direct {v3, v5, v2, v8, v6}, LX/6QM;-><init>(Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnClickListener;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1087503
    :cond_9
    move-object v1, v1

    .line 1087504
    iget-object v2, p0, LX/6Qa;->g:LX/6QL;

    iget-object v3, v0, LX/47G;->c:LX/0Px;

    .line 1087505
    iget-object v5, v2, LX/6QL;->f:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 1087506
    iget-object v5, v2, LX/6QL;->g:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 1087507
    if-eqz v3, :cond_11

    .line 1087508
    iget-object v5, v2, LX/6QL;->f:Ljava/util/List;

    .line 1087509
    invoke-static {}, LX/0PM;->f()Ljava/util/TreeMap;

    move-result-object v8

    .line 1087510
    new-instance v9, LX/6QK;

    const/4 v6, 0x0

    invoke-direct {v9, v6}, LX/6QK;-><init>(Landroid/content/pm/PermissionGroupInfo;)V

    .line 1087511
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_a
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1087512
    :try_start_0
    iget-object p2, v2, LX/6QL;->e:Landroid/content/pm/PackageManager;

    const/4 p3, 0x0

    invoke-virtual {p2, v6, p3}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object p2

    .line 1087513
    iget-object p3, p2, Landroid/content/pm/PermissionInfo;->group:Ljava/lang/String;

    .line 1087514
    if-nez p3, :cond_c

    .line 1087515
    iget v6, p2, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    if-eqz v6, :cond_b

    iget v6, p2, Landroid/content/pm/PermissionInfo;->protectionLevel:I

    const/4 p3, 0x1

    if-ne v6, p3, :cond_a

    .line 1087516
    :cond_b
    iget-object v6, v9, LX/6QK;->b:Ljava/util/List;

    invoke-interface {v6, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1087517
    :catch_0
    goto :goto_3

    .line 1087518
    :cond_c
    invoke-interface {v8, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/6QK;

    .line 1087519
    if-nez v6, :cond_d

    .line 1087520
    iget-object v6, v2, LX/6QL;->e:Landroid/content/pm/PackageManager;

    const/4 p4, 0x0

    invoke-virtual {v6, p3, p4}, Landroid/content/pm/PackageManager;->getPermissionGroupInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionGroupInfo;

    move-result-object p4

    .line 1087521
    new-instance v6, LX/6QK;

    invoke-direct {v6, p4}, LX/6QK;-><init>(Landroid/content/pm/PermissionGroupInfo;)V

    .line 1087522
    invoke-interface {v8, p3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1087523
    :cond_d
    iget-object v6, v6, LX/6QK;->b:Ljava/util/List;

    invoke-interface {v6, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1087524
    :cond_e
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object p1

    .line 1087525
    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_f

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/6QK;

    .line 1087526
    invoke-interface {p1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1087527
    :cond_f
    new-instance v6, LX/6QJ;

    invoke-direct {v6, v2}, LX/6QJ;-><init>(LX/6QL;)V

    invoke-static {p1, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1087528
    iget-object v6, v9, LX/6QK;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_10

    .line 1087529
    invoke-interface {p1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1087530
    :cond_10
    move-object v6, p1

    .line 1087531
    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1087532
    :cond_11
    if-eqz v1, :cond_12

    .line 1087533
    iget-object v5, v2, LX/6QL;->g:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1087534
    :cond_12
    invoke-virtual {v2}, LX/6QL;->notifyDataSetChanged()V

    .line 1087535
    :cond_13
    new-instance v1, LX/6R0;

    iget-object v2, p0, LX/6Qa;->r:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/6R0;-><init>(Landroid/content/Context;)V

    .line 1087536
    iget-object v2, v0, LX/47G;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/6R0;->setTitle(Ljava/lang/CharSequence;)V

    .line 1087537
    invoke-virtual {v1, v4}, LX/6R0;->setCancelable(Z)V

    .line 1087538
    iget-object v2, v0, LX/47G;->b:Ljava/lang/String;

    .line 1087539
    iget-object v3, v1, LX/6R0;->g:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1087540
    iget-object v5, v1, LX/6R0;->g:Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_18

    const/16 v3, 0x8

    :goto_5
    invoke-virtual {v5, v3}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->setVisibility(I)V

    .line 1087541
    invoke-virtual {v1, v4}, LX/6R0;->setCanceledOnTouchOutside(Z)V

    .line 1087542
    iget-object v2, p0, LX/6Qa;->g:LX/6QL;

    .line 1087543
    iget-object v3, v1, LX/6R0;->f:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v2}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 1087544
    sget-object v2, LX/6Qz;->LOADED:LX/6Qz;

    invoke-virtual {v1, v2}, LX/6R0;->a(LX/6Qz;)V

    .line 1087545
    iget-object v2, v0, LX/47G;->f:Ljava/lang/String;

    .line 1087546
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_19

    .line 1087547
    iget-object v3, v1, Lcom/facebook/directinstall/ui/InstallDialog;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1087548
    iget-object v3, v1, Lcom/facebook/directinstall/ui/InstallDialog;->d:Lcom/facebook/resources/ui/FbTextView;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1087549
    :goto_6
    const v2, 0x7f081e8b

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(I)V

    .line 1087550
    iget-object v2, v0, LX/47G;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/net/Uri;)V

    .line 1087551
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Z)V

    .line 1087552
    invoke-virtual {v1, v4}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Z)V

    .line 1087553
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->c(Z)V

    .line 1087554
    iget-object v2, v0, LX/47G;->m:Ljava/lang/String;

    if-eqz v2, :cond_14

    .line 1087555
    iget-object v2, v0, LX/47G;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->b(Ljava/lang/CharSequence;)V

    .line 1087556
    :cond_14
    new-instance v2, LX/6QR;

    invoke-direct {v2, p0, v0}, LX/6QR;-><init>(LX/6Qa;LX/47G;)V

    invoke-virtual {v1, v2}, LX/6R0;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1087557
    new-instance v2, LX/6QS;

    invoke-direct {v2, p0, v1, v0}, LX/6QS;-><init>(LX/6Qa;LX/6R0;LX/47G;)V

    invoke-virtual {v1, v2}, Lcom/facebook/directinstall/ui/InstallDialog;->a(Landroid/view/View$OnClickListener;)V

    .line 1087558
    invoke-virtual {v1}, LX/6R0;->show()V

    .line 1087559
    iget-object v1, p0, LX/6Qa;->m:LX/6Qw;

    const-string v2, "neko_di_show_dialog"

    const-string v3, "permissions"

    invoke-virtual {v1, v2, v3, v0}, LX/6Qw;->a(Ljava/lang/String;Ljava/lang/String;LX/47G;)V

    .line 1087560
    move v0, v7

    .line 1087561
    goto/16 :goto_1

    .line 1087562
    :cond_15
    iget-object v0, p0, LX/6Qa;->r:Landroid/content/Context;

    instance-of v0, v0, Lcom/facebook/directinstall/feed/InstallDialogActivity;

    if-eqz v0, :cond_16

    .line 1087563
    iget-object v0, p0, LX/6Qa;->r:Landroid/content/Context;

    check-cast v0, Lcom/facebook/directinstall/feed/InstallDialogActivity;

    invoke-virtual {v0}, Lcom/facebook/directinstall/feed/InstallDialogActivity;->finish()V

    :cond_16
    move v0, v6

    .line 1087564
    goto/16 :goto_1

    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1087565
    :cond_18
    const/4 v3, 0x0

    goto :goto_5

    .line 1087566
    :cond_19
    iget-object v3, v1, Lcom/facebook/directinstall/ui/InstallDialog;->d:Lcom/facebook/resources/ui/FbTextView;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
