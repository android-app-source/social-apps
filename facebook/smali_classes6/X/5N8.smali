.class public LX/5N8;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:LX/0yc;

.field private final c:LX/396;


# direct methods
.method public constructor <init>(LX/0yc;LX/396;)V
    .locals 1

    .prologue
    .line 905917
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 905918
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/5N8;->a:Landroid/graphics/Paint;

    .line 905919
    iput-object p1, p0, LX/5N8;->b:LX/0yc;

    .line 905920
    iput-object p2, p0, LX/5N8;->c:LX/396;

    .line 905921
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/16 v9, 0xff

    const/16 v2, 0xd7

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 905922
    iget-object v0, p0, LX/5N8;->b:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 905923
    :goto_0
    return-void

    .line 905924
    :cond_0
    invoke-virtual {p0}, LX/5N8;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 905925
    iget-object v0, p0, LX/5N8;->b:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/5N8;->c:LX/396;

    iget-object v0, v0, LX/396;->c:LX/397;

    sget-object v1, LX/397;->VIDEO:LX/397;

    invoke-virtual {v0, v1}, LX/397;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 905926
    iget-object v0, p0, LX/5N8;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 905927
    iget-object v0, p0, LX/5N8;->a:Landroid/graphics/Paint;

    const/16 v1, 0x33

    invoke-static {v1, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 905928
    :goto_1
    iget v0, v6, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    iget v0, v6, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, LX/5N8;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 905929
    iget v0, v6, Landroid/graphics/Rect;->left:I

    iget v1, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v8

    .line 905930
    iget v1, v6, Landroid/graphics/Rect;->top:I

    iget v2, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v8

    .line 905931
    iget-object v2, p0, LX/5N8;->a:Landroid/graphics/Paint;

    invoke-static {v9, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 905932
    iget-object v2, p0, LX/5N8;->b:LX/0yc;

    iget v3, v6, Landroid/graphics/Rect;->right:I

    iget v4, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, v6, Landroid/graphics/Rect;->bottom:I

    iget v5, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget-object v5, p0, LX/5N8;->c:LX/396;

    invoke-virtual {v2, v3, v4, v5}, LX/0yc;->a(FFLX/396;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 905933
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v8

    sub-float/2addr v0, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v8

    sub-float/2addr v1, v3

    iget-object v3, p0, LX/5N8;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 905934
    :cond_1
    iget-object v0, p0, LX/5N8;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 905935
    iget-object v0, p0, LX/5N8;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 905936
    iget-object v0, p0, LX/5N8;->a:Landroid/graphics/Paint;

    invoke-static {v9, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 905937
    const/4 v0, 0x0

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 905938
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 905939
    return-void
.end method
