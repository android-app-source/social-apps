.class public final enum LX/6be;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6be;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6be;

.field public static final enum FAILED:LX/6be;

.field public static final enum RETRY:LX/6be;

.field public static final enum START:LX/6be;

.field public static final enum SUCCEEDED:LX/6be;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1113762
    new-instance v0, LX/6be;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, LX/6be;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6be;->START:LX/6be;

    .line 1113763
    new-instance v0, LX/6be;

    const-string v1, "RETRY"

    invoke-direct {v0, v1, v3}, LX/6be;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6be;->RETRY:LX/6be;

    .line 1113764
    new-instance v0, LX/6be;

    const-string v1, "SUCCEEDED"

    invoke-direct {v0, v1, v4}, LX/6be;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6be;->SUCCEEDED:LX/6be;

    .line 1113765
    new-instance v0, LX/6be;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/6be;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6be;->FAILED:LX/6be;

    .line 1113766
    const/4 v0, 0x4

    new-array v0, v0, [LX/6be;

    sget-object v1, LX/6be;->START:LX/6be;

    aput-object v1, v0, v2

    sget-object v1, LX/6be;->RETRY:LX/6be;

    aput-object v1, v0, v3

    sget-object v1, LX/6be;->SUCCEEDED:LX/6be;

    aput-object v1, v0, v4

    sget-object v1, LX/6be;->FAILED:LX/6be;

    aput-object v1, v0, v5

    sput-object v0, LX/6be;->$VALUES:[LX/6be;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1113767
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6be;
    .locals 1

    .prologue
    .line 1113768
    const-class v0, LX/6be;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6be;

    return-object v0
.end method

.method public static values()[LX/6be;
    .locals 1

    .prologue
    .line 1113769
    sget-object v0, LX/6be;->$VALUES:[LX/6be;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6be;

    return-object v0
.end method
