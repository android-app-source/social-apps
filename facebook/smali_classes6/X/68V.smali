.class public abstract LX/68V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/68U;


# static fields
.field private static final d:LX/68r;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/68r",
            "<",
            "LX/69E;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:I

.field public c:I

.field public final e:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1055345
    new-instance v0, LX/68r;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, LX/68r;-><init>(I)V

    sput-object v0, LX/68V;->d:LX/68r;

    .line 1055346
    return-void
.end method

.method public constructor <init>(II)V
    .locals 4

    .prologue
    .line 1055347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1055348
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/68V;->e:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1055349
    iput p1, p0, LX/68V;->b:I

    .line 1055350
    iput p2, p0, LX/68V;->c:I

    .line 1055351
    return-void
.end method

.method public static a(Ljava/io/InputStream;Z)LX/69E;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1055352
    sget-object v0, LX/68V;->d:LX/68r;

    invoke-virtual {v0}, LX/68r;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/69E;

    .line 1055353
    if-nez v0, :cond_0

    .line 1055354
    new-instance v0, LX/69E;

    const/high16 v1, 0x20000

    new-array v1, v1, [B

    invoke-direct {v0, v2, v1, v3}, LX/69E;-><init>(Ljava/lang/String;[BI)V

    .line 1055355
    :cond_0
    iget-object v1, v0, LX/69E;->b:[B

    move-object v4, v1

    move v1, v3

    .line 1055356
    :goto_0
    :try_start_0
    array-length v3, v4

    sub-int/2addr v3, v1

    invoke-virtual {p0, v4, v1, v3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_3

    .line 1055357
    add-int/2addr v3, v1

    .line 1055358
    :try_start_1
    array-length v1, v4

    if-lt v3, v1, :cond_5

    .line 1055359
    array-length v1, v4

    shl-int/lit8 v1, v1, 0x1

    new-array v1, v1, [B

    .line 1055360
    const/4 v5, 0x0

    const/4 v6, 0x0

    array-length v7, v4

    invoke-static {v4, v5, v1, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v4, v1

    move v1, v3

    .line 1055361
    goto :goto_0

    .line 1055362
    :catch_0
    move-exception v3

    .line 1055363
    :goto_1
    if-eqz p1, :cond_1

    if-nez v1, :cond_3

    .line 1055364
    :cond_1
    sget-object v1, LX/68V;->d:LX/68r;

    invoke-virtual {v1, v0}, LX/68r;->a(Ljava/lang/Object;)Z

    .line 1055365
    if-eqz p1, :cond_2

    .line 1055366
    sget-object v0, LX/31U;->u:LX/31U;

    invoke-virtual {v0, v3}, LX/31U;->a(Ljava/lang/Throwable;)V

    move-object v0, v2

    .line 1055367
    :goto_2
    return-object v0

    .line 1055368
    :cond_2
    throw v3

    .line 1055369
    :cond_3
    iget-object v3, v0, LX/69E;->b:[B

    if-eq v3, v4, :cond_4

    .line 1055370
    sget-object v3, LX/68V;->d:LX/68r;

    invoke-virtual {v3, v0}, LX/68r;->a(Ljava/lang/Object;)Z

    .line 1055371
    new-instance v0, LX/69E;

    invoke-direct {v0, v2, v4, v1}, LX/69E;-><init>(Ljava/lang/String;[BI)V

    goto :goto_2

    .line 1055372
    :cond_4
    iput v1, v0, LX/69E;->c:I

    goto :goto_2

    .line 1055373
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v3

    move-object v3, v8

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_0
.end method

.method public static c(LX/69E;)V
    .locals 2

    .prologue
    .line 1055374
    iget-object v0, p0, LX/69E;->b:[B

    array-length v0, v0

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_0

    .line 1055375
    const/4 v0, 0x0

    iput-object v0, p0, LX/69E;->a:Ljava/lang/String;

    .line 1055376
    sget-object v0, LX/68V;->d:LX/68r;

    invoke-virtual {v0, p0}, LX/68r;->a(Ljava/lang/Object;)Z

    .line 1055377
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/net/URL;)LX/69E;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1055378
    if-nez p1, :cond_1

    .line 1055379
    :cond_0
    :goto_0
    return-object v0

    .line 1055380
    :cond_1
    invoke-static {}, LX/31U;->a()J

    move-result-wide v4

    .line 1055381
    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1055382
    const/4 v1, 0x1

    :try_start_1
    invoke-static {v2, v1}, LX/68V;->a(Ljava/io/InputStream;Z)LX/69E;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1055383
    if-eqz v2, :cond_2

    .line 1055384
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1055385
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 1055386
    iget-object v1, p0, LX/68V;->e:Ljava/util/concurrent/atomic/AtomicLong;

    iget v2, v0, LX/69E;->c:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 1055387
    sget-object v1, LX/31U;->e:LX/31U;

    iget v2, v0, LX/69E;->c:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, LX/31U;->a(J)V

    .line 1055388
    sget-object v1, LX/31U;->d:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, LX/31U;->a(J)V

    goto :goto_0

    .line 1055389
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 1055390
    :goto_2
    :try_start_3
    sget-object v3, LX/31U;->r:LX/31U;

    invoke-virtual {v3, v1}, LX/31U;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1055391
    if-eqz v2, :cond_3

    .line 1055392
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1055393
    :cond_3
    :goto_3
    if-eqz v0, :cond_0

    .line 1055394
    iget-object v1, p0, LX/68V;->e:Ljava/util/concurrent/atomic/AtomicLong;

    iget v2, v0, LX/69E;->c:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 1055395
    sget-object v1, LX/31U;->e:LX/31U;

    iget v2, v0, LX/69E;->c:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, LX/31U;->a(J)V

    .line 1055396
    sget-object v1, LX/31U;->d:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, LX/31U;->a(J)V

    goto :goto_0

    .line 1055397
    :catchall_0
    move-exception v1

    move-object v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1055398
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1055399
    :cond_4
    :goto_5
    if-eqz v0, :cond_5

    .line 1055400
    iget-object v2, p0, LX/68V;->e:Ljava/util/concurrent/atomic/AtomicLong;

    iget v3, v0, LX/69E;->c:I

    int-to-long v6, v3

    invoke-virtual {v2, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 1055401
    sget-object v2, LX/31U;->e:LX/31U;

    iget v0, v0, LX/69E;->c:I

    int-to-long v6, v0

    invoke-virtual {v2, v6, v7}, LX/31U;->a(J)V

    .line 1055402
    sget-object v0, LX/31U;->d:LX/31U;

    invoke-static {}, LX/31U;->a()J

    move-result-wide v2

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/31U;->a(J)V

    :cond_5
    throw v1

    :catch_1
    goto :goto_1

    :catch_2
    goto :goto_3

    :catch_3
    goto :goto_5

    .line 1055403
    :catchall_1
    move-exception v1

    goto :goto_4

    .line 1055404
    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method public b(III)LX/69C;
    .locals 3

    .prologue
    .line 1055405
    invoke-virtual {p0, p1, p2, p3}, LX/68V;->c(III)Ljava/net/URL;

    move-result-object v0

    .line 1055406
    if-nez v0, :cond_0

    .line 1055407
    sget-object v0, LX/68V;->a:LX/69C;

    .line 1055408
    :goto_0
    return-object v0

    .line 1055409
    :cond_0
    invoke-virtual {p0, v0}, LX/68V;->a(Ljava/net/URL;)LX/69E;

    move-result-object v1

    .line 1055410
    if-nez v1, :cond_1

    .line 1055411
    const/4 v0, 0x0

    goto :goto_0

    .line 1055412
    :cond_1
    iget-object v0, v1, LX/69E;->b:[B

    iget v2, v1, LX/69E;->c:I

    invoke-static {v0, v2}, LX/69C;->a([BI)LX/69C;

    move-result-object v0

    .line 1055413
    invoke-static {v1}, LX/68V;->c(LX/69E;)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 1055414
    iget-object v0, p0, LX/68V;->e:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1055415
    return-void
.end method

.method public abstract c(III)Ljava/net/URL;
.end method
