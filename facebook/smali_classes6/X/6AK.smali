.class public LX/6AK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final a:LX/2cE;

.field private final b:LX/1fP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1058857
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6AK;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2cE;LX/1fP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058976
    iput-object p1, p0, LX/6AK;->a:LX/2cE;

    .line 1058977
    iput-object p2, p0, LX/6AK;->b:LX/1fP;

    .line 1058978
    return-void
.end method

.method public static a(LX/0QB;)LX/6AK;
    .locals 8

    .prologue
    .line 1058946
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1058947
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1058948
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1058949
    if-nez v1, :cond_0

    .line 1058950
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1058951
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1058952
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1058953
    sget-object v1, LX/6AK;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1058954
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1058955
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1058956
    :cond_1
    if-nez v1, :cond_4

    .line 1058957
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1058958
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1058959
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1058960
    new-instance p0, LX/6AK;

    invoke-static {v0}, LX/2cE;->a(LX/0QB;)LX/2cE;

    move-result-object v1

    check-cast v1, LX/2cE;

    invoke-static {v0}, LX/1fP;->b(LX/0QB;)LX/1fP;

    move-result-object v7

    check-cast v7, LX/1fP;

    invoke-direct {p0, v1, v7}, LX/6AK;-><init>(LX/2cE;LX/1fP;)V

    .line 1058961
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1058962
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1058963
    if-nez v1, :cond_2

    .line 1058964
    sget-object v0, LX/6AK;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6AK;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1058965
    :goto_1
    if-eqz v0, :cond_3

    .line 1058966
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1058967
    :goto_3
    check-cast v0, LX/6AK;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1058968
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1058969
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1058970
    :catchall_1
    move-exception v0

    .line 1058971
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1058972
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1058973
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1058974
    :cond_2
    :try_start_8
    sget-object v0, LX/6AK;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6AK;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/6AK;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 8

    .prologue
    .line 1058924
    iget-object v0, p0, LX/6AK;->a:LX/2cE;

    .line 1058925
    iget-object v1, v0, LX/2cE;->f:Lcom/facebook/omnistore/Collection;

    move-object v7, v1

    .line 1058926
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez v7, :cond_1

    .line 1058927
    :cond_0
    :goto_0
    return-void

    .line 1058928
    :cond_1
    iget-object v0, p0, LX/6AK;->b:LX/1fP;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, LX/1fP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 1058929
    new-instance v0, LX/0eX;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, LX/0eX;-><init>(I)V

    .line 1058930
    invoke-virtual {v0, p3}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v1

    .line 1058931
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/0eX;->b(I)V

    .line 1058932
    const/4 v2, 0x3

    const/4 p3, 0x0

    invoke-virtual {v0, v2, p6, p3}, LX/0eX;->b(III)V

    .line 1058933
    const/4 v2, 0x2

    const/4 p3, 0x0

    invoke-virtual {v0, v2, p5, p3}, LX/0eX;->b(III)V

    .line 1058934
    const/4 v2, 0x1

    const/4 p3, 0x0

    invoke-virtual {v0, v2, p4, p3}, LX/0eX;->b(III)V

    .line 1058935
    const/4 v2, 0x0

    .line 1058936
    invoke-virtual {v0, v2, v1, v2}, LX/0eX;->c(III)V

    .line 1058937
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v2

    .line 1058938
    move v2, v2

    .line 1058939
    move v1, v2

    .line 1058940
    invoke-virtual {v0, v1}, LX/0eX;->c(I)V

    .line 1058941
    iget-object v1, v0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    move-object v0, v1

    .line 1058942
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    new-array v1, v1, [B

    .line 1058943
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 1058944
    move-object v0, v1

    .line 1058945
    invoke-virtual {v7, p1, p2, v0}, Lcom/facebook/omnistore/Collection;->saveObject(Ljava/lang/String;Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method private static d(LX/6AK;LX/0Px;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1058915
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 1058916
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 1058917
    instance-of v4, v3, Lcom/facebook/graphql/model/GraphQLStory;

    move v4, v4

    .line 1058918
    if-eqz v4, :cond_0

    const-string v4, "User"

    .line 1058919
    iget-object v5, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    move-object v5, v5

    .line 1058920
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1058921
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1058922
    :cond_1
    return-void

    .line 1058923
    :cond_2
    invoke-static {v0}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, LX/6AL;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v3}, LX/6AL;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v8

    invoke-static {v3}, LX/6AL;->c(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v9

    move-object v3, p0

    invoke-static/range {v3 .. v9}, LX/6AK;->a(LX/6AK;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1058979
    const-string v1, "FeedUnitUpdateHandler.getCacheEntriesCount"

    const v2, 0x7f25d41b

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1058980
    iget-object v1, p0, LX/6AK;->a:LX/2cE;

    .line 1058981
    iget-object v2, v1, LX/2cE;->f:Lcom/facebook/omnistore/Collection;

    move-object v1, v2

    .line 1058982
    if-nez v1, :cond_0

    .line 1058983
    :goto_0
    return v0

    .line 1058984
    :cond_0
    :try_start_0
    sget-object v2, LX/0ql;->b:Ljava/lang/String;

    const/4 v3, -0x1

    sget-object v4, Lcom/facebook/omnistore/Collection$SortDirection;->DESCENDING:Lcom/facebook/omnistore/Collection$SortDirection;

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/omnistore/Collection;->query(Ljava/lang/String;ILcom/facebook/omnistore/Collection$SortDirection;)Lcom/facebook/omnistore/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1058985
    const/4 v1, 0x0

    .line 1058986
    :goto_1
    :try_start_1
    invoke-virtual {v2}, Lcom/facebook/omnistore/Cursor;->step()Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 1058987
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1058988
    :cond_1
    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v2}, Lcom/facebook/omnistore/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1058989
    :cond_2
    const v1, -0x66896552

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 1058990
    :catch_0
    move-exception v1

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1058991
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_4
    invoke-virtual {v2}, Lcom/facebook/omnistore/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_3
    :goto_2
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1058992
    :catchall_1
    move-exception v0

    const v1, -0x2b74faaf

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1058993
    :catch_1
    move-exception v2

    :try_start_6
    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, Lcom/facebook/omnistore/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2
.end method

.method public final a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1058906
    const-string v0, "FeedUnitUpdateHandler.deleteFeedUnits"

    const v1, 0x3ec8e0fd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1058907
    iget-object v0, p0, LX/6AK;->a:LX/2cE;

    .line 1058908
    iget-object v1, v0, LX/2cE;->f:Lcom/facebook/omnistore/Collection;

    move-object v2, v1

    .line 1058909
    if-nez v2, :cond_0

    .line 1058910
    :goto_0
    return-void

    .line 1058911
    :cond_0
    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1058912
    invoke-virtual {v2, v0}, Lcom/facebook/omnistore/Collection;->deleteObject(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1058913
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1058914
    :cond_1
    const v0, -0x29def00

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x6934e396

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/api/feed/FetchFeedResult;)V
    .locals 12

    .prologue
    .line 1058891
    const-string v0, "FeedUnitUpdateHandler.saveFeedUnits"

    const v1, -0x5272fec3

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1058892
    :try_start_0
    iget-object v0, p1, Lcom/facebook/api/feed/FetchFeedResult;->a:Lcom/facebook/api/feed/FetchFeedParams;

    move-object v0, v0

    .line 1058893
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v1

    .line 1058894
    sget-object v1, Lcom/facebook/api/feedtype/FeedType;->b:Lcom/facebook/api/feedtype/FeedType;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v1, v0, :cond_0

    .line 1058895
    const v0, -0x2c8075c3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1058896
    :goto_0
    return-void

    .line 1058897
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    .line 1058898
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1058899
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    .line 1058900
    invoke-static {v2}, LX/1uc;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v6

    .line 1058901
    instance-of v7, v5, Lcom/facebook/graphql/model/GraphQLStory;

    move v7, v7

    .line 1058902
    if-eqz v7, :cond_1

    const-string v7, "User"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1058903
    :cond_1
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058904
    :cond_2
    const v0, -0x6039223b

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, -0x77b7ea08

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1058905
    :cond_3
    invoke-static {v2}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5}, LX/6AL;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-static {v5}, LX/6AL;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v10

    invoke-static {v5}, LX/6AL;->c(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v11

    move-object v5, p0

    invoke-static/range {v5 .. v11}, LX/6AK;->a(LX/6AK;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 7

    .prologue
    .line 1058884
    const-string v0, "FeedUnitUpdateHandler.saveFeedUnit"

    const v1, 0x1d41f948

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1058885
    instance-of v0, p3, Lcom/facebook/graphql/model/GraphQLStory;

    move v0, v0

    .line 1058886
    if-nez v0, :cond_0

    .line 1058887
    const v0, 0x3022b841

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1058888
    :goto_0
    return-void

    .line 1058889
    :cond_0
    :try_start_0
    invoke-static {p3}, LX/6AL;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {p3}, LX/6AL;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v5

    invoke-static {p3}, LX/6AL;->c(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v6}, LX/6AK;->a(LX/6AK;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1058890
    const v0, 0x671bbc8a

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x2aaa2d64

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1058858
    const-string v0, "FeedUnitUpdateHandler.synchronizedCacheWithHomeStoriesDB"

    const v1, 0x4a565bbb    # 3512046.8f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1058859
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1058860
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 1058861
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    instance-of v4, v4, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_0

    const-string v4, "User"

    .line 1058862
    iget-object v5, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A:Ljava/lang/String;

    move-object v5, v5

    .line 1058863
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1058864
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1058865
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1058866
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1058867
    invoke-virtual {p0}, LX/6AK;->a()I

    move-result v1

    .line 1058868
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1058869
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1058870
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iget-object v5, p0, LX/6AK;->a:LX/2cE;

    invoke-static {v2, v3, v4, v5}, LX/6AM;->a(LX/0Px;LX/0Pz;LX/0Pz;LX/2cE;)V

    .line 1058871
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {p0, v2}, LX/6AK;->d(LX/6AK;LX/0Px;)V

    .line 1058872
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/6AK;->a(LX/0Px;)V

    .line 1058873
    iget-object v2, p0, LX/6AK;->b:LX/1fP;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    .line 1058874
    iget-object v5, v2, LX/1fP;->a:LX/0Zb;

    const-string p0, "feed_omnistore_sync"

    const/4 p1, 0x0

    invoke-interface {v5, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 1058875
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1058876
    const-string p0, "home_stories_entry"

    invoke-virtual {v5, p0, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1058877
    const-string p0, "omnistore_entry"

    invoke-virtual {v5, p0, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1058878
    const-string p0, "added_entry"

    invoke-virtual {v5, p0, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1058879
    const-string p0, "deleted_entry"

    invoke-virtual {v5, p0, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1058880
    invoke-virtual {v5}, LX/0oG;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1058881
    :cond_2
    const v0, -0x4763dcf8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1058882
    return-void

    .line 1058883
    :catchall_0
    move-exception v0

    const v1, -0x4d3b419

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
