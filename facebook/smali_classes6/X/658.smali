.class public final enum LX/658;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/658;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/658;

.field public static final enum SSL_3_0:LX/658;

.field public static final enum TLS_1_0:LX/658;

.field public static final enum TLS_1_1:LX/658;

.field public static final enum TLS_1_2:LX/658;


# instance fields
.field public final javaName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1046475
    new-instance v0, LX/658;

    const-string v1, "TLS_1_2"

    const-string v2, "TLSv1.2"

    invoke-direct {v0, v1, v3, v2}, LX/658;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/658;->TLS_1_2:LX/658;

    .line 1046476
    new-instance v0, LX/658;

    const-string v1, "TLS_1_1"

    const-string v2, "TLSv1.1"

    invoke-direct {v0, v1, v4, v2}, LX/658;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/658;->TLS_1_1:LX/658;

    .line 1046477
    new-instance v0, LX/658;

    const-string v1, "TLS_1_0"

    const-string v2, "TLSv1"

    invoke-direct {v0, v1, v5, v2}, LX/658;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/658;->TLS_1_0:LX/658;

    .line 1046478
    new-instance v0, LX/658;

    const-string v1, "SSL_3_0"

    const-string v2, "SSLv3"

    invoke-direct {v0, v1, v6, v2}, LX/658;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/658;->SSL_3_0:LX/658;

    .line 1046479
    const/4 v0, 0x4

    new-array v0, v0, [LX/658;

    sget-object v1, LX/658;->TLS_1_2:LX/658;

    aput-object v1, v0, v3

    sget-object v1, LX/658;->TLS_1_1:LX/658;

    aput-object v1, v0, v4

    sget-object v1, LX/658;->TLS_1_0:LX/658;

    aput-object v1, v0, v5

    sget-object v1, LX/658;->SSL_3_0:LX/658;

    aput-object v1, v0, v6

    sput-object v0, LX/658;->$VALUES:[LX/658;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1046472
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1046473
    iput-object p3, p0, LX/658;->javaName:Ljava/lang/String;

    .line 1046474
    return-void
.end method

.method public static forJavaName(Ljava/lang/String;)LX/658;
    .locals 3

    .prologue
    .line 1046464
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1046465
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected TLS version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046466
    :sswitch_0
    const-string v1, "TLSv1.2"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "TLSv1.1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "TLSv1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "SSLv3"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 1046467
    :pswitch_0
    sget-object v0, LX/658;->TLS_1_2:LX/658;

    .line 1046468
    :goto_1
    return-object v0

    .line 1046469
    :pswitch_1
    sget-object v0, LX/658;->TLS_1_1:LX/658;

    goto :goto_1

    .line 1046470
    :pswitch_2
    sget-object v0, LX/658;->TLS_1_0:LX/658;

    goto :goto_1

    .line 1046471
    :pswitch_3
    sget-object v0, LX/658;->SSL_3_0:LX/658;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x1dfc3f27 -> :sswitch_1
        -0x1dfc3f26 -> :sswitch_0
        0x4b88569 -> :sswitch_3
        0x4c38896 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/658;
    .locals 1

    .prologue
    .line 1046463
    const-class v0, LX/658;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/658;

    return-object v0
.end method

.method public static values()[LX/658;
    .locals 1

    .prologue
    .line 1046462
    sget-object v0, LX/658;->$VALUES:[LX/658;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/658;

    return-object v0
.end method


# virtual methods
.method public final javaName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1046461
    iget-object v0, p0, LX/658;->javaName:Ljava/lang/String;

    return-object v0
.end method
