.class public final LX/5ae;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 955324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 955325
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 955326
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 955327
    invoke-static {p0, p1}, LX/5ae;->b(LX/15w;LX/186;)I

    move-result v1

    .line 955328
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 955329
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 48

    .prologue
    .line 955473
    const/16 v44, 0x0

    .line 955474
    const/16 v43, 0x0

    .line 955475
    const/16 v42, 0x0

    .line 955476
    const/16 v41, 0x0

    .line 955477
    const/16 v40, 0x0

    .line 955478
    const/16 v39, 0x0

    .line 955479
    const/16 v38, 0x0

    .line 955480
    const/16 v37, 0x0

    .line 955481
    const/16 v36, 0x0

    .line 955482
    const/16 v35, 0x0

    .line 955483
    const/16 v34, 0x0

    .line 955484
    const/16 v33, 0x0

    .line 955485
    const/16 v32, 0x0

    .line 955486
    const/16 v31, 0x0

    .line 955487
    const/16 v30, 0x0

    .line 955488
    const/16 v29, 0x0

    .line 955489
    const/16 v28, 0x0

    .line 955490
    const/16 v27, 0x0

    .line 955491
    const/16 v26, 0x0

    .line 955492
    const/16 v25, 0x0

    .line 955493
    const/16 v24, 0x0

    .line 955494
    const/16 v23, 0x0

    .line 955495
    const/16 v22, 0x0

    .line 955496
    const/16 v21, 0x0

    .line 955497
    const/16 v20, 0x0

    .line 955498
    const/16 v19, 0x0

    .line 955499
    const/16 v18, 0x0

    .line 955500
    const/16 v17, 0x0

    .line 955501
    const/16 v16, 0x0

    .line 955502
    const/4 v15, 0x0

    .line 955503
    const/4 v14, 0x0

    .line 955504
    const/4 v13, 0x0

    .line 955505
    const/4 v12, 0x0

    .line 955506
    const/4 v11, 0x0

    .line 955507
    const/4 v10, 0x0

    .line 955508
    const/4 v9, 0x0

    .line 955509
    const/4 v8, 0x0

    .line 955510
    const/4 v7, 0x0

    .line 955511
    const/4 v6, 0x0

    .line 955512
    const/4 v5, 0x0

    .line 955513
    const/4 v4, 0x0

    .line 955514
    const/4 v3, 0x0

    .line 955515
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v45

    sget-object v46, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    if-eq v0, v1, :cond_1

    .line 955516
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 955517
    const/4 v3, 0x0

    .line 955518
    :goto_0
    return v3

    .line 955519
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 955520
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v45

    sget-object v46, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    if-eq v0, v1, :cond_25

    .line 955521
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v45

    .line 955522
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 955523
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v46

    sget-object v47, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    if-eq v0, v1, :cond_1

    if-eqz v45, :cond_1

    .line 955524
    const-string v46, "__type__"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-nez v46, :cond_2

    const-string v46, "__typename"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_3

    .line 955525
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v44

    goto :goto_1

    .line 955526
    :cond_3
    const-string v46, "animated_image_full_screen"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_4

    .line 955527
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v43

    goto :goto_1

    .line 955528
    :cond_4
    const-string v46, "animated_image_large_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_5

    .line 955529
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v42

    goto :goto_1

    .line 955530
    :cond_5
    const-string v46, "animated_image_medium_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_6

    .line 955531
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v41

    goto :goto_1

    .line 955532
    :cond_6
    const-string v46, "animated_image_original_dimensions"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_7

    .line 955533
    invoke-static/range {p0 .. p1}, LX/5ac;->a(LX/15w;LX/186;)I

    move-result v40

    goto :goto_1

    .line 955534
    :cond_7
    const-string v46, "animated_image_render_as_sticker"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_8

    .line 955535
    const/4 v9, 0x1

    .line 955536
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 955537
    :cond_8
    const-string v46, "animated_image_small_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_9

    .line 955538
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v38

    goto/16 :goto_1

    .line 955539
    :cond_9
    const-string v46, "animated_static_image_full_screen"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_a

    .line 955540
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v37

    goto/16 :goto_1

    .line 955541
    :cond_a
    const-string v46, "animated_static_image_large_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_b

    .line 955542
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v36

    goto/16 :goto_1

    .line 955543
    :cond_b
    const-string v46, "animated_static_image_medium_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_c

    .line 955544
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v35

    goto/16 :goto_1

    .line 955545
    :cond_c
    const-string v46, "animated_static_image_small_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_d

    .line 955546
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v34

    goto/16 :goto_1

    .line 955547
    :cond_d
    const-string v46, "attachment_fbid"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_e

    .line 955548
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto/16 :goto_1

    .line 955549
    :cond_e
    const-string v46, "attachment_video_url"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_f

    .line 955550
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 955551
    :cond_f
    const-string v46, "attribution_app"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_10

    .line 955552
    invoke-static/range {p0 .. p1}, LX/5Tw;->a(LX/15w;LX/186;)I

    move-result v31

    goto/16 :goto_1

    .line 955553
    :cond_10
    const-string v46, "attribution_app_scoped_ids"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_11

    .line 955554
    invoke-static/range {p0 .. p1}, LX/5Tx;->a(LX/15w;LX/186;)I

    move-result v30

    goto/16 :goto_1

    .line 955555
    :cond_11
    const-string v46, "attribution_metadata"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_12

    .line 955556
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v29

    goto/16 :goto_1

    .line 955557
    :cond_12
    const-string v46, "call_id"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_13

    .line 955558
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 955559
    :cond_13
    const-string v46, "filename"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_14

    .line 955560
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 955561
    :cond_14
    const-string v46, "filesize"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_15

    .line 955562
    const/4 v8, 0x1

    .line 955563
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v26

    goto/16 :goto_1

    .line 955564
    :cond_15
    const-string v46, "image_full_screen"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_16

    .line 955565
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 955566
    :cond_16
    const-string v46, "image_large_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_17

    .line 955567
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 955568
    :cond_17
    const-string v46, "image_medium_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_18

    .line 955569
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 955570
    :cond_18
    const-string v46, "image_small_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_19

    .line 955571
    invoke-static/range {p0 .. p1}, LX/5aX;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 955572
    :cond_19
    const-string v46, "image_type"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1a

    .line 955573
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLMessageImageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageImageType;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    goto/16 :goto_1

    .line 955574
    :cond_1a
    const-string v46, "is_voicemail"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1b

    .line 955575
    const/4 v7, 0x1

    .line 955576
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 955577
    :cond_1b
    const-string v46, "messaging_attribution"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1c

    .line 955578
    invoke-static/range {p0 .. p1}, LX/5Ty;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 955579
    :cond_1c
    const-string v46, "mimetype"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1d

    .line 955580
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 955581
    :cond_1d
    const-string v46, "mini_preview"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1e

    .line 955582
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 955583
    :cond_1e
    const-string v46, "original_dimensions"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1f

    .line 955584
    invoke-static/range {p0 .. p1}, LX/5ad;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 955585
    :cond_1f
    const-string v46, "playable_duration_in_ms"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_20

    .line 955586
    const/4 v6, 0x1

    .line 955587
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto/16 :goto_1

    .line 955588
    :cond_20
    const-string v46, "render_as_sticker"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_21

    .line 955589
    const/4 v5, 0x1

    .line 955590
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 955591
    :cond_21
    const-string v46, "rotation"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_22

    .line 955592
    const/4 v4, 0x1

    .line 955593
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 955594
    :cond_22
    const-string v46, "streamingImageThumbnail"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_23

    .line 955595
    invoke-static/range {p0 .. p1}, LX/5ao;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 955596
    :cond_23
    const-string v46, "video_filesize"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_24

    .line 955597
    const/4 v3, 0x1

    .line 955598
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto/16 :goto_1

    .line 955599
    :cond_24
    const-string v46, "video_type"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_0

    .line 955600
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLMessageVideoType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessageVideoType;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto/16 :goto_1

    .line 955601
    :cond_25
    const/16 v45, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 955602
    const/16 v45, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 955603
    const/16 v44, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v44

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 955604
    const/16 v43, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v43

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 955605
    const/16 v42, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v42

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 955606
    const/16 v41, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 955607
    if-eqz v9, :cond_26

    .line 955608
    const/4 v9, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 955609
    :cond_26
    const/4 v9, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955610
    const/4 v9, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955611
    const/16 v9, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955612
    const/16 v9, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955613
    const/16 v9, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955614
    const/16 v9, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955615
    const/16 v9, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955616
    const/16 v9, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955617
    const/16 v9, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955618
    const/16 v9, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955619
    const/16 v9, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955620
    const/16 v9, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 955621
    if-eqz v8, :cond_27

    .line 955622
    const/16 v8, 0x12

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 955623
    :cond_27
    const/16 v8, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 955624
    const/16 v8, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 955625
    const/16 v8, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 955626
    const/16 v8, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 955627
    const/16 v8, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 955628
    if-eqz v7, :cond_28

    .line 955629
    const/16 v7, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 955630
    :cond_28
    const/16 v7, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 955631
    const/16 v7, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 955632
    const/16 v7, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 955633
    const/16 v7, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 955634
    if-eqz v6, :cond_29

    .line 955635
    const/16 v6, 0x1d

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15, v7}, LX/186;->a(III)V

    .line 955636
    :cond_29
    if-eqz v5, :cond_2a

    .line 955637
    const/16 v5, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->a(IZ)V

    .line 955638
    :cond_2a
    if-eqz v4, :cond_2b

    .line 955639
    const/16 v4, 0x1f

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13, v5}, LX/186;->a(III)V

    .line 955640
    :cond_2b
    const/16 v4, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 955641
    if-eqz v3, :cond_2c

    .line 955642
    const/16 v3, 0x21

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11, v4}, LX/186;->a(III)V

    .line 955643
    :cond_2c
    const/16 v3, 0x22

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 955644
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0x22

    const/16 v3, 0x17

    const/4 v2, 0x0

    .line 955330
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 955331
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 955332
    if-eqz v0, :cond_0

    .line 955333
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955334
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 955335
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955336
    if-eqz v0, :cond_1

    .line 955337
    const-string v1, "animated_image_full_screen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955338
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955339
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955340
    if-eqz v0, :cond_2

    .line 955341
    const-string v1, "animated_image_large_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955342
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955343
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955344
    if-eqz v0, :cond_3

    .line 955345
    const-string v1, "animated_image_medium_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955346
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955347
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955348
    if-eqz v0, :cond_4

    .line 955349
    const-string v1, "animated_image_original_dimensions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955350
    invoke-static {p0, v0, p2}, LX/5ac;->a(LX/15i;ILX/0nX;)V

    .line 955351
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 955352
    if-eqz v0, :cond_5

    .line 955353
    const-string v1, "animated_image_render_as_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955354
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 955355
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955356
    if-eqz v0, :cond_6

    .line 955357
    const-string v1, "animated_image_small_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955358
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955359
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955360
    if-eqz v0, :cond_7

    .line 955361
    const-string v1, "animated_static_image_full_screen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955362
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955363
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955364
    if-eqz v0, :cond_8

    .line 955365
    const-string v1, "animated_static_image_large_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955366
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955367
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955368
    if-eqz v0, :cond_9

    .line 955369
    const-string v1, "animated_static_image_medium_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955370
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955371
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955372
    if-eqz v0, :cond_a

    .line 955373
    const-string v1, "animated_static_image_small_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955374
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955375
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955376
    if-eqz v0, :cond_b

    .line 955377
    const-string v1, "attachment_fbid"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955378
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955379
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955380
    if-eqz v0, :cond_c

    .line 955381
    const-string v1, "attachment_video_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955382
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955383
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955384
    if-eqz v0, :cond_d

    .line 955385
    const-string v1, "attribution_app"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955386
    invoke-static {p0, v0, p2, p3}, LX/5Tw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 955387
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955388
    if-eqz v0, :cond_e

    .line 955389
    const-string v1, "attribution_app_scoped_ids"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955390
    invoke-static {p0, v0, p2, p3}, LX/5Tx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 955391
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955392
    if-eqz v0, :cond_f

    .line 955393
    const-string v1, "attribution_metadata"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955394
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955395
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955396
    if-eqz v0, :cond_10

    .line 955397
    const-string v1, "call_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955398
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955399
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955400
    if-eqz v0, :cond_11

    .line 955401
    const-string v1, "filename"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955402
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955403
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 955404
    if-eqz v0, :cond_12

    .line 955405
    const-string v1, "filesize"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955406
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 955407
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955408
    if-eqz v0, :cond_13

    .line 955409
    const-string v1, "image_full_screen"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955410
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955411
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955412
    if-eqz v0, :cond_14

    .line 955413
    const-string v1, "image_large_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955414
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955415
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955416
    if-eqz v0, :cond_15

    .line 955417
    const-string v1, "image_medium_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955418
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955419
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955420
    if-eqz v0, :cond_16

    .line 955421
    const-string v1, "image_small_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955422
    invoke-static {p0, v0, p2}, LX/5aX;->a(LX/15i;ILX/0nX;)V

    .line 955423
    :cond_16
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 955424
    if-eqz v0, :cond_17

    .line 955425
    const-string v0, "image_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955426
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955427
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 955428
    if-eqz v0, :cond_18

    .line 955429
    const-string v1, "is_voicemail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955430
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 955431
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955432
    if-eqz v0, :cond_19

    .line 955433
    const-string v1, "messaging_attribution"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955434
    invoke-static {p0, v0, p2}, LX/5Ty;->a(LX/15i;ILX/0nX;)V

    .line 955435
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955436
    if-eqz v0, :cond_1a

    .line 955437
    const-string v1, "mimetype"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955438
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955439
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955440
    if-eqz v0, :cond_1b

    .line 955441
    const-string v1, "mini_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955442
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955443
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955444
    if-eqz v0, :cond_1c

    .line 955445
    const-string v1, "original_dimensions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955446
    invoke-static {p0, v0, p2}, LX/5ad;->a(LX/15i;ILX/0nX;)V

    .line 955447
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 955448
    if-eqz v0, :cond_1d

    .line 955449
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955450
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 955451
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 955452
    if-eqz v0, :cond_1e

    .line 955453
    const-string v1, "render_as_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955454
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 955455
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 955456
    if-eqz v0, :cond_1f

    .line 955457
    const-string v1, "rotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955458
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 955459
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955460
    if-eqz v0, :cond_20

    .line 955461
    const-string v1, "streamingImageThumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955462
    invoke-static {p0, v0, p2}, LX/5ao;->a(LX/15i;ILX/0nX;)V

    .line 955463
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 955464
    if-eqz v0, :cond_21

    .line 955465
    const-string v1, "video_filesize"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955466
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 955467
    :cond_21
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 955468
    if-eqz v0, :cond_22

    .line 955469
    const-string v0, "video_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955470
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955471
    :cond_22
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 955472
    return-void
.end method
