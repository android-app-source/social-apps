.class public final enum LX/69H;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/69H;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/69H;

.field public static final enum GT:LX/69H;

.field public static final enum GTE:LX/69H;

.field public static final enum LT:LX/69H;

.field public static final enum LTE:LX/69H;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1057979
    new-instance v0, LX/69H;

    const-string v1, "LT"

    invoke-direct {v0, v1, v2}, LX/69H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/69H;->LT:LX/69H;

    .line 1057980
    new-instance v0, LX/69H;

    const-string v1, "LTE"

    invoke-direct {v0, v1, v3}, LX/69H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/69H;->LTE:LX/69H;

    .line 1057981
    new-instance v0, LX/69H;

    const-string v1, "GT"

    invoke-direct {v0, v1, v4}, LX/69H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/69H;->GT:LX/69H;

    .line 1057982
    new-instance v0, LX/69H;

    const-string v1, "GTE"

    invoke-direct {v0, v1, v5}, LX/69H;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/69H;->GTE:LX/69H;

    .line 1057983
    const/4 v0, 0x4

    new-array v0, v0, [LX/69H;

    sget-object v1, LX/69H;->LT:LX/69H;

    aput-object v1, v0, v2

    sget-object v1, LX/69H;->LTE:LX/69H;

    aput-object v1, v0, v3

    sget-object v1, LX/69H;->GT:LX/69H;

    aput-object v1, v0, v4

    sget-object v1, LX/69H;->GTE:LX/69H;

    aput-object v1, v0, v5

    sput-object v0, LX/69H;->$VALUES:[LX/69H;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1057984
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/69H;
    .locals 1

    .prologue
    .line 1057985
    const-class v0, LX/69H;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/69H;

    return-object v0
.end method

.method public static values()[LX/69H;
    .locals 1

    .prologue
    .line 1057986
    sget-object v0, LX/69H;->$VALUES:[LX/69H;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/69H;

    return-object v0
.end method
