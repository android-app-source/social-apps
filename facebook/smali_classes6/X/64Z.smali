.class public final LX/64Z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:LX/673;


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1044762
    instance-of v0, p1, LX/64Z;

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/64Z;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, LX/64Z;

    iget-object v0, v0, LX/64Z;->a:Ljava/lang/String;

    .line 1044763
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/64Z;->c:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, LX/64Z;

    iget-object v0, v0, LX/64Z;->c:Ljava/lang/String;

    .line 1044764
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/64Z;->d:LX/673;

    check-cast p1, LX/64Z;

    iget-object v1, p1, LX/64Z;->d:LX/673;

    .line 1044765
    invoke-virtual {v0, v1}, LX/673;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 1044766
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1044767
    iget-object v0, p0, LX/64Z;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1044768
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/64Z;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1044769
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/64Z;->d:LX/673;

    invoke-virtual {v1}, LX/673;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1044770
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1044771
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/64Z;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/64Z;->d:LX/673;

    invoke-virtual {v1}, LX/673;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
