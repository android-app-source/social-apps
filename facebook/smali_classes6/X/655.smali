.class public final LX/655;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final a:LX/650;

.field public final b:LX/64x;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:LX/64l;

.field public final f:LX/64n;

.field public final g:LX/656;

.field public final h:LX/655;

.field public final i:LX/655;

.field public final j:LX/655;

.field public final k:J

.field public final l:J

.field private volatile m:LX/64W;


# direct methods
.method public constructor <init>(LX/654;)V
    .locals 2

    .prologue
    .line 1046410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046411
    iget-object v0, p1, LX/654;->a:LX/650;

    iput-object v0, p0, LX/655;->a:LX/650;

    .line 1046412
    iget-object v0, p1, LX/654;->b:LX/64x;

    iput-object v0, p0, LX/655;->b:LX/64x;

    .line 1046413
    iget v0, p1, LX/654;->c:I

    iput v0, p0, LX/655;->c:I

    .line 1046414
    iget-object v0, p1, LX/654;->d:Ljava/lang/String;

    iput-object v0, p0, LX/655;->d:Ljava/lang/String;

    .line 1046415
    iget-object v0, p1, LX/654;->e:LX/64l;

    iput-object v0, p0, LX/655;->e:LX/64l;

    .line 1046416
    iget-object v0, p1, LX/654;->f:LX/64m;

    invoke-virtual {v0}, LX/64m;->a()LX/64n;

    move-result-object v0

    iput-object v0, p0, LX/655;->f:LX/64n;

    .line 1046417
    iget-object v0, p1, LX/654;->g:LX/656;

    iput-object v0, p0, LX/655;->g:LX/656;

    .line 1046418
    iget-object v0, p1, LX/654;->h:LX/655;

    iput-object v0, p0, LX/655;->h:LX/655;

    .line 1046419
    iget-object v0, p1, LX/654;->i:LX/655;

    iput-object v0, p0, LX/655;->i:LX/655;

    .line 1046420
    iget-object v0, p1, LX/654;->j:LX/655;

    iput-object v0, p0, LX/655;->j:LX/655;

    .line 1046421
    iget-wide v0, p1, LX/654;->k:J

    iput-wide v0, p0, LX/655;->k:J

    .line 1046422
    iget-wide v0, p1, LX/654;->l:J

    iput-wide v0, p0, LX/655;->l:J

    .line 1046423
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1046424
    const/4 v0, 0x0

    .line 1046425
    iget-object v1, p0, LX/655;->f:LX/64n;

    invoke-virtual {v1, p1}, LX/64n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1046426
    if-eqz v1, :cond_0

    move-object v0, v1

    :cond_0
    move-object v0, v0

    .line 1046427
    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1046428
    iget-object v0, p0, LX/655;->g:LX/656;

    invoke-virtual {v0}, LX/656;->close()V

    .line 1046429
    return-void
.end method

.method public final h()LX/64W;
    .locals 1

    .prologue
    .line 1046430
    iget-object v0, p0, LX/655;->m:LX/64W;

    .line 1046431
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/655;->f:LX/64n;

    invoke-static {v0}, LX/64W;->a(LX/64n;)LX/64W;

    move-result-object v0

    iput-object v0, p0, LX/655;->m:LX/64W;

    goto :goto_0
.end method

.method public final newBuilder()LX/654;
    .locals 2

    .prologue
    .line 1046432
    new-instance v0, LX/654;

    invoke-direct {v0, p0}, LX/654;-><init>(LX/655;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1046433
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Response{protocol="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/655;->b:LX/64x;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/655;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/655;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/655;->a:LX/650;

    .line 1046434
    iget-object p0, v1, LX/650;->a:LX/64q;

    move-object v1, p0

    .line 1046435
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1046436
    return-object v0
.end method
