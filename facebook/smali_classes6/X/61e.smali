.class public LX/61e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2MV;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/61e;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/2MM;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2MM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1040406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040407
    iput-object p1, p0, LX/61e;->a:Landroid/content/Context;

    .line 1040408
    iput-object p2, p0, LX/61e;->b:LX/2MM;

    .line 1040409
    return-void
.end method

.method private static a(Landroid/media/MediaMetadataRetriever;II)I
    .locals 2

    .prologue
    .line 1040445
    invoke-virtual {p0, p1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 1040446
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1040447
    :goto_0
    return p2

    .line 1040448
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 1040449
    :catch_0
    goto :goto_0
.end method

.method private static a(Landroid/media/MediaMetadataRetriever;IJ)J
    .locals 2

    .prologue
    .line 1040440
    invoke-virtual {p0, p1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 1040441
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1040442
    :goto_0
    return-wide p2

    .line 1040443
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    .line 1040444
    :catch_0
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/61e;
    .locals 5

    .prologue
    .line 1040427
    sget-object v0, LX/61e;->c:LX/61e;

    if-nez v0, :cond_1

    .line 1040428
    const-class v1, LX/61e;

    monitor-enter v1

    .line 1040429
    :try_start_0
    sget-object v0, LX/61e;->c:LX/61e;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1040430
    if-eqz v2, :cond_0

    .line 1040431
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1040432
    new-instance p0, LX/61e;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v4

    check-cast v4, LX/2MM;

    invoke-direct {p0, v3, v4}, LX/61e;-><init>(Landroid/content/Context;LX/2MM;)V

    .line 1040433
    move-object v0, p0

    .line 1040434
    sput-object v0, LX/61e;->c:LX/61e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1040435
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1040436
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1040437
    :cond_1
    sget-object v0, LX/61e;->c:LX/61e;

    return-object v0

    .line 1040438
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1040439
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/60x;
    .locals 17

    .prologue
    .line 1040410
    new-instance v16, Landroid/media/MediaMetadataRetriever;

    invoke-direct/range {v16 .. v16}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1040411
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/61e;->a:Landroid/content/Context;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1040412
    const/4 v8, 0x0

    .line 1040413
    const/16 v2, 0x12

    const/4 v3, -0x1

    move-object/from16 v0, v16

    invoke-static {v0, v2, v3}, LX/61e;->a(Landroid/media/MediaMetadataRetriever;II)I

    move-result v6

    .line 1040414
    const/16 v2, 0x13

    const/4 v3, -0x1

    move-object/from16 v0, v16

    invoke-static {v0, v2, v3}, LX/61e;->a(Landroid/media/MediaMetadataRetriever;II)I

    move-result v7

    .line 1040415
    const/16 v2, 0x14

    const/4 v3, -0x1

    move-object/from16 v0, v16

    invoke-static {v0, v2, v3}, LX/61e;->a(Landroid/media/MediaMetadataRetriever;II)I

    move-result v9

    .line 1040416
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 1040417
    const/16 v2, 0x18

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-static {v0, v2, v3}, LX/61e;->a(Landroid/media/MediaMetadataRetriever;II)I

    move-result v8

    .line 1040418
    :cond_0
    const/16 v2, 0x9

    const-wide/16 v4, 0x0

    move-object/from16 v0, v16

    invoke-static {v0, v2, v4, v5}, LX/61e;->a(Landroid/media/MediaMetadataRetriever;IJ)J

    move-result-wide v4

    .line 1040419
    const-wide/16 v10, -0x1

    .line 1040420
    move-object/from16 v0, p0

    iget-object v2, v0, LX/61e;->b:LX/2MM;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v2

    .line 1040421
    if-eqz v2, :cond_1

    .line 1040422
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v10

    .line 1040423
    :cond_1
    const/16 v2, 0x17

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v14

    .line 1040424
    const/4 v2, 0x5

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v15

    .line 1040425
    new-instance v3, LX/60x;

    const/4 v12, -0x1

    const/4 v13, 0x0

    invoke-direct/range {v3 .. v15}, LX/60x;-><init>(JIIIIJILX/60v;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1040426
    invoke-virtual/range {v16 .. v16}, Landroid/media/MediaMetadataRetriever;->release()V

    return-object v3

    :catchall_0
    move-exception v2

    invoke-virtual/range {v16 .. v16}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v2
.end method
