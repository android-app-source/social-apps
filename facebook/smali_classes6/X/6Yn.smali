.class public LX/6Yn;
.super LX/6Ya;
.source ""


# instance fields
.field public final c:LX/4g9;

.field public final d:LX/0Or;
    .annotation runtime Lcom/facebook/iorg/common/upsell/annotations/IsInZeroUpsellShowUseDataOrStayFreeScreenGateKeeper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0yH;

.field public f:LX/6YP;

.field public g:LX/6YV;

.field public h:Z


# direct methods
.method public constructor <init>(LX/4g9;LX/0Or;LX/0yH;LX/6YV;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/upsell/annotations/IsInZeroUpsellShowUseDataOrStayFreeScreenGateKeeper;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroAnalyticsLogger;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "LX/6YV;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110539
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1110540
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Yn;->h:Z

    .line 1110541
    iput-object p1, p0, LX/6Yn;->c:LX/4g9;

    .line 1110542
    iput-object p2, p0, LX/6Yn;->d:LX/0Or;

    .line 1110543
    iput-object p3, p0, LX/6Yn;->e:LX/0yH;

    .line 1110544
    iput-object p4, p0, LX/6Yn;->g:LX/6YV;

    .line 1110545
    return-void
.end method

.method public static a(LX/6Y7;Lcom/facebook/iorg/common/upsell/server/UpsellPromo;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 1110546
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->c:Ljava/lang/String;

    move-object v2, v0

    .line 1110547
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->d:Ljava/lang/String;

    move-object v3, v0

    .line 1110548
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->h:Ljava/lang/String;

    move-object v4, v0

    .line 1110549
    move-object v0, p0

    move-object v1, p2

    move-object v5, p1

    move v6, p3

    invoke-virtual/range {v0 .. v6}, LX/6Y7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Parcelable;Z)LX/6Y7;

    .line 1110550
    return-void
.end method

.method public static g(LX/6Yn;)LX/6YK;
    .locals 3

    .prologue
    .line 1110551
    new-instance v0, LX/6YL;

    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6YL;-><init>(Landroid/content/Context;)V

    .line 1110552
    new-instance v1, LX/6Ym;

    invoke-direct {v1, p0}, LX/6Ym;-><init>(LX/6Yn;)V

    .line 1110553
    iput-object v1, v0, LX/6YL;->b:Landroid/view/View$OnClickListener;

    .line 1110554
    iget-object v2, v0, LX/6YL;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6YJ;

    .line 1110555
    invoke-virtual {v2, v1}, LX/6YJ;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1110556
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 10

    .prologue
    .line 1110557
    new-instance v0, LX/6YP;

    invoke-direct {v0, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6Yn;->f:LX/6YP;

    .line 1110558
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110559
    iget-object p1, v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->u:Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    move-object v0, p1

    .line 1110560
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1110561
    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 1110562
    :cond_0
    :goto_0
    iget-object v0, p0, LX/6Yn;->f:LX/6YP;

    return-object v0

    .line 1110563
    :cond_1
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1110564
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    move v2, v3

    .line 1110565
    :goto_1
    new-instance v7, LX/6Y7;

    invoke-direct {v7}, LX/6Y7;-><init>()V

    .line 1110566
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->c:LX/0Px;

    move-object v8, v1

    .line 1110567
    if-eqz v2, :cond_2

    .line 1110568
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v6

    move v5, v4

    :goto_2
    if-ge v5, v6, :cond_2

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 1110569
    iget-boolean v9, v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->i:Z

    move v9, v9

    .line 1110570
    if-eqz v9, :cond_5

    .line 1110571
    iget-object v5, v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->k:Ljava/lang/String;

    move-object v5, v5

    .line 1110572
    invoke-static {v7, v1, v5, v3}, LX/6Yn;->a(LX/6Y7;Lcom/facebook/iorg/common/upsell/server/UpsellPromo;Ljava/lang/String;Z)V

    .line 1110573
    :cond_2
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v4

    move v5, v4

    :goto_3
    if-ge v6, v9, :cond_8

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    .line 1110574
    iget-object p1, v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->k:Ljava/lang/String;

    move-object p1, p1

    .line 1110575
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 1110576
    if-eqz v2, :cond_3

    .line 1110577
    iget-boolean p1, v1, Lcom/facebook/iorg/common/upsell/server/UpsellPromo;->i:Z

    move p1, p1

    .line 1110578
    if-nez p1, :cond_7

    .line 1110579
    :cond_3
    if-eqz v2, :cond_6

    if-nez v5, :cond_6

    .line 1110580
    iget-object v5, v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->l:Ljava/lang/String;

    move-object v5, v5

    .line 1110581
    invoke-static {v7, v1, v5, v4}, LX/6Yn;->a(LX/6Y7;Lcom/facebook/iorg/common/upsell/server/UpsellPromo;Ljava/lang/String;Z)V

    move v1, v3

    .line 1110582
    :goto_4
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v1

    goto :goto_3

    :cond_4
    move v2, v4

    .line 1110583
    goto :goto_1

    .line 1110584
    :cond_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_2

    .line 1110585
    :cond_6
    const-string p1, ""

    invoke-static {v7, v1, p1, v4}, LX/6Yn;->a(LX/6Y7;Lcom/facebook/iorg/common/upsell/server/UpsellPromo;Ljava/lang/String;Z)V

    :cond_7
    move v1, v5

    goto :goto_4

    .line 1110586
    :cond_8
    invoke-virtual {v7}, LX/6Y7;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1110587
    new-instance v1, LX/6Y5;

    invoke-direct {v1}, LX/6Y5;-><init>()V

    iget-object v2, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    invoke-virtual {v2}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->q()I

    move-result v2

    .line 1110588
    iput v2, v1, LX/6Y5;->s:I

    .line 1110589
    move-object v1, v1

    .line 1110590
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1110591
    iput-object v2, v1, LX/6Y5;->t:Ljava/lang/String;

    .line 1110592
    move-object v1, v1

    .line 1110593
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6Y5;->a(Ljava/lang/Boolean;)LX/6Y5;

    move-result-object v2

    .line 1110594
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1110595
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1110596
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1110597
    invoke-virtual {v2, v1}, LX/6Y5;->a(Ljava/lang/String;)LX/6Y5;

    .line 1110598
    :goto_5
    iget-object v1, p0, LX/6Yn;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1110599
    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v3, 0x7f080e0a

    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/6YN;->USE_DATA_OR_STAY_IN_FREE:LX/6YN;

    invoke-virtual {p0, v3}, LX/6Ya;->a(LX/6YN;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    .line 1110600
    :cond_9
    :goto_6
    iget-object v1, p0, LX/6Yn;->f:LX/6YP;

    invoke-virtual {v1, v2}, LX/6YP;->a(LX/6Y5;)V

    .line 1110601
    invoke-static {p0}, LX/6Yn;->g(LX/6Yn;)LX/6YK;

    move-result-object v1

    .line 1110602
    invoke-virtual {v1, v7}, LX/6YK;->a(LX/6Y7;)V

    .line 1110603
    iget-object v2, p0, LX/6Yn;->f:LX/6YP;

    .line 1110604
    iget-object v3, v2, LX/6YP;->c:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1110605
    goto/16 :goto_0

    .line 1110606
    :cond_a
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1110607
    invoke-virtual {v2, v1}, LX/6Y5;->a(Ljava/lang/String;)LX/6Y5;

    goto :goto_5

    .line 1110608
    :cond_b
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1110609
    iput-object v1, v2, LX/6Y5;->c:Ljava/lang/String;

    .line 1110610
    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    const v3, 0x7f080e0b

    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/6Ya;->c()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/6Y5;->b(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    .line 1110611
    iget-object v1, p0, LX/6Yn;->g:LX/6YV;

    iget-object v3, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110612
    iget-object v4, v3, Lcom/facebook/iorg/common/zero/ui/ZeroDialogFragment;->p:LX/0yY;

    move-object v3, v4

    .line 1110613
    invoke-interface {v1, v3}, LX/6YV;->b(LX/0yY;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1110614
    new-instance v1, LX/6Yl;

    invoke-direct {v1, p0}, LX/6Yl;-><init>(LX/6Yn;)V

    .line 1110615
    iput-object v1, v7, LX/6Y7;->b:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 1110616
    iget-object v1, p0, LX/6Yn;->g:LX/6YV;

    invoke-interface {v1}, LX/6YV;->a()Z

    move-result v1

    .line 1110617
    iput-boolean v1, v7, LX/6Y7;->c:Z

    .line 1110618
    goto :goto_6
.end method
