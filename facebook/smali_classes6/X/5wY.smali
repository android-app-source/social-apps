.class public final LX/5wY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1024649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 1024650
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1024651
    iget-object v1, p0, LX/5wY;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1024652
    iget-object v3, p0, LX/5wY;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel$AlbumModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1024653
    iget-object v5, p0, LX/5wY;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1024654
    iget-object v6, p0, LX/5wY;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1024655
    iget-object v7, p0, LX/5wY;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1024656
    const/4 v8, 0x5

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1024657
    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 1024658
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1024659
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1024660
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1024661
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1024662
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1024663
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1024664
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1024665
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1024666
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1024667
    new-instance v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;-><init>(LX/15i;)V

    .line 1024668
    return-object v1
.end method
