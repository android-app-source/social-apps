.class public final LX/6bC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/6bA;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/6bD;


# direct methods
.method public constructor <init>(LX/6bD;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1113122
    iput-object p1, p0, LX/6bC;->b:LX/6bD;

    iput-object p2, p0, LX/6bC;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1113123
    iget-object v0, p0, LX/6bC;->b:LX/6bD;

    iget-object v1, p0, LX/6bC;->a:Ljava/lang/String;

    .line 1113124
    iget-object v2, v0, LX/6bD;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6bO;

    .line 1113125
    if-eqz v2, :cond_1

    .line 1113126
    iget-object p0, v2, LX/6bO;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz p0, :cond_0

    iget-object p0, v2, LX/6bO;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {p0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result p0

    if-nez p0, :cond_0

    .line 1113127
    iget-object p0, v2, LX/6bO;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 p1, 0x1

    invoke-interface {p0, p1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1113128
    :cond_0
    iget-object v2, v0, LX/6bD;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113129
    :cond_1
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1113130
    return-void
.end method
