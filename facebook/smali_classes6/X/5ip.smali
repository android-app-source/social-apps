.class public final LX/5ip;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 984926
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 984927
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 984928
    :goto_0
    return v1

    .line 984929
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_3

    .line 984930
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 984931
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 984932
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 984933
    const-string v9, "size"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 984934
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 984935
    :cond_1
    const-string v9, "size_dimension"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 984936
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_1

    .line 984937
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 984938
    :cond_3
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 984939
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 984940
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 984941
    :cond_4
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 984942
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v7, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const-wide/16 v2, 0x0

    .line 984943
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 984944
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 984945
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    .line 984946
    const-string v2, "size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984947
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 984948
    :cond_0
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 984949
    if-eqz v0, :cond_1

    .line 984950
    const-string v0, "size_dimension"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 984951
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 984952
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 984953
    return-void
.end method
