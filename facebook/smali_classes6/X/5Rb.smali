.class public LX/5Rb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 915774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 915775
    return-void
.end method

.method public static a(Lcom/facebook/stickers/model/Sticker;)Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 915776
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->setStickerId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/stickers/model/Sticker;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->setPackId(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->setStaticWebUri(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->setStaticDiskUri(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->setAnimatedWebUri(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    if-nez v2, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->setAnimatedDiskUri(Ljava/lang/String;)Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerStickerData$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/facebook/stickers/model/Sticker;->f:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method
