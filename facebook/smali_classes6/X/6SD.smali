.class public final LX/6SD;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1091742
    const-class v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoVODCommentsQueryModel;

    const v0, -0x51daf8eb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchLiveVideoVODCommentsQuery"

    const-string v6, "e61d51bffcdfd0750fc4f868eb26dcc3"

    const-string v7, "video"

    const-string v8, "10155207367521729"

    const-string v9, "10155259087836729"

    .line 1091743
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1091744
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1091745
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1091746
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1091747
    sparse-switch v0, :sswitch_data_0

    .line 1091748
    :goto_0
    return-object p1

    .line 1091749
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1091750
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1091751
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1091752
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1091753
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1091754
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1091755
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1091756
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1091757
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x65d81c4d -> :sswitch_7
        -0x41a91745 -> :sswitch_4
        -0x3fcd81be -> :sswitch_3
        -0x3b85b241 -> :sswitch_8
        -0x1a57a594 -> :sswitch_6
        0x5a7510f -> :sswitch_0
        0xc765056 -> :sswitch_2
        0x1f588673 -> :sswitch_1
        0x69d55a61 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1091758
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 1091759
    :goto_1
    return v0

    .line 1091760
    :pswitch_1
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    const-string v2, "8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    .line 1091761
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1091762
    :pswitch_5
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1091763
    :pswitch_6
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x35
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
