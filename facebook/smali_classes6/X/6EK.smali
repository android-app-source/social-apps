.class public abstract LX/6EK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6ET;

.field public b:Landroid/content/Context;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1066009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066010
    iput-object p1, p0, LX/6EK;->b:Landroid/content/Context;

    .line 1066011
    return-void
.end method


# virtual methods
.method public abstract a()Landroid/content/Intent;
.end method

.method public final b()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1066012
    invoke-virtual {p0}, LX/6EK;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1066013
    const-string v1, "permission"

    iget-object v2, p0, LX/6EK;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1066014
    const-string v1, "app_id"

    iget-object v2, p0, LX/6EK;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1066015
    const-string v1, "app_name"

    iget-object v2, p0, LX/6EK;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1066016
    const-string v1, "calling_url"

    iget-object v2, p0, LX/6EK;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1066017
    const-string v1, "request_source"

    iget-object v2, p0, LX/6EK;->a:LX/6ET;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1066018
    return-object v0
.end method
