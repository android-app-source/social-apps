.class public final LX/5Jz;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/5K1;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/5K0;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 897985
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "binder"

    aput-object v2, v0, v1

    sput-object v0, LX/5Jz;->b:[Ljava/lang/String;

    .line 897986
    sput v3, LX/5Jz;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 897983
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 897984
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/5Jz;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/5Jz;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/5Jz;LX/1De;IILX/5K0;)V
    .locals 1

    .prologue
    .line 897979
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 897980
    iput-object p4, p0, LX/5Jz;->a:LX/5K0;

    .line 897981
    iget-object v0, p0, LX/5Jz;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 897982
    return-void
.end method


# virtual methods
.method public final a(LX/1OX;)LX/5Jz;
    .locals 1

    .prologue
    .line 897977
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    iput-object p1, v0, LX/5K0;->g:LX/1OX;

    .line 897978
    return-object p0
.end method

.method public final a(LX/1Of;)LX/5Jz;
    .locals 1

    .prologue
    .line 897975
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    iput-object p1, v0, LX/5K0;->d:LX/1Of;

    .line 897976
    return-object p0
.end method

.method public final a(LX/1dQ;)LX/5Jz;
    .locals 1

    .prologue
    .line 897973
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    iput-object p1, v0, LX/5K0;->a:LX/1dQ;

    .line 897974
    return-object p0
.end method

.method public final a(LX/3x6;)LX/5Jz;
    .locals 1

    .prologue
    .line 897946
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    iput-object p1, v0, LX/5K0;->f:LX/3x6;

    .line 897947
    return-object p0
.end method

.method public final a(LX/5Je;)LX/5Jz;
    .locals 2

    .prologue
    .line 897970
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    iput-object p1, v0, LX/5K0;->c:LX/5Je;

    .line 897971
    iget-object v0, p0, LX/5Jz;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 897972
    return-object p0
.end method

.method public final a(LX/5K7;)LX/5Jz;
    .locals 1

    .prologue
    .line 897968
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    iput-object p1, v0, LX/5K0;->e:LX/5K7;

    .line 897969
    return-object p0
.end method

.method public final a(Z)LX/5Jz;
    .locals 1

    .prologue
    .line 897966
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    iput-boolean p1, v0, LX/5K0;->h:Z

    .line 897967
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 897962
    invoke-super {p0}, LX/1X5;->a()V

    .line 897963
    const/4 v0, 0x0

    iput-object v0, p0, LX/5Jz;->a:LX/5K0;

    .line 897964
    sget-object v0, LX/5K1;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 897965
    return-void
.end method

.method public final b(Z)LX/5Jz;
    .locals 1

    .prologue
    .line 897960
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    iput-boolean p1, v0, LX/5K0;->k:Z

    .line 897961
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/5K1;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 897950
    iget-object v1, p0, LX/5Jz;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/5Jz;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/5Jz;->c:I

    if-ge v1, v2, :cond_2

    .line 897951
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 897952
    :goto_0
    sget v2, LX/5Jz;->c:I

    if-ge v0, v2, :cond_1

    .line 897953
    iget-object v2, p0, LX/5Jz;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 897954
    sget-object v2, LX/5Jz;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 897955
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 897956
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 897957
    :cond_2
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    .line 897958
    invoke-virtual {p0}, LX/5Jz;->a()V

    .line 897959
    return-object v0
.end method

.method public final h(I)LX/5Jz;
    .locals 1

    .prologue
    .line 897948
    iget-object v0, p0, LX/5Jz;->a:LX/5K0;

    iput p1, v0, LX/5K0;->l:I

    .line 897949
    return-object p0
.end method
