.class public LX/5OM;
.super LX/0ht;
.source ""

# interfaces
.implements LX/5OE;
.implements LX/5OF;


# instance fields
.field public a:Z

.field private l:LX/5OG;

.field public m:LX/5OI;

.field public n:Ljava/lang/CharSequence;

.field public o:Landroid/view/View;

.field public p:LX/5OO;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 908660
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/5OM;-><init>(Landroid/content/Context;I)V

    .line 908661
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 908644
    const/4 v3, 0x1

    .line 908645
    if-ne p2, v3, :cond_2

    .line 908646
    const p2, 0x7f0e0253

    .line 908647
    :cond_0
    :goto_0
    move v0, p2

    .line 908648
    invoke-direct {p0, p1, v0}, LX/0ht;-><init>(Landroid/content/Context;I)V

    .line 908649
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    if-eqz v0, :cond_1

    .line 908650
    sget-object v0, LX/5OV;->SLIDE_UP:LX/5OV;

    invoke-virtual {p0, v0}, LX/0ht;->a(LX/5OV;)V

    .line 908651
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0ht;->c(Z)V

    .line 908652
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5OM;->a:Z

    .line 908653
    return-void

    .line 908654
    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    .line 908655
    const p2, 0x7f0e0255

    goto :goto_0

    .line 908656
    :cond_3
    const/high16 v0, 0x1000000

    if-ge p2, v0, :cond_0

    .line 908657
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 908658
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010247

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 908659
    iget p2, v0, Landroid/util/TypedValue;->resourceId:I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 908642
    invoke-virtual {p0}, LX/0ht;->l()V

    .line 908643
    return-void
.end method

.method public final a(LX/5OG;)V
    .locals 1

    .prologue
    .line 908638
    iput-object p1, p0, LX/5OM;->l:LX/5OG;

    .line 908639
    iget-object v0, p0, LX/5OM;->l:LX/5OG;

    invoke-virtual {v0, p0}, LX/5OG;->a(LX/5OE;)V

    .line 908640
    iget-object v0, p0, LX/5OM;->l:LX/5OG;

    invoke-virtual {v0, p0}, LX/5OG;->a(LX/5OF;)V

    .line 908641
    return-void
.end method

.method public final a(LX/5OG;Z)V
    .locals 0

    .prologue
    .line 908581
    invoke-virtual {p0, p1}, LX/5OM;->a(LX/5OG;)V

    .line 908582
    if-eqz p2, :cond_0

    .line 908583
    invoke-virtual {p0}, LX/0ht;->d()V

    .line 908584
    :goto_0
    return-void

    .line 908585
    :cond_0
    invoke-virtual {p0}, LX/0ht;->f()V

    goto :goto_0
.end method

.method public final a(LX/5OO;)V
    .locals 0

    .prologue
    .line 908636
    iput-object p1, p0, LX/5OM;->p:LX/5OO;

    .line 908637
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 908627
    invoke-virtual {p0}, LX/5OM;->b()LX/5OK;

    move-result-object v0

    .line 908628
    if-nez v0, :cond_0

    .line 908629
    :goto_0
    return-void

    .line 908630
    :cond_0
    iget-boolean v1, p0, LX/0ht;->r:Z

    move v1, v1

    .line 908631
    if-eqz v1, :cond_1

    .line 908632
    invoke-virtual {p0, v0}, LX/0ht;->e(Landroid/view/View;)V

    .line 908633
    invoke-virtual {p0}, LX/0ht;->e()V

    goto :goto_0

    .line 908634
    :cond_1
    invoke-virtual {p0, v0}, LX/0ht;->d(Landroid/view/View;)V

    .line 908635
    invoke-super {p0, p1}, LX/0ht;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 908624
    iget-boolean v0, p0, LX/5OM;->a:Z

    if-eq v0, p1, :cond_0

    .line 908625
    iput-boolean p1, p0, LX/5OM;->a:Z

    .line 908626
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 908662
    iget-object v0, p0, LX/5OM;->p:LX/5OO;

    if-eqz v0, :cond_0

    .line 908663
    iget-object v0, p0, LX/5OM;->p:LX/5OO;

    invoke-interface {v0, p1}, LX/5OO;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 908664
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()LX/5OK;
    .locals 5

    .prologue
    .line 908595
    iget-object v0, p0, LX/5OM;->l:LX/5OG;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/5OM;->l:LX/5OG;

    invoke-virtual {v0}, LX/5OG;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 908596
    new-instance v0, LX/5OK;

    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5OK;-><init>(Landroid/content/Context;)V

    .line 908597
    iget-object v1, p0, LX/5OM;->o:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 908598
    iget-object v1, p0, LX/5OM;->o:Landroid/view/View;

    .line 908599
    invoke-virtual {v0}, LX/5OK;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    .line 908600
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/5OK;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 908601
    invoke-static {v0, v1}, LX/5OK;->setTitleView(LX/5OK;Landroid/view/View;)V

    .line 908602
    iput-object v1, v0, LX/5OK;->m:Landroid/view/View;

    .line 908603
    invoke-virtual {v0, v2}, LX/5OK;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 908604
    :goto_0
    iget-object v1, p0, LX/5OM;->l:LX/5OG;

    invoke-virtual {v0, v1}, LX/5OK;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 908605
    iget-object v1, p0, LX/5OM;->l:LX/5OG;

    invoke-virtual {v0, v1}, LX/5OK;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 908606
    iget-boolean v1, p0, LX/5OM;->a:Z

    invoke-virtual {v0, v1}, LX/5OK;->a(Z)V

    .line 908607
    iget-boolean v1, p0, LX/0ht;->e:Z

    invoke-virtual {v0, v1}, LX/5OK;->setShowFullWidth(Z)V

    .line 908608
    iget v1, p0, LX/0ht;->c:I

    invoke-virtual {v0, v1}, LX/5OK;->setMaxWidth(I)V

    .line 908609
    invoke-virtual {p0}, LX/0ht;->h()Landroid/view/View;

    move-result-object v1

    .line 908610
    if-eqz v1, :cond_0

    invoke-static {v0}, LX/0vv;->t(Landroid/view/View;)I

    move-result v2

    if-nez v2, :cond_0

    .line 908611
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, LX/5OK;->setMinimumWidth(I)V

    .line 908612
    :cond_0
    :goto_1
    return-object v0

    .line 908613
    :cond_1
    iget-object v1, p0, LX/5OM;->n:Ljava/lang/CharSequence;

    .line 908614
    invoke-virtual {v0}, LX/5OK;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    .line 908615
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/5OK;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 908616
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 908617
    invoke-virtual {v0}, LX/5OK;->getHeaderViewsCount()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 908618
    invoke-static {v0}, LX/5OK;->a(LX/5OK;)V

    .line 908619
    :cond_2
    :goto_2
    invoke-virtual {v0, v2}, LX/5OK;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 908620
    goto :goto_0

    .line 908621
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 908622
    :cond_4
    iget-object v3, v0, LX/5OK;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0, v3}, LX/5OK;->setTitleView(LX/5OK;Landroid/view/View;)V

    .line 908623
    iget-object v3, v0, LX/5OK;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 908590
    iget-object v0, p0, LX/5OM;->m:LX/5OI;

    if-nez v0, :cond_0

    .line 908591
    new-instance v0, LX/5OI;

    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5OI;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/5OM;->m:LX/5OI;

    .line 908592
    :cond_0
    iget-object v0, p0, LX/5OM;->m:LX/5OI;

    move-object v0, v0

    .line 908593
    invoke-virtual {p0}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/5OI;->inflate(ILandroid/view/Menu;)V

    .line 908594
    return-void
.end method

.method public final c()LX/5OG;
    .locals 2

    .prologue
    .line 908586
    iget-object v0, p0, LX/5OM;->l:LX/5OG;

    if-nez v0, :cond_0

    .line 908587
    new-instance v0, LX/5OG;

    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 908588
    invoke-virtual {p0, v0}, LX/5OM;->a(LX/5OG;)V

    .line 908589
    :cond_0
    iget-object v0, p0, LX/5OM;->l:LX/5OG;

    return-object v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 908578
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 908579
    iput-object v0, p0, LX/5OM;->n:Ljava/lang/CharSequence;

    .line 908580
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 908569
    invoke-virtual {p0}, LX/5OM;->b()LX/5OK;

    move-result-object v0

    .line 908570
    if-nez v0, :cond_0

    .line 908571
    :goto_0
    return-void

    .line 908572
    :cond_0
    iget-boolean v1, p0, LX/0ht;->r:Z

    move v1, v1

    .line 908573
    if-eqz v1, :cond_1

    .line 908574
    invoke-virtual {p0, v0}, LX/0ht;->e(Landroid/view/View;)V

    .line 908575
    invoke-virtual {p0}, LX/0ht;->e()V

    goto :goto_0

    .line 908576
    :cond_1
    invoke-virtual {p0, v0}, LX/0ht;->d(Landroid/view/View;)V

    .line 908577
    invoke-super {p0}, LX/0ht;->d()V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 908566
    sget-object v0, LX/5OV;->SLIDE_UP:LX/5OV;

    invoke-virtual {p0, v0}, LX/0ht;->a(LX/5OV;)V

    .line 908567
    invoke-super {p0}, LX/0ht;->e()V

    .line 908568
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 908563
    sget-object v0, LX/5OV;->SLIDE_DOWN:LX/5OV;

    invoke-virtual {p0, v0}, LX/0ht;->a(LX/5OV;)V

    .line 908564
    invoke-super {p0}, LX/0ht;->f()V

    .line 908565
    return-void
.end method
