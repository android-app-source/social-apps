.class public final LX/5KK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:LX/0P2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P2",
            "<TK;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/java2js/JSContext;


# direct methods
.method public constructor <init>(Lcom/facebook/java2js/JSContext;)V
    .locals 1

    .prologue
    .line 898540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 898541
    iput-object p1, p0, LX/5KK;->b:Lcom/facebook/java2js/JSContext;

    .line 898542
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    iput-object v0, p0, LX/5KK;->a:LX/0P2;

    .line 898543
    return-void
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<TK;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 898544
    iget-object v0, p0, LX/5KK;->a:LX/0P2;

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)LX/5KK;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/lang/Object;",
            ")",
            "LX/5KK;"
        }
    .end annotation

    .prologue
    .line 898536
    if-nez p2, :cond_0

    .line 898537
    iget-object v0, p0, LX/5KK;->a:LX/0P2;

    iget-object v1, p0, LX/5KK;->b:Lcom/facebook/java2js/JSContext;

    invoke-static {v1}, Lcom/facebook/java2js/JSValue;->makeNull(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 898538
    :goto_0
    return-object p0

    .line 898539
    :cond_0
    iget-object v0, p0, LX/5KK;->a:LX/0P2;

    invoke-virtual {v0, p1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0
.end method
