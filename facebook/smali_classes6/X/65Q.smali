.class public final LX/65Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64e;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/64e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1047072
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1047073
    const/4 v0, 0x0

    iput v0, p0, LX/65Q;->b:I

    .line 1047074
    iput-object p1, p0, LX/65Q;->a:Ljava/util/List;

    .line 1047075
    return-void
.end method


# virtual methods
.method public final a(Ljavax/net/ssl/SSLSocket;)LX/64e;
    .locals 5

    .prologue
    .line 1047076
    const/4 v1, 0x0

    .line 1047077
    iget v0, p0, LX/65Q;->b:I

    iget-object v2, p0, LX/65Q;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 1047078
    iget-object v0, p0, LX/65Q;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/64e;

    .line 1047079
    invoke-virtual {v0, p1}, LX/64e;->a(Ljavax/net/ssl/SSLSocket;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1047080
    add-int/lit8 v1, v2, 0x1

    iput v1, p0, LX/65Q;->b:I

    .line 1047081
    :goto_1
    if-nez v0, :cond_1

    .line 1047082
    new-instance v0, Ljava/net/UnknownServiceException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to find acceptable protocols. isFallback="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, LX/65Q;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", modes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/65Q;->a:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", supported protocols="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1047083
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047084
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1047085
    :cond_1
    iget v1, p0, LX/65Q;->b:I

    move v2, v1

    :goto_2
    iget-object v1, p0, LX/65Q;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_4

    .line 1047086
    iget-object v1, p0, LX/65Q;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/64e;

    invoke-virtual {v1, p1}, LX/64e;->a(Ljavax/net/ssl/SSLSocket;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1047087
    const/4 v1, 0x1

    .line 1047088
    :goto_3
    move v1, v1

    .line 1047089
    iput-boolean v1, p0, LX/65Q;->c:Z

    .line 1047090
    sget-object v1, LX/64t;->a:LX/64t;

    iget-boolean v2, p0, LX/65Q;->d:Z

    invoke-virtual {v1, v0, p1, v2}, LX/64t;->a(LX/64e;Ljavax/net/ssl/SSLSocket;Z)V

    .line 1047091
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 1047092
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1047093
    :cond_4
    const/4 v1, 0x0

    goto :goto_3
.end method
