.class public LX/6AG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "LX/4XS;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/graphql/model/GraphQLNode;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLNode;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058820
    iput-object p1, p0, LX/6AG;->a:Ljava/lang/String;

    .line 1058821
    iput-object p2, p0, LX/6AG;->b:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1058822
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058823
    iget-object v0, p0, LX/6AG;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1058824
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p2, LX/4XS;

    .line 1058825
    iget-object v0, p0, LX/6AG;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1058826
    :goto_0
    return-void

    .line 1058827
    :cond_0
    iget-object v0, p0, LX/6AG;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->pE()LX/0Px;

    move-result-object v0

    .line 1058828
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p1, "confirmed_profiles"

    invoke-virtual {v1, p1, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1058829
    iget-object v0, p0, LX/6AG;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->pF()LX/0Px;

    move-result-object v0

    .line 1058830
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p0, "pending_profiles"

    invoke-virtual {v1, p0, v0}, LX/4VK;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1058831
    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1058832
    const-class v0, Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1058833
    const-string v0, "SocialSearchProfileRecommendationEditMutatingVisitor"

    return-object v0
.end method
