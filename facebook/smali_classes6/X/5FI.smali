.class public final LX/5FI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 885101
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 885102
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885103
    :goto_0
    return v1

    .line 885104
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885105
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 885106
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 885107
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885108
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 885109
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 885110
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 885111
    :cond_2
    const-string v4, "template_themes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 885112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 885113
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_3

    .line 885114
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 885115
    const/4 v4, 0x0

    .line 885116
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_a

    .line 885117
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885118
    :goto_3
    move v3, v4

    .line 885119
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 885120
    :cond_3
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 885121
    goto :goto_1

    .line 885122
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 885123
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 885124
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 885125
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 885126
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885127
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 885128
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 885129
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885130
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_7

    if-eqz v6, :cond_7

    .line 885131
    const-string v7, "color"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 885132
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 885133
    :cond_8
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 885134
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_4

    .line 885135
    :cond_9
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 885136
    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 885137
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 885138
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_3

    :cond_a
    move v3, v4

    move v5, v4

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 885139
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885140
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885141
    if-eqz v0, :cond_0

    .line 885142
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885143
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885144
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885145
    if-eqz v0, :cond_4

    .line 885146
    const-string v1, "template_themes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885147
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 885148
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 885149
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 885150
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885151
    const/4 p1, 0x0

    invoke-virtual {p0, v2, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 885152
    if-eqz p1, :cond_1

    .line 885153
    const-string p3, "color"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885154
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885155
    :cond_1
    const/4 p1, 0x1

    invoke-virtual {p0, v2, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 885156
    if-eqz p1, :cond_2

    .line 885157
    const-string p3, "name"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885158
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885159
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885160
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 885161
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 885162
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885163
    return-void
.end method
