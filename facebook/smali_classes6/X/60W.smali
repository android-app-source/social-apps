.class public final LX/60W;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1038208
    const/4 v8, 0x0

    .line 1038209
    const/4 v7, 0x0

    .line 1038210
    const/4 v6, 0x0

    .line 1038211
    const-wide/16 v4, 0x0

    .line 1038212
    const/4 v3, 0x0

    .line 1038213
    const/4 v2, 0x0

    .line 1038214
    const/4 v1, 0x0

    .line 1038215
    const/4 v0, 0x0

    .line 1038216
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 1038217
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1038218
    const/4 v0, 0x0

    .line 1038219
    :goto_0
    return v0

    .line 1038220
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v10, :cond_a

    .line 1038221
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 1038222
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1038223
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v1, :cond_0

    .line 1038224
    const-string v10, "actors"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1038225
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1038226
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_1

    .line 1038227
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_1

    .line 1038228
    invoke-static {p0, p1}, LX/60S;->b(LX/15w;LX/186;)I

    move-result v9

    .line 1038229
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1038230
    :cond_1
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1038231
    move v9, v1

    goto :goto_1

    .line 1038232
    :cond_2
    const-string v10, "attachments"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1038233
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1038234
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v10, :cond_3

    .line 1038235
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v10, :cond_3

    .line 1038236
    invoke-static {p0, p1}, LX/60U;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1038237
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1038238
    :cond_3
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1038239
    move v5, v1

    goto :goto_1

    .line 1038240
    :cond_4
    const-string v10, "cache_id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1038241
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v4, v1

    goto/16 :goto_1

    .line 1038242
    :cond_5
    const-string v10, "creation_time"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1038243
    const/4 v0, 0x1

    .line 1038244
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto/16 :goto_1

    .line 1038245
    :cond_6
    const-string v10, "id"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1038246
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v8, v1

    goto/16 :goto_1

    .line 1038247
    :cond_7
    const-string v10, "shareable"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1038248
    invoke-static {p0, p1}, LX/60V;->a(LX/15w;LX/186;)I

    move-result v1

    move v7, v1

    goto/16 :goto_1

    .line 1038249
    :cond_8
    const-string v10, "tracking"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1038250
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    move v6, v1

    goto/16 :goto_1

    .line 1038251
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1038252
    :cond_a
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1038253
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1038254
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1038255
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1038256
    if-eqz v0, :cond_b

    .line 1038257
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1038258
    :cond_b
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1038259
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1038260
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1038261
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_c
    move v9, v8

    move v8, v3

    move v12, v2

    move-wide v2, v4

    move v4, v6

    move v5, v7

    move v7, v12

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 1038262
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1038263
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1038264
    if-eqz v0, :cond_1

    .line 1038265
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1038266
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1038267
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 1038268
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2}, LX/60S;->a(LX/15i;ILX/0nX;)V

    .line 1038269
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1038270
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1038271
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1038272
    if-eqz v0, :cond_3

    .line 1038273
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1038274
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1038275
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 1038276
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v4

    invoke-static {p0, v4, p2, p3}, LX/60U;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1038277
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1038278
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1038279
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1038280
    if-eqz v0, :cond_4

    .line 1038281
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1038282
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1038283
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1038284
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 1038285
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1038286
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1038287
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1038288
    if-eqz v0, :cond_6

    .line 1038289
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1038290
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1038291
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1038292
    if-eqz v0, :cond_7

    .line 1038293
    const-string v1, "shareable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1038294
    invoke-static {p0, v0, p2}, LX/60V;->a(LX/15i;ILX/0nX;)V

    .line 1038295
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1038296
    if-eqz v0, :cond_8

    .line 1038297
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1038298
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1038299
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1038300
    return-void
.end method
