.class public final LX/5bA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 26

    .prologue
    .line 957098
    const/16 v22, 0x0

    .line 957099
    const/16 v21, 0x0

    .line 957100
    const/16 v20, 0x0

    .line 957101
    const/16 v19, 0x0

    .line 957102
    const/16 v18, 0x0

    .line 957103
    const/16 v17, 0x0

    .line 957104
    const/16 v16, 0x0

    .line 957105
    const/4 v15, 0x0

    .line 957106
    const/4 v14, 0x0

    .line 957107
    const/4 v13, 0x0

    .line 957108
    const/4 v12, 0x0

    .line 957109
    const/4 v11, 0x0

    .line 957110
    const/4 v10, 0x0

    .line 957111
    const/4 v9, 0x0

    .line 957112
    const/4 v8, 0x0

    .line 957113
    const/4 v7, 0x0

    .line 957114
    const/4 v6, 0x0

    .line 957115
    const/4 v5, 0x0

    .line 957116
    const/4 v4, 0x0

    .line 957117
    const/4 v3, 0x0

    .line 957118
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    .line 957119
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 957120
    const/4 v3, 0x0

    .line 957121
    :goto_0
    return v3

    .line 957122
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 957123
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_13

    .line 957124
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v23

    .line 957125
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 957126
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    if-eqz v23, :cond_1

    .line 957127
    const-string v24, "__type__"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_2

    const-string v24, "__typename"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 957128
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v22

    goto :goto_1

    .line 957129
    :cond_3
    const-string v24, "answered_video_call"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 957130
    const/4 v5, 0x1

    .line 957131
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto :goto_1

    .line 957132
    :cond_4
    const-string v24, "answered_voice_call"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 957133
    const/4 v4, 0x1

    .line 957134
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto :goto_1

    .line 957135
    :cond_5
    const-string v24, "blob_attachments"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 957136
    invoke-static/range {p0 .. p1}, LX/5b6;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 957137
    :cond_6
    const-string v24, "commerce_message_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 957138
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMNCommerceMessageType;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    goto :goto_1

    .line 957139
    :cond_7
    const-string v24, "extensible_attachment"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 957140
    invoke-static/range {p0 .. p1}, LX/5bW;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 957141
    :cond_8
    const-string v24, "genie_sender"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 957142
    invoke-static/range {p0 .. p1}, LX/5aW;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 957143
    :cond_9
    const-string v24, "is_sponsored"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 957144
    const/4 v3, 0x1

    .line 957145
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto/16 :goto_1

    .line 957146
    :cond_a
    const-string v24, "message"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 957147
    invoke-static/range {p0 .. p1}, LX/5b7;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 957148
    :cond_b
    const-string v24, "message_id"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 957149
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 957150
    :cond_c
    const-string v24, "message_sender"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 957151
    invoke-static/range {p0 .. p1}, LX/5bu;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 957152
    :cond_d
    const-string v24, "montage_reply_data"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 957153
    invoke-static/range {p0 .. p1}, LX/5b8;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 957154
    :cond_e
    const-string v24, "p2p_log_message_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 957155
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentMessageType;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto/16 :goto_1

    .line 957156
    :cond_f
    const-string v24, "reply_type"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_10

    .line 957157
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageAdminReplyType;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 957158
    :cond_10
    const-string v24, "snippet"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 957159
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 957160
    :cond_11
    const-string v24, "sticker"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_12

    .line 957161
    invoke-static/range {p0 .. p1}, LX/5b9;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 957162
    :cond_12
    const-string v24, "tags_list"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 957163
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 957164
    :cond_13
    const/16 v23, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 957165
    const/16 v23, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 957166
    if-eqz v5, :cond_14

    .line 957167
    const/4 v5, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 957168
    :cond_14
    if-eqz v4, :cond_15

    .line 957169
    const/4 v4, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 957170
    :cond_15
    const/4 v4, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 957171
    const/4 v4, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 957172
    const/4 v4, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 957173
    const/4 v4, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 957174
    if-eqz v3, :cond_16

    .line 957175
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->a(IZ)V

    .line 957176
    :cond_16
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 957177
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 957178
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 957179
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 957180
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 957181
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 957182
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 957183
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 957184
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 957185
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v5, 0x10

    const/16 v4, 0xd

    const/16 v3, 0xc

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 957186
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 957187
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 957188
    if-eqz v0, :cond_0

    .line 957189
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957190
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 957191
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 957192
    if-eqz v0, :cond_1

    .line 957193
    const-string v1, "answered_video_call"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957194
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 957195
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 957196
    if-eqz v0, :cond_2

    .line 957197
    const-string v1, "answered_voice_call"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957198
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 957199
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957200
    if-eqz v0, :cond_4

    .line 957201
    const-string v1, "blob_attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957202
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 957203
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 957204
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v6

    invoke-static {p0, v6, p2, p3}, LX/5b6;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 957205
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 957206
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 957207
    :cond_4
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 957208
    if-eqz v0, :cond_5

    .line 957209
    const-string v0, "commerce_message_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957210
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957211
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957212
    if-eqz v0, :cond_6

    .line 957213
    const-string v1, "extensible_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957214
    invoke-static {p0, v0, p2, p3}, LX/5bW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957215
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957216
    if-eqz v0, :cond_7

    .line 957217
    const-string v1, "genie_sender"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957218
    invoke-static {p0, v0, p2, p3}, LX/5aW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957219
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 957220
    if-eqz v0, :cond_8

    .line 957221
    const-string v1, "is_sponsored"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957222
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 957223
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957224
    if-eqz v0, :cond_9

    .line 957225
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957226
    invoke-static {p0, v0, p2}, LX/5b7;->a(LX/15i;ILX/0nX;)V

    .line 957227
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 957228
    if-eqz v0, :cond_a

    .line 957229
    const-string v1, "message_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957230
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957231
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957232
    if-eqz v0, :cond_b

    .line 957233
    const-string v1, "message_sender"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957234
    invoke-static {p0, v0, p2, p3}, LX/5bu;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 957235
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957236
    if-eqz v0, :cond_c

    .line 957237
    const-string v1, "montage_reply_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957238
    invoke-static {p0, v0, p2}, LX/5b8;->a(LX/15i;ILX/0nX;)V

    .line 957239
    :cond_c
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 957240
    if-eqz v0, :cond_d

    .line 957241
    const-string v0, "p2p_log_message_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957242
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957243
    :cond_d
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 957244
    if-eqz v0, :cond_e

    .line 957245
    const-string v0, "reply_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957246
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957247
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 957248
    if-eqz v0, :cond_f

    .line 957249
    const-string v1, "snippet"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957250
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957251
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957252
    if-eqz v0, :cond_10

    .line 957253
    const-string v1, "sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957254
    invoke-static {p0, v0, p2}, LX/5b9;->a(LX/15i;ILX/0nX;)V

    .line 957255
    :cond_10
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 957256
    if-eqz v0, :cond_11

    .line 957257
    const-string v0, "tags_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957258
    invoke-virtual {p0, p1, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 957259
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 957260
    return-void
.end method
