.class public LX/6eI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final e:Ljava/lang/Object;


# instance fields
.field public final b:LX/3QX;

.field public final c:LX/0Uh;

.field public d:Landroid/net/ConnectivityManager;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1117734
    const-class v0, LX/6eI;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6eI;->a:Ljava/lang/String;

    .line 1117735
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6eI;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3QX;LX/0Uh;Landroid/net/ConnectivityManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1117736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117737
    iput-object p1, p0, LX/6eI;->b:LX/3QX;

    .line 1117738
    iput-object p2, p0, LX/6eI;->c:LX/0Uh;

    .line 1117739
    iput-object p3, p0, LX/6eI;->d:Landroid/net/ConnectivityManager;

    .line 1117740
    return-void
.end method

.method public static a(LX/0QB;)LX/6eI;
    .locals 9

    .prologue
    .line 1117741
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1117742
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1117743
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1117744
    if-nez v1, :cond_0

    .line 1117745
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1117746
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1117747
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1117748
    sget-object v1, LX/6eI;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1117749
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1117750
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1117751
    :cond_1
    if-nez v1, :cond_4

    .line 1117752
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1117753
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1117754
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1117755
    new-instance p0, LX/6eI;

    invoke-static {v0}, LX/3QX;->a(LX/0QB;)LX/3QX;

    move-result-object v1

    check-cast v1, LX/3QX;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/0nI;->b(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v8

    check-cast v8, Landroid/net/ConnectivityManager;

    invoke-direct {p0, v1, v7, v8}, LX/6eI;-><init>(LX/3QX;LX/0Uh;Landroid/net/ConnectivityManager;)V

    .line 1117756
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1117757
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1117758
    if-nez v1, :cond_2

    .line 1117759
    sget-object v0, LX/6eI;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6eI;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1117760
    :goto_1
    if-eqz v0, :cond_3

    .line 1117761
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1117762
    :goto_3
    check-cast v0, LX/6eI;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1117763
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1117764
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1117765
    :catchall_1
    move-exception v0

    .line 1117766
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1117767
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1117768
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1117769
    :cond_2
    :try_start_8
    sget-object v0, LX/6eI;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6eI;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 3

    .prologue
    .line 1117770
    monitor-enter p0

    :try_start_0
    const/4 v0, 0x1

    .line 1117771
    iget-object v1, p0, LX/6eI;->d:Landroid/net/ConnectivityManager;

    invoke-virtual {v1, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1117772
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_0
    move v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1117773
    if-eqz v0, :cond_0

    .line 1117774
    const/4 v0, 0x0

    .line 1117775
    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    const/4 v0, 0x0

    .line 1117776
    iget-object v1, p0, LX/6eI;->c:LX/0Uh;

    const/16 v2, 0x129

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1117777
    :cond_1
    :goto_2
    move v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1117778
    goto :goto_1

    .line 1117779
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    const/4 v0, 0x0

    goto :goto_0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1117780
    :cond_3
    iget-object v1, p0, LX/6eI;->b:LX/3QX;

    sget-object v2, LX/6hP;->DATA_SAVER_MODE_ENABLED:LX/6hP;

    invoke-virtual {v1, v2}, LX/3QX;->a(LX/6hP;)LX/0am;

    move-result-object v1

    .line 1117781
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1117782
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_2
.end method
