.class public LX/6Fi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1068887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/bugreporter/BugReport;)Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1068888
    iget-object v0, p0, Lcom/facebook/bugreporter/BugReport;->u:Ljava/lang/String;

    move-object v0, v0

    .line 1068889
    iget-object v1, p0, Lcom/facebook/bugreporter/BugReport;->v:Ljava/lang/String;

    move-object v1, v1

    .line 1068890
    iget-boolean v2, p0, Lcom/facebook/bugreporter/BugReport;->x:Z

    move v2, v2

    .line 1068891
    invoke-static {v0, v1, v2}, LX/6Fi;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1068892
    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1068893
    :cond_0
    const/4 v0, 0x0

    .line 1068894
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v0

    .line 1068895
    iput-object p0, v0, LX/0SK;->a:Ljava/lang/String;

    .line 1068896
    move-object v0, v0

    .line 1068897
    iput-object p1, v0, LX/0SK;->b:Ljava/lang/String;

    .line 1068898
    move-object v0, v0

    .line 1068899
    iput-boolean p2, v0, LX/0SK;->d:Z

    .line 1068900
    move-object v0, v0

    .line 1068901
    invoke-virtual {v0}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    goto :goto_0
.end method
