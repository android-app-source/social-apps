.class public final LX/5CY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 868260
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 868261
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 868262
    :goto_0
    return v1

    .line 868263
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 868264
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 868265
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 868266
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 868267
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 868268
    const-string v3, "edges"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 868269
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 868270
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 868271
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 868272
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 868273
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_a

    .line 868274
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 868275
    :goto_3
    move v2, v3

    .line 868276
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 868277
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 868278
    goto :goto_1

    .line 868279
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 868280
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 868281
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 868282
    :cond_5
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 868283
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 868284
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 868285
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_5

    if-eqz v7, :cond_5

    .line 868286
    const-string v8, "is_currently_selected"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 868287
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    move v6, v2

    move v2, v4

    goto :goto_4

    .line 868288
    :cond_6
    const-string v8, "node"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 868289
    invoke-static {p0, p1}, LX/5CX;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_4

    .line 868290
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 868291
    :cond_8
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 868292
    if-eqz v2, :cond_9

    .line 868293
    invoke-virtual {p1, v3, v6}, LX/186;->a(IZ)V

    .line 868294
    :cond_9
    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 868295
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_a
    move v2, v3

    move v5, v3

    move v6, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 868296
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 868297
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 868298
    if-eqz v0, :cond_3

    .line 868299
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868300
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 868301
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 868302
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 868303
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 868304
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 868305
    if-eqz v3, :cond_0

    .line 868306
    const-string p1, "is_currently_selected"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868307
    invoke-virtual {p2, v3}, LX/0nX;->a(Z)V

    .line 868308
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 868309
    if-eqz v3, :cond_1

    .line 868310
    const-string p1, "node"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 868311
    invoke-static {p0, v3, p2, p3}, LX/5CX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 868312
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 868313
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 868314
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 868315
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 868316
    return-void
.end method
