.class public final LX/63P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:LX/63Q;


# direct methods
.method public constructor <init>(LX/63Q;)V
    .locals 0

    .prologue
    .line 1043261
    iput-object p1, p0, LX/63P;->a:LX/63Q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 1043260
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1043236
    iget-object v1, p0, LX/63P;->a:LX/63Q;

    iget-boolean v1, v1, LX/63Q;->d:Z

    if-eqz v1, :cond_2

    .line 1043237
    iget-object v1, p0, LX/63P;->a:LX/63Q;

    iget-object v1, v1, LX/63Q;->a:LX/63S;

    invoke-interface {v1}, LX/63S;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1043238
    iget-object v1, p0, LX/63P;->a:LX/63Q;

    .line 1043239
    iget-object v2, v1, LX/63Q;->a:LX/63S;

    invoke-interface {v2}, LX/63S;->mK_()LX/63R;

    move-result-object v2

    invoke-interface {v2}, LX/63R;->getFadingView()Landroid/view/View;

    move-result-object v2

    .line 1043240
    iget-object p1, v1, LX/63Q;->a:LX/63S;

    invoke-interface {p1}, LX/63S;->g()I

    move-result p1

    int-to-float p1, p1

    .line 1043241
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result p2

    if-eqz p2, :cond_0

    .line 1043242
    const/4 p2, 0x0

    invoke-virtual {v2, p2}, Landroid/view/View;->setVisibility(I)V

    .line 1043243
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object p2, v1, LX/63Q;->b:LX/63T;

    invoke-interface {p2}, LX/63T;->getHeight()I

    move-result p2

    sub-int p2, v2, p2

    .line 1043244
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1043245
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p3

    int-to-float p4, p2

    cmpg-float p3, p3, p4

    if-gez p3, :cond_1

    .line 1043246
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    int-to-float p1, p2

    div-float/2addr v2, p1

    .line 1043247
    :cond_1
    move v1, v2

    .line 1043248
    iget-object v2, p0, LX/63P;->a:LX/63Q;

    cmpl-float v3, v1, v3

    if-nez v3, :cond_3

    .line 1043249
    :goto_0
    iput-boolean v0, v2, LX/63Q;->f:Z

    .line 1043250
    iget-object v0, p0, LX/63P;->a:LX/63Q;

    iget-object v0, v0, LX/63Q;->a:LX/63S;

    invoke-interface {v0}, LX/63S;->mK_()LX/63R;

    move-result-object v0

    .line 1043251
    invoke-interface {v0}, LX/63R;->getFadingView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1043252
    iget-object v2, p0, LX/63P;->a:LX/63Q;

    iget-object v2, v2, LX/63Q;->b:LX/63T;

    invoke-interface {v2, v1}, LX/63T;->a(F)V

    .line 1043253
    invoke-interface {v0, v1}, LX/63R;->a(F)V

    .line 1043254
    :cond_2
    :goto_1
    return-void

    .line 1043255
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1043256
    :cond_4
    iget-object v1, p0, LX/63P;->a:LX/63Q;

    iget-boolean v1, v1, LX/63Q;->f:Z

    if-nez v1, :cond_2

    .line 1043257
    iget-object v1, p0, LX/63P;->a:LX/63Q;

    .line 1043258
    iput-boolean v0, v1, LX/63Q;->f:Z

    .line 1043259
    iget-object v0, p0, LX/63P;->a:LX/63Q;

    iget-object v0, v0, LX/63Q;->b:LX/63T;

    invoke-interface {v0, v3}, LX/63T;->a(F)V

    goto :goto_1
.end method
