.class public final LX/6M5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1079354
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1079355
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1079356
    :goto_0
    return v1

    .line 1079357
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1079358
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1079359
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1079360
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1079361
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1079362
    const-string v5, "context_status"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1079363
    invoke-static {p0, p1}, LX/6M3;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1079364
    :cond_2
    const-string v5, "context_status_secondary"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1079365
    invoke-static {p0, p1}, LX/6M4;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1079366
    :cond_3
    const-string v5, "context_type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1079367
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLUserChatContextType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLUserChatContextType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 1079368
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1079369
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1079370
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1079371
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1079372
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1079373
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1079374
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1079375
    if-eqz v0, :cond_0

    .line 1079376
    const-string v1, "context_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1079377
    invoke-static {p0, v0, p2}, LX/6M3;->a(LX/15i;ILX/0nX;)V

    .line 1079378
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1079379
    if-eqz v0, :cond_1

    .line 1079380
    const-string v1, "context_status_secondary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1079381
    invoke-static {p0, v0, p2}, LX/6M4;->a(LX/15i;ILX/0nX;)V

    .line 1079382
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1079383
    if-eqz v0, :cond_2

    .line 1079384
    const-string v0, "context_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1079385
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1079386
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1079387
    return-void
.end method
