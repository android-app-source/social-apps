.class public final LX/663;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public final d:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1049195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049196
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, LX/663;->d:[I

    return-void
.end method


# virtual methods
.method public final a(III)LX/663;
    .locals 3

    .prologue
    .line 1049170
    iget-object v0, p0, LX/663;->d:[I

    array-length v0, v0

    if-lt p1, v0, :cond_0

    .line 1049171
    :goto_0
    return-object p0

    .line 1049172
    :cond_0
    const/4 v0, 0x1

    shl-int/2addr v0, p1

    .line 1049173
    iget v1, p0, LX/663;->a:I

    or-int/2addr v1, v0

    iput v1, p0, LX/663;->a:I

    .line 1049174
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    .line 1049175
    iget v1, p0, LX/663;->b:I

    or-int/2addr v1, v0

    iput v1, p0, LX/663;->b:I

    .line 1049176
    :goto_1
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_2

    .line 1049177
    iget v1, p0, LX/663;->c:I

    or-int/2addr v0, v1

    iput v0, p0, LX/663;->c:I

    .line 1049178
    :goto_2
    iget-object v0, p0, LX/663;->d:[I

    aput p3, v0, p1

    goto :goto_0

    .line 1049179
    :cond_1
    iget v1, p0, LX/663;->b:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iput v1, p0, LX/663;->b:I

    goto :goto_1

    .line 1049180
    :cond_2
    iget v1, p0, LX/663;->c:I

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, v1

    iput v0, p0, LX/663;->c:I

    goto :goto_2
.end method

.method public final a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1049197
    shl-int v1, v0, p1

    .line 1049198
    iget v2, p0, LX/663;->a:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1049193
    iget v0, p0, LX/663;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    move-result v0

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 1049194
    iget-object v0, p0, LX/663;->d:[I

    aget v0, v0, p1

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1049192
    iget v0, p0, LX/663;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/663;->d:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final c(I)I
    .locals 4

    .prologue
    .line 1049182
    const/4 v0, 0x0

    .line 1049183
    const/4 v1, 0x1

    .line 1049184
    shl-int v2, v1, p1

    .line 1049185
    iget v3, p0, LX/663;->c:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_2

    :goto_0
    move v1, v1

    .line 1049186
    if-eqz v1, :cond_0

    const/4 v0, 0x2

    .line 1049187
    :cond_0
    const/4 v1, 0x1

    .line 1049188
    shl-int v2, v1, p1

    .line 1049189
    iget v3, p0, LX/663;->b:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_3

    :goto_1
    move v1, v1

    .line 1049190
    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x1

    .line 1049191
    :cond_1
    return v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final f(I)I
    .locals 2

    .prologue
    .line 1049181
    iget v0, p0, LX/663;->a:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/663;->d:[I

    const/4 v1, 0x7

    aget p1, v0, v1

    :cond_0
    return p1
.end method
