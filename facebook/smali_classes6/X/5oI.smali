.class public final LX/5oI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 1006499
    const/4 v15, 0x0

    .line 1006500
    const/4 v14, 0x0

    .line 1006501
    const/4 v13, 0x0

    .line 1006502
    const/4 v12, 0x0

    .line 1006503
    const/4 v11, 0x0

    .line 1006504
    const/4 v10, 0x0

    .line 1006505
    const/4 v9, 0x0

    .line 1006506
    const/4 v8, 0x0

    .line 1006507
    const/4 v7, 0x0

    .line 1006508
    const/4 v6, 0x0

    .line 1006509
    const/4 v5, 0x0

    .line 1006510
    const/4 v4, 0x0

    .line 1006511
    const/4 v3, 0x0

    .line 1006512
    const/4 v2, 0x0

    .line 1006513
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 1006514
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1006515
    const/4 v2, 0x0

    .line 1006516
    :goto_0
    return v2

    .line 1006517
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1006518
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 1006519
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 1006520
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1006521
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 1006522
    const-string v17, "checkin_location"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1006523
    invoke-static/range {p0 .. p1}, LX/5o6;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 1006524
    :cond_2
    const-string v17, "frame"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1006525
    invoke-static/range {p0 .. p1}, LX/5j6;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 1006526
    :cond_3
    const-string v17, "frame_pack"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1006527
    invoke-static/range {p0 .. p1}, LX/5il;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 1006528
    :cond_4
    const-string v17, "link_attachment"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1006529
    invoke-static/range {p0 .. p1}, LX/5o7;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1006530
    :cond_5
    const-string v17, "mask_effect"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1006531
    invoke-static/range {p0 .. p1}, LX/5oB;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1006532
    :cond_6
    const-string v17, "meme_category"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1006533
    invoke-static/range {p0 .. p1}, LX/5PC;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1006534
    :cond_7
    const-string v17, "minutiae_action"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1006535
    invoke-static/range {p0 .. p1}, LX/5oD;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1006536
    :cond_8
    const-string v17, "particle_effect"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1006537
    invoke-static/range {p0 .. p1}, LX/5ji;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1006538
    :cond_9
    const-string v17, "profile_picture_overlay"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1006539
    invoke-static/range {p0 .. p1}, LX/5Qb;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1006540
    :cond_a
    const-string v17, "shader_filter"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 1006541
    invoke-static/range {p0 .. p1}, LX/5jl;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1006542
    :cond_b
    const-string v17, "style_transfer"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 1006543
    invoke-static/range {p0 .. p1}, LX/5jr;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1006544
    :cond_c
    const-string v17, "suggested_cover_photos"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 1006545
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1006546
    :cond_d
    const-string v17, "tagging_action"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 1006547
    invoke-static/range {p0 .. p1}, LX/5oG;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1006548
    :cond_e
    const-string v17, "thumbnail_image"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1006549
    invoke-static/range {p0 .. p1}, LX/5oH;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1006550
    :cond_f
    const/16 v16, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1006551
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1006552
    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1006553
    const/4 v14, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1006554
    const/4 v13, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1006555
    const/4 v12, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1006556
    const/4 v11, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1006557
    const/4 v10, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1006558
    const/4 v9, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1006559
    const/16 v8, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 1006560
    const/16 v7, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1006561
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1006562
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1006563
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1006564
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1006565
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 1006436
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1006437
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006438
    if-eqz v0, :cond_0

    .line 1006439
    const-string v1, "checkin_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006440
    invoke-static {p0, v0, p2}, LX/5o6;->a(LX/15i;ILX/0nX;)V

    .line 1006441
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006442
    if-eqz v0, :cond_1

    .line 1006443
    const-string v1, "frame"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006444
    invoke-static {p0, v0, p2, p3}, LX/5j6;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006445
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006446
    if-eqz v0, :cond_2

    .line 1006447
    const-string v1, "frame_pack"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006448
    invoke-static {p0, v0, p2, p3}, LX/5il;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006449
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006450
    if-eqz v0, :cond_3

    .line 1006451
    const-string v1, "link_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006452
    invoke-static {p0, v0, p2}, LX/5o7;->a(LX/15i;ILX/0nX;)V

    .line 1006453
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006454
    if-eqz v0, :cond_4

    .line 1006455
    const-string v1, "mask_effect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006456
    invoke-static {p0, v0, p2, p3}, LX/5oB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006457
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006458
    if-eqz v0, :cond_5

    .line 1006459
    const-string v1, "meme_category"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006460
    invoke-static {p0, v0, p2, p3}, LX/5PC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006461
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006462
    if-eqz v0, :cond_6

    .line 1006463
    const-string v1, "minutiae_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006464
    invoke-static {p0, v0, p2, p3}, LX/5oD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006465
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006466
    if-eqz v0, :cond_7

    .line 1006467
    const-string v1, "particle_effect"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006468
    invoke-static {p0, v0, p2, p3}, LX/5ji;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006469
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006470
    if-eqz v0, :cond_8

    .line 1006471
    const-string v1, "profile_picture_overlay"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006472
    invoke-static {p0, v0, p2, p3}, LX/5Qb;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006473
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006474
    if-eqz v0, :cond_9

    .line 1006475
    const-string v1, "shader_filter"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006476
    invoke-static {p0, v0, p2}, LX/5jl;->a(LX/15i;ILX/0nX;)V

    .line 1006477
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006478
    if-eqz v0, :cond_a

    .line 1006479
    const-string v1, "style_transfer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006480
    invoke-static {p0, v0, p2, p3}, LX/5jr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006481
    :cond_a
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1006482
    if-eqz v0, :cond_b

    .line 1006483
    const-string v0, "suggested_cover_photos"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006484
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1006485
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006486
    if-eqz v0, :cond_d

    .line 1006487
    const-string v1, "tagging_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006488
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1006489
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_c

    .line 1006490
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5oG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006491
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1006492
    :cond_c
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1006493
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006494
    if-eqz v0, :cond_e

    .line 1006495
    const-string v1, "thumbnail_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006496
    invoke-static {p0, v0, p2}, LX/5oH;->a(LX/15i;ILX/0nX;)V

    .line 1006497
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1006498
    return-void
.end method
