.class public LX/6eL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field private static final f:LX/0Tn;

.field private static final g:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1117860
    sget-object v0, LX/0Tm;->c:LX/0Tn;

    const-string v1, "low_data_mode/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1117861
    sput-object v0, LX/6eL;->f:LX/0Tn;

    const-string v1, "deferred_images/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6eL;->g:LX/0Tn;

    .line 1117862
    sget-object v0, LX/6eL;->f:LX/0Tn;

    const-string v1, "low_data_mode_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6eL;->a:LX/0Tn;

    .line 1117863
    sget-object v0, LX/6eL;->f:LX/0Tn;

    const-string v1, "low_data_mode_nux_anchor"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6eL;->b:LX/0Tn;

    .line 1117864
    sget-object v0, LX/6eL;->f:LX/0Tn;

    const-string v1, "data_saver_mode_last_reset_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6eL;->c:LX/0Tn;

    .line 1117865
    sget-object v0, LX/6eL;->f:LX/0Tn;

    const-string v1, "data_saver_mode_bytes_saved"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6eL;->d:LX/0Tn;

    .line 1117866
    sget-object v0, LX/6eL;->g:LX/0Tn;

    const-string v1, "data_saved_mode_images_deferred"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6eL;->e:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1117867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
