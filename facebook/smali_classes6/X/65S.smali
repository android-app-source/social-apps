.class public final LX/65S;
.super LX/65R;
.source ""


# instance fields
.field public a:Ljava/net/Socket;

.field public volatile b:LX/65c;

.field public c:I

.field public d:LX/671;

.field public e:LX/670;

.field public f:I

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/Reference",
            "<",
            "LX/65W;",
            ">;>;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:J

.field public final k:LX/657;

.field private l:Ljava/net/Socket;

.field public m:LX/64l;

.field private n:LX/64x;


# direct methods
.method public constructor <init>(LX/657;)V
    .locals 2

    .prologue
    .line 1047097
    invoke-direct {p0}, LX/65R;-><init>()V

    .line 1047098
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/65S;->g:Ljava/util/List;

    .line 1047099
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LX/65S;->i:J

    .line 1047100
    iput-object p1, p0, LX/65S;->k:LX/657;

    .line 1047101
    return-void
.end method

.method private a(IILX/650;LX/64q;)LX/650;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1047339
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CONNECT "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-static {p4, v1}, LX/65A;->a(LX/64q;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " HTTP/1.1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1047340
    :cond_0
    new-instance v4, LX/66H;

    iget-object v0, p0, LX/65S;->d:LX/671;

    iget-object v1, p0, LX/65S;->e:LX/670;

    invoke-direct {v4, v2, v2, v0, v1}, LX/66H;-><init>(LX/64w;LX/65W;LX/671;LX/670;)V

    .line 1047341
    iget-object v0, p0, LX/65S;->d:LX/671;

    invoke-interface {v0}, LX/65D;->a()LX/65f;

    move-result-object v0

    int-to-long v6, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7, v1}, LX/65f;->a(JLjava/util/concurrent/TimeUnit;)LX/65f;

    .line 1047342
    iget-object v0, p0, LX/65S;->e:LX/670;

    invoke-interface {v0}, LX/65J;->a()LX/65f;

    move-result-object v0

    int-to-long v6, p2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7, v1}, LX/65f;->a(JLjava/util/concurrent/TimeUnit;)LX/65f;

    .line 1047343
    iget-object v0, p3, LX/650;->c:LX/64n;

    move-object v0, v0

    .line 1047344
    invoke-virtual {v4, v0, v3}, LX/66H;->a(LX/64n;Ljava/lang/String;)V

    .line 1047345
    invoke-virtual {v4}, LX/66H;->c()V

    .line 1047346
    invoke-virtual {v4}, LX/66H;->d()LX/654;

    move-result-object v0

    .line 1047347
    iput-object p3, v0, LX/654;->a:LX/650;

    .line 1047348
    move-object v0, v0

    .line 1047349
    invoke-virtual {v0}, LX/654;->a()LX/655;

    move-result-object v5

    .line 1047350
    invoke-static {v5}, LX/66M;->a(LX/655;)J

    move-result-wide v0

    .line 1047351
    const-wide/16 v6, -0x1

    cmp-long v6, v0, v6

    if-nez v6, :cond_1

    .line 1047352
    const-wide/16 v0, 0x0

    .line 1047353
    :cond_1
    invoke-virtual {v4, v0, v1}, LX/66H;->a(J)LX/65D;

    move-result-object v0

    .line 1047354
    const v1, 0x7fffffff

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v4}, LX/65A;->b(LX/65D;ILjava/util/concurrent/TimeUnit;)Z

    .line 1047355
    invoke-interface {v0}, LX/65D;->close()V

    .line 1047356
    iget v0, v5, LX/655;->c:I

    move v0, v0

    .line 1047357
    sparse-switch v0, :sswitch_data_0

    .line 1047358
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response code for CONNECT: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1047359
    iget v2, v5, LX/655;->c:I

    move v2, v2

    .line 1047360
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047361
    :sswitch_0
    iget-object v0, p0, LX/65S;->d:LX/671;

    invoke-interface {v0}, LX/671;->c()LX/672;

    move-result-object v0

    invoke-virtual {v0}, LX/672;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/65S;->e:LX/670;

    invoke-interface {v0}, LX/670;->c()LX/672;

    move-result-object v0

    invoke-virtual {v0}, LX/672;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1047362
    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "TLS tunnel buffered too many bytes!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object p3, v2

    .line 1047363
    :goto_0
    return-object p3

    .line 1047364
    :sswitch_1
    iget-object v0, p0, LX/65S;->k:LX/657;

    .line 1047365
    iget-object v1, v0, LX/657;->a:LX/64R;

    move-object v0, v1

    .line 1047366
    iget-object v1, v0, LX/64R;->d:LX/64S;

    move-object v0, v1

    .line 1047367
    invoke-interface {v0}, LX/64S;->a()LX/650;

    move-result-object p3

    .line 1047368
    if-nez p3, :cond_4

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to authenticate with proxy"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047369
    :cond_4
    const-string v0, "close"

    const-string v1, "Connection"

    invoke-virtual {v5, v1}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x197 -> :sswitch_1
    .end sparse-switch
.end method

.method private a(IIILX/65Q;)V
    .locals 6

    .prologue
    const/16 v5, 0x15

    const/4 v4, 0x0

    .line 1047314
    new-instance v0, LX/64z;

    invoke-direct {v0}, LX/64z;-><init>()V

    iget-object v1, p0, LX/65S;->k:LX/657;

    .line 1047315
    iget-object v2, v1, LX/657;->a:LX/64R;

    move-object v1, v2

    .line 1047316
    iget-object v2, v1, LX/64R;->a:LX/64q;

    move-object v1, v2

    .line 1047317
    invoke-virtual {v0, v1}, LX/64z;->a(LX/64q;)LX/64z;

    move-result-object v0

    const-string v1, "Host"

    iget-object v2, p0, LX/65S;->k:LX/657;

    .line 1047318
    iget-object v3, v2, LX/657;->a:LX/64R;

    move-object v2, v3

    .line 1047319
    iget-object v3, v2, LX/64R;->a:LX/64q;

    move-object v2, v3

    .line 1047320
    const/4 v3, 0x1

    invoke-static {v2, v3}, LX/65A;->a(LX/64q;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    move-result-object v0

    const-string v1, "Proxy-Connection"

    const-string v2, "Keep-Alive"

    .line 1047321
    invoke-virtual {v0, v1, v2}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    move-result-object v0

    const-string v1, "User-Agent"

    .line 1047322
    const-string v2, "okhttp/3.4.1"

    move-object v2, v2

    .line 1047323
    invoke-virtual {v0, v1, v2}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    move-result-object v0

    .line 1047324
    invoke-virtual {v0}, LX/64z;->b()LX/650;

    move-result-object v0

    .line 1047325
    move-object v1, v0

    .line 1047326
    iget-object v0, v1, LX/650;->a:LX/64q;

    move-object v2, v0

    .line 1047327
    const/4 v0, 0x0

    .line 1047328
    :goto_0
    add-int/lit8 v0, v0, 0x1

    if-le v0, v5, :cond_0

    .line 1047329
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Too many tunnel connections attempted: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047330
    :cond_0
    invoke-static {p0, p1, p2}, LX/65S;->a(LX/65S;II)V

    .line 1047331
    invoke-direct {p0, p2, p3, v1, v2}, LX/65S;->a(IILX/650;LX/64q;)LX/650;

    move-result-object v1

    .line 1047332
    if-eqz v1, :cond_1

    .line 1047333
    iget-object v3, p0, LX/65S;->l:Ljava/net/Socket;

    invoke-static {v3}, LX/65A;->a(Ljava/net/Socket;)V

    .line 1047334
    iput-object v4, p0, LX/65S;->l:Ljava/net/Socket;

    .line 1047335
    iput-object v4, p0, LX/65S;->e:LX/670;

    .line 1047336
    iput-object v4, p0, LX/65S;->d:LX/671;

    goto :goto_0

    .line 1047337
    :cond_1
    invoke-static {p0, p2, p3, p4}, LX/65S;->a(LX/65S;IILX/65Q;)V

    .line 1047338
    return-void
.end method

.method private a(LX/65Q;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1047246
    iget-object v0, p0, LX/65S;->k:LX/657;

    .line 1047247
    iget-object v2, v0, LX/657;->a:LX/64R;

    move-object v2, v2

    .line 1047248
    iget-object v0, v2, LX/64R;->i:Ljavax/net/ssl/SSLSocketFactory;

    move-object v0, v0

    .line 1047249
    :try_start_0
    iget-object v3, p0, LX/65S;->l:Ljava/net/Socket;

    .line 1047250
    iget-object v4, v2, LX/64R;->a:LX/64q;

    move-object v4, v4

    .line 1047251
    iget-object v5, v4, LX/64q;->e:Ljava/lang/String;

    move-object v4, v5

    .line 1047252
    iget-object v5, v2, LX/64R;->a:LX/64q;

    move-object v5, v5

    .line 1047253
    iget v6, v5, LX/64q;->f:I

    move v5, v6

    .line 1047254
    const/4 v6, 0x1

    .line 1047255
    invoke-virtual {v0, v3, v4, v5, v6}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;
    :try_end_0
    .catch Ljava/lang/AssertionError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047256
    :try_start_1
    invoke-virtual {p1, v0}, LX/65Q;->a(Ljavax/net/ssl/SSLSocket;)LX/64e;

    move-result-object v3

    .line 1047257
    iget-boolean v4, v3, LX/64e;->f:Z

    move v4, v4

    .line 1047258
    if-eqz v4, :cond_0

    .line 1047259
    sget-object v4, LX/66Y;->a:LX/66Y;

    move-object v4, v4

    .line 1047260
    iget-object v5, v2, LX/64R;->a:LX/64q;

    move-object v5, v5

    .line 1047261
    iget-object v6, v5, LX/64q;->e:Ljava/lang/String;

    move-object v5, v6

    .line 1047262
    iget-object v6, v2, LX/64R;->e:Ljava/util/List;

    move-object v6, v6

    .line 1047263
    invoke-virtual {v4, v0, v5, v6}, LX/66Y;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V

    .line 1047264
    :cond_0
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    .line 1047265
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v4

    invoke-static {v4}, LX/64l;->a(Ljavax/net/ssl/SSLSession;)LX/64l;

    move-result-object v4

    .line 1047266
    iget-object v5, v2, LX/64R;->j:Ljavax/net/ssl/HostnameVerifier;

    move-object v5, v5

    .line 1047267
    iget-object v6, v2, LX/64R;->a:LX/64q;

    move-object v6, v6

    .line 1047268
    iget-object v7, v6, LX/64q;->e:Ljava/lang/String;

    move-object v6, v7

    .line 1047269
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1047270
    iget-object v3, v4, LX/64l;->c:Ljava/util/List;

    move-object v1, v3

    .line 1047271
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/security/cert/X509Certificate;

    .line 1047272
    new-instance v3, Ljavax/net/ssl/SSLPeerUnverifiedException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Hostname "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1047273
    iget-object v5, v2, LX/64R;->a:LX/64q;

    move-object v2, v5

    .line 1047274
    iget-object v5, v2, LX/64q;->e:Ljava/lang/String;

    move-object v2, v5

    .line 1047275
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " not verified:\n    certificate: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1047276
    invoke-static {v1}, LX/64a;->a(Ljava/security/cert/Certificate;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n    DN: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1047277
    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v4

    invoke-interface {v4}, Ljava/security/Principal;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n    subjectAltNames: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1047278
    const/4 v4, 0x7

    invoke-static {v1, v4}, LX/66g;->a(Ljava/security/cert/X509Certificate;I)Ljava/util/List;

    move-result-object v4

    .line 1047279
    const/4 v5, 0x2

    invoke-static {v1, v5}, LX/66g;->a(Ljava/security/cert/X509Certificate;I)Ljava/util/List;

    move-result-object v5

    .line 1047280
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 1047281
    invoke-interface {v6, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1047282
    invoke-interface {v6, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1047283
    move-object v1, v6

    .line 1047284
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Ljava/lang/AssertionError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1047285
    :catch_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    .line 1047286
    :goto_0
    :try_start_2
    invoke-static {v0}, LX/65A;->a(Ljava/lang/AssertionError;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1047287
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_1

    .line 1047288
    sget-object v2, LX/66Y;->a:LX/66Y;

    move-object v2, v2

    .line 1047289
    invoke-virtual {v2, v1}, LX/66Y;->b(Ljavax/net/ssl/SSLSocket;)V

    .line 1047290
    :cond_1
    invoke-static {v1}, LX/65A;->a(Ljava/net/Socket;)V

    throw v0

    .line 1047291
    :cond_2
    :try_start_3
    iget-object v5, v2, LX/64R;->k:LX/64a;

    move-object v5, v5

    .line 1047292
    iget-object v6, v2, LX/64R;->a:LX/64q;

    move-object v2, v6

    .line 1047293
    iget-object v6, v2, LX/64q;->e:Ljava/lang/String;

    move-object v2, v6

    .line 1047294
    iget-object v6, v4, LX/64l;->c:Ljava/util/List;

    move-object v6, v6

    .line 1047295
    invoke-virtual {v5, v2, v6}, LX/64a;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1047296
    iget-boolean v2, v3, LX/64e;->f:Z

    move v2, v2

    .line 1047297
    if-eqz v2, :cond_3

    .line 1047298
    sget-object v2, LX/66Y;->a:LX/66Y;

    move-object v1, v2

    .line 1047299
    invoke-virtual {v1, v0}, LX/66Y;->a(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;

    move-result-object v1

    .line 1047300
    :cond_3
    iput-object v0, p0, LX/65S;->a:Ljava/net/Socket;

    .line 1047301
    iget-object v2, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-static {v2}, LX/67B;->b(Ljava/net/Socket;)LX/65D;

    move-result-object v2

    invoke-static {v2}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v2

    iput-object v2, p0, LX/65S;->d:LX/671;

    .line 1047302
    iget-object v2, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-static {v2}, LX/67B;->a(Ljava/net/Socket;)LX/65J;

    move-result-object v2

    invoke-static {v2}, LX/67B;->a(LX/65J;)LX/670;

    move-result-object v2

    iput-object v2, p0, LX/65S;->e:LX/670;

    .line 1047303
    iput-object v4, p0, LX/65S;->m:LX/64l;

    .line 1047304
    if-eqz v1, :cond_5

    .line 1047305
    invoke-static {v1}, LX/64x;->get(Ljava/lang/String;)LX/64x;

    move-result-object v1

    :goto_2
    iput-object v1, p0, LX/65S;->n:LX/64x;
    :try_end_3
    .catch Ljava/lang/AssertionError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1047306
    if-eqz v0, :cond_4

    .line 1047307
    sget-object v1, LX/66Y;->a:LX/66Y;

    move-object v1, v1

    .line 1047308
    invoke-virtual {v1, v0}, LX/66Y;->b(Ljavax/net/ssl/SSLSocket;)V

    .line 1047309
    :cond_4
    return-void

    .line 1047310
    :cond_5
    :try_start_4
    sget-object v1, LX/64x;->HTTP_1_1:LX/64x;
    :try_end_4
    .catch Ljava/lang/AssertionError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 1047311
    :cond_6
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1047312
    :catchall_1
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_1

    .line 1047313
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public static a(LX/65S;II)V
    .locals 4

    .prologue
    .line 1047227
    iget-object v0, p0, LX/65S;->k:LX/657;

    .line 1047228
    iget-object v1, v0, LX/657;->b:Ljava/net/Proxy;

    move-object v1, v1

    .line 1047229
    iget-object v0, p0, LX/65S;->k:LX/657;

    .line 1047230
    iget-object v2, v0, LX/657;->a:LX/64R;

    move-object v0, v2

    .line 1047231
    invoke-virtual {v1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    sget-object v3, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    sget-object v3, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v2, v3, :cond_1

    .line 1047232
    :cond_0
    iget-object v1, v0, LX/64R;->c:Ljavax/net/SocketFactory;

    move-object v0, v1

    .line 1047233
    invoke-virtual {v0}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/65S;->l:Ljava/net/Socket;

    .line 1047234
    iget-object v0, p0, LX/65S;->l:Ljava/net/Socket;

    invoke-virtual {v0, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 1047235
    :try_start_0
    sget-object v0, LX/66Y;->a:LX/66Y;

    move-object v0, v0

    .line 1047236
    iget-object v1, p0, LX/65S;->l:Ljava/net/Socket;

    iget-object v2, p0, LX/65S;->k:LX/657;

    .line 1047237
    iget-object v3, v2, LX/657;->c:Ljava/net/InetSocketAddress;

    move-object v2, v3

    .line 1047238
    invoke-virtual {v0, v1, v2, p1}, LX/66Y;->a(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1047239
    iget-object v0, p0, LX/65S;->l:Ljava/net/Socket;

    invoke-static {v0}, LX/67B;->b(Ljava/net/Socket;)LX/65D;

    move-result-object v0

    invoke-static {v0}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v0

    iput-object v0, p0, LX/65S;->d:LX/671;

    .line 1047240
    iget-object v0, p0, LX/65S;->l:Ljava/net/Socket;

    invoke-static {v0}, LX/67B;->a(Ljava/net/Socket;)LX/65J;

    move-result-object v0

    invoke-static {v0}, LX/67B;->a(LX/65J;)LX/670;

    move-result-object v0

    iput-object v0, p0, LX/65S;->e:LX/670;

    .line 1047241
    return-void

    .line 1047242
    :cond_1
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0, v1}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    goto :goto_0

    .line 1047243
    :catch_0
    new-instance v0, Ljava/net/ConnectException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to connect to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/65S;->k:LX/657;

    .line 1047244
    iget-object v3, v2, LX/657;->c:Ljava/net/InetSocketAddress;

    move-object v2, v3

    .line 1047245
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/65S;IILX/65Q;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    .line 1047190
    iget-object v0, p0, LX/65S;->k:LX/657;

    .line 1047191
    iget-object v1, v0, LX/657;->a:LX/64R;

    move-object v0, v1

    .line 1047192
    iget-object v1, v0, LX/64R;->i:Ljavax/net/ssl/SSLSocketFactory;

    move-object v0, v1

    .line 1047193
    if-eqz v0, :cond_2

    .line 1047194
    invoke-direct {p0, p3}, LX/65S;->a(LX/65Q;)V

    .line 1047195
    :goto_0
    iget-object v0, p0, LX/65S;->n:LX/64x;

    sget-object v1, LX/64x;->SPDY_3:LX/64x;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/65S;->n:LX/64x;

    sget-object v1, LX/64x;->HTTP_2:LX/64x;

    if-ne v0, v1, :cond_3

    .line 1047196
    :cond_0
    iget-object v0, p0, LX/65S;->a:Ljava/net/Socket;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 1047197
    new-instance v0, LX/65a;

    invoke-direct {v0, v2}, LX/65a;-><init>(Z)V

    iget-object v1, p0, LX/65S;->a:Ljava/net/Socket;

    iget-object v2, p0, LX/65S;->k:LX/657;

    .line 1047198
    iget-object v3, v2, LX/657;->a:LX/64R;

    move-object v2, v3

    .line 1047199
    iget-object v3, v2, LX/64R;->a:LX/64q;

    move-object v2, v3

    .line 1047200
    iget-object v3, v2, LX/64q;->e:Ljava/lang/String;

    move-object v2, v3

    .line 1047201
    iget-object v3, p0, LX/65S;->d:LX/671;

    iget-object v4, p0, LX/65S;->e:LX/670;

    .line 1047202
    iput-object v1, v0, LX/65a;->a:Ljava/net/Socket;

    .line 1047203
    iput-object v2, v0, LX/65a;->b:Ljava/lang/String;

    .line 1047204
    iput-object v3, v0, LX/65a;->c:LX/671;

    .line 1047205
    iput-object v4, v0, LX/65a;->d:LX/670;

    .line 1047206
    move-object v0, v0

    .line 1047207
    iget-object v1, p0, LX/65S;->n:LX/64x;

    .line 1047208
    iput-object v1, v0, LX/65a;->f:LX/64x;

    .line 1047209
    move-object v0, v0

    .line 1047210
    iput-object p0, v0, LX/65a;->e:LX/65R;

    .line 1047211
    move-object v0, v0

    .line 1047212
    new-instance v1, LX/65c;

    invoke-direct {v1, v0}, LX/65c;-><init>(LX/65a;)V

    move-object v0, v1

    .line 1047213
    const/4 v5, 0x1

    const/high16 v9, 0x10000

    .line 1047214
    if-eqz v5, :cond_1

    .line 1047215
    iget-object v6, v0, LX/65c;->i:LX/65Z;

    invoke-interface {v6}, LX/65Z;->a()V

    .line 1047216
    iget-object v6, v0, LX/65c;->i:LX/65Z;

    iget-object v7, v0, LX/65c;->e:LX/663;

    invoke-interface {v6, v7}, LX/65Z;->b(LX/663;)V

    .line 1047217
    iget-object v6, v0, LX/65c;->e:LX/663;

    invoke-virtual {v6, v9}, LX/663;->f(I)I

    move-result v6

    .line 1047218
    if-eq v6, v9, :cond_1

    .line 1047219
    iget-object v7, v0, LX/65c;->i:LX/65Z;

    const/4 v8, 0x0

    sub-int/2addr v6, v9

    int-to-long v10, v6

    invoke-interface {v7, v8, v10, v11}, LX/65Z;->a(IJ)V

    .line 1047220
    :cond_1
    iget-object v6, v0, LX/65c;->j:Lokhttp3/internal/framed/FramedConnection$Reader;

    const v7, -0x4b083243

    invoke-static {v6, v7}, LX/00l;->a(Ljava/lang/Runnable;I)Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 1047221
    invoke-virtual {v0}, LX/65c;->b()I

    move-result v1

    iput v1, p0, LX/65S;->f:I

    .line 1047222
    iput-object v0, p0, LX/65S;->b:LX/65c;

    .line 1047223
    :goto_1
    return-void

    .line 1047224
    :cond_2
    sget-object v0, LX/64x;->HTTP_1_1:LX/64x;

    iput-object v0, p0, LX/65S;->n:LX/64x;

    .line 1047225
    iget-object v0, p0, LX/65S;->l:Ljava/net/Socket;

    iput-object v0, p0, LX/65S;->a:Ljava/net/Socket;

    goto :goto_0

    .line 1047226
    :cond_3
    iput v2, p0, LX/65S;->f:I

    goto :goto_1
.end method


# virtual methods
.method public final a(IIILjava/util/List;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List",
            "<",
            "LX/64e;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1047138
    iget-object v0, p0, LX/65S;->n:LX/64x;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047139
    :cond_0
    new-instance v3, LX/65Q;

    invoke-direct {v3, p4}, LX/65Q;-><init>(Ljava/util/List;)V

    .line 1047140
    iget-object v0, p0, LX/65S;->k:LX/657;

    .line 1047141
    iget-object v2, v0, LX/657;->a:LX/64R;

    move-object v0, v2

    .line 1047142
    iget-object v2, v0, LX/64R;->i:Ljavax/net/ssl/SSLSocketFactory;

    move-object v0, v2

    .line 1047143
    if-nez v0, :cond_9

    .line 1047144
    sget-object v0, LX/64e;->c:LX/64e;

    invoke-interface {p4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1047145
    new-instance v0, LX/65U;

    new-instance v1, Ljava/net/UnknownServiceException;

    const-string v2, "CLEARTEXT communication not enabled for client"

    invoke-direct {v1, v2}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, LX/65U;-><init>(Ljava/io/IOException;)V

    throw v0

    .line 1047146
    :cond_1
    iget-object v0, p0, LX/65S;->k:LX/657;

    .line 1047147
    iget-object v2, v0, LX/657;->a:LX/64R;

    move-object v0, v2

    .line 1047148
    iget-object v2, v0, LX/64R;->a:LX/64q;

    move-object v0, v2

    .line 1047149
    iget-object v2, v0, LX/64q;->e:Ljava/lang/String;

    move-object v0, v2

    .line 1047150
    sget-object v2, LX/66Y;->a:LX/66Y;

    move-object v2, v2

    .line 1047151
    invoke-virtual {v2, v0}, LX/66Y;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1047152
    new-instance v1, LX/65U;

    new-instance v2, Ljava/net/UnknownServiceException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CLEARTEXT communication to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " not permitted by network security policy"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, LX/65U;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 1047153
    :cond_2
    :goto_0
    iget-object v2, p0, LX/65S;->n:LX/64x;

    if-nez v2, :cond_8

    .line 1047154
    :try_start_0
    iget-object v2, p0, LX/65S;->k:LX/657;

    .line 1047155
    iget-object v4, v2, LX/657;->a:LX/64R;

    iget-object v4, v4, LX/64R;->i:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v4, :cond_a

    iget-object v4, v2, LX/657;->b:Ljava/net/Proxy;

    invoke-virtual {v4}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v4

    sget-object p4, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v4, p4, :cond_a

    const/4 v4, 0x1

    :goto_1
    move v2, v4

    .line 1047156
    if-eqz v2, :cond_5

    .line 1047157
    invoke-direct {p0, p1, p2, p3, v3}, LX/65S;->a(IIILX/65Q;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1047158
    :catch_0
    move-exception v2

    .line 1047159
    iget-object v4, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-static {v4}, LX/65A;->a(Ljava/net/Socket;)V

    .line 1047160
    iget-object v4, p0, LX/65S;->l:Ljava/net/Socket;

    invoke-static {v4}, LX/65A;->a(Ljava/net/Socket;)V

    .line 1047161
    iput-object v1, p0, LX/65S;->a:Ljava/net/Socket;

    .line 1047162
    iput-object v1, p0, LX/65S;->l:Ljava/net/Socket;

    .line 1047163
    iput-object v1, p0, LX/65S;->d:LX/671;

    .line 1047164
    iput-object v1, p0, LX/65S;->e:LX/670;

    .line 1047165
    iput-object v1, p0, LX/65S;->m:LX/64l;

    .line 1047166
    iput-object v1, p0, LX/65S;->n:LX/64x;

    .line 1047167
    if-nez v0, :cond_6

    .line 1047168
    new-instance v0, LX/65U;

    invoke-direct {v0, v2}, LX/65U;-><init>(Ljava/io/IOException;)V

    .line 1047169
    :goto_2
    if-eqz p5, :cond_4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1047170
    iput-boolean v5, v3, LX/65Q;->d:Z

    .line 1047171
    iget-boolean p4, v3, LX/65Q;->c:Z

    if-nez p4, :cond_b

    .line 1047172
    :cond_3
    :goto_3
    move v2, v4

    .line 1047173
    if-nez v2, :cond_2

    .line 1047174
    :cond_4
    throw v0

    .line 1047175
    :cond_5
    :try_start_1
    invoke-static {p0, p1, p2}, LX/65S;->a(LX/65S;II)V

    .line 1047176
    invoke-static {p0, p2, p3, v3}, LX/65S;->a(LX/65S;IILX/65Q;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1047177
    goto :goto_0

    .line 1047178
    :cond_6
    iget-object v4, v0, LX/65U;->lastException:Ljava/io/IOException;

    .line 1047179
    sget-object v5, LX/65U;->a:Ljava/lang/reflect/Method;

    if-eqz v5, :cond_7

    .line 1047180
    :try_start_2
    sget-object v5, LX/65U;->a:Ljava/lang/reflect/Method;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p4, 0x0

    aput-object v4, v6, p4

    invoke-virtual {v5, v2, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1047181
    :cond_7
    :goto_4
    iput-object v2, v0, LX/65U;->lastException:Ljava/io/IOException;

    .line 1047182
    goto :goto_2

    .line 1047183
    :cond_8
    return-void

    :cond_9
    move-object v0, v1

    goto :goto_0

    :cond_a
    :try_start_3
    const/4 v4, 0x0

    goto :goto_1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1047184
    :cond_b
    instance-of p4, v2, Ljava/net/ProtocolException;

    if-nez p4, :cond_3

    .line 1047185
    instance-of p4, v2, Ljava/io/InterruptedIOException;

    if-nez p4, :cond_3

    .line 1047186
    instance-of p4, v2, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz p4, :cond_c

    .line 1047187
    invoke-virtual {v2}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object p4

    instance-of p4, p4, Ljava/security/cert/CertificateException;

    if-nez p4, :cond_3

    .line 1047188
    :cond_c
    instance-of p4, v2, Ljavax/net/ssl/SSLPeerUnverifiedException;

    if-nez p4, :cond_3

    .line 1047189
    instance-of p4, v2, Ljavax/net/ssl/SSLHandshakeException;

    if-nez p4, :cond_d

    instance-of p4, v2, Ljavax/net/ssl/SSLProtocolException;

    if-eqz p4, :cond_3

    :cond_d
    move v4, v5

    goto :goto_3

    :catch_1
    goto :goto_4

    :catch_2
    goto :goto_4
.end method

.method public final a(LX/65c;)V
    .locals 1

    .prologue
    .line 1047136
    invoke-virtual {p1}, LX/65c;->b()I

    move-result v0

    iput v0, p0, LX/65S;->f:I

    .line 1047137
    return-void
.end method

.method public final a(LX/65i;)V
    .locals 1

    .prologue
    .line 1047134
    sget-object v0, LX/65X;->REFUSED_STREAM:LX/65X;

    invoke-virtual {p1, v0}, LX/65i;->a(LX/65X;)V

    .line 1047135
    return-void
.end method

.method public final a(Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1047121
    iget-object v2, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 1047122
    :cond_1
    :goto_0
    return v0

    .line 1047123
    :cond_2
    iget-object v2, p0, LX/65S;->b:LX/65c;

    if-nez v2, :cond_1

    .line 1047124
    if-eqz p1, :cond_1

    .line 1047125
    :try_start_0
    iget-object v2, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getSoTimeout()I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 1047126
    :try_start_1
    iget-object v2, p0, LX/65S;->a:Ljava/net/Socket;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 1047127
    iget-object v2, p0, LX/65S;->d:LX/671;

    invoke-interface {v2}, LX/671;->e()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    .line 1047128
    :try_start_2
    iget-object v2, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    move v0, v1

    .line 1047129
    goto :goto_0

    .line 1047130
    :cond_3
    iget-object v2, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    goto :goto_0

    .line 1047131
    :catch_0
    goto :goto_0

    .line 1047132
    :catchall_0
    move-exception v2

    iget-object v4, p0, LX/65S;->a:Ljava/net/Socket;

    invoke-virtual {v4, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v2
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1047133
    :catch_1
    move v0, v1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1047119
    iget-object v0, p0, LX/65S;->l:Ljava/net/Socket;

    invoke-static {v0}, LX/65A;->a(Ljava/net/Socket;)V

    .line 1047120
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1047102
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Connection{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/65S;->k:LX/657;

    .line 1047103
    iget-object v2, v1, LX/657;->a:LX/64R;

    move-object v1, v2

    .line 1047104
    iget-object v2, v1, LX/64R;->a:LX/64q;

    move-object v1, v2

    .line 1047105
    iget-object v2, v1, LX/64q;->e:Ljava/lang/String;

    move-object v1, v2

    .line 1047106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/65S;->k:LX/657;

    .line 1047107
    iget-object v2, v1, LX/657;->a:LX/64R;

    move-object v1, v2

    .line 1047108
    iget-object v2, v1, LX/64R;->a:LX/64q;

    move-object v1, v2

    .line 1047109
    iget v2, v1, LX/64q;->f:I

    move v1, v2

    .line 1047110
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", proxy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/65S;->k:LX/657;

    .line 1047111
    iget-object v2, v1, LX/657;->b:Ljava/net/Proxy;

    move-object v1, v2

    .line 1047112
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hostAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/65S;->k:LX/657;

    .line 1047113
    iget-object v2, v1, LX/657;->c:Ljava/net/InetSocketAddress;

    move-object v1, v2

    .line 1047114
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cipherSuite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/65S;->m:LX/64l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/65S;->m:LX/64l;

    .line 1047115
    iget-object v2, v0, LX/64l;->b:LX/64b;

    move-object v0, v2

    .line 1047116
    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " protocol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/65S;->n:LX/64x;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1047117
    return-object v0

    .line 1047118
    :cond_0
    const-string v0, "none"

    goto :goto_0
.end method
