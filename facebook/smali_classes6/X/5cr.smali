.class public final LX/5cr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 964025
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 964026
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 964027
    :goto_0
    return v1

    .line 964028
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 964029
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 964030
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 964031
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 964032
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 964033
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 964034
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 964035
    :cond_3
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 964036
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 964037
    :cond_4
    const-string v6, "mutual_friends"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 964038
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 964039
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_c

    .line 964040
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 964041
    :goto_2
    move v2, v5

    .line 964042
    goto :goto_1

    .line 964043
    :cond_5
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 964044
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 964045
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 964046
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 964047
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 964048
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 964049
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 964050
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 964051
    :cond_8
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 964052
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 964053
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 964054
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_8

    if-eqz v8, :cond_8

    .line 964055
    const-string v9, "count"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 964056
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v7, v2

    move v2, v6

    goto :goto_3

    .line 964057
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 964058
    :cond_a
    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 964059
    if-eqz v2, :cond_b

    .line 964060
    invoke-virtual {p1, v5, v7, v5}, LX/186;->a(III)V

    .line 964061
    :cond_b
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_c
    move v2, v5

    move v7, v5

    goto :goto_3
.end method
