.class public LX/5qX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/Choreographer$FrameCallback;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final a:Landroid/view/Choreographer;

.field private final b:LX/5pX;

.field private final c:LX/5rQ;

.field private final d:LX/5qV;

.field private e:Z

.field private f:J

.field private g:J

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Z

.field private m:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "LX/5qW;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/Choreographer;LX/5pX;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 1009409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1009410
    iput-boolean v0, p0, LX/5qX;->e:Z

    .line 1009411
    iput-wide v2, p0, LX/5qX;->f:J

    .line 1009412
    iput-wide v2, p0, LX/5qX;->g:J

    .line 1009413
    iput v0, p0, LX/5qX;->h:I

    .line 1009414
    iput v0, p0, LX/5qX;->i:I

    .line 1009415
    iput v0, p0, LX/5qX;->j:I

    .line 1009416
    iput v0, p0, LX/5qX;->k:I

    .line 1009417
    iput-boolean v0, p0, LX/5qX;->l:Z

    .line 1009418
    iput-object p1, p0, LX/5qX;->a:Landroid/view/Choreographer;

    .line 1009419
    iput-object p2, p0, LX/5qX;->b:LX/5pX;

    .line 1009420
    const-class v0, LX/5rQ;

    invoke-virtual {p2, v0}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    iput-object v0, p0, LX/5qX;->c:LX/5rQ;

    .line 1009421
    new-instance v0, LX/5qV;

    invoke-direct {v0}, LX/5qV;-><init>()V

    iput-object v0, p0, LX/5qX;->d:LX/5qV;

    .line 1009422
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1009404
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5qX;->e:Z

    .line 1009405
    iget-object v0, p0, LX/5qX;->b:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->a()Lcom/facebook/react/bridge/CatalystInstance;

    move-result-object v0

    iget-object v1, p0, LX/5qX;->d:LX/5qV;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/CatalystInstance;->a(LX/5pU;)V

    .line 1009406
    iget-object v0, p0, LX/5qX;->c:LX/5rQ;

    iget-object v1, p0, LX/5qX;->d:LX/5qV;

    invoke-virtual {v0, v1}, LX/5rQ;->a(LX/5qU;)V

    .line 1009407
    iget-object v0, p0, LX/5qX;->a:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 1009408
    return-void
.end method

.method private d()D
    .locals 6

    .prologue
    .line 1009401
    iget-wide v0, p0, LX/5qX;->g:J

    iget-wide v2, p0, LX/5qX;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1009402
    const-wide/16 v0, 0x0

    .line 1009403
    :goto_0
    return-wide v0

    :cond_0
    invoke-direct {p0}, LX/5qX;->f()I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x41cdcd6500000000L    # 1.0E9

    mul-double/2addr v0, v2

    iget-wide v2, p0, LX/5qX;->g:J

    iget-wide v4, p0, LX/5qX;->f:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method private e()D
    .locals 6

    .prologue
    .line 1009398
    iget-wide v0, p0, LX/5qX;->g:J

    iget-wide v2, p0, LX/5qX;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1009399
    const-wide/16 v0, 0x0

    .line 1009400
    :goto_0
    return-wide v0

    :cond_0
    invoke-direct {p0}, LX/5qX;->g()I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x41cdcd6500000000L    # 1.0E9

    mul-double/2addr v0, v2

    iget-wide v2, p0, LX/5qX;->g:J

    iget-wide v4, p0, LX/5qX;->f:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method private f()I
    .locals 1

    .prologue
    .line 1009397
    iget v0, p0, LX/5qX;->h:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private g()I
    .locals 1

    .prologue
    .line 1009360
    iget v0, p0, LX/5qX;->k:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private h()I
    .locals 4

    .prologue
    .line 1009394
    invoke-direct {p0}, LX/5qX;->i()I

    move-result v0

    int-to-double v0, v0

    .line 1009395
    const-wide v2, 0x4030e66666666666L    # 16.9

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-int v0, v0

    .line 1009396
    return v0
.end method

.method private i()I
    .locals 4

    .prologue
    .line 1009393
    iget-wide v0, p0, LX/5qX;->g:J

    long-to-double v0, v0

    iget-wide v2, p0, LX/5qX;->f:J

    long-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-int v0, v0

    const v1, 0xf4240

    div-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(J)LX/5qW;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1009388
    iget-object v0, p0, LX/5qX;->m:Ljava/util/TreeMap;

    const-string v1, "FPS was not recorded at each frame!"

    invoke-static {v0, v1}, LX/0nE;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 1009389
    iget-object v0, p0, LX/5qX;->m:Ljava/util/TreeMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    .line 1009390
    if-nez v0, :cond_0

    .line 1009391
    const/4 v0, 0x0

    .line 1009392
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5qW;

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1009384
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LX/5qX;->m:Ljava/util/TreeMap;

    .line 1009385
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5qX;->l:Z

    .line 1009386
    invoke-direct {p0}, LX/5qX;->c()V

    .line 1009387
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1009380
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5qX;->e:Z

    .line 1009381
    iget-object v0, p0, LX/5qX;->b:LX/5pX;

    invoke-virtual {v0}, LX/5pX;->a()Lcom/facebook/react/bridge/CatalystInstance;

    move-result-object v0

    iget-object v1, p0, LX/5qX;->d:LX/5qV;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/CatalystInstance;->b(LX/5pU;)V

    .line 1009382
    iget-object v0, p0, LX/5qX;->c:LX/5rQ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/5rQ;->a(LX/5qU;)V

    .line 1009383
    return-void
.end method

.method public final doFrame(J)V
    .locals 11

    .prologue
    .line 1009361
    iget-boolean v0, p0, LX/5qX;->e:Z

    if-eqz v0, :cond_0

    .line 1009362
    :goto_0
    return-void

    .line 1009363
    :cond_0
    iget-wide v0, p0, LX/5qX;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 1009364
    iput-wide p1, p0, LX/5qX;->f:J

    .line 1009365
    :cond_1
    iget-wide v0, p0, LX/5qX;->g:J

    .line 1009366
    iput-wide p1, p0, LX/5qX;->g:J

    .line 1009367
    iget-object v2, p0, LX/5qX;->d:LX/5qV;

    invoke-virtual {v2, v0, v1, p1, p2}, LX/5qV;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1009368
    iget v0, p0, LX/5qX;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/5qX;->k:I

    .line 1009369
    :cond_2
    iget v0, p0, LX/5qX;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/5qX;->h:I

    .line 1009370
    invoke-direct {p0}, LX/5qX;->h()I

    move-result v4

    .line 1009371
    iget v0, p0, LX/5qX;->i:I

    sub-int v0, v4, v0

    add-int/lit8 v0, v0, -0x1

    .line 1009372
    const/4 v1, 0x4

    if-lt v0, v1, :cond_3

    .line 1009373
    iget v0, p0, LX/5qX;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/5qX;->j:I

    .line 1009374
    :cond_3
    iget-boolean v0, p0, LX/5qX;->l:Z

    if-eqz v0, :cond_4

    .line 1009375
    iget-object v0, p0, LX/5qX;->m:Ljava/util/TreeMap;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1009376
    new-instance v1, LX/5qW;

    invoke-direct {p0}, LX/5qX;->f()I

    move-result v2

    invoke-direct {p0}, LX/5qX;->g()I

    move-result v3

    iget v5, p0, LX/5qX;->j:I

    invoke-direct {p0}, LX/5qX;->d()D

    move-result-wide v6

    invoke-direct {p0}, LX/5qX;->e()D

    move-result-wide v8

    invoke-direct {p0}, LX/5qX;->i()I

    move-result v10

    invoke-direct/range {v1 .. v10}, LX/5qW;-><init>(IIIIDDI)V

    .line 1009377
    iget-object v0, p0, LX/5qX;->m:Ljava/util/TreeMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1009378
    :cond_4
    iput v4, p0, LX/5qX;->i:I

    .line 1009379
    iget-object v0, p0, LX/5qX;->a:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto :goto_0
.end method
