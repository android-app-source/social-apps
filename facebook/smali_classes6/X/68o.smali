.class public LX/68o;
.super LX/67m;
.source ""


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field private D:Z

.field private final o:F

.field private final p:Landroid/graphics/Paint;

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 4

    .prologue
    const/high16 v3, 0x40400000    # 3.0f

    const/high16 v2, 0x40000000    # 2.0f

    .line 1056519
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1056520
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/68o;->p:Landroid/graphics/Paint;

    .line 1056521
    const/high16 v0, 0x41400000    # 12.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68o;->q:F

    .line 1056522
    const/high16 v0, 0x42140000    # 37.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68o;->r:F

    .line 1056523
    const/high16 v0, 0x3f000000    # 0.5f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68o;->s:F

    .line 1056524
    iget v0, p0, LX/67m;->d:F

    mul-float/2addr v0, v2

    iput v0, p0, LX/68o;->t:F

    .line 1056525
    const/high16 v0, 0x40a00000    # 5.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68o;->u:F

    .line 1056526
    const/high16 v0, 0x41000000    # 8.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68o;->v:F

    .line 1056527
    iget v0, p0, LX/67m;->d:F

    mul-float/2addr v0, v3

    iput v0, p0, LX/68o;->x:F

    .line 1056528
    const/high16 v0, 0x42400000    # 48.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, LX/68o;->o:F

    .line 1056529
    const/4 v0, 0x3

    iput v0, p0, LX/68o;->j:I

    .line 1056530
    iput v3, p0, LX/68o;->k:F

    .line 1056531
    iget v0, p0, LX/68o;->o:F

    iget v1, p0, LX/68o;->r:F

    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p0, LX/68o;->C:F

    .line 1056532
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68o;->l:Z

    .line 1056533
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1056471
    iget v2, p0, LX/68o;->y:F

    iget v3, p0, LX/68o;->r:F

    sub-float/2addr v2, v3

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_0

    iget v2, p0, LX/68o;->y:F

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_0

    iget v2, p0, LX/68o;->z:F

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_0

    iget v2, p0, LX/68o;->z:F

    iget v3, p0, LX/68o;->r:F

    add-float/2addr v2, v3

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    .line 1056472
    iput-boolean v0, p0, LX/68o;->D:Z

    .line 1056473
    const/4 v0, 0x2

    .line 1056474
    :goto_0
    return v0

    .line 1056475
    :cond_0
    iget v2, p0, LX/68o;->y:F

    iget v3, p0, LX/68o;->r:F

    sub-float/2addr v2, v3

    iget v3, p0, LX/68o;->C:F

    sub-float/2addr v2, v3

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_1

    iget v2, p0, LX/68o;->y:F

    iget v3, p0, LX/68o;->C:F

    add-float/2addr v2, v3

    cmpg-float v2, p1, v2

    if-gtz v2, :cond_1

    iget v2, p0, LX/68o;->z:F

    iget v3, p0, LX/68o;->C:F

    sub-float/2addr v2, v3

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_1

    iget v2, p0, LX/68o;->z:F

    iget v3, p0, LX/68o;->r:F

    add-float/2addr v2, v3

    iget v3, p0, LX/68o;->C:F

    add-float/2addr v2, v3

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_1

    .line 1056476
    iput-boolean v0, p0, LX/68o;->D:Z

    goto :goto_0

    .line 1056477
    :cond_1
    iput-boolean v1, p0, LX/68o;->D:Z

    move v0, v1

    .line 1056478
    goto :goto_0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1056494
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1056495
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 1056496
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/68o;->q:F

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->e:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/68o;->y:F

    .line 1056497
    iget v0, p0, LX/68o;->q:F

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->d:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, LX/68o;->z:F

    .line 1056498
    iget v0, p0, LX/68o;->y:F

    iget v1, p0, LX/68o;->r:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, LX/68o;->A:F

    .line 1056499
    iget v0, p0, LX/68o;->z:F

    iget v1, p0, LX/68o;->r:F

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, LX/68o;->B:F

    .line 1056500
    iget v0, p0, LX/68o;->v:F

    iget v1, p0, LX/68o;->x:F

    add-float/2addr v0, v1

    iput v0, p0, LX/68o;->w:F

    .line 1056501
    iget-object v0, p0, LX/68o;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1056502
    iget-object v1, p0, LX/68o;->p:Landroid/graphics/Paint;

    iget-boolean v0, p0, LX/68o;->D:Z

    if-eqz v0, :cond_0

    const v0, -0x222223

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1056503
    iget-object v0, p0, LX/68o;->p:Landroid/graphics/Paint;

    const/16 v1, 0xe6

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1056504
    iget v0, p0, LX/68o;->y:F

    iget v1, p0, LX/68o;->r:F

    sub-float v1, v0, v1

    iget v2, p0, LX/68o;->z:F

    iget v3, p0, LX/68o;->y:F

    iget v0, p0, LX/68o;->z:F

    iget v4, p0, LX/68o;->r:F

    add-float/2addr v4, v0

    iget-object v5, p0, LX/68o;->p:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1056505
    iget-object v0, p0, LX/68o;->p:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1056506
    iget v0, p0, LX/68o;->A:F

    iget v1, p0, LX/68o;->B:F

    iget v2, p0, LX/68o;->u:F

    iget-object v3, p0, LX/68o;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1056507
    iget-object v0, p0, LX/68o;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1056508
    iget-object v0, p0, LX/68o;->p:Landroid/graphics/Paint;

    iget v1, p0, LX/68o;->t:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1056509
    iget v0, p0, LX/68o;->A:F

    iget v1, p0, LX/68o;->B:F

    iget v2, p0, LX/68o;->v:F

    iget-object v3, p0, LX/68o;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1056510
    iget v1, p0, LX/68o;->A:F

    iget v0, p0, LX/68o;->B:F

    iget v2, p0, LX/68o;->v:F

    sub-float v2, v0, v2

    iget v3, p0, LX/68o;->A:F

    iget v0, p0, LX/68o;->B:F

    iget v4, p0, LX/68o;->w:F

    sub-float v4, v0, v4

    iget-object v5, p0, LX/68o;->p:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1056511
    iget v1, p0, LX/68o;->A:F

    iget v0, p0, LX/68o;->B:F

    iget v2, p0, LX/68o;->v:F

    add-float/2addr v2, v0

    iget v3, p0, LX/68o;->A:F

    iget v0, p0, LX/68o;->B:F

    iget v4, p0, LX/68o;->w:F

    add-float/2addr v4, v0

    iget-object v5, p0, LX/68o;->p:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1056512
    iget v0, p0, LX/68o;->A:F

    iget v1, p0, LX/68o;->v:F

    sub-float v1, v0, v1

    iget v2, p0, LX/68o;->B:F

    iget v0, p0, LX/68o;->A:F

    iget v3, p0, LX/68o;->w:F

    sub-float v3, v0, v3

    iget v4, p0, LX/68o;->B:F

    iget-object v5, p0, LX/68o;->p:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1056513
    iget v0, p0, LX/68o;->A:F

    iget v1, p0, LX/68o;->v:F

    add-float/2addr v1, v0

    iget v2, p0, LX/68o;->B:F

    iget v0, p0, LX/68o;->A:F

    iget v3, p0, LX/68o;->w:F

    add-float/2addr v3, v0

    iget v4, p0, LX/68o;->B:F

    iget-object v5, p0, LX/68o;->p:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1056514
    iget-object v0, p0, LX/68o;->p:Landroid/graphics/Paint;

    iget v1, p0, LX/68o;->s:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1056515
    iget-object v0, p0, LX/68o;->p:Landroid/graphics/Paint;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1056516
    iget v0, p0, LX/68o;->y:F

    iget v1, p0, LX/68o;->r:F

    sub-float v1, v0, v1

    iget v2, p0, LX/68o;->z:F

    iget v3, p0, LX/68o;->y:F

    iget v0, p0, LX/68o;->z:F

    iget v4, p0, LX/68o;->r:F

    add-float/2addr v4, v0

    iget-object v5, p0, LX/68o;->p:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1056517
    return-void

    .line 1056518
    :cond_0
    const/4 v0, -0x1

    goto/16 :goto_0
.end method

.method public final b(FF)Z
    .locals 8

    .prologue
    .line 1056489
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {v0}, LX/680;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1056490
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {v0}, LX/680;->e()Landroid/location/Location;

    move-result-object v0

    .line 1056491
    if-eqz v0, :cond_0

    .line 1056492
    iget-object v1, p0, LX/67m;->e:LX/680;

    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    const/high16 v0, 0x41700000    # 15.0f

    invoke-static {v2, v0}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/67d;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/680;->a(LX/67d;)V

    .line 1056493
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final d(FF)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1056484
    iget-boolean v1, p0, LX/68o;->D:Z

    if-eqz v1, :cond_1

    iget v1, p0, LX/68o;->y:F

    iget v2, p0, LX/68o;->r:F

    sub-float/2addr v1, v2

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_0

    iget v1, p0, LX/68o;->y:F

    cmpl-float v1, p1, v1

    if-gtz v1, :cond_0

    iget v1, p0, LX/68o;->z:F

    cmpg-float v1, p2, v1

    if-ltz v1, :cond_0

    iget v1, p0, LX/68o;->z:F

    iget v2, p0, LX/68o;->r:F

    add-float/2addr v1, v2

    cmpl-float v1, p2, v1

    if-lez v1, :cond_1

    .line 1056485
    :cond_0
    iput-boolean v0, p0, LX/68o;->D:Z

    .line 1056486
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1056487
    const/4 v0, 0x1

    .line 1056488
    :cond_1
    return v0
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 1056482
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1056483
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 1056479
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68o;->D:Z

    .line 1056480
    invoke-virtual {p0}, LX/67m;->f()V

    .line 1056481
    return-void
.end method
