.class public LX/55T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 829348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 829349
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 829350
    check-cast p1, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;

    .line 829351
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 829352
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "hideable_token"

    iget-object v2, p1, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829353
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "story_graphql_token"

    iget-object v2, p1, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829354
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "action"

    iget-object v2, p1, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->g:Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;->b()Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829355
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "story_location"

    iget-object v2, p1, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->h:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829356
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "undo"

    iget-boolean v2, p1, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->i:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829357
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "tracking"

    iget-object v2, p1, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829358
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "action_path"

    iget-object v2, p1, Lcom/facebook/api/feed/NegativeFeedbackActionOnFeedMethod$Params;->j:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829359
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829360
    new-instance v0, LX/14N;

    const-string v1, "negativeFeedbackActionOnFeedStory"

    const-string v2, "POST"

    const-string v3, "multifeed_afro_actions"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 829361
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 829362
    const-string v1, "action_path"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
