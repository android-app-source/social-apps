.class public final LX/5Ln;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 29

    .prologue
    .line 902847
    const/16 v25, 0x0

    .line 902848
    const/16 v24, 0x0

    .line 902849
    const/16 v23, 0x0

    .line 902850
    const/16 v22, 0x0

    .line 902851
    const/16 v21, 0x0

    .line 902852
    const/16 v20, 0x0

    .line 902853
    const/16 v19, 0x0

    .line 902854
    const/16 v18, 0x0

    .line 902855
    const/16 v17, 0x0

    .line 902856
    const/16 v16, 0x0

    .line 902857
    const/4 v15, 0x0

    .line 902858
    const/4 v14, 0x0

    .line 902859
    const/4 v13, 0x0

    .line 902860
    const/4 v12, 0x0

    .line 902861
    const/4 v11, 0x0

    .line 902862
    const/4 v10, 0x0

    .line 902863
    const/4 v9, 0x0

    .line 902864
    const/4 v8, 0x0

    .line 902865
    const/4 v7, 0x0

    .line 902866
    const/4 v6, 0x0

    .line 902867
    const/4 v5, 0x0

    .line 902868
    const/4 v4, 0x0

    .line 902869
    const/4 v3, 0x0

    .line 902870
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    .line 902871
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 902872
    const/4 v3, 0x0

    .line 902873
    :goto_0
    return v3

    .line 902874
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 902875
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_13

    .line 902876
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v26

    .line 902877
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 902878
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    if-eqz v26, :cond_1

    .line 902879
    const-string v27, "all_icons"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 902880
    invoke-static/range {p0 .. p1}, LX/5Lm;->a(LX/15w;LX/186;)I

    move-result v25

    goto :goto_1

    .line 902881
    :cond_2
    const-string v27, "glyph"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 902882
    invoke-static/range {p0 .. p1}, LX/5Lj;->a(LX/15w;LX/186;)I

    move-result v24

    goto :goto_1

    .line 902883
    :cond_3
    const-string v27, "iconImageLarge"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 902884
    invoke-static/range {p0 .. p1}, LX/5Lk;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 902885
    :cond_4
    const-string v27, "id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 902886
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto :goto_1

    .line 902887
    :cond_5
    const-string v27, "is_linking_verb"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 902888
    const/4 v7, 0x1

    .line 902889
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto :goto_1

    .line 902890
    :cond_6
    const-string v27, "legacy_api_id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 902891
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto :goto_1

    .line 902892
    :cond_7
    const-string v27, "prefetch_priority"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 902893
    const/4 v6, 0x1

    .line 902894
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto/16 :goto_1

    .line 902895
    :cond_8
    const-string v27, "present_participle"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 902896
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 902897
    :cond_9
    const-string v27, "previewTemplateAtPlace"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 902898
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 902899
    :cond_a
    const-string v27, "previewTemplateNoTags"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 902900
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 902901
    :cond_b
    const-string v27, "previewTemplateWithPeople"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 902902
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 902903
    :cond_c
    const-string v27, "previewTemplateWithPeopleAtPlace"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 902904
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 902905
    :cond_d
    const-string v27, "previewTemplateWithPerson"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 902906
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 902907
    :cond_e
    const-string v27, "previewTemplateWithPersonAtPlace"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 902908
    invoke-static/range {p0 .. p1}, LX/5Li;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 902909
    :cond_f
    const-string v27, "prompt"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_10

    .line 902910
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 902911
    :cond_10
    const-string v27, "supports_audio_suggestions"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_11

    .line 902912
    const/4 v5, 0x1

    .line 902913
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 902914
    :cond_11
    const-string v27, "supports_freeform"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_12

    .line 902915
    const/4 v4, 0x1

    .line 902916
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 902917
    :cond_12
    const-string v27, "supports_offline_posting"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 902918
    const/4 v3, 0x1

    .line 902919
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 902920
    :cond_13
    const/16 v26, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 902921
    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 902922
    const/16 v25, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 902923
    const/16 v24, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 902924
    const/16 v23, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 902925
    if-eqz v7, :cond_14

    .line 902926
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 902927
    :cond_14
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 902928
    if-eqz v6, :cond_15

    .line 902929
    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 902930
    :cond_15
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 902931
    const/16 v6, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 902932
    const/16 v6, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 902933
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 902934
    const/16 v6, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 902935
    const/16 v6, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 902936
    const/16 v6, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 902937
    const/16 v6, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11}, LX/186;->b(II)V

    .line 902938
    if-eqz v5, :cond_16

    .line 902939
    const/16 v5, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10}, LX/186;->a(IZ)V

    .line 902940
    :cond_16
    if-eqz v4, :cond_17

    .line 902941
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->a(IZ)V

    .line 902942
    :cond_17
    if-eqz v3, :cond_18

    .line 902943
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 902944
    :cond_18
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 902945
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 902946
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 902947
    if-eqz v0, :cond_0

    .line 902948
    const-string v1, "all_icons"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902949
    invoke-static {p0, v0, p2}, LX/5Lm;->a(LX/15i;ILX/0nX;)V

    .line 902950
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 902951
    if-eqz v0, :cond_1

    .line 902952
    const-string v1, "glyph"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902953
    invoke-static {p0, v0, p2}, LX/5Lj;->a(LX/15i;ILX/0nX;)V

    .line 902954
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 902955
    if-eqz v0, :cond_2

    .line 902956
    const-string v1, "iconImageLarge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902957
    invoke-static {p0, v0, p2}, LX/5Lk;->a(LX/15i;ILX/0nX;)V

    .line 902958
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 902959
    if-eqz v0, :cond_3

    .line 902960
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902961
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 902962
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 902963
    if-eqz v0, :cond_4

    .line 902964
    const-string v1, "is_linking_verb"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902965
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 902966
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 902967
    if-eqz v0, :cond_5

    .line 902968
    const-string v1, "legacy_api_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902969
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 902970
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 902971
    if-eqz v0, :cond_6

    .line 902972
    const-string v1, "prefetch_priority"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902973
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 902974
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 902975
    if-eqz v0, :cond_7

    .line 902976
    const-string v1, "present_participle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902977
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 902978
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 902979
    if-eqz v0, :cond_8

    .line 902980
    const-string v1, "previewTemplateAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902981
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 902982
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 902983
    if-eqz v0, :cond_9

    .line 902984
    const-string v1, "previewTemplateNoTags"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902985
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 902986
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 902987
    if-eqz v0, :cond_a

    .line 902988
    const-string v1, "previewTemplateWithPeople"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902989
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 902990
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 902991
    if-eqz v0, :cond_b

    .line 902992
    const-string v1, "previewTemplateWithPeopleAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902993
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 902994
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 902995
    if-eqz v0, :cond_c

    .line 902996
    const-string v1, "previewTemplateWithPerson"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 902997
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 902998
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 902999
    if-eqz v0, :cond_d

    .line 903000
    const-string v1, "previewTemplateWithPersonAtPlace"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903001
    invoke-static {p0, v0, p2, p3}, LX/5Li;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 903002
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 903003
    if-eqz v0, :cond_e

    .line 903004
    const-string v1, "prompt"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903005
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 903006
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 903007
    if-eqz v0, :cond_f

    .line 903008
    const-string v1, "supports_audio_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903009
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 903010
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 903011
    if-eqz v0, :cond_10

    .line 903012
    const-string v1, "supports_freeform"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903013
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 903014
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 903015
    if-eqz v0, :cond_11

    .line 903016
    const-string v1, "supports_offline_posting"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 903017
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 903018
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 903019
    return-void
.end method
