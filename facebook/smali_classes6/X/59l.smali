.class public final LX/59l;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 854244
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 854245
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 854246
    :goto_0
    return v1

    .line 854247
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_3

    .line 854248
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 854249
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 854250
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 854251
    const-string v9, "copyright_block_time"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 854252
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 854253
    :cond_1
    const-string v9, "copyright_block_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 854254
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCopyrightBlockType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_1

    .line 854255
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 854256
    :cond_3
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 854257
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 854258
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 854259
    :cond_4
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 854260
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v7, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    .line 854261
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 854262
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 854263
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 854264
    const-string v2, "copyright_block_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854265
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 854266
    :cond_0
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 854267
    if-eqz v0, :cond_1

    .line 854268
    const-string v0, "copyright_block_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854269
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 854270
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 854271
    return-void
.end method
