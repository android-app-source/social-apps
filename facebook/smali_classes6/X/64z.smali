.class public final LX/64z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/64q;

.field public b:Ljava/lang/String;

.field public c:LX/64m;

.field public d:LX/651;

.field public e:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1046332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046333
    const-string v0, "GET"

    iput-object v0, p0, LX/64z;->b:Ljava/lang/String;

    .line 1046334
    new-instance v0, LX/64m;

    invoke-direct {v0}, LX/64m;-><init>()V

    iput-object v0, p0, LX/64z;->c:LX/64m;

    .line 1046335
    return-void
.end method

.method public constructor <init>(LX/650;)V
    .locals 1

    .prologue
    .line 1046325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046326
    iget-object v0, p1, LX/650;->a:LX/64q;

    iput-object v0, p0, LX/64z;->a:LX/64q;

    .line 1046327
    iget-object v0, p1, LX/650;->b:Ljava/lang/String;

    iput-object v0, p0, LX/64z;->b:Ljava/lang/String;

    .line 1046328
    iget-object v0, p1, LX/650;->d:LX/651;

    iput-object v0, p0, LX/64z;->d:LX/651;

    .line 1046329
    iget-object v0, p1, LX/650;->e:Ljava/lang/Object;

    iput-object v0, p0, LX/64z;->e:Ljava/lang/Object;

    .line 1046330
    iget-object v0, p1, LX/650;->c:LX/64n;

    invoke-virtual {v0}, LX/64n;->newBuilder()LX/64m;

    move-result-object v0

    iput-object v0, p0, LX/64z;->c:LX/64m;

    .line 1046331
    return-void
.end method


# virtual methods
.method public final a(LX/64q;)LX/64z;
    .locals 2

    .prologue
    .line 1046322
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046323
    :cond_0
    iput-object p1, p0, LX/64z;->a:LX/64q;

    .line 1046324
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/64z;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1046311
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046312
    :cond_0
    const-string v3, "ws:"

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1046313
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1046314
    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 1046315
    new-instance v1, LX/64p;

    invoke-direct {v1}, LX/64p;-><init>()V

    .line 1046316
    invoke-virtual {v1, v0, p1}, LX/64p;->a(LX/64q;Ljava/lang/String;)LX/64o;

    move-result-object v2

    .line 1046317
    sget-object v3, LX/64o;->SUCCESS:LX/64o;

    if-ne v2, v3, :cond_2

    invoke-virtual {v1}, LX/64p;->c()LX/64q;

    move-result-object v0

    :cond_2
    move-object v0, v0

    .line 1046318
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected url: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046319
    :cond_3
    const-string v3, "wss:"

    move-object v0, p1

    move v4, v2

    move v5, v6

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1046320
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1046321
    :cond_4
    invoke-virtual {p0, v0}, LX/64z;->a(LX/64q;)LX/64z;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/651;)LX/64z;
    .locals 3

    .prologue
    .line 1046291
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "method == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046292
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "method.length() == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046293
    :cond_1
    if-eqz p2, :cond_2

    invoke-static {p1}, LX/66N;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1046294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "method "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must not have a request body."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046295
    :cond_2
    if-nez p2, :cond_3

    invoke-static {p1}, LX/66N;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1046296
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "method "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must have a request body."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046297
    :cond_3
    iput-object p1, p0, LX/64z;->b:Ljava/lang/String;

    .line 1046298
    iput-object p2, p0, LX/64z;->d:LX/651;

    .line 1046299
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/64z;
    .locals 1

    .prologue
    .line 1046306
    iget-object v0, p0, LX/64z;->c:LX/64m;

    .line 1046307
    invoke-static {p1, p2}, LX/64m;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046308
    invoke-virtual {v0, p1}, LX/64m;->b(Ljava/lang/String;)LX/64m;

    .line 1046309
    invoke-virtual {v0, p1, p2}, LX/64m;->b(Ljava/lang/String;Ljava/lang/String;)LX/64m;

    .line 1046310
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/64z;
    .locals 1

    .prologue
    .line 1046304
    iget-object v0, p0, LX/64z;->c:LX/64m;

    invoke-virtual {v0, p1}, LX/64m;->b(Ljava/lang/String;)LX/64m;

    .line 1046305
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/64z;
    .locals 1

    .prologue
    .line 1046302
    iget-object v0, p0, LX/64z;->c:LX/64m;

    invoke-virtual {v0, p1, p2}, LX/64m;->a(Ljava/lang/String;Ljava/lang/String;)LX/64m;

    .line 1046303
    return-object p0
.end method

.method public final b()LX/650;
    .locals 2

    .prologue
    .line 1046300
    iget-object v0, p0, LX/64z;->a:LX/64q;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046301
    :cond_0
    new-instance v0, LX/650;

    invoke-direct {v0, p0}, LX/650;-><init>(LX/64z;)V

    return-object v0
.end method
