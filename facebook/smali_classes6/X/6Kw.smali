.class public LX/6Kw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements LX/6Kd;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private final a:Landroid/view/TextureView;

.field private final b:Landroid/view/TextureView$SurfaceTextureListener;

.field private c:Landroid/view/Surface;

.field private d:LX/6Kl;


# direct methods
.method public constructor <init>(Landroid/view/TextureView;Landroid/view/TextureView$SurfaceTextureListener;)V
    .locals 2
    .param p2    # Landroid/view/TextureView$SurfaceTextureListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1077745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077746
    iput-object p1, p0, LX/6Kw;->a:Landroid/view/TextureView;

    .line 1077747
    iput-object p2, p0, LX/6Kw;->b:Landroid/view/TextureView$SurfaceTextureListener;

    .line 1077748
    iget-object v0, p0, LX/6Kw;->a:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1077749
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, LX/6Kw;->a:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    .line 1077750
    :goto_0
    iget-object v0, p0, LX/6Kw;->a:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 1077751
    return-void

    .line 1077752
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6Kl;)V
    .locals 2

    .prologue
    .line 1077753
    iput-object p1, p0, LX/6Kw;->d:LX/6Kl;

    .line 1077754
    iget-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6Kw;->a:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1077755
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, LX/6Kw;->a:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    .line 1077756
    :cond_0
    iget-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 1077757
    iget-object v0, p0, LX/6Kw;->d:LX/6Kl;

    iget-object v1, p0, LX/6Kw;->c:Landroid/view/Surface;

    invoke-virtual {v0, p0, v1}, LX/6Kl;->a(LX/6Kd;Landroid/view/Surface;)V

    .line 1077758
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1077763
    invoke-virtual {p0}, LX/6Kw;->dE_()V

    .line 1077764
    iget-object v0, p0, LX/6Kw;->a:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 1077765
    return-void
.end method

.method public final dE_()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1077759
    iget-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1077760
    iput-object v1, p0, LX/6Kw;->c:Landroid/view/Surface;

    .line 1077761
    iput-object v1, p0, LX/6Kw;->d:LX/6Kl;

    .line 1077762
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1077743
    return-void
.end method

.method public final getHeight()I
    .locals 1

    .prologue
    .line 1077744
    iget-object v0, p0, LX/6Kw;->a:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getHeight()I

    move-result v0

    return v0
.end method

.method public final getWidth()I
    .locals 1

    .prologue
    .line 1077742
    iget-object v0, p0, LX/6Kw;->a:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getWidth()I

    move-result v0

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 1077741
    iget-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    .prologue
    .line 1077732
    iget-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 1077733
    iget-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 1077734
    :cond_0
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    .line 1077735
    iget-object v0, p0, LX/6Kw;->d:LX/6Kl;

    if-eqz v0, :cond_1

    .line 1077736
    iget-object v0, p0, LX/6Kw;->d:LX/6Kl;

    iget-object v1, p0, LX/6Kw;->c:Landroid/view/Surface;

    invoke-virtual {v0, p0, v1}, LX/6Kl;->a(LX/6Kd;Landroid/view/Surface;)V

    .line 1077737
    iget-object v0, p0, LX/6Kw;->d:LX/6Kl;

    invoke-virtual {v0, p0}, LX/6Kl;->a(LX/6Kd;)V

    .line 1077738
    :cond_1
    iget-object v0, p0, LX/6Kw;->b:Landroid/view/TextureView$SurfaceTextureListener;

    if-eqz v0, :cond_2

    .line 1077739
    iget-object v0, p0, LX/6Kw;->b:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V

    .line 1077740
    :cond_2
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 1077727
    iget-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Kw;->d:LX/6Kl;

    if-eqz v0, :cond_0

    .line 1077728
    iget-object v0, p0, LX/6Kw;->d:LX/6Kl;

    invoke-virtual {v0, p0}, LX/6Kl;->b(LX/6Kd;)V

    .line 1077729
    :cond_0
    iget-object v0, p0, LX/6Kw;->b:Landroid/view/TextureView$SurfaceTextureListener;

    if-eqz v0, :cond_1

    .line 1077730
    iget-object v0, p0, LX/6Kw;->b:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z

    .line 1077731
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 1077722
    iget-object v0, p0, LX/6Kw;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Kw;->d:LX/6Kl;

    if-eqz v0, :cond_0

    .line 1077723
    iget-object v0, p0, LX/6Kw;->d:LX/6Kl;

    invoke-virtual {v0, p0}, LX/6Kl;->a(LX/6Kd;)V

    .line 1077724
    :cond_0
    iget-object v0, p0, LX/6Kw;->b:Landroid/view/TextureView$SurfaceTextureListener;

    if-eqz v0, :cond_1

    .line 1077725
    iget-object v0, p0, LX/6Kw;->b:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V

    .line 1077726
    :cond_1
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    .prologue
    .line 1077719
    iget-object v0, p0, LX/6Kw;->b:Landroid/view/TextureView$SurfaceTextureListener;

    if-eqz v0, :cond_0

    .line 1077720
    iget-object v0, p0, LX/6Kw;->b:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-interface {v0, p1}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V

    .line 1077721
    :cond_0
    return-void
.end method
