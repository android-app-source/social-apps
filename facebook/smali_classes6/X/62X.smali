.class public LX/62X;
.super LX/3x6;
.source ""


# instance fields
.field private final a:Landroid/graphics/Paint;

.field public b:I

.field public c:I

.field private d:LX/62W;

.field public e:Z


# direct methods
.method public constructor <init>(II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1041708
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1041709
    iput v1, p0, LX/62X;->b:I

    .line 1041710
    iput v1, p0, LX/62X;->c:I

    .line 1041711
    new-instance v0, LX/62W;

    invoke-direct {v0}, LX/62W;-><init>()V

    iput-object v0, p0, LX/62X;->d:LX/62W;

    .line 1041712
    iput-boolean v1, p0, LX/62X;->e:Z

    .line 1041713
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/62X;->a:Landroid/graphics/Paint;

    .line 1041714
    iget-object v0, p0, LX/62X;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1041715
    iget-object v0, p0, LX/62X;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1041716
    iget-object v0, p0, LX/62X;->a:Landroid/graphics/Paint;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1041717
    iget-object v0, p0, LX/62X;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 1041718
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;)V
    .locals 9

    .prologue
    .line 1041719
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v8

    .line 1041720
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    add-int/lit8 v0, v8, -0x1

    if-ge v7, v0, :cond_1

    .line 1041721
    invoke-virtual {p2, v7}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1041722
    const/4 v0, 0x1

    move v0, v0

    .line 1041723
    if-eqz v0, :cond_0

    .line 1041724
    iget-object v0, p0, LX/62X;->d:LX/62W;

    iget-object v2, p0, LX/62X;->a:Landroid/graphics/Paint;

    iget v5, p0, LX/62X;->b:I

    iget v6, p0, LX/62X;->c:I

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, LX/62W;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;II)V

    .line 1041725
    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 1041726
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 1

    .prologue
    .line 1041727
    invoke-super {p0, p1, p2, p3}, LX/3x6;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 1041728
    iget-boolean v0, p0, LX/62X;->e:Z

    if-nez v0, :cond_0

    .line 1041729
    invoke-direct {p0, p1, p2}, LX/62X;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;)V

    .line 1041730
    :cond_0
    return-void
.end method

.method public final b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 1

    .prologue
    .line 1041731
    invoke-super {p0, p1, p2, p3}, LX/3x6;->b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 1041732
    iget-boolean v0, p0, LX/62X;->e:Z

    if-eqz v0, :cond_0

    .line 1041733
    invoke-direct {p0, p1, p2}, LX/62X;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;)V

    .line 1041734
    :cond_0
    return-void
.end method
