.class public final LX/5Tw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 923288
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 923289
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 923290
    :goto_0
    return v1

    .line 923291
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 923292
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 923293
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 923294
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 923295
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 923296
    const-string v7, "android_app_config"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 923297
    const/4 v6, 0x0

    .line 923298
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_c

    .line 923299
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 923300
    :goto_2
    move v5, v6

    .line 923301
    goto :goto_1

    .line 923302
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 923303
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 923304
    :cond_3
    const-string v7, "messenger_app_attribution_visibility"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 923305
    invoke-static {p0, p1}, LX/5Tv;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 923306
    :cond_4
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 923307
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 923308
    :cond_5
    const-string v7, "square_logo"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 923309
    const/4 v6, 0x0

    .line 923310
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v7, :cond_10

    .line 923311
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 923312
    :goto_3
    move v0, v6

    .line 923313
    goto :goto_1

    .line 923314
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 923315
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 923316
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 923317
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 923318
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 923319
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 923320
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 923321
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 923322
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 923323
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 923324
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 923325
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_9

    if-eqz v8, :cond_9

    .line 923326
    const-string v9, "key_hashes"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 923327
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_4

    .line 923328
    :cond_a
    const-string v9, "package_name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 923329
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 923330
    :cond_b
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 923331
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 923332
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 923333
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_2

    :cond_c
    move v5, v6

    move v7, v6

    goto :goto_4

    .line 923334
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 923335
    :cond_e
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_f

    .line 923336
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 923337
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 923338
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_e

    if-eqz v7, :cond_e

    .line 923339
    const-string v8, "uri"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 923340
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 923341
    :cond_f
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 923342
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 923343
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_10
    move v0, v6

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 923344
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 923345
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 923346
    if-eqz v0, :cond_2

    .line 923347
    const-string v1, "android_app_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923348
    const/4 p3, 0x0

    .line 923349
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 923350
    invoke-virtual {p0, v0, p3}, LX/15i;->g(II)I

    move-result v1

    .line 923351
    if-eqz v1, :cond_0

    .line 923352
    const-string v1, "key_hashes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923353
    invoke-virtual {p0, v0, p3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 923354
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 923355
    if-eqz v1, :cond_1

    .line 923356
    const-string p3, "package_name"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923357
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 923358
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 923359
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 923360
    if-eqz v0, :cond_3

    .line 923361
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923362
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 923363
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 923364
    if-eqz v0, :cond_7

    .line 923365
    const-string v1, "messenger_app_attribution_visibility"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923366
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 923367
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 923368
    if-eqz v1, :cond_4

    .line 923369
    const-string p3, "hide_attribution"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923370
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 923371
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 923372
    if-eqz v1, :cond_5

    .line 923373
    const-string p3, "hide_install_button"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923374
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 923375
    :cond_5
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 923376
    if-eqz v1, :cond_6

    .line 923377
    const-string p3, "hide_reply_button"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923378
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 923379
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 923380
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 923381
    if-eqz v0, :cond_8

    .line 923382
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923383
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 923384
    :cond_8
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 923385
    if-eqz v0, :cond_a

    .line 923386
    const-string v1, "square_logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923387
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 923388
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 923389
    if-eqz v1, :cond_9

    .line 923390
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 923391
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 923392
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 923393
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 923394
    return-void
.end method
