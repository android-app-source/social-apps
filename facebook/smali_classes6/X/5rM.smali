.class public LX/5rM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[D>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1011239
    new-instance v0, LX/5rL;

    invoke-direct {v0}, LX/5rL;-><init>()V

    sput-object v0, LX/5rM;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1011240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/5pG;Ljava/lang/String;)D
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1011241
    const/4 v1, 0x1

    .line 1011242
    invoke-interface {p0, p1}, LX/5pG;->getType(Ljava/lang/String;)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v0

    sget-object v3, Lcom/facebook/react/bridge/ReadableType;->String:Lcom/facebook/react/bridge/ReadableType;

    if-ne v0, v3, :cond_2

    .line 1011243
    invoke-interface {p0, p1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1011244
    const-string v3, "rad"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1011245
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1011246
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    float-to-double v2, v0

    move-wide v4, v2

    move v2, v1

    move-wide v0, v4

    .line 1011247
    :goto_1
    if-eqz v2, :cond_3

    :goto_2
    return-wide v0

    .line 1011248
    :cond_1
    const-string v3, "deg"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1011249
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move v1, v2

    goto :goto_0

    .line 1011250
    :cond_2
    invoke-interface {p0, p1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    move-wide v4, v2

    move v2, v1

    move-wide v0, v4

    goto :goto_1

    .line 1011251
    :cond_3
    invoke-static {v0, v1}, LX/5qr;->a(D)D

    move-result-wide v0

    goto :goto_2
.end method

.method public static a(LX/5pC;[D)V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    .line 1011252
    sget-object v0, LX/5rM;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [D

    .line 1011253
    invoke-static {p1}, LX/5qr;->a([D)V

    .line 1011254
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v12

    move v11, v10

    :goto_0
    if-ge v11, v12, :cond_10

    .line 1011255
    invoke-interface {p0, v11}, LX/5pC;->a(I)LX/5pG;

    move-result-object v0

    .line 1011256
    invoke-interface {v0}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v2

    invoke-interface {v2}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v2

    .line 1011257
    invoke-static {v1}, LX/5qr;->a([D)V

    .line 1011258
    const-string v3, "matrix"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1011259
    invoke-interface {v0, v2}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v2

    move v0, v10

    .line 1011260
    :goto_1
    const/16 v3, 0x10

    if-ge v0, v3, :cond_1

    .line 1011261
    invoke-interface {v2, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v4

    aput-wide v4, v1, v0

    .line 1011262
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1011263
    :cond_0
    const-string v3, "perspective"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1011264
    invoke-interface {v0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v2, v3}, LX/5qr;->a([DD)V

    .line 1011265
    :cond_1
    :goto_2
    invoke-static {p1, p1, v1}, LX/5qr;->a([D[D[D)V

    .line 1011266
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_0

    .line 1011267
    :cond_2
    const-string v3, "rotateX"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1011268
    invoke-static {v0, v2}, LX/5rM;->a(LX/5pG;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v2, v3}, LX/5qr;->f([DD)V

    goto :goto_2

    .line 1011269
    :cond_3
    const-string v3, "rotateY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1011270
    invoke-static {v0, v2}, LX/5rM;->a(LX/5pG;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v2, v3}, LX/5qr;->g([DD)V

    goto :goto_2

    .line 1011271
    :cond_4
    const-string v3, "rotate"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "rotateZ"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1011272
    :cond_5
    invoke-static {v0, v2}, LX/5rM;->a(LX/5pG;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v2, v3}, LX/5qr;->h([DD)V

    goto :goto_2

    .line 1011273
    :cond_6
    const-string v3, "scale"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1011274
    invoke-interface {v0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 1011275
    invoke-static {v1, v2, v3}, LX/5qr;->b([DD)V

    .line 1011276
    invoke-static {v1, v2, v3}, LX/5qr;->c([DD)V

    goto :goto_2

    .line 1011277
    :cond_7
    const-string v3, "scaleX"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1011278
    invoke-interface {v0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v2, v3}, LX/5qr;->b([DD)V

    goto :goto_2

    .line 1011279
    :cond_8
    const-string v3, "scaleY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1011280
    invoke-interface {v0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v2, v3}, LX/5qr;->c([DD)V

    goto :goto_2

    .line 1011281
    :cond_9
    const-string v3, "translate"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1011282
    invoke-interface {v0, v2}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    .line 1011283
    invoke-interface {v0, v10}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    .line 1011284
    const/4 v4, 0x1

    invoke-interface {v0, v4}, LX/5pC;->getDouble(I)D

    move-result-wide v4

    .line 1011285
    invoke-interface {v0}, LX/5pC;->size()I

    move-result v6

    if-le v6, v13, :cond_a

    invoke-interface {v0, v13}, LX/5pC;->getDouble(I)D

    move-result-wide v6

    .line 1011286
    :goto_3
    invoke-static/range {v1 .. v7}, LX/5qr;->a([DDDD)V

    goto/16 :goto_2

    :cond_a
    move-wide v6, v8

    .line 1011287
    goto :goto_3

    .line 1011288
    :cond_b
    const-string v3, "translateX"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1011289
    invoke-interface {v0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v2, v3, v8, v9}, LX/5qr;->a([DDD)V

    goto/16 :goto_2

    .line 1011290
    :cond_c
    const-string v3, "translateY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1011291
    invoke-interface {v0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v8, v9, v2, v3}, LX/5qr;->a([DDD)V

    goto/16 :goto_2

    .line 1011292
    :cond_d
    const-string v3, "skewX"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1011293
    invoke-static {v0, v2}, LX/5rM;->a(LX/5pG;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v2, v3}, LX/5qr;->d([DD)V

    goto/16 :goto_2

    .line 1011294
    :cond_e
    const-string v3, "skewY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1011295
    invoke-static {v0, v2}, LX/5rM;->a(LX/5pG;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v1, v2, v3}, LX/5qr;->e([DD)V

    goto/16 :goto_2

    .line 1011296
    :cond_f
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported transform type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011297
    :cond_10
    return-void
.end method
