.class public final LX/5gw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 977185
    const/16 v26, 0x0

    .line 977186
    const/16 v25, 0x0

    .line 977187
    const/16 v24, 0x0

    .line 977188
    const/16 v23, 0x0

    .line 977189
    const/16 v22, 0x0

    .line 977190
    const/16 v19, 0x0

    .line 977191
    const-wide/16 v20, 0x0

    .line 977192
    const/16 v18, 0x0

    .line 977193
    const/16 v17, 0x0

    .line 977194
    const/16 v16, 0x0

    .line 977195
    const/4 v13, 0x0

    .line 977196
    const-wide/16 v14, 0x0

    .line 977197
    const/4 v12, 0x0

    .line 977198
    const/4 v11, 0x0

    .line 977199
    const/4 v10, 0x0

    .line 977200
    const/4 v9, 0x0

    .line 977201
    const/4 v8, 0x0

    .line 977202
    const/4 v7, 0x0

    .line 977203
    const/4 v6, 0x0

    .line 977204
    const/4 v5, 0x0

    .line 977205
    const/4 v4, 0x0

    .line 977206
    const/4 v3, 0x0

    .line 977207
    const/4 v2, 0x0

    .line 977208
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_19

    .line 977209
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 977210
    const/4 v2, 0x0

    .line 977211
    :goto_0
    return v2

    .line 977212
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v28

    if-eq v2, v0, :cond_12

    .line 977213
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 977214
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 977215
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 977216
    const-string v28, "album_cover_photo"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_1

    .line 977217
    invoke-static/range {p0 .. p1}, LX/5go;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 977218
    :cond_1
    const-string v28, "album_type"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 977219
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 977220
    :cond_2
    const-string v28, "allow_contributors"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 977221
    const/4 v2, 0x1

    .line 977222
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v25, v10

    move v10, v2

    goto :goto_1

    .line 977223
    :cond_3
    const-string v28, "can_edit_caption"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 977224
    const/4 v2, 0x1

    .line 977225
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v24, v9

    move v9, v2

    goto :goto_1

    .line 977226
    :cond_4
    const-string v28, "can_upload"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 977227
    const/4 v2, 0x1

    .line 977228
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v23, v7

    move v7, v2

    goto/16 :goto_1

    .line 977229
    :cond_5
    const-string v28, "can_viewer_delete"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 977230
    const/4 v2, 0x1

    .line 977231
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v22, v6

    move v6, v2

    goto/16 :goto_1

    .line 977232
    :cond_6
    const-string v28, "created_time"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 977233
    const/4 v2, 0x1

    .line 977234
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 977235
    :cond_7
    const-string v28, "explicit_place"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 977236
    invoke-static/range {p0 .. p1}, LX/5gp;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 977237
    :cond_8
    const-string v28, "id"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 977238
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 977239
    :cond_9
    const-string v28, "media_owner_object"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 977240
    invoke-static/range {p0 .. p1}, LX/5gq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 977241
    :cond_a
    const-string v28, "message"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 977242
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 977243
    :cond_b
    const-string v28, "modified_time"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 977244
    const/4 v2, 0x1

    .line 977245
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v16

    move v8, v2

    goto/16 :goto_1

    .line 977246
    :cond_c
    const-string v28, "owner"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 977247
    invoke-static/range {p0 .. p1}, LX/5gr;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 977248
    :cond_d
    const-string v28, "photo_items"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 977249
    invoke-static/range {p0 .. p1}, LX/5gs;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 977250
    :cond_e
    const-string v28, "privacy_scope"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 977251
    invoke-static/range {p0 .. p1}, LX/5gv;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 977252
    :cond_f
    const-string v28, "title"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 977253
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 977254
    :cond_10
    const-string v28, "url"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 977255
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 977256
    :cond_11
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 977257
    :cond_12
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 977258
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 977259
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 977260
    if-eqz v10, :cond_13

    .line 977261
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 977262
    :cond_13
    if-eqz v9, :cond_14

    .line 977263
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 977264
    :cond_14
    if-eqz v7, :cond_15

    .line 977265
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 977266
    :cond_15
    if-eqz v6, :cond_16

    .line 977267
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 977268
    :cond_16
    if-eqz v3, :cond_17

    .line 977269
    const/4 v3, 0x6

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 977270
    :cond_17
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 977271
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 977272
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 977273
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 977274
    if-eqz v8, :cond_18

    .line 977275
    const/16 v3, 0xb

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 977276
    :cond_18
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 977277
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 977278
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 977279
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 977280
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 977281
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_19
    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v19

    move/from16 v19, v16

    move/from16 v30, v10

    move v10, v7

    move v7, v5

    move/from16 v31, v12

    move v12, v9

    move v9, v6

    move v6, v4

    move-wide/from16 v4, v20

    move/from16 v20, v17

    move/from16 v21, v18

    move-wide/from16 v16, v14

    move/from16 v18, v13

    move/from16 v13, v30

    move/from16 v15, v31

    move v14, v11

    move v11, v8

    move v8, v2

    goto/16 :goto_1
.end method
