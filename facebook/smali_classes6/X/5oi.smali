.class public LX/5oi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26y;


# instance fields
.field public a:I

.field private final b:LX/0ag;

.field private final c:LX/0ej;

.field private final d:LX/26w;

.field private final e:I


# direct methods
.method public constructor <init>(LX/0ag;LX/0ej;LX/26w;I)V
    .locals 1

    .prologue
    .line 1007455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007456
    const/4 v0, -0x1

    iput v0, p0, LX/5oi;->a:I

    .line 1007457
    iput-object p1, p0, LX/5oi;->b:LX/0ag;

    .line 1007458
    iput-object p2, p0, LX/5oi;->c:LX/0ej;

    .line 1007459
    iput-object p3, p0, LX/5oi;->d:LX/26w;

    .line 1007460
    iput p4, p0, LX/5oi;->e:I

    .line 1007461
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1007462
    iget-object v0, p0, LX/5oi;->b:LX/0ag;

    invoke-virtual {v0, p1}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/5oi;->a:I

    .line 1007463
    return-void
.end method

.method public final a(Ljava/lang/String;IIZ)V
    .locals 6

    .prologue
    .line 1007464
    iget-object v3, p0, LX/5oi;->c:LX/0ej;

    iget-object v4, p0, LX/5oi;->d:LX/26w;

    sget-object v5, LX/0oc;->ASSIGNED:LX/0oc;

    move v0, p2

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    .line 1007465
    iget v0, p0, LX/5oi;->e:I

    iget v1, p0, LX/5oi;->a:I

    if-eq v0, v1, :cond_0

    .line 1007466
    iget-object v3, p0, LX/5oi;->c:LX/0ej;

    iget-object v4, p0, LX/5oi;->d:LX/26w;

    sget-object v5, LX/0oc;->OVERRIDE:LX/0oc;

    move v0, p2

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    .line 1007467
    :cond_0
    return-void
.end method
