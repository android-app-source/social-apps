.class public final LX/6dV;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1116214
    new-instance v0, LX/0U1;

    const-string v1, "event_reminder_type"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dV;->a:LX/0U1;

    .line 1116215
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dV;->b:LX/0U1;

    .line 1116216
    new-instance v0, LX/0U1;

    const-string v1, "event_reminder_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dV;->c:LX/0U1;

    .line 1116217
    new-instance v0, LX/0U1;

    const-string v1, "event_reminder_timestamp"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dV;->d:LX/0U1;

    .line 1116218
    new-instance v0, LX/0U1;

    const-string v1, "event_reminder_title"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dV;->e:LX/0U1;

    .line 1116219
    new-instance v0, LX/0U1;

    const-string v1, "allows_rsvp"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dV;->f:LX/0U1;

    .line 1116220
    new-instance v0, LX/0U1;

    const-string v1, "event_reminder_location_name"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dV;->g:LX/0U1;

    return-void
.end method
