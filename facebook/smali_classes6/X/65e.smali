.class public final LX/65e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65D;


# static fields
.field public static final synthetic a:Z


# instance fields
.field public final synthetic b:LX/65i;

.field private final c:LX/672;

.field private final d:LX/672;

.field private final e:J

.field public f:Z

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1048105
    const-class v0, LX/65i;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, LX/65e;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(LX/65i;J)V
    .locals 2

    .prologue
    .line 1048106
    iput-object p1, p0, LX/65e;->b:LX/65i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048107
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    iput-object v0, p0, LX/65e;->c:LX/672;

    .line 1048108
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    iput-object v0, p0, LX/65e;->d:LX/672;

    .line 1048109
    iput-wide p2, p0, LX/65e;->e:J

    .line 1048110
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 1048111
    iget-object v0, p0, LX/65e;->b:LX/65i;

    iget-object v0, v0, LX/65i;->j:LX/65h;

    invoke-virtual {v0}, LX/65g;->c()V

    .line 1048112
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/65e;->d:LX/672;

    .line 1048113
    iget-wide v4, v0, LX/672;->b:J

    move-wide v0, v4

    .line 1048114
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/65e;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/65e;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/65e;->b:LX/65i;

    iget-object v0, v0, LX/65i;->l:LX/65X;

    if-nez v0, :cond_0

    .line 1048115
    iget-object v0, p0, LX/65e;->b:LX/65i;

    invoke-static {v0}, LX/65i;->l(LX/65i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1048116
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/65e;->b:LX/65i;

    iget-object v1, v1, LX/65i;->j:LX/65h;

    invoke-virtual {v1}, LX/65h;->b()V

    throw v0

    :cond_0
    iget-object v0, p0, LX/65e;->b:LX/65i;

    iget-object v0, v0, LX/65i;->j:LX/65h;

    invoke-virtual {v0}, LX/65h;->b()V

    .line 1048117
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1048118
    iget-boolean v0, p0, LX/65e;->f:Z

    if-eqz v0, :cond_0

    .line 1048119
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048120
    :cond_0
    iget-object v0, p0, LX/65e;->b:LX/65i;

    iget-object v0, v0, LX/65i;->l:LX/65X;

    if-eqz v0, :cond_1

    .line 1048121
    new-instance v0, LX/667;

    iget-object v1, p0, LX/65e;->b:LX/65i;

    iget-object v1, v1, LX/65i;->l:LX/65X;

    invoke-direct {v0, v1}, LX/667;-><init>(LX/65X;)V

    throw v0

    .line 1048122
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    .line 1048123
    cmp-long v0, p2, v4

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048124
    :cond_0
    iget-object v2, p0, LX/65e;->b:LX/65i;

    monitor-enter v2

    .line 1048125
    :try_start_0
    invoke-direct {p0}, LX/65e;->b()V

    .line 1048126
    invoke-direct {p0}, LX/65e;->c()V

    .line 1048127
    iget-object v0, p0, LX/65e;->d:LX/672;

    .line 1048128
    iget-wide v8, v0, LX/672;->b:J

    move-wide v0, v8

    .line 1048129
    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    const-wide/16 v0, -0x1

    monitor-exit v2

    .line 1048130
    :goto_0
    return-wide v0

    .line 1048131
    :cond_1
    iget-object v0, p0, LX/65e;->d:LX/672;

    iget-object v1, p0, LX/65e;->d:LX/672;

    .line 1048132
    iget-wide v8, v1, LX/672;->b:J

    move-wide v4, v8

    .line 1048133
    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-virtual {v0, p1, v4, v5}, LX/672;->a(LX/672;J)J

    move-result-wide v0

    .line 1048134
    iget-object v3, p0, LX/65e;->b:LX/65i;

    iget-wide v4, v3, LX/65i;->a:J

    add-long/2addr v4, v0

    iput-wide v4, v3, LX/65i;->a:J

    .line 1048135
    iget-object v3, p0, LX/65e;->b:LX/65i;

    iget-wide v4, v3, LX/65i;->a:J

    iget-object v3, p0, LX/65e;->b:LX/65i;

    .line 1048136
    iget-object v3, v3, LX/65i;->f:LX/65c;

    iget-object v3, v3, LX/65c;->e:LX/663;

    const/high16 v6, 0x10000

    invoke-virtual {v3, v6}, LX/663;->f(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_2

    .line 1048137
    iget-object v3, p0, LX/65e;->b:LX/65i;

    iget-object v3, v3, LX/65i;->f:LX/65c;

    iget-object v4, p0, LX/65e;->b:LX/65i;

    iget v4, v4, LX/65i;->e:I

    iget-object v5, p0, LX/65e;->b:LX/65i;

    iget-wide v6, v5, LX/65i;->a:J

    invoke-virtual {v3, v4, v6, v7}, LX/65c;->a(IJ)V

    .line 1048138
    iget-object v3, p0, LX/65e;->b:LX/65i;

    const-wide/16 v4, 0x0

    iput-wide v4, v3, LX/65i;->a:J

    .line 1048139
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1048140
    iget-object v2, p0, LX/65e;->b:LX/65i;

    iget-object v2, v2, LX/65i;->f:LX/65c;

    monitor-enter v2

    .line 1048141
    :try_start_1
    iget-object v3, p0, LX/65e;->b:LX/65i;

    iget-object v3, v3, LX/65i;->f:LX/65c;

    iget-wide v4, v3, LX/65c;->c:J

    add-long/2addr v4, v0

    iput-wide v4, v3, LX/65c;->c:J

    .line 1048142
    iget-object v3, p0, LX/65e;->b:LX/65i;

    iget-object v3, v3, LX/65i;->f:LX/65c;

    iget-wide v4, v3, LX/65c;->c:J

    iget-object v3, p0, LX/65e;->b:LX/65i;

    .line 1048143
    iget-object v3, v3, LX/65i;->f:LX/65c;

    iget-object v3, v3, LX/65c;->e:LX/663;

    const/high16 v6, 0x10000

    invoke-virtual {v3, v6}, LX/663;->f(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_3

    .line 1048144
    iget-object v3, p0, LX/65e;->b:LX/65i;

    iget-object v3, v3, LX/65i;->f:LX/65c;

    const/4 v4, 0x0

    iget-object v5, p0, LX/65e;->b:LX/65i;

    iget-object v5, v5, LX/65i;->f:LX/65c;

    iget-wide v6, v5, LX/65c;->c:J

    invoke-virtual {v3, v4, v6, v7}, LX/65c;->a(IJ)V

    .line 1048145
    iget-object v3, p0, LX/65e;->b:LX/65i;

    iget-object v3, v3, LX/65i;->f:LX/65c;

    const-wide/16 v4, 0x0

    iput-wide v4, v3, LX/65c;->c:J

    .line 1048146
    :cond_3
    monitor-exit v2

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1048147
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1048148
    iget-object v0, p0, LX/65e;->b:LX/65i;

    iget-object v0, v0, LX/65i;->j:LX/65h;

    return-object v0
.end method

.method public final a(LX/671;J)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1048149
    sget-boolean v0, LX/65e;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/65e;->b:LX/65i;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048150
    :cond_0
    sub-long/2addr p2, v4

    .line 1048151
    iget-object v3, p0, LX/65e;->b:LX/65i;

    monitor-enter v3

    .line 1048152
    :try_start_0
    iget-object v0, p0, LX/65e;->d:LX/672;

    invoke-virtual {v0}, LX/672;->b()J

    move-result-wide v4

    cmp-long v0, v4, v10

    if-nez v0, :cond_7

    move v0, v1

    .line 1048153
    :goto_0
    iget-object v4, p0, LX/65e;->d:LX/672;

    iget-object v5, p0, LX/65e;->c:LX/672;

    invoke-virtual {v4, v5}, LX/672;->a(LX/65D;)J

    .line 1048154
    if-eqz v0, :cond_1

    .line 1048155
    iget-object v0, p0, LX/65e;->b:LX/65i;

    const v4, 0x18139baf

    invoke-static {v0, v4}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1048156
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1048157
    :cond_2
    cmp-long v0, p2, v10

    if-lez v0, :cond_3

    .line 1048158
    iget-object v3, p0, LX/65e;->b:LX/65i;

    monitor-enter v3

    .line 1048159
    :try_start_1
    iget-boolean v4, p0, LX/65e;->g:Z

    .line 1048160
    iget-object v0, p0, LX/65e;->d:LX/672;

    invoke-virtual {v0}, LX/672;->b()J

    move-result-wide v6

    add-long/2addr v6, p2

    iget-wide v8, p0, LX/65e;->e:J

    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    move v0, v1

    .line 1048161
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048162
    if-eqz v0, :cond_5

    .line 1048163
    invoke-interface {p1, p2, p3}, LX/671;->f(J)V

    .line 1048164
    iget-object v0, p0, LX/65e;->b:LX/65i;

    sget-object v1, LX/65X;->FLOW_CONTROL_ERROR:LX/65X;

    invoke-virtual {v0, v1}, LX/65i;->b(LX/65X;)V

    .line 1048165
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v2

    .line 1048166
    goto :goto_1

    .line 1048167
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1048168
    :cond_5
    if-eqz v4, :cond_6

    .line 1048169
    invoke-interface {p1, p2, p3}, LX/671;->f(J)V

    goto :goto_2

    .line 1048170
    :cond_6
    iget-object v0, p0, LX/65e;->c:LX/672;

    invoke-interface {p1, v0, p2, p3}, LX/65D;->a(LX/672;J)J

    move-result-wide v4

    .line 1048171
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_7
    move v0, v2

    .line 1048172
    goto :goto_0

    .line 1048173
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final close()V
    .locals 3

    .prologue
    .line 1048174
    iget-object v1, p0, LX/65e;->b:LX/65i;

    monitor-enter v1

    .line 1048175
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LX/65e;->f:Z

    .line 1048176
    iget-object v0, p0, LX/65e;->d:LX/672;

    invoke-virtual {v0}, LX/672;->s()V

    .line 1048177
    iget-object v0, p0, LX/65e;->b:LX/65i;

    const v2, 0x15dc7f11

    invoke-static {v0, v2}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1048178
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048179
    iget-object v0, p0, LX/65e;->b:LX/65i;

    invoke-static {v0}, LX/65i;->j(LX/65i;)V

    .line 1048180
    return-void

    .line 1048181
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
