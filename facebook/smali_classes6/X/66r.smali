.class public final LX/66r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65J;


# instance fields
.field public final synthetic a:LX/66s;

.field public b:I

.field public c:J

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>(LX/66s;)V
    .locals 0

    .prologue
    .line 1051123
    iput-object p1, p0, LX/66r;->a:LX/66s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1051146
    iget-object v0, p0, LX/66r;->a:LX/66s;

    iget-object v0, v0, LX/66s;->d:LX/670;

    invoke-interface {v0}, LX/65J;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final a_(LX/672;J)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 1051147
    iget-boolean v0, p0, LX/66r;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051148
    :cond_0
    iget-object v0, p0, LX/66r;->a:LX/66s;

    iget-object v0, v0, LX/66s;->f:LX/672;

    invoke-virtual {v0, p1, p2, p3}, LX/672;->a_(LX/672;J)V

    .line 1051149
    iget-boolean v0, p0, LX/66r;->d:Z

    if-eqz v0, :cond_2

    iget-wide v0, p0, LX/66r;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/66r;->a:LX/66s;

    .line 1051150
    iget-object v0, v0, LX/66s;->f:LX/672;

    .line 1051151
    iget-wide v8, v0, LX/672;->b:J

    move-wide v0, v8

    .line 1051152
    iget-wide v2, p0, LX/66r;->c:J

    const-wide/16 v4, 0x2000

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 1051153
    :goto_0
    iget-object v1, p0, LX/66r;->a:LX/66s;

    iget-object v1, v1, LX/66s;->f:LX/672;

    invoke-virtual {v1}, LX/672;->g()J

    move-result-wide v2

    .line 1051154
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    if-nez v0, :cond_1

    .line 1051155
    iget-object v7, p0, LX/66r;->a:LX/66s;

    monitor-enter v7

    .line 1051156
    :try_start_0
    iget-object v0, p0, LX/66r;->a:LX/66s;

    iget v1, p0, LX/66r;->b:I

    iget-boolean v4, p0, LX/66r;->d:Z

    const/4 v5, 0x0

    .line 1051157
    invoke-static/range {v0 .. v5}, LX/66s;->a$redex0(LX/66s;IJZZ)V

    .line 1051158
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1051159
    iput-boolean v6, p0, LX/66r;->d:Z

    .line 1051160
    :cond_1
    return-void

    :cond_2
    move v0, v6

    .line 1051161
    goto :goto_0

    .line 1051162
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final close()V
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 1051134
    iget-boolean v0, p0, LX/66r;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051135
    :cond_0
    iget-object v6, p0, LX/66r;->a:LX/66s;

    monitor-enter v6

    .line 1051136
    :try_start_0
    iget-object v0, p0, LX/66r;->a:LX/66s;

    iget v1, p0, LX/66r;->b:I

    iget-object v2, p0, LX/66r;->a:LX/66s;

    iget-object v2, v2, LX/66s;->f:LX/672;

    .line 1051137
    iget-wide v8, v2, LX/672;->b:J

    move-wide v2, v8

    .line 1051138
    iget-boolean v4, p0, LX/66r;->d:Z

    const/4 v5, 0x1

    .line 1051139
    invoke-static/range {v0 .. v5}, LX/66s;->a$redex0(LX/66s;IJZZ)V

    .line 1051140
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1051141
    iput-boolean v7, p0, LX/66r;->e:Z

    .line 1051142
    iget-object v0, p0, LX/66r;->a:LX/66s;

    const/4 v1, 0x0

    .line 1051143
    iput-boolean v1, v0, LX/66s;->h:Z

    .line 1051144
    return-void

    .line 1051145
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final flush()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 1051124
    iget-boolean v0, p0, LX/66r;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051125
    :cond_0
    iget-object v6, p0, LX/66r;->a:LX/66s;

    monitor-enter v6

    .line 1051126
    :try_start_0
    iget-object v0, p0, LX/66r;->a:LX/66s;

    iget v1, p0, LX/66r;->b:I

    iget-object v2, p0, LX/66r;->a:LX/66s;

    iget-object v2, v2, LX/66s;->f:LX/672;

    .line 1051127
    iget-wide v8, v2, LX/672;->b:J

    move-wide v2, v8

    .line 1051128
    iget-boolean v4, p0, LX/66r;->d:Z

    const/4 v5, 0x0

    .line 1051129
    invoke-static/range {v0 .. v5}, LX/66s;->a$redex0(LX/66s;IJZZ)V

    .line 1051130
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1051131
    iput-boolean v7, p0, LX/66r;->d:Z

    .line 1051132
    return-void

    .line 1051133
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
