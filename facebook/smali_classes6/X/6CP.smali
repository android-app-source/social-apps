.class public LX/6CP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/6CH;

.field private final b:LX/0tX;


# direct methods
.method public constructor <init>(LX/6CH;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063813
    iput-object p1, p0, LX/6CP;->a:LX/6CH;

    .line 1063814
    iput-object p2, p0, LX/6CP;->b:LX/0tX;

    .line 1063815
    return-void
.end method

.method public static a(LX/6CP;LX/4HH;LX/6CO;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1063816
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1063817
    new-instance v0, LX/6CJ;

    invoke-direct {v0}, LX/6CJ;-><init>()V

    move-object v0, v0

    .line 1063818
    const-string v1, "input"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1063819
    iget-object v1, p0, LX/6CP;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/6CN;

    invoke-direct {v1, p0, p2, p3}, LX/6CN;-><init>(LX/6CP;LX/6CO;Ljava/lang/String;)V

    .line 1063820
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1063821
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1063822
    return-void
.end method

.method public static b(LX/0QB;)LX/6CP;
    .locals 3

    .prologue
    .line 1063823
    new-instance v2, LX/6CP;

    invoke-static {p0}, LX/6CH;->b(LX/0QB;)LX/6CH;

    move-result-object v0

    check-cast v0, LX/6CH;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-direct {v2, v0, v1}, LX/6CP;-><init>(LX/6CH;LX/0tX;)V

    .line 1063824
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/6CO;)V
    .locals 2

    .prologue
    .line 1063825
    new-instance v0, LX/4HH;

    invoke-direct {v0}, LX/4HH;-><init>()V

    const-string v1, "RESET_CART"

    invoke-virtual {v0, v1}, LX/4HH;->a(Ljava/lang/String;)LX/4HH;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4HH;->b(Ljava/lang/String;)LX/4HH;

    move-result-object v0

    .line 1063826
    invoke-static {p0, v0, p2, p1}, LX/6CP;->a(LX/6CP;LX/4HH;LX/6CO;Ljava/lang/String;)V

    .line 1063827
    return-void
.end method
