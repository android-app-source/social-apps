.class public final LX/5ma;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1002347
    const/4 v10, 0x0

    .line 1002348
    const/4 v9, 0x0

    .line 1002349
    const/4 v8, 0x0

    .line 1002350
    const/4 v7, 0x0

    .line 1002351
    const/4 v6, 0x0

    .line 1002352
    const/4 v5, 0x0

    .line 1002353
    const/4 v4, 0x0

    .line 1002354
    const/4 v3, 0x0

    .line 1002355
    const/4 v2, 0x0

    .line 1002356
    const/4 v1, 0x0

    .line 1002357
    const/4 v0, 0x0

    .line 1002358
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1002359
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1002360
    const/4 v0, 0x0

    .line 1002361
    :goto_0
    return v0

    .line 1002362
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1002363
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_d

    .line 1002364
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1002365
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1002366
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1002367
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1002368
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1002369
    :cond_3
    const-string v12, "address"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1002370
    invoke-static {p0, p1}, LX/5mT;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1002371
    :cond_4
    const-string v12, "category_icon"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1002372
    invoke-static {p0, p1}, LX/5mU;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1002373
    :cond_5
    const-string v12, "contextual_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1002374
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1002375
    :cond_6
    const-string v12, "flowable_taggable_activity"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1002376
    invoke-static {p0, p1}, LX/5mX;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1002377
    :cond_7
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1002378
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1002379
    :cond_8
    const-string v12, "location"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1002380
    invoke-static {p0, p1}, LX/5mY;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1002381
    :cond_9
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1002382
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1002383
    :cond_a
    const-string v12, "page_visits"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1002384
    invoke-static {p0, p1}, LX/5mZ;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1002385
    :cond_b
    const-string v12, "place_topic_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 1002386
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 1002387
    :cond_c
    const-string v12, "place_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1002388
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPlaceType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto/16 :goto_1

    .line 1002389
    :cond_d
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1002390
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 1002391
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1002392
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1002393
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1002394
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1002395
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1002396
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1002397
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 1002398
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1002399
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1002400
    const/16 v1, 0xa

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1002401
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    const/4 v1, 0x0

    .line 1002402
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1002403
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1002404
    if-eqz v0, :cond_0

    .line 1002405
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002406
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1002407
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1002408
    if-eqz v0, :cond_1

    .line 1002409
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002410
    invoke-static {p0, v0, p2}, LX/5mT;->a(LX/15i;ILX/0nX;)V

    .line 1002411
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1002412
    if-eqz v0, :cond_2

    .line 1002413
    const-string v1, "category_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002414
    invoke-static {p0, v0, p2}, LX/5mU;->a(LX/15i;ILX/0nX;)V

    .line 1002415
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1002416
    if-eqz v0, :cond_3

    .line 1002417
    const-string v1, "contextual_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002418
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1002419
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1002420
    if-eqz v0, :cond_4

    .line 1002421
    const-string v1, "flowable_taggable_activity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002422
    invoke-static {p0, v0, p2, p3}, LX/5mX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1002423
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1002424
    if-eqz v0, :cond_5

    .line 1002425
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002426
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1002427
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1002428
    if-eqz v0, :cond_6

    .line 1002429
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002430
    invoke-static {p0, v0, p2}, LX/5mY;->a(LX/15i;ILX/0nX;)V

    .line 1002431
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1002432
    if-eqz v0, :cond_7

    .line 1002433
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002434
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1002435
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1002436
    if-eqz v0, :cond_8

    .line 1002437
    const-string v1, "page_visits"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002438
    invoke-static {p0, v0, p2}, LX/5mZ;->a(LX/15i;ILX/0nX;)V

    .line 1002439
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1002440
    if-eqz v0, :cond_9

    .line 1002441
    const-string v1, "place_topic_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002442
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1002443
    :cond_9
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1002444
    if-eqz v0, :cond_a

    .line 1002445
    const-string v0, "place_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1002446
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1002447
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1002448
    return-void
.end method
