.class public LX/6Kp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6Ko;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1077489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077490
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/6Kp;->a:Ljava/util/List;

    .line 1077491
    return-void
.end method


# virtual methods
.method public final a()LX/6Ko;
    .locals 2

    .prologue
    .line 1077492
    monitor-enter p0

    .line 1077493
    :try_start_0
    iget-object v0, p0, LX/6Kp;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1077494
    iget-object v0, p0, LX/6Kp;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Ko;

    monitor-exit p0

    .line 1077495
    :goto_0
    return-object v0

    .line 1077496
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1077497
    new-instance v0, LX/6Ko;

    invoke-direct {v0}, LX/6Ko;-><init>()V

    goto :goto_0

    .line 1077498
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/6Ko;)V
    .locals 2

    .prologue
    .line 1077499
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Trying to return a null object"

    invoke-static {v0, v1}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 1077500
    const/4 v0, 0x0

    iput-object v0, p1, LX/6Ko;->a:LX/7Sb;

    .line 1077501
    iget-object v0, p1, LX/6Ko;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1077502
    monitor-enter p0

    .line 1077503
    :try_start_0
    iget-object v0, p0, LX/6Kp;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1077504
    monitor-exit p0

    return-void

    .line 1077505
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1077506
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
