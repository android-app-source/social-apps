.class public LX/6aM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:LX/692;

.field public c:Lcom/facebook/android/maps/model/LatLng;

.field public d:LX/697;

.field public e:Landroid/graphics/Point;

.field public f:F

.field public g:F

.field public h:F

.field public i:F

.field public j:I

.field public k:I

.field public l:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 1112358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112359
    iput p1, p0, LX/6aM;->a:I

    .line 1112360
    return-void
.end method


# virtual methods
.method public final a()LX/67d;
    .locals 4

    .prologue
    .line 1112374
    iget v0, p0, LX/6aM;->a:I

    packed-switch v0, :pswitch_data_0

    .line 1112375
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1112376
    :pswitch_0
    iget-object v0, p0, LX/6aM;->b:LX/692;

    const/4 v3, 0x1

    .line 1112377
    new-instance v1, LX/67d;

    invoke-direct {v1}, LX/67d;-><init>()V

    .line 1112378
    iget-object v2, v0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    iput-object v2, v1, LX/67d;->a:Lcom/facebook/android/maps/model/LatLng;

    .line 1112379
    iget v2, v0, LX/692;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 1112380
    iget v2, v0, LX/692;->b:F

    iput v2, v1, LX/67d;->b:F

    .line 1112381
    :cond_0
    iget v2, v0, LX/692;->d:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    .line 1112382
    iget v2, v0, LX/692;->d:F

    iput v2, v1, LX/67d;->h:F

    .line 1112383
    :cond_1
    move-object v0, v1

    .line 1112384
    goto :goto_0

    .line 1112385
    :pswitch_1
    iget-object v0, p0, LX/6aM;->c:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v0}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;)LX/67d;

    move-result-object v0

    goto :goto_0

    .line 1112386
    :pswitch_2
    iget-object v0, p0, LX/6aM;->d:LX/697;

    iget v1, p0, LX/6aM;->l:I

    invoke-static {v0, v1}, LX/67e;->a(LX/697;I)LX/67d;

    move-result-object v0

    goto :goto_0

    .line 1112387
    :pswitch_3
    iget-object v0, p0, LX/6aM;->d:LX/697;

    iget v1, p0, LX/6aM;->j:I

    iget v2, p0, LX/6aM;->k:I

    iget v3, p0, LX/6aM;->l:I

    invoke-static {v0, v1, v2, v3}, LX/67e;->a(LX/697;III)LX/67d;

    move-result-object v0

    goto :goto_0

    .line 1112388
    :pswitch_4
    iget-object v0, p0, LX/6aM;->c:Lcom/facebook/android/maps/model/LatLng;

    iget v1, p0, LX/6aM;->f:F

    invoke-static {v0, v1}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/67d;

    move-result-object v0

    goto :goto_0

    .line 1112389
    :pswitch_5
    iget v0, p0, LX/6aM;->h:F

    iget v1, p0, LX/6aM;->i:F

    .line 1112390
    new-instance v2, LX/67d;

    invoke-direct {v2}, LX/67d;-><init>()V

    .line 1112391
    iput v0, v2, LX/67d;->f:F

    .line 1112392
    iput v1, v2, LX/67d;->g:F

    .line 1112393
    move-object v0, v2

    .line 1112394
    goto :goto_0

    .line 1112395
    :pswitch_6
    iget v0, p0, LX/6aM;->g:F

    .line 1112396
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/67e;->a(FLandroid/graphics/Point;)LX/67d;

    move-result-object v1

    move-object v0, v1

    .line 1112397
    goto :goto_0

    .line 1112398
    :pswitch_7
    iget v0, p0, LX/6aM;->g:F

    iget-object v1, p0, LX/6aM;->e:Landroid/graphics/Point;

    invoke-static {v0, v1}, LX/67e;->a(FLandroid/graphics/Point;)LX/67d;

    move-result-object v0

    goto :goto_0

    .line 1112399
    :pswitch_8
    invoke-static {}, LX/67e;->a()LX/67d;

    move-result-object v0

    goto :goto_0

    .line 1112400
    :pswitch_9
    invoke-static {}, LX/67e;->b()LX/67d;

    move-result-object v0

    goto :goto_0

    .line 1112401
    :pswitch_a
    iget v0, p0, LX/6aM;->f:F

    invoke-static {v0}, LX/67e;->c(F)LX/67d;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final b()LX/7aQ;
    .locals 4

    .prologue
    .line 1112361
    iget v0, p0, LX/6aM;->a:I

    packed-switch v0, :pswitch_data_0

    .line 1112362
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1112363
    :pswitch_0
    iget-object v0, p0, LX/6aM;->b:LX/692;

    invoke-static {v0}, LX/6as;->a(LX/692;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-static {v0}, LX/7aR;->a(Lcom/google/android/gms/maps/model/CameraPosition;)LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112364
    :pswitch_1
    iget-object v0, p0, LX/6aM;->c:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v0}, LX/6as;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0}, LX/7aR;->a(Lcom/google/android/gms/maps/model/LatLng;)LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112365
    :pswitch_2
    iget-object v0, p0, LX/6aM;->d:LX/697;

    invoke-static {v0}, LX/6as;->a(LX/697;)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    iget v1, p0, LX/6aM;->l:I

    invoke-static {v0, v1}, LX/7aR;->a(Lcom/google/android/gms/maps/model/LatLngBounds;I)LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112366
    :pswitch_3
    iget-object v0, p0, LX/6aM;->d:LX/697;

    invoke-static {v0}, LX/6as;->a(LX/697;)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    iget v1, p0, LX/6aM;->j:I

    iget v2, p0, LX/6aM;->k:I

    iget v3, p0, LX/6aM;->l:I

    invoke-static {v0, v1, v2, v3}, LX/7aR;->a(Lcom/google/android/gms/maps/model/LatLngBounds;III)LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112367
    :pswitch_4
    iget-object v0, p0, LX/6aM;->c:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v0}, LX/6as;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iget v1, p0, LX/6aM;->f:F

    invoke-static {v0, v1}, LX/7aR;->a(Lcom/google/android/gms/maps/model/LatLng;F)LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112368
    :pswitch_5
    iget v0, p0, LX/6aM;->h:F

    iget v1, p0, LX/6aM;->i:F

    invoke-static {v0, v1}, LX/7aR;->a(FF)LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112369
    :pswitch_6
    iget v0, p0, LX/6aM;->g:F

    invoke-static {v0}, LX/7aR;->b(F)LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112370
    :pswitch_7
    iget v0, p0, LX/6aM;->g:F

    iget-object v1, p0, LX/6aM;->e:Landroid/graphics/Point;

    invoke-static {v0, v1}, LX/7aR;->a(FLandroid/graphics/Point;)LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112371
    :pswitch_8
    invoke-static {}, LX/7aR;->a()LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112372
    :pswitch_9
    invoke-static {}, LX/7aR;->b()LX/7aQ;

    move-result-object v0

    goto :goto_0

    .line 1112373
    :pswitch_a
    iget v0, p0, LX/6aM;->f:F

    invoke-static {v0}, LX/7aR;->a(F)LX/7aQ;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
