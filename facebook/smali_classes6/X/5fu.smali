.class public final enum LX/5fu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5fu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5fu;

.field public static final enum DEFAULT:LX/5fu;

.field public static final enum NO_CURRENCY_SYMBOL:LX/5fu;

.field public static final enum NO_CURRENCY_SYMBOL_NOR_EMPTY_DECIMALS:LX/5fu;

.field public static final enum NO_EMPTY_DECIMALS:LX/5fu;


# instance fields
.field private final mHasCurrencySymbol:Z

.field private final mHasEmptyDecimals:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 972441
    new-instance v0, LX/5fu;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2, v3, v3}, LX/5fu;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/5fu;->DEFAULT:LX/5fu;

    .line 972442
    new-instance v0, LX/5fu;

    const-string v1, "NO_CURRENCY_SYMBOL"

    invoke-direct {v0, v1, v3, v2, v3}, LX/5fu;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/5fu;->NO_CURRENCY_SYMBOL:LX/5fu;

    .line 972443
    new-instance v0, LX/5fu;

    const-string v1, "NO_EMPTY_DECIMALS"

    invoke-direct {v0, v1, v4, v3, v2}, LX/5fu;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    .line 972444
    new-instance v0, LX/5fu;

    const-string v1, "NO_CURRENCY_SYMBOL_NOR_EMPTY_DECIMALS"

    invoke-direct {v0, v1, v5, v2, v2}, LX/5fu;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, LX/5fu;->NO_CURRENCY_SYMBOL_NOR_EMPTY_DECIMALS:LX/5fu;

    .line 972445
    const/4 v0, 0x4

    new-array v0, v0, [LX/5fu;

    sget-object v1, LX/5fu;->DEFAULT:LX/5fu;

    aput-object v1, v0, v2

    sget-object v1, LX/5fu;->NO_CURRENCY_SYMBOL:LX/5fu;

    aput-object v1, v0, v3

    sget-object v1, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    aput-object v1, v0, v4

    sget-object v1, LX/5fu;->NO_CURRENCY_SYMBOL_NOR_EMPTY_DECIMALS:LX/5fu;

    aput-object v1, v0, v5

    sput-object v0, LX/5fu;->$VALUES:[LX/5fu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 972437
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 972438
    iput-boolean p3, p0, LX/5fu;->mHasCurrencySymbol:Z

    .line 972439
    iput-boolean p4, p0, LX/5fu;->mHasEmptyDecimals:Z

    .line 972440
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5fu;
    .locals 1

    .prologue
    .line 972436
    const-class v0, LX/5fu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5fu;

    return-object v0
.end method

.method public static values()[LX/5fu;
    .locals 1

    .prologue
    .line 972435
    sget-object v0, LX/5fu;->$VALUES:[LX/5fu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5fu;

    return-object v0
.end method


# virtual methods
.method public final hasCurrencySymbol()Z
    .locals 1

    .prologue
    .line 972433
    iget-boolean v0, p0, LX/5fu;->mHasCurrencySymbol:Z

    return v0
.end method

.method public final hasEmptyDecimals()Z
    .locals 1

    .prologue
    .line 972434
    iget-boolean v0, p0, LX/5fu;->mHasEmptyDecimals:Z

    return v0
.end method
