.class public final LX/5Fa;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 885708
    const/4 v13, 0x0

    .line 885709
    const/4 v12, 0x0

    .line 885710
    const/4 v11, 0x0

    .line 885711
    const/4 v10, 0x0

    .line 885712
    const-wide/16 v8, 0x0

    .line 885713
    const/4 v7, 0x0

    .line 885714
    const/4 v6, 0x0

    .line 885715
    const/4 v5, 0x0

    .line 885716
    const/4 v4, 0x0

    .line 885717
    const/4 v3, 0x0

    .line 885718
    const/4 v2, 0x0

    .line 885719
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 885720
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 885721
    const/4 v2, 0x0

    .line 885722
    :goto_0
    return v2

    .line 885723
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_a

    .line 885724
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 885725
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 885726
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 885727
    const-string v6, "actors"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 885728
    invoke-static/range {p0 .. p1}, LX/5FV;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 885729
    :cond_1
    const-string v6, "attached_story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 885730
    invoke-static/range {p0 .. p1}, LX/5FW;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 885731
    :cond_2
    const-string v6, "attachments"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 885732
    invoke-static/range {p0 .. p1}, LX/5FZ;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 885733
    :cond_3
    const-string v6, "cache_id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 885734
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 885735
    :cond_4
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 885736
    const/4 v2, 0x1

    .line 885737
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 885738
    :cond_5
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 885739
    invoke-static/range {p0 .. p1}, LX/5Av;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 885740
    :cond_6
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 885741
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 885742
    :cond_7
    const-string v6, "message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 885743
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 885744
    :cond_8
    const-string v6, "substory_count"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 885745
    const/4 v2, 0x1

    .line 885746
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v9, v6

    goto/16 :goto_1

    .line 885747
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 885748
    :cond_a
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 885749
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 885750
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 885751
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 885752
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 885753
    if-eqz v3, :cond_b

    .line 885754
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 885755
    :cond_b
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 885756
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 885757
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 885758
    if-eqz v8, :cond_c

    .line 885759
    const/16 v2, 0x8

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 885760
    :cond_c
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v14, v12

    move v15, v13

    move v12, v7

    move v13, v11

    move v7, v10

    move v11, v6

    move v10, v5

    move-wide/from16 v17, v8

    move v8, v2

    move v9, v4

    move-wide/from16 v4, v17

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 885761
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885762
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 885763
    if-eqz v0, :cond_1

    .line 885764
    const-string v1, "actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885765
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 885766
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 885767
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5FV;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 885768
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 885769
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 885770
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885771
    if-eqz v0, :cond_2

    .line 885772
    const-string v1, "attached_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885773
    invoke-static {p0, v0, p2}, LX/5FW;->a(LX/15i;ILX/0nX;)V

    .line 885774
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885775
    if-eqz v0, :cond_4

    .line 885776
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885777
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 885778
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 885779
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5FZ;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 885780
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 885781
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 885782
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885783
    if-eqz v0, :cond_5

    .line 885784
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885785
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885786
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 885787
    cmp-long v2, v0, v4

    if-eqz v2, :cond_6

    .line 885788
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885789
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 885790
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885791
    if-eqz v0, :cond_7

    .line 885792
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885793
    invoke-static {p0, v0, p2, p3}, LX/5Av;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 885794
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885795
    if-eqz v0, :cond_8

    .line 885796
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885797
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885798
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885799
    if-eqz v0, :cond_9

    .line 885800
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885801
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 885802
    :cond_9
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 885803
    if-eqz v0, :cond_a

    .line 885804
    const-string v1, "substory_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885805
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 885806
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885807
    return-void
.end method
