.class public LX/5Jd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation


# static fields
.field public static final a:I

.field public static final b:Landroid/graphics/Typeface;

.field public static final c:Landroid/text/Layout$Alignment;

.field private static final d:[Landroid/text/Layout$Alignment;

.field private static final e:[Landroid/text/TextUtils$TruncateAt;

.field private static final f:Landroid/graphics/Typeface;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 897331
    invoke-static {}, Landroid/text/Layout$Alignment;->values()[Landroid/text/Layout$Alignment;

    move-result-object v0

    sput-object v0, LX/5Jd;->d:[Landroid/text/Layout$Alignment;

    .line 897332
    invoke-static {}, Landroid/text/TextUtils$TruncateAt;->values()[Landroid/text/TextUtils$TruncateAt;

    move-result-object v0

    sput-object v0, LX/5Jd;->e:[Landroid/text/TextUtils$TruncateAt;

    .line 897333
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 897334
    sput-object v0, LX/5Jd;->f:Landroid/graphics/Typeface;

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    sput v0, LX/5Jd;->a:I

    .line 897335
    sget-object v0, LX/5Jd;->f:Landroid/graphics/Typeface;

    sput-object v0, LX/5Jd;->b:Landroid/graphics/Typeface;

    .line 897336
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sput-object v0, LX/5Jd;->c:Landroid/text/Layout$Alignment;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 897343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 897344
    return-void
.end method

.method public static a(LX/1De;IILX/1no;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;IZ)V
    .locals 27
    .param p4    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p6    # Landroid/text/TextUtils$TruncateAt;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p10    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p11    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p12    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p14    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p15    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p16    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p17    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p18    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p19    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p20    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p21    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p22    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p23    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p24    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p25    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p26    # Landroid/text/Layout$Alignment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p27    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 897337
    new-instance v1, Landroid/widget/EditText;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p9

    move/from16 v8, p10

    move/from16 v9, p11

    move/from16 v10, p12

    move/from16 v11, p13

    move/from16 v12, p14

    move/from16 v13, p15

    move-object/from16 v14, p16

    move/from16 v15, p17

    move-object/from16 v16, p18

    move/from16 v17, p19

    move/from16 v18, p20

    move/from16 v19, p21

    move/from16 v20, p22

    move/from16 v21, p23

    move/from16 v22, p24

    move-object/from16 v23, p25

    move-object/from16 v24, p26

    move/from16 v25, p27

    move/from16 v26, p28

    .line 897338
    invoke-static/range {v1 .. v26}, LX/5Jd;->a(Landroid/widget/EditText;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;IZ)V

    .line 897339
    invoke-static/range {p1 .. p1}, LX/1oC;->a(I)I

    move-result v2

    invoke-static/range {p2 .. p2}, LX/1oC;->a(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/EditText;->measure(II)V

    .line 897340
    invoke-virtual {v1}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v2

    move-object/from16 v0, p3

    iput v2, v0, LX/1no;->a:I

    .line 897341
    invoke-virtual {v1}, Landroid/widget/EditText;->getMeasuredHeight()I

    move-result v1

    move-object/from16 v0, p3

    iput v1, v0, LX/1no;->b:I

    .line 897342
    return-void
.end method

.method public static a(LX/1De;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1np",
            "<",
            "Landroid/text/TextUtils$TruncateAt;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/content/res/ColorStateList;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Landroid/text/Layout$Alignment;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 897345
    sget-object v1, LX/03r;->Text:[I

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/1De;->a([II)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 897346
    const/4 v1, 0x0

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_11

    .line 897347
    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v4

    .line 897348
    const/16 v5, 0x7

    if-ne v4, v5, :cond_1

    .line 897349
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p6, v4}, LX/1np;->a(Ljava/lang/Object;)V

    .line 897350
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 897351
    :cond_1
    const/16 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 897352
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {p7, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 897353
    :cond_2
    const/16 v5, 0x0

    if-ne v4, v5, :cond_3

    .line 897354
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p10

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 897355
    :cond_3
    const/16 v5, 0x5

    if-ne v4, v5, :cond_4

    .line 897356
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    .line 897357
    if-lez v4, :cond_0

    .line 897358
    sget-object v5, LX/5Jd;->e:[Landroid/text/TextUtils$TruncateAt;

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v5, v4

    invoke-virtual {p1, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 897359
    :cond_4
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_5

    const/16 v5, 0x11

    if-ne v4, v5, :cond_5

    .line 897360
    sget-object v5, LX/5Jd;->d:[Landroid/text/Layout$Alignment;

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    aget-object v4, v5, v4

    move-object/from16 v0, p11

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 897361
    :cond_5
    const/16 v5, 0x9

    if-ne v4, v5, :cond_6

    .line 897362
    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 897363
    :cond_6
    const/16 v5, 0x8

    if-ne v4, v5, :cond_7

    .line 897364
    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p4, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 897365
    :cond_7
    const/16 v5, 0xa

    if-ne v4, v5, :cond_8

    .line 897366
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p5, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897367
    :cond_8
    const/16 v5, 0x4

    if-ne v4, v5, :cond_9

    .line 897368
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p8, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897369
    :cond_9
    const/16 v5, 0x3

    if-ne v4, v5, :cond_a

    .line 897370
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897371
    :cond_a
    const/16 v5, 0x1

    if-ne v4, v5, :cond_b

    .line 897372
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p12

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897373
    :cond_b
    const/16 v5, 0x10

    if-ne v4, v5, :cond_c

    .line 897374
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {p2, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897375
    :cond_c
    const/16 v5, 0xd

    if-ne v4, v5, :cond_d

    .line 897376
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p14

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897377
    :cond_d
    const/16 v5, 0xe

    if-ne v4, v5, :cond_e

    .line 897378
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p15

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897379
    :cond_e
    const/16 v5, 0xf

    if-ne v4, v5, :cond_f

    .line 897380
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p13

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897381
    :cond_f
    const/16 v5, 0xc

    if-ne v4, v5, :cond_10

    .line 897382
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p16

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897383
    :cond_10
    const/16 v5, 0x6

    if-ne v4, v5, :cond_0

    .line 897384
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p17

    invoke-virtual {v0, v4}, LX/1np;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 897385
    :cond_11
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 897386
    return-void
.end method

.method public static a(LX/1De;LX/5Jc;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;IZ)V
    .locals 1
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Landroid/text/TextUtils$TruncateAt;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p8    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p9    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p10    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p12    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->BOOL:LX/32B;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p14    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p15    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p16    # Landroid/content/res/ColorStateList;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p17    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p18    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p19    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_TEXT:LX/32B;
        .end annotation
    .end param
    .param p20    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_OFFSET:LX/32B;
        .end annotation
    .end param
    .param p21    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->FLOAT:LX/32B;
        .end annotation
    .end param
    .param p22    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p23    # Landroid/graphics/Typeface;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p24    # Landroid/text/Layout$Alignment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p25    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p26    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 897292
    invoke-static {p0}, LX/5JZ;->c(LX/1De;)LX/1dQ;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/5Jc;->a(LX/1dQ;)V

    .line 897293
    invoke-static/range {p1 .. p26}, LX/5Jd;->a(Landroid/widget/EditText;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;IZ)V

    .line 897294
    return-void
.end method

.method private static a(Landroid/widget/EditText;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextUtils$TruncateAt;IIIFFFIZILandroid/content/res/ColorStateList;ILandroid/content/res/ColorStateList;IIIFFILandroid/graphics/Typeface;Landroid/text/Layout$Alignment;IZ)V
    .locals 5

    .prologue
    .line 897295
    invoke-virtual {p0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 897296
    invoke-virtual {p0, p2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 897297
    invoke-virtual {p0, p3}, Landroid/widget/EditText;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 897298
    invoke-virtual {p0, p4}, Landroid/widget/EditText;->setMinLines(I)V

    .line 897299
    invoke-virtual {p0, p5}, Landroid/widget/EditText;->setMaxLines(I)V

    .line 897300
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v4, p6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 897301
    invoke-virtual {p0, p7, p8, p9, p10}, Landroid/widget/EditText;->setShadowLayer(FFFI)V

    .line 897302
    move/from16 v0, p11

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setSingleLine(Z)V

    .line 897303
    move/from16 v0, p16

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setLinkTextColor(I)V

    .line 897304
    move/from16 v0, p17

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setHighlightColor(I)V

    .line 897305
    move/from16 v0, p18

    int-to-float v2, v0

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setTextSize(F)V

    .line 897306
    move/from16 v0, p19

    move/from16 v1, p20

    invoke-virtual {p0, v0, v1}, Landroid/widget/EditText;->setLineSpacing(FF)V

    .line 897307
    move-object/from16 v0, p22

    move/from16 v1, p21

    invoke-virtual {p0, v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 897308
    move/from16 v0, p24

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setGravity(I)V

    .line 897309
    move/from16 v0, p25

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 897310
    move/from16 v0, p25

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 897311
    move/from16 v0, p25

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setClickable(Z)V

    .line 897312
    move/from16 v0, p25

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setLongClickable(Z)V

    .line 897313
    move/from16 v0, p25

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 897314
    if-eqz p13, :cond_0

    .line 897315
    move-object/from16 v0, p13

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 897316
    :goto_0
    if-eqz p15, :cond_1

    .line 897317
    move-object/from16 v0, p15

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 897318
    :goto_1
    sget-object v2, LX/5Ja;->a:[I

    invoke-virtual/range {p23 .. p23}, Landroid/text/Layout$Alignment;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 897319
    :goto_2
    return-void

    .line 897320
    :cond_0
    move/from16 v0, p12

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_0

    .line 897321
    :cond_1
    move/from16 v0, p14

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setHintTextColor(I)V

    goto :goto_1

    .line 897322
    :pswitch_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_2

    .line 897323
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setTextAlignment(I)V

    goto :goto_2

    .line 897324
    :cond_2
    or-int/lit8 v2, p24, 0x3

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setGravity(I)V

    goto :goto_2

    .line 897325
    :pswitch_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_3

    .line 897326
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setTextAlignment(I)V

    goto :goto_2

    .line 897327
    :cond_3
    or-int/lit8 v2, p24, 0x5

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setGravity(I)V

    goto :goto_2

    .line 897328
    :pswitch_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_4

    .line 897329
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setTextAlignment(I)V

    goto :goto_2

    .line 897330
    :cond_4
    or-int/lit8 v2, p24, 0x1

    invoke-virtual {p0, v2}, Landroid/widget/EditText;->setGravity(I)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
