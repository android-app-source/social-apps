.class public LX/5qC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/5qA;

.field private b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<+",
            "LX/5p4;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/5p4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>(Ljava/lang/Class;LX/5qQ;LX/0Or;)V
    .locals 1
    .param p2    # LX/5qQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/5p4;",
            ">;",
            "LX/5qQ;",
            "LX/0Or",
            "<+",
            "LX/5p4;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1009009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1009010
    if-nez p2, :cond_0

    new-instance p2, LX/5qB;

    invoke-direct {p2, p0, p1}, LX/5qB;-><init>(LX/5qC;Ljava/lang/Class;)V

    :cond_0
    iput-object p2, p0, LX/5qC;->a:LX/5qA;

    .line 1009011
    iput-object p3, p0, LX/5qC;->b:LX/0Or;

    .line 1009012
    iget-object v0, p0, LX/5qC;->a:LX/5qA;

    invoke-interface {v0}, LX/5qA;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1009013
    invoke-direct {p0}, LX/5qC;->e()LX/5p4;

    move-result-object v0

    iput-object v0, p0, LX/5qC;->c:LX/5p4;

    .line 1009014
    :cond_1
    return-void
.end method

.method private a(LX/5p4;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x2000

    .line 1009045
    const-string v0, "initialize"

    invoke-static {v4, v5, v0}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v0

    .line 1009046
    instance-of v1, p1, Lcom/facebook/react/cxxbridge/CxxModuleWrapper;

    if-eqz v1, :cond_0

    .line 1009047
    const-string v1, "className"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    .line 1009048
    :goto_0
    invoke-virtual {v0}, LX/0BN;->a()V

    .line 1009049
    invoke-static {p1}, LX/5qC;->b(LX/5p4;)V

    .line 1009050
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1009051
    return-void

    .line 1009052
    :cond_0
    const-string v1, "name"

    iget-object v2, p0, LX/5qC;->a:LX/5qA;

    invoke-interface {v2}, LX/5qA;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    goto :goto_0
.end method

.method private static b(LX/5p4;)V
    .locals 2

    .prologue
    .line 1009053
    invoke-static {}, LX/5qG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1009054
    invoke-interface {p0}, LX/5p4;->d()V

    .line 1009055
    :goto_0
    return-void

    .line 1009056
    :cond_0
    new-instance v0, LX/5pw;

    invoke-direct {v0}, LX/5pw;-><init>()V

    .line 1009057
    new-instance v1, Lcom/facebook/react/cxxbridge/ModuleHolder$1;

    invoke-direct {v1, p0, v0}, Lcom/facebook/react/cxxbridge/ModuleHolder$1;-><init>(LX/5p4;LX/5pw;)V

    invoke-static {v1}, LX/5qG;->a(Ljava/lang/Runnable;)V

    .line 1009058
    const v1, 0x79336d51

    :try_start_0
    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1009059
    :catch_0
    move-exception v0

    .line 1009060
    :goto_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1009061
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private e()LX/5p4;
    .locals 2

    .prologue
    .line 1009028
    invoke-direct {p0}, LX/5qC;->f()LX/5p4;

    move-result-object v0

    .line 1009029
    const/4 v1, 0x0

    iput-object v1, p0, LX/5qC;->b:LX/0Or;

    .line 1009030
    return-object v0
.end method

.method private f()LX/5p4;
    .locals 6

    .prologue
    const-wide/16 v4, 0x2000

    .line 1009031
    iget-object v0, p0, LX/5qC;->a:LX/5qA;

    instance-of v1, v0, LX/5qB;

    .line 1009032
    if-eqz v1, :cond_3

    iget-object v0, p0, LX/5qC;->a:LX/5qA;

    check-cast v0, LX/5qB;

    iget-object v0, v0, LX/5qB;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 1009033
    :goto_0
    if-nez v1, :cond_0

    .line 1009034
    const-string v2, "CREATE_MODULE_START"

    invoke-static {v2}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1009035
    :cond_0
    const-string v2, "createModule"

    invoke-static {v4, v5, v2}, LX/0BL;->a(JLjava/lang/String;)LX/0BN;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v2, v3, v0}, LX/0BN;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0BN;

    move-result-object v0

    invoke-virtual {v0}, LX/0BN;->a()V

    .line 1009036
    iget-object v0, p0, LX/5qC;->b:LX/0Or;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5p4;

    .line 1009037
    iget-boolean v2, p0, LX/5qC;->d:Z

    if-eqz v2, :cond_1

    .line 1009038
    invoke-direct {p0, v0}, LX/5qC;->a(LX/5p4;)V

    .line 1009039
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/5qC;->d:Z

    .line 1009040
    :cond_1
    invoke-static {v4, v5}, LX/018;->a(J)V

    .line 1009041
    if-nez v1, :cond_2

    .line 1009042
    const-string v1, "CREATE_MODULE_END"

    invoke-static {v1}, Lcom/facebook/react/bridge/ReactMarker;->logMarker(Ljava/lang/String;)V

    .line 1009043
    :cond_2
    return-object v0

    .line 1009044
    :cond_3
    iget-object v0, p0, LX/5qC;->a:LX/5qA;

    invoke-interface {v0}, LX/5qA;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1009023
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/5qC;->c:LX/5p4;

    if-eqz v0, :cond_0

    .line 1009024
    iget-object v0, p0, LX/5qC;->c:LX/5p4;

    invoke-direct {p0, v0}, LX/5qC;->a(LX/5p4;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009025
    :goto_0
    monitor-exit p0

    return-void

    .line 1009026
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/5qC;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1009027
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 1009019
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/5qC;->c:LX/5p4;

    if-eqz v0, :cond_0

    .line 1009020
    iget-object v0, p0, LX/5qC;->c:LX/5p4;

    invoke-interface {v0}, LX/5p4;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009021
    :cond_0
    monitor-exit p0

    return-void

    .line 1009022
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()LX/5p4;
    .locals 1

    .prologue
    .line 1009015
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/5qC;->c:LX/5p4;

    if-nez v0, :cond_0

    .line 1009016
    invoke-direct {p0}, LX/5qC;->e()LX/5p4;

    move-result-object v0

    iput-object v0, p0, LX/5qC;->c:LX/5p4;

    .line 1009017
    :cond_0
    iget-object v0, p0, LX/5qC;->c:LX/5p4;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1009018
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
