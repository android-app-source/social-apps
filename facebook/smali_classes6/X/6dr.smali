.class public final LX/6dr;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0sv;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x1

    .line 1116484
    new-instance v0, LX/0su;

    sget-object v1, LX/6dq;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/6dr;->a:LX/0sv;

    .line 1116485
    sget-object v0, LX/6dq;->a:LX/0U1;

    sget-object v1, LX/6dq;->b:LX/0U1;

    sget-object v2, LX/6dq;->c:LX/0U1;

    sget-object v3, LX/6dq;->d:LX/0U1;

    sget-object v4, LX/6dq;->e:LX/0U1;

    sget-object v5, LX/6dq;->f:LX/0U1;

    sget-object v6, LX/6dq;->g:LX/0U1;

    sget-object v7, LX/6dq;->h:LX/0U1;

    sget-object v8, LX/6dq;->i:LX/0U1;

    sget-object v9, LX/6dq;->j:LX/0U1;

    sget-object v10, LX/6dq;->k:LX/0U1;

    sget-object v11, LX/6dq;->l:LX/0U1;

    const/16 v12, 0x25

    new-array v12, v12, [LX/0U1;

    const/4 v13, 0x0

    sget-object v14, LX/6dq;->m:LX/0U1;

    aput-object v14, v12, v13

    sget-object v13, LX/6dq;->n:LX/0U1;

    aput-object v13, v12, v15

    const/4 v13, 0x2

    sget-object v14, LX/6dq;->o:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x3

    sget-object v14, LX/6dq;->p:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x4

    sget-object v14, LX/6dq;->q:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x5

    sget-object v14, LX/6dq;->r:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x6

    sget-object v14, LX/6dq;->s:LX/0U1;

    aput-object v14, v12, v13

    const/4 v13, 0x7

    sget-object v14, LX/6dq;->t:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x8

    sget-object v14, LX/6dq;->u:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x9

    sget-object v14, LX/6dq;->v:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xa

    sget-object v14, LX/6dq;->w:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xb

    sget-object v14, LX/6dq;->x:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xc

    sget-object v14, LX/6dq;->y:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xd

    sget-object v14, LX/6dq;->z:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xe

    sget-object v14, LX/6dq;->A:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0xf

    sget-object v14, LX/6dq;->B:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x10

    sget-object v14, LX/6dq;->C:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x11

    sget-object v14, LX/6dq;->D:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x12

    sget-object v14, LX/6dq;->E:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x13

    sget-object v14, LX/6dq;->F:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x14

    sget-object v14, LX/6dq;->G:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x15

    sget-object v14, LX/6dq;->H:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x16

    sget-object v14, LX/6dq;->I:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x17

    sget-object v14, LX/6dq;->J:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x18

    sget-object v14, LX/6dq;->K:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x19

    sget-object v14, LX/6dq;->L:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x1a

    sget-object v14, LX/6dq;->M:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x1b

    sget-object v14, LX/6dq;->N:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x1c

    sget-object v14, LX/6dq;->O:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x1d

    sget-object v14, LX/6dq;->P:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x1e

    sget-object v14, LX/6dq;->Q:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x1f

    sget-object v14, LX/6dq;->R:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x20

    sget-object v14, LX/6dq;->S:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x21

    sget-object v14, LX/6dq;->T:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x22

    sget-object v14, LX/6dq;->U:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x23

    sget-object v14, LX/6dq;->V:LX/0U1;

    aput-object v14, v12, v13

    const/16 v13, 0x24

    sget-object v14, LX/6dq;->W:LX/0U1;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/6dr;->b:LX/0Px;

    .line 1116486
    const-string v0, "threads"

    const-string v1, "threads_legacy_thread_id_index"

    sget-object v2, LX/6dq;->b:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6dr;->c:Ljava/lang/String;

    .line 1116487
    const-string v0, "threads"

    const-string v1, "threads_montage_thread_key_index"

    sget-object v2, LX/6dq;->W:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1116488
    invoke-static {v2}, LX/0Tz;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    .line 1116489
    invoke-static {v0, v1, v3, v15}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1116490
    sput-object v0, LX/6dr;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1116482
    const-string v0, "threads"

    sget-object v1, LX/6dr;->b:LX/0Px;

    sget-object v2, LX/6dr;->a:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 1116483
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 1116477
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1116478
    sget-object v0, LX/6dr;->c:Ljava/lang/String;

    const v1, 0x2ea2eca

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0xe80e8ea

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116479
    sget-object v0, LX/6dr;->d:Ljava/lang/String;

    const v1, 0x47ee74d1

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x2cbace17

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1116480
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 1116481
    return-void
.end method
