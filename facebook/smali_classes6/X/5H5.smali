.class public final LX/5H5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 889554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;)V
    .locals 1

    .prologue
    .line 889535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889536
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->b:Ljava/lang/String;

    iput-object v0, p0, LX/5H5;->a:Ljava/lang/String;

    .line 889537
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->a:Ljava/lang/String;

    iput-object v0, p0, LX/5H5;->b:Ljava/lang/String;

    .line 889538
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->c:Ljava/lang/String;

    iput-object v0, p0, LX/5H5;->c:Ljava/lang/String;

    .line 889539
    iget-boolean v0, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->e:Z

    iput-boolean v0, p0, LX/5H5;->d:Z

    .line 889540
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->d:Ljava/lang/String;

    iput-object v0, p0, LX/5H5;->e:Ljava/lang/String;

    .line 889541
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;->f:Ljava/lang/String;

    iput-object v0, p0, LX/5H5;->f:Ljava/lang/String;

    .line 889542
    return-void
.end method


# virtual methods
.method public final a(Z)LX/5H5;
    .locals 0

    .prologue
    .line 889543
    iput-boolean p1, p0, LX/5H5;->d:Z

    .line 889544
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/5H5;
    .locals 0

    .prologue
    .line 889545
    iput-object p1, p0, LX/5H5;->a:Ljava/lang/String;

    .line 889546
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/5H5;
    .locals 0

    .prologue
    .line 889547
    iput-object p1, p0, LX/5H5;->b:Ljava/lang/String;

    .line 889548
    return-object p0
.end method

.method public final d(Ljava/lang/String;)LX/5H5;
    .locals 0

    .prologue
    .line 889549
    iput-object p1, p0, LX/5H5;->c:Ljava/lang/String;

    .line 889550
    return-object p0
.end method

.method public final e(Ljava/lang/String;)LX/5H5;
    .locals 0

    .prologue
    .line 889551
    iput-object p1, p0, LX/5H5;->f:Ljava/lang/String;

    .line 889552
    return-object p0
.end method

.method public final g()Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;
    .locals 1

    .prologue
    .line 889553
    new-instance v0, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

    invoke-direct {v0, p0}, Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;-><init>(LX/5H5;)V

    return-object v0
.end method
