.class public final LX/5Fp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 886296
    const/4 v10, 0x0

    .line 886297
    const/4 v9, 0x0

    .line 886298
    const/4 v8, 0x0

    .line 886299
    const/4 v7, 0x0

    .line 886300
    const/4 v6, 0x0

    .line 886301
    const/4 v5, 0x0

    .line 886302
    const/4 v4, 0x0

    .line 886303
    const/4 v3, 0x0

    .line 886304
    const/4 v2, 0x0

    .line 886305
    const/4 v1, 0x0

    .line 886306
    const/4 v0, 0x0

    .line 886307
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 886308
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 886309
    const/4 v0, 0x0

    .line 886310
    :goto_0
    return v0

    .line 886311
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 886312
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 886313
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 886314
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 886315
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 886316
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 886317
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 886318
    :cond_3
    const-string v12, "android_urls"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 886319
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 886320
    :cond_4
    const-string v12, "application"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 886321
    invoke-static {p0, p1}, LX/40y;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 886322
    :cond_5
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 886323
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 886324
    :cond_6
    const-string v12, "is_music_item"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 886325
    const/4 v0, 0x1

    .line 886326
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 886327
    :cond_7
    const-string v12, "map_points"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 886328
    invoke-static {p0, p1}, LX/4aX;->b(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 886329
    :cond_8
    const-string v12, "music_object"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 886330
    invoke-static {p0, p1}, LX/5EY;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 886331
    :cond_9
    const-string v12, "music_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 886332
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLMusicType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMusicType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 886333
    :cond_a
    const-string v12, "musicians"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 886334
    invoke-static {p0, p1}, LX/5Fq;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 886335
    :cond_b
    const-string v12, "preview_urls"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 886336
    invoke-static {p0, p1}, LX/5Fr;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 886337
    :cond_c
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 886338
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 886339
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 886340
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 886341
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 886342
    if-eqz v0, :cond_d

    .line 886343
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 886344
    :cond_d
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 886345
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 886346
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 886347
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 886348
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 886349
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 886350
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 886351
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 886352
    if-eqz v0, :cond_0

    .line 886353
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886354
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 886355
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 886356
    if-eqz v0, :cond_1

    .line 886357
    const-string v0, "android_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886358
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 886359
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886360
    if-eqz v0, :cond_2

    .line 886361
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886362
    invoke-static {p0, v0, p2, p3}, LX/40y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 886363
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 886364
    if-eqz v0, :cond_3

    .line 886365
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886366
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 886367
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 886368
    if-eqz v0, :cond_4

    .line 886369
    const-string v1, "is_music_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886370
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 886371
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886372
    if-eqz v0, :cond_5

    .line 886373
    const-string v1, "map_points"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886374
    invoke-static {p0, v0, p2, p3}, LX/4aX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 886375
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886376
    if-eqz v0, :cond_6

    .line 886377
    const-string v1, "music_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886378
    invoke-static {p0, v0, p2, p3}, LX/5EY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 886379
    :cond_6
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 886380
    if-eqz v0, :cond_7

    .line 886381
    const-string v0, "music_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886382
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 886383
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886384
    if-eqz v0, :cond_8

    .line 886385
    const-string v1, "musicians"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886386
    invoke-static {p0, v0, p2, p3}, LX/5Fq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 886387
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 886388
    if-eqz v0, :cond_9

    .line 886389
    const-string v1, "preview_urls"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 886390
    invoke-static {p0, v0, p2, p3}, LX/5Fr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 886391
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 886392
    return-void
.end method
