.class public final LX/5TE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 921407
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 921408
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 921409
    :goto_0
    return v1

    .line 921410
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 921411
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 921412
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 921413
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 921414
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 921415
    const-string v4, "product_items_edge"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 921416
    const/4 v3, 0x0

    .line 921417
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 921418
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 921419
    :goto_2
    move v2, v3

    .line 921420
    goto :goto_1

    .line 921421
    :cond_2
    const-string v4, "variant_labels"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 921422
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 921423
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 921424
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 921425
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 921426
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 921427
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 921428
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 921429
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 921430
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 921431
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 921432
    const-string v5, "nodes"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 921433
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 921434
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 921435
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 921436
    invoke-static {p0, p1}, LX/5TD;->b(LX/15w;LX/186;)I

    move-result v4

    .line 921437
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 921438
    :cond_7
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 921439
    goto :goto_3

    .line 921440
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 921441
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 921442
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v2, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 921443
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 921444
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 921445
    if-eqz v0, :cond_2

    .line 921446
    const-string v1, "product_items_edge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921447
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 921448
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 921449
    if-eqz v1, :cond_1

    .line 921450
    const-string v3, "nodes"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921451
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 921452
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 921453
    invoke-virtual {p0, v1, v3}, LX/15i;->q(II)I

    move-result v0

    invoke-static {p0, v0, p2, p3}, LX/5TD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 921454
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 921455
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 921456
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 921457
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 921458
    if-eqz v0, :cond_3

    .line 921459
    const-string v0, "variant_labels"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 921460
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 921461
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 921462
    return-void
.end method
