.class public LX/6Eu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Dk;


# instance fields
.field private final a:LX/6rC;

.field private final b:LX/6F6;


# direct methods
.method public constructor <init>(LX/6rC;LX/6F6;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1066756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066757
    iput-object p1, p0, LX/6Eu;->a:LX/6rC;

    .line 1066758
    iput-object p2, p0, LX/6Eu;->b:LX/6F6;

    .line 1066759
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1066750
    iget-object v0, p0, LX/6Eu;->b:LX/6F6;

    const/4 p0, 0x0

    .line 1066751
    iput-object p0, v0, LX/6F6;->d:LX/6Ew;

    .line 1066752
    iput-object p0, v0, LX/6F6;->c:LX/6Ez;

    .line 1066753
    iget-object v1, v0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    const-string v2, "onCheckoutCancel"

    invoke-virtual {v1, v2}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1066754
    :goto_0
    return-void

    .line 1066755
    :cond_0
    iget-object v1, v0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    iget-object v2, v0, LX/6F6;->b:Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;

    invoke-virtual {v2}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v2

    const-string v3, "checkoutCancel"

    invoke-static {v2, v3, p0}, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->a(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(LX/6qn;)V
    .locals 1

    .prologue
    .line 1066746
    iget-object v0, p0, LX/6Eu;->a:LX/6rC;

    invoke-virtual {v0, p1}, LX/6rC;->a(LX/6qn;)V

    .line 1066747
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1066748
    iget-object v0, p0, LX/6Eu;->a:LX/6rC;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/6rC;->a(Lcom/facebook/payments/checkout/model/CheckoutData;IILandroid/content/Intent;)V

    .line 1066749
    return-void
.end method
