.class public abstract LX/68P;
.super LX/67m;
.source ""


# static fields
.field private static final x:[Ljava/lang/String;

.field public static final y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/68P;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field public final o:LX/68s;

.field public p:LX/68i;

.field public q:D

.field public r:I

.field public s:I

.field public t:I

.field public u:Z

.field public v:I

.field public final w:[I

.field public final z:LX/31i;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1055166
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, LX/68P;->x:[Ljava/lang/String;

    .line 1055167
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 1055168
    sget-object v1, LX/68P;->x:[Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1055169
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1055170
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, LX/68P;->y:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(LX/680;LX/68s;)V
    .locals 2

    .prologue
    .line 1055158
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1055159
    new-instance v0, LX/68i;

    invoke-direct {v0}, LX/68i;-><init>()V

    iput-object v0, p0, LX/68P;->p:LX/68i;

    .line 1055160
    const-wide v0, 0x3ff3333333333333L    # 1.2

    iput-wide v0, p0, LX/68P;->q:D

    .line 1055161
    new-instance v0, LX/31i;

    invoke-direct {v0}, LX/31i;-><init>()V

    iput-object v0, p0, LX/68P;->z:LX/31i;

    .line 1055162
    const/4 v0, -0x1

    iput v0, p0, LX/68P;->v:I

    .line 1055163
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/68P;->w:[I

    .line 1055164
    iput-object p2, p0, LX/68P;->o:LX/68s;

    .line 1055165
    return-void
.end method

.method private s()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1055146
    iget v0, p0, LX/68P;->v:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-boolean v0, p0, LX/67m;->i:Z

    if-nez v0, :cond_1

    .line 1055147
    :cond_0
    return-void

    .line 1055148
    :cond_1
    iget-object v0, p0, LX/68P;->o:LX/68s;

    iget v2, p0, LX/68P;->v:I

    .line 1055149
    iput v2, v0, LX/68s;->d:I

    .line 1055150
    iget v0, p0, LX/68P;->v:I

    shl-int v3, v7, v0

    move v2, v1

    .line 1055151
    :goto_0
    if-ge v2, v3, :cond_0

    move v0, v1

    .line 1055152
    :goto_1
    if-ge v0, v3, :cond_3

    .line 1055153
    iget-object v4, p0, LX/68P;->o:LX/68s;

    iget v5, p0, LX/68P;->v:I

    iget-object v6, p0, LX/68P;->p:LX/68i;

    invoke-virtual {v4, v2, v0, v5, v6}, LX/68s;->a(IIILX/68i;)V

    .line 1055154
    iget-object v4, p0, LX/68P;->p:LX/68i;

    iget-object v4, v4, LX/68i;->a:LX/69C;

    if-nez v4, :cond_2

    iget-object v4, p0, LX/68P;->p:LX/68i;

    iget v4, v4, LX/68i;->h:I

    if-eq v4, v7, :cond_2

    .line 1055155
    iget v4, p0, LX/68P;->v:I

    const/4 v5, 0x2

    invoke-virtual {p0, v2, v0, v4, v5}, LX/68P;->a(IIII)V

    .line 1055156
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1055157
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method private t()V
    .locals 4

    .prologue
    .line 1055138
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1055139
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1055140
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 1055141
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->postInvalidateOnAnimation()V

    .line 1055142
    :goto_0
    return-void

    .line 1055143
    :cond_0
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1055144
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 1055145
    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Lcom/facebook/android/maps/MapView;->postInvalidateDelayed(J)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IIII)V
    .locals 11

    .prologue
    .line 1055126
    iget v0, p0, LX/67m;->h:I

    iget v1, p0, LX/67m;->h:I

    invoke-static {v0, v1}, LX/69C;->a(II)LX/69C;

    move-result-object v6

    .line 1055127
    invoke-virtual {v6, p1, p2, p3}, LX/69C;->a(III)LX/69C;

    .line 1055128
    const/4 v0, 0x1

    iput v0, v6, LX/69C;->l:I

    .line 1055129
    iget-object v0, p0, LX/68P;->o:LX/68s;

    invoke-virtual {v0, v6}, LX/68s;->a(LX/69C;)V

    .line 1055130
    new-instance v0, Lcom/facebook/android/maps/TiledMapDrawable$1;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/android/maps/TiledMapDrawable$1;-><init>(LX/68P;IIIILX/69C;)V

    sget-object v1, LX/68P;->x:[Ljava/lang/String;

    aget-object v1, v1, p3

    .line 1055131
    sget v7, LX/31l;->d:I

    add-int/lit8 v8, v7, -0x1

    sput v8, LX/31l;->d:I

    int-to-long v7, v7

    const/16 v9, 0x20

    shl-long/2addr v7, v9

    .line 1055132
    iput-wide v7, v0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->a:J

    .line 1055133
    iput-object v1, v0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->c:Ljava/lang/String;

    .line 1055134
    const-wide/16 v7, 0x0

    .line 1055135
    iput-wide v7, v0, Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;->b:J

    .line 1055136
    sget-object v7, LX/31l;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v7, v0}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 1055137
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 27

    .prologue
    .line 1055044
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->e:LX/680;

    invoke-virtual {v2}, LX/680;->g()Lcom/facebook/android/maps/MapView;

    move-result-object v14

    .line 1055045
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/68P;->r:I

    .line 1055046
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, LX/68P;->s:I

    .line 1055047
    iget v2, v14, Lcom/facebook/android/maps/MapView;->g:I

    move-object/from16 v0, p0

    iget v3, v0, LX/68P;->A:I

    if-eq v2, v3, :cond_0

    .line 1055048
    sget-object v2, LX/68P;->x:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget v3, v0, LX/68P;->A:I

    aget-object v2, v2, v3

    invoke-static {v2}, LX/31l;->a(Ljava/lang/String;)V

    .line 1055049
    :cond_0
    iget v2, v14, Lcom/facebook/android/maps/MapView;->g:I

    move-object/from16 v0, p0

    iput v2, v0, LX/68P;->A:I

    .line 1055050
    goto :goto_1

    .line 1055051
    :cond_1
    :goto_0
    return-void

    .line 1055052
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/67m;->f:LX/31h;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/68P;->z:LX/31i;

    invoke-virtual {v2, v3}, LX/31h;->a(LX/31i;)V

    .line 1055053
    iget-wide v2, v14, Lcom/facebook/android/maps/MapView;->m:D

    .line 1055054
    move-object/from16 v0, p0

    iget-object v4, v0, LX/68P;->z:LX/31i;

    iget-wide v4, v4, LX/31i;->c:D

    cmpg-double v4, v2, v4

    if-gez v4, :cond_2

    .line 1055055
    iget-wide v4, v14, Lcom/facebook/android/maps/MapView;->o:D

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    add-double/2addr v2, v4

    .line 1055056
    :cond_2
    iget-wide v10, v14, Lcom/facebook/android/maps/MapView;->n:D

    .line 1055057
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->save(I)I

    .line 1055058
    iget v4, v14, Lcom/facebook/android/maps/MapView;->j:F

    iget v5, v14, Lcom/facebook/android/maps/MapView;->e:F

    iget v6, v14, Lcom/facebook/android/maps/MapView;->f:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1055059
    iget v4, v14, Lcom/facebook/android/maps/MapView;->h:F

    iget v5, v14, Lcom/facebook/android/maps/MapView;->h:F

    iget v6, v14, Lcom/facebook/android/maps/MapView;->e:F

    iget v7, v14, Lcom/facebook/android/maps/MapView;->f:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 1055060
    iget v4, v14, Lcom/facebook/android/maps/MapView;->s:I

    int-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/68P;->z:LX/31i;

    iget-wide v6, v6, LX/31i;->c:D

    mul-double/2addr v4, v6

    double-to-int v15, v4

    .line 1055061
    iget v4, v14, Lcom/facebook/android/maps/MapView;->s:I

    int-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/68P;->z:LX/31i;

    iget-wide v6, v6, LX/31i;->a:D

    mul-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v16, v0

    .line 1055062
    iget v4, v14, Lcom/facebook/android/maps/MapView;->s:I

    int-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/68P;->z:LX/31i;

    iget-wide v6, v6, LX/31i;->d:D

    mul-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v17, v0

    .line 1055063
    iget v4, v14, Lcom/facebook/android/maps/MapView;->s:I

    int-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, LX/68P;->z:LX/31i;

    iget-wide v6, v6, LX/31i;->b:D

    mul-double/2addr v4, v6

    double-to-int v0, v4

    move/from16 v18, v0

    .line 1055064
    move-object/from16 v0, p0

    iget v4, v0, LX/68P;->B:I

    if-ne v4, v15, :cond_3

    move-object/from16 v0, p0

    iget v4, v0, LX/68P;->C:I

    move/from16 v0, v16

    if-ne v4, v0, :cond_3

    move-object/from16 v0, p0

    iget v4, v0, LX/68P;->D:I

    move/from16 v0, v17

    if-ne v4, v0, :cond_3

    move-object/from16 v0, p0

    iget v4, v0, LX/68P;->E:I

    move/from16 v0, v18

    if-eq v4, v0, :cond_4

    .line 1055065
    :cond_3
    sget-object v4, LX/68P;->x:[Ljava/lang/String;

    iget v5, v14, Lcom/facebook/android/maps/MapView;->g:I

    aget-object v4, v4, v5

    invoke-static {v4}, LX/31l;->a(Ljava/lang/String;)V

    .line 1055066
    :cond_4
    move-object/from16 v0, p0

    iput v15, v0, LX/68P;->B:I

    .line 1055067
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, LX/68P;->C:I

    .line 1055068
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, LX/68P;->D:I

    .line 1055069
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, LX/68P;->E:I

    .line 1055070
    iget v4, v14, Lcom/facebook/android/maps/MapView;->s:I

    add-int/lit8 v19, v4, -0x1

    .line 1055071
    const/4 v9, 0x0

    .line 1055072
    const/4 v8, -0x1

    .line 1055073
    sub-int v4, v17, v15

    add-int/lit8 v5, v4, 0x1

    .line 1055074
    sub-int v4, v18, v16

    add-int/lit8 v6, v4, 0x1

    .line 1055075
    if-le v5, v6, :cond_9

    move v4, v5

    .line 1055076
    :goto_2
    mul-int v20, v4, v4

    .line 1055077
    add-int/lit8 v4, v5, -0x1

    shr-int/lit8 v4, v4, 0x1

    add-int v21, v15, v4

    .line 1055078
    add-int/lit8 v4, v6, -0x1

    shr-int/lit8 v4, v4, 0x1

    add-int v22, v16, v4

    .line 1055079
    iget-wide v4, v14, Lcom/facebook/android/maps/MapView;->r:J

    long-to-double v4, v4

    move/from16 v0, v21

    int-to-double v6, v0

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v6, v12

    iget v12, v14, Lcom/facebook/android/maps/MapView;->s:I

    int-to-double v12, v12

    div-double/2addr v6, v12

    sub-double v2, v6, v2

    mul-double/2addr v2, v4

    iget v4, v14, Lcom/facebook/android/maps/MapView;->e:F

    float-to-double v4, v4

    add-double/2addr v2, v4

    double-to-float v0, v2

    move/from16 v23, v0

    .line 1055080
    iget-wide v2, v14, Lcom/facebook/android/maps/MapView;->r:J

    long-to-double v2, v2

    move/from16 v0, v22

    int-to-double v4, v0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v4, v6

    iget v6, v14, Lcom/facebook/android/maps/MapView;->s:I

    int-to-double v6, v6

    div-double/2addr v4, v6

    sub-double/2addr v4, v10

    mul-double/2addr v2, v4

    iget v4, v14, Lcom/facebook/android/maps/MapView;->f:F

    float-to-double v4, v4

    add-double/2addr v2, v4

    double-to-float v0, v2

    move/from16 v24, v0

    .line 1055081
    const/4 v10, 0x0

    .line 1055082
    const/4 v4, 0x0

    .line 1055083
    const/4 v3, 0x0

    .line 1055084
    const/4 v2, 0x0

    move v11, v2

    move v12, v3

    move v13, v4

    :goto_3
    move/from16 v0, v20

    if-ge v11, v0, :cond_f

    .line 1055085
    add-int v25, v13, v21

    .line 1055086
    add-int v26, v12, v22

    .line 1055087
    move-object/from16 v0, p0

    iget v2, v0, LX/67m;->h:I

    mul-int/2addr v2, v13

    int-to-float v2, v2

    add-float v3, v23, v2

    .line 1055088
    move-object/from16 v0, p0

    iget v2, v0, LX/67m;->h:I

    mul-int/2addr v2, v12

    int-to-float v2, v2

    add-float v4, v24, v2

    .line 1055089
    move/from16 v0, v26

    move/from16 v1, v16

    if-lt v0, v1, :cond_5

    move/from16 v0, v26

    move/from16 v1, v18

    if-gt v0, v1, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, LX/67m;->h:I

    int-to-float v2, v2

    add-float v5, v3, v2

    move-object/from16 v0, p0

    iget v2, v0, LX/67m;->h:I

    int-to-float v2, v2

    add-float v6, v4, v2

    sget-object v7, Landroid/graphics/Canvas$EdgeType;->BW:Landroid/graphics/Canvas$EdgeType;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->quickReject(FFFFLandroid/graphics/Canvas$EdgeType;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_5
    const/4 v2, 0x1

    .line 1055090
    :goto_4
    if-nez v2, :cond_12

    .line 1055091
    and-int v5, v25, v19

    .line 1055092
    move-object/from16 v0, p0

    iget-object v2, v0, LX/68P;->o:LX/68s;

    iget v6, v14, Lcom/facebook/android/maps/MapView;->g:I

    move-object/from16 v0, p0

    iget-object v7, v0, LX/68P;->p:LX/68i;

    move/from16 v0, v26

    invoke-virtual {v2, v5, v0, v6, v7}, LX/68s;->a(IIILX/68i;)V

    .line 1055093
    move-object/from16 v0, p0

    iget-object v2, v0, LX/68P;->p:LX/68i;

    iget-object v2, v2, LX/68i;->a:LX/69C;

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    .line 1055094
    :goto_5
    if-nez v2, :cond_c

    move-object/from16 v0, p0

    iget-object v6, v0, LX/68P;->p:LX/68i;

    iget v6, v6, LX/68i;->h:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_c

    .line 1055095
    iget v6, v14, Lcom/facebook/android/maps/MapView;->g:I

    const/4 v7, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v5, v1, v6, v7}, LX/68P;->a(IIII)V

    .line 1055096
    :cond_6
    :goto_6
    move-object/from16 v0, p0

    iget-object v5, v0, LX/68P;->p:LX/68i;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v3, v4}, LX/68i;->a(Landroid/graphics/Canvas;FF)V

    .line 1055097
    move-object/from16 v0, p0

    iget v3, v0, LX/68P;->r:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, LX/68P;->r:I

    .line 1055098
    if-nez v2, :cond_d

    .line 1055099
    move-object/from16 v0, p0

    iget v2, v0, LX/68P;->s:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, LX/68P;->s:I

    move v6, v10

    .line 1055100
    :goto_7
    if-eq v13, v12, :cond_8

    if-gez v13, :cond_7

    neg-int v2, v13

    if-eq v2, v12, :cond_8

    :cond_7
    if-lez v13, :cond_11

    rsub-int/lit8 v2, v12, 0x1

    if-ne v13, v2, :cond_11

    .line 1055101
    :cond_8
    neg-int v5, v8

    move v4, v9

    .line 1055102
    :goto_8
    add-int v2, v25, v5

    .line 1055103
    add-int v3, v26, v4

    .line 1055104
    move/from16 v0, v16

    if-gt v0, v3, :cond_e

    move/from16 v0, v18

    if-gt v3, v0, :cond_e

    if-gt v15, v2, :cond_e

    move/from16 v0, v17

    if-gt v2, v0, :cond_e

    .line 1055105
    add-int v3, v13, v5

    .line 1055106
    add-int v2, v12, v4

    .line 1055107
    :goto_9
    add-int/lit8 v7, v11, 0x1

    move v11, v7

    move v12, v2

    move v13, v3

    move v10, v6

    move v8, v4

    move v9, v5

    goto/16 :goto_3

    :cond_9
    move v4, v6

    .line 1055108
    goto/16 :goto_2

    .line 1055109
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 1055110
    :cond_b
    const/4 v2, 0x0

    goto :goto_5

    .line 1055111
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, LX/68P;->p:LX/68i;

    iget v5, v5, LX/68i;->h:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_6

    sget-object v5, LX/68P;->y:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1055112
    sget-object v5, LX/68P;->y:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1055113
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, LX/68P;->p:LX/68i;

    iget-object v2, v2, LX/68i;->a:LX/69C;

    iget-wide v2, v2, LX/69C;->d:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_12

    .line 1055114
    const/4 v2, 0x1

    move v6, v2

    goto :goto_7

    .line 1055115
    :cond_e
    and-int/lit8 v2, v5, 0x1

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v2, v13

    shr-int/lit8 v3, v4, 0x1

    and-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v2

    .line 1055116
    and-int/lit8 v2, v4, 0x1

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v2, v12

    neg-int v7, v5

    shr-int/lit8 v7, v7, 0x1

    and-int/lit8 v7, v7, 0x1

    add-int/2addr v2, v7

    .line 1055117
    neg-int v5, v5

    .line 1055118
    neg-int v4, v4

    goto :goto_9

    .line 1055119
    :cond_f
    move-object/from16 v0, p0

    iget v2, v0, LX/68P;->r:I

    move-object/from16 v0, p0

    iget v3, v0, LX/68P;->t:I

    if-le v2, v3, :cond_10

    .line 1055120
    move-object/from16 v0, p0

    iget v2, v0, LX/68P;->r:I

    move-object/from16 v0, p0

    iput v2, v0, LX/68P;->t:I

    .line 1055121
    move-object/from16 v0, p0

    iget-object v2, v0, LX/68P;->w:[I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/68P;->a([I)V

    .line 1055122
    move-object/from16 v0, p0

    iget-object v2, v0, LX/68P;->o:LX/68s;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/68P;->w:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, LX/68s;->a(I)LX/68s;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/68P;->w:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, LX/68s;->b(I)LX/68s;

    .line 1055123
    :cond_10
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1055124
    if-eqz v10, :cond_1

    .line 1055125
    invoke-direct/range {p0 .. p0}, LX/68P;->t()V

    goto/16 :goto_0

    :cond_11
    move v4, v8

    move v5, v9

    goto/16 :goto_8

    :cond_12
    move v6, v10

    goto/16 :goto_7
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1055041
    invoke-super {p0, p1}, LX/67m;->a(Z)V

    .line 1055042
    invoke-direct {p0}, LX/68P;->s()V

    .line 1055043
    return-void
.end method

.method public a([I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1055036
    iget v0, p0, LX/68P;->t:I

    int-to-double v0, v0

    iget-wide v2, p0, LX/68P;->q:D

    mul-double/2addr v0, v2

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 1055037
    iget v1, p0, LX/68P;->t:I

    sub-int v1, v0, v1

    add-int/lit8 v1, v1, -0x1

    .line 1055038
    const/4 v2, 0x0

    aput v0, p1, v2

    .line 1055039
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    aput v0, p1, v4

    .line 1055040
    return-void
.end method

.method public abstract b(III)LX/69C;
.end method

.method public b()V
    .locals 0

    .prologue
    .line 1055033
    invoke-super {p0}, LX/67m;->b()V

    .line 1055034
    invoke-direct {p0}, LX/68P;->s()V

    .line 1055035
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 1055020
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/68P;->o:LX/68s;

    .line 1055021
    iget v2, v0, LX/68s;->d:I

    move v0, v2

    .line 1055022
    if-ne v0, v1, :cond_1

    .line 1055023
    iget-object v0, p0, LX/68P;->o:LX/68s;

    invoke-virtual {p0}, LX/68P;->c()I

    move-result v1

    .line 1055024
    iput v1, v0, LX/68s;->d:I

    .line 1055025
    invoke-direct {p0}, LX/68P;->s()V

    .line 1055026
    :cond_0
    :goto_0
    return-void

    .line 1055027
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, LX/68P;->o:LX/68s;

    .line 1055028
    iget v2, v0, LX/68s;->d:I

    move v0, v2

    .line 1055029
    if-eq v0, v1, :cond_0

    .line 1055030
    iget-object v0, p0, LX/68P;->o:LX/68s;

    .line 1055031
    iput v1, v0, LX/68s;->d:I

    .line 1055032
    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1055019
    const/4 v0, -0x1

    return v0
.end method

.method public l()V
    .locals 0

    .prologue
    .line 1055016
    invoke-super {p0}, LX/67m;->l()V

    .line 1055017
    invoke-virtual {p0}, LX/68P;->p()V

    .line 1055018
    return-void
.end method

.method public p()V
    .locals 2

    .prologue
    .line 1055008
    iget-object v0, p0, LX/68P;->o:LX/68s;

    .line 1055009
    iget-object v1, v0, LX/68s;->b:LX/69C;

    .line 1055010
    :goto_0
    if-eqz v1, :cond_0

    .line 1055011
    iget-object p0, v1, LX/69C;->j:LX/69C;

    .line 1055012
    invoke-virtual {v1}, LX/69C;->c()V

    move-object v1, p0

    .line 1055013
    goto :goto_0

    .line 1055014
    :cond_0
    invoke-static {v0}, LX/68s;->d(LX/68s;)V

    .line 1055015
    return-void
.end method

.method public q()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1055003
    invoke-virtual {p0}, LX/68P;->p()V

    .line 1055004
    iput v1, p0, LX/68P;->v:I

    .line 1055005
    iget-object v0, p0, LX/68P;->o:LX/68s;

    .line 1055006
    iput v1, v0, LX/68s;->d:I

    .line 1055007
    return-void
.end method
