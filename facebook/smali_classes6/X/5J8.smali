.class public final LX/5J8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 896598
    const/4 v12, 0x0

    .line 896599
    const/4 v11, 0x0

    .line 896600
    const/4 v10, 0x0

    .line 896601
    const/4 v9, 0x0

    .line 896602
    const/4 v8, 0x0

    .line 896603
    const/4 v7, 0x0

    .line 896604
    const/4 v6, 0x0

    .line 896605
    const/4 v5, 0x0

    .line 896606
    const/4 v4, 0x0

    .line 896607
    const/4 v3, 0x0

    .line 896608
    const/4 v2, 0x0

    .line 896609
    const/4 v1, 0x0

    .line 896610
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 896611
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 896612
    const/4 v1, 0x0

    .line 896613
    :goto_0
    return v1

    .line 896614
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 896615
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 896616
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 896617
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 896618
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 896619
    const-string v14, "__type__"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2

    const-string v14, "__typename"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 896620
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v12

    goto :goto_1

    .line 896621
    :cond_3
    const-string v14, "can_viewer_add_to_attachment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 896622
    const/4 v2, 0x1

    .line 896623
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 896624
    :cond_4
    const-string v14, "can_viewer_remove_from_attachment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 896625
    const/4 v1, 0x1

    .line 896626
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 896627
    :cond_5
    const-string v14, "confirmed_places_for_attachment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 896628
    invoke-static/range {p0 .. p1}, LX/5Ec;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 896629
    :cond_6
    const-string v14, "confirmed_profiles"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 896630
    invoke-static/range {p0 .. p1}, LX/5G9;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 896631
    :cond_7
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 896632
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 896633
    :cond_8
    const-string v14, "lightweight_recs"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 896634
    invoke-static/range {p0 .. p1}, LX/5G6;->b(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 896635
    :cond_9
    const-string v14, "pending_place_slots"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 896636
    invoke-static/range {p0 .. p1}, LX/5G4;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 896637
    :cond_a
    const-string v14, "pending_places_for_attachment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 896638
    invoke-static/range {p0 .. p1}, LX/5Ec;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 896639
    :cond_b
    const-string v14, "pending_profiles"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 896640
    invoke-static/range {p0 .. p1}, LX/5G9;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 896641
    :cond_c
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 896642
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 896643
    if-eqz v2, :cond_d

    .line 896644
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 896645
    :cond_d
    if-eqz v1, :cond_e

    .line 896646
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, LX/186;->a(IZ)V

    .line 896647
    :cond_e
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 896648
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 896649
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 896650
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 896651
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 896652
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 896653
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 896654
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 896655
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 896656
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 896657
    if-eqz v0, :cond_0

    .line 896658
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896659
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 896660
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 896661
    if-eqz v0, :cond_1

    .line 896662
    const-string v1, "can_viewer_add_to_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896663
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 896664
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 896665
    if-eqz v0, :cond_2

    .line 896666
    const-string v1, "can_viewer_remove_from_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896667
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 896668
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 896669
    if-eqz v0, :cond_3

    .line 896670
    const-string v1, "confirmed_places_for_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896671
    invoke-static {p0, v0, p2, p3}, LX/5Ec;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 896672
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 896673
    if-eqz v0, :cond_4

    .line 896674
    const-string v1, "confirmed_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896675
    invoke-static {p0, v0, p2, p3}, LX/5G9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 896676
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 896677
    if-eqz v0, :cond_5

    .line 896678
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896679
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 896680
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 896681
    if-eqz v0, :cond_6

    .line 896682
    const-string v1, "lightweight_recs"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896683
    invoke-static {p0, v0, p2, p3}, LX/5G6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 896684
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 896685
    if-eqz v0, :cond_7

    .line 896686
    const-string v1, "pending_place_slots"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896687
    invoke-static {p0, v0, p2, p3}, LX/5G4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 896688
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 896689
    if-eqz v0, :cond_8

    .line 896690
    const-string v1, "pending_places_for_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896691
    invoke-static {p0, v0, p2, p3}, LX/5Ec;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 896692
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 896693
    if-eqz v0, :cond_9

    .line 896694
    const-string v1, "pending_profiles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 896695
    invoke-static {p0, v0, p2, p3}, LX/5G9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 896696
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 896697
    return-void
.end method
