.class public LX/6Ez;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Du;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:LX/6F6;

.field private final b:LX/6rI;

.field public final c:Landroid/content/Context;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:LX/6Et;

.field public f:Lcom/facebook/payments/checkout/model/CheckoutData;

.field public g:LX/6Ex;

.field public h:Ljava/lang/String;

.field private i:LX/6qh;

.field private final j:LX/6Ex;


# direct methods
.method public constructor <init>(LX/6F6;LX/6rI;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/6Et;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1066938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066939
    new-instance v0, LX/6Ey;

    invoke-direct {v0, p0}, LX/6Ey;-><init>(LX/6Ez;)V

    iput-object v0, p0, LX/6Ez;->j:LX/6Ex;

    .line 1066940
    iput-object p1, p0, LX/6Ez;->a:LX/6F6;

    .line 1066941
    iput-object p2, p0, LX/6Ez;->b:LX/6rI;

    .line 1066942
    iput-object p3, p0, LX/6Ez;->c:Landroid/content/Context;

    .line 1066943
    iput-object p4, p0, LX/6Ez;->d:Ljava/util/concurrent/Executor;

    .line 1066944
    iput-object p5, p0, LX/6Ez;->e:LX/6Et;

    .line 1066945
    return-void
.end method

.method public static a(LX/0QB;)LX/6Ez;
    .locals 9

    .prologue
    .line 1066927
    const-class v1, LX/6Ez;

    monitor-enter v1

    .line 1066928
    :try_start_0
    sget-object v0, LX/6Ez;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1066929
    sput-object v2, LX/6Ez;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1066930
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1066931
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1066932
    new-instance v3, LX/6Ez;

    invoke-static {v0}, LX/6F6;->a(LX/0QB;)LX/6F6;

    move-result-object v4

    check-cast v4, LX/6F6;

    invoke-static {v0}, LX/6rI;->b(LX/0QB;)LX/6rI;

    move-result-object v5

    check-cast v5, LX/6rI;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/6Et;->b(LX/0QB;)LX/6Et;

    move-result-object v8

    check-cast v8, LX/6Et;

    invoke-direct/range {v3 .. v8}, LX/6Ez;-><init>(LX/6F6;LX/6rI;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/6Et;)V

    .line 1066933
    move-object v0, v3

    .line 1066934
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1066935
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/6Ez;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1066936
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1066937
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/payments/checkout/model/CheckoutData;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1066904
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;->f:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 1066905
    if-nez v0, :cond_0

    const v0, 0x7f081d7e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;->a:Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    iget-object v0, v0, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static b$redex0(LX/6Ez;Ljava/lang/String;)V
    .locals 3
    .param p0    # LX/6Ez;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1066922
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1066923
    iget-object v0, p0, LX/6Ez;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081d7d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 1066924
    :cond_0
    iget-object v0, p0, LX/6Ez;->g:LX/6Ex;

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/6Ex;->a(Ljava/lang/Throwable;)V

    .line 1066925
    iget-object v0, p0, LX/6Ez;->c:Landroid/content/Context;

    iget-object v1, p0, LX/6Ez;->f:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/payments/checkout/CheckoutCommonParams;->E:Z

    iget-object v2, p0, LX/6Ez;->i:LX/6qh;

    invoke-static {v0, p1, v1, v2}, LX/6rI;->a(Landroid/content/Context;Ljava/lang/String;ZLX/6qh;)V

    .line 1066926
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1066920
    iput-object p1, p0, LX/6Ez;->f:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1066921
    iget-object v0, p0, LX/6Ez;->b:LX/6rI;

    invoke-virtual {v0, p1}, LX/6rI;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1066915
    iput-object v0, p0, LX/6Ez;->g:LX/6Ex;

    .line 1066916
    iput-object v0, p0, LX/6Ez;->h:Ljava/lang/String;

    .line 1066917
    iput-object v0, p0, LX/6Ez;->i:LX/6qh;

    .line 1066918
    iput-object v0, p0, LX/6Ez;->f:Lcom/facebook/payments/checkout/model/CheckoutData;

    .line 1066919
    return-void
.end method

.method public final a(LX/6Ex;)V
    .locals 2

    .prologue
    .line 1066912
    iput-object p1, p0, LX/6Ez;->g:LX/6Ex;

    .line 1066913
    iget-object v0, p0, LX/6Ez;->b:LX/6rI;

    iget-object v1, p0, LX/6Ez;->j:LX/6Ex;

    invoke-virtual {v0, v1}, LX/6rI;->a(LX/6Ex;)V

    .line 1066914
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 1

    .prologue
    .line 1066909
    iput-object p1, p0, LX/6Ez;->i:LX/6qh;

    .line 1066910
    iget-object v0, p0, LX/6Ez;->b:LX/6rI;

    invoke-virtual {v0, p1}, LX/6rI;->a(LX/6qh;)V

    .line 1066911
    return-void
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 1066908
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 1066907
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 1066906
    return-void
.end method
