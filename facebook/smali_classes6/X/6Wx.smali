.class public LX/6Wx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Pc;


# instance fields
.field private final a:LX/5Pd;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1106618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106619
    new-instance v0, LX/5Pd;

    invoke-direct {v0, p1}, LX/5Pd;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/6Wx;->a:LX/5Pd;

    .line 1106620
    return-void
.end method

.method public static b(LX/0QB;)LX/6Wx;
    .locals 2

    .prologue
    .line 1106621
    new-instance v1, LX/6Wx;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/6Wx;-><init>(Landroid/content/res/Resources;)V

    .line 1106622
    return-object v1
.end method


# virtual methods
.method public final a(II)LX/5Pb;
    .locals 1

    .prologue
    .line 1106623
    iget-object v0, p0, LX/6Wx;->a:LX/5Pd;

    invoke-virtual {v0, p1, p2}, LX/5Pd;->a(II)LX/5Pb;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIZ)LX/5Pb;
    .locals 1

    .prologue
    .line 1106624
    iget-object v0, p0, LX/6Wx;->a:LX/5Pd;

    invoke-virtual {v0, p1, p2, p3}, LX/5Pd;->a(IIZ)LX/5Pb;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)LX/5Pb;
    .locals 1

    .prologue
    .line 1106625
    iget-object v0, p0, LX/6Wx;->a:LX/5Pd;

    invoke-virtual {v0, p1, p2, p3}, LX/5Pd;->a(Ljava/lang/String;Ljava/lang/String;Z)LX/5Pb;

    move-result-object v0

    return-object v0
.end method

.method public final a()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 1106626
    iget-object v0, p0, LX/6Wx;->a:LX/5Pd;

    invoke-virtual {v0}, LX/5Pd;->a()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method
