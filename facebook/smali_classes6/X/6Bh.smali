.class public LX/6Bh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2IT;

.field private final b:LX/0SG;

.field private c:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/facebook/assetdownload/AssetDownloadConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/facebook/assetdownload/AssetDownloadConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(LX/2IT;LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1063059
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063060
    iput-boolean v1, p0, LX/6Bh;->e:Z

    .line 1063061
    iput-boolean v1, p0, LX/6Bh;->f:Z

    .line 1063062
    iput-object p1, p0, LX/6Bh;->a:LX/2IT;

    .line 1063063
    iput-object p2, p0, LX/6Bh;->b:LX/0SG;

    .line 1063064
    new-instance v0, LX/6Bg;

    invoke-direct {v0}, LX/6Bg;-><init>()V

    invoke-static {v0}, LX/0RA;->a(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, LX/6Bh;->d:Ljava/util/TreeSet;

    .line 1063065
    new-instance v0, LX/6Bg;

    invoke-direct {v0}, LX/6Bg;-><init>()V

    invoke-static {v0}, LX/0RA;->a(Ljava/util/Comparator;)Ljava/util/TreeSet;

    move-result-object v0

    iput-object v0, p0, LX/6Bh;->c:Ljava/util/TreeSet;

    .line 1063066
    return-void
.end method

.method private static a(LX/6Bh;)V
    .locals 14

    .prologue
    const-wide v12, 0xfc579c00L

    const-wide/32 v10, 0xf731400

    const/16 v2, 0x8

    .line 1063050
    iget-object v0, p0, LX/6Bh;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/6Bh;->e:Z

    if-nez v0, :cond_0

    .line 1063051
    iget-object v1, p0, LX/6Bh;->a:LX/2IT;

    sget-object v3, LX/6BU;->MUST_BE_WIFI:LX/6BU;

    iget-object v0, p0, LX/6Bh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    sub-long/2addr v4, v12

    iget-object v0, p0, LX/6Bh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    sub-long/2addr v6, v10

    iget-object v0, p0, LX/6Bh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v8

    invoke-virtual/range {v1 .. v9}, LX/2IT;->a(ILX/6BU;JJJ)LX/0Px;

    move-result-object v0

    .line 1063052
    iget-object v1, p0, LX/6Bh;->c:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 1063053
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    iput-boolean v0, p0, LX/6Bh;->e:Z

    .line 1063054
    :cond_0
    iget-object v0, p0, LX/6Bh;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/6Bh;->f:Z

    if-nez v0, :cond_1

    .line 1063055
    iget-object v1, p0, LX/6Bh;->a:LX/2IT;

    sget-object v3, LX/6BU;->CAN_BE_ANY:LX/6BU;

    iget-object v0, p0, LX/6Bh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    sub-long/2addr v4, v12

    iget-object v0, p0, LX/6Bh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    sub-long/2addr v6, v10

    iget-object v0, p0, LX/6Bh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v8

    invoke-virtual/range {v1 .. v9}, LX/2IT;->a(ILX/6BU;JJJ)LX/0Px;

    move-result-object v0

    .line 1063056
    iget-object v1, p0, LX/6Bh;->d:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 1063057
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    iput-boolean v0, p0, LX/6Bh;->f:Z

    .line 1063058
    :cond_1
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Z)Lcom/facebook/assetdownload/AssetDownloadConfiguration;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1063035
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/6Bh;->a(LX/6Bh;)V

    .line 1063036
    if-eqz p1, :cond_4

    iget-object v0, p0, LX/6Bh;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1063037
    iget-object v0, p0, LX/6Bh;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;

    move-object v2, v0

    .line 1063038
    :goto_0
    iget-object v0, p0, LX/6Bh;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1063039
    iget-object v0, p0, LX/6Bh;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1063040
    :goto_1
    if-nez v2, :cond_1

    if-nez v0, :cond_1

    move-object v0, v1

    .line 1063041
    :cond_0
    :goto_2
    monitor-exit p0

    return-object v0

    .line 1063042
    :cond_1
    if-eqz v2, :cond_0

    .line 1063043
    if-nez v0, :cond_2

    move-object v0, v2

    .line 1063044
    goto :goto_2

    .line 1063045
    :cond_2
    :try_start_1
    iget v1, v2, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mPriority:I

    move v1, v1

    .line 1063046
    iget v3, v0, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mPriority:I

    move v3, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1063047
    if-le v1, v3, :cond_0

    move-object v0, v2

    goto :goto_2

    .line 1063048
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    move-object v2, v1

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;)Z
    .locals 1

    .prologue
    .line 1063049
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6Bh;->d:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6Bh;->c:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
