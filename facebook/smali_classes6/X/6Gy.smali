.class public final LX/6Gy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1071156
    iput-object p1, p0, LX/6Gy;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    iput-object p2, p0, LX/6Gy;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1071157
    iget-object v0, p0, LX/6Gy;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0fa9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1071158
    iget-object v0, p0, LX/6Gy;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0faa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1071159
    :try_start_0
    iget-object v0, p0, LX/6Gy;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->b:LX/43C;

    iget-object v1, p0, LX/6Gy;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, LX/43C;->a(Ljava/lang/String;IIIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1071160
    iget-object v1, p0, LX/6Gy;->b:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->d:Ljava/util/Map;

    iget-object v2, p0, LX/6Gy;->a:Landroid/net/Uri;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/facebook/bitmaps/ImageResizer$ImageResizingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1071161
    :goto_0
    return-object v0

    .line 1071162
    :catch_0
    move-exception v0

    .line 1071163
    sget-object v1, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerPersistentFragment;->a:Ljava/lang/String;

    const-string v2, "Unable to create thumbnail bitmap."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1071164
    const/4 v0, 0x0

    goto :goto_0
.end method
