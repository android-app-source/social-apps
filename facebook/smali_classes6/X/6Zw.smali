.class public final LX/6Zw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/6al;

.field public final synthetic b:Lcom/facebook/maps/GenericMapsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/maps/GenericMapsFragment;LX/6al;)V
    .locals 0

    .prologue
    .line 1111758
    iput-object p1, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    iput-object p2, p0, LX/6Zw;->a:LX/6al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x6cccdefc

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1111759
    iget-object v1, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v1, v1, Lcom/facebook/maps/GenericMapsFragment;->d:LX/6a5;

    .line 1111760
    const-string v2, "generic_map_my_location_button_clicked"

    invoke-static {v1, v2}, LX/6a5;->d(LX/6a5;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    .line 1111761
    if-nez v2, :cond_2

    .line 1111762
    :goto_0
    iget-object v1, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v1, v1, Lcom/facebook/maps/GenericMapsFragment;->c:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->a()LX/0yG;

    move-result-object v1

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-eq v1, v2, :cond_0

    .line 1111763
    iget-object v1, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    const-string v2, "mechanism_my_location_button"

    .line 1111764
    iput-object v2, v1, Lcom/facebook/maps/GenericMapsFragment;->q:Ljava/lang/String;

    .line 1111765
    iget-object v1, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v1, v1, Lcom/facebook/maps/GenericMapsFragment;->d:LX/6a5;

    iget-object v2, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v2, v2, Lcom/facebook/maps/GenericMapsFragment;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/6a5;->a(Ljava/lang/String;)V

    .line 1111766
    iget-object v1, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v1, v1, Lcom/facebook/maps/GenericMapsFragment;->b:LX/6Zb;

    new-instance v2, LX/2si;

    invoke-direct {v2}, LX/2si;-><init>()V

    const-string v3, "surface_generic_map_fragment"

    const-string v4, "mechanism_my_location_button"

    invoke-virtual {v1, v2, v3, v4}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 1111767
    const v1, -0x3c4503f8

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1111768
    :goto_1
    return-void

    .line 1111769
    :cond_0
    iget-object v1, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v1, v1, Lcom/facebook/maps/GenericMapsFragment;->m:Lcom/facebook/android/maps/model/LatLng;

    if-nez v1, :cond_1

    .line 1111770
    iget-object v1, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v2, p0, LX/6Zw;->a:LX/6al;

    invoke-static {v1, v2}, Lcom/facebook/maps/GenericMapsFragment;->b$redex0(Lcom/facebook/maps/GenericMapsFragment;LX/6al;)V

    .line 1111771
    :goto_2
    const v1, 0x31bad419

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    .line 1111772
    :cond_1
    iget-object v1, p0, LX/6Zw;->b:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v2, p0, LX/6Zw;->a:LX/6al;

    invoke-static {v1, v2}, Lcom/facebook/maps/GenericMapsFragment;->c(Lcom/facebook/maps/GenericMapsFragment;LX/6al;)V

    goto :goto_2

    .line 1111773
    :cond_2
    invoke-virtual {v2}, LX/0oG;->d()V

    goto :goto_0
.end method
