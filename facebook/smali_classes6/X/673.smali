.class public LX/673;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "LX/673;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:[C

.field public static final b:LX/673;


# instance fields
.field public transient c:I

.field public transient d:Ljava/lang/String;

.field public final data:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1052032
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, LX/673;->a:[C

    .line 1052033
    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-static {v0}, LX/673;->a([B)LX/673;

    move-result-object v0

    sput-object v0, LX/673;->b:LX/673;

    return-void

    nop

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public constructor <init>([B)V
    .locals 0

    .prologue
    .line 1052104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052105
    iput-object p1, p0, LX/673;->data:[B

    .line 1052106
    return-void
.end method

.method private static a(Ljava/io/InputStream;I)LX/673;
    .locals 4

    .prologue
    .line 1052096
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052097
    :cond_0
    if-gez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052098
    :cond_1
    new-array v1, p1, [B

    .line 1052099
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_3

    .line 1052100
    sub-int v2, p1, v0

    invoke-virtual {p0, v1, v0, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 1052101
    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1052102
    :cond_2
    add-int/2addr v0, v2

    goto :goto_0

    .line 1052103
    :cond_3
    new-instance v0, LX/673;

    invoke-direct {v0, v1}, LX/673;-><init>([B)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)LX/673;
    .locals 2

    .prologue
    .line 1052092
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "s == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052093
    :cond_0
    new-instance v0, LX/673;

    sget-object v1, LX/67J;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-direct {v0, v1}, LX/673;-><init>([B)V

    .line 1052094
    iput-object p0, v0, LX/673;->d:Ljava/lang/String;

    .line 1052095
    return-object v0
.end method

.method public static varargs a([B)LX/673;
    .locals 2

    .prologue
    .line 1052090
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "data == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052091
    :cond_0
    new-instance v1, LX/673;

    invoke-virtual {p0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v1, v0}, LX/673;-><init>([B)V

    return-object v1
.end method

.method public static b(Ljava/lang/String;)LX/673;
    .locals 15

    .prologue
    .line 1052046
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "base64 == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052047
    :cond_0
    const/16 v14, 0x20

    const/16 v13, 0xd

    const/16 v12, 0xa

    const/16 v11, 0x9

    const/4 v8, 0x0

    .line 1052048
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    move v9, v2

    .line 1052049
    :goto_0
    if-lez v9, :cond_2

    .line 1052050
    add-int/lit8 v2, v9, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1052051
    const/16 v3, 0x3d

    if-eq v2, v3, :cond_1

    if-eq v2, v12, :cond_1

    if-eq v2, v13, :cond_1

    if-eq v2, v14, :cond_1

    if-ne v2, v11, :cond_2

    .line 1052052
    :cond_1
    add-int/lit8 v2, v9, -0x1

    move v9, v2

    goto :goto_0

    .line 1052053
    :cond_2
    int-to-long v2, v9

    const-wide/16 v4, 0x6

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x8

    div-long/2addr v2, v4

    long-to-int v2, v2

    new-array v6, v2, [B

    move v7, v8

    move v3, v8

    move v4, v8

    move v5, v8

    .line 1052054
    :goto_1
    if-ge v7, v9, :cond_b

    .line 1052055
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1052056
    const/16 v10, 0x41

    if-lt v2, v10, :cond_3

    const/16 v10, 0x5a

    if-gt v2, v10, :cond_3

    .line 1052057
    add-int/lit8 v2, v2, -0x41

    .line 1052058
    :goto_2
    shl-int/lit8 v3, v3, 0x6

    int-to-byte v2, v2

    or-int/2addr v2, v3

    .line 1052059
    add-int/lit8 v3, v4, 0x1

    .line 1052060
    rem-int/lit8 v4, v3, 0x4

    if-nez v4, :cond_10

    .line 1052061
    add-int/lit8 v4, v5, 0x1

    shr-int/lit8 v10, v2, 0x10

    int-to-byte v10, v10

    aput-byte v10, v6, v5

    .line 1052062
    add-int/lit8 v5, v4, 0x1

    shr-int/lit8 v10, v2, 0x8

    int-to-byte v10, v10

    aput-byte v10, v6, v4

    .line 1052063
    add-int/lit8 v4, v5, 0x1

    int-to-byte v10, v2

    aput-byte v10, v6, v5

    .line 1052064
    :goto_3
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v5, v4

    move v4, v3

    move v3, v2

    goto :goto_1

    .line 1052065
    :cond_3
    const/16 v10, 0x61

    if-lt v2, v10, :cond_4

    const/16 v10, 0x7a

    if-gt v2, v10, :cond_4

    .line 1052066
    add-int/lit8 v2, v2, -0x47

    goto :goto_2

    .line 1052067
    :cond_4
    const/16 v10, 0x30

    if-lt v2, v10, :cond_5

    const/16 v10, 0x39

    if-gt v2, v10, :cond_5

    .line 1052068
    add-int/lit8 v2, v2, 0x4

    goto :goto_2

    .line 1052069
    :cond_5
    const/16 v10, 0x2b

    if-eq v2, v10, :cond_6

    const/16 v10, 0x2d

    if-ne v2, v10, :cond_7

    .line 1052070
    :cond_6
    const/16 v2, 0x3e

    goto :goto_2

    .line 1052071
    :cond_7
    const/16 v10, 0x2f

    if-eq v2, v10, :cond_8

    const/16 v10, 0x5f

    if-ne v2, v10, :cond_9

    .line 1052072
    :cond_8
    const/16 v2, 0x3f

    goto :goto_2

    .line 1052073
    :cond_9
    if-eq v2, v12, :cond_11

    if-eq v2, v13, :cond_11

    if-eq v2, v14, :cond_11

    if-eq v2, v11, :cond_11

    .line 1052074
    const/4 v2, 0x0

    .line 1052075
    :goto_4
    move-object v1, v2

    .line 1052076
    if-eqz v1, :cond_a

    new-instance v0, LX/673;

    invoke-direct {v0, v1}, LX/673;-><init>([B)V

    :goto_5
    return-object v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_5

    .line 1052077
    :cond_b
    rem-int/lit8 v2, v4, 0x4

    .line 1052078
    const/4 v4, 0x1

    if-ne v2, v4, :cond_c

    .line 1052079
    const/4 v2, 0x0

    goto :goto_4

    .line 1052080
    :cond_c
    const/4 v4, 0x2

    if-ne v2, v4, :cond_e

    .line 1052081
    shl-int/lit8 v3, v3, 0xc

    .line 1052082
    add-int/lit8 v2, v5, 0x1

    shr-int/lit8 v3, v3, 0x10

    int-to-byte v3, v3

    aput-byte v3, v6, v5

    move v5, v2

    .line 1052083
    :cond_d
    :goto_6
    array-length v2, v6

    if-ne v5, v2, :cond_f

    move-object v2, v6

    goto :goto_4

    .line 1052084
    :cond_e
    const/4 v4, 0x3

    if-ne v2, v4, :cond_d

    .line 1052085
    shl-int/lit8 v2, v3, 0x6

    .line 1052086
    add-int/lit8 v3, v5, 0x1

    shr-int/lit8 v4, v2, 0x10

    int-to-byte v4, v4

    aput-byte v4, v6, v5

    .line 1052087
    add-int/lit8 v5, v3, 0x1

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, v6, v3

    goto :goto_6

    .line 1052088
    :cond_f
    new-array v2, v5, [B

    .line 1052089
    invoke-static {v6, v8, v2, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_4

    :cond_10
    move v4, v5

    goto :goto_3

    :cond_11
    move v2, v3

    move v3, v4

    move v4, v5

    goto :goto_3
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 3

    .prologue
    .line 1052038
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 1052039
    invoke-static {p1, v0}, LX/673;->a(Ljava/io/InputStream;I)LX/673;

    move-result-object v0

    .line 1052040
    :try_start_0
    const-class v1, LX/673;

    const-string v2, "data"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 1052041
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 1052042
    iget-object v0, v0, LX/673;->data:[B

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1052043
    return-void

    .line 1052044
    :catch_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1052045
    :catch_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 1052035
    iget-object v0, p0, LX/673;->data:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 1052036
    iget-object v0, p0, LX/673;->data:[B

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->write([B)V

    .line 1052037
    return-void
.end method


# virtual methods
.method public a(I)B
    .locals 1

    .prologue
    .line 1052034
    iget-object v0, p0, LX/673;->data:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public a(II)LX/673;
    .locals 4

    .prologue
    .line 1052020
    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "beginIndex < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052021
    :cond_0
    iget-object v0, p0, LX/673;->data:[B

    array-length v0, v0

    if-le p2, v0, :cond_1

    .line 1052022
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "endIndex > length("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/673;->data:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052023
    :cond_1
    sub-int v0, p2, p1

    .line 1052024
    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "endIndex < beginIndex"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052025
    :cond_2
    if-nez p1, :cond_3

    iget-object v1, p0, LX/673;->data:[B

    array-length v1, v1

    if-ne p2, v1, :cond_3

    .line 1052026
    :goto_0
    return-object p0

    .line 1052027
    :cond_3
    new-array v1, v0, [B

    .line 1052028
    iget-object v2, p0, LX/673;->data:[B

    const/4 v3, 0x0

    invoke-static {v2, p1, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1052029
    new-instance p0, LX/673;

    invoke-direct {p0, v1}, LX/673;-><init>([B)V

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1052030
    iget-object v0, p0, LX/673;->d:Ljava/lang/String;

    .line 1052031
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, LX/673;->data:[B

    sget-object v2, LX/67J;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    iput-object v0, p0, LX/673;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(LX/672;)V
    .locals 3

    .prologue
    .line 1052107
    iget-object v0, p0, LX/673;->data:[B

    const/4 v1, 0x0

    iget-object v2, p0, LX/673;->data:[B

    array-length v2, v2

    invoke-virtual {p1, v0, v1, v2}, LX/672;->b([BII)LX/672;

    .line 1052108
    return-void
.end method

.method public a(I[BII)Z
    .locals 1

    .prologue
    .line 1051949
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/673;->data:[B

    array-length v0, v0

    sub-int/2addr v0, p4

    if-gt p1, v0, :cond_0

    if-ltz p3, :cond_0

    array-length v0, p2

    sub-int/2addr v0, p4

    if-gt p3, v0, :cond_0

    iget-object v0, p0, LX/673;->data:[B

    .line 1051950
    invoke-static {v0, p1, p2, p3, p4}, LX/67J;->a([BI[BII)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 1051951
    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1051952
    iget-object v0, p0, LX/673;->data:[B

    .line 1051953
    sget-object p0, LX/66y;->a:[B

    invoke-static {v0, p0}, LX/66y;->a([B[B)Ljava/lang/String;

    move-result-object p0

    move-object v0, p0

    .line 1051954
    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1051955
    iget-object v1, p0, LX/673;->data:[B

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    new-array v2, v1, [C

    .line 1051956
    iget-object v3, p0, LX/673;->data:[B

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-byte v5, v3, v0

    .line 1051957
    add-int/lit8 v6, v1, 0x1

    sget-object v7, LX/673;->a:[C

    shr-int/lit8 v8, v5, 0x4

    and-int/lit8 v8, v8, 0xf

    aget-char v7, v7, v8

    aput-char v7, v2, v1

    .line 1051958
    add-int/lit8 v1, v6, 0x1

    sget-object v7, LX/673;->a:[C

    and-int/lit8 v5, v5, 0xf

    aget-char v5, v7, v5

    aput-char v5, v2, v6

    .line 1051959
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1051960
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 9

    .prologue
    .line 1051961
    check-cast p1, LX/673;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 1051962
    invoke-virtual {p0}, LX/673;->e()I

    move-result v4

    .line 1051963
    invoke-virtual {p1}, LX/673;->e()I

    move-result v5

    .line 1051964
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_3

    .line 1051965
    invoke-virtual {p0, v3}, LX/673;->a(I)B

    move-result v7

    and-int/lit16 v7, v7, 0xff

    .line 1051966
    invoke-virtual {p1, v3}, LX/673;->a(I)B

    move-result v8

    and-int/lit16 v8, v8, 0xff

    .line 1051967
    if-eq v7, v8, :cond_2

    .line 1051968
    if-ge v7, v8, :cond_1

    .line 1051969
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 1051970
    goto :goto_1

    .line 1051971
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1051972
    :cond_3
    if-ne v4, v5, :cond_4

    move v0, v2

    goto :goto_1

    .line 1051973
    :cond_4
    if-lt v4, v5, :cond_0

    move v0, v1

    goto :goto_1
.end method

.method public d()LX/673;
    .locals 6

    .prologue
    const/16 v5, 0x5a

    const/16 v4, 0x41

    .line 1051974
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/673;->data:[B

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 1051975
    iget-object v0, p0, LX/673;->data:[B

    aget-byte v3, v0, v1

    .line 1051976
    if-lt v3, v4, :cond_3

    if-gt v3, v5, :cond_3

    .line 1051977
    iget-object v0, p0, LX/673;->data:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 1051978
    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v3, v3, 0x20

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    move v1, v2

    .line 1051979
    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 1051980
    aget-byte v2, v0, v1

    .line 1051981
    if-lt v2, v4, :cond_0

    if-gt v2, v5, :cond_0

    .line 1051982
    add-int/lit8 v2, v2, 0x20

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 1051983
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1051984
    :cond_1
    new-instance p0, LX/673;

    invoke-direct {p0, v0}, LX/673;-><init>([B)V

    .line 1051985
    :cond_2
    return-object p0

    .line 1051986
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 1051987
    iget-object v0, p0, LX/673;->data:[B

    array-length v0, v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1051988
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 1051989
    :goto_0
    return v0

    .line 1051990
    :cond_0
    instance-of v0, p1, LX/673;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/673;

    .line 1051991
    invoke-virtual {v0}, LX/673;->e()I

    move-result v0

    iget-object v3, p0, LX/673;->data:[B

    array-length v3, v3

    if-ne v0, v3, :cond_1

    check-cast p1, LX/673;

    iget-object v0, p0, LX/673;->data:[B

    iget-object v3, p0, LX/673;->data:[B

    array-length v3, v3

    .line 1051992
    invoke-virtual {p1, v2, v0, v2, v3}, LX/673;->a(I[BII)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1051993
    goto :goto_0
.end method

.method public f()[B
    .locals 1

    .prologue
    .line 1051994
    iget-object v0, p0, LX/673;->data:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1051995
    iget v0, p0, LX/673;->c:I

    .line 1051996
    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/673;->data:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    iput v0, p0, LX/673;->c:I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x40

    .line 1051997
    iget-object v0, p0, LX/673;->data:[B

    array-length v0, v0

    if-nez v0, :cond_0

    .line 1051998
    const-string v0, "[size=0]"

    .line 1051999
    :goto_0
    return-object v0

    .line 1052000
    :cond_0
    invoke-virtual {p0}, LX/673;->a()Ljava/lang/String;

    move-result-object v0

    .line 1052001
    const/4 v1, 0x0

    .line 1052002
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v1, v5, :cond_8

    .line 1052003
    if-ne v2, v3, :cond_4

    .line 1052004
    :goto_2
    move v1, v1

    .line 1052005
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 1052006
    iget-object v0, p0, LX/673;->data:[B

    array-length v0, v0

    if-gt v0, v3, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[hex="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1052007
    invoke-virtual {p0}, LX/673;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[size="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/673;->data:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1052008
    invoke-virtual {p0, v4, v3}, LX/673;->a(II)LX/673;

    move-result-object v1

    invoke-virtual {v1}, LX/673;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u2026]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1052009
    :cond_2
    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\\"

    const-string v4, "\\\\"

    .line 1052010
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\n"

    const-string v4, "\\n"

    .line 1052011
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\r"

    const-string v4, "\\r"

    .line 1052012
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 1052013
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[size="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/673;->data:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u2026]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[text="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1052014
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v6

    .line 1052015
    invoke-static {v6}, Ljava/lang/Character;->isISOControl(I)Z

    move-result v7

    if-eqz v7, :cond_5

    const/16 v7, 0xa

    if-eq v6, v7, :cond_5

    const/16 v7, 0xd

    if-ne v6, v7, :cond_6

    :cond_5
    const v7, 0xfffd

    if-ne v6, v7, :cond_7

    .line 1052016
    :cond_6
    const/4 v1, -0x1

    goto/16 :goto_2

    .line 1052017
    :cond_7
    add-int/lit8 v2, v2, 0x1

    .line 1052018
    invoke-static {v6}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    add-int/2addr v1, v6

    goto/16 :goto_1

    .line 1052019
    :cond_8
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    goto/16 :goto_2
.end method
