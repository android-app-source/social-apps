.class public final LX/5FE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 884939
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 884940
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 884941
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 884942
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 884943
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 884944
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884945
    :goto_1
    move v1, v2

    .line 884946
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 884947
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 884948
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 884949
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 884950
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 884951
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 884952
    const-string v9, "height"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 884953
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v4

    move v4, v3

    goto :goto_2

    .line 884954
    :cond_2
    const-string v9, "uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 884955
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_2

    .line 884956
    :cond_3
    const-string v9, "width"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 884957
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_2

    .line 884958
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 884959
    :cond_5
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 884960
    if-eqz v4, :cond_6

    .line 884961
    invoke-virtual {p1, v2, v7, v2}, LX/186;->a(III)V

    .line 884962
    :cond_6
    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 884963
    if-eqz v1, :cond_7

    .line 884964
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5, v2}, LX/186;->a(III)V

    .line 884965
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 884966
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 884967
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 884968
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x0

    .line 884969
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 884970
    invoke-virtual {p0, v1, p3, p3}, LX/15i;->a(III)I

    move-result v2

    .line 884971
    if-eqz v2, :cond_0

    .line 884972
    const-string v3, "height"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884973
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 884974
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 884975
    if-eqz v2, :cond_1

    .line 884976
    const-string v3, "uri"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884977
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884978
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2, p3}, LX/15i;->a(III)I

    move-result v2

    .line 884979
    if-eqz v2, :cond_2

    .line 884980
    const-string v3, "width"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884981
    invoke-virtual {p2, v2}, LX/0nX;->b(I)V

    .line 884982
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 884983
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 884984
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 884985
    return-void
.end method
