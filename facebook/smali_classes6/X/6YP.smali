.class public LX/6YP;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/ProgressBar;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/view/ViewGroup;

.field public d:Landroid/widget/TextView;

.field public e:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;

.field public f:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;

.field private g:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

.field private h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

.field private i:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

.field private j:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1110040
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1110041
    invoke-virtual {p0}, LX/6YP;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1110042
    const v1, 0x7f031552

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1110043
    const v0, 0x7f0d2ffc

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/6YP;->a:Landroid/widget/ProgressBar;

    .line 1110044
    const v0, 0x7f0d2ff7

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/6YP;->b:Landroid/widget/TextView;

    .line 1110045
    const v0, 0x7f0d0807

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/6YP;->c:Landroid/view/ViewGroup;

    .line 1110046
    const v0, 0x7f0d2ff9

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/6YP;->d:Landroid/widget/TextView;

    .line 1110047
    const v0, 0x7f0d2ffb

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;

    iput-object v0, p0, LX/6YP;->e:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;

    .line 1110048
    const v0, 0x7f0d2ff6

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;

    iput-object v0, p0, LX/6YP;->f:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;

    .line 1110049
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 p1, -0x2

    invoke-direct {v0, v1, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1110050
    invoke-virtual {p0, v0}, LX/6YP;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1110051
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1110052
    iget-object v0, p0, LX/6YP;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1110053
    return-void
.end method

.method public final a(LX/6Y5;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1110054
    iget-object v0, p0, LX/6YP;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1110055
    iget v0, p1, LX/6Y5;->s:I

    move v0, v0

    .line 1110056
    if-eqz v0, :cond_9

    .line 1110057
    iget-object v0, p0, LX/6YP;->f:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;

    .line 1110058
    iget v1, p1, LX/6Y5;->s:I

    move v1, v1

    .line 1110059
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->setTitleImageResource(I)V

    .line 1110060
    iget-object v0, p0, LX/6YP;->f:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;

    invoke-virtual {v0, v3}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->setVisibility(I)V

    .line 1110061
    :cond_0
    :goto_0
    iget-object v0, p1, LX/6Y5;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1110062
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1110063
    iget-object v0, p0, LX/6YP;->b:Landroid/widget/TextView;

    .line 1110064
    iget-object v1, p1, LX/6Y5;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1110065
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1110066
    iget-object v0, p0, LX/6YP;->b:Landroid/widget/TextView;

    .line 1110067
    iget-object v1, p1, LX/6Y5;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1110068
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1110069
    iget-boolean v0, p1, LX/6Y5;->b:Z

    move v0, v0

    .line 1110070
    if-eqz v0, :cond_1

    .line 1110071
    iget-object v0, p0, LX/6YP;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/6YP;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0a03f4

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1110072
    :cond_1
    iget-object v0, p0, LX/6YP;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1110073
    :cond_2
    iget-object v0, p1, LX/6Y5;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1110074
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1110075
    iget-object v0, p1, LX/6Y5;->d:Landroid/text/Spannable;

    move-object v0, v0

    .line 1110076
    if-eqz v0, :cond_a

    .line 1110077
    iget-object v0, p0, LX/6YP;->d:Landroid/widget/TextView;

    .line 1110078
    iget-object v1, p1, LX/6Y5;->d:Landroid/text/Spannable;

    move-object v1, v1

    .line 1110079
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1110080
    iget-object v0, p0, LX/6YP;->d:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1110081
    :goto_1
    iget-object v0, p0, LX/6YP;->d:Landroid/widget/TextView;

    .line 1110082
    iget-object v1, p1, LX/6Y5;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1110083
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1110084
    iget-object v0, p0, LX/6YP;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1110085
    iget-object v0, p1, LX/6Y5;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1110086
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1110087
    iget-object v0, p0, LX/6YP;->d:Landroid/widget/TextView;

    iget-object v1, p0, LX/6YP;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, LX/6YP;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b06dd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v5, p0, LX/6YP;->d:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, LX/6YP;->d:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v0, v1, v4, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1110088
    :cond_3
    iget-object v0, p0, LX/6YP;->e:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogContentView;->a(LX/6Y5;)V

    .line 1110089
    iget-object v0, p1, LX/6Y5;->p:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-object v0, v0

    .line 1110090
    if-eqz v0, :cond_4

    .line 1110091
    const v0, 0x7f0d2ffa

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;

    .line 1110092
    invoke-virtual {v0, v3}, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->setVisibility(I)V

    .line 1110093
    iget-object v1, p1, LX/6Y5;->p:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-object v1, v1

    .line 1110094
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->setCheckListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1110095
    iget-boolean v1, p1, LX/6Y5;->q:Z

    move v1, v1

    .line 1110096
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDontShowAgainCheckbox;->setChecked(Z)V

    .line 1110097
    :cond_4
    iget-object v0, p1, LX/6Y5;->i:Ljava/lang/String;

    move-object v0, v0

    .line 1110098
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1110099
    iget-object v0, p1, LX/6Y5;->j:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 1110100
    if-eqz v0, :cond_b

    move v1, v2

    .line 1110101
    :goto_2
    iget-object v0, p1, LX/6Y5;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1110102
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1110103
    iget-object v0, p1, LX/6Y5;->l:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 1110104
    if-eqz v0, :cond_c

    move v4, v2

    .line 1110105
    :goto_3
    iget-object v0, p1, LX/6Y5;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1110106
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1110107
    iget-object v0, p1, LX/6Y5;->n:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 1110108
    if-eqz v0, :cond_d

    .line 1110109
    :goto_4
    if-nez v1, :cond_5

    if-nez v4, :cond_5

    if-eqz v2, :cond_6

    .line 1110110
    :cond_5
    const v0, 0x7f0d2ffd

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/6YP;->j:Landroid/widget/LinearLayout;

    .line 1110111
    iget-object v0, p0, LX/6YP;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1110112
    invoke-virtual {p1}, LX/6Y5;->r()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1110113
    iget-object v0, p0, LX/6YP;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1110114
    const v0, 0x7f0d3000

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    iput-object v0, p0, LX/6YP;->h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110115
    iget-object v0, p0, LX/6YP;->j:Landroid/widget/LinearLayout;

    iget-object v5, p0, LX/6YP;->h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1110116
    iget-object v0, p0, LX/6YP;->j:Landroid/widget/LinearLayout;

    iget-object v5, p0, LX/6YP;->h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    invoke-virtual {v0, v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1110117
    :cond_6
    if-eqz v1, :cond_7

    .line 1110118
    const v0, 0x7f0d2ffe

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    iput-object v0, p0, LX/6YP;->g:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110119
    iget-object v0, p0, LX/6YP;->g:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110120
    iget-object v1, p1, LX/6Y5;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1110121
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setText(Ljava/lang/String;)V

    .line 1110122
    iget-object v0, p0, LX/6YP;->g:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110123
    iget-object v1, p1, LX/6Y5;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1110124
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1110125
    iget-object v0, p0, LX/6YP;->g:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110126
    iget-object v1, p1, LX/6Y5;->j:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 1110127
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1110128
    iget-object v0, p0, LX/6YP;->g:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    invoke-virtual {v0, v3}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setVisibility(I)V

    .line 1110129
    :cond_7
    if-eqz v4, :cond_8

    .line 1110130
    const v0, 0x7f0d3000

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    iput-object v0, p0, LX/6YP;->h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110131
    iget-object v0, p0, LX/6YP;->h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110132
    iget-object v1, p1, LX/6Y5;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1110133
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setText(Ljava/lang/String;)V

    .line 1110134
    iget-object v0, p0, LX/6YP;->h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110135
    iget-object v1, p1, LX/6Y5;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1110136
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1110137
    iget-object v0, p0, LX/6YP;->h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110138
    iget-object v1, p1, LX/6Y5;->l:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 1110139
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1110140
    iget-object v0, p0, LX/6YP;->h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    invoke-virtual {v0, v3}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setVisibility(I)V

    .line 1110141
    invoke-virtual {p1}, LX/6Y5;->r()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1110142
    iget-object v0, p0, LX/6YP;->h:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    invoke-virtual {p0}, LX/6YP;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0a03f1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setTextColor(I)V

    .line 1110143
    :cond_8
    if-eqz v2, :cond_f

    .line 1110144
    invoke-virtual {p1}, LX/6Y5;->r()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1110145
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should not have 3 horizontal buttons"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1110146
    :cond_9
    iget-object v0, p1, LX/6Y5;->t:Ljava/lang/String;

    move-object v0, v0

    .line 1110147
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1110148
    iget-object v0, p0, LX/6YP;->f:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;

    .line 1110149
    iget-object v1, p1, LX/6Y5;->t:Ljava/lang/String;

    move-object v1, v1

    .line 1110150
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->setTitleImageByUrl(Ljava/lang/String;)V

    .line 1110151
    iget-object v0, p0, LX/6YP;->f:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;

    invoke-virtual {v0, v3}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogExtraTitleBarView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1110152
    :cond_a
    iget-object v0, p0, LX/6YP;->d:Landroid/widget/TextView;

    .line 1110153
    iget-object v1, p1, LX/6Y5;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1110154
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_b
    move v1, v3

    .line 1110155
    goto/16 :goto_2

    :cond_c
    move v4, v3

    .line 1110156
    goto/16 :goto_3

    :cond_d
    move v2, v3

    .line 1110157
    goto/16 :goto_4

    .line 1110158
    :cond_e
    const v0, 0x7f0d2fff

    invoke-virtual {p0, v0}, LX/6YP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    iput-object v0, p0, LX/6YP;->i:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110159
    iget-object v0, p0, LX/6YP;->i:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110160
    iget-object v1, p1, LX/6Y5;->m:Ljava/lang/String;

    move-object v1, v1

    .line 1110161
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setText(Ljava/lang/String;)V

    .line 1110162
    iget-object v0, p0, LX/6YP;->i:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110163
    iget-object v1, p1, LX/6Y5;->m:Ljava/lang/String;

    move-object v1, v1

    .line 1110164
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1110165
    iget-object v0, p0, LX/6YP;->i:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    .line 1110166
    iget-object v1, p1, LX/6Y5;->n:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 1110167
    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1110168
    iget-object v0, p0, LX/6YP;->i:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;

    invoke-virtual {v0, v3}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogButton;->setVisibility(I)V

    .line 1110169
    :cond_f
    return-void
.end method
