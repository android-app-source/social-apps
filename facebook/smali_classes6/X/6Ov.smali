.class public LX/6Ov;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1085960
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085961
    return-void
.end method

.method private static a(LX/0Px;LX/0Px;Ljava/lang/String;Z)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1085943
    if-eqz p3, :cond_1

    .line 1085944
    const/4 v1, 0x0

    .line 1085945
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p0, :cond_5

    :cond_0
    move-object v0, v1

    .line 1085946
    :goto_0
    move-object v0, v0

    .line 1085947
    if-nez v0, :cond_4

    .line 1085948
    :goto_1
    move-object v0, p1

    .line 1085949
    :goto_2
    return-object v0

    .line 1085950
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1085951
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p0

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, p0, :cond_3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 1085952
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_2

    .line 1085953
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1085954
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1085955
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1085956
    goto :goto_2

    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    goto :goto_1

    .line 1085957
    :cond_5
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;

    .line 1085958
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineAppCollection;->b()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_6

    goto :goto_0

    :cond_7
    move-object v0, v1

    .line 1085959
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/6Ov;
    .locals 1

    .prologue
    .line 1085940
    new-instance v0, LX/6Ov;

    invoke-direct {v0}, LX/6Ov;-><init>()V

    .line 1085941
    move-object v0, v0

    .line 1085942
    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 6

    .prologue
    .line 1085921
    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v2

    .line 1085922
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1085923
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1085924
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1085925
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1085926
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1085927
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 1085928
    :cond_1
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1085929
    iput-object v0, v2, LX/4XR;->jB:LX/0Px;

    .line 1085930
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1085931
    const/4 v1, 0x0

    .line 1085932
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1085933
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1085934
    const/4 v0, 0x1

    .line 1085935
    :goto_0
    if-nez v0, :cond_3

    .line 1085936
    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1085937
    :cond_3
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1085938
    iput-object v0, v2, LX/4XR;->cm:LX/0Px;

    .line 1085939
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    return-object v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Z)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 4

    .prologue
    .line 1085896
    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    .line 1085897
    if-eqz p2, :cond_4

    .line 1085898
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->lR()LX/0Px;

    move-result-object v0

    .line 1085899
    if-nez v0, :cond_1

    .line 1085900
    :cond_0
    :goto_0
    return-object p0

    .line 1085901
    :cond_1
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1085902
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1085903
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1085904
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1085905
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1085906
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 1085907
    :cond_3
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1085908
    iput-object v0, v1, LX/4XR;->cm:LX/0Px;

    .line 1085909
    :goto_1
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_0

    .line 1085910
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->go()LX/0Px;

    move-result-object v0

    .line 1085911
    if-eqz v0, :cond_0

    .line 1085912
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1085913
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1085914
    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1085915
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1085916
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1085917
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 1085918
    :cond_6
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1085919
    iput-object v0, v1, LX/4XR;->jB:LX/0Px;

    .line 1085920
    goto :goto_1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Z)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1085881
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1085882
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1085883
    :cond_0
    :goto_0
    return-object p0

    .line 1085884
    :cond_1
    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v0

    .line 1085885
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kV()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1085886
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kV()LX/0Px;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kU()LX/0Px;

    move-result-object v2

    invoke-static {v1, v2, p1, p2}, LX/6Ov;->a(LX/0Px;LX/0Px;Ljava/lang/String;Z)LX/0Px;

    move-result-object v1

    .line 1085887
    iput-object v1, v0, LX/4XR;->pJ:LX/0Px;

    .line 1085888
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_0

    .line 1085889
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->kV()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1085890
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-static {v1}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->kV()LX/0Px;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hb()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->kU()LX/0Px;

    move-result-object v3

    invoke-static {v2, v3, p1, p2}, LX/6Ov;->a(LX/0Px;LX/0Px;Ljava/lang/String;Z)LX/0Px;

    move-result-object v2

    .line 1085891
    iput-object v2, v1, LX/4XR;->pJ:LX/0Px;

    .line 1085892
    move-object v1, v1

    .line 1085893
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 1085894
    iput-object v1, v0, LX/4XR;->kz:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1085895
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1085855
    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    .line 1085856
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oT()LX/0Px;

    move-result-object v0

    .line 1085857
    if-eqz v0, :cond_0

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1085858
    :cond_0
    :goto_0
    return-object p0

    .line 1085859
    :cond_1
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1085860
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1085861
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1085862
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;

    .line 1085863
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceListUserCreatedRecommendation;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1085864
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 1085865
    :cond_3
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1085866
    iput-object v0, v1, LX/4XR;->hE:LX/0Px;

    .line 1085867
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 4

    .prologue
    .line 1085868
    invoke-static {p0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v1

    .line 1085869
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->oV()LX/0Px;

    move-result-object v0

    .line 1085870
    if-nez v0, :cond_0

    .line 1085871
    :goto_0
    return-object p0

    .line 1085872
    :cond_0
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1085873
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1085874
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1085875
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;

    .line 1085876
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPendingPlaceSlot;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1085877
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 1085878
    :cond_2
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1085879
    iput-object v0, v1, LX/4XR;->jA:LX/0Px;

    .line 1085880
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    goto :goto_0
.end method
