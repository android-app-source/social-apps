.class public final LX/6DB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/6CR;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/6CR;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1064681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064682
    iput-object p1, p0, LX/6DB;->a:LX/0QB;

    .line 1064683
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1064741
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6DB;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1064685
    packed-switch p2, :pswitch_data_0

    .line 1064686
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1064687
    :pswitch_0
    new-instance v1, LX/6CS;

    invoke-static {p1}, LX/6CP;->b(LX/0QB;)LX/6CP;

    move-result-object v0

    check-cast v0, LX/6CP;

    invoke-direct {v1, v0}, LX/6CS;-><init>(LX/6CP;)V

    .line 1064688
    move-object v0, v1

    .line 1064689
    :goto_0
    return-object v0

    .line 1064690
    :pswitch_1
    new-instance v1, LX/6CU;

    invoke-static {p1}, LX/6CP;->b(LX/0QB;)LX/6CP;

    move-result-object v0

    check-cast v0, LX/6CP;

    invoke-direct {v1, v0}, LX/6CU;-><init>(LX/6CP;)V

    .line 1064691
    move-object v0, v1

    .line 1064692
    goto :goto_0

    .line 1064693
    :pswitch_2
    new-instance v1, LX/6CW;

    invoke-static {p1}, LX/6CP;->b(LX/0QB;)LX/6CP;

    move-result-object v0

    check-cast v0, LX/6CP;

    invoke-direct {v1, v0}, LX/6CW;-><init>(LX/6CP;)V

    .line 1064694
    move-object v0, v1

    .line 1064695
    goto :goto_0

    .line 1064696
    :pswitch_3
    new-instance v2, LX/6DU;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v4

    check-cast v4, LX/1Bf;

    invoke-static {p1}, LX/6DM;->b(LX/0QB;)LX/6DM;

    move-result-object v5

    check-cast v5, LX/6DM;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    const-class v8, LX/6D0;

    invoke-interface {p1, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/6D0;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    const/16 v10, 0x12cb

    invoke-static {p1, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v2 .. v10}, LX/6DU;-><init>(Landroid/content/Context;LX/1Bf;LX/6DM;LX/0Uh;Ljava/util/concurrent/Executor;LX/6D0;LX/03V;LX/0Or;)V

    .line 1064697
    move-object v0, v2

    .line 1064698
    goto :goto_0

    .line 1064699
    :pswitch_4
    new-instance v2, LX/6DV;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v1

    check-cast v1, LX/1Bf;

    invoke-direct {v2, v0, v1}, LX/6DV;-><init>(Landroid/content/Context;LX/1Bf;)V

    .line 1064700
    move-object v0, v2

    .line 1064701
    goto :goto_0

    .line 1064702
    :pswitch_5
    new-instance v2, LX/6De;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {p1}, LX/6DM;->b(LX/0QB;)LX/6DM;

    move-result-object v6

    check-cast v6, LX/6DM;

    invoke-static {p1}, LX/6DS;->b(LX/0QB;)LX/6DS;

    move-result-object v7

    check-cast v7, LX/6DS;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p1}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {p1}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v10

    check-cast v10, LX/1Bf;

    const-class v11, LX/6D0;

    invoke-interface {p1, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/6D0;

    invoke-static {p1}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v12

    check-cast v12, LX/0W3;

    invoke-direct/range {v2 .. v12}, LX/6De;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/6DM;LX/6DS;LX/03V;Ljava/util/concurrent/Executor;LX/1Bf;LX/6D0;LX/0W3;)V

    .line 1064703
    move-object v0, v2

    .line 1064704
    goto/16 :goto_0

    .line 1064705
    :pswitch_6
    new-instance v0, LX/6EA;

    invoke-direct {v0}, LX/6EA;-><init>()V

    .line 1064706
    move-object v0, v0

    .line 1064707
    move-object v0, v0

    .line 1064708
    goto/16 :goto_0

    .line 1064709
    :pswitch_7
    new-instance v3, LX/6ED;

    invoke-direct {v3}, LX/6ED;-><init>()V

    .line 1064710
    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/6EC;->a(LX/0QB;)LX/6EC;

    move-result-object v1

    check-cast v1, LX/6EC;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    .line 1064711
    iput-object v0, v3, LX/6ED;->b:Landroid/content/Context;

    iput-object v1, v3, LX/6ED;->c:LX/6EC;

    iput-object v2, v3, LX/6ED;->d:LX/03V;

    .line 1064712
    move-object v0, v3

    .line 1064713
    goto/16 :goto_0

    .line 1064714
    :pswitch_8
    new-instance v0, LX/6Ee;

    invoke-direct {v0}, LX/6Ee;-><init>()V

    .line 1064715
    const-class v1, Landroid/content/Context;

    invoke-interface {p1, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p1}, LX/6EF;->b(LX/0QB;)LX/6EF;

    move-result-object v4

    check-cast v4, LX/6EF;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/6EV;->b(LX/0QB;)LX/6EV;

    move-result-object v6

    check-cast v6, LX/6EV;

    invoke-static {p1}, LX/6EW;->a(LX/0QB;)LX/6EW;

    move-result-object v7

    check-cast v7, LX/6EW;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    const-class v9, LX/6D0;

    invoke-interface {p1, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/6D0;

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    .line 1064716
    iput-object v1, v0, LX/6Ee;->b:Landroid/content/Context;

    iput-object v2, v0, LX/6Ee;->c:LX/0SG;

    iput-object v3, v0, LX/6Ee;->d:LX/03V;

    iput-object v4, v0, LX/6Ee;->e:LX/6EF;

    iput-object v5, v0, LX/6Ee;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v0, LX/6Ee;->g:LX/6EV;

    iput-object v7, v0, LX/6Ee;->h:LX/6EW;

    iput-object v8, v0, LX/6Ee;->i:LX/0Uh;

    iput-object v9, v0, LX/6Ee;->j:LX/6D0;

    iput-object v10, v0, LX/6Ee;->a:Ljava/util/concurrent/Executor;

    .line 1064717
    move-object v0, v0

    .line 1064718
    goto/16 :goto_0

    .line 1064719
    :pswitch_9
    new-instance v2, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/6Eg;->a(LX/0QB;)LX/6Eg;

    move-result-object v5

    check-cast v5, LX/6Eg;

    invoke-static {p1}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v6

    check-cast v6, LX/0y3;

    invoke-static {p1}, LX/6Ef;->a(LX/0QB;)LX/6Ef;

    move-result-object v7

    check-cast v7, LX/6Ef;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    const/16 v9, 0xc81

    invoke-static {p1, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v2 .. v10}, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionJSBridgeHandler;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/6Eg;LX/0y3;LX/6Ef;LX/03V;LX/0Or;LX/0Uh;)V

    .line 1064720
    move-object v0, v2

    .line 1064721
    goto/16 :goto_0

    .line 1064722
    :pswitch_a
    new-instance v0, LX/6Es;

    invoke-direct {v0}, LX/6Es;-><init>()V

    .line 1064723
    move-object v0, v0

    .line 1064724
    move-object v0, v0

    .line 1064725
    goto/16 :goto_0

    .line 1064726
    :pswitch_b
    new-instance v2, LX/6F8;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/6F6;->a(LX/0QB;)LX/6F6;

    move-result-object v5

    check-cast v5, LX/6F6;

    invoke-static {p1}, LX/6rt;->b(LX/0QB;)LX/6rt;

    move-result-object v6

    check-cast v6, LX/6rt;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {p1}, LX/6rs;->b(LX/0QB;)LX/6rs;

    move-result-object v8

    check-cast v8, LX/6rs;

    invoke-static {p1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v9

    check-cast v9, LX/0lC;

    invoke-direct/range {v2 .. v9}, LX/6F8;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/6F6;LX/6rt;LX/03V;LX/6rs;LX/0lC;)V

    .line 1064727
    move-object v0, v2

    .line 1064728
    goto/16 :goto_0

    .line 1064729
    :pswitch_c
    new-instance v2, LX/6FH;

    invoke-static {p1}, LX/6FJ;->b(LX/0QB;)LX/6FJ;

    move-result-object v0

    check-cast v0, LX/6FJ;

    invoke-static {p1}, LX/6FE;->a(LX/0QB;)LX/6FE;

    move-result-object v1

    check-cast v1, LX/6FE;

    invoke-direct {v2, v0, v1}, LX/6FH;-><init>(LX/6FJ;LX/6FE;)V

    .line 1064730
    move-object v0, v2

    .line 1064731
    goto/16 :goto_0

    .line 1064732
    :pswitch_d
    new-instance v2, LX/6FL;

    invoke-static {p1}, LX/6FJ;->b(LX/0QB;)LX/6FJ;

    move-result-object v0

    check-cast v0, LX/6FJ;

    invoke-static {p1}, LX/6FE;->a(LX/0QB;)LX/6FE;

    move-result-object v1

    check-cast v1, LX/6FE;

    invoke-direct {v2, v0, v1}, LX/6FL;-><init>(LX/6FJ;LX/6FE;)V

    .line 1064733
    move-object v0, v2

    .line 1064734
    goto/16 :goto_0

    .line 1064735
    :pswitch_e
    new-instance v4, LX/6FN;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/6FE;->a(LX/0QB;)LX/6FE;

    move-result-object v2

    check-cast v2, LX/6FE;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v0, v1, v2, v3}, LX/6FN;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/6FE;LX/0Uh;)V

    .line 1064736
    move-object v0, v4

    .line 1064737
    goto/16 :goto_0

    .line 1064738
    :pswitch_f
    new-instance v2, LX/7i8;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/1Bf;->a(LX/0QB;)LX/1Bf;

    move-result-object v1

    check-cast v1, LX/1Bf;

    invoke-direct {v2, v0, v1}, LX/7i8;-><init>(Landroid/content/Context;LX/1Bf;)V

    .line 1064739
    move-object v0, v2

    .line 1064740
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1064684
    const/16 v0, 0x10

    return v0
.end method
