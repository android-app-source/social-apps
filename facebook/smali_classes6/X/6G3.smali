.class public LX/6G3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/6G3;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:Ljava/util/concurrent/Executor;

.field private final d:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1069815
    const-class v0, LX/6G3;

    sput-object v0, LX/6G3;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0Uh;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1069810
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1069811
    iput-object p1, p0, LX/6G3;->b:Landroid/content/Context;

    .line 1069812
    iput-object p2, p0, LX/6G3;->c:Ljava/util/concurrent/Executor;

    .line 1069813
    iput-object p3, p0, LX/6G3;->d:LX/0Uh;

    .line 1069814
    return-void
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;)LX/6FR;
    .locals 3

    .prologue
    .line 1069836
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1069837
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1069838
    new-instance v2, LX/6FR;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v0, v1}, LX/6FR;-><init>(Landroid/net/Uri;Ljava/io/OutputStream;)V

    return-object v2
.end method

.method public static a(LX/0QB;)LX/6G3;
    .locals 6

    .prologue
    .line 1069816
    sget-object v0, LX/6G3;->e:LX/6G3;

    if-nez v0, :cond_1

    .line 1069817
    const-class v1, LX/6G3;

    monitor-enter v1

    .line 1069818
    :try_start_0
    sget-object v0, LX/6G3;->e:LX/6G3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1069819
    if-eqz v2, :cond_0

    .line 1069820
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1069821
    new-instance p0, LX/6G3;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, LX/6G3;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0Uh;)V

    .line 1069822
    move-object v0, p0

    .line 1069823
    sput-object v0, LX/6G3;->e:LX/6G3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1069824
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1069825
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1069826
    :cond_1
    sget-object v0, LX/6G3;->e:LX/6G3;

    return-object v0

    .line 1069827
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1069828
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6G3;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1069829
    invoke-static {p0, p1}, LX/6G3;->c(LX/6G3;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1069830
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1069831
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 1069832
    :cond_0
    :goto_0
    return-object v0

    .line 1069833
    :catch_0
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 1069834
    :goto_1
    sget-object v2, LX/6G3;->a:Ljava/lang/Class;

    const-string v3, "Exception caught in createDirectory"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1069835
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static a(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 1069795
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 1069796
    if-eqz v1, :cond_1

    .line 1069797
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1069798
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1069799
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1069800
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1069801
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 1069802
    return-void
.end method

.method public static a(Ljava/io/File;LX/6FR;)V
    .locals 1

    .prologue
    .line 1069803
    iget-object v0, p1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069804
    invoke-static {p0, v0}, LX/1t3;->a(Ljava/io/File;Ljava/io/OutputStream;)V

    .line 1069805
    iget-object v0, p1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069806
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 1069807
    iget-object v0, p1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069808
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 1069809
    return-void
.end method

.method public static a(Ljava/io/InputStream;LX/6FR;)V
    .locals 1

    .prologue
    .line 1069792
    iget-object v0, p1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069793
    invoke-static {p0, v0}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 1069794
    return-void
.end method

.method public static b(Ljava/io/File;Ljava/lang/String;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1069786
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1069787
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1069788
    :try_start_0
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, LX/009;->writeReportToStream(Ljava/lang/Throwable;Ljava/io/OutputStream;)V

    .line 1069789
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    .line 1069790
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1069791
    invoke-static {v1, v4}, LX/1md;->a(Ljava/io/Closeable;Z)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1, v4}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method

.method public static b(Ljava/io/File;)Lcom/facebook/bugreporter/BugReport;
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InstanceMethodCanBeStatic",
            "AndroidTrapInstanceMethodCanBeStatic"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 1069770
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "SerializedBugReport"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1069771
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1069772
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v0, v4

    new-array v3, v0, [B

    .line 1069773
    const/4 v0, 0x0

    array-length v4, v3

    invoke-virtual {v1, v3, v0, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 1069774
    new-instance v0, Lcom/facebook/bugreporter/BugReport;

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/facebook/bugreporter/BugReport;-><init>(Ljava/nio/ByteBuffer;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v2, v0

    .line 1069775
    :goto_0
    if-eqz v1, :cond_0

    .line 1069776
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1069777
    :cond_0
    :goto_1
    return-object v2

    .line 1069778
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1069779
    :goto_2
    sget-object v3, LX/6G3;->a:Ljava/lang/Class;

    const-string v4, "Failed to retrieve the persisted bug report!"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v0, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1069780
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 1069781
    :goto_3
    sget-object v3, LX/6G3;->a:Ljava/lang/Class;

    const-string v4, "Failed to retrieve the persisted bug report!"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v3, v0, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1069782
    :catch_2
    move-exception v0

    .line 1069783
    sget-object v1, LX/6G3;->a:Ljava/lang/Class;

    const-string v3, "Failed to release the file reader!"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1069784
    :catch_3
    move-exception v0

    goto :goto_3

    .line 1069785
    :catch_4
    move-exception v0

    goto :goto_2
.end method

.method public static final b(Lcom/facebook/bugreporter/BugReport;)V
    .locals 3

    .prologue
    .line 1069757
    new-instance v0, Ljava/io/File;

    .line 1069758
    iget-object v1, p0, Lcom/facebook/bugreporter/BugReport;->a:Landroid/net/Uri;

    move-object v1, v1

    .line 1069759
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1069760
    const/4 v1, 0x0

    .line 1069761
    :try_start_0
    const-string v2, "SerializedBugReport"

    invoke-static {v0, v2}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;

    move-result-object v1

    .line 1069762
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-static {p0}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v0, v1}, LX/6G3;->a(Ljava/io/InputStream;LX/6FR;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1069763
    if-eqz v1, :cond_0

    .line 1069764
    iget-object v0, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069765
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 1069766
    :cond_0
    return-void

    .line 1069767
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 1069768
    iget-object v2, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v2

    .line 1069769
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_1
    throw v0
.end method

.method public static c(LX/6G3;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 1069753
    iget-object v0, p0, LX/6G3;->d:LX/0Uh;

    const/16 v1, 0x37c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1069754
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/6G3;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1069755
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/6G3;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Ljava/lang/String;)Ljava/io/File;
    .locals 1

    .prologue
    .line 1069756
    const-string v0, "bugreport_attachments"

    invoke-static {p0, v0, p1}, LX/6G3;->a(LX/6G3;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
