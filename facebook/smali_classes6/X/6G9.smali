.class public abstract LX/6G9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field public a:LX/78G;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1069871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1069872
    new-instance v0, LX/78G;

    invoke-direct {v0}, LX/78G;-><init>()V

    iput-object v0, p0, LX/6G9;->a:LX/78G;

    .line 1069873
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 1069874
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 9

    .prologue
    .line 1069875
    iget-object v0, p0, LX/6G9;->a:LX/78G;

    .line 1069876
    iget-object v1, v0, LX/78G;->b:LX/0ZL;

    invoke-virtual {v1}, LX/0ZL;->d()I

    move-result v1

    .line 1069877
    if-nez v1, :cond_a

    .line 1069878
    iget-wide v1, p1, Landroid/hardware/SensorEvent;->timestamp:J

    const-wide/32 v3, 0x3b9aca00

    add-long/2addr v1, v3

    iput-wide v1, v0, LX/78G;->c:J

    .line 1069879
    iget-object v1, v0, LX/78G;->b:LX/0ZL;

    new-instance v2, LX/78J;

    invoke-direct {v2, p1}, LX/78J;-><init>(Landroid/hardware/SensorEvent;)V

    invoke-virtual {v1, v2}, LX/0ZL;->a(Ljava/lang/Object;)V

    .line 1069880
    :goto_0
    iget-object v0, p0, LX/6G9;->a:LX/78G;

    const p1, 0x4150af7e

    const v8, -0x3eaf5082

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 1069881
    iget-object v1, v0, LX/78G;->d:LX/78I;

    invoke-virtual {v1}, LX/78I;->a()V

    .line 1069882
    iget-object v1, v0, LX/78G;->e:LX/78I;

    invoke-virtual {v1}, LX/78I;->a()V

    .line 1069883
    iget-object v1, v0, LX/78G;->f:LX/78I;

    invoke-virtual {v1}, LX/78I;->a()V

    move v2, v3

    .line 1069884
    :goto_1
    iget-object v1, v0, LX/78G;->b:LX/0ZL;

    invoke-virtual {v1}, LX/0ZL;->d()I

    move-result v1

    if-ge v2, v1, :cond_6

    .line 1069885
    iget-object v1, v0, LX/78G;->b:LX/0ZL;

    invoke-virtual {v1, v2}, LX/0ZL;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/78J;

    .line 1069886
    iget-object v7, v0, LX/78G;->d:LX/78I;

    iget v4, v1, LX/78J;->a:F

    cmpl-float v4, v4, p1

    if-lez v4, :cond_0

    move v4, v5

    :goto_2
    iget v6, v1, LX/78J;->a:F

    cmpg-float v6, v6, v8

    if-gez v6, :cond_1

    move v6, v5

    :goto_3
    invoke-virtual {v7, v4, v6}, LX/78I;->a(ZZ)V

    .line 1069887
    iget-object v7, v0, LX/78G;->e:LX/78I;

    iget v4, v1, LX/78J;->b:F

    cmpl-float v4, v4, p1

    if-lez v4, :cond_2

    move v4, v5

    :goto_4
    iget v6, v1, LX/78J;->b:F

    cmpg-float v6, v6, v8

    if-gez v6, :cond_3

    move v6, v5

    :goto_5
    invoke-virtual {v7, v4, v6}, LX/78I;->a(ZZ)V

    .line 1069888
    iget-object v6, v0, LX/78G;->f:LX/78I;

    iget v4, v1, LX/78J;->c:F

    cmpl-float v4, v4, p1

    if-lez v4, :cond_4

    move v4, v5

    :goto_6
    iget v1, v1, LX/78J;->c:F

    cmpg-float v1, v1, v8

    if-gez v1, :cond_5

    move v1, v5

    :goto_7
    invoke-virtual {v6, v4, v1}, LX/78I;->a(ZZ)V

    .line 1069889
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_0
    move v4, v3

    .line 1069890
    goto :goto_2

    :cond_1
    move v6, v3

    goto :goto_3

    :cond_2
    move v4, v3

    .line 1069891
    goto :goto_4

    :cond_3
    move v6, v3

    goto :goto_5

    :cond_4
    move v4, v3

    .line 1069892
    goto :goto_6

    :cond_5
    move v1, v3

    goto :goto_7

    .line 1069893
    :cond_6
    iget-object v1, v0, LX/78G;->d:LX/78I;

    invoke-virtual {v1}, LX/78I;->b()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, v0, LX/78G;->e:LX/78I;

    invoke-virtual {v1}, LX/78I;->b()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, v0, LX/78G;->f:LX/78I;

    invoke-virtual {v1}, LX/78I;->b()Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    move v3, v5

    :cond_8
    move v0, v3

    .line 1069894
    if-eqz v0, :cond_9

    .line 1069895
    iget-object v0, p0, LX/6G9;->a:LX/78G;

    invoke-virtual {v0}, LX/78G;->a()V

    .line 1069896
    invoke-virtual {p0}, LX/6G9;->a()V

    .line 1069897
    :cond_9
    return-void

    .line 1069898
    :cond_a
    iget-wide v3, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v5, v0, LX/78G;->c:J

    cmp-long v2, v3, v5

    if-gez v2, :cond_b

    const/16 v2, 0x20

    if-lt v1, v2, :cond_c

    :cond_b
    const/16 v2, 0xa

    if-ge v1, v2, :cond_d

    .line 1069899
    :cond_c
    iget-object v1, v0, LX/78G;->b:LX/0ZL;

    new-instance v2, LX/78J;

    invoke-direct {v2, p1}, LX/78J;-><init>(Landroid/hardware/SensorEvent;)V

    invoke-virtual {v1, v2}, LX/0ZL;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1069900
    :cond_d
    iget-object v1, v0, LX/78G;->b:LX/0ZL;

    invoke-virtual {v1}, LX/0ZL;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/78J;

    .line 1069901
    invoke-virtual {v1, p1}, LX/78J;->a(Landroid/hardware/SensorEvent;)V

    .line 1069902
    iget-object v2, v0, LX/78G;->b:LX/0ZL;

    invoke-virtual {v2, v1}, LX/0ZL;->a(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
