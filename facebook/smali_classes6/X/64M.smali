.class public final LX/64M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1p2;


# instance fields
.field public final synthetic a:LX/64N;

.field private b:Landroid/content/SharedPreferences$Editor;


# direct methods
.method public constructor <init>(LX/64N;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .prologue
    .line 1044484
    iput-object p1, p0, LX/64M;->a:LX/64N;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044485
    iput-object p2, p0, LX/64M;->b:Landroid/content/SharedPreferences$Editor;

    .line 1044486
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/1p2;
    .locals 4

    .prologue
    .line 1044487
    iget-object v0, p0, LX/64M;->a:LX/64N;

    iget-object v0, v0, LX/64N;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1044488
    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v2, v3, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2f

    if-ne v2, v3, :cond_0

    .line 1044489
    :cond_1
    iget-object v2, p0, LX/64M;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 1044490
    :cond_2
    return-object p0
.end method

.method public final a(Ljava/lang/String;I)LX/1p2;
    .locals 1

    .prologue
    .line 1044491
    iget-object v0, p0, LX/64M;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1044492
    return-object p0
.end method

.method public final a(Ljava/lang/String;J)LX/1p2;
    .locals 2

    .prologue
    .line 1044493
    iget-object v0, p0, LX/64M;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 1044494
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/1p2;
    .locals 1

    .prologue
    .line 1044495
    iget-object v0, p0, LX/64M;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1044496
    return-object p0
.end method

.method public final a(Ljava/lang/String;Z)LX/1p2;
    .locals 1

    .prologue
    .line 1044497
    iget-object v0, p0, LX/64M;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1044498
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1044499
    iget-object v0, p0, LX/64M;->b:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1044500
    return-void
.end method
