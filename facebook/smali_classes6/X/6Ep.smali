.class public LX/6Ep;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Co;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1066682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066683
    iput-object p1, p0, LX/6Ep;->a:Landroid/content/Context;

    .line 1066684
    iput-object p2, p0, LX/6Ep;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1066685
    return-void
.end method


# virtual methods
.method public final a()LX/6Eo;
    .locals 1

    .prologue
    .line 1066686
    sget-object v0, LX/6Eo;->MANAGE_SETTINGS:LX/6Eo;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1066687
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1066688
    new-instance v0, LX/6En;

    iget-object v1, p0, LX/6Ep;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/6En;-><init>(Landroid/content/Context;)V

    const-string v1, "JS_BRIDGE_APP_ID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1066689
    iput-object v1, v0, LX/6En;->b:Ljava/lang/String;

    .line 1066690
    move-object v0, v0

    .line 1066691
    const-string v1, "JS_BRIDGE_APP_NAME"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1066692
    iput-object v1, v0, LX/6En;->c:Ljava/lang/String;

    .line 1066693
    move-object v0, v0

    .line 1066694
    const-string v1, "JS_BRIDGE_SHOW_INSTANT_EXPERIENCES_AUTOFILL_SETTING"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1066695
    iput-boolean v1, v0, LX/6En;->d:Z

    .line 1066696
    move-object v0, v0

    .line 1066697
    invoke-virtual {v0}, LX/6En;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1066698
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1066699
    iget-object v1, p0, LX/6Ep;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/6Ep;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1066700
    return-void
.end method
