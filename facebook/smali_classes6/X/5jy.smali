.class public final LX/5jy;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 989250
    const-class v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;

    const v0, 0x3e87034e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "FetchPhotosExtraLoggingMetadataQuery"

    const-string v6, "7f936830fa81a95d3f7f470b317323df"

    const-string v7, "nodes"

    const-string v8, "10155069965666729"

    const/4 v9, 0x0

    .line 989251
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 989252
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 989253
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 989254
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 989255
    packed-switch v0, :pswitch_data_0

    .line 989256
    :goto_0
    return-object p1

    .line 989257
    :pswitch_0
    const-string p1, "0"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64212b1
        :pswitch_0
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 989258
    new-instance v0, Lcom/facebook/photos/data/protocol/FetchPhotosMetadataQuery$FetchPhotosExtraLoggingMetadataQueryString$1;

    const-class v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaExtraMetadataForLoggingModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/photos/data/protocol/FetchPhotosMetadataQuery$FetchPhotosExtraLoggingMetadataQueryString$1;-><init>(LX/5jy;Ljava/lang/Class;)V

    return-object v0
.end method
