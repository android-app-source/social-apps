.class public LX/6WW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1nq;

.field public b:Landroid/text/Layout;

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1106401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106402
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    iput-object v0, p0, LX/6WW;->a:LX/1nq;

    .line 1106403
    const/4 v0, 0x0

    iput-object v0, p0, LX/6WW;->b:Landroid/text/Layout;

    .line 1106404
    const/4 v0, 0x0

    iput v0, p0, LX/6WW;->c:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1106400
    iget-object v0, p0, LX/6WW;->a:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 1106398
    iget-object v0, p0, LX/6WW;->a:LX/1nq;

    invoke-static {v0, p1, p2}, LX/5OZ;->b(LX/1nq;Landroid/content/Context;I)V

    .line 1106399
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1106392
    iget v0, p0, LX/6WW;->c:I

    if-nez v0, :cond_0

    .line 1106393
    iget-object v0, p0, LX/6WW;->b:Landroid/text/Layout;

    if-eqz v0, :cond_0

    .line 1106394
    iget v0, p0, LX/6WW;->d:I

    int-to-float v0, v0

    iget v1, p0, LX/6WW;->e:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1106395
    iget-object v0, p0, LX/6WW;->b:Landroid/text/Layout;

    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 1106396
    iget v0, p0, LX/6WW;->d:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, LX/6WW;->e:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1106397
    :cond_0
    return-void
.end method

.method public final a(Landroid/text/Layout$Alignment;)V
    .locals 1

    .prologue
    .line 1106390
    iget-object v0, p0, LX/6WW;->a:LX/1nq;

    invoke-virtual {v0, p1}, LX/1nq;->a(Landroid/text/Layout$Alignment;)LX/1nq;

    .line 1106391
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 1106387
    invoke-virtual {p0}, LX/6WW;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1106388
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1106389
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1106405
    iget-object v0, p0, LX/6WW;->a:LX/1nq;

    invoke-virtual {v0, p1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 1106406
    return-void
.end method

.method public final a(ZIII)V
    .locals 2

    .prologue
    .line 1106369
    iget v0, p0, LX/6WW;->c:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 1106370
    if-eqz p1, :cond_1

    :goto_0
    iput p2, p0, LX/6WW;->d:I

    .line 1106371
    iput p3, p0, LX/6WW;->e:I

    .line 1106372
    :cond_0
    return-void

    .line 1106373
    :cond_1
    invoke-virtual {p0}, LX/6WW;->b()I

    move-result v0

    sub-int p2, p4, v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1106386
    iget-object v0, p0, LX/6WW;->b:Landroid/text/Layout;

    invoke-static {v0}, LX/1nt;->a(Landroid/text/Layout;)I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1106381
    iget-object v1, p0, LX/6WW;->a:LX/1nq;

    if-ne p1, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, LX/1nq;->b(Z)LX/1nq;

    .line 1106382
    iget-object v0, p0, LX/6WW;->a:LX/1nq;

    invoke-virtual {v0, p1}, LX/1nq;->f(I)LX/1nq;

    .line 1106383
    iget-object v0, p0, LX/6WW;->a:LX/1nq;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1nq;->a(Landroid/text/TextUtils$TruncateAt;)LX/1nq;

    .line 1106384
    return-void

    .line 1106385
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1106380
    iget v0, p0, LX/6WW;->c:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/6WW;->b:Landroid/text/Layout;

    invoke-static {v0}, LX/1nt;->b(Landroid/text/Layout;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 1106375
    iget v0, p0, LX/6WW;->c:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 1106376
    iget-object v0, p0, LX/6WW;->a:LX/1nq;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1nq;->a(I)LX/1nq;

    .line 1106377
    iget-object v0, p0, LX/6WW;->a:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    iput-object v0, p0, LX/6WW;->b:Landroid/text/Layout;

    .line 1106378
    :goto_0
    return-void

    .line 1106379
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/6WW;->b:Landroid/text/Layout;

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1106374
    invoke-virtual {p0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
