.class public final LX/66q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Z

.field public final b:LX/671;

.field private final c:LX/66k;

.field private final d:LX/65D;

.field public e:Z

.field public f:Z

.field public g:I

.field public h:J

.field public i:J

.field public j:Z

.field public k:Z

.field public l:Z

.field public final m:[B

.field public final n:[B


# direct methods
.method public constructor <init>(ZLX/671;LX/66k;)V
    .locals 2

    .prologue
    .line 1051031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1051032
    new-instance v0, LX/66p;

    invoke-direct {v0, p0}, LX/66p;-><init>(LX/66q;)V

    iput-object v0, p0, LX/66q;->d:LX/65D;

    .line 1051033
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, LX/66q;->m:[B

    .line 1051034
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, LX/66q;->n:[B

    .line 1051035
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051036
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "frameCallback == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051037
    :cond_1
    iput-boolean p1, p0, LX/66q;->a:Z

    .line 1051038
    iput-object p2, p0, LX/66q;->b:LX/671;

    .line 1051039
    iput-object p3, p0, LX/66q;->c:LX/66k;

    .line 1051040
    return-void
.end method

.method public static b(LX/66q;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1051041
    iget-boolean v0, p0, LX/66q;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051042
    :cond_0
    iget-object v0, p0, LX/66q;->b:LX/671;

    invoke-interface {v0}, LX/671;->h()B

    move-result v0

    and-int/lit16 v5, v0, 0xff

    .line 1051043
    and-int/lit8 v0, v5, 0xf

    iput v0, p0, LX/66q;->g:I

    .line 1051044
    and-int/lit16 v0, v5, 0x80

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/66q;->j:Z

    .line 1051045
    and-int/lit8 v0, v5, 0x8

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, LX/66q;->k:Z

    .line 1051046
    iget-boolean v0, p0, LX/66q;->k:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/66q;->j:Z

    if-nez v0, :cond_3

    .line 1051047
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Control frames must be final."

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    .line 1051048
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1051049
    goto :goto_1

    .line 1051050
    :cond_3
    and-int/lit8 v0, v5, 0x40

    if-eqz v0, :cond_5

    move v4, v1

    .line 1051051
    :goto_2
    and-int/lit8 v0, v5, 0x20

    if-eqz v0, :cond_6

    move v3, v1

    .line 1051052
    :goto_3
    and-int/lit8 v0, v5, 0x10

    if-eqz v0, :cond_7

    move v0, v1

    .line 1051053
    :goto_4
    if-nez v4, :cond_4

    if-nez v3, :cond_4

    if-eqz v0, :cond_8

    .line 1051054
    :cond_4
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Reserved flags are unsupported."

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v4, v2

    .line 1051055
    goto :goto_2

    :cond_6
    move v3, v2

    .line 1051056
    goto :goto_3

    :cond_7
    move v0, v2

    .line 1051057
    goto :goto_4

    .line 1051058
    :cond_8
    iget-object v0, p0, LX/66q;->b:LX/671;

    invoke-interface {v0}, LX/671;->h()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 1051059
    and-int/lit16 v3, v0, 0x80

    if-eqz v3, :cond_9

    :goto_5
    iput-boolean v1, p0, LX/66q;->l:Z

    .line 1051060
    iget-boolean v1, p0, LX/66q;->l:Z

    iget-boolean v2, p0, LX/66q;->a:Z

    if-ne v1, v2, :cond_a

    .line 1051061
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Client-sent frames must be masked. Server sent must not."

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    move v1, v2

    .line 1051062
    goto :goto_5

    .line 1051063
    :cond_a
    and-int/lit8 v0, v0, 0x7f

    int-to-long v0, v0

    iput-wide v0, p0, LX/66q;->h:J

    .line 1051064
    iget-wide v0, p0, LX/66q;->h:J

    const-wide/16 v2, 0x7e

    cmp-long v0, v0, v2

    if-nez v0, :cond_c

    .line 1051065
    iget-object v0, p0, LX/66q;->b:LX/671;

    invoke-interface {v0}, LX/671;->i()S

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xffff

    and-long/2addr v0, v2

    iput-wide v0, p0, LX/66q;->h:J

    .line 1051066
    :cond_b
    iput-wide v6, p0, LX/66q;->i:J

    .line 1051067
    iget-boolean v0, p0, LX/66q;->k:Z

    if-eqz v0, :cond_d

    iget-wide v0, p0, LX/66q;->h:J

    const-wide/16 v2, 0x7d

    cmp-long v0, v0, v2

    if-lez v0, :cond_d

    .line 1051068
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Control frame must be less than 125B."

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051069
    :cond_c
    iget-wide v0, p0, LX/66q;->h:J

    const-wide/16 v2, 0x7f

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    .line 1051070
    iget-object v0, p0, LX/66q;->b:LX/671;

    invoke-interface {v0}, LX/671;->k()J

    move-result-wide v0

    iput-wide v0, p0, LX/66q;->h:J

    .line 1051071
    iget-wide v0, p0, LX/66q;->h:J

    cmp-long v0, v0, v6

    if-gez v0, :cond_b

    .line 1051072
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Frame length 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/66q;->h:J

    .line 1051073
    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > 0x7FFFFFFFFFFFFFFF"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051074
    :cond_d
    iget-boolean v0, p0, LX/66q;->l:Z

    if-eqz v0, :cond_e

    .line 1051075
    iget-object v0, p0, LX/66q;->b:LX/671;

    iget-object v1, p0, LX/66q;->m:[B

    invoke-interface {v0, v1}, LX/671;->a([B)V

    .line 1051076
    :cond_e
    return-void
.end method

.method public static c(LX/66q;)V
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 1051077
    const/4 v0, 0x0

    .line 1051078
    iget-wide v2, p0, LX/66q;->i:J

    iget-wide v4, p0, LX/66q;->h:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1051079
    new-instance v6, LX/672;

    invoke-direct {v6}, LX/672;-><init>()V

    .line 1051080
    iget-boolean v0, p0, LX/66q;->a:Z

    if-eqz v0, :cond_2

    .line 1051081
    iget-object v0, p0, LX/66q;->b:LX/671;

    iget-wide v2, p0, LX/66q;->h:J

    invoke-interface {v0, v6, v2, v3}, LX/671;->b(LX/672;J)V

    move-object v0, v6

    .line 1051082
    :cond_0
    :goto_0
    iget v1, p0, LX/66q;->g:I

    packed-switch v1, :pswitch_data_0

    .line 1051083
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown control opcode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/66q;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051084
    :cond_1
    iget-object v0, p0, LX/66q;->n:[B

    int-to-long v1, v7

    iget-object v3, p0, LX/66q;->m:[B

    iget-wide v4, p0, LX/66q;->i:J

    invoke-static/range {v0 .. v5}, LX/66n;->a([BJ[BJ)V

    .line 1051085
    iget-object v0, p0, LX/66q;->n:[B

    invoke-virtual {v6, v0, v8, v7}, LX/672;->b([BII)LX/672;

    .line 1051086
    iget-wide v0, p0, LX/66q;->i:J

    int-to-long v2, v7

    add-long/2addr v0, v2

    iput-wide v0, p0, LX/66q;->i:J

    .line 1051087
    :cond_2
    iget-wide v0, p0, LX/66q;->i:J

    iget-wide v2, p0, LX/66q;->h:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_5

    .line 1051088
    iget-wide v0, p0, LX/66q;->h:J

    iget-wide v2, p0, LX/66q;->i:J

    sub-long/2addr v0, v2

    iget-object v2, p0, LX/66q;->n:[B

    array-length v2, v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 1051089
    iget-object v1, p0, LX/66q;->b:LX/671;

    iget-object v2, p0, LX/66q;->n:[B

    invoke-interface {v1, v2, v8, v0}, LX/671;->a([BII)I

    move-result v7

    .line 1051090
    const/4 v0, -0x1

    if-ne v7, v0, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1051091
    :pswitch_0
    iget-object v1, p0, LX/66q;->c:LX/66k;

    .line 1051092
    iget-object v2, v1, LX/66k;->b:Ljava/util/concurrent/Executor;

    new-instance v3, Lokhttp3/internal/ws/RealWebSocket$1$1;

    const-string v4, "OkHttp %s WebSocket Pong Reply"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v1, LX/66k;->c:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-direct {v3, v1, v4, v5, v0}, Lokhttp3/internal/ws/RealWebSocket$1$1;-><init>(LX/66k;Ljava/lang/String;[Ljava/lang/Object;LX/672;)V

    const v4, 0x565ed5d9

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1051093
    :goto_1
    :pswitch_1
    return-void

    .line 1051094
    :pswitch_2
    const/16 v2, 0x3e8

    .line 1051095
    const-string v1, ""

    .line 1051096
    if-eqz v0, :cond_4

    .line 1051097
    iget-wide v9, v0, LX/672;->b:J

    move-wide v4, v9

    .line 1051098
    const-wide/16 v6, 0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    .line 1051099
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Malformed close payload length of 1."

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051100
    :cond_3
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    .line 1051101
    invoke-virtual {v0}, LX/672;->i()S

    move-result v1

    .line 1051102
    invoke-static {v1, v8}, LX/66n;->a(IZ)V

    .line 1051103
    invoke-virtual {v0}, LX/672;->p()Ljava/lang/String;

    move-result-object v0

    .line 1051104
    :goto_2
    iget-object v2, p0, LX/66q;->c:LX/66k;

    invoke-virtual {v2, v1, v0}, LX/66k;->a(ILjava/lang/String;)V

    .line 1051105
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/66q;->e:Z

    goto :goto_1

    :cond_4
    move-object v0, v1

    move v1, v2

    goto :goto_2

    :cond_5
    move-object v0, v6

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static d(LX/66q;)V
    .locals 3

    .prologue
    .line 1051106
    iget v0, p0, LX/66q;->g:I

    packed-switch v0, :pswitch_data_0

    .line 1051107
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown opcode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/66q;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051108
    :pswitch_0
    sget-object v0, LX/66l;->a:LX/64s;

    .line 1051109
    :goto_0
    iget-object v1, p0, LX/66q;->d:LX/65D;

    invoke-static {v1}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v1

    .line 1051110
    new-instance v2, LX/66o;

    invoke-direct {v2, p0, v0, v1}, LX/66o;-><init>(LX/66q;LX/64s;LX/671;)V

    .line 1051111
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/66q;->f:Z

    .line 1051112
    iget-object v0, p0, LX/66q;->c:LX/66k;

    .line 1051113
    iget-object v1, v0, LX/66k;->a:LX/K09;

    invoke-virtual {v1, v2}, LX/K09;->a(LX/656;)V

    .line 1051114
    iget-boolean v0, p0, LX/66q;->f:Z

    if-nez v0, :cond_0

    .line 1051115
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Listener failed to call close on message payload."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051116
    :pswitch_1
    sget-object v0, LX/66l;->b:LX/64s;

    goto :goto_0

    .line 1051117
    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static e(LX/66q;)V
    .locals 1

    .prologue
    .line 1051118
    :goto_0
    iget-boolean v0, p0, LX/66q;->e:Z

    if-nez v0, :cond_0

    .line 1051119
    invoke-static {p0}, LX/66q;->b(LX/66q;)V

    .line 1051120
    iget-boolean v0, p0, LX/66q;->k:Z

    if-eqz v0, :cond_0

    .line 1051121
    invoke-static {p0}, LX/66q;->c(LX/66q;)V

    goto :goto_0

    .line 1051122
    :cond_0
    return-void
.end method
