.class public LX/6Ri;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0Uh;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/0Uh;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1090170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1090171
    iput-object p1, p0, LX/6Ri;->a:LX/0ad;

    .line 1090172
    iput-object p2, p0, LX/6Ri;->b:LX/0Uh;

    .line 1090173
    iget-object v0, p0, LX/6Ri;->a:LX/0ad;

    sget-char v1, LX/1v6;->P:C

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1090174
    const/4 v1, 0x1

    .line 1090175
    const/4 v0, 0x0

    .line 1090176
    if-nez v2, :cond_0

    .line 1090177
    iget-object v2, p0, LX/6Ri;->a:LX/0ad;

    sget-char v3, LX/1v6;->Q:C

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 1090178
    if-eqz v2, :cond_1

    .line 1090179
    const-string v2, "fullscreen"

    .line 1090180
    const-string v0, "on"

    iget-object v1, p0, LX/6Ri;->a:LX/0ad;

    sget-char v3, LX/1v6;->Q:C

    const-string v4, "on"

    invoke-interface {v1, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1090181
    const-string v0, "on"

    iget-object v3, p0, LX/6Ri;->a:LX/0ad;

    sget-char v4, LX/1v6;->R:C

    const-string v5, "off"

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1090182
    :cond_0
    :goto_1
    iput-object v2, p0, LX/6Ri;->c:Ljava/lang/String;

    .line 1090183
    iput-boolean v1, p0, LX/6Ri;->d:Z

    .line 1090184
    iput-boolean v0, p0, LX/6Ri;->e:Z

    .line 1090185
    return-void

    .line 1090186
    :cond_1
    iget-object v2, p0, LX/6Ri;->b:LX/0Uh;

    const/16 v3, 0x360

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v2, v2

    .line 1090187
    if-eqz v2, :cond_2

    .line 1090188
    const-string v2, "fullscreen"

    goto :goto_1

    .line 1090189
    :cond_2
    const-string v2, "letterbox"

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 1090190
    iget-object v0, p0, LX/6Ri;->c:Ljava/lang/String;

    const-string v1, "fullscreen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
