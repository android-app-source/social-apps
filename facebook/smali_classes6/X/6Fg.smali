.class public LX/6Fg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:LX/6Fb;

.field public r:Z

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0P1;LX/0P1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Fb;LX/0P1;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/6Fb;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1068746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1068747
    iput-object p1, p0, LX/6Fg;->a:Ljava/lang/String;

    .line 1068748
    iput-object p2, p0, LX/6Fg;->b:Ljava/lang/String;

    .line 1068749
    iput-object p3, p0, LX/6Fg;->c:LX/0Px;

    .line 1068750
    iput-object p4, p0, LX/6Fg;->d:LX/0P1;

    .line 1068751
    iput-object p5, p0, LX/6Fg;->e:LX/0P1;

    .line 1068752
    iput-object p6, p0, LX/6Fg;->g:Ljava/lang/String;

    .line 1068753
    iput-object p7, p0, LX/6Fg;->h:Ljava/lang/String;

    .line 1068754
    iput-object p8, p0, LX/6Fg;->i:Ljava/lang/String;

    .line 1068755
    iput-object p9, p0, LX/6Fg;->j:Ljava/lang/String;

    .line 1068756
    iput-object p10, p0, LX/6Fg;->k:Ljava/lang/String;

    .line 1068757
    iput-object p11, p0, LX/6Fg;->l:Ljava/lang/String;

    .line 1068758
    iput-object p12, p0, LX/6Fg;->m:Ljava/lang/String;

    .line 1068759
    iput-object p13, p0, LX/6Fg;->n:Ljava/lang/String;

    .line 1068760
    iput-object p14, p0, LX/6Fg;->o:Ljava/lang/String;

    .line 1068761
    move-object/from16 v0, p15

    iput-object v0, p0, LX/6Fg;->p:Ljava/lang/String;

    .line 1068762
    move-object/from16 v0, p16

    iput-object v0, p0, LX/6Fg;->q:LX/6Fb;

    .line 1068763
    move-object/from16 v0, p17

    iput-object v0, p0, LX/6Fg;->f:LX/0P1;

    .line 1068764
    move/from16 v0, p18

    iput-boolean v0, p0, LX/6Fg;->r:Z

    .line 1068765
    move-object/from16 v0, p19

    iput-object v0, p0, LX/6Fg;->s:Ljava/lang/String;

    .line 1068766
    move-object/from16 v0, p20

    iput-object v0, p0, LX/6Fg;->t:Ljava/lang/String;

    .line 1068767
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1068768
    iget-object v0, p0, LX/6Fg;->b:Ljava/lang/String;

    return-object v0
.end method
