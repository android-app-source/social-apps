.class public LX/6O4;
.super Landroid/database/CursorWrapper;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final e:I

.field private static final f:I

.field private static final g:I

.field private static final h:I

.field private static final i:I

.field private static final j:I

.field private static final k:I

.field private static final l:I

.field private static final m:I

.field private static final n:I

.field private static final o:I

.field private static final p:I


# instance fields
.field private final b:LX/3fh;

.field private final c:LX/2Rd;

.field private d:[Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1084766
    const-class v0, LX/6O4;

    sput-object v0, LX/6O4;->a:Ljava/lang/Class;

    .line 1084767
    const-string v0, "_id"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->e:I

    .line 1084768
    const-string v0, "_count"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->f:I

    .line 1084769
    const-string v0, "user_id"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->g:I

    .line 1084770
    const-string v0, "display_name"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->h:I

    .line 1084771
    const-string v0, "sort_name"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->i:I

    .line 1084772
    const-string v0, "user_image_url"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->j:I

    .line 1084773
    const-string v0, "contact_type"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->k:I

    .line 1084774
    const-string v0, "first_name"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->l:I

    .line 1084775
    const-string v0, "last_name"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->m:I

    .line 1084776
    const-string v0, "cell"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->n:I

    .line 1084777
    const-string v0, "other"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->o:I

    .line 1084778
    const-string v0, "search_token"

    invoke-static {v0}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, LX/6O4;->p:I

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;LX/3fh;LX/2Rd;)V
    .locals 1

    .prologue
    .line 1084779
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 1084780
    iput-object p2, p0, LX/6O4;->b:LX/3fh;

    .line 1084781
    iput-object p3, p0, LX/6O4;->c:LX/2Rd;

    .line 1084782
    sget-object v0, LX/0P0;->b:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/6O4;->d:[Ljava/lang/Object;

    .line 1084783
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1084784
    sget-object v0, LX/0P0;->d:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private a()Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1084785
    :try_start_0
    iget-object v0, p0, LX/6O4;->b:LX/3fh;

    const/4 v4, 0x0

    invoke-super {p0, v4}, Landroid/database/CursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/3fh;->a(Ljava/lang/String;)Lcom/facebook/contacts/graphql/Contact;

    move-result-object v5

    .line 1084786
    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v4

    .line 1084787
    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->f()Lcom/facebook/user/model/Name;

    move-result-object v0

    .line 1084788
    if-eqz v0, :cond_2

    .line 1084789
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v0

    .line 1084790
    :goto_0
    iget-object v6, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v7, LX/6O4;->e:I

    invoke-virtual {p0}, LX/6O4;->getPosition()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 1084791
    iget-object v6, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v7, LX/6O4;->f:I

    invoke-virtual {p0}, LX/6O4;->getCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 1084792
    iget-object v6, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v7, LX/6O4;->g:I

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    .line 1084793
    iget-object v6, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v7, LX/6O4;->h:I

    aput-object v4, v6, v7

    .line 1084794
    iget-object v4, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v6, LX/6O4;->i:I

    aput-object v0, v4, v6

    .line 1084795
    iget-object v0, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v4, LX/6O4;->j:I

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v4

    .line 1084796
    iget-object v0, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v4, LX/6O4;->k:I

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->A()LX/2RU;

    move-result-object v6

    aput-object v6, v0, v4

    .line 1084797
    iget-object v0, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v4, LX/6O4;->l:I

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v4

    .line 1084798
    iget-object v0, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v4, LX/6O4;->m:I

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/user/model/Name;->c()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v4

    .line 1084799
    iget-object v4, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v6, LX/6O4;->n:I

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v0

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactPhone;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactPhone;->c()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v4, v6

    .line 1084800
    iget-object v4, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v6, LX/6O4;->o:I

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/ContactPhone;

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/ContactPhone;->c()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v4, v6

    .line 1084801
    iget-object v0, p0, LX/6O4;->d:[Ljava/lang/Object;

    sget v3, LX/6O4;->p:I

    iget-object v4, p0, LX/6O4;->c:LX/2Rd;

    invoke-virtual {v5}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 1084802
    :goto_3
    return v0

    :cond_0
    move-object v0, v3

    .line 1084803
    goto :goto_1

    :cond_1
    move-object v0, v3

    .line 1084804
    goto :goto_2

    .line 1084805
    :catch_0
    move-exception v0

    .line 1084806
    sget-object v1, LX/6O4;->a:Ljava/lang/Class;

    const-string v3, "Error deserializing contact"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    .line 1084807
    goto :goto_3

    :cond_2
    move-object v0, v4

    goto/16 :goto_0
.end method


# virtual methods
.method public final copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 1

    .prologue
    .line 1084808
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getBlob(I)[B
    .locals 1

    .prologue
    .line 1084816
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getColumnCount()I
    .locals 1

    .prologue
    .line 1084809
    sget-object v0, LX/0P0;->b:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public final getColumnIndex(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1084810
    invoke-static {p1}, LX/6O4;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 1084811
    sget-object v0, LX/0P0;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1084812
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No column "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1084813
    :cond_0
    invoke-virtual {p0, p1}, LX/6O4;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getColumnName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1084814
    sget-object v0, LX/0P0;->b:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1084815
    sget-object v0, LX/0P0;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public final getDouble(I)D
    .locals 1

    .prologue
    .line 1084764
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getFloat(I)F
    .locals 1

    .prologue
    .line 1084765
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getInt(I)I
    .locals 2

    .prologue
    .line 1084763
    invoke-virtual {p0, p1}, LX/6O4;->getLong(I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final getLong(I)J
    .locals 2

    .prologue
    .line 1084762
    iget-object v0, p0, LX/6O4;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getShort(I)S
    .locals 2

    .prologue
    .line 1084761
    invoke-virtual {p0, p1}, LX/6O4;->getLong(I)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-short v0, v0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1084760
    iget-object v0, p0, LX/6O4;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getType(I)I
    .locals 2

    .prologue
    .line 1084759
    sget-object v0, LX/0P0;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final isNull(I)Z
    .locals 1

    .prologue
    .line 1084758
    iget-object v0, p0, LX/6O4;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final move(I)Z
    .locals 1

    .prologue
    .line 1084757
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->move(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/6O4;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final moveToFirst()Z
    .locals 1

    .prologue
    .line 1084756
    invoke-super {p0}, Landroid/database/CursorWrapper;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/6O4;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final moveToLast()Z
    .locals 1

    .prologue
    .line 1084755
    invoke-super {p0}, Landroid/database/CursorWrapper;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/6O4;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final moveToNext()Z
    .locals 1

    .prologue
    .line 1084754
    invoke-super {p0}, Landroid/database/CursorWrapper;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/6O4;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final moveToPosition(I)Z
    .locals 1

    .prologue
    .line 1084753
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/6O4;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final moveToPrevious()Z
    .locals 1

    .prologue
    .line 1084752
    invoke-super {p0}, Landroid/database/CursorWrapper;->moveToPrevious()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/6O4;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
