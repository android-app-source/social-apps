.class public final LX/6Fy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:LX/6G2;


# direct methods
.method public constructor <init>(LX/6G2;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 1069510
    iput-object p1, p0, LX/6Fy;->b:LX/6G2;

    iput-object p2, p0, LX/6Fy;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 1069511
    iget-object v0, p0, LX/6Fy;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1069512
    iget-object v0, p0, LX/6Fy;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1069513
    iget-object v0, p0, LX/6Fy;->b:LX/6G2;

    iget-object v0, v0, LX/6G2;->j:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_DID_DISMISS_CREATION_DIALOG:LX/6GY;

    invoke-virtual {v0, v1}, LX/6GZ;->a(LX/6GY;)V

    .line 1069514
    :cond_0
    return-void
.end method
