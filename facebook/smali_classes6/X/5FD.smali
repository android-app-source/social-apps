.class public final LX/5FD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    .line 884894
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 884895
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 884896
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 884897
    const/4 v2, 0x0

    .line 884898
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 884899
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884900
    :goto_1
    move v1, v2

    .line 884901
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 884902
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 884903
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 884904
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 884905
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 884906
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 884907
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 884908
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 884909
    :cond_3
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_2

    .line 884910
    :cond_4
    const-string v6, "title"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 884911
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    .line 884912
    :cond_5
    const-string v6, "url"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 884913
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_2

    .line 884914
    :cond_6
    const/4 v5, 0x3

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 884915
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 884916
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 884917
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 884918
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 884919
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 884920
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 884921
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 p3, 0x0

    .line 884922
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 884923
    invoke-virtual {p0, v1, p3}, LX/15i;->g(II)I

    move-result v2

    .line 884924
    if-eqz v2, :cond_0

    .line 884925
    const-string v2, "__type__"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884926
    invoke-static {p0, v1, p3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 884927
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 884928
    if-eqz v2, :cond_1

    .line 884929
    const-string p3, "title"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884930
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884931
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 884932
    if-eqz v2, :cond_2

    .line 884933
    const-string p3, "url"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 884934
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 884935
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 884936
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 884937
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 884938
    return-void
.end method
