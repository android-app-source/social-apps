.class public LX/5jA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 985878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 985879
    return-void
.end method

.method public static a(LX/362;)LX/5iE;
    .locals 1

    .prologue
    .line 985880
    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 985881
    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/TextParams;

    if-eqz v0, :cond_2

    .line 985882
    new-instance v0, LX/5jN;

    check-cast p0, Lcom/facebook/photos/creativeediting/model/TextParams;

    invoke-direct {v0, p0}, LX/5jN;-><init>(Lcom/facebook/photos/creativeediting/model/TextParams;)V

    .line 985883
    :goto_1
    return-object v0

    .line 985884
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 985885
    :cond_2
    instance-of v0, p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    if-eqz v0, :cond_3

    .line 985886
    new-instance v0, LX/5iF;

    check-cast p0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    invoke-direct {v0, p0}, LX/5iF;-><init>(Lcom/facebook/photos/creativeediting/model/DoodleParams;)V

    goto :goto_1

    .line 985887
    :cond_3
    new-instance v0, LX/5jG;

    check-cast p0, Lcom/facebook/photos/creativeediting/model/StickerParams;

    invoke-direct {v0, p0}, LX/5jG;-><init>(Lcom/facebook/photos/creativeediting/model/StickerParams;)V

    goto :goto_1
.end method
