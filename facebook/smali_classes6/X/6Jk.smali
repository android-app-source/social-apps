.class public LX/6Jk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/6Ia;

.field public final c:LX/6Ji;

.field private final d:LX/6Jj;

.field public final e:LX/6Jr;

.field public f:Z

.field public g:Z

.field public h:LX/6JC;

.field public i:LX/6JB;

.field public j:LX/6JR;

.field public k:LX/6Ik;

.field public l:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1075667
    const-class v0, LX/6Jk;

    sput-object v0, LX/6Jk;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/6Jr;LX/6Ia;LX/6JB;LX/6JR;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1075668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075669
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/6Jk;->l:Ljava/util/concurrent/CountDownLatch;

    .line 1075670
    iput-object p1, p0, LX/6Jk;->e:LX/6Jr;

    .line 1075671
    iput-object p2, p0, LX/6Jk;->b:LX/6Ia;

    .line 1075672
    invoke-virtual {p3}, LX/6JB;->a()LX/6JB;

    move-result-object v0

    iput-object v0, p0, LX/6Jk;->i:LX/6JB;

    .line 1075673
    new-instance v0, LX/6Ji;

    invoke-direct {v0, p0}, LX/6Ji;-><init>(LX/6Jk;)V

    iput-object v0, p0, LX/6Jk;->c:LX/6Ji;

    .line 1075674
    new-instance v0, LX/6Jj;

    invoke-direct {v0, p1}, LX/6Jj;-><init>(LX/6Jr;)V

    iput-object v0, p0, LX/6Jk;->d:LX/6Jj;

    .line 1075675
    invoke-static {p0, p4}, LX/6Jk;->a(LX/6Jk;LX/6JR;)V

    .line 1075676
    return-void
.end method

.method public static a(LX/6Jk;LX/6JR;)V
    .locals 4

    .prologue
    .line 1075677
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t pass null preview size"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1075678
    iget-object v0, p0, LX/6Jk;->j:LX/6JR;

    invoke-virtual {p1, v0}, LX/6JR;->a(LX/6JR;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1075679
    :goto_1
    return-void

    .line 1075680
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1075681
    :cond_1
    iput-object p1, p0, LX/6Jk;->j:LX/6JR;

    .line 1075682
    iget-object v0, p0, LX/6Jk;->e:LX/6Jr;

    new-instance v1, LX/7Sl;

    iget v2, p1, LX/6JR;->a:I

    iget v3, p1, LX/6JR;->b:I

    invoke-direct {v1, v2, v3}, LX/7Sl;-><init>(II)V

    invoke-virtual {v0, v1}, LX/6Jr;->a(LX/7Sb;)V

    goto :goto_1
.end method

.method public static h(LX/6Jk;)V
    .locals 2

    .prologue
    .line 1075683
    invoke-virtual {p0}, LX/6Jk;->c()LX/7Sm;

    move-result-object v0

    .line 1075684
    if-eqz v0, :cond_0

    .line 1075685
    iget-object v1, p0, LX/6Jk;->e:LX/6Jr;

    invoke-virtual {v1, v0}, LX/6Jr;->a(LX/7Sb;)V

    .line 1075686
    :cond_0
    return-void
.end method

.method public static i(LX/6Jk;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1075687
    iget-object v0, p0, LX/6Jk;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1075688
    :goto_0
    return-void

    .line 1075689
    :cond_0
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LX/6Jk;->l:Ljava/util/concurrent/CountDownLatch;

    .line 1075690
    :try_start_0
    iget-object v0, p0, LX/6Jk;->i:LX/6JB;

    new-instance v1, LX/6JA;

    iget-object v2, p0, LX/6Jk;->c:LX/6Ji;

    iget-object v2, v2, LX/6Ji;->b:Landroid/graphics/SurfaceTexture;

    iget-object v3, p0, LX/6Jk;->j:LX/6JR;

    iget v3, v3, LX/6JR;->a:I

    iget-object v4, p0, LX/6Jk;->j:LX/6JR;

    iget v4, v4, LX/6JR;->b:I

    invoke-direct {v1, v2, v3, v4}, LX/6JA;-><init>(Landroid/graphics/SurfaceTexture;II)V

    .line 1075691
    iget-object v2, v0, LX/6JB;->f:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075692
    iget-object v0, p0, LX/6Jk;->b:LX/6Ia;

    iget-object v1, p0, LX/6Jk;->i:LX/6JB;

    invoke-interface {v0, v1}, LX/6Ia;->a(LX/6JB;)V

    .line 1075693
    iget-object v0, p0, LX/6Jk;->d:LX/6Jj;

    iget-object v1, p0, LX/6Jk;->b:LX/6Ia;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1075694
    if-eqz v1, :cond_1

    move v2, v3

    :goto_1
    const-string v6, "Camera can\'t be null"

    invoke-static {v2, v6}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075695
    :try_start_1
    invoke-interface {v1}, LX/6Ia;->a()LX/6IP;

    move-result-object v2

    .line 1075696
    invoke-interface {v2}, LX/6IP;->f()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    :goto_2
    const-string v4, "Camera does not have any supported frame pixel formats"

    invoke-static {v3, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1075697
    invoke-interface {v2}, LX/6IP;->f()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v0, v2}, LX/6Ia;->a(LX/6Jj;I)V
    :try_end_1
    .catch LX/6JJ; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1075698
    :goto_3
    :try_start_2
    iget-object v0, p0, LX/6Jk;->e:LX/6Jr;

    invoke-virtual {p0}, LX/6Jk;->d()LX/7Sj;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Jr;->a(LX/7Sb;)V

    .line 1075699
    invoke-static {p0}, LX/6Jk;->h(LX/6Jk;)V

    .line 1075700
    iget-object v0, p0, LX/6Jk;->b:LX/6Ia;

    new-instance v1, LX/6Je;

    invoke-direct {v1, p0}, LX/6Je;-><init>(LX/6Jk;)V

    iget-object v2, p0, LX/6Jk;->h:LX/6JC;

    invoke-interface {v0, v1, v2}, LX/6Ia;->a(LX/6Ik;LX/6JC;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1075701
    iput-boolean v5, p0, LX/6Jk;->g:Z

    goto :goto_0

    .line 1075702
    :catch_0
    move-exception v0

    .line 1075703
    iget-object v1, p0, LX/6Jk;->l:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1075704
    throw v0

    :cond_1
    move v2, v4

    .line 1075705
    goto :goto_1

    :cond_2
    move v3, v4

    .line 1075706
    goto :goto_2

    .line 1075707
    :catch_1
    move-exception v2

    .line 1075708
    sget-object v3, LX/6Jj;->a:Ljava/lang/Class;

    const-string v4, "Failed to set camerapreview"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1075709
    iget-object v0, p0, LX/6Jk;->b:LX/6Ia;

    const-string v1, "Camera device should be non null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1075710
    iget-object v0, p0, LX/6Jk;->b:LX/6Ia;

    new-instance v1, LX/6Jd;

    invoke-direct {v1, p0}, LX/6Jd;-><init>(LX/6Jk;)V

    invoke-interface {v0, p1, v1}, LX/6Ia;->a(ILX/6Jd;)V

    .line 1075711
    return-void
.end method

.method public final c()LX/7Sm;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1075712
    :try_start_0
    iget-object v0, p0, LX/6Jk;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1075713
    :goto_0
    return-object v1

    .line 1075714
    :cond_0
    iget-object v0, p0, LX/6Jk;->b:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->a()LX/6IP;

    move-result-object v2

    .line 1075715
    new-instance v0, LX/7Sm;

    iget-object v3, p0, LX/6Jk;->b:LX/6Ia;

    invoke-interface {v3}, LX/6Ia;->g()I

    move-result v3

    invoke-interface {v2}, LX/6IP;->i()I

    move-result v2

    invoke-direct {v0, v3, v2}, LX/7Sm;-><init>(II)V
    :try_end_0
    .catch LX/6JJ; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v1, v0

    .line 1075716
    goto :goto_0

    .line 1075717
    :catch_0
    move-exception v0

    .line 1075718
    sget-object v2, LX/6Jk;->a:Ljava/lang/Class;

    const-string v3, "Failed to create rotation event"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public final d()LX/7Sj;
    .locals 2

    .prologue
    .line 1075719
    sget-object v0, LX/6Jf;->a:[I

    iget-object v1, p0, LX/6Jk;->b:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->d()LX/6JF;

    move-result-object v1

    invoke-virtual {v1}, LX/6JF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1075720
    new-instance v0, LX/7Sj;

    sget-object v1, LX/7Si;->FRONT:LX/7Si;

    invoke-direct {v0, v1}, LX/7Sj;-><init>(LX/7Si;)V

    :goto_0
    return-object v0

    .line 1075721
    :pswitch_0
    new-instance v0, LX/7Sj;

    sget-object v1, LX/7Si;->BACK:LX/7Si;

    invoke-direct {v0, v1}, LX/7Sj;-><init>(LX/7Si;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
