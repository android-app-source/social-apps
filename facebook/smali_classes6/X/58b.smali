.class public final LX/58b;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 847352
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 847353
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 847354
    :goto_0
    return v1

    .line 847355
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_5

    .line 847356
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 847357
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 847358
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 847359
    const-string v12, "amount"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 847360
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 847361
    :cond_1
    const-string v12, "currency"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 847362
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 847363
    :cond_2
    const-string v12, "offset"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 847364
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v9, v6

    move v6, v7

    goto :goto_1

    .line 847365
    :cond_3
    const-string v12, "offset_amount"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 847366
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 847367
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 847368
    :cond_5
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 847369
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 847370
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 847371
    :cond_6
    invoke-virtual {p1, v7, v10}, LX/186;->b(II)V

    .line 847372
    if-eqz v6, :cond_7

    .line 847373
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v9, v1}, LX/186;->a(III)V

    .line 847374
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 847375
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v6, v1

    move v0, v1

    move v8, v1

    move v9, v1

    move v10, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 847376
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 847377
    invoke-virtual {p0, p1, v4, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 847378
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    .line 847379
    const-string v2, "amount"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847380
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 847381
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 847382
    if-eqz v0, :cond_1

    .line 847383
    const-string v1, "currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847384
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847385
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4}, LX/15i;->a(III)I

    move-result v0

    .line 847386
    if-eqz v0, :cond_2

    .line 847387
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847388
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 847389
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 847390
    if-eqz v0, :cond_3

    .line 847391
    const-string v1, "offset_amount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847392
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847393
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 847394
    return-void
.end method
