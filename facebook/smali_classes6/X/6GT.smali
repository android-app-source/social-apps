.class public final LX/6GT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;)V
    .locals 0

    .prologue
    .line 1070561
    iput-object p1, p0, LX/6GT;->a:Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1070548
    iget-object v0, p0, LX/6GT;->a:Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1070549
    iget-object v0, p0, LX/6GT;->a:Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v0

    .line 1070550
    if-nez v0, :cond_0

    .line 1070551
    :goto_0
    return-void

    .line 1070552
    :cond_0
    iget-object v1, p0, LX/6GT;->a:Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->p:LX/6GU;

    invoke-virtual {v1, p2}, LX/6GU;->a(I)Lcom/facebook/bugreporter/activity/chooser/ChooserOption;

    move-result-object v1

    .line 1070553
    iget-object v2, p0, LX/6GT;->a:Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    iget-object v2, v2, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->o:LX/6GZ;

    .line 1070554
    iget-object v3, v1, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->e:LX/6GY;

    move-object v3, v3

    .line 1070555
    invoke-virtual {v2, v3}, LX/6GZ;->a(LX/6GY;)V

    .line 1070556
    iget-object v2, v1, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1070557
    sget-object v2, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1070558
    iget-object v1, p0, LX/6GT;->a:Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->m:LX/6G2;

    invoke-static {}, LX/6FY;->newBuilder()LX/6FX;

    move-result-object v2

    sget-object v3, LX/6Fb;->SETTINGS_REPORT_PROBLEM:LX/6Fb;

    invoke-virtual {v2, v3}, LX/6FX;->a(LX/6Fb;)LX/6FX;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/6FX;->a(Landroid/content/Context;)LX/6FX;

    move-result-object v0

    invoke-virtual {v0}, LX/6FX;->a()LX/6FY;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6G2;->a(LX/6FY;)V

    goto :goto_0

    .line 1070559
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1070560
    iget-object v1, p0, LX/6GT;->a:Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;

    iget-object v1, v1, Lcom/facebook/bugreporter/activity/chooser/ChooserFragment;->n:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
