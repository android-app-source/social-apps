.class public final LX/5v2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1020867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;)LX/5v2;
    .locals 2

    .prologue
    .line 1020868
    new-instance v0, LX/5v2;

    invoke-direct {v0}, LX/5v2;-><init>()V

    .line 1020869
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->m()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->a:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    .line 1020870
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->n()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->b:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    .line 1020871
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->c:Ljava/lang/String;

    .line 1020872
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->o()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 1020873
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->bk_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->e:Ljava/lang/String;

    .line 1020874
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->bl_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->f:Ljava/lang/String;

    .line 1020875
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->g:Ljava/lang/String;

    .line 1020876
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->h:Ljava/lang/String;

    .line 1020877
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/5v2;->i:Ljava/lang/String;

    .line 1020878
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v13, 0x0

    const/4 v2, 0x0

    .line 1020879
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1020880
    iget-object v1, p0, LX/5v2;->a:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1020881
    iget-object v3, p0, LX/5v2;->b:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultsPageFilterCustomPageValueModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1020882
    iget-object v5, p0, LX/5v2;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1020883
    iget-object v6, p0, LX/5v2;->d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1020884
    iget-object v7, p0, LX/5v2;->e:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1020885
    iget-object v8, p0, LX/5v2;->f:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1020886
    iget-object v9, p0, LX/5v2;->g:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1020887
    iget-object v10, p0, LX/5v2;->h:Ljava/lang/String;

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1020888
    iget-object v11, p0, LX/5v2;->i:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1020889
    const/16 v12, 0x9

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1020890
    invoke-virtual {v0, v13, v1}, LX/186;->b(II)V

    .line 1020891
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1020892
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1020893
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1020894
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1020895
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1020896
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1020897
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1020898
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1020899
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1020900
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1020901
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1020902
    invoke-virtual {v1, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1020903
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1020904
    new-instance v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    invoke-direct {v1, v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;-><init>(LX/15i;)V

    .line 1020905
    return-object v1
.end method
