.class public final LX/6Qr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:I

.field private f:I

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/directinstall/intent/DirectInstallAppDetails$Screenshot;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1088089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088090
    iput-object v0, p0, LX/6Qr;->a:Ljava/lang/String;

    .line 1088091
    iput-object v0, p0, LX/6Qr;->b:Ljava/lang/String;

    .line 1088092
    iput-object v0, p0, LX/6Qr;->c:Ljava/lang/String;

    .line 1088093
    iput-object v0, p0, LX/6Qr;->d:Ljava/lang/String;

    .line 1088094
    iput-object v0, p0, LX/6Qr;->g:Ljava/lang/String;

    .line 1088095
    iput-object v0, p0, LX/6Qr;->h:Ljava/lang/String;

    .line 1088096
    iput-object v0, p0, LX/6Qr;->i:Ljava/lang/String;

    .line 1088097
    iput-object v0, p0, LX/6Qr;->j:Ljava/lang/String;

    .line 1088098
    iput-object v0, p0, LX/6Qr;->k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    .line 1088099
    iput-object v0, p0, LX/6Qr;->l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/directinstall/intent/DirectInstallAppDetails;
    .locals 17

    .prologue
    .line 1088100
    new-instance v1, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Qr;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/6Qr;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/6Qr;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/6Qr;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v6, v0, LX/6Qr;->e:I

    move-object/from16 v0, p0

    iget v7, v0, LX/6Qr;->f:I

    move-object/from16 v0, p0

    iget-object v8, v0, LX/6Qr;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/6Qr;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/6Qr;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/6Qr;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/6Qr;->k:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/6Qr;->l:Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/6Qr;->m:LX/0Px;

    if-nez v14, :cond_0

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v14

    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, LX/6Qr;->n:LX/0Px;

    if-nez v15, :cond_1

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v15

    :goto_1
    const/16 v16, 0x0

    invoke-direct/range {v1 .. v16}, Lcom/facebook/directinstall/intent/DirectInstallAppDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;Lcom/facebook/directinstall/intent/DirectInstallAppDetails$TextWithEntities;LX/0Px;LX/0Px;B)V

    return-object v1

    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, LX/6Qr;->m:LX/0Px;

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, LX/6Qr;->n:LX/0Px;

    goto :goto_1
.end method
