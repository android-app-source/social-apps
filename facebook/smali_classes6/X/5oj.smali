.class public LX/5oj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26y;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/5oj;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0ag;

.field private final c:LX/0ej;

.field private final d:LX/26w;

.field private final e:I

.field private final f:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

.field private g:I

.field private h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1007499
    const-class v0, LX/5oj;

    sput-object v0, LX/5oj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0ag;LX/0ej;LX/26w;ILcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;)V
    .locals 1

    .prologue
    .line 1007491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007492
    const/4 v0, -0x1

    iput v0, p0, LX/5oj;->g:I

    .line 1007493
    iput-object p1, p0, LX/5oj;->b:LX/0ag;

    .line 1007494
    iput-object p2, p0, LX/5oj;->c:LX/0ej;

    .line 1007495
    iput-object p3, p0, LX/5oj;->d:LX/26w;

    .line 1007496
    iput p4, p0, LX/5oj;->e:I

    .line 1007497
    iput-object p5, p0, LX/5oj;->f:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    .line 1007498
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 1007477
    iput-object p1, p0, LX/5oj;->h:Ljava/lang/String;

    .line 1007478
    iget-object v0, p0, LX/5oj;->b:LX/0ag;

    invoke-virtual {v0, p1}, LX/0ag;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/5oj;->g:I

    .line 1007479
    iget v0, p0, LX/5oj;->e:I

    iget v1, p0, LX/5oj;->g:I

    if-ne v0, v1, :cond_0

    .line 1007480
    add-int/lit8 v0, p2, 0x0

    .line 1007481
    add-int/lit8 v1, p2, 0x1

    .line 1007482
    add-int/lit8 v2, p2, 0x2

    .line 1007483
    add-int/lit8 v3, p2, 0x3

    .line 1007484
    iget-object v4, p0, LX/5oj;->d:LX/26w;

    sget-object v5, LX/0oc;->OVERRIDE:LX/0oc;

    iget-object v6, p0, LX/5oj;->f:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    .line 1007485
    iget-boolean p1, v6, LX/2Wx;->d:Z

    move v6, p1

    .line 1007486
    invoke-virtual {v4, v5, v0, v6}, LX/26w;->a(LX/0oc;IZ)V

    .line 1007487
    iget-object v0, p0, LX/5oj;->d:LX/26w;

    sget-object v4, LX/0oc;->OVERRIDE:LX/0oc;

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v1, v5}, LX/26w;->a(LX/0oc;IZ)V

    .line 1007488
    iget-object v0, p0, LX/5oj;->d:LX/26w;

    sget-object v1, LX/0oc;->OVERRIDE:LX/0oc;

    iget-object v4, p0, LX/5oj;->f:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    invoke-virtual {v4}, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, LX/26w;->a(LX/0oc;ILjava/lang/String;)V

    .line 1007489
    iget-object v0, p0, LX/5oj;->d:LX/26w;

    sget-object v1, LX/0oc;->OVERRIDE:LX/0oc;

    iget-object v2, p0, LX/5oj;->f:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    invoke-virtual {v2}, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, LX/26w;->a(LX/0oc;ILjava/lang/String;)V

    .line 1007490
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;IIZ)V
    .locals 7

    .prologue
    .line 1007468
    iget-object v3, p0, LX/5oj;->c:LX/0ej;

    iget-object v4, p0, LX/5oj;->d:LX/26w;

    sget-object v5, LX/0oc;->ASSIGNED:LX/0oc;

    move v0, p2

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    .line 1007469
    iget v0, p0, LX/5oj;->e:I

    iget v1, p0, LX/5oj;->g:I

    if-eq v0, v1, :cond_1

    .line 1007470
    iget-object v3, p0, LX/5oj;->c:LX/0ej;

    iget-object v4, p0, LX/5oj;->d:LX/26w;

    sget-object v5, LX/0oc;->OVERRIDE:LX/0oc;

    move v0, p2

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, LX/2Wy;->a(IIILX/0ej;LX/26w;LX/0oc;)V

    .line 1007471
    :cond_0
    :goto_0
    return-void

    .line 1007472
    :cond_1
    if-nez p4, :cond_0

    .line 1007473
    iget-object v0, p0, LX/5oj;->f:Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;

    invoke-virtual {v0}, Lcom/facebook/abtest/qe/bootstrap/data/QuickExperimentInfo;->f()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1007474
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1007475
    :cond_2
    iget-object v0, p0, LX/5oj;->d:LX/26w;

    invoke-virtual {v0, p2}, LX/26w;->a(I)V

    goto :goto_0

    .line 1007476
    :cond_3
    iget-object v0, p0, LX/5oj;->d:LX/26w;

    sget-object v1, LX/0oc;->OVERRIDE:LX/0oc;

    iget-object v2, p0, LX/5oj;->h:Ljava/lang/String;

    move-object v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v6}, LX/26w;->a(LX/0oc;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    goto :goto_0
.end method
