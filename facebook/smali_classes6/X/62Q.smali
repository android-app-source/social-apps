.class public final LX/62Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1lD;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1041583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1041584
    sget-object v0, LX/1lD;->LOAD_FINISHED:LX/1lD;

    iput-object v0, p0, LX/62Q;->a:LX/1lD;

    .line 1041585
    iput-object v1, p0, LX/62Q;->b:Ljava/lang/String;

    .line 1041586
    iput-object v1, p0, LX/62Q;->c:Ljava/lang/String;

    .line 1041587
    const/4 v0, 0x0

    iput v0, p0, LX/62Q;->d:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;
    .locals 2

    .prologue
    .line 1041588
    iget-object v0, p0, LX/62Q;->a:LX/1lD;

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    if-ne v0, v1, :cond_0

    .line 1041589
    iget-object v0, p0, LX/62Q;->b:Ljava/lang/String;

    const-string v1, "Error message cannot be null."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1041590
    :cond_0
    new-instance v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    invoke-direct {v0, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;-><init>(LX/62Q;)V

    return-object v0
.end method
