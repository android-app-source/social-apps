.class public LX/6Ok;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1085479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085480
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;)LX/0XK;
    .locals 2
    .param p0    # Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1085481
    if-nez p0, :cond_0

    .line 1085482
    sget-object v0, LX/0XK;->UNSET:LX/0XK;

    .line 1085483
    :goto_0
    return-object v0

    .line 1085484
    :cond_0
    sget-object v0, LX/6Oj;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1085485
    sget-object v0, LX/0XK;->UNSET:LX/0XK;

    goto :goto_0

    .line 1085486
    :pswitch_0
    sget-object v0, LX/0XK;->CONNECTED:LX/0XK;

    goto :goto_0

    .line 1085487
    :pswitch_1
    sget-object v0, LX/0XK;->NO_CONNECTION:LX/0XK;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/facebook/contacts/graphql/Contact;)Lcom/facebook/user/model/User;
    .locals 10

    .prologue
    .line 1085488
    new-instance v3, LX/0XI;

    invoke-direct {v3}, LX/0XI;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v4

    .line 1085489
    iput-object v4, v3, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 1085490
    move-object v3, v3

    .line 1085491
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->K()Ljava/lang/String;

    move-result-object v4

    .line 1085492
    iput-object v4, v3, LX/0XI;->l:Ljava/lang/String;

    .line 1085493
    move-object v3, v3

    .line 1085494
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->F()J

    move-result-wide v5

    .line 1085495
    iput-wide v5, v3, LX/0XI;->an:J

    .line 1085496
    move-object v3, v3

    .line 1085497
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->p()Z

    move-result v4

    .line 1085498
    iput-boolean v4, v3, LX/0XI;->Q:Z

    .line 1085499
    move-object v3, v3

    .line 1085500
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v4

    .line 1085501
    iput-object v4, v3, LX/0XI;->n:Ljava/lang/String;

    .line 1085502
    move-object v3, v3

    .line 1085503
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->h()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->i()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    .line 1085504
    :cond_0
    const/4 v4, 0x0

    .line 1085505
    :goto_0
    move-object v4, v4

    .line 1085506
    iput-object v4, v3, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 1085507
    move-object v3, v3

    .line 1085508
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->m()F

    move-result v4

    .line 1085509
    iput v4, v3, LX/0XI;->t:F

    .line 1085510
    move-object v4, v3

    .line 1085511
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->x()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v5, :cond_2

    const/4 v3, 0x1

    .line 1085512
    :goto_1
    iput-boolean v3, v4, LX/0XI;->F:Z

    .line 1085513
    move-object v3, v4

    .line 1085514
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->r()LX/03R;

    move-result-object v4

    .line 1085515
    iput-object v4, v3, LX/0XI;->u:LX/03R;

    .line 1085516
    move-object v3, v3

    .line 1085517
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->s()Z

    move-result v4

    .line 1085518
    iput-boolean v4, v3, LX/0XI;->z:Z

    .line 1085519
    move-object v3, v3

    .line 1085520
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->t()J

    move-result-wide v5

    .line 1085521
    iput-wide v5, v3, LX/0XI;->D:J

    .line 1085522
    move-object v3, v3

    .line 1085523
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->w()J

    move-result-wide v5

    .line 1085524
    iput-wide v5, v3, LX/0XI;->E:J

    .line 1085525
    move-object v3, v3

    .line 1085526
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->D()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->C()I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0XI;->a(II)LX/0XI;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->H()J

    move-result-wide v5

    .line 1085527
    iput-wide v5, v3, LX/0XI;->X:J

    .line 1085528
    move-object v3, v3

    .line 1085529
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->I()Z

    move-result v4

    .line 1085530
    iput-boolean v4, v3, LX/0XI;->Y:Z

    .line 1085531
    move-object v3, v3

    .line 1085532
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->N()Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    move-result-object v4

    invoke-static {v4}, LX/6Ok;->a(Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;)LX/0XK;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0XI;->a(LX/0XK;)LX/0XI;

    move-result-object v3

    const-string v4, "user"

    .line 1085533
    iput-object v4, v3, LX/0XI;->y:Ljava/lang/String;

    .line 1085534
    move-object v3, v3

    .line 1085535
    move-object v0, v3

    .line 1085536
    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1085537
    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 1085538
    :goto_2
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0

    .line 1085539
    :cond_1
    sget-object v1, LX/0XG;->FACEBOOK_CONTACT:LX/0XG;

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    new-instance v4, Lcom/facebook/user/model/PicSquare;

    new-instance v5, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->j()I

    move-result v6

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    new-instance v6, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->k()I

    move-result v7

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->h()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    new-instance v7, Lcom/facebook/user/model/PicSquareUrlWithSize;

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->l()I

    move-result v8

    invoke-virtual {p0}, Lcom/facebook/contacts/graphql/Contact;->i()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    invoke-direct {v4, v5, v6, v7}, Lcom/facebook/user/model/PicSquare;-><init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V

    goto/16 :goto_0
.end method
