.class public final LX/59q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 854393
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 854394
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 854395
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 854396
    invoke-static {p0, p1}, LX/59q;->b(LX/15w;LX/186;)I

    move-result v1

    .line 854397
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 854398
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 854399
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 854400
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 854401
    :goto_0
    return v1

    .line 854402
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 854403
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 854404
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 854405
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 854406
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 854407
    const-string v8, "media"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 854408
    invoke-static {p0, p1}, LX/59n;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 854409
    :cond_2
    const-string v8, "source"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 854410
    const/4 v7, 0x0

    .line 854411
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_c

    .line 854412
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 854413
    :goto_2
    move v5, v7

    .line 854414
    goto :goto_1

    .line 854415
    :cond_3
    const-string v8, "style_list"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 854416
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 854417
    :cond_4
    const-string v8, "target"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 854418
    invoke-static {p0, p1}, LX/59p;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 854419
    :cond_5
    const-string v8, "title"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 854420
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 854421
    :cond_6
    const-string v8, "url"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 854422
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 854423
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 854424
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 854425
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 854426
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 854427
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 854428
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 854429
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 854430
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1

    .line 854431
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 854432
    :cond_a
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 854433
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 854434
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 854435
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_a

    if-eqz v8, :cond_a

    .line 854436
    const-string v9, "text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 854437
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 854438
    :cond_b
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 854439
    invoke-virtual {p1, v7, v5}, LX/186;->b(II)V

    .line 854440
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_2

    :cond_c
    move v5, v7

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 854441
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 854442
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 854443
    if-eqz v0, :cond_0

    .line 854444
    const-string v1, "media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854445
    invoke-static {p0, v0, p2, p3}, LX/59n;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 854446
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 854447
    if-eqz v0, :cond_2

    .line 854448
    const-string v1, "source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854449
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 854450
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 854451
    if-eqz v1, :cond_1

    .line 854452
    const-string v3, "text"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854453
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 854454
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 854455
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 854456
    if-eqz v0, :cond_3

    .line 854457
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854458
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 854459
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 854460
    if-eqz v0, :cond_4

    .line 854461
    const-string v1, "target"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854462
    invoke-static {p0, v0, p2, p3}, LX/59p;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 854463
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 854464
    if-eqz v0, :cond_5

    .line 854465
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854466
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 854467
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 854468
    if-eqz v0, :cond_6

    .line 854469
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854470
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 854471
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 854472
    return-void
.end method
