.class public final LX/6P8;
.super LX/3dH;
.source ""


# instance fields
.field public final b:LX/3d2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3d2",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/3d2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3d2",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Lcom/facebook/graphql/enums/GraphQLSavedState;

.field public final synthetic f:LX/3iW;


# direct methods
.method public constructor <init>(LX/3iW;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 3

    .prologue
    .line 1086030
    iput-object p1, p0, LX/6P8;->f:LX/3iW;

    iput-object p2, p0, LX/6P8;->d:Ljava/lang/String;

    iput-object p3, p0, LX/6P8;->e:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-direct {p0}, LX/3dH;-><init>()V

    .line 1086031
    new-instance v0, LX/3d2;

    const-class v1, Lcom/facebook/graphql/model/GraphQLPlace;

    new-instance v2, LX/6P6;

    invoke-direct {v2, p0}, LX/6P6;-><init>(LX/6P8;)V

    invoke-direct {v0, v1, v2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    iput-object v0, p0, LX/6P8;->b:LX/3d2;

    .line 1086032
    new-instance v0, LX/3d2;

    const-class v1, Lcom/facebook/graphql/model/GraphQLNode;

    new-instance v2, LX/6P7;

    invoke-direct {v2, p0}, LX/6P7;-><init>(LX/6P8;)V

    invoke-direct {v0, v1, v2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    iput-object v0, p0, LX/6P8;->c:LX/3d2;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1086033
    iget-object v0, p0, LX/6P8;->b:LX/3d2;

    iget-object v1, p0, LX/6P8;->c:LX/3d2;

    invoke-virtual {v1, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086034
    iget-object v0, p0, LX/6P8;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086035
    const-string v0, "FeedStoryCacheAdapter.updateSaveStatus"

    return-object v0
.end method
