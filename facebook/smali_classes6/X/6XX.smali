.class public LX/6XX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/proxygen/HTTPTransportCallback;


# instance fields
.field public a:LX/2BD;


# direct methods
.method public constructor <init>(LX/2BD;)V
    .locals 0

    .prologue
    .line 1108501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1108502
    iput-object p1, p0, LX/6XX;->a:LX/2BD;

    .line 1108503
    return-void
.end method


# virtual methods
.method public final bodyBytesGenerated(J)V
    .locals 1

    .prologue
    .line 1108499
    iget-object v0, p0, LX/6XX;->a:LX/2BD;

    invoke-interface {v0, p1, p2}, LX/2BD;->a(J)V

    .line 1108500
    return-void
.end method

.method public final bodyBytesReceived(J)V
    .locals 0

    .prologue
    .line 1108498
    return-void
.end method

.method public final firstByteFlushed()V
    .locals 0

    .prologue
    .line 1108497
    return-void
.end method

.method public final firstHeaderByteFlushed()V
    .locals 0

    .prologue
    .line 1108496
    return-void
.end method

.method public final getEnabledCallbackFlag()I
    .locals 1

    .prologue
    .line 1108494
    iget-object v0, p0, LX/6XX;->a:LX/2BD;

    invoke-interface {v0}, LX/2BD;->a()V

    .line 1108495
    const/16 v0, 0x40

    return v0
.end method

.method public final headerBytesGenerated(JJ)V
    .locals 0

    .prologue
    .line 1108493
    return-void
.end method

.method public final headerBytesReceived(JJ)V
    .locals 0

    .prologue
    .line 1108490
    return-void
.end method

.method public final lastByteAcked(J)V
    .locals 0

    .prologue
    .line 1108492
    return-void
.end method

.method public final lastByteFlushed()V
    .locals 0

    .prologue
    .line 1108491
    return-void
.end method
