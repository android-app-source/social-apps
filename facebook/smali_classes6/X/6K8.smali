.class public LX/6K8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Jx;


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/6Jt;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/6JG;

.field public d:LX/6K9;

.field private e:Z

.field private f:Ljava/lang/Runnable;

.field private g:LX/6Ia;


# direct methods
.method public constructor <init>(LX/6Jt;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 1076455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076456
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/6K8;->b:Ljava/lang/ref/WeakReference;

    .line 1076457
    iput-object p2, p0, LX/6K8;->a:Landroid/os/Handler;

    .line 1076458
    sget-object v0, LX/6K9;->STOPPED:LX/6K9;

    iput-object v0, p0, LX/6K8;->d:LX/6K9;

    .line 1076459
    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1076449
    invoke-static {}, LX/6K8;->d()V

    .line 1076450
    iget-boolean v0, p0, LX/6K8;->e:Z

    if-eqz v0, :cond_0

    .line 1076451
    iput-object p1, p0, LX/6K8;->f:Ljava/lang/Runnable;

    .line 1076452
    :goto_0
    return-void

    .line 1076453
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6K8;->e:Z

    .line 1076454
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public static a$redex0(LX/6K8;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1076438
    invoke-static {}, LX/6K8;->d()V

    .line 1076439
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6K8;->e:Z

    .line 1076440
    if-eqz p1, :cond_1

    .line 1076441
    iget-object v0, p0, LX/6K8;->f:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1076442
    iget-object v0, p0, LX/6K8;->f:Ljava/lang/Runnable;

    .line 1076443
    iput-object v1, p0, LX/6K8;->f:Ljava/lang/Runnable;

    .line 1076444
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/6K8;->e:Z

    .line 1076445
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1076446
    :cond_0
    :goto_0
    return-void

    .line 1076447
    :cond_1
    iput-object v1, p0, LX/6K8;->f:Ljava/lang/Runnable;

    .line 1076448
    invoke-static {p0}, LX/6K8;->c(LX/6K8;)V

    goto :goto_0
.end method

.method public static b(LX/6K8;Ljava/io/File;LX/6JG;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1076424
    iget-object v0, p0, LX/6K8;->b:Ljava/lang/ref/WeakReference;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1076425
    iget-object v0, p0, LX/6K8;->g:LX/6Ia;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1076426
    iget-object v0, p0, LX/6K8;->d:LX/6K9;

    sget-object v1, LX/6K9;->RECORDING:LX/6K9;

    if-ne v0, v1, :cond_0

    .line 1076427
    invoke-static {p0, v2}, LX/6K8;->a$redex0(LX/6K8;Z)V

    .line 1076428
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recording video has already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1076429
    :cond_0
    iget-object v0, p0, LX/6K8;->d:LX/6K9;

    sget-object v1, LX/6K9;->PREPARED:LX/6K9;

    if-eq v0, v1, :cond_1

    .line 1076430
    invoke-static {p0, v2}, LX/6K8;->a$redex0(LX/6K8;Z)V

    .line 1076431
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "prepare() must be called before start"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1076432
    :cond_1
    sget-object v0, LX/6K9;->RECORDING_STARTED:LX/6K9;

    iput-object v0, p0, LX/6K8;->d:LX/6K9;

    .line 1076433
    iget-object v0, p0, LX/6K8;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/6Jt;->b(I)V

    .line 1076434
    iget-object v0, p0, LX/6K8;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    invoke-virtual {v0}, LX/6Jt;->f()V

    .line 1076435
    iput-object p2, p0, LX/6K8;->c:LX/6JG;

    .line 1076436
    iget-object v0, p0, LX/6K8;->g:LX/6Ia;

    new-instance v1, LX/6K7;

    invoke-direct {v1, p0}, LX/6K7;-><init>(LX/6K8;)V

    invoke-interface {v0, p1, v1}, LX/6Ia;->a(Ljava/io/File;LX/6JG;)V

    .line 1076437
    return-void
.end method

.method public static c(LX/6K8;)V
    .locals 2

    .prologue
    .line 1076416
    iget-object v0, p0, LX/6K8;->b:Ljava/lang/ref/WeakReference;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1076417
    iget-object v0, p0, LX/6K8;->d:LX/6K9;

    sget-object v1, LX/6K9;->RECORDING:LX/6K9;

    if-eq v0, v1, :cond_0

    .line 1076418
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/6K8;->a$redex0(LX/6K8;Z)V

    .line 1076419
    :goto_0
    return-void

    .line 1076420
    :cond_0
    iget-object v0, p0, LX/6K8;->g:LX/6Ia;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1076421
    sget-object v0, LX/6K9;->STOP_STARTED:LX/6K9;

    iput-object v0, p0, LX/6K8;->d:LX/6K9;

    .line 1076422
    iget-object v0, p0, LX/6K8;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, LX/6Jt;->b(I)V

    .line 1076423
    iget-object v0, p0, LX/6K8;->g:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->e()V

    goto :goto_0
.end method

.method private static d()V
    .locals 2

    .prologue
    .line 1076413
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1076414
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecorderCoordinatorImpl methods must be called on the UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1076415
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/6K9;
    .locals 1

    .prologue
    .line 1076404
    iget-object v0, p0, LX/6K8;->d:LX/6K9;

    return-object v0
.end method

.method public final a(LX/6Ia;)V
    .locals 0

    .prologue
    .line 1076411
    iput-object p1, p0, LX/6K8;->g:LX/6Ia;

    .line 1076412
    return-void
.end method

.method public final a(LX/6JR;LX/6JU;I)V
    .locals 1

    .prologue
    .line 1076409
    new-instance v0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$1;

    invoke-direct {v0, p0, p2}, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$1;-><init>(LX/6K8;LX/6JU;)V

    invoke-direct {p0, v0}, LX/6K8;->a(Ljava/lang/Runnable;)V

    .line 1076410
    return-void
.end method

.method public final a(Ljava/io/File;LX/6JG;)V
    .locals 1

    .prologue
    .line 1076407
    new-instance v0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$3;-><init>(LX/6K8;Ljava/io/File;LX/6JG;)V

    invoke-direct {p0, v0}, LX/6K8;->a(Ljava/lang/Runnable;)V

    .line 1076408
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1076405
    new-instance v0, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$4;

    invoke-direct {v0, p0}, Lcom/facebook/cameracore/capturecoordinator/RecorderCoordinatorImplNative$4;-><init>(LX/6K8;)V

    invoke-direct {p0, v0}, LX/6K8;->a(Ljava/lang/Runnable;)V

    .line 1076406
    return-void
.end method
