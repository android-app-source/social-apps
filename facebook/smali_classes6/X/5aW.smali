.class public final LX/5aW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 955091
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 955092
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 955093
    :goto_0
    return v1

    .line 955094
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 955095
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 955096
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 955097
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 955098
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 955099
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 955100
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 955101
    :cond_2
    const-string v4, "messaging_actor"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 955102
    invoke-static {p0, p1}, LX/5aV;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 955103
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 955104
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 955105
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 955106
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 955107
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 955108
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955109
    if-eqz v0, :cond_0

    .line 955110
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955111
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955112
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955113
    if-eqz v0, :cond_1

    .line 955114
    const-string v1, "messaging_actor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955115
    invoke-static {p0, v0, p2}, LX/5aV;->a(LX/15i;ILX/0nX;)V

    .line 955116
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 955117
    return-void
.end method
