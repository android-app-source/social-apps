.class public LX/6Pe;
.super LX/6PU;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/enums/StoryVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V
    .locals 0
    .param p2    # Lcom/facebook/graphql/enums/StoryVisibility;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1086279
    invoke-direct {p0, p1}, LX/6PU;-><init>(Ljava/lang/String;)V

    .line 1086280
    iput-object p2, p0, LX/6Pe;->a:Lcom/facebook/graphql/enums/StoryVisibility;

    .line 1086281
    iput p3, p0, LX/6Pe;->b:I

    .line 1086282
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;LX/4Yr;)V
    .locals 2

    .prologue
    .line 1086283
    iget-object v0, p0, LX/6Pe;->a:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Pe;->a:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/StoryVisibility;->name()Ljava/lang/String;

    move-result-object v0

    .line 1086284
    :goto_0
    invoke-virtual {p2, v0}, LX/4Yr;->b(Ljava/lang/String;)V

    .line 1086285
    iget v0, p0, LX/6Pe;->b:I

    .line 1086286
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p0, "local_story_visible_height"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p0, p1}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086287
    return-void

    .line 1086288
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086289
    const-string v0, "SetStoryVisibilityMutatingVisitor"

    return-object v0
.end method
