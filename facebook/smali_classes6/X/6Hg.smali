.class public final LX/6Hg;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/6Hh;


# direct methods
.method public constructor <init>(LX/6Hh;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1072599
    iput-object p1, p0, LX/6Hg;->a:LX/6Hh;

    .line 1072600
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1072601
    return-void
.end method

.method public static a(LX/6Hg;Ljava/util/ArrayList;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/hardware/Camera$Face;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1072602
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Hg;->a:LX/6Hh;

    iget-object v2, v2, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 1072603
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v11

    .line 1072604
    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v4, v3, :cond_1

    .line 1072605
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/Camera$Face;

    iget-object v12, v3, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    .line 1072606
    invoke-virtual {v12}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v13

    .line 1072607
    invoke-virtual {v12}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v14

    .line 1072608
    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1072609
    const/4 v5, 0x0

    .line 1072610
    move-object/from16 v0, p0

    iget-object v3, v0, LX/6Hg;->a:LX/6Hh;

    iget-object v3, v3, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v15

    const/4 v3, 0x0

    move v10, v3

    :goto_1
    if-ge v10, v15, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/6Hg;->a:LX/6Hh;

    iget-object v3, v3, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6Hi;

    .line 1072611
    iget-object v6, v3, LX/6Hi;->f:Landroid/graphics/Rect;

    .line 1072612
    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v6

    invoke-static {v13, v14, v7, v6}, LX/6Hh;->a(FFFF)D

    move-result-wide v6

    .line 1072613
    cmpg-double v16, v6, v8

    if-gez v16, :cond_7

    .line 1072614
    :goto_2
    add-int/lit8 v5, v10, 0x1

    move v10, v5

    move-wide v8, v6

    move-object v5, v3

    goto :goto_1

    .line 1072615
    :cond_0
    invoke-virtual {v11, v12, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1072616
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1072617
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1072618
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 1072619
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v3, 0x0

    move v4, v3

    :goto_3
    if-ge v4, v5, :cond_2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6Hi;

    .line 1072620
    move-object/from16 v0, p0

    iget-object v6, v0, LX/6Hg;->a:LX/6Hh;

    iget-object v6, v6, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1072621
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 1072622
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Hg;->a:LX/6Hh;

    iget-object v2, v2, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 1072623
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v4

    .line 1072624
    invoke-virtual {v11}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 1072625
    invoke-virtual {v11, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6Hi;

    .line 1072626
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v7

    iget v8, v3, LX/6Hi;->d:F

    iget v3, v3, LX/6Hi;->e:F

    invoke-static {v6, v7, v8, v3}, LX/6Hh;->a(FFFF)D

    move-result-wide v6

    .line 1072627
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v4, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1072628
    :cond_3
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 1072629
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v2

    new-array v5, v2, [Ljava/lang/Double;

    .line 1072630
    const/4 v2, 0x0

    .line 1072631
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    .line 1072632
    aput-object v2, v5, v3

    .line 1072633
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .line 1072634
    goto :goto_5

    .line 1072635
    :cond_4
    invoke-static {v5}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 1072636
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/6Hg;->a:LX/6Hh;

    iget-object v3, v3, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int v6, v2, v3

    .line 1072637
    const/4 v2, 0x0

    move v3, v2

    :goto_6
    if-ge v3, v6, :cond_5

    .line 1072638
    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v3

    aget-object v2, v5, v2

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 1072639
    new-instance v7, LX/6Hi;

    invoke-direct {v7, v2}, LX/6Hi;-><init>(Landroid/graphics/Rect;)V

    .line 1072640
    move-object/from16 v0, p0

    iget-object v8, v0, LX/6Hg;->a:LX/6Hh;

    iget-object v8, v8, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1072641
    invoke-virtual {v11, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1072642
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 1072643
    :cond_5
    invoke-virtual {v11}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    .line 1072644
    invoke-virtual {v11, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6Hi;

    .line 1072645
    invoke-virtual {v3, v2}, LX/6Hi;->a(Landroid/graphics/Rect;)V

    goto :goto_7

    .line 1072646
    :cond_6
    return-void

    :cond_7
    move-object v3, v5

    move-wide v6, v8

    goto/16 :goto_2
.end method

.method public static a(LX/6Hg;Z)V
    .locals 2

    .prologue
    .line 1072647
    if-eqz p1, :cond_0

    .line 1072648
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    iget-object v0, v0, LX/6Hh;->i:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1072649
    :goto_0
    return-void

    .line 1072650
    :cond_0
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    iget-object v0, v0, LX/6Hh;->i:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public static b(LX/6Hg;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1072651
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    iget-object v0, v0, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_3

    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    iget-object v0, v0, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Hi;

    .line 1072652
    iget-boolean v4, v0, LX/6Hi;->h:Z

    if-eqz v4, :cond_2

    .line 1072653
    iget v4, v0, LX/6Hi;->d:F

    iget v5, v0, LX/6Hi;->e:F

    iget v6, v0, LX/6Hi;->i:F

    iget v7, v0, LX/6Hi;->j:F

    invoke-static {v4, v5, v6, v7}, LX/6Hh;->a(FFFF)D

    move-result-wide v4

    .line 1072654
    const-wide/high16 v6, 0x405e000000000000L    # 120.0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1

    .line 1072655
    iput-boolean v2, v0, LX/6Hi;->h:Z

    .line 1072656
    iput v2, v0, LX/6Hi;->k:I

    .line 1072657
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1072658
    :cond_1
    iget v4, v0, LX/6Hi;->k:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, LX/6Hi;->k:I

    goto :goto_1

    .line 1072659
    :cond_2
    iget-wide v4, v0, LX/6Hi;->g:D

    const-wide/high16 v6, 0x404e000000000000L    # 60.0

    cmpg-double v4, v4, v6

    if-gez v4, :cond_0

    .line 1072660
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/6Hi;->h:Z

    .line 1072661
    iget v4, v0, LX/6Hi;->d:F

    iput v4, v0, LX/6Hi;->i:F

    .line 1072662
    iget v4, v0, LX/6Hi;->e:F

    iput v4, v0, LX/6Hi;->j:F

    goto :goto_1

    .line 1072663
    :cond_3
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    .line 1072664
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1072665
    sget-object v0, LX/6Hh;->a:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown TrackingHandler message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1072666
    :goto_0
    return-void

    .line 1072667
    :pswitch_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1072668
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    .line 1072669
    iput-boolean v1, v0, LX/6Hh;->f:Z

    .line 1072670
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    iget-object v0, v0, LX/6Hh;->e:[Landroid/hardware/Camera$Face;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 1072671
    invoke-static {p0, v1}, LX/6Hg;->a(LX/6Hg;Z)V

    .line 1072672
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    .line 1072673
    iput-boolean v2, v0, LX/6Hh;->f:Z

    .line 1072674
    :goto_1
    goto :goto_0

    .line 1072675
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    iget-object v0, v0, LX/6Hh;->e:[Landroid/hardware/Camera$Face;

    array-length v0, v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1072676
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    iget-object v1, v0, LX/6Hh;->e:[Landroid/hardware/Camera$Face;

    array-length v4, v1

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_2

    aget-object v5, v1, v0

    .line 1072677
    iget v6, v5, Landroid/hardware/Camera$Face;->score:I

    const/16 p1, 0x28

    if-lt v6, p1, :cond_1

    .line 1072678
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1072679
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1072680
    :cond_2
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    iget-object v0, v0, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 1072681
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_3
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Face;

    .line 1072682
    iget-object v0, v0, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    .line 1072683
    iget-object v5, p0, LX/6Hg;->a:LX/6Hh;

    iget-object v5, v5, LX/6Hh;->d:Ljava/util/ArrayList;

    new-instance v6, LX/6Hi;

    invoke-direct {v6, v0}, LX/6Hi;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1072684
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1072685
    :cond_3
    invoke-static {p0, v2}, LX/6Hg;->a(LX/6Hg;Z)V

    .line 1072686
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    .line 1072687
    iput-boolean v2, v0, LX/6Hh;->f:Z

    .line 1072688
    goto :goto_1

    .line 1072689
    :cond_4
    invoke-static {p0, v3}, LX/6Hg;->a(LX/6Hg;Ljava/util/ArrayList;)V

    .line 1072690
    invoke-static {p0}, LX/6Hg;->b(LX/6Hg;)V

    .line 1072691
    invoke-static {p0, v2}, LX/6Hg;->a(LX/6Hg;Z)V

    .line 1072692
    iget-object v0, p0, LX/6Hg;->a:LX/6Hh;

    .line 1072693
    iput-boolean v2, v0, LX/6Hh;->f:Z

    .line 1072694
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
