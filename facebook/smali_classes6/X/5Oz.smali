.class public final enum LX/5Oz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Oz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Oz;

.field public static final enum CI_PROMOTION_EMPTY_FEED:LX/5Oz;

.field public static final enum FEED_PYMK:LX/5Oz;

.field public static final enum FEED_REQUESTS:LX/5Oz;

.field public static final enum FIND_FRIENDS_BOOKMARK:LX/5Oz;

.field public static final enum FRIENDLIST_EMPTY_STATE:LX/5Oz;

.field public static final enum FRIENDLIST_UPSELL:LX/5Oz;

.field public static final enum FRIENDS_BOOKMARK:LX/5Oz;

.field public static final enum FRIENDS_TAB_SEE_ALL_PYMK:LX/5Oz;

.field public static final enum FRIENDS_TAB_SPROUT_LAUNCHER:LX/5Oz;

.field public static final enum FRIEND_REQUEST_RICH_NOTIFICATION:LX/5Oz;

.field public static final enum FRIEND_REQUEST_TRAY_NOTIFICATION:LX/5Oz;

.field public static final enum INCOMING_FR_QP:LX/5Oz;

.field public static final enum NEW_USER_FRIENDING_DIALOG:LX/5Oz;

.field public static final enum NOTIFICATIONS_FRIENDING_TAB_FIND_FRIENDS:LX/5Oz;

.field public static final enum NOTIFICATIONS_FRIENDING_TAB_SEE_ALL_FRIEND_REQUESTS:LX/5Oz;

.field public static final enum REACTION_FRIEND_REQUESTS_CARD:LX/5Oz;

.field public static final enum TIMELINE_ABOUT_FRIENDS_APP:LX/5Oz;

.field public static final enum TIMELINE_PENDING_REQUESTS_PROMPT:LX/5Oz;

.field public static final enum TIMELINE_PYMK:LX/5Oz;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 909793
    new-instance v0, LX/5Oz;

    const-string v1, "CI_PROMOTION_EMPTY_FEED"

    invoke-direct {v0, v1, v3}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->CI_PROMOTION_EMPTY_FEED:LX/5Oz;

    .line 909794
    new-instance v0, LX/5Oz;

    const-string v1, "FEED_REQUESTS"

    invoke-direct {v0, v1, v4}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FEED_REQUESTS:LX/5Oz;

    .line 909795
    new-instance v0, LX/5Oz;

    const-string v1, "FEED_PYMK"

    invoke-direct {v0, v1, v5}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FEED_PYMK:LX/5Oz;

    .line 909796
    new-instance v0, LX/5Oz;

    const-string v1, "FIND_FRIENDS_BOOKMARK"

    invoke-direct {v0, v1, v6}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FIND_FRIENDS_BOOKMARK:LX/5Oz;

    .line 909797
    new-instance v0, LX/5Oz;

    const-string v1, "FRIEND_REQUEST_RICH_NOTIFICATION"

    invoke-direct {v0, v1, v7}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FRIEND_REQUEST_RICH_NOTIFICATION:LX/5Oz;

    .line 909798
    new-instance v0, LX/5Oz;

    const-string v1, "FRIEND_REQUEST_TRAY_NOTIFICATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FRIEND_REQUEST_TRAY_NOTIFICATION:LX/5Oz;

    .line 909799
    new-instance v0, LX/5Oz;

    const-string v1, "FRIENDLIST_EMPTY_STATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FRIENDLIST_EMPTY_STATE:LX/5Oz;

    .line 909800
    new-instance v0, LX/5Oz;

    const-string v1, "FRIENDLIST_UPSELL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FRIENDLIST_UPSELL:LX/5Oz;

    .line 909801
    new-instance v0, LX/5Oz;

    const-string v1, "FRIENDS_BOOKMARK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FRIENDS_BOOKMARK:LX/5Oz;

    .line 909802
    new-instance v0, LX/5Oz;

    const-string v1, "FRIENDS_TAB_SEE_ALL_PYMK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FRIENDS_TAB_SEE_ALL_PYMK:LX/5Oz;

    .line 909803
    new-instance v0, LX/5Oz;

    const-string v1, "FRIENDS_TAB_SPROUT_LAUNCHER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->FRIENDS_TAB_SPROUT_LAUNCHER:LX/5Oz;

    .line 909804
    new-instance v0, LX/5Oz;

    const-string v1, "INCOMING_FR_QP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->INCOMING_FR_QP:LX/5Oz;

    .line 909805
    new-instance v0, LX/5Oz;

    const-string v1, "NEW_USER_FRIENDING_DIALOG"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->NEW_USER_FRIENDING_DIALOG:LX/5Oz;

    .line 909806
    new-instance v0, LX/5Oz;

    const-string v1, "NOTIFICATIONS_FRIENDING_TAB_FIND_FRIENDS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->NOTIFICATIONS_FRIENDING_TAB_FIND_FRIENDS:LX/5Oz;

    .line 909807
    new-instance v0, LX/5Oz;

    const-string v1, "NOTIFICATIONS_FRIENDING_TAB_SEE_ALL_FRIEND_REQUESTS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->NOTIFICATIONS_FRIENDING_TAB_SEE_ALL_FRIEND_REQUESTS:LX/5Oz;

    .line 909808
    new-instance v0, LX/5Oz;

    const-string v1, "TIMELINE_ABOUT_FRIENDS_APP"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->TIMELINE_ABOUT_FRIENDS_APP:LX/5Oz;

    .line 909809
    new-instance v0, LX/5Oz;

    const-string v1, "TIMELINE_PENDING_REQUESTS_PROMPT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->TIMELINE_PENDING_REQUESTS_PROMPT:LX/5Oz;

    .line 909810
    new-instance v0, LX/5Oz;

    const-string v1, "TIMELINE_PYMK"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->TIMELINE_PYMK:LX/5Oz;

    .line 909811
    new-instance v0, LX/5Oz;

    const-string v1, "REACTION_FRIEND_REQUESTS_CARD"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, LX/5Oz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Oz;->REACTION_FRIEND_REQUESTS_CARD:LX/5Oz;

    .line 909812
    const/16 v0, 0x13

    new-array v0, v0, [LX/5Oz;

    sget-object v1, LX/5Oz;->CI_PROMOTION_EMPTY_FEED:LX/5Oz;

    aput-object v1, v0, v3

    sget-object v1, LX/5Oz;->FEED_REQUESTS:LX/5Oz;

    aput-object v1, v0, v4

    sget-object v1, LX/5Oz;->FEED_PYMK:LX/5Oz;

    aput-object v1, v0, v5

    sget-object v1, LX/5Oz;->FIND_FRIENDS_BOOKMARK:LX/5Oz;

    aput-object v1, v0, v6

    sget-object v1, LX/5Oz;->FRIEND_REQUEST_RICH_NOTIFICATION:LX/5Oz;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5Oz;->FRIEND_REQUEST_TRAY_NOTIFICATION:LX/5Oz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5Oz;->FRIENDLIST_EMPTY_STATE:LX/5Oz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5Oz;->FRIENDLIST_UPSELL:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5Oz;->FRIENDS_BOOKMARK:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5Oz;->FRIENDS_TAB_SEE_ALL_PYMK:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/5Oz;->FRIENDS_TAB_SPROUT_LAUNCHER:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/5Oz;->INCOMING_FR_QP:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/5Oz;->NEW_USER_FRIENDING_DIALOG:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/5Oz;->NOTIFICATIONS_FRIENDING_TAB_FIND_FRIENDS:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/5Oz;->NOTIFICATIONS_FRIENDING_TAB_SEE_ALL_FRIEND_REQUESTS:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/5Oz;->TIMELINE_ABOUT_FRIENDS_APP:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/5Oz;->TIMELINE_PENDING_REQUESTS_PROMPT:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/5Oz;->TIMELINE_PYMK:LX/5Oz;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/5Oz;->REACTION_FRIEND_REQUESTS_CARD:LX/5Oz;

    aput-object v2, v0, v1

    sput-object v0, LX/5Oz;->$VALUES:[LX/5Oz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 909813
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Oz;
    .locals 1

    .prologue
    .line 909814
    const-class v0, LX/5Oz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Oz;

    return-object v0
.end method

.method public static values()[LX/5Oz;
    .locals 1

    .prologue
    .line 909815
    sget-object v0, LX/5Oz;->$VALUES:[LX/5Oz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Oz;

    return-object v0
.end method
