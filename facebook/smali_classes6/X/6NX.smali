.class public LX/6NX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/6NX;


# instance fields
.field private final a:LX/0So;

.field private final b:LX/6Nc;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Pk;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Lcom/facebook/omnistore/Collection;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0So;LX/6Nc;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0So;",
            "LX/6Nc;",
            "LX/0Or",
            "<",
            "LX/2Pk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1083591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1083592
    iput-object p1, p0, LX/6NX;->a:LX/0So;

    .line 1083593
    iput-object p2, p0, LX/6NX;->b:LX/6Nc;

    .line 1083594
    iput-object p3, p0, LX/6NX;->c:LX/0Or;

    .line 1083595
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6NX;->d:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1083596
    return-void
.end method

.method public static a(LX/0QB;)LX/6NX;
    .locals 6

    .prologue
    .line 1083597
    sget-object v0, LX/6NX;->e:LX/6NX;

    if-nez v0, :cond_1

    .line 1083598
    const-class v1, LX/6NX;

    monitor-enter v1

    .line 1083599
    :try_start_0
    sget-object v0, LX/6NX;->e:LX/6NX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1083600
    if-eqz v2, :cond_0

    .line 1083601
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1083602
    new-instance v5, LX/6NX;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    .line 1083603
    new-instance p0, LX/6Nc;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v4}, LX/6Nc;-><init>(LX/0Zb;)V

    .line 1083604
    move-object v4, p0

    .line 1083605
    check-cast v4, LX/6Nc;

    const/16 p0, 0xea6

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, LX/6NX;-><init>(LX/0So;LX/6Nc;LX/0Or;)V

    .line 1083606
    move-object v0, v5

    .line 1083607
    sput-object v0, LX/6NX;->e:LX/6NX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1083608
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1083609
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1083610
    :cond_1
    sget-object v0, LX/6NX;->e:LX/6NX;

    return-object v0

    .line 1083611
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1083612
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/omnistore/Collection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1083613
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6NX;->d:Lcom/google/common/util/concurrent/SettableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(LX/6Nb;)Lcom/facebook/omnistore/Collection;
    .locals 10

    .prologue
    .line 1083614
    invoke-direct {p0}, LX/6NX;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1083615
    :try_start_0
    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1083616
    const v0, -0x736c168a

    invoke-static {v1, v0}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/Collection;

    .line 1083617
    :goto_0
    return-object v0

    .line 1083618
    :cond_0
    iget-object v0, p0, LX/6NX;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 1083619
    iget-object v0, p0, LX/6NX;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Pk;

    invoke-virtual {v0}, LX/2Pk;->init()V

    .line 1083620
    const v0, -0x250a1af7

    invoke-static {v1, v0}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/omnistore/Collection;

    .line 1083621
    iget-object v1, p0, LX/6NX;->a:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    .line 1083622
    iget-object v1, p0, LX/6NX;->b:LX/6Nc;

    sub-long v2, v4, v2

    .line 1083623
    # getter for: LX/6Nb;->mCaller:Ljava/lang/String;
    invoke-static {p1}, LX/6Nb;->access$000(LX/6Nb;)Ljava/lang/String;

    .line 1083624
    iget-object v6, v1, LX/6Nc;->b:LX/0Zb;

    const-string v7, "contacts_waited_on_collection"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 1083625
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1083626
    const-string v7, "call_site"

    iget-object v8, p1, LX/6Nb;->mCaller:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1083627
    const-string v7, "wait_time_ms"

    invoke-virtual {v6, v7, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1083628
    const-string v7, "process_uptime_ms"

    invoke-static {}, Landroid/os/Process;->getElapsedCpuTime()J

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1083629
    invoke-virtual {v6}, LX/0oG;->d()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1083630
    :cond_1
    goto :goto_0

    .line 1083631
    :catch_0
    move-exception v0

    .line 1083632
    :goto_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1083633
    :catch_1
    move-exception v0

    goto :goto_1
.end method
