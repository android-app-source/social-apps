.class public LX/6ZU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/1wc;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1111149
    const-class v0, LX/6ZU;

    sput-object v0, LX/6ZU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1wc;LX/0Or;Landroid/content/Context;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/location/gmsupsell/IsGooglePlayServicesLocationDialogAllowed;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1wc;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1111150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1111151
    iput-object p1, p0, LX/6ZU;->b:LX/1wc;

    .line 1111152
    iput-object p2, p0, LX/6ZU;->c:LX/0Or;

    .line 1111153
    iput-object p3, p0, LX/6ZU;->d:Landroid/content/Context;

    .line 1111154
    return-void
.end method

.method public static a$redex0(LX/6ZU;LX/2wX;LX/2si;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "LX/2si;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/6ZS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1111155
    invoke-virtual {p1}, LX/2wX;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1111156
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Google Api Client unexpectedly disconnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1111157
    :cond_0
    new-instance v0, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v0}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    .line 1111158
    iget-object v1, p2, LX/2si;->a:LX/0yF;

    invoke-static {v1}, LX/2xI;->a(LX/0yF;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    .line 1111159
    new-instance v1, LX/7a1;

    invoke-direct {v1}, LX/7a1;-><init>()V

    iget-object v2, v1, LX/7a1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 1111160
    iget-boolean v1, p2, LX/2si;->b:Z

    iput-boolean v1, v0, LX/7a1;->b:Z

    move-object v1, v0

    .line 1111161
    iget-boolean v0, p2, LX/2si;->c:Z

    if-eqz v0, :cond_1

    .line 1111162
    iget-object v0, p0, LX/6ZU;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1111163
    const-string v2, "android.hardware.bluetooth"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    .line 1111164
    const-string v3, "android.hardware.bluetooth_le"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    .line 1111165
    const-string v4, "android.permission.BLUETOOTH"

    iget-object v5, p0, LX/6ZU;->d:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 1111166
    const-string v5, "android.permission.BLUETOOTH_ADMIN"

    iget-object p2, p0, LX/6ZU;->d:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, v5, p2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1111167
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    if-nez v4, :cond_2

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1111168
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v1, LX/7a1;->c:Z

    move-object v0, v1

    .line 1111169
    new-instance v1, Lcom/google/android/gms/location/LocationSettingsRequest;

    iget-object v2, v0, LX/7a1;->a:Ljava/util/ArrayList;

    iget-boolean v3, v0, LX/7a1;->b:Z

    iget-boolean v4, v0, LX/7a1;->c:Z

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/location/LocationSettingsRequest;-><init>(Ljava/util/List;ZZ)V

    move-object v0, v1

    .line 1111170
    sget-object v1, LX/2vm;->d:LX/2vy;

    invoke-interface {v1, p1, v0}, LX/2vy;->a(LX/2wX;Lcom/google/android/gms/location/LocationSettingsRequest;)LX/2wg;

    move-result-object v0

    .line 1111171
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 1111172
    new-instance v2, LX/3K6;

    invoke-direct {v2, p0, p1, v1}, LX/3K6;-><init>(LX/6ZU;LX/2wX;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v0, v2}, LX/2wg;->a(LX/27U;)V

    .line 1111173
    return-object v1

    .line 1111174
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6ZU;
    .locals 4

    .prologue
    .line 1111175
    new-instance v2, LX/6ZU;

    invoke-static {p0}, LX/1wc;->b(LX/0QB;)LX/1wc;

    move-result-object v0

    check-cast v0, LX/1wc;

    const/16 v1, 0x14d5

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {v2, v0, v3, v1}, LX/6ZU;-><init>(LX/1wc;LX/0Or;Landroid/content/Context;)V

    .line 1111176
    return-object v2
.end method


# virtual methods
.method public final a(LX/2si;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2si;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/6ZS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1111177
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111178
    iget-object v0, p0, LX/6ZU;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1111179
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GK check failed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1111180
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/6ZU;->b:LX/1wc;

    sget-object v1, LX/2vm;->a:LX/2vs;

    invoke-virtual {v0, v1}, LX/1wc;->a(LX/2vs;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/6ZR;

    invoke-direct {v1, p0, p1}, LX/6ZR;-><init>(LX/6ZU;LX/2si;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
