.class public final LX/56g;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 838566
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 838567
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 838568
    :goto_0
    return v1

    .line 838569
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 838570
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 838571
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 838572
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 838573
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 838574
    const-string v8, "context_card_photo"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 838575
    invoke-static {p0, p1}, LX/56e;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 838576
    :cond_2
    const-string v8, "context_content"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 838577
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 838578
    :cond_3
    const-string v8, "context_content_style"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 838579
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 838580
    :cond_4
    const-string v8, "context_cta"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 838581
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 838582
    :cond_5
    const-string v8, "context_image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 838583
    invoke-static {p0, p1}, LX/56f;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 838584
    :cond_6
    const-string v8, "context_title"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 838585
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 838586
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 838587
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 838588
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 838589
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 838590
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 838591
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 838592
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 838593
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 838594
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 838595
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 838596
    if-eqz v0, :cond_0

    .line 838597
    const-string v1, "context_card_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838598
    invoke-static {p0, v0, p2, p3}, LX/56e;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 838599
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 838600
    if-eqz v0, :cond_1

    .line 838601
    const-string v0, "context_content"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838602
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 838603
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 838604
    if-eqz v0, :cond_2

    .line 838605
    const-string v0, "context_content_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838606
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 838607
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 838608
    if-eqz v0, :cond_3

    .line 838609
    const-string v1, "context_cta"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838610
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 838611
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 838612
    if-eqz v0, :cond_4

    .line 838613
    const-string v1, "context_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838614
    invoke-static {p0, v0, p2}, LX/56f;->a(LX/15i;ILX/0nX;)V

    .line 838615
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 838616
    if-eqz v0, :cond_5

    .line 838617
    const-string v1, "context_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 838618
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 838619
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 838620
    return-void
.end method
