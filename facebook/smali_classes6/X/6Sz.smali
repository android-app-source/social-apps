.class public final LX/6Sz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1097059
    const/4 v12, 0x0

    .line 1097060
    const/4 v11, 0x0

    .line 1097061
    const/4 v10, 0x0

    .line 1097062
    const/4 v9, 0x0

    .line 1097063
    const/4 v8, 0x0

    .line 1097064
    const/4 v7, 0x0

    .line 1097065
    const/4 v6, 0x0

    .line 1097066
    const/4 v5, 0x0

    .line 1097067
    const/4 v4, 0x0

    .line 1097068
    const/4 v3, 0x0

    .line 1097069
    const/4 v2, 0x0

    .line 1097070
    const/4 v1, 0x0

    .line 1097071
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1097072
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1097073
    const/4 v1, 0x0

    .line 1097074
    :goto_0
    return v1

    .line 1097075
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1097076
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 1097077
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1097078
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1097079
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1097080
    const-string v14, "can_viewer_react"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1097081
    const/4 v2, 0x1

    .line 1097082
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 1097083
    :cond_2
    const-string v14, "comments"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1097084
    invoke-static/range {p0 .. p1}, LX/6Sw;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1097085
    :cond_3
    const-string v14, "important_reactors"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1097086
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1097087
    :cond_4
    const-string v14, "likers"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1097088
    invoke-static/range {p0 .. p1}, LX/6Sx;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1097089
    :cond_5
    const-string v14, "reactors"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1097090
    invoke-static/range {p0 .. p1}, LX/5DL;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1097091
    :cond_6
    const-string v14, "reshares"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1097092
    invoke-static/range {p0 .. p1}, LX/6Sy;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1097093
    :cond_7
    const-string v14, "supported_reactions"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1097094
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1097095
    :cond_8
    const-string v14, "top_reactions"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1097096
    invoke-static/range {p0 .. p1}, LX/5DK;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1097097
    :cond_9
    const-string v14, "viewer_acts_as_person"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1097098
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1097099
    :cond_a
    const-string v14, "viewer_feedback_reaction_key"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1097100
    const/4 v1, 0x1

    .line 1097101
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v3

    goto/16 :goto_1

    .line 1097102
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1097103
    if-eqz v2, :cond_c

    .line 1097104
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 1097105
    :cond_c
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1097106
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1097107
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1097108
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1097109
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1097110
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1097111
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1097112
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1097113
    if-eqz v1, :cond_d

    .line 1097114
    const/16 v1, 0x9

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3, v2}, LX/186;->a(III)V

    .line 1097115
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1097116
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1097117
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 1097118
    if-eqz v0, :cond_0

    .line 1097119
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097120
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1097121
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097122
    if-eqz v0, :cond_1

    .line 1097123
    const-string v1, "comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097124
    invoke-static {p0, v0, p2}, LX/6Sw;->a(LX/15i;ILX/0nX;)V

    .line 1097125
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097126
    if-eqz v0, :cond_2

    .line 1097127
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097128
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1097129
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097130
    if-eqz v0, :cond_3

    .line 1097131
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097132
    invoke-static {p0, v0, p2}, LX/6Sx;->a(LX/15i;ILX/0nX;)V

    .line 1097133
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097134
    if-eqz v0, :cond_4

    .line 1097135
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097136
    invoke-static {p0, v0, p2}, LX/5DL;->a(LX/15i;ILX/0nX;)V

    .line 1097137
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097138
    if-eqz v0, :cond_5

    .line 1097139
    const-string v1, "reshares"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097140
    invoke-static {p0, v0, p2}, LX/6Sy;->a(LX/15i;ILX/0nX;)V

    .line 1097141
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097142
    if-eqz v0, :cond_6

    .line 1097143
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097144
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1097145
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097146
    if-eqz v0, :cond_7

    .line 1097147
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097148
    invoke-static {p0, v0, p2, p3}, LX/5DK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1097149
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1097150
    if-eqz v0, :cond_8

    .line 1097151
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097152
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 1097153
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1097154
    if-eqz v0, :cond_9

    .line 1097155
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1097156
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1097157
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1097158
    return-void
.end method
