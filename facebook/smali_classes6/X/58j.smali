.class public final LX/58j;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 848560
    const/16 v17, 0x0

    .line 848561
    const/16 v16, 0x0

    .line 848562
    const/4 v15, 0x0

    .line 848563
    const/4 v14, 0x0

    .line 848564
    const/4 v13, 0x0

    .line 848565
    const/4 v12, 0x0

    .line 848566
    const/4 v11, 0x0

    .line 848567
    const/4 v10, 0x0

    .line 848568
    const/4 v9, 0x0

    .line 848569
    const/4 v8, 0x0

    .line 848570
    const/4 v7, 0x0

    .line 848571
    const/4 v6, 0x0

    .line 848572
    const/4 v5, 0x0

    .line 848573
    const/4 v4, 0x0

    .line 848574
    const/4 v3, 0x0

    .line 848575
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 848576
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 848577
    const/4 v3, 0x0

    .line 848578
    :goto_0
    return v3

    .line 848579
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 848580
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_10

    .line 848581
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 848582
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 848583
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 848584
    const-string v19, "action_links"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 848585
    invoke-static/range {p0 .. p1}, LX/57A;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 848586
    :cond_2
    const-string v19, "associated_application"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 848587
    invoke-static/range {p0 .. p1}, LX/40v;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 848588
    :cond_3
    const-string v19, "attachment_properties"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 848589
    invoke-static/range {p0 .. p1}, LX/58U;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 848590
    :cond_4
    const-string v19, "deduplication_key"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 848591
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 848592
    :cond_5
    const-string v19, "description"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 848593
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v13

    goto :goto_1

    .line 848594
    :cond_6
    const-string v19, "media_reference_token"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 848595
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 848596
    :cond_7
    const-string v19, "source"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 848597
    invoke-static/range {p0 .. p1}, LX/58V;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 848598
    :cond_8
    const-string v19, "style_infos"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 848599
    invoke-static/range {p0 .. p1}, LX/58k;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 848600
    :cond_9
    const-string v19, "style_list"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 848601
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 848602
    :cond_a
    const-string v19, "subtitle"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 848603
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 848604
    :cond_b
    const-string v19, "target"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 848605
    invoke-static/range {p0 .. p1}, LX/58i;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 848606
    :cond_c
    const-string v19, "title"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_d

    .line 848607
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 848608
    :cond_d
    const-string v19, "title_with_entities"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 848609
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 848610
    :cond_e
    const-string v19, "tracking"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_f

    .line 848611
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 848612
    :cond_f
    const-string v19, "url"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 848613
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 848614
    :cond_10
    const/16 v18, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 848615
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848616
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 848617
    const/16 v16, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 848618
    const/4 v15, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 848619
    const/4 v14, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 848620
    const/4 v13, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 848621
    const/4 v12, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 848622
    const/4 v11, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 848623
    const/16 v10, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 848624
    const/16 v9, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 848625
    const/16 v8, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 848626
    const/16 v7, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 848627
    const/16 v6, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 848628
    const/16 v5, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 848629
    const/16 v4, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 848630
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
