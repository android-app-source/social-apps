.class public final LX/5iV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:D

.field public b:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 983201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 983202
    new-instance v0, LX/186;

    const/16 v2, 0x80

    invoke-direct {v0, v2}, LX/186;-><init>(I)V

    .line 983203
    iget-object v2, p0, LX/5iV;->b:Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 983204
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 983205
    iget-wide v2, p0, LX/5iV;->a:D

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 983206
    invoke-virtual {v0, v8, v6}, LX/186;->b(II)V

    .line 983207
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    .line 983208
    invoke-virtual {v0, v2}, LX/186;->d(I)V

    .line 983209
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 983210
    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 983211
    new-instance v0, LX/15i;

    move-object v1, v2

    move-object v2, v7

    move-object v3, v7

    move v4, v8

    move-object v5, v7

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 983212
    new-instance v1, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;

    invoke-direct {v1, v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel$NodesModel$ImagePortraitSizeModel;-><init>(LX/15i;)V

    .line 983213
    return-object v1
.end method
