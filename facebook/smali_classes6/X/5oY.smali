.class public LX/5oY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/5oY;


# instance fields
.field private final a:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1007129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007130
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/5oY;->a:LX/0QI;

    .line 1007131
    return-void
.end method

.method public static a(LX/0QB;)LX/5oY;
    .locals 3

    .prologue
    .line 1007132
    sget-object v0, LX/5oY;->b:LX/5oY;

    if-nez v0, :cond_1

    .line 1007133
    const-class v1, LX/5oY;

    monitor-enter v1

    .line 1007134
    :try_start_0
    sget-object v0, LX/5oY;->b:LX/5oY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1007135
    if-eqz v2, :cond_0

    .line 1007136
    :try_start_1
    new-instance v0, LX/5oY;

    invoke-direct {v0}, LX/5oY;-><init>()V

    .line 1007137
    move-object v0, v0

    .line 1007138
    sput-object v0, LX/5oY;->b:LX/5oY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1007139
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1007140
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1007141
    :cond_1
    sget-object v0, LX/5oY;->b:LX/5oY;

    return-object v0

    .line 1007142
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1007143
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007124
    iget-object v0, p0, LX/5oY;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1007125
    if-nez v0, :cond_0

    .line 1007126
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1007127
    invoke-virtual {p0, p1, v0}, LX/5oY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1007128
    :cond_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1007122
    iget-object v0, p0, LX/5oY;->a:LX/0QI;

    invoke-interface {v0, p1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1007123
    return-void
.end method
