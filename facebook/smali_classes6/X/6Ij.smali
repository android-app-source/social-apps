.class public final LX/6Ij;
.super Landroid/hardware/camera2/CameraDevice$StateCallback;
.source ""


# instance fields
.field public final synthetic a:LX/6Iu;


# direct methods
.method public constructor <init>(LX/6Iu;)V
    .locals 0

    .prologue
    .line 1074547
    iput-object p1, p0, LX/6Ij;->a:LX/6Iu;

    invoke-direct {p0}, Landroid/hardware/camera2/CameraDevice$StateCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClosed(Landroid/hardware/camera2/CameraDevice;)V
    .locals 2

    .prologue
    .line 1074548
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    const/4 v1, 0x0

    .line 1074549
    iput-boolean v1, v0, LX/6Iu;->f:Z

    .line 1074550
    return-void
.end method

.method public final onDisconnected(Landroid/hardware/camera2/CameraDevice;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1074551
    invoke-static {p1}, LX/0J3;->a(Landroid/hardware/camera2/CameraDevice;)V

    .line 1074552
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    const/4 v1, 0x0

    .line 1074553
    iput-boolean v1, v0, LX/6Iu;->f:Z

    .line 1074554
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    .line 1074555
    iput-object v3, v0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    .line 1074556
    sget-object v0, LX/6Iu;->a:Ljava/lang/String;

    const-string v1, "Camera was disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1074557
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    const/4 v1, 0x4

    const-string v2, "Camera was disconnected"

    invoke-static {v0, v1, v2, v3}, LX/6Iu;->a$redex0(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 1074558
    return-void
.end method

.method public final onError(Landroid/hardware/camera2/CameraDevice;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1074502
    invoke-static {p1}, LX/0J3;->a(Landroid/hardware/camera2/CameraDevice;)V

    .line 1074503
    sget-object v0, LX/6Iu;->a:Ljava/lang/String;

    const-string v1, "CameraStateCallback onError: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1074504
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    .line 1074505
    iput-boolean v4, v0, LX/6Iu;->f:Z

    .line 1074506
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    .line 1074507
    iput-object v5, v0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    .line 1074508
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    const-string v1, "Error encountered in camera device"

    invoke-static {v0, p2, v1, v5}, LX/6Iu;->a$redex0(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 1074509
    return-void
.end method

.method public final onOpened(Landroid/hardware/camera2/CameraDevice;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    invoke-static {p1}, LX/0Kw;->a(Landroid/hardware/camera2/CameraDevice;)V

    .line 1074510
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    .line 1074511
    iput-boolean v6, v0, LX/6Iu;->f:Z

    .line 1074512
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    .line 1074513
    iput-object p1, v0, LX/6Iu;->i:Landroid/hardware/camera2/CameraDevice;

    .line 1074514
    :try_start_0
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    iget-object v0, v0, LX/6Iu;->h:Landroid/hardware/camera2/CameraManager;

    iget-object v1, p0, LX/6Ij;->a:LX/6Iu;

    iget-object v1, v1, LX/6Iu;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/hardware/camera2/CameraManager;->getCameraCharacteristics(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;

    move-result-object v2

    .line 1074515
    iget-object v1, p0, LX/6Ij;->a:LX/6Iu;

    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_ORIENTATION:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v2, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1074516
    iput v0, v1, LX/6Iu;->o:I

    .line 1074517
    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_INFO_ACTIVE_ARRAY_SIZE:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v2, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1074518
    sget-object v1, Landroid/hardware/camera2/CameraCharacteristics;->SCALER_AVAILABLE_MAX_DIGITAL_ZOOM:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v2, v1}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 1074519
    iget-object v3, p0, LX/6Ij;->a:LX/6Iu;

    new-instance v4, LX/6If;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-direct {v4, v5, v0, v1}, LX/6If;-><init>(III)V

    .line 1074520
    iput-object v4, v3, LX/6Iu;->w:LX/6If;

    .line 1074521
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    new-instance v1, LX/6Ih;

    invoke-direct {v1, v2}, LX/6Ih;-><init>(Landroid/hardware/camera2/CameraCharacteristics;)V

    .line 1074522
    iput-object v1, v0, LX/6Iu;->H:LX/6Ih;

    .line 1074523
    iget-object v1, p0, LX/6Ij;->a:LX/6Iu;

    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->INFO_SUPPORTED_HARDWARE_LEVEL:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {v2, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1074524
    iget-object v2, v1, LX/6Iu;->q:LX/6KN;

    if-nez v2, :cond_1

    .line 1074525
    :goto_0
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    .line 1074526
    iget-object v1, v0, LX/6Iu;->q:LX/6KN;

    if-eqz v1, :cond_0

    .line 1074527
    iget-object v1, v0, LX/6Iu;->q:LX/6KN;

    iget-object v2, v0, LX/6Iu;->d:LX/6JF;

    iget-object v3, v0, LX/6Iu;->H:LX/6Ih;

    invoke-virtual {v1, v2, v3}, LX/6KN;->a(LX/6JF;LX/6IP;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074528
    :cond_0
    :goto_1
    iget-object v0, p0, LX/6Ij;->a:LX/6Iu;

    iget-object v0, v0, LX/6Iu;->e:LX/6Iv;

    new-instance v1, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/cameracore/camerasdk/api2/FbCameraDeviceImplV2$1$1;-><init>(LX/6Ij;)V

    invoke-virtual {v0, v1}, LX/6Iv;->a(Ljava/lang/Runnable;)V

    .line 1074529
    return-void

    .line 1074530
    :catch_0
    move-exception v0

    .line 1074531
    iget-object v1, p0, LX/6Ij;->a:LX/6Iu;

    .line 1074532
    invoke-static {v1, v6}, LX/6Iu;->d$redex0(LX/6Iu;I)V

    .line 1074533
    iget-object v1, p0, LX/6Ij;->a:LX/6Iu;

    const/4 v2, 0x4

    const-string v3, "Couldn\'t open camera"

    invoke-static {v1, v2, v3, v0}, LX/6Iu;->a$redex0(LX/6Iu;ILjava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1074534
    :cond_1
    const-string v2, "NONE"

    .line 1074535
    const/4 v3, 0x3

    if-lt v0, v3, :cond_2

    .line 1074536
    const-string v2, "LEVEL_%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1074537
    :goto_2
    iget-object v3, v1, LX/6Iu;->q:LX/6KN;

    .line 1074538
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "camera2_supported_hardware_level_report"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1074539
    const-string v5, "product_name"

    iget-object v1, v3, LX/6KN;->e:Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1074540
    const-string v5, "supported_hardware_level"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1074541
    iget-object v5, v3, LX/6KN;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1074542
    goto :goto_0

    .line 1074543
    :cond_2
    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 1074544
    :pswitch_0
    const-string v2, "LIMITED"

    goto :goto_2

    .line 1074545
    :pswitch_1
    const-string v2, "LEGACY"

    goto :goto_2

    .line 1074546
    :pswitch_2
    const-string v2, "FULL"

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
