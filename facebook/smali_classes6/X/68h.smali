.class public final LX/68h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field public static final a:Ljava/nio/charset/Charset;

.field public static final b:Ljava/nio/charset/Charset;

.field public static final c:Ljava/util/regex/Pattern;

.field public static final r:Ljava/io/OutputStream;


# instance fields
.field public final d:Ljava/util/concurrent/ThreadPoolExecutor;

.field public final e:Ljava/io/File;

.field private final f:Ljava/io/File;

.field private final g:Ljava/io/File;

.field private final h:Ljava/io/File;

.field private final i:I

.field private j:J

.field public final k:I

.field private l:J

.field public m:Ljava/io/Writer;

.field private final n:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/68d;",
            ">;"
        }
    .end annotation
.end field

.field public o:I

.field private p:J

.field private final q:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1056100
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LX/68h;->a:Ljava/nio/charset/Charset;

    .line 1056101
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, LX/68h;->b:Ljava/nio/charset/Charset;

    .line 1056102
    const-string v0, "[a-z0-9_-]{1,120}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/68h;->c:Ljava/util/regex/Pattern;

    .line 1056103
    new-instance v0, LX/68a;

    invoke-direct {v0}, LX/68a;-><init>()V

    sput-object v0, LX/68h;->r:Ljava/io/OutputStream;

    return-void
.end method

.method private constructor <init>(Ljava/io/File;IIJ)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1056086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1056087
    iput-wide v4, p0, LX/68h;->l:J

    .line 1056088
    new-instance v0, Ljava/util/LinkedHashMap;

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {v0, v2, v1, v3}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    iput-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    .line 1056089
    iput-wide v4, p0, LX/68h;->p:J

    .line 1056090
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, LX/68h;->d:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 1056091
    new-instance v0, LX/68Z;

    invoke-direct {v0, p0}, LX/68Z;-><init>(LX/68h;)V

    iput-object v0, p0, LX/68h;->q:Ljava/util/concurrent/Callable;

    .line 1056092
    iput-object p1, p0, LX/68h;->e:Ljava/io/File;

    .line 1056093
    iput p2, p0, LX/68h;->i:I

    .line 1056094
    new-instance v0, Ljava/io/File;

    const-string v1, "journal"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/68h;->f:Ljava/io/File;

    .line 1056095
    new-instance v0, Ljava/io/File;

    const-string v1, "journal.tmp"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/68h;->g:Ljava/io/File;

    .line 1056096
    new-instance v0, Ljava/io/File;

    const-string v1, "journal.bkp"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/68h;->h:Ljava/io/File;

    .line 1056097
    iput p3, p0, LX/68h;->k:I

    .line 1056098
    iput-wide p4, p0, LX/68h;->j:J

    .line 1056099
    return-void
.end method

.method public static declared-synchronized a(LX/68h;Ljava/lang/String;J)LX/68c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1056071
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/68h;->f(LX/68h;)V

    .line 1056072
    invoke-static {p1}, LX/68h;->e(Ljava/lang/String;)V

    .line 1056073
    iget-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68d;

    .line 1056074
    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_0

    iget-wide v2, v0, LX/68d;->f:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, v2, p2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 1056075
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1056076
    :cond_1
    if-nez v0, :cond_2

    .line 1056077
    :try_start_1
    new-instance v0, LX/68d;

    invoke-direct {v0, p0, p1}, LX/68d;-><init>(LX/68h;Ljava/lang/String;)V

    .line 1056078
    iget-object v1, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 1056079
    :goto_1
    new-instance v0, LX/68c;

    invoke-direct {v0, p0, v1}, LX/68c;-><init>(LX/68h;LX/68d;)V

    .line 1056080
    iput-object v0, v1, LX/68d;->e:LX/68c;

    .line 1056081
    iget-object v1, p0, LX/68h;->m:Ljava/io/Writer;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DIRTY "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1056082
    iget-object v1, p0, LX/68h;->m:Ljava/io/Writer;

    invoke-virtual {v1}, Ljava/io/Writer;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1056083
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1056084
    :cond_2
    :try_start_2
    iget-object v2, v0, LX/68d;->e:LX/68c;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 1056085
    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public static a(Ljava/io/File;IIJ)LX/68h;
    .locals 7

    .prologue
    .line 1056048
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gtz v0, :cond_0

    .line 1056049
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxSize <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1056050
    :cond_0
    if-gtz p2, :cond_1

    .line 1056051
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "valueCount <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1056052
    :cond_1
    new-instance v0, Ljava/io/File;

    const-string v1, "journal.bkp"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1056053
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1056054
    new-instance v1, Ljava/io/File;

    const-string v2, "journal"

    invoke-direct {v1, p0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1056055
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1056056
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1056057
    :cond_2
    :goto_0
    new-instance v0, LX/68h;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, LX/68h;-><init>(Ljava/io/File;IIJ)V

    .line 1056058
    iget-object v1, v0, LX/68h;->f:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1056059
    :try_start_0
    invoke-direct {v0}, LX/68h;->b()V

    .line 1056060
    invoke-direct {v0}, LX/68h;->c()V

    .line 1056061
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, v0, LX/68h;->f:Ljava/io/File;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    sget-object v4, LX/68h;->a:Ljava/nio/charset/Charset;

    invoke-direct {v2, v3, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v1, v0, LX/68h;->m:Ljava/io/Writer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1056062
    :goto_1
    return-object v0

    .line 1056063
    :cond_3
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/68h;->a(Ljava/io/File;Ljava/io/File;Z)V

    goto :goto_0

    .line 1056064
    :catch_0
    move-exception v1

    .line 1056065
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DiskLruCache "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is corrupt: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", removing"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1056066
    invoke-virtual {v0}, LX/68h;->close()V

    .line 1056067
    iget-object v1, v0, LX/68h;->e:Ljava/io/File;

    invoke-static {v1}, LX/68h;->a(Ljava/io/File;)V

    .line 1056068
    :cond_4
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    .line 1056069
    new-instance v0, LX/68h;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, LX/68h;-><init>(Ljava/io/File;IIJ)V

    .line 1056070
    invoke-static {v0}, LX/68h;->d$redex0(LX/68h;)V

    goto :goto_1
.end method

.method public static a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 1056043
    if-eqz p0, :cond_0

    .line 1056044
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1056045
    :cond_0
    :goto_0
    return-void

    .line 1056046
    :catch_0
    move-exception v0

    .line 1056047
    throw v0

    :catch_1
    goto :goto_0
.end method

.method public static a(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 1056033
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 1056034
    if-nez v1, :cond_0

    .line 1056035
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "not a readable directory: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1056036
    :cond_0
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1056037
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1056038
    invoke-static {v3}, LX/68h;->a(Ljava/io/File;)V

    .line 1056039
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1056040
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed to delete file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1056041
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1056042
    :cond_3
    return-void
.end method

.method private static a(Ljava/io/File;Ljava/io/File;Z)V
    .locals 1

    .prologue
    .line 1056028
    if-eqz p2, :cond_0

    .line 1056029
    invoke-static {p1}, LX/68h;->b(Ljava/io/File;)V

    .line 1056030
    :cond_0
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1056031
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 1056032
    :cond_1
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/68h;LX/68c;Z)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 1055988
    monitor-enter p0

    :try_start_0
    iget-object v2, p1, LX/68c;->b:LX/68d;

    .line 1055989
    iget-object v1, v2, LX/68d;->e:LX/68c;

    if-eq v1, p1, :cond_0

    .line 1055990
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1055991
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1055992
    :cond_0
    if-eqz p2, :cond_4

    :try_start_1
    iget-boolean v1, v2, LX/68d;->d:Z

    if-nez v1, :cond_4

    move v1, v0

    .line 1055993
    :goto_0
    iget v3, p0, LX/68h;->k:I

    if-ge v1, v3, :cond_4

    .line 1055994
    iget-object v3, p1, LX/68c;->c:[Z

    aget-boolean v3, v3, v1

    if-nez v3, :cond_1

    .line 1055995
    invoke-virtual {p1}, LX/68c;->b()V

    .line 1055996
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Newly created entry didn\'t create value for index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055997
    :cond_1
    invoke-virtual {v2, v1}, LX/68d;->b(I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1055998
    invoke-virtual {p1}, LX/68c;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1055999
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    .line 1056000
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1056001
    :cond_4
    :goto_2
    :try_start_2
    iget v1, p0, LX/68h;->k:I

    if-ge v0, v1, :cond_7

    .line 1056002
    invoke-virtual {v2, v0}, LX/68d;->b(I)Ljava/io/File;

    move-result-object v1

    .line 1056003
    if-eqz p2, :cond_6

    .line 1056004
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1056005
    invoke-virtual {v2, v0}, LX/68d;->a(I)Ljava/io/File;

    move-result-object v3

    .line 1056006
    invoke-virtual {v1, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1056007
    iget-object v1, v2, LX/68d;->c:[J

    aget-wide v4, v1, v0

    .line 1056008
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 1056009
    iget-object v1, v2, LX/68d;->c:[J

    aput-wide v6, v1, v0

    .line 1056010
    iget-wide v8, p0, LX/68h;->l:J

    sub-long v4, v8, v4

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/68h;->l:J

    .line 1056011
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1056012
    :cond_6
    invoke-static {v1}, LX/68h;->b(Ljava/io/File;)V

    goto :goto_3

    .line 1056013
    :cond_7
    iget v0, p0, LX/68h;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/68h;->o:I

    .line 1056014
    const/4 v0, 0x0

    .line 1056015
    iput-object v0, v2, LX/68d;->e:LX/68c;

    .line 1056016
    iget-boolean v0, v2, LX/68d;->d:Z

    or-int/2addr v0, p2

    if-eqz v0, :cond_a

    .line 1056017
    const/4 v0, 0x1

    .line 1056018
    iput-boolean v0, v2, LX/68d;->d:Z

    .line 1056019
    iget-object v0, p0, LX/68h;->m:Ljava/io/Writer;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CLEAN "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v2, LX/68d;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, LX/68d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1056020
    if-eqz p2, :cond_8

    .line 1056021
    iget-wide v0, p0, LX/68h;->p:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v0

    iput-wide v4, p0, LX/68h;->p:J

    .line 1056022
    iput-wide v0, v2, LX/68d;->f:J

    .line 1056023
    :cond_8
    :goto_4
    iget-object v0, p0, LX/68h;->m:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    .line 1056024
    iget-wide v0, p0, LX/68h;->l:J

    iget-wide v2, p0, LX/68h;->j:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_9

    invoke-static {p0}, LX/68h;->e(LX/68h;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1056025
    :cond_9
    iget-object v0, p0, LX/68h;->d:Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v1, p0, LX/68h;->q:Ljava/util/concurrent/Callable;

    const v2, 0x32549bc0

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    goto/16 :goto_1

    .line 1056026
    :cond_a
    iget-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    iget-object v1, v2, LX/68d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1056027
    iget-object v0, p0, LX/68h;->m:Ljava/io/Writer;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "REMOVE "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, LX/68d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4
.end method

.method private b()V
    .locals 8

    .prologue
    .line 1055973
    new-instance v1, LX/68g;

    new-instance v0, Ljava/io/FileInputStream;

    iget-object v2, p0, LX/68h;->f:Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    sget-object v2, LX/68h;->a:Ljava/nio/charset/Charset;

    invoke-direct {v1, p0, v0, v2}, LX/68g;-><init>(LX/68h;Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 1055974
    :try_start_0
    invoke-virtual {v1}, LX/68g;->a()Ljava/lang/String;

    move-result-object v0

    .line 1055975
    invoke-virtual {v1}, LX/68g;->a()Ljava/lang/String;

    move-result-object v2

    .line 1055976
    invoke-virtual {v1}, LX/68g;->a()Ljava/lang/String;

    move-result-object v3

    .line 1055977
    invoke-virtual {v1}, LX/68g;->a()Ljava/lang/String;

    move-result-object v4

    .line 1055978
    invoke-virtual {v1}, LX/68g;->a()Ljava/lang/String;

    move-result-object v5

    .line 1055979
    const-string v6, "libcore.io.DiskLruCache"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "1"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, p0, LX/68h;->i:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, p0, LX/68h;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1055980
    :cond_0
    new-instance v3, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "unexpected journal header: ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1055981
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/68h;->a(Ljava/io/Closeable;)V

    throw v0

    .line 1055982
    :cond_1
    const/4 v0, 0x0

    .line 1055983
    :goto_0
    :try_start_1
    invoke-virtual {v1}, LX/68g;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, LX/68h;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1055984
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1055985
    :catch_0
    :try_start_2
    iget-object v2, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->size()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, LX/68h;->o:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1055986
    invoke-static {v1}, LX/68h;->a(Ljava/io/Closeable;)V

    .line 1055987
    return-void
.end method

.method private static b(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 1056104
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1056105
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 1056106
    :cond_0
    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1055957
    iget-object v0, p0, LX/68h;->g:Ljava/io/File;

    invoke-static {v0}, LX/68h;->b(Ljava/io/File;)V

    .line 1055958
    iget-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1055959
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68d;

    .line 1055960
    iget-object v1, v0, LX/68d;->e:LX/68c;

    if-nez v1, :cond_1

    move v1, v2

    .line 1055961
    :goto_1
    iget v4, p0, LX/68h;->k:I

    if-ge v1, v4, :cond_0

    .line 1055962
    iget-wide v4, p0, LX/68h;->l:J

    iget-object v6, v0, LX/68d;->c:[J

    aget-wide v6, v6, v1

    add-long/2addr v4, v6

    iput-wide v4, p0, LX/68h;->l:J

    .line 1055963
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1055964
    :cond_1
    const/4 v1, 0x0

    .line 1055965
    iput-object v1, v0, LX/68d;->e:LX/68c;

    .line 1055966
    move v1, v2

    .line 1055967
    :goto_2
    iget v4, p0, LX/68h;->k:I

    if-ge v1, v4, :cond_2

    .line 1055968
    invoke-virtual {v0, v1}, LX/68d;->a(I)Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, LX/68h;->b(Ljava/io/File;)V

    .line 1055969
    invoke-virtual {v0, v1}, LX/68d;->b(I)Ljava/io/File;

    move-result-object v4

    invoke-static {v4}, LX/68h;->b(Ljava/io/File;)V

    .line 1055970
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1055971
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1055972
    :cond_3
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/16 v1, 0x20

    const/4 v7, 0x5

    const/4 v5, -0x1

    .line 1055929
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 1055930
    if-ne v2, v5, :cond_0

    .line 1055931
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected journal line: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055932
    :cond_0
    add-int/lit8 v0, v2, 0x1

    .line 1055933
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    .line 1055934
    if-ne v3, v5, :cond_2

    .line 1055935
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1055936
    const/4 v1, 0x6

    if-ne v2, v1, :cond_7

    const-string v1, "REMOVE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1055937
    iget-object v1, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1055938
    :cond_1
    :goto_0
    return-void

    .line 1055939
    :cond_2
    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1055940
    :goto_1
    iget-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68d;

    .line 1055941
    if-nez v0, :cond_3

    .line 1055942
    new-instance v0, LX/68d;

    invoke-direct {v0, p0, v1}, LX/68d;-><init>(LX/68h;Ljava/lang/String;)V

    .line 1055943
    iget-object v4, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1055944
    :cond_3
    if-eq v3, v5, :cond_4

    if-ne v2, v7, :cond_4

    const-string v1, "CLEAN"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1055945
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1055946
    const/4 v2, 0x1

    .line 1055947
    iput-boolean v2, v0, LX/68d;->d:Z

    .line 1055948
    const/4 v2, 0x0

    .line 1055949
    iput-object v2, v0, LX/68d;->e:LX/68c;

    .line 1055950
    invoke-static {v0, v1}, LX/68d;->a$redex0(LX/68d;[Ljava/lang/String;)V

    goto :goto_0

    .line 1055951
    :cond_4
    if-ne v3, v5, :cond_5

    if-ne v2, v7, :cond_5

    const-string v1, "DIRTY"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1055952
    new-instance v1, LX/68c;

    invoke-direct {v1, p0, v0}, LX/68c;-><init>(LX/68h;LX/68d;)V

    .line 1055953
    iput-object v1, v0, LX/68d;->e:LX/68c;

    .line 1055954
    goto :goto_0

    .line 1055955
    :cond_5
    if-ne v3, v5, :cond_6

    const/4 v0, 0x4

    if-ne v2, v0, :cond_6

    const-string v0, "READ"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1055956
    :cond_6
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected journal line: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move-object v1, v0

    goto :goto_1
.end method

.method public static declared-synchronized d$redex0(LX/68h;)V
    .locals 5

    .prologue
    .line 1055904
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/68h;->m:Ljava/io/Writer;

    if-eqz v0, :cond_0

    .line 1055905
    iget-object v0, p0, LX/68h;->m:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 1055906
    :cond_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, LX/68h;->g:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    sget-object v3, LX/68h;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, v2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1055907
    :try_start_1
    const-string v0, "libcore.io.DiskLruCache"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1055908
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1055909
    const-string v0, "1"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1055910
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1055911
    iget v0, p0, LX/68h;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1055912
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1055913
    iget v0, p0, LX/68h;->k:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1055914
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1055915
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1055916
    iget-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68d;

    .line 1055917
    iget-object v3, v0, LX/68d;->e:LX/68c;

    if-eqz v3, :cond_1

    .line 1055918
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DIRTY "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, LX/68d;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1055919
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/Writer;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1055920
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1055921
    :cond_1
    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CLEAN "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, LX/68d;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, LX/68d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1055922
    :cond_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/Writer;->close()V

    .line 1055923
    iget-object v0, p0, LX/68h;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1055924
    iget-object v0, p0, LX/68h;->f:Ljava/io/File;

    iget-object v1, p0, LX/68h;->h:Ljava/io/File;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/68h;->a(Ljava/io/File;Ljava/io/File;Z)V

    .line 1055925
    :cond_3
    iget-object v0, p0, LX/68h;->g:Ljava/io/File;

    iget-object v1, p0, LX/68h;->f:Ljava/io/File;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/68h;->a(Ljava/io/File;Ljava/io/File;Z)V

    .line 1055926
    iget-object v0, p0, LX/68h;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1055927
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, LX/68h;->f:Ljava/io/File;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    sget-object v3, LX/68h;->a:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v0, p0, LX/68h;->m:Ljava/io/Writer;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1055928
    monitor-exit p0

    return-void
.end method

.method private static e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1055900
    sget-object v0, LX/68h;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1055901
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1055902
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "keys must match regex [a-z0-9_-]{1,120}: \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055903
    :cond_0
    return-void
.end method

.method public static e(LX/68h;)Z
    .locals 2

    .prologue
    .line 1055899
    iget v0, p0, LX/68h;->o:I

    const/16 v1, 0x7d0

    if-lt v0, v1, :cond_0

    iget v0, p0, LX/68h;->o:I

    iget-object v1, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f(LX/68h;)V
    .locals 2

    .prologue
    .line 1055896
    iget-object v0, p0, LX/68h;->m:Ljava/io/Writer;

    if-nez v0, :cond_0

    .line 1055897
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "cache is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055898
    :cond_0
    return-void
.end method

.method public static g(LX/68h;)V
    .locals 4

    .prologue
    .line 1055892
    :goto_0
    iget-wide v0, p0, LX/68h;->l:J

    iget-wide v2, p0, LX/68h;->j:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1055893
    iget-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1055894
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/68h;->c(Ljava/lang/String;)Z

    goto :goto_0

    .line 1055895
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)LX/68e;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1055872
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/68h;->f(LX/68h;)V

    .line 1055873
    invoke-static {p1}, LX/68h;->e(Ljava/lang/String;)V

    .line 1055874
    iget-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1055875
    if-nez v0, :cond_1

    .line 1055876
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 1055877
    :cond_1
    :try_start_1
    iget-boolean v3, v0, LX/68d;->d:Z

    if-eqz v3, :cond_0

    .line 1055878
    iget v3, p0, LX/68h;->k:I

    new-array v6, v3, [Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v2

    .line 1055879
    :goto_1
    :try_start_2
    iget v4, p0, LX/68h;->k:I

    if-ge v3, v4, :cond_2

    .line 1055880
    new-instance v4, Ljava/io/FileInputStream;

    invoke-virtual {v0, v3}, LX/68d;->a(I)Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    aput-object v4, v6, v3
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1055881
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1055882
    :catch_0
    move v0, v2

    :goto_2
    :try_start_3
    iget v2, p0, LX/68h;->k:I

    if-ge v0, v2, :cond_0

    .line 1055883
    aget-object v2, v6, v0

    if-eqz v2, :cond_0

    .line 1055884
    aget-object v2, v6, v0

    invoke-static {v2}, LX/68h;->a(Ljava/io/Closeable;)V

    .line 1055885
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1055886
    :cond_2
    iget v1, p0, LX/68h;->o:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/68h;->o:I

    .line 1055887
    iget-object v1, p0, LX/68h;->m:Ljava/io/Writer;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "READ "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1055888
    invoke-static {p0}, LX/68h;->e(LX/68h;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1055889
    iget-object v1, p0, LX/68h;->d:Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v2, p0, LX/68h;->q:Ljava/util/concurrent/Callable;

    const v3, -0x1d891154

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;

    .line 1055890
    :cond_3
    new-instance v1, LX/68e;

    iget-wide v4, v0, LX/68d;->f:J

    iget-object v7, v0, LX/68d;->c:[J

    const/4 v8, 0x0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v8}, LX/68e;-><init>(LX/68h;Ljava/lang/String;J[Ljava/io/InputStream;[JB)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1055891
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1055854
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/68h;->f(LX/68h;)V

    .line 1055855
    invoke-static {p1}, LX/68h;->e(Ljava/lang/String;)V

    .line 1055856
    iget-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68d;

    .line 1055857
    if-eqz v0, :cond_0

    iget-object v2, v0, LX/68d;->e:LX/68c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 1055858
    :goto_0
    monitor-exit p0

    return v0

    .line 1055859
    :cond_1
    :try_start_1
    iget-wide v2, p0, LX/68h;->l:J

    iget-object v4, v0, LX/68d;->c:[J

    aget-wide v4, v4, v1

    sub-long/2addr v2, v4

    iput-wide v2, p0, LX/68h;->l:J

    .line 1055860
    iget-object v2, v0, LX/68d;->c:[J

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v1

    .line 1055861
    add-int/lit8 v1, v1, 0x1

    :cond_2
    iget v2, p0, LX/68h;->k:I

    if-ge v1, v2, :cond_3

    .line 1055862
    invoke-virtual {v0, v1}, LX/68d;->a(I)Ljava/io/File;

    move-result-object v2

    .line 1055863
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1055864
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "failed to delete "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1055865
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1055866
    :cond_3
    :try_start_2
    iget v0, p0, LX/68h;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/68h;->o:I

    .line 1055867
    iget-object v0, p0, LX/68h;->m:Ljava/io/Writer;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "REMOVE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1055868
    iget-object v0, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1055869
    invoke-static {p0}, LX/68h;->e(LX/68h;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1055870
    iget-object v0, p0, LX/68h;->d:Ljava/util/concurrent/ThreadPoolExecutor;

    iget-object v1, p0, LX/68h;->q:Ljava/util/concurrent/Callable;

    const v2, -0x7b8c79d2

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Callable;I)Ljava/util/concurrent/Future;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1055871
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized close()V
    .locals 3

    .prologue
    .line 1055845
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/68h;->m:Ljava/io/Writer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1055846
    :goto_0
    monitor-exit p0

    return-void

    .line 1055847
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/68h;->n:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/68d;

    .line 1055848
    iget-object v2, v0, LX/68d;->e:LX/68c;

    if-eqz v2, :cond_1

    .line 1055849
    iget-object v0, v0, LX/68d;->e:LX/68c;

    invoke-virtual {v0}, LX/68c;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1055850
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1055851
    :cond_2
    :try_start_2
    invoke-static {p0}, LX/68h;->g(LX/68h;)V

    .line 1055852
    iget-object v0, p0, LX/68h;->m:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 1055853
    const/4 v0, 0x0

    iput-object v0, p0, LX/68h;->m:Ljava/io/Writer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
