.class public LX/541;
.super LX/540;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/540",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 827664
    const/4 v0, 0x2

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, LX/540;-><init>(I)V

    .line 827665
    return-void
.end method


# virtual methods
.method public final isEmpty()Z
    .locals 6

    .prologue
    .line 827666
    iget-wide v4, p0, LX/540;->g:J

    move-wide v0, v4

    .line 827667
    iget-wide v4, p0, LX/53y;->g:J

    move-wide v2, v4

    .line 827668
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final offer(Ljava/lang/Object;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x1

    const-wide/16 v8, 0x0

    .line 827669
    if-nez p1, :cond_0

    .line 827670
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null is not a valid element"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 827671
    :cond_0
    iget-object v0, p0, LX/53w;->e:[J

    .line 827672
    :cond_1
    iget-wide v12, p0, LX/53y;->g:J

    move-wide v2, v12

    .line 827673
    invoke-virtual {p0, v2, v3}, LX/53w;->d(J)J

    move-result-wide v4

    .line 827674
    invoke-static {v0, v4, v5}, LX/53w;->a([JJ)J

    move-result-wide v6

    .line 827675
    sub-long/2addr v6, v2

    .line 827676
    cmp-long v1, v6, v8

    if-nez v1, :cond_2

    .line 827677
    add-long v6, v2, v10

    invoke-virtual {p0, v2, v3, v6, v7}, LX/53y;->b(JJ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 827678
    invoke-virtual {p0, v2, v3}, LX/53v;->a(J)J

    move-result-wide v6

    .line 827679
    invoke-virtual {p0, v6, v7, p1}, LX/53v;->a(JLjava/lang/Object;)V

    .line 827680
    add-long/2addr v2, v10

    invoke-static {v0, v4, v5, v2, v3}, LX/53w;->a([JJJ)V

    .line 827681
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 827682
    :cond_2
    cmp-long v1, v6, v8

    if-gez v1, :cond_1

    .line 827683
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final peek()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 827684
    iget-wide v2, p0, LX/540;->g:J

    move-wide v0, v2

    .line 827685
    invoke-virtual {p0, v0, v1}, LX/53v;->a(J)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LX/53v;->b(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final poll()Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 827686
    iget-object v1, p0, LX/53w;->e:[J

    .line 827687
    :cond_0
    iget-wide v10, p0, LX/540;->g:J

    move-wide v2, v10

    .line 827688
    invoke-virtual {p0, v2, v3}, LX/53w;->d(J)J

    move-result-wide v4

    .line 827689
    invoke-static {v1, v4, v5}, LX/53w;->a([JJ)J

    move-result-wide v6

    .line 827690
    const-wide/16 v8, 0x1

    add-long/2addr v8, v2

    sub-long/2addr v6, v8

    .line 827691
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_1

    .line 827692
    const-wide/16 v6, 0x1

    add-long/2addr v6, v2

    invoke-virtual {p0, v2, v3, v6, v7}, LX/540;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827693
    invoke-virtual {p0, v2, v3}, LX/53v;->a(J)J

    move-result-wide v6

    .line 827694
    invoke-virtual {p0, v6, v7}, LX/53v;->b(J)Ljava/lang/Object;

    move-result-object v0

    .line 827695
    const/4 v8, 0x0

    invoke-virtual {p0, v6, v7, v8}, LX/53v;->a(JLjava/lang/Object;)V

    .line 827696
    iget v6, p0, LX/53v;->b:I

    int-to-long v6, v6

    add-long/2addr v2, v6

    invoke-static {v1, v4, v5, v2, v3}, LX/53w;->a([JJJ)V

    .line 827697
    :goto_0
    return-object v0

    .line 827698
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v0, v6, v2

    if-gez v0, :cond_0

    .line 827699
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 8

    .prologue
    .line 827700
    iget-wide v6, p0, LX/540;->g:J

    move-wide v0, v6

    .line 827701
    :goto_0
    iget-wide v6, p0, LX/53y;->g:J

    move-wide v4, v6

    .line 827702
    iget-wide v6, p0, LX/540;->g:J

    move-wide v2, v6

    .line 827703
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 827704
    sub-long v0, v4, v2

    long-to-int v0, v0

    return v0

    :cond_0
    move-wide v0, v2

    .line 827705
    goto :goto_0
.end method
