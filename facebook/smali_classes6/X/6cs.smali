.class public LX/6cs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static volatile f:LX/6cs;


# instance fields
.field public e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1114674
    sget-object v0, LX/6dd;->a:LX/0U1;

    .line 1114675
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114676
    sput-object v0, LX/6cs;->a:Ljava/lang/String;

    .line 1114677
    sget-object v0, LX/6dd;->b:LX/0U1;

    .line 1114678
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114679
    sput-object v0, LX/6cs;->b:Ljava/lang/String;

    .line 1114680
    sget-object v0, LX/6dd;->c:LX/0U1;

    .line 1114681
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 1114682
    sput-object v0, LX/6cs;->c:Ljava/lang/String;

    .line 1114683
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/6cs;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/6cs;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/6cs;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    sput-object v0, LX/6cs;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1114684
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1114685
    return-void
.end method

.method public static a(LX/0QB;)LX/6cs;
    .locals 4

    .prologue
    .line 1114686
    sget-object v0, LX/6cs;->f:LX/6cs;

    if-nez v0, :cond_1

    .line 1114687
    const-class v1, LX/6cs;

    monitor-enter v1

    .line 1114688
    :try_start_0
    sget-object v0, LX/6cs;->f:LX/6cs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1114689
    if-eqz v2, :cond_0

    .line 1114690
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1114691
    new-instance v3, LX/6cs;

    invoke-direct {v3}, LX/6cs;-><init>()V

    .line 1114692
    const/16 p0, 0x274b

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1114693
    iput-object p0, v3, LX/6cs;->e:LX/0Or;

    .line 1114694
    move-object v0, v3

    .line 1114695
    sput-object v0, LX/6cs;->f:LX/6cs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1114696
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1114697
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1114698
    :cond_1
    sget-object v0, LX/6cs;->f:LX/6cs;

    return-object v0

    .line 1114699
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1114700
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0vV;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0vV",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1114701
    iget-object v0, p0, LX/6cs;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1114702
    sget-object v1, LX/6cs;->a:Ljava/lang/String;

    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 1114703
    const-string v1, "message_reactions"

    sget-object v2, LX/6cs;->d:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1114704
    if-nez v1, :cond_0

    .line 1114705
    :goto_0
    return-object v5

    .line 1114706
    :cond_0
    sget-object v0, LX/6cs;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1114707
    sget-object v2, LX/6cs;->c:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1114708
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v5

    .line 1114709
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1114710
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/user/model/UserKey;->a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v3

    .line 1114711
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1114712
    invoke-virtual {v5, v4, v3}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1114713
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V
    .locals 5

    .prologue
    .line 1114714
    iget-object v0, p0, LX/6cs;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1114715
    const v0, 0x51fa136c

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1114716
    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [LX/0ux;

    const/4 v2, 0x0

    sget-object v3, LX/6cs;->a:Ljava/lang/String;

    invoke-static {v3, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    sget-object v3, LX/6cs;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v0}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    .line 1114717
    const-string v2, "message_reactions"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1114718
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1114719
    const v0, -0x11a07d73

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1114720
    return-void

    .line 1114721
    :catchall_0
    move-exception v0

    const v2, 0x4a43a31d    # 3205319.2f

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1114722
    iget-object v0, p0, LX/6cs;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1114723
    const v0, -0x80a84a9

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1114724
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1114725
    sget-object v2, LX/6cs;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114726
    sget-object v2, LX/6cs;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/user/model/UserKey;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114727
    sget-object v2, LX/6cs;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114728
    const-string v2, "message_reactions"

    const/4 v3, 0x0

    const v4, 0x74a509e0

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x29ca6f2d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1114729
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1114730
    const v0, 0x5a8dcb4c

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1114731
    return-void

    .line 1114732
    :catchall_0
    move-exception v0

    const v2, 0x1a533c24

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
