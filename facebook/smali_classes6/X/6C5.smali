.class public LX/6C5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/6C5;


# instance fields
.field private final a:LX/0Zb;

.field public final b:LX/0kb;

.field public final c:LX/0ka;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0kb;LX/0ka;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063445
    iput-object p1, p0, LX/6C5;->a:LX/0Zb;

    .line 1063446
    iput-object p2, p0, LX/6C5;->b:LX/0kb;

    .line 1063447
    iput-object p3, p0, LX/6C5;->c:LX/0ka;

    .line 1063448
    return-void
.end method

.method public static a(LX/0QB;)LX/6C5;
    .locals 6

    .prologue
    .line 1063449
    sget-object v0, LX/6C5;->d:LX/6C5;

    if-nez v0, :cond_1

    .line 1063450
    const-class v1, LX/6C5;

    monitor-enter v1

    .line 1063451
    :try_start_0
    sget-object v0, LX/6C5;->d:LX/6C5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1063452
    if-eqz v2, :cond_0

    .line 1063453
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1063454
    new-instance p0, LX/6C5;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v5

    check-cast v5, LX/0ka;

    invoke-direct {p0, v3, v4, v5}, LX/6C5;-><init>(LX/0Zb;LX/0kb;LX/0ka;)V

    .line 1063455
    move-object v0, p0

    .line 1063456
    sput-object v0, LX/6C5;->d:LX/6C5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1063457
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1063458
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1063459
    :cond_1
    sget-object v0, LX/6C5;->d:LX/6C5;

    return-object v0

    .line 1063460
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1063461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 1063462
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fb4a_iab_spin_user_interaction"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1063463
    invoke-virtual {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063464
    iget-object v1, p0, LX/6C5;->b:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1063465
    const-string v1, "client_network"

    const-string p1, "offline"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063466
    :goto_0
    iget-object v1, p0, LX/6C5;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1063467
    return-void

    .line 1063468
    :cond_0
    iget-object v1, p0, LX/6C5;->c:LX/0ka;

    invoke-virtual {v1}, LX/0ka;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1063469
    const-string v1, "client_network"

    const-string p1, "wifi"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1063470
    :cond_1
    const-string v1, "client_network"

    const-string p1, "mobile"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method
