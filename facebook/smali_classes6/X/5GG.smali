.class public final LX/5GG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 887501
    const/4 v15, 0x0

    .line 887502
    const/4 v14, 0x0

    .line 887503
    const/4 v11, 0x0

    .line 887504
    const-wide/16 v12, 0x0

    .line 887505
    const/4 v10, 0x0

    .line 887506
    const/4 v9, 0x0

    .line 887507
    const/4 v8, 0x0

    .line 887508
    const/4 v7, 0x0

    .line 887509
    const/4 v6, 0x0

    .line 887510
    const/4 v5, 0x0

    .line 887511
    const/4 v4, 0x0

    .line 887512
    const/4 v3, 0x0

    .line 887513
    const/4 v2, 0x0

    .line 887514
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_f

    .line 887515
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 887516
    const/4 v2, 0x0

    .line 887517
    :goto_0
    return v2

    .line 887518
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v17

    if-eq v3, v0, :cond_d

    .line 887519
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 887520
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 887521
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_0

    if-eqz v3, :cond_0

    .line 887522
    const-string v17, "android_app_config"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 887523
    invoke-static/range {p0 .. p1}, LX/40x;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v16, v3

    goto :goto_1

    .line 887524
    :cond_1
    const-string v17, "android_store_url"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 887525
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 887526
    :cond_2
    const-string v17, "app_center_cover_image"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 887527
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 887528
    :cond_3
    const-string v17, "average_star_rating"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 887529
    const/4 v2, 0x1

    .line 887530
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    goto :goto_1

    .line 887531
    :cond_4
    const-string v17, "canvas_url"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 887532
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v15, v3

    goto :goto_1

    .line 887533
    :cond_5
    const-string v17, "global_usage_summary_sentence"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 887534
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    move v14, v3

    goto/16 :goto_1

    .line 887535
    :cond_6
    const-string v17, "id"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 887536
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v13, v3

    goto/16 :goto_1

    .line 887537
    :cond_7
    const-string v17, "instant_experiences_settings"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 887538
    invoke-static/range {p0 .. p1}, LX/40O;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto/16 :goto_1

    .line 887539
    :cond_8
    const-string v17, "name"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 887540
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 887541
    :cond_9
    const-string v17, "privacy_url"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 887542
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 887543
    :cond_a
    const-string v17, "social_usage_summary_sentence"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 887544
    invoke-static/range {p0 .. p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 887545
    :cond_b
    const-string v17, "square_logo"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 887546
    invoke-static/range {p0 .. p1}, LX/4aW;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 887547
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 887548
    :cond_d
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 887549
    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 887550
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 887551
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 887552
    if-eqz v2, :cond_e

    .line 887553
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 887554
    :cond_e
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 887555
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 887556
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 887557
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 887558
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 887559
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 887560
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 887561
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 887562
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v16, v15

    move v15, v10

    move v10, v5

    move/from16 v19, v6

    move v6, v11

    move/from16 v11, v19

    move/from16 v20, v8

    move v8, v3

    move/from16 v21, v9

    move v9, v4

    move-wide v4, v12

    move v12, v7

    move/from16 v13, v20

    move v7, v14

    move/from16 v14, v21

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 887563
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 887564
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887565
    if-eqz v0, :cond_0

    .line 887566
    const-string v1, "android_app_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887567
    invoke-static {p0, v0, p2}, LX/40x;->a(LX/15i;ILX/0nX;)V

    .line 887568
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887569
    if-eqz v0, :cond_1

    .line 887570
    const-string v1, "android_store_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887571
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887572
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887573
    if-eqz v0, :cond_2

    .line 887574
    const-string v1, "app_center_cover_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887575
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 887576
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 887577
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_3

    .line 887578
    const-string v2, "average_star_rating"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887579
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 887580
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887581
    if-eqz v0, :cond_4

    .line 887582
    const-string v1, "canvas_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887583
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887584
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887585
    if-eqz v0, :cond_5

    .line 887586
    const-string v1, "global_usage_summary_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887587
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 887588
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887589
    if-eqz v0, :cond_6

    .line 887590
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887591
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887592
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887593
    if-eqz v0, :cond_7

    .line 887594
    const-string v1, "instant_experiences_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887595
    invoke-static {p0, v0, p2, p3}, LX/40O;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 887596
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887597
    if-eqz v0, :cond_8

    .line 887598
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887599
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887600
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 887601
    if-eqz v0, :cond_9

    .line 887602
    const-string v1, "privacy_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887603
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 887604
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887605
    if-eqz v0, :cond_a

    .line 887606
    const-string v1, "social_usage_summary_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887607
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 887608
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 887609
    if-eqz v0, :cond_b

    .line 887610
    const-string v1, "square_logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 887611
    invoke-static {p0, v0, p2}, LX/4aW;->a(LX/15i;ILX/0nX;)V

    .line 887612
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 887613
    return-void
.end method
