.class public final enum LX/5zi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5zi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5zi;

.field public static final enum CAMERA_CORE:LX/5zi;

.field public static final enum OTHER:LX/5zi;

.field public static final enum QUICK_CAM:LX/5zi;

.field private static final VALUE_MAP:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/5zi;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final DBSerialValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1035434
    new-instance v1, LX/5zi;

    const-string v2, "QUICK_CAM"

    const-string v3, "QUICK_CAM"

    invoke-direct {v1, v2, v0, v3}, LX/5zi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zi;->QUICK_CAM:LX/5zi;

    .line 1035435
    new-instance v1, LX/5zi;

    const-string v2, "CAMERA_CORE"

    const-string v3, "CAMERA_CORE"

    invoke-direct {v1, v2, v4, v3}, LX/5zi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zi;->CAMERA_CORE:LX/5zi;

    .line 1035436
    new-instance v1, LX/5zi;

    const-string v2, "OTHER"

    const-string v3, "OTHER"

    invoke-direct {v1, v2, v5, v3}, LX/5zi;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/5zi;->OTHER:LX/5zi;

    .line 1035437
    const/4 v1, 0x3

    new-array v1, v1, [LX/5zi;

    sget-object v2, LX/5zi;->QUICK_CAM:LX/5zi;

    aput-object v2, v1, v0

    sget-object v2, LX/5zi;->CAMERA_CORE:LX/5zi;

    aput-object v2, v1, v4

    sget-object v2, LX/5zi;->OTHER:LX/5zi;

    aput-object v2, v1, v5

    sput-object v1, LX/5zi;->$VALUES:[LX/5zi;

    .line 1035438
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1035439
    invoke-static {}, LX/5zi;->values()[LX/5zi;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1035440
    iget-object v5, v4, LX/5zi;->DBSerialValue:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1035441
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1035442
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/5zi;->VALUE_MAP:LX/0P1;

    .line 1035443
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1035444
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1035445
    iput-object p3, p0, LX/5zi;->DBSerialValue:Ljava/lang/String;

    .line 1035446
    return-void
.end method

.method public static fromDBSerialValue(Ljava/lang/String;)LX/5zi;
    .locals 3

    .prologue
    .line 1035447
    sget-object v0, LX/5zi;->VALUE_MAP:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5zi;

    .line 1035448
    if-nez v0, :cond_0

    .line 1035449
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported Type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1035450
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5zi;
    .locals 1

    .prologue
    .line 1035451
    const-class v0, LX/5zi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5zi;

    return-object v0
.end method

.method public static values()[LX/5zi;
    .locals 1

    .prologue
    .line 1035452
    sget-object v0, LX/5zi;->$VALUES:[LX/5zi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5zi;

    return-object v0
.end method
