.class public LX/6JR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1075503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075504
    iput p1, p0, LX/6JR;->a:I

    .line 1075505
    iput p2, p0, LX/6JR;->b:I

    .line 1075506
    return-void
.end method


# virtual methods
.method public final a(LX/6JR;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1075493
    if-eqz p1, :cond_0

    .line 1075494
    iget v1, p0, LX/6JR;->a:I

    iget v2, p1, LX/6JR;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/6JR;->b:I

    iget v2, p1, LX/6JR;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1075495
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1075500
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/6JR;

    if-eqz v0, :cond_0

    .line 1075501
    check-cast p1, LX/6JR;

    invoke-virtual {p0, p1}, LX/6JR;->a(LX/6JR;)Z

    move-result v0

    .line 1075502
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1075497
    iget v0, p0, LX/6JR;->a:I

    .line 1075498
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/6JR;->b:I

    add-int/2addr v0, v1

    .line 1075499
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1075496
    const-string v0, "%dx%d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, LX/6JR;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, LX/6JR;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
