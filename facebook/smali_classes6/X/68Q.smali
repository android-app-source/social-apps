.class public final LX/68Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field private f:LX/680;


# direct methods
.method public constructor <init>(LX/680;)V
    .locals 1

    .prologue
    .line 1055171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1055172
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/68Q;->a:Z

    .line 1055173
    iput-object p1, p0, LX/68Q;->f:LX/680;

    .line 1055174
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 1055175
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v0, v0, LX/680;->x:LX/68X;

    if-nez v0, :cond_1

    .line 1055176
    iget-object v0, p0, LX/68Q;->f:LX/680;

    new-instance v1, LX/68X;

    iget-object v2, p0, LX/68Q;->f:LX/680;

    invoke-direct {v1, v2}, LX/68X;-><init>(LX/680;)V

    iput-object v1, v0, LX/680;->x:LX/68X;

    .line 1055177
    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v1, p0, LX/68Q;->f:LX/680;

    iget-object v1, v1, LX/680;->x:LX/68X;

    invoke-virtual {v0, v1}, LX/680;->a(LX/67m;)LX/67m;

    .line 1055178
    :cond_0
    :goto_0
    return-void

    .line 1055179
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v0, v0, LX/680;->x:LX/68X;

    if-eqz v0, :cond_0

    .line 1055180
    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v1, p0, LX/68Q;->f:LX/680;

    iget-object v1, v1, LX/680;->x:LX/68X;

    invoke-virtual {v0, v1}, LX/680;->b(LX/67m;)V

    .line 1055181
    iget-object v0, p0, LX/68Q;->f:LX/680;

    const/4 v1, 0x0

    iput-object v1, v0, LX/680;->x:LX/68X;

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1055182
    iput-boolean p1, p0, LX/68Q;->a:Z

    .line 1055183
    invoke-virtual {p0}, LX/68Q;->e()V

    .line 1055184
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1055185
    iget-boolean v0, p0, LX/68Q;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/68Q;->f:LX/680;

    invoke-virtual {v0}, LX/680;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1055186
    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, p0, LX/68Q;->f:LX/680;

    iget-object v1, v1, LX/680;->v:LX/68o;

    if-nez v1, :cond_2

    .line 1055187
    iget-object v0, p0, LX/68Q;->f:LX/680;

    new-instance v1, LX/68o;

    iget-object v2, p0, LX/68Q;->f:LX/680;

    invoke-direct {v1, v2}, LX/68o;-><init>(LX/680;)V

    iput-object v1, v0, LX/680;->v:LX/68o;

    .line 1055188
    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v1, p0, LX/68Q;->f:LX/680;

    iget-object v1, v1, LX/680;->v:LX/68o;

    invoke-virtual {v0, v1}, LX/680;->a(LX/67m;)LX/67m;

    .line 1055189
    :cond_0
    :goto_1
    return-void

    .line 1055190
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1055191
    :cond_2
    if-nez v0, :cond_0

    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v0, v0, LX/680;->v:LX/68o;

    if-eqz v0, :cond_0

    .line 1055192
    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v1, p0, LX/68Q;->f:LX/680;

    iget-object v1, v1, LX/680;->v:LX/68o;

    invoke-virtual {v0, v1}, LX/680;->b(LX/67m;)V

    .line 1055193
    iget-object v0, p0, LX/68Q;->f:LX/680;

    const/4 v1, 0x0

    iput-object v1, v0, LX/680;->v:LX/68o;

    goto :goto_1
.end method

.method public final f(Z)V
    .locals 3

    .prologue
    .line 1055194
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v0, v0, LX/680;->u:LX/68v;

    if-nez v0, :cond_1

    .line 1055195
    iget-object v0, p0, LX/68Q;->f:LX/680;

    new-instance v1, LX/68v;

    iget-object v2, p0, LX/68Q;->f:LX/680;

    invoke-direct {v1, v2}, LX/68v;-><init>(LX/680;)V

    iput-object v1, v0, LX/680;->u:LX/68v;

    .line 1055196
    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v1, p0, LX/68Q;->f:LX/680;

    iget-object v1, v1, LX/680;->u:LX/68v;

    invoke-virtual {v0, v1}, LX/680;->a(LX/67m;)LX/67m;

    .line 1055197
    :cond_0
    :goto_0
    return-void

    .line 1055198
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v0, v0, LX/680;->u:LX/68v;

    if-eqz v0, :cond_0

    .line 1055199
    iget-object v0, p0, LX/68Q;->f:LX/680;

    iget-object v1, p0, LX/68Q;->f:LX/680;

    iget-object v1, v1, LX/680;->u:LX/68v;

    invoke-virtual {v0, v1}, LX/680;->b(LX/67m;)V

    .line 1055200
    iget-object v0, p0, LX/68Q;->f:LX/680;

    const/4 v1, 0x0

    iput-object v1, v0, LX/680;->u:LX/68v;

    goto :goto_0
.end method
