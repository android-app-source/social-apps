.class public LX/6Um;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/01T;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1102913
    sget-object v0, LX/01T;->FB4A:LX/01T;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/6Um;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1102914
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1102915
    return-void
.end method

.method public static a(LX/00H;Lcom/facebook/prefs/shared/FbSharedPreferences;)Ljava/lang/Boolean;
    .locals 2
    .annotation runtime Lcom/facebook/fbui/runtimelinter/IsRuntimeLinterEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 1102916
    sget-object v0, LX/6Um;->a:Ljava/util/Set;

    .line 1102917
    iget-object v1, p0, LX/00H;->j:LX/01T;

    move-object v1, v1

    .line 1102918
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/6Un;->b:LX/0Tn;

    sget-object v1, LX/6Un;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {p1, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInstanceForTest_MaximumViewDepthRule(LX/0QA;)LX/6Ut;
    .locals 1

    .prologue
    .line 1102919
    invoke-static {p0}, LX/6Ut;->a(LX/0QB;)LX/6Ut;

    move-result-object v0

    check-cast v0, LX/6Ut;

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1102920
    return-void
.end method
