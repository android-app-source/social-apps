.class public LX/6Pv;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0TD;

.field public final c:LX/2F7;

.field public final d:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1086554
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "device_free_space"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "device_available_space"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "device_total_space"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "cache_size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "app_data_size"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/6Pv;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/2F7;LX/0Zb;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086556
    iput-object p1, p0, LX/6Pv;->b:LX/0TD;

    .line 1086557
    iput-object p2, p0, LX/6Pv;->c:LX/2F7;

    .line 1086558
    iput-object p3, p0, LX/6Pv;->d:LX/0Zb;

    .line 1086559
    return-void
.end method
