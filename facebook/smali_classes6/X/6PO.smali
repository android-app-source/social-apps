.class public final LX/6PO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

.field public final synthetic b:LX/3iV;


# direct methods
.method public constructor <init>(LX/3iV;Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;)V
    .locals 0

    .prologue
    .line 1086141
    iput-object p1, p0, LX/6PO;->b:LX/3iV;

    iput-object p2, p0, LX/6PO;->a:Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1086142
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1086143
    const-string v1, "setNotifyMeParams"

    iget-object v2, p0, LX/6PO;->a:Lcom/facebook/api/ufiservices/common/SetNotifyMeParams;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1086144
    iget-object v1, p0, LX/6PO;->b:LX/3iV;

    iget-object v1, v1, LX/3iV;->e:LX/0aG;

    const-string v2, "feed_set_notify_me"

    const v3, -0x1fc525aa

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    const-wide/16 v2, 0x3c

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const v4, -0x6008d4df

    invoke-static {v0, v2, v3, v1, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 1086145
    return-object v0
.end method
