.class public final LX/66v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/64y;

.field private final b:Ljava/util/Random;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/64w;LX/650;)V
    .locals 1

    .prologue
    .line 1051289
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    invoke-direct {p0, p1, p2, v0}, LX/66v;-><init>(LX/64w;LX/650;Ljava/util/Random;)V

    .line 1051290
    return-void
.end method

.method private constructor <init>(LX/64w;LX/650;Ljava/util/Random;)V
    .locals 4

    .prologue
    .line 1051291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1051292
    const-string v0, "GET"

    .line 1051293
    iget-object v1, p2, LX/650;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1051294
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1051295
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Request must be GET: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1051296
    iget-object v2, p2, LX/650;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1051297
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051298
    :cond_0
    iput-object p3, p0, LX/66v;->b:Ljava/util/Random;

    .line 1051299
    const/16 v0, 0x10

    new-array v0, v0, [B

    .line 1051300
    invoke-virtual {p3, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 1051301
    invoke-static {v0}, LX/673;->a([B)LX/673;

    move-result-object v0

    invoke-virtual {v0}, LX/673;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/66v;->c:Ljava/lang/String;

    .line 1051302
    new-instance v0, LX/64v;

    invoke-direct {v0, p1}, LX/64v;-><init>(LX/64w;)V

    move-object v0, v0

    .line 1051303
    sget-object v1, LX/64x;->HTTP_1_1:LX/64x;

    .line 1051304
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/64v;->a(Ljava/util/List;)LX/64v;

    move-result-object v0

    .line 1051305
    invoke-virtual {v0}, LX/64v;->a()LX/64w;

    move-result-object v0

    .line 1051306
    invoke-virtual {p2}, LX/650;->newBuilder()LX/64z;

    move-result-object v1

    const-string v2, "Upgrade"

    const-string v3, "websocket"

    .line 1051307
    invoke-virtual {v1, v2, v3}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    move-result-object v1

    const-string v2, "Connection"

    const-string v3, "Upgrade"

    .line 1051308
    invoke-virtual {v1, v2, v3}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    move-result-object v1

    const-string v2, "Sec-WebSocket-Key"

    iget-object v3, p0, LX/66v;->c:Ljava/lang/String;

    .line 1051309
    invoke-virtual {v1, v2, v3}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    move-result-object v1

    const-string v2, "Sec-WebSocket-Version"

    const-string v3, "13"

    .line 1051310
    invoke-virtual {v1, v2, v3}, LX/64z;->a(Ljava/lang/String;Ljava/lang/String;)LX/64z;

    move-result-object v1

    .line 1051311
    invoke-virtual {v1}, LX/64z;->b()LX/650;

    move-result-object v1

    .line 1051312
    invoke-virtual {v0, v1}, LX/64w;->a(LX/650;)LX/64y;

    move-result-object v0

    iput-object v0, p0, LX/66v;->a:LX/64y;

    .line 1051313
    return-void
.end method

.method public static a$redex0(LX/66v;LX/655;LX/K09;)V
    .locals 5

    .prologue
    .line 1051314
    iget v0, p1, LX/655;->c:I

    move v0, v0

    .line 1051315
    const/16 v1, 0x65

    if-eq v0, v1, :cond_0

    .line 1051316
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected HTTP 101 response but was \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1051317
    iget v2, p1, LX/655;->c:I

    move v2, v2

    .line 1051318
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1051319
    iget-object v2, p1, LX/655;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1051320
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051321
    :cond_0
    const-string v0, "Connection"

    invoke-virtual {p1, v0}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1051322
    const-string v1, "Upgrade"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1051323
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected \'Connection\' header value \'Upgrade\' but was \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1051324
    :cond_1
    const-string v0, "Upgrade"

    invoke-virtual {p1, v0}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1051325
    const-string v1, "websocket"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1051326
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected \'Upgrade\' header value \'websocket\' but was \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1051327
    :cond_2
    const-string v0, "Sec-WebSocket-Accept"

    invoke-virtual {p1, v0}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1051328
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/66v;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/65A;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1051329
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1051330
    new-instance v2, Ljava/net/ProtocolException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Expected \'Sec-WebSocket-Accept\' header value \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\' but was \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1051331
    :cond_3
    sget-object v0, LX/64t;->a:LX/64t;

    iget-object v1, p0, LX/66v;->a:LX/64y;

    invoke-virtual {v0, v1}, LX/64t;->a(LX/64y;)LX/65W;

    move-result-object v0

    .line 1051332
    iget-object v1, p0, LX/66v;->b:Ljava/util/Random;

    invoke-static {v0, p1, v1, p2}, LX/66u;->a(LX/65W;LX/655;Ljava/util/Random;LX/K09;)LX/66m;

    move-result-object v0

    .line 1051333
    invoke-virtual {p2, v0}, LX/K09;->a(LX/66l;)V

    .line 1051334
    :cond_4
    const/4 v1, 0x0

    .line 1051335
    :try_start_0
    iget-object v2, v0, LX/66m;->d:LX/66q;

    .line 1051336
    invoke-static {v2}, LX/66q;->b(LX/66q;)V

    .line 1051337
    iget-boolean v3, v2, LX/66q;->k:Z

    if-eqz v3, :cond_8

    .line 1051338
    invoke-static {v2}, LX/66q;->c(LX/66q;)V

    .line 1051339
    :goto_0
    iget-boolean v2, v0, LX/66m;->h:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_5

    const/4 v1, 0x1

    .line 1051340
    :cond_5
    :goto_1
    move v1, v1

    .line 1051341
    if-nez v1, :cond_4

    .line 1051342
    return-void

    .line 1051343
    :catch_0
    move-exception v2

    .line 1051344
    iget-boolean v3, v0, LX/66m;->f:Z

    if-nez v3, :cond_6

    instance-of v3, v2, Ljava/net/ProtocolException;

    if-eqz v3, :cond_6

    .line 1051345
    :try_start_1
    iget-object v3, v0, LX/66m;->c:LX/66s;

    const/16 v4, 0x3ea

    const/4 p0, 0x0

    invoke-virtual {v3, v4, p0}, LX/66s;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1051346
    :cond_6
    :goto_2
    iget-object v3, v0, LX/66m;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v4, 0x0

    const/4 p0, 0x1

    invoke-virtual {v3, v4, p0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1051347
    :try_start_2
    invoke-virtual {v0}, LX/66m;->b()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1051348
    :cond_7
    :goto_3
    iget-object v3, v0, LX/66m;->e:LX/K09;

    invoke-virtual {v3, v2}, LX/K09;->a(Ljava/io/IOException;)V

    .line 1051349
    goto :goto_1

    .line 1051350
    :cond_8
    :try_start_3
    invoke-static {v2}, LX/66q;->d(LX/66q;)V

    goto :goto_0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_1
    goto :goto_3

    :catch_2
    goto :goto_2
.end method
