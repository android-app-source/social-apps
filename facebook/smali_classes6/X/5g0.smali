.class public final enum LX/5g0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5g0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5g0;

.field public static final enum GROUP_COMMERCE_REQUEST:LX/5g0;

.field public static final enum GROUP_COMMERCE_SEND:LX/5g0;

.field public static final enum INCENTIVES:LX/5g0;

.field public static final enum MESSENGER_COMMERCE:LX/5g0;

.field public static final enum META_RANGE_REQUEST_FLOW:LX/5g0;

.field public static final enum META_RANGE_SEND_FLOW:LX/5g0;

.field public static final enum MONEY_PENNY:LX/5g0;

.field public static final enum NUX:LX/5g0;

.field public static final enum REQUEST:LX/5g0;

.field public static final enum REQUEST_ACK:LX/5g0;

.field public static final enum SEND:LX/5g0;

.field public static final enum SENDER_INCENTIVES_REDEEM:LX/5g0;

.field public static final enum SETTINGS:LX/5g0;

.field public static final enum THREAD_DETAILS_SEND_FLOW:LX/5g0;

.field public static final enum TRIGGER_SEND_FLOW:LX/5g0;

.field public static final enum UNLOCK:LX/5g0;


# instance fields
.field public final analyticsModule:Ljava/lang/String;

.field public final type:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 972563
    new-instance v0, LX/5g0;

    const-string v1, "GROUP_COMMERCE_SEND"

    const-string v2, "group_commerce_send"

    const-string v3, "p2p_group_commerce_send"

    invoke-direct {v0, v1, v5, v2, v3}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->GROUP_COMMERCE_SEND:LX/5g0;

    .line 972564
    new-instance v0, LX/5g0;

    const-string v1, "GROUP_COMMERCE_REQUEST"

    const-string v2, "group_commerce_request"

    const-string v3, "p2p_group_commerce_request"

    invoke-direct {v0, v1, v6, v2, v3}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->GROUP_COMMERCE_REQUEST:LX/5g0;

    .line 972565
    new-instance v0, LX/5g0;

    const-string v1, "MESSENGER_COMMERCE"

    const-string v2, "messenger_commerce"

    const-string v3, "mc_pay"

    invoke-direct {v0, v1, v7, v2, v3}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->MESSENGER_COMMERCE:LX/5g0;

    .line 972566
    new-instance v0, LX/5g0;

    const-string v1, "MONEY_PENNY"

    const-string v2, "money_penny"

    const-string v3, "mp_pay"

    invoke-direct {v0, v1, v8, v2, v3}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->MONEY_PENNY:LX/5g0;

    .line 972567
    new-instance v0, LX/5g0;

    const-string v1, "REQUEST"

    const-string v2, "request"

    const-string v3, "p2p_request"

    invoke-direct {v0, v1, v9, v2, v3}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->REQUEST:LX/5g0;

    .line 972568
    new-instance v0, LX/5g0;

    const-string v1, "REQUEST_ACK"

    const/4 v2, 0x5

    const-string v3, "request_ack"

    const-string v4, "p2p_request"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->REQUEST_ACK:LX/5g0;

    .line 972569
    new-instance v0, LX/5g0;

    const-string v1, "SEND"

    const/4 v2, 0x6

    const-string v3, "send"

    const-string v4, "p2p_send"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->SEND:LX/5g0;

    .line 972570
    new-instance v0, LX/5g0;

    const-string v1, "THREAD_DETAILS_SEND_FLOW"

    const/4 v2, 0x7

    const-string v3, "thread_details_send"

    const-string v4, "p2p_thread_details"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->THREAD_DETAILS_SEND_FLOW:LX/5g0;

    .line 972571
    new-instance v0, LX/5g0;

    const-string v1, "TRIGGER_SEND_FLOW"

    const/16 v2, 0x8

    const-string v3, "trigger_send"

    const-string v4, "p2p_trigger"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->TRIGGER_SEND_FLOW:LX/5g0;

    .line 972572
    new-instance v0, LX/5g0;

    const-string v1, "META_RANGE_SEND_FLOW"

    const/16 v2, 0x9

    const-string v3, "meta_range_send"

    const-string v4, "p2p_metarange_cta"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->META_RANGE_SEND_FLOW:LX/5g0;

    .line 972573
    new-instance v0, LX/5g0;

    const-string v1, "META_RANGE_REQUEST_FLOW"

    const/16 v2, 0xa

    const-string v3, "meta_range_request"

    const-string v4, "p2p_metarange_cta"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->META_RANGE_REQUEST_FLOW:LX/5g0;

    .line 972574
    new-instance v0, LX/5g0;

    const-string v1, "INCENTIVES"

    const/16 v2, 0xb

    const-string v3, "incentives"

    const-string v4, "p2p_incentives"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->INCENTIVES:LX/5g0;

    .line 972575
    new-instance v0, LX/5g0;

    const-string v1, "NUX"

    const/16 v2, 0xc

    const-string v3, "nux"

    const-string v4, "p2p_receive"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->NUX:LX/5g0;

    .line 972576
    new-instance v0, LX/5g0;

    const-string v1, "SENDER_INCENTIVES_REDEEM"

    const/16 v2, 0xd

    const-string v3, "sender_incentives_redeem"

    const-string v4, "p2p_sender_incentives_redeem"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->SENDER_INCENTIVES_REDEEM:LX/5g0;

    .line 972577
    new-instance v0, LX/5g0;

    const-string v1, "SETTINGS"

    const/16 v2, 0xe

    const-string v3, "settings"

    const-string v4, "p2p_settings"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->SETTINGS:LX/5g0;

    .line 972578
    new-instance v0, LX/5g0;

    const-string v1, "UNLOCK"

    const/16 v2, 0xf

    const-string v3, "unlock"

    const-string v4, "p2p_unlock"

    invoke-direct {v0, v1, v2, v3, v4}, LX/5g0;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/5g0;->UNLOCK:LX/5g0;

    .line 972579
    const/16 v0, 0x10

    new-array v0, v0, [LX/5g0;

    sget-object v1, LX/5g0;->GROUP_COMMERCE_SEND:LX/5g0;

    aput-object v1, v0, v5

    sget-object v1, LX/5g0;->GROUP_COMMERCE_REQUEST:LX/5g0;

    aput-object v1, v0, v6

    sget-object v1, LX/5g0;->MESSENGER_COMMERCE:LX/5g0;

    aput-object v1, v0, v7

    sget-object v1, LX/5g0;->MONEY_PENNY:LX/5g0;

    aput-object v1, v0, v8

    sget-object v1, LX/5g0;->REQUEST:LX/5g0;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/5g0;->REQUEST_ACK:LX/5g0;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5g0;->SEND:LX/5g0;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5g0;->THREAD_DETAILS_SEND_FLOW:LX/5g0;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5g0;->TRIGGER_SEND_FLOW:LX/5g0;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5g0;->META_RANGE_SEND_FLOW:LX/5g0;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/5g0;->META_RANGE_REQUEST_FLOW:LX/5g0;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/5g0;->INCENTIVES:LX/5g0;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/5g0;->NUX:LX/5g0;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/5g0;->SENDER_INCENTIVES_REDEEM:LX/5g0;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/5g0;->SETTINGS:LX/5g0;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/5g0;->UNLOCK:LX/5g0;

    aput-object v2, v0, v1

    sput-object v0, LX/5g0;->$VALUES:[LX/5g0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 972580
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 972581
    iput-object p3, p0, LX/5g0;->type:Ljava/lang/String;

    .line 972582
    iput-object p4, p0, LX/5g0;->analyticsModule:Ljava/lang/String;

    .line 972583
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/5g0;
    .locals 5

    .prologue
    .line 972584
    invoke-static {}, LX/5g0;->values()[LX/5g0;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 972585
    iget-object v4, v0, LX/5g0;->type:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 972586
    :goto_1
    return-object v0

    .line 972587
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 972588
    :cond_1
    sget-object v0, LX/5g0;->SETTINGS:LX/5g0;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/5g0;
    .locals 1

    .prologue
    .line 972589
    const-class v0, LX/5g0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5g0;

    return-object v0
.end method

.method public static values()[LX/5g0;
    .locals 1

    .prologue
    .line 972590
    sget-object v0, LX/5g0;->$VALUES:[LX/5g0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5g0;

    return-object v0
.end method
