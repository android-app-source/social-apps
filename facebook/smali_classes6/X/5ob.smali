.class public final enum LX/5ob;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5ob;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5ob;

.field public static final enum FEED:LX/5ob;

.field public static final enum INLINE_COMPOSER:LX/5ob;

.field public static final enum STICKY_FEED:LX/5ob;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1007404
    new-instance v0, LX/5ob;

    const-string v1, "INLINE_COMPOSER"

    invoke-direct {v0, v1, v2}, LX/5ob;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5ob;->INLINE_COMPOSER:LX/5ob;

    .line 1007405
    new-instance v0, LX/5ob;

    const-string v1, "STICKY_FEED"

    invoke-direct {v0, v1, v3}, LX/5ob;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5ob;->STICKY_FEED:LX/5ob;

    .line 1007406
    new-instance v0, LX/5ob;

    const-string v1, "FEED"

    invoke-direct {v0, v1, v4}, LX/5ob;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5ob;->FEED:LX/5ob;

    .line 1007407
    const/4 v0, 0x3

    new-array v0, v0, [LX/5ob;

    sget-object v1, LX/5ob;->INLINE_COMPOSER:LX/5ob;

    aput-object v1, v0, v2

    sget-object v1, LX/5ob;->STICKY_FEED:LX/5ob;

    aput-object v1, v0, v3

    sget-object v1, LX/5ob;->FEED:LX/5ob;

    aput-object v1, v0, v4

    sput-object v0, LX/5ob;->$VALUES:[LX/5ob;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1007408
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5ob;
    .locals 1

    .prologue
    .line 1007409
    const-class v0, LX/5ob;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5ob;

    return-object v0
.end method

.method public static values()[LX/5ob;
    .locals 1

    .prologue
    .line 1007410
    sget-object v0, LX/5ob;->$VALUES:[LX/5ob;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5ob;

    return-object v0
.end method
