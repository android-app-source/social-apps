.class public LX/5OG;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Landroid/view/Menu;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements LX/34d;


# instance fields
.field public a:LX/5OE;

.field public b:LX/5OF;

.field public c:Landroid/content/Context;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/3Ai;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Landroid/content/res/ColorStateList;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 908227
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 908228
    iput-boolean v0, p0, LX/5OG;->e:Z

    .line 908229
    iput-boolean v0, p0, LX/5OG;->g:Z

    .line 908230
    iput-object p1, p0, LX/5OG;->c:Landroid/content/Context;

    .line 908231
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5OG;->d:Ljava/util/List;

    .line 908232
    return-void
.end method

.method private a(Landroid/view/MenuItem;)Landroid/view/SubMenu;
    .locals 2

    .prologue
    .line 908233
    new-instance v0, LX/5OJ;

    iget-object v1, p0, LX/5OG;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/5OJ;-><init>(Landroid/content/Context;)V

    .line 908234
    iput-object p0, v0, LX/5OJ;->d:LX/5OG;

    .line 908235
    iput-object p1, v0, LX/5OJ;->c:Landroid/view/MenuItem;

    .line 908236
    iget-object v1, p0, LX/5OG;->a:LX/5OE;

    invoke-virtual {v0, v1}, LX/5OG;->a(LX/5OE;)V

    .line 908237
    iget-object v1, p0, LX/5OG;->b:LX/5OF;

    invoke-virtual {v0, v1}, LX/5OG;->a(LX/5OF;)V

    .line 908238
    check-cast p1, LX/3Ai;

    .line 908239
    iput-object v0, p1, LX/3Ai;->n:Landroid/view/SubMenu;

    .line 908240
    return-object v0
.end method


# virtual methods
.method public final a(I)LX/3Ai;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 908241
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;III)V

    .line 908242
    invoke-virtual {p0, v0}, LX/5OG;->a(LX/3Ai;)V

    .line 908243
    return-object v0
.end method

.method public final a(II)LX/3Ai;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 908244
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;III)V

    .line 908245
    invoke-virtual {v0, p2}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 908246
    invoke-virtual {p0, v0}, LX/5OG;->a(LX/3Ai;)V

    .line 908247
    return-object v0
.end method

.method public final a(III)LX/3Ai;
    .locals 1

    .prologue
    .line 908248
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, p1, p2, p3}, LX/3Ai;-><init>(Landroid/view/Menu;III)V

    .line 908249
    invoke-virtual {p0, v0}, LX/5OG;->a(LX/3Ai;)V

    .line 908250
    return-object v0
.end method

.method public final a(IILjava/lang/CharSequence;)LX/3Ai;
    .locals 1

    .prologue
    .line 908251
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, p1, p2, p3}, LX/3Ai;-><init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V

    .line 908252
    invoke-virtual {p0, v0}, LX/5OG;->a(LX/3Ai;)V

    .line 908253
    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/3Ai;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 908254
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V

    .line 908255
    invoke-virtual {p0, v0}, LX/5OG;->a(LX/3Ai;)V

    .line 908256
    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)LX/3Ai;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 908257
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V

    .line 908258
    invoke-virtual {v0, p2}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 908259
    invoke-virtual {p0, v0}, LX/5OG;->a(LX/3Ai;)V

    .line 908260
    return-object v0
.end method

.method public final a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 908261
    iget-object v0, p0, LX/5OG;->c:Landroid/content/Context;

    return-object v0
.end method

.method public final a(LX/3Ai;)V
    .locals 4

    .prologue
    .line 908262
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 908263
    :goto_0
    return-void

    .line 908264
    :cond_0
    const/4 v0, 0x0

    .line 908265
    iget-object v1, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 908266
    invoke-virtual {v0}, LX/3Ai;->getOrder()I

    move-result v0

    invoke-virtual {p1}, LX/3Ai;->getOrder()I

    move-result v3

    if-le v0, v3, :cond_1

    .line 908267
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 908268
    const v0, -0xcefdc7d

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 908269
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 908270
    goto :goto_1

    .line 908271
    :cond_2
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 908272
    const v0, -0x6a694cf6

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final a(LX/5OE;)V
    .locals 3

    .prologue
    .line 908273
    iget-object v0, p0, LX/5OG;->a:LX/5OE;

    if-eq v0, p1, :cond_1

    .line 908274
    iput-object p1, p0, LX/5OG;->a:LX/5OE;

    .line 908275
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 908276
    invoke-virtual {v0}, LX/3Ai;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 908277
    invoke-virtual {v0}, LX/3Ai;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, LX/5OJ;

    .line 908278
    iget-object v2, p0, LX/5OG;->a:LX/5OE;

    invoke-virtual {v0, v2}, LX/5OG;->a(LX/5OE;)V

    goto :goto_0

    .line 908279
    :cond_1
    return-void
.end method

.method public final a(LX/5OF;)V
    .locals 3

    .prologue
    .line 908280
    iget-object v0, p0, LX/5OG;->b:LX/5OF;

    if-eq v0, p1, :cond_1

    .line 908281
    iput-object p1, p0, LX/5OG;->b:LX/5OF;

    .line 908282
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 908283
    invoke-virtual {v0}, LX/3Ai;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 908284
    invoke-virtual {v0}, LX/3Ai;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, LX/5OJ;

    .line 908285
    iget-object v2, p0, LX/5OG;->b:LX/5OF;

    invoke-virtual {v0, v2}, LX/5OG;->a(LX/5OF;)V

    goto :goto_0

    .line 908286
    :cond_1
    return-void
.end method

.method public final a(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 908303
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5OG;->g:Z

    .line 908304
    iput-object p1, p0, LX/5OG;->f:Landroid/content/res/ColorStateList;

    .line 908305
    const v0, -0x3d1f7077

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 908306
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 908287
    iget-boolean v0, p0, LX/5OG;->e:Z

    if-eq v0, p1, :cond_0

    .line 908288
    iput-boolean p1, p0, LX/5OG;->e:Z

    .line 908289
    const v0, -0x6c5371e8

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 908290
    :cond_0
    return-void
.end method

.method public final synthetic add(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 908291
    invoke-virtual {p0, p1}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 908292
    invoke-virtual {p0, p2, p3, p4}, LX/5OG;->a(III)LX/3Ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 908293
    invoke-virtual {p0, p2, p3, p4}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 908294
    invoke-virtual {p0, p1}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 1

    .prologue
    .line 908296
    const/4 v0, 0x0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908297
    invoke-virtual {p0, p1}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 908298
    invoke-direct {p0, v0}, LX/5OG;->a(Landroid/view/MenuItem;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908299
    invoke-virtual {p0, p2, p3, p4}, LX/5OG;->a(III)LX/3Ai;

    move-result-object v0

    .line 908300
    invoke-direct {p0, v0}, LX/5OG;->a(Landroid/view/MenuItem;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908301
    invoke-virtual {p0, p2, p3, p4}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 908302
    invoke-direct {p0, v0}, LX/5OG;->a(Landroid/view/MenuItem;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908225
    invoke-virtual {p0, p1}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 908226
    invoke-direct {p0, v0}, LX/5OG;->a(Landroid/view/MenuItem;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 908295
    const/4 v0, 0x1

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 908137
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 908138
    const v0, 0x1d1842eb

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 908139
    return-void
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 908141
    iget-object v0, p0, LX/5OG;->b:LX/5OF;

    if-eqz v0, :cond_0

    .line 908142
    iget-object v0, p0, LX/5OG;->b:LX/5OF;

    invoke-interface {v0}, LX/5OF;->a()V

    .line 908143
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 908144
    const v0, 0xaeb9073

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 908145
    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 908146
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 908147
    invoke-virtual {v0}, LX/3Ai;->getItemId()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 908148
    :goto_0
    return-object v0

    .line 908149
    :cond_1
    invoke-virtual {v0}, LX/3Ai;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 908150
    invoke-virtual {v0}, LX/3Ai;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    .line 908151
    invoke-interface {v0, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 908152
    if-eqz v0, :cond_0

    goto :goto_0

    .line 908153
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 908154
    iget-object v1, p0, LX/5OG;->d:Ljava/util/List;

    if-nez v1, :cond_0

    .line 908155
    :goto_0
    return v0

    .line 908156
    :cond_0
    iget-object v1, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 908157
    invoke-virtual {v0}, LX/3Ai;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 908158
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 908159
    goto :goto_1

    :cond_1
    move v0, v1

    .line 908160
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 908161
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    .line 908162
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 908163
    add-int/lit8 p1, p1, -0x1

    .line 908164
    :cond_1
    if-gez p1, :cond_0

    .line 908165
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 908140
    invoke-virtual {p0, p1}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 908166
    invoke-virtual {p0, p1}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 908167
    if-nez p2, :cond_1

    .line 908168
    new-instance p2, LX/5OD;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, LX/5OD;-><init>(Landroid/content/Context;)V

    .line 908169
    :goto_0
    invoke-virtual {p0, p1}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/5OD;->a(Landroid/view/MenuItem;)V

    .line 908170
    iget-boolean v0, p0, LX/5OG;->e:Z

    invoke-virtual {p2, v0}, LX/5OD;->a(Z)V

    .line 908171
    iget-boolean v0, p0, LX/5OG;->g:Z

    if-eqz v0, :cond_0

    .line 908172
    iget-object v0, p0, LX/5OG;->f:Landroid/content/res/ColorStateList;

    invoke-virtual {p2, v0}, LX/5OD;->a(Landroid/content/res/ColorStateList;)V

    .line 908173
    :cond_0
    return-object p2

    .line 908174
    :cond_1
    check-cast p2, LX/5OD;

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 908175
    const/4 v0, 0x0

    return v0
.end method

.method public hasVisibleItems()Z
    .locals 2

    .prologue
    .line 908176
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 908177
    invoke-virtual {v0}, LX/3Ai;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 908178
    const/4 v0, 0x1

    .line 908179
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 908180
    invoke-virtual {p0, p1}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 908181
    const/4 v0, 0x0

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 908182
    iget-object v0, p0, LX/5OG;->a:LX/5OE;

    if-eqz v0, :cond_1

    .line 908183
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 908184
    add-int/lit8 p3, p3, -0x1

    .line 908185
    :cond_0
    invoke-virtual {p0, p3}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 908186
    instance-of v1, v0, LX/3Ai;

    if-nez v1, :cond_2

    .line 908187
    iget-object v1, p0, LX/5OG;->a:LX/5OE;

    invoke-interface {v1, v0}, LX/5OE;->a(Landroid/view/MenuItem;)Z

    .line 908188
    invoke-virtual {p0}, LX/5OG;->close()V

    .line 908189
    :cond_1
    :goto_0
    return-void

    .line 908190
    :cond_2
    check-cast v0, LX/3Ai;

    .line 908191
    invoke-virtual {v0}, LX/3Ai;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 908192
    invoke-virtual {v0}, LX/3Ai;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 908193
    invoke-virtual {p0}, LX/5OG;->close()V

    goto :goto_0

    .line 908194
    :cond_3
    invoke-virtual {v0}, LX/3Ai;->hasSubMenu()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 908195
    invoke-virtual {v0}, LX/3Ai;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    check-cast v0, LX/5OJ;

    .line 908196
    iget-object v1, p0, LX/5OG;->b:LX/5OF;

    if-eqz v1, :cond_1

    .line 908197
    iget-object v1, p0, LX/5OG;->b:LX/5OF;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/5OF;->a(LX/5OG;Z)V

    goto :goto_0

    .line 908198
    :cond_4
    iget-object v1, p0, LX/5OG;->a:LX/5OE;

    invoke-interface {v1, v0}, LX/5OE;->a(Landroid/view/MenuItem;)Z

    .line 908199
    invoke-virtual {p0}, LX/5OG;->close()V

    goto :goto_0
.end method

.method public final performIdentifierAction(II)Z
    .locals 2

    .prologue
    .line 908200
    invoke-virtual {p0, p1}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 908201
    instance-of v1, v0, LX/3Ai;

    if-eqz v1, :cond_0

    .line 908202
    check-cast v0, LX/3Ai;

    invoke-virtual {v0}, LX/3Ai;->a()Z

    move-result v0

    .line 908203
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1

    .prologue
    .line 908204
    const/4 v0, 0x0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 0

    .prologue
    .line 908205
    return-void
.end method

.method public final removeItem(I)V
    .locals 4

    .prologue
    .line 908206
    const/4 v0, 0x0

    .line 908207
    iget-object v1, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ai;

    .line 908208
    invoke-virtual {v0}, LX/3Ai;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_1

    move-object v1, v0

    .line 908209
    goto :goto_0

    .line 908210
    :cond_1
    invoke-virtual {v0}, LX/3Ai;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 908211
    invoke-virtual {v0}, LX/3Ai;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v0

    .line 908212
    invoke-interface {v0, p1}, Landroid/view/SubMenu;->removeItem(I)V

    goto :goto_0

    .line 908213
    :cond_2
    if-eqz v1, :cond_3

    .line 908214
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 908215
    :cond_3
    const v0, -0x52ff1f15

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 908216
    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 0

    .prologue
    .line 908217
    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .locals 0

    .prologue
    .line 908218
    return-void
.end method

.method public final setGroupVisible(IZ)V
    .locals 0

    .prologue
    .line 908219
    return-void
.end method

.method public final setQwertyMode(Z)V
    .locals 0

    .prologue
    .line 908220
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 908221
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/5OG;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0

    .prologue
    .line 908222
    if-eqz p1, :cond_0

    .line 908223
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 908224
    :cond_0
    return-void
.end method
