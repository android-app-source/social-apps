.class public final LX/5Mh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 905626
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 905627
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 905628
    :goto_0
    return v1

    .line 905629
    :cond_0
    const-string v9, "has_next_page"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 905630
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    .line 905631
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 905632
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 905633
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 905634
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 905635
    const-string v9, "end_cursor"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 905636
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 905637
    :cond_2
    const-string v9, "has_previous_page"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 905638
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 905639
    :cond_3
    const-string v9, "start_cursor"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 905640
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 905641
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 905642
    :cond_5
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 905643
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 905644
    if-eqz v3, :cond_6

    .line 905645
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 905646
    :cond_6
    if-eqz v0, :cond_7

    .line 905647
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 905648
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 905649
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 905650
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 905651
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 905652
    if-eqz v0, :cond_0

    .line 905653
    const-string v1, "end_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 905654
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 905655
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 905656
    if-eqz v0, :cond_1

    .line 905657
    const-string v1, "has_next_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 905658
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 905659
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 905660
    if-eqz v0, :cond_2

    .line 905661
    const-string v1, "has_previous_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 905662
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 905663
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 905664
    if-eqz v0, :cond_3

    .line 905665
    const-string v1, "start_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 905666
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 905667
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 905668
    return-void
.end method
