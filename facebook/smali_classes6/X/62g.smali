.class public LX/62g;
.super LX/3x6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DATA:",
        "Ljava/lang/Object;",
        "VIEW:",
        "Landroid/view/View;",
        ">",
        "LX/3x6;"
    }
.end annotation


# instance fields
.field private final a:LX/Eqw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/widget/recyclerview/StickyHeaderItemDecorator$StickyHeaderAdapter",
            "<TDATA;TVIEW;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TDATA;TVIEW;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<TVIEW;",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<TVIEW;",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/graphics/Paint;

.field private f:Z

.field private g:I

.field private h:Z


# direct methods
.method public constructor <init>(LX/Eqw;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/recyclerview/StickyHeaderItemDecorator$StickyHeaderAdapter",
            "<TDATA;TVIEW;>;Z)V"
        }
    .end annotation

    .prologue
    .line 1041919
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1041920
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/62g;->b:Ljava/util/Map;

    .line 1041921
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/62g;->c:Ljava/util/LinkedHashMap;

    .line 1041922
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/62g;->d:Ljava/util/LinkedHashMap;

    .line 1041923
    const/4 v0, 0x0

    iput v0, p0, LX/62g;->g:I

    .line 1041924
    iput-object p1, p0, LX/62g;->a:LX/Eqw;

    .line 1041925
    iput-boolean p2, p0, LX/62g;->f:Z

    .line 1041926
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/62g;->e:Landroid/graphics/Paint;

    .line 1041927
    iget-object v0, p0, LX/62g;->e:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1041928
    iget-object v0, p0, LX/62g;->e:Landroid/graphics/Paint;

    .line 1041929
    const/4 p0, 0x0

    move v1, p0

    .line 1041930
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1041931
    return-void
.end method

.method private a(Landroid/support/v7/widget/RecyclerView;I)Landroid/view/View;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView;",
            "I)TVIEW;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1041897
    iget-boolean v0, p0, LX/62g;->h:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    iget v1, p0, LX/62g;->g:I

    if-eq v0, v1, :cond_1

    .line 1041898
    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    iput v0, p0, LX/62g;->g:I

    .line 1041899
    iget-object v0, p0, LX/62g;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1041900
    iget-object v0, p0, LX/62g;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1041901
    iput-boolean v4, p0, LX/62g;->h:Z

    .line 1041902
    :cond_1
    iget-object v0, p0, LX/62g;->a:LX/Eqw;

    invoke-virtual {v0, p2}, LX/Eqw;->a(I)Ljava/lang/Object;

    move-result-object v2

    .line 1041903
    iget-object v0, p0, LX/62g;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1041904
    iget-object v0, p0, LX/62g;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1041905
    :goto_0
    return-object v0

    .line 1041906
    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1041907
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1041908
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v5, 0x7f0306f6

    const/4 p2, 0x0

    invoke-virtual {v1, v5, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1041909
    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    const p2, 0x7f0a00d5

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v5, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v1, v5}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1041910
    move-object v1, v1

    .line 1041911
    iget-object v0, p0, LX/62g;->a:LX/Eqw;

    invoke-virtual {v0, v1, v2}, LX/Eqw;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 1041912
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_4

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    :goto_1
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/view/View;->measure(II)V

    .line 1041913
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v0, v3}, Landroid/view/View;->layout(IIII)V

    .line 1041914
    iget-object v0, p0, LX/62g;->b:Ljava/util/Map;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1041915
    iget-object v0, p0, LX/62g;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1041916
    iget-object v0, p0, LX/62g;->d:Ljava/util/LinkedHashMap;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    move-object v0, v1

    .line 1041917
    goto :goto_0

    .line 1041918
    :cond_4
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_1
.end method

.method private a(ILandroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1041877
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    move v0, v1

    .line 1041878
    :goto_0
    return v0

    .line 1041879
    :cond_0
    if-nez p1, :cond_1

    move v0, v2

    .line 1041880
    goto :goto_0

    .line 1041881
    :cond_1
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    .line 1041882
    instance-of v3, v0, LX/3wu;

    if-eqz v3, :cond_3

    .line 1041883
    check-cast v0, LX/3wu;

    .line 1041884
    iget-object v3, v0, LX/3wu;->h:LX/3wr;

    move-object v3, v3

    .line 1041885
    iget v4, v0, LX/3wu;->c:I

    move v0, v4

    .line 1041886
    invoke-virtual {v3, p1, v0}, LX/3wr;->a(II)I

    move-result v0

    .line 1041887
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v3

    .line 1041888
    sub-int v0, v3, v0

    .line 1041889
    if-nez v0, :cond_2

    move v0, v2

    .line 1041890
    goto :goto_0

    .line 1041891
    :cond_2
    iget-object v3, p0, LX/62g;->a:LX/Eqw;

    invoke-virtual {v3, v0}, LX/Eqw;->a(I)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, LX/62g;->a:LX/Eqw;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, LX/Eqw;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v3, v0, :cond_4

    move v0, v2

    .line 1041892
    goto :goto_0

    .line 1041893
    :cond_3
    instance-of v0, v0, LX/1P1;

    if-eqz v0, :cond_4

    .line 1041894
    iget-object v0, p0, LX/62g;->a:LX/Eqw;

    invoke-virtual {v0, p1}, LX/Eqw;->a(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v3, p0, LX/62g;->a:LX/Eqw;

    add-int/lit8 v4, p1, -0x1

    invoke-virtual {v3, v4}, LX/Eqw;->a(I)Ljava/lang/Object;

    move-result-object v3

    if-eq v0, v3, :cond_4

    move v0, v2

    .line 1041895
    goto :goto_0

    :cond_4
    move v0, v1

    .line 1041896
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 2

    .prologue
    .line 1041832
    invoke-super {p0, p1, p2, p3, p4}, LX/3x6;->a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 1041833
    iget-boolean v0, p0, LX/62g;->f:Z

    if-nez v0, :cond_1

    .line 1041834
    :cond_0
    :goto_0
    return-void

    .line 1041835
    :cond_1
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v0

    .line 1041836
    invoke-direct {p0, v0, p2, p3}, LX/62g;->a(ILandroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1041837
    invoke-direct {p0, p3, v0}, LX/62g;->a(Landroid/support/v7/widget/RecyclerView;I)Landroid/view/View;

    move-result-object v0

    .line 1041838
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1041839
    invoke-super {p0, p1, p2, p3}, LX/3x6;->b(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 1041840
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v7

    .line 1041841
    if-nez v7, :cond_1

    .line 1041842
    :cond_0
    :goto_0
    return-void

    .line 1041843
    :cond_1
    const/4 v5, 0x0

    .line 1041844
    iget-object v0, p0, LX/62g;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1041845
    const/4 v3, 0x1

    move v6, v2

    .line 1041846
    :goto_1
    if-ge v6, v7, :cond_5

    .line 1041847
    invoke-virtual {p2, v6}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1041848
    invoke-virtual {p2, v8}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v9

    .line 1041849
    const/4 v0, -0x1

    if-eq v9, v0, :cond_8

    .line 1041850
    invoke-direct {p0, p2, v9}, LX/62g;->a(Landroid/support/v7/widget/RecyclerView;I)Landroid/view/View;

    move-result-object v4

    .line 1041851
    iget-boolean v0, p0, LX/62g;->f:Z

    if-eqz v0, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    move v1, v0

    .line 1041852
    :goto_2
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 1041853
    if-nez v5, :cond_4

    .line 1041854
    iget-object v0, p0, LX/62g;->c:Ljava/util/LinkedHashMap;

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v2, v2, v10, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v4, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1041855
    invoke-direct {p0, v9, v8, p2}, LX/62g;->a(ILandroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    move-object v1, v4

    .line 1041856
    :goto_3
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move-object v5, v1

    move v3, v0

    goto :goto_1

    :cond_2
    move v1, v2

    .line 1041857
    goto :goto_2

    .line 1041858
    :cond_3
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v1

    if-ge v0, v1, :cond_9

    move v0, v2

    move-object v1, v4

    .line 1041859
    goto :goto_3

    .line 1041860
    :cond_4
    invoke-direct {p0, v9, v8, p2}, LX/62g;->a(ILandroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1041861
    iget-object v0, p0, LX/62g;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1041862
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v9

    sub-int v1, v9, v1

    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual {v0, v2, v1, v10, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 1041863
    iget-object v1, p0, LX/62g;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 1041864
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1041865
    iget v3, v0, Landroid/graphics/Rect;->top:I

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    .line 1041866
    iget v3, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v8

    sub-int/2addr v3, v8

    iput v3, v1, Landroid/graphics/Rect;->top:I

    move v1, v2

    .line 1041867
    :goto_4
    iget-object v3, p0, LX/62g;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v4, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    move-object v1, v5

    goto :goto_3

    .line 1041868
    :cond_5
    iget-object v0, p0, LX/62g;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1041869
    iget-object v1, p0, LX/62g;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 1041870
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1041871
    iget v4, v1, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p1, v4, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1041872
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 1041873
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_5

    .line 1041874
    :cond_6
    if-nez v3, :cond_0

    .line 1041875
    iget-object v0, p0, LX/62g;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1041876
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget-object v5, p0, LX/62g;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_7
    move v1, v3

    goto :goto_4

    :cond_8
    move v0, v3

    move-object v1, v5

    goto/16 :goto_3

    :cond_9
    move v0, v3

    move-object v1, v4

    goto/16 :goto_3
.end method
