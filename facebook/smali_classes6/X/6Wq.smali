.class public LX/6Wq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/6Wd;",
        "LX/0lF;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6Wq;


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1106539
    const-string v0, "fqlQueryMethod"

    invoke-direct {p0, v0}, LX/6Wq;-><init>(Ljava/lang/String;)V

    .line 1106540
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1106541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106542
    iput-object p1, p0, LX/6Wq;->a:Ljava/lang/String;

    .line 1106543
    return-void
.end method

.method public static a(LX/0QB;)LX/6Wq;
    .locals 3

    .prologue
    .line 1106544
    sget-object v0, LX/6Wq;->b:LX/6Wq;

    if-nez v0, :cond_1

    .line 1106545
    const-class v1, LX/6Wq;

    monitor-enter v1

    .line 1106546
    :try_start_0
    sget-object v0, LX/6Wq;->b:LX/6Wq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1106547
    if-eqz v2, :cond_0

    .line 1106548
    :try_start_1
    new-instance v0, LX/6Wq;

    invoke-direct {v0}, LX/6Wq;-><init>()V

    .line 1106549
    move-object v0, v0

    .line 1106550
    sput-object v0, LX/6Wq;->b:LX/6Wq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1106551
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1106552
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1106553
    :cond_1
    sget-object v0, LX/6Wq;->b:LX/6Wq;

    return-object v0

    .line 1106554
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1106555
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1106556
    check-cast p1, LX/6Wd;

    .line 1106557
    const/4 v0, 0x2

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 1106558
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "query"

    invoke-virtual {p1}, LX/6Wd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1106559
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1106560
    new-instance v0, LX/14N;

    .line 1106561
    iget-object v1, p0, LX/6Wq;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1106562
    const-string v2, "GET"

    const-string v3, "method/fql.query"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1106563
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1106564
    :try_start_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1106565
    :catch_0
    move-exception v0

    .line 1106566
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Exception when trying to get parser"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
