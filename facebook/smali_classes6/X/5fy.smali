.class public LX/5fy;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 972526
    sput-object v0, LX/5fy;->a:LX/0Px;

    .line 972527
    sput-object v0, LX/5fy;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 972483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/InputStream;)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 972484
    :try_start_0
    new-instance v0, LX/1BC;

    invoke-direct {v0}, LX/1BC;-><init>()V

    .line 972485
    invoke-virtual {v0, p0}, LX/0lp;->a(Ljava/io/InputStream;)LX/15w;

    move-result-object v0

    .line 972486
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 972487
    :goto_0
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_OBJECT:LX/15z;

    if-eq v1, v2, :cond_5

    .line 972488
    invoke-virtual {v0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    .line 972489
    const-string v2, "adsCurrencyCodes"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 972490
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 972491
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 972492
    :goto_1
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_0

    .line 972493
    invoke-virtual {v0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 972494
    :catch_0
    :goto_2
    return-void

    .line 972495
    :cond_0
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    sput-object v1, LX/5fy;->a:LX/0Px;

    goto :goto_0

    .line 972496
    :cond_1
    const-string v2, "allCurrenciesByCode"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 972497
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 972498
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 972499
    :goto_3
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 972500
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 972501
    invoke-virtual {v0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 972502
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 972503
    :goto_4
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_2

    .line 972504
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 972505
    invoke-virtual {v0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 972506
    :cond_2
    invoke-static {v2}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 972507
    :cond_3
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    sput-object v1, LX/5fy;->b:LX/0P1;

    goto :goto_0

    .line 972508
    :cond_4
    invoke-virtual {v0}, LX/15w;->f()LX/15w;

    goto/16 :goto_0

    .line 972509
    :cond_5
    invoke-virtual {v0}, LX/15w;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method public static c(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 972510
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "currency_config"

    const-string v2, "raw"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 972511
    if-nez v1, :cond_1

    .line 972512
    :cond_0
    :goto_0
    return-void

    .line 972513
    :cond_1
    const/4 v0, 0x0

    .line 972514
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 972515
    :try_start_1
    invoke-static {v0}, LX/5fy;->a(Ljava/io/InputStream;)V
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 972516
    if-eqz v0, :cond_0

    .line 972517
    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 972518
    :catch_0
    goto :goto_0

    .line 972519
    :catch_1
    if-eqz v0, :cond_0

    .line 972520
    :try_start_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 972521
    :catch_2
    goto :goto_0

    .line 972522
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_2

    .line 972523
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 972524
    :cond_2
    :goto_2
    throw v0

    :catch_3
    goto :goto_2

    .line 972525
    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1
.end method
