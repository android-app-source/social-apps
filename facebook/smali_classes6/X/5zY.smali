.class public final LX/5zY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 31

    .prologue
    .line 1035198
    const/16 v27, 0x0

    .line 1035199
    const/16 v26, 0x0

    .line 1035200
    const/16 v25, 0x0

    .line 1035201
    const/16 v24, 0x0

    .line 1035202
    const/16 v23, 0x0

    .line 1035203
    const/16 v22, 0x0

    .line 1035204
    const/16 v21, 0x0

    .line 1035205
    const/16 v20, 0x0

    .line 1035206
    const/16 v19, 0x0

    .line 1035207
    const/16 v18, 0x0

    .line 1035208
    const/16 v17, 0x0

    .line 1035209
    const/16 v16, 0x0

    .line 1035210
    const/4 v15, 0x0

    .line 1035211
    const/4 v14, 0x0

    .line 1035212
    const/4 v13, 0x0

    .line 1035213
    const/4 v12, 0x0

    .line 1035214
    const/4 v11, 0x0

    .line 1035215
    const/4 v10, 0x0

    .line 1035216
    const/4 v9, 0x0

    .line 1035217
    const/4 v8, 0x0

    .line 1035218
    const/4 v7, 0x0

    .line 1035219
    const/4 v6, 0x0

    .line 1035220
    const/4 v5, 0x0

    .line 1035221
    const/4 v4, 0x0

    .line 1035222
    const/4 v3, 0x0

    .line 1035223
    const/4 v2, 0x0

    .line 1035224
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    .line 1035225
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1035226
    const/4 v2, 0x0

    .line 1035227
    :goto_0
    return v2

    .line 1035228
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1035229
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_12

    .line 1035230
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v28

    .line 1035231
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1035232
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1

    if-eqz v28, :cond_1

    .line 1035233
    const-string v29, "can_viewer_act_as_memorial_contact"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_2

    .line 1035234
    const/4 v10, 0x1

    .line 1035235
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto :goto_1

    .line 1035236
    :cond_2
    const-string v29, "can_viewer_block"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_3

    .line 1035237
    const/4 v9, 0x1

    .line 1035238
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto :goto_1

    .line 1035239
    :cond_3
    const-string v29, "can_viewer_message"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_4

    .line 1035240
    const/4 v8, 0x1

    .line 1035241
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto :goto_1

    .line 1035242
    :cond_4
    const-string v29, "can_viewer_poke"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_5

    .line 1035243
    const/4 v7, 0x1

    .line 1035244
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto :goto_1

    .line 1035245
    :cond_5
    const-string v29, "can_viewer_post"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_6

    .line 1035246
    const/4 v6, 0x1

    .line 1035247
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto :goto_1

    .line 1035248
    :cond_6
    const-string v29, "can_viewer_report"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_7

    .line 1035249
    const/4 v5, 0x1

    .line 1035250
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto :goto_1

    .line 1035251
    :cond_7
    const-string v29, "friends"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_8

    .line 1035252
    invoke-static/range {p0 .. p1}, LX/5zW;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1035253
    :cond_8
    const-string v29, "friendship_status"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_9

    .line 1035254
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    goto/16 :goto_1

    .line 1035255
    :cond_9
    const-string v29, "id"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_a

    .line 1035256
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 1035257
    :cond_a
    const-string v29, "is_followed_by_everyone"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_b

    .line 1035258
    const/4 v4, 0x1

    .line 1035259
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto/16 :goto_1

    .line 1035260
    :cond_b
    const-string v29, "is_viewer_coworker"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_c

    .line 1035261
    const/4 v3, 0x1

    .line 1035262
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 1035263
    :cond_c
    const-string v29, "name"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_d

    .line 1035264
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 1035265
    :cond_d
    const-string v29, "posted_item_privacy_scope"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_e

    .line 1035266
    invoke-static/range {p0 .. p1}, LX/5R2;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1035267
    :cond_e
    const-string v29, "secondary_subscribe_status"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_f

    .line 1035268
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    goto/16 :goto_1

    .line 1035269
    :cond_f
    const-string v29, "subscribe_status"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_10

    .line 1035270
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    goto/16 :goto_1

    .line 1035271
    :cond_10
    const-string v29, "url"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_11

    .line 1035272
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1035273
    :cond_11
    const-string v29, "viewer_can_see_profile_insights"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_0

    .line 1035274
    const/4 v2, 0x1

    .line 1035275
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 1035276
    :cond_12
    const/16 v28, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1035277
    if-eqz v10, :cond_13

    .line 1035278
    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1035279
    :cond_13
    if-eqz v9, :cond_14

    .line 1035280
    const/4 v9, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1035281
    :cond_14
    if-eqz v8, :cond_15

    .line 1035282
    const/4 v8, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1035283
    :cond_15
    if-eqz v7, :cond_16

    .line 1035284
    const/4 v7, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1035285
    :cond_16
    if-eqz v6, :cond_17

    .line 1035286
    const/4 v6, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1035287
    :cond_17
    if-eqz v5, :cond_18

    .line 1035288
    const/4 v5, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1035289
    :cond_18
    const/4 v5, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1035290
    const/4 v5, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1035291
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1035292
    if-eqz v4, :cond_19

    .line 1035293
    const/16 v4, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1035294
    :cond_19
    if-eqz v3, :cond_1a

    .line 1035295
    const/16 v3, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1035296
    :cond_1a
    const/16 v3, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1035297
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1035298
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1035299
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1035300
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1035301
    if-eqz v2, :cond_1b

    .line 1035302
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 1035303
    :cond_1b
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
