.class public final enum LX/6bP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6bP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6bP;

.field public static final enum NEWEST_FIRST:LX/6bP;

.field public static final enum OLDEST_FIRST:LX/6bP;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1113562
    new-instance v0, LX/6bP;

    const-string v1, "NEWEST_FIRST"

    invoke-direct {v0, v1, v2}, LX/6bP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6bP;->NEWEST_FIRST:LX/6bP;

    .line 1113563
    new-instance v0, LX/6bP;

    const-string v1, "OLDEST_FIRST"

    invoke-direct {v0, v1, v3}, LX/6bP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6bP;->OLDEST_FIRST:LX/6bP;

    .line 1113564
    const/4 v0, 0x2

    new-array v0, v0, [LX/6bP;

    sget-object v1, LX/6bP;->NEWEST_FIRST:LX/6bP;

    aput-object v1, v0, v2

    sget-object v1, LX/6bP;->OLDEST_FIRST:LX/6bP;

    aput-object v1, v0, v3

    sput-object v0, LX/6bP;->$VALUES:[LX/6bP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1113565
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6bP;
    .locals 1

    .prologue
    .line 1113566
    const-class v0, LX/6bP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6bP;

    return-object v0
.end method

.method public static values()[LX/6bP;
    .locals 1

    .prologue
    .line 1113567
    sget-object v0, LX/6bP;->$VALUES:[LX/6bP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6bP;

    return-object v0
.end method
