.class public final LX/59W;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 49

    .prologue
    .line 852236
    const/16 v45, 0x0

    .line 852237
    const/16 v44, 0x0

    .line 852238
    const/16 v43, 0x0

    .line 852239
    const/16 v42, 0x0

    .line 852240
    const/16 v41, 0x0

    .line 852241
    const/16 v40, 0x0

    .line 852242
    const/16 v39, 0x0

    .line 852243
    const/16 v38, 0x0

    .line 852244
    const/16 v37, 0x0

    .line 852245
    const/16 v36, 0x0

    .line 852246
    const/16 v35, 0x0

    .line 852247
    const/16 v34, 0x0

    .line 852248
    const/16 v33, 0x0

    .line 852249
    const/16 v32, 0x0

    .line 852250
    const/16 v31, 0x0

    .line 852251
    const/16 v30, 0x0

    .line 852252
    const/16 v29, 0x0

    .line 852253
    const/16 v28, 0x0

    .line 852254
    const/16 v27, 0x0

    .line 852255
    const/16 v26, 0x0

    .line 852256
    const/16 v25, 0x0

    .line 852257
    const/16 v24, 0x0

    .line 852258
    const/16 v23, 0x0

    .line 852259
    const/16 v22, 0x0

    .line 852260
    const/16 v21, 0x0

    .line 852261
    const/16 v20, 0x0

    .line 852262
    const/16 v19, 0x0

    .line 852263
    const/16 v18, 0x0

    .line 852264
    const/16 v17, 0x0

    .line 852265
    const/16 v16, 0x0

    .line 852266
    const/4 v15, 0x0

    .line 852267
    const/4 v14, 0x0

    .line 852268
    const/4 v13, 0x0

    .line 852269
    const/4 v12, 0x0

    .line 852270
    const/4 v11, 0x0

    .line 852271
    const/4 v10, 0x0

    .line 852272
    const/4 v9, 0x0

    .line 852273
    const/4 v8, 0x0

    .line 852274
    const/4 v7, 0x0

    .line 852275
    const/4 v6, 0x0

    .line 852276
    const/4 v5, 0x0

    .line 852277
    const/4 v4, 0x0

    .line 852278
    const/4 v3, 0x0

    .line 852279
    const/4 v2, 0x0

    .line 852280
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v46

    sget-object v47, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    if-eq v0, v1, :cond_1

    .line 852281
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 852282
    const/4 v2, 0x0

    .line 852283
    :goto_0
    return v2

    .line 852284
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 852285
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v46

    sget-object v47, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    if-eq v0, v1, :cond_1f

    .line 852286
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v46

    .line 852287
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 852288
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v47

    sget-object v48, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    if-eq v0, v1, :cond_1

    if-eqz v46, :cond_1

    .line 852289
    const-string v47, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    .line 852290
    const/4 v15, 0x1

    .line 852291
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v45

    goto :goto_1

    .line 852292
    :cond_2
    const-string v47, "can_see_voice_switcher"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_3

    .line 852293
    const/4 v14, 0x1

    .line 852294
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v44

    goto :goto_1

    .line 852295
    :cond_3
    const-string v47, "can_viewer_comment"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_4

    .line 852296
    const/4 v13, 0x1

    .line 852297
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v43

    goto :goto_1

    .line 852298
    :cond_4
    const-string v47, "can_viewer_comment_with_photo"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_5

    .line 852299
    const/4 v12, 0x1

    .line 852300
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v42

    goto :goto_1

    .line 852301
    :cond_5
    const-string v47, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_6

    .line 852302
    const/4 v11, 0x1

    .line 852303
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v41

    goto :goto_1

    .line 852304
    :cond_6
    const-string v47, "can_viewer_comment_with_video"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_7

    .line 852305
    const/4 v10, 0x1

    .line 852306
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v40

    goto :goto_1

    .line 852307
    :cond_7
    const-string v47, "can_viewer_like"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_8

    .line 852308
    const/4 v9, 0x1

    .line 852309
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto/16 :goto_1

    .line 852310
    :cond_8
    const-string v47, "can_viewer_react"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_9

    .line 852311
    const/4 v8, 0x1

    .line 852312
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto/16 :goto_1

    .line 852313
    :cond_9
    const-string v47, "can_viewer_subscribe"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_a

    .line 852314
    const/4 v7, 0x1

    .line 852315
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto/16 :goto_1

    .line 852316
    :cond_a
    const-string v47, "comments_disabled_notice"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_b

    .line 852317
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v36

    goto/16 :goto_1

    .line 852318
    :cond_b
    const-string v47, "comments_mirroring_domain"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_c

    .line 852319
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    goto/16 :goto_1

    .line 852320
    :cond_c
    const-string v47, "count_multi_company_groups_visible"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_d

    .line 852321
    const/4 v6, 0x1

    .line 852322
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v34

    goto/16 :goto_1

    .line 852323
    :cond_d
    const-string v47, "default_comment_ordering"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_e

    .line 852324
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto/16 :goto_1

    .line 852325
    :cond_e
    const-string v47, "does_viewer_like"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_f

    .line 852326
    const/4 v5, 0x1

    .line 852327
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v32

    goto/16 :goto_1

    .line 852328
    :cond_f
    const-string v47, "have_comments_been_disabled"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_10

    .line 852329
    const/4 v4, 0x1

    .line 852330
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 852331
    :cond_10
    const-string v47, "id"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_11

    .line 852332
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 852333
    :cond_11
    const-string v47, "important_reactors"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_12

    .line 852334
    invoke-static/range {p0 .. p1}, LX/5DS;->a(LX/15w;LX/186;)I

    move-result v29

    goto/16 :goto_1

    .line 852335
    :cond_12
    const-string v47, "is_viewer_subscribed"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_13

    .line 852336
    const/4 v3, 0x1

    .line 852337
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 852338
    :cond_13
    const-string v47, "legacy_api_post_id"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_14

    .line 852339
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 852340
    :cond_14
    const-string v47, "likers"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_15

    .line 852341
    invoke-static/range {p0 .. p1}, LX/59S;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 852342
    :cond_15
    const-string v47, "reactors"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_16

    .line 852343
    invoke-static/range {p0 .. p1}, LX/5DH;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 852344
    :cond_16
    const-string v47, "remixable_photo_uri"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_17

    .line 852345
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v24

    goto/16 :goto_1

    .line 852346
    :cond_17
    const-string v47, "reshares"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_18

    .line 852347
    invoke-static/range {p0 .. p1}, LX/59T;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 852348
    :cond_18
    const-string v47, "seen_by"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_19

    .line 852349
    invoke-static/range {p0 .. p1}, LX/59U;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 852350
    :cond_19
    const-string v47, "supported_reactions"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1a

    .line 852351
    invoke-static/range {p0 .. p1}, LX/5DM;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 852352
    :cond_1a
    const-string v47, "top_level_comments"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1b

    .line 852353
    invoke-static/range {p0 .. p1}, LX/59L;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 852354
    :cond_1b
    const-string v47, "top_reactions"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1c

    .line 852355
    invoke-static/range {p0 .. p1}, LX/5DG;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 852356
    :cond_1c
    const-string v47, "viewer_acts_as_page"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1d

    .line 852357
    invoke-static/range {p0 .. p1}, LX/59V;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 852358
    :cond_1d
    const-string v47, "viewer_acts_as_person"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1e

    .line 852359
    invoke-static/range {p0 .. p1}, LX/5DT;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 852360
    :cond_1e
    const-string v47, "viewer_feedback_reaction_key"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_0

    .line 852361
    const/4 v2, 0x1

    .line 852362
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 852363
    :cond_1f
    const/16 v46, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 852364
    if-eqz v15, :cond_20

    .line 852365
    const/4 v15, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v15, v1}, LX/186;->a(IZ)V

    .line 852366
    :cond_20
    if-eqz v14, :cond_21

    .line 852367
    const/4 v14, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v14, v1}, LX/186;->a(IZ)V

    .line 852368
    :cond_21
    if-eqz v13, :cond_22

    .line 852369
    const/4 v13, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v13, v1}, LX/186;->a(IZ)V

    .line 852370
    :cond_22
    if-eqz v12, :cond_23

    .line 852371
    const/4 v12, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 852372
    :cond_23
    if-eqz v11, :cond_24

    .line 852373
    const/4 v11, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 852374
    :cond_24
    if-eqz v10, :cond_25

    .line 852375
    const/4 v10, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 852376
    :cond_25
    if-eqz v9, :cond_26

    .line 852377
    const/4 v9, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 852378
    :cond_26
    if-eqz v8, :cond_27

    .line 852379
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 852380
    :cond_27
    if-eqz v7, :cond_28

    .line 852381
    const/16 v7, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 852382
    :cond_28
    const/16 v7, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 852383
    const/16 v7, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 852384
    if-eqz v6, :cond_29

    .line 852385
    const/16 v6, 0xb

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 852386
    :cond_29
    const/16 v6, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 852387
    if-eqz v5, :cond_2a

    .line 852388
    const/16 v5, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 852389
    :cond_2a
    if-eqz v4, :cond_2b

    .line 852390
    const/16 v4, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 852391
    :cond_2b
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 852392
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 852393
    if-eqz v3, :cond_2c

    .line 852394
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 852395
    :cond_2c
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852396
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852397
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852398
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852399
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852400
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852401
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852402
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852403
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852404
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852405
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 852406
    if-eqz v2, :cond_2d

    .line 852407
    const/16 v2, 0x1d

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 852408
    :cond_2d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 852409
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 852410
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 852411
    if-eqz v0, :cond_0

    .line 852412
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852413
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852414
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852415
    if-eqz v0, :cond_1

    .line 852416
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852417
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852418
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852419
    if-eqz v0, :cond_2

    .line 852420
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852421
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852422
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852423
    if-eqz v0, :cond_3

    .line 852424
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852425
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852426
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852427
    if-eqz v0, :cond_4

    .line 852428
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852429
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852430
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852431
    if-eqz v0, :cond_5

    .line 852432
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852433
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852434
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852435
    if-eqz v0, :cond_6

    .line 852436
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852437
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852438
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852439
    if-eqz v0, :cond_7

    .line 852440
    const-string v1, "can_viewer_react"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852441
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852442
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852443
    if-eqz v0, :cond_8

    .line 852444
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852445
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852446
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852447
    if-eqz v0, :cond_9

    .line 852448
    const-string v1, "comments_disabled_notice"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852449
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 852450
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 852451
    if-eqz v0, :cond_a

    .line 852452
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852453
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 852454
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 852455
    if-eqz v0, :cond_b

    .line 852456
    const-string v1, "count_multi_company_groups_visible"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852457
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 852458
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 852459
    if-eqz v0, :cond_c

    .line 852460
    const-string v1, "default_comment_ordering"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852461
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 852462
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852463
    if-eqz v0, :cond_d

    .line 852464
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852465
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852466
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852467
    if-eqz v0, :cond_e

    .line 852468
    const-string v1, "have_comments_been_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852469
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852470
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 852471
    if-eqz v0, :cond_f

    .line 852472
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852473
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 852474
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852475
    if-eqz v0, :cond_10

    .line 852476
    const-string v1, "important_reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852477
    invoke-static {p0, v0, p2, p3}, LX/5DS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 852478
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 852479
    if-eqz v0, :cond_11

    .line 852480
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852481
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 852482
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 852483
    if-eqz v0, :cond_12

    .line 852484
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852485
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 852486
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852487
    if-eqz v0, :cond_13

    .line 852488
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852489
    invoke-static {p0, v0, p2}, LX/59S;->a(LX/15i;ILX/0nX;)V

    .line 852490
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852491
    if-eqz v0, :cond_14

    .line 852492
    const-string v1, "reactors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852493
    invoke-static {p0, v0, p2, p3}, LX/5DH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 852494
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 852495
    if-eqz v0, :cond_15

    .line 852496
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852497
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 852498
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852499
    if-eqz v0, :cond_16

    .line 852500
    const-string v1, "reshares"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852501
    invoke-static {p0, v0, p2}, LX/59T;->a(LX/15i;ILX/0nX;)V

    .line 852502
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852503
    if-eqz v0, :cond_17

    .line 852504
    const-string v1, "seen_by"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852505
    invoke-static {p0, v0, p2}, LX/59U;->a(LX/15i;ILX/0nX;)V

    .line 852506
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852507
    if-eqz v0, :cond_18

    .line 852508
    const-string v1, "supported_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852509
    invoke-static {p0, v0, p2, p3}, LX/5DM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 852510
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852511
    if-eqz v0, :cond_19

    .line 852512
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852513
    invoke-static {p0, v0, p2, p3}, LX/59L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 852514
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852515
    if-eqz v0, :cond_1a

    .line 852516
    const-string v1, "top_reactions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852517
    invoke-static {p0, v0, p2, p3}, LX/5DG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 852518
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852519
    if-eqz v0, :cond_1b

    .line 852520
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852521
    invoke-static {p0, v0, p2, p3}, LX/59V;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 852522
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 852523
    if-eqz v0, :cond_1c

    .line 852524
    const-string v1, "viewer_acts_as_person"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852525
    invoke-static {p0, v0, p2}, LX/5DT;->a(LX/15i;ILX/0nX;)V

    .line 852526
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 852527
    if-eqz v0, :cond_1d

    .line 852528
    const-string v1, "viewer_feedback_reaction_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 852529
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 852530
    :cond_1d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 852531
    return-void
.end method
