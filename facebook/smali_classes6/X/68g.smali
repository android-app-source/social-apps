.class public final LX/68g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final synthetic a:LX/68h;

.field private final b:Ljava/io/InputStream;

.field public final c:Ljava/nio/charset/Charset;

.field private d:[B

.field private e:I

.field private f:I


# direct methods
.method private constructor <init>(LX/68h;Ljava/io/InputStream;ILjava/nio/charset/Charset;)V
    .locals 2

    .prologue
    .line 1055834
    iput-object p1, p0, LX/68g;->a:LX/68h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1055835
    if-eqz p2, :cond_0

    if-nez p4, :cond_1

    .line 1055836
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1055837
    :cond_1
    if-gez p3, :cond_2

    .line 1055838
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "capacity <= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055839
    :cond_2
    sget-object v0, LX/68h;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p4, v0}, Ljava/nio/charset/Charset;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1055840
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported encoding"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055841
    :cond_3
    iput-object p2, p0, LX/68g;->b:Ljava/io/InputStream;

    .line 1055842
    iput-object p4, p0, LX/68g;->c:Ljava/nio/charset/Charset;

    .line 1055843
    new-array v0, p3, [B

    iput-object v0, p0, LX/68g;->d:[B

    .line 1055844
    return-void
.end method

.method public constructor <init>(LX/68h;Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    .locals 1

    .prologue
    .line 1055832
    const/16 v0, 0x2000

    invoke-direct {p0, p1, p2, v0, p3}, LX/68g;-><init>(LX/68h;Ljava/io/InputStream;ILjava/nio/charset/Charset;)V

    .line 1055833
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1055826
    iget-object v0, p0, LX/68g;->b:Ljava/io/InputStream;

    iget-object v1, p0, LX/68g;->d:[B

    iget-object v2, p0, LX/68g;->d:[B

    array-length v2, v2

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 1055827
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1055828
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1055829
    :cond_0
    iput v3, p0, LX/68g;->e:I

    .line 1055830
    iput v0, p0, LX/68g;->f:I

    .line 1055831
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v6, 0xa

    .line 1055795
    iget-object v3, p0, LX/68g;->b:Ljava/io/InputStream;

    monitor-enter v3

    .line 1055796
    :try_start_0
    iget-object v0, p0, LX/68g;->d:[B

    if-nez v0, :cond_0

    .line 1055797
    new-instance v0, Ljava/io/IOException;

    const-string v1, "LineReader is closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1055798
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1055799
    :cond_0
    :try_start_1
    iget v0, p0, LX/68g;->e:I

    iget v1, p0, LX/68g;->f:I

    if-lt v0, v1, :cond_1

    .line 1055800
    invoke-direct {p0}, LX/68g;->b()V

    .line 1055801
    :cond_1
    iget v2, p0, LX/68g;->e:I

    :goto_0
    iget v0, p0, LX/68g;->f:I

    if-eq v2, v0, :cond_4

    .line 1055802
    iget-object v0, p0, LX/68g;->d:[B

    aget-byte v0, v0, v2

    if-ne v0, v6, :cond_3

    .line 1055803
    iget v0, p0, LX/68g;->e:I

    if-eq v2, v0, :cond_2

    iget-object v0, p0, LX/68g;->d:[B

    add-int/lit8 v1, v2, -0x1

    aget-byte v0, v0, v1

    const/16 v1, 0xd

    if-ne v0, v1, :cond_2

    add-int/lit8 v0, v2, -0x1

    move v1, v0

    .line 1055804
    :goto_1
    new-instance v0, Ljava/lang/String;

    iget-object v4, p0, LX/68g;->d:[B

    iget v5, p0, LX/68g;->e:I

    iget v6, p0, LX/68g;->e:I

    sub-int/2addr v1, v6

    iget-object v6, p0, LX/68g;->c:Ljava/nio/charset/Charset;

    invoke-virtual {v6}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v4, v5, v1, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 1055805
    add-int/lit8 v1, v2, 0x1

    iput v1, p0, LX/68g;->e:I

    .line 1055806
    monitor-exit v3

    .line 1055807
    :goto_2
    return-object v0

    :cond_2
    move v1, v2

    .line 1055808
    goto :goto_1

    .line 1055809
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1055810
    :cond_4
    new-instance v1, LX/68f;

    iget v0, p0, LX/68g;->f:I

    iget v2, p0, LX/68g;->e:I

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x50

    invoke-direct {v1, p0, v0}, LX/68f;-><init>(LX/68g;I)V

    .line 1055811
    :cond_5
    iget-object v0, p0, LX/68g;->d:[B

    iget v2, p0, LX/68g;->e:I

    iget v4, p0, LX/68g;->f:I

    iget v5, p0, LX/68g;->e:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 1055812
    const/4 v0, -0x1

    iput v0, p0, LX/68g;->f:I

    .line 1055813
    invoke-direct {p0}, LX/68g;->b()V

    .line 1055814
    iget v0, p0, LX/68g;->e:I

    :goto_3
    iget v2, p0, LX/68g;->f:I

    if-eq v0, v2, :cond_5

    .line 1055815
    iget-object v2, p0, LX/68g;->d:[B

    aget-byte v2, v2, v0

    if-ne v2, v6, :cond_7

    .line 1055816
    iget v2, p0, LX/68g;->e:I

    if-eq v0, v2, :cond_6

    .line 1055817
    iget-object v2, p0, LX/68g;->d:[B

    iget v4, p0, LX/68g;->e:I

    iget v5, p0, LX/68g;->e:I

    sub-int v5, v0, v5

    invoke-virtual {v1, v2, v4, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 1055818
    :cond_6
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/68g;->e:I

    .line 1055819
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1055820
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 1055821
    iget-object v1, p0, LX/68g;->b:Ljava/io/InputStream;

    monitor-enter v1

    .line 1055822
    :try_start_0
    iget-object v0, p0, LX/68g;->d:[B

    if-eqz v0, :cond_0

    .line 1055823
    const/4 v0, 0x0

    iput-object v0, p0, LX/68g;->d:[B

    .line 1055824
    iget-object v0, p0, LX/68g;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 1055825
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
