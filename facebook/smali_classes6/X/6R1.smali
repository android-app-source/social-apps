.class public LX/6R1;
.super Landroid/widget/ProgressBar;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1088401
    invoke-direct {p0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 1088402
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1088403
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1088404
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1088405
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1088406
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1088407
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 1088408
    :goto_0
    if-eqz v0, :cond_0

    .line 1088409
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1088410
    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p0}, LX/6R1;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, LX/6R1;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1088411
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onDraw(Landroid/graphics/Canvas;)V

    .line 1088412
    if-eqz v0, :cond_1

    .line 1088413
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1088414
    :cond_1
    return-void

    .line 1088415
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
