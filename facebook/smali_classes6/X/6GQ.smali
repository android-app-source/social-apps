.class public LX/6GQ;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/03R;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03R;)V
    .locals 0
    .param p2    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1070452
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1070453
    iput-object p1, p0, LX/6GQ;->a:Landroid/content/Context;

    .line 1070454
    iput-object p2, p0, LX/6GQ;->b:LX/03R;

    .line 1070455
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;
    .locals 1

    .prologue
    .line 1070456
    iget-object v0, p0, LX/6GQ;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1070457
    iget-object v0, p0, LX/6GQ;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    .line 1070458
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1070459
    iget-object v0, p0, LX/6GQ;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1070460
    invoke-virtual {p0, p1}, LX/6GQ;->a(I)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 4

    .prologue
    .line 1070461
    iget-object v0, p0, LX/6GQ;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1070462
    iget-object v0, p0, LX/6GQ;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    .line 1070463
    iget-wide v2, v0, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->c:J

    move-wide v0, v2

    .line 1070464
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1070465
    check-cast p2, LX/6GS;

    .line 1070466
    iget-object v0, p0, LX/6GQ;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "listview index is not valid"

    invoke-static {v0, v1}, LX/45Y;->a(ZLjava/lang/String;)V

    .line 1070467
    if-nez p2, :cond_0

    .line 1070468
    new-instance p2, LX/6GS;

    iget-object v0, p0, LX/6GQ;->a:Landroid/content/Context;

    iget-object v1, p0, LX/6GQ;->b:LX/03R;

    invoke-direct {p2, v0, v1}, LX/6GS;-><init>(Landroid/content/Context;LX/03R;)V

    .line 1070469
    :cond_0
    invoke-virtual {p0, p1}, LX/6GQ;->a(I)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v0

    .line 1070470
    iput-object v0, p2, LX/6GS;->c:Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    .line 1070471
    iget-object v1, p2, LX/6GS;->a:Landroid/widget/TextView;

    iget-object p0, p2, LX/6GS;->b:LX/03R;

    invoke-virtual {v0, p0}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(LX/03R;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1070472
    return-object p2

    .line 1070473
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1070474
    const/4 v0, 0x1

    return v0
.end method
