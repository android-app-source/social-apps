.class public LX/6BH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/api/ufiservices/common/DeleteCommentParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1061815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1061816
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1061809
    check-cast p1, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;

    .line 1061810
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1061811
    iget-object v0, p1, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1061812
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "tracking"

    iget-object v2, p1, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1061813
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1061814
    :cond_0
    new-instance v0, LX/14N;

    const-string v1, "graphCommentDelete"

    const-string v2, "DELETE"

    iget-object v3, p1, Lcom/facebook/api/ufiservices/common/DeleteCommentParams;->b:Ljava/lang/String;

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1061807
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1061808
    const/4 v0, 0x0

    return-object v0
.end method
