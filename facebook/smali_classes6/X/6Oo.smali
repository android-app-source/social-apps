.class public LX/6Oo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0aG;


# direct methods
.method public constructor <init>(LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1085559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1085560
    iput-object p1, p0, LX/6Oo;->a:LX/0aG;

    .line 1085561
    return-void
.end method

.method public static a(LX/6Oo;LX/0Rf;LX/0rS;Z)LX/1ML;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0rS;",
            "Z)",
            "LX/1ML;"
        }
    .end annotation

    .prologue
    .line 1085562
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1085563
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1085564
    new-instance v1, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;

    invoke-direct {v1, p1, p2}, Lcom/facebook/contacts/server/FetchMultipleContactsByFbidParams;-><init>(LX/0Rf;LX/0rS;)V

    .line 1085565
    const-string v2, "fetchMultipleContactsParams"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1085566
    if-eqz p3, :cond_0

    .line 1085567
    iget-object v1, p0, LX/6Oo;->a:LX/0aG;

    const-string v2, "fetch_contacts"

    const v3, -0x15e3fe95

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 1085568
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/6Oo;->a:LX/0aG;

    const-string v2, "fetch_contacts"

    const v3, 0x59a98ed7

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6Oo;
    .locals 2

    .prologue
    .line 1085569
    new-instance v1, LX/6Oo;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-direct {v1, v0}, LX/6Oo;-><init>(LX/0aG;)V

    .line 1085570
    return-object v1
.end method


# virtual methods
.method public final a(LX/0Rf;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0rS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1085571
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1085572
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, LX/6Oo;->a(LX/6Oo;LX/0Rf;LX/0rS;Z)LX/1ML;

    move-result-object v0

    move-object v0, v0

    .line 1085573
    new-instance v1, LX/6Om;

    invoke-direct {v1, p0}, LX/6Om;-><init>(LX/6Oo;)V

    invoke-static {v0, v1}, LX/2Ck;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/user/model/UserKey;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/user/model/UserKey;",
            "LX/0rS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/contacts/graphql/Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1085574
    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, p2, v1}, LX/6Oo;->a(LX/6Oo;LX/0Rf;LX/0rS;Z)LX/1ML;

    move-result-object v0

    move-object v0, v0

    .line 1085575
    new-instance v1, LX/6On;

    invoke-direct {v1, p0}, LX/6On;-><init>(LX/6Oo;)V

    invoke-static {v0, v1}, LX/2Ck;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
