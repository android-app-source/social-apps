.class public LX/5rl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:[I

.field public final b:LX/5qw;

.field private final c:LX/5om;

.field private final d:Ljava/lang/Object;

.field public final e:Ljava/lang/Object;

.field private final f:LX/5rZ;

.field private final g:LX/5pY;

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mDispatchRunnablesLock"
    .end annotation
.end field

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5rT;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "LX/5rT;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mNonBatchedOperationsLock"
    .end annotation
.end field

.field public k:LX/5qU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z


# direct methods
.method public constructor <init>(LX/5pY;LX/5qw;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1012050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1012051
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, LX/5rl;->a:[I

    .line 1012052
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/5rl;->d:Ljava/lang/Object;

    .line 1012053
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/5rl;->e:Ljava/lang/Object;

    .line 1012054
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5rl;->h:Ljava/util/ArrayList;

    .line 1012055
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    .line 1012056
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/5rl;->j:Ljava/util/ArrayDeque;

    .line 1012057
    iput-boolean v1, p0, LX/5rl;->l:Z

    .line 1012058
    iput-object p2, p0, LX/5rl;->b:LX/5qw;

    .line 1012059
    iget-object v0, p2, LX/5qw;->b:LX/5om;

    move-object v0, v0

    .line 1012060
    iput-object v0, p0, LX/5rl;->c:LX/5om;

    .line 1012061
    new-instance v0, LX/5rZ;

    invoke-direct {v0, p0, p1}, LX/5rZ;-><init>(LX/5rl;LX/5pX;)V

    iput-object v0, p0, LX/5rl;->f:LX/5rZ;

    .line 1012062
    iput-object p1, p0, LX/5rl;->g:LX/5pY;

    .line 1012063
    return-void
.end method

.method public static f(LX/5rl;)V
    .locals 3

    .prologue
    .line 1012064
    iget-object v2, p0, LX/5rl;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 1012065
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, LX/5rl;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1012066
    iget-object v0, p0, LX/5rl;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1012067
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1012068
    :cond_0
    iget-object v0, p0, LX/5rl;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1012069
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1012070
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5re;

    invoke-direct {v1, p0, p1}, LX/5re;-><init>(LX/5rl;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012071
    return-void
.end method

.method public a(IFFLcom/facebook/react/bridge/Callback;)V
    .locals 8

    .prologue
    .line 1012072
    iget-object v7, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v0, LX/5ra;

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LX/5ra;-><init>(LX/5rl;IFFLcom/facebook/react/bridge/Callback;B)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012073
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 1012074
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5rf;

    invoke-direct {v1, p0, p1, p2}, LX/5rf;-><init>(LX/5rl;II)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012075
    return-void
.end method

.method public final a(IIIIII)V
    .locals 9

    .prologue
    .line 1012076
    iget-object v8, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v0, LX/5ri;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, LX/5ri;-><init>(LX/5rl;IIIIII)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012077
    return-void
.end method

.method public final a(IILX/5pC;)V
    .locals 2

    .prologue
    .line 1012092
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5rY;

    invoke-direct {v1, p0, p1, p2, p3}, LX/5rY;-><init>(LX/5rl;IILX/5pC;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012093
    return-void
.end method

.method public final a(IIZ)V
    .locals 7

    .prologue
    .line 1012078
    iget-object v6, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v0, LX/5rV;

    const/4 v4, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/5rV;-><init>(LX/5rl;IIZZ)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012079
    return-void
.end method

.method public final a(ILX/5pC;Lcom/facebook/react/bridge/Callback;)V
    .locals 2

    .prologue
    .line 1012080
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5rh;

    invoke-direct {v1, p0, p1, p2, p3}, LX/5rh;-><init>(LX/5rl;ILX/5pC;Lcom/facebook/react/bridge/Callback;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012081
    return-void
.end method

.method public final a(ILX/5rC;)V
    .locals 3

    .prologue
    .line 1012082
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5rj;

    invoke-direct {v1, p0, p1, p2}, LX/5rj;-><init>(LX/5rl;ILX/5rC;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012083
    return-void
.end method

.method public final a(ILX/5rH;LX/5rJ;)V
    .locals 7

    .prologue
    .line 1012084
    invoke-static {}, LX/5pe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1012085
    iget-object v0, p0, LX/5rl;->b:LX/5qw;

    invoke-virtual {v0, p1, p2, p3}, LX/5qw;->a(ILX/5rH;LX/5rJ;)V

    .line 1012086
    :goto_0
    return-void

    .line 1012087
    :cond_0
    new-instance v5, Ljava/util/concurrent/Semaphore;

    const/4 v0, 0x0

    invoke-direct {v5, v0}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 1012088
    iget-object v6, p0, LX/5rl;->g:LX/5pY;

    new-instance v0, Lcom/facebook/react/uimanager/UIViewOperationQueue$1;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/react/uimanager/UIViewOperationQueue$1;-><init>(LX/5rl;ILX/5rH;LX/5rJ;Ljava/util/concurrent/Semaphore;)V

    invoke-virtual {v6, v0}, LX/5pX;->a(Ljava/lang/Runnable;)V

    .line 1012089
    const-wide/16 v0, 0x1388

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v0, v1, v2}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    const-string v1, "Timed out adding root view"

    invoke-static {v0, v1}, LX/5pd;->a(ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1012090
    :catch_0
    move-exception v0

    .line 1012091
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(ILcom/facebook/react/bridge/Callback;)V
    .locals 3

    .prologue
    .line 1012048
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5rd;

    invoke-direct {v1, p0, p1, p2}, LX/5rd;-><init>(LX/5rl;ILcom/facebook/react/bridge/Callback;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012049
    return-void
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1012046
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5rk;

    invoke-direct {v1, p0, p1, p2}, LX/5rk;-><init>(LX/5rl;ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012047
    return-void
.end method

.method public final a(I[I[LX/5rn;[I)V
    .locals 7
    .param p2    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [LX/5rn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # [I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1012004
    iget-object v6, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v0, LX/5rb;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/5rb;-><init>(LX/5rl;I[I[LX/5rn;[I)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012005
    return-void
.end method

.method public final a(LX/5pG;)V
    .locals 3

    .prologue
    .line 1012006
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5rW;

    invoke-direct {v1, p0, p1}, LX/5rW;-><init>(LX/5rl;LX/5pG;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012007
    return-void
.end method

.method public final a(LX/5rJ;ILjava/lang/String;LX/5rC;)V
    .locals 8
    .param p4    # LX/5rC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1012008
    iget-object v6, p0, LX/5rl;->e:Ljava/lang/Object;

    monitor-enter v6

    .line 1012009
    :try_start_0
    iget-object v7, p0, LX/5rl;->j:Ljava/util/ArrayDeque;

    new-instance v0, LX/5rX;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/5rX;-><init>(LX/5rl;LX/5rJ;ILjava/lang/String;LX/5rC;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 1012010
    monitor-exit v6

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/5rT;)V
    .locals 1

    .prologue
    .line 1012011
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012012
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1012013
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5rg;

    invoke-direct {v1, p0, p1}, LX/5rg;-><init>(LX/5rl;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012014
    return-void
.end method

.method public final b(I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1012015
    iget-object v1, p0, LX/5rl;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v1, v0

    .line 1012016
    :goto_0
    if-eqz v1, :cond_0

    .line 1012017
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/5rl;->i:Ljava/util/ArrayList;

    .line 1012018
    :cond_0
    iget-object v2, p0, LX/5rl;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 1012019
    :try_start_0
    iget-object v3, p0, LX/5rl;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1012020
    iget-object v0, p0, LX/5rl;->j:Ljava/util/ArrayDeque;

    iget-object v3, p0, LX/5rl;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v3

    new-array v3, v3, [LX/5rT;

    invoke-virtual {v0, v3}, Ljava/util/ArrayDeque;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5rT;

    .line 1012021
    iget-object v3, p0, LX/5rl;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->clear()V

    .line 1012022
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1012023
    iget-object v2, p0, LX/5rl;->k:LX/5qU;

    if-eqz v2, :cond_2

    .line 1012024
    iget-object v2, p0, LX/5rl;->k:LX/5qU;

    invoke-interface {v2}, LX/5qU;->c()V

    .line 1012025
    :cond_2
    iget-object v2, p0, LX/5rl;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 1012026
    :try_start_1
    iget-object v3, p0, LX/5rl;->h:Ljava/util/ArrayList;

    new-instance v4, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;

    invoke-direct {v4, p0, p1, v0, v1}, Lcom/facebook/react/uimanager/UIViewOperationQueue$2;-><init>(LX/5rl;I[LX/5rT;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012027
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1012028
    iget-boolean v0, p0, LX/5rl;->l:Z

    if-nez v0, :cond_3

    .line 1012029
    new-instance v0, Lcom/facebook/react/uimanager/UIViewOperationQueue$3;

    invoke-direct {v0, p0}, Lcom/facebook/react/uimanager/UIViewOperationQueue$3;-><init>(LX/5rl;)V

    invoke-static {v0}, LX/5pe;->a(Ljava/lang/Runnable;)V

    .line 1012030
    :cond_3
    return-void

    .line 1012031
    :cond_4
    iget-object v1, p0, LX/5rl;->i:Ljava/util/ArrayList;

    goto :goto_0

    .line 1012032
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1012033
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final b(ILcom/facebook/react/bridge/Callback;)V
    .locals 3

    .prologue
    .line 1012034
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v1, LX/5rc;

    invoke-direct {v1, p0, p1, p2}, LX/5rc;-><init>(LX/5rl;ILcom/facebook/react/bridge/Callback;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012035
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1012036
    iget-object v0, p0, LX/5rl;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1012037
    iget-object v6, p0, LX/5rl;->i:Ljava/util/ArrayList;

    new-instance v0, LX/5rV;

    const/4 v4, 0x1

    move-object v1, p0

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/5rV;-><init>(LX/5rl;IIZZ)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012038
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1012039
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5rl;->l:Z

    .line 1012040
    invoke-static {}, LX/5r6;->a()LX/5r6;

    move-result-object v0

    sget-object v1, LX/5r4;->DISPATCH_UI:LX/5r4;

    iget-object v2, p0, LX/5rl;->f:LX/5rZ;

    invoke-virtual {v0, v1, v2}, LX/5r6;->a(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 1012041
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1012042
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5rl;->l:Z

    .line 1012043
    invoke-static {}, LX/5r6;->a()LX/5r6;

    move-result-object v0

    sget-object v1, LX/5r4;->DISPATCH_UI:LX/5r4;

    iget-object v2, p0, LX/5rl;->f:LX/5rZ;

    invoke-virtual {v0, v1, v2}, LX/5r6;->b(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 1012044
    invoke-static {p0}, LX/5rl;->f(LX/5rl;)V

    .line 1012045
    return-void
.end method
