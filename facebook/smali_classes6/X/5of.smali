.class public LX/5of;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/5of;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private d:I


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007434
    iget-object v0, p0, LX/5of;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1007416
    iget v0, p0, LX/5of;->c:I

    return v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 1007421
    iget v0, p0, LX/5of;->c:I

    packed-switch v0, :pswitch_data_0

    .line 1007422
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/5of;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1007423
    :pswitch_0
    iget v0, p0, LX/5of;->d:I

    int-to-short v0, v0

    invoke-static {v0}, LX/0c2;->b(S)I

    move-result v0

    .line 1007424
    :goto_0
    return v0

    .line 1007425
    :pswitch_1
    iget v0, p0, LX/5of;->d:I

    int-to-char v0, v0

    .line 1007426
    shr-int/lit8 v1, v0, 0x1

    move v0, v1

    .line 1007427
    goto :goto_0

    .line 1007428
    :pswitch_2
    iget v0, p0, LX/5of;->d:I

    .line 1007429
    shr-int/lit8 v1, v0, 0x1

    move v0, v1

    .line 1007430
    goto :goto_0

    .line 1007431
    :pswitch_3
    iget v0, p0, LX/5of;->d:I

    int-to-long v0, v0

    invoke-static {v0, v1}, LX/0c2;->b(J)I

    move-result v0

    goto :goto_0

    .line 1007432
    :pswitch_4
    iget v0, p0, LX/5of;->d:I

    int-to-float v0, v0

    invoke-static {v0}, LX/0c2;->b(F)I

    move-result v0

    goto :goto_0

    .line 1007433
    :pswitch_5
    iget v0, p0, LX/5of;->d:I

    int-to-double v0, v0

    invoke-static {v0, v1}, LX/0c2;->b(D)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1007419
    check-cast p1, LX/5of;

    .line 1007420
    iget-object v0, p0, LX/5of;->a:Ljava/lang/String;

    iget-object v1, p1, LX/5of;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1007418
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/5of;

    if-eqz v0, :cond_0

    check-cast p1, LX/5of;

    iget-object v0, p1, LX/5of;->a:Ljava/lang/String;

    iget-object v1, p0, LX/5of;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1007417
    iget-object v0, p0, LX/5of;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
