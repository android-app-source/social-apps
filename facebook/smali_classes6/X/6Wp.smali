.class public LX/6Wp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/6Wf;",
        "LX/0lF;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1106523
    const-string v0, "fqlMultiQueryMethod"

    invoke-direct {p0, v0}, LX/6Wp;-><init>(Ljava/lang/String;)V

    .line 1106524
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1106525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106526
    iput-object p1, p0, LX/6Wp;->a:Ljava/lang/String;

    .line 1106527
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1106528
    check-cast p1, LX/6Wf;

    .line 1106529
    const/4 v0, 0x2

    invoke-static {v0}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 1106530
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "queries"

    invoke-virtual {p1}, LX/6Wf;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1106531
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1106532
    new-instance v0, LX/14N;

    .line 1106533
    iget-object v1, p0, LX/6Wp;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1106534
    const-string v2, "GET"

    const-string v3, "method/fql.multiquery"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1106535
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1106536
    :try_start_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1106537
    :catch_0
    move-exception v0

    .line 1106538
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Exception when trying to get node"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
