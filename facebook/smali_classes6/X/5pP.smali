.class public LX/5pP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Ljava/io/Writer;

.field private final b:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "LX/5pO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/Writer;)V
    .locals 1

    .prologue
    .line 1008115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008116
    iput-object p1, p0, LX/5pP;->a:Ljava/io/Writer;

    .line 1008117
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, LX/5pP;->b:Ljava/util/Deque;

    .line 1008118
    return-void
.end method

.method private a(C)V
    .locals 1

    .prologue
    .line 1008109
    iget-object v0, p0, LX/5pP;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pop()Ljava/lang/Object;

    .line 1008110
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(I)V

    .line 1008111
    return-void
.end method

.method private a(LX/5pO;)V
    .locals 1

    .prologue
    .line 1008112
    iget-object v0, p0, LX/5pP;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->pop()Ljava/lang/Object;

    .line 1008113
    iget-object v0, p0, LX/5pP;->b:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 1008114
    return-void
.end method

.method private a(LX/5pO;C)V
    .locals 1

    .prologue
    .line 1008136
    iget-object v0, p0, LX/5pP;->b:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->push(Ljava/lang/Object;)V

    .line 1008137
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    invoke-virtual {v0, p2}, Ljava/io/Writer;->write(I)V

    .line 1008138
    return-void
.end method

.method private c()LX/5pP;
    .locals 2

    .prologue
    .line 1008072
    invoke-direct {p0}, LX/5pP;->d()V

    .line 1008073
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1008074
    return-object p0
.end method

.method private c(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/16 v8, 0x22

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1008119
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    invoke-virtual {v0, v8}, Ljava/io/Writer;->write(I)V

    .line 1008120
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    .line 1008121
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 1008122
    sparse-switch v3, :sswitch_data_0

    .line 1008123
    const/16 v4, 0x1f

    if-gt v3, v4, :cond_0

    .line 1008124
    iget-object v4, p0, LX/5pP;->a:Ljava/io/Writer;

    const-string v5, "\\u%04x"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1008125
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1008126
    :sswitch_0
    iget-object v3, p0, LX/5pP;->a:Ljava/io/Writer;

    const-string v4, "\\t"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 1008127
    :sswitch_1
    iget-object v3, p0, LX/5pP;->a:Ljava/io/Writer;

    const-string v4, "\\b"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 1008128
    :sswitch_2
    iget-object v3, p0, LX/5pP;->a:Ljava/io/Writer;

    const-string v4, "\\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 1008129
    :sswitch_3
    iget-object v3, p0, LX/5pP;->a:Ljava/io/Writer;

    const-string v4, "\\r"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 1008130
    :sswitch_4
    iget-object v3, p0, LX/5pP;->a:Ljava/io/Writer;

    const-string v4, "\\f"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 1008131
    :sswitch_5
    iget-object v4, p0, LX/5pP;->a:Ljava/io/Writer;

    const/16 v5, 0x5c

    invoke-virtual {v4, v5}, Ljava/io/Writer;->write(I)V

    .line 1008132
    :cond_0
    iget-object v4, p0, LX/5pP;->a:Ljava/io/Writer;

    invoke-virtual {v4, v3}, Ljava/io/Writer;->write(I)V

    goto :goto_1

    .line 1008133
    :sswitch_6
    iget-object v4, p0, LX/5pP;->a:Ljava/io/Writer;

    const-string v5, "\\u%04x"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    .line 1008134
    :cond_1
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    invoke-virtual {v0, v8}, Ljava/io/Writer;->write(I)V

    .line 1008135
    return-void

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x9 -> :sswitch_0
        0xa -> :sswitch_2
        0xc -> :sswitch_4
        0xd -> :sswitch_3
        0x22 -> :sswitch_5
        0x5c -> :sswitch_5
        0x2028 -> :sswitch_6
        0x2029 -> :sswitch_6
    .end sparse-switch
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1008095
    iget-object v0, p0, LX/5pP;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pO;

    .line 1008096
    sget-object v1, LX/5pN;->a:[I

    invoke-virtual {v0}, LX/5pO;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1008097
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown scope: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1008098
    :pswitch_0
    sget-object v0, LX/5pO;->ARRAY:LX/5pO;

    invoke-direct {p0, v0}, LX/5pP;->a(LX/5pO;)V

    .line 1008099
    :goto_0
    :pswitch_1
    return-void

    .line 1008100
    :pswitch_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, LX/5pO;->EMPTY_OBJECT:LX/5pO;

    invoke-virtual {v1}, LX/5pO;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008101
    :pswitch_3
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private e()V
    .locals 4

    .prologue
    .line 1008102
    iget-object v0, p0, LX/5pP;->b:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pO;

    .line 1008103
    sget-object v1, LX/5pN;->a:[I

    invoke-virtual {v0}, LX/5pO;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1008104
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown scope: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1008105
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "name not allowed in array"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008106
    :pswitch_1
    sget-object v0, LX/5pO;->OBJECT:LX/5pO;

    invoke-direct {p0, v0}, LX/5pP;->a(LX/5pO;)V

    .line 1008107
    :goto_0
    return-void

    .line 1008108
    :pswitch_2
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/5pP;
    .locals 2

    .prologue
    .line 1008093
    sget-object v0, LX/5pO;->EMPTY_OBJECT:LX/5pO;

    const/16 v1, 0x7b

    invoke-direct {p0, v0, v1}, LX/5pP;->a(LX/5pO;C)V

    .line 1008094
    return-object p0
.end method

.method public final a(J)LX/5pP;
    .locals 3

    .prologue
    .line 1008090
    invoke-direct {p0}, LX/5pP;->d()V

    .line 1008091
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1008092
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/5pP;
    .locals 2

    .prologue
    .line 1008084
    if-nez p1, :cond_0

    .line 1008085
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008086
    :cond_0
    invoke-direct {p0}, LX/5pP;->e()V

    .line 1008087
    invoke-direct {p0, p1}, LX/5pP;->c(Ljava/lang/String;)V

    .line 1008088
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 1008089
    return-object p0
.end method

.method public final b()LX/5pP;
    .locals 1

    .prologue
    .line 1008082
    const/16 v0, 0x7d

    invoke-direct {p0, v0}, LX/5pP;->a(C)V

    .line 1008083
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/5pP;
    .locals 0

    .prologue
    .line 1008077
    if-nez p1, :cond_0

    .line 1008078
    invoke-direct {p0}, LX/5pP;->c()LX/5pP;

    move-result-object p0

    .line 1008079
    :goto_0
    return-object p0

    .line 1008080
    :cond_0
    invoke-direct {p0}, LX/5pP;->d()V

    .line 1008081
    invoke-direct {p0, p1}, LX/5pP;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1008075
    iget-object v0, p0, LX/5pP;->a:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    .line 1008076
    return-void
.end method
