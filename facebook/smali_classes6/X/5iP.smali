.class public final LX/5iP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionThumbnailModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J

.field public e:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Z

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$InstructionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 982877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 982850
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 982851
    iget-object v1, p0, LX/5iP;->a:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionTextModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 982852
    iget-object v2, p0, LX/5iP;->b:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionThumbnailModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 982853
    iget-object v3, p0, LX/5iP;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$CreativeFilterModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 982854
    iget-object v6, p0, LX/5iP;->e:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameImageAssetsModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 982855
    iget-object v7, p0, LX/5iP;->f:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 982856
    iget-object v8, p0, LX/5iP;->h:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 982857
    iget-object v9, p0, LX/5iP;->i:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$InstructionsModel;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 982858
    const/16 v10, 0xb

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 982859
    invoke-virtual {v0, v12, v1}, LX/186;->b(II)V

    .line 982860
    invoke-virtual {v0, v13, v2}, LX/186;->b(II)V

    .line 982861
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 982862
    const/4 v1, 0x3

    iget-wide v2, p0, LX/5iP;->d:J

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 982863
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 982864
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 982865
    const/4 v1, 0x6

    iget-boolean v2, p0, LX/5iP;->g:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 982866
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 982867
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 982868
    const/16 v1, 0x9

    iget-boolean v2, p0, LX/5iP;->j:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 982869
    const/16 v1, 0xa

    iget-wide v2, p0, LX/5iP;->k:J

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 982870
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 982871
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 982872
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 982873
    invoke-virtual {v1, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 982874
    new-instance v0, LX/15i;

    move-object v2, v11

    move-object v3, v11

    move v4, v13

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 982875
    new-instance v1, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-direct {v1, v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;-><init>(LX/15i;)V

    .line 982876
    return-object v1
.end method
