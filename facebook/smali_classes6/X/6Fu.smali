.class public final LX/6Fu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/bugreporter/BugReport;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6FY;

.field public final synthetic b:Landroid/os/Bundle;

.field public final synthetic c:LX/0P1;

.field public final synthetic d:LX/6G2;


# direct methods
.method public constructor <init>(LX/6G2;LX/6FY;Landroid/os/Bundle;LX/0P1;)V
    .locals 0

    .prologue
    .line 1069457
    iput-object p1, p0, LX/6Fu;->d:LX/6G2;

    iput-object p2, p0, LX/6Fu;->a:LX/6FY;

    iput-object p3, p0, LX/6Fu;->b:Landroid/os/Bundle;

    iput-object p4, p0, LX/6Fu;->c:LX/0P1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1069458
    const/4 v7, 0x0

    .line 1069459
    :try_start_0
    iget-object v0, p0, LX/6Fu;->d:LX/6G2;

    iget-object v0, v0, LX/6G2;->l:LX/6Ft;

    iget-object v1, p0, LX/6Fu;->a:LX/6FY;

    iget-object v1, v1, LX/6FY;->e:Ljava/util/List;

    iget-object v2, p0, LX/6Fu;->b:Landroid/os/Bundle;

    iget-object v3, p0, LX/6Fu;->a:LX/6FY;

    iget-object v3, v3, LX/6FY;->a:Landroid/content/Context;

    iget-object v4, p0, LX/6Fu;->c:LX/0P1;

    iget-object v5, p0, LX/6Fu;->a:LX/6FY;

    iget-object v5, v5, LX/6FY;->c:LX/0Rf;

    iget-object v6, p0, LX/6Fu;->a:LX/6FY;

    iget-object v6, v6, LX/6FY;->b:LX/6Fb;

    invoke-virtual/range {v0 .. v6}, LX/6Ft;->a(Ljava/util/List;Landroid/os/Bundle;Landroid/content/Context;LX/0P1;LX/0Rf;LX/6Fb;)LX/6FU;

    move-result-object v0

    invoke-virtual {v0}, LX/6FU;->y()Lcom/facebook/bugreporter/BugReport;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 1069460
    :catch_0
    move-exception v0

    .line 1069461
    const-string v1, "BugReporter"

    const-string v2, "Error creating the bug report"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1069462
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1069463
    :catch_1
    move-exception v0

    .line 1069464
    const-string v1, "BugReporter"

    const-string v2, "Error creating the bug report"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1069465
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
