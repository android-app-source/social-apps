.class public LX/6J2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6J0;

.field public b:LX/6Ik;

.field public c:LX/6Ik;

.field private final d:LX/6Ia;

.field public e:LX/6J1;


# direct methods
.method public constructor <init>(LX/6Ia;)V
    .locals 1

    .prologue
    .line 1075235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075236
    iput-object p1, p0, LX/6J2;->d:LX/6Ia;

    .line 1075237
    sget-object v0, LX/6J0;->CLOSED:LX/6J0;

    iput-object v0, p0, LX/6J2;->a:LX/6J0;

    .line 1075238
    sget-object v0, LX/6J1;->NONE:LX/6J1;

    iput-object v0, p0, LX/6J2;->e:LX/6J1;

    .line 1075239
    return-void
.end method

.method public static c(LX/6J2;LX/6Ik;)V
    .locals 2

    .prologue
    .line 1075232
    sget-object v0, LX/6J0;->OPEN_IN_PROGRESS:LX/6J0;

    iput-object v0, p0, LX/6J2;->a:LX/6J0;

    .line 1075233
    iget-object v0, p0, LX/6J2;->d:LX/6Ia;

    new-instance v1, LX/6Ix;

    invoke-direct {v1, p0, p1}, LX/6Ix;-><init>(LX/6J2;LX/6Ik;)V

    invoke-interface {v0, v1}, LX/6Ia;->a(LX/6Ik;)V

    .line 1075234
    return-void
.end method

.method public static d()V
    .locals 2

    .prologue
    .line 1075229
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1075230
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CameraLifecycleProxy methods must be called on the UI thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1075231
    :cond_0
    return-void
.end method

.method public static d(LX/6J2;LX/6Ik;)V
    .locals 2

    .prologue
    .line 1075226
    sget-object v0, LX/6J0;->CLOSE_IN_PROGRESS:LX/6J0;

    iput-object v0, p0, LX/6J2;->a:LX/6J0;

    .line 1075227
    iget-object v0, p0, LX/6J2;->d:LX/6Ia;

    new-instance v1, LX/6Iy;

    invoke-direct {v1, p0, p1}, LX/6Iy;-><init>(LX/6J2;LX/6Ik;)V

    invoke-interface {v0, v1}, LX/6Ia;->b(LX/6Ik;)V

    .line 1075228
    return-void
.end method

.method public static e(LX/6J2;)Z
    .locals 2

    .prologue
    .line 1075225
    iget-object v0, p0, LX/6J2;->a:LX/6J0;

    sget-object v1, LX/6J0;->OPEN_IN_PROGRESS:LX/6J0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/6J2;->a:LX/6J0;

    sget-object v1, LX/6J0;->PREVIEW_IN_PROGRESS:LX/6J0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/6J2;->a:LX/6J0;

    sget-object v1, LX/6J0;->CLOSE_IN_PROGRESS:LX/6J0;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/6J2;)V
    .locals 3

    .prologue
    .line 1075220
    sget-object v0, LX/6Iz;->a:[I

    iget-object v1, p0, LX/6J2;->e:LX/6J1;

    invoke-virtual {v1}, LX/6J1;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1075221
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown operation to interrupt: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6J2;->e:LX/6J1;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1075222
    :pswitch_0
    iget-object v0, p0, LX/6J2;->c:LX/6Ik;

    invoke-interface {v0}, LX/6Ik;->a()V

    .line 1075223
    :goto_0
    :pswitch_1
    return-void

    .line 1075224
    :pswitch_2
    iget-object v0, p0, LX/6J2;->b:LX/6Ik;

    invoke-interface {v0}, LX/6Ik;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/6J0;)V
    .locals 0

    .prologue
    .line 1075206
    invoke-static {}, LX/6J2;->d()V

    .line 1075207
    iput-object p1, p0, LX/6J2;->a:LX/6J0;

    .line 1075208
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1075209
    invoke-static {}, LX/6J2;->d()V

    .line 1075210
    iget-object v0, p0, LX/6J2;->e:LX/6J1;

    sget-object v1, LX/6J1;->NONE:LX/6J1;

    if-ne v0, v1, :cond_0

    .line 1075211
    const/4 v0, 0x0

    .line 1075212
    :goto_0
    return v0

    .line 1075213
    :cond_0
    iget-object v0, p0, LX/6J2;->e:LX/6J1;

    .line 1075214
    sget-object v1, LX/6J1;->NONE:LX/6J1;

    iput-object v1, p0, LX/6J2;->e:LX/6J1;

    .line 1075215
    sget-object v1, LX/6J1;->OPEN:LX/6J1;

    if-ne v0, v1, :cond_2

    .line 1075216
    iget-object v0, p0, LX/6J2;->c:LX/6Ik;

    invoke-static {p0, v0}, LX/6J2;->c(LX/6J2;LX/6Ik;)V

    .line 1075217
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1075218
    :cond_2
    sget-object v1, LX/6J1;->CLOSE:LX/6J1;

    if-ne v0, v1, :cond_1

    .line 1075219
    iget-object v0, p0, LX/6J2;->b:LX/6Ik;

    invoke-static {p0, v0}, LX/6J2;->d(LX/6J2;LX/6Ik;)V

    goto :goto_1
.end method
