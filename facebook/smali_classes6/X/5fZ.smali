.class public final LX/5fZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5fY;


# instance fields
.field public a:[F

.field public final synthetic b:LX/5fY;

.field public final synthetic c:Lcom/facebook/optic/CameraPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/optic/CameraPreviewView;LX/5fY;)V
    .locals 1

    .prologue
    .line 971727
    iput-object p1, p0, LX/5fZ;->c:Lcom/facebook/optic/CameraPreviewView;

    iput-object p2, p0, LX/5fZ;->b:LX/5fY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 971728
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/5fZ;->a:[F

    return-void
.end method


# virtual methods
.method public final a(LX/5fd;Landroid/graphics/Point;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 971718
    iget-object v0, p0, LX/5fZ;->b:LX/5fY;

    if-nez v0, :cond_0

    .line 971719
    :goto_0
    return-void

    .line 971720
    :cond_0
    if-eqz p2, :cond_1

    .line 971721
    iget-object v0, p0, LX/5fZ;->a:[F

    iget v1, p2, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    aput v1, v0, v3

    .line 971722
    iget-object v0, p0, LX/5fZ;->a:[F

    iget v1, p2, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    aput v1, v0, v4

    .line 971723
    iget-object v0, p0, LX/5fZ;->c:Lcom/facebook/optic/CameraPreviewView;

    iget-object v1, p0, LX/5fZ;->a:[F

    .line 971724
    invoke-static {v0, v1}, Lcom/facebook/optic/CameraPreviewView;->a$redex0(Lcom/facebook/optic/CameraPreviewView;[F)V

    .line 971725
    iget-object v0, p0, LX/5fZ;->b:LX/5fY;

    new-instance v1, Landroid/graphics/Point;

    iget-object v2, p0, LX/5fZ;->a:[F

    aget v2, v2, v3

    float-to-int v2, v2

    iget-object v3, p0, LX/5fZ;->a:[F

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v0, p1, v1}, LX/5fY;->a(LX/5fd;Landroid/graphics/Point;)V

    goto :goto_0

    .line 971726
    :cond_1
    iget-object v0, p0, LX/5fZ;->b:LX/5fY;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/5fY;->a(LX/5fd;Landroid/graphics/Point;)V

    goto :goto_0
.end method
