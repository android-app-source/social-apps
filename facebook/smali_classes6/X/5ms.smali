.class public LX/5ms;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1002877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1002878
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1002881
    check-cast p1, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;

    .line 1002882
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1002883
    iget-object v1, p1, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1002884
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "album_id"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1002885
    :cond_0
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "album_type"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->c:LX/5mu;

    invoke-virtual {v3}, LX/5mu;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1002886
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "caller"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->b:LX/5mv;

    invoke-virtual {v3}, LX/5mv;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1002887
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "caps_privacy"

    iget-boolean v3, p1, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->f:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1002888
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "client_time"

    iget-wide v4, p1, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1002889
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "privacy"

    iget-object v3, p1, Lcom/facebook/privacy/protocol/BulkEditAlbumPhotoPrivacyParams;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1002890
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "me/album_privacy_bulk_edit"

    .line 1002891
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1002892
    move-object v1, v1

    .line 1002893
    const-string v2, "POST"

    .line 1002894
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1002895
    move-object v1, v1

    .line 1002896
    const-string v2, "bulkEditAlbumPhotoPrivacy"

    .line 1002897
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1002898
    move-object v1, v1

    .line 1002899
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1002900
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1002901
    move-object v1, v1

    .line 1002902
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1002903
    move-object v0, v1

    .line 1002904
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1002879
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1002880
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
