.class public final LX/5iF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5iE;


# instance fields
.field public a:Landroid/net/Uri;

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 982365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982366
    const/4 v0, 0x0

    iput-object v0, p0, LX/5iF;->a:Landroid/net/Uri;

    .line 982367
    iput v1, p0, LX/5iF;->b:F

    .line 982368
    iput v1, p0, LX/5iF;->c:F

    .line 982369
    iput v1, p0, LX/5iF;->d:F

    .line 982370
    iput v1, p0, LX/5iF;->e:F

    .line 982371
    iput v1, p0, LX/5iF;->f:F

    .line 982372
    iput-object p1, p0, LX/5iF;->a:Landroid/net/Uri;

    .line 982373
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/creativeediting/model/DoodleParams;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 982351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 982352
    const/4 v0, 0x0

    iput-object v0, p0, LX/5iF;->a:Landroid/net/Uri;

    .line 982353
    iput v1, p0, LX/5iF;->b:F

    .line 982354
    iput v1, p0, LX/5iF;->c:F

    .line 982355
    iput v1, p0, LX/5iF;->d:F

    .line 982356
    iput v1, p0, LX/5iF;->e:F

    .line 982357
    iput v1, p0, LX/5iF;->f:F

    .line 982358
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->d()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/5iF;->a:Landroid/net/Uri;

    .line 982359
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->l()F

    move-result v0

    iput v0, p0, LX/5iF;->b:F

    .line 982360
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->m()F

    move-result v0

    iput v0, p0, LX/5iF;->c:F

    .line 982361
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->e()F

    move-result v0

    iput v0, p0, LX/5iF;->d:F

    .line 982362
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->f()F

    move-result v0

    iput v0, p0, LX/5iF;->e:F

    .line 982363
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/DoodleParams;->c()F

    move-result v0

    iput v0, p0, LX/5iF;->f:F

    .line 982364
    return-void
.end method


# virtual methods
.method public final a(Z)LX/5iE;
    .locals 0

    .prologue
    .line 982350
    return-object p0
.end method

.method public final a()Lcom/facebook/photos/creativeediting/model/DoodleParams;
    .locals 2

    .prologue
    .line 982347
    iget-object v0, p0, LX/5iF;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 982348
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5iF;->g:Ljava/lang/String;

    .line 982349
    :cond_0
    new-instance v0, Lcom/facebook/photos/creativeediting/model/DoodleParams;

    invoke-direct {v0, p0}, Lcom/facebook/photos/creativeediting/model/DoodleParams;-><init>(LX/5iF;)V

    return-object v0
.end method

.method public final synthetic b()LX/5i8;
    .locals 1

    .prologue
    .line 982374
    invoke-virtual {p0}, LX/5iF;->a()Lcom/facebook/photos/creativeediting/model/DoodleParams;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f(F)LX/5iE;
    .locals 1

    .prologue
    .line 982344
    iput p1, p0, LX/5iF;->e:F

    .line 982345
    move-object v0, p0

    .line 982346
    return-object v0
.end method

.method public final synthetic g(F)LX/5iE;
    .locals 1

    .prologue
    .line 982341
    iput p1, p0, LX/5iF;->d:F

    .line 982342
    move-object v0, p0

    .line 982343
    return-object v0
.end method

.method public final synthetic h(F)LX/5iE;
    .locals 1

    .prologue
    .line 982338
    iput p1, p0, LX/5iF;->c:F

    .line 982339
    move-object v0, p0

    .line 982340
    return-object v0
.end method

.method public final synthetic i(F)LX/5iE;
    .locals 1

    .prologue
    .line 982335
    iput p1, p0, LX/5iF;->b:F

    .line 982336
    move-object v0, p0

    .line 982337
    return-object v0
.end method

.method public final synthetic j(F)LX/5iE;
    .locals 1

    .prologue
    .line 982332
    iput p1, p0, LX/5iF;->f:F

    .line 982333
    move-object v0, p0

    .line 982334
    return-object v0
.end method
