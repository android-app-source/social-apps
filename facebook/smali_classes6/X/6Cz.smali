.class public LX/6Cz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6CA;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6D4;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6Co;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/browserextensions/common/BrowserExtensionsExternalUrlHandler;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/browserextensions/common/BrowserExtensionsAutofillHandler;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6CF;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/6CG;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/browserextensions/common/BrowserExtensionsUserActionListener;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Landroid/os/Bundle;

.field public final j:LX/6D3;


# direct methods
.method public constructor <init>(LX/6D3;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Landroid/os/Bundle;)V
    .locals 0
    .param p10    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6D3;",
            "Ljava/util/Set",
            "<",
            "LX/6CA;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/6CF;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/6CG;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/6D4;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/6Co;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/browserextensions/common/BrowserExtensionsExternalUrlHandler;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/browserextensions/common/BrowserExtensionsAutofillHandler;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/browserextensions/common/BrowserExtensionsUserActionListener;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1064493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064494
    iput-object p2, p0, LX/6Cz;->a:Ljava/util/Set;

    .line 1064495
    iput-object p3, p0, LX/6Cz;->f:Ljava/util/Set;

    .line 1064496
    iput-object p4, p0, LX/6Cz;->g:Ljava/util/Set;

    .line 1064497
    iput-object p5, p0, LX/6Cz;->b:Ljava/util/Set;

    .line 1064498
    iput-object p10, p0, LX/6Cz;->i:Landroid/os/Bundle;

    .line 1064499
    iput-object p1, p0, LX/6Cz;->j:LX/6D3;

    .line 1064500
    iput-object p6, p0, LX/6Cz;->c:Ljava/util/Set;

    .line 1064501
    iput-object p7, p0, LX/6Cz;->d:Ljava/util/Set;

    .line 1064502
    iput-object p8, p0, LX/6Cz;->e:Ljava/util/Set;

    .line 1064503
    iput-object p9, p0, LX/6Cz;->h:Ljava/util/Set;

    .line 1064504
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1064505
    iget-object v0, p0, LX/6Cz;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6CG;

    .line 1064506
    invoke-interface {v0, p1}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1064507
    invoke-interface {v0, p2, p3, p4, p5}, LX/6CG;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1064508
    :cond_1
    return-void
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1064509
    iget-object v0, p0, LX/6Cz;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6CF;

    .line 1064510
    iget-object v2, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v2, v2

    .line 1064511
    invoke-interface {v0, v2}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1064512
    invoke-interface {v0, p1, p2}, LX/6CF;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    goto :goto_0

    .line 1064513
    :cond_1
    return-void
.end method

.method public final a(Ljava/util/ArrayList;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1064514
    iget-object v0, p0, LX/6Cz;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6CF;

    .line 1064515
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v2, v2

    .line 1064516
    invoke-interface {v0, v2}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1064517
    invoke-interface {v0, p1, p2, p3}, LX/6CF;->a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    goto :goto_0

    .line 1064518
    :cond_1
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1064519
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1064520
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 1064521
    invoke-virtual {v0}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1064522
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1064523
    iget-object v0, p0, LX/6Cz;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6CF;

    .line 1064524
    iget-object v3, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v3, v3

    .line 1064525
    invoke-interface {v0, v3}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1064526
    invoke-interface {v0, v2, p2, p3}, LX/6CF;->c(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    goto :goto_1

    .line 1064527
    :cond_2
    return-void
.end method

.method public final b(Ljava/util/ArrayList;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1064528
    iget-object v0, p0, LX/6Cz;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6CF;

    .line 1064529
    iget-object v2, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v2, v2

    .line 1064530
    invoke-interface {v0, v2}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1064531
    invoke-interface {v0, p1, p2, p3}, LX/6CF;->b(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    goto :goto_0

    .line 1064532
    :cond_1
    return-void
.end method
