.class public LX/6Ck;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/03V;

.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/0tX;

.field public d:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteQueryFragmentsModels$MessengerExtensionFavoritesQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/browserextensions/commerce/favorite/graphql/FavoriteMutationFragmentsModels$MessengerExtensionFavoriteMutationModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;Ljava/util/concurrent/ExecutorService;LX/0tX;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1064291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064292
    iput-object p1, p0, LX/6Ck;->a:LX/03V;

    .line 1064293
    iput-object p2, p0, LX/6Ck;->b:Ljava/util/concurrent/ExecutorService;

    .line 1064294
    iput-object p3, p0, LX/6Ck;->c:LX/0tX;

    .line 1064295
    return-void
.end method

.method public static a(LX/6Ck;LX/4HI;Ljava/util/concurrent/Executor;LX/6Cj;)V
    .locals 2

    .prologue
    .line 1064296
    iget-object v0, p0, LX/6Ck;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Ck;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1064297
    :cond_0
    :goto_0
    new-instance v0, LX/6CX;

    invoke-direct {v0}, LX/6CX;-><init>()V

    move-object v0, v0

    .line 1064298
    const-string v1, "input"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1064299
    iget-object v1, p0, LX/6Ck;->c:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/6Ck;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1064300
    iget-object v0, p0, LX/6Ck;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/6Ci;

    invoke-direct {v1, p0, p3}, LX/6Ci;-><init>(LX/6Ck;LX/6Cj;)V

    invoke-static {v0, v1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1064301
    return-void

    .line 1064302
    :cond_1
    iget-object v0, p0, LX/6Ck;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_0
.end method
