.class public final enum LX/6Wv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Wv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Wv;

.field public static final enum DONE_LOADING:LX/6Wv;

.field public static final enum HIDDEN:LX/6Wv;

.field public static final enum LOADING:LX/6Wv;

.field public static final enum READY_TO_PLAY:LX/6Wv;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1106610
    new-instance v0, LX/6Wv;

    const-string v1, "READY_TO_PLAY"

    invoke-direct {v0, v1, v2}, LX/6Wv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    .line 1106611
    new-instance v0, LX/6Wv;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/6Wv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Wv;->LOADING:LX/6Wv;

    .line 1106612
    new-instance v0, LX/6Wv;

    const-string v1, "DONE_LOADING"

    invoke-direct {v0, v1, v4}, LX/6Wv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Wv;->DONE_LOADING:LX/6Wv;

    .line 1106613
    new-instance v0, LX/6Wv;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v5}, LX/6Wv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Wv;->HIDDEN:LX/6Wv;

    .line 1106614
    const/4 v0, 0x4

    new-array v0, v0, [LX/6Wv;

    sget-object v1, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    aput-object v1, v0, v2

    sget-object v1, LX/6Wv;->LOADING:LX/6Wv;

    aput-object v1, v0, v3

    sget-object v1, LX/6Wv;->DONE_LOADING:LX/6Wv;

    aput-object v1, v0, v4

    sget-object v1, LX/6Wv;->HIDDEN:LX/6Wv;

    aput-object v1, v0, v5

    sput-object v0, LX/6Wv;->$VALUES:[LX/6Wv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1106609
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Wv;
    .locals 1

    .prologue
    .line 1106615
    const-class v0, LX/6Wv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Wv;

    return-object v0
.end method

.method public static values()[LX/6Wv;
    .locals 1

    .prologue
    .line 1106608
    sget-object v0, LX/6Wv;->$VALUES:[LX/6Wv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Wv;

    return-object v0
.end method
