.class public final LX/6Vm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1RC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1RC",
        "<TP;",
        "LX/1Ra",
        "<",
        "Landroid/view/ViewGroup;",
        ">;TE;TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Cz;

.field public final synthetic b:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/1Qx;


# direct methods
.method public constructor <init>(LX/1Cz;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Landroid/content/Context;LX/1Qx;)V
    .locals 0

    .prologue
    .line 1104428
    iput-object p1, p0, LX/6Vm;->a:LX/1Cz;

    iput-object p2, p0, LX/6Vm;->b:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iput-object p3, p0, LX/6Vm;->c:Landroid/content/Context;

    iput-object p4, p0, LX/6Vm;->d:LX/1Qx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1104429
    iget-object v0, p0, LX/6Vm;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1104430
    iget-object v0, p0, LX/6Vm;->b:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    iget-object v1, p0, LX/6Vm;->c:Landroid/content/Context;

    iget-object v2, p0, LX/6Vm;->d:LX/1Qx;

    .line 1104431
    new-instance p0, LX/6Vn;

    invoke-direct {p0, v0, p2, v2, v1}, LX/6Vn;-><init>(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;LX/1Qx;Landroid/content/Context;)V

    move-object v0, p0

    .line 1104432
    invoke-virtual {v0, p3}, LX/1Ra;->a(LX/1PW;)V

    .line 1104433
    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;)V
    .locals 0

    .prologue
    .line 1104434
    check-cast p2, LX/1Ra;

    .line 1104435
    invoke-virtual {p2, p3}, LX/1Ra;->b(LX/1PW;)V

    .line 1104436
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xfcaf2fe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1104437
    check-cast p2, LX/1Ra;

    check-cast p4, Landroid/view/ViewGroup;

    .line 1104438
    invoke-virtual {p2, p4}, LX/1Ra;->a(Landroid/view/View;)V

    .line 1104439
    const/16 v1, 0x1f

    const v2, -0xcb0ef57

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1104440
    check-cast p2, LX/1Ra;

    check-cast p4, Landroid/view/ViewGroup;

    .line 1104441
    invoke-virtual {p2, p4}, LX/1Ra;->b(Landroid/view/View;)V

    .line 1104442
    return-void
.end method
