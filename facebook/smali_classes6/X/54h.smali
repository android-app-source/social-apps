.class public final LX/54h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# static fields
.field public static final b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "LX/54h;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:I

.field private final c:LX/0vR;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 828224
    const-class v0, LX/54h;

    const-string v1, "a"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, LX/54h;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 828225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 828226
    const/4 v0, 0x0

    iput-object v0, p0, LX/54h;->c:LX/0vR;

    .line 828227
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 828228
    sget-object v0, LX/54h;->b:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828229
    iget-object v0, p0, LX/54h;->c:LX/0vR;

    if-eqz v0, :cond_0

    .line 828230
    iget-object v0, p0, LX/54h;->c:LX/0vR;

    invoke-interface {v0}, LX/0vR;->a()V

    .line 828231
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 828232
    iget v0, p0, LX/54h;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
