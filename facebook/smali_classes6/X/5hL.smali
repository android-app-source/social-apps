.class public final LX/5hL;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 978621
    const-class v1, Lcom/facebook/photos/albums/protocols/VideoListVideosGraphQLModels$VideosInVideoListDetailQueryModel;

    const v0, 0x41ffd7ea

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "VideosInVideoListDetailQuery"

    const-string v6, "c2a73acab873d2b4858f320cdf97b664"

    const-string v7, "node"

    const-string v8, "10155072967761729"

    const-string v9, "10155259090631729"

    .line 978622
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 978623
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 978624
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 978625
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 978626
    sparse-switch v0, :sswitch_data_0

    .line 978627
    :goto_0
    return-object p1

    .line 978628
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 978629
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 978630
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 978631
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 978632
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 978633
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 978634
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 978635
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 978636
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 978637
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 978638
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7c5a5b68 -> :sswitch_a
        -0x69b6761e -> :sswitch_5
        -0x5fc0bca0 -> :sswitch_7
        -0x25a646c8 -> :sswitch_2
        -0x2177e47b -> :sswitch_3
        0x1912dbbb -> :sswitch_1
        0x1918b88b -> :sswitch_4
        0x2216a858 -> :sswitch_0
        0x2292beef -> :sswitch_9
        0x26d0c0ff -> :sswitch_8
        0x73a026b5 -> :sswitch_6
    .end sparse-switch
.end method
