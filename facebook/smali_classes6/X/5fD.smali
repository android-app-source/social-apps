.class public final LX/5fD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "Landroid/hardware/Camera$Size;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5f5;

.field public final synthetic b:LX/5fQ;


# direct methods
.method public constructor <init>(LX/5fQ;LX/5f5;)V
    .locals 0

    .prologue
    .line 971066
    iput-object p1, p0, LX/5fD;->b:LX/5fQ;

    iput-object p2, p0, LX/5fD;->a:LX/5f5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 971067
    iget-object v0, p0, LX/5fD;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 971068
    iget-object v0, p0, LX/5fD;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fD;->b:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    .line 971069
    iget-object v1, p0, LX/5fD;->b:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/5fS;->a(Ljava/lang/String;)V

    .line 971070
    iget-object v1, p0, LX/5fD;->b:LX/5fQ;

    iget-boolean v1, v1, LX/5fQ;->D:Z

    invoke-virtual {v0, v1}, LX/5fS;->b(Z)V

    .line 971071
    :cond_0
    iget-object v0, p0, LX/5fD;->a:LX/5f5;

    if-eqz v0, :cond_1

    .line 971072
    iget-object v0, p0, LX/5fD;->a:LX/5f5;

    invoke-interface {v0, p1}, LX/5f5;->a(Ljava/lang/Exception;)V

    .line 971073
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 971056
    check-cast p1, Landroid/hardware/Camera$Size;

    .line 971057
    iget-object v0, p0, LX/5fD;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    if-nez v0, :cond_1

    .line 971058
    iget-object v0, p0, LX/5fD;->a:LX/5f5;

    if-eqz v0, :cond_0

    .line 971059
    iget-object v0, p0, LX/5fD;->a:LX/5f5;

    new-instance v1, LX/5fN;

    iget-object v2, p0, LX/5fD;->b:LX/5fQ;

    const-string v3, "Cannot start preview"

    invoke-direct {v1, v2, v3}, LX/5fN;-><init>(LX/5fQ;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/5f5;->a(Ljava/lang/Exception;)V

    .line 971060
    :cond_0
    :goto_0
    return-void

    .line 971061
    :cond_1
    iget-object v0, p0, LX/5fD;->b:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/5fD;->b:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->i:LX/5fM;

    invoke-static {v1}, LX/5fQ;->b(LX/5fM;)I

    move-result v1

    invoke-static {v0, v1}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v0

    .line 971062
    iget-object v1, p0, LX/5fD;->b:LX/5fQ;

    iget-object v1, v1, LX/5fQ;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/5fS;->a(Ljava/lang/String;)V

    .line 971063
    iget-object v1, p0, LX/5fD;->b:LX/5fQ;

    iget-boolean v1, v1, LX/5fQ;->D:Z

    invoke-virtual {v0, v1}, LX/5fS;->b(Z)V

    .line 971064
    iget-object v0, p0, LX/5fD;->a:LX/5f5;

    if-eqz v0, :cond_0

    .line 971065
    iget-object v0, p0, LX/5fD;->a:LX/5f5;

    invoke-interface {v0, p1}, LX/5f5;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
