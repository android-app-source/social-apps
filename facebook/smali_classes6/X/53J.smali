.class public final LX/53J;
.super LX/0zZ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0zZ",
        "<",
        "LX/0v9",
        "<+TT;>;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0vH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0vH",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:LX/0zZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zZ",
            "<-TT;>;"
        }
    .end annotation
.end field

.field public c:I

.field public final d:LX/4V7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4V7",
            "<",
            "LX/53G",
            "<TT;>;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/53H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53H",
            "<TT;>;"
        }
    .end annotation
.end field

.field public f:I

.field private g:Z

.field private final h:Z

.field private i:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field public volatile j:LX/53s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53s",
            "<",
            "LX/53G",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public k:LX/53n;

.field private l:I

.field private m:Z


# direct methods
.method public constructor <init>(LX/0zZ;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zZ",
            "<-TT;>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 827059
    invoke-direct {p0, p1}, LX/0zZ;-><init>(LX/0zZ;)V

    .line 827060
    sget-object v0, LX/0vH;->a:LX/0vH;

    move-object v0, v0

    .line 827061
    iput-object v0, p0, LX/53J;->a:LX/0vH;

    .line 827062
    const/4 v0, 0x0

    iput-object v0, p0, LX/53J;->k:LX/53n;

    .line 827063
    iput v1, p0, LX/53J;->l:I

    .line 827064
    iput-boolean v1, p0, LX/53J;->m:Z

    .line 827065
    iput v1, p0, LX/53J;->c:I

    .line 827066
    new-instance v0, LX/53I;

    invoke-direct {v0, p0}, LX/53I;-><init>(LX/53J;)V

    iput-object v0, p0, LX/53J;->d:LX/4V7;

    .line 827067
    iput-object p1, p0, LX/53J;->b:LX/0zZ;

    .line 827068
    new-instance v0, LX/53H;

    invoke-direct {v0, p0}, LX/53H;-><init>(LX/53J;)V

    iput-object v0, p0, LX/53J;->e:LX/53H;

    .line 827069
    iput-boolean p2, p0, LX/53J;->h:Z

    .line 827070
    invoke-virtual {p1, p0}, LX/0zZ;->a(LX/0za;)V

    .line 827071
    iget-object v0, p0, LX/53J;->e:LX/53H;

    invoke-virtual {p1, v0}, LX/0zZ;->a(LX/52p;)V

    .line 827072
    return-void
.end method

.method public static a$redex0(LX/53J;Ljava/lang/Throwable;Z)V
    .locals 2

    .prologue
    .line 827040
    iget-boolean v0, p0, LX/53J;->h:Z

    if-eqz v0, :cond_6

    .line 827041
    monitor-enter p0

    .line 827042
    :try_start_0
    iget-object v0, p0, LX/53J;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-nez v0, :cond_0

    .line 827043
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LX/53J;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 827044
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827045
    iget-object v0, p0, LX/53J;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 827046
    const/4 v0, 0x0

    .line 827047
    monitor-enter p0

    .line 827048
    if-nez p2, :cond_1

    .line 827049
    :try_start_1
    iget v1, p0, LX/53J;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/53J;->f:I

    .line 827050
    :cond_1
    iget v1, p0, LX/53J;->f:I

    if-nez v1, :cond_2

    iget-boolean v1, p0, LX/53J;->g:Z

    if-nez v1, :cond_3

    :cond_2
    iget v1, p0, LX/53J;->f:I

    if-gez v1, :cond_4

    .line 827051
    :cond_3
    const/4 v0, 0x1

    .line 827052
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 827053
    if-eqz v0, :cond_5

    .line 827054
    invoke-static {p0}, LX/53J;->k(LX/53J;)V

    .line 827055
    :cond_5
    :goto_0
    return-void

    .line 827056
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 827057
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 827058
    :cond_6
    iget-object v0, p0, LX/53J;->b:LX/0zZ;

    invoke-virtual {v0, p1}, LX/0zZ;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(LX/53J;LX/53q;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/53q",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x1

    .line 827026
    iget-object v0, p1, LX/53q;->b:Ljava/lang/Object;

    move-object v0, v0

    .line 827027
    invoke-static {p0}, LX/53J;->g$redex0(LX/53J;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 827028
    :try_start_0
    iget-object v1, p0, LX/53J;->b:LX/0zZ;

    invoke-virtual {v1, v0}, LX/0zZ;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827029
    invoke-static {p0}, LX/53J;->f$redex0(LX/53J;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827030
    invoke-static {p0}, LX/53J;->h(LX/53J;)Z

    .line 827031
    :cond_0
    invoke-virtual {p0, v2, v3}, LX/0zZ;->a(J)V

    .line 827032
    :goto_0
    return-void

    .line 827033
    :catchall_0
    move-exception v0

    invoke-static {p0}, LX/53J;->f$redex0(LX/53J;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 827034
    invoke-static {p0}, LX/53J;->h(LX/53J;)Z

    .line 827035
    :cond_1
    invoke-virtual {p0, v2, v3}, LX/0zZ;->a(J)V

    throw v0

    .line 827036
    :cond_2
    invoke-static {p0}, LX/53J;->e$redex0(LX/53J;)V

    .line 827037
    :try_start_1
    iget-object v1, p0, LX/53J;->k:LX/53n;

    invoke-virtual {v1, v0}, LX/53n;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch LX/52z; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 827038
    :catch_0
    move-exception v0

    .line 827039
    invoke-virtual {p0, v0}, LX/53J;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static c(LX/53J;LX/53q;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/53q",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 827001
    invoke-static {p0}, LX/53J;->g$redex0(LX/53J;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 827002
    const/4 v1, 0x0

    .line 827003
    :try_start_0
    iget-object v0, p0, LX/53J;->e:LX/53H;

    iget-wide v0, v0, LX/53H;->c:J

    .line 827004
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 827005
    const/4 v1, 0x1

    .line 827006
    iget-object v0, p0, LX/53J;->b:LX/0zZ;

    .line 827007
    iget-object v2, p1, LX/53q;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 827008
    invoke-virtual {v0, v2}, LX/0zZ;->a(Ljava/lang/Object;)V

    .line 827009
    sget-object v0, LX/53H;->a:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    iget-object v2, p0, LX/53J;->e:LX/53H;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->decrementAndGet(Ljava/lang/Object;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827010
    invoke-static {p0}, LX/53J;->f$redex0(LX/53J;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 827011
    invoke-static {p0}, LX/53J;->h(LX/53J;)Z

    .line 827012
    :cond_0
    invoke-virtual {p0, v4, v5}, LX/0zZ;->a(J)V

    .line 827013
    :goto_0
    return-void

    .line 827014
    :cond_1
    invoke-static {p0}, LX/53J;->f$redex0(LX/53J;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 827015
    invoke-static {p0}, LX/53J;->h(LX/53J;)Z

    .line 827016
    :cond_2
    invoke-static {p0}, LX/53J;->e$redex0(LX/53J;)V

    .line 827017
    :try_start_1
    iget-object v0, p0, LX/53J;->k:LX/53n;

    .line 827018
    iget-object v1, p1, LX/53q;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 827019
    invoke-virtual {v0, v1}, LX/53n;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch LX/52z; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 827020
    :catch_0
    move-exception v0

    .line 827021
    invoke-virtual {p0, v0}, LX/53J;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 827022
    :catchall_0
    move-exception v0

    invoke-static {p0}, LX/53J;->f$redex0(LX/53J;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 827023
    invoke-static {p0}, LX/53J;->h(LX/53J;)Z

    .line 827024
    :cond_3
    if-eqz v1, :cond_4

    .line 827025
    invoke-virtual {p0, v4, v5}, LX/0zZ;->a(J)V

    :cond_4
    throw v0
.end method

.method private static e$redex0(LX/53J;)V
    .locals 1

    .prologue
    .line 826997
    iget-object v0, p0, LX/53J;->k:LX/53n;

    if-nez v0, :cond_0

    .line 826998
    invoke-static {}, LX/53n;->d()LX/53n;

    move-result-object v0

    iput-object v0, p0, LX/53J;->k:LX/53n;

    .line 826999
    iget-object v0, p0, LX/53J;->k:LX/53n;

    invoke-virtual {p0, v0}, LX/0zZ;->a(LX/0za;)V

    .line 827000
    :cond_0
    return-void
.end method

.method private static declared-synchronized f$redex0(LX/53J;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 826879
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, LX/53J;->m:Z

    .line 826880
    iget v1, p0, LX/53J;->l:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 826881
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 826882
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static synthetic g(LX/53J;)Z
    .locals 1

    .prologue
    .line 826996
    invoke-static {p0}, LX/53J;->f$redex0(LX/53J;)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized g$redex0(LX/53J;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 826989
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, LX/53J;->m:Z

    if-eqz v2, :cond_0

    .line 826990
    iget v1, p0, LX/53J;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/53J;->l:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 826991
    :goto_0
    monitor-exit p0

    return v0

    .line 826992
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/53J;->m:Z

    .line 826993
    const/4 v0, 0x0

    iput v0, p0, LX/53J;->l:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 826994
    goto :goto_0

    .line 826995
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static h(LX/53J;)Z
    .locals 13

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 826958
    :cond_0
    invoke-static {p0}, LX/53J;->g$redex0(LX/53J;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 826959
    :try_start_0
    const-wide/16 v9, 0x0

    const/4 v7, 0x0

    .line 826960
    iget-object v8, p0, LX/53J;->k:LX/53n;

    if-eqz v8, :cond_3

    .line 826961
    iget-object v8, p0, LX/53J;->e:LX/53H;

    iget-wide v11, v8, LX/53H;->c:J

    .line 826962
    cmp-long v8, v11, v9

    if-gez v8, :cond_1

    .line 826963
    :goto_0
    iget-object v8, p0, LX/53J;->k:LX/53n;

    invoke-virtual {v8}, LX/53n;->i()Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 826964
    iget-object v9, p0, LX/53J;->b:LX/0zZ;

    invoke-static {v9, v8}, LX/0vH;->a(LX/0vA;Ljava/lang/Object;)Z

    .line 826965
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 826966
    :cond_1
    cmp-long v8, v11, v9

    if-lez v8, :cond_3

    move v8, v7

    .line 826967
    :goto_1
    int-to-long v9, v8

    cmp-long v9, v9, v11

    if-gez v9, :cond_2

    .line 826968
    iget-object v9, p0, LX/53J;->k:LX/53n;

    invoke-virtual {v9}, LX/53n;->i()Ljava/lang/Object;

    move-result-object v9

    .line 826969
    if-eqz v9, :cond_2

    .line 826970
    iget-object v10, p0, LX/53J;->b:LX/0zZ;

    invoke-static {v10, v9}, LX/0vH;->a(LX/0vA;Ljava/lang/Object;)Z

    .line 826971
    add-int/lit8 v9, v7, 0x1

    .line 826972
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move v7, v9

    goto :goto_1

    .line 826973
    :cond_2
    sget-object v8, LX/53H;->a:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    iget-object v9, p0, LX/53J;->e:LX/53H;

    neg-int v10, v7

    int-to-long v11, v10

    invoke-virtual {v8, v9, v11, v12}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndAdd(Ljava/lang/Object;J)J

    .line 826974
    :cond_3
    move v2, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 826975
    :try_start_1
    iget-object v3, p0, LX/53J;->j:LX/53s;

    if-eqz v3, :cond_4

    .line 826976
    iget-object v3, p0, LX/53J;->j:LX/53s;

    iget-object v4, p0, LX/53J;->d:LX/4V7;

    iget v5, p0, LX/53J;->c:I

    invoke-virtual {v3, v4, v5}, LX/53s;->a(LX/4V7;I)I

    move-result v3

    iput v3, p0, LX/53J;->c:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 826977
    :cond_4
    invoke-static {p0}, LX/53J;->f$redex0(LX/53J;)Z

    move-result v3

    .line 826978
    if-lez v2, :cond_5

    .line 826979
    int-to-long v4, v2

    invoke-virtual {p0, v4, v5}, LX/0zZ;->a(J)V

    .line 826980
    :cond_5
    if-nez v3, :cond_0

    .line 826981
    :cond_6
    :goto_2
    return v0

    .line 826982
    :catchall_0
    move-exception v2

    move-object v6, v2

    move v2, v1

    move-object v1, v6

    :goto_3
    invoke-static {p0}, LX/53J;->f$redex0(LX/53J;)Z

    move-result v3

    .line 826983
    if-lez v2, :cond_7

    .line 826984
    int-to-long v4, v2

    invoke-virtual {p0, v4, v5}, LX/0zZ;->a(J)V

    .line 826985
    :cond_7
    if-eqz v3, :cond_6

    .line 826986
    throw v1

    :cond_8
    move v0, v1

    .line 826987
    goto :goto_2

    .line 826988
    :catchall_1
    move-exception v1

    goto :goto_3
.end method

.method public static k(LX/53J;)V
    .locals 4

    .prologue
    .line 826944
    invoke-static {p0}, LX/53J;->h(LX/53J;)Z

    .line 826945
    iget-boolean v0, p0, LX/53J;->h:Z

    if-eqz v0, :cond_2

    .line 826946
    monitor-enter p0

    .line 826947
    :try_start_0
    iget-object v0, p0, LX/53J;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 826948
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 826949
    if-eqz v0, :cond_1

    .line 826950
    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 826951
    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 826952
    iget-object v1, p0, LX/53J;->b:LX/0zZ;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {v1, v0}, LX/0zZ;->a(Ljava/lang/Throwable;)V

    .line 826953
    :goto_0
    return-void

    .line 826954
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 826955
    :cond_0
    iget-object v1, p0, LX/53J;->b:LX/0zZ;

    new-instance v2, LX/52x;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, LX/52x;-><init>(Ljava/util/Collection;B)V

    invoke-virtual {v1, v2}, LX/0zZ;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 826956
    :cond_1
    iget-object v0, p0, LX/53J;->b:LX/0zZ;

    invoke-virtual {v0}, LX/0zZ;->R_()V

    goto :goto_0

    .line 826957
    :cond_2
    iget-object v0, p0, LX/53J;->b:LX/0zZ;

    invoke-virtual {v0}, LX/0zZ;->R_()V

    goto :goto_0
.end method


# virtual methods
.method public final R_()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 826935
    const/4 v0, 0x0

    .line 826936
    monitor-enter p0

    .line 826937
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, LX/53J;->g:Z

    .line 826938
    iget v2, p0, LX/53J;->f:I

    if-nez v2, :cond_1

    iget-object v2, p0, LX/53J;->k:LX/53n;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/53J;->k:LX/53n;

    invoke-virtual {v2}, LX/53n;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 826939
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 826940
    if-eqz v0, :cond_2

    .line 826941
    invoke-static {p0}, LX/53J;->k(LX/53J;)V

    .line 826942
    :cond_2
    return-void

    .line 826943
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/53G;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/53G",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 826912
    const/4 v0, 0x0

    .line 826913
    monitor-enter p0

    .line 826914
    :try_start_0
    iget v1, p0, LX/53J;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/53J;->f:I

    .line 826915
    iget v1, p0, LX/53J;->f:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, LX/53J;->g:Z

    if-eqz v1, :cond_0

    .line 826916
    const/4 v0, 0x1

    .line 826917
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 826918
    iget-object v1, p0, LX/53J;->j:LX/53s;

    iget v2, p1, LX/53G;->a:I

    .line 826919
    iget v3, v1, LX/53s;->b:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    iget-object v3, v1, LX/53s;->a:LX/53h;

    if-eqz v3, :cond_1

    if-gez v2, :cond_3

    .line 826920
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 826921
    invoke-static {p0}, LX/53J;->k(LX/53J;)V

    .line 826922
    :cond_2
    return-void

    .line 826923
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 826924
    :cond_3
    iget-object v3, v1, LX/53s;->a:LX/53h;

    const/4 v1, 0x0

    .line 826925
    sget v4, LX/53h;->d:I

    if-ge v2, v4, :cond_4

    .line 826926
    iget-object v4, v3, LX/53h;->f:LX/53f;

    iget-object v4, v4, LX/53f;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v4, v2, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->getAndSet(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 826927
    :goto_1
    invoke-static {v3, v2}, LX/53h;->d(LX/53h;I)V

    .line 826928
    move-object v3, v4

    .line 826929
    check-cast v3, LX/0za;

    .line 826930
    if-eqz v3, :cond_1

    .line 826931
    if-eqz v3, :cond_1

    .line 826932
    invoke-interface {v3}, LX/0za;->b()V

    goto :goto_0

    .line 826933
    :cond_4
    sget v4, LX/53h;->d:I

    rem-int v4, v2, v4

    .line 826934
    invoke-static {v3, v2}, LX/53h;->c(LX/53h;I)LX/53f;

    move-result-object p1

    iget-object p1, p1, LX/53f;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {p1, v4, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->getAndSet(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 826889
    check-cast p1, LX/0v9;

    .line 826890
    instance-of v0, p1, LX/53q;

    if-eqz v0, :cond_1

    .line 826891
    check-cast p1, LX/53q;

    .line 826892
    iget-object v1, p0, LX/53J;->e:LX/53H;

    iget-wide v1, v1, LX/53H;->c:J

    const-wide v3, 0x7fffffffffffffffL

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    .line 826893
    invoke-static {p0, p1}, LX/53J;->b(LX/53J;LX/53q;)V

    .line 826894
    :cond_0
    :goto_0
    return-void

    .line 826895
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, LX/0zZ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 826896
    monitor-enter p0

    .line 826897
    :try_start_0
    iget v0, p0, LX/53J;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/53J;->f:I

    .line 826898
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 826899
    iget-object v1, p0, LX/53J;->j:LX/53s;

    if-nez v1, :cond_2

    .line 826900
    new-instance v1, LX/53s;

    invoke-direct {v1}, LX/53s;-><init>()V

    iput-object v1, p0, LX/53J;->j:LX/53s;

    .line 826901
    iget-object v1, p0, LX/53J;->j:LX/53s;

    invoke-virtual {p0, v1}, LX/0zZ;->a(LX/0za;)V

    .line 826902
    :cond_2
    const/4 v1, 0x0

    .line 826903
    iget-object v2, p0, LX/53J;->e:LX/53H;

    iget-wide v3, v2, LX/53H;->c:J

    const-wide v5, 0x7fffffffffffffffL

    cmp-long v2, v3, v5

    if-eqz v2, :cond_3

    .line 826904
    iget-object v1, p0, LX/53J;->e:LX/53H;

    .line 826905
    :cond_3
    new-instance v2, LX/53G;

    invoke-direct {v2, p0, v1}, LX/53G;-><init>(LX/53J;LX/53H;)V

    .line 826906
    iget-object v1, p0, LX/53J;->j:LX/53s;

    invoke-virtual {v1, v2}, LX/53s;->a(LX/0za;)I

    move-result v1

    iput v1, v2, LX/53G;->a:I

    .line 826907
    invoke-virtual {p1, v2}, LX/0v9;->a(LX/0zZ;)LX/0za;

    .line 826908
    const-wide/16 v1, 0x1

    invoke-virtual {p0, v1, v2}, LX/0zZ;->a(J)V

    .line 826909
    goto :goto_0

    .line 826910
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 826911
    :cond_4
    invoke-static {p0, p1}, LX/53J;->c(LX/53J;LX/53q;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 826885
    iget-boolean v0, p0, LX/53J;->g:Z

    if-nez v0, :cond_0

    .line 826886
    iput-boolean v1, p0, LX/53J;->g:Z

    .line 826887
    invoke-static {p0, p1, v1}, LX/53J;->a$redex0(LX/53J;Ljava/lang/Throwable;Z)V

    .line 826888
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 826883
    sget v0, LX/53n;->c:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/0zZ;->a(J)V

    .line 826884
    return-void
.end method
