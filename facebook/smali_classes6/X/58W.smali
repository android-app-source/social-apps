.class public final LX/58W;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 847168
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 847169
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 847170
    :goto_0
    return v1

    .line 847171
    :cond_0
    const-string v8, "is_verified"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 847172
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v5, v3

    move v3, v2

    .line 847173
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 847174
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 847175
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 847176
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 847177
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 847178
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 847179
    :cond_2
    const-string v8, "is_verified_page"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 847180
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 847181
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 847182
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 847183
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 847184
    if-eqz v3, :cond_5

    .line 847185
    invoke-virtual {p1, v2, v5}, LX/186;->a(IZ)V

    .line 847186
    :cond_5
    if-eqz v0, :cond_6

    .line 847187
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 847188
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 847189
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 847190
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 847191
    if-eqz v0, :cond_0

    .line 847192
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847193
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 847194
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 847195
    if-eqz v0, :cond_1

    .line 847196
    const-string v1, "is_verified"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847197
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 847198
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 847199
    if-eqz v0, :cond_2

    .line 847200
    const-string v1, "is_verified_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 847201
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 847202
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 847203
    return-void
.end method
