.class public final enum LX/5zs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5zs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5zs;

.field public static final enum CONTENT:LX/5zs;

.field public static final enum FILE:LX/5zs;

.field public static final enum HTTP:LX/5zs;

.field public static final enum HTTPS:LX/5zs;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1035891
    new-instance v0, LX/5zs;

    const-string v1, "CONTENT"

    invoke-direct {v0, v1, v2}, LX/5zs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5zs;->CONTENT:LX/5zs;

    .line 1035892
    new-instance v0, LX/5zs;

    const-string v1, "FILE"

    invoke-direct {v0, v1, v3}, LX/5zs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5zs;->FILE:LX/5zs;

    .line 1035893
    new-instance v0, LX/5zs;

    const-string v1, "HTTP"

    invoke-direct {v0, v1, v4}, LX/5zs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5zs;->HTTP:LX/5zs;

    .line 1035894
    new-instance v0, LX/5zs;

    const-string v1, "HTTPS"

    invoke-direct {v0, v1, v5}, LX/5zs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5zs;->HTTPS:LX/5zs;

    .line 1035895
    const/4 v0, 0x4

    new-array v0, v0, [LX/5zs;

    sget-object v1, LX/5zs;->CONTENT:LX/5zs;

    aput-object v1, v0, v2

    sget-object v1, LX/5zs;->FILE:LX/5zs;

    aput-object v1, v0, v3

    sget-object v1, LX/5zs;->HTTP:LX/5zs;

    aput-object v1, v0, v4

    sget-object v1, LX/5zs;->HTTPS:LX/5zs;

    aput-object v1, v0, v5

    sput-object v0, LX/5zs;->$VALUES:[LX/5zs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1035896
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static from(Landroid/net/Uri;)LX/5zs;
    .locals 3

    .prologue
    .line 1035897
    invoke-static {p0}, LX/5zs;->fromOrNull(Landroid/net/Uri;)LX/5zs;

    move-result-object v0

    .line 1035898
    if-nez v0, :cond_0

    .line 1035899
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Scheme not recognized: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1035900
    :cond_0
    return-object v0
.end method

.method public static from(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zs;
    .locals 1

    .prologue
    .line 1035901
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/5zs;->from(Landroid/net/Uri;)LX/5zs;

    move-result-object v0

    return-object v0
.end method

.method public static fromOrNull(Landroid/net/Uri;)LX/5zs;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1035902
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1035903
    :goto_0
    return-object v0

    .line 1035904
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1035905
    :pswitch_0
    sget-object v0, LX/5zs;->CONTENT:LX/5zs;

    goto :goto_0

    .line 1035906
    :sswitch_0
    const-string v3, "content"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v3, "file"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v3, "http"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v3, "https"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    .line 1035907
    :pswitch_1
    sget-object v0, LX/5zs;->FILE:LX/5zs;

    goto :goto_0

    .line 1035908
    :pswitch_2
    sget-object v0, LX/5zs;->HTTP:LX/5zs;

    goto :goto_0

    .line 1035909
    :pswitch_3
    sget-object v0, LX/5zs;->HTTPS:LX/5zs;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2ff57c -> :sswitch_1
        0x310888 -> :sswitch_2
        0x5f008eb -> :sswitch_3
        0x38b73479 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static fromOrNull(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zs;
    .locals 1

    .prologue
    .line 1035910
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/5zs;->fromOrNull(Landroid/net/Uri;)LX/5zs;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5zs;
    .locals 1

    .prologue
    .line 1035911
    const-class v0, LX/5zs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5zs;

    return-object v0
.end method

.method public static values()[LX/5zs;
    .locals 1

    .prologue
    .line 1035912
    sget-object v0, LX/5zs;->$VALUES:[LX/5zs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5zs;

    return-object v0
.end method


# virtual methods
.method public final isLikelyLocal()Z
    .locals 1

    .prologue
    .line 1035913
    sget-object v0, LX/5zs;->CONTENT:LX/5zs;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/5zs;->FILE:LX/5zs;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
