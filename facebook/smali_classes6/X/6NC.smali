.class public LX/6NC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3On;


# instance fields
.field public a:Z

.field private final b:Lcom/facebook/omnistore/Cursor;

.field private final c:LX/6Nf;

.field private d:Lcom/facebook/user/model/User;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/omnistore/Cursor;LX/6Nf;)V
    .locals 1

    .prologue
    .line 1082733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1082734
    iput-object p1, p0, LX/6NC;->b:Lcom/facebook/omnistore/Cursor;

    .line 1082735
    iput-object p2, p0, LX/6NC;->c:LX/6Nf;

    .line 1082736
    const/4 v0, 0x0

    iput-object v0, p0, LX/6NC;->d:Lcom/facebook/user/model/User;

    .line 1082737
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6NC;->a:Z

    .line 1082738
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1082729
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6NC;->a:Z

    .line 1082730
    iget-object v0, p0, LX/6NC;->b:Lcom/facebook/omnistore/Cursor;

    invoke-virtual {v0}, Lcom/facebook/omnistore/Cursor;->step()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6NC;->c:LX/6Nf;

    iget-object v1, p0, LX/6NC;->b:Lcom/facebook/omnistore/Cursor;

    invoke-virtual {v1}, Lcom/facebook/omnistore/Cursor;->getPrimaryKey()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/6NC;->b:Lcom/facebook/omnistore/Cursor;

    invoke-virtual {v2}, Lcom/facebook/omnistore/Cursor;->getBlob()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/6Nf;->a(Ljava/lang/String;Ljava/nio/ByteBuffer;)Lcom/facebook/user/model/User;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/6NC;->d:Lcom/facebook/user/model/User;

    .line 1082731
    return-void

    .line 1082732
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 1082739
    iget-object v0, p0, LX/6NC;->b:Lcom/facebook/omnistore/Cursor;

    invoke-virtual {v0}, Lcom/facebook/omnistore/Cursor;->close()V

    .line 1082740
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 1082726
    iget-boolean v0, p0, LX/6NC;->a:Z

    if-eqz v0, :cond_0

    .line 1082727
    invoke-direct {p0}, LX/6NC;->b()V

    .line 1082728
    :cond_0
    iget-object v0, p0, LX/6NC;->d:Lcom/facebook/user/model/User;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1082722
    iget-boolean v0, p0, LX/6NC;->a:Z

    if-eqz v0, :cond_0

    .line 1082723
    invoke-direct {p0}, LX/6NC;->b()V

    .line 1082724
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6NC;->a:Z

    .line 1082725
    iget-object v0, p0, LX/6NC;->d:Lcom/facebook/user/model/User;

    return-object v0
.end method

.method public final remove()V
    .locals 3

    .prologue
    .line 1082721
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support remove()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
