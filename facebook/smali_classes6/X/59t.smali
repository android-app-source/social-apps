.class public final LX/59t;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 854641
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 854642
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 854643
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 854644
    invoke-static {p0, p1}, LX/59t;->b(LX/15w;LX/186;)I

    move-result v1

    .line 854645
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 854646
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 854618
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 854619
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 854620
    if-eqz v0, :cond_0

    .line 854621
    const-string v1, "index"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854622
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 854623
    :cond_0
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 854624
    if-eqz v0, :cond_1

    .line 854625
    const-string v0, "instream_placement"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854626
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 854627
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 854628
    if-eqz v0, :cond_2

    .line 854629
    const-string v1, "max_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854630
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 854631
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 854632
    if-eqz v0, :cond_3

    .line 854633
    const-string v1, "min_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854634
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 854635
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 854636
    if-eqz v0, :cond_4

    .line 854637
    const-string v1, "time_offset_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 854638
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 854639
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 854640
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 854647
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 854648
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 854649
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/59t;->a(LX/15i;ILX/0nX;)V

    .line 854650
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 854651
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 854652
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 854589
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 854590
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 854591
    :goto_0
    return v1

    .line 854592
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 854593
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 854594
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 854595
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 854596
    const-string v12, "index"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 854597
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v10, v5

    move v5, v2

    goto :goto_1

    .line 854598
    :cond_1
    const-string v12, "instream_placement"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 854599
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstreamPlacement;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 854600
    :cond_2
    const-string v12, "max_duration_in_ms"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 854601
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v2

    goto :goto_1

    .line 854602
    :cond_3
    const-string v12, "min_duration_in_ms"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 854603
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 854604
    :cond_4
    const-string v12, "time_offset_in_ms"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 854605
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 854606
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 854607
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 854608
    if-eqz v5, :cond_7

    .line 854609
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 854610
    :cond_7
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 854611
    if-eqz v4, :cond_8

    .line 854612
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8, v1}, LX/186;->a(III)V

    .line 854613
    :cond_8
    if-eqz v3, :cond_9

    .line 854614
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v7, v1}, LX/186;->a(III)V

    .line 854615
    :cond_9
    if-eqz v0, :cond_a

    .line 854616
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6, v1}, LX/186;->a(III)V

    .line 854617
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method
