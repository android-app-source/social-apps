.class public LX/6Fa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field public static final b:LX/0Tn;

.field private static final c:LX/0Tn;

.field private static final d:LX/0Tn;

.field private static volatile l:LX/6Fa;


# instance fields
.field public final e:LX/6G3;

.field private final f:Lcom/facebook/bugreporter/BugReportUploader;

.field private final g:LX/2fa;

.field public final h:LX/6GZ;

.field public final i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final j:LX/2E5;

.field private final k:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1068366
    const-class v0, LX/6Fa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Fa;->a:Ljava/lang/String;

    .line 1068367
    sget-object v0, LX/0Tm;->b:LX/0Tn;

    sget-object v1, LX/6Fa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "reports"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6Fa;->b:LX/0Tn;

    .line 1068368
    sget-object v0, LX/0Tm;->b:LX/0Tn;

    sget-object v1, LX/6Fa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "attachments"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6Fa;->c:LX/0Tn;

    .line 1068369
    sget-object v0, LX/0Tm;->b:LX/0Tn;

    sget-object v1, LX/6Fa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "attachment_meta"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6Fa;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/6G3;Lcom/facebook/bugreporter/BugReportUploader;LX/2fa;LX/6GZ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2E5;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1068166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1068167
    iput-object p1, p0, LX/6Fa;->e:LX/6G3;

    .line 1068168
    iput-object p2, p0, LX/6Fa;->f:Lcom/facebook/bugreporter/BugReportUploader;

    .line 1068169
    iput-object p3, p0, LX/6Fa;->g:LX/2fa;

    .line 1068170
    iput-object p4, p0, LX/6Fa;->h:LX/6GZ;

    .line 1068171
    iput-object p5, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1068172
    iput-object p6, p0, LX/6Fa;->j:LX/2E5;

    .line 1068173
    iput-object p7, p0, LX/6Fa;->k:LX/0W3;

    .line 1068174
    return-void
.end method

.method public static a(LX/0QB;)LX/6Fa;
    .locals 11

    .prologue
    .line 1068175
    sget-object v0, LX/6Fa;->l:LX/6Fa;

    if-nez v0, :cond_1

    .line 1068176
    const-class v1, LX/6Fa;

    monitor-enter v1

    .line 1068177
    :try_start_0
    sget-object v0, LX/6Fa;->l:LX/6Fa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1068178
    if-eqz v2, :cond_0

    .line 1068179
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1068180
    new-instance v3, LX/6Fa;

    invoke-static {v0}, LX/6G3;->a(LX/0QB;)LX/6G3;

    move-result-object v4

    check-cast v4, LX/6G3;

    invoke-static {v0}, Lcom/facebook/bugreporter/BugReportUploader;->a(LX/0QB;)Lcom/facebook/bugreporter/BugReportUploader;

    move-result-object v5

    check-cast v5, Lcom/facebook/bugreporter/BugReportUploader;

    invoke-static {v0}, LX/2fa;->a(LX/0QB;)LX/2fa;

    move-result-object v6

    check-cast v6, LX/2fa;

    invoke-static {v0}, LX/6GZ;->b(LX/0QB;)LX/6GZ;

    move-result-object v7

    check-cast v7, LX/6GZ;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2E5;->a(LX/0QB;)LX/2E5;

    move-result-object v9

    check-cast v9, LX/2E5;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v10

    check-cast v10, LX/0W3;

    invoke-direct/range {v3 .. v10}, LX/6Fa;-><init>(LX/6G3;Lcom/facebook/bugreporter/BugReportUploader;LX/2fa;LX/6GZ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2E5;LX/0W3;)V

    .line 1068181
    move-object v0, v3

    .line 1068182
    sput-object v0, LX/6Fa;->l:LX/6Fa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1068183
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1068184
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1068185
    :cond_1
    sget-object v0, LX/6Fa;->l:LX/6Fa;

    return-object v0

    .line 1068186
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1068187
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0hN;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1068188
    sget-object v0, LX/6Fa;->c:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {v0, p2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {p0, v0, p3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1068189
    sget-object v0, LX/6Fa;->d:LX/0Tn;

    invoke-virtual {v0, p1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {v0, p2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1068190
    const-string v1, "report_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-interface {p0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1068191
    const-string v1, "filename"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {p0, v0, p2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 1068192
    return-void
.end method

.method private static a(LX/6Fa;Ljava/io/File;LX/0Tn;LX/0Tn;)V
    .locals 2

    .prologue
    .line 1068193
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 1068194
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 1068195
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_0

    .line 1068196
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1068197
    :cond_0
    iget-object v0, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-interface {v0, p2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0, p3}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1068198
    return-void
.end method

.method private static a(LX/6Fa;Ljava/lang/String;LX/0Px;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1068199
    iget-object v1, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    .line 1068200
    iget-object v1, p0, LX/6Fa;->k:LX/0W3;

    sget-wide v4, LX/0X5;->aY:J

    invoke-interface {v1, v4, v5, v0}, LX/0W4;->a(JZ)Z

    move-result v1

    .line 1068201
    if-eqz v1, :cond_0

    .line 1068202
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1068203
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1068204
    const-string v5, "screenshot-%d.png"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1068205
    invoke-static {v3, p1, v5, v0}, LX/6Fa;->a(LX/0hN;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1068206
    add-int/lit8 v2, v2, 0x1

    .line 1068207
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1068208
    :cond_0
    if-eqz p3, :cond_1

    .line 1068209
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1068210
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1068211
    invoke-static {v3, p1, v0, v1}, LX/6Fa;->a(LX/0hN;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1068212
    :cond_1
    invoke-interface {v3}, LX/0hN;->commit()V

    .line 1068213
    return-void
.end method

.method public static a(LX/6Fa;Ljava/io/File;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1068214
    invoke-static {p1}, LX/6G3;->b(Ljava/io/File;)Lcom/facebook/bugreporter/BugReport;

    move-result-object v2

    .line 1068215
    if-nez v2, :cond_0

    .line 1068216
    invoke-static {p1}, LX/6G3;->a(Ljava/io/File;)V

    .line 1068217
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_CORRUPTED_DIRECTORY_DELETED:LX/6GY;

    invoke-virtual {v0, v2}, LX/6GZ;->a(LX/6GY;)V

    move v0, v1

    .line 1068218
    :goto_0
    return v0

    .line 1068219
    :cond_0
    iget-object v3, p0, LX/6Fa;->f:Lcom/facebook/bugreporter/BugReportUploader;

    invoke-virtual {v3, v2}, Lcom/facebook/bugreporter/BugReportUploader;->a(Lcom/facebook/bugreporter/BugReport;)Ljava/lang/String;

    move-result-object v3

    .line 1068220
    if-eqz v3, :cond_2

    .line 1068221
    iget-object v0, v2, Lcom/facebook/bugreporter/BugReport;->d:LX/0Px;

    move-object v0, v0

    .line 1068222
    iget-object v4, v2, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    move-object v4, v4

    .line 1068223
    invoke-static {p0, v3, v0, v4}, LX/6Fa;->a(LX/6Fa;Ljava/lang/String;LX/0Px;Ljava/util/Map;)V

    .line 1068224
    invoke-static {p1}, LX/6G3;->a(Ljava/io/File;)V

    .line 1068225
    iget v0, v2, Lcom/facebook/bugreporter/BugReport;->s:I

    move v0, v0

    .line 1068226
    if-lez v0, :cond_1

    .line 1068227
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_RETRY_UPLOAD_SUCCESS:LX/6GY;

    invoke-virtual {v0, v2}, LX/6GZ;->a(LX/6GY;)V

    :goto_1
    move v0, v1

    .line 1068228
    goto :goto_0

    .line 1068229
    :cond_1
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_DID_UPLOAD:LX/6GY;

    invoke-virtual {v0, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_1

    .line 1068230
    :cond_2
    iget v3, v2, Lcom/facebook/bugreporter/BugReport;->s:I

    move v3, v3

    .line 1068231
    iget-object v4, p0, LX/6Fa;->k:LX/0W3;

    sget-wide v6, LX/0X5;->aL:J

    const/4 v5, 0x3

    invoke-interface {v4, v6, v7, v5}, LX/0W4;->a(JI)I

    move-result v4

    if-lt v3, v4, :cond_4

    .line 1068232
    iget-object v0, v2, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    move-object v0, v0

    .line 1068233
    if-eqz v0, :cond_3

    .line 1068234
    iget-object v0, v2, Lcom/facebook/bugreporter/BugReport;->t:LX/0P1;

    move-object v0, v0

    .line 1068235
    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1068236
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_2

    .line 1068237
    :cond_3
    invoke-static {p1}, LX/6G3;->a(Ljava/io/File;)V

    .line 1068238
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    const/4 v3, 0x0

    .line 1068239
    sget-object v2, LX/6GY;->BUG_REPORT_FAILED_EXCEEDED_RETRIES:LX/6GY;

    invoke-static {v0, v2, v3}, LX/6GZ;->a(LX/6GZ;LX/6GY;Ljava/util/Map;)V

    .line 1068240
    sget-object v2, LX/6GY;->BUG_REPORT_FAILED_EXCEEDED_RETRIES:LX/6GY;

    invoke-static {v0, v2, v3}, LX/6GZ;->b(LX/6GZ;LX/6GY;Ljava/util/Map;)V

    .line 1068241
    move v0, v1

    .line 1068242
    goto :goto_0

    .line 1068243
    :cond_4
    invoke-static {}, Lcom/facebook/bugreporter/BugReport;->newBuilder()LX/6FU;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/6FU;->a(Lcom/facebook/bugreporter/BugReport;)LX/6FU;

    move-result-object v4

    .line 1068244
    iget v5, v2, Lcom/facebook/bugreporter/BugReport;->s:I

    move v5, v5

    .line 1068245
    add-int/lit8 v5, v5, 0x1

    .line 1068246
    iput v5, v4, LX/6FU;->t:I

    .line 1068247
    move-object v4, v4

    .line 1068248
    invoke-virtual {v4}, LX/6FU;->y()Lcom/facebook/bugreporter/BugReport;

    move-result-object v4

    .line 1068249
    :try_start_0
    invoke-static {v4}, LX/6G3;->b(Lcom/facebook/bugreporter/BugReport;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1068250
    :goto_3
    iget-object v2, p0, LX/6Fa;->h:LX/6GZ;

    sget-object v3, LX/6GY;->BUG_REPORT_FAILED_TO_UPLOAD:LX/6GY;

    invoke-virtual {v2, v3}, LX/6GZ;->a(LX/6GY;)V

    .line 1068251
    sget-object v2, LX/6Fa;->a:Ljava/lang/String;

    const-string v3, "Failed to upload bug report during. Path: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1068252
    :catch_0
    move-exception v5

    .line 1068253
    sget-object v6, LX/6G3;->a:Ljava/lang/Class;

    const-string v7, "Failed to persist serialized bug report."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v6, v5, v7, v8}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method

.method private static a(LX/6Fa;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 1068254
    iget-object v0, p0, LX/6Fa;->g:LX/2fa;

    const/4 v2, 0x0

    .line 1068255
    new-instance v1, LX/6FT;

    invoke-direct {v1, p3, p1, p2}, LX/6FT;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 1068256
    :try_start_0
    iget-object v3, v0, LX/2fa;->b:LX/11H;

    iget-object v4, v0, LX/2fa;->c:LX/6FS;

    invoke-virtual {v3, v4, v1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1068257
    :goto_0
    move v0, v1

    .line 1068258
    return v0

    .line 1068259
    :catch_0
    move-exception v1

    .line 1068260
    sget-object v3, LX/2fa;->a:Ljava/lang/Class;

    const-string v4, "Unable to upload attachment: %s"

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    aput-object p1, p0, v2

    invoke-static {v3, v1, v4, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1068261
    iget-object v3, v0, LX/2fa;->d:LX/03V;

    sget-object v4, LX/2fa;->a:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v2

    goto :goto_0
.end method

.method public static d(LX/6Fa;)Z
    .locals 14

    .prologue
    .line 1068262
    iget-object v0, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6Fa;->c:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v1

    .line 1068263
    const/4 v0, 0x1

    .line 1068264
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v8, v0

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1068265
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, LX/0Tn;

    .line 1068266
    sget-object v1, LX/6Fa;->c:LX/0Tn;

    invoke-virtual {v6, v1}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v1

    .line 1068267
    sget-object v2, LX/6Fa;->d:LX/0Tn;

    invoke-virtual {v2, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    move-object v7, v1

    check-cast v7, LX/0Tn;

    .line 1068268
    iget-object v2, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v1, "report_id"

    invoke-virtual {v7, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1068269
    iget-object v2, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v1, "retry_num"

    invoke-virtual {v7, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 1068270
    iget-object v2, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-string v1, "filename"

    invoke-virtual {v7, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1068271
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1068272
    const/4 v4, 0x0

    .line 1068273
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v9, Ljava/net/URI;

    invoke-direct {v9, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v9}, Ljava/io/File;-><init>(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v9, v1

    .line 1068274
    :goto_1
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1068275
    :cond_0
    iget-object v0, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    invoke-interface {v0, v6}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0, v7}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    .line 1068276
    :catch_0
    move-exception v0

    .line 1068277
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 1068278
    const-string v1, "TAG"

    const-string v9, "Ignoring invalid debug attachment: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v2, v11, v12

    invoke-static {v1, v0, v9, v11}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v9, v4

    goto :goto_1

    .line 1068279
    :cond_1
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v12, 0x0

    cmp-long v0, v0, v12

    if-nez v0, :cond_3

    .line 1068280
    :cond_2
    invoke-static {p0, v9, v6, v7}, LX/6Fa;->a(LX/6Fa;Ljava/io/File;LX/0Tn;LX/0Tn;)V

    goto/16 :goto_0

    .line 1068281
    :cond_3
    invoke-static {p0, v2, v9, v3}, LX/6Fa;->a(LX/6Fa;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Z

    move-result v11

    .line 1068282
    if-eqz v11, :cond_5

    .line 1068283
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_4

    .line 1068284
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_ATTACHMENT_DID_UPLOAD:LX/6GY;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, LX/6GZ;->a(LX/6GY;Ljava/lang/String;Ljava/lang/String;J)V

    .line 1068285
    :goto_2
    invoke-static {p0, v9, v6, v7}, LX/6Fa;->a(LX/6Fa;Ljava/io/File;LX/0Tn;LX/0Tn;)V

    .line 1068286
    :goto_3
    if-eqz v8, :cond_7

    if-eqz v11, :cond_7

    const/4 v0, 0x1

    :goto_4
    move v8, v0

    .line 1068287
    goto/16 :goto_0

    .line 1068288
    :cond_4
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_ATTACHMENT_RETRY_UPLOAD_SUCCESS:LX/6GY;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, LX/6GZ;->a(LX/6GY;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_2

    .line 1068289
    :cond_5
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, LX/6Fa;->k:LX/0W3;

    sget-wide v12, LX/0X5;->aK:J

    const/4 v4, 0x2

    invoke-interface {v1, v12, v13, v4}, LX/0W4;->a(JI)I

    move-result v1

    if-lt v0, v1, :cond_6

    .line 1068290
    invoke-static {p0, v9, v6, v7}, LX/6Fa;->a(LX/6Fa;Ljava/io/File;LX/0Tn;LX/0Tn;)V

    .line 1068291
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_EXCEEDED_RETRIES:LX/6GY;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, LX/6GZ;->a(LX/6GY;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_3

    .line 1068292
    :cond_6
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1068293
    iget-object v0, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    const-string v0, "retry_num"

    invoke-virtual {v7, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v4, v0, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1068294
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_UPLOAD:LX/6GY;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, LX/6GZ;->a(LX/6GY;Ljava/lang/String;Ljava/lang/String;J)V

    .line 1068295
    sget-object v0, LX/6Fa;->a:Ljava/lang/String;

    const-string v1, "Failed to upload bug report attachment. Path: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 1068296
    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    .line 1068297
    :cond_8
    return v8
.end method


# virtual methods
.method public final a(Lcom/facebook/bugreporter/BugReport;)V
    .locals 7

    .prologue
    .line 1068298
    iget-object v0, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v0, LX/6Fa;->b:LX/0Tn;

    .line 1068299
    iget-object v2, p1, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    move-object v2, v2

    .line 1068300
    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1068301
    iget-object v2, p1, Lcom/facebook/bugreporter/BugReport;->a:Landroid/net/Uri;

    move-object v2, v2

    .line 1068302
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1068303
    iget-object v0, p0, LX/6Fa;->j:LX/2E5;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/2E5;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1068304
    sget-object v0, LX/6Fa;->a:Ljava/lang/String;

    const-string v1, "Failed to schedule upload for a bug report."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068305
    :cond_0
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    .line 1068306
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "bug_report_created"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "bugreporter"

    .line 1068307
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1068308
    move-object v1, v1

    .line 1068309
    const-string v2, "bug_creation_ttime"

    .line 1068310
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->r:Ljava/lang/String;

    move-object v3, v3

    .line 1068311
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_source"

    .line 1068312
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->q:LX/6Fb;

    move-object v3, v3

    .line 1068313
    invoke-virtual {v3}, LX/6Fb;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_category_id"

    .line 1068314
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->h:Ljava/lang/String;

    move-object v3, v3

    .line 1068315
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_description"

    .line 1068316
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1068317
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_build_number"

    .line 1068318
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->k:Ljava/lang/String;

    move-object v3, v3

    .line 1068319
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_build_timestamp"

    .line 1068320
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->l:Ljava/lang/String;

    move-object v3, v3

    .line 1068321
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_git_hash"

    .line 1068322
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->j:Ljava/lang/String;

    move-object v3, v3

    .line 1068323
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_git_branch"

    .line 1068324
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->m:Ljava/lang/String;

    move-object v3, v3

    .line 1068325
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_network_type"

    .line 1068326
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->n:Ljava/lang/String;

    move-object v3, v3

    .line 1068327
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_network_subtype"

    .line 1068328
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->o:Ljava/lang/String;

    move-object v3, v3

    .line 1068329
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_report_id"

    .line 1068330
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    move-object v3, v3

    .line 1068331
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "bug_timed_out_attachments"

    .line 1068332
    iget-object v3, p1, Lcom/facebook/bugreporter/BugReport;->w:Ljava/lang/String;

    move-object v3, v3

    .line 1068333
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1068334
    iget-object v2, v0, LX/6GZ;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1068335
    const/16 v6, 0x14

    .line 1068336
    iget-object v0, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6Fa;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    .line 1068337
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-gt v1, v6, :cond_6

    .line 1068338
    :cond_1
    iget-object v0, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6Fa;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v1

    .line 1068339
    iget-object v0, p0, LX/6Fa;->e:LX/6G3;

    .line 1068340
    const-string v2, "bugreports"

    invoke-static {v0, v2}, LX/6G3;->c(LX/6G3;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1068341
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 1068342
    move-object v2, v2

    .line 1068343
    if-eqz v2, :cond_2

    array-length v0, v2

    if-nez v0, :cond_7

    .line 1068344
    :cond_2
    return-void

    .line 1068345
    :cond_3
    iget-object v0, p0, LX/6Fa;->h:LX/6GZ;

    const/4 v4, 0x0

    .line 1068346
    sget-object v1, LX/6GY;->BUG_REPORT_FAILED_EXCEEDED_QUEUE_SIZE:LX/6GY;

    invoke-static {v0, v1, v4}, LX/6GZ;->a(LX/6GZ;LX/6GY;Ljava/util/Map;)V

    .line 1068347
    sget-object v1, LX/6GY;->BUG_REPORT_FAILED_EXCEEDED_QUEUE_SIZE:LX/6GY;

    invoke-static {v0, v1, v4}, LX/6GZ;->b(LX/6GZ;LX/6GY;Ljava/util/Map;)V

    .line 1068348
    new-instance v1, Ljava/io/File;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, LX/6G3;->a(Ljava/io/File;)V

    .line 1068349
    iget-object v0, p0, LX/6Fa;->e:LX/6G3;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6G3;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1068350
    if-eqz v0, :cond_4

    .line 1068351
    invoke-static {v0}, LX/6G3;->a(Ljava/io/File;)V

    .line 1068352
    :cond_4
    iget-object v0, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1068353
    iget-object v0, p0, LX/6Fa;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6Fa;->b:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    move-object v2, v0

    .line 1068354
    :goto_0
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    if-le v0, v6, :cond_1

    .line 1068355
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1068356
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Tn;

    sget-object v3, LX/6Fa;->b:LX/0Tn;

    invoke-virtual {v1, v3}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1068357
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    move-object v3, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1068358
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Tn;

    sget-object v5, LX/6Fa;->b:LX/0Tn;

    invoke-virtual {v1, v5}, LX/0To;->b(LX/0To;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1068359
    if-le v2, v1, :cond_5

    move p1, v1

    move-object v1, v0

    move v0, p1

    :goto_2
    move v2, v0

    move-object v3, v1

    .line 1068360
    goto :goto_1

    :cond_5
    move v0, v2

    move-object v1, v3

    goto :goto_2

    :cond_6
    move-object v2, v0

    goto :goto_0

    .line 1068361
    :cond_7
    array-length v3, v2

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 1068362
    sget-object v5, LX/6Fa;->b:LX/0Tn;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 1068363
    invoke-static {v4}, LX/6G3;->a(Ljava/io/File;)V

    .line 1068364
    iget-object v4, p0, LX/6Fa;->h:LX/6GZ;

    sget-object v5, LX/6GY;->BUG_REPORT_UNTRACKED_DIRECTORY_DELETED:LX/6GY;

    invoke-virtual {v4, v5}, LX/6GZ;->a(LX/6GY;)V

    .line 1068365
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method
