.class public abstract LX/5Jf;
.super LX/5Je;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5Je",
        "<",
        "LX/3wu;",
        "LX/5K5;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3wu;)V
    .locals 1

    .prologue
    .line 897425
    new-instance v0, LX/5K5;

    invoke-direct {v0}, LX/5K5;-><init>()V

    invoke-direct {p0, p1, p2, v0}, LX/5Jf;-><init>(Landroid/content/Context;LX/3wu;LX/5K5;)V

    .line 897426
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/3wu;LX/5K5;)V
    .locals 0

    .prologue
    .line 897469
    invoke-direct {p0, p1, p2, p3}, LX/5Je;-><init>(Landroid/content/Context;LX/1OR;LX/5K5;)V

    .line 897470
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 897459
    invoke-super {p0, p1, p2, p3}, LX/5Je;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 897460
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/3wu;

    .line 897461
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v1

    .line 897462
    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v2

    .line 897463
    if-eq v1, v3, :cond_0

    if-ne v2, v3, :cond_1

    .line 897464
    :cond_0
    :goto_0
    return-void

    .line 897465
    :cond_1
    iget-object v0, p0, LX/3mY;->i:LX/3me;

    move-object v0, v0

    .line 897466
    check-cast v0, LX/5K5;

    sub-int/2addr v2, v1

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/5K5;->a(II)V

    .line 897467
    goto :goto_0
    .line 897468
.end method

.method public final b(III)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 897443
    invoke-virtual {p0}, LX/3mY;->bF_()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 897444
    iput v3, p0, LX/5Jf;->a:I

    .line 897445
    iput v3, p0, LX/5Jf;->b:I

    .line 897446
    :cond_0
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/3wu;

    .line 897447
    iget v1, v0, LX/1P1;->j:I

    move v0, v1

    .line 897448
    if-ne v0, v2, :cond_1

    .line 897449
    invoke-virtual {p0}, LX/3mY;->c()I

    move-result v1

    .line 897450
    invoke-virtual {p0}, LX/3mY;->d()I

    move-result v0

    .line 897451
    :goto_0
    iget v4, p0, LX/5Jf;->b:I

    add-int/2addr v4, p2

    iput v4, p0, LX/5Jf;->b:I

    .line 897452
    iget v4, p0, LX/5Jf;->b:I

    if-ge v4, v0, :cond_2

    move v0, v2

    .line 897453
    :goto_1
    return v0

    .line 897454
    :cond_1
    invoke-virtual {p0}, LX/3mY;->d()I

    move-result v1

    .line 897455
    invoke-virtual {p0}, LX/3mY;->c()I

    move-result v0

    move v5, p3

    move p3, p2

    move p2, v5

    goto :goto_0

    .line 897456
    :cond_2
    iput v3, p0, LX/5Jf;->b:I

    .line 897457
    iget v0, p0, LX/5Jf;->a:I

    add-int/2addr v0, p3

    iput v0, p0, LX/5Jf;->a:I

    .line 897458
    iget v0, p0, LX/5Jf;->a:I

    mul-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_3

    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final e(I)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 897435
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/3wu;

    .line 897436
    iget v1, v0, LX/3wu;->c:I

    move v1, v1

    .line 897437
    iget-object v2, v0, LX/3wu;->h:LX/3wr;

    move-object v2, v2

    .line 897438
    invoke-virtual {v2, p1}, LX/3wr;->a(I)I

    move-result v2

    .line 897439
    iget v3, v0, LX/1P1;->j:I

    move v0, v3

    .line 897440
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 897441
    invoke-super {p0, p1}, LX/5Je;->e(I)I

    move-result v0

    invoke-static {v0}, LX/1mh;->b(I)I

    move-result v0

    div-int/2addr v0, v1

    mul-int/2addr v0, v2

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, LX/1mh;->a(II)I

    move-result v0

    .line 897442
    :goto_0
    return v0

    :cond_0
    invoke-static {v4, v4}, LX/1mh;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method public final f(I)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 897427
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/3wu;

    .line 897428
    iget v1, v0, LX/3wu;->c:I

    move v1, v1

    .line 897429
    iget-object v2, v0, LX/3wu;->h:LX/3wr;

    move-object v2, v2

    .line 897430
    invoke-virtual {v2, p1}, LX/3wr;->a(I)I

    move-result v2

    .line 897431
    iget v3, v0, LX/1P1;->j:I

    move v0, v3

    .line 897432
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 897433
    invoke-static {v4, v4}, LX/1mh;->a(II)I

    move-result v0

    .line 897434
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/5Je;->f(I)I

    move-result v0

    invoke-static {v0}, LX/1mh;->b(I)I

    move-result v0

    div-int/2addr v0, v1

    mul-int/2addr v0, v2

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, LX/1mh;->a(II)I

    move-result v0

    goto :goto_0
.end method
