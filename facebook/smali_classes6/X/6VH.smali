.class public final enum LX/6VH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6VH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6VH;

.field public static final enum CLICK_TO_COMPOSER:LX/6VH;

.field public static final enum IMPRESSION:LX/6VH;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1103927
    new-instance v0, LX/6VH;

    const-string v1, "IMPRESSION"

    invoke-direct {v0, v1, v2}, LX/6VH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VH;->IMPRESSION:LX/6VH;

    .line 1103928
    new-instance v0, LX/6VH;

    const-string v1, "CLICK_TO_COMPOSER"

    invoke-direct {v0, v1, v3}, LX/6VH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6VH;->CLICK_TO_COMPOSER:LX/6VH;

    .line 1103929
    const/4 v0, 0x2

    new-array v0, v0, [LX/6VH;

    sget-object v1, LX/6VH;->IMPRESSION:LX/6VH;

    aput-object v1, v0, v2

    sget-object v1, LX/6VH;->CLICK_TO_COMPOSER:LX/6VH;

    aput-object v1, v0, v3

    sput-object v0, LX/6VH;->$VALUES:[LX/6VH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1103924
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6VH;
    .locals 1

    .prologue
    .line 1103926
    const-class v0, LX/6VH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6VH;

    return-object v0
.end method

.method public static values()[LX/6VH;
    .locals 1

    .prologue
    .line 1103925
    sget-object v0, LX/6VH;->$VALUES:[LX/6VH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6VH;

    return-object v0
.end method
