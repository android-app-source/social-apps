.class public LX/6HV;
.super Landroid/view/SurfaceView;
.source ""

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Landroid/view/SurfaceHolder;

.field public c:Landroid/hardware/Camera;

.field public d:LX/6HS;

.field public e:Lcom/facebook/qrcode/QRCodeFragment;

.field public f:LX/6Ha;

.field public g:LX/6He;

.field public final h:LX/6HF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1072154
    const-class v0, LX/6HV;

    sput-object v0, LX/6HV;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/hardware/Camera;LX/6Ha;LX/6He;LX/6HF;)V
    .locals 2

    .prologue
    .line 1072155
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 1072156
    iput-object p2, p0, LX/6HV;->c:Landroid/hardware/Camera;

    .line 1072157
    iput-object p3, p0, LX/6HV;->f:LX/6Ha;

    .line 1072158
    iput-object p5, p0, LX/6HV;->h:LX/6HF;

    .line 1072159
    invoke-virtual {p0}, LX/6HV;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, LX/6HV;->b:Landroid/view/SurfaceHolder;

    .line 1072160
    iget-object v0, p0, LX/6HV;->b:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 1072161
    iget-object v0, p0, LX/6HV;->b:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 1072162
    iput-object p4, p0, LX/6HV;->g:LX/6He;

    .line 1072163
    return-void
.end method


# virtual methods
.method public final onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 3

    .prologue
    .line 1072164
    iget-object v0, p0, LX/6HV;->e:Lcom/facebook/qrcode/QRCodeFragment;

    if-eqz v0, :cond_0

    .line 1072165
    iget-object v0, p0, LX/6HV;->e:Lcom/facebook/qrcode/QRCodeFragment;

    invoke-virtual {p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    .line 1072166
    iget-object v2, v0, Lcom/facebook/qrcode/QRCodeFragment;->ab:Ljava/util/TimerTask;

    if-eqz v2, :cond_1

    .line 1072167
    :cond_0
    :goto_0
    return-void

    .line 1072168
    :cond_1
    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    .line 1072169
    iget-object p0, v0, Lcom/facebook/qrcode/QRCodeFragment;->x:LX/FUm;

    iget p2, v2, Landroid/hardware/Camera$Size;->width:I

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p0, p1, p2, v2}, LX/FUm;->a([BII)V

    goto :goto_0
.end method

.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    .prologue
    .line 1072170
    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 4

    .prologue
    .line 1072171
    iget-object v0, p0, LX/6HV;->d:LX/6HS;

    if-eqz v0, :cond_0

    .line 1072172
    iget-object v0, p0, LX/6HV;->d:LX/6HS;

    invoke-virtual {v0}, LX/6HS;->a()V

    .line 1072173
    :cond_0
    const/4 v0, 0x0

    .line 1072174
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1072175
    :cond_1
    :goto_0
    move v0, v0

    .line 1072176
    iget-object v1, p0, LX/6HV;->d:LX/6HS;

    if-eqz v1, :cond_2

    .line 1072177
    iget-object v1, p0, LX/6HV;->d:LX/6HS;

    invoke-virtual {v1, v0}, LX/6HS;->a(Z)V

    .line 1072178
    :cond_2
    return-void

    .line 1072179
    :cond_3
    iput-object p1, p0, LX/6HV;->b:Landroid/view/SurfaceHolder;

    .line 1072180
    iget-object v1, p0, LX/6HV;->c:Landroid/hardware/Camera;

    if-eqz v1, :cond_1

    .line 1072181
    iget-object v1, p0, LX/6HV;->g:LX/6He;

    if-eqz v1, :cond_4

    .line 1072182
    iget-object v1, p0, LX/6HV;->g:LX/6He;

    const/4 v2, 0x0

    .line 1072183
    iget-object v3, v1, LX/6He;->b:Landroid/hardware/Camera;

    if-eqz v3, :cond_4

    iget-boolean v3, v1, LX/6He;->e:Z

    if-eqz v3, :cond_4

    iget-boolean v3, v1, LX/6He;->f:Z

    if-nez v3, :cond_6

    .line 1072184
    :cond_4
    :goto_1
    iget-object v1, p0, LX/6HV;->c:Landroid/hardware/Camera;

    const v2, -0x27157bb5

    invoke-static {v1, v2}, LX/0J2;->c(Landroid/hardware/Camera;I)V

    .line 1072185
    iget-object v1, p0, LX/6HV;->f:LX/6Ha;

    invoke-virtual {v1}, LX/6Ha;->d()V

    .line 1072186
    :try_start_0
    iget-object v1, p0, LX/6HV;->f:LX/6Ha;

    invoke-virtual {v1}, LX/6Ha;->g()V

    .line 1072187
    iget-object v1, p0, LX/6HV;->c:Landroid/hardware/Camera;

    iget-object v2, p0, LX/6HV;->b:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1072188
    :goto_2
    :try_start_1
    iget-object v1, p0, LX/6HV;->c:Landroid/hardware/Camera;

    const v2, -0x53cb688c

    invoke-static {v1, v2}, LX/0J2;->b(Landroid/hardware/Camera;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1072189
    iget-object v0, p0, LX/6HV;->f:LX/6Ha;

    .line 1072190
    const/4 v1, 0x0

    iput v1, v0, LX/6Ha;->c:I

    .line 1072191
    iget-object v0, p0, LX/6HV;->g:LX/6He;

    if-eqz v0, :cond_5

    .line 1072192
    iget-object v0, p0, LX/6HV;->g:LX/6He;

    .line 1072193
    iget-object v3, v0, LX/6He;->b:Landroid/hardware/Camera;

    if-eqz v3, :cond_5

    iget-boolean v3, v0, LX/6He;->e:Z

    if-eqz v3, :cond_5

    iget-boolean v3, v0, LX/6He;->f:Z

    if-eqz v3, :cond_7

    .line 1072194
    :cond_5
    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    .line 1072195
    :catch_0
    move-exception v1

    .line 1072196
    iget-object v2, p0, LX/6HV;->h:LX/6HF;

    const-string v3, "CameraPreview/setPreviewDisplay failed"

    invoke-interface {v2, v3, v1}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 1072197
    :catch_1
    move-exception v1

    .line 1072198
    iget-object v2, p0, LX/6HV;->h:LX/6HF;

    const-string v3, "CameraPreview/startPreview failed"

    invoke-interface {v2, v3, v1}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 1072199
    :cond_6
    iget-object v3, v1, LX/6He;->b:Landroid/hardware/Camera;

    const/4 p1, 0x0

    invoke-virtual {v3, p1}, Landroid/hardware/Camera;->setFaceDetectionListener(Landroid/hardware/Camera$FaceDetectionListener;)V

    .line 1072200
    :try_start_2
    iget-object v3, v1, LX/6He;->b:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->stopFaceDetection()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1072201
    iput-boolean v2, v1, LX/6He;->f:Z

    .line 1072202
    goto :goto_1

    .line 1072203
    :catch_2
    const-string v3, "Could not stop face detection"

    invoke-static {v1, v3}, LX/6He;->a(LX/6He;Ljava/lang/String;)V

    goto :goto_1

    .line 1072204
    :cond_7
    :try_start_3
    iget-object v3, v0, LX/6He;->b:Landroid/hardware/Camera;

    invoke-virtual {v3}, Landroid/hardware/Camera;->startFaceDetection()V

    .line 1072205
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/6He;->f:Z
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_3

    .line 1072206
    :catch_3
    const-string v1, "Could not start FD -- not supported"

    invoke-static {v0, v1}, LX/6He;->a(LX/6He;Ljava/lang/String;)V

    .line 1072207
    goto :goto_3

    .line 1072208
    :catch_4
    const-string v1, "Could not start FD -- already running or failure"

    invoke-static {v0, v1}, LX/6He;->a(LX/6He;Ljava/lang/String;)V

    .line 1072209
    goto :goto_3
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 1072210
    return-void
.end method
