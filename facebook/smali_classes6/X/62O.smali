.class public LX/62O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1UP;


# instance fields
.field public a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

.field private b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private c:LX/1DI;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V
    .locals 1

    .prologue
    .line 1041565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1041566
    if-nez p1, :cond_0

    new-instance v0, LX/62Q;

    invoke-direct {v0}, LX/62Q;-><init>()V

    invoke-virtual {v0}, LX/62Q;->a()Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-result-object p1

    :cond_0
    iput-object p1, p0, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 1041567
    iput-object p2, p0, LX/62O;->c:LX/1DI;

    .line 1041568
    return-void
.end method

.method public static e(LX/62O;)V
    .locals 3

    .prologue
    .line 1041569
    iget-object v0, p0, LX/62O;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-nez v0, :cond_0

    .line 1041570
    :goto_0
    return-void

    .line 1041571
    :cond_0
    sget-object v0, LX/62N;->a:[I

    iget-object v1, p0, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 1041572
    iget-object v2, v1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    move-object v1, v2

    .line 1041573
    invoke-virtual {v1}, LX/1lD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1041574
    :pswitch_0
    iget-object v0, p0, LX/62O;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_0

    .line 1041575
    :pswitch_1
    iget-object v0, p0, LX/62O;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto :goto_0

    .line 1041576
    :pswitch_2
    iget-object v0, p0, LX/62O;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iget-object v2, p0, LX/62O;->c:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1041561
    iget-object v0, p0, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    sget-object v1, LX/1lD;->LOADING:LX/1lD;

    .line 1041562
    iput-object v1, v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    .line 1041563
    invoke-static {p0}, LX/62O;->e(LX/62O;)V

    .line 1041564
    return-void
.end method

.method public final a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V
    .locals 0

    .prologue
    .line 1041577
    iput-object p1, p0, LX/62O;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1041578
    invoke-static {p0}, LX/62O;->e(LX/62O;)V

    .line 1041579
    return-void
.end method

.method public final a(Ljava/lang/String;LX/1DI;)V
    .locals 2

    .prologue
    .line 1041554
    iget-object v0, p0, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    .line 1041555
    iput-object v1, v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    .line 1041556
    iget-object v0, p0, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 1041557
    iput-object p1, v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->b:Ljava/lang/String;

    .line 1041558
    iput-object p2, p0, LX/62O;->c:LX/1DI;

    .line 1041559
    invoke-static {p0}, LX/62O;->e(LX/62O;)V

    .line 1041560
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1041548
    iget-object v0, p0, LX/62O;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    sget-object v1, LX/1lD;->LOAD_FINISHED:LX/1lD;

    .line 1041549
    iput-object v1, v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->a:LX/1lD;

    .line 1041550
    invoke-static {p0}, LX/62O;->e(LX/62O;)V

    .line 1041551
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1041552
    const/4 v0, 0x0

    iput-object v0, p0, LX/62O;->b:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1041553
    return-void
.end method
