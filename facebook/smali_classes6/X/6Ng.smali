.class public final LX/6Ng;
.super LX/0eW;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1083862
    invoke-direct {p0}, LX/0eW;-><init>()V

    return-void
.end method

.method private static a(LX/0eX;)I
    .locals 1

    .prologue
    .line 1083892
    invoke-virtual {p0}, LX/0eX;->c()I

    move-result v0

    .line 1083893
    return v0
.end method

.method public static a(LX/0eX;IIIIFFZZZJZBBIIZFI)I
    .locals 2

    .prologue
    .line 1083872
    const/16 v1, 0x12

    invoke-virtual {p0, v1}, LX/0eX;->b(I)V

    .line 1083873
    invoke-static {p0, p10, p11}, LX/6Ng;->a(LX/0eX;J)V

    .line 1083874
    move/from16 v0, p19

    invoke-static {p0, v0}, LX/6Ng;->g(LX/0eX;I)V

    .line 1083875
    move/from16 v0, p18

    invoke-static {p0, v0}, LX/6Ng;->c(LX/0eX;F)V

    .line 1083876
    move/from16 v0, p16

    invoke-static {p0, v0}, LX/6Ng;->f(LX/0eX;I)V

    .line 1083877
    move/from16 v0, p15

    invoke-static {p0, v0}, LX/6Ng;->e(LX/0eX;I)V

    .line 1083878
    invoke-static {p0, p6}, LX/6Ng;->b(LX/0eX;F)V

    .line 1083879
    invoke-static {p0, p5}, LX/6Ng;->a(LX/0eX;F)V

    .line 1083880
    invoke-static {p0, p4}, LX/6Ng;->d(LX/0eX;I)V

    .line 1083881
    invoke-static {p0, p3}, LX/6Ng;->c(LX/0eX;I)V

    .line 1083882
    invoke-static {p0, p2}, LX/6Ng;->b(LX/0eX;I)V

    .line 1083883
    invoke-static {p0, p1}, LX/6Ng;->a(LX/0eX;I)V

    .line 1083884
    move/from16 v0, p17

    invoke-static {p0, v0}, LX/6Ng;->e(LX/0eX;Z)V

    .line 1083885
    move/from16 v0, p14

    invoke-static {p0, v0}, LX/6Ng;->b(LX/0eX;B)V

    .line 1083886
    invoke-static {p0, p13}, LX/6Ng;->a(LX/0eX;B)V

    .line 1083887
    invoke-static {p0, p12}, LX/6Ng;->d(LX/0eX;Z)V

    .line 1083888
    invoke-static {p0, p9}, LX/6Ng;->c(LX/0eX;Z)V

    .line 1083889
    invoke-static {p0, p8}, LX/6Ng;->b(LX/0eX;Z)V

    .line 1083890
    invoke-static {p0, p7}, LX/6Ng;->a(LX/0eX;Z)V

    .line 1083891
    invoke-static {p0}, LX/6Ng;->a(LX/0eX;)I

    move-result v1

    return v1
.end method

.method private static a(LX/0eX;B)V
    .locals 2

    .prologue
    .line 1083871
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IBI)V

    return-void
.end method

.method private static a(LX/0eX;F)V
    .locals 4

    .prologue
    .line 1083870
    const/4 v0, 0x4

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, p1, v2, v3}, LX/0eX;->a(IFD)V

    return-void
.end method

.method private static a(LX/0eX;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1083869
    invoke-virtual {p0, v0, p1, v0}, LX/0eX;->c(III)V

    return-void
.end method

.method private static a(LX/0eX;J)V
    .locals 7

    .prologue
    .line 1083868
    const/16 v1, 0x9

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, LX/0eX;->a(IJJ)V

    return-void
.end method

.method private static a(LX/0eX;Z)V
    .locals 2

    .prologue
    .line 1083867
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IZZ)V

    return-void
.end method

.method private static b(LX/0eX;B)V
    .locals 2

    .prologue
    .line 1083866
    const/16 v0, 0xc

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IBI)V

    return-void
.end method

.method private static b(LX/0eX;F)V
    .locals 4

    .prologue
    .line 1083865
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, p1, v2, v3}, LX/0eX;->a(IFD)V

    return-void
.end method

.method private static b(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083864
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static b(LX/0eX;Z)V
    .locals 2

    .prologue
    .line 1083863
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IZZ)V

    return-void
.end method

.method private static c(LX/0eX;F)V
    .locals 4

    .prologue
    .line 1083861
    const/16 v0, 0x10

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, p1, v2, v3}, LX/0eX;->a(IFD)V

    return-void
.end method

.method private static c(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083860
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static c(LX/0eX;Z)V
    .locals 2

    .prologue
    .line 1083894
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IZZ)V

    return-void
.end method

.method private static d(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083843
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static d(LX/0eX;Z)V
    .locals 2

    .prologue
    .line 1083845
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IZZ)V

    return-void
.end method

.method private static e(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083846
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static e(LX/0eX;Z)V
    .locals 2

    .prologue
    .line 1083844
    const/16 v0, 0xf

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->a(IZZ)V

    return-void
.end method

.method private static f(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083847
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method

.method private static g(LX/0eX;I)V
    .locals 2

    .prologue
    .line 1083848
    const/16 v0, 0x11

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, LX/0eX;->c(III)V

    return-void
.end method


# virtual methods
.method public final a(LX/6Nl;)LX/6Nl;
    .locals 2

    .prologue
    .line 1083849
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->b(I)I

    move-result v0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1083850
    iput v0, p1, LX/6Nl;->a:I

    iput-object v1, p1, LX/6Nl;->b:Ljava/nio/ByteBuffer;

    move-object v0, p1

    .line 1083851
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1083852
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()F
    .locals 3

    .prologue
    .line 1083853
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getFloat(I)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1083854
    const/16 v1, 0x12

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final i()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1083855
    const/16 v1, 0x14

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final j()J
    .locals 3

    .prologue
    .line 1083856
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final m()B
    .locals 3

    .prologue
    .line 1083857
    const/16 v0, 0x1c

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1083858
    const/16 v1, 0x22

    invoke-virtual {p0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v3, p0, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1083859
    const/16 v0, 0x26

    invoke-virtual {p0, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, LX/0eW;->a:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, LX/0eW;->c(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
