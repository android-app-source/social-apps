.class public final LX/595;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 850095
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 850096
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 850097
    :goto_0
    return v1

    .line 850098
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 850099
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 850100
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 850101
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 850102
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 850103
    const-string v4, "feedback_typers"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 850104
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 850105
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_d

    .line 850106
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 850107
    :goto_2
    move v2, v3

    .line 850108
    goto :goto_1

    .line 850109
    :cond_2
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 850110
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 850111
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 850112
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 850113
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 850114
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 850115
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 850116
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 850117
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 850118
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_5

    if-eqz v10, :cond_5

    .line 850119
    const-string v11, "count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 850120
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v9, v6

    move v6, v4

    goto :goto_3

    .line 850121
    :cond_6
    const-string v11, "friend_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 850122
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v8, v5

    move v5, v4

    goto :goto_3

    .line 850123
    :cond_7
    const-string v11, "other_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 850124
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v7, v2

    move v2, v4

    goto :goto_3

    .line 850125
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 850126
    :cond_9
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 850127
    if-eqz v6, :cond_a

    .line 850128
    invoke-virtual {p1, v3, v9, v3}, LX/186;->a(III)V

    .line 850129
    :cond_a
    if-eqz v5, :cond_b

    .line 850130
    invoke-virtual {p1, v4, v8, v3}, LX/186;->a(III)V

    .line 850131
    :cond_b
    if-eqz v2, :cond_c

    .line 850132
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7, v3}, LX/186;->a(III)V

    .line 850133
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_d
    move v2, v3

    move v5, v3

    move v6, v3

    move v7, v3

    move v8, v3

    move v9, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 850134
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 850135
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 850136
    if-eqz v0, :cond_3

    .line 850137
    const-string v1, "feedback_typers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850138
    const/4 p3, 0x0

    .line 850139
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 850140
    invoke-virtual {p0, v0, p3, p3}, LX/15i;->a(III)I

    move-result v1

    .line 850141
    if-eqz v1, :cond_0

    .line 850142
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850143
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 850144
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, p3}, LX/15i;->a(III)I

    move-result v1

    .line 850145
    if-eqz v1, :cond_1

    .line 850146
    const-string v2, "friend_count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850147
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 850148
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, p3}, LX/15i;->a(III)I

    move-result v1

    .line 850149
    if-eqz v1, :cond_2

    .line 850150
    const-string v2, "other_count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850151
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 850152
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 850153
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 850154
    if-eqz v0, :cond_4

    .line 850155
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 850156
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 850157
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 850158
    return-void
.end method
