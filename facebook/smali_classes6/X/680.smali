.class public final LX/680;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/67o;
.implements LX/67p;
.implements LX/67q;


# instance fields
.field public A:Lcom/facebook/android/maps/MapView;

.field public final B:I

.field private final C:[F

.field private final D:Landroid/graphics/Matrix;

.field public final E:LX/68Q;

.field private F:LX/6aX;

.field private G:Z

.field private H:LX/68u;

.field private I:LX/68u;

.field private J:LX/68u;

.field private K:LX/68u;

.field private L:F

.field private M:F

.field public N:LX/6aU;

.field public O:LX/67t;

.field public a:F

.field public b:F

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:LX/67n;

.field public final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/67n;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/67m;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/3BW;

.field public final k:LX/31h;

.field public final l:LX/68m;

.field public m:LX/67y;

.field public n:LX/67z;

.field public o:LX/6aR;

.field public p:LX/6ad;

.field public q:LX/67x;

.field public r:LX/67v;

.field public s:LX/D1H;

.field public t:LX/67w;

.field public u:LX/68v;

.field public v:LX/68o;

.field public w:LX/68p;

.field public x:LX/68X;

.field private y:LX/67m;

.field public final z:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/android/maps/MapView;LX/681;)V
    .locals 4

    .prologue
    .line 1053849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1053850
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/680;->C:[F

    .line 1053851
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/680;->D:Landroid/graphics/Matrix;

    .line 1053852
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/680;->G:Z

    .line 1053853
    const/high16 v0, 0x41980000    # 19.0f

    iput v0, p0, LX/680;->a:F

    .line 1053854
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, LX/680;->b:F

    .line 1053855
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/680;->h:Ljava/util/ArrayList;

    .line 1053856
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/680;->i:Ljava/util/List;

    .line 1053857
    iput-object p1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    .line 1053858
    invoke-virtual {p1}, Lcom/facebook/android/maps/MapView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/680;->z:Landroid/content/Context;

    .line 1053859
    new-instance v0, LX/31h;

    invoke-direct {v0, p0}, LX/31h;-><init>(LX/680;)V

    iput-object v0, p0, LX/680;->k:LX/31h;

    .line 1053860
    new-instance v0, LX/68Q;

    invoke-direct {v0, p0}, LX/68Q;-><init>(LX/680;)V

    iput-object v0, p0, LX/680;->E:LX/68Q;

    .line 1053861
    iget-object v0, p0, LX/680;->z:Landroid/content/Context;

    invoke-static {v0}, LX/68I;->a(Landroid/content/Context;)I

    .line 1053862
    iget-object v0, p0, LX/680;->z:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 1053863
    const/16 v1, 0x140

    if-lt v0, v1, :cond_2

    const/16 v0, 0x200

    :goto_0
    iput v0, p0, LX/680;->B:I

    .line 1053864
    new-instance v0, LX/68n;

    iget-object v1, p0, LX/680;->z:Landroid/content/Context;

    iget v2, p0, LX/680;->B:I

    iget v3, p0, LX/680;->B:I

    invoke-direct {v0, v1, v2, v3}, LX/68n;-><init>(Landroid/content/Context;II)V

    .line 1053865
    new-instance v1, LX/68m;

    invoke-direct {v1, p0, v0}, LX/68m;-><init>(LX/680;LX/68n;)V

    invoke-virtual {p0, v1}, LX/680;->a(LX/67m;)LX/67m;

    move-result-object v0

    check-cast v0, LX/68m;

    iput-object v0, p0, LX/680;->l:LX/68m;

    .line 1053866
    new-instance v0, LX/3BW;

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v1}, Lcom/facebook/android/maps/MapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3BW;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/680;->j:LX/3BW;

    .line 1053867
    iget-object v0, p0, LX/680;->j:LX/3BW;

    new-instance v1, LX/67u;

    invoke-direct {v1, p0}, LX/67u;-><init>(LX/680;)V

    .line 1053868
    iput-object v1, v0, LX/3BW;->f:LX/67t;

    .line 1053869
    iget-object v2, v0, LX/3BW;->f:LX/67t;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/3BW;->d:Landroid/location/Location;

    if-eqz v2, :cond_0

    iget-boolean v2, v0, LX/3BW;->e:Z

    if-eqz v2, :cond_0

    .line 1053870
    iget-object v2, v0, LX/3BW;->f:LX/67t;

    iget-object v3, v0, LX/3BW;->d:Landroid/location/Location;

    invoke-interface {v2, v3}, LX/67t;->a(Landroid/location/Location;)V

    .line 1053871
    :cond_0
    if-eqz p2, :cond_1

    .line 1053872
    iget-object v0, p0, LX/680;->E:LX/68Q;

    .line 1053873
    iget-boolean v1, p2, LX/681;->b:Z

    move v1, v1

    .line 1053874
    invoke-virtual {v0, v1}, LX/68Q;->a(Z)V

    .line 1053875
    iget-object v0, p0, LX/680;->E:LX/68Q;

    .line 1053876
    iget-boolean v1, p2, LX/681;->d:Z

    move v1, v1

    .line 1053877
    iput-boolean v1, v0, LX/68Q;->b:Z

    .line 1053878
    iget-object v0, p0, LX/680;->E:LX/68Q;

    .line 1053879
    iget-boolean v1, p2, LX/681;->e:Z

    move v1, v1

    .line 1053880
    iput-boolean v1, v0, LX/68Q;->c:Z

    .line 1053881
    iget-object v0, p0, LX/680;->E:LX/68Q;

    .line 1053882
    iget-boolean v1, p2, LX/681;->f:Z

    move v1, v1

    .line 1053883
    iput-boolean v1, v0, LX/68Q;->d:Z

    .line 1053884
    iget-object v0, p0, LX/680;->E:LX/68Q;

    .line 1053885
    iget-boolean v1, p2, LX/681;->i:Z

    move v1, v1

    .line 1053886
    invoke-virtual {v0, v1}, LX/68Q;->f(Z)V

    .line 1053887
    iget-object v0, p0, LX/680;->E:LX/68Q;

    .line 1053888
    iget-boolean v1, p2, LX/681;->j:Z

    move v1, v1

    .line 1053889
    iput-boolean v1, v0, LX/68Q;->e:Z

    .line 1053890
    iget v0, p2, LX/681;->l:F

    move v0, v0

    .line 1053891
    invoke-static {v0}, LX/680;->c(F)F

    move-result v0

    iput v0, p0, LX/680;->a:F

    .line 1053892
    iget v0, p2, LX/681;->k:F

    move v0, v0

    .line 1053893
    invoke-static {v0}, LX/680;->c(F)F

    move-result v0

    iput v0, p0, LX/680;->b:F

    .line 1053894
    iget-object v0, p0, LX/680;->l:LX/68m;

    .line 1053895
    iget v1, p2, LX/681;->c:I

    move v1, v1

    .line 1053896
    iget v2, v0, LX/68m;->D:I

    if-ne v1, v2, :cond_3

    .line 1053897
    :cond_1
    :goto_1
    return-void

    .line 1053898
    :cond_2
    const/16 v0, 0x100

    goto/16 :goto_0

    .line 1053899
    :cond_3
    iput v1, v0, LX/68m;->D:I

    .line 1053900
    if-nez v1, :cond_4

    .line 1053901
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/67m;->a(Z)V

    goto :goto_1

    .line 1053902
    :cond_4
    iget-boolean v2, v0, LX/67m;->i:Z

    move v2, v2

    .line 1053903
    if-nez v2, :cond_5

    .line 1053904
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/67m;->a(Z)V

    .line 1053905
    :cond_5
    iget-object v2, v0, LX/68m;->A:LX/68n;

    .line 1053906
    packed-switch v1, :pswitch_data_0

    .line 1053907
    const/4 v3, 0x0

    iput-object v3, v2, LX/68n;->g:Ljava/lang/String;

    .line 1053908
    :goto_2
    iget-object v2, v0, LX/67m;->e:LX/680;

    invoke-virtual {v2}, LX/680;->t()V

    .line 1053909
    iget-object v2, v0, LX/67m;->e:LX/680;

    .line 1053910
    iget-object v3, v2, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v2, v3

    .line 1053911
    invoke-virtual {v2}, Lcom/facebook/android/maps/MapView;->invalidate()V

    goto :goto_1

    .line 1053912
    :pswitch_0
    const-string v3, "live_maps"

    iput-object v3, v2, LX/68n;->g:Ljava/lang/String;

    goto :goto_2

    .line 1053913
    :pswitch_1
    const-string v3, "crowdsourcing_osm"

    iput-object v3, v2, LX/68n;->g:Ljava/lang/String;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(F)F
    .locals 2

    .prologue
    .line 1053914
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p0, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x41980000    # 19.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private w()I
    .locals 2

    .prologue
    .line 1053915
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v0, v0, Lcom/facebook/android/maps/MapView;->c:I

    iget v1, p0, LX/680;->c:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/680;->e:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private x()I
    .locals 2

    .prologue
    .line 1053916
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v0, v0, Lcom/facebook/android/maps/MapView;->d:I

    iget v1, p0, LX/680;->d:I

    sub-int/2addr v0, v1

    iget v1, p0, LX/680;->f:I

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(LX/67m;)LX/67m;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/67m;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1053917
    iget-object v0, p0, LX/680;->i:Ljava/util/List;

    sget-object v1, LX/67m;->a:Ljava/util/Comparator;

    invoke-static {v0, p1, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 1053918
    if-gtz v0, :cond_0

    .line 1053919
    rsub-int/lit8 v0, v0, -0x1

    .line 1053920
    iget-object v1, p0, LX/680;->i:Ljava/util/List;

    invoke-interface {v1, v0, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1053921
    invoke-virtual {p1}, LX/67m;->b()V

    .line 1053922
    invoke-virtual {p0}, LX/680;->s()V

    .line 1053923
    :cond_0
    return-object p1
.end method

.method public final a(LX/699;)LX/698;
    .locals 1

    .prologue
    .line 1053924
    new-instance v0, LX/698;

    invoke-direct {v0, p0, p1}, LX/698;-><init>(LX/680;LX/699;)V

    invoke-virtual {p0, v0}, LX/680;->a(LX/67m;)LX/67m;

    move-result-object v0

    check-cast v0, LX/698;

    .line 1053925
    iput-object p0, v0, LX/698;->H:LX/67o;

    .line 1053926
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1053927
    iget-object v0, p0, LX/680;->H:LX/68u;

    if-eqz v0, :cond_0

    .line 1053928
    iget-object v0, p0, LX/680;->H:LX/68u;

    invoke-virtual {v0}, LX/68u;->g()V

    .line 1053929
    :cond_0
    iget-object v0, p0, LX/680;->I:LX/68u;

    if-eqz v0, :cond_1

    .line 1053930
    iget-object v0, p0, LX/680;->I:LX/68u;

    invoke-virtual {v0}, LX/68u;->g()V

    .line 1053931
    :cond_1
    iget-object v0, p0, LX/680;->J:LX/68u;

    if-eqz v0, :cond_2

    .line 1053932
    iget-object v0, p0, LX/680;->J:LX/68u;

    invoke-virtual {v0}, LX/68u;->g()V

    .line 1053933
    :cond_2
    iget-object v0, p0, LX/680;->K:LX/68u;

    if-eqz v0, :cond_3

    .line 1053934
    iget-object v0, p0, LX/680;->K:LX/68u;

    invoke-virtual {v0}, LX/68u;->g()V

    .line 1053935
    :cond_3
    return-void
.end method

.method public final a(IIII)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 1053936
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v2, v1, Lcom/facebook/android/maps/MapView;->m:D

    iget v1, p0, LX/680;->c:I

    iget v4, p0, LX/680;->e:I

    sub-int/2addr v1, v4

    sub-int v4, p1, p3

    sub-int/2addr v1, v4

    int-to-long v4, v1

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v6, v1, Lcom/facebook/android/maps/MapView;->r:J

    shl-long/2addr v6, v10

    div-long/2addr v4, v6

    long-to-double v4, v4

    add-double/2addr v2, v4

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v4, v1, Lcom/facebook/android/maps/MapView;->n:D

    iget v1, p0, LX/680;->d:I

    iget v6, p0, LX/680;->f:I

    sub-int/2addr v1, v6

    sub-int v6, p2, p4

    sub-int/2addr v1, v6

    int-to-long v6, v1

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v8, v1, Lcom/facebook/android/maps/MapView;->r:J

    shl-long/2addr v8, v10

    div-long/2addr v6, v8

    long-to-double v6, v6

    add-double/2addr v4, v6

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/MapView;->a(DD)V

    .line 1053937
    iput p1, p0, LX/680;->c:I

    .line 1053938
    iput p2, p0, LX/680;->d:I

    .line 1053939
    iput p3, p0, LX/680;->e:I

    .line 1053940
    iput p4, p0, LX/680;->f:I

    .line 1053941
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->requestLayout()V

    .line 1053942
    invoke-virtual {p0}, LX/680;->s()V

    .line 1053943
    return-void
.end method

.method public final a(LX/67d;)V
    .locals 2

    .prologue
    .line 1054061
    const/16 v0, 0x5dc

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/680;->a(LX/67d;ILX/6aX;)V

    .line 1054062
    return-void
.end method

.method public final a(LX/67d;ILX/6aX;)V
    .locals 12

    .prologue
    .line 1053944
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-boolean v0, v0, Lcom/facebook/android/maps/MapView;->q:Z

    if-eqz v0, :cond_1

    .line 1053945
    :cond_0
    :goto_0
    return-void

    .line 1053946
    :cond_1
    if-eqz p2, :cond_2

    .line 1053947
    iget-object v0, p0, LX/680;->l:LX/68m;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/68P;->b(Z)V

    .line 1053948
    :cond_2
    invoke-virtual {p0}, LX/680;->a()V

    .line 1053949
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/680;->G:Z

    .line 1053950
    invoke-virtual {p0}, LX/680;->k()F

    move-result v6

    .line 1053951
    invoke-virtual {p0}, LX/680;->l()F

    move-result v7

    .line 1053952
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getZoom()F

    move-result v0

    .line 1053953
    iput v6, p0, LX/680;->L:F

    .line 1053954
    iput v7, p0, LX/680;->M:F

    .line 1053955
    iget v1, p1, LX/67d;->b:F

    const/high16 v2, -0x31000000

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_d

    .line 1053956
    iget v0, p1, LX/67d;->b:F

    .line 1053957
    :cond_3
    :goto_1
    iget v1, p0, LX/680;->b:F

    iget v2, p0, LX/680;->a:F

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 1053958
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v4, v0, Lcom/facebook/android/maps/MapView;->m:D

    .line 1053959
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v2, v0, Lcom/facebook/android/maps/MapView;->n:D

    .line 1053960
    iget-object v0, p1, LX/67d;->a:Lcom/facebook/android/maps/model/LatLng;

    if-nez v0, :cond_4

    iget-object v0, p1, LX/67d;->i:LX/697;

    if-eqz v0, :cond_16

    .line 1053961
    :cond_4
    iget-object v0, p1, LX/67d;->a:Lcom/facebook/android/maps/model/LatLng;

    if-eqz v0, :cond_15

    iget-object v0, p1, LX/67d;->a:Lcom/facebook/android/maps/model/LatLng;

    .line 1053962
    :goto_2
    iget-wide v2, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v2, v3}, LX/31h;->d(D)F

    move-result v1

    float-to-double v2, v1

    .line 1053963
    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v0, v1}, LX/31h;->b(D)F

    move-result v0

    float-to-double v0, v0

    .line 1053964
    iget-object v4, p0, LX/680;->C:[F

    const/4 v5, 0x0

    iget-object v9, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v9, v9, Lcom/facebook/android/maps/MapView;->e:F

    sub-float/2addr v9, v6

    aput v9, v4, v5

    .line 1053965
    iget-object v4, p0, LX/680;->C:[F

    const/4 v5, 0x1

    iget-object v9, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v9, v9, Lcom/facebook/android/maps/MapView;->f:F

    sub-float/2addr v9, v7

    aput v9, v4, v5

    .line 1053966
    iget-object v4, p0, LX/680;->C:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_5

    iget-object v4, p0, LX/680;->C:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_6

    .line 1053967
    :cond_5
    const/4 v4, 0x1

    float-to-int v5, v8

    shl-int/2addr v4, v5

    iget v5, p0, LX/680;->B:I

    mul-int/2addr v4, v5

    .line 1053968
    const/high16 v5, 0x3f800000    # 1.0f

    rem-float v5, v8, v5

    const/high16 v9, 0x3f800000    # 1.0f

    add-float/2addr v5, v9

    .line 1053969
    iget-object v9, p0, LX/680;->D:Landroid/graphics/Matrix;

    invoke-virtual {v9, v5, v5}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1053970
    iget-object v5, p0, LX/680;->D:Landroid/graphics/Matrix;

    iget-object v9, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v9, v9, Lcom/facebook/android/maps/MapView;->j:F

    invoke-virtual {v5, v9}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1053971
    iget-object v5, p0, LX/680;->D:Landroid/graphics/Matrix;

    iget-object v9, p0, LX/680;->D:Landroid/graphics/Matrix;

    invoke-virtual {v5, v9}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1053972
    iget-object v5, p0, LX/680;->D:Landroid/graphics/Matrix;

    iget-object v9, p0, LX/680;->C:[F

    invoke-virtual {v5, v9}, Landroid/graphics/Matrix;->mapVectors([F)V

    .line 1053973
    iget-object v5, p0, LX/680;->C:[F

    const/4 v9, 0x0

    aget v5, v5, v9

    int-to-float v9, v4

    div-float/2addr v5, v9

    float-to-double v10, v5

    add-double/2addr v2, v10

    .line 1053974
    iget-object v5, p0, LX/680;->C:[F

    const/4 v9, 0x1

    aget v5, v5, v9

    int-to-float v4, v4

    div-float v4, v5, v4

    float-to-double v4, v4

    add-double/2addr v0, v4

    .line 1053975
    :cond_6
    :goto_3
    iget-object v4, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v4, v4, Lcom/facebook/android/maps/MapView;->j:F

    .line 1053976
    iget v5, p1, LX/67d;->h:F

    const/high16 v9, -0x31000000

    cmpl-float v5, v5, v9

    if-eqz v5, :cond_7

    .line 1053977
    iget v4, p1, LX/67d;->h:F

    const/high16 v5, 0x43b40000    # 360.0f

    rem-float/2addr v4, v5

    .line 1053978
    iget-object v5, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v5, v5, Lcom/facebook/android/maps/MapView;->j:F

    sub-float/2addr v5, v4

    const/high16 v9, 0x43340000    # 180.0f

    cmpl-float v5, v5, v9

    if-lez v5, :cond_1a

    .line 1053979
    const/high16 v5, 0x43b40000    # 360.0f

    add-float/2addr v4, v5

    .line 1053980
    :cond_7
    :goto_4
    invoke-static {v2, v3}, Lcom/facebook/android/maps/MapView;->a(D)D

    move-result-wide v2

    .line 1053981
    iget-object v5, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    const/4 v9, 0x1

    float-to-int v10, v8

    shl-int/2addr v9, v10

    iget v10, p0, LX/680;->B:I

    mul-int/2addr v9, v10

    int-to-long v10, v9

    invoke-virtual {v5, v0, v1, v10, v11}, Lcom/facebook/android/maps/MapView;->a(DJ)D

    move-result-wide v10

    .line 1053982
    if-gtz p2, :cond_1b

    .line 1053983
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getZoom()F

    move-result v0

    cmpl-float v0, v8, v0

    if-eqz v0, :cond_8

    .line 1053984
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v1, p0, LX/680;->L:F

    iget v5, p0, LX/680;->M:F

    invoke-virtual {v0, v8, v1, v5}, Lcom/facebook/android/maps/MapView;->c(FFF)Z

    .line 1053985
    :cond_8
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v0, v0, Lcom/facebook/android/maps/MapView;->m:D

    cmpl-double v0, v2, v0

    if-nez v0, :cond_9

    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v0, v0, Lcom/facebook/android/maps/MapView;->n:D

    cmpl-double v0, v10, v0

    if-eqz v0, :cond_a

    .line 1053986
    :cond_9
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, v2, v3, v10, v11}, Lcom/facebook/android/maps/MapView;->a(DD)V

    .line 1053987
    :cond_a
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v0, v0, Lcom/facebook/android/maps/MapView;->j:F

    cmpl-float v0, v4, v0

    if-eqz v0, :cond_b

    .line 1053988
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, v4, v6, v7}, Lcom/facebook/android/maps/MapView;->d(FFF)V

    .line 1053989
    :cond_b
    invoke-virtual {p0}, LX/680;->s()V

    .line 1053990
    invoke-virtual {p0}, LX/680;->q()V

    .line 1053991
    :cond_c
    :goto_5
    iget-object v0, p0, LX/680;->H:LX/68u;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/680;->I:LX/68u;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/680;->J:LX/68u;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/680;->K:LX/68u;

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    .line 1053992
    const/4 v0, 0x0

    iput-object v0, p0, LX/680;->F:LX/6aX;

    .line 1053993
    invoke-virtual {p3}, LX/6aX;->b()V

    goto/16 :goto_0

    .line 1053994
    :cond_d
    iget v1, p1, LX/67d;->c:F

    const/high16 v2, -0x31000000

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_f

    .line 1053995
    iget v1, p1, LX/67d;->c:F

    add-float/2addr v0, v1

    .line 1053996
    iget v1, p1, LX/67d;->d:F

    const/high16 v2, -0x31000000

    cmpl-float v1, v1, v2

    if-nez v1, :cond_e

    iget v1, p1, LX/67d;->e:F

    const/high16 v2, -0x31000000

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_3

    .line 1053997
    :cond_e
    iget v1, p1, LX/67d;->d:F

    iput v1, p0, LX/680;->L:F

    .line 1053998
    iget v1, p1, LX/67d;->e:F

    iput v1, p0, LX/680;->M:F

    goto/16 :goto_1

    .line 1053999
    :cond_f
    iget-object v1, p1, LX/67d;->i:LX/697;

    if-eqz v1, :cond_3

    .line 1054000
    iget-object v2, p1, LX/67d;->i:LX/697;

    .line 1054001
    iget v0, p1, LX/67d;->j:I

    if-lez v0, :cond_10

    iget v0, p1, LX/67d;->j:I

    .line 1054002
    :goto_6
    iget v1, p1, LX/67d;->k:I

    if-lez v1, :cond_11

    iget v1, p1, LX/67d;->k:I

    .line 1054003
    :goto_7
    if-nez v0, :cond_12

    if-nez v1, :cond_12

    .line 1054004
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error using newLatLngBounds(LatLngBounds, int): Map size can\'t be 0. Most likely, layout has not yet occured for the map view.  Either wait until layout has occurred or use newLatLngBounds(LatLngBounds, int, int, int) which allows you to specify the map\'s dimensions."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1054005
    :cond_10
    invoke-direct {p0}, LX/680;->w()I

    move-result v0

    goto :goto_6

    .line 1054006
    :cond_11
    invoke-direct {p0}, LX/680;->x()I

    move-result v1

    goto :goto_7

    .line 1054007
    :cond_12
    iget v3, p1, LX/67d;->l:I

    mul-int/lit8 v3, v3, 0x2

    .line 1054008
    add-int v4, v0, v3

    invoke-direct {p0}, LX/680;->w()I

    move-result v5

    if-le v4, v5, :cond_13

    .line 1054009
    invoke-direct {p0}, LX/680;->w()I

    move-result v0

    sub-int/2addr v0, v3

    .line 1054010
    :cond_13
    add-int v4, v1, v3

    invoke-direct {p0}, LX/680;->x()I

    move-result v5

    if-le v4, v5, :cond_14

    .line 1054011
    invoke-direct {p0}, LX/680;->x()I

    move-result v1

    sub-int/2addr v1, v3

    .line 1054012
    :cond_14
    const/4 v3, 0x0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1054013
    const/4 v3, 0x0

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1054014
    iget-object v3, v2, LX/697;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v3, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v4, v5}, LX/31h;->d(D)F

    move-result v3

    iget-object v4, v2, LX/697;->c:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v4, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v4, v5}, LX/31h;->d(D)F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 1054015
    iget-object v4, v2, LX/697;->c:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v4, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v4, v5}, LX/31h;->b(D)F

    move-result v4

    iget-object v2, v2, LX/697;->b:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v8, v2, Lcom/facebook/android/maps/model/LatLng;->a:D

    invoke-static {v8, v9}, LX/31h;->b(D)F

    move-result v2

    sub-float v2, v4, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 1054016
    int-to-float v0, v0

    div-float/2addr v0, v3

    iget v3, p0, LX/680;->B:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sget-wide v8, Lcom/facebook/android/maps/MapView;->a:D

    div-double/2addr v4, v8

    double-to-float v0, v4

    .line 1054017
    int-to-float v1, v1

    div-float/2addr v1, v2

    iget v2, p0, LX/680;->B:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    sget-wide v4, Lcom/facebook/android/maps/MapView;->a:D

    div-double/2addr v2, v4

    double-to-float v1, v2

    .line 1054018
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto/16 :goto_1

    .line 1054019
    :cond_15
    iget-object v0, p1, LX/67d;->i:LX/697;

    invoke-virtual {v0}, LX/697;->b()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    goto/16 :goto_2

    .line 1054020
    :cond_16
    iget v0, p1, LX/67d;->f:F

    const/high16 v1, -0x31000000

    cmpl-float v0, v0, v1

    if-nez v0, :cond_17

    iget v0, p1, LX/67d;->g:F

    const/high16 v1, -0x31000000

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_25

    .line 1054021
    :cond_17
    iget v0, p1, LX/67d;->f:F

    const/high16 v1, -0x31000000

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_18

    iget v0, p1, LX/67d;->f:F

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v10, v1, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v1, v10

    div-float/2addr v0, v1

    :goto_8
    float-to-double v0, v0

    add-double/2addr v4, v0

    .line 1054022
    iget v0, p1, LX/67d;->g:F

    const/high16 v1, -0x31000000

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_19

    iget v0, p1, LX/67d;->g:F

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v10, v1, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v1, v10

    div-float/2addr v0, v1

    :goto_9
    float-to-double v0, v0

    add-double/2addr v0, v2

    move-wide v2, v4

    goto/16 :goto_3

    .line 1054023
    :cond_18
    const/4 v0, 0x0

    goto :goto_8

    .line 1054024
    :cond_19
    const/4 v0, 0x0

    goto :goto_9

    .line 1054025
    :cond_1a
    iget-object v5, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v5, v5, Lcom/facebook/android/maps/MapView;->j:F

    sub-float v5, v4, v5

    const/high16 v9, 0x43340000    # 180.0f

    cmpl-float v5, v5, v9

    if-lez v5, :cond_7

    .line 1054026
    const/high16 v5, 0x43b40000    # 360.0f

    sub-float/2addr v4, v5

    goto/16 :goto_4

    .line 1054027
    :cond_1b
    iput-object p3, p0, LX/680;->F:LX/6aX;

    .line 1054028
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getZoom()F

    move-result v0

    cmpl-float v0, v8, v0

    if-eqz v0, :cond_1c

    .line 1054029
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getZoom()F

    move-result v0

    invoke-static {v0, v8}, LX/68u;->a(FF)LX/68u;

    move-result-object v0

    iput-object v0, p0, LX/680;->J:LX/68u;

    .line 1054030
    iget-object v0, p0, LX/680;->J:LX/68u;

    invoke-virtual {v0, p0}, LX/68u;->a(LX/67p;)V

    .line 1054031
    iget-object v0, p0, LX/680;->J:LX/68u;

    invoke-virtual {v0, p0}, LX/68u;->a(LX/67q;)V

    .line 1054032
    iget-object v0, p0, LX/680;->J:LX/68u;

    int-to-long v6, p2

    invoke-virtual {v0, v6, v7}, LX/68u;->a(J)LX/68u;

    .line 1054033
    :cond_1c
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v0, v0, Lcom/facebook/android/maps/MapView;->m:D

    cmpl-double v0, v2, v0

    if-eqz v0, :cond_1d

    .line 1054034
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v0, v0, Lcom/facebook/android/maps/MapView;->m:D

    sub-double v0, v2, v0

    .line 1054035
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpl-double v5, v0, v6

    if-lez v5, :cond_23

    .line 1054036
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-double v0, v2, v0

    .line 1054037
    :goto_a
    iget-object v2, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v2, v2, Lcom/facebook/android/maps/MapView;->m:D

    double-to-float v2, v2

    double-to-float v0, v0

    invoke-static {v2, v0}, LX/68u;->a(FF)LX/68u;

    move-result-object v0

    iput-object v0, p0, LX/680;->H:LX/68u;

    .line 1054038
    iget-object v0, p0, LX/680;->H:LX/68u;

    invoke-virtual {v0, p0}, LX/68u;->a(LX/67p;)V

    .line 1054039
    iget-object v0, p0, LX/680;->H:LX/68u;

    invoke-virtual {v0, p0}, LX/68u;->a(LX/67q;)V

    .line 1054040
    iget-object v0, p0, LX/680;->H:LX/68u;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, LX/68u;->a(J)LX/68u;

    .line 1054041
    :cond_1d
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v0, v0, Lcom/facebook/android/maps/MapView;->n:D

    cmpl-double v0, v10, v0

    if-eqz v0, :cond_1e

    .line 1054042
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v0, v0, Lcom/facebook/android/maps/MapView;->n:D

    double-to-float v0, v0

    double-to-float v1, v10

    invoke-static {v0, v1}, LX/68u;->a(FF)LX/68u;

    move-result-object v0

    iput-object v0, p0, LX/680;->I:LX/68u;

    .line 1054043
    iget-object v0, p0, LX/680;->I:LX/68u;

    invoke-virtual {v0, p0}, LX/68u;->a(LX/67p;)V

    .line 1054044
    iget-object v0, p0, LX/680;->I:LX/68u;

    invoke-virtual {v0, p0}, LX/68u;->a(LX/67q;)V

    .line 1054045
    iget-object v0, p0, LX/680;->I:LX/68u;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, LX/68u;->a(J)LX/68u;

    .line 1054046
    :cond_1e
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v0, v0, Lcom/facebook/android/maps/MapView;->j:F

    cmpl-float v0, v4, v0

    if-eqz v0, :cond_1f

    .line 1054047
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v0, v0, Lcom/facebook/android/maps/MapView;->j:F

    invoke-static {v0, v4}, LX/68u;->a(FF)LX/68u;

    move-result-object v0

    iput-object v0, p0, LX/680;->K:LX/68u;

    .line 1054048
    iget-object v0, p0, LX/680;->K:LX/68u;

    invoke-virtual {v0, p0}, LX/68u;->a(LX/67p;)V

    .line 1054049
    iget-object v0, p0, LX/680;->K:LX/68u;

    invoke-virtual {v0, p0}, LX/68u;->a(LX/67q;)V

    .line 1054050
    iget-object v0, p0, LX/680;->K:LX/68u;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, LX/68u;->a(J)LX/68u;

    .line 1054051
    :cond_1f
    iget-object v0, p0, LX/680;->H:LX/68u;

    if-eqz v0, :cond_20

    .line 1054052
    iget-object v0, p0, LX/680;->H:LX/68u;

    invoke-virtual {v0}, LX/68u;->f()V

    .line 1054053
    :cond_20
    iget-object v0, p0, LX/680;->I:LX/68u;

    if-eqz v0, :cond_21

    .line 1054054
    iget-object v0, p0, LX/680;->I:LX/68u;

    invoke-virtual {v0}, LX/68u;->f()V

    .line 1054055
    :cond_21
    iget-object v0, p0, LX/680;->J:LX/68u;

    if-eqz v0, :cond_22

    .line 1054056
    iget-object v0, p0, LX/680;->J:LX/68u;

    invoke-virtual {v0}, LX/68u;->f()V

    .line 1054057
    :cond_22
    iget-object v0, p0, LX/680;->K:LX/68u;

    if-eqz v0, :cond_c

    .line 1054058
    iget-object v0, p0, LX/680;->K:LX/68u;

    invoke-virtual {v0}, LX/68u;->f()V

    goto/16 :goto_5

    .line 1054059
    :cond_23
    const-wide/high16 v6, -0x4020000000000000L    # -0.5

    cmpg-double v0, v0, v6

    if-gez v0, :cond_24

    .line 1054060
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    goto/16 :goto_a

    :cond_24
    move-wide v0, v2

    goto/16 :goto_a

    :cond_25
    move-wide v0, v2

    move-wide v2, v4

    goto/16 :goto_3
.end method

.method public final a(LX/67w;)V
    .locals 1

    .prologue
    .line 1054120
    iput-object p1, p0, LX/680;->t:LX/67w;

    .line 1054121
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-boolean v0, v0, Lcom/facebook/android/maps/MapView;->b:Z

    if-eqz v0, :cond_0

    .line 1054122
    invoke-virtual {p0}, LX/680;->p()V

    .line 1054123
    :cond_0
    return-void
.end method

.method public final a(LX/68u;)V
    .locals 6

    .prologue
    .line 1054099
    iget-object v0, p0, LX/680;->H:LX/68u;

    if-ne p1, v0, :cond_1

    .line 1054100
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-object v1, p0, LX/680;->H:LX/68u;

    .line 1054101
    iget v2, v1, LX/68u;->C:F

    move v1, v2

    .line 1054102
    float-to-double v2, v1

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v4, v1, Lcom/facebook/android/maps/MapView;->n:D

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/MapView;->a(DD)V

    .line 1054103
    invoke-virtual {p0}, LX/680;->s()V

    .line 1054104
    :cond_0
    :goto_0
    return-void

    .line 1054105
    :cond_1
    iget-object v0, p0, LX/680;->I:LX/68u;

    if-ne p1, v0, :cond_2

    .line 1054106
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v2, v1, Lcom/facebook/android/maps/MapView;->m:D

    iget-object v1, p0, LX/680;->I:LX/68u;

    .line 1054107
    iget v4, v1, LX/68u;->C:F

    move v1, v4

    .line 1054108
    float-to-double v4, v1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/MapView;->a(DD)V

    .line 1054109
    invoke-virtual {p0}, LX/680;->s()V

    goto :goto_0

    .line 1054110
    :cond_2
    iget-object v0, p0, LX/680;->J:LX/68u;

    if-ne p1, v0, :cond_3

    .line 1054111
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    .line 1054112
    iget v1, p1, LX/68u;->C:F

    move v1, v1

    .line 1054113
    iget v2, p0, LX/680;->L:F

    iget v3, p0, LX/680;->M:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/android/maps/MapView;->c(FFF)Z

    .line 1054114
    invoke-virtual {p0}, LX/680;->s()V

    goto :goto_0

    .line 1054115
    :cond_3
    iget-object v0, p0, LX/680;->K:LX/68u;

    if-ne p1, v0, :cond_0

    .line 1054116
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    .line 1054117
    iget v1, p1, LX/68u;->C:F

    move v1, v1

    .line 1054118
    invoke-virtual {p0}, LX/680;->k()F

    move-result v2

    invoke-virtual {p0}, LX/680;->l()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/android/maps/MapView;->d(FFF)V

    .line 1054119
    invoke-virtual {p0}, LX/680;->s()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1054084
    iget-object v0, p0, LX/680;->j:LX/3BW;

    invoke-virtual {v0, p1}, LX/3BW;->a(Z)V

    .line 1054085
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/680;->w:LX/68p;

    if-nez v0, :cond_1

    .line 1054086
    new-instance v0, LX/68p;

    invoke-direct {v0, p0}, LX/68p;-><init>(LX/680;)V

    iput-object v0, p0, LX/680;->w:LX/68p;

    .line 1054087
    iget-object v0, p0, LX/680;->w:LX/68p;

    invoke-virtual {p0, v0}, LX/680;->a(LX/67m;)LX/67m;

    .line 1054088
    iget-object v0, p0, LX/680;->w:LX/68p;

    .line 1054089
    iget-object v1, v0, LX/68p;->r:LX/68u;

    .line 1054090
    iget-boolean p1, v1, LX/68u;->s:Z

    move v1, p1

    .line 1054091
    if-nez v1, :cond_0

    .line 1054092
    iget-object v1, v0, LX/68p;->r:LX/68u;

    invoke-virtual {v1}, LX/68u;->f()V

    .line 1054093
    :cond_0
    :goto_0
    iget-object v0, p0, LX/680;->E:LX/68Q;

    invoke-virtual {v0}, LX/68Q;->e()V

    .line 1054094
    return-void

    .line 1054095
    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, LX/680;->w:LX/68p;

    if-eqz v0, :cond_0

    .line 1054096
    iget-object v0, p0, LX/680;->w:LX/68p;

    invoke-virtual {v0}, LX/68p;->p()V

    .line 1054097
    iget-object v0, p0, LX/680;->w:LX/68p;

    invoke-virtual {p0, v0}, LX/680;->b(LX/67m;)V

    .line 1054098
    const/4 v0, 0x0

    iput-object v0, p0, LX/680;->w:LX/68p;

    goto :goto_0
.end method

.method public final a(LX/698;)Z
    .locals 2

    .prologue
    .line 1054079
    iget-object v0, p0, LX/680;->o:LX/6aR;

    if-eqz v0, :cond_0

    .line 1054080
    iget-object v0, p0, LX/680;->o:LX/6aR;

    .line 1054081
    iget-object v1, v0, LX/6aR;->a:LX/IJK;

    invoke-static {p1}, LX/6ax;->a(LX/698;)LX/6ax;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/IJK;->a(LX/6ax;)V

    .line 1054082
    const/4 v0, 0x1

    .line 1054083
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1054069
    iget-object v0, p0, LX/680;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1054070
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1054071
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67m;

    .line 1054072
    iget v2, v0, LX/67m;->j:I

    move v2, v2

    .line 1054073
    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 1054074
    iget v2, v0, LX/67m;->j:I

    move v0, v2

    .line 1054075
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 1054076
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1054077
    :cond_2
    invoke-virtual {p0}, LX/680;->s()V

    .line 1054078
    return-void
.end method

.method public final b(LX/67d;)V
    .locals 2

    .prologue
    .line 1054067
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/680;->a(LX/67d;ILX/6aX;)V

    .line 1054068
    return-void
.end method

.method public final b(LX/67m;)V
    .locals 1

    .prologue
    .line 1054063
    invoke-virtual {p1}, LX/67m;->d()V

    .line 1054064
    iget-object v0, p0, LX/680;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1054065
    invoke-virtual {p0}, LX/680;->s()V

    .line 1054066
    return-void
.end method

.method public final b(LX/68u;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1053831
    iget-object v0, p0, LX/680;->H:LX/68u;

    if-ne p1, v0, :cond_3

    .line 1053832
    iput-object v1, p0, LX/680;->H:LX/68u;

    .line 1053833
    :cond_0
    :goto_0
    invoke-virtual {p1}, LX/68u;->a()V

    .line 1053834
    iget-boolean v0, p0, LX/680;->G:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/680;->H:LX/68u;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/680;->I:LX/68u;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/680;->J:LX/68u;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/680;->K:LX/68u;

    if-nez v0, :cond_2

    .line 1053835
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/680;->G:Z

    .line 1053836
    iget-object v0, p0, LX/680;->F:LX/6aX;

    if-eqz v0, :cond_1

    .line 1053837
    iget-object v0, p0, LX/680;->F:LX/6aX;

    .line 1053838
    iput-object v1, p0, LX/680;->F:LX/6aX;

    .line 1053839
    invoke-virtual {v0}, LX/6aX;->b()V

    .line 1053840
    :cond_1
    invoke-virtual {p0}, LX/680;->q()V

    .line 1053841
    :cond_2
    return-void

    .line 1053842
    :cond_3
    iget-object v0, p0, LX/680;->I:LX/68u;

    if-ne p1, v0, :cond_4

    .line 1053843
    iput-object v1, p0, LX/680;->I:LX/68u;

    goto :goto_0

    .line 1053844
    :cond_4
    iget-object v0, p0, LX/680;->J:LX/68u;

    if-ne p1, v0, :cond_5

    .line 1053845
    iput-object v1, p0, LX/680;->J:LX/68u;

    goto :goto_0

    .line 1053846
    :cond_5
    iget-object v0, p0, LX/680;->K:LX/68u;

    if-ne p1, v0, :cond_0

    .line 1053847
    iput-object v1, p0, LX/680;->K:LX/68u;

    goto :goto_0
.end method

.method public final b(LX/698;)Z
    .locals 1

    .prologue
    .line 1053848
    iget-object v0, p0, LX/680;->m:LX/67y;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/680;->m:LX/67y;

    invoke-interface {v0, p1}, LX/67y;->a(LX/698;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/692;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1053758
    iget-object v0, p0, LX/680;->C:[F

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v1, v1, Lcom/facebook/android/maps/MapView;->e:F

    invoke-virtual {p0}, LX/680;->k()F

    move-result v2

    sub-float/2addr v1, v2

    aput v1, v0, v3

    .line 1053759
    iget-object v0, p0, LX/680;->C:[F

    iget-object v1, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v1, v1, Lcom/facebook/android/maps/MapView;->f:F

    invoke-virtual {p0}, LX/680;->l()F

    move-result v2

    sub-float/2addr v1, v2

    aput v1, v0, v6

    .line 1053760
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-object v0, v0, Lcom/facebook/android/maps/MapView;->l:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/680;->C:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapVectors([F)V

    .line 1053761
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v0, v0, Lcom/facebook/android/maps/MapView;->m:D

    iget-object v2, p0, LX/680;->C:[F

    aget v2, v2, v3

    iget-object v3, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v4, v3, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v3, v4

    div-float/2addr v2, v3

    float-to-double v2, v2

    sub-double/2addr v0, v2

    .line 1053762
    iget-object v2, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v2, v2, Lcom/facebook/android/maps/MapView;->n:D

    iget-object v4, p0, LX/680;->C:[F

    aget v4, v4, v6

    iget-object v5, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget-wide v6, v5, Lcom/facebook/android/maps/MapView;->r:J

    long-to-float v5, v6

    div-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    .line 1053763
    new-instance v4, LX/692;

    new-instance v5, Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v2, v3}, LX/31h;->a(D)D

    move-result-wide v2

    invoke-static {v0, v1}, LX/31h;->c(D)D

    move-result-wide v0

    invoke-direct {v5, v2, v3, v0, v1}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getZoom()F

    move-result v0

    const/4 v1, 0x0

    iget-object v2, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v2, v2, Lcom/facebook/android/maps/MapView;->j:F

    invoke-direct {v4, v5, v0, v1, v2}, LX/692;-><init>(Lcom/facebook/android/maps/model/LatLng;FFF)V

    return-object v4
.end method

.method public final c(LX/67m;)V
    .locals 1

    .prologue
    .line 1053764
    iget-object v0, p0, LX/680;->y:LX/67m;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/680;->y:LX/67m;

    if-eq v0, p1, :cond_0

    .line 1053765
    iget-object v0, p0, LX/680;->y:LX/67m;

    invoke-virtual {v0}, LX/67m;->e()V

    .line 1053766
    :cond_0
    iput-object p1, p0, LX/680;->y:LX/67m;

    .line 1053767
    return-void
.end method

.method public final c(LX/68u;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1053768
    iget-object v0, p0, LX/680;->H:LX/68u;

    if-ne p1, v0, :cond_3

    .line 1053769
    iput-object v1, p0, LX/680;->H:LX/68u;

    .line 1053770
    :cond_0
    :goto_0
    invoke-virtual {p1}, LX/68u;->a()V

    .line 1053771
    iget-object v0, p0, LX/680;->H:LX/68u;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/680;->I:LX/68u;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/680;->J:LX/68u;

    if-nez v0, :cond_2

    iget-object v0, p0, LX/680;->K:LX/68u;

    if-nez v0, :cond_2

    .line 1053772
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/680;->G:Z

    .line 1053773
    iget-object v0, p0, LX/680;->F:LX/6aX;

    if-eqz v0, :cond_1

    .line 1053774
    iget-object v0, p0, LX/680;->F:LX/6aX;

    .line 1053775
    iput-object v1, p0, LX/680;->F:LX/6aX;

    .line 1053776
    iget-object v1, v0, LX/6aX;->a:LX/6aj;

    invoke-interface {v1}, LX/6aj;->a()V

    .line 1053777
    :cond_1
    invoke-virtual {p0}, LX/680;->q()V

    .line 1053778
    :cond_2
    return-void

    .line 1053779
    :cond_3
    iget-object v0, p0, LX/680;->I:LX/68u;

    if-ne p1, v0, :cond_4

    .line 1053780
    iput-object v1, p0, LX/680;->I:LX/68u;

    goto :goto_0

    .line 1053781
    :cond_4
    iget-object v0, p0, LX/680;->J:LX/68u;

    if-ne p1, v0, :cond_5

    .line 1053782
    iput-object v1, p0, LX/680;->J:LX/68u;

    goto :goto_0

    .line 1053783
    :cond_5
    iget-object v0, p0, LX/680;->K:LX/68u;

    if-ne p1, v0, :cond_0

    .line 1053784
    iput-object v1, p0, LX/680;->K:LX/68u;

    goto :goto_0
.end method

.method public final c(LX/698;)Z
    .locals 1

    .prologue
    .line 1053785
    iget-object v0, p0, LX/680;->n:LX/67z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/680;->n:LX/67z;

    invoke-interface {v0}, LX/67z;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1053786
    iget v0, p0, LX/680;->B:I

    return v0
.end method

.method public final d(LX/698;)V
    .locals 0

    .prologue
    .line 1053787
    invoke-virtual {p0, p1}, LX/680;->b(LX/67m;)V

    .line 1053788
    invoke-virtual {p0, p1}, LX/680;->a(LX/67m;)LX/67m;

    .line 1053789
    return-void
.end method

.method public final e()Landroid/location/Location;
    .locals 1

    .prologue
    .line 1053790
    iget-object v0, p0, LX/680;->j:LX/3BW;

    .line 1053791
    iget-object p0, v0, LX/3BW;->d:Landroid/location/Location;

    move-object v0, p0

    .line 1053792
    return-object v0
.end method

.method public final f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1053793
    iget-object v0, p0, LX/680;->z:Landroid/content/Context;

    return-object v0
.end method

.method public final g()Lcom/facebook/android/maps/MapView;
    .locals 1

    .prologue
    .line 1053794
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    return-object v0
.end method

.method public final h()LX/31h;
    .locals 1

    .prologue
    .line 1053795
    iget-object v0, p0, LX/680;->k:LX/31h;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1053796
    iget-object v0, p0, LX/680;->w:LX/68p;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()F
    .locals 3

    .prologue
    .line 1053797
    iget v0, p0, LX/680;->c:I

    int-to-float v0, v0

    invoke-direct {p0}, LX/680;->w()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final l()F
    .locals 3

    .prologue
    .line 1053798
    iget v0, p0, LX/680;->d:I

    int-to-float v0, v0

    invoke-direct {p0}, LX/680;->x()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1053799
    iget-object v0, p0, LX/680;->t:LX/67w;

    if-eqz v0, :cond_0

    .line 1053800
    iget-object v0, p0, LX/680;->t:LX/67w;

    invoke-interface {v0}, LX/67w;->a()V

    .line 1053801
    const/4 v0, 0x0

    iput-object v0, p0, LX/680;->t:LX/67w;

    .line 1053802
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 3

    .prologue
    .line 1053803
    iget-object v0, p0, LX/680;->g:LX/67n;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/680;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1053804
    :cond_0
    return-void

    .line 1053805
    :cond_1
    invoke-virtual {p0}, LX/680;->c()LX/692;

    move-result-object v1

    .line 1053806
    iget-object v0, p0, LX/680;->g:LX/67n;

    if-eqz v0, :cond_2

    .line 1053807
    iget-object v0, p0, LX/680;->g:LX/67n;

    invoke-interface {v0, v1}, LX/67n;->a(LX/692;)V

    .line 1053808
    :cond_2
    iget-object v0, p0, LX/680;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1053809
    iget-object v0, p0, LX/680;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67n;

    .line 1053810
    invoke-interface {v0, v1}, LX/67n;->a(LX/692;)V

    goto :goto_0
.end method

.method public final r()Z
    .locals 9

    .prologue
    .line 1053811
    iget-object v0, p0, LX/680;->s:LX/D1H;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/680;->s:LX/D1H;

    .line 1053812
    iget-object v1, v0, LX/D1H;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iget-object v1, v1, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    invoke-virtual {v1}, LX/680;->e()Landroid/location/Location;

    move-result-object v1

    .line 1053813
    if-eqz v1, :cond_0

    .line 1053814
    iget-object v2, v0, LX/D1H;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iget-object v2, v2, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->t:LX/680;

    .line 1053815
    iget-object v3, v2, LX/680;->k:LX/31h;

    move-object v2, v3

    .line 1053816
    iget-object v3, v0, LX/D1H;->a:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    new-instance v4, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v4}, LX/31h;->a(Lcom/facebook/android/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v1

    invoke-static {v3, v2, v1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a$redex0(Lcom/facebook/storelocator/StoreLocatorMapDelegate;LX/31h;Landroid/graphics/Point;)V

    .line 1053817
    :cond_0
    const/4 v1, 0x1

    move v0, v1

    .line 1053818
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 1053819
    iget-object v0, p0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->invalidate()V

    .line 1053820
    return-void
.end method

.method public final t()V
    .locals 4

    .prologue
    .line 1053821
    const/4 v0, 0x0

    iget-object v1, p0, LX/680;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 1053822
    iget-object v0, p0, LX/680;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67m;

    .line 1053823
    instance-of v3, v0, LX/68P;

    if-eqz v3, :cond_1

    .line 1053824
    check-cast v0, LX/68P;

    invoke-virtual {v0}, LX/68P;->p()V

    .line 1053825
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1053826
    :cond_1
    instance-of v3, v0, LX/67r;

    if-eqz v3, :cond_0

    .line 1053827
    check-cast v0, LX/67r;

    .line 1053828
    iget-object v3, v0, LX/67r;->F:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1053829
    goto :goto_1

    .line 1053830
    :cond_2
    return-void
.end method
