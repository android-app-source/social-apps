.class public final enum LX/5nf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5nf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5nf;

.field public static final enum EXPOSED:LX/5nf;

.field public static final enum FRIENDS_PRIVACY:LX/5nf;

.field public static final enum HOLDOUT:LX/5nf;

.field public static final enum LEARN_MORE:LX/5nf;

.field public static final enum MORE_OPTIONS:LX/5nf;

.field public static final enum NAVIGATED_BACK:LX/5nf;

.field public static final enum OTHER_PRIVACY:LX/5nf;

.field public static final enum SKIPPED:LX/5nf;

.field public static final enum WIDEST_PRIVACY:LX/5nf;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1004488
    new-instance v0, LX/5nf;

    const-string v1, "EXPOSED"

    const-string v2, "exposure"

    invoke-direct {v0, v1, v4, v2}, LX/5nf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nf;->EXPOSED:LX/5nf;

    .line 1004489
    new-instance v0, LX/5nf;

    const-string v1, "HOLDOUT"

    const-string v2, "holdout"

    invoke-direct {v0, v1, v5, v2}, LX/5nf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nf;->HOLDOUT:LX/5nf;

    .line 1004490
    new-instance v0, LX/5nf;

    const-string v1, "SKIPPED"

    const-string v2, "dismiss"

    invoke-direct {v0, v1, v6, v2}, LX/5nf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nf;->SKIPPED:LX/5nf;

    .line 1004491
    new-instance v0, LX/5nf;

    const-string v1, "LEARN_MORE"

    const-string v2, "learn_more"

    invoke-direct {v0, v1, v7, v2}, LX/5nf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nf;->LEARN_MORE:LX/5nf;

    .line 1004492
    new-instance v0, LX/5nf;

    const-string v1, "WIDEST_PRIVACY"

    const-string v2, "set_widest"

    invoke-direct {v0, v1, v8, v2}, LX/5nf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nf;->WIDEST_PRIVACY:LX/5nf;

    .line 1004493
    new-instance v0, LX/5nf;

    const-string v1, "OTHER_PRIVACY"

    const/4 v2, 0x5

    const-string v3, "set_other_privacy"

    invoke-direct {v0, v1, v2, v3}, LX/5nf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nf;->OTHER_PRIVACY:LX/5nf;

    .line 1004494
    new-instance v0, LX/5nf;

    const-string v1, "FRIENDS_PRIVACY"

    const/4 v2, 0x6

    const-string v3, "friends"

    invoke-direct {v0, v1, v2, v3}, LX/5nf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nf;->FRIENDS_PRIVACY:LX/5nf;

    .line 1004495
    new-instance v0, LX/5nf;

    const-string v1, "MORE_OPTIONS"

    const/4 v2, 0x7

    const-string v3, "more_options"

    invoke-direct {v0, v1, v2, v3}, LX/5nf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nf;->MORE_OPTIONS:LX/5nf;

    .line 1004496
    new-instance v0, LX/5nf;

    const-string v1, "NAVIGATED_BACK"

    const/16 v2, 0x8

    const-string v3, "dismiss_back"

    invoke-direct {v0, v1, v2, v3}, LX/5nf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5nf;->NAVIGATED_BACK:LX/5nf;

    .line 1004497
    const/16 v0, 0x9

    new-array v0, v0, [LX/5nf;

    sget-object v1, LX/5nf;->EXPOSED:LX/5nf;

    aput-object v1, v0, v4

    sget-object v1, LX/5nf;->HOLDOUT:LX/5nf;

    aput-object v1, v0, v5

    sget-object v1, LX/5nf;->SKIPPED:LX/5nf;

    aput-object v1, v0, v6

    sget-object v1, LX/5nf;->LEARN_MORE:LX/5nf;

    aput-object v1, v0, v7

    sget-object v1, LX/5nf;->WIDEST_PRIVACY:LX/5nf;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5nf;->OTHER_PRIVACY:LX/5nf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5nf;->FRIENDS_PRIVACY:LX/5nf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5nf;->MORE_OPTIONS:LX/5nf;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5nf;->NAVIGATED_BACK:LX/5nf;

    aput-object v2, v0, v1

    sput-object v0, LX/5nf;->$VALUES:[LX/5nf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1004498
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1004499
    iput-object p3, p0, LX/5nf;->eventName:Ljava/lang/String;

    .line 1004500
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5nf;
    .locals 1

    .prologue
    .line 1004501
    const-class v0, LX/5nf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5nf;

    return-object v0
.end method

.method public static values()[LX/5nf;
    .locals 1

    .prologue
    .line 1004486
    sget-object v0, LX/5nf;->$VALUES:[LX/5nf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5nf;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004487
    iget-object v0, p0, LX/5nf;->eventName:Ljava/lang/String;

    return-object v0
.end method
