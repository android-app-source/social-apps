.class public LX/5oW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/5oW;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1007044
    const-class v0, LX/5oW;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/5oW;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1007027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1007028
    iput-object p1, p0, LX/5oW;->b:LX/0Zb;

    .line 1007029
    iput-object p2, p0, LX/5oW;->c:LX/03V;

    .line 1007030
    return-void
.end method

.method public static a(LX/0QB;)LX/5oW;
    .locals 5

    .prologue
    .line 1007031
    sget-object v0, LX/5oW;->d:LX/5oW;

    if-nez v0, :cond_1

    .line 1007032
    const-class v1, LX/5oW;

    monitor-enter v1

    .line 1007033
    :try_start_0
    sget-object v0, LX/5oW;->d:LX/5oW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1007034
    if-eqz v2, :cond_0

    .line 1007035
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1007036
    new-instance p0, LX/5oW;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/5oW;-><init>(LX/0Zb;LX/03V;)V

    .line 1007037
    move-object v0, p0

    .line 1007038
    sput-object v0, LX/5oW;->d:LX/5oW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1007039
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1007040
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1007041
    :cond_1
    sget-object v0, LX/5oW;->d:LX/5oW;

    return-object v0

    .line 1007042
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1007043
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/5oW;LX/5oV;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V
    .locals 2

    .prologue
    .line 1007024
    invoke-static {p0, p1, p2}, LX/5oW;->b(LX/5oW;LX/5oU;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1007025
    iget-object v1, p0, LX/5oW;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1007026
    return-void
.end method

.method public static b(LX/5oW;LX/5oU;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 7

    .prologue
    .line 1007006
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "inline_composer_prompt_event"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "action"

    invoke-interface {p1}, LX/5oU;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "prompt_id"

    iget-object v2, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "prompt_session_id"

    iget-object v2, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "prompt_type"

    iget-object v2, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1007007
    iget-object v1, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1007008
    const-string v1, "tracking_string"

    iget-object v2, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1007009
    :cond_0
    iget-object v1, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1007010
    const-string v1, "composer_session_id"

    iget-object v2, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1007011
    :cond_1
    sget-object v1, LX/5oV;->IMPRESSION:LX/5oV;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1007012
    const-string v1, "prompt_state"

    iget-object v2, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptViewState:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1007013
    :cond_2
    iget-object v1, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    .line 1007014
    iget-object v2, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    .line 1007015
    iget-object v3, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    .line 1007016
    if-nez v1, :cond_3

    if-nez v2, :cond_3

    if-eqz v3, :cond_4

    .line 1007017
    :cond_3
    iget-object v3, p0, LX/5oW;->c:LX/03V;

    sget-object v4, LX/5oW;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "PromptAnalytics validation failed when logging "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/5oU;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": Missing required field -  prompt_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " prompt_type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " tracking_string="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1007018
    :cond_4
    if-nez v1, :cond_6

    if-nez v2, :cond_6

    .line 1007019
    iget-object v1, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 1007020
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->CLIPBOARD:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPromptType;->SOUVENIR:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPromptType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_5
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1007021
    if-eqz v1, :cond_6

    iget-object v1, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1007022
    iget-object v1, p0, LX/5oW;->c:LX/03V;

    sget-object v2, LX/5oW;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PromptAnalytics validation failed when logging "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/5oU;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": no \':\' in promptId prompt_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " prompt_type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " tracking_string="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->trackingString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1007023
    :cond_6
    return-object v0

    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Lcom/facebook/productionprompts/logging/PromptAnalytics;)V
    .locals 1

    .prologue
    .line 1007004
    sget-object v0, LX/5oV;->OPEN_SELECTING_CONTENT:LX/5oV;

    invoke-static {p0, v0, p1}, LX/5oW;->a(LX/5oW;LX/5oV;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 1007005
    return-void
.end method
