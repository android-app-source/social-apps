.class public LX/6Yi;
.super LX/6Ya;
.source ""


# instance fields
.field private final c:LX/7Y8;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/4g9;


# direct methods
.method public constructor <init>(LX/7Y8;LX/0Or;LX/4g9;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroBroadcastManager;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroAnalyticsLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1110439
    invoke-direct {p0}, LX/6Ya;-><init>()V

    .line 1110440
    iput-object p1, p0, LX/6Yi;->c:LX/7Y8;

    .line 1110441
    iput-object p2, p0, LX/6Yi;->d:LX/0Or;

    .line 1110442
    iput-object p3, p0, LX/6Yi;->e:LX/4g9;

    .line 1110443
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1110444
    iget-object v0, p0, LX/6Yi;->e:LX/4g9;

    sget-object v1, LX/4g6;->g:LX/4g6;

    invoke-virtual {p0}, LX/6Ya;->e()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4g9;->a(LX/4g5;Ljava/util/Map;)V

    .line 1110445
    iget-object v0, p0, LX/6Yi;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1110446
    iget-object v0, p0, LX/6Yi;->c:LX/7Y8;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "zero_token_request_reason"

    sget-object v3, LX/32P;->UPSELL:LX/32P;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7Y8;->a(Landroid/content/Intent;)V

    .line 1110447
    :cond_0
    iget-object v0, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110448
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->v:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    move-object v0, v1

    .line 1110449
    invoke-virtual {v0}, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->c()Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;

    move-result-object v0

    .line 1110450
    invoke-virtual {p0}, LX/6Ya;->f()LX/6Y5;

    move-result-object v1

    .line 1110451
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1110452
    invoke-virtual {v1, v2}, LX/6Y5;->a(Ljava/lang/String;)LX/6Y5;

    move-result-object v1

    .line 1110453
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1110454
    iput-object v2, v1, LX/6Y5;->c:Ljava/lang/String;

    .line 1110455
    move-object v1, v1

    .line 1110456
    iget-object v2, v0, Lcom/facebook/iorg/common/upsell/server/UpsellDialogScreenContent;->c:Ljava/lang/String;

    move-object v0, v2

    .line 1110457
    invoke-virtual {p0}, LX/6Ya;->d()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/6Y5;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)LX/6Y5;

    move-result-object v0

    .line 1110458
    new-instance v1, LX/6YP;

    invoke-direct {v1, p1}, LX/6YP;-><init>(Landroid/content/Context;)V

    .line 1110459
    invoke-virtual {v1, v0}, LX/6YP;->a(LX/6Y5;)V

    .line 1110460
    return-object v1
.end method
