.class public LX/5OZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 909137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1nq;Landroid/content/Context;I)V
    .locals 2
    .param p2    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 909116
    const/4 v0, 0x0

    .line 909117
    const/4 v1, 0x0

    invoke-static {p0, p1, v1, v0, p2}, LX/5OZ;->a(LX/1nq;Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 909118
    return-void
.end method

.method public static a(LX/1nq;Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 16
    .param p3    # I
        .annotation build Landroid/support/annotation/AttrRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 909138
    sget-object v4, LX/JMZ;->TextStyle:[I

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 909139
    const/16 v5, 0x0

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 909140
    if-lez v5, :cond_0

    .line 909141
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5}, LX/5OZ;->b(LX/1nq;Landroid/content/Context;I)V

    .line 909142
    :cond_0
    const/16 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    .line 909143
    const/16 v6, 0x1

    const/16 v7, 0xf

    invoke-virtual {v4, v6, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    .line 909144
    const/16 v7, 0x7

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    .line 909145
    const/16 v8, 0x8

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v8

    .line 909146
    const/16 v9, 0x9

    const/4 v10, 0x0

    invoke-virtual {v4, v9, v10}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v9

    .line 909147
    const/16 v10, 0xa

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v10

    .line 909148
    const/16 v11, 0x2

    const/4 v12, -0x1

    invoke-virtual {v4, v11, v12}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v11

    .line 909149
    const/16 v12, 0x4

    const/4 v13, 0x0

    invoke-virtual {v4, v12, v13}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v12

    .line 909150
    const/16 v13, 0x6

    const/4 v14, 0x0

    invoke-virtual {v4, v13, v14}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v13

    .line 909151
    const/16 v14, 0x5

    const v15, 0x7fffffff

    invoke-virtual {v4, v14, v15}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v14

    .line 909152
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 909153
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, LX/1nq;->a(Landroid/content/res/ColorStateList;)LX/1nq;

    .line 909154
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LX/1nq;->b(I)LX/1nq;

    .line 909155
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v8, v9, v7}, LX/1nq;->a(FFFI)LX/1nq;

    .line 909156
    const/4 v4, -0x1

    if-eq v11, v4, :cond_1

    .line 909157
    invoke-static {v11}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/1nq;->a(Landroid/graphics/Typeface;)LX/1nq;

    .line 909158
    :goto_0
    if-lez v12, :cond_2

    const/4 v4, 0x4

    if-ge v12, v4, :cond_2

    .line 909159
    invoke-static {}, Landroid/text/TextUtils$TruncateAt;->values()[Landroid/text/TextUtils$TruncateAt;

    move-result-object v4

    add-int/lit8 v5, v12, -0x1

    aget-object v4, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/1nq;->a(Landroid/text/TextUtils$TruncateAt;)LX/1nq;

    .line 909160
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, LX/1nq;->b(Z)LX/1nq;

    .line 909161
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LX/1nq;->f(I)LX/1nq;

    .line 909162
    return-void

    .line 909163
    :cond_1
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/1nq;->a(Landroid/graphics/Typeface;)LX/1nq;

    goto :goto_0

    .line 909164
    :cond_2
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LX/1nq;->a(Landroid/text/TextUtils$TruncateAt;)LX/1nq;

    goto :goto_1
.end method

.method public static b(LX/1nq;Landroid/content/Context;I)V
    .locals 9
    .param p2    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 909119
    sget-object v0, LX/JMZ;->TextAppearance:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 909120
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 909121
    const/16 v2, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 909122
    const/16 v3, 0x4

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 909123
    if-eqz v3, :cond_0

    .line 909124
    const/16 v4, 0x5

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    .line 909125
    const/16 v5, 0x6

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v5

    .line 909126
    const/16 v6, 0x7

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v6

    .line 909127
    invoke-virtual {p0, v6, v4, v5, v3}, LX/1nq;->a(FFFI)LX/1nq;

    .line 909128
    :cond_0
    const/16 v3, 0x2

    invoke-virtual {v0, v3, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 909129
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 909130
    if-eqz v1, :cond_1

    .line 909131
    invoke-virtual {p0, v1}, LX/1nq;->a(Landroid/content/res/ColorStateList;)LX/1nq;

    .line 909132
    :cond_1
    if-eqz v2, :cond_2

    .line 909133
    invoke-virtual {p0, v2}, LX/1nq;->b(I)LX/1nq;

    .line 909134
    :cond_2
    if-eq v3, v8, :cond_3

    .line 909135
    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1nq;->a(Landroid/graphics/Typeface;)LX/1nq;

    .line 909136
    :cond_3
    return-void
.end method
