.class public final LX/6QG;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public l:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

.field public final synthetic m:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1087174
    iput-object p1, p0, LX/6QG;->m:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    .line 1087175
    invoke-direct {p0, p3}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1087176
    iput-object p2, p0, LX/6QG;->l:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    .line 1087177
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1087178
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0x7c43b6ca

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1087179
    iget-object v1, p0, LX/6QG;->l:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    .line 1087180
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->e:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 1087181
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->e:Ljava/util/ArrayList;

    .line 1087182
    :goto_0
    move-object v1, v2

    .line 1087183
    invoke-virtual {p0}, LX/1a1;->d()I

    move-result v2

    .line 1087184
    iget-object v3, p0, LX/6QG;->m:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    iget-object v3, v3, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->h:LX/6QI;

    iget-object v4, p0, LX/6QG;->m:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    iget-object v4, v4, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->f:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1087185
    iget-object v5, v4, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v4, v5

    .line 1087186
    iget-object v5, v4, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1087187
    iget-object v5, p0, LX/6QG;->m:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    iget-object v5, v5, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->f:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    .line 1087188
    iget-object v6, v5, Lcom/facebook/directinstall/intent/DirectInstallAppData;->b:Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;

    move-object v5, v6

    .line 1087189
    iget-object v6, v5, Lcom/facebook/directinstall/intent/DirectInstallAppDescriptor;->e:Ljava/lang/String;

    move-object v5, v6

    .line 1087190
    iget-object v6, p0, LX/6QG;->m:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    iget-object v6, v6, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->g:Ljava/util/Map;

    .line 1087191
    const-string v9, "tap"

    move-object v8, v3

    move-object v10, v4

    move-object v11, v5

    move v12, v2

    move-object v13, v6

    invoke-static/range {v8 .. v13}, LX/6QI;->a(LX/6QI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/Map;)V

    .line 1087192
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, LX/6QG;->l:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    .line 1087193
    iget-object v5, v4, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->b:Landroid/content/Context;

    move-object v4, v5

    .line 1087194
    const-class v5, Lcom/facebook/directinstall/appdetails/ScreenshotFullscreenActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1087195
    const-string v4, "screenshot_url_list"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1087196
    const-string v1, "screenshot_current_position"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1087197
    iget-object v1, p0, LX/6QG;->m:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    iget-object v1, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->f:Lcom/facebook/directinstall/intent/DirectInstallAppData;

    invoke-static {v3, v1}, LX/6Qv;->a(Landroid/content/Intent;Lcom/facebook/directinstall/intent/DirectInstallAppData;)V

    .line 1087198
    iget-object v1, p0, LX/6QG;->m:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    iget-object v1, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->g:Ljava/util/Map;

    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    invoke-static {v3, v1}, LX/6Qv;->a(Landroid/content/Intent;LX/0P1;)V

    .line 1087199
    iget-object v1, p0, LX/6QG;->l:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    .line 1087200
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->c:Lcom/facebook/content/SecureContextHelper;

    move-object v1, v2

    .line 1087201
    iget-object v2, p0, LX/6QG;->l:Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;

    .line 1087202
    iget-object v4, v2, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->b:Landroid/content/Context;

    move-object v2, v4

    .line 1087203
    invoke-interface {v1, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1087204
    const v1, 0x7ee3b060

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1087205
    :cond_0
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->d:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1087206
    const/4 v2, 0x0

    goto :goto_0

    .line 1087207
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->e:Ljava/util/ArrayList;

    .line 1087208
    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v4

    .line 1087209
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    .line 1087210
    iget-object v5, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->e:Ljava/util/ArrayList;

    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6QF;

    .line 1087211
    iget-object v6, v2, LX/6QF;->a:Landroid/net/Uri;

    move-object v2, v6

    .line 1087212
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1087213
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1087214
    :cond_2
    iget-object v2, v1, Lcom/facebook/directinstall/appdetails/ScreenshotItemsAdapter;->e:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method
