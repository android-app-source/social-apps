.class public final LX/5J2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserCreatedRecommendationDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListPendingSlotDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$CommentPlaceInfoPageFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceListUserProfileRecommendationDetailsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 896165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v12, 0x0

    const/4 v2, 0x0

    .line 896138
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 896139
    iget-object v1, p0, LX/5J2;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 896140
    iget-object v3, p0, LX/5J2;->d:LX/0Px;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 896141
    iget-object v5, p0, LX/5J2;->e:LX/0Px;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 896142
    iget-object v6, p0, LX/5J2;->f:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 896143
    iget-object v7, p0, LX/5J2;->g:LX/0Px;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 896144
    iget-object v8, p0, LX/5J2;->h:LX/0Px;

    invoke-static {v0, v8}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 896145
    iget-object v9, p0, LX/5J2;->i:LX/0Px;

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 896146
    iget-object v10, p0, LX/5J2;->j:LX/0Px;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 896147
    const/16 v11, 0xa

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 896148
    invoke-virtual {v0, v12, v1}, LX/186;->b(II)V

    .line 896149
    iget-boolean v1, p0, LX/5J2;->b:Z

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 896150
    const/4 v1, 0x2

    iget-boolean v11, p0, LX/5J2;->c:Z

    invoke-virtual {v0, v1, v11}, LX/186;->a(IZ)V

    .line 896151
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 896152
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 896153
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 896154
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 896155
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 896156
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 896157
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 896158
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 896159
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 896160
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 896161
    invoke-virtual {v1, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 896162
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 896163
    new-instance v1, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;

    invoke-direct {v1, v0}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchProfileRecommendationMutationsModels$CommentFieldsProfileRecommendationMutationModel$AttachmentsModel$TargetModel;-><init>(LX/15i;)V

    .line 896164
    return-object v1
.end method
