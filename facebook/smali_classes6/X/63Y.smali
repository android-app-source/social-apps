.class public LX/63Y;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1043376
    const v0, 0x7f0d0046

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f0d0047

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0d0048

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0d0049

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/63Y;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1043328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1043329
    return-void
.end method

.method public static a(Landroid/view/Menu;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v8, -0x1

    .line 1043330
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043331
    iget v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->r:I

    move v1, v1

    .line 1043332
    if-eq v1, v8, :cond_5

    .line 1043333
    iget v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->r:I

    move v1, v1

    .line 1043334
    :goto_1
    const-string v2, ""

    invoke-interface {p0, v4, v1, v4, v2}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v7

    .line 1043335
    iget v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->l:I

    move v1, v1

    .line 1043336
    if-eqz v1, :cond_2

    .line 1043337
    iget v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->l:I

    move v1, v1

    .line 1043338
    invoke-static {v7, v1}, LX/3rl;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    .line 1043339
    invoke-static {v7}, LX/3rl;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v2

    .line 1043340
    if-eqz v2, :cond_2

    .line 1043341
    instance-of v1, v2, LX/63a;

    if-eqz v1, :cond_1

    move-object v1, v2

    .line 1043342
    check-cast v1, LX/63a;

    .line 1043343
    iget-boolean v5, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    move v5, v5

    .line 1043344
    if-nez v5, :cond_0

    .line 1043345
    iget v5, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    move v5, v5

    .line 1043346
    if-ne v5, v8, :cond_6

    .line 1043347
    :cond_0
    iget v5, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->m:I

    move v5, v5

    .line 1043348
    :goto_2
    invoke-interface {v1, v5}, LX/63a;->setButtonTintColor(I)V

    .line 1043349
    :cond_1
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->k:Ljava/lang/String;

    move-object v1, v1

    .line 1043350
    invoke-virtual {v2, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1043351
    iget-boolean v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->t:Z

    move v1, v1

    .line 1043352
    invoke-virtual {v2, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1043353
    :cond_2
    iget v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v1, v1

    .line 1043354
    if-eq v1, v8, :cond_7

    .line 1043355
    iget v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->h:I

    move v1, v1

    .line 1043356
    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1043357
    :cond_3
    :goto_3
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1043358
    if-eqz v1, :cond_4

    .line 1043359
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->i:Ljava/lang/String;

    move-object v1, v1

    .line 1043360
    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1043361
    :cond_4
    iget v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->s:I

    move v1, v1

    .line 1043362
    invoke-static {v7, v1}, LX/3rl;->a(Landroid/view/MenuItem;I)Z

    .line 1043363
    iget-boolean v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    move v0, v1

    .line 1043364
    invoke-interface {v7, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1043365
    add-int/lit8 v0, v3, 0x1

    .line 1043366
    sget-object v1, LX/63Y;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_8

    move v3, v0

    .line 1043367
    goto :goto_0

    .line 1043368
    :cond_5
    sget-object v1, LX/63Y;->a:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1

    .line 1043369
    :cond_6
    iget v5, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->v:I

    move v5, v5

    .line 1043370
    goto :goto_2

    .line 1043371
    :cond_7
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 1043372
    if-eqz v1, :cond_3

    .line 1043373
    iget-object v1, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->d:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 1043374
    invoke-interface {v7, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_3

    .line 1043375
    :cond_8
    return-void
.end method

.method public static a(Landroid/view/MenuItem;Ljava/util/List;LX/63W;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MenuItem;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;",
            "LX/63W;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 1043303
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 1043304
    invoke-interface {p0}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    move v1, v2

    .line 1043305
    :goto_0
    sget-object v0, LX/63Y;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1043306
    sget-object v0, LX/63Y;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 1043307
    :goto_1
    if-ne v1, v3, :cond_3

    .line 1043308
    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1043309
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043310
    iget v3, v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->r:I

    move v0, v3

    .line 1043311
    if-ne v0, v4, :cond_2

    .line 1043312
    :goto_3
    move v0, v2

    .line 1043313
    if-ltz v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1043314
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 1043315
    invoke-static {p0}, LX/3rl;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, LX/63W;->a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1043316
    const/4 v0, 0x1

    .line 1043317
    :goto_4
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_4

    .line 1043318
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1043319
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move v2, v1

    goto :goto_3

    :cond_4
    move v1, v3

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Ljava/util/List;LX/63W;)V
    .locals 4
    .param p3    # LX/63W;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;",
            "LX/63W;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1043320
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    .line 1043321
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 1043322
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1043323
    invoke-static {v2}, LX/3rl;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v2

    .line 1043324
    if-eqz v2, :cond_0

    .line 1043325
    new-instance v3, LX/63X;

    invoke-direct {v3, p0, p3, p2, v0}, LX/63X;-><init>(LX/63Y;LX/63W;Ljava/util/List;I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1043326
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1043327
    :cond_1
    return-void
.end method
