.class public LX/61m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2MV;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/61m;


# instance fields
.field private final a:LX/2MM;

.field private final b:LX/5Ow;

.field private final c:LX/0Zb;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/5Ow;LX/2MM;LX/0Uh;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1040652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040653
    iput-object p2, p0, LX/61m;->a:LX/2MM;

    .line 1040654
    iput-object p1, p0, LX/61m;->b:LX/5Ow;

    .line 1040655
    iput-object p4, p0, LX/61m;->c:LX/0Zb;

    .line 1040656
    iput-object p3, p0, LX/61m;->d:LX/0Uh;

    .line 1040657
    return-void
.end method

.method public static a(LX/0QB;)LX/61m;
    .locals 7

    .prologue
    .line 1040636
    sget-object v0, LX/61m;->e:LX/61m;

    if-nez v0, :cond_1

    .line 1040637
    const-class v1, LX/61m;

    monitor-enter v1

    .line 1040638
    :try_start_0
    sget-object v0, LX/61m;->e:LX/61m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1040639
    if-eqz v2, :cond_0

    .line 1040640
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1040641
    new-instance p0, LX/61m;

    .line 1040642
    new-instance v4, LX/5Ow;

    invoke-static {v0}, LX/2MW;->a(LX/0QB;)LX/2MY;

    move-result-object v3

    check-cast v3, LX/2MY;

    invoke-direct {v4, v3}, LX/5Ow;-><init>(LX/2MY;)V

    .line 1040643
    move-object v3, v4

    .line 1040644
    check-cast v3, LX/5Ow;

    invoke-static {v0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v4

    check-cast v4, LX/2MM;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-direct {p0, v3, v4, v5, v6}, LX/61m;-><init>(LX/5Ow;LX/2MM;LX/0Uh;LX/0Zb;)V

    .line 1040645
    move-object v0, p0

    .line 1040646
    sput-object v0, LX/61m;->e:LX/61m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1040647
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1040648
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1040649
    :cond_1
    sget-object v0, LX/61m;->e:LX/61m;

    return-object v0

    .line 1040650
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1040651
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/60v;)V
    .locals 3

    .prologue
    .line 1040658
    iget-object v0, p0, LX/61m;->c:LX/0Zb;

    const-string v1, "video_upload_spherical_metadata_found"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1040659
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1040660
    const-string v2, "isSpherical"

    iget-boolean v0, p1, LX/60v;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "true"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1040661
    const-string v0, "projectionType"

    iget-object v2, p1, LX/60v;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1040662
    const-string v0, "stereoMode"

    iget-object v2, p1, LX/60v;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1040663
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1040664
    :cond_0
    return-void

    .line 1040665
    :cond_1
    const-string v0, "false"

    goto :goto_0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1040605
    iget-object v0, p0, LX/61m;->c:LX/0Zb;

    const-string v1, "video_upload_spherical_metadata_exception"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1040606
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1040607
    const-string v1, "exception"

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1040608
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1040609
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/60x;
    .locals 18

    .prologue
    .line 1040610
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, LX/61m;->a:LX/2MM;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/61m;->d:LX/0Uh;

    const/16 v4, 0x268

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_0
    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v2}, LX/2MM;->a(Landroid/net/Uri;LX/46h;)LX/46f;
    :try_end_0
    .catch Lcom/facebook/ffmpeg/FFMpegBadDataException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v16

    .line 1040611
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/61m;->b:LX/5Ow;

    move-object/from16 v0, v16

    iget-object v3, v0, LX/46f;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5Ow;->a(Ljava/lang/String;)Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->a()Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v17

    .line 1040612
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->d()J

    move-result-wide v4

    .line 1040613
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->e()I

    move-result v6

    .line 1040614
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->f()I

    move-result v7

    .line 1040615
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->c()I

    move-result v8

    .line 1040616
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->g()I

    move-result v9

    .line 1040617
    move-object/from16 v0, v16

    iget-object v2, v0, LX/46f;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v10

    .line 1040618
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->h()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v12

    .line 1040619
    const/4 v3, 0x0

    .line 1040620
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->i()Ljava/lang/String;

    move-result-object v13

    .line 1040621
    if-eqz v13, :cond_0

    .line 1040622
    new-instance v2, LX/60v;

    invoke-direct {v2, v13}, LX/60v;-><init>(Ljava/lang/String;)V

    move-object v3, v2

    .line 1040623
    :cond_0
    if-eqz v3, :cond_1

    .line 1040624
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LX/61m;->a(LX/60v;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    move-object v13, v3

    .line 1040625
    :goto_1
    :try_start_4
    new-instance v3, LX/60x;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v3 .. v15}, LX/60x;-><init>(JIIIIJILX/60v;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1040626
    :try_start_5
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->b()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1040627
    :try_start_6
    invoke-virtual/range {v16 .. v16}, LX/46f;->a()V

    return-object v3

    .line 1040628
    :cond_2
    sget-object v2, LX/46h;->PREFER_SDCARD:LX/46h;
    :try_end_6
    .catch Lcom/facebook/ffmpeg/FFMpegBadDataException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_0

    .line 1040629
    :catch_0
    move-exception v2

    .line 1040630
    :try_start_7
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/61m;->a(Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-object v13, v3

    goto :goto_1

    .line 1040631
    :catchall_0
    move-exception v2

    :try_start_8
    invoke-virtual/range {v17 .. v17}, Lcom/facebook/ffmpeg/FFMpegMediaMetadataRetriever;->b()V

    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1040632
    :catchall_1
    move-exception v2

    :try_start_9
    invoke-virtual/range {v16 .. v16}, LX/46f;->a()V

    throw v2
    :try_end_9
    .catch Lcom/facebook/ffmpeg/FFMpegBadDataException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_2

    .line 1040633
    :catch_1
    move-exception v2

    .line 1040634
    :goto_2
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3, v2}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 1040635
    :catch_2
    move-exception v2

    goto :goto_2
.end method
