.class public final LX/5pL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/react/bridge/ExecutorToken;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/react/bridge/CatalystInstance;

.field private final c:LX/5pJ;


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ExecutorToken;Lcom/facebook/react/bridge/CatalystInstance;LX/5pJ;)V
    .locals 1

    .prologue
    .line 1008032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008033
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/5pL;->a:Ljava/lang/ref/WeakReference;

    .line 1008034
    iput-object p2, p0, LX/5pL;->b:Lcom/facebook/react/bridge/CatalystInstance;

    .line 1008035
    iput-object p3, p0, LX/5pL;->c:LX/5pJ;

    .line 1008036
    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p3    # [Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1008037
    iget-object v0, p0, LX/5pL;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/bridge/ExecutorToken;

    .line 1008038
    if-nez v0, :cond_0

    .line 1008039
    const-string v0, "React"

    const-string v1, "Dropping JS call, ExecutorToken went away..."

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008040
    :goto_0
    return-object v5

    .line 1008041
    :cond_0
    if-eqz p3, :cond_1

    invoke-static {p3}, LX/5op;->a([Ljava/lang/Object;)Lcom/facebook/react/bridge/WritableNativeArray;

    move-result-object v1

    .line 1008042
    :goto_1
    iget-object v2, p0, LX/5pL;->b:Lcom/facebook/react/bridge/CatalystInstance;

    iget-object v3, p0, LX/5pL;->c:LX/5pJ;

    invoke-virtual {v3}, LX/5pJ;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v0, v3, v4, v1}, Lcom/facebook/react/bridge/CatalystInstance;->callFunction(Lcom/facebook/react/bridge/ExecutorToken;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/react/bridge/NativeArray;)V

    goto :goto_0

    .line 1008043
    :cond_1
    new-instance v1, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v1}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    goto :goto_1
.end method
