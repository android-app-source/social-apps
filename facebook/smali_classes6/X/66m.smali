.class public abstract LX/66m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/66l;


# instance fields
.field public final c:LX/66s;

.field public final d:LX/66q;

.field public final e:LX/K09;

.field public volatile f:Z

.field private g:Z

.field public h:Z

.field public final i:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(ZLX/671;LX/670;Ljava/util/Random;Ljava/util/concurrent/Executor;LX/K09;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1050929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1050930
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, LX/66m;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1050931
    iput-object p6, p0, LX/66m;->e:LX/K09;

    .line 1050932
    new-instance v0, LX/66s;

    invoke-direct {v0, p1, p3, p4}, LX/66s;-><init>(ZLX/670;Ljava/util/Random;)V

    iput-object v0, p0, LX/66m;->c:LX/66s;

    .line 1050933
    new-instance v0, LX/66q;

    new-instance v1, LX/66k;

    invoke-direct {v1, p0, p6, p5, p7}, LX/66k;-><init>(LX/66m;LX/K09;Ljava/util/concurrent/Executor;Ljava/lang/String;)V

    invoke-direct {v0, p1, p2, v1}, LX/66q;-><init>(ZLX/671;LX/66k;)V

    iput-object v0, p0, LX/66m;->d:LX/66q;

    .line 1050934
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1050935
    iget-boolean v0, p0, LX/66m;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050936
    :cond_0
    iput-boolean v3, p0, LX/66m;->f:Z

    .line 1050937
    :try_start_0
    iget-object v0, p0, LX/66m;->c:LX/66s;

    invoke-virtual {v0, p1, p2}, LX/66s;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1050938
    return-void

    .line 1050939
    :catch_0
    move-exception v0

    .line 1050940
    iget-object v1, p0, LX/66m;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1050941
    :try_start_1
    invoke-virtual {p0}, LX/66m;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1050942
    :cond_1
    :goto_0
    throw v0

    :catch_1
    goto :goto_0
.end method

.method public final a(LX/651;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1050943
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "message == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050944
    :cond_0
    iget-boolean v0, p0, LX/66m;->f:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050945
    :cond_1
    iget-boolean v0, p0, LX/66m;->g:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call close()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050946
    :cond_2
    invoke-virtual {p1}, LX/651;->a()LX/64s;

    move-result-object v0

    .line 1050947
    if-nez v0, :cond_3

    .line 1050948
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Message content type was null. Must use WebSocket.TEXT or WebSocket.BINARY."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050949
    :cond_3
    iget-object v2, v0, LX/64s;->e:Ljava/lang/String;

    move-object v2, v2

    .line 1050950
    sget-object v3, LX/66l;->a:LX/64s;

    .line 1050951
    iget-object v4, v3, LX/64s;->e:Ljava/lang/String;

    move-object v3, v4

    .line 1050952
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v1

    .line 1050953
    :goto_0
    iget-object v2, p0, LX/66m;->c:LX/66s;

    invoke-virtual {p1}, LX/651;->b()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, LX/66s;->a(IJ)LX/65J;

    move-result-object v0

    invoke-static {v0}, LX/67B;->a(LX/65J;)LX/670;

    move-result-object v0

    .line 1050954
    :try_start_0
    invoke-virtual {p1, v0}, LX/651;->a(LX/670;)V

    .line 1050955
    invoke-interface {v0}, LX/65J;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1050956
    return-void

    .line 1050957
    :cond_4
    sget-object v3, LX/66l;->b:LX/64s;

    .line 1050958
    iget-object v4, v3, LX/64s;->e:Ljava/lang/String;

    move-object v3, v4

    .line 1050959
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1050960
    const/4 v0, 0x2

    goto :goto_0

    .line 1050961
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown message content type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1050962
    iget-object v3, v0, LX/64s;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1050963
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1050964
    iget-object v3, v0, LX/64s;->e:Ljava/lang/String;

    move-object v0, v3

    .line 1050965
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Must use WebSocket.TEXT or WebSocket.BINARY."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1050966
    :catch_0
    move-exception v0

    .line 1050967
    iput-boolean v1, p0, LX/66m;->g:Z

    .line 1050968
    throw v0
.end method

.method public final a(LX/672;)V
    .locals 2

    .prologue
    .line 1050969
    iget-boolean v0, p0, LX/66m;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050970
    :cond_0
    iget-boolean v0, p0, LX/66m;->g:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "must call close()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1050971
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/66m;->c:LX/66s;

    invoke-virtual {v0, p1}, LX/66s;->a(LX/672;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1050972
    return-void

    .line 1050973
    :catch_0
    move-exception v0

    .line 1050974
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/66m;->g:Z

    .line 1050975
    throw v0
.end method

.method public abstract b()V
.end method
