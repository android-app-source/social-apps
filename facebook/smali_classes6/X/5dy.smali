.class public final LX/5dy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5di;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5di",
        "<",
        "Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 966459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;
    .locals 2

    .prologue
    .line 966460
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;

    const-string v1, "confidence"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->d(LX/0lF;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;-><init>(I)V

    return-object v0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966461
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966462
    new-array v0, p1, [Lcom/facebook/messaging/model/messagemetadata/WatchMovieMetadata;

    return-object v0
.end method
