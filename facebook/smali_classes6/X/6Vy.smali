.class public final enum LX/6Vy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Vy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Vy;

.field public static final enum DELETE_INTERCEPT:LX/6Vy;

.field public static final enum FEED_POST_CHEVRON:LX/6Vy;

.field public static final enum FEED_STORY:LX/6Vy;

.field public static final enum GROUP_POST_CHEVRON:LX/6Vy;

.field public static final enum YOUR_POSTS:LX/6Vy;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1104526
    new-instance v0, LX/6Vy;

    const-string v1, "YOUR_POSTS"

    invoke-direct {v0, v1, v2}, LX/6Vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Vy;->YOUR_POSTS:LX/6Vy;

    .line 1104527
    new-instance v0, LX/6Vy;

    const-string v1, "GROUP_POST_CHEVRON"

    invoke-direct {v0, v1, v3}, LX/6Vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Vy;->GROUP_POST_CHEVRON:LX/6Vy;

    .line 1104528
    new-instance v0, LX/6Vy;

    const-string v1, "FEED_POST_CHEVRON"

    invoke-direct {v0, v1, v4}, LX/6Vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Vy;->FEED_POST_CHEVRON:LX/6Vy;

    .line 1104529
    new-instance v0, LX/6Vy;

    const-string v1, "DELETE_INTERCEPT"

    invoke-direct {v0, v1, v5}, LX/6Vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Vy;->DELETE_INTERCEPT:LX/6Vy;

    .line 1104530
    new-instance v0, LX/6Vy;

    const-string v1, "FEED_STORY"

    invoke-direct {v0, v1, v6}, LX/6Vy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Vy;->FEED_STORY:LX/6Vy;

    .line 1104531
    const/4 v0, 0x5

    new-array v0, v0, [LX/6Vy;

    sget-object v1, LX/6Vy;->YOUR_POSTS:LX/6Vy;

    aput-object v1, v0, v2

    sget-object v1, LX/6Vy;->GROUP_POST_CHEVRON:LX/6Vy;

    aput-object v1, v0, v3

    sget-object v1, LX/6Vy;->FEED_POST_CHEVRON:LX/6Vy;

    aput-object v1, v0, v4

    sget-object v1, LX/6Vy;->DELETE_INTERCEPT:LX/6Vy;

    aput-object v1, v0, v5

    sget-object v1, LX/6Vy;->FEED_STORY:LX/6Vy;

    aput-object v1, v0, v6

    sput-object v0, LX/6Vy;->$VALUES:[LX/6Vy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1104532
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Vy;
    .locals 1

    .prologue
    .line 1104533
    const-class v0, LX/6Vy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Vy;

    return-object v0
.end method

.method public static values()[LX/6Vy;
    .locals 1

    .prologue
    .line 1104534
    sget-object v0, LX/6Vy;->$VALUES:[LX/6Vy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Vy;

    return-object v0
.end method
