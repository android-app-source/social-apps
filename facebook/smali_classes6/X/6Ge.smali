.class public LX/6Ge;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Fe;


# static fields
.field public static final a:LX/0Tn;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0lC;

.field private final e:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1070751
    const-class v0, LX/6Fe;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Ge;->b:Ljava/lang/String;

    .line 1070752
    sget-object v0, LX/0Tm;->b:LX/0Tn;

    sget-object v1, LX/6Ge;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "status"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/6Ge;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lB;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1070753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1070754
    iput-object p1, p0, LX/6Ge;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1070755
    iput-object p2, p0, LX/6Ge;->d:LX/0lC;

    .line 1070756
    iput-object p3, p0, LX/6Ge;->e:LX/0SG;

    .line 1070757
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1070758
    sget-object v0, LX/6Ge;->a:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method private a(Lcom/facebook/bugreporter/BugReport;Lcom/facebook/bugreporter/debug/BugReportUploadStatus;)V
    .locals 3

    .prologue
    .line 1070759
    iget-object v0, p0, LX/6Ge;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 1070760
    iget-object v1, p1, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1070761
    invoke-static {v1}, LX/6Ge;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    iget-object v2, p0, LX/6Ge;->d:LX/0lC;

    invoke-virtual {p2, v2}, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->a(LX/0lC;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1070762
    iget-object v0, p0, LX/6Ge;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6Ge;->a:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->d(LX/0Tn;)Ljava/util/Set;

    move-result-object v0

    .line 1070763
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    .line 1070764
    :goto_0
    return-void

    .line 1070765
    :cond_0
    invoke-virtual {p0}, LX/6Ge;->a()Ljava/util/List;

    move-result-object v1

    .line 1070766
    new-instance v0, LX/6Gd;

    invoke-direct {v0, p0}, LX/6Gd;-><init>(LX/6Ge;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1070767
    const/4 v0, 0x0

    .line 1070768
    iget-object v2, p0, LX/6Ge;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 1070769
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    .line 1070770
    const/4 p2, 0x5

    if-ge v1, p2, :cond_1

    .line 1070771
    iget-object v0, v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->reportId:Ljava/lang/String;

    invoke-static {v0}, LX/6Ge;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {v2, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 1070772
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1070773
    goto :goto_1

    .line 1070774
    :cond_1
    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method private a(Lcom/facebook/bugreporter/BugReport;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1070775
    invoke-direct {p0, p1}, LX/6Ge;->b(Lcom/facebook/bugreporter/BugReport;)Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    move-result-object v0

    .line 1070776
    iget-object v1, p0, LX/6Ge;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 1070777
    iget-object v4, v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->failedUploadAttempts:Ljava/util/List;

    invoke-interface {v4, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1070778
    iput-wide v2, v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->wallTimeOfLastUpdateOfStatus:J

    .line 1070779
    invoke-direct {p0, p1, v0}, LX/6Ge;->a(Lcom/facebook/bugreporter/BugReport;Lcom/facebook/bugreporter/debug/BugReportUploadStatus;)V

    .line 1070780
    return-void
.end method

.method private b(Lcom/facebook/bugreporter/BugReport;)Lcom/facebook/bugreporter/debug/BugReportUploadStatus;
    .locals 10

    .prologue
    .line 1070781
    iget-object v0, p0, LX/6Ge;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1070782
    iget-object v1, p1, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    move-object v1, v1

    .line 1070783
    invoke-static {v1}, LX/6Ge;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1070784
    if-nez v0, :cond_0

    .line 1070785
    new-instance v3, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    .line 1070786
    iget-object v4, p1, Lcom/facebook/bugreporter/BugReport;->g:Ljava/lang/String;

    move-object v4, v4

    .line 1070787
    iget-object v5, p1, Lcom/facebook/bugreporter/BugReport;->r:Ljava/lang/String;

    move-object v5, v5

    .line 1070788
    iget-object v6, p1, Lcom/facebook/bugreporter/BugReport;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1070789
    iget-object v7, p1, Lcom/facebook/bugreporter/BugReport;->n:Ljava/lang/String;

    move-object v7, v7

    .line 1070790
    const/4 v8, 0x0

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-direct/range {v3 .. v9}, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V

    move-object v0, v3

    .line 1070791
    :goto_0
    return-object v0

    .line 1070792
    :cond_0
    iget-object v1, p0, LX/6Ge;->d:LX/0lC;

    invoke-static {v1, v0}, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->a(LX/0lC;Ljava/lang/String;)Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bugreporter/debug/BugReportUploadStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1070793
    iget-object v0, p0, LX/6Ge;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6Ge;->a:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v0

    .line 1070794
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1070795
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1070796
    iget-object v4, p0, LX/6Ge;->d:LX/0lC;

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->a(LX/0lC;Ljava/lang/String;)Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    move-result-object v0

    .line 1070797
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Deserialization failed for: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1070798
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1070799
    :cond_0
    return-object v2
.end method

.method public final a(Lcom/facebook/bugreporter/BugReport;)V
    .locals 5

    .prologue
    .line 1070800
    invoke-direct {p0, p1}, LX/6Ge;->b(Lcom/facebook/bugreporter/BugReport;)Lcom/facebook/bugreporter/debug/BugReportUploadStatus;

    move-result-object v0

    .line 1070801
    iget-object v1, p0, LX/6Ge;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 1070802
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->isSuccessfullyUploaded:Z

    .line 1070803
    iput-wide v2, v0, Lcom/facebook/bugreporter/debug/BugReportUploadStatus;->wallTimeOfLastUpdateOfStatus:J

    .line 1070804
    invoke-direct {p0, p1, v0}, LX/6Ge;->a(Lcom/facebook/bugreporter/BugReport;Lcom/facebook/bugreporter/debug/BugReportUploadStatus;)V

    .line 1070805
    return-void
.end method

.method public final a(Lcom/facebook/bugreporter/BugReport;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1070806
    const-string v0, "ErrorCode: %s, message: %s"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/6Ge;->a(Lcom/facebook/bugreporter/BugReport;Ljava/lang/String;)V

    .line 1070807
    return-void
.end method

.method public final a(Lcom/facebook/bugreporter/BugReport;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1070808
    const-string v0, "Type: %s, message: %s"

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/6Ge;->a(Lcom/facebook/bugreporter/BugReport;Ljava/lang/String;)V

    .line 1070809
    return-void
.end method
