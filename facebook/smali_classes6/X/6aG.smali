.class public LX/6aG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/6ZP;


# direct methods
.method public constructor <init>(LX/6ZP;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1112263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1112264
    iput-object p1, p0, LX/6aG;->b:LX/6ZP;

    .line 1112265
    iput-object p2, p0, LX/6aG;->a:Landroid/content/res/Resources;

    .line 1112266
    return-void
.end method

.method private a(D)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v3, 0x3e8

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 1112229
    iget-object v0, p0, LX/6aG;->b:LX/6ZP;

    .line 1112230
    iget-object v1, v0, LX/6ZP;->b:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1112231
    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 1112232
    sget-object v2, LX/6ZP;->a:LX/0Rf;

    invoke-virtual {v2, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1112233
    sget-object v1, LX/6ZO;->IMPERIAL:LX/6ZO;

    .line 1112234
    :goto_0
    move-object v0, v1

    .line 1112235
    sget-object v1, LX/6ZO;->IMPERIAL:LX/6ZO;

    if-ne v0, v1, :cond_1

    .line 1112236
    const-wide v0, 0x400a3f290abb44e5L    # 3.28084

    mul-double/2addr v0, p1

    .line 1112237
    invoke-static {v0, v1}, LX/6aG;->b(D)I

    move-result v2

    .line 1112238
    if-lt v2, v3, :cond_0

    .line 1112239
    iget-object v2, p0, LX/6aG;->a:Landroid/content/res/Resources;

    const v3, 0x7f0801c0

    new-array v4, v4, [Ljava/lang/Object;

    const-wide v6, 0x40b4a00000000000L    # 5280.0

    div-double/2addr v0, v6

    invoke-static {v0, v1}, LX/6aG;->c(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1112240
    :goto_1
    return-object v0

    .line 1112241
    :cond_0
    iget-object v0, p0, LX/6aG;->a:Landroid/content/res/Resources;

    const v1, 0x7f0801c1

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v8

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1112242
    :cond_1
    invoke-static {p1, p2}, LX/6aG;->b(D)I

    move-result v0

    .line 1112243
    if-lt v0, v3, :cond_2

    .line 1112244
    iget-object v0, p0, LX/6aG;->a:Landroid/content/res/Resources;

    const v1, 0x7f0801c3

    new-array v2, v4, [Ljava/lang/Object;

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double v4, p1, v4

    invoke-static {v4, v5}, LX/6aG;->c(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1112245
    :cond_2
    iget-object v1, p0, LX/6aG;->a:Landroid/content/res/Resources;

    const v2, 0x7f0801c2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    sget-object v1, LX/6ZO;->METRIC:LX/6ZO;

    goto :goto_0
.end method

.method private static b(D)I
    .locals 4

    .prologue
    .line 1112246
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    invoke-static {p0, p1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 1112247
    div-double v2, p0, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public static b(LX/0QB;)LX/6aG;
    .locals 3

    .prologue
    .line 1112248
    new-instance v2, LX/6aG;

    .line 1112249
    new-instance v1, LX/6ZP;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/6ZP;-><init>(Landroid/content/res/Resources;)V

    .line 1112250
    move-object v0, v1

    .line 1112251
    check-cast v0, LX/6ZP;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v2, v0, v1}, LX/6aG;-><init>(LX/6ZP;Landroid/content/res/Resources;)V

    .line 1112252
    return-object v2
.end method

.method private static c(D)D
    .locals 4

    .prologue
    .line 1112253
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    invoke-static {p0, p1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v2

    .line 1112254
    div-double v2, p0, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-double v2, v2

    mul-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(Landroid/location/Location;Landroid/location/Location;)Ljava/lang/String;
    .locals 9

    .prologue
    .line 1112255
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 1112256
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 1112257
    const/4 v0, 0x0

    aget v0, v8, v0

    float-to-double v0, v0

    .line 1112258
    invoke-direct {p0, v0, v1}, LX/6aG;->a(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;DLjava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1112259
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 1112260
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-virtual {p2}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 1112261
    const/4 v0, 0x0

    aget v0, v8, v0

    float-to-double v0, v0

    .line 1112262
    cmpg-double v2, v0, p3

    if-gtz v2, :cond_0

    invoke-direct {p0, v0, v1}, LX/6aG;->a(D)Ljava/lang/String;

    move-result-object p5

    :cond_0
    return-object p5
.end method
