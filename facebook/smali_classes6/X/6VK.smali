.class public final enum LX/6VK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6VK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6VK;

.field public static final enum FRIENDS_LOCATIONS_FEEDSTORY_ITEM_IMPRESSION:LX/6VK;

.field public static final enum FRIENDS_LOCATIONS_FEEDSTORY_SCROLL:LX/6VK;

.field public static final enum FRIENDS_LOCATIONS_FEEDSTORY_SCROLL_NEAR_END:LX/6VK;

.field public static final enum FRIENDS_LOCATIONS_FEEDSTORY_SEE_ALL:LX/6VK;

.field public static final enum FRIENDS_LOCATIONS_FEEDSTORY_TAP_MESSAGE:LX/6VK;

.field public static final enum FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE:LX/6VK;

.field public static final enum FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE_PIC:LX/6VK;

.field public static final enum FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_MAP:LX/6VK;

.field public static final enum FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_PROFILE:LX/6VK;


# instance fields
.field public final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1103944
    new-instance v0, LX/6VK;

    const-string v1, "FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE_PIC"

    const-string v2, "friends_locations_feedstory_tap_profile_pic"

    invoke-direct {v0, v1, v4, v2}, LX/6VK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE_PIC:LX/6VK;

    .line 1103945
    new-instance v0, LX/6VK;

    const-string v1, "FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE"

    const-string v2, "friends_locations_feedstory_tap_profile"

    invoke-direct {v0, v1, v5, v2}, LX/6VK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE:LX/6VK;

    .line 1103946
    new-instance v0, LX/6VK;

    const-string v1, "FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_PROFILE"

    const-string v2, "friends_locations_feedstory_v2_tap_profile"

    invoke-direct {v0, v1, v6, v2}, LX/6VK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_PROFILE:LX/6VK;

    .line 1103947
    new-instance v0, LX/6VK;

    const-string v1, "FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_MAP"

    const-string v2, "friends_locations_feedstory_v2_tap_map"

    invoke-direct {v0, v1, v7, v2}, LX/6VK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_MAP:LX/6VK;

    .line 1103948
    new-instance v0, LX/6VK;

    const-string v1, "FRIENDS_LOCATIONS_FEEDSTORY_SEE_ALL"

    const-string v2, "friends_locations_feedstory_see_all"

    invoke-direct {v0, v1, v8, v2}, LX/6VK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_SEE_ALL:LX/6VK;

    .line 1103949
    new-instance v0, LX/6VK;

    const-string v1, "FRIENDS_LOCATIONS_FEEDSTORY_TAP_MESSAGE"

    const/4 v2, 0x5

    const-string v3, "friends_locations_feedstory_tap_message"

    invoke-direct {v0, v1, v2, v3}, LX/6VK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_MESSAGE:LX/6VK;

    .line 1103950
    new-instance v0, LX/6VK;

    const-string v1, "FRIENDS_LOCATIONS_FEEDSTORY_SCROLL"

    const/4 v2, 0x6

    const-string v3, "friends_locations_feedstory_scroll"

    invoke-direct {v0, v1, v2, v3}, LX/6VK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_SCROLL:LX/6VK;

    .line 1103951
    new-instance v0, LX/6VK;

    const-string v1, "FRIENDS_LOCATIONS_FEEDSTORY_ITEM_IMPRESSION"

    const/4 v2, 0x7

    const-string v3, "friends_locations_feedstory_item_impression"

    invoke-direct {v0, v1, v2, v3}, LX/6VK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_ITEM_IMPRESSION:LX/6VK;

    .line 1103952
    new-instance v0, LX/6VK;

    const-string v1, "FRIENDS_LOCATIONS_FEEDSTORY_SCROLL_NEAR_END"

    const/16 v2, 0x8

    const-string v3, "friends_locations_feedstory_scroll_near_end"

    invoke-direct {v0, v1, v2, v3}, LX/6VK;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_SCROLL_NEAR_END:LX/6VK;

    .line 1103953
    const/16 v0, 0x9

    new-array v0, v0, [LX/6VK;

    sget-object v1, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE_PIC:LX/6VK;

    aput-object v1, v0, v4

    sget-object v1, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE:LX/6VK;

    aput-object v1, v0, v5

    sget-object v1, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_PROFILE:LX/6VK;

    aput-object v1, v0, v6

    sget-object v1, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_MAP:LX/6VK;

    aput-object v1, v0, v7

    sget-object v1, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_SEE_ALL:LX/6VK;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_MESSAGE:LX/6VK;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_SCROLL:LX/6VK;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_ITEM_IMPRESSION:LX/6VK;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_SCROLL_NEAR_END:LX/6VK;

    aput-object v2, v0, v1

    sput-object v0, LX/6VK;->$VALUES:[LX/6VK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1103954
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1103955
    iput-object p3, p0, LX/6VK;->mName:Ljava/lang/String;

    .line 1103956
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6VK;
    .locals 1

    .prologue
    .line 1103957
    const-class v0, LX/6VK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6VK;

    return-object v0
.end method

.method public static values()[LX/6VK;
    .locals 1

    .prologue
    .line 1103958
    sget-object v0, LX/6VK;->$VALUES:[LX/6VK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6VK;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103959
    iget-object v0, p0, LX/6VK;->mName:Ljava/lang/String;

    return-object v0
.end method
