.class public final LX/58o;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 849153
    const-class v1, Lcom/facebook/api/graphql/commentservice/CommentsServiceModels$CommentDeleteMutationModel;

    const v0, 0xb7f96ec

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "CommentDeleteMutation"

    const-string v6, "60934e0a03ae0775732939e3aa9eb0bc"

    const-string v7, "comment_delete"

    const-string v8, "0"

    const-string v9, "10155069963156729"

    const/4 v10, 0x0

    .line 849154
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 849155
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 849156
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 849157
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 849158
    sparse-switch v0, :sswitch_data_0

    .line 849159
    :goto_0
    return-object p1

    .line 849160
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 849161
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x3c54de38 -> :sswitch_1
        0x5fb57ca -> :sswitch_0
    .end sparse-switch
.end method
