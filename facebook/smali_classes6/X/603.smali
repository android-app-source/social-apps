.class public LX/603;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:I

.field public c:Ljava/lang/Integer;

.field public d:Lcom/facebook/uicontrib/datepicker/Period;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/Integer;Lcom/facebook/uicontrib/datepicker/Period;)V
    .locals 7

    .prologue
    .line 1036386
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 1036387
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, LX/603;->a:Ljava/util/List;

    .line 1036388
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    .line 1036389
    const/4 v1, 0x1

    :goto_0
    const/16 v3, 0x1f

    if-gt v1, v3, :cond_0

    .line 1036390
    sget-object v3, LX/603;->a:Ljava/util/List;

    int-to-long v5, v1

    invoke-virtual {v2, v5, v6}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1036391
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1036392
    :cond_0
    iput p3, p0, LX/603;->b:I

    .line 1036393
    iput-object p4, p0, LX/603;->c:Ljava/lang/Integer;

    .line 1036394
    iput-object p5, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    .line 1036395
    invoke-static {p0}, LX/603;->b(LX/603;)Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, LX/603;->a(LX/603;Ljava/util/List;)V

    .line 1036396
    return-void
.end method

.method public static a(II)I
    .locals 3

    .prologue
    .line 1036380
    packed-switch p1, :pswitch_data_0

    .line 1036381
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a valid month: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1036382
    :pswitch_0
    const/16 v0, 0x1f

    .line 1036383
    :goto_0
    return v0

    .line 1036384
    :pswitch_1
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v0, p0}, Ljava/util/GregorianCalendar;->isLeapYear(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1d

    goto :goto_0

    :cond_0
    const/16 v0, 0x1c

    goto :goto_0

    .line 1036385
    :pswitch_2
    const/16 v0, 0x1e

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/603;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1036377
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1036378
    invoke-virtual {p0, v0}, LX/603;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 1036379
    :cond_0
    return-void
.end method

.method public static b(LX/603;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1036365
    iget v0, p0, LX/603;->b:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/603;->c:Ljava/lang/Integer;

    if-nez v0, :cond_2

    .line 1036366
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1036367
    :cond_1
    :goto_0
    return-object v0

    .line 1036368
    :cond_2
    sget-object v0, LX/603;->a:Ljava/util/List;

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1036369
    invoke-static {p0}, LX/603;->c(LX/603;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {p0}, LX/603;->d(LX/603;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1036370
    :cond_3
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 1036371
    :cond_4
    iget v1, p0, LX/603;->b:I

    iget-object v2, p0, LX/603;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, LX/603;->a(II)I

    move-result v1

    .line 1036372
    invoke-interface {v0, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 1036373
    invoke-static {p0}, LX/603;->f(LX/603;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1036374
    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->f()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 1036375
    :cond_5
    invoke-static {p0}, LX/603;->e(LX/603;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1036376
    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->e()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(LX/603;)Z
    .locals 2

    .prologue
    .line 1036364
    iget v0, p0, LX/603;->b:I

    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->a()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget v0, p0, LX/603;->b:I

    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->a()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/603;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/603;)Z
    .locals 2

    .prologue
    .line 1036363
    iget v0, p0, LX/603;->b:I

    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->b()I

    move-result v1

    if-gt v0, v1, :cond_0

    iget v0, p0, LX/603;->b:I

    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->b()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/603;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->d()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/603;)Z
    .locals 2

    .prologue
    .line 1036360
    iget v0, p0, LX/603;->b:I

    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Period;->c()Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, LX/603;->c:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/603;)Z
    .locals 2

    .prologue
    .line 1036362
    iget v0, p0, LX/603;->b:I

    iget-object v1, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v1}, Lcom/facebook/uicontrib/datepicker/Period;->b()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/603;->d:Lcom/facebook/uicontrib/datepicker/Period;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Period;->d()Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, LX/603;->c:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)I
    .locals 2

    .prologue
    .line 1036361
    sget-object v0, LX/603;->a:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/603;->getPosition(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
