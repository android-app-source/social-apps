.class public final LX/67e;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1053278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LX/67d;
    .locals 2

    .prologue
    .line 1053285
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/67e;->a(FLandroid/graphics/Point;)LX/67d;

    move-result-object v0

    return-object v0
.end method

.method public static a(FLandroid/graphics/Point;)LX/67d;
    .locals 2

    .prologue
    .line 1053279
    new-instance v0, LX/67d;

    invoke-direct {v0}, LX/67d;-><init>()V

    .line 1053280
    iput p0, v0, LX/67d;->c:F

    .line 1053281
    if-eqz p1, :cond_0

    .line 1053282
    iget v1, p1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iput v1, v0, LX/67d;->d:F

    .line 1053283
    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iput v1, v0, LX/67d;->e:F

    .line 1053284
    :cond_0
    return-object v0
.end method

.method public static a(LX/697;I)LX/67d;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1053271
    invoke-static {p0, v0, v0, p1}, LX/67e;->a(LX/697;III)LX/67d;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/697;III)LX/67d;
    .locals 1

    .prologue
    .line 1053272
    new-instance v0, LX/67d;

    invoke-direct {v0}, LX/67d;-><init>()V

    .line 1053273
    iput-object p0, v0, LX/67d;->i:LX/697;

    .line 1053274
    iput p1, v0, LX/67d;->j:I

    .line 1053275
    iput p2, v0, LX/67d;->k:I

    .line 1053276
    iput p3, v0, LX/67d;->l:I

    .line 1053277
    return-object v0
.end method

.method public static a(Lcom/facebook/android/maps/model/LatLng;)LX/67d;
    .locals 1

    .prologue
    .line 1053268
    new-instance v0, LX/67d;

    invoke-direct {v0}, LX/67d;-><init>()V

    .line 1053269
    iput-object p0, v0, LX/67d;->a:Lcom/facebook/android/maps/model/LatLng;

    .line 1053270
    return-object v0
.end method

.method public static a(Lcom/facebook/android/maps/model/LatLng;F)LX/67d;
    .locals 1

    .prologue
    .line 1053264
    new-instance v0, LX/67d;

    invoke-direct {v0}, LX/67d;-><init>()V

    .line 1053265
    iput-object p0, v0, LX/67d;->a:Lcom/facebook/android/maps/model/LatLng;

    .line 1053266
    iput p1, v0, LX/67d;->b:F

    .line 1053267
    return-object v0
.end method

.method public static b()LX/67d;
    .locals 2

    .prologue
    .line 1053263
    const/high16 v0, -0x40800000    # -1.0f

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/67e;->a(FLandroid/graphics/Point;)LX/67d;

    move-result-object v0

    return-object v0
.end method

.method public static c(F)LX/67d;
    .locals 1

    .prologue
    .line 1053260
    new-instance v0, LX/67d;

    invoke-direct {v0}, LX/67d;-><init>()V

    .line 1053261
    iput p0, v0, LX/67d;->b:F

    .line 1053262
    return-object v0
.end method
