.class public LX/5qa;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/5qa;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1009470
    const/4 v0, 0x0

    sput-object v0, LX/5qa;->a:LX/5qa;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1009468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1009469
    return-void
.end method

.method public static a()LX/5qa;
    .locals 1

    .prologue
    .line 1009465
    sget-object v0, LX/5qa;->a:LX/5qa;

    if-nez v0, :cond_0

    .line 1009466
    new-instance v0, LX/5qa;

    invoke-direct {v0}, LX/5qa;-><init>()V

    sput-object v0, LX/5qa;->a:LX/5qa;

    .line 1009467
    :cond_0
    sget-object v0, LX/5qa;->a:LX/5qa;

    return-object v0
.end method

.method public static final a(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 1009463
    const-string v0, "RCTI18nUtil_allowRTL"

    invoke-static {p0, v0, p1}, LX/5qa;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 1009464
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 1009461
    const-string v0, "com.facebook.react.modules.i18nmanager.I18nUtil"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1009462
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1009457
    const-string v0, "com.facebook.react.modules.i18nmanager.I18nUtil"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1009458
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1009459
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1009460
    return-void
.end method

.method public static final b(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 1009455
    const-string v0, "RCTI18nUtil_forceRTL"

    invoke-static {p0, v0, p1}, LX/5qa;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 1009456
    return-void
.end method

.method private static b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1009453
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, LX/3rK;->a(Ljava/util/Locale;)I

    move-result v1

    .line 1009454
    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 1009452
    const-string v0, "RCTI18nUtil_allowRTL"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/5qa;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static c(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 1009449
    const-string v0, "RCTI18nUtil_forceRTL"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/5qa;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1009450
    invoke-static {p1}, LX/5qa;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1009451
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, LX/5qa;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, LX/5qa;->b()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
