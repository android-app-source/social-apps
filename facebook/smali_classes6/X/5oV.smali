.class public final enum LX/5oV;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/5oU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5oV;",
        ">;",
        "LX/5oU;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5oV;

.field public static final enum CANCEL_COMPOSER:LX/5oV;

.field public static final enum CLOSE_SUGGESTION:LX/5oV;

.field public static final enum HIDE_FLYOUT:LX/5oV;

.field public static final enum IMPRESSION:LX/5oV;

.field public static final enum OPEN_SELECTING_CONTENT:LX/5oV;

.field public static final enum OPEN_SUGGESTING_CONTENT:LX/5oV;

.field public static final enum POST_WITHOUT_PROMPT:LX/5oV;

.field public static final enum POST_WITH_PROMPT:LX/5oV;

.field public static final enum SELECT_SUGGESTION:LX/5oV;

.field public static final enum SHOW_FLYOUT:LX/5oV;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1006987
    new-instance v0, LX/5oV;

    const-string v1, "OPEN_SELECTING_CONTENT"

    const-string v2, "open_selecting_content"

    invoke-direct {v0, v1, v4, v2}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->OPEN_SELECTING_CONTENT:LX/5oV;

    .line 1006988
    new-instance v0, LX/5oV;

    const-string v1, "OPEN_SUGGESTING_CONTENT"

    const-string v2, "open_suggesting_content"

    invoke-direct {v0, v1, v5, v2}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->OPEN_SUGGESTING_CONTENT:LX/5oV;

    .line 1006989
    new-instance v0, LX/5oV;

    const-string v1, "CLOSE_SUGGESTION"

    const-string v2, "close_suggestion"

    invoke-direct {v0, v1, v6, v2}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->CLOSE_SUGGESTION:LX/5oV;

    .line 1006990
    new-instance v0, LX/5oV;

    const-string v1, "SELECT_SUGGESTION"

    const-string v2, "select_suggestion"

    invoke-direct {v0, v1, v7, v2}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->SELECT_SUGGESTION:LX/5oV;

    .line 1006991
    new-instance v0, LX/5oV;

    const-string v1, "CANCEL_COMPOSER"

    const-string v2, "cancel_composer"

    invoke-direct {v0, v1, v8, v2}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->CANCEL_COMPOSER:LX/5oV;

    .line 1006992
    new-instance v0, LX/5oV;

    const-string v1, "POST_WITH_PROMPT"

    const/4 v2, 0x5

    const-string v3, "post_prompt"

    invoke-direct {v0, v1, v2, v3}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->POST_WITH_PROMPT:LX/5oV;

    .line 1006993
    new-instance v0, LX/5oV;

    const-string v1, "POST_WITHOUT_PROMPT"

    const/4 v2, 0x6

    const-string v3, "post_without_prompt"

    invoke-direct {v0, v1, v2, v3}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->POST_WITHOUT_PROMPT:LX/5oV;

    .line 1006994
    new-instance v0, LX/5oV;

    const-string v1, "IMPRESSION"

    const/4 v2, 0x7

    const-string v3, "impression"

    invoke-direct {v0, v1, v2, v3}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->IMPRESSION:LX/5oV;

    .line 1006995
    new-instance v0, LX/5oV;

    const-string v1, "HIDE_FLYOUT"

    const/16 v2, 0x8

    const-string v3, "hide_suggestion"

    invoke-direct {v0, v1, v2, v3}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->HIDE_FLYOUT:LX/5oV;

    .line 1006996
    new-instance v0, LX/5oV;

    const-string v1, "SHOW_FLYOUT"

    const/16 v2, 0x9

    const-string v3, "show_suggestion"

    invoke-direct {v0, v1, v2, v3}, LX/5oV;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5oV;->SHOW_FLYOUT:LX/5oV;

    .line 1006997
    const/16 v0, 0xa

    new-array v0, v0, [LX/5oV;

    sget-object v1, LX/5oV;->OPEN_SELECTING_CONTENT:LX/5oV;

    aput-object v1, v0, v4

    sget-object v1, LX/5oV;->OPEN_SUGGESTING_CONTENT:LX/5oV;

    aput-object v1, v0, v5

    sget-object v1, LX/5oV;->CLOSE_SUGGESTION:LX/5oV;

    aput-object v1, v0, v6

    sget-object v1, LX/5oV;->SELECT_SUGGESTION:LX/5oV;

    aput-object v1, v0, v7

    sget-object v1, LX/5oV;->CANCEL_COMPOSER:LX/5oV;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5oV;->POST_WITH_PROMPT:LX/5oV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5oV;->POST_WITHOUT_PROMPT:LX/5oV;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5oV;->IMPRESSION:LX/5oV;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5oV;->HIDE_FLYOUT:LX/5oV;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/5oV;->SHOW_FLYOUT:LX/5oV;

    aput-object v2, v0, v1

    sput-object v0, LX/5oV;->$VALUES:[LX/5oV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1006998
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1006999
    iput-object p3, p0, LX/5oV;->name:Ljava/lang/String;

    .line 1007000
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5oV;
    .locals 1

    .prologue
    .line 1007001
    const-class v0, LX/5oV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5oV;

    return-object v0
.end method

.method public static values()[LX/5oV;
    .locals 1

    .prologue
    .line 1007002
    sget-object v0, LX/5oV;->$VALUES:[LX/5oV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5oV;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1007003
    iget-object v0, p0, LX/5oV;->name:Ljava/lang/String;

    return-object v0
.end method
