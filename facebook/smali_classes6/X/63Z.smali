.class public LX/63Z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/63Z;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1043377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1043378
    return-void
.end method

.method public static a(LX/0QB;)LX/63Z;
    .locals 3

    .prologue
    .line 1043379
    sget-object v0, LX/63Z;->a:LX/63Z;

    if-nez v0, :cond_1

    .line 1043380
    const-class v1, LX/63Z;

    monitor-enter v1

    .line 1043381
    :try_start_0
    sget-object v0, LX/63Z;->a:LX/63Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1043382
    if-eqz v2, :cond_0

    .line 1043383
    :try_start_1
    new-instance v0, LX/63Z;

    invoke-direct {v0}, LX/63Z;-><init>()V

    .line 1043384
    move-object v0, v0

    .line 1043385
    sput-object v0, LX/63Z;->a:LX/63Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1043386
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1043387
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1043388
    :cond_1
    sget-object v0, LX/63Z;->a:LX/63Z;

    return-object v0

    .line 1043389
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1043390
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1043391
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1043392
    if-eqz v1, :cond_0

    .line 1043393
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1043394
    :cond_0
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static b(Landroid/app/Activity;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1043395
    const v1, 0x7f0d00bb

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1043396
    if-eqz v1, :cond_0

    .line 1043397
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1043398
    :cond_0
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method
