.class public final LX/53s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0za;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/0za;",
        ">",
        "Ljava/lang/Object;",
        "LX/0za;"
    }
.end annotation


# static fields
.field private static final c:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater",
            "<",
            "LX/53s;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/4V7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4V7",
            "<",
            "LX/0za;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public volatile a:LX/53h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/53h",
            "<TT;>;"
        }
    .end annotation
.end field

.field public volatile b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 827531
    const-class v0, LX/53s;

    const-string v1, "b"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, LX/53s;->c:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    .line 827532
    new-instance v0, LX/53r;

    invoke-direct {v0}, LX/53r;-><init>()V

    sput-object v0, LX/53s;->d:LX/4V7;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 827533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827534
    sget-object v0, LX/53h;->e:LX/53d;

    invoke-virtual {v0}, LX/53d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/53h;

    move-object v0, v0

    .line 827535
    iput-object v0, p0, LX/53s;->a:LX/53h;

    .line 827536
    const/4 v0, 0x0

    iput v0, p0, LX/53s;->b:I

    .line 827537
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/0za;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 827538
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/53s;->b:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, LX/53s;->a:LX/53h;

    if-nez v0, :cond_2

    .line 827539
    :cond_0
    invoke-interface {p1}, LX/0za;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 827540
    const/4 v0, -0x1

    .line 827541
    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    .line 827542
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/53s;->a:LX/53h;

    .line 827543
    invoke-static {v0}, LX/53h;->e(LX/53h;)I

    move-result v1

    .line 827544
    sget v3, LX/53h;->d:I

    if-ge v1, v3, :cond_3

    .line 827545
    iget-object v3, v0, LX/53h;->f:LX/53f;

    iget-object v3, v3, LX/53f;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v3, v1, p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 827546
    :goto_1
    move v0, v1

    .line 827547
    iget v1, p0, LX/53s;->b:I

    if-ne v1, v2, :cond_1

    .line 827548
    invoke-interface {p1}, LX/0za;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 827549
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 827550
    :cond_3
    sget v3, LX/53h;->d:I

    rem-int v3, v1, v3

    .line 827551
    invoke-static {v0, v1}, LX/53h;->c(LX/53h;I)LX/53f;

    move-result-object v4

    iget-object v4, v4, LX/53f;->a:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v4, v3, p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public final declared-synchronized a(LX/4V7;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4V7",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 827552
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/53s;->b:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/53s;->a:LX/53h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 827553
    :cond_0
    const/4 v0, 0x0

    .line 827554
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LX/53s;->a:LX/53h;

    invoke-virtual {v0, p1, p2}, LX/53h;->a(LX/4V7;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 827555
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 827556
    sget-object v0, LX/53s;->c:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/53s;->a:LX/53h;

    if-eqz v0, :cond_0

    .line 827557
    iget-object v0, p0, LX/53s;->a:LX/53h;

    .line 827558
    if-nez v0, :cond_1

    .line 827559
    :goto_0
    iget-object v0, p0, LX/53s;->a:LX/53h;

    .line 827560
    const/4 v1, 0x0

    iput-object v1, p0, LX/53s;->a:LX/53h;

    .line 827561
    invoke-virtual {v0}, LX/53h;->b()V

    .line 827562
    :cond_0
    return-void

    .line 827563
    :cond_1
    sget-object v1, LX/53s;->d:LX/4V7;

    .line 827564
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/53h;->a(LX/4V7;I)I

    .line 827565
    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 827566
    iget v1, p0, LX/53s;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
