.class public final LX/6Hf;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/6Hh;


# direct methods
.method public constructor <init>(LX/6Hh;)V
    .locals 0

    .prologue
    .line 1072585
    iput-object p1, p0, LX/6Hf;->a:LX/6Hh;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 1072586
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1072587
    sget-object v0, LX/6Hh;->a:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown PublishingHandler msg type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1072588
    :goto_0
    return-void

    .line 1072589
    :pswitch_0
    iget-object v0, p0, LX/6Hf;->a:LX/6Hh;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/6Hh;->a$redex0(LX/6Hh;Ljava/util/List;)V

    goto :goto_0

    .line 1072590
    :pswitch_1
    monitor-enter p0

    .line 1072591
    :try_start_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1072592
    iget-object v0, p0, LX/6Hf;->a:LX/6Hh;

    iget-object v0, v0, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/6Hf;->a:LX/6Hh;

    iget-object v0, v0, LX/6Hh;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Hi;

    .line 1072593
    invoke-virtual {v0}, LX/6Hi;->b()LX/6Hi;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1072594
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1072595
    :cond_0
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 1072596
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1072597
    iget-object v1, p0, LX/6Hf;->a:LX/6Hh;

    invoke-static {v1, v0}, LX/6Hh;->a$redex0(LX/6Hh;Ljava/util/List;)V

    goto :goto_0

    .line 1072598
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
