.class public LX/6DM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/6DO;

.field public final d:LX/6DR;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/6DO;LX/6DR;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1064881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064882
    iput-object p1, p0, LX/6DM;->a:Ljava/util/concurrent/Executor;

    .line 1064883
    iput-object p3, p0, LX/6DM;->c:LX/6DO;

    .line 1064884
    iput-object p4, p0, LX/6DM;->d:LX/6DR;

    .line 1064885
    iput-object p2, p0, LX/6DM;->b:Ljava/util/concurrent/Executor;

    .line 1064886
    return-void
.end method

.method public static a(LX/6DM;LX/6DJ;)V
    .locals 3

    .prologue
    .line 1064887
    iget-object v0, p0, LX/6DM;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/browserextensions/common/autofill/AutofillDataProvider$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/browserextensions/common/autofill/AutofillDataProvider$4;-><init>(LX/6DM;LX/6DJ;)V

    const v2, -0x3486f72c    # -1.6320724E7f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1064888
    return-void
.end method

.method public static a$redex0(LX/6DM;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1064855
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1064856
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1064857
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1064858
    invoke-interface {v2, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1064859
    const-string v0, "name-autofill-data"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1064860
    iget-object v0, p0, LX/6DM;->c:LX/6DO;

    invoke-virtual {v0}, LX/6DO;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1064861
    :cond_0
    :goto_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1064862
    invoke-interface {v4, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1064863
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 1064864
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 1064865
    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;->a(Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1064866
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1064867
    :cond_3
    const-string v0, "telephone-autofill-data"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1064868
    iget-object v0, p0, LX/6DM;->c:LX/6DO;

    invoke-virtual {v0}, LX/6DO;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1064869
    :cond_4
    const-string v0, "address-autofill-data"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1064870
    iget-object v0, p0, LX/6DM;->c:LX/6DO;

    invoke-virtual {v0}, LX/6DO;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1064871
    :cond_5
    const-string v0, "string-autofill-data"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1064872
    iget-object v0, p0, LX/6DM;->c:LX/6DO;

    .line 1064873
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1064874
    invoke-static {}, Lcom/facebook/browserextensions/ipc/autofill/StringAutofillData;->e()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1064875
    invoke-virtual {v0, v1}, LX/6DO;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 1064876
    :cond_6
    move-object v0, v5

    .line 1064877
    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    .line 1064878
    :cond_7
    invoke-interface {v2, v5}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1064879
    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1064880
    return-object v4
.end method

.method public static b(LX/0QB;)LX/6DM;
    .locals 5

    .prologue
    .line 1064853
    new-instance v4, LX/6DM;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/6DO;->a(LX/0QB;)LX/6DO;

    move-result-object v2

    check-cast v2, LX/6DO;

    invoke-static {p0}, LX/6DR;->a(LX/0QB;)LX/6DR;

    move-result-object v3

    check-cast v3, LX/6DR;

    invoke-direct {v4, v0, v1, v2, v3}, LX/6DM;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/6DO;LX/6DR;)V

    .line 1064854
    return-object v4
.end method
