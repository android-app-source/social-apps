.class public final LX/5VB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 928265
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 928266
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 928267
    :goto_0
    return v1

    .line 928268
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 928269
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 928270
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 928271
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 928272
    const-string v8, "is_supported_carrier"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 928273
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 928274
    :cond_1
    const-string v8, "legal_terms_of_service_text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 928275
    invoke-static {p0, p1}, LX/5VA;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 928276
    :cond_2
    const-string v8, "logo"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 928277
    invoke-static {p0, p1}, LX/5VF;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 928278
    :cond_3
    const-string v8, "name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 928279
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 928280
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 928281
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 928282
    if-eqz v0, :cond_6

    .line 928283
    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 928284
    :cond_6
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 928285
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 928286
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 928287
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 928288
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 928289
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 928290
    if-eqz v0, :cond_0

    .line 928291
    const-string v1, "is_supported_carrier"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928292
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 928293
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928294
    if-eqz v0, :cond_1

    .line 928295
    const-string v1, "legal_terms_of_service_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928296
    invoke-static {p0, v0, p2}, LX/5VA;->a(LX/15i;ILX/0nX;)V

    .line 928297
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 928298
    if-eqz v0, :cond_2

    .line 928299
    const-string v1, "logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928300
    invoke-static {p0, v0, p2}, LX/5VF;->a(LX/15i;ILX/0nX;)V

    .line 928301
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 928302
    if-eqz v0, :cond_3

    .line 928303
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 928304
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 928305
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 928306
    return-void
.end method
