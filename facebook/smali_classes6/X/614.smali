.class public LX/614;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/60z;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public final b:I

.field private c:Landroid/media/MediaCodec$BufferInfo;

.field public d:Z


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;)V
    .locals 1

    .prologue
    .line 1039528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039529
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/614;->a:Ljava/lang/ref/WeakReference;

    .line 1039530
    iput p2, p0, LX/614;->b:I

    .line 1039531
    iput-object p3, p0, LX/614;->c:Landroid/media/MediaCodec$BufferInfo;

    .line 1039532
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/614;->d:Z

    .line 1039533
    return-void
.end method


# virtual methods
.method public final a()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 1039520
    iget-object v0, p0, LX/614;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public final a(IIJI)V
    .locals 7

    .prologue
    .line 1039524
    iget-object v0, p0, LX/614;->c:Landroid/media/MediaCodec$BufferInfo;

    if-nez v0, :cond_0

    .line 1039525
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, LX/614;->c:Landroid/media/MediaCodec$BufferInfo;

    .line 1039526
    :cond_0
    iget-object v1, p0, LX/614;->c:Landroid/media/MediaCodec$BufferInfo;

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Landroid/media/MediaCodec$BufferInfo;->set(IIJI)V

    .line 1039527
    return-void
.end method

.method public final b()Landroid/media/MediaCodec$BufferInfo;
    .locals 1

    .prologue
    .line 1039523
    iget-object v0, p0, LX/614;->c:Landroid/media/MediaCodec$BufferInfo;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1039522
    iget v0, p0, LX/614;->b:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1039521
    iget-boolean v0, p0, LX/614;->d:Z

    return v0
.end method
