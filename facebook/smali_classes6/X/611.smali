.class public final enum LX/611;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/611;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/611;

.field public static final enum DECODER:LX/611;

.field public static final enum ENCODER:LX/611;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1039499
    new-instance v0, LX/611;

    const-string v1, "DECODER"

    const-string v2, "decoder"

    invoke-direct {v0, v1, v3, v2}, LX/611;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/611;->DECODER:LX/611;

    .line 1039500
    new-instance v0, LX/611;

    const-string v1, "ENCODER"

    const-string v2, "encoder"

    invoke-direct {v0, v1, v4, v2}, LX/611;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/611;->ENCODER:LX/611;

    .line 1039501
    const/4 v0, 0x2

    new-array v0, v0, [LX/611;

    sget-object v1, LX/611;->DECODER:LX/611;

    aput-object v1, v0, v3

    sget-object v1, LX/611;->ENCODER:LX/611;

    aput-object v1, v0, v4

    sput-object v0, LX/611;->$VALUES:[LX/611;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1039492
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1039493
    iput-object p3, p0, LX/611;->value:Ljava/lang/String;

    .line 1039494
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/611;
    .locals 1

    .prologue
    .line 1039495
    const-class v0, LX/611;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/611;

    return-object v0
.end method

.method public static values()[LX/611;
    .locals 1

    .prologue
    .line 1039496
    sget-object v0, LX/611;->$VALUES:[LX/611;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/611;

    return-object v0
.end method


# virtual methods
.method public final isDecoder()Z
    .locals 2

    .prologue
    .line 1039497
    iget-object v0, p0, LX/611;->value:Ljava/lang/String;

    sget-object v1, LX/611;->DECODER:LX/611;

    iget-object v1, v1, LX/611;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isEncoder()Z
    .locals 2

    .prologue
    .line 1039498
    iget-object v0, p0, LX/611;->value:Ljava/lang/String;

    sget-object v1, LX/611;->ENCODER:LX/611;

    iget-object v1, v1, LX/611;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
