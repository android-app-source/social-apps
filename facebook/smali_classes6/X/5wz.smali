.class public final LX/5wz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 57

    .prologue
    .line 1026980
    const/16 v50, 0x0

    .line 1026981
    const/16 v49, 0x0

    .line 1026982
    const/16 v48, 0x0

    .line 1026983
    const/16 v47, 0x0

    .line 1026984
    const/16 v46, 0x0

    .line 1026985
    const/16 v45, 0x0

    .line 1026986
    const/16 v44, 0x0

    .line 1026987
    const/16 v43, 0x0

    .line 1026988
    const/16 v42, 0x0

    .line 1026989
    const/16 v41, 0x0

    .line 1026990
    const/16 v40, 0x0

    .line 1026991
    const/16 v39, 0x0

    .line 1026992
    const/16 v38, 0x0

    .line 1026993
    const/16 v37, 0x0

    .line 1026994
    const/16 v36, 0x0

    .line 1026995
    const/16 v35, 0x0

    .line 1026996
    const/16 v34, 0x0

    .line 1026997
    const/16 v33, 0x0

    .line 1026998
    const/16 v32, 0x0

    .line 1026999
    const/16 v31, 0x0

    .line 1027000
    const/16 v30, 0x0

    .line 1027001
    const/16 v29, 0x0

    .line 1027002
    const/16 v28, 0x0

    .line 1027003
    const-wide/16 v26, 0x0

    .line 1027004
    const/16 v25, 0x0

    .line 1027005
    const/16 v24, 0x0

    .line 1027006
    const/16 v23, 0x0

    .line 1027007
    const/16 v22, 0x0

    .line 1027008
    const/16 v21, 0x0

    .line 1027009
    const/16 v20, 0x0

    .line 1027010
    const/16 v19, 0x0

    .line 1027011
    const/16 v18, 0x0

    .line 1027012
    const/16 v17, 0x0

    .line 1027013
    const/16 v16, 0x0

    .line 1027014
    const/4 v15, 0x0

    .line 1027015
    const/4 v14, 0x0

    .line 1027016
    const/4 v13, 0x0

    .line 1027017
    const/4 v12, 0x0

    .line 1027018
    const/4 v11, 0x0

    .line 1027019
    const/4 v10, 0x0

    .line 1027020
    const/4 v9, 0x0

    .line 1027021
    const/4 v8, 0x0

    .line 1027022
    const/4 v7, 0x0

    .line 1027023
    const/4 v6, 0x0

    .line 1027024
    const/4 v5, 0x0

    .line 1027025
    const/4 v4, 0x0

    .line 1027026
    const/4 v3, 0x0

    .line 1027027
    const/4 v2, 0x0

    .line 1027028
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v51

    sget-object v52, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    if-eq v0, v1, :cond_32

    .line 1027029
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1027030
    const/4 v2, 0x0

    .line 1027031
    :goto_0
    return v2

    .line 1027032
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v52, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v52

    if-eq v2, v0, :cond_21

    .line 1027033
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1027034
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1027035
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v52

    sget-object v53, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 1027036
    const-string v52, "alternate_name"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1

    .line 1027037
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v51, v2

    goto :goto_1

    .line 1027038
    :cond_1
    const-string v52, "can_viewer_act_as_memorial_contact"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_2

    .line 1027039
    const/4 v2, 0x1

    .line 1027040
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    move/from16 v50, v20

    move/from16 v20, v2

    goto :goto_1

    .line 1027041
    :cond_2
    const-string v52, "can_viewer_block"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_3

    .line 1027042
    const/4 v2, 0x1

    .line 1027043
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    move/from16 v49, v19

    move/from16 v19, v2

    goto :goto_1

    .line 1027044
    :cond_3
    const-string v52, "can_viewer_message"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_4

    .line 1027045
    const/4 v2, 0x1

    .line 1027046
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    move/from16 v48, v18

    move/from16 v18, v2

    goto :goto_1

    .line 1027047
    :cond_4
    const-string v52, "can_viewer_poke"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_5

    .line 1027048
    const/4 v2, 0x1

    .line 1027049
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    move/from16 v47, v17

    move/from16 v17, v2

    goto/16 :goto_1

    .line 1027050
    :cond_5
    const-string v52, "can_viewer_post"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_6

    .line 1027051
    const/4 v2, 0x1

    .line 1027052
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    move/from16 v46, v16

    move/from16 v16, v2

    goto/16 :goto_1

    .line 1027053
    :cond_6
    const-string v52, "can_viewer_report"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_7

    .line 1027054
    const/4 v2, 0x1

    .line 1027055
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    move/from16 v45, v15

    move v15, v2

    goto/16 :goto_1

    .line 1027056
    :cond_7
    const-string v52, "cover_photo"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_8

    .line 1027057
    invoke-static/range {p0 .. p1}, LX/5wq;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v44, v2

    goto/16 :goto_1

    .line 1027058
    :cond_8
    const-string v52, "disable_profile_photo_expansion"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_9

    .line 1027059
    const/4 v2, 0x1

    .line 1027060
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    move/from16 v43, v14

    move v14, v2

    goto/16 :goto_1

    .line 1027061
    :cond_9
    const-string v52, "friends"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_a

    .line 1027062
    invoke-static/range {p0 .. p1}, LX/5zW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v42, v2

    goto/16 :goto_1

    .line 1027063
    :cond_a
    const-string v52, "friendship_status"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_b

    .line 1027064
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v41, v2

    goto/16 :goto_1

    .line 1027065
    :cond_b
    const-string v52, "id"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_c

    .line 1027066
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v40, v2

    goto/16 :goto_1

    .line 1027067
    :cond_c
    const-string v52, "is_followed_by_everyone"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_d

    .line 1027068
    const/4 v2, 0x1

    .line 1027069
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    move/from16 v39, v13

    move v13, v2

    goto/16 :goto_1

    .line 1027070
    :cond_d
    const-string v52, "is_memorialized"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_e

    .line 1027071
    const/4 v2, 0x1

    .line 1027072
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v38, v12

    move v12, v2

    goto/16 :goto_1

    .line 1027073
    :cond_e
    const-string v52, "is_partial"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_f

    .line 1027074
    const/4 v2, 0x1

    .line 1027075
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v37, v11

    move v11, v2

    goto/16 :goto_1

    .line 1027076
    :cond_f
    const-string v52, "is_verified"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_10

    .line 1027077
    const/4 v2, 0x1

    .line 1027078
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v36, v10

    move v10, v2

    goto/16 :goto_1

    .line 1027079
    :cond_10
    const-string v52, "is_viewer_coworker"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_11

    .line 1027080
    const/4 v2, 0x1

    .line 1027081
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v35, v7

    move v7, v2

    goto/16 :goto_1

    .line 1027082
    :cond_11
    const-string v52, "is_work_user"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_12

    .line 1027083
    const/4 v2, 0x1

    .line 1027084
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v34, v6

    move v6, v2

    goto/16 :goto_1

    .line 1027085
    :cond_12
    const-string v52, "name"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_13

    .line 1027086
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v33, v2

    goto/16 :goto_1

    .line 1027087
    :cond_13
    const-string v52, "posted_item_privacy_scope"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_14

    .line 1027088
    invoke-static/range {p0 .. p1}, LX/5R2;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v32, v2

    goto/16 :goto_1

    .line 1027089
    :cond_14
    const-string v52, "profile_intro_card"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_15

    .line 1027090
    invoke-static/range {p0 .. p1}, LX/5wv;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 1027091
    :cond_15
    const-string v52, "profile_photo"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_16

    .line 1027092
    invoke-static/range {p0 .. p1}, LX/5wx;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 1027093
    :cond_16
    const-string v52, "profile_picture"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_17

    .line 1027094
    invoke-static/range {p0 .. p1}, LX/3lU;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 1027095
    :cond_17
    const-string v52, "profile_picture_expiration_time"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_18

    .line 1027096
    const/4 v2, 0x1

    .line 1027097
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1027098
    :cond_18
    const-string v52, "profile_picture_is_silhouette"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_19

    .line 1027099
    const/4 v2, 0x1

    .line 1027100
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v28, v9

    move v9, v2

    goto/16 :goto_1

    .line 1027101
    :cond_19
    const-string v52, "profile_video"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1a

    .line 1027102
    invoke-static/range {p0 .. p1}, LX/5w2;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 1027103
    :cond_1a
    const-string v52, "secondary_subscribe_status"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1b

    .line 1027104
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 1027105
    :cond_1b
    const-string v52, "structured_name"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1c

    .line 1027106
    invoke-static/range {p0 .. p1}, LX/5wy;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 1027107
    :cond_1c
    const-string v52, "subscribe_status"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1d

    .line 1027108
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1027109
    :cond_1d
    const-string v52, "timeline_context_items"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1e

    .line 1027110
    invoke-static/range {p0 .. p1}, LX/5wn;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 1027111
    :cond_1e
    const-string v52, "url"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v52

    if-eqz v52, :cond_1f

    .line 1027112
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 1027113
    :cond_1f
    const-string v52, "viewer_can_see_profile_insights"

    move-object/from16 v0, v52

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 1027114
    const/4 v2, 0x1

    .line 1027115
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move/from16 v21, v8

    move v8, v2

    goto/16 :goto_1

    .line 1027116
    :cond_20
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1027117
    :cond_21
    const/16 v2, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1027118
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027119
    if-eqz v20, :cond_22

    .line 1027120
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027121
    :cond_22
    if-eqz v19, :cond_23

    .line 1027122
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027123
    :cond_23
    if-eqz v18, :cond_24

    .line 1027124
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027125
    :cond_24
    if-eqz v17, :cond_25

    .line 1027126
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027127
    :cond_25
    if-eqz v16, :cond_26

    .line 1027128
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027129
    :cond_26
    if-eqz v15, :cond_27

    .line 1027130
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027131
    :cond_27
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027132
    if-eqz v14, :cond_28

    .line 1027133
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027134
    :cond_28
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027135
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027136
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027137
    if-eqz v13, :cond_29

    .line 1027138
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027139
    :cond_29
    if-eqz v12, :cond_2a

    .line 1027140
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027141
    :cond_2a
    if-eqz v11, :cond_2b

    .line 1027142
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027143
    :cond_2b
    if-eqz v10, :cond_2c

    .line 1027144
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027145
    :cond_2c
    if-eqz v7, :cond_2d

    .line 1027146
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027147
    :cond_2d
    if-eqz v6, :cond_2e

    .line 1027148
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027149
    :cond_2e
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027150
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027151
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027152
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027153
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027154
    if-eqz v3, :cond_2f

    .line 1027155
    const/16 v3, 0x17

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1027156
    :cond_2f
    if-eqz v9, :cond_30

    .line 1027157
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027158
    :cond_30
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027159
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027160
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027161
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027162
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027163
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1027164
    if-eqz v8, :cond_31

    .line 1027165
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1027166
    :cond_31
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_32
    move/from16 v51, v50

    move/from16 v50, v49

    move/from16 v49, v48

    move/from16 v48, v47

    move/from16 v47, v46

    move/from16 v46, v45

    move/from16 v45, v44

    move/from16 v44, v43

    move/from16 v43, v42

    move/from16 v42, v41

    move/from16 v41, v40

    move/from16 v40, v39

    move/from16 v39, v38

    move/from16 v38, v37

    move/from16 v37, v36

    move/from16 v36, v35

    move/from16 v35, v34

    move/from16 v34, v33

    move/from16 v33, v32

    move/from16 v32, v31

    move/from16 v31, v30

    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v25

    move/from16 v25, v22

    move/from16 v22, v19

    move/from16 v19, v16

    move/from16 v16, v13

    move v13, v10

    move v10, v7

    move v7, v6

    move v6, v5

    move/from16 v54, v17

    move/from16 v17, v14

    move v14, v11

    move v11, v8

    move v8, v2

    move-wide/from16 v55, v26

    move/from16 v26, v23

    move/from16 v27, v24

    move/from16 v24, v21

    move/from16 v23, v20

    move/from16 v21, v18

    move/from16 v20, v54

    move/from16 v18, v15

    move v15, v12

    move v12, v9

    move v9, v3

    move v3, v4

    move-wide/from16 v4, v55

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0x1c

    const/16 v3, 0x1a

    const/16 v2, 0xa

    .line 1027167
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1027168
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1027169
    if-eqz v0, :cond_0

    .line 1027170
    const-string v1, "alternate_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027171
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1027172
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027173
    if-eqz v0, :cond_1

    .line 1027174
    const-string v1, "can_viewer_act_as_memorial_contact"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027175
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027176
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027177
    if-eqz v0, :cond_2

    .line 1027178
    const-string v1, "can_viewer_block"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027179
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027180
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027181
    if-eqz v0, :cond_3

    .line 1027182
    const-string v1, "can_viewer_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027183
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027184
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027185
    if-eqz v0, :cond_4

    .line 1027186
    const-string v1, "can_viewer_poke"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027187
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027188
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027189
    if-eqz v0, :cond_5

    .line 1027190
    const-string v1, "can_viewer_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027191
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027192
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027193
    if-eqz v0, :cond_6

    .line 1027194
    const-string v1, "can_viewer_report"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027195
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027196
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1027197
    if-eqz v0, :cond_7

    .line 1027198
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027199
    invoke-static {p0, v0, p2, p3}, LX/5wq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1027200
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027201
    if-eqz v0, :cond_8

    .line 1027202
    const-string v1, "disable_profile_photo_expansion"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027203
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027204
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1027205
    if-eqz v0, :cond_9

    .line 1027206
    const-string v1, "friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027207
    invoke-static {p0, v0, p2}, LX/5zW;->a(LX/15i;ILX/0nX;)V

    .line 1027208
    :cond_9
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1027209
    if-eqz v0, :cond_a

    .line 1027210
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027211
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1027212
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1027213
    if-eqz v0, :cond_b

    .line 1027214
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027215
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1027216
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027217
    if-eqz v0, :cond_c

    .line 1027218
    const-string v1, "is_followed_by_everyone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027219
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027220
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027221
    if-eqz v0, :cond_d

    .line 1027222
    const-string v1, "is_memorialized"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027223
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027224
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027225
    if-eqz v0, :cond_e

    .line 1027226
    const-string v1, "is_partial"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027227
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027228
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027229
    if-eqz v0, :cond_f

    .line 1027230
    const-string v1, "is_verified"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027231
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027232
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027233
    if-eqz v0, :cond_10

    .line 1027234
    const-string v1, "is_viewer_coworker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027235
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027236
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027237
    if-eqz v0, :cond_11

    .line 1027238
    const-string v1, "is_work_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027239
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027240
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1027241
    if-eqz v0, :cond_12

    .line 1027242
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027243
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1027244
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1027245
    if-eqz v0, :cond_13

    .line 1027246
    const-string v1, "posted_item_privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027247
    invoke-static {p0, v0, p2, p3}, LX/5R2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1027248
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1027249
    if-eqz v0, :cond_14

    .line 1027250
    const-string v1, "profile_intro_card"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027251
    invoke-static {p0, v0, p2, p3}, LX/5wv;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1027252
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1027253
    if-eqz v0, :cond_15

    .line 1027254
    const-string v1, "profile_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027255
    invoke-static {p0, v0, p2, p3}, LX/5wx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1027256
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1027257
    if-eqz v0, :cond_16

    .line 1027258
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027259
    invoke-static {p0, v0, p2}, LX/3lU;->a(LX/15i;ILX/0nX;)V

    .line 1027260
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1027261
    cmp-long v2, v0, v6

    if-eqz v2, :cond_17

    .line 1027262
    const-string v2, "profile_picture_expiration_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027263
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1027264
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027265
    if-eqz v0, :cond_18

    .line 1027266
    const-string v1, "profile_picture_is_silhouette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027267
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027268
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1027269
    if-eqz v0, :cond_19

    .line 1027270
    const-string v1, "profile_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027271
    invoke-static {p0, v0, p2, p3}, LX/5w2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1027272
    :cond_19
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1027273
    if-eqz v0, :cond_1a

    .line 1027274
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027275
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1027276
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1027277
    if-eqz v0, :cond_1b

    .line 1027278
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027279
    invoke-static {p0, v0, p2, p3}, LX/5wy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1027280
    :cond_1b
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1027281
    if-eqz v0, :cond_1c

    .line 1027282
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027283
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1027284
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1027285
    if-eqz v0, :cond_1d

    .line 1027286
    const-string v1, "timeline_context_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027287
    invoke-static {p0, v0, p2, p3}, LX/5wn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1027288
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1027289
    if-eqz v0, :cond_1e

    .line 1027290
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027291
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1027292
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1027293
    if-eqz v0, :cond_1f

    .line 1027294
    const-string v1, "viewer_can_see_profile_insights"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1027295
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1027296
    :cond_1f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1027297
    return-void
.end method
