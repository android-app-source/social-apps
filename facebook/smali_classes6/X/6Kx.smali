.class public final enum LX/6Kx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Kx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Kx;

.field public static final enum PREPARED:LX/6Kx;

.field public static final enum STARTED:LX/6Kx;

.field public static final enum STOPPED:LX/6Kx;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1077766
    new-instance v0, LX/6Kx;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v2}, LX/6Kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Kx;->STOPPED:LX/6Kx;

    .line 1077767
    new-instance v0, LX/6Kx;

    const-string v1, "PREPARED"

    invoke-direct {v0, v1, v3}, LX/6Kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Kx;->PREPARED:LX/6Kx;

    .line 1077768
    new-instance v0, LX/6Kx;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v4}, LX/6Kx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Kx;->STARTED:LX/6Kx;

    .line 1077769
    const/4 v0, 0x3

    new-array v0, v0, [LX/6Kx;

    sget-object v1, LX/6Kx;->STOPPED:LX/6Kx;

    aput-object v1, v0, v2

    sget-object v1, LX/6Kx;->PREPARED:LX/6Kx;

    aput-object v1, v0, v3

    sget-object v1, LX/6Kx;->STARTED:LX/6Kx;

    aput-object v1, v0, v4

    sput-object v0, LX/6Kx;->$VALUES:[LX/6Kx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1077770
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Kx;
    .locals 1

    .prologue
    .line 1077771
    const-class v0, LX/6Kx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Kx;

    return-object v0
.end method

.method public static values()[LX/6Kx;
    .locals 1

    .prologue
    .line 1077772
    sget-object v0, LX/6Kx;->$VALUES:[LX/6Kx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Kx;

    return-object v0
.end method
