.class public LX/6Kv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements LX/6Kd;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private final a:Landroid/view/SurfaceView;

.field private b:LX/6Kl;

.field private c:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Landroid/view/SurfaceView;)V
    .locals 2

    .prologue
    .line 1077712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077713
    iput-object p1, p0, LX/6Kv;->a:Landroid/view/SurfaceView;

    .line 1077714
    iget-object v0, p0, LX/6Kv;->a:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 1077715
    iget-object v0, p0, LX/6Kv;->a:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    .line 1077716
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1077717
    iput-object v0, p0, LX/6Kv;->c:Landroid/view/Surface;

    .line 1077718
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/6Kl;)V
    .locals 2

    .prologue
    .line 1077708
    iput-object p1, p0, LX/6Kv;->b:LX/6Kl;

    .line 1077709
    iget-object v0, p0, LX/6Kv;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 1077710
    iget-object v0, p0, LX/6Kv;->b:LX/6Kl;

    iget-object v1, p0, LX/6Kv;->c:Landroid/view/Surface;

    invoke-virtual {v0, p0, v1}, LX/6Kl;->a(LX/6Kd;Landroid/view/Surface;)V

    .line 1077711
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1077705
    invoke-virtual {p0}, LX/6Kv;->dE_()V

    .line 1077706
    iget-object v0, p0, LX/6Kv;->a:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 1077707
    return-void
.end method

.method public final dE_()V
    .locals 1

    .prologue
    .line 1077703
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Kv;->b:LX/6Kl;

    .line 1077704
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1077702
    return-void
.end method

.method public final getHeight()I
    .locals 1

    .prologue
    .line 1077701
    iget-object v0, p0, LX/6Kv;->a:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHeight()I

    move-result v0

    return v0
.end method

.method public final getWidth()I
    .locals 1

    .prologue
    .line 1077688
    iget-object v0, p0, LX/6Kv;->a:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getWidth()I

    move-result v0

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 1077700
    iget-object v0, p0, LX/6Kv;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Kv;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    .prologue
    .line 1077697
    iget-object v0, p0, LX/6Kv;->b:LX/6Kl;

    if-eqz v0, :cond_0

    .line 1077698
    iget-object v0, p0, LX/6Kv;->b:LX/6Kl;

    invoke-virtual {v0, p0}, LX/6Kl;->a(LX/6Kd;)V

    .line 1077699
    :cond_0
    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 1077693
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, LX/6Kv;->c:Landroid/view/Surface;

    .line 1077694
    iget-object v0, p0, LX/6Kv;->b:LX/6Kl;

    if-eqz v0, :cond_0

    .line 1077695
    iget-object v0, p0, LX/6Kv;->b:LX/6Kl;

    iget-object v1, p0, LX/6Kv;->c:Landroid/view/Surface;

    invoke-virtual {v0, p0, v1}, LX/6Kl;->a(LX/6Kd;Landroid/view/Surface;)V

    .line 1077696
    :cond_0
    return-void
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 1077689
    iget-object v0, p0, LX/6Kv;->b:LX/6Kl;

    if-eqz v0, :cond_0

    .line 1077690
    iget-object v0, p0, LX/6Kv;->b:LX/6Kl;

    invoke-virtual {v0, p0}, LX/6Kl;->b(LX/6Kd;)V

    .line 1077691
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Kv;->c:Landroid/view/Surface;

    .line 1077692
    return-void
.end method
