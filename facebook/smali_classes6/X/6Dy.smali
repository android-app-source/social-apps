.class public abstract LX/6Dy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DATA_",
        "LOADER::Lcom/facebook/payments/checkout/CheckoutDataLoader;",
        "DATA_MUTATOR::",
        "LX/6qd;",
        "ORDER_STATUS_HAND",
        "LER::Lcom/facebook/payments/checkout/CheckoutOrderStatusHandler;",
        "ON_ACTIVITY_RESU",
        "LT_HANDLER::Lcom/facebook/payments/checkout/CheckoutOnActivityResultHandler;",
        "SUB_SCREEN_PARAMS_GENERATOR::",
        "LX/6E0;",
        "ROWS_GENERATOR::",
        "LX/6Ds;",
        "SENDER::",
        "LX/6Du;",
        "STATE_MACHINE_ORGANIZER::",
        "LX/6Dw;",
        "STATE_MACHINE_HAND",
        "LER::Lcom/facebook/payments/checkout/statemachine/CheckoutStateMachineHandler;",
        "ROW_VIEW_HO",
        "LDER_FACTORY::Lcom/facebook/payments/checkout/recyclerview/CheckoutRowViewHolderFactory;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/6qw;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TDATA_",
            "LOADER;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TDATA_MUTATOR;>;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TORDER_STATUS_HAND",
            "LER;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TON_ACTIVITY_RESU",
            "LT_HANDLER;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSUB_SCREEN_PARAMS_GENERATOR;>;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TROWS_GENERATOR;>;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSENDER;>;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSTATE_MACHINE_ORGANIZER;>;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TSTATE_MACHINE_HAND",
            "LER;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<TROW_VIEW_HO",
            "LDER_FACTORY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6qw;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6qw;",
            "LX/0Ot",
            "<TDATA_",
            "LOADER;",
            ">;",
            "LX/0Ot",
            "<TDATA_MUTATOR;>;",
            "LX/0Ot",
            "<TORDER_STATUS_HAND",
            "LER;",
            ">;",
            "LX/0Ot",
            "<TON_ACTIVITY_RESU",
            "LT_HANDLER;",
            ">;",
            "LX/0Ot",
            "<TSUB_SCREEN_PARAMS_GENERATOR;>;",
            "LX/0Ot",
            "<TROWS_GENERATOR;>;",
            "LX/0Ot",
            "<TSENDER;>;",
            "LX/0Ot",
            "<TSTATE_MACHINE_ORGANIZER;>;",
            "LX/0Ot",
            "<TSTATE_MACHINE_HAND",
            "LER;",
            ">;",
            "LX/0Ot",
            "<TROW_VIEW_HO",
            "LDER_FACTORY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1065761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065762
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6qw;

    iput-object v0, p0, LX/6Dy;->a:LX/6qw;

    .line 1065763
    iput-object p2, p0, LX/6Dy;->b:LX/0Ot;

    .line 1065764
    iput-object p3, p0, LX/6Dy;->c:LX/0Ot;

    .line 1065765
    iput-object p4, p0, LX/6Dy;->d:LX/0Ot;

    .line 1065766
    iput-object p5, p0, LX/6Dy;->e:LX/0Ot;

    .line 1065767
    iput-object p6, p0, LX/6Dy;->f:LX/0Ot;

    .line 1065768
    iput-object p7, p0, LX/6Dy;->g:LX/0Ot;

    .line 1065769
    iput-object p8, p0, LX/6Dy;->h:LX/0Ot;

    .line 1065770
    iput-object p9, p0, LX/6Dy;->i:LX/0Ot;

    .line 1065771
    iput-object p10, p0, LX/6Dy;->j:LX/0Ot;

    .line 1065772
    iput-object p11, p0, LX/6Dy;->k:LX/0Ot;

    .line 1065773
    return-void
.end method
