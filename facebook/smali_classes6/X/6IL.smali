.class public LX/6IL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field private final b:Landroid/graphics/Rect;

.field public c:LX/6IC;

.field public d:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1073808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1073809
    iput-object p1, p0, LX/6IL;->a:Landroid/view/View;

    .line 1073810
    iput-object p2, p0, LX/6IL;->b:Landroid/graphics/Rect;

    .line 1073811
    return-void
.end method

.method public static c(LX/6IL;)V
    .locals 8

    .prologue
    const/16 v7, 0xc

    const/16 v6, 0xb

    const/16 v5, 0xa

    const/16 v4, 0x9

    const/4 v3, 0x0

    .line 1073852
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1073853
    iget-object v1, p0, LX/6IL;->c:LX/6IC;

    sget-object v2, LX/6IC;->TOP_LEFT:LX/6IC;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/6IL;->c:LX/6IC;

    sget-object v2, LX/6IC;->BOTTOM_LEFT:LX/6IC;

    if-ne v1, v2, :cond_2

    .line 1073854
    :cond_0
    iget-object v1, p0, LX/6IL;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1073855
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1073856
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1073857
    invoke-virtual {v0, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1073858
    :goto_0
    iget-object v1, p0, LX/6IL;->c:LX/6IC;

    sget-object v2, LX/6IC;->TOP_LEFT:LX/6IC;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, LX/6IL;->c:LX/6IC;

    sget-object v2, LX/6IC;->TOP_RIGHT:LX/6IC;

    if-ne v1, v2, :cond_3

    .line 1073859
    :cond_1
    iget-object v1, p0, LX/6IL;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1073860
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1073861
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1073862
    invoke-virtual {v0, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1073863
    :goto_1
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1073864
    return-void

    .line 1073865
    :cond_2
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1073866
    iget-object v1, p0, LX/6IL;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1073867
    invoke-virtual {v0, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1073868
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_0

    .line 1073869
    :cond_3
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1073870
    iget-object v1, p0, LX/6IL;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1073871
    invoke-virtual {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1073872
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1073849
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1073850
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1073851
    return-void
.end method

.method public final a(FZ)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x190

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x2

    const/4 v1, 0x1

    .line 1073819
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRotation()F

    move-result v0

    invoke-static {v0, p1}, LX/6IF;->a(FF)F

    move-result v3

    .line 1073820
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v4

    .line 1073821
    cmpl-float v0, v4, v8

    if-eqz v0, :cond_0

    move v0, v1

    .line 1073822
    :goto_0
    iget-object v5, p0, LX/6IL;->a:Landroid/view/View;

    iget-object v6, p0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setPivotX(F)V

    .line 1073823
    iget-object v5, p0, LX/6IL;->a:Landroid/view/View;

    iget-object v6, p0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setPivotY(F)V

    .line 1073824
    invoke-virtual {p0}, LX/6IL;->b()V

    .line 1073825
    if-ne p2, v0, :cond_2

    .line 1073826
    if-eqz p2, :cond_1

    .line 1073827
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    const-string v2, "alpha"

    new-array v4, v7, [F

    fill-array-data v4, :array_0

    invoke-static {v0, v2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    .line 1073828
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1073829
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 1073830
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v7}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 1073831
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    new-instance v1, LX/6IK;

    invoke-direct {v1, p0, v3}, LX/6IK;-><init>(LX/6IL;F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1073832
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1073833
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1073834
    goto :goto_0

    .line 1073835
    :cond_1
    invoke-static {p0}, LX/6IL;->c(LX/6IL;)V

    .line 1073836
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setRotation(F)V

    .line 1073837
    const/4 v0, 0x0

    iput-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    goto :goto_1

    .line 1073838
    :cond_2
    if-eqz p2, :cond_3

    .line 1073839
    new-instance v0, LX/6IK;

    invoke-direct {v0, p0, v3}, LX/6IK;-><init>(LX/6IL;F)V

    .line 1073840
    invoke-static {v0}, LX/6IK;->a$redex0(LX/6IK;)V

    .line 1073841
    iget-object v3, p0, LX/6IL;->a:Landroid/view/View;

    const-string v5, "alpha"

    new-array v6, v7, [F

    aput v4, v6, v2

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v6, v1

    invoke-static {v3, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    .line 1073842
    iget-object v1, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1073843
    iget-object v1, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1073844
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    .line 1073845
    :cond_3
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    const-string v5, "alpha"

    new-array v6, v7, [F

    aput v4, v6, v2

    aput v8, v6, v1

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    .line 1073846
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1073847
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    new-instance v1, LX/6IK;

    invoke-direct {v1, p0, v3}, LX/6IK;-><init>(LX/6IL;F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1073848
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1073817
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1073818
    return-void
.end method

.method public final a(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1073815
    iget-object v0, p0, LX/6IL;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1073816
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1073812
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 1073813
    iget-object v0, p0, LX/6IL;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1073814
    :cond_0
    return-void
.end method
