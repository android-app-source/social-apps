.class public LX/6If;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/graphics/Rect;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field public g:I


# direct methods
.method public constructor <init>(III)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1074361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1074362
    iput p1, p0, LX/6If;->d:I

    .line 1074363
    iput p2, p0, LX/6If;->e:I

    .line 1074364
    iput p3, p0, LX/6If;->f:I

    .line 1074365
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, LX/6If;->d:I

    iget v2, p0, LX/6If;->e:I

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/6If;->a:Landroid/graphics/Rect;

    .line 1074366
    iget v0, p0, LX/6If;->d:I

    iget v1, p0, LX/6If;->f:I

    div-int/2addr v0, v1

    .line 1074367
    iget v1, p0, LX/6If;->e:I

    iget v2, p0, LX/6If;->f:I

    div-int/2addr v1, v2

    .line 1074368
    iget v2, p0, LX/6If;->d:I

    sub-int v0, v2, v0

    iput v0, p0, LX/6If;->b:I

    .line 1074369
    iget v0, p0, LX/6If;->e:I

    sub-int/2addr v0, v1

    iput v0, p0, LX/6If;->c:I

    .line 1074370
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    .line 1074371
    if-gez p1, :cond_0

    .line 1074372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Zoom level cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074373
    :cond_0
    iget v0, p0, LX/6If;->f:I

    if-le p1, v0, :cond_1

    .line 1074374
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Zoom level cannot be larger then the maximum: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/6If;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074375
    :cond_1
    iput p1, p0, LX/6If;->g:I

    .line 1074376
    iget v0, p0, LX/6If;->b:I

    div-int/lit8 v0, v0, 0x64

    iget v1, p0, LX/6If;->g:I

    mul-int/2addr v0, v1

    .line 1074377
    iget v1, p0, LX/6If;->c:I

    div-int/lit8 v1, v1, 0x64

    iget v2, p0, LX/6If;->g:I

    mul-int/2addr v1, v2

    .line 1074378
    iget-object v2, p0, LX/6If;->a:Landroid/graphics/Rect;

    iget v3, p0, LX/6If;->d:I

    sub-int/2addr v3, v0

    iget v4, p0, LX/6If;->e:I

    sub-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1074379
    return-void
.end method
