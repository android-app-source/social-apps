.class public LX/6NZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0aG;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2It;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1083659
    const-class v0, LX/6NZ;

    sput-object v0, LX/6NZ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0Ot;LX/0Ot;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0Ot",
            "<",
            "LX/2It;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1083660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1083661
    iput-object p1, p0, LX/6NZ;->b:LX/0aG;

    .line 1083662
    iput-object p2, p0, LX/6NZ;->c:LX/0Ot;

    .line 1083663
    iput-object p3, p0, LX/6NZ;->d:LX/0Ot;

    .line 1083664
    iput-object p4, p0, LX/6NZ;->e:Ljava/util/concurrent/Executor;

    .line 1083665
    return-void
.end method
