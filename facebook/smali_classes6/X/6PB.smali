.class public abstract LX/6PB;
.super LX/3dH;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final b:LX/3d4;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3d4",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/3d2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3d2",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1086059
    invoke-direct {p0}, LX/3dH;-><init>()V

    .line 1086060
    new-instance v0, LX/6PE;

    invoke-direct {v0, p0}, LX/6PE;-><init>(LX/6PB;)V

    iput-object v0, p0, LX/6PB;->b:LX/3d4;

    .line 1086061
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086062
    new-instance v0, LX/3d2;

    const-class v1, Lcom/facebook/graphql/model/GraphQLPage;

    iget-object v2, p0, LX/6PB;->b:LX/3d4;

    invoke-direct {v0, v1, v2}, LX/3d2;-><init>(Ljava/lang/Class;LX/3d4;)V

    iput-object v0, p0, LX/6PB;->d:LX/3d2;

    .line 1086063
    iput-object p1, p0, LX/6PB;->e:Ljava/lang/String;

    .line 1086064
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/graphql/model/GraphQLPage;)Lcom/facebook/graphql/model/GraphQLPage;
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 1086065
    iget-object v0, p0, LX/6PB;->d:LX/3d2;

    invoke-virtual {v0, p1}, LX/3d2;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086066
    iget-object v0, p0, LX/6PB;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
