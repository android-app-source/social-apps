.class public final LX/6PJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

.field public final synthetic b:LX/3iV;


# direct methods
.method public constructor <init>(LX/3iV;Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;)V
    .locals 0

    .prologue
    .line 1086113
    iput-object p1, p0, LX/6PJ;->b:LX/3iV;

    iput-object p2, p0, LX/6PJ;->a:Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1086114
    iget-object v0, p0, LX/6PJ;->b:LX/3iV;

    iget-object v1, p0, LX/6PJ;->a:Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    .line 1086115
    new-instance v3, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;

    iget-object v4, v1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a:Ljava/lang/String;

    iget-boolean v5, v1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->b:Z

    iget-object v6, v1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->c:Lcom/facebook/graphql/model/GraphQLActor;

    iget-object v7, v1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    const/4 v8, 0x0

    iget-object v9, v1, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->e:Ljava/lang/String;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/api/ufiservices/common/ToggleLikeParams;-><init>(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    move-object v1, v3

    .line 1086116
    const-string v2, "feed_toggle_page_like"

    invoke-static {v0, v1, v2}, LX/3iV;->a$redex0(LX/3iV;Lcom/facebook/api/ufiservices/common/ToggleLikeParams;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
