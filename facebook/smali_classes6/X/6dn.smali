.class public final LX/6dn;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0sv;

.field private static final b:LX/0sv;

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    .line 1116383
    new-instance v0, LX/0su;

    sget-object v1, LX/6dm;->a:LX/0U1;

    sget-object v2, LX/6dm;->b:LX/0U1;

    sget-object v3, LX/6dm;->f:LX/0U1;

    invoke-static {v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/6dn;->a:LX/0sv;

    .line 1116384
    new-instance v0, LX/2Qn;

    sget-object v1, LX/6dm;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const-string v2, "threads"

    sget-object v3, LX/6dq;->a:LX/0U1;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    const-string v4, "ON DELETE CASCADE"

    invoke-direct {v0, v1, v2, v3, v4}, LX/2Qn;-><init>(LX/0Px;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    sput-object v0, LX/6dn;->b:LX/0sv;

    .line 1116385
    sget-object v0, LX/6dm;->a:LX/0U1;

    sget-object v1, LX/6dm;->b:LX/0U1;

    sget-object v2, LX/6dm;->c:LX/0U1;

    sget-object v3, LX/6dm;->d:LX/0U1;

    sget-object v4, LX/6dm;->e:LX/0U1;

    sget-object v5, LX/6dm;->f:LX/0U1;

    sget-object v6, LX/6dm;->k:LX/0U1;

    sget-object v7, LX/6dm;->g:LX/0U1;

    sget-object v8, LX/6dm;->h:LX/0U1;

    sget-object v9, LX/6dm;->i:LX/0U1;

    sget-object v10, LX/6dm;->j:LX/0U1;

    sget-object v11, LX/6dm;->l:LX/0U1;

    const/4 v12, 0x0

    new-array v12, v12, [LX/0U1;

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/6dn;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 1116380
    const-string v0, "thread_participants"

    sget-object v1, LX/6dn;->c:LX/0Px;

    sget-object v2, LX/6dn;->a:LX/0sv;

    sget-object v3, LX/6dn;->b:LX/0sv;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 1116381
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    .prologue
    .line 1116382
    return-void
.end method
