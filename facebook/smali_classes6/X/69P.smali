.class public LX/69P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/Comparator;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1058028
    new-instance v0, LX/69P;

    invoke-direct {v0}, LX/69P;-><init>()V

    sput-object v0, LX/69P;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1058029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1058030
    check-cast p1, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    check-cast p2, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 1058031
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0ql;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
