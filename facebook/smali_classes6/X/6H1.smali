.class public final LX/6H1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6H2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1071199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/6H1;

    const/16 v1, 0x1876

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, LX/6H1;->a:LX/0Or;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x741ee605

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1071200
    const-string v0, "com.facebook.bugreporter.scheduler.AlarmsBroadcastReceiver.RETRY_UPLOAD"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1071201
    const/16 v0, 0x27

    const v2, -0x25cb3cc8

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1071202
    :goto_0
    return-void

    .line 1071203
    :cond_0
    invoke-static {p0, p1}, LX/6H1;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1071204
    iget-object v0, p0, LX/6H1;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6H2;

    .line 1071205
    invoke-virtual {v0}, LX/6H2;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1071206
    const v0, -0x5a0a709c

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0
.end method
