.class public final LX/5Gi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 889056
    const-wide/16 v12, 0x0

    .line 889057
    const-wide/16 v10, 0x0

    .line 889058
    const-wide/16 v8, 0x0

    .line 889059
    const-wide/16 v6, 0x0

    .line 889060
    const/4 v5, 0x0

    .line 889061
    const/4 v4, 0x0

    .line 889062
    const/4 v3, 0x0

    .line 889063
    const/4 v2, 0x0

    .line 889064
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 889065
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 889066
    const/4 v2, 0x0

    .line 889067
    :goto_0
    return v2

    .line 889068
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_5

    .line 889069
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 889070
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 889071
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 889072
    const-string v6, "east"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 889073
    const/4 v2, 0x1

    .line 889074
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 889075
    :cond_1
    const-string v6, "north"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 889076
    const/4 v2, 0x1

    .line 889077
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v16, v6

    goto :goto_1

    .line 889078
    :cond_2
    const-string v6, "south"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 889079
    const/4 v2, 0x1

    .line 889080
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide v14, v6

    goto :goto_1

    .line 889081
    :cond_3
    const-string v6, "west"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 889082
    const/4 v2, 0x1

    .line 889083
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto :goto_1

    .line 889084
    :cond_4
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 889085
    :cond_5
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 889086
    if-eqz v3, :cond_6

    .line 889087
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 889088
    :cond_6
    if-eqz v10, :cond_7

    .line 889089
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 889090
    :cond_7
    if-eqz v9, :cond_8

    .line 889091
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 889092
    :cond_8
    if-eqz v8, :cond_9

    .line 889093
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 889094
    :cond_9
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move-wide v14, v8

    move-wide/from16 v16, v10

    move v10, v4

    move v8, v2

    move v9, v3

    move v3, v5

    move-wide v4, v12

    move-wide v12, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 889037
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 889038
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 889039
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 889040
    const-string v2, "east"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 889041
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 889042
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 889043
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 889044
    const-string v2, "north"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 889045
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 889046
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 889047
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 889048
    const-string v2, "south"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 889049
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 889050
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 889051
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 889052
    const-string v2, "west"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 889053
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 889054
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 889055
    return-void
.end method
