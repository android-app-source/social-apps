.class public LX/5qN;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "JSCHeapCapture"
.end annotation


# static fields
.field private static final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/5qN;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/react/devsupport/JSCHeapCapture$HeapCapture;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/5qM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1009179
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, LX/5qN;->c:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1009180
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1009181
    iput-object v0, p0, LX/5qN;->a:Lcom/facebook/react/devsupport/JSCHeapCapture$HeapCapture;

    .line 1009182
    iput-object v0, p0, LX/5qN;->b:LX/5qM;

    .line 1009183
    return-void
.end method

.method private static declared-synchronized a(LX/5qN;)V
    .locals 3

    .prologue
    .line 1009184
    const-class v1, LX/5qN;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5qN;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1009185
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "a JSCHeapCapture registered more than once"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009186
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1009187
    :cond_0
    :try_start_1
    sget-object v0, LX/5qN;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1009188
    monitor-exit v1

    return-void
.end method

.method private static declared-synchronized b(LX/5qN;)V
    .locals 2

    .prologue
    .line 1009189
    const-class v1, LX/5qN;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/5qN;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009190
    monitor-exit v1

    return-void

    .line 1009191
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized captureComplete(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1009192
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/5qN;->b:LX/5qM;

    if-eqz v0, :cond_0

    .line 1009193
    if-nez p2, :cond_1

    .line 1009194
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1009195
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/5qN;->b:LX/5qM;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1009196
    :cond_0
    monitor-exit p0

    return-void

    .line 1009197
    :cond_1
    :try_start_1
    new-instance v0, LX/5qL;

    invoke-direct {v0, p2}, LX/5qL;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1009198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1009199
    invoke-super {p0}, LX/5pb;->d()V

    .line 1009200
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1009201
    const-class v1, Lcom/facebook/react/devsupport/JSCHeapCapture$HeapCapture;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/devsupport/JSCHeapCapture$HeapCapture;

    iput-object v0, p0, LX/5qN;->a:Lcom/facebook/react/devsupport/JSCHeapCapture$HeapCapture;

    .line 1009202
    invoke-static {p0}, LX/5qN;->a(LX/5qN;)V

    .line 1009203
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1009204
    invoke-super {p0}, LX/5pb;->f()V

    .line 1009205
    invoke-static {p0}, LX/5qN;->b(LX/5qN;)V

    .line 1009206
    const/4 v0, 0x0

    iput-object v0, p0, LX/5qN;->a:Lcom/facebook/react/devsupport/JSCHeapCapture$HeapCapture;

    .line 1009207
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1009208
    const-string v0, "JSCHeapCapture"

    return-object v0
.end method
