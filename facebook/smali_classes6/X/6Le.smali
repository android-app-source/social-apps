.class public LX/6Le;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1078616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078617
    return-void
.end method


# virtual methods
.method public final a(ILX/03R;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SharedPreferencesUse"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1078618
    invoke-static {}, LX/00y;->a()Landroid/app/ActivityThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActivityThread;->getApplication()Landroid/app/Application;

    move-result-object v1

    move-object v1, v1

    .line 1078619
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 1078620
    :goto_0
    if-nez v1, :cond_2

    .line 1078621
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Context not available"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078622
    :cond_0
    :goto_1
    return-void

    .line 1078623
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1078624
    :cond_2
    const-string v2, "terminate_handler_flags_store"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1078625
    const/16 v2, 0x62

    if-ne p1, v2, :cond_0

    .line 1078626
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "android_enable_terminate_handler"

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne p2, v3, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1
.end method
