.class public final enum LX/5Rn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Rn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Rn;

.field public static final enum NORMAL:LX/5Rn;

.field public static final enum SAVE_DRAFT:LX/5Rn;

.field public static final enum SCHEDULE_POST:LX/5Rn;


# instance fields
.field private final mContentType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 916670
    new-instance v0, LX/5Rn;

    const-string v1, "NORMAL"

    const-string v2, "PUBLISH"

    invoke-direct {v0, v1, v3, v2}, LX/5Rn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    .line 916671
    new-instance v0, LX/5Rn;

    const-string v1, "SCHEDULE_POST"

    const-string v2, "SCHEDULED"

    invoke-direct {v0, v1, v4, v2}, LX/5Rn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    .line 916672
    new-instance v0, LX/5Rn;

    const-string v1, "SAVE_DRAFT"

    const-string v2, "DRAFT"

    invoke-direct {v0, v1, v5, v2}, LX/5Rn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5Rn;->SAVE_DRAFT:LX/5Rn;

    .line 916673
    const/4 v0, 0x3

    new-array v0, v0, [LX/5Rn;

    sget-object v1, LX/5Rn;->NORMAL:LX/5Rn;

    aput-object v1, v0, v3

    sget-object v1, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    aput-object v1, v0, v4

    sget-object v1, LX/5Rn;->SAVE_DRAFT:LX/5Rn;

    aput-object v1, v0, v5

    sput-object v0, LX/5Rn;->$VALUES:[LX/5Rn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 916674
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 916675
    iput-object p3, p0, LX/5Rn;->mContentType:Ljava/lang/String;

    .line 916676
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Rn;
    .locals 1

    .prologue
    .line 916677
    const-class v0, LX/5Rn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rn;

    return-object v0
.end method

.method public static values()[LX/5Rn;
    .locals 1

    .prologue
    .line 916678
    sget-object v0, LX/5Rn;->$VALUES:[LX/5Rn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Rn;

    return-object v0
.end method


# virtual methods
.method public final getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 916679
    iget-object v0, p0, LX/5Rn;->mContentType:Ljava/lang/String;

    return-object v0
.end method
