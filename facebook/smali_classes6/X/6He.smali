.class public LX/6He;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$FaceDetectionListener;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Landroid/hardware/Camera;

.field private final c:LX/03V;

.field public d:I

.field public e:Z

.field public f:Z

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/camera/facetracking/FaceDetectionManager$FaceDetectionListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1072584
    const-class v0, LX/6He;

    sput-object v0, LX/6He;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/hardware/Camera;LX/03V;)V
    .locals 1

    .prologue
    .line 1072561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072562
    const/4 v0, -0x1

    iput v0, p0, LX/6He;->d:I

    .line 1072563
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6He;->e:Z

    .line 1072564
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6He;->f:Z

    .line 1072565
    iput-object p1, p0, LX/6He;->b:Landroid/hardware/Camera;

    .line 1072566
    iput-object p2, p0, LX/6He;->c:LX/03V;

    .line 1072567
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/6He;->g:Ljava/util/List;

    .line 1072568
    return-void
.end method

.method public static a(LX/6He;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1072580
    sget-object v0, LX/6He;->a:Ljava/lang/Class;

    invoke-static {v0, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1072581
    iget-object v0, p0, LX/6He;->c:LX/03V;

    sget-object v1, LX/6He;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072582
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6He;->f:Z

    .line 1072583
    return-void
.end method


# virtual methods
.method public final onFaceDetection([Landroid/hardware/Camera$Face;Landroid/hardware/Camera;)V
    .locals 7

    .prologue
    .line 1072569
    iget v0, p0, LX/6He;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1072570
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 1072571
    iget-object v2, v2, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    .line 1072572
    iget v3, v2, Landroid/graphics/Rect;->left:I

    mul-int/lit8 v3, v3, -0x1

    iget v4, v2, Landroid/graphics/Rect;->top:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    mul-int/lit8 v5, v5, -0x1

    iget v6, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 1072573
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1072574
    :cond_0
    iget-object v0, p0, LX/6He;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Hh;

    .line 1072575
    iget-boolean p0, v0, LX/6Hh;->f:Z

    if-nez p0, :cond_1

    .line 1072576
    iput-object p1, v0, LX/6Hh;->e:[Landroid/hardware/Camera$Face;

    .line 1072577
    iget-object p0, v0, LX/6Hh;->h:Landroid/os/Handler;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1072578
    :cond_1
    goto :goto_1

    .line 1072579
    :cond_2
    return-void
.end method
