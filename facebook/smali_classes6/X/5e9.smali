.class public final enum LX/5e9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5e9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5e9;

.field public static final enum GROUP:LX/5e9;

.field public static final enum ONE_TO_ONE:LX/5e9;

.field public static final enum PENDING_MY_MONTAGE:LX/5e9;

.field public static final enum PENDING_THREAD:LX/5e9;

.field public static final enum SMS:LX/5e9;

.field public static final enum TINCAN:LX/5e9;

.field public static final enum TINCAN_MULTI_ENDPOINT:LX/5e9;


# instance fields
.field public final dbValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 966673
    new-instance v0, LX/5e9;

    const-string v1, "ONE_TO_ONE"

    const-string v2, "ONE_TO_ONE"

    invoke-direct {v0, v1, v4, v2}, LX/5e9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5e9;->ONE_TO_ONE:LX/5e9;

    .line 966674
    new-instance v0, LX/5e9;

    const-string v1, "GROUP"

    const-string v2, "GROUP"

    invoke-direct {v0, v1, v5, v2}, LX/5e9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5e9;->GROUP:LX/5e9;

    .line 966675
    new-instance v0, LX/5e9;

    const-string v1, "TINCAN"

    const-string v2, "TINCAN"

    invoke-direct {v0, v1, v6, v2}, LX/5e9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5e9;->TINCAN:LX/5e9;

    .line 966676
    new-instance v0, LX/5e9;

    const-string v1, "TINCAN_MULTI_ENDPOINT"

    const-string v2, "TINCAN_MULTI_ENDPOINT"

    invoke-direct {v0, v1, v7, v2}, LX/5e9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5e9;->TINCAN_MULTI_ENDPOINT:LX/5e9;

    .line 966677
    new-instance v0, LX/5e9;

    const-string v1, "PENDING_THREAD"

    const-string v2, "PENDING_THREAD"

    invoke-direct {v0, v1, v8, v2}, LX/5e9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5e9;->PENDING_THREAD:LX/5e9;

    .line 966678
    new-instance v0, LX/5e9;

    const-string v1, "PENDING_MY_MONTAGE"

    const/4 v2, 0x5

    const-string v3, "PENDING_MY_MONTAGE"

    invoke-direct {v0, v1, v2, v3}, LX/5e9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5e9;->PENDING_MY_MONTAGE:LX/5e9;

    .line 966679
    new-instance v0, LX/5e9;

    const-string v1, "SMS"

    const/4 v2, 0x6

    const-string v3, "SMS"

    invoke-direct {v0, v1, v2, v3}, LX/5e9;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5e9;->SMS:LX/5e9;

    .line 966680
    const/4 v0, 0x7

    new-array v0, v0, [LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    aput-object v1, v0, v4

    sget-object v1, LX/5e9;->GROUP:LX/5e9;

    aput-object v1, v0, v5

    sget-object v1, LX/5e9;->TINCAN:LX/5e9;

    aput-object v1, v0, v6

    sget-object v1, LX/5e9;->TINCAN_MULTI_ENDPOINT:LX/5e9;

    aput-object v1, v0, v7

    sget-object v1, LX/5e9;->PENDING_THREAD:LX/5e9;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/5e9;->PENDING_MY_MONTAGE:LX/5e9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5e9;->SMS:LX/5e9;

    aput-object v2, v0, v1

    sput-object v0, LX/5e9;->$VALUES:[LX/5e9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 966670
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 966671
    iput-object p3, p0, LX/5e9;->dbValue:Ljava/lang/String;

    .line 966672
    return-void
.end method

.method public static fromDbValue(Ljava/lang/String;)LX/5e9;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 966681
    invoke-static {}, LX/5e9;->values()[LX/5e9;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 966682
    iget-object v4, v0, LX/5e9;->dbValue:Ljava/lang/String;

    invoke-static {v4, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 966683
    :goto_1
    return-object v0

    .line 966684
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 966685
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/5e9;
    .locals 1

    .prologue
    .line 966669
    const-class v0, LX/5e9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5e9;

    return-object v0
.end method

.method public static values()[LX/5e9;
    .locals 1

    .prologue
    .line 966668
    sget-object v0, LX/5e9;->$VALUES:[LX/5e9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5e9;

    return-object v0
.end method
