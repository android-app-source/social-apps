.class public final LX/6Bi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6BZ;


# instance fields
.field private final a:LX/0SG;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:Landroid/net/ConnectivityManager;

.field private final d:J

.field private final e:Z


# direct methods
.method private constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/net/ConnectivityManager;J)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1063067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063068
    iput-object p1, p0, LX/6Bi;->a:LX/0SG;

    .line 1063069
    iput-object p2, p0, LX/6Bi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1063070
    iput-object p3, p0, LX/6Bi;->c:Landroid/net/ConnectivityManager;

    .line 1063071
    iput-wide p4, p0, LX/6Bi;->d:J

    .line 1063072
    iget-object v0, p0, LX/6Bi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6Bj;->a:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 1063073
    cmp-long v2, v0, v4

    if-gtz v2, :cond_0

    .line 1063074
    invoke-direct {p0}, LX/6Bi;->d()V

    .line 1063075
    iget-object v0, p0, LX/6Bi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/6Bj;->a:LX/0Tn;

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    .line 1063076
    :cond_0
    iget-object v2, p0, LX/6Bi;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, LX/6Bi;->d:J

    sub-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    iget-object v2, p0, LX/6Bi;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/6Bi;->e:Z

    .line 1063077
    return-void

    .line 1063078
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/net/ConnectivityManager;JB)V
    .locals 0

    .prologue
    .line 1063079
    invoke-direct/range {p0 .. p5}, LX/6Bi;-><init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/net/ConnectivityManager;J)V

    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1063080
    iget-object v0, p0, LX/6Bi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/6Bj;->a:LX/0Tn;

    iget-object v2, p0, LX/6Bi;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1063081
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 1063082
    iget-object v0, p0, LX/6Bi;->c:Landroid/net/ConnectivityManager;

    const/4 v1, 0x1

    .line 1063083
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1063084
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v1, :cond_2

    :goto_0
    move v0, v1

    .line 1063085
    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/6Bi;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1063086
    invoke-virtual {p0}, LX/6Bi;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1063087
    invoke-direct {p0}, LX/6Bi;->d()V

    .line 1063088
    :cond_0
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1063089
    const-string v0, "MayBeWaitForWifiEligibilityCallback"

    return-object v0
.end method
