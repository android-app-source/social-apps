.class public final LX/6Hz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/6IA;


# direct methods
.method public constructor <init>(LX/6IA;)V
    .locals 0

    .prologue
    .line 1072923
    iput-object p1, p0, LX/6Hz;->a:LX/6IA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    const v0, 0x394302c8

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1072924
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    iget v1, v1, LX/6IA;->X:I

    if-eq v1, v2, :cond_0

    .line 1072925
    const v1, 0x2e058a61

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1072926
    :goto_0
    return-void

    .line 1072927
    :cond_0
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    iget-object v1, v1, LX/6IA;->ar:LX/6HU;

    .line 1072928
    iget-boolean v2, v1, LX/6HU;->s:Z

    move v1, v2

    .line 1072929
    if-nez v1, :cond_1

    .line 1072930
    const v1, 0x68e2a4af

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1072931
    :cond_1
    invoke-static {}, LX/2Ib;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1072932
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    invoke-static {v1}, LX/6IA;->t(LX/6IA;)V

    .line 1072933
    const v1, -0x60dd18bb

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1072934
    :cond_2
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    iget-object v1, v1, LX/6IA;->ar:LX/6HU;

    .line 1072935
    iget-boolean v2, v1, LX/6HU;->k:Z

    move v1, v2

    .line 1072936
    if-eqz v1, :cond_3

    .line 1072937
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    iget-object v1, v1, LX/6IA;->ar:LX/6HU;

    invoke-virtual {v1}, LX/6HU;->i()V

    .line 1072938
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/6IA;->g(LX/6IA;Z)V

    .line 1072939
    :goto_1
    const v1, -0x65029a39

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1072940
    :cond_3
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    iget-object v1, v1, LX/6IA;->ar:LX/6HU;

    .line 1072941
    const/4 p1, 0x5

    const/4 v7, 0x1

    const/4 v6, -0x1

    .line 1072942
    invoke-static {}, LX/2Ib;->b()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1072943
    sget-object v2, LX/6HU;->b:Ljava/lang/Class;

    const-string v4, "prepare failed - external storage is not writable"

    invoke-static {v2, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1072944
    :goto_2
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    if-nez v2, :cond_5

    .line 1072945
    sget-object v2, LX/6HU;->b:Ljava/lang/Class;

    const-string v4, "Fail to initialize media recorder"

    invoke-static {v2, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1072946
    :goto_3
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    iget-object v1, v1, LX/6IA;->ar:LX/6HU;

    .line 1072947
    iget-boolean v2, v1, LX/6HU;->k:Z

    move v1, v2

    .line 1072948
    if-eqz v1, :cond_4

    .line 1072949
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    invoke-static {v1, v3}, LX/6IA;->g(LX/6IA;Z)V

    goto :goto_1

    .line 1072950
    :cond_4
    iget-object v1, p0, LX/6Hz;->a:LX/6IA;

    .line 1072951
    invoke-static {v1}, LX/6IA;->q(LX/6IA;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08119d

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1072952
    goto :goto_1

    .line 1072953
    :cond_5
    iget-object v2, v1, LX/6HU;->t:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 1072954
    sget-object v2, LX/6HU;->b:Ljava/lang/Class;

    const-string v4, "Invalid video output file name"

    invoke-static {v2, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1072955
    invoke-static {v1}, LX/6HU;->C(LX/6HU;)V

    goto :goto_3

    .line 1072956
    :cond_6
    :try_start_0
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->start()V

    .line 1072957
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/6HU;->k:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1072958
    :catch_0
    move-exception v2

    .line 1072959
    iget-object v4, v1, LX/6HU;->B:LX/6HF;

    const-string v5, "start MediaRecorder failed"

    invoke-interface {v4, v5, v2}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1072960
    invoke-static {v1}, LX/6HU;->z(LX/6HU;)V

    .line 1072961
    invoke-static {v1}, LX/6HU;->C(LX/6HU;)V

    .line 1072962
    invoke-static {v1}, LX/6HU;->K(LX/6HU;)V

    .line 1072963
    invoke-static {v1}, LX/6HU;->G(LX/6HU;)V

    goto :goto_3

    .line 1072964
    :cond_7
    new-instance v2, Landroid/media/MediaRecorder;

    invoke-direct {v2}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    .line 1072965
    :try_start_1
    iget-object v2, v1, LX/6HU;->d:Landroid/hardware/Camera;

    const v4, 0x46bf9d5d

    invoke-static {v2, v4}, LX/0J2;->c(Landroid/hardware/Camera;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 1072966
    :goto_4
    :try_start_2
    iget-object v2, v1, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->unlock()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 1072967
    :goto_5
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    iget-object v4, v1, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v2, v4}, Landroid/media/MediaRecorder;->setCamera(Landroid/hardware/Camera;)V

    .line 1072968
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v2, p1}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 1072969
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v2, v7}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    .line 1072970
    :try_start_3
    iget v2, v1, LX/6HU;->x:I

    iget v4, v1, LX/6HU;->H:I

    invoke-static {v2, v4}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    move-result-object v4

    .line 1072971
    iget v2, v1, LX/6HU;->I:I

    if-eq v2, v6, :cond_9

    iget v2, v1, LX/6HU;->I:I

    :goto_6
    iput v2, v4, Landroid/media/CamcorderProfile;->duration:I

    .line 1072972
    iget v2, v1, LX/6HU;->J:I

    if-eq v2, v6, :cond_a

    iget v2, v1, LX/6HU;->J:I

    :goto_7
    iput v2, v4, Landroid/media/CamcorderProfile;->fileFormat:I

    .line 1072973
    iget v2, v4, Landroid/media/CamcorderProfile;->fileFormat:I

    iput v2, v1, LX/6HU;->J:I

    .line 1072974
    iget v2, v1, LX/6HU;->K:I

    if-eq v2, v6, :cond_b

    iget v2, v1, LX/6HU;->K:I

    :goto_8
    iput v2, v4, Landroid/media/CamcorderProfile;->videoCodec:I

    .line 1072975
    iget v2, v1, LX/6HU;->L:I

    if-eq v2, v6, :cond_8

    iget v2, v1, LX/6HU;->M:I

    if-eq v2, v6, :cond_8

    .line 1072976
    iget v2, v1, LX/6HU;->L:I

    iput v2, v4, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    .line 1072977
    iget v2, v1, LX/6HU;->M:I

    iput v2, v4, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    .line 1072978
    :cond_8
    iget v2, v1, LX/6HU;->N:I

    if-eq v2, v6, :cond_c

    iget v2, v1, LX/6HU;->N:I

    :goto_9
    iput v2, v4, Landroid/media/CamcorderProfile;->videoFrameRate:I

    .line 1072979
    iget v2, v1, LX/6HU;->O:I

    if-eq v2, v6, :cond_d

    iget v2, v1, LX/6HU;->O:I

    :goto_a
    iput v2, v4, Landroid/media/CamcorderProfile;->videoBitRate:I

    .line 1072980
    iget v2, v1, LX/6HU;->P:I

    if-eq v2, v6, :cond_e

    iget v2, v1, LX/6HU;->P:I

    :goto_b
    iput v2, v4, Landroid/media/CamcorderProfile;->audioCodec:I

    .line 1072981
    iget v2, v1, LX/6HU;->Q:I

    if-eq v2, v6, :cond_f

    iget v2, v1, LX/6HU;->Q:I

    :goto_c
    iput v2, v4, Landroid/media/CamcorderProfile;->audioSampleRate:I

    .line 1072982
    iget v2, v1, LX/6HU;->R:I

    if-eq v2, v6, :cond_10

    iget v2, v1, LX/6HU;->R:I

    :goto_d
    iput v2, v4, Landroid/media/CamcorderProfile;->audioBitRate:I

    .line 1072983
    iget v2, v1, LX/6HU;->S:I

    if-eq v2, v6, :cond_11

    iget v2, v1, LX/6HU;->S:I

    :goto_e
    iput v2, v4, Landroid/media/CamcorderProfile;->audioChannels:I

    .line 1072984
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v2, v4}, Landroid/media/MediaRecorder;->setProfile(Landroid/media/CamcorderProfile;)V

    .line 1072985
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-static {v1}, LX/6HU;->D(LX/6HU;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/media/MediaRecorder;->setOrientationHint(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 1072986
    :goto_f
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    iget-object v4, v1, LX/6HU;->e:LX/6HV;

    .line 1072987
    iget-object v5, v4, LX/6HV;->b:Landroid/view/SurfaceHolder;

    invoke-interface {v5}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v5

    move-object v4, v5

    .line 1072988
    invoke-virtual {v2, v4}, Landroid/media/MediaRecorder;->setPreviewDisplay(Landroid/view/Surface;)V

    .line 1072989
    iget v2, v1, LX/6HU;->J:I

    packed-switch v2, :pswitch_data_0

    .line 1072990
    const-string v2, ".3gp"

    .line 1072991
    :goto_10
    iget-object v4, v1, LX/6HU;->j:LX/2Ib;

    .line 1072992
    const-string v5, "FB_VID"

    invoke-static {v4, v5, v2}, LX/2Ib;->a(LX/2Ib;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    move-object v2, v5

    .line 1072993
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/6HU;->t:Ljava/lang/String;

    .line 1072994
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    iget-object v4, v1, LX/6HU;->t:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 1072995
    :try_start_4
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->prepare()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 1072996
    :catch_1
    move-exception v2

    .line 1072997
    iget-object v4, v1, LX/6HU;->B:LX/6HF;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "prepare MediaRecorder failed for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, LX/6HU;->t:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1072998
    invoke-static {v1}, LX/6HU;->z(LX/6HU;)V

    .line 1072999
    invoke-static {v1}, LX/6HU;->C(LX/6HU;)V

    goto/16 :goto_2

    .line 1073000
    :catch_2
    move-exception v2

    .line 1073001
    iget-object v4, v1, LX/6HU;->B:LX/6HF;

    const-string v5, "initializeRecorder/stopPreview failed"

    invoke-interface {v4, v5, v2}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_4

    .line 1073002
    :catch_3
    move-exception v2

    .line 1073003
    iget-object v4, v1, LX/6HU;->B:LX/6HF;

    const-string v5, "initializeRecorder/unlock failed"

    invoke-interface {v4, v5, v2}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_5

    .line 1073004
    :cond_9
    :try_start_5
    iget v2, v4, Landroid/media/CamcorderProfile;->duration:I

    goto/16 :goto_6

    .line 1073005
    :cond_a
    iget v2, v4, Landroid/media/CamcorderProfile;->fileFormat:I

    goto/16 :goto_7

    .line 1073006
    :cond_b
    iget v2, v4, Landroid/media/CamcorderProfile;->videoCodec:I

    goto/16 :goto_8

    .line 1073007
    :cond_c
    iget v2, v4, Landroid/media/CamcorderProfile;->videoFrameRate:I

    goto/16 :goto_9

    .line 1073008
    :cond_d
    iget v2, v4, Landroid/media/CamcorderProfile;->videoBitRate:I

    goto/16 :goto_a

    .line 1073009
    :cond_e
    iget v2, v4, Landroid/media/CamcorderProfile;->audioCodec:I

    goto/16 :goto_b

    .line 1073010
    :cond_f
    iget v2, v4, Landroid/media/CamcorderProfile;->audioSampleRate:I

    goto/16 :goto_c

    .line 1073011
    :cond_10
    iget v2, v4, Landroid/media/CamcorderProfile;->audioBitRate:I

    goto/16 :goto_d

    .line 1073012
    :cond_11
    iget v2, v4, Landroid/media/CamcorderProfile;->audioChannels:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_e

    .line 1073013
    :catch_4
    move-exception v2

    .line 1073014
    iget-object v4, v1, LX/6HU;->B:LX/6HF;

    const-string v5, "Setup MediaRecorder failed"

    invoke-interface {v4, v5, v2}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1073015
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v2}, Landroid/media/MediaRecorder;->reset()V

    .line 1073016
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    iget-object v4, v1, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v2, v4}, Landroid/media/MediaRecorder;->setCamera(Landroid/hardware/Camera;)V

    .line 1073017
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v2, p1}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 1073018
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v2, v7}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    .line 1073019
    iget v2, v1, LX/6HU;->x:I

    invoke-static {v2, v7}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    move-result-object v2

    .line 1073020
    iget-object v4, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v4, v2}, Landroid/media/MediaRecorder;->setProfile(Landroid/media/CamcorderProfile;)V

    .line 1073021
    iget-object v2, v1, LX/6HU;->i:Landroid/media/MediaRecorder;

    const v4, 0x927c0

    invoke-virtual {v2, v4}, Landroid/media/MediaRecorder;->setMaxDuration(I)V

    goto/16 :goto_f

    .line 1073022
    :pswitch_0
    const-string v2, ".mp4"

    goto/16 :goto_10

    .line 1073023
    :pswitch_1
    const-string v2, ".3gp"

    goto/16 :goto_10

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
