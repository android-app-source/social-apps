.class public final LX/650;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/64q;

.field public final b:Ljava/lang/String;

.field public final c:LX/64n;

.field public final d:LX/651;

.field public final e:Ljava/lang/Object;

.field private volatile f:LX/64W;


# direct methods
.method public constructor <init>(LX/64z;)V
    .locals 1

    .prologue
    .line 1046336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046337
    iget-object v0, p1, LX/64z;->a:LX/64q;

    iput-object v0, p0, LX/650;->a:LX/64q;

    .line 1046338
    iget-object v0, p1, LX/64z;->b:Ljava/lang/String;

    iput-object v0, p0, LX/650;->b:Ljava/lang/String;

    .line 1046339
    iget-object v0, p1, LX/64z;->c:LX/64m;

    invoke-virtual {v0}, LX/64m;->a()LX/64n;

    move-result-object v0

    iput-object v0, p0, LX/650;->c:LX/64n;

    .line 1046340
    iget-object v0, p1, LX/64z;->d:LX/651;

    iput-object v0, p0, LX/650;->d:LX/651;

    .line 1046341
    iget-object v0, p1, LX/64z;->e:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/64z;->e:Ljava/lang/Object;

    :goto_0
    iput-object v0, p0, LX/650;->e:Ljava/lang/Object;

    .line 1046342
    return-void

    :cond_0
    move-object v0, p0

    .line 1046343
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1046344
    iget-object v0, p0, LX/650;->c:LX/64n;

    invoke-virtual {v0, p1}, LX/64n;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/64W;
    .locals 1

    .prologue
    .line 1046345
    iget-object v0, p0, LX/650;->f:LX/64W;

    .line 1046346
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/650;->c:LX/64n;

    invoke-static {v0}, LX/64W;->a(LX/64n;)LX/64W;

    move-result-object v0

    iput-object v0, p0, LX/650;->f:LX/64W;

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1046347
    iget-object v0, p0, LX/650;->a:LX/64q;

    invoke-virtual {v0}, LX/64q;->c()Z

    move-result v0

    return v0
.end method

.method public final newBuilder()LX/64z;
    .locals 2

    .prologue
    .line 1046348
    new-instance v0, LX/64z;

    invoke-direct {v0, p0}, LX/64z;-><init>(LX/650;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1046349
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Request{method="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/650;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/650;->a:LX/64q;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/650;->e:Ljava/lang/Object;

    if-eq v0, p0, :cond_0

    iget-object v0, p0, LX/650;->e:Ljava/lang/Object;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
