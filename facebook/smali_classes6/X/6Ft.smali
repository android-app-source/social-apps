.class public LX/6Ft;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final t:LX/0PO;

.field public static final u:Ljava/lang/String;


# instance fields
.field private final a:LX/6G3;

.field public final b:Landroid/net/ConnectivityManager;

.field public final c:LX/03V;

.field public final d:LX/6Gl;

.field private final e:LX/0Uh;

.field public final f:LX/44D;

.field private final g:LX/3yR;

.field private final h:LX/4gp;

.field private final i:LX/5eF;

.field public final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/1MS;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0Zd;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/1Er;

.field public final m:LX/6V0;

.field private final n:LX/0TD;

.field public final o:LX/0W3;

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/6GZ;

.field private final r:LX/0SI;

.field public final s:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1069455
    const-string v0, ", "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    sput-object v0, LX/6Ft;->t:LX/0PO;

    .line 1069456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/6G2;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Ft;->u:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/6G3;Landroid/net/ConnectivityManager;LX/03V;LX/6Gl;LX/0Uh;LX/44D;LX/3yR;LX/4gp;LX/5eF;Ljava/util/Set;Ljava/util/Set;LX/1Er;LX/6V0;LX/0W3;LX/0Or;LX/6GZ;LX/0SI;LX/0Uh;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p18    # LX/0SI;
        .annotation runtime Lcom/facebook/auth/viewercontext/ViewerContextManagerForApp;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/6G3;",
            "Landroid/net/ConnectivityManager;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/6Gl;",
            "Lcom/facebook/gk/store/GkAccessorByName;",
            "LX/44D;",
            "LX/3yR;",
            "LX/4gp;",
            "LX/5eF;",
            "Ljava/util/Set",
            "<",
            "LX/1MS;",
            ">;",
            "Ljava/util/Set",
            "<",
            "LX/0Zd;",
            ">;",
            "LX/1Er;",
            "LX/6V0;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/6GZ;",
            "LX/0SI;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1069339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1069340
    iput-object p1, p0, LX/6Ft;->n:LX/0TD;

    .line 1069341
    iput-object p2, p0, LX/6Ft;->a:LX/6G3;

    .line 1069342
    iput-object p3, p0, LX/6Ft;->b:Landroid/net/ConnectivityManager;

    .line 1069343
    iput-object p4, p0, LX/6Ft;->c:LX/03V;

    .line 1069344
    iput-object p5, p0, LX/6Ft;->d:LX/6Gl;

    .line 1069345
    iput-object p6, p0, LX/6Ft;->e:LX/0Uh;

    .line 1069346
    iput-object p7, p0, LX/6Ft;->f:LX/44D;

    .line 1069347
    iput-object p8, p0, LX/6Ft;->g:LX/3yR;

    .line 1069348
    iput-object p9, p0, LX/6Ft;->h:LX/4gp;

    .line 1069349
    iput-object p10, p0, LX/6Ft;->i:LX/5eF;

    .line 1069350
    iput-object p11, p0, LX/6Ft;->j:Ljava/util/Set;

    .line 1069351
    iput-object p12, p0, LX/6Ft;->k:Ljava/util/Set;

    .line 1069352
    iput-object p13, p0, LX/6Ft;->l:LX/1Er;

    .line 1069353
    iput-object p14, p0, LX/6Ft;->m:LX/6V0;

    .line 1069354
    move-object/from16 v0, p15

    iput-object v0, p0, LX/6Ft;->o:LX/0W3;

    .line 1069355
    move-object/from16 v0, p16

    iput-object v0, p0, LX/6Ft;->p:LX/0Or;

    .line 1069356
    move-object/from16 v0, p17

    iput-object v0, p0, LX/6Ft;->q:LX/6GZ;

    .line 1069357
    move-object/from16 v0, p18

    iput-object v0, p0, LX/6Ft;->r:LX/0SI;

    .line 1069358
    move-object/from16 v0, p19

    iput-object v0, p0, LX/6Ft;->s:LX/0Uh;

    .line 1069359
    return-void
.end method

.method private a()LX/6FU;
    .locals 7

    .prologue
    .line 1069360
    invoke-static {}, LX/6G6;->a()LX/6G6;

    move-result-object v1

    .line 1069361
    invoke-static {}, Lcom/facebook/bugreporter/BugReport;->newBuilder()LX/6FU;

    move-result-object v0

    .line 1069362
    iget-object v2, v1, LX/6G6;->b:Ljava/io/File;

    move-object v2, v2

    .line 1069363
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1069364
    iput-object v2, v0, LX/6FU;->a:Landroid/net/Uri;

    .line 1069365
    move-object v0, v0

    .line 1069366
    iget-object v2, v1, LX/6G6;->b:Ljava/io/File;

    move-object v2, v2

    .line 1069367
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1069368
    iput-object v2, v0, LX/6FU;->h:Ljava/lang/String;

    .line 1069369
    move-object v0, v0

    .line 1069370
    iget-object v2, v1, LX/6G6;->h:LX/6Fb;

    move-object v2, v2

    .line 1069371
    iput-object v2, v0, LX/6FU;->r:LX/6Fb;

    .line 1069372
    move-object v2, v0

    .line 1069373
    iget-object v0, p0, LX/6Ft;->o:LX/0W3;

    sget-wide v4, LX/0X5;->aY:J

    const/4 v3, 0x0

    invoke-interface {v0, v4, v5, v3}, LX/0W4;->a(JZ)Z

    move-result v0

    .line 1069374
    iget-object v3, v1, LX/6G6;->c:Ljava/util/List;

    move-object v3, v3

    .line 1069375
    if-eqz v0, :cond_2

    .line 1069376
    iget-object v0, v1, LX/6G6;->i:Ljava/io/File;

    move-object v0, v0

    .line 1069377
    :goto_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1069378
    const/4 v4, 0x0

    move v5, v4

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_0

    .line 1069379
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-static {v4, v0, v5}, LX/6Ft;->a(Landroid/graphics/Bitmap;Ljava/io/File;I)Landroid/net/Uri;

    move-result-object v4

    .line 1069380
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1069381
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 1069382
    :cond_0
    iput-object v6, v2, LX/6FU;->d:Ljava/util/List;

    .line 1069383
    iget-object v0, p0, LX/6Ft;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1069384
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1069385
    iput-object v3, v2, LX/6FU;->u:Ljava/lang/String;

    .line 1069386
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1069387
    iput-object v3, v2, LX/6FU;->v:Ljava/lang/String;

    .line 1069388
    iget-boolean v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v3, v3

    .line 1069389
    iput-boolean v3, v2, LX/6FU;->x:Z

    .line 1069390
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    iget-object v3, p0, LX/6Ft;->j:Ljava/util/Set;

    invoke-virtual {v0, v3}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    .line 1069391
    iget-object v3, v1, LX/6G6;->g:LX/0Rf;

    move-object v3, v3

    .line 1069392
    invoke-virtual {v0, v3}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 1069393
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MS;

    .line 1069394
    :try_start_0
    invoke-interface {v0}, LX/1MS;->prepareDataForWriting()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1069395
    :catch_0
    move-exception v0

    .line 1069396
    iget-object v4, p0, LX/6Ft;->c:LX/03V;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LX/6Ft;->u:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "prepareDataForWriting"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1069397
    :cond_1
    return-object v2

    .line 1069398
    :cond_2
    iget-object v0, v1, LX/6G6;->b:Ljava/io/File;

    move-object v0, v0

    .line 1069399
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/6Ft;
    .locals 1

    .prologue
    .line 1069400
    invoke-static {p0}, LX/6Ft;->b(LX/0QB;)LX/6Ft;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/graphics/Bitmap;Ljava/io/File;I)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 1069401
    :try_start_0
    const-string v0, "screenshot%d.png"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1069402
    invoke-static {p1, v0}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1069403
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    .line 1069404
    iget-object v3, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v3, v3

    .line 1069405
    invoke-virtual {p0, v0, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1069406
    iget-object v0, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069407
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 1069408
    iget-object v0, v1, LX/6FR;->b:Landroid/net/Uri;

    move-object v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1069409
    :try_start_2
    iget-object v2, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v2

    .line 1069410
    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 1069411
    :goto_0
    return-object v0

    .line 1069412
    :catchall_0
    move-exception v0

    .line 1069413
    iget-object v2, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v2

    .line 1069414
    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1069415
    :catch_0
    move-exception v0

    .line 1069416
    const-string v1, "BugReportWriter"

    const-string v2, "Exception saving screenshot"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069417
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1069418
    iget-object v0, p0, LX/6Ft;->a:LX/6G3;

    .line 1069419
    const-string v1, "bugreports"

    invoke-static {v0, v1, p1}, LX/6G3;->a(LX/6G3;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    move-object v0, v1

    .line 1069420
    if-nez v0, :cond_0

    .line 1069421
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not create directory"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1069422
    :cond_0
    return-object v0
.end method

.method public static a(LX/6Ft;LX/6FU;Landroid/content/Context;Ljava/io/File;Ljava/io/File;LX/0P1;LX/0Rf;Landroid/os/Bundle;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6FU;",
            "Landroid/content/Context;",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Rf",
            "<",
            "LX/1MS;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1069423
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Ft;->n:LX/0TD;

    new-instance v3, LX/6Fk;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v3, v0, v1}, LX/6Fk;-><init>(LX/6Ft;Ljava/io/File;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v15

    .line 1069424
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v7

    .line 1069425
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    .line 1069426
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 1069427
    move-object/from16 v0, p0

    iget-object v8, v0, LX/6Ft;->n:LX/0TD;

    new-instance v2, LX/6Fl;

    move-object/from16 v3, p0

    move-object/from16 v4, p4

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v7}, LX/6Fl;-><init>(LX/6Ft;Ljava/io/File;Ljava/io/File;Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v8, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const-string v3, "quick_experiments"

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069428
    move-object/from16 v0, p0

    iget-object v8, v0, LX/6Ft;->n:LX/0TD;

    new-instance v2, LX/6Fm;

    move-object/from16 v3, p0

    move-object/from16 v4, p4

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v7}, LX/6Fm;-><init>(LX/6Ft;Ljava/io/File;Ljava/io/File;Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v8, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const-string v3, "mobileconfig"

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069429
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Ft;->n:LX/0TD;

    new-instance v4, LX/6Fn;

    move-object/from16 v5, p0

    move-object/from16 v8, p4

    move-object/from16 v9, p3

    invoke-direct/range {v4 .. v9}, LX/6Fn;-><init>(LX/6Ft;Ljava/util/Map;Ljava/util/Map;Ljava/io/File;Ljava/io/File;)V

    invoke-interface {v2, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const-string v3, "debug_logs"

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069430
    move-object/from16 v0, p0

    iget-object v8, v0, LX/6Ft;->n:LX/0TD;

    new-instance v2, LX/6Fo;

    move-object/from16 v3, p0

    move-object/from16 v4, p4

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v7}, LX/6Fo;-><init>(LX/6Ft;Ljava/io/File;Ljava/io/File;Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v8, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const-string v3, "anr_traces"

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069431
    move-object/from16 v0, p0

    iget-object v8, v0, LX/6Ft;->n:LX/0TD;

    new-instance v2, LX/6Fp;

    move-object/from16 v3, p0

    move-object/from16 v4, p4

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v7}, LX/6Fp;-><init>(LX/6Ft;Ljava/io/File;Ljava/io/File;Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v8, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const-string v3, "gatekeepers"

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069432
    move-object/from16 v0, p0

    iget-object v8, v0, LX/6Ft;->n:LX/0TD;

    new-instance v2, LX/6Fq;

    move-object/from16 v3, p0

    move-object/from16 v4, p4

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v7}, LX/6Fq;-><init>(LX/6Ft;Ljava/io/File;Ljava/io/File;Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v8, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const-string v3, "stack_trace_dump"

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069433
    move-object/from16 v0, p2

    instance-of v2, v0, LX/0ew;

    if-eqz v2, :cond_0

    .line 1069434
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Ft;->n:LX/0TD;

    new-instance v3, LX/6Fr;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1, v6, v7}, LX/6Fr;-><init>(LX/6Ft;Landroid/content/Context;Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const-string v3, "sysdump"

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069435
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Ft;->n:LX/0TD;

    new-instance v8, LX/6Fs;

    move-object/from16 v9, p0

    move-object/from16 v10, p4

    move-object/from16 v11, p3

    move-object/from16 v12, p5

    move-object v13, v6

    move-object v14, v7

    invoke-direct/range {v8 .. v14}, LX/6Fs;-><init>(LX/6Ft;Ljava/io/File;Ljava/io/File;LX/0P1;Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v2, v8}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const-string v3, "debug_info"

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1069436
    if-eqz p7, :cond_1

    .line 1069437
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Ft;->n:LX/0TD;

    new-instance v3, LX/6Fj;

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v3, v0, v1, v6, v7}, LX/6Fj;-><init>(LX/6Ft;Landroid/os/Bundle;Ljava/util/Map;Ljava/util/Map;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    const-string v3, "view_hierarchy"

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object/from16 v8, p0

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object v11, v7

    move-object v12, v6

    move-object/from16 v13, p6

    .line 1069438
    invoke-static/range {v8 .. v13}, LX/6Ft;->a(LX/6Ft;Ljava/io/File;Ljava/io/File;Ljava/util/Map;Ljava/util/Map;LX/0Rf;)V

    .line 1069439
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Ft;->o:LX/0W3;

    sget-wide v4, LX/0X5;->bj:J

    const/4 v3, 0x1

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JZ)Z

    move-result v5

    .line 1069440
    move-object/from16 v0, p0

    iget-object v2, v0, LX/6Ft;->o:LX/0W3;

    sget-wide v8, LX/0X5;->bk:J

    const/16 v3, 0xf

    invoke-interface {v2, v8, v9, v3}, LX/0W4;->a(JI)I

    move-result v8

    .line 1069441
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1069442
    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Future;

    .line 1069443
    if-eqz v5, :cond_2

    .line 1069444
    int-to-long v12, v8

    :try_start_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const v4, -0x111c91ef

    invoke-static {v2, v12, v13, v3, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1069445
    :catch_0
    move-exception v3

    move-object v4, v3

    .line 1069446
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1069447
    move-object/from16 v0, p0

    iget-object v11, v0, LX/6Ft;->c:LX/03V;

    const-string v12, "bugReportAttachmentFutureTimeout"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Bug report attachment future timed out: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069448
    const/4 v4, 0x1

    invoke-interface {v2, v4}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1069449
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1069450
    :cond_2
    const v3, -0x5f6df7f0

    invoke-static {v2, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    goto :goto_0

    .line 1069451
    :cond_3
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "none"

    .line 1069452
    :goto_1
    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/6FU;->a(Landroid/net/Uri;)LX/6FU;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/6FU;->n(Ljava/lang/String;)LX/6FU;

    move-result-object v2

    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    invoke-virtual {v3, v7}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v3

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6FU;->a(LX/0P1;)LX/6FU;

    move-result-object v2

    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    invoke-virtual {v3, v6}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    move-result-object v3

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6FU;->b(LX/0P1;)LX/6FU;

    move-result-object v3

    const v2, -0x6e8d2cdd

    invoke-static {v15, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v3, v2}, LX/6FU;->b(Landroid/net/Uri;)LX/6FU;

    move-result-object v2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6FU;->c(Ljava/lang/String;)LX/6FU;

    .line 1069453
    return-void

    .line 1069454
    :cond_4
    const-string v2, ", "

    invoke-static {v2, v9}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private static a(LX/6Ft;Ljava/io/File;Ljava/io/File;Ljava/util/Map;Ljava/util/Map;LX/0Rf;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Rf",
            "<",
            "LX/1MS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1069309
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    iget-object v1, p0, LX/6Ft;->j:Ljava/util/Set;

    invoke-virtual {v0, v1}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    .line 1069310
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1MS;

    .line 1069311
    :try_start_0
    invoke-interface {v0}, LX/1MS;->shouldSendAsync()Z

    move-result v3

    .line 1069312
    if-eqz v3, :cond_1

    move-object v1, p2

    :goto_1
    invoke-interface {v0, v1}, LX/1MS;->getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;

    move-result-object v1

    .line 1069313
    if-eqz v1, :cond_0

    .line 1069314
    if-eqz v3, :cond_2

    move-object v0, p4

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1069315
    :catch_0
    move-exception v0

    .line 1069316
    :goto_3
    const-string v1, "BugReportWriter"

    const-string v3, "Failed to serialize bug report extra file attachment from provider"

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069317
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v1}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_0

    :cond_1
    move-object v1, p1

    .line 1069318
    goto :goto_1

    :cond_2
    move-object v0, p3

    .line 1069319
    goto :goto_2

    .line 1069320
    :cond_3
    return-void

    .line 1069321
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public static a$redex0(LX/6Ft;LX/0ew;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1069322
    iget-object v0, p0, LX/6Ft;->l:LX/1Er;

    const-string v1, "dumpsys.txt"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v3

    .line 1069323
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1069324
    new-instance v4, Ljava/io/BufferedOutputStream;

    invoke-direct {v4, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1069325
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    .line 1069326
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1069327
    :try_start_1
    invoke-interface {p1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v2

    const-string v4, ""

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v2, v4, v0, v1, v5}, LX/0gc;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1069328
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 1069329
    :cond_0
    :goto_0
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 1069330
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1069331
    :goto_1
    :try_start_2
    const-string v2, "BugReportWriter"

    const-string v4, "Failed to sysdump activity"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v2, v0, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1069332
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v2}, LX/6GZ;->a(LX/6GY;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1069333
    if-eqz v1, :cond_0

    .line 1069334
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    goto :goto_0

    .line 1069335
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_1

    .line 1069336
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    :cond_1
    throw v0

    .line 1069337
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1069338
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a$redex0(LX/6Ft;Ljava/io/File;LX/0P1;)Landroid/net/Uri;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1069259
    :try_start_0
    const-string v0, "extra_data.txt"

    invoke-static {p1, v0}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 1069260
    :try_start_1
    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v0, Ljava/io/PrintWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    .line 1069261
    iget-object v5, v3, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v5, v5

    .line 1069262
    const-string v6, "UTF8"

    invoke-direct {v1, v5, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-direct {v4, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1069263
    :try_start_2
    invoke-virtual {p2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1069264
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v6, "[^a-zA-Z0-9_ ]"

    const-string v7, "_"

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1069265
    invoke-virtual {v4, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069266
    const/16 v1, 0x9

    invoke-virtual {v4, v1}, Ljava/io/BufferedWriter;->write(I)V

    .line 1069267
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069268
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 1069269
    :catch_0
    move-exception v0

    move-object v1, v3

    move-object v3, v4

    .line 1069270
    :goto_1
    :try_start_3
    const-string v4, "BugReportWriter"

    const-string v5, "Exception saving DebugInfo"

    invoke-static {v4, v5, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069271
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v4, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v4}, LX/6GZ;->a(LX/6GY;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1069272
    if-eqz v3, :cond_0

    .line 1069273
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1069274
    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    .line 1069275
    :try_start_5
    iget-object v0, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069276
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_1
    :goto_3
    move-object v0, v2

    .line 1069277
    :cond_2
    :goto_4
    return-object v0

    .line 1069278
    :cond_3
    :try_start_6
    iget-object v0, v3, LX/6FR;->b:Landroid/net/Uri;

    move-object v0, v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1069279
    :try_start_7
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 1069280
    :goto_5
    if-eqz v3, :cond_2

    .line 1069281
    :try_start_8
    iget-object v1, v3, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v1

    .line 1069282
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_4

    .line 1069283
    :catch_1
    move-exception v1

    .line 1069284
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069285
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_4

    .line 1069286
    :catch_2
    move-exception v1

    .line 1069287
    const-string v2, "BugReportWriter"

    const-string v4, "Exception closing DebugInfo report."

    invoke-static {v2, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069288
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_5

    .line 1069289
    :catch_3
    move-exception v0

    .line 1069290
    const-string v3, "BugReportWriter"

    const-string v4, "Exception closing DebugInfo report."

    invoke-static {v3, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069291
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v3, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v3}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_2

    .line 1069292
    :catch_4
    move-exception v0

    .line 1069293
    const-string v1, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v1, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069294
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v1}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_3

    .line 1069295
    :catchall_0
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    :goto_6
    if-eqz v4, :cond_4

    .line 1069296
    :try_start_9
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 1069297
    :cond_4
    :goto_7
    if-eqz v3, :cond_5

    .line 1069298
    :try_start_a
    iget-object v1, v3, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v1

    .line 1069299
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 1069300
    :cond_5
    :goto_8
    throw v0

    .line 1069301
    :catch_5
    move-exception v1

    .line 1069302
    const-string v2, "BugReportWriter"

    const-string v4, "Exception closing DebugInfo report."

    invoke-static {v2, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069303
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_7

    .line 1069304
    :catch_6
    move-exception v1

    .line 1069305
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069306
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_8

    .line 1069307
    :catchall_1
    move-exception v0

    move-object v4, v2

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object v4, v3

    move-object v3, v1

    goto :goto_6

    .line 1069308
    :catch_7
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto/16 :goto_1

    :catch_8
    move-exception v0

    move-object v1, v3

    move-object v3, v2

    goto/16 :goto_1
.end method

.method public static b(LX/0QB;)LX/6Ft;
    .locals 22

    .prologue
    .line 1069257
    new-instance v2, LX/6Ft;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/6G3;->a(LX/0QB;)LX/6G3;

    move-result-object v4

    check-cast v4, LX/6G3;

    invoke-static/range {p0 .. p0}, LX/0nI;->a(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v5

    check-cast v5, Landroid/net/ConnectivityManager;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static/range {p0 .. p0}, LX/FB2;->a(LX/0QB;)LX/FB2;

    move-result-object v7

    check-cast v7, LX/6Gl;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/44D;->a(LX/0QB;)LX/44D;

    move-result-object v9

    check-cast v9, LX/44D;

    invoke-static/range {p0 .. p0}, LX/3yR;->a(LX/0QB;)LX/3yR;

    move-result-object v10

    check-cast v10, LX/3yR;

    invoke-static/range {p0 .. p0}, LX/4gp;->a(LX/0QB;)LX/4gp;

    move-result-object v11

    check-cast v11, LX/4gp;

    invoke-static/range {p0 .. p0}, LX/5eG;->a(LX/0QB;)LX/5eF;

    move-result-object v12

    check-cast v12, LX/5eF;

    invoke-static/range {p0 .. p0}, LX/43U;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/43T;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v15

    check-cast v15, LX/1Er;

    invoke-static/range {p0 .. p0}, LX/6V0;->a(LX/0QB;)LX/6V0;

    move-result-object v16

    check-cast v16, LX/6V0;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v17

    check-cast v17, LX/0W3;

    const/16 v18, 0x19e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/6GZ;->a(LX/0QB;)LX/6GZ;

    move-result-object v19

    check-cast v19, LX/6GZ;

    invoke-static/range {p0 .. p0}, LX/0WI;->a(LX/0QB;)LX/0SI;

    move-result-object v20

    check-cast v20, LX/0SI;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v21

    check-cast v21, LX/0Uh;

    invoke-direct/range {v2 .. v21}, LX/6Ft;-><init>(LX/0TD;LX/6G3;Landroid/net/ConnectivityManager;LX/03V;LX/6Gl;LX/0Uh;LX/44D;LX/3yR;LX/4gp;LX/5eF;Ljava/util/Set;Ljava/util/Set;LX/1Er;LX/6V0;LX/0W3;LX/0Or;LX/6GZ;LX/0SI;LX/0Uh;)V

    .line 1069258
    return-object v2
.end method

.method private b(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    .prologue
    .line 1069253
    iget-object v0, p0, LX/6Ft;->a:LX/6G3;

    invoke-virtual {v0, p1}, LX/6G3;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1069254
    if-nez v0, :cond_0

    .line 1069255
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not create directory"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1069256
    :cond_0
    return-object v0
.end method

.method public static b$redex0(LX/6Ft;Ljava/io/File;)Landroid/net/Uri;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1069207
    :try_start_0
    const-string v1, "mobileconfigs.txt"

    invoke-static {p1, v1}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1069208
    :try_start_1
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/PrintWriter;

    .line 1069209
    iget-object v4, v2, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v4, v4

    .line 1069210
    invoke-direct {v1, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1069211
    :try_start_2
    iget-object v1, p0, LX/6Ft;->h:LX/4gp;

    const-string v4, ""

    const/4 v5, 0x0

    iget-object v6, p0, LX/6Ft;->i:LX/5eF;

    iget-object v6, v6, LX/5eF;->a:LX/0Px;

    invoke-virtual {v1, v4, v5, v6}, LX/4gp;->a(Ljava/lang/String;ZLX/0Px;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069212
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->newLine()V

    .line 1069213
    iget-object v1, v2, LX/6FR;->b:Landroid/net/Uri;

    move-object v0, v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1069214
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1069215
    :goto_0
    if-eqz v2, :cond_0

    .line 1069216
    :try_start_4
    iget-object v1, v2, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v1

    .line 1069217
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1069218
    :cond_0
    :goto_1
    return-object v0

    .line 1069219
    :catch_0
    move-exception v1

    .line 1069220
    const-string v3, "BugReportWriter"

    const-string v4, "Exception closing MobileConfig report."

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069221
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v3, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v3}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_0

    .line 1069222
    :catch_1
    move-exception v1

    .line 1069223
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069224
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_1

    .line 1069225
    :catch_2
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    .line 1069226
    :goto_2
    :try_start_5
    const-string v4, "BugReportWriter"

    const-string v5, "Exception saving mobile config dump"

    invoke-static {v4, v5, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069227
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v4, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v4}, LX/6GZ;->a(LX/6GY;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1069228
    if-eqz v3, :cond_1

    .line 1069229
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 1069230
    :cond_1
    :goto_3
    if-eqz v2, :cond_0

    .line 1069231
    :try_start_7
    iget-object v1, v2, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v1

    .line 1069232
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 1069233
    :catch_3
    move-exception v1

    .line 1069234
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069235
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_1

    .line 1069236
    :catch_4
    move-exception v1

    .line 1069237
    const-string v3, "BugReportWriter"

    const-string v4, "Exception closing MobileConfig report."

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069238
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v3, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v3}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_3

    .line 1069239
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    :goto_4
    if-eqz v3, :cond_2

    .line 1069240
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 1069241
    :cond_2
    :goto_5
    if-eqz v2, :cond_3

    .line 1069242
    :try_start_9
    iget-object v1, v2, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v1

    .line 1069243
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 1069244
    :cond_3
    :goto_6
    throw v0

    .line 1069245
    :catch_5
    move-exception v1

    .line 1069246
    const-string v3, "BugReportWriter"

    const-string v4, "Exception closing MobileConfig report."

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069247
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v3, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v3}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_5

    .line 1069248
    :catch_6
    move-exception v1

    .line 1069249
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069250
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_6

    .line 1069251
    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    .line 1069252
    :catch_7
    move-exception v1

    move-object v3, v0

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_2
.end method

.method public static c$redex0(LX/6Ft;Ljava/io/File;)Landroid/net/Uri;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1069143
    :try_start_0
    const-string v0, "quick_experiments.txt"

    invoke-static {p1, v0}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1069144
    :try_start_1
    iget-object v0, p0, LX/6Ft;->g:LX/3yR;

    invoke-virtual {v0}, LX/3yR;->a()LX/0Px;

    move-result-object v0

    .line 1069145
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/PrintWriter;

    .line 1069146
    iget-object v5, v2, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v5, v5

    .line 1069147
    invoke-direct {v4, v5}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1069148
    :try_start_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3yQ;

    .line 1069149
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p1, v0, LX/3yQ;->name:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string p1, " = "

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, LX/3yQ;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string p1, " ["

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    iget-boolean v5, v0, LX/3yQ;->quicker:Z

    if-eqz v5, :cond_a

    const-string v5, "quicker"

    :goto_1
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string p1, "]"

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1069150
    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069151
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->newLine()V

    .line 1069152
    iget-object v5, v0, LX/3yQ;->groupNames:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    .line 1069153
    const/4 v5, 0x0

    move v6, v5

    :goto_2
    if-ge v6, v7, :cond_3

    .line 1069154
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 1069155
    const-string v5, "  "

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069156
    iget-object v5, v0, LX/3yQ;->groupNames:LX/0Px;

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069157
    iget v5, v0, LX/3yQ;->serverAssignedGroupIndex:I

    if-ne v6, v5, :cond_0

    .line 1069158
    const-string v5, "server group"

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1069159
    :cond_0
    iget v5, v0, LX/3yQ;->clientOverrideGroupIndex:I

    if-ne v6, v5, :cond_1

    .line 1069160
    const-string v5, "client override"

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1069161
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1069162
    const-string v5, " ("

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069163
    sget-object v5, LX/6Ft;->t:LX/0PO;

    invoke-virtual {v5, p1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069164
    const-string v5, ")"

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069165
    :cond_2
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_2

    .line 1069166
    :cond_3
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto/16 :goto_0

    .line 1069167
    :catch_0
    move-exception v0

    .line 1069168
    :goto_3
    :try_start_3
    const-string v4, "BugReportWriter"

    const-string v5, "Exception saving quick experiments"

    invoke-static {v4, v5, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069169
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v4, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v4}, LX/6GZ;->a(LX/6GY;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1069170
    if-eqz v3, :cond_4

    .line 1069171
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1069172
    :cond_4
    :goto_4
    if-eqz v2, :cond_5

    .line 1069173
    :try_start_5
    iget-object v0, v2, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069174
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_5
    :goto_5
    move-object v0, v1

    .line 1069175
    :cond_6
    :goto_6
    return-object v0

    .line 1069176
    :cond_7
    :try_start_6
    iget-object v0, v2, LX/6FR;->b:Landroid/net/Uri;

    move-object v0, v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1069177
    :try_start_7
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 1069178
    :goto_7
    if-eqz v2, :cond_6

    .line 1069179
    :try_start_8
    iget-object v1, v2, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v1

    .line 1069180
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_6

    .line 1069181
    :catch_1
    move-exception v1

    .line 1069182
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069183
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_6

    .line 1069184
    :catch_2
    move-exception v1

    .line 1069185
    const-string v3, "BugReportWriter"

    const-string v4, "Exception closing quick experiments report."

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069186
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v3, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v3}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_7

    .line 1069187
    :catch_3
    move-exception v0

    .line 1069188
    const-string v3, "BugReportWriter"

    const-string v4, "Exception closing quick experiments report."

    invoke-static {v3, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069189
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v3, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v3}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_4

    .line 1069190
    :catch_4
    move-exception v0

    .line 1069191
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069192
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_5

    .line 1069193
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_8
    if-eqz v3, :cond_8

    .line 1069194
    :try_start_9
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 1069195
    :cond_8
    :goto_9
    if-eqz v2, :cond_9

    .line 1069196
    :try_start_a
    iget-object v1, v2, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v1

    .line 1069197
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 1069198
    :cond_9
    :goto_a
    throw v0

    .line 1069199
    :catch_5
    move-exception v1

    .line 1069200
    const-string v3, "BugReportWriter"

    const-string v4, "Exception closing quick experiments report."

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069201
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v3, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v3}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_9

    .line 1069202
    :catch_6
    move-exception v1

    .line 1069203
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069204
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_a

    .line 1069205
    :catchall_1
    move-exception v0

    move-object v3, v1

    goto :goto_8

    :catchall_2
    move-exception v0

    goto :goto_8

    .line 1069206
    :catch_7
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto/16 :goto_3

    :catch_8
    move-exception v0

    move-object v3, v1

    goto/16 :goto_3

    :cond_a
    const-string v5, "classic"

    goto/16 :goto_1
.end method

.method public static f(Ljava/io/File;)Landroid/net/Uri;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1069012
    :try_start_0
    const-string v0, "stacktrace-dump.txt"

    invoke-static {p0, v0}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1069013
    :try_start_1
    iget-object v0, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069014
    invoke-static {v0}, LX/0Bl;->dumpStackTraces(Ljava/io/OutputStream;)V

    .line 1069015
    iget-object v0, v1, LX/6FR;->b:Landroid/net/Uri;

    move-object v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1069016
    :try_start_2
    iget-object v2, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v2

    .line 1069017
    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V

    .line 1069018
    :goto_0
    return-object v0

    .line 1069019
    :catchall_0
    move-exception v0

    .line 1069020
    iget-object v2, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v2

    .line 1069021
    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/1md;->a(Ljava/io/Closeable;Z)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1069022
    :catch_0
    move-exception v0

    .line 1069023
    const-string v1, "BugReportWriter"

    const-string v2, "Exception saving stack trace"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069024
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g$redex0(LX/6Ft;Ljava/io/File;)Landroid/net/Uri;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1069095
    :try_start_0
    const-string v0, "gatekeepers.txt"

    invoke-static {p1, v0}, LX/6G3;->a(Ljava/io/File;Ljava/lang/String;)LX/6FR;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 1069096
    :try_start_1
    iget-object v0, p0, LX/6Ft;->e:LX/0Uh;

    invoke-virtual {v0}, LX/0Uh;->a()Ljava/util/SortedMap;

    move-result-object v0

    .line 1069097
    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/PrintWriter;

    .line 1069098
    iget-object v5, v3, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v5, v5

    .line 1069099
    invoke-direct {v1, v5}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v4, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1069100
    :try_start_2
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1069101
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " = "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1069102
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->newLine()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 1069103
    :catch_0
    move-exception v0

    move-object v1, v3

    move-object v3, v4

    .line 1069104
    :goto_1
    :try_start_3
    const-string v4, "BugReportWriter"

    const-string v5, "Exception saving Gatekeepers."

    invoke-static {v4, v5, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069105
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v4, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v4}, LX/6GZ;->a(LX/6GY;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1069106
    if-eqz v3, :cond_0

    .line 1069107
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1069108
    :cond_0
    :goto_2
    if-eqz v1, :cond_1

    .line 1069109
    :try_start_5
    iget-object v0, v1, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v0, v0

    .line 1069110
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_1
    :goto_3
    move-object v0, v2

    .line 1069111
    :cond_2
    :goto_4
    return-object v0

    .line 1069112
    :cond_3
    :try_start_6
    iget-object v0, v3, LX/6FR;->b:Landroid/net/Uri;

    move-object v0, v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1069113
    :try_start_7
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 1069114
    :goto_5
    if-eqz v3, :cond_2

    .line 1069115
    :try_start_8
    iget-object v1, v3, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v1

    .line 1069116
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_4

    .line 1069117
    :catch_1
    move-exception v1

    .line 1069118
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069119
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_4

    .line 1069120
    :catch_2
    move-exception v1

    .line 1069121
    const-string v2, "BugReportWriter"

    const-string v4, "Exception closing Gatekeepers report."

    invoke-static {v2, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069122
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_5

    .line 1069123
    :catch_3
    move-exception v0

    .line 1069124
    const-string v3, "BugReportWriter"

    const-string v4, "Exception closing Gatekeepers report."

    invoke-static {v3, v4, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069125
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v3, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v3}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_2

    .line 1069126
    :catch_4
    move-exception v0

    .line 1069127
    const-string v1, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v1, v3, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069128
    iget-object v0, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v1, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v0, v1}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_3

    .line 1069129
    :catchall_0
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    :goto_6
    if-eqz v4, :cond_4

    .line 1069130
    :try_start_9
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 1069131
    :cond_4
    :goto_7
    if-eqz v3, :cond_5

    .line 1069132
    :try_start_a
    iget-object v1, v3, LX/6FR;->a:Ljava/io/OutputStream;

    move-object v1, v1

    .line 1069133
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 1069134
    :cond_5
    :goto_8
    throw v0

    .line 1069135
    :catch_5
    move-exception v1

    .line 1069136
    const-string v2, "BugReportWriter"

    const-string v4, "Exception closing Gatekeepers report."

    invoke-static {v2, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069137
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_7

    .line 1069138
    :catch_6
    move-exception v1

    .line 1069139
    const-string v2, "BugReportWriter"

    const-string v3, "Exception closing attachment stream."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1069140
    iget-object v1, p0, LX/6Ft;->q:LX/6GZ;

    sget-object v2, LX/6GY;->BUG_REPORT_ATTACHMENT_FAILED_TO_SERIALIZE:LX/6GY;

    invoke-virtual {v1, v2}, LX/6GZ;->a(LX/6GY;)V

    goto :goto_8

    .line 1069141
    :catchall_1
    move-exception v0

    move-object v4, v2

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object v4, v3

    move-object v3, v1

    goto :goto_6

    .line 1069142
    :catch_7
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    goto/16 :goto_1

    :catch_8
    move-exception v0

    move-object v1, v3

    move-object v3, v2

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(LX/6FU;)LX/6FU;
    .locals 11

    .prologue
    .line 1069044
    iget-object v0, p1, LX/6FU;->u:Ljava/lang/String;

    move-object v0, v0

    .line 1069045
    iget-object v1, p1, LX/6FU;->v:Ljava/lang/String;

    move-object v1, v1

    .line 1069046
    iget-boolean v2, p1, LX/6FU;->x:Z

    move v2, v2

    .line 1069047
    invoke-static {v0, v1, v2}, LX/6Fi;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    move-object v0, v0

    .line 1069048
    iget-object v1, p0, LX/6Ft;->r:LX/0SI;

    invoke-interface {v1, v0}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    move-result-object v2

    const/4 v1, 0x0

    .line 1069049
    :try_start_0
    invoke-static {}, LX/6G6;->a()LX/6G6;

    move-result-object v4

    .line 1069050
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 1069051
    iget-object v5, v4, LX/6G6;->f:LX/0P1;

    move-object v5, v5

    .line 1069052
    invoke-virtual {v3, v5}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 1069053
    iget-object v5, p0, LX/6Ft;->k:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Zd;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1069054
    :try_start_1
    invoke-interface {v5}, LX/0Zd;->d()Ljava/util/Map;

    move-result-object v5

    .line 1069055
    if-eqz v5, :cond_0

    .line 1069056
    invoke-virtual {v3, v5}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    goto :goto_0

    .line 1069057
    :catch_0
    move-exception v5

    .line 1069058
    iget-object v7, p0, LX/6Ft;->c:LX/03V;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, LX/6Ft;->u:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "addExtraDataFromWorkerThread"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1069059
    :cond_1
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v8

    .line 1069060
    const-string v3, "StoryZombies"

    invoke-virtual {v8, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1069061
    iput-object v3, p1, LX/6FU;->q:Ljava/lang/String;

    .line 1069062
    iget-object v3, v4, LX/6G6;->e:Landroid/content/Context;

    move-object v3, v3

    .line 1069063
    if-eqz v3, :cond_5

    .line 1069064
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v6
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1069065
    :try_start_3
    iget-object v5, p0, LX/6Ft;->d:LX/6Gl;

    invoke-interface {v5, v3}, LX/6Gl;->a(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1069066
    :goto_1
    :try_start_4
    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    move-object v5, v5

    .line 1069067
    :goto_2
    iput-object v5, p1, LX/6FU;->g:LX/0P1;

    .line 1069068
    iget-object v3, v4, LX/6G6;->e:Landroid/content/Context;

    move-object v5, v3

    .line 1069069
    iget-object v3, v4, LX/6G6;->b:Ljava/io/File;

    move-object v6, v3

    .line 1069070
    iget-object v3, v4, LX/6G6;->i:Ljava/io/File;

    move-object v7, v3

    .line 1069071
    iget-object v3, v4, LX/6G6;->g:LX/0Rf;

    move-object v9, v3

    .line 1069072
    iget-object v3, v4, LX/6G6;->d:Landroid/os/Bundle;

    move-object v10, v3

    .line 1069073
    move-object v3, p0

    move-object v4, p1

    invoke-static/range {v3 .. v10}, LX/6Ft;->a(LX/6Ft;LX/6FU;Landroid/content/Context;Ljava/io/File;Ljava/io/File;LX/0P1;LX/0Rf;Landroid/os/Bundle;)V

    .line 1069074
    iget-object v3, p0, LX/6Ft;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v3}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1069075
    if-eqz v3, :cond_6

    .line 1069076
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v4

    .line 1069077
    iput-object v4, p1, LX/6FU;->o:Ljava/lang/String;

    .line 1069078
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v3

    .line 1069079
    iput-object v3, p1, LX/6FU;->p:Ljava/lang/String;

    .line 1069080
    :goto_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1069081
    iput-object v3, p1, LX/6FU;->s:Ljava/lang/String;

    .line 1069082
    move-object v3, p1

    .line 1069083
    move-object v0, v3
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1069084
    if-eqz v2, :cond_2

    invoke-interface {v2}, LX/1mW;->close()V

    :cond_2
    return-object v0

    .line 1069085
    :catch_1
    move-exception v1

    :try_start_5
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1069086
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_3

    if-eqz v1, :cond_4

    :try_start_6
    invoke-interface {v2}, LX/1mW;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    :cond_3
    :goto_4
    throw v0

    :catch_2
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_4

    :cond_4
    invoke-interface {v2}, LX/1mW;->close()V

    goto :goto_4

    .line 1069087
    :cond_5
    new-instance v5, LX/0P2;

    invoke-direct {v5}, LX/0P2;-><init>()V

    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    goto :goto_2

    .line 1069088
    :catch_3
    move-exception v5

    .line 1069089
    iget-object v7, p0, LX/6Ft;->c:LX/03V;

    const-string v9, "BugReporter.getFlytrapExtrasFromWorkerThread"

    invoke-virtual {v7, v9, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1069090
    :cond_6
    const-string v3, "NONE"

    .line 1069091
    iput-object v3, p1, LX/6FU;->o:Ljava/lang/String;

    .line 1069092
    const-string v3, "NONE"

    .line 1069093
    iput-object v3, p1, LX/6FU;->p:Ljava/lang/String;

    .line 1069094
    goto :goto_3
.end method

.method public final a(Ljava/util/List;Landroid/os/Bundle;Landroid/content/Context;LX/0P1;LX/0Rf;LX/6Fb;)LX/6FU;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/os/Bundle;",
            "Landroid/content/Context;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Rf",
            "<",
            "LX/1MS;",
            ">;",
            "LX/6Fb;",
            ")",
            "LX/6FU;"
        }
    .end annotation

    .prologue
    .line 1069025
    invoke-static {}, LX/6G6;->a()LX/6G6;

    move-result-object v0

    .line 1069026
    iget-object v1, p0, LX/6Ft;->a:LX/6G3;

    .line 1069027
    const-string v3, "bugreports"

    invoke-static {v1, v3}, LX/6G3;->c(LX/6G3;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 1069028
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 1069029
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1069030
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1069031
    move-object v1, v4

    .line 1069032
    invoke-direct {p0, v1}, LX/6Ft;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 1069033
    iput-object v2, v0, LX/6G6;->b:Ljava/io/File;

    .line 1069034
    invoke-direct {p0, v1}, LX/6Ft;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1069035
    iput-object v1, v0, LX/6G6;->i:Ljava/io/File;

    .line 1069036
    iput-object p1, v0, LX/6G6;->c:Ljava/util/List;

    .line 1069037
    iput-object p2, v0, LX/6G6;->d:Landroid/os/Bundle;

    .line 1069038
    iput-object p3, v0, LX/6G6;->e:Landroid/content/Context;

    .line 1069039
    iput-object p4, v0, LX/6G6;->f:LX/0P1;

    .line 1069040
    iput-object p5, v0, LX/6G6;->g:LX/0Rf;

    .line 1069041
    iput-object p6, v0, LX/6G6;->h:LX/6Fb;

    .line 1069042
    invoke-direct {p0}, LX/6Ft;->a()LX/6FU;

    move-result-object v0

    .line 1069043
    return-object v0
.end method
