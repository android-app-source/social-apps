.class public final LX/59J;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 33

    .prologue
    .line 851740
    const/16 v27, 0x0

    .line 851741
    const/16 v26, 0x0

    .line 851742
    const/16 v25, 0x0

    .line 851743
    const/16 v24, 0x0

    .line 851744
    const/16 v23, 0x0

    .line 851745
    const/16 v22, 0x0

    .line 851746
    const/16 v21, 0x0

    .line 851747
    const/16 v20, 0x0

    .line 851748
    const/16 v17, 0x0

    .line 851749
    const-wide/16 v18, 0x0

    .line 851750
    const/16 v16, 0x0

    .line 851751
    const/4 v15, 0x0

    .line 851752
    const/4 v14, 0x0

    .line 851753
    const/4 v13, 0x0

    .line 851754
    const/4 v12, 0x0

    .line 851755
    const/4 v11, 0x0

    .line 851756
    const/4 v10, 0x0

    .line 851757
    const/4 v9, 0x0

    .line 851758
    const/4 v8, 0x0

    .line 851759
    const/4 v7, 0x0

    .line 851760
    const/4 v6, 0x0

    .line 851761
    const/4 v5, 0x0

    .line 851762
    const/4 v4, 0x0

    .line 851763
    const/4 v3, 0x0

    .line 851764
    const/4 v2, 0x0

    .line 851765
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1b

    .line 851766
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 851767
    const/4 v2, 0x0

    .line 851768
    :goto_0
    return v2

    .line 851769
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v29, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v29

    if-eq v2, v0, :cond_12

    .line 851770
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 851771
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 851772
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 851773
    const-string v29, "attachments"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_1

    .line 851774
    invoke-static/range {p0 .. p1}, LX/59I;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 851775
    :cond_1
    const-string v29, "author"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_2

    .line 851776
    invoke-static/range {p0 .. p1}, LX/59B;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 851777
    :cond_2
    const-string v29, "body"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_3

    .line 851778
    invoke-static/range {p0 .. p1}, LX/59F;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 851779
    :cond_3
    const-string v29, "body_markdown_html"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_4

    .line 851780
    invoke-static/range {p0 .. p1}, LX/59C;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 851781
    :cond_4
    const-string v29, "can_edit_constituent_title"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_5

    .line 851782
    const/4 v2, 0x1

    .line 851783
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    move/from16 v24, v12

    move v12, v2

    goto :goto_1

    .line 851784
    :cond_5
    const-string v29, "can_viewer_delete"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_6

    .line 851785
    const/4 v2, 0x1

    .line 851786
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    move/from16 v23, v11

    move v11, v2

    goto/16 :goto_1

    .line 851787
    :cond_6
    const-string v29, "can_viewer_edit"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_7

    .line 851788
    const/4 v2, 0x1

    .line 851789
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v22, v7

    move v7, v2

    goto/16 :goto_1

    .line 851790
    :cond_7
    const-string v29, "can_viewer_share"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_8

    .line 851791
    const/4 v2, 0x1

    .line 851792
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v21, v6

    move v6, v2

    goto/16 :goto_1

    .line 851793
    :cond_8
    const-string v29, "constituent_title"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_9

    .line 851794
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 851795
    :cond_9
    const-string v29, "created_time"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_a

    .line 851796
    const/4 v2, 0x1

    .line 851797
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 851798
    :cond_a
    const-string v29, "edit_history"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_b

    .line 851799
    invoke-static/range {p0 .. p1}, LX/59G;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 851800
    :cond_b
    const-string v29, "id"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_c

    .line 851801
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 851802
    :cond_c
    const-string v29, "is_featured"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_d

    .line 851803
    const/4 v2, 0x1

    .line 851804
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    move/from16 v17, v10

    move v10, v2

    goto/16 :goto_1

    .line 851805
    :cond_d
    const-string v29, "is_marked_as_spam"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_e

    .line 851806
    const/4 v2, 0x1

    .line 851807
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v16, v9

    move v9, v2

    goto/16 :goto_1

    .line 851808
    :cond_e
    const-string v29, "is_pinned"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_f

    .line 851809
    const/4 v2, 0x1

    .line 851810
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move v15, v8

    move v8, v2

    goto/16 :goto_1

    .line 851811
    :cond_f
    const-string v29, "spam_display_mode"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_10

    .line 851812
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 851813
    :cond_10
    const-string v29, "translatability_for_viewer"

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 851814
    invoke-static/range {p0 .. p1}, LX/59K;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 851815
    :cond_11
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 851816
    :cond_12
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 851817
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851818
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851819
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851820
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851821
    if-eqz v12, :cond_13

    .line 851822
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851823
    :cond_13
    if-eqz v11, :cond_14

    .line 851824
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851825
    :cond_14
    if-eqz v7, :cond_15

    .line 851826
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851827
    :cond_15
    if-eqz v6, :cond_16

    .line 851828
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851829
    :cond_16
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851830
    if-eqz v3, :cond_17

    .line 851831
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 851832
    :cond_17
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851833
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 851834
    if-eqz v10, :cond_18

    .line 851835
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851836
    :cond_18
    if-eqz v9, :cond_19

    .line 851837
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 851838
    :cond_19
    if-eqz v8, :cond_1a

    .line 851839
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 851840
    :cond_1a
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 851841
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 851842
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1b
    move/from16 v28, v27

    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v21, v20

    move/from16 v20, v17

    move/from16 v17, v14

    move v14, v11

    move v11, v8

    move v8, v2

    move-wide/from16 v31, v18

    move/from16 v18, v15

    move/from16 v19, v16

    move/from16 v16, v13

    move v15, v12

    move v12, v9

    move v13, v10

    move v10, v4

    move v9, v3

    move v3, v5

    move-wide/from16 v4, v31

    goto/16 :goto_1
.end method
