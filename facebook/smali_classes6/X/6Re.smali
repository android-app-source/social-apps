.class public LX/6Re;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/6Re;


# instance fields
.field public final a:Z


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1090142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1090143
    const/16 v0, 0x43c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/6Re;->a:Z

    .line 1090144
    return-void
.end method

.method public static a(LX/0QB;)LX/6Re;
    .locals 4

    .prologue
    .line 1090145
    sget-object v0, LX/6Re;->b:LX/6Re;

    if-nez v0, :cond_1

    .line 1090146
    const-class v1, LX/6Re;

    monitor-enter v1

    .line 1090147
    :try_start_0
    sget-object v0, LX/6Re;->b:LX/6Re;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1090148
    if-eqz v2, :cond_0

    .line 1090149
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1090150
    new-instance p0, LX/6Re;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/6Re;-><init>(LX/0Uh;)V

    .line 1090151
    move-object v0, p0

    .line 1090152
    sput-object v0, LX/6Re;->b:LX/6Re;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1090153
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1090154
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1090155
    :cond_1
    sget-object v0, LX/6Re;->b:LX/6Re;

    return-object v0

    .line 1090156
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1090157
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
