.class public final LX/5tF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1015507
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1015508
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1015509
    :goto_0
    return v1

    .line 1015510
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_2

    .line 1015511
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1015512
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1015513
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 1015514
    const-string v8, "value"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1015515
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 1015516
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1015517
    :cond_2
    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1015518
    if-eqz v0, :cond_3

    move-object v0, p1

    .line 1015519
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1015520
    :cond_3
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1015521
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1015522
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1015523
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    .line 1015524
    const-string v2, "value"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1015525
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1015526
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1015527
    return-void
.end method
