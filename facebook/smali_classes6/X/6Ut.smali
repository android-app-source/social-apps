.class public final LX/6Ut;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Uo;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/6Ut;


# instance fields
.field public final b:LX/6V0;

.field public final c:LX/0lC;

.field private final d:LX/03V;


# direct methods
.method public constructor <init>(LX/6V0;LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1103119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103120
    iput-object p1, p0, LX/6Ut;->b:LX/6V0;

    .line 1103121
    iput-object p2, p0, LX/6Ut;->c:LX/0lC;

    .line 1103122
    iput-object p3, p0, LX/6Ut;->d:LX/03V;

    .line 1103123
    return-void
.end method

.method public static a(LX/6Ut;Landroid/view/View;LX/162;I)I
    .locals 5

    .prologue
    .line 1103101
    add-int/lit8 v2, p3, 0x1

    .line 1103102
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1103103
    :goto_0
    return v2

    .line 1103104
    :cond_0
    const/16 v0, 0x13

    if-le v2, v0, :cond_1

    .line 1103105
    iget-object v0, p0, LX/6Ut;->c:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v0

    .line 1103106
    const-string v1, "offending_view"

    iget-object v3, p0, LX/6Ut;->b:LX/6V0;

    sget-object v4, LX/6Uz;->NONE:LX/6Uz;

    invoke-virtual {v3, p1, v4}, LX/6V0;->b(Landroid/view/View;LX/6Uz;)LX/0m9;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 1103107
    const-string v1, "offending_view_depth"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1103108
    move-object v0, v0

    .line 1103109
    invoke-virtual {p2, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 1103110
    :cond_1
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 1103111
    check-cast p1, Landroid/view/ViewGroup;

    .line 1103112
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    .line 1103113
    const/4 v0, 0x0

    move v3, v0

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    .line 1103114
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1103115
    invoke-static {p0, v0, p2, v2}, LX/6Ut;->a(LX/6Ut;Landroid/view/View;LX/162;I)I

    move-result v0

    .line 1103116
    if-le v0, v1, :cond_4

    .line 1103117
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_2
    move v1, v2

    :cond_3
    move v2, v1

    .line 1103118
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public static a(LX/0QB;)LX/6Ut;
    .locals 6

    .prologue
    .line 1103124
    sget-object v0, LX/6Ut;->e:LX/6Ut;

    if-nez v0, :cond_1

    .line 1103125
    const-class v1, LX/6Ut;

    monitor-enter v1

    .line 1103126
    :try_start_0
    sget-object v0, LX/6Ut;->e:LX/6Ut;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1103127
    if-eqz v2, :cond_0

    .line 1103128
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1103129
    new-instance p0, LX/6Ut;

    invoke-static {v0}, LX/6V0;->b(LX/0QB;)LX/6V0;

    move-result-object v3

    check-cast v3, LX/6V0;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/6Ut;-><init>(LX/6V0;LX/0lC;LX/03V;)V

    .line 1103130
    move-object v0, p0

    .line 1103131
    sput-object v0, LX/6Ut;->e:LX/6Ut;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1103132
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1103133
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1103134
    :cond_1
    sget-object v0, LX/6Ut;->e:LX/6Ut;

    return-object v0

    .line 1103135
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1103136
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1103097
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x82

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1103098
    const-string v1, "Flatten view hierarchies. Don\'t add ViewGroups when they\'re unnecessary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1103099
    const-string v1, ". Consider using LayoutParams to achieve your layout needs."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1103100
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1103080
    iget-object v0, p0, LX/6Ut;->c:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->f()LX/162;

    move-result-object v0

    .line 1103081
    const/4 v1, -0x1

    invoke-static {p0, p1, v0, v1}, LX/6Ut;->a(LX/6Ut;Landroid/view/View;LX/162;I)I

    move-result v1

    move v1, v1

    .line 1103082
    const-string v2, "max_view_depth"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103083
    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v1

    .line 1103084
    if-nez v1, :cond_0

    .line 1103085
    const/4 v0, 0x0

    .line 1103086
    :goto_0
    return v0

    .line 1103087
    :cond_0
    const-string v2, "num_rule_breakers"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103088
    :try_start_0
    iget-object v1, p0, LX/6Ut;->c:LX/0lC;

    invoke-virtual {v1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1103089
    :goto_1
    const-string v1, "rule_breakers"

    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103090
    const/4 v0, 0x1

    goto :goto_0

    .line 1103091
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1103092
    const-string v2, "Error serializing rule breakers: "

    .line 1103093
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1103094
    iget-object v3, p0, LX/6Ut;->d:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1103095
    sget-object v3, LX/6Ut;->a:Ljava/lang/Class;

    invoke-static {v3, v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1103096
    const-string v0, "View Hierarchy Too Deep"

    return-object v0
.end method
