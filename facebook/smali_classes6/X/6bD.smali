.class public LX/6bD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0TD;

.field private b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6bO;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/6bO;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/6b8;


# direct methods
.method public constructor <init>(LX/0TD;LX/0Or;LX/6b8;)V
    .locals 1
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "LX/6bO;",
            ">;",
            "LX/6b8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1113131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113132
    iput-object p1, p0, LX/6bD;->a:LX/0TD;

    .line 1113133
    iput-object p2, p0, LX/6bD;->b:LX/0Or;

    .line 1113134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/6bD;->c:Ljava/util/Map;

    .line 1113135
    iput-object p3, p0, LX/6bD;->d:LX/6b8;

    .line 1113136
    return-void
.end method

.method public static a(LX/0QB;)LX/6bD;
    .locals 4

    .prologue
    .line 1113137
    new-instance v2, LX/6bD;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, LX/0TD;

    const/16 v1, 0x2642

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const-class v1, LX/6b8;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/6b8;

    invoke-direct {v2, v0, v3, v1}, LX/6bD;-><init>(LX/0TD;LX/0Or;LX/6b8;)V

    .line 1113138
    move-object v0, v2

    .line 1113139
    return-object v0
.end method

.method public static b(LX/6bD;Ljava/lang/String;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/media/transcode/MediaTranscodeParameters;LX/6b7;)LX/6bA;
    .locals 2

    .prologue
    .line 1113140
    if-nez p2, :cond_0

    .line 1113141
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must provide non null item to transcode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1113142
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    .line 1113143
    sget-object v1, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v1, :cond_1

    .line 1113144
    iget-object v0, p0, LX/6bD;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bO;

    .line 1113145
    iget-object v1, p0, LX/6bD;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113146
    check-cast p2, Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v0, p2, p3, p4}, LX/6bO;->a(Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/media/transcode/MediaTranscodeParameters;LX/6b7;)LX/6bA;

    move-result-object v0

    .line 1113147
    iget-object v1, p0, LX/6bD;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113148
    return-object v0

    .line 1113149
    :cond_1
    sget-object v1, LX/4gF;->PHOTO:LX/4gF;

    if-ne v0, v1, :cond_2

    .line 1113150
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Photo transcodes are currently unsupported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1113151
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unsupported media type passed into transcoder"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
