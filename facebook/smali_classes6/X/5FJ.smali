.class public final LX/5FJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 885164
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 885165
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885166
    :goto_0
    return v1

    .line 885167
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885168
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 885169
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 885170
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885171
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 885172
    const-string v7, "focus"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 885173
    invoke-static {p0, p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 885174
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 885175
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 885176
    :cond_3
    const-string v7, "largeImage"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 885177
    const/4 v6, 0x0

    .line 885178
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v7, :cond_b

    .line 885179
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885180
    :goto_2
    move v3, v6

    .line 885181
    goto :goto_1

    .line 885182
    :cond_4
    const-string v7, "mediumImage"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 885183
    const/4 v6, 0x0

    .line 885184
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v7, :cond_f

    .line 885185
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885186
    :goto_3
    move v2, v6

    .line 885187
    goto :goto_1

    .line 885188
    :cond_5
    const-string v7, "url"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 885189
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 885190
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 885191
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 885192
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 885193
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 885194
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 885195
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 885196
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 885197
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885198
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 885199
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 885200
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885201
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 885202
    const-string v8, "uri"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 885203
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_4

    .line 885204
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 885205
    invoke-virtual {p1, v6, v3}, LX/186;->b(II)V

    .line 885206
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_2

    :cond_b
    move v3, v6

    goto :goto_4

    .line 885207
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 885208
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_e

    .line 885209
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 885210
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 885211
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_d

    if-eqz v7, :cond_d

    .line 885212
    const-string v8, "uri"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 885213
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_5

    .line 885214
    :cond_e
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 885215
    invoke-virtual {p1, v6, v2}, LX/186;->b(II)V

    .line 885216
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_f
    move v2, v6

    goto :goto_5
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 885217
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885218
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885219
    if-eqz v0, :cond_0

    .line 885220
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885221
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 885222
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885223
    if-eqz v0, :cond_1

    .line 885224
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885225
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885226
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885227
    if-eqz v0, :cond_3

    .line 885228
    const-string v1, "largeImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885229
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885230
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 885231
    if-eqz v1, :cond_2

    .line 885232
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885233
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885234
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885235
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 885236
    if-eqz v0, :cond_5

    .line 885237
    const-string v1, "mediumImage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885238
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 885239
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 885240
    if-eqz v1, :cond_4

    .line 885241
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885242
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885243
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885244
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 885245
    if-eqz v0, :cond_6

    .line 885246
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 885247
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 885248
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 885249
    return-void
.end method
