.class public LX/6Zb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0T3;

.field private final b:LX/6ZU;

.field public final c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field private final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/6Za;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/6Zc;

.field public f:Lcom/facebook/base/activity/FbFragmentActivity;

.field private g:LX/6ZZ;

.field public h:Z

.field public i:LX/6ZY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/6ZU;LX/6Zc;Ljava/util/concurrent/Executor;LX/1Ck;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1111242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1111243
    new-instance v0, LX/6ZV;

    invoke-direct {v0, p0}, LX/6ZV;-><init>(LX/6Zb;)V

    iput-object v0, p0, LX/6Zb;->a:LX/0T3;

    .line 1111244
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Zb;->h:Z

    .line 1111245
    iput-object p1, p0, LX/6Zb;->b:LX/6ZU;

    .line 1111246
    iput-object p3, p0, LX/6Zb;->c:Ljava/util/concurrent/Executor;

    .line 1111247
    iput-object p4, p0, LX/6Zb;->d:LX/1Ck;

    .line 1111248
    iput-object p2, p0, LX/6Zb;->e:LX/6Zc;

    .line 1111249
    return-void
.end method

.method public static a(LX/0QB;)LX/6Zb;
    .locals 1

    .prologue
    .line 1111295
    invoke-static {p0}, LX/6Zb;->b(LX/0QB;)LX/6Zb;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/6Zb;LX/6ZY;)V
    .locals 6

    .prologue
    .line 1111286
    iget-object v0, p0, LX/6Zb;->e:LX/6Zc;

    iget-object v1, p0, LX/6Zb;->j:Ljava/lang/String;

    iget-object v2, p0, LX/6Zb;->k:Ljava/lang/String;

    invoke-virtual {p1}, LX/6ZY;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1111287
    const-string v4, "gms_ls_upsell_result"

    invoke-static {v0, v4}, LX/6Zc;->a(LX/6Zc;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    .line 1111288
    if-nez v4, :cond_0

    .line 1111289
    :goto_0
    iget-object v0, p0, LX/6Zb;->g:LX/6ZZ;

    invoke-interface {v0, p1}, LX/6ZZ;->a(LX/6ZY;)V

    .line 1111290
    return-void

    .line 1111291
    :cond_0
    const-string v5, "surface"

    invoke-virtual {v4, v5, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1111292
    const-string v5, "mechanism"

    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1111293
    const-string v5, "result"

    invoke-virtual {v4, v5, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1111294
    invoke-virtual {v4}, LX/0oG;->d()V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/6Zb;
    .locals 5

    .prologue
    .line 1111281
    new-instance v4, LX/6Zb;

    invoke-static {p0}, LX/6ZU;->b(LX/0QB;)LX/6ZU;

    move-result-object v0

    check-cast v0, LX/6ZU;

    .line 1111282
    new-instance v3, LX/6Zc;

    invoke-static {p0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v1

    check-cast v1, LX/0Zm;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v3, v1, v2}, LX/6Zc;-><init>(LX/0Zm;LX/0Zb;)V

    .line 1111283
    move-object v1, v3

    .line 1111284
    check-cast v1, LX/6Zc;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-direct {v4, v0, v1, v2, v3}, LX/6Zb;-><init>(LX/6ZU;LX/6Zc;Ljava/util/concurrent/Executor;LX/1Ck;)V

    .line 1111285
    return-object v4
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1111275
    iget-object v0, p0, LX/6Zb;->d:LX/1Ck;

    sget-object v1, LX/6Za;->GMS_SETTINGS_LOOKUP_TASK:LX/6Za;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1111276
    iget-object v0, p0, LX/6Zb;->f:Lcom/facebook/base/activity/FbFragmentActivity;

    if-eqz v0, :cond_0

    .line 1111277
    iget-object v0, p0, LX/6Zb;->f:Lcom/facebook/base/activity/FbFragmentActivity;

    iget-object v1, p0, LX/6Zb;->a:LX/0T3;

    invoke-virtual {v0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(LX/0T2;)V

    .line 1111278
    :cond_0
    iput-object v2, p0, LX/6Zb;->f:Lcom/facebook/base/activity/FbFragmentActivity;

    .line 1111279
    iput-object v2, p0, LX/6Zb;->g:LX/6ZZ;

    .line 1111280
    return-void
.end method

.method public final a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1111259
    iget-boolean v0, p0, LX/6Zb;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/6Zb;->i:LX/6ZY;

    if-eqz v0, :cond_1

    .line 1111260
    :cond_0
    :goto_0
    return-void

    .line 1111261
    :cond_1
    iget-object v0, p0, LX/6Zb;->f:Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111262
    iget-object v0, p0, LX/6Zb;->g:LX/6ZZ;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111263
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111264
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111265
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111266
    iput-object p2, p0, LX/6Zb;->j:Ljava/lang/String;

    .line 1111267
    iput-object p3, p0, LX/6Zb;->k:Ljava/lang/String;

    .line 1111268
    iget-object v0, p0, LX/6Zb;->e:LX/6Zc;

    iget-object v1, p0, LX/6Zb;->j:Ljava/lang/String;

    iget-object v2, p0, LX/6Zb;->k:Ljava/lang/String;

    .line 1111269
    const-string v3, "gms_ls_upsell_requested"

    invoke-static {v0, v3}, LX/6Zc;->a(LX/6Zc;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    .line 1111270
    if-nez v3, :cond_2

    .line 1111271
    :goto_1
    iget-object v0, p0, LX/6Zb;->d:LX/1Ck;

    sget-object v1, LX/6Za;->GMS_SETTINGS_LOOKUP_TASK:LX/6Za;

    iget-object v2, p0, LX/6Zb;->b:LX/6ZU;

    invoke-virtual {v2, p1}, LX/6ZU;->a(LX/2si;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/6ZW;

    invoke-direct {v3, p0}, LX/6ZW;-><init>(LX/6Zb;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 1111272
    :cond_2
    const-string p2, "surface"

    invoke-virtual {v3, p2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1111273
    const-string p2, "mechanism"

    invoke-virtual {v3, p2, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1111274
    invoke-virtual {v3}, LX/0oG;->d()V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/base/activity/FbFragmentActivity;LX/6ZZ;)V
    .locals 2

    .prologue
    .line 1111255
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/activity/FbFragmentActivity;

    iput-object v0, p0, LX/6Zb;->f:Lcom/facebook/base/activity/FbFragmentActivity;

    .line 1111256
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ZZ;

    iput-object v0, p0, LX/6Zb;->g:LX/6ZZ;

    .line 1111257
    iget-object v0, p0, LX/6Zb;->f:Lcom/facebook/base/activity/FbFragmentActivity;

    iget-object v1, p0, LX/6Zb;->a:LX/0T3;

    invoke-virtual {v0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 1111258
    return-void
.end method

.method public final a(Lcom/facebook/base/fragment/FbFragment;LX/6ZZ;)V
    .locals 2

    .prologue
    .line 1111250
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1111251
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111252
    instance-of v1, v0, Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1111253
    check-cast v0, Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-virtual {p0, v0, p2}, LX/6Zb;->a(Lcom/facebook/base/activity/FbFragmentActivity;LX/6ZZ;)V

    .line 1111254
    return-void
.end method
