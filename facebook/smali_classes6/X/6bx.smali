.class public final enum LX/6bx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6bx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6bx;

.field public static final enum FACEBOOK_STORY_ATTACHMENT:LX/6bx;

.field public static final enum MESSAGE_ATTACHMENT:LX/6bx;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1114023
    new-instance v0, LX/6bx;

    const-string v1, "MESSAGE_ATTACHMENT"

    invoke-direct {v0, v1, v2}, LX/6bx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6bx;->MESSAGE_ATTACHMENT:LX/6bx;

    .line 1114024
    new-instance v0, LX/6bx;

    const-string v1, "FACEBOOK_STORY_ATTACHMENT"

    invoke-direct {v0, v1, v3}, LX/6bx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6bx;->FACEBOOK_STORY_ATTACHMENT:LX/6bx;

    .line 1114025
    const/4 v0, 0x2

    new-array v0, v0, [LX/6bx;

    sget-object v1, LX/6bx;->MESSAGE_ATTACHMENT:LX/6bx;

    aput-object v1, v0, v2

    sget-object v1, LX/6bx;->FACEBOOK_STORY_ATTACHMENT:LX/6bx;

    aput-object v1, v0, v3

    sput-object v0, LX/6bx;->$VALUES:[LX/6bx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1114027
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6bx;
    .locals 1

    .prologue
    .line 1114028
    const-class v0, LX/6bx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6bx;

    return-object v0
.end method

.method public static values()[LX/6bx;
    .locals 1

    .prologue
    .line 1114026
    sget-object v0, LX/6bx;->$VALUES:[LX/6bx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6bx;

    return-object v0
.end method
