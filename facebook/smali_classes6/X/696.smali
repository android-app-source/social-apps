.class public final LX/696;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field private b:D

.field private c:D

.field private d:D

.field private e:D


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1057318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1057319
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/696;->a:Z

    .line 1057320
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/android/maps/model/LatLng;)LX/696;
    .locals 8

    .prologue
    .line 1057321
    iget-boolean v0, p0, LX/696;->a:Z

    if-nez v0, :cond_0

    .line 1057322
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iput-wide v0, p0, LX/696;->b:D

    .line 1057323
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iput-wide v0, p0, LX/696;->c:D

    .line 1057324
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    iput-wide v0, p0, LX/696;->d:D

    .line 1057325
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    iput-wide v0, p0, LX/696;->e:D

    .line 1057326
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/696;->a:Z

    .line 1057327
    :cond_0
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v2, p0, LX/696;->c:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 1057328
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iput-wide v0, p0, LX/696;->c:D

    .line 1057329
    :cond_1
    :goto_0
    iget-wide v0, p0, LX/696;->d:D

    iget-wide v2, p0, LX/696;->e:D

    invoke-static {v0, v1, v2, v3}, LX/697;->b(DD)D

    move-result-wide v0

    .line 1057330
    iget-wide v2, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-wide v4, p0, LX/696;->e:D

    invoke-static {v2, v3, v4, v5}, LX/697;->b(DD)D

    move-result-wide v2

    .line 1057331
    iget-wide v4, p0, LX/696;->d:D

    iget-wide v6, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v4, v5, v6, v7}, LX/697;->b(DD)D

    move-result-wide v4

    .line 1057332
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Double;->compare(DD)I

    move-result v6

    if-gtz v6, :cond_3

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-gtz v0, :cond_3

    .line 1057333
    :goto_1
    return-object p0

    .line 1057334
    :cond_2
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v2, p0, LX/696;->b:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 1057335
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    iput-wide v0, p0, LX/696;->b:D

    goto :goto_0

    .line 1057336
    :cond_3
    cmpg-double v0, v2, v4

    if-gtz v0, :cond_4

    .line 1057337
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    iput-wide v0, p0, LX/696;->d:D

    goto :goto_1

    .line 1057338
    :cond_4
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    iput-wide v0, p0, LX/696;->e:D

    goto :goto_1
.end method

.method public final a()LX/697;
    .locals 8

    .prologue
    .line 1057339
    new-instance v0, LX/697;

    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v2, p0, LX/696;->b:D

    iget-wide v4, p0, LX/696;->e:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    new-instance v2, Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, p0, LX/696;->c:D

    iget-wide v6, p0, LX/696;->d:D

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v0, v1, v2}, LX/697;-><init>(Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;)V

    return-object v0
.end method
