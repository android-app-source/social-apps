.class public final LX/5AY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 30

    .prologue
    .line 858717
    const/16 v26, 0x0

    .line 858718
    const/16 v25, 0x0

    .line 858719
    const/16 v24, 0x0

    .line 858720
    const/16 v23, 0x0

    .line 858721
    const/16 v22, 0x0

    .line 858722
    const/16 v21, 0x0

    .line 858723
    const/16 v20, 0x0

    .line 858724
    const/16 v19, 0x0

    .line 858725
    const/16 v18, 0x0

    .line 858726
    const/16 v17, 0x0

    .line 858727
    const/16 v16, 0x0

    .line 858728
    const/4 v15, 0x0

    .line 858729
    const/4 v14, 0x0

    .line 858730
    const/4 v13, 0x0

    .line 858731
    const/4 v12, 0x0

    .line 858732
    const/4 v11, 0x0

    .line 858733
    const/4 v10, 0x0

    .line 858734
    const/4 v9, 0x0

    .line 858735
    const/4 v8, 0x0

    .line 858736
    const/4 v7, 0x0

    .line 858737
    const/4 v6, 0x0

    .line 858738
    const/4 v5, 0x0

    .line 858739
    const/4 v4, 0x0

    .line 858740
    const/4 v3, 0x0

    .line 858741
    const/4 v2, 0x0

    .line 858742
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    .line 858743
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 858744
    const/4 v2, 0x0

    .line 858745
    :goto_0
    return v2

    .line 858746
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 858747
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_10

    .line 858748
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v27

    .line 858749
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 858750
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    if-eqz v27, :cond_1

    .line 858751
    const-string v28, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 858752
    const/4 v11, 0x1

    .line 858753
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto :goto_1

    .line 858754
    :cond_2
    const-string v28, "can_see_voice_switcher"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 858755
    const/4 v10, 0x1

    .line 858756
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto :goto_1

    .line 858757
    :cond_3
    const-string v28, "can_viewer_comment"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 858758
    const/4 v9, 0x1

    .line 858759
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto :goto_1

    .line 858760
    :cond_4
    const-string v28, "can_viewer_comment_with_photo"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 858761
    const/4 v8, 0x1

    .line 858762
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto :goto_1

    .line 858763
    :cond_5
    const-string v28, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 858764
    const/4 v7, 0x1

    .line 858765
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto :goto_1

    .line 858766
    :cond_6
    const-string v28, "can_viewer_comment_with_video"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 858767
    const/4 v6, 0x1

    .line 858768
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto :goto_1

    .line 858769
    :cond_7
    const-string v28, "can_viewer_like"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 858770
    const/4 v5, 0x1

    .line 858771
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 858772
    :cond_8
    const-string v28, "can_viewer_subscribe"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 858773
    const/4 v4, 0x1

    .line 858774
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto/16 :goto_1

    .line 858775
    :cond_9
    const-string v28, "comments_mirroring_domain"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 858776
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 858777
    :cond_a
    const-string v28, "does_viewer_like"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 858778
    const/4 v3, 0x1

    .line 858779
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 858780
    :cond_b
    const-string v28, "id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 858781
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 858782
    :cond_c
    const-string v28, "is_viewer_subscribed"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 858783
    const/4 v2, 0x1

    .line 858784
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto/16 :goto_1

    .line 858785
    :cond_d
    const-string v28, "legacy_api_post_id"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 858786
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 858787
    :cond_e
    const-string v28, "remixable_photo_uri"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 858788
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 858789
    :cond_f
    const-string v28, "viewer_acts_as_page"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 858790
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 858791
    :cond_10
    const/16 v27, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 858792
    if-eqz v11, :cond_11

    .line 858793
    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 858794
    :cond_11
    if-eqz v10, :cond_12

    .line 858795
    const/4 v10, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 858796
    :cond_12
    if-eqz v9, :cond_13

    .line 858797
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 858798
    :cond_13
    if-eqz v8, :cond_14

    .line 858799
    const/4 v8, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 858800
    :cond_14
    if-eqz v7, :cond_15

    .line 858801
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 858802
    :cond_15
    if-eqz v6, :cond_16

    .line 858803
    const/4 v6, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 858804
    :cond_16
    if-eqz v5, :cond_17

    .line 858805
    const/4 v5, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 858806
    :cond_17
    if-eqz v4, :cond_18

    .line 858807
    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 858808
    :cond_18
    const/16 v4, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 858809
    if-eqz v3, :cond_19

    .line 858810
    const/16 v3, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 858811
    :cond_19
    const/16 v3, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 858812
    if-eqz v2, :cond_1a

    .line 858813
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->a(IZ)V

    .line 858814
    :cond_1a
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 858815
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 858816
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 858817
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 858818
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 858819
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858820
    if-eqz v0, :cond_0

    .line 858821
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858822
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858823
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858824
    if-eqz v0, :cond_1

    .line 858825
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858826
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858827
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858828
    if-eqz v0, :cond_2

    .line 858829
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858830
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858831
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858832
    if-eqz v0, :cond_3

    .line 858833
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858834
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858835
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858836
    if-eqz v0, :cond_4

    .line 858837
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858838
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858839
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858840
    if-eqz v0, :cond_5

    .line 858841
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858842
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858843
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858844
    if-eqz v0, :cond_6

    .line 858845
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858846
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858847
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858848
    if-eqz v0, :cond_7

    .line 858849
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858850
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858851
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 858852
    if-eqz v0, :cond_8

    .line 858853
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858854
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 858855
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858856
    if-eqz v0, :cond_9

    .line 858857
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858858
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858859
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 858860
    if-eqz v0, :cond_a

    .line 858861
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858862
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 858863
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 858864
    if-eqz v0, :cond_b

    .line 858865
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858866
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 858867
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 858868
    if-eqz v0, :cond_c

    .line 858869
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858870
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 858871
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 858872
    if-eqz v0, :cond_d

    .line 858873
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858874
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 858875
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 858876
    if-eqz v0, :cond_e

    .line 858877
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 858878
    invoke-static {p0, v0, p2, p3}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 858879
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 858880
    return-void
.end method
