.class public final enum LX/6ZT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6ZT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6ZT;

.field public static final enum EASY_RESOLUTION_ATTEMPTED:LX/6ZT;

.field public static final enum EASY_RESOLUTION_POSSIBLE:LX/6ZT;

.field public static final enum EASY_RESOLUTION_UNAVAILABLE:LX/6ZT;

.field public static final enum LOCATION_SETTINGS_OK:LX/6ZT;

.field public static final enum UNKNOWN:LX/6ZT;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1111143
    new-instance v0, LX/6ZT;

    const-string v1, "LOCATION_SETTINGS_OK"

    invoke-direct {v0, v1, v2}, LX/6ZT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZT;->LOCATION_SETTINGS_OK:LX/6ZT;

    .line 1111144
    new-instance v0, LX/6ZT;

    const-string v1, "EASY_RESOLUTION_UNAVAILABLE"

    invoke-direct {v0, v1, v3}, LX/6ZT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZT;->EASY_RESOLUTION_UNAVAILABLE:LX/6ZT;

    .line 1111145
    new-instance v0, LX/6ZT;

    const-string v1, "EASY_RESOLUTION_POSSIBLE"

    invoke-direct {v0, v1, v4}, LX/6ZT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZT;->EASY_RESOLUTION_POSSIBLE:LX/6ZT;

    .line 1111146
    new-instance v0, LX/6ZT;

    const-string v1, "EASY_RESOLUTION_ATTEMPTED"

    invoke-direct {v0, v1, v5}, LX/6ZT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZT;->EASY_RESOLUTION_ATTEMPTED:LX/6ZT;

    .line 1111147
    new-instance v0, LX/6ZT;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/6ZT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6ZT;->UNKNOWN:LX/6ZT;

    .line 1111148
    const/4 v0, 0x5

    new-array v0, v0, [LX/6ZT;

    sget-object v1, LX/6ZT;->LOCATION_SETTINGS_OK:LX/6ZT;

    aput-object v1, v0, v2

    sget-object v1, LX/6ZT;->EASY_RESOLUTION_UNAVAILABLE:LX/6ZT;

    aput-object v1, v0, v3

    sget-object v1, LX/6ZT;->EASY_RESOLUTION_POSSIBLE:LX/6ZT;

    aput-object v1, v0, v4

    sget-object v1, LX/6ZT;->EASY_RESOLUTION_ATTEMPTED:LX/6ZT;

    aput-object v1, v0, v5

    sget-object v1, LX/6ZT;->UNKNOWN:LX/6ZT;

    aput-object v1, v0, v6

    sput-object v0, LX/6ZT;->$VALUES:[LX/6ZT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1111142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromLocationSettingsResultStatus(I)LX/6ZT;
    .locals 1

    .prologue
    .line 1111135
    sparse-switch p0, :sswitch_data_0

    .line 1111136
    sget-object v0, LX/6ZT;->UNKNOWN:LX/6ZT;

    :goto_0
    return-object v0

    .line 1111137
    :sswitch_0
    sget-object v0, LX/6ZT;->LOCATION_SETTINGS_OK:LX/6ZT;

    goto :goto_0

    .line 1111138
    :sswitch_1
    sget-object v0, LX/6ZT;->EASY_RESOLUTION_UNAVAILABLE:LX/6ZT;

    goto :goto_0

    .line 1111139
    :sswitch_2
    sget-object v0, LX/6ZT;->EASY_RESOLUTION_POSSIBLE:LX/6ZT;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x6 -> :sswitch_2
        0x2136 -> :sswitch_1
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/6ZT;
    .locals 1

    .prologue
    .line 1111141
    const-class v0, LX/6ZT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ZT;

    return-object v0
.end method

.method public static values()[LX/6ZT;
    .locals 1

    .prologue
    .line 1111140
    sget-object v0, LX/6ZT;->$VALUES:[LX/6ZT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6ZT;

    return-object v0
.end method
