.class public final enum LX/5Ro;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Ro;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Ro;

.field public static final enum AUTOMATIC:LX/5Ro;

.field public static final enum COMPOST:LX/5Ro;

.field public static final enum NONE:LX/5Ro;

.field public static final enum NOTIFICATION:LX/5Ro;

.field public static final enum OFFLINE_POSTING_HEADER:LX/5Ro;

.field public static final enum REACTION:LX/5Ro;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 916680
    new-instance v0, LX/5Ro;

    const-string v1, "AUTOMATIC"

    invoke-direct {v0, v1, v3}, LX/5Ro;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Ro;->AUTOMATIC:LX/5Ro;

    .line 916681
    new-instance v0, LX/5Ro;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/5Ro;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Ro;->NONE:LX/5Ro;

    .line 916682
    new-instance v0, LX/5Ro;

    const-string v1, "NOTIFICATION"

    invoke-direct {v0, v1, v5}, LX/5Ro;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Ro;->NOTIFICATION:LX/5Ro;

    .line 916683
    new-instance v0, LX/5Ro;

    const-string v1, "OFFLINE_POSTING_HEADER"

    invoke-direct {v0, v1, v6}, LX/5Ro;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Ro;->OFFLINE_POSTING_HEADER:LX/5Ro;

    .line 916684
    new-instance v0, LX/5Ro;

    const-string v1, "COMPOST"

    invoke-direct {v0, v1, v7}, LX/5Ro;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Ro;->COMPOST:LX/5Ro;

    .line 916685
    new-instance v0, LX/5Ro;

    const-string v1, "REACTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5Ro;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Ro;->REACTION:LX/5Ro;

    .line 916686
    const/4 v0, 0x6

    new-array v0, v0, [LX/5Ro;

    sget-object v1, LX/5Ro;->AUTOMATIC:LX/5Ro;

    aput-object v1, v0, v3

    sget-object v1, LX/5Ro;->NONE:LX/5Ro;

    aput-object v1, v0, v4

    sget-object v1, LX/5Ro;->NOTIFICATION:LX/5Ro;

    aput-object v1, v0, v5

    sget-object v1, LX/5Ro;->OFFLINE_POSTING_HEADER:LX/5Ro;

    aput-object v1, v0, v6

    sget-object v1, LX/5Ro;->COMPOST:LX/5Ro;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5Ro;->REACTION:LX/5Ro;

    aput-object v2, v0, v1

    sput-object v0, LX/5Ro;->$VALUES:[LX/5Ro;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 916687
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Ro;
    .locals 1

    .prologue
    .line 916688
    const-class v0, LX/5Ro;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Ro;

    return-object v0
.end method

.method public static values()[LX/5Ro;
    .locals 1

    .prologue
    .line 916689
    sget-object v0, LX/5Ro;->$VALUES:[LX/5Ro;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Ro;

    return-object v0
.end method
