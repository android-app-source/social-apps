.class public final LX/5DF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 871289
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 871290
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 871291
    :goto_0
    return v1

    .line 871292
    :cond_0
    const-string v6, "reaction_count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 871293
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 871294
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 871295
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 871296
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 871297
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 871298
    const-string v6, "node"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 871299
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 871300
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_a

    .line 871301
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 871302
    :goto_2
    move v4, v5

    .line 871303
    goto :goto_1

    .line 871304
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 871305
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 871306
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 871307
    if-eqz v0, :cond_4

    .line 871308
    invoke-virtual {p1, v2, v3, v1}, LX/186;->a(III)V

    .line 871309
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1

    .line 871310
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 871311
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 871312
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 871313
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_6

    if-eqz v8, :cond_6

    .line 871314
    const-string v9, "key"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 871315
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v7, v4

    move v4, v6

    goto :goto_3

    .line 871316
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 871317
    :cond_8
    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 871318
    if-eqz v4, :cond_9

    .line 871319
    invoke-virtual {p1, v5, v7, v5}, LX/186;->a(III)V

    .line 871320
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_a
    move v4, v5

    move v7, v5

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 871321
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 871322
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 871323
    if-eqz v0, :cond_1

    .line 871324
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871325
    const/4 v1, 0x0

    .line 871326
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 871327
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 871328
    if-eqz v1, :cond_0

    .line 871329
    const-string p3, "key"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871330
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 871331
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 871332
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 871333
    if-eqz v0, :cond_2

    .line 871334
    const-string v1, "reaction_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 871335
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 871336
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 871337
    return-void
.end method
