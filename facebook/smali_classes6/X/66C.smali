.class public final LX/66C;
.super LX/66A;
.source ""


# instance fields
.field public final synthetic d:LX/66H;

.field private final e:LX/64q;

.field private f:J

.field private g:Z


# direct methods
.method public constructor <init>(LX/66H;LX/64q;)V
    .locals 2

    .prologue
    .line 1049584
    iput-object p1, p0, LX/66C;->d:LX/66H;

    invoke-direct {p0, p1}, LX/66A;-><init>(LX/66H;)V

    .line 1049585
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/66C;->f:J

    .line 1049586
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/66C;->g:Z

    .line 1049587
    iput-object p2, p0, LX/66C;->e:LX/64q;

    .line 1049588
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1049589
    iget-wide v0, p0, LX/66C;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1049590
    iget-object v0, p0, LX/66C;->d:LX/66H;

    iget-object v0, v0, LX/66H;->c:LX/671;

    invoke-interface {v0}, LX/671;->q()Ljava/lang/String;

    .line 1049591
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/66C;->d:LX/66H;

    iget-object v0, v0, LX/66H;->c:LX/671;

    invoke-interface {v0}, LX/671;->n()J

    move-result-wide v0

    iput-wide v0, p0, LX/66C;->f:J

    .line 1049592
    iget-object v0, p0, LX/66C;->d:LX/66H;

    iget-object v0, v0, LX/66H;->c:LX/671;

    invoke-interface {v0}, LX/671;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1049593
    iget-wide v2, p0, LX/66C;->f:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1049594
    :cond_1
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "expected chunk size and optional extensions but was \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, LX/66C;->f:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1049595
    :catch_0
    move-exception v0

    .line 1049596
    new-instance v1, Ljava/net/ProtocolException;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1049597
    :cond_2
    iget-wide v0, p0, LX/66C;->f:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    .line 1049598
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/66C;->g:Z

    .line 1049599
    iget-object v0, p0, LX/66C;->d:LX/66H;

    iget-object v0, v0, LX/66H;->a:LX/64w;

    .line 1049600
    iget-object v1, v0, LX/64w;->h:LX/64g;

    move-object v0, v1

    .line 1049601
    iget-object v1, p0, LX/66C;->e:LX/64q;

    iget-object v2, p0, LX/66C;->d:LX/66H;

    invoke-virtual {v2}, LX/66H;->e()LX/64n;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/66M;->a(LX/64g;LX/64q;LX/64n;)V

    .line 1049602
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/66A;->a(Z)V

    .line 1049603
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(LX/672;J)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v0, -0x1

    .line 1049604
    cmp-long v2, p2, v4

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049605
    :cond_0
    iget-boolean v2, p0, LX/66A;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049606
    :cond_1
    iget-boolean v2, p0, LX/66C;->g:Z

    if-nez v2, :cond_3

    .line 1049607
    :cond_2
    :goto_0
    return-wide v0

    .line 1049608
    :cond_3
    iget-wide v2, p0, LX/66C;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    iget-wide v2, p0, LX/66C;->f:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_5

    .line 1049609
    :cond_4
    invoke-direct {p0}, LX/66C;->b()V

    .line 1049610
    iget-boolean v2, p0, LX/66C;->g:Z

    if-eqz v2, :cond_2

    .line 1049611
    :cond_5
    iget-object v2, p0, LX/66C;->d:LX/66H;

    iget-object v2, v2, LX/66H;->c:LX/671;

    iget-wide v4, p0, LX/66C;->f:J

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, LX/65D;->a(LX/672;J)J

    move-result-wide v2

    .line 1049612
    cmp-long v0, v2, v0

    if-nez v0, :cond_6

    .line 1049613
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/66A;->a(Z)V

    .line 1049614
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049615
    :cond_6
    iget-wide v0, p0, LX/66C;->f:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/66C;->f:J

    move-wide v0, v2

    .line 1049616
    goto :goto_0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 1049617
    iget-boolean v0, p0, LX/66A;->b:Z

    if-eqz v0, :cond_0

    .line 1049618
    :goto_0
    return-void

    .line 1049619
    :cond_0
    iget-boolean v0, p0, LX/66C;->g:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p0, v0, v1}, LX/65A;->a(LX/65D;ILjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1049620
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/66A;->a(Z)V

    .line 1049621
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/66C;->b:Z

    goto :goto_0
.end method
