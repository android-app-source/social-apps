.class public final LX/6Yo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Yb;


# direct methods
.method public constructor <init>(LX/6Yb;)V
    .locals 0

    .prologue
    .line 1110641
    iput-object p1, p0, LX/6Yo;->a:LX/6Yb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1110619
    iget-object v0, p0, LX/6Yo;->a:LX/6Yb;

    invoke-static {v0}, LX/6Yb;->c(LX/6Yb;)V

    .line 1110620
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1110621
    check-cast p1, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    .line 1110622
    if-eqz p1, :cond_0

    .line 1110623
    iget-object v0, p1, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->e:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult$Page;

    move-object v0, v0

    .line 1110624
    if-nez v0, :cond_1

    .line 1110625
    :cond_0
    iget-object v0, p0, LX/6Yo;->a:LX/6Yb;

    invoke-static {v0}, LX/6Yb;->c(LX/6Yb;)V

    .line 1110626
    :goto_0
    return-void

    .line 1110627
    :cond_1
    iget-object v0, p0, LX/6Yo;->a:LX/6Yb;

    iget-object v0, v0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110628
    iput-object p1, v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->v:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    .line 1110629
    iget-object v0, p0, LX/6Yo;->a:LX/6Yb;

    iget-object v0, v0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    new-instance v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    invoke-direct {v1, p1}, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;-><init>(Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;)V

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V

    .line 1110630
    sget-object v0, LX/6Yp;->a:[I

    .line 1110631
    iget-object v1, p1, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->a:Ljava/lang/String;

    invoke-static {v1}, LX/6YD;->fromStatus(Ljava/lang/String;)LX/6YD;

    move-result-object v1

    move-object v1, v1

    .line 1110632
    invoke-virtual {v1}, LX/6YD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1110633
    iget-object v0, p0, LX/6Yo;->a:LX/6Yb;

    iget-object v0, v0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110634
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->v:Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;

    move-object v0, v1

    .line 1110635
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/server/ZeroPromoResult;->c:Lcom/facebook/iorg/common/upsell/server/UpsellPromo;

    move-object v0, v1

    .line 1110636
    if-nez v0, :cond_2

    .line 1110637
    iget-object v0, p0, LX/6Yo;->a:LX/6Yb;

    iget-object v0, v0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/6YN;->BUY_FAILURE:LX/6YN;

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    goto :goto_0

    .line 1110638
    :pswitch_0
    iget-object v0, p0, LX/6Yo;->a:LX/6Yb;

    iget-object v0, v0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/6YN;->BUY_SUCCESS:LX/6YN;

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    goto :goto_0

    .line 1110639
    :pswitch_1
    iget-object v0, p0, LX/6Yo;->a:LX/6Yb;

    iget-object v0, v0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/6YN;->BUY_MAYBE:LX/6YN;

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    goto :goto_0

    .line 1110640
    :cond_2
    iget-object v0, p0, LX/6Yo;->a:LX/6Yb;

    iget-object v0, v0, LX/6Yb;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    sget-object v1, LX/6YN;->SHOW_LOAN:LX/6YN;

    invoke-virtual {v0, v1}, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->a(LX/6YN;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
