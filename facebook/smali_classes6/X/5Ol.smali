.class public final enum LX/5Ol;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Ol;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Ol;

.field public static final enum COLD:LX/5Ol;

.field public static final enum WARM:LX/5Ol;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 909411
    new-instance v0, LX/5Ol;

    const-string v1, "WARM"

    invoke-direct {v0, v1, v2}, LX/5Ol;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Ol;->WARM:LX/5Ol;

    .line 909412
    new-instance v0, LX/5Ol;

    const-string v1, "COLD"

    invoke-direct {v0, v1, v3}, LX/5Ol;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Ol;->COLD:LX/5Ol;

    .line 909413
    const/4 v0, 0x2

    new-array v0, v0, [LX/5Ol;

    sget-object v1, LX/5Ol;->WARM:LX/5Ol;

    aput-object v1, v0, v2

    sget-object v1, LX/5Ol;->COLD:LX/5Ol;

    aput-object v1, v0, v3

    sput-object v0, LX/5Ol;->$VALUES:[LX/5Ol;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 909414
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Ol;
    .locals 1

    .prologue
    .line 909415
    const-class v0, LX/5Ol;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Ol;

    return-object v0
.end method

.method public static values()[LX/5Ol;
    .locals 1

    .prologue
    .line 909416
    sget-object v0, LX/5Ol;->$VALUES:[LX/5Ol;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Ol;

    return-object v0
.end method
