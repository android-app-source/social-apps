.class public LX/62h;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1041932
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/62h;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1041933
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1041934
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1041935
    const v0, 0x7f03035d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1041936
    const v0, 0x7f0a00f7

    invoke-virtual {p0, v0}, LX/62h;->setBackgroundResource(I)V

    .line 1041937
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/62h;->setOrientation(I)V

    .line 1041938
    return-void
.end method


# virtual methods
.method public setRetryClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1041939
    const v0, 0x7f0d0b17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1041940
    return-void
.end method
