.class public final LX/64R;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/64q;

.field public final b:LX/64j;

.field public final c:Ljavax/net/SocketFactory;

.field public final d:LX/64S;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64x;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/64e;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/net/ProxySelector;

.field public final h:Ljava/net/Proxy;

.field public final i:Ljavax/net/ssl/SSLSocketFactory;

.field public final j:Ljavax/net/ssl/HostnameVerifier;

.field public final k:LX/64a;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILX/64j;Ljavax/net/SocketFactory;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;LX/64a;LX/64S;Ljava/net/Proxy;Ljava/util/List;Ljava/util/List;Ljava/net/ProxySelector;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LX/64j;",
            "Ljavax/net/SocketFactory;",
            "Ljavax/net/ssl/SSLSocketFactory;",
            "Ljavax/net/ssl/HostnameVerifier;",
            "LX/64a;",
            "LX/64S;",
            "Ljava/net/Proxy;",
            "Ljava/util/List",
            "<",
            "LX/64x;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/64e;",
            ">;",
            "Ljava/net/ProxySelector;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1044569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044570
    new-instance v1, LX/64p;

    invoke-direct {v1}, LX/64p;-><init>()V

    if-eqz p5, :cond_0

    const-string v0, "https"

    .line 1044571
    :goto_0
    invoke-virtual {v1, v0}, LX/64p;->a(Ljava/lang/String;)LX/64p;

    move-result-object v0

    .line 1044572
    invoke-virtual {v0, p1}, LX/64p;->b(Ljava/lang/String;)LX/64p;

    move-result-object v0

    .line 1044573
    invoke-virtual {v0, p2}, LX/64p;->a(I)LX/64p;

    move-result-object v0

    .line 1044574
    invoke-virtual {v0}, LX/64p;->c()LX/64q;

    move-result-object v0

    iput-object v0, p0, LX/64R;->a:LX/64q;

    .line 1044575
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "dns == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1044576
    :cond_0
    const-string v0, "http"

    goto :goto_0

    .line 1044577
    :cond_1
    iput-object p3, p0, LX/64R;->b:LX/64j;

    .line 1044578
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "socketFactory == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1044579
    :cond_2
    iput-object p4, p0, LX/64R;->c:Ljavax/net/SocketFactory;

    .line 1044580
    if-nez p8, :cond_3

    .line 1044581
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "proxyAuthenticator == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1044582
    :cond_3
    iput-object p8, p0, LX/64R;->d:LX/64S;

    .line 1044583
    if-nez p10, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "protocols == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1044584
    :cond_4
    invoke-static {p10}, LX/65A;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/64R;->e:Ljava/util/List;

    .line 1044585
    if-nez p11, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "connectionSpecs == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1044586
    :cond_5
    invoke-static {p11}, LX/65A;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/64R;->f:Ljava/util/List;

    .line 1044587
    if-nez p12, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "proxySelector == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1044588
    :cond_6
    iput-object p12, p0, LX/64R;->g:Ljava/net/ProxySelector;

    .line 1044589
    iput-object p9, p0, LX/64R;->h:Ljava/net/Proxy;

    .line 1044590
    iput-object p5, p0, LX/64R;->i:Ljavax/net/ssl/SSLSocketFactory;

    .line 1044591
    iput-object p6, p0, LX/64R;->j:Ljavax/net/ssl/HostnameVerifier;

    .line 1044592
    iput-object p7, p0, LX/64R;->k:LX/64a;

    .line 1044593
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1044594
    instance-of v1, p1, LX/64R;

    if-eqz v1, :cond_0

    .line 1044595
    check-cast p1, LX/64R;

    .line 1044596
    iget-object v1, p0, LX/64R;->a:LX/64q;

    iget-object v2, p1, LX/64R;->a:LX/64q;

    invoke-virtual {v1, v2}, LX/64q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64R;->b:LX/64j;

    iget-object v2, p1, LX/64R;->b:LX/64j;

    .line 1044597
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64R;->d:LX/64S;

    iget-object v2, p1, LX/64R;->d:LX/64S;

    .line 1044598
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64R;->e:Ljava/util/List;

    iget-object v2, p1, LX/64R;->e:Ljava/util/List;

    .line 1044599
    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64R;->f:Ljava/util/List;

    iget-object v2, p1, LX/64R;->f:Ljava/util/List;

    .line 1044600
    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64R;->g:Ljava/net/ProxySelector;

    iget-object v2, p1, LX/64R;->g:Ljava/net/ProxySelector;

    .line 1044601
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64R;->h:Ljava/net/Proxy;

    iget-object v2, p1, LX/64R;->h:Ljava/net/Proxy;

    .line 1044602
    invoke-static {v1, v2}, LX/65A;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64R;->i:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v2, p1, LX/64R;->i:Ljavax/net/ssl/SSLSocketFactory;

    .line 1044603
    invoke-static {v1, v2}, LX/65A;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64R;->j:Ljavax/net/ssl/HostnameVerifier;

    iget-object v2, p1, LX/64R;->j:Ljavax/net/ssl/HostnameVerifier;

    .line 1044604
    invoke-static {v1, v2}, LX/65A;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/64R;->k:LX/64a;

    iget-object v2, p1, LX/64R;->k:LX/64a;

    .line 1044605
    invoke-static {v1, v2}, LX/65A;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 1044606
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1044607
    iget-object v0, p0, LX/64R;->a:LX/64q;

    invoke-virtual {v0}, LX/64q;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1044608
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/64R;->b:LX/64j;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1044609
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/64R;->d:LX/64S;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1044610
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/64R;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1044611
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/64R;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1044612
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/64R;->g:Ljava/net/ProxySelector;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1044613
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/64R;->h:Ljava/net/Proxy;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/64R;->h:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 1044614
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/64R;->i:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/64R;->i:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 1044615
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/64R;->j:Ljavax/net/ssl/HostnameVerifier;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/64R;->j:Ljavax/net/ssl/HostnameVerifier;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 1044616
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/64R;->k:LX/64a;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/64R;->k:LX/64a;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 1044617
    return v0

    :cond_1
    move v0, v1

    .line 1044618
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1044619
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1044620
    goto :goto_2
.end method
