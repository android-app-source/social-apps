.class public final LX/57O;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 841345
    const/4 v9, 0x0

    .line 841346
    const-wide/16 v10, 0x0

    .line 841347
    const/4 v8, 0x0

    .line 841348
    const/4 v7, 0x0

    .line 841349
    const/4 v6, 0x0

    .line 841350
    const/4 v5, 0x0

    .line 841351
    const/4 v4, 0x0

    .line 841352
    const/4 v3, 0x0

    .line 841353
    const/4 v2, 0x0

    .line 841354
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_b

    .line 841355
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 841356
    const/4 v2, 0x0

    .line 841357
    :goto_0
    return v2

    .line 841358
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v7, :cond_9

    .line 841359
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 841360
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 841361
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v14, :cond_0

    if-eqz v3, :cond_0

    .line 841362
    const-string v7, "application"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 841363
    invoke-static/range {p0 .. p1}, LX/57K;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 841364
    :cond_1
    const-string v7, "default_expiration_time"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 841365
    const/4 v2, 0x1

    .line 841366
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 841367
    :cond_2
    const-string v7, "description"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 841368
    invoke-static/range {p0 .. p1}, LX/4av;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 841369
    :cond_3
    const-string v7, "mask"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 841370
    invoke-static/range {p0 .. p1}, LX/57L;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 841371
    :cond_4
    const-string v7, "not_installed_description"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 841372
    invoke-static/range {p0 .. p1}, LX/4av;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 841373
    :cond_5
    const-string v7, "not_installed_title"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 841374
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 841375
    :cond_6
    const-string v7, "profile"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 841376
    invoke-static/range {p0 .. p1}, LX/57N;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto :goto_1

    .line 841377
    :cond_7
    const-string v7, "title"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 841378
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 841379
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 841380
    :cond_9
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 841381
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 841382
    if-eqz v2, :cond_a

    .line 841383
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 841384
    :cond_a
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 841385
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 841386
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 841387
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 841388
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 841389
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 841390
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v12, v7

    move v13, v8

    move v8, v3

    move v15, v6

    move v6, v9

    move v9, v4

    move/from16 v16, v5

    move-wide v4, v10

    move/from16 v10, v16

    move v11, v15

    goto/16 :goto_1
.end method
