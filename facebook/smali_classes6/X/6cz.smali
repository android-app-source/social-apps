.class public LX/6cz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/6cz;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/ContentResolver;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6dB;",
            ">;",
            "Landroid/content/ContentResolver;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1115051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1115052
    iput-object p1, p0, LX/6cz;->a:LX/0Or;

    .line 1115053
    iput-object p2, p0, LX/6cz;->b:Landroid/content/ContentResolver;

    .line 1115054
    return-void
.end method

.method public static a(LX/0QB;)LX/6cz;
    .locals 5

    .prologue
    .line 1115055
    sget-object v0, LX/6cz;->c:LX/6cz;

    if-nez v0, :cond_1

    .line 1115056
    const-class v1, LX/6cz;

    monitor-enter v1

    .line 1115057
    :try_start_0
    sget-object v0, LX/6cz;->c:LX/6cz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1115058
    if-eqz v2, :cond_0

    .line 1115059
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1115060
    new-instance v4, LX/6cz;

    const/16 v3, 0x2747

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-direct {v4, p0, v3}, LX/6cz;-><init>(LX/0Or;Landroid/content/ContentResolver;)V

    .line 1115061
    move-object v0, v4

    .line 1115062
    sput-object v0, LX/6cz;->c:LX/6cz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1115063
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1115064
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1115065
    :cond_1
    sget-object v0, LX/6cz;->c:LX/6cz;

    return-object v0

    .line 1115066
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1115067
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1115068
    iget-object v1, p0, LX/6cz;->b:Landroid/content/ContentResolver;

    iget-object v0, p0, LX/6cz;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dB;

    iget-object v0, v0, LX/6dB;->e:LX/6d8;

    iget-object v0, v0, LX/6d8;->a:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1115069
    if-gez v0, :cond_0

    .line 1115070
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to delete threads database"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1115071
    :cond_0
    return-void
.end method


# virtual methods
.method public final clearUserData()V
    .locals 0

    .prologue
    .line 1115072
    invoke-direct {p0}, LX/6cz;->a()V

    .line 1115073
    return-void
.end method
