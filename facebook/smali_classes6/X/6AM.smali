.class public LX/6AM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1059004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;LX/0Pz;LX/0Pz;LX/2cE;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;",
            "LX/0Pz",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;",
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2cE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1059005
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v4

    .line 1059006
    const/4 v3, 0x0

    .line 1059007
    iget-object v0, p3, LX/2cE;->f:Lcom/facebook/omnistore/Collection;

    move-object v0, v0

    .line 1059008
    if-nez v0, :cond_1

    .line 1059009
    :cond_0
    :goto_0
    return-void

    .line 1059010
    :cond_1
    sget-object v1, LX/0ql;->b:Ljava/lang/String;

    const/4 v2, -0x1

    sget-object v5, Lcom/facebook/omnistore/Collection$SortDirection;->DESCENDING:Lcom/facebook/omnistore/Collection$SortDirection;

    invoke-virtual {v0, v1, v2, v5}, Lcom/facebook/omnistore/Collection;->query(Ljava/lang/String;ILcom/facebook/omnistore/Collection$SortDirection;)Lcom/facebook/omnistore/Cursor;

    move-result-object v5

    .line 1059011
    const/4 v1, 0x0

    .line 1059012
    :try_start_0
    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->step()Z

    move-result v2

    .line 1059013
    :goto_1
    if-ge v3, v4, :cond_8

    if-eqz v2, :cond_8

    .line 1059014
    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->d()Ljava/lang/String;

    move-result-object v0

    .line 1059015
    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->getSortKey()Ljava/lang/String;

    move-result-object v6

    .line 1059016
    invoke-virtual {v0, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 1059017
    if-nez v0, :cond_2

    .line 1059018
    add-int/lit8 v3, v3, 0x1

    .line 1059019
    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->step()Z

    move-result v2

    goto :goto_1

    .line 1059020
    :cond_2
    if-lez v0, :cond_3

    .line 1059021
    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1059022
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1059023
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->getPrimaryKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1059024
    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->step()Z

    move-result v2

    goto :goto_1

    .line 1059025
    :goto_2
    if-ge v0, v4, :cond_7

    .line 1059026
    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1059027
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1059028
    :goto_3
    if-eqz v0, :cond_4

    .line 1059029
    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->getPrimaryKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1059030
    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->step()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    goto :goto_3

    .line 1059031
    :cond_4
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->close()V

    goto :goto_0

    .line 1059032
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1059033
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_4
    if-eqz v5, :cond_5

    if-eqz v1, :cond_6

    :try_start_2
    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_5
    :goto_5
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_5

    :cond_6
    invoke-virtual {v5}, Lcom/facebook/omnistore/Cursor;->close()V

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v0, v3

    goto :goto_2
.end method
