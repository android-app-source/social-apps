.class public final enum LX/6OT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6OT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6OT;

.field public static final enum EMAIL:LX/6OT;

.field public static final enum EMAIL_PUBLIC_HASH:LX/6OT;

.field public static final enum NAME:LX/6OT;

.field public static final enum PHONE:LX/6OT;

.field public static final enum PHONE_PUBLIC_HASH:LX/6OT;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1085164
    new-instance v0, LX/6OT;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v2}, LX/6OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OT;->NAME:LX/6OT;

    .line 1085165
    new-instance v0, LX/6OT;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v3}, LX/6OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OT;->EMAIL:LX/6OT;

    .line 1085166
    new-instance v0, LX/6OT;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v4}, LX/6OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OT;->PHONE:LX/6OT;

    .line 1085167
    new-instance v0, LX/6OT;

    const-string v1, "EMAIL_PUBLIC_HASH"

    invoke-direct {v0, v1, v5}, LX/6OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OT;->EMAIL_PUBLIC_HASH:LX/6OT;

    .line 1085168
    new-instance v0, LX/6OT;

    const-string v1, "PHONE_PUBLIC_HASH"

    invoke-direct {v0, v1, v6}, LX/6OT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6OT;->PHONE_PUBLIC_HASH:LX/6OT;

    .line 1085169
    const/4 v0, 0x5

    new-array v0, v0, [LX/6OT;

    sget-object v1, LX/6OT;->NAME:LX/6OT;

    aput-object v1, v0, v2

    sget-object v1, LX/6OT;->EMAIL:LX/6OT;

    aput-object v1, v0, v3

    sget-object v1, LX/6OT;->PHONE:LX/6OT;

    aput-object v1, v0, v4

    sget-object v1, LX/6OT;->EMAIL_PUBLIC_HASH:LX/6OT;

    aput-object v1, v0, v5

    sget-object v1, LX/6OT;->PHONE_PUBLIC_HASH:LX/6OT;

    aput-object v1, v0, v6

    sput-object v0, LX/6OT;->$VALUES:[LX/6OT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1085170
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6OT;
    .locals 1

    .prologue
    .line 1085171
    const-class v0, LX/6OT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6OT;

    return-object v0
.end method

.method public static values()[LX/6OT;
    .locals 1

    .prologue
    .line 1085172
    sget-object v0, LX/6OT;->$VALUES:[LX/6OT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6OT;

    return-object v0
.end method
