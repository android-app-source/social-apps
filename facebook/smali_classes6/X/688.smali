.class public final LX/688;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/31X;


# instance fields
.field public final synthetic a:LX/3BT;

.field private final b:Landroid/app/AlertDialog$Builder;


# direct methods
.method public constructor <init>(LX/3BT;)V
    .locals 2

    .prologue
    .line 1054228
    iput-object p1, p0, LX/688;->a:LX/3BT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1054229
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, LX/688;->a:LX/3BT;

    iget-object v1, v1, LX/3BT;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/688;->b:Landroid/app/AlertDialog$Builder;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)LX/31X;
    .locals 1

    .prologue
    .line 1054226
    iget-object v0, p0, LX/688;->b:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1054227
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/31X;
    .locals 1

    .prologue
    .line 1054224
    iget-object v0, p0, LX/688;->b:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1054225
    return-object p0
.end method

.method public final a()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 1054223
    iget-object v0, p0, LX/688;->b:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)LX/31X;
    .locals 1

    .prologue
    .line 1054219
    iget-object v0, p0, LX/688;->b:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1054220
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/31X;
    .locals 1

    .prologue
    .line 1054221
    iget-object v0, p0, LX/688;->b:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1, p2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1054222
    return-object p0
.end method
