.class public final LX/6eQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/6eR;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6eS;

.field private final b:Lcom/facebook/ui/media/attachments/MediaResource;


# direct methods
.method public constructor <init>(LX/6eS;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 0

    .prologue
    .line 1117890
    iput-object p1, p0, LX/6eQ;->a:LX/6eS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117891
    iput-object p2, p0, LX/6eQ;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1117892
    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1117893
    iget-object v0, p0, LX/6eQ;->a:LX/6eS;

    iget-object v1, p0, LX/6eQ;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1117894
    invoke-virtual {v0, v1}, LX/6eS;->c(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6eR;

    move-result-object v2

    .line 1117895
    sget-object p0, LX/6eR;->VALID:LX/6eR;

    if-eq v2, p0, :cond_0

    :goto_0
    move-object v0, v2

    .line 1117896
    return-object v0

    .line 1117897
    :cond_0
    iget-object v2, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object p0, LX/2MK;->PHOTO:LX/2MK;

    if-eq v2, p0, :cond_1

    .line 1117898
    sget-object v2, LX/6eR;->VALID:LX/6eR;

    .line 1117899
    :goto_1
    move-object v2, v2

    .line 1117900
    goto :goto_0

    .line 1117901
    :cond_1
    iget v2, v1, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    if-lez v2, :cond_2

    iget v2, v1, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    if-lez v2, :cond_2

    .line 1117902
    sget-object v2, LX/6eR;->VALID:LX/6eR;

    goto :goto_1

    .line 1117903
    :cond_2
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1117904
    const/4 p0, 0x1

    iput-boolean p0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1117905
    iget-object p0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1117906
    iget p0, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez p0, :cond_3

    iget v2, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gtz v2, :cond_4

    :cond_3
    sget-object v2, LX/6eR;->CORRUPTED:LX/6eR;

    goto :goto_1

    :cond_4
    sget-object v2, LX/6eR;->VALID:LX/6eR;

    goto :goto_1
.end method
