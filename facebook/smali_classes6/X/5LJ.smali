.class public final LX/5LJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 900288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;
    .locals 17

    .prologue
    .line 900289
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 900290
    move-object/from16 v0, p0

    iget-object v2, v0, LX/5LJ;->a:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityModel$AllIconsModel;

    invoke-static {v1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 900291
    move-object/from16 v0, p0

    iget-object v3, v0, LX/5LJ;->b:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$GlyphModel;

    invoke-static {v1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 900292
    move-object/from16 v0, p0

    iget-object v4, v0, LX/5LJ;->c:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableActivityFieldsModel$IconImageLargeModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 900293
    move-object/from16 v0, p0

    iget-object v5, v0, LX/5LJ;->d:Ljava/lang/String;

    invoke-virtual {v1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 900294
    move-object/from16 v0, p0

    iget-object v6, v0, LX/5LJ;->f:Ljava/lang/String;

    invoke-virtual {v1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 900295
    move-object/from16 v0, p0

    iget-object v7, v0, LX/5LJ;->h:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 900296
    move-object/from16 v0, p0

    iget-object v8, v0, LX/5LJ;->i:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 900297
    move-object/from16 v0, p0

    iget-object v9, v0, LX/5LJ;->j:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 900298
    move-object/from16 v0, p0

    iget-object v10, v0, LX/5LJ;->k:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 900299
    move-object/from16 v0, p0

    iget-object v11, v0, LX/5LJ;->l:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 900300
    move-object/from16 v0, p0

    iget-object v12, v0, LX/5LJ;->m:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 900301
    move-object/from16 v0, p0

    iget-object v13, v0, LX/5LJ;->n:Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    invoke-static {v1, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 900302
    move-object/from16 v0, p0

    iget-object v14, v0, LX/5LJ;->o:Ljava/lang/String;

    invoke-virtual {v1, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 900303
    move-object/from16 v0, p0

    iget-object v15, v0, LX/5LJ;->s:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$MinutiaeTaggableObjectsModel;

    invoke-static {v1, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 900304
    const/16 v16, 0x13

    move/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 900305
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 900306
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 900307
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 900308
    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 900309
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/5LJ;->e:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 900310
    const/4 v2, 0x5

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 900311
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget v3, v0, LX/5LJ;->g:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/186;->a(III)V

    .line 900312
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 900313
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 900314
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 900315
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 900316
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 900317
    const/16 v2, 0xc

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 900318
    const/16 v2, 0xd

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 900319
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 900320
    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/5LJ;->p:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 900321
    const/16 v2, 0x10

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/5LJ;->q:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 900322
    const/16 v2, 0x11

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/5LJ;->r:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 900323
    const/16 v2, 0x12

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 900324
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 900325
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 900326
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 900327
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 900328
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 900329
    new-instance v2, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;

    invoke-direct {v2, v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$FetchTaggableSuggestionsAtPlaceQueryModel;-><init>(LX/15i;)V

    .line 900330
    return-object v2
.end method
