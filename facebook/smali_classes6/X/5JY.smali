.class public final LX/5JY;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5JZ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Landroid/text/TextUtils$TruncateAt;

.field public d:I

.field public e:I

.field public f:I

.field public g:F

.field public h:F

.field public i:F

.field public j:I

.field public k:Z

.field public l:I

.field public m:Landroid/content/res/ColorStateList;

.field public n:I

.field public o:Landroid/content/res/ColorStateList;

.field public p:I

.field public q:I

.field public r:I

.field public s:F

.field public t:F

.field public u:I

.field public v:Landroid/graphics/Typeface;

.field public w:Landroid/text/Layout$Alignment;

.field public x:I

.field public y:Z

.field public z:LX/1dQ;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x7fffffff

    const/high16 v1, -0x1000000

    .line 897083
    invoke-static {}, LX/5JZ;->q()LX/5JZ;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 897084
    const/high16 v0, -0x80000000

    iput v0, p0, LX/5JY;->d:I

    .line 897085
    iput v2, p0, LX/5JY;->e:I

    .line 897086
    iput v2, p0, LX/5JY;->f:I

    .line 897087
    const v0, -0x777778

    iput v0, p0, LX/5JY;->j:I

    .line 897088
    iput v1, p0, LX/5JY;->l:I

    .line 897089
    const v0, -0x333334

    iput v0, p0, LX/5JY;->n:I

    .line 897090
    iput v1, p0, LX/5JY;->p:I

    .line 897091
    const/16 v0, 0xd

    iput v0, p0, LX/5JY;->r:I

    .line 897092
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/5JY;->t:F

    .line 897093
    sget v0, LX/5Jd;->a:I

    iput v0, p0, LX/5JY;->u:I

    .line 897094
    sget-object v0, LX/5Jd;->b:Landroid/graphics/Typeface;

    iput-object v0, p0, LX/5JY;->v:Landroid/graphics/Typeface;

    .line 897095
    sget-object v0, LX/5Jd;->c:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LX/5JY;->w:Landroid/text/Layout$Alignment;

    .line 897096
    const v0, 0x800013

    iput v0, p0, LX/5JY;->x:I

    .line 897097
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5JY;->y:Z

    .line 897098
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 897164
    const-string v0, "EditText"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 897099
    if-ne p0, p1, :cond_1

    .line 897100
    :cond_0
    :goto_0
    return v0

    .line 897101
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 897102
    goto :goto_0

    .line 897103
    :cond_3
    check-cast p1, LX/5JY;

    .line 897104
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 897105
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 897106
    if-eq v2, v3, :cond_0

    .line 897107
    iget-object v2, p0, LX/5JY;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/5JY;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/5JY;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 897108
    goto :goto_0

    .line 897109
    :cond_5
    iget-object v2, p1, LX/5JY;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 897110
    :cond_6
    iget-object v2, p0, LX/5JY;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/5JY;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/5JY;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 897111
    goto :goto_0

    .line 897112
    :cond_8
    iget-object v2, p1, LX/5JY;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 897113
    :cond_9
    iget-object v2, p0, LX/5JY;->c:Landroid/text/TextUtils$TruncateAt;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/5JY;->c:Landroid/text/TextUtils$TruncateAt;

    iget-object v3, p1, LX/5JY;->c:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/text/TextUtils$TruncateAt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 897114
    goto :goto_0

    .line 897115
    :cond_b
    iget-object v2, p1, LX/5JY;->c:Landroid/text/TextUtils$TruncateAt;

    if-nez v2, :cond_a

    .line 897116
    :cond_c
    iget v2, p0, LX/5JY;->d:I

    iget v3, p1, LX/5JY;->d:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 897117
    goto :goto_0

    .line 897118
    :cond_d
    iget v2, p0, LX/5JY;->e:I

    iget v3, p1, LX/5JY;->e:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 897119
    goto :goto_0

    .line 897120
    :cond_e
    iget v2, p0, LX/5JY;->f:I

    iget v3, p1, LX/5JY;->f:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 897121
    goto :goto_0

    .line 897122
    :cond_f
    iget v2, p0, LX/5JY;->g:F

    iget v3, p1, LX/5JY;->g:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_10

    move v0, v1

    .line 897123
    goto :goto_0

    .line 897124
    :cond_10
    iget v2, p0, LX/5JY;->h:F

    iget v3, p1, LX/5JY;->h:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_11

    move v0, v1

    .line 897125
    goto/16 :goto_0

    .line 897126
    :cond_11
    iget v2, p0, LX/5JY;->i:F

    iget v3, p1, LX/5JY;->i:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_12

    move v0, v1

    .line 897127
    goto/16 :goto_0

    .line 897128
    :cond_12
    iget v2, p0, LX/5JY;->j:I

    iget v3, p1, LX/5JY;->j:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 897129
    goto/16 :goto_0

    .line 897130
    :cond_13
    iget-boolean v2, p0, LX/5JY;->k:Z

    iget-boolean v3, p1, LX/5JY;->k:Z

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 897131
    goto/16 :goto_0

    .line 897132
    :cond_14
    iget v2, p0, LX/5JY;->l:I

    iget v3, p1, LX/5JY;->l:I

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 897133
    goto/16 :goto_0

    .line 897134
    :cond_15
    iget-object v2, p0, LX/5JY;->m:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/5JY;->m:Landroid/content/res/ColorStateList;

    iget-object v3, p1, LX/5JY;->m:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 897135
    goto/16 :goto_0

    .line 897136
    :cond_17
    iget-object v2, p1, LX/5JY;->m:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_16

    .line 897137
    :cond_18
    iget v2, p0, LX/5JY;->n:I

    iget v3, p1, LX/5JY;->n:I

    if-eq v2, v3, :cond_19

    move v0, v1

    .line 897138
    goto/16 :goto_0

    .line 897139
    :cond_19
    iget-object v2, p0, LX/5JY;->o:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_1b

    iget-object v2, p0, LX/5JY;->o:Landroid/content/res/ColorStateList;

    iget-object v3, p1, LX/5JY;->o:Landroid/content/res/ColorStateList;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    .line 897140
    goto/16 :goto_0

    .line 897141
    :cond_1b
    iget-object v2, p1, LX/5JY;->o:Landroid/content/res/ColorStateList;

    if-nez v2, :cond_1a

    .line 897142
    :cond_1c
    iget v2, p0, LX/5JY;->p:I

    iget v3, p1, LX/5JY;->p:I

    if-eq v2, v3, :cond_1d

    move v0, v1

    .line 897143
    goto/16 :goto_0

    .line 897144
    :cond_1d
    iget v2, p0, LX/5JY;->q:I

    iget v3, p1, LX/5JY;->q:I

    if-eq v2, v3, :cond_1e

    move v0, v1

    .line 897145
    goto/16 :goto_0

    .line 897146
    :cond_1e
    iget v2, p0, LX/5JY;->r:I

    iget v3, p1, LX/5JY;->r:I

    if-eq v2, v3, :cond_1f

    move v0, v1

    .line 897147
    goto/16 :goto_0

    .line 897148
    :cond_1f
    iget v2, p0, LX/5JY;->s:F

    iget v3, p1, LX/5JY;->s:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_20

    move v0, v1

    .line 897149
    goto/16 :goto_0

    .line 897150
    :cond_20
    iget v2, p0, LX/5JY;->t:F

    iget v3, p1, LX/5JY;->t:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_21

    move v0, v1

    .line 897151
    goto/16 :goto_0

    .line 897152
    :cond_21
    iget v2, p0, LX/5JY;->u:I

    iget v3, p1, LX/5JY;->u:I

    if-eq v2, v3, :cond_22

    move v0, v1

    .line 897153
    goto/16 :goto_0

    .line 897154
    :cond_22
    iget-object v2, p0, LX/5JY;->v:Landroid/graphics/Typeface;

    if-eqz v2, :cond_24

    iget-object v2, p0, LX/5JY;->v:Landroid/graphics/Typeface;

    iget-object v3, p1, LX/5JY;->v:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Typeface;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_25

    :cond_23
    move v0, v1

    .line 897155
    goto/16 :goto_0

    .line 897156
    :cond_24
    iget-object v2, p1, LX/5JY;->v:Landroid/graphics/Typeface;

    if-nez v2, :cond_23

    .line 897157
    :cond_25
    iget-object v2, p0, LX/5JY;->w:Landroid/text/Layout$Alignment;

    if-eqz v2, :cond_27

    iget-object v2, p0, LX/5JY;->w:Landroid/text/Layout$Alignment;

    iget-object v3, p1, LX/5JY;->w:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, Landroid/text/Layout$Alignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    :cond_26
    move v0, v1

    .line 897158
    goto/16 :goto_0

    .line 897159
    :cond_27
    iget-object v2, p1, LX/5JY;->w:Landroid/text/Layout$Alignment;

    if-nez v2, :cond_26

    .line 897160
    :cond_28
    iget v2, p0, LX/5JY;->x:I

    iget v3, p1, LX/5JY;->x:I

    if-eq v2, v3, :cond_29

    move v0, v1

    .line 897161
    goto/16 :goto_0

    .line 897162
    :cond_29
    iget-boolean v2, p0, LX/5JY;->y:Z

    iget-boolean v3, p1, LX/5JY;->y:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 897163
    goto/16 :goto_0
.end method
