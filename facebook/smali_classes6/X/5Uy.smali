.class public final LX/5Uy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 927593
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_c

    .line 927594
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 927595
    :goto_0
    return v1

    .line 927596
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 927597
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 927598
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 927599
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 927600
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 927601
    const-string v12, "bubble_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 927602
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 927603
    :cond_2
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 927604
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 927605
    :cond_3
    const-string v12, "merchant_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 927606
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 927607
    :cond_4
    const-string v12, "order_payment_method"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 927608
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 927609
    :cond_5
    const-string v12, "partner_logo"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 927610
    invoke-static {p0, p1}, LX/5VF;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 927611
    :cond_6
    const-string v12, "receipt_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 927612
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 927613
    :cond_7
    const-string v12, "receipt_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 927614
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 927615
    :cond_8
    const-string v12, "status"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 927616
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 927617
    :cond_9
    const-string v12, "structured_address"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 927618
    invoke-static {p0, p1}, LX/5V0;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 927619
    :cond_a
    const-string v12, "total"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 927620
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 927621
    :cond_b
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 927622
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 927623
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 927624
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 927625
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 927626
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 927627
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 927628
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 927629
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 927630
    const/16 v1, 0x8

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 927631
    const/16 v1, 0x9

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 927632
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 927633
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 927634
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 927635
    if-eqz v0, :cond_0

    .line 927636
    const-string v0, "bubble_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927637
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927638
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927639
    if-eqz v0, :cond_1

    .line 927640
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927641
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927642
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927643
    if-eqz v0, :cond_2

    .line 927644
    const-string v1, "merchant_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927645
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927646
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927647
    if-eqz v0, :cond_3

    .line 927648
    const-string v1, "order_payment_method"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927649
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927650
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 927651
    if-eqz v0, :cond_4

    .line 927652
    const-string v1, "partner_logo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927653
    invoke-static {p0, v0, p2}, LX/5VF;->a(LX/15i;ILX/0nX;)V

    .line 927654
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927655
    if-eqz v0, :cond_5

    .line 927656
    const-string v1, "receipt_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927657
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927658
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927659
    if-eqz v0, :cond_6

    .line 927660
    const-string v1, "receipt_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927661
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927662
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927663
    if-eqz v0, :cond_7

    .line 927664
    const-string v1, "status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927665
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927666
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 927667
    if-eqz v0, :cond_8

    .line 927668
    const-string v1, "structured_address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927669
    invoke-static {p0, v0, p2}, LX/5V0;->a(LX/15i;ILX/0nX;)V

    .line 927670
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 927671
    if-eqz v0, :cond_9

    .line 927672
    const-string v1, "total"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 927673
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 927674
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 927675
    return-void
.end method
