.class public final enum LX/6JO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6JO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6JO;

.field public static final enum AUTO:LX/6JO;

.field public static final enum CONTINUOUS_PICTURE:LX/6JO;

.field public static final enum CONTINUOUS_VIDEO:LX/6JO;

.field public static final enum EXTENDED_DOF:LX/6JO;

.field public static final enum MACRO:LX/6JO;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1075465
    new-instance v0, LX/6JO;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v2}, LX/6JO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6JO;->AUTO:LX/6JO;

    .line 1075466
    new-instance v0, LX/6JO;

    const-string v1, "MACRO"

    invoke-direct {v0, v1, v3}, LX/6JO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6JO;->MACRO:LX/6JO;

    .line 1075467
    new-instance v0, LX/6JO;

    const-string v1, "EXTENDED_DOF"

    invoke-direct {v0, v1, v4}, LX/6JO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6JO;->EXTENDED_DOF:LX/6JO;

    .line 1075468
    new-instance v0, LX/6JO;

    const-string v1, "CONTINUOUS_PICTURE"

    invoke-direct {v0, v1, v5}, LX/6JO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6JO;->CONTINUOUS_PICTURE:LX/6JO;

    .line 1075469
    new-instance v0, LX/6JO;

    const-string v1, "CONTINUOUS_VIDEO"

    invoke-direct {v0, v1, v6}, LX/6JO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6JO;->CONTINUOUS_VIDEO:LX/6JO;

    .line 1075470
    const/4 v0, 0x5

    new-array v0, v0, [LX/6JO;

    sget-object v1, LX/6JO;->AUTO:LX/6JO;

    aput-object v1, v0, v2

    sget-object v1, LX/6JO;->MACRO:LX/6JO;

    aput-object v1, v0, v3

    sget-object v1, LX/6JO;->EXTENDED_DOF:LX/6JO;

    aput-object v1, v0, v4

    sget-object v1, LX/6JO;->CONTINUOUS_PICTURE:LX/6JO;

    aput-object v1, v0, v5

    sget-object v1, LX/6JO;->CONTINUOUS_VIDEO:LX/6JO;

    aput-object v1, v0, v6

    sput-object v0, LX/6JO;->$VALUES:[LX/6JO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1075471
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6JO;
    .locals 1

    .prologue
    .line 1075472
    const-class v0, LX/6JO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6JO;

    return-object v0
.end method

.method public static values()[LX/6JO;
    .locals 1

    .prologue
    .line 1075473
    sget-object v0, LX/6JO;->$VALUES:[LX/6JO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6JO;

    return-object v0
.end method
