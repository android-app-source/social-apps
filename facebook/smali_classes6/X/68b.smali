.class public final LX/68b;
.super Ljava/io/FilterOutputStream;
.source ""


# instance fields
.field public final synthetic a:LX/68c;


# direct methods
.method public constructor <init>(LX/68c;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 1055711
    iput-object p1, p0, LX/68b;->a:LX/68c;

    .line 1055712
    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1055713
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 2

    .prologue
    .line 1055714
    :try_start_0
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1055715
    :goto_0
    return-void

    .line 1055716
    :catch_0
    iget-object v0, p0, LX/68b;->a:LX/68c;

    const/4 v1, 0x1

    .line 1055717
    iput-boolean v1, v0, LX/68c;->d:Z

    .line 1055718
    goto :goto_0
.end method

.method public final flush()V
    .locals 2

    .prologue
    .line 1055719
    :try_start_0
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1055720
    :goto_0
    return-void

    .line 1055721
    :catch_0
    iget-object v0, p0, LX/68b;->a:LX/68c;

    const/4 v1, 0x1

    .line 1055722
    iput-boolean v1, v0, LX/68c;->d:Z

    .line 1055723
    goto :goto_0
.end method

.method public final write(I)V
    .locals 2

    .prologue
    .line 1055724
    :try_start_0
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1055725
    :goto_0
    return-void

    .line 1055726
    :catch_0
    iget-object v0, p0, LX/68b;->a:LX/68c;

    const/4 v1, 0x1

    .line 1055727
    iput-boolean v1, v0, LX/68c;->d:Z

    .line 1055728
    goto :goto_0
.end method

.method public final write([BII)V
    .locals 2

    .prologue
    .line 1055729
    :try_start_0
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1055730
    :goto_0
    return-void

    .line 1055731
    :catch_0
    iget-object v0, p0, LX/68b;->a:LX/68c;

    const/4 v1, 0x1

    .line 1055732
    iput-boolean v1, v0, LX/68c;->d:Z

    .line 1055733
    goto :goto_0
.end method
