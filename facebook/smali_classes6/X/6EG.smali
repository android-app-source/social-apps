.class public final LX/6EG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lorg/json/JSONObject;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;)V
    .locals 0

    .prologue
    .line 1065971
    iput-object p1, p0, LX/6EG;->a:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1065972
    iget-object v0, p0, LX/6EG;->a:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->p:LX/6EP;

    if-eqz v0, :cond_0

    .line 1065973
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1065974
    :try_start_0
    const-string v0, "error"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1065975
    :goto_0
    iget-object v0, p0, LX/6EG;->a:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->p:LX/6EP;

    invoke-virtual {v0, v1}, LX/6EP;->a(Lorg/json/JSONObject;)V

    .line 1065976
    :cond_0
    iget-object v0, p0, LX/6EG;->a:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const-string v1, "Login failed."

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 1065977
    return-void

    .line 1065978
    :catch_0
    move-exception v0

    .line 1065979
    iget-object v2, p0, LX/6EG;->a:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->C:LX/03V;

    sget-object v3, Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;->x:Ljava/lang/String;

    const-string v4, "Failed to create error result"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1065980
    check-cast p1, Lorg/json/JSONObject;

    .line 1065981
    iget-object v0, p0, LX/6EG;->a:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->p:LX/6EP;

    invoke-virtual {v0, p1}, LX/6EP;->a(Lorg/json/JSONObject;)V

    .line 1065982
    iget-object v0, p0, LX/6EG;->a:Lcom/facebook/browserextensions/common/identity/LoginDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1065983
    return-void
.end method
