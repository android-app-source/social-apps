.class public final LX/5Tn;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 922719
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 922720
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 922721
    :goto_0
    return v1

    .line 922722
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_3

    .line 922723
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 922724
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 922725
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_0

    if-eqz v9, :cond_0

    .line 922726
    const-string v10, "height_ratio"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 922727
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v7

    goto :goto_1

    .line 922728
    :cond_1
    const-string v10, "hide_share_button"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 922729
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    move v8, v6

    move v6, v7

    goto :goto_1

    .line 922730
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 922731
    :cond_3
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 922732
    if-eqz v0, :cond_4

    move-object v0, p1

    .line 922733
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 922734
    :cond_4
    if-eqz v6, :cond_5

    .line 922735
    invoke-virtual {p1, v7, v8}, LX/186;->a(IZ)V

    .line 922736
    :cond_5
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v6, v1

    move v0, v1

    move v8, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 922737
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 922738
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 922739
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_0

    .line 922740
    const-string v2, "height_ratio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922741
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 922742
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 922743
    if-eqz v0, :cond_1

    .line 922744
    const-string v1, "hide_share_button"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922745
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 922746
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 922747
    return-void
.end method
