.class public final LX/632;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/text/BetterEditTextView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/text/BetterEditTextView;)V
    .locals 0

    .prologue
    .line 1042765
    iput-object p1, p0, LX/632;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/widget/text/BetterEditTextView;B)V
    .locals 0

    .prologue
    .line 1042766
    invoke-direct {p0, p1}, LX/632;-><init>(Lcom/facebook/widget/text/BetterEditTextView;)V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1042767
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1042768
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1042769
    iget-object v0, p0, LX/632;->a:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1042770
    invoke-static {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->a$redex0(Lcom/facebook/widget/text/BetterEditTextView;Ljava/lang/CharSequence;)V

    .line 1042771
    iget-object v0, p0, LX/632;->a:Lcom/facebook/widget/text/BetterEditTextView;

    iget-boolean v0, v0, Lcom/facebook/widget/text/BetterEditTextView;->g:Z

    if-eqz v0, :cond_1

    .line 1042772
    iget-object v0, p0, LX/632;->a:Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v1, 0x0

    .line 1042773
    iput-boolean v1, v0, Lcom/facebook/widget/text/BetterEditTextView;->g:Z

    .line 1042774
    :cond_0
    :goto_0
    return-void

    .line 1042775
    :cond_1
    iget-object v0, p0, LX/632;->a:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v0, v0, Lcom/facebook/widget/text/BetterEditTextView;->c:LX/633;

    if-eqz v0, :cond_0

    .line 1042776
    iget-object v0, p0, LX/632;->a:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v0, v0, Lcom/facebook/widget/text/BetterEditTextView;->c:LX/633;

    invoke-interface {v0, p1}, LX/633;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
