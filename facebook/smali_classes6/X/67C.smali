.class public final LX/67C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/670;


# instance fields
.field public final a:LX/672;

.field public final b:LX/65J;

.field public c:Z


# direct methods
.method public constructor <init>(LX/65J;)V
    .locals 2

    .prologue
    .line 1052408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052409
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    iput-object v0, p0, LX/67C;->a:LX/672;

    .line 1052410
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052411
    :cond_0
    iput-object p1, p0, LX/67C;->b:LX/65J;

    .line 1052412
    return-void
.end method


# virtual methods
.method public final a(LX/65D;)J
    .locals 6

    .prologue
    .line 1052442
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052443
    :cond_0
    const-wide/16 v0, 0x0

    .line 1052444
    :goto_0
    iget-object v2, p0, LX/67C;->a:LX/672;

    const-wide/16 v4, 0x2000

    invoke-interface {p1, v2, v4, v5}, LX/65D;->a(LX/672;J)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 1052445
    add-long/2addr v0, v2

    .line 1052446
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    goto :goto_0

    .line 1052447
    :cond_1
    return-wide v0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1052441
    iget-object v0, p0, LX/67C;->b:LX/65J;

    invoke-interface {v0}, LX/65J;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final a_(LX/672;J)V
    .locals 2

    .prologue
    .line 1052437
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052438
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1, p2, p3}, LX/672;->a_(LX/672;J)V

    .line 1052439
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    .line 1052440
    return-void
.end method

.method public final b(LX/673;)LX/670;
    .locals 2

    .prologue
    .line 1052434
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052435
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1}, LX/672;->a(LX/673;)LX/672;

    .line 1052436
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/670;
    .locals 2

    .prologue
    .line 1052431
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052432
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1}, LX/672;->a(Ljava/lang/String;)LX/672;

    .line 1052433
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final c([B)LX/670;
    .locals 2

    .prologue
    .line 1052428
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052429
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1}, LX/672;->b([B)LX/672;

    .line 1052430
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final c([BII)LX/670;
    .locals 2

    .prologue
    .line 1052425
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052426
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1, p2, p3}, LX/672;->b([BII)LX/672;

    .line 1052427
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/672;
    .locals 1

    .prologue
    .line 1052424
    iget-object v0, p0, LX/67C;->a:LX/672;

    return-object v0
.end method

.method public final close()V
    .locals 6

    .prologue
    .line 1052413
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_1

    .line 1052414
    :cond_0
    :goto_0
    return-void

    .line 1052415
    :cond_1
    const/4 v0, 0x0

    .line 1052416
    :try_start_0
    iget-object v1, p0, LX/67C;->a:LX/672;

    iget-wide v2, v1, LX/672;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 1052417
    iget-object v1, p0, LX/67C;->b:LX/65J;

    iget-object v2, p0, LX/67C;->a:LX/672;

    iget-object v3, p0, LX/67C;->a:LX/672;

    iget-wide v4, v3, LX/672;->b:J

    invoke-interface {v1, v2, v4, v5}, LX/65J;->a_(LX/672;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 1052418
    :cond_2
    :goto_1
    :try_start_1
    iget-object v1, p0, LX/67C;->b:LX/65J;

    invoke-interface {v1}, LX/65J;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 1052419
    :cond_3
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/67C;->c:Z

    .line 1052420
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/67J;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1052421
    :catch_0
    move-exception v1

    .line 1052422
    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_2

    .line 1052423
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final d()LX/670;
    .locals 6

    .prologue
    .line 1052448
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052449
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    .line 1052450
    iget-wide v4, v0, LX/672;->b:J

    move-wide v0, v4

    .line 1052451
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget-object v2, p0, LX/67C;->b:LX/65J;

    iget-object v3, p0, LX/67C;->a:LX/672;

    invoke-interface {v2, v3, v0, v1}, LX/65J;->a_(LX/672;J)V

    .line 1052452
    :cond_1
    return-object p0
.end method

.method public final f(I)LX/670;
    .locals 2

    .prologue
    .line 1052380
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052381
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1}, LX/672;->d(I)LX/672;

    .line 1052382
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final flush()V
    .locals 4

    .prologue
    .line 1052383
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052384
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    iget-wide v0, v0, LX/672;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1052385
    iget-object v0, p0, LX/67C;->b:LX/65J;

    iget-object v1, p0, LX/67C;->a:LX/672;

    iget-object v2, p0, LX/67C;->a:LX/672;

    iget-wide v2, v2, LX/672;->b:J

    invoke-interface {v0, v1, v2, v3}, LX/65J;->a_(LX/672;J)V

    .line 1052386
    :cond_1
    iget-object v0, p0, LX/67C;->b:LX/65J;

    invoke-interface {v0}, LX/65J;->flush()V

    .line 1052387
    return-void
.end method

.method public final g(I)LX/670;
    .locals 2

    .prologue
    .line 1052388
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052389
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1}, LX/672;->c(I)LX/672;

    .line 1052390
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final h(I)LX/670;
    .locals 2

    .prologue
    .line 1052391
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052392
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1}, LX/672;->b(I)LX/672;

    .line 1052393
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final j(J)LX/670;
    .locals 3

    .prologue
    .line 1052394
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052395
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1, p2}, LX/672;->i(J)LX/672;

    .line 1052396
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final k(J)LX/670;
    .locals 3

    .prologue
    .line 1052397
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052398
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1, p2}, LX/672;->h(J)LX/672;

    .line 1052399
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final l(J)LX/670;
    .locals 3

    .prologue
    .line 1052400
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052401
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0, p1, p2}, LX/672;->g(J)LX/672;

    .line 1052402
    invoke-virtual {p0}, LX/67C;->t()LX/670;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/670;
    .locals 4

    .prologue
    .line 1052403
    iget-boolean v0, p0, LX/67C;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052404
    :cond_0
    iget-object v0, p0, LX/67C;->a:LX/672;

    invoke-virtual {v0}, LX/672;->g()J

    move-result-wide v0

    .line 1052405
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget-object v2, p0, LX/67C;->b:LX/65J;

    iget-object v3, p0, LX/67C;->a:LX/672;

    invoke-interface {v2, v3, v0, v1}, LX/65J;->a_(LX/672;J)V

    .line 1052406
    :cond_1
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1052407
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "buffer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/67C;->b:LX/65J;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
