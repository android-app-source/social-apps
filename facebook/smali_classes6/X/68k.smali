.class public final LX/68k;
.super LX/68i;
.source ""


# instance fields
.field public final synthetic i:LX/68m;


# direct methods
.method public constructor <init>(LX/68m;)V
    .locals 0

    .prologue
    .line 1056313
    iput-object p1, p0, LX/68k;->i:LX/68m;

    invoke-direct {p0}, LX/68i;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;FF)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x4

    const/4 v0, 0x0

    .line 1056314
    invoke-super {p0, p1, p2, p3}, LX/68i;->a(Landroid/graphics/Canvas;FF)V

    .line 1056315
    iget-object v1, p0, LX/68i;->a:LX/69C;

    if-nez v1, :cond_0

    .line 1056316
    sget-object v1, LX/68m;->z:Landroid/graphics/Bitmap;

    iget-object v3, p0, LX/68i;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, p2, p3, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1056317
    :cond_0
    iget-object v1, p0, LX/68i;->a:LX/69C;

    if-eqz v1, :cond_3

    .line 1056318
    iget-object v0, p0, LX/68i;->a:LX/69C;

    iget v0, v0, LX/69C;->h:I

    if-nez v0, :cond_1

    .line 1056319
    iget-object v0, p0, LX/68i;->a:LX/69C;

    iget-object v1, p0, LX/68i;->a:LX/69C;

    iget-object v2, p0, LX/68k;->i:LX/68m;

    iget v2, v2, LX/68m;->D:I

    invoke-static {v1, v2}, LX/3BU;->a(LX/69C;I)I

    move-result v1

    iput v1, v0, LX/69C;->h:I

    .line 1056320
    :cond_1
    iget-object v0, p0, LX/68i;->a:LX/69C;

    iget v0, v0, LX/69C;->h:I

    .line 1056321
    :cond_2
    :goto_0
    iget-object v1, p0, LX/68k;->i:LX/68m;

    iget-object v1, v1, LX/68m;->B:LX/68Y;

    iget v2, v1, LX/68Y;->o:I

    or-int/2addr v0, v2

    iput v0, v1, LX/68Y;->o:I

    .line 1056322
    return-void

    .line 1056323
    :cond_3
    iget-object v1, p0, LX/68i;->c:[LX/69C;

    if-eqz v1, :cond_8

    move v4, v0

    move v1, v0

    .line 1056324
    :goto_1
    if-ge v4, v6, :cond_6

    .line 1056325
    iget-object v3, p0, LX/68i;->c:[LX/69C;

    aget-object v3, v3, v4

    if-eqz v3, :cond_5

    iget-object v3, p0, LX/68i;->c:[LX/69C;

    aget-object v3, v3, v4

    .line 1056326
    iget-object v5, v3, LX/69C;->r:Landroid/graphics/Bitmap;

    move-object v3, v5

    .line 1056327
    :goto_2
    if-eqz v3, :cond_4

    sget-object v5, LX/69C;->a:Landroid/graphics/Bitmap;

    if-eq v3, v5, :cond_4

    .line 1056328
    iget-object v3, p0, LX/68i;->c:[LX/69C;

    aget-object v3, v3, v4

    iget v3, v3, LX/69C;->h:I

    or-int/2addr v1, v3

    .line 1056329
    add-int/lit8 v0, v0, 0x1

    .line 1056330
    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_5
    move-object v3, v2

    .line 1056331
    goto :goto_2

    :cond_6
    move v7, v0

    move v0, v1

    move v1, v7

    .line 1056332
    :goto_3
    if-eq v1, v6, :cond_2

    .line 1056333
    iget-object v1, p0, LX/68i;->b:LX/69C;

    if-eqz v1, :cond_7

    iget-object v1, p0, LX/68i;->b:LX/69C;

    .line 1056334
    iget-object v2, v1, LX/69C;->r:Landroid/graphics/Bitmap;

    move-object v1, v2

    .line 1056335
    :goto_4
    if-eqz v1, :cond_2

    sget-object v2, LX/69C;->a:Landroid/graphics/Bitmap;

    if-eq v1, v2, :cond_2

    .line 1056336
    iget-object v1, p0, LX/68i;->b:LX/69C;

    iget v1, v1, LX/69C;->h:I

    or-int/2addr v0, v1

    goto :goto_0

    :cond_7
    move-object v1, v2

    .line 1056337
    goto :goto_4

    :cond_8
    move v1, v0

    goto :goto_3
.end method
