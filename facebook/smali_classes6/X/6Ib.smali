.class public LX/6Ib;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ia;


# instance fields
.field public final a:LX/6JF;

.field public b:LX/6Ik;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/6JB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/6Ik;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/6JG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/6JC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/6KN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Z

.field public j:Z

.field private k:I

.field public l:LX/6Jj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I

.field private n:Ljava/lang/String;

.field public o:LX/6IP;

.field private final p:LX/6IY;

.field private final q:LX/5fi;

.field private final r:LX/5f5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5f5",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/6JF;LX/6KN;)V
    .locals 1

    .prologue
    .line 1074233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1074234
    new-instance v0, LX/6IY;

    invoke-direct {v0, p0}, LX/6IY;-><init>(LX/6Ib;)V

    iput-object v0, p0, LX/6Ib;->p:LX/6IY;

    .line 1074235
    new-instance v0, LX/6IZ;

    invoke-direct {v0, p0}, LX/6IZ;-><init>(LX/6Ib;)V

    iput-object v0, p0, LX/6Ib;->q:LX/5fi;

    .line 1074236
    new-instance v0, LX/6IR;

    invoke-direct {v0, p0}, LX/6IR;-><init>(LX/6Ib;)V

    iput-object v0, p0, LX/6Ib;->r:LX/5f5;

    .line 1074237
    iput-object p1, p0, LX/6Ib;->a:LX/6JF;

    .line 1074238
    iput-object p2, p0, LX/6Ib;->g:LX/6KN;

    .line 1074239
    return-void
.end method

.method public static a(LX/6JF;)LX/5fM;
    .locals 1

    .prologue
    .line 1074240
    sget-object v0, LX/6JF;->FRONT:LX/6JF;

    if-ne p0, v0, :cond_0

    sget-object v0, LX/5fM;->FRONT:LX/5fM;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/5fM;->BACK:LX/5fM;

    goto :goto_0
.end method

.method private a(Ljava/io/File;LX/6JG;LX/6JC;)V
    .locals 2
    .param p3    # LX/6JC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1074241
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1074242
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1074243
    :cond_1
    iget-boolean v0, p0, LX/6Ib;->j:Z

    if-eqz v0, :cond_2

    .line 1074244
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1074245
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6Ib;->j:Z

    .line 1074246
    if-eqz p3, :cond_3

    .line 1074247
    invoke-static {p0, p3}, LX/6Ib;->a$redex0(LX/6Ib;LX/6JC;)V

    .line 1074248
    :cond_3
    iput-object p2, p0, LX/6Ib;->e:LX/6JG;

    .line 1074249
    const/4 v0, 0x2

    invoke-static {p0, v0}, LX/6Ib;->b$redex0(LX/6Ib;I)V

    .line 1074250
    :try_start_0
    invoke-static {p1}, LX/6JM;->a(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074251
    :goto_0
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074252
    new-instance v1, LX/6IX;

    invoke-direct {v1, p0}, LX/6IX;-><init>(LX/6Ib;)V

    .line 1074253
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, LX/5fQ;->a(LX/5f5;Ljava/lang/String;)V

    .line 1074254
    return-void

    .line 1074255
    :catch_0
    move-exception v0

    .line 1074256
    invoke-static {p0, v0}, LX/6Ib;->a$redex0(LX/6Ib;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/6Ib;LX/5fS;)V
    .locals 2

    .prologue
    .line 1074257
    iget-object v0, p0, LX/6Ib;->c:LX/6JB;

    iget v0, v0, LX/6JB;->a:I

    iget-object v1, p0, LX/6Ib;->c:LX/6JB;

    iget v1, v1, LX/6JB;->b:I

    invoke-virtual {p1, v0, v1}, LX/5fS;->b(II)V

    .line 1074258
    invoke-direct {p0}, LX/6Ib;->n()I

    move-result v0

    invoke-direct {p0}, LX/6Ib;->o()I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/5fS;->a(II)V

    .line 1074259
    return-void
.end method

.method public static a$redex0(LX/6Ib;LX/6JC;)V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1074260
    if-nez p1, :cond_1

    .line 1074261
    :cond_0
    :goto_0
    return-void

    .line 1074262
    :cond_1
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074263
    invoke-virtual {v0}, LX/5fQ;->b()LX/5fS;

    move-result-object v3

    .line 1074264
    iget-object v0, p1, LX/6JC;->b:LX/6JO;

    .line 1074265
    if-nez v0, :cond_2

    .line 1074266
    invoke-virtual {p0}, LX/6Ib;->a()LX/6IP;

    move-result-object v0

    invoke-interface {v0}, LX/6IP;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/6J8;->a(Ljava/util/List;)LX/6JO;

    move-result-object v0

    .line 1074267
    :cond_2
    sget-object v4, LX/6IO;->b:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v0, v4

    .line 1074268
    if-eqz v0, :cond_3

    invoke-virtual {v3}, LX/5fS;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1074269
    invoke-virtual {v3, v0}, LX/5fS;->b(Ljava/lang/String;)V

    .line 1074270
    :cond_3
    iget-object v0, p1, LX/6JC;->a:LX/6JN;

    .line 1074271
    sget-object v4, LX/6IO;->d:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v4, v4

    .line 1074272
    if-eqz v4, :cond_6

    invoke-virtual {v3}, LX/5fS;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1074273
    invoke-static {p0, v6}, LX/6Ib;->b$redex0(LX/6Ib;I)V

    .line 1074274
    sget-object v0, LX/6JN;->TORCH:LX/6JN;

    iget-object v5, p1, LX/6JC;->a:LX/6JN;

    invoke-virtual {v0, v5}, LX/6JN;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1074275
    invoke-static {p0, v7}, LX/6Ib;->b$redex0(LX/6Ib;I)V

    move v0, v1

    .line 1074276
    :goto_1
    invoke-virtual {v3, v4}, LX/5fS;->a(Ljava/lang/String;)V

    .line 1074277
    :goto_2
    invoke-virtual {v3, v2}, LX/5fS;->a(Z)V

    .line 1074278
    if-eqz v1, :cond_4

    .line 1074279
    invoke-static {p0, v6}, LX/6Ib;->c(LX/6Ib;I)V

    .line 1074280
    :cond_4
    if-eqz v0, :cond_0

    .line 1074281
    invoke-static {p0, v7}, LX/6Ib;->c(LX/6Ib;I)V

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v2

    move v1, v2

    goto :goto_2
.end method

.method public static a$redex0(LX/6Ib;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1074282
    const/4 v0, 0x2

    invoke-static {p0, v0}, LX/6Ib;->d(LX/6Ib;I)V

    .line 1074283
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Ib;->j:Z

    .line 1074284
    iget-object v0, p0, LX/6Ib;->f:LX/6JC;

    invoke-static {p0, v0}, LX/6Ib;->a$redex0(LX/6Ib;LX/6JC;)V

    .line 1074285
    iget-object v0, p0, LX/6Ib;->e:LX/6JG;

    if-eqz v0, :cond_0

    .line 1074286
    iget-object v0, p0, LX/6Ib;->e:LX/6JG;

    new-instance v1, LX/6JJ;

    const-string v2, "Failed to start video recording"

    invoke-direct {v1, v2, p1}, LX/6JJ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, LX/6JG;->a(LX/6JJ;)V

    .line 1074287
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Ib;->e:LX/6JG;

    .line 1074288
    :cond_0
    return-void
.end method

.method public static b$redex0(LX/6Ib;I)V
    .locals 3

    .prologue
    .line 1074289
    iget-object v0, p0, LX/6Ib;->g:LX/6KN;

    if-eqz v0, :cond_0

    .line 1074290
    iget-object v0, p0, LX/6Ib;->g:LX/6KN;

    iget-object v1, p0, LX/6Ib;->n:Ljava/lang/String;

    sget-object v2, LX/6J9;->CAMERA1:LX/6J9;

    invoke-virtual {v0, p1, v1, v2}, LX/6KN;->a(ILjava/lang/String;LX/6J9;)V

    .line 1074291
    :cond_0
    return-void
.end method

.method public static c(LX/6Ib;I)V
    .locals 1

    .prologue
    .line 1074220
    iget-object v0, p0, LX/6Ib;->g:LX/6KN;

    if-eqz v0, :cond_0

    .line 1074221
    iget-object v0, p0, LX/6Ib;->g:LX/6KN;

    invoke-virtual {v0, p1}, LX/6KN;->a(I)V

    .line 1074222
    :cond_0
    return-void
.end method

.method public static d(LX/6Ib;I)V
    .locals 1

    .prologue
    .line 1074292
    iget-object v0, p0, LX/6Ib;->g:LX/6KN;

    if-eqz v0, :cond_0

    .line 1074293
    iget-object v0, p0, LX/6Ib;->g:LX/6KN;

    invoke-virtual {v0, p1}, LX/6KN;->b(I)V

    .line 1074294
    :cond_0
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1074295
    iget-boolean v0, p0, LX/6Ib;->h:Z

    if-nez v0, :cond_0

    .line 1074296
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Camera is not open"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074297
    :cond_0
    return-void
.end method

.method private n()I
    .locals 2

    .prologue
    .line 1074298
    iget-object v0, p0, LX/6Ib;->c:LX/6JB;

    .line 1074299
    iget-object v1, v0, LX/6JB;->f:Ljava/util/List;

    move-object v0, v1

    .line 1074300
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JA;

    iget v0, v0, LX/6JA;->b:I

    return v0
.end method

.method private o()I
    .locals 2

    .prologue
    .line 1074301
    iget-object v0, p0, LX/6Ib;->c:LX/6JB;

    .line 1074302
    iget-object v1, v0, LX/6JB;->f:Ljava/util/List;

    move-object v0, v1

    .line 1074303
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6JA;

    iget v0, v0, LX/6JA;->c:I

    return v0
.end method


# virtual methods
.method public final a()LX/6IP;
    .locals 1

    .prologue
    .line 1074304
    invoke-direct {p0}, LX/6Ib;->l()V

    .line 1074305
    iget-object v0, p0, LX/6Ib;->o:LX/6IP;

    return-object v0
.end method

.method public final a(FF)V
    .locals 9

    .prologue
    const/4 v4, 0x4

    .line 1074306
    invoke-static {p0, v4}, LX/6Ib;->b$redex0(LX/6Ib;I)V

    .line 1074307
    iget-object v0, p0, LX/6Ib;->a:LX/6JF;

    const/4 v8, 0x0

    const/high16 v7, 0x447a0000    # 1000.0f

    const/4 v5, 0x0

    const/high16 v6, -0x3b860000    # -1000.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1074308
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 1074309
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v5, v5, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v6, v6, v7, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v6, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v1, v5, v6}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1074310
    sget-object v1, LX/5fQ;->b:LX/5fQ;

    move-object v1, v1

    .line 1074311
    invoke-virtual {v1}, LX/5fQ;->f()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v3, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1074312
    sget-object v1, LX/6JF;->FRONT:LX/6JF;

    if-ne v0, v1, :cond_0

    const/high16 v1, -0x40800000    # -1.0f

    :goto_0
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1074313
    const/4 v1, 0x2

    new-array v1, v1, [F

    aput p1, v1, v8

    const/4 v2, 0x1

    aput p2, v1, v2

    .line 1074314
    invoke-virtual {v3, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1074315
    new-instance v2, Landroid/graphics/Point;

    aget v3, v1, v8

    invoke-static {v3}, LX/6Ic;->a(F)I

    move-result v3

    const/4 v5, 0x1

    aget v1, v1, v5

    invoke-static {v1}, LX/6Ic;->a(F)I

    move-result v1

    invoke-direct {v2, v3, v1}, Landroid/graphics/Point;-><init>(II)V

    move-object v0, v2

    .line 1074316
    sget-object v1, LX/5fQ;->b:LX/5fQ;

    move-object v1, v1

    .line 1074317
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v3}, LX/5fQ;->b(II)V

    .line 1074318
    sget-object v1, LX/5fQ;->b:LX/5fQ;

    move-object v1, v1

    .line 1074319
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v0}, LX/5fQ;->a(II)V

    .line 1074320
    invoke-static {p0, v4}, LX/6Ib;->c(LX/6Ib;I)V

    .line 1074321
    return-void

    :cond_0
    move v1, v2

    .line 1074322
    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1074139
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074140
    invoke-virtual {v0, p1}, LX/5fQ;->c(I)V

    .line 1074141
    return-void
.end method

.method public final a(ILX/6Jd;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/cameracore/camerasdk/common/Callback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1074323
    iget v0, p0, LX/6Ib;->k:I

    if-ne v0, p1, :cond_0

    .line 1074324
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, LX/6Jd;->a(Ljava/lang/Object;)V

    .line 1074325
    :goto_0
    return-void

    .line 1074326
    :cond_0
    iput p1, p0, LX/6Ib;->k:I

    .line 1074327
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074328
    mul-int/lit8 v1, p1, 0x5a

    invoke-virtual {v0, v1}, LX/5fQ;->a(I)V

    .line 1074329
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074330
    new-instance v1, LX/6IS;

    invoke-direct {v1, p0, p2}, LX/6IS;-><init>(LX/6Ib;LX/6Jd;)V

    invoke-virtual {v0, p1, v1}, LX/5fQ;->a(ILX/5f5;)V

    goto :goto_0
.end method

.method public final a(LX/6Ik;)V
    .locals 3

    .prologue
    .line 1074223
    iget-object v0, p0, LX/6Ib;->b:LX/6Ik;

    if-eqz v0, :cond_0

    .line 1074224
    new-instance v0, LX/6JK;

    const/4 v1, 0x1

    const-string v2, "Camera is already in use"

    invoke-direct {v0, v1, v2}, LX/6JK;-><init>(ILjava/lang/String;)V

    invoke-interface {p1, v0}, LX/6Ik;->a(Ljava/lang/Throwable;)V

    .line 1074225
    :goto_0
    return-void

    .line 1074226
    :cond_0
    iput-object p1, p0, LX/6Ib;->b:LX/6Ik;

    .line 1074227
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6Ib;->n:Ljava/lang/String;

    .line 1074228
    iget-boolean v0, p0, LX/6Ib;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/6Ib;->b:LX/6Ik;

    if-nez v0, :cond_2

    .line 1074229
    :cond_1
    :goto_1
    goto :goto_0

    .line 1074230
    :cond_2
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/6Ib;->b$redex0(LX/6Ib;I)V

    .line 1074231
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074232
    iget-object v1, p0, LX/6Ib;->a:LX/6JF;

    invoke-static {v1}, LX/6Ib;->a(LX/6JF;)LX/5fM;

    move-result-object v1

    new-instance v2, LX/6IV;

    invoke-direct {v2, p0}, LX/6IV;-><init>(LX/6Ib;)V

    invoke-virtual {v0, v1, v2}, LX/5fQ;->a(LX/5fM;LX/5f5;)V

    goto :goto_1
.end method

.method public final a(LX/6Ik;LX/6JC;)V
    .locals 10

    .prologue
    .line 1074117
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1074118
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1074119
    :cond_1
    iget-boolean v0, p0, LX/6Ib;->h:Z

    if-nez v0, :cond_2

    .line 1074120
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Camera is not open"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074121
    :cond_2
    iget-object v0, p0, LX/6Ib;->c:LX/6JB;

    if-nez v0, :cond_3

    .line 1074122
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Camera settings are not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074123
    :cond_3
    iput-object p1, p0, LX/6Ib;->d:LX/6Ik;

    .line 1074124
    iput-object p2, p0, LX/6Ib;->f:LX/6JC;

    .line 1074125
    const/16 v0, 0x8

    invoke-static {p0, v0}, LX/6Ib;->b$redex0(LX/6Ib;I)V

    .line 1074126
    iget-object v0, p0, LX/6Ib;->f:LX/6JC;

    invoke-static {p0, v0}, LX/6Ib;->a$redex0(LX/6Ib;LX/6JC;)V

    .line 1074127
    iget-object v0, p0, LX/6Ib;->l:LX/6Jj;

    if-eqz v0, :cond_4

    .line 1074128
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074129
    iget v1, p0, LX/6Ib;->m:I

    .line 1074130
    iget-object v2, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v3, v0, LX/5fQ;->i:LX/5fM;

    invoke-static {v3}, LX/5fQ;->b(LX/5fM;)I

    move-result v3

    invoke-static {v2, v3}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/5fS;->a(I)V

    .line 1074131
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074132
    iget-object v1, p0, LX/6Ib;->p:LX/6IY;

    invoke-virtual {v0, v1}, LX/5fQ;->a(LX/6IY;)V

    .line 1074133
    :cond_4
    iget-object v0, p0, LX/6Ib;->c:LX/6JB;

    .line 1074134
    iget-object v1, v0, LX/6JB;->f:Ljava/util/List;

    move-object v0, v1

    .line 1074135
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, LX/6JA;

    .line 1074136
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074137
    iget-object v1, v5, LX/6JA;->a:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, LX/6Ib;->a:LX/6JF;

    invoke-static {v2}, LX/6Ib;->a(LX/6JF;)LX/5fM;

    move-result-object v2

    iget v3, p0, LX/6Ib;->k:I

    iget v4, v5, LX/6JA;->b:I

    iget v5, v5, LX/6JA;->c:I

    sget-object v6, LX/5fO;->HIGH:LX/5fO;

    sget-object v7, LX/5fO;->HIGH:LX/5fO;

    iget-object v8, p0, LX/6Ib;->q:LX/5fi;

    iget-object v9, p0, LX/6Ib;->r:LX/5f5;

    invoke-virtual/range {v0 .. v9}, LX/5fQ;->a(Landroid/graphics/SurfaceTexture;LX/5fM;IIILX/5fO;LX/5fO;LX/5fi;LX/5f5;)V

    .line 1074138
    return-void
.end method

.method public final a(LX/6JB;)V
    .locals 2

    .prologue
    .line 1074142
    iget-object v0, p1, LX/6JB;->f:Ljava/util/List;

    move-object v0, v0

    .line 1074143
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1074144
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Camera does not support multiple output surfaces"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074145
    :cond_0
    iput-object p1, p0, LX/6Ib;->c:LX/6JB;

    .line 1074146
    iget v0, p1, LX/6JB;->e:I

    iput v0, p0, LX/6Ib;->k:I

    .line 1074147
    return-void
.end method

.method public final a(LX/6Jj;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1074148
    iget-object v0, p0, LX/6Ib;->d:LX/6Ik;

    if-eqz v0, :cond_0

    .line 1074149
    new-instance v0, LX/6JJ;

    const-string v1, "Cannot set FrameCallback while in preview"

    invoke-direct {v0, v1}, LX/6JJ;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074150
    :cond_0
    iget-object v0, p0, LX/6Ib;->c:LX/6JB;

    if-nez v0, :cond_1

    .line 1074151
    new-instance v0, LX/6JJ;

    const-string v1, "Camera settings has to specified first"

    invoke-direct {v0, v1}, LX/6JJ;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074152
    :cond_1
    if-nez p1, :cond_3

    .line 1074153
    iput-object v4, p0, LX/6Ib;->l:LX/6Jj;

    .line 1074154
    const/16 v0, 0x11

    iput v0, p0, LX/6Ib;->m:I

    .line 1074155
    :cond_2
    :goto_0
    return-void

    .line 1074156
    :cond_3
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074157
    iget-object v1, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    iget-object v2, v0, LX/5fQ;->i:LX/5fM;

    invoke-static {v2}, LX/5fQ;->b(LX/5fM;)I

    move-result v2

    invoke-static {v1, v2}, LX/5fS;->a(Landroid/hardware/Camera;I)LX/5fS;

    move-result-object v1

    invoke-virtual {v1}, LX/5fS;->a()Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 1074158
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1074159
    new-instance v0, LX/6JJ;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Frame callback format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6JJ;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074160
    :cond_4
    iput-object p1, p0, LX/6Ib;->l:LX/6Jj;

    .line 1074161
    iput p2, p0, LX/6Ib;->m:I

    .line 1074162
    iget v0, p0, LX/6Ib;->m:I

    invoke-static {v0}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result v0

    .line 1074163
    const/4 v1, -0x1

    if-ne v0, v1, :cond_5

    .line 1074164
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ImageFormat.getBitsPerPixel for format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " returned -1 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074165
    :cond_5
    sget-object v1, LX/5fQ;->b:LX/5fQ;

    move-object v1, v1

    .line 1074166
    invoke-virtual {v1, v4}, LX/5fQ;->a(LX/6IY;)V

    .line 1074167
    invoke-direct {p0}, LX/6Ib;->n()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-direct {p0}, LX/6Ib;->o()I

    move-result v1

    mul-int/2addr v1, v0

    .line 1074168
    rem-int/lit8 v0, v1, 0x8

    if-eqz v0, :cond_6

    .line 1074169
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Total bits for Frame callback buffer should be a multiple of 8"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074170
    :cond_6
    const/4 v0, 0x0

    :goto_1
    const/4 v2, 0x2

    if-ge v0, v2, :cond_2

    .line 1074171
    :try_start_0
    sget-object v2, LX/5fQ;->b:LX/5fQ;

    move-object v2, v2

    .line 1074172
    div-int/lit8 v3, v1, 0x8

    new-array v3, v3, [B

    .line 1074173
    if-eqz v3, :cond_7

    .line 1074174
    iget-object p1, v2, LX/5fQ;->d:Landroid/hardware/Camera;

    invoke-virtual {p1, v3}, Landroid/hardware/Camera;->addCallbackBuffer([B)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074175
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1074176
    :catch_0
    move-exception v0

    .line 1074177
    sget-object v2, LX/5fQ;->b:LX/5fQ;

    move-object v2, v2

    .line 1074178
    invoke-virtual {v2, v4}, LX/5fQ;->a(LX/6IY;)V

    .line 1074179
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to allocate "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    div-int/lit8 v1, v1, 0x8

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes for preview size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, LX/6Ib;->n()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, LX/6Ib;->o()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1074180
    new-instance v2, LX/6JK;

    const/4 v3, 0x6

    invoke-direct {v2, v3, v1, v0}, LX/6JK;-><init>(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 1074181
    iget-object v0, p0, LX/6Ib;->b:LX/6Ik;

    if-eqz v0, :cond_8

    .line 1074182
    iget-object v0, p0, LX/6Ib;->b:LX/6Ik;

    invoke-interface {v0, v2}, LX/6Ik;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1074183
    :cond_8
    throw v2
.end method

.method public final a(Ljava/io/File;LX/6JG;)V
    .locals 1

    .prologue
    .line 1074184
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/6Ib;->a(Ljava/io/File;LX/6JG;LX/6JC;)V

    .line 1074185
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1074186
    sget-object v0, LX/6JE;->a:LX/6JE;

    invoke-virtual {p0, v0}, LX/6Ib;->b(LX/6Ik;)V

    .line 1074187
    return-void
.end method

.method public final b(LX/6Ik;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1074188
    iget-boolean v0, p0, LX/6Ib;->h:Z

    if-nez v0, :cond_0

    .line 1074189
    :goto_0
    return-void

    .line 1074190
    :cond_0
    const/4 v2, 0x0

    .line 1074191
    invoke-virtual {p0}, LX/6Ib;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1074192
    :goto_1
    iput-object v1, p0, LX/6Ib;->b:LX/6Ik;

    .line 1074193
    iput-object v1, p0, LX/6Ib;->d:LX/6Ik;

    .line 1074194
    iput-object v1, p0, LX/6Ib;->c:LX/6JB;

    goto :goto_0

    .line 1074195
    :cond_1
    const/4 v0, 0x7

    invoke-static {p0, v0}, LX/6Ib;->b$redex0(LX/6Ib;I)V

    .line 1074196
    iput-boolean v2, p0, LX/6Ib;->h:Z

    .line 1074197
    iput-boolean v2, p0, LX/6Ib;->i:Z

    .line 1074198
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074199
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/5fQ;->a(LX/6IY;)V

    .line 1074200
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074201
    new-instance v2, LX/6IW;

    invoke-direct {v2, p0, p1}, LX/6IW;-><init>(LX/6Ib;LX/6Ik;)V

    invoke-virtual {v0, v2}, LX/5fQ;->a(LX/5f5;)V

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1074202
    iget-boolean v0, p0, LX/6Ib;->h:Z

    return v0
.end method

.method public final d()LX/6JF;
    .locals 1

    .prologue
    .line 1074203
    iget-object v0, p0, LX/6Ib;->a:LX/6JF;

    return-object v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 1074204
    iget-boolean v0, p0, LX/6Ib;->j:Z

    if-nez v0, :cond_0

    .line 1074205
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not currently recording video"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1074206
    :cond_0
    const/16 v0, 0xb

    invoke-static {p0, v0}, LX/6Ib;->b$redex0(LX/6Ib;I)V

    .line 1074207
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074208
    new-instance v1, LX/6IT;

    invoke-direct {v1, p0}, LX/6IT;-><init>(LX/6Ib;)V

    new-instance v2, LX/6IU;

    invoke-direct {v2, p0}, LX/6IU;-><init>(LX/6Ib;)V

    iget-object v3, p0, LX/6Ib;->q:LX/5fi;

    invoke-virtual {v0, v1, v2, v3}, LX/5fQ;->a(LX/5f5;LX/5f5;LX/5fi;)V

    .line 1074209
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1074210
    iget-boolean v0, p0, LX/6Ib;->j:Z

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 1074211
    iget v0, p0, LX/6Ib;->k:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 1074212
    const/4 v0, 0x0

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 1074213
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074214
    invoke-virtual {v0}, LX/5fQ;->u()I

    move-result v0

    return v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1074215
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v0, v0

    .line 1074216
    invoke-virtual {v0}, LX/5fQ;->j()V

    .line 1074217
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Ib;->i:Z

    .line 1074218
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1074219
    iget-boolean v0, p0, LX/6Ib;->i:Z

    return v0
.end method
