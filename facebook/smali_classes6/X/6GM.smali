.class public final LX/6GM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;)V
    .locals 0

    .prologue
    .line 1070278
    iput-object p1, p0, LX/6GM;->a:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1070279
    sget-object v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->a:Ljava/lang/Class;

    const-string v1, "Unable to copy attachment for bug report."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1070280
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1070281
    check-cast p1, Landroid/net/Uri;

    .line 1070282
    iget-object v0, p0, LX/6GM;->a:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->m:LX/6FU;

    .line 1070283
    iget-object p0, v0, LX/6FU;->d:Ljava/util/List;

    if-nez p0, :cond_0

    .line 1070284
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object p0

    iput-object p0, v0, LX/6FU;->d:Ljava/util/List;

    .line 1070285
    :cond_0
    iget-object p0, v0, LX/6FU;->d:Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1070286
    return-void
.end method
