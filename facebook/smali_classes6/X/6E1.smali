.class public LX/6E1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6E0;


# instance fields
.field private final a:LX/6tM;


# direct methods
.method public constructor <init>(LX/6tM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1065788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065789
    iput-object p1, p0, LX/6E1;->a:LX/6tM;

    .line 1065790
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)Lcom/facebook/payments/confirmation/ConfirmationParams;
    .locals 1

    .prologue
    .line 1065787
    sget-object v0, LX/6uW;->MESSENGER_COMMERCE:LX/6uW;

    invoke-static {p1, p2, v0}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;LX/6uW;)Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;
    .locals 1

    .prologue
    .line 1065786
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;
    .locals 1

    .prologue
    .line 1065785
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    invoke-virtual {v0, p1, p2}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;
    .locals 1

    .prologue
    .line 1065776
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    invoke-virtual {v0, p1, p2}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;
    .locals 1

    .prologue
    .line 1065784
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;
    .locals 1

    .prologue
    .line 1065783
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    invoke-virtual {v0, p1, p2}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;
    .locals 1

    .prologue
    .line 1065782
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;
    .locals 1

    .prologue
    .line 1065781
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->d(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/model/ShippingParams;
    .locals 1

    .prologue
    .line 1065780
    sget-object v0, LX/72g;->SIMPLE:LX/72g;

    invoke-static {p1, v0}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/72g;)Lcom/facebook/payments/shipping/model/ShippingParams;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;
    .locals 2

    .prologue
    .line 1065779
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    sget-object v1, LX/72g;->SIMPLE:LX/72g;

    invoke-virtual {v0, p1, v1}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;LX/72g;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;
    .locals 2

    .prologue
    .line 1065778
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    sget-object v1, LX/71C;->MESSENGER_COMMERCE_SHIPPING_OPTION_PICKER:LX/71C;

    invoke-virtual {v0, p1, v1}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/71C;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final h(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;
    .locals 3

    .prologue
    .line 1065777
    iget-object v0, p0, LX/6E1;->a:LX/6tM;

    sget-object v1, LX/71C;->MESSENGER_COMMERCE:LX/71C;

    sget-object v2, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/71C;LX/0Px;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method
