.class public final LX/6Ta;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 1098530
    const/16 v24, 0x0

    .line 1098531
    const/16 v23, 0x0

    .line 1098532
    const/16 v22, 0x0

    .line 1098533
    const/16 v21, 0x0

    .line 1098534
    const/16 v20, 0x0

    .line 1098535
    const/16 v19, 0x0

    .line 1098536
    const/16 v18, 0x0

    .line 1098537
    const/16 v17, 0x0

    .line 1098538
    const/16 v16, 0x0

    .line 1098539
    const/4 v15, 0x0

    .line 1098540
    const/4 v14, 0x0

    .line 1098541
    const/4 v13, 0x0

    .line 1098542
    const/4 v12, 0x0

    .line 1098543
    const/4 v11, 0x0

    .line 1098544
    const/4 v10, 0x0

    .line 1098545
    const/4 v9, 0x0

    .line 1098546
    const/4 v8, 0x0

    .line 1098547
    const/4 v7, 0x0

    .line 1098548
    const/4 v6, 0x0

    .line 1098549
    const/4 v5, 0x0

    .line 1098550
    const/4 v4, 0x0

    .line 1098551
    const/4 v3, 0x0

    .line 1098552
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    .line 1098553
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1098554
    const/4 v3, 0x0

    .line 1098555
    :goto_0
    return v3

    .line 1098556
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1098557
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_f

    .line 1098558
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v25

    .line 1098559
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1098560
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    if-eqz v25, :cond_1

    .line 1098561
    const-string v26, "atom_size"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2

    .line 1098562
    const/4 v10, 0x1

    .line 1098563
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v24

    goto :goto_1

    .line 1098564
    :cond_2
    const-string v26, "bitrate"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 1098565
    const/4 v9, 0x1

    .line 1098566
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v23

    goto :goto_1

    .line 1098567
    :cond_3
    const-string v26, "broadcast_status"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 1098568
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v22

    goto :goto_1

    .line 1098569
    :cond_4
    const-string v26, "hdAtomSize"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 1098570
    const/4 v8, 0x1

    .line 1098571
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v21

    goto :goto_1

    .line 1098572
    :cond_5
    const-string v26, "hdBitrate"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 1098573
    const/4 v7, 0x1

    .line 1098574
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    goto :goto_1

    .line 1098575
    :cond_6
    const-string v26, "id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 1098576
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto/16 :goto_1

    .line 1098577
    :cond_7
    const-string v26, "is_live_streaming"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 1098578
    const/4 v6, 0x1

    .line 1098579
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto/16 :goto_1

    .line 1098580
    :cond_8
    const-string v26, "live_viewer_count"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 1098581
    const/4 v5, 0x1

    .line 1098582
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto/16 :goto_1

    .line 1098583
    :cond_9
    const-string v26, "live_viewer_count_read_only"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 1098584
    const/4 v4, 0x1

    .line 1098585
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 1098586
    :cond_a
    const-string v26, "playableUrlHdString"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 1098587
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 1098588
    :cond_b
    const-string v26, "playable_duration_in_ms"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_c

    .line 1098589
    const/4 v3, 0x1

    .line 1098590
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 1098591
    :cond_c
    const-string v26, "playable_url"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_d

    .line 1098592
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 1098593
    :cond_d
    const-string v26, "playlist"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_e

    .line 1098594
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1098595
    :cond_e
    const-string v26, "preferredPlayableUrlString"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 1098596
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 1098597
    :cond_f
    const/16 v25, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1098598
    if-eqz v10, :cond_10

    .line 1098599
    const/4 v10, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v10, v1, v2}, LX/186;->a(III)V

    .line 1098600
    :cond_10
    if-eqz v9, :cond_11

    .line 1098601
    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v9, v1, v10}, LX/186;->a(III)V

    .line 1098602
    :cond_11
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 1098603
    if-eqz v8, :cond_12

    .line 1098604
    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 1098605
    :cond_12
    if-eqz v7, :cond_13

    .line 1098606
    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1, v8}, LX/186;->a(III)V

    .line 1098607
    :cond_13
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1098608
    if-eqz v6, :cond_14

    .line 1098609
    const/4 v6, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1098610
    :cond_14
    if-eqz v5, :cond_15

    .line 1098611
    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 1098612
    :cond_15
    if-eqz v4, :cond_16

    .line 1098613
    const/16 v4, 0x8

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 1098614
    :cond_16
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1098615
    if-eqz v3, :cond_17

    .line 1098616
    const/16 v3, 0xa

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14, v4}, LX/186;->a(III)V

    .line 1098617
    :cond_17
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1098618
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1098619
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1098620
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
