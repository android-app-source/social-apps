.class public final LX/6Gw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/6H0;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;)V
    .locals 0

    .prologue
    .line 1071055
    iput-object p1, p0, LX/6Gw;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/6H0;)V
    .locals 1

    .prologue
    .line 1071056
    iget-object v0, p0, LX/6Gw;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    iget-object v0, v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1071057
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1071058
    iget-object v0, p0, LX/6Gw;->a:Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;

    const v1, 0x7f081930

    .line 1071059
    invoke-static {v0, v1}, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a$redex0(Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;I)V

    .line 1071060
    sget-object v0, Lcom/facebook/bugreporter/imagepicker/BugReporterImagePickerFragment;->a:Ljava/lang/String;

    const-string v1, "Unable to create a thumbnail"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1071061
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1071062
    check-cast p1, LX/6H0;

    invoke-direct {p0, p1}, LX/6Gw;->a(LX/6H0;)V

    return-void
.end method
