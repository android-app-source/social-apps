.class public final LX/5pm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/5pi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:LX/5pi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1008506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/5pi;)LX/5pm;
    .locals 2

    .prologue
    .line 1008507
    iget-object v0, p0, LX/5pm;->a:LX/5pi;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Setting native modules queue spec multiple times!"

    invoke-static {v0, v1}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1008508
    iput-object p1, p0, LX/5pm;->a:LX/5pi;

    .line 1008509
    return-object p0

    .line 1008510
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()LX/5pn;
    .locals 4

    .prologue
    .line 1008511
    new-instance v2, LX/5pn;

    iget-object v0, p0, LX/5pm;->a:LX/5pi;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pi;

    iget-object v1, p0, LX/5pm;->b:LX/5pi;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5pi;

    invoke-direct {v2, v0, v1}, LX/5pn;-><init>(LX/5pi;LX/5pi;)V

    return-object v2
.end method

.method public final b(LX/5pi;)LX/5pm;
    .locals 2

    .prologue
    .line 1008512
    iget-object v0, p0, LX/5pm;->b:LX/5pi;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Setting JS queue multiple times!"

    invoke-static {v0, v1}, LX/0nE;->a(ZLjava/lang/String;)V

    .line 1008513
    iput-object p1, p0, LX/5pm;->b:LX/5pi;

    .line 1008514
    return-object p0

    .line 1008515
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
