.class public final enum LX/5sP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5sP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5sP;

.field public static final enum CREATE:LX/5sP;

.field public static final enum DELETE:LX/5sP;

.field public static final enum UPDATE:LX/5sP;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1012835
    new-instance v0, LX/5sP;

    const-string v1, "CREATE"

    const-string v2, "create"

    invoke-direct {v0, v1, v3, v2}, LX/5sP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sP;->CREATE:LX/5sP;

    .line 1012836
    new-instance v0, LX/5sP;

    const-string v1, "UPDATE"

    const-string v2, "update"

    invoke-direct {v0, v1, v4, v2}, LX/5sP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sP;->UPDATE:LX/5sP;

    .line 1012837
    new-instance v0, LX/5sP;

    const-string v1, "DELETE"

    const-string v2, "delete"

    invoke-direct {v0, v1, v5, v2}, LX/5sP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/5sP;->DELETE:LX/5sP;

    .line 1012838
    const/4 v0, 0x3

    new-array v0, v0, [LX/5sP;

    sget-object v1, LX/5sP;->CREATE:LX/5sP;

    aput-object v1, v0, v3

    sget-object v1, LX/5sP;->UPDATE:LX/5sP;

    aput-object v1, v0, v4

    sget-object v1, LX/5sP;->DELETE:LX/5sP;

    aput-object v1, v0, v5

    sput-object v0, LX/5sP;->$VALUES:[LX/5sP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1012839
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1012840
    iput-object p3, p0, LX/5sP;->mName:Ljava/lang/String;

    .line 1012841
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5sP;
    .locals 1

    .prologue
    .line 1012842
    const-class v0, LX/5sP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5sP;

    return-object v0
.end method

.method public static values()[LX/5sP;
    .locals 1

    .prologue
    .line 1012843
    sget-object v0, LX/5sP;->$VALUES:[LX/5sP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5sP;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1012844
    iget-object v0, p0, LX/5sP;->mName:Ljava/lang/String;

    return-object v0
.end method
