.class public final LX/5Jh;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5Ji;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:LX/1dV;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 897517
    invoke-static {}, LX/5Ji;->q()LX/5Ji;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 897518
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5Jh;->g:Z

    .line 897519
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 897516
    const-string v0, "HorizontalScroll"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/5Ji;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 897520
    check-cast p1, LX/5Jh;

    .line 897521
    iget-object v0, p1, LX/5Jh;->b:LX/1dV;

    iput-object v0, p0, LX/5Jh;->b:LX/1dV;

    .line 897522
    iget-object v0, p1, LX/5Jh;->c:Ljava/lang/Integer;

    iput-object v0, p0, LX/5Jh;->c:Ljava/lang/Integer;

    .line 897523
    iget-object v0, p1, LX/5Jh;->d:Ljava/lang/Integer;

    iput-object v0, p0, LX/5Jh;->d:Ljava/lang/Integer;

    .line 897524
    iget-object v0, p1, LX/5Jh;->e:Ljava/lang/Integer;

    iput-object v0, p0, LX/5Jh;->e:Ljava/lang/Integer;

    .line 897525
    iget-object v0, p1, LX/5Jh;->f:Ljava/lang/Integer;

    iput-object v0, p0, LX/5Jh;->f:Ljava/lang/Integer;

    .line 897526
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 897503
    if-ne p0, p1, :cond_1

    .line 897504
    :cond_0
    :goto_0
    return v0

    .line 897505
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 897506
    goto :goto_0

    .line 897507
    :cond_3
    check-cast p1, LX/5Jh;

    .line 897508
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 897509
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 897510
    if-eq v2, v3, :cond_0

    .line 897511
    iget-object v2, p0, LX/5Jh;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/5Jh;->a:LX/1X1;

    iget-object v3, p1, LX/5Jh;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 897512
    goto :goto_0

    .line 897513
    :cond_5
    iget-object v2, p1, LX/5Jh;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 897514
    :cond_6
    iget-boolean v2, p0, LX/5Jh;->g:Z

    iget-boolean v3, p1, LX/5Jh;->g:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 897515
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 3

    .prologue
    .line 897493
    const/4 v2, 0x0

    .line 897494
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/5Jh;

    .line 897495
    iget-object v1, v0, LX/5Jh;->a:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/5Jh;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/5Jh;->a:LX/1X1;

    .line 897496
    iput-object v2, v0, LX/5Jh;->b:LX/1dV;

    .line 897497
    iput-object v2, v0, LX/5Jh;->c:Ljava/lang/Integer;

    .line 897498
    iput-object v2, v0, LX/5Jh;->d:Ljava/lang/Integer;

    .line 897499
    iput-object v2, v0, LX/5Jh;->e:Ljava/lang/Integer;

    .line 897500
    iput-object v2, v0, LX/5Jh;->f:Ljava/lang/Integer;

    .line 897501
    return-object v0

    :cond_0
    move-object v1, v2

    .line 897502
    goto :goto_0
.end method
