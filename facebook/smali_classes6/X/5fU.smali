.class public final LX/5fU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "Landroid/hardware/Camera$Size;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/optic/CameraPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/optic/CameraPreviewView;)V
    .locals 0

    .prologue
    .line 971668
    iput-object p1, p0, LX/5fU;->a:Lcom/facebook/optic/CameraPreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/hardware/Camera$Size;)V
    .locals 5

    .prologue
    .line 971669
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Started camera preview "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " x "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 971670
    iget-object v0, p0, LX/5fU;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v1, p0, LX/5fU;->a:Lcom/facebook/optic/CameraPreviewView;

    iget v1, v1, Lcom/facebook/optic/CameraPreviewView;->b:I

    iget-object v2, p0, LX/5fU;->a:Lcom/facebook/optic/CameraPreviewView;

    iget v2, v2, Lcom/facebook/optic/CameraPreviewView;->c:I

    iget v3, p1, Landroid/hardware/Camera$Size;->width:I

    iget v4, p1, Landroid/hardware/Camera$Size;->height:I

    .line 971671
    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/optic/CameraPreviewView;->a$redex0(Lcom/facebook/optic/CameraPreviewView;IIII)V

    .line 971672
    monitor-enter p0

    .line 971673
    :try_start_0
    iget-object v0, p0, LX/5fU;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->l:LX/5fb;

    if-eqz v0, :cond_0

    .line 971674
    iget-object v0, p0, LX/5fU;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->l:LX/5fb;

    invoke-interface {v0}, LX/5fb;->a()V

    .line 971675
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 971676
    monitor-enter p0

    .line 971677
    :try_start_0
    iget-object v0, p0, LX/5fU;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->l:LX/5fb;

    if-eqz v0, :cond_0

    .line 971678
    iget-object v0, p0, LX/5fU;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->l:LX/5fb;

    invoke-interface {v0, p1}, LX/5fb;->a(Ljava/lang/Exception;)V

    .line 971679
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 971680
    sget-object v0, Lcom/facebook/optic/CameraPreviewView;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 971681
    return-void

    .line 971682
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 971683
    check-cast p1, Landroid/hardware/Camera$Size;

    invoke-direct {p0, p1}, LX/5fU;->a(Landroid/hardware/Camera$Size;)V

    return-void
.end method
