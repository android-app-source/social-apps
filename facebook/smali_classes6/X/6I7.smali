.class public abstract LX/6I7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/03V;

.field public c:LX/6II;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1073135
    const-class v0, Lcom/facebook/camera/activity/CameraActivity;

    sput-object v0, LX/6I7;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 1

    .prologue
    .line 1073131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1073132
    sget-object v0, LX/6II;->READY:LX/6II;

    iput-object v0, p0, LX/6I7;->c:LX/6II;

    .line 1073133
    iput-object p1, p0, LX/6I7;->a:LX/03V;

    .line 1073134
    return-void
.end method

.method public static b(LX/6I7;LX/6II;)V
    .locals 2

    .prologue
    .line 1073125
    iget-object v0, p0, LX/6I7;->c:LX/6II;

    move-object v0, v0

    .line 1073126
    iput-object p1, p0, LX/6I7;->c:LX/6II;

    .line 1073127
    iget-object v1, p0, LX/6I7;->c:LX/6II;

    move-object v1, v1

    .line 1073128
    if-eq v0, v1, :cond_0

    .line 1073129
    invoke-virtual {p0, v1}, LX/6I7;->a(LX/6II;)V

    .line 1073130
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a(LX/6II;)V
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1073122
    sget-object v0, LX/6II;->READY:LX/6II;

    iput-object v0, p0, LX/6I7;->c:LX/6II;

    .line 1073123
    iget-object v0, p0, LX/6I7;->c:LX/6II;

    invoke-virtual {p0, v0}, LX/6I7;->a(LX/6II;)V

    .line 1073124
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1073120
    iget-object v0, p0, LX/6I7;->c:LX/6II;

    move-object v0, v0

    .line 1073121
    sget-object v1, LX/6II;->READY:LX/6II;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/6II;->QUEUED:LX/6II;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
