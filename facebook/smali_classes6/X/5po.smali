.class public LX/5po;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Zk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Zk",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:[Ljava/lang/Object;

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1008529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1008530
    const/4 v0, 0x0

    iput v0, p0, LX/5po;->b:I

    .line 1008531
    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, LX/5po;->a:[Ljava/lang/Object;

    .line 1008532
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1008539
    monitor-enter p0

    :try_start_0
    iget v1, p0, LX/5po;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1008540
    :goto_0
    monitor-exit p0

    return-object v0

    .line 1008541
    :cond_0
    :try_start_1
    iget v0, p0, LX/5po;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/5po;->b:I

    .line 1008542
    iget v1, p0, LX/5po;->b:I

    .line 1008543
    iget-object v0, p0, LX/5po;->a:[Ljava/lang/Object;

    aget-object v0, v0, v1

    .line 1008544
    iget-object v2, p0, LX/5po;->a:[Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v3, v2, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1008545
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 1008546
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/5po;->b:I

    iget-object v1, p0, LX/5po;->a:[Ljava/lang/Object;

    array-length v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 1008547
    const/4 v0, 0x0

    .line 1008548
    :goto_0
    monitor-exit p0

    return v0

    .line 1008549
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/5po;->a:[Ljava/lang/Object;

    iget v1, p0, LX/5po;->b:I

    aput-object p1, v0, v1

    .line 1008550
    iget v0, p0, LX/5po;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/5po;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1008551
    const/4 v0, 0x1

    goto :goto_0

    .line 1008552
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1008533
    monitor-enter p0

    :goto_0
    :try_start_0
    iget v1, p0, LX/5po;->b:I

    if-ge v0, v1, :cond_0

    .line 1008534
    iget-object v1, p0, LX/5po;->a:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 1008535
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1008536
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, LX/5po;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008537
    monitor-exit p0

    return-void

    .line 1008538
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
