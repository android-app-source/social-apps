.class public final enum LX/5SU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5SU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5SU;

.field public static final enum JAVAMETHOD:LX/5SU;

.field public static final enum JSARRAY:LX/5SU;

.field public static final enum JSBOOLEAN:LX/5SU;

.field public static final enum JSFUNCTION:LX/5SU;

.field public static final enum JSNULL:LX/5SU;

.field public static final enum JSNUMBER:LX/5SU;

.field public static final enum JSOBJECT:LX/5SU;

.field public static final enum JSSTRING:LX/5SU;

.field public static final enum JSUNDEFINED:LX/5SU;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 918324
    new-instance v0, LX/5SU;

    const-string v1, "JSNULL"

    invoke-direct {v0, v1, v3}, LX/5SU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SU;->JSNULL:LX/5SU;

    .line 918325
    new-instance v0, LX/5SU;

    const-string v1, "JSBOOLEAN"

    invoke-direct {v0, v1, v4}, LX/5SU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SU;->JSBOOLEAN:LX/5SU;

    .line 918326
    new-instance v0, LX/5SU;

    const-string v1, "JSNUMBER"

    invoke-direct {v0, v1, v5}, LX/5SU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SU;->JSNUMBER:LX/5SU;

    .line 918327
    new-instance v0, LX/5SU;

    const-string v1, "JSSTRING"

    invoke-direct {v0, v1, v6}, LX/5SU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SU;->JSSTRING:LX/5SU;

    .line 918328
    new-instance v0, LX/5SU;

    const-string v1, "JSOBJECT"

    invoke-direct {v0, v1, v7}, LX/5SU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SU;->JSOBJECT:LX/5SU;

    .line 918329
    new-instance v0, LX/5SU;

    const-string v1, "JSARRAY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5SU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SU;->JSARRAY:LX/5SU;

    .line 918330
    new-instance v0, LX/5SU;

    const-string v1, "JSUNDEFINED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/5SU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SU;->JSUNDEFINED:LX/5SU;

    .line 918331
    new-instance v0, LX/5SU;

    const-string v1, "JSFUNCTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/5SU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SU;->JSFUNCTION:LX/5SU;

    .line 918332
    new-instance v0, LX/5SU;

    const-string v1, "JAVAMETHOD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/5SU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5SU;->JAVAMETHOD:LX/5SU;

    .line 918333
    const/16 v0, 0x9

    new-array v0, v0, [LX/5SU;

    sget-object v1, LX/5SU;->JSNULL:LX/5SU;

    aput-object v1, v0, v3

    sget-object v1, LX/5SU;->JSBOOLEAN:LX/5SU;

    aput-object v1, v0, v4

    sget-object v1, LX/5SU;->JSNUMBER:LX/5SU;

    aput-object v1, v0, v5

    sget-object v1, LX/5SU;->JSSTRING:LX/5SU;

    aput-object v1, v0, v6

    sget-object v1, LX/5SU;->JSOBJECT:LX/5SU;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5SU;->JSARRAY:LX/5SU;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5SU;->JSUNDEFINED:LX/5SU;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5SU;->JSFUNCTION:LX/5SU;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/5SU;->JAVAMETHOD:LX/5SU;

    aput-object v2, v0, v1

    sput-object v0, LX/5SU;->$VALUES:[LX/5SU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 918323
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5SU;
    .locals 1

    .prologue
    .line 918315
    const-class v0, LX/5SU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5SU;

    return-object v0
.end method

.method public static values()[LX/5SU;
    .locals 1

    .prologue
    .line 918322
    sget-object v0, LX/5SU;->$VALUES:[LX/5SU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5SU;

    return-object v0
.end method


# virtual methods
.method public final isArray()Z
    .locals 1

    .prologue
    .line 918321
    sget-object v0, LX/5SU;->JSARRAY:LX/5SU;

    invoke-virtual {p0, v0}, LX/5SU;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isBoolean()Z
    .locals 1

    .prologue
    .line 918320
    sget-object v0, LX/5SU;->JSBOOLEAN:LX/5SU;

    invoke-virtual {p0, v0}, LX/5SU;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isFunction()Z
    .locals 1

    .prologue
    .line 918319
    sget-object v0, LX/5SU;->JSFUNCTION:LX/5SU;

    invoke-virtual {p0, v0}, LX/5SU;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/5SU;->JAVAMETHOD:LX/5SU;

    invoke-virtual {p0, v0}, LX/5SU;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isNull()Z
    .locals 1

    .prologue
    .line 918318
    sget-object v0, LX/5SU;->JSNULL:LX/5SU;

    invoke-virtual {p0, v0}, LX/5SU;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isNumber()Z
    .locals 1

    .prologue
    .line 918317
    sget-object v0, LX/5SU;->JSNUMBER:LX/5SU;

    invoke-virtual {p0, v0}, LX/5SU;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isObject()Z
    .locals 1

    .prologue
    .line 918316
    sget-object v0, LX/5SU;->JSOBJECT:LX/5SU;

    invoke-virtual {p0, v0}, LX/5SU;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isString()Z
    .locals 1

    .prologue
    .line 918314
    sget-object v0, LX/5SU;->JSSTRING:LX/5SU;

    invoke-virtual {p0, v0}, LX/5SU;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final isUndefined()Z
    .locals 1

    .prologue
    .line 918313
    sget-object v0, LX/5SU;->JSUNDEFINED:LX/5SU;

    invoke-virtual {p0, v0}, LX/5SU;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
