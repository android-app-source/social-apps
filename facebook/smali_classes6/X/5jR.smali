.class public final LX/5jR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 986815
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 986816
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 986817
    :goto_0
    return v1

    .line 986818
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 986819
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 986820
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 986821
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 986822
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 986823
    const-string v6, "face_recognition_model"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 986824
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 986825
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_2

    .line 986826
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_2

    .line 986827
    const/4 v6, 0x0

    .line 986828
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_c

    .line 986829
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 986830
    :goto_3
    move v5, v6

    .line 986831
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 986832
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 986833
    goto :goto_1

    .line 986834
    :cond_3
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 986835
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 986836
    :cond_4
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 986837
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 986838
    :cond_5
    const-string v6, "packaged_file"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 986839
    const/4 v5, 0x0

    .line 986840
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_11

    .line 986841
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 986842
    :goto_4
    move v0, v5

    .line 986843
    goto/16 :goto_1

    .line 986844
    :cond_6
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 986845
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 986846
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 986847
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 986848
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 986849
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 986850
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 986851
    :cond_9
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 986852
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 986853
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 986854
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_9

    if-eqz v8, :cond_9

    .line 986855
    const-string v9, "filename"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 986856
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_5

    .line 986857
    :cond_a
    const-string v9, "uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 986858
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_5

    .line 986859
    :cond_b
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 986860
    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 986861
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 986862
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_c
    move v5, v6

    move v7, v6

    goto :goto_5

    .line 986863
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 986864
    :cond_e
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_10

    .line 986865
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 986866
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 986867
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_e

    if-eqz v7, :cond_e

    .line 986868
    const-string v8, "filename"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 986869
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_6

    .line 986870
    :cond_f
    const-string v8, "uri"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 986871
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_6

    .line 986872
    :cond_10
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 986873
    invoke-virtual {p1, v5, v6}, LX/186;->b(II)V

    .line 986874
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 986875
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_4

    :cond_11
    move v0, v5

    move v6, v5

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 986876
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 986877
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 986878
    if-eqz v0, :cond_3

    .line 986879
    const-string v1, "face_recognition_model"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 986880
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 986881
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 986882
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 986883
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 986884
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 986885
    if-eqz v3, :cond_0

    .line 986886
    const-string p3, "filename"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 986887
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 986888
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 986889
    if-eqz v3, :cond_1

    .line 986890
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 986891
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 986892
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 986893
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 986894
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 986895
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 986896
    if-eqz v0, :cond_4

    .line 986897
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 986898
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 986899
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 986900
    if-eqz v0, :cond_5

    .line 986901
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 986902
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 986903
    :cond_5
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 986904
    if-eqz v0, :cond_8

    .line 986905
    const-string v1, "packaged_file"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 986906
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 986907
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 986908
    if-eqz v1, :cond_6

    .line 986909
    const-string v2, "filename"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 986910
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 986911
    :cond_6
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 986912
    if-eqz v1, :cond_7

    .line 986913
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 986914
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 986915
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 986916
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 986917
    return-void
.end method
