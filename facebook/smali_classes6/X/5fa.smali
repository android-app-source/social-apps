.class public final LX/5fa;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/optic/CameraPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/optic/CameraPreviewView;)V
    .locals 0

    .prologue
    .line 971729
    iput-object p1, p0, LX/5fa;->a:Lcom/facebook/optic/CameraPreviewView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/optic/CameraPreviewView;B)V
    .locals 0

    .prologue
    .line 971730
    invoke-direct {p0, p1}, LX/5fa;-><init>(Lcom/facebook/optic/CameraPreviewView;)V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 971731
    const/4 v0, 0x1

    return v0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 971732
    const/4 v0, 0x1

    return v0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 971733
    sget-object v0, LX/5fQ;->b:LX/5fQ;

    move-object v3, v0

    .line 971734
    invoke-virtual {v3}, LX/5fQ;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 971735
    iget-boolean v0, v3, LX/5fQ;->m:Z

    move v0, v0

    .line 971736
    if-eqz v0, :cond_1

    .line 971737
    :cond_0
    :goto_0
    return v2

    .line 971738
    :cond_1
    iget-object v0, p0, LX/5fa;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-boolean v0, v0, Lcom/facebook/optic/CameraPreviewView;->r:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/5fa;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-boolean v0, v0, Lcom/facebook/optic/CameraPreviewView;->s:Z

    if-eqz v0, :cond_0

    .line 971739
    :cond_2
    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->e()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 971740
    :cond_3
    iget-object v0, p0, LX/5fa;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->q:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 971741
    const/4 v0, 0x2

    new-array v4, v0, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    aput v0, v4, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    aput v0, v4, v1

    .line 971742
    iget-object v0, p0, LX/5fa;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-object v0, v0, Lcom/facebook/optic/CameraPreviewView;->q:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 971743
    iget-object v0, p0, LX/5fa;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-boolean v0, v0, Lcom/facebook/optic/CameraPreviewView;->r:Z

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 971744
    aget v0, v4, v2

    float-to-int v0, v0

    aget v5, v4, v1

    float-to-int v5, v5

    invoke-virtual {v3, v0, v5}, LX/5fQ;->b(II)V

    move v0, v1

    .line 971745
    :goto_1
    iget-object v5, p0, LX/5fa;->a:Lcom/facebook/optic/CameraPreviewView;

    iget-boolean v5, v5, Lcom/facebook/optic/CameraPreviewView;->s:Z

    if-eqz v5, :cond_4

    invoke-static {}, Lcom/facebook/optic/CameraPreviewView;->f()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 971746
    aget v0, v4, v2

    float-to-int v0, v0

    aget v2, v4, v1

    float-to-int v2, v2

    invoke-virtual {v3, v0, v2}, LX/5fQ;->a(II)V

    move v0, v1

    :cond_4
    move v2, v0

    .line 971747
    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1
.end method
