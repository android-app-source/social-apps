.class public final LX/5Ll;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 902756
    const/16 v18, 0x0

    .line 902757
    const/16 v17, 0x0

    .line 902758
    const/16 v16, 0x0

    .line 902759
    const/4 v15, 0x0

    .line 902760
    const/4 v14, 0x0

    .line 902761
    const/4 v13, 0x0

    .line 902762
    const/4 v12, 0x0

    .line 902763
    const/4 v11, 0x0

    .line 902764
    const/4 v10, 0x0

    .line 902765
    const/4 v9, 0x0

    .line 902766
    const/4 v8, 0x0

    .line 902767
    const/4 v7, 0x0

    .line 902768
    const/4 v6, 0x0

    .line 902769
    const/4 v5, 0x0

    .line 902770
    const/4 v4, 0x0

    .line 902771
    const/4 v3, 0x0

    .line 902772
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 902773
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 902774
    const/4 v3, 0x0

    .line 902775
    :goto_0
    return v3

    .line 902776
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 902777
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_c

    .line 902778
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 902779
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 902780
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 902781
    const-string v20, "glyph"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 902782
    invoke-static/range {p0 .. p1}, LX/5Lj;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 902783
    :cond_2
    const-string v20, "iconImageLarge"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 902784
    invoke-static/range {p0 .. p1}, LX/5Lk;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 902785
    :cond_3
    const-string v20, "id"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 902786
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto :goto_1

    .line 902787
    :cond_4
    const-string v20, "is_linking_verb"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 902788
    const/4 v7, 0x1

    .line 902789
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto :goto_1

    .line 902790
    :cond_5
    const-string v20, "legacy_api_id"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 902791
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 902792
    :cond_6
    const-string v20, "prefetch_priority"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 902793
    const/4 v6, 0x1

    .line 902794
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto :goto_1

    .line 902795
    :cond_7
    const-string v20, "present_participle"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 902796
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 902797
    :cond_8
    const-string v20, "prompt"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 902798
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 902799
    :cond_9
    const-string v20, "supports_audio_suggestions"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 902800
    const/4 v5, 0x1

    .line 902801
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 902802
    :cond_a
    const-string v20, "supports_freeform"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 902803
    const/4 v4, 0x1

    .line 902804
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 902805
    :cond_b
    const-string v20, "supports_offline_posting"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 902806
    const/4 v3, 0x1

    .line 902807
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 902808
    :cond_c
    const/16 v19, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 902809
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 902810
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 902811
    const/16 v17, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 902812
    if-eqz v7, :cond_d

    .line 902813
    const/4 v7, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v15}, LX/186;->a(IZ)V

    .line 902814
    :cond_d
    const/4 v7, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->b(II)V

    .line 902815
    if-eqz v6, :cond_e

    .line 902816
    const/4 v6, 0x5

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13, v7}, LX/186;->a(III)V

    .line 902817
    :cond_e
    const/4 v6, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 902818
    const/4 v6, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11}, LX/186;->b(II)V

    .line 902819
    if-eqz v5, :cond_f

    .line 902820
    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10}, LX/186;->a(IZ)V

    .line 902821
    :cond_f
    if-eqz v4, :cond_10

    .line 902822
    const/16 v4, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->a(IZ)V

    .line 902823
    :cond_10
    if-eqz v3, :cond_11

    .line 902824
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 902825
    :cond_11
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
