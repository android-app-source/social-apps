.class public final LX/6dm;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1116368
    new-instance v0, LX/0U1;

    const-string v1, "thread_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->a:LX/0U1;

    .line 1116369
    new-instance v0, LX/0U1;

    const-string v1, "user_key"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->b:LX/0U1;

    .line 1116370
    new-instance v0, LX/0U1;

    const-string v1, "email"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->c:LX/0U1;

    .line 1116371
    new-instance v0, LX/0U1;

    const-string v1, "phone"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->d:LX/0U1;

    .line 1116372
    new-instance v0, LX/0U1;

    const-string v1, "sms_participant_fbid"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->e:LX/0U1;

    .line 1116373
    new-instance v0, LX/0U1;

    const-string v1, "type"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->f:LX/0U1;

    .line 1116374
    new-instance v0, LX/0U1;

    const-string v1, "last_read_receipt_time"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->g:LX/0U1;

    .line 1116375
    new-instance v0, LX/0U1;

    const-string v1, "last_read_receipt_watermark_time"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->h:LX/0U1;

    .line 1116376
    new-instance v0, LX/0U1;

    const-string v1, "last_delivered_receipt_time"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->i:LX/0U1;

    .line 1116377
    new-instance v0, LX/0U1;

    const-string v1, "last_delivered_receipt_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->j:LX/0U1;

    .line 1116378
    new-instance v0, LX/0U1;

    const-string v1, "is_admin"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->k:LX/0U1;

    .line 1116379
    new-instance v0, LX/0U1;

    const-string v1, "request_timestamp_ms"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/6dm;->l:LX/0U1;

    return-void
.end method
