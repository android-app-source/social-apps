.class public LX/6Bn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile d:LX/6Bn;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/0So;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1063110
    const-class v0, LX/6Bn;

    sput-object v0, LX/6Bn;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1063111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1063112
    iput-object p1, p0, LX/6Bn;->b:LX/0Zb;

    .line 1063113
    iput-object p2, p0, LX/6Bn;->c:LX/0So;

    .line 1063114
    return-void
.end method

.method public static a(LX/0QB;)LX/6Bn;
    .locals 5

    .prologue
    .line 1063115
    sget-object v0, LX/6Bn;->d:LX/6Bn;

    if-nez v0, :cond_1

    .line 1063116
    const-class v1, LX/6Bn;

    monitor-enter v1

    .line 1063117
    :try_start_0
    sget-object v0, LX/6Bn;->d:LX/6Bn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1063118
    if-eqz v2, :cond_0

    .line 1063119
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1063120
    new-instance p0, LX/6Bn;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {p0, v3, v4}, LX/6Bn;-><init>(LX/0Zb;LX/0So;)V

    .line 1063121
    move-object v0, p0

    .line 1063122
    sput-object v0, LX/6Bn;->d:LX/6Bn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1063123
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1063124
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1063125
    :cond_1
    sget-object v0, LX/6Bn;->d:LX/6Bn;

    return-object v0

    .line 1063126
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1063127
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/6Bn;Lcom/facebook/assetdownload/AssetDownloadConfiguration;JZ)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 1063128
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "assetdownload_download"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1063129
    const-string v1, "identifier"

    .line 1063130
    iget-object v2, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mIdentifier:Ljava/lang/String;

    move-object v2, v2

    .line 1063131
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063132
    const-string v1, "analytics_tag"

    .line 1063133
    iget-object v2, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mAnalyticsTag:Ljava/lang/String;

    move-object v2, v2

    .line 1063134
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063135
    const-string v1, "storage"

    .line 1063136
    iget-object v2, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mStorageConstraint:LX/6BV;

    move-object v2, v2

    .line 1063137
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063138
    const-string v1, "connection"

    .line 1063139
    iget-object v2, p1, Lcom/facebook/assetdownload/AssetDownloadConfiguration;->mConnectionConstraint:LX/6BU;

    move-object v2, v2

    .line 1063140
    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063141
    const-string v1, "time_elapsed"

    iget-object v2, p0, LX/6Bn;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    sub-long/2addr v2, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063142
    const-string v1, "wifi_available"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063143
    return-object v0
.end method


# virtual methods
.method public final a(JLjava/util/HashSet;IIJZ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/HashSet",
            "<",
            "LX/6BZ;",
            ">;IIJZ)V"
        }
    .end annotation

    .prologue
    .line 1063144
    iget-object v0, p0, LX/6Bn;->b:LX/0Zb;

    const-string v1, "assetdownload_session"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1063145
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1063146
    const-string v0, "time_elapsed"

    iget-object v2, p0, LX/6Bn;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    sub-long/2addr v2, p1

    invoke-virtual {v1, v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1063147
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1063148
    :try_start_0
    invoke-virtual {p3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6BZ;

    .line 1063149
    invoke-interface {v0}, LX/6BZ;->a()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1063150
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 1063151
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1063152
    :cond_1
    invoke-interface {v0}, LX/6BZ;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1063153
    :catch_0
    :cond_2
    const-string v0, "failing_eligibility_callbacks"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1063154
    const-string v0, "total_assets_processed"

    invoke-virtual {v1, v0, p4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1063155
    const-string v0, "total_assets_downloaded"

    invoke-virtual {v1, v0, p5}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1063156
    const-string v0, "total_content_bytes"

    invoke-virtual {v1, v0, p6, p7}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1063157
    const-string v0, "wifi_available"

    invoke-virtual {v1, v0, p8}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1063158
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1063159
    :cond_3
    return-void
.end method

.method public final a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;JJZ)V
    .locals 4

    .prologue
    .line 1063160
    invoke-static {p0, p1, p2, p3, p6}, LX/6Bn;->a(LX/6Bn;Lcom/facebook/assetdownload/AssetDownloadConfiguration;JZ)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1063161
    const-string v1, "download_status"

    const-string v2, "success"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063162
    const-string v1, "content_bytes"

    invoke-virtual {v0, v1, p4, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063163
    iget-object v1, p0, LX/6Bn;->b:LX/0Zb;

    const/16 v2, 0x14

    invoke-interface {v1, v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 1063164
    return-void
.end method

.method public final a(Lcom/facebook/assetdownload/AssetDownloadConfiguration;JLjava/lang/Exception;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1063165
    invoke-static {p0, p1, p2, p3, p5}, LX/6Bn;->a(LX/6Bn;Lcom/facebook/assetdownload/AssetDownloadConfiguration;JZ)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1063166
    const-string v0, "download_status"

    const-string v3, "failure"

    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063167
    const-string v3, "exception"

    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063168
    const-string v0, "exception_class"

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {v2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1063169
    iget-object v0, p0, LX/6Bn;->b:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1063170
    return-void

    :cond_1
    move-object v0, v1

    .line 1063171
    goto :goto_0
.end method
