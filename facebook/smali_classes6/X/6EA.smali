.class public LX/6EA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6CR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6CR",
        "<",
        "Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1065882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1065883
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1065884
    const-string v0, "getUserID"

    return-object v0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 4

    .prologue
    .line 1065885
    check-cast p1, Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;

    .line 1065886
    const-string v0, "JS_BRIDGE_ASID"

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 1065887
    const-string v0, "JS_BRIDGE_PSID"

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 1065888
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1065889
    :goto_0
    if-eqz v0, :cond_1

    .line 1065890
    invoke-virtual {p1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v0

    .line 1065891
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1065892
    const-string p0, "callbackID"

    invoke-virtual {v3, p0, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065893
    const-string p0, "asid"

    invoke-virtual {v3, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065894
    const-string p0, "psid"

    invoke-virtual {v3, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065895
    move-object v0, v3

    .line 1065896
    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    .line 1065897
    :goto_1
    return-void

    .line 1065898
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1065899
    :cond_1
    sget-object v0, LX/6Cy;->BROWSER_EXTENSION_FAILED_TO_GET_USERID:LX/6Cy;

    invoke-virtual {v0}, LX/6Cy;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    goto :goto_1
.end method
