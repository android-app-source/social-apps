.class public final LX/6So;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1096738
    const/4 v9, 0x0

    .line 1096739
    const/4 v8, 0x0

    .line 1096740
    const/4 v7, 0x0

    .line 1096741
    const/4 v6, 0x0

    .line 1096742
    const/4 v5, 0x0

    .line 1096743
    const/4 v4, 0x0

    .line 1096744
    const/4 v3, 0x0

    .line 1096745
    const/4 v2, 0x0

    .line 1096746
    const/4 v1, 0x0

    .line 1096747
    const/4 v0, 0x0

    .line 1096748
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1096749
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096750
    const/4 v0, 0x0

    .line 1096751
    :goto_0
    return v0

    .line 1096752
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096753
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 1096754
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1096755
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1096756
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1096757
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1096758
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_1

    .line 1096759
    :cond_3
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1096760
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1096761
    :cond_4
    const-string v11, "is_profile_eligible_for_live_with"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1096762
    const/4 v2, 0x1

    .line 1096763
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 1096764
    :cond_5
    const-string v11, "is_verified"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1096765
    const/4 v1, 0x1

    .line 1096766
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 1096767
    :cond_6
    const-string v11, "is_viewer_friend"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1096768
    const/4 v0, 0x1

    .line 1096769
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    goto :goto_1

    .line 1096770
    :cond_7
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1096771
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1096772
    :cond_8
    const-string v11, "profile_picture"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1096773
    const/4 v10, 0x0

    .line 1096774
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v11, :cond_10

    .line 1096775
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096776
    :goto_2
    move v3, v10

    .line 1096777
    goto/16 :goto_1

    .line 1096778
    :cond_9
    const/4 v10, 0x7

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1096779
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1096780
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1096781
    if-eqz v2, :cond_a

    .line 1096782
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->a(IZ)V

    .line 1096783
    :cond_a
    if-eqz v1, :cond_b

    .line 1096784
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 1096785
    :cond_b
    if-eqz v0, :cond_c

    .line 1096786
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1096787
    :cond_c
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1096788
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1096789
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1096790
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1096791
    :cond_e
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_f

    .line 1096792
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1096793
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1096794
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_e

    if-eqz v11, :cond_e

    .line 1096795
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 1096796
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    .line 1096797
    :cond_f
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1096798
    invoke-virtual {p1, v10, v3}, LX/186;->b(II)V

    .line 1096799
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_2

    :cond_10
    move v3, v10

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1096800
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1096801
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1096802
    if-eqz v0, :cond_0

    .line 1096803
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096804
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1096805
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096806
    if-eqz v0, :cond_1

    .line 1096807
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096808
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096809
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096810
    if-eqz v0, :cond_2

    .line 1096811
    const-string v1, "is_profile_eligible_for_live_with"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096812
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096813
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096814
    if-eqz v0, :cond_3

    .line 1096815
    const-string v1, "is_verified"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096816
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096817
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096818
    if-eqz v0, :cond_4

    .line 1096819
    const-string v1, "is_viewer_friend"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096820
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096821
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096822
    if-eqz v0, :cond_5

    .line 1096823
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096824
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096825
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096826
    if-eqz v0, :cond_7

    .line 1096827
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096828
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1096829
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1096830
    if-eqz v1, :cond_6

    .line 1096831
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096832
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096833
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1096834
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1096835
    return-void
.end method
