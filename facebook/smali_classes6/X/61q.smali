.class public LX/61q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/61q;


# instance fields
.field private final a:LX/0W3;

.field private volatile b:I


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1040732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1040733
    const/4 v0, -0x1

    iput v0, p0, LX/61q;->b:I

    .line 1040734
    iput-object p1, p0, LX/61q;->a:LX/0W3;

    .line 1040735
    return-void
.end method

.method public static a(LX/0QB;)LX/61q;
    .locals 3

    .prologue
    .line 1040722
    sget-object v0, LX/61q;->c:LX/61q;

    if-nez v0, :cond_1

    .line 1040723
    const-class v1, LX/61q;

    monitor-enter v1

    .line 1040724
    :try_start_0
    sget-object v0, LX/61q;->c:LX/61q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1040725
    if-eqz v2, :cond_0

    .line 1040726
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/61q;->b(LX/0QB;)LX/61q;

    move-result-object v0

    sput-object v0, LX/61q;->c:LX/61q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1040727
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1040728
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1040729
    :cond_1
    sget-object v0, LX/61q;->c:LX/61q;

    return-object v0

    .line 1040730
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1040731
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/61q;
    .locals 2

    .prologue
    .line 1040720
    new-instance v1, LX/61q;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v0

    check-cast v0, LX/0W3;

    invoke-direct {v1, v0}, LX/61q;-><init>(LX/0W3;)V

    .line 1040721
    return-object v1
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    .line 1040717
    iget v0, p0, LX/61q;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1040718
    iget-object v0, p0, LX/61q;->a:LX/0W3;

    sget-wide v2, LX/0X5;->ii:J

    const/16 v1, 0x3c

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/61q;->b:I

    .line 1040719
    :cond_0
    iget v0, p0, LX/61q;->b:I

    return v0
.end method
