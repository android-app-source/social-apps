.class public final LX/6Jz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6JU;


# instance fields
.field public final synthetic a:LX/6K6;


# direct methods
.method public constructor <init>(LX/6K6;)V
    .locals 0

    .prologue
    .line 1076128
    iput-object p1, p0, LX/6Jz;->a:LX/6K6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1076110
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    const/4 v1, 0x0

    .line 1076111
    iput-boolean v1, v0, LX/6K6;->j:Z

    .line 1076112
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->m:LX/6Jc;

    new-instance v1, LX/6Jy;

    invoke-direct {v1, p0}, LX/6Jy;-><init>(LX/6Jz;)V

    iget-object v2, p0, LX/6Jz;->a:LX/6K6;

    iget-object v2, v2, LX/6K6;->b:Landroid/os/Handler;

    .line 1076113
    new-instance v3, LX/6Jb;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, LX/6Jb;-><init>(I)V

    .line 1076114
    iget-object v4, v0, LX/6Jc;->h:LX/6LC;

    if-nez v4, :cond_0

    .line 1076115
    invoke-virtual {v3}, LX/6Jb;->b()I

    .line 1076116
    :cond_0
    iget-object v4, v0, LX/6Jc;->g:LX/6L1;

    if-nez v4, :cond_1

    .line 1076117
    invoke-virtual {v3}, LX/6Jb;->b()I

    .line 1076118
    :cond_1
    iget v4, v3, LX/6Jb;->a:I

    move v4, v4

    .line 1076119
    if-nez v4, :cond_3

    .line 1076120
    invoke-static {v0, v1, v2}, LX/6Jc;->d(LX/6Jc;LX/6JU;Landroid/os/Handler;)V

    .line 1076121
    :cond_2
    :goto_0
    return-void

    .line 1076122
    :cond_3
    iget-object v4, v0, LX/6Jc;->h:LX/6LC;

    if-eqz v4, :cond_4

    .line 1076123
    iget-object v4, v0, LX/6Jc;->h:LX/6LC;

    new-instance v5, LX/6JY;

    invoke-direct {v5, v0, v3, v1, v2}, LX/6JY;-><init>(LX/6Jc;LX/6Jb;LX/6JU;Landroid/os/Handler;)V

    iget-object p0, v0, LX/6Jc;->e:Landroid/os/Handler;

    invoke-interface {v4, v5, p0}, LX/6LC;->c(LX/6JU;Landroid/os/Handler;)V

    .line 1076124
    :cond_4
    iget-object v4, v0, LX/6Jc;->g:LX/6L1;

    if-eqz v4, :cond_2

    .line 1076125
    iget-object v4, v0, LX/6Jc;->g:LX/6L1;

    new-instance v5, LX/6JZ;

    invoke-direct {v5, v0, v3, v1, v2}, LX/6JZ;-><init>(LX/6Jc;LX/6Jb;LX/6JU;Landroid/os/Handler;)V

    iget-object v3, v0, LX/6Jc;->e:Landroid/os/Handler;

    .line 1076126
    iget-object p0, v4, LX/6L1;->b:Landroid/os/Handler;

    new-instance v0, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$3;

    invoke-direct {v0, v4, v5, v3}, Lcom/facebook/cameracore/mediapipeline/recorder/AudioEncoderImpl$3;-><init>(LX/6L1;LX/6JU;Landroid/os/Handler;)V

    const v1, 0x2fb0659d

    invoke-static {p0, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1076127
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1076098
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    sget-object v1, LX/6K9;->STOPPED:LX/6K9;

    .line 1076099
    iput-object v1, v0, LX/6K6;->q:LX/6K9;

    .line 1076100
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    .line 1076101
    iput-boolean v3, v0, LX/6K6;->j:Z

    .line 1076102
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->p:LX/6JG;

    if-eqz v0, :cond_0

    .line 1076103
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->p:LX/6JG;

    new-instance v1, LX/6JJ;

    const-string v2, "Failed to stop audio recorder"

    invoke-direct {v1, v2, p1}, LX/6JJ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, LX/6JG;->a(LX/6JJ;)V

    .line 1076104
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    const/4 v1, 0x0

    .line 1076105
    iput-object v1, v0, LX/6K6;->p:LX/6JG;

    .line 1076106
    :cond_0
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, LX/6Jt;->c(I)V

    .line 1076107
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    iget-object v0, v0, LX/6K6;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Jt;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, LX/6Jt;->d(I)V

    .line 1076108
    iget-object v0, p0, LX/6Jz;->a:LX/6K6;

    invoke-static {v0, v3}, LX/6K6;->a(LX/6K6;Z)V

    .line 1076109
    return-void
.end method
