.class public final LX/5Cm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 869448
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 869449
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 869450
    :goto_0
    return v1

    .line 869451
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 869452
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 869453
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 869454
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 869455
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 869456
    const-string v4, "feedback_reaction_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 869457
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 869458
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_9

    .line 869459
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 869460
    :goto_2
    move v2, v3

    .line 869461
    goto :goto_1

    .line 869462
    :cond_2
    const-string v4, "node"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 869463
    invoke-static {p0, p1}, LX/5Cl;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 869464
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 869465
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 869466
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 869467
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 869468
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 869469
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 869470
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 869471
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_5

    if-eqz v6, :cond_5

    .line 869472
    const-string v7, "key"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 869473
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v5, v2

    move v2, v4

    goto :goto_3

    .line 869474
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 869475
    :cond_7
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 869476
    if-eqz v2, :cond_8

    .line 869477
    invoke-virtual {p1, v3, v5, v3}, LX/186;->a(III)V

    .line 869478
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v2, v3

    move v5, v3

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 869479
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 869480
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 869481
    if-eqz v0, :cond_1

    .line 869482
    const-string v1, "feedback_reaction_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869483
    const/4 v1, 0x0

    .line 869484
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 869485
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 869486
    if-eqz v1, :cond_0

    .line 869487
    const-string v2, "key"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869488
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 869489
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 869490
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 869491
    if-eqz v0, :cond_2

    .line 869492
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 869493
    invoke-static {p0, v0, p2, p3}, LX/5Cl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 869494
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 869495
    return-void
.end method
