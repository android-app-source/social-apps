.class public LX/6Zn;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Lcom/facebook/android/maps/MapView;

.field public d:LX/7au;

.field public e:LX/6al;

.field public final f:LX/681;

.field public g:LX/68M;

.field public h:LX/3BT;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1111664
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1111665
    const/4 v0, 0x2

    iput v0, p0, LX/6Zn;->a:I

    .line 1111666
    const/4 v0, 0x0

    iput v0, p0, LX/6Zn;->b:I

    .line 1111667
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Zn;->f:LX/681;

    .line 1111668
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/681;)V
    .locals 1

    .prologue
    .line 1111659
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1111660
    const/4 v0, 0x2

    iput v0, p0, LX/6Zn;->a:I

    .line 1111661
    const/4 v0, 0x0

    iput v0, p0, LX/6Zn;->b:I

    .line 1111662
    iput-object p2, p0, LX/6Zn;->f:LX/681;

    .line 1111663
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1111650
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1111651
    const/4 v0, 0x2

    iput v0, p0, LX/6Zn;->a:I

    .line 1111652
    const/4 v0, 0x0

    iput v0, p0, LX/6Zn;->b:I

    .line 1111653
    invoke-static {p1, p2}, LX/681;->a(Landroid/content/Context;Landroid/util/AttributeSet;)LX/681;

    move-result-object v0

    iput-object v0, p0, LX/6Zn;->f:LX/681;

    .line 1111654
    invoke-static {p2}, LX/6as;->b(Landroid/util/AttributeSet;)Ljava/lang/Integer;

    move-result-object v0

    .line 1111655
    if-eqz v0, :cond_0

    .line 1111656
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/6Zn;->b:I

    .line 1111657
    :cond_0
    invoke-static {p2}, LX/6as;->a(Landroid/util/AttributeSet;)I

    move-result v0

    iput v0, p0, LX/6Zn;->a:I

    .line 1111658
    return-void
.end method


# virtual methods
.method public a(LX/680;)LX/68M;
    .locals 3

    .prologue
    .line 1111649
    new-instance v0, LX/68M;

    invoke-virtual {p0}, LX/6Zn;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, LX/6Zn;->b:I

    invoke-direct {v0, v1, p1, v2}, LX/68M;-><init>(Landroid/content/Context;LX/680;I)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1111646
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-nez v0, :cond_0

    .line 1111647
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    invoke-virtual {v0}, LX/7au;->f()V

    .line 1111648
    :cond_0
    return-void
.end method

.method public a(LX/6Zz;)V
    .locals 2

    .prologue
    .line 1111585
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_1

    .line 1111586
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    new-instance v1, LX/6at;

    invoke-direct {v1, p0, p1}, LX/6at;-><init>(LX/6Zn;LX/6Zz;)V

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 1111587
    :cond_0
    :goto_0
    return-void

    .line 1111588
    :cond_1
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    if-eqz v0, :cond_0

    .line 1111589
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    new-instance v1, LX/6au;

    invoke-direct {v1, p0, p1}, LX/6au;-><init>(LX/6Zn;LX/6Zz;)V

    invoke-virtual {v0, v1}, LX/7au;->a(LX/6an;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 1111622
    iget v0, p0, LX/6Zn;->a:I

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 1111623
    const-string v0, "state_map_source"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/6Zn;->a:I

    .line 1111624
    :cond_0
    const/4 v0, 0x0

    .line 1111625
    iget-object v1, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-nez v1, :cond_1

    iget-object v1, p0, LX/6Zn;->d:LX/7au;

    if-nez v1, :cond_1

    .line 1111626
    iget v0, p0, LX/6Zn;->a:I

    if-nez v0, :cond_6

    .line 1111627
    iget-object v0, p0, LX/6Zn;->f:LX/681;

    if-eqz v0, :cond_5

    .line 1111628
    new-instance v0, Lcom/facebook/android/maps/MapView;

    invoke-virtual {p0}, LX/6Zn;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/6Zn;->f:LX/681;

    invoke-direct {v0, v1, v2}, Lcom/facebook/android/maps/MapView;-><init>(Landroid/content/Context;LX/681;)V

    iput-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    .line 1111629
    :goto_0
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    .line 1111630
    :goto_1
    move-object v0, v0

    .line 1111631
    :cond_1
    iget-object v1, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-eqz v1, :cond_3

    .line 1111632
    iget-object v1, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v1, p1}, Lcom/facebook/android/maps/MapView;->a(Landroid/os/Bundle;)V

    .line 1111633
    iget-object v1, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    new-instance v2, LX/6av;

    invoke-direct {v2, p0}, LX/6av;-><init>(LX/6Zn;)V

    invoke-virtual {v1, v2}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 1111634
    :goto_2
    if-eqz v0, :cond_2

    .line 1111635
    invoke-virtual {p0, v0}, LX/6Zn;->addView(Landroid/view/View;)V

    .line 1111636
    :cond_2
    return-void

    .line 1111637
    :cond_3
    iget-object v1, p0, LX/6Zn;->d:LX/7au;

    if-eqz v1, :cond_4

    .line 1111638
    iget-object v1, p0, LX/6Zn;->d:LX/7au;

    invoke-virtual {v1, p1}, LX/7au;->a(Landroid/os/Bundle;)V

    .line 1111639
    invoke-virtual {p0}, LX/6Zn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/7av;->a(Landroid/content/Context;)I

    goto :goto_2

    .line 1111640
    :cond_4
    new-instance v0, LX/6aw;

    const-string v1, "You MUST set a MapLibrarySelector on the MapViewDelegate before the MapView is created!"

    invoke-direct {v0, v1}, LX/6aw;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1111641
    :cond_5
    new-instance v0, Lcom/facebook/android/maps/MapView;

    invoke-virtual {p0}, LX/6Zn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/MapView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    goto :goto_0

    .line 1111642
    :cond_6
    iget-object v0, p0, LX/6Zn;->f:LX/681;

    if-eqz v0, :cond_7

    .line 1111643
    new-instance v0, LX/7au;

    invoke-virtual {p0}, LX/6Zn;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/6Zn;->f:LX/681;

    invoke-static {v2}, LX/6as;->a(LX/681;)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7au;-><init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V

    iput-object v0, p0, LX/6Zn;->d:LX/7au;

    .line 1111644
    :goto_3
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    goto :goto_1

    .line 1111645
    :cond_7
    new-instance v0, LX/7au;

    invoke-virtual {p0}, LX/6Zn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7au;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/6Zn;->d:LX/7au;

    goto :goto_3
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1111618
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_0

    .line 1111619
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->b()V

    .line 1111620
    :goto_0
    return-void

    .line 1111621
    :cond_0
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    invoke-virtual {v0}, LX/7au;->g()V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1111613
    const-string v0, "state_map_source"

    iget v1, p0, LX/6Zn;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1111614
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_0

    .line 1111615
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, p1}, Lcom/facebook/android/maps/MapView;->b(Landroid/os/Bundle;)V

    .line 1111616
    :goto_0
    return-void

    .line 1111617
    :cond_0
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    invoke-virtual {v0, p1}, LX/7au;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1111610
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-nez v0, :cond_0

    .line 1111611
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    invoke-virtual {v0}, LX/7au;->d()V

    .line 1111612
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1111607
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-nez v0, :cond_0

    .line 1111608
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    invoke-virtual {v0}, LX/7au;->b()V

    .line 1111609
    :cond_0
    return-void
.end method

.method public getFacebookMapView()Lcom/facebook/android/maps/MapView;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1111606
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    return-object v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 1111605
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    invoke-virtual {v0}, LX/7au;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1111595
    if-eqz p1, :cond_2

    .line 1111596
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    if-eqz v0, :cond_1

    .line 1111597
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    invoke-virtual {v0, v1}, LX/7au;->setVisibility(I)V

    .line 1111598
    :cond_0
    :goto_0
    return-void

    .line 1111599
    :cond_1
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_0

    .line 1111600
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/MapView;->setVisibility(I)V

    goto :goto_0

    .line 1111601
    :cond_2
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    if-eqz v0, :cond_3

    .line 1111602
    iget-object v0, p0, LX/6Zn;->d:LX/7au;

    invoke-virtual {v0, v2}, LX/7au;->setVisibility(I)V

    goto :goto_0

    .line 1111603
    :cond_3
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_0

    .line 1111604
    iget-object v0, p0, LX/6Zn;->c:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v0, v2}, Lcom/facebook/android/maps/MapView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setMapReporterLauncher(LX/3BT;)V
    .locals 1

    .prologue
    .line 1111590
    iput-object p1, p0, LX/6Zn;->h:LX/3BT;

    .line 1111591
    iget-object v0, p0, LX/6Zn;->g:LX/68M;

    if-eqz v0, :cond_0

    .line 1111592
    iget-object v0, p0, LX/6Zn;->g:LX/68M;

    .line 1111593
    iput-object p1, v0, LX/68M;->y:LX/3BT;

    .line 1111594
    :cond_0
    return-void
.end method
