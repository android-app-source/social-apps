.class public final LX/6GB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field public final synthetic a:LX/6GC;

.field private final b:Landroid/hardware/SensorEventListener;


# direct methods
.method public constructor <init>(LX/6GC;Landroid/hardware/SensorEventListener;)V
    .locals 0

    .prologue
    .line 1069945
    iput-object p1, p0, LX/6GB;->a:LX/6GC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1069946
    iput-object p2, p0, LX/6GB;->b:Landroid/hardware/SensorEventListener;

    .line 1069947
    return-void
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 1

    .prologue
    .line 1069948
    iget-object v0, p0, LX/6GB;->a:LX/6GC;

    iget-object v0, v0, LX/6GC;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    .line 1069949
    iget-object v0, p0, LX/6GB;->b:Landroid/hardware/SensorEventListener;

    invoke-interface {v0, p1, p2}, Landroid/hardware/SensorEventListener;->onAccuracyChanged(Landroid/hardware/Sensor;I)V

    .line 1069950
    :cond_0
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1

    .prologue
    .line 1069951
    iget-object v0, p0, LX/6GB;->a:LX/6GC;

    iget-object v0, v0, LX/6GC;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_0

    .line 1069952
    iget-object v0, p0, LX/6GB;->b:Landroid/hardware/SensorEventListener;

    invoke-interface {v0, p1}, Landroid/hardware/SensorEventListener;->onSensorChanged(Landroid/hardware/SensorEvent;)V

    .line 1069953
    :cond_0
    return-void
.end method
