.class public final LX/5OT;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/fbui/popover/PopoverViewFlipper;


# direct methods
.method public constructor <init>(Lcom/facebook/fbui/popover/PopoverViewFlipper;)V
    .locals 0

    .prologue
    .line 908823
    iput-object p1, p0, LX/5OT;->a:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/fbui/popover/PopoverViewFlipper;B)V
    .locals 0

    .prologue
    .line 908824
    invoke-direct {p0, p1}, LX/5OT;-><init>(Lcom/facebook/fbui/popover/PopoverViewFlipper;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 908825
    invoke-virtual {p1, v2, v3}, LX/0wd;->g(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 908826
    iget-wide v8, p1, LX/0wd;->i:D

    move-wide v0, v8

    .line 908827
    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 908828
    invoke-virtual {p1}, LX/0wd;->j()LX/0wd;

    .line 908829
    :goto_0
    return-void

    .line 908830
    :cond_0
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v6, v0

    .line 908831
    iget-object v7, p0, LX/5OT;->a:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    .line 908832
    float-to-double v0, v6

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {v7, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setAlpha(F)V

    .line 908833
    invoke-virtual {v7, v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setScaleX(F)V

    .line 908834
    invoke-virtual {v7, v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setScaleY(F)V

    goto :goto_0
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 908835
    iget-object v1, p0, LX/5OT;->a:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 908836
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 2

    .prologue
    .line 908837
    iget-object v1, p0, LX/5OT;->a:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-static {v1}, LX/0wc;->a(Landroid/view/View;)V

    .line 908838
    return-void
.end method
