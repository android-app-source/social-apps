.class public LX/69q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/69q;


# instance fields
.field private final a:LX/0jU;


# direct methods
.method public constructor <init>(LX/0jU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1058347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1058348
    iput-object p1, p0, LX/69q;->a:LX/0jU;

    .line 1058349
    return-void
.end method

.method public static a(LX/0QB;)LX/69q;
    .locals 4

    .prologue
    .line 1058350
    sget-object v0, LX/69q;->b:LX/69q;

    if-nez v0, :cond_1

    .line 1058351
    const-class v1, LX/69q;

    monitor-enter v1

    .line 1058352
    :try_start_0
    sget-object v0, LX/69q;->b:LX/69q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1058353
    if-eqz v2, :cond_0

    .line 1058354
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1058355
    new-instance p0, LX/69q;

    invoke-static {v0}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v3

    check-cast v3, LX/0jU;

    invoke-direct {p0, v3}, LX/69q;-><init>(LX/0jU;)V

    .line 1058356
    move-object v0, p0

    .line 1058357
    sput-object v0, LX/69q;->b:LX/69q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058358
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1058359
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1058360
    :cond_1
    sget-object v0, LX/69q;->b:LX/69q;

    return-object v0

    .line 1058361
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1058362
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 1058363
    iget-object v0, p0, LX/69q;->a:LX/0jU;

    invoke-virtual {v0}, LX/0jU;->a()V

    .line 1058364
    return-void
.end method
