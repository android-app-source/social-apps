.class public final LX/6ZM;
.super LX/6ZK;
.source ""


# instance fields
.field public final synthetic a:Landroid/app/PendingIntent;

.field public final synthetic b:LX/2Fw;


# direct methods
.method public constructor <init>(LX/2Fw;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 1111088
    iput-object p1, p0, LX/6ZM;->b:LX/2Fw;

    iput-object p2, p0, LX/6ZM;->a:Landroid/app/PendingIntent;

    invoke-direct {p0}, LX/6ZK;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1111089
    iget-object v0, p0, LX/6ZM;->b:LX/2Fw;

    invoke-static {v0}, LX/2Fw;->a$redex0(LX/2Fw;)V

    .line 1111090
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1111091
    iget-object v0, p0, LX/6ZM;->b:LX/2Fw;

    .line 1111092
    iget-object v1, p0, LX/6ZK;->a:LX/2wX;

    move-object v1, v1

    .line 1111093
    iget-object v2, p0, LX/6ZM;->a:Landroid/app/PendingIntent;

    invoke-static {v0, v1, v2, p0}, LX/2Fw;->a$redex0(LX/2Fw;LX/2wX;Landroid/app/PendingIntent;LX/6ZK;)V

    .line 1111094
    iget-object v0, p0, LX/6ZM;->b:LX/2Fw;

    .line 1111095
    iget-object v1, p0, LX/6ZK;->a:LX/2wX;

    move-object v1, v1

    .line 1111096
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111097
    :try_start_0
    invoke-virtual {v1}, LX/2wX;->g()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1111098
    :goto_0
    return-void

    .line 1111099
    :catch_0
    move-exception v2

    .line 1111100
    invoke-static {v2}, LX/3KX;->a(Ljava/lang/RuntimeException;)V

    .line 1111101
    iget-object v3, v0, LX/2Fw;->c:LX/03V;

    const-string p1, "fb_location_continuous_listener_google_play"

    const-string p0, "Google exception on disconnect"

    invoke-virtual {v3, p1, p0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1

    .prologue
    .line 1111102
    iget-object v0, p0, LX/6ZM;->b:LX/2Fw;

    invoke-static {v0, p1}, LX/2Fw;->a$redex0(LX/2Fw;Lcom/google/android/gms/common/ConnectionResult;)V

    .line 1111103
    return-void
.end method
