.class public final LX/6Xy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/interstitial/manager/InterstitialTriggerContext;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1109014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/interstitial/manager/InterstitialTriggerContext;
    .locals 1

    .prologue
    .line 1109015
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    invoke-direct {v0, p0}, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1109016
    invoke-static {p1}, LX/6Xy;->a(Landroid/os/Parcel;)Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1109017
    new-array v0, p1, [Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    move-object v0, v0

    .line 1109018
    return-object v0
.end method
