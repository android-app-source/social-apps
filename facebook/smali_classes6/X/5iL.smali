.class public final enum LX/5iL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5iL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5iL;

.field public static final enum AE08bit:LX/5iL;

.field public static final enum FallRGB:LX/5iL;

.field public static final enum PassThrough:LX/5iL;

.field public static final enum SpringRGB:LX/5iL;

.field public static final enum SummerRGB:LX/5iL;

.field public static final enum VintageRGB:LX/5iL;

.field public static final enum WinterRGB:LX/5iL;

.field public static final enum ZebraBW:LX/5iL;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 982698
    new-instance v0, LX/5iL;

    const-string v1, "PassThrough"

    invoke-direct {v0, v1, v3}, LX/5iL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5iL;->PassThrough:LX/5iL;

    .line 982699
    new-instance v0, LX/5iL;

    const-string v1, "AE08bit"

    invoke-direct {v0, v1, v4}, LX/5iL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5iL;->AE08bit:LX/5iL;

    .line 982700
    new-instance v0, LX/5iL;

    const-string v1, "VintageRGB"

    invoke-direct {v0, v1, v5}, LX/5iL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5iL;->VintageRGB:LX/5iL;

    .line 982701
    new-instance v0, LX/5iL;

    const-string v1, "SpringRGB"

    invoke-direct {v0, v1, v6}, LX/5iL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5iL;->SpringRGB:LX/5iL;

    .line 982702
    new-instance v0, LX/5iL;

    const-string v1, "SummerRGB"

    invoke-direct {v0, v1, v7}, LX/5iL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5iL;->SummerRGB:LX/5iL;

    .line 982703
    new-instance v0, LX/5iL;

    const-string v1, "FallRGB"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/5iL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5iL;->FallRGB:LX/5iL;

    .line 982704
    new-instance v0, LX/5iL;

    const-string v1, "WinterRGB"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/5iL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5iL;->WinterRGB:LX/5iL;

    .line 982705
    new-instance v0, LX/5iL;

    const-string v1, "ZebraBW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/5iL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5iL;->ZebraBW:LX/5iL;

    .line 982706
    const/16 v0, 0x8

    new-array v0, v0, [LX/5iL;

    sget-object v1, LX/5iL;->PassThrough:LX/5iL;

    aput-object v1, v0, v3

    sget-object v1, LX/5iL;->AE08bit:LX/5iL;

    aput-object v1, v0, v4

    sget-object v1, LX/5iL;->VintageRGB:LX/5iL;

    aput-object v1, v0, v5

    sget-object v1, LX/5iL;->SpringRGB:LX/5iL;

    aput-object v1, v0, v6

    sget-object v1, LX/5iL;->SummerRGB:LX/5iL;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/5iL;->FallRGB:LX/5iL;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/5iL;->WinterRGB:LX/5iL;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/5iL;->ZebraBW:LX/5iL;

    aput-object v2, v0, v1

    sput-object v0, LX/5iL;->$VALUES:[LX/5iL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 982707
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getValue(Ljava/lang/String;)LX/5iL;
    .locals 5

    .prologue
    .line 982708
    invoke-static {}, LX/5iL;->values()[LX/5iL;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 982709
    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 982710
    :goto_1
    return-object v0

    .line 982711
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 982712
    :cond_1
    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    goto :goto_1
.end method

.method public static isFilter(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 982713
    invoke-static {}, LX/5iL;->values()[LX/5iL;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 982714
    invoke-virtual {v4}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 982715
    const/4 v0, 0x1

    .line 982716
    :cond_0
    return v0

    .line 982717
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/5iL;
    .locals 1

    .prologue
    .line 982718
    const-class v0, LX/5iL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5iL;

    return-object v0
.end method

.method public static values()[LX/5iL;
    .locals 1

    .prologue
    .line 982719
    sget-object v0, LX/5iL;->$VALUES:[LX/5iL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5iL;

    return-object v0
.end method
