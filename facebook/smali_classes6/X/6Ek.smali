.class public final LX/6Ek;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;)V
    .locals 0

    .prologue
    .line 1066607
    iput-object p1, p0, LX/6Ek;->a:Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1066587
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1066588
    if-nez v0, :cond_0

    .line 1066589
    iget-object v0, p0, LX/6Ek;->a:Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->f:LX/6Ef;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Ef;->a(Ljava/lang/Boolean;)V

    .line 1066590
    :goto_0
    return v3

    .line 1066591
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1066592
    sget-object v1, LX/6D6;->LOCATION:LX/6D6;

    invoke-virtual {v1}, LX/6D6;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1066593
    new-instance v1, LX/6Ej;

    iget-object v2, p0, LX/6Ek;->a:Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->j:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/6Ej;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/6Ek;->a:Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->g:Ljava/lang/String;

    .line 1066594
    iput-object v2, v1, LX/6EK;->c:Ljava/lang/String;

    .line 1066595
    move-object v1, v1

    .line 1066596
    iget-object v2, p0, LX/6Ek;->a:Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->h:Ljava/lang/String;

    .line 1066597
    iput-object v2, v1, LX/6EK;->d:Ljava/lang/String;

    .line 1066598
    move-object v1, v1

    .line 1066599
    iput-object v0, v1, LX/6EK;->e:Ljava/util/ArrayList;

    .line 1066600
    move-object v0, v1

    .line 1066601
    sget-object v1, LX/6ET;->MANAGE_PERMISSION:LX/6ET;

    .line 1066602
    iput-object v1, v0, LX/6EK;->a:LX/6ET;

    .line 1066603
    move-object v0, v0

    .line 1066604
    invoke-virtual {v0}, LX/6EK;->b()Landroid/content/Intent;

    move-result-object v0

    .line 1066605
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1066606
    iget-object v1, p0, LX/6Ek;->a:Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    iget-object v1, v1, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/6Ek;->a:Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    iget-object v2, v2, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;->j:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
