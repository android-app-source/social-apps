.class public final LX/65I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/650;

.field public final b:LX/655;


# direct methods
.method public constructor <init>(LX/650;LX/655;)V
    .locals 0

    .prologue
    .line 1046922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046923
    iput-object p1, p0, LX/65I;->a:LX/650;

    .line 1046924
    iput-object p2, p0, LX/65I;->b:LX/655;

    .line 1046925
    return-void
.end method

.method public static a(LX/655;LX/650;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1046926
    iget v1, p0, LX/655;->c:I

    move v1, v1

    .line 1046927
    sparse-switch v1, :sswitch_data_0

    .line 1046928
    :cond_0
    :goto_0
    return v0

    .line 1046929
    :sswitch_0
    const-string v1, "Expires"

    invoke-virtual {p0, v1}, LX/655;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1046930
    invoke-virtual {p0}, LX/655;->h()LX/64W;

    move-result-object v1

    .line 1046931
    iget v2, v1, LX/64W;->f:I

    move v1, v2

    .line 1046932
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 1046933
    invoke-virtual {p0}, LX/655;->h()LX/64W;

    move-result-object v1

    .line 1046934
    iget-boolean v2, v1, LX/64W;->i:Z

    move v1, v2

    .line 1046935
    if-nez v1, :cond_1

    .line 1046936
    invoke-virtual {p0}, LX/655;->h()LX/64W;

    move-result-object v1

    .line 1046937
    iget-boolean v2, v1, LX/64W;->h:Z

    move v1, v2

    .line 1046938
    if-eqz v1, :cond_0

    .line 1046939
    :cond_1
    :sswitch_1
    invoke-virtual {p0}, LX/655;->h()LX/64W;

    move-result-object v1

    .line 1046940
    iget-boolean v2, v1, LX/64W;->e:Z

    move v1, v2

    .line 1046941
    if-nez v1, :cond_0

    invoke-virtual {p1}, LX/650;->e()LX/64W;

    move-result-object v1

    .line 1046942
    iget-boolean v2, v1, LX/64W;->e:Z

    move v1, v2

    .line 1046943
    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0xcb -> :sswitch_1
        0xcc -> :sswitch_1
        0x12c -> :sswitch_1
        0x12d -> :sswitch_1
        0x12e -> :sswitch_0
        0x133 -> :sswitch_0
        0x134 -> :sswitch_1
        0x194 -> :sswitch_1
        0x195 -> :sswitch_1
        0x19a -> :sswitch_1
        0x19e -> :sswitch_1
        0x1f5 -> :sswitch_1
    .end sparse-switch
.end method
