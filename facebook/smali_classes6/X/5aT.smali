.class public final LX/5aT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 48

    .prologue
    .line 954687
    const/16 v44, 0x0

    .line 954688
    const/16 v43, 0x0

    .line 954689
    const/16 v42, 0x0

    .line 954690
    const/16 v41, 0x0

    .line 954691
    const/16 v40, 0x0

    .line 954692
    const/16 v39, 0x0

    .line 954693
    const/16 v38, 0x0

    .line 954694
    const/16 v37, 0x0

    .line 954695
    const/16 v36, 0x0

    .line 954696
    const/16 v35, 0x0

    .line 954697
    const/16 v34, 0x0

    .line 954698
    const/16 v33, 0x0

    .line 954699
    const/16 v32, 0x0

    .line 954700
    const/16 v31, 0x0

    .line 954701
    const/16 v30, 0x0

    .line 954702
    const/16 v29, 0x0

    .line 954703
    const/16 v28, 0x0

    .line 954704
    const/16 v27, 0x0

    .line 954705
    const/16 v26, 0x0

    .line 954706
    const/16 v25, 0x0

    .line 954707
    const/16 v24, 0x0

    .line 954708
    const/16 v23, 0x0

    .line 954709
    const/16 v22, 0x0

    .line 954710
    const/16 v21, 0x0

    .line 954711
    const/16 v20, 0x0

    .line 954712
    const/16 v19, 0x0

    .line 954713
    const/16 v18, 0x0

    .line 954714
    const/16 v17, 0x0

    .line 954715
    const/16 v16, 0x0

    .line 954716
    const/4 v15, 0x0

    .line 954717
    const/4 v14, 0x0

    .line 954718
    const/4 v13, 0x0

    .line 954719
    const/4 v12, 0x0

    .line 954720
    const/4 v11, 0x0

    .line 954721
    const/4 v10, 0x0

    .line 954722
    const/4 v9, 0x0

    .line 954723
    const/4 v8, 0x0

    .line 954724
    const/4 v7, 0x0

    .line 954725
    const/4 v6, 0x0

    .line 954726
    const/4 v5, 0x0

    .line 954727
    const/4 v4, 0x0

    .line 954728
    const/4 v3, 0x0

    .line 954729
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v45

    sget-object v46, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    if-eq v0, v1, :cond_1

    .line 954730
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 954731
    const/4 v3, 0x0

    .line 954732
    :goto_0
    return v3

    .line 954733
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 954734
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v45

    sget-object v46, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    if-eq v0, v1, :cond_26

    .line 954735
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v45

    .line 954736
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 954737
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v46

    sget-object v47, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    if-eq v0, v1, :cond_1

    if-eqz v45, :cond_1

    .line 954738
    const-string v46, "__type__"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-nez v46, :cond_2

    const-string v46, "__typename"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_3

    .line 954739
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v44

    goto :goto_1

    .line 954740
    :cond_3
    const-string v46, "ad_preferences_link"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_4

    .line 954741
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v43

    goto :goto_1

    .line 954742
    :cond_4
    const-string v46, "ad_properties"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_5

    .line 954743
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v42

    goto :goto_1

    .line 954744
    :cond_5
    const-string v46, "bot_items"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_6

    .line 954745
    invoke-static/range {p0 .. p1}, LX/5aF;->a(LX/15w;LX/186;)I

    move-result v41

    goto :goto_1

    .line 954746
    :cond_6
    const-string v46, "call_to_action"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_7

    .line 954747
    invoke-static/range {p0 .. p1}, LX/5To;->a(LX/15w;LX/186;)I

    move-result v40

    goto :goto_1

    .line 954748
    :cond_7
    const-string v46, "cart_item_count"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_8

    .line 954749
    const/4 v8, 0x1

    .line 954750
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v39

    goto/16 :goto_1

    .line 954751
    :cond_8
    const-string v46, "collapsed_manage_description"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_9

    .line 954752
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    goto/16 :goto_1

    .line 954753
    :cond_9
    const-string v46, "collapsed_text"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_a

    .line 954754
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    goto/16 :goto_1

    .line 954755
    :cond_a
    const-string v46, "color_choices"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_b

    .line 954756
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v36

    goto/16 :goto_1

    .line 954757
    :cond_b
    const-string v46, "ctas"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_c

    .line 954758
    invoke-static/range {p0 .. p1}, LX/5To;->b(LX/15w;LX/186;)I

    move-result v35

    goto/16 :goto_1

    .line 954759
    :cond_c
    const-string v46, "emoji_choices"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_d

    .line 954760
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v34

    goto/16 :goto_1

    .line 954761
    :cond_d
    const-string v46, "event"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_e

    .line 954762
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    goto/16 :goto_1

    .line 954763
    :cond_e
    const-string v46, "expanded_text"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_f

    .line 954764
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v32

    goto/16 :goto_1

    .line 954765
    :cond_f
    const-string v46, "favorite_item_cta"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_10

    .line 954766
    invoke-static/range {p0 .. p1}, LX/5To;->a(LX/15w;LX/186;)I

    move-result v31

    goto/16 :goto_1

    .line 954767
    :cond_10
    const-string v46, "favorite_item_title"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_11

    .line 954768
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 954769
    :cond_11
    const-string v46, "game"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_12

    .line 954770
    invoke-static/range {p0 .. p1}, LX/5aG;->a(LX/15w;LX/186;)I

    move-result v29

    goto/16 :goto_1

    .line 954771
    :cond_12
    const-string v46, "game_type"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_13

    .line 954772
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 954773
    :cond_13
    const-string v46, "intent_id"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_14

    .line 954774
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 954775
    :cond_14
    const-string v46, "is_new_high_score"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_15

    .line 954776
    const/4 v7, 0x1

    .line 954777
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto/16 :goto_1

    .line 954778
    :cond_15
    const-string v46, "is_video_call"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_16

    .line 954779
    const/4 v6, 0x1

    .line 954780
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto/16 :goto_1

    .line 954781
    :cond_16
    const-string v46, "leaderboard"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_17

    .line 954782
    invoke-static/range {p0 .. p1}, LX/5aJ;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 954783
    :cond_17
    const-string v46, "message_state"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_18

    .line 954784
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMediaSubscriptionManageMessageStateType;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v23

    goto/16 :goto_1

    .line 954785
    :cond_18
    const-string v46, "native_booking_request"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_19

    .line 954786
    invoke-static/range {p0 .. p1}, LX/5aK;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 954787
    :cond_19
    const-string v46, "nickname"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1a

    .line 954788
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto/16 :goto_1

    .line 954789
    :cond_1a
    const-string v46, "nickname_choices"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1b

    .line 954790
    invoke-static/range {p0 .. p1}, LX/5aL;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 954791
    :cond_1b
    const-string v46, "option_count"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1c

    .line 954792
    const/4 v5, 0x1

    .line 954793
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto/16 :goto_1

    .line 954794
    :cond_1c
    const-string v46, "page"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1d

    .line 954795
    invoke-static/range {p0 .. p1}, LX/5aM;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 954796
    :cond_1d
    const-string v46, "question"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1e

    .line 954797
    invoke-static/range {p0 .. p1}, LX/5aR;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 954798
    :cond_1e
    const-string v46, "ride_provider"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_1f

    .line 954799
    invoke-static/range {p0 .. p1}, LX/5aS;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 954800
    :cond_1f
    const-string v46, "score"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_20

    .line 954801
    const/4 v4, 0x1

    .line 954802
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto/16 :goto_1

    .line 954803
    :cond_20
    const-string v46, "server_info_data"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_21

    .line 954804
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 954805
    :cond_21
    const-string v46, "target_id"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_22

    .line 954806
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 954807
    :cond_22
    const-string v46, "theme_color"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_23

    .line 954808
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 954809
    :cond_23
    const-string v46, "thread_icon_emoji"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_24

    .line 954810
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 954811
    :cond_24
    const-string v46, "thread_message_lifetime"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_25

    .line 954812
    const/4 v3, 0x1

    .line 954813
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto/16 :goto_1

    .line 954814
    :cond_25
    const-string v46, "update_type"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v45

    if-eqz v45, :cond_0

    .line 954815
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantGameUpdateXMATUpdateType;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 954816
    :cond_26
    const/16 v45, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 954817
    const/16 v45, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 954818
    const/16 v44, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v44

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 954819
    const/16 v43, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v43

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 954820
    const/16 v42, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v42

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 954821
    const/16 v41, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 954822
    if-eqz v8, :cond_27

    .line 954823
    const/4 v8, 0x5

    const/16 v40, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v8, v1, v2}, LX/186;->a(III)V

    .line 954824
    :cond_27
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954825
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954826
    const/16 v8, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954827
    const/16 v8, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954828
    const/16 v8, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954829
    const/16 v8, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954830
    const/16 v8, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954831
    const/16 v8, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954832
    const/16 v8, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954833
    const/16 v8, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954834
    const/16 v8, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954835
    const/16 v8, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 954836
    if-eqz v7, :cond_28

    .line 954837
    const/16 v7, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 954838
    :cond_28
    if-eqz v6, :cond_29

    .line 954839
    const/16 v6, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 954840
    :cond_29
    const/16 v6, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 954841
    const/16 v6, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 954842
    const/16 v6, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 954843
    const/16 v6, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 954844
    const/16 v6, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 954845
    if-eqz v5, :cond_2a

    .line 954846
    const/16 v5, 0x19

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v5, v1, v6}, LX/186;->a(III)V

    .line 954847
    :cond_2a
    const/16 v5, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 954848
    const/16 v5, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 954849
    const/16 v5, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 954850
    if-eqz v4, :cond_2b

    .line 954851
    const/16 v4, 0x1d

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15, v5}, LX/186;->a(III)V

    .line 954852
    :cond_2b
    const/16 v4, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 954853
    const/16 v4, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 954854
    const/16 v4, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 954855
    const/16 v4, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 954856
    if-eqz v3, :cond_2c

    .line 954857
    const/16 v3, 0x22

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v4}, LX/186;->a(III)V

    .line 954858
    :cond_2c
    const/16 v3, 0x23

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 954859
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 10

    .prologue
    const/16 v6, 0x15

    const/16 v5, 0xa

    const/16 v4, 0x8

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 954860
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 954861
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 954862
    if-eqz v0, :cond_0

    .line 954863
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954864
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 954865
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954866
    if-eqz v0, :cond_1

    .line 954867
    const-string v1, "ad_preferences_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954868
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954869
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 954870
    if-eqz v0, :cond_2

    .line 954871
    const-string v0, "ad_properties"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954872
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 954873
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954874
    if-eqz v0, :cond_a

    .line 954875
    const-string v1, "bot_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954876
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 954877
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_9

    .line 954878
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    .line 954879
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 954880
    const/4 v7, 0x0

    invoke-virtual {p0, v3, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 954881
    if-eqz v7, :cond_3

    .line 954882
    const-string v8, "bot_id"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954883
    invoke-virtual {p2, v7}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954884
    :cond_3
    const/4 v7, 0x1

    invoke-virtual {p0, v3, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 954885
    if-eqz v7, :cond_4

    .line 954886
    const-string v8, "description"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954887
    invoke-virtual {p2, v7}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954888
    :cond_4
    const/4 v7, 0x2

    invoke-virtual {p0, v3, v7}, LX/15i;->g(II)I

    move-result v7

    .line 954889
    if-eqz v7, :cond_7

    .line 954890
    const-string v8, "icon"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954891
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 954892
    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, LX/15i;->g(II)I

    move-result v8

    .line 954893
    if-eqz v8, :cond_6

    .line 954894
    const-string v9, "image"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954895
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 954896
    const/4 v9, 0x0

    invoke-virtual {p0, v8, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v9

    .line 954897
    if-eqz v9, :cond_5

    .line 954898
    const-string v7, "uri"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954899
    invoke-virtual {p2, v9}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954900
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 954901
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 954902
    :cond_7
    const/4 v7, 0x3

    invoke-virtual {p0, v3, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 954903
    if-eqz v7, :cond_8

    .line 954904
    const-string v8, "title"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954905
    invoke-virtual {p2, v7}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954906
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 954907
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 954908
    :cond_9
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 954909
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954910
    if-eqz v0, :cond_b

    .line 954911
    const-string v1, "call_to_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954912
    invoke-static {p0, v0, p2, p3}, LX/5To;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 954913
    :cond_b
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 954914
    if-eqz v0, :cond_c

    .line 954915
    const-string v1, "cart_item_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954916
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 954917
    :cond_c
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954918
    if-eqz v0, :cond_d

    .line 954919
    const-string v1, "collapsed_manage_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954920
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954921
    :cond_d
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954922
    if-eqz v0, :cond_e

    .line 954923
    const-string v1, "collapsed_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954924
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954925
    :cond_e
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 954926
    if-eqz v0, :cond_f

    .line 954927
    const-string v0, "color_choices"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954928
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 954929
    :cond_f
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954930
    if-eqz v0, :cond_10

    .line 954931
    const-string v1, "ctas"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954932
    invoke-static {p0, v0, p2, p3}, LX/5To;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 954933
    :cond_10
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 954934
    if-eqz v0, :cond_11

    .line 954935
    const-string v0, "emoji_choices"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954936
    invoke-virtual {p0, p1, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 954937
    :cond_11
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954938
    if-eqz v0, :cond_12

    .line 954939
    const-string v1, "event"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954940
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954941
    :cond_12
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954942
    if-eqz v0, :cond_13

    .line 954943
    const-string v1, "expanded_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954944
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954945
    :cond_13
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954946
    if-eqz v0, :cond_14

    .line 954947
    const-string v1, "favorite_item_cta"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954948
    invoke-static {p0, v0, p2, p3}, LX/5To;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 954949
    :cond_14
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954950
    if-eqz v0, :cond_15

    .line 954951
    const-string v1, "favorite_item_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954952
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954953
    :cond_15
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954954
    if-eqz v0, :cond_16

    .line 954955
    const-string v1, "game"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954956
    invoke-static {p0, v0, p2, p3}, LX/5aG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 954957
    :cond_16
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954958
    if-eqz v0, :cond_17

    .line 954959
    const-string v1, "game_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954960
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954961
    :cond_17
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954962
    if-eqz v0, :cond_18

    .line 954963
    const-string v1, "intent_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954964
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954965
    :cond_18
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 954966
    if-eqz v0, :cond_19

    .line 954967
    const-string v1, "is_new_high_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954968
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 954969
    :cond_19
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 954970
    if-eqz v0, :cond_1a

    .line 954971
    const-string v1, "is_video_call"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954972
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 954973
    :cond_1a
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954974
    if-eqz v0, :cond_1b

    .line 954975
    const-string v1, "leaderboard"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954976
    invoke-static {p0, v0, p2, p3}, LX/5aJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 954977
    :cond_1b
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 954978
    if-eqz v0, :cond_1c

    .line 954979
    const-string v0, "message_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954980
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954981
    :cond_1c
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954982
    if-eqz v0, :cond_1d

    .line 954983
    const-string v1, "native_booking_request"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954984
    invoke-static {p0, v0, p2}, LX/5aK;->a(LX/15i;ILX/0nX;)V

    .line 954985
    :cond_1d
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 954986
    if-eqz v0, :cond_1e

    .line 954987
    const-string v1, "nickname"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954988
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 954989
    :cond_1e
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954990
    if-eqz v0, :cond_1f

    .line 954991
    const-string v1, "nickname_choices"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954992
    invoke-static {p0, v0, p2, p3}, LX/5aL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 954993
    :cond_1f
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 954994
    if-eqz v0, :cond_20

    .line 954995
    const-string v1, "option_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 954996
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 954997
    :cond_20
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 954998
    if-eqz v0, :cond_21

    .line 954999
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955000
    invoke-static {p0, v0, p2, p3}, LX/5aM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 955001
    :cond_21
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955002
    if-eqz v0, :cond_22

    .line 955003
    const-string v1, "question"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955004
    invoke-static {p0, v0, p2, p3}, LX/5aR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 955005
    :cond_22
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955006
    if-eqz v0, :cond_24

    .line 955007
    const-string v1, "ride_provider"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955008
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 955009
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 955010
    if-eqz v1, :cond_23

    .line 955011
    const-string v3, "name"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955012
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955013
    :cond_23
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 955014
    :cond_24
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 955015
    if-eqz v0, :cond_25

    .line 955016
    const-string v1, "score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955017
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 955018
    :cond_25
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955019
    if-eqz v0, :cond_26

    .line 955020
    const-string v1, "server_info_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955021
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955022
    :cond_26
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955023
    if-eqz v0, :cond_27

    .line 955024
    const-string v1, "target_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955025
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955026
    :cond_27
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955027
    if-eqz v0, :cond_28

    .line 955028
    const-string v1, "theme_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955029
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955030
    :cond_28
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 955031
    if-eqz v0, :cond_29

    .line 955032
    const-string v1, "thread_icon_emoji"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955033
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955034
    :cond_29
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 955035
    if-eqz v0, :cond_2a

    .line 955036
    const-string v1, "thread_message_lifetime"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955037
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 955038
    :cond_2a
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 955039
    if-eqz v0, :cond_2b

    .line 955040
    const-string v0, "update_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 955041
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 955042
    :cond_2b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 955043
    return-void
.end method
