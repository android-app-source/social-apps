.class public final LX/6RL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1089616
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1089617
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1089618
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1089619
    invoke-static {p0, p1}, LX/6RL;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1089620
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1089621
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1089622
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1089623
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1089624
    if-eqz v0, :cond_0

    .line 1089625
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1089626
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1089627
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1089628
    if-eqz v0, :cond_1

    .line 1089629
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1089630
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1089631
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1089632
    if-eqz v0, :cond_2

    .line 1089633
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1089634
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1089635
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1089636
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1089610
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1089611
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1089612
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/6RL;->a(LX/15i;ILX/0nX;)V

    .line 1089613
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1089614
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1089615
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1089589
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1089590
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1089591
    :goto_0
    return v1

    .line 1089592
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 1089593
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1089594
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1089595
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 1089596
    const-string v8, "height"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1089597
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 1089598
    :cond_1
    const-string v8, "uri"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1089599
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1089600
    :cond_2
    const-string v8, "width"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1089601
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1089602
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1089603
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1089604
    if-eqz v3, :cond_5

    .line 1089605
    invoke-virtual {p1, v1, v6, v1}, LX/186;->a(III)V

    .line 1089606
    :cond_5
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1089607
    if-eqz v0, :cond_6

    .line 1089608
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 1089609
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method
