.class public final LX/5To;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 922748
    const/4 v13, 0x0

    .line 922749
    const/4 v12, 0x0

    .line 922750
    const/4 v11, 0x0

    .line 922751
    const/4 v10, 0x0

    .line 922752
    const/4 v9, 0x0

    .line 922753
    const/4 v8, 0x0

    .line 922754
    const/4 v7, 0x0

    .line 922755
    const/4 v6, 0x0

    .line 922756
    const/4 v5, 0x0

    .line 922757
    const/4 v4, 0x0

    .line 922758
    const/4 v3, 0x0

    .line 922759
    const/4 v2, 0x0

    .line 922760
    const/4 v1, 0x0

    .line 922761
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 922762
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 922763
    const/4 v1, 0x0

    .line 922764
    :goto_0
    return v1

    .line 922765
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 922766
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_c

    .line 922767
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 922768
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 922769
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 922770
    const-string v15, "action_open_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 922771
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMessengerCallToActionType;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    goto :goto_1

    .line 922772
    :cond_2
    const-string v15, "action_targets"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 922773
    invoke-static/range {p0 .. p1}, LX/5Tl;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 922774
    :cond_3
    const-string v15, "action_title"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 922775
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 922776
    :cond_4
    const-string v15, "action_url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 922777
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 922778
    :cond_5
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 922779
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 922780
    :cond_6
    const-string v15, "is_disabled"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 922781
    const/4 v2, 0x1

    .line 922782
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 922783
    :cond_7
    const-string v15, "is_mutable_by_server"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 922784
    const/4 v1, 0x1

    .line 922785
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 922786
    :cond_8
    const-string v15, "native_url"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 922787
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 922788
    :cond_9
    const-string v15, "payment_metadata"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 922789
    invoke-static/range {p0 .. p1}, LX/5Tm;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 922790
    :cond_a
    const-string v15, "user_confirmation"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 922791
    invoke-static/range {p0 .. p1}, LX/5Tk;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 922792
    :cond_b
    const-string v15, "webview_metadata"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 922793
    invoke-static/range {p0 .. p1}, LX/5Tn;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 922794
    :cond_c
    const/16 v14, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 922795
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 922796
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 922797
    const/4 v12, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 922798
    const/4 v11, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 922799
    const/4 v10, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 922800
    if-eqz v2, :cond_d

    .line 922801
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 922802
    :cond_d
    if-eqz v1, :cond_e

    .line 922803
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 922804
    :cond_e
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 922805
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 922806
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 922807
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 922808
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 922809
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 922810
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 922811
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5To;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 922812
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 922813
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 922814
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 922815
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 922816
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 922817
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 922818
    invoke-static {p0, p1}, LX/5To;->a(LX/15w;LX/186;)I

    move-result v1

    .line 922819
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 922820
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 922821
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 922822
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 922823
    if-eqz v0, :cond_0

    .line 922824
    const-string v0, "action_open_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922825
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 922826
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 922827
    if-eqz v0, :cond_2

    .line 922828
    const-string v1, "action_targets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922829
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 922830
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 922831
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/5Tl;->a(LX/15i;ILX/0nX;)V

    .line 922832
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 922833
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 922834
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 922835
    if-eqz v0, :cond_3

    .line 922836
    const-string v1, "action_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922837
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 922838
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 922839
    if-eqz v0, :cond_4

    .line 922840
    const-string v1, "action_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922841
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 922842
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 922843
    if-eqz v0, :cond_5

    .line 922844
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922845
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 922846
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 922847
    if-eqz v0, :cond_6

    .line 922848
    const-string v1, "is_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922849
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 922850
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 922851
    if-eqz v0, :cond_7

    .line 922852
    const-string v1, "is_mutable_by_server"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922853
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 922854
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 922855
    if-eqz v0, :cond_8

    .line 922856
    const-string v1, "native_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922857
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 922858
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 922859
    if-eqz v0, :cond_9

    .line 922860
    const-string v1, "payment_metadata"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922861
    invoke-static {p0, v0, p2}, LX/5Tm;->a(LX/15i;ILX/0nX;)V

    .line 922862
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 922863
    if-eqz v0, :cond_a

    .line 922864
    const-string v1, "user_confirmation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922865
    invoke-static {p0, v0, p2}, LX/5Tk;->a(LX/15i;ILX/0nX;)V

    .line 922866
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 922867
    if-eqz v0, :cond_b

    .line 922868
    const-string v1, "webview_metadata"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 922869
    invoke-static {p0, v0, p2}, LX/5Tn;->a(LX/15i;ILX/0nX;)V

    .line 922870
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 922871
    return-void
.end method
