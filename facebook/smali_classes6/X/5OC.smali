.class public LX/5OC;
.super LX/34c;
.source ""

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field public c:Landroid/view/MenuItem;

.field public d:LX/34c;

.field private e:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 908003
    invoke-direct {p0, p1}, LX/34c;-><init>(Landroid/content/Context;)V

    .line 908004
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 908039
    iget-object v0, p0, LX/5OC;->e:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    .line 908040
    new-instance v0, LX/3Ai;

    invoke-direct {v0, p0, v1, v1, p1}, LX/3Ai;-><init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V

    iput-object v0, p0, LX/5OC;->e:Landroid/view/MenuItem;

    .line 908041
    :cond_0
    if-eqz p1, :cond_1

    .line 908042
    iget-object v0, p0, LX/5OC;->e:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 908043
    :cond_1
    if-eqz p2, :cond_2

    .line 908044
    iget-object v0, p0, LX/5OC;->e:Landroid/view/MenuItem;

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 908045
    :cond_2
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 908046
    return-void
.end method


# virtual methods
.method public final b(Landroid/view/MenuItem;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 908031
    iget-object v1, p0, LX/5OC;->e:Landroid/view/MenuItem;

    if-eqz v1, :cond_1

    .line 908032
    iget-object v1, p0, LX/5OC;->e:Landroid/view/MenuItem;

    if-ne p1, v1, :cond_0

    .line 908033
    :goto_0
    return v0

    .line 908034
    :cond_0
    const/4 v0, 0x1

    .line 908035
    :cond_1
    invoke-super {p0, p1}, LX/34c;->b(Landroid/view/MenuItem;)I

    move-result v1

    .line 908036
    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    move v0, v1

    .line 908037
    goto :goto_0

    .line 908038
    :cond_2
    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final clear()V
    .locals 0

    .prologue
    .line 908028
    invoke-virtual {p0}, LX/5OC;->clearHeader()V

    .line 908029
    invoke-super {p0}, LX/34c;->clear()V

    .line 908030
    return-void
.end method

.method public final clearHeader()V
    .locals 1

    .prologue
    .line 908026
    const/4 v0, 0x0

    iput-object v0, p0, LX/5OC;->e:Landroid/view/MenuItem;

    .line 908027
    return-void
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 908024
    invoke-super {p0}, LX/34c;->e()I

    move-result v0

    .line 908025
    iget-object v1, p0, LX/5OC;->e:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public final getItem()Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 908023
    iget-object v0, p0, LX/5OC;->c:Landroid/view/MenuItem;

    return-object v0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 908017
    iget-object v0, p0, LX/5OC;->e:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 908018
    if-nez p1, :cond_0

    .line 908019
    iget-object v0, p0, LX/5OC;->e:Landroid/view/MenuItem;

    .line 908020
    :goto_0
    return-object v0

    .line 908021
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 908022
    :cond_1
    invoke-super {p0, p1}, LX/34c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908047
    iget-object v0, p0, LX/34c;->c:Landroid/content/Context;

    move-object v0, v0

    .line 908048
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/5OC;->setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    .line 908049
    return-object p0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908015
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/5OC;->a(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    .line 908016
    return-object p0
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908012
    iget-object v0, p0, LX/34c;->c:Landroid/content/Context;

    move-object v0, v0

    .line 908013
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/5OC;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 908014
    return-object p0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 908010
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/5OC;->a(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    .line 908011
    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 908009
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 908008
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 0

    .prologue
    .line 908007
    return-object p0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 908005
    invoke-super {p0}, LX/34c;->size()I

    move-result v0

    .line 908006
    iget-object v1, p0, LX/5OC;->e:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method
