.class public final LX/52q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0vR;


# instance fields
.field public a:J

.field public final synthetic b:LX/54k;

.field public final synthetic c:LX/0vR;

.field public final synthetic d:J

.field public final synthetic e:J

.field public final synthetic f:LX/52r;


# direct methods
.method public constructor <init>(LX/52r;LX/54k;LX/0vR;JJ)V
    .locals 2

    .prologue
    .line 826440
    iput-object p1, p0, LX/52q;->f:LX/52r;

    iput-object p2, p0, LX/52q;->b:LX/54k;

    iput-object p3, p0, LX/52q;->c:LX/0vR;

    iput-wide p4, p0, LX/52q;->d:J

    iput-wide p6, p0, LX/52q;->e:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 826441
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/52q;->a:J

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 826442
    iget-object v0, p0, LX/52q;->b:LX/54k;

    invoke-virtual {v0}, LX/54k;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 826443
    iget-object v0, p0, LX/52q;->c:LX/0vR;

    invoke-interface {v0}, LX/0vR;->a()V

    .line 826444
    iget-wide v0, p0, LX/52q;->d:J

    iget-wide v2, p0, LX/52q;->a:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, LX/52q;->a:J

    iget-wide v4, p0, LX/52q;->e:J

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 826445
    iget-object v2, p0, LX/52q;->b:LX/54k;

    iget-object v3, p0, LX/52q;->f:LX/52r;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, LX/52r;->a()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    sub-long/2addr v0, v4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p0, v0, v1, v4}, LX/52r;->a(LX/0vR;JLjava/util/concurrent/TimeUnit;)LX/0za;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/54k;->a(LX/0za;)V

    .line 826446
    :cond_0
    return-void
.end method
