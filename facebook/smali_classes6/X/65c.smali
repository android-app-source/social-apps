.class public final LX/65c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field public static final synthetic k:Z

.field public static final l:Ljava/util/concurrent/ExecutorService;


# instance fields
.field public final a:LX/64x;

.field public final b:Z

.field public c:J

.field public d:J

.field public e:LX/663;

.field public final f:LX/663;

.field public final g:LX/65s;

.field public final h:Ljava/net/Socket;

.field public final i:LX/65Z;

.field public final j:Lokhttp3/internal/framed/FramedConnection$Reader;

.field public final m:LX/65R;

.field public final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/65i;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/lang/String;

.field public p:I

.field public q:I

.field public r:Z

.field public final s:Ljava/util/concurrent/ExecutorService;

.field private t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/660;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/661;

.field private v:I

.field public w:Z

.field public final x:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 1048036
    const-class v0, LX/65c;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    sput-boolean v0, LX/65c;->k:Z

    .line 1048037
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, 0x7fffffff

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    const-string v0, "OkHttp FramedConnection"

    .line 1048038
    invoke-static {v0, v8}, LX/65A;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v1, LX/65c;->l:Ljava/util/concurrent/ExecutorService;

    .line 1048039
    return-void

    :cond_0
    move v0, v2

    .line 1048040
    goto :goto_0
.end method

.method public constructor <init>(LX/65a;)V
    .locals 11

    .prologue
    const/4 v10, 0x7

    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1048002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1048003
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/65c;->n:Ljava/util/Map;

    .line 1048004
    const-wide/16 v4, 0x0

    iput-wide v4, p0, LX/65c;->c:J

    .line 1048005
    new-instance v0, LX/663;

    invoke-direct {v0}, LX/663;-><init>()V

    iput-object v0, p0, LX/65c;->e:LX/663;

    .line 1048006
    new-instance v0, LX/663;

    invoke-direct {v0}, LX/663;-><init>()V

    iput-object v0, p0, LX/65c;->f:LX/663;

    .line 1048007
    iput-boolean v2, p0, LX/65c;->w:Z

    .line 1048008
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LX/65c;->x:Ljava/util/Set;

    .line 1048009
    iget-object v0, p1, LX/65a;->f:LX/64x;

    iput-object v0, p0, LX/65c;->a:LX/64x;

    .line 1048010
    iget-object v0, p1, LX/65a;->g:LX/661;

    iput-object v0, p0, LX/65c;->u:LX/661;

    .line 1048011
    iget-boolean v0, p1, LX/65a;->h:Z

    iput-boolean v0, p0, LX/65c;->b:Z

    .line 1048012
    iget-object v0, p1, LX/65a;->e:LX/65R;

    iput-object v0, p0, LX/65c;->m:LX/65R;

    .line 1048013
    iget-boolean v0, p1, LX/65a;->h:Z

    if-eqz v0, :cond_3

    move v0, v3

    :goto_0
    iput v0, p0, LX/65c;->q:I

    .line 1048014
    iget-boolean v0, p1, LX/65a;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/65c;->a:LX/64x;

    sget-object v4, LX/64x;->HTTP_2:LX/64x;

    if-ne v0, v4, :cond_0

    .line 1048015
    iget v0, p0, LX/65c;->q:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/65c;->q:I

    .line 1048016
    :cond_0
    iget-boolean v0, p1, LX/65a;->h:Z

    if-eqz v0, :cond_1

    move v1, v3

    :cond_1
    iput v1, p0, LX/65c;->v:I

    .line 1048017
    iget-boolean v0, p1, LX/65a;->h:Z

    if-eqz v0, :cond_2

    .line 1048018
    iget-object v0, p0, LX/65c;->e:LX/663;

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v10, v2, v1}, LX/663;->a(III)LX/663;

    .line 1048019
    :cond_2
    iget-object v0, p1, LX/65a;->b:Ljava/lang/String;

    iput-object v0, p0, LX/65c;->o:Ljava/lang/String;

    .line 1048020
    iget-object v0, p0, LX/65c;->a:LX/64x;

    sget-object v1, LX/64x;->HTTP_2:LX/64x;

    if-ne v0, v1, :cond_4

    .line 1048021
    new-instance v0, LX/65t;

    invoke-direct {v0}, LX/65t;-><init>()V

    iput-object v0, p0, LX/65c;->g:LX/65s;

    .line 1048022
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const-string v0, "OkHttp %s Push Observer"

    new-array v8, v3, [Ljava/lang/Object;

    iget-object v9, p0, LX/65c;->o:Ljava/lang/String;

    aput-object v9, v8, v2

    .line 1048023
    invoke-static {v0, v8}, LX/65A;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, LX/65A;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, LX/65c;->s:Ljava/util/concurrent/ExecutorService;

    .line 1048024
    iget-object v0, p0, LX/65c;->f:LX/663;

    const v1, 0xffff

    invoke-virtual {v0, v10, v2, v1}, LX/663;->a(III)LX/663;

    .line 1048025
    iget-object v0, p0, LX/65c;->f:LX/663;

    const/4 v1, 0x5

    const/16 v3, 0x4000

    invoke-virtual {v0, v1, v2, v3}, LX/663;->a(III)LX/663;

    .line 1048026
    :goto_1
    iget-object v0, p0, LX/65c;->f:LX/663;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, LX/663;->f(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LX/65c;->d:J

    .line 1048027
    iget-object v0, p1, LX/65a;->a:Ljava/net/Socket;

    iput-object v0, p0, LX/65c;->h:Ljava/net/Socket;

    .line 1048028
    iget-object v0, p0, LX/65c;->g:LX/65s;

    iget-object v1, p1, LX/65a;->d:LX/670;

    iget-boolean v3, p0, LX/65c;->b:Z

    invoke-interface {v0, v1, v3}, LX/65s;->a(LX/670;Z)LX/65Z;

    move-result-object v0

    iput-object v0, p0, LX/65c;->i:LX/65Z;

    .line 1048029
    new-instance v0, Lokhttp3/internal/framed/FramedConnection$Reader;

    iget-object v1, p0, LX/65c;->g:LX/65s;

    iget-object v3, p1, LX/65a;->c:LX/671;

    iget-boolean v4, p0, LX/65c;->b:Z

    invoke-interface {v1, v3, v4}, LX/65s;->a(LX/671;Z)LX/65Y;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lokhttp3/internal/framed/FramedConnection$Reader;-><init>(LX/65c;LX/65Y;)V

    iput-object v0, p0, LX/65c;->j:Lokhttp3/internal/framed/FramedConnection$Reader;

    .line 1048030
    return-void

    :cond_3
    move v0, v1

    .line 1048031
    goto/16 :goto_0

    .line 1048032
    :cond_4
    iget-object v0, p0, LX/65c;->a:LX/64x;

    sget-object v1, LX/64x;->SPDY_3:LX/64x;

    if-ne v0, v1, :cond_5

    .line 1048033
    new-instance v0, LX/666;

    invoke-direct {v0}, LX/666;-><init>()V

    iput-object v0, p0, LX/65c;->g:LX/65s;

    .line 1048034
    const/4 v0, 0x0

    iput-object v0, p0, LX/65c;->s:Ljava/util/concurrent/ExecutorService;

    goto :goto_1

    .line 1048035
    :cond_5
    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, LX/65c;->a:LX/64x;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static a(LX/65c;ILjava/util/List;ZZ)LX/65i;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;ZZ)",
            "LX/65i;"
        }
    .end annotation

    .prologue
    .line 1047975
    if-nez p3, :cond_0

    const/4 v3, 0x1

    .line 1047976
    :goto_0
    if-nez p4, :cond_1

    const/4 v4, 0x1

    .line 1047977
    :goto_1
    iget-object v9, p0, LX/65c;->i:LX/65Z;

    monitor-enter v9

    .line 1047978
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1047979
    :try_start_1
    iget-boolean v0, p0, LX/65c;->r:Z

    if-eqz v0, :cond_2

    .line 1047980
    new-instance v0, Ljava/io/IOException;

    const-string v1, "shutdown"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047981
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 1047982
    :catchall_1
    move-exception v0

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 1047983
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 1047984
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 1047985
    :cond_2
    :try_start_3
    iget v1, p0, LX/65c;->q:I

    .line 1047986
    iget v0, p0, LX/65c;->q:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/65c;->q:I

    .line 1047987
    new-instance v0, LX/65i;

    move-object v2, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/65i;-><init>(ILX/65c;ZZLjava/util/List;)V

    .line 1047988
    if-eqz p3, :cond_3

    iget-wide v6, p0, LX/65c;->d:J

    const-wide/16 v10, 0x0

    cmp-long v2, v6, v10

    if-eqz v2, :cond_3

    iget-wide v6, v0, LX/65i;->b:J

    const-wide/16 v10, 0x0

    cmp-long v2, v6, v10

    if-nez v2, :cond_6

    :cond_3
    const/4 v2, 0x1

    move v8, v2

    .line 1047989
    :goto_2
    invoke-virtual {v0}, LX/65i;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1047990
    iget-object v2, p0, LX/65c;->n:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1047991
    :cond_4
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1047992
    if-nez p1, :cond_7

    .line 1047993
    :try_start_4
    iget-object v2, p0, LX/65c;->i:LX/65Z;

    move v5, v1

    move v6, p1

    move-object v7, p2

    invoke-interface/range {v2 .. v7}, LX/65Z;->a(ZZIILjava/util/List;)V

    .line 1047994
    :goto_3
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1047995
    if-eqz v8, :cond_5

    .line 1047996
    iget-object v1, p0, LX/65c;->i:LX/65Z;

    invoke-interface {v1}, LX/65Z;->b()V

    .line 1047997
    :cond_5
    return-object v0

    .line 1047998
    :cond_6
    const/4 v2, 0x0

    move v8, v2

    goto :goto_2

    .line 1047999
    :cond_7
    :try_start_5
    iget-boolean v2, p0, LX/65c;->b:Z

    if-eqz v2, :cond_8

    .line 1048000
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "client streams shouldn\'t have associated stream IDs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1048001
    :cond_8
    iget-object v2, p0, LX/65c;->i:LX/65Z;

    invoke-interface {v2, p1, v1, p2}, LX/65Z;->a(IILjava/util/List;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_3
.end method

.method private a(LX/65X;)V
    .locals 4

    .prologue
    .line 1047964
    iget-object v1, p0, LX/65c;->i:LX/65Z;

    monitor-enter v1

    .line 1047965
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047966
    :try_start_1
    iget-boolean v0, p0, LX/65c;->r:Z

    if-eqz v0, :cond_0

    .line 1047967
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1047968
    :goto_0
    return-void

    .line 1047969
    :cond_0
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, LX/65c;->r:Z

    .line 1047970
    iget v0, p0, LX/65c;->p:I

    .line 1047971
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1047972
    :try_start_4
    iget-object v2, p0, LX/65c;->i:LX/65Z;

    sget-object v3, LX/65A;->a:[B

    invoke-interface {v2, v0, p1, v3}, LX/65Z;->a(ILX/65X;[B)V

    .line 1047973
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 1047974
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/65c;ILX/671;IZ)V
    .locals 11

    .prologue
    .line 1047955
    new-instance v5, LX/672;

    invoke-direct {v5}, LX/672;-><init>()V

    .line 1047956
    int-to-long v0, p3

    invoke-interface {p2, v0, v1}, LX/671;->a(J)V

    .line 1047957
    int-to-long v0, p3

    invoke-interface {p2, v5, v0, v1}, LX/65D;->a(LX/672;J)J

    .line 1047958
    iget-wide v9, v5, LX/672;->b:J

    move-wide v0, v9

    .line 1047959
    int-to-long v2, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1047960
    iget-wide v9, v5, LX/672;->b:J

    move-wide v2, v9

    .line 1047961
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047962
    :cond_0
    iget-object v8, p0, LX/65c;->s:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lokhttp3/internal/framed/FramedConnection$6;

    const-string v2, "OkHttp %s Push Data[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, LX/65c;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lokhttp3/internal/framed/FramedConnection$6;-><init>(LX/65c;Ljava/lang/String;[Ljava/lang/Object;ILX/672;IZ)V

    const v1, -0x67568d25

    invoke-static {v8, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1047963
    return-void
.end method

.method public static a$redex0(LX/65c;ILjava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1047946
    monitor-enter p0

    .line 1047947
    :try_start_0
    iget-object v0, p0, LX/65c;->x:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1047948
    sget-object v0, LX/65X;->PROTOCOL_ERROR:LX/65X;

    invoke-virtual {p0, p1, v0}, LX/65c;->a(ILX/65X;)V

    .line 1047949
    monitor-exit p0

    .line 1047950
    :goto_0
    return-void

    .line 1047951
    :cond_0
    iget-object v0, p0, LX/65c;->x:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1047952
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047953
    iget-object v6, p0, LX/65c;->s:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lokhttp3/internal/framed/FramedConnection$4;

    const-string v2, "OkHttp %s Push Request[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, LX/65c;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lokhttp3/internal/framed/FramedConnection$4;-><init>(LX/65c;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;)V

    const v1, -0x2bde74b4

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0

    .line 1047954
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a$redex0(LX/65c;ILjava/util/List;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1047944
    iget-object v7, p0, LX/65c;->s:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lokhttp3/internal/framed/FramedConnection$5;

    const-string v2, "OkHttp %s Push Headers[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, LX/65c;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lokhttp3/internal/framed/FramedConnection$5;-><init>(LX/65c;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;Z)V

    const v1, -0x59669991

    invoke-static {v7, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1047945
    return-void
.end method

.method public static a$redex0(LX/65c;LX/65X;LX/65X;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1047914
    sget-boolean v0, LX/65c;->k:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1047915
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, LX/65c;->a(LX/65X;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    .line 1047916
    :goto_0
    monitor-enter p0

    .line 1047917
    :try_start_1
    iget-object v0, p0, LX/65c;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1047918
    iget-object v0, p0, LX/65c;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v4, p0, LX/65c;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [LX/65i;

    invoke-interface {v0, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/65i;

    .line 1047919
    iget-object v4, p0, LX/65c;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    move-object v5, v0

    .line 1047920
    :goto_1
    iget-object v0, p0, LX/65c;->t:Ljava/util/Map;

    if-eqz v0, :cond_7

    .line 1047921
    iget-object v0, p0, LX/65c;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, LX/65c;->t:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [LX/660;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/660;

    .line 1047922
    const/4 v2, 0x0

    iput-object v2, p0, LX/65c;->t:Ljava/util/Map;

    move-object v4, v0

    .line 1047923
    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1047924
    if-eqz v5, :cond_3

    .line 1047925
    array-length v6, v5

    move v2, v3

    move-object v0, v1

    :goto_3
    if-ge v2, v6, :cond_2

    aget-object v1, v5, v2

    .line 1047926
    :try_start_2
    invoke-virtual {v1, p2}, LX/65i;->a(LX/65X;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1047927
    :cond_1
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 1047928
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1047929
    goto :goto_0

    .line 1047930
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1047931
    :catch_1
    move-exception v1

    .line 1047932
    if-eqz v0, :cond_1

    move-object v0, v1

    goto :goto_4

    :cond_2
    move-object v1, v0

    .line 1047933
    :cond_3
    if-eqz v4, :cond_4

    .line 1047934
    array-length v2, v4

    move v0, v3

    :goto_5
    if-ge v0, v2, :cond_4

    aget-object v3, v4, v0

    .line 1047935
    invoke-virtual {v3}, LX/660;->c()V

    .line 1047936
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1047937
    :cond_4
    :try_start_4
    iget-object v0, p0, LX/65c;->i:LX/65Z;

    invoke-interface {v0}, LX/65Z;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move-object v0, v1

    .line 1047938
    :cond_5
    :goto_6
    :try_start_5
    iget-object v1, p0, LX/65c;->h:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1047939
    :goto_7
    if-eqz v0, :cond_6

    throw v0

    .line 1047940
    :catch_2
    move-exception v0

    .line 1047941
    if-eqz v1, :cond_5

    move-object v0, v1

    goto :goto_6

    .line 1047942
    :cond_6
    return-void

    .line 1047943
    :catch_3
    move-exception v0

    goto :goto_7

    :cond_7
    move-object v4, v2

    goto :goto_2

    :cond_8
    move-object v5, v2

    goto :goto_1
.end method

.method public static b$redex0(LX/65c;ZIILX/660;)V
    .locals 2

    .prologue
    .line 1047910
    iget-object v1, p0, LX/65c;->i:LX/65Z;

    monitor-enter v1

    .line 1047911
    if-eqz p4, :cond_0

    :try_start_0
    invoke-virtual {p4}, LX/660;->a()V

    .line 1047912
    :cond_0
    iget-object v0, p0, LX/65c;->i:LX/65Z;

    invoke-interface {v0, p1, p2, p3}, LX/65Z;->a(ZII)V

    .line 1047913
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static declared-synchronized c$redex0(LX/65c;I)LX/660;
    .locals 2

    .prologue
    .line 1047869
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65c;->t:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/65c;->t:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/660;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static d(LX/65c;I)Z
    .locals 2

    .prologue
    .line 1047909
    iget-object v0, p0, LX/65c;->a:LX/64x;

    sget-object v1, LX/64x;->HTTP_2:LX/64x;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(I)LX/65i;
    .locals 2

    .prologue
    .line 1047908
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65c;->n:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(IJ)V
    .locals 8

    .prologue
    .line 1047906
    sget-object v0, LX/65c;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lokhttp3/internal/framed/FramedConnection$2;

    const-string v3, "OkHttp Window Update %s stream %d"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v5, p0, LX/65c;->o:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    move-object v2, p0

    move v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Lokhttp3/internal/framed/FramedConnection$2;-><init>(LX/65c;Ljava/lang/String;[Ljava/lang/Object;IJ)V

    const v2, -0x362e513d

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1047907
    return-void
.end method

.method public final a(ILX/65X;)V
    .locals 7

    .prologue
    .line 1047904
    sget-object v6, LX/65c;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lokhttp3/internal/framed/FramedConnection$1;

    const-string v2, "OkHttp %s stream %d"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, LX/65c;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lokhttp3/internal/framed/FramedConnection$1;-><init>(LX/65c;Ljava/lang/String;[Ljava/lang/Object;ILX/65X;)V

    const v1, 0x5dc77d43

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1047905
    return-void
.end method

.method public final a(IZLX/672;J)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const-wide/16 v8, 0x0

    .line 1047886
    cmp-long v0, p4, v8

    if-nez v0, :cond_2

    .line 1047887
    iget-object v0, p0, LX/65c;->i:LX/65Z;

    invoke-interface {v0, p2, p1, p3, v1}, LX/65Z;->a(ZILX/672;I)V

    .line 1047888
    :cond_0
    return-void

    .line 1047889
    :cond_1
    :try_start_0
    iget-wide v2, p0, LX/65c;->d:J

    invoke-static {p4, p5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v0, v2

    .line 1047890
    iget-object v2, p0, LX/65c;->i:LX/65Z;

    invoke-interface {v2}, LX/65Z;->c()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1047891
    iget-wide v4, p0, LX/65c;->d:J

    int-to-long v6, v2

    sub-long/2addr v4, v6

    iput-wide v4, p0, LX/65c;->d:J

    .line 1047892
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047893
    int-to-long v4, v2

    sub-long/2addr p4, v4

    .line 1047894
    iget-object v3, p0, LX/65c;->i:LX/65Z;

    if-eqz p2, :cond_4

    cmp-long v0, p4, v8

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v3, v0, p1, p3, v2}, LX/65Z;->a(ZILX/672;I)V

    .line 1047895
    :cond_2
    cmp-long v0, p4, v8

    if-lez v0, :cond_0

    .line 1047896
    monitor-enter p0

    .line 1047897
    :goto_1
    :try_start_1
    iget-wide v2, p0, LX/65c;->d:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_1

    .line 1047898
    iget-object v0, p0, LX/65c;->n:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1047899
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1047900
    :catch_0
    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 1047901
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1047902
    :cond_3
    const v0, -0x70641dd8

    :try_start_3
    invoke-static {p0, v0}, LX/02L;->a(Ljava/lang/Object;I)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :cond_4
    move v0, v1

    .line 1047903
    goto :goto_0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 1047883
    iget-wide v0, p0, LX/65c;->d:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/65c;->d:J

    .line 1047884
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const v0, 0x14832254

    invoke-static {p0, v0}, LX/02L;->c(Ljava/lang/Object;I)V

    .line 1047885
    :cond_0
    return-void
.end method

.method public final declared-synchronized b()I
    .locals 4

    .prologue
    .line 1047880
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65c;->f:LX/663;

    const v1, 0x7fffffff

    .line 1047881
    iget v2, v0, LX/663;->a:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/663;->d:[I

    const/4 v3, 0x4

    aget v1, v2, v3

    :cond_0
    move v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047882
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)LX/65i;
    .locals 2

    .prologue
    .line 1047876
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/65c;->n:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65i;

    .line 1047877
    const v1, 0x78c4cecf

    invoke-static {p0, v1}, LX/02L;->c(Ljava/lang/Object;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047878
    monitor-exit p0

    return-object v0

    .line 1047879
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(ILX/65X;)V
    .locals 1

    .prologue
    .line 1047874
    iget-object v0, p0, LX/65c;->i:LX/65Z;

    invoke-interface {v0, p1, p2}, LX/65Z;->a(ILX/65X;)V

    .line 1047875
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1047872
    iget-object v0, p0, LX/65c;->i:LX/65Z;

    invoke-interface {v0}, LX/65Z;->b()V

    .line 1047873
    return-void
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 1047870
    sget-object v0, LX/65X;->NO_ERROR:LX/65X;

    sget-object v1, LX/65X;->CANCEL:LX/65X;

    invoke-static {p0, v0, v1}, LX/65c;->a$redex0(LX/65c;LX/65X;LX/65X;)V

    .line 1047871
    return-void
.end method
