.class public final LX/5Jo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field private final a:LX/5Jq;


# direct methods
.method public constructor <init>(LX/5Jq;)V
    .locals 0

    .prologue
    .line 897752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 897753
    iput-object p1, p0, LX/5Jo;->a:LX/5Jq;

    .line 897754
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 2

    .prologue
    .line 897744
    iget-object v0, p0, LX/5Jo;->a:LX/5Jq;

    iput p1, v0, LX/5Jq;->a:I

    .line 897745
    iget-object v0, p0, LX/5Jo;->a:LX/5Jq;

    .line 897746
    iget-object v1, v0, LX/3mY;->i:LX/3me;

    move-object v0, v1

    .line 897747
    check-cast v0, LX/5Jp;

    .line 897748
    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, LX/5Jp;->a(II)V

    .line 897749
    iget-object v0, p0, LX/5Jo;->a:LX/5Jq;

    iget-object v0, v0, LX/5Jq;->g:LX/0hc;

    if-eqz v0, :cond_0

    .line 897750
    iget-object v0, p0, LX/5Jo;->a:LX/5Jq;

    iget-object v0, v0, LX/5Jq;->g:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    .line 897751
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 5

    .prologue
    .line 897732
    iget-object v0, p0, LX/5Jo;->a:LX/5Jq;

    iget-object v0, v0, LX/5Jq;->f:Landroid/support/v4/view/ViewPager;

    .line 897733
    const/4 v1, 0x0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    .line 897734
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/components/ComponentView;

    .line 897735
    invoke-virtual {v1}, Lcom/facebook/components/ComponentView;->k()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 897736
    invoke-virtual {v1}, Lcom/facebook/components/ComponentView;->j()V

    .line 897737
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 897738
    :cond_1
    iget-object v0, p0, LX/5Jo;->a:LX/5Jq;

    iget-object v0, v0, LX/5Jq;->g:LX/0hc;

    if-eqz v0, :cond_2

    .line 897739
    iget-object v0, p0, LX/5Jo;->a:LX/5Jq;

    iget-object v0, v0, LX/5Jq;->g:LX/0hc;

    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    .line 897740
    :cond_2
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 897741
    iget-object v0, p0, LX/5Jo;->a:LX/5Jq;

    iget-object v0, v0, LX/5Jq;->g:LX/0hc;

    if-eqz v0, :cond_0

    .line 897742
    iget-object v0, p0, LX/5Jo;->a:LX/5Jq;

    iget-object v0, v0, LX/5Jq;->g:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    .line 897743
    :cond_0
    return-void
.end method
