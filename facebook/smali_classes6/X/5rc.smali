.class public final LX/5rc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5rT;


# instance fields
.field public final synthetic a:LX/5rl;

.field private final b:I

.field private final c:Lcom/facebook/react/bridge/Callback;


# direct methods
.method public constructor <init>(LX/5rl;ILcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 1011934
    iput-object p1, p0, LX/5rc;->a:LX/5rl;

    .line 1011935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1011936
    iput p2, p0, LX/5rc;->b:I

    .line 1011937
    iput-object p3, p0, LX/5rc;->c:Lcom/facebook/react/bridge/Callback;

    .line 1011938
    return-void
.end method

.method public synthetic constructor <init>(LX/5rl;ILcom/facebook/react/bridge/Callback;B)V
    .locals 0

    .prologue
    .line 1011925
    invoke-direct {p0, p1, p2, p3}, LX/5rc;-><init>(LX/5rl;ILcom/facebook/react/bridge/Callback;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1011926
    :try_start_0
    iget-object v0, p0, LX/5rc;->a:LX/5rl;

    iget-object v0, v0, LX/5rl;->b:LX/5qw;

    iget v1, p0, LX/5rc;->b:I

    iget-object v2, p0, LX/5rc;->a:LX/5rl;

    iget-object v2, v2, LX/5rl;->a:[I

    invoke-virtual {v0, v1, v2}, LX/5qw;->b(I[I)V
    :try_end_0
    .catch LX/5qz; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011927
    iget-object v0, p0, LX/5rc;->a:LX/5rl;

    iget-object v0, v0, LX/5rl;->a:[I

    aget v0, v0, v6

    int-to-float v0, v0

    invoke-static {v0}, LX/5r2;->c(F)F

    move-result v0

    .line 1011928
    iget-object v1, p0, LX/5rc;->a:LX/5rl;

    iget-object v1, v1, LX/5rl;->a:[I

    aget v1, v1, v7

    int-to-float v1, v1

    invoke-static {v1}, LX/5r2;->c(F)F

    move-result v1

    .line 1011929
    iget-object v2, p0, LX/5rc;->a:LX/5rl;

    iget-object v2, v2, LX/5rl;->a:[I

    aget v2, v2, v8

    int-to-float v2, v2

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    .line 1011930
    iget-object v3, p0, LX/5rc;->a:LX/5rl;

    iget-object v3, v3, LX/5rl;->a:[I

    aget v3, v3, v9

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    .line 1011931
    iget-object v4, p0, LX/5rc;->c:Lcom/facebook/react/bridge/Callback;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v9

    invoke-interface {v4, v5}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 1011932
    :goto_0
    return-void

    .line 1011933
    :catch_0
    iget-object v0, p0, LX/5rc;->c:Lcom/facebook/react/bridge/Callback;

    new-array v1, v6, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
