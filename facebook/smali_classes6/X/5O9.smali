.class public final enum LX/5O9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5O9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5O9;

.field public static final enum DB_FETCH:LX/5O9;

.field public static final enum GRAPHQL:LX/5O9;

.field public static final enum NOTIFICATION_PREFETCH:LX/5O9;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 907997
    new-instance v0, LX/5O9;

    const-string v1, "DB_FETCH"

    invoke-direct {v0, v1, v2}, LX/5O9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5O9;->DB_FETCH:LX/5O9;

    .line 907998
    new-instance v0, LX/5O9;

    const-string v1, "GRAPHQL"

    invoke-direct {v0, v1, v3}, LX/5O9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5O9;->GRAPHQL:LX/5O9;

    .line 907999
    new-instance v0, LX/5O9;

    const-string v1, "NOTIFICATION_PREFETCH"

    invoke-direct {v0, v1, v4}, LX/5O9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5O9;->NOTIFICATION_PREFETCH:LX/5O9;

    .line 908000
    const/4 v0, 0x3

    new-array v0, v0, [LX/5O9;

    sget-object v1, LX/5O9;->DB_FETCH:LX/5O9;

    aput-object v1, v0, v2

    sget-object v1, LX/5O9;->GRAPHQL:LX/5O9;

    aput-object v1, v0, v3

    sget-object v1, LX/5O9;->NOTIFICATION_PREFETCH:LX/5O9;

    aput-object v1, v0, v4

    sput-object v0, LX/5O9;->$VALUES:[LX/5O9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 907996
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/5O9;
    .locals 1

    .prologue
    .line 907995
    const-class v0, LX/5O9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5O9;

    return-object v0
.end method

.method public static values()[LX/5O9;
    .locals 1

    .prologue
    .line 907994
    sget-object v0, LX/5O9;->$VALUES:[LX/5O9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5O9;

    return-object v0
.end method
