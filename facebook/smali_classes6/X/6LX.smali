.class public final LX/6LX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/6LZ",
        "<TRESU",
        "LT;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Object;

.field public final synthetic b:I

.field public final synthetic c:LX/6Lb;


# direct methods
.method public constructor <init>(LX/6Lb;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 1078559
    iput-object p1, p0, LX/6LX;->c:LX/6Lb;

    iput-object p2, p0, LX/6LX;->a:Ljava/lang/Object;

    iput p3, p0, LX/6LX;->b:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1078545
    iget-object v0, p0, LX/6LX;->c:LX/6Lb;

    const/4 v1, 0x0

    .line 1078546
    iput-object v1, v0, LX/6Lb;->c:LX/1Mv;

    .line 1078547
    iget-object v0, p0, LX/6LX;->c:LX/6Lb;

    iget-object v0, v0, LX/6Lb;->d:LX/3Mb;

    iget-object v1, p0, LX/6LX;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, LX/3Mb;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1078548
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1078549
    check-cast p1, LX/6LZ;

    .line 1078550
    iget-object v0, p0, LX/6LX;->c:LX/6Lb;

    iget-object v1, p0, LX/6LX;->a:Ljava/lang/Object;

    iget v2, p0, LX/6LX;->b:I

    .line 1078551
    const/4 v3, 0x0

    iput-object v3, v0, LX/6Lb;->c:LX/1Mv;

    .line 1078552
    iget-object v3, v0, LX/6Lb;->d:LX/3Mb;

    iget-object v4, p1, LX/6LZ;->a:Ljava/lang/Object;

    invoke-interface {v3, v1, v4}, LX/3Mb;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1078553
    iget-object v3, p1, LX/6LZ;->b:LX/6La;

    sget-object v4, LX/6La;->FINAL:LX/6La;

    if-ne v3, v4, :cond_0

    .line 1078554
    iget-object v3, v0, LX/6Lb;->d:LX/3Mb;

    iget-object v4, p1, LX/6LZ;->a:Ljava/lang/Object;

    invoke-interface {v3, v1, v4}, LX/3Mb;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1078555
    :goto_0
    return-void

    .line 1078556
    :cond_0
    const/4 v3, 0x2

    if-lt v2, v3, :cond_1

    .line 1078557
    iget-object v3, v0, LX/6Lb;->d:LX/3Mb;

    new-instance v4, Ljava/lang/Exception;

    const-string p0, "Too many attempts"

    invoke-direct {v4, p0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v1, v4}, LX/3Mb;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 1078558
    :cond_1
    add-int/lit8 v3, v2, 0x1

    invoke-static {v0, v1, p1, v3}, LX/6Lb;->a$redex0(LX/6Lb;Ljava/lang/Object;LX/6LZ;I)V

    goto :goto_0
.end method
