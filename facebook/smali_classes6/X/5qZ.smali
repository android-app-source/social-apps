.class public LX/5qZ;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "I18nManager"
.end annotation


# instance fields
.field private final a:LX/5qa;


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 1009437
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 1009438
    invoke-static {}, LX/5qa;->a()LX/5qa;

    move-result-object v0

    iput-object v0, p0, LX/5qZ;->a:LX/5qa;

    .line 1009439
    return-void
.end method


# virtual methods
.method public allowRTL(Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1009434
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v0

    .line 1009435
    invoke-static {v1, p1}, LX/5qa;->a(Landroid/content/Context;Z)V

    .line 1009436
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1009440
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 1009441
    invoke-virtual {v0}, LX/5pY;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    .line 1009442
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1009443
    invoke-static {}, LX/5ps;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 1009444
    const-string v2, "isRTL"

    iget-object v3, p0, LX/5qZ;->a:LX/5qa;

    .line 1009445
    iget-object v4, p0, LX/5pb;->a:LX/5pY;

    move-object v4, v4

    .line 1009446
    invoke-virtual {v3, v4}, LX/5qa;->a(Landroid/content/Context;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1009447
    const-string v2, "localeIdentifier"

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1009448
    return-object v1
.end method

.method public forceRTL(Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 1009431
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v0

    .line 1009432
    invoke-static {v1, p1}, LX/5qa;->b(Landroid/content/Context;Z)V

    .line 1009433
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1009430
    const-string v0, "I18nManager"

    return-object v0
.end method
