.class public LX/6DH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile j:LX/6DH;


# instance fields
.field public final b:LX/03V;

.field public final c:Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

.field public final d:Ljava/util/concurrent/Executor;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1064807
    const-class v0, LX/6DH;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6DH;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;Ljava/util/concurrent/Executor;LX/0Uh;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1064801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1064802
    iput-object p1, p0, LX/6DH;->b:LX/03V;

    .line 1064803
    iput-object p2, p0, LX/6DH;->c:Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    .line 1064804
    iput-object p3, p0, LX/6DH;->d:Ljava/util/concurrent/Executor;

    .line 1064805
    iput-object p4, p0, LX/6DH;->i:LX/0Uh;

    .line 1064806
    return-void
.end method

.method public static a(LX/0QB;)LX/6DH;
    .locals 7

    .prologue
    .line 1064788
    sget-object v0, LX/6DH;->j:LX/6DH;

    if-nez v0, :cond_1

    .line 1064789
    const-class v1, LX/6DH;

    monitor-enter v1

    .line 1064790
    :try_start_0
    sget-object v0, LX/6DH;->j:LX/6DH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1064791
    if-eqz v2, :cond_0

    .line 1064792
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1064793
    new-instance p0, LX/6DH;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;->b(LX/0QB;)Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    move-result-object v4

    check-cast v4, Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;

    invoke-static {v0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/6DH;-><init>(LX/03V;Lcom/facebook/browserextensions/common/GraphApiProxyProcessor;Ljava/util/concurrent/Executor;LX/0Uh;)V

    .line 1064794
    move-object v0, p0

    .line 1064795
    sput-object v0, LX/6DH;->j:LX/6DH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1064796
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1064797
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1064798
    :cond_1
    sget-object v0, LX/6DH;->j:LX/6DH;

    return-object v0

    .line 1064799
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1064800
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
