.class public final LX/6Eq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/6Co;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/6Co;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1066713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066714
    iput-object p1, p0, LX/6Eq;->a:LX/0QB;

    .line 1066715
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1066716
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6Eq;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1066702
    packed-switch p2, :pswitch_data_0

    .line 1066703
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1066704
    :pswitch_0
    new-instance v0, LX/6Cp;

    invoke-direct {v0}, LX/6Cp;-><init>()V

    .line 1066705
    const-class v1, Landroid/content/Context;

    invoke-interface {p1, v1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3770

    invoke-static {p1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x1827

    invoke-static {p1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const/16 p2, 0x1822

    invoke-static {p1, p2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    .line 1066706
    iput-object v1, v0, LX/6Cp;->a:LX/0Ot;

    iput-object v2, v0, LX/6Cp;->b:LX/0Ot;

    iput-object p0, v0, LX/6Cp;->c:LX/0Ot;

    iput-object p2, v0, LX/6Cp;->d:LX/0Ot;

    .line 1066707
    move-object v0, v0

    .line 1066708
    :goto_0
    return-object v0

    .line 1066709
    :pswitch_1
    new-instance v2, LX/6Ep;

    const-class v0, Landroid/content/Context;

    invoke-interface {p1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/6Ep;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 1066710
    move-object v0, v2

    .line 1066711
    goto :goto_0

    .line 1066712
    :pswitch_2
    invoke-static {p1}, Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;->b(LX/0QB;)Lcom/facebook/browserextensions/core/menuitems/InstantExperienceAddToHomeScreenMenuItemHandler;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1066701
    const/4 v0, 0x3

    return v0
.end method
