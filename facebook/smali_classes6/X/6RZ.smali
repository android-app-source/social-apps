.class public LX/6RZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile x:LX/6RZ;


# instance fields
.field private a:Ljava/text/DateFormat;

.field private b:Ljava/text/DateFormat;

.field private c:Ljava/util/TimeZone;

.field public final d:Landroid/content/Context;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/text/DateFormat;

.field public g:Ljava/text/DateFormat;

.field private h:Ljava/text/DateFormat;

.field private i:Ljava/text/DateFormat;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field private r:[Ljava/lang/String;

.field private s:Ljava/text/DateFormat;

.field private t:Ljava/text/DateFormat;

.field private u:Ljava/text/DateFormat;

.field private v:Ljava/lang/String;

.field private w:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/0Or;LX/1qw;)V
    .locals 3
    .param p4    # LX/0Or;
        .annotation build Lcom/facebook/events/dateformatter/annotation/EventsTimeZone;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;",
            "LX/1qw;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1089962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1089963
    iput-object p1, p0, LX/6RZ;->d:Landroid/content/Context;

    .line 1089964
    iput-object p2, p0, LX/6RZ;->e:LX/0Or;

    .line 1089965
    sget-object v0, LX/6Ra;->b:Ljava/lang/String;

    sget-object v1, LX/6Ra;->a:Ljava/lang/String;

    sget-object v2, LX/6Ra;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/6Ra;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1089966
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/TimeZone;

    invoke-static {p0, v0, v1}, LX/6RZ;->b(LX/6RZ;Ljava/util/Locale;Ljava/util/TimeZone;)V

    .line 1089967
    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    .line 1089968
    new-instance v1, LX/6RV;

    invoke-direct {v1, p0, v0}, LX/6RV;-><init>(LX/6RZ;Ljava/util/TimeZone;)V

    invoke-virtual {p5, v1}, LX/1qw;->a(LX/1rV;)V

    .line 1089969
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-interface {p4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/TimeZone;

    .line 1089970
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 1089971
    const-string p1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v2, p1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1089972
    new-instance p1, LX/6RW;

    invoke-direct {p1, p0, v0, v1}, LX/6RW;-><init>(LX/6RZ;Ljava/util/Locale;Ljava/util/TimeZone;)V

    .line 1089973
    iget-object p2, p0, LX/6RZ;->d:Landroid/content/Context;

    invoke-virtual {p2, p1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1089974
    return-void
.end method

.method private static a(II)I
    .locals 2

    .prologue
    .line 1089917
    rsub-int/lit8 v0, p1, 0x4

    .line 1089918
    if-gez v0, :cond_0

    .line 1089919
    add-int/lit8 v0, v0, 0x7

    .line 1089920
    :cond_0
    const v1, 0x253d8c    # 3.419992E-39f

    sub-int v0, v1, v0

    .line 1089921
    sub-int v0, p0, v0

    div-int/lit8 v0, v0, 0x7

    return v0
.end method

.method private static a(JLjava/util/TimeZone;)I
    .locals 2

    .prologue
    .line 1089922
    invoke-virtual {p2, p0, p1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    .line 1089923
    int-to-long v0, v0

    invoke-static {p0, p1, v0, v1}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/Calendar;Ljava/util/Calendar;I)I
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1089924
    invoke-virtual {p0, p1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1089925
    :goto_0
    invoke-virtual {p0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 1089926
    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1089927
    sub-int v2, v1, v0

    .line 1089928
    if-nez v2, :cond_1

    .line 1089929
    invoke-virtual {p1, p2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {p0, p2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 1089930
    :goto_1
    return v0

    :cond_0
    move-object v5, p0

    move-object p0, p1

    move-object p1, v5

    .line 1089931
    goto :goto_0

    .line 1089932
    :cond_1
    packed-switch p2, :pswitch_data_0

    .line 1089933
    invoke-virtual {p0, p2}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    .line 1089934
    :goto_2
    invoke-virtual {p0, p2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p1, p2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 1089935
    if-le v2, v4, :cond_2

    .line 1089936
    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v0, v2

    add-int/2addr v0, v1

    goto :goto_1

    .line 1089937
    :pswitch_0
    const/4 v0, 0x0

    .line 1089938
    goto :goto_2

    .line 1089939
    :pswitch_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1089940
    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/6RZ;
    .locals 9

    .prologue
    .line 1089941
    sget-object v0, LX/6RZ;->x:LX/6RZ;

    if-nez v0, :cond_1

    .line 1089942
    const-class v1, LX/6RZ;

    monitor-enter v1

    .line 1089943
    :try_start_0
    sget-object v0, LX/6RZ;->x:LX/6RZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1089944
    if-eqz v2, :cond_0

    .line 1089945
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1089946
    new-instance v3, LX/6RZ;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x2e4

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x1617

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1619

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/1qw;->a(LX/0QB;)LX/1qw;

    move-result-object v8

    check-cast v8, LX/1qw;

    invoke-direct/range {v3 .. v8}, LX/6RZ;-><init>(Landroid/content/Context;LX/0Or;LX/0Or;LX/0Or;LX/1qw;)V

    .line 1089947
    move-object v0, v3

    .line 1089948
    sput-object v0, LX/6RZ;->x:LX/6RZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1089949
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1089950
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1089951
    :cond_1
    sget-object v0, LX/6RZ;->x:LX/6RZ;

    return-object v0

    .line 1089952
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1089953
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6RZ;Ljava/util/Date;Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1089954
    if-eqz p2, :cond_0

    invoke-virtual {p3, p1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3, p2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1089955
    iget-object v0, p0, LX/6RZ;->n:Ljava/lang/String;

    .line 1089956
    :goto_0
    return-object v0

    .line 1089957
    :cond_0
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {p0, v1, v2, v3, v4}, LX/6RZ;->c(LX/6RZ;JJ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1089958
    goto :goto_0
.end method

.method private a(ZLjava/util/Date;Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 4
    .param p3    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1089959
    if-eqz p3, :cond_0

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v0

    sget-object v1, LX/6RY;->TODAY:LX/6RY;

    if-ne v0, v1, :cond_1

    .line 1089960
    :cond_0
    invoke-static {p0, p1, p2, p4}, LX/6RZ;->d(LX/6RZ;ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1089961
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/6RZ;->g:Ljava/text/DateFormat;

    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ZLjava/util/Date;Ljava/util/Date;Ljava/util/Date;Z)Ljava/lang/String;
    .locals 8
    .param p3    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1090077
    if-nez p1, :cond_0

    if-nez p3, :cond_1

    .line 1090078
    :cond_0
    invoke-static {p0, p1, p2, p4}, LX/6RZ;->d(LX/6RZ;ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1090079
    :goto_0
    return-object v0

    :cond_1
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1090080
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {p0, v1, v2, v3, v4}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v1

    sget-object v2, LX/6RY;->TODAY:LX/6RY;

    if-ne v1, v2, :cond_2

    .line 1090081
    invoke-static {p0, p1, p2, p4}, LX/6RZ;->d(LX/6RZ;ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1090082
    invoke-static {p0, p3}, LX/6RZ;->j(LX/6RZ;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 1090083
    iget-object v3, p0, LX/6RZ;->k:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v5

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1090084
    :goto_1
    move-object v0, v1

    .line 1090085
    goto :goto_0

    .line 1090086
    :cond_2
    if-eqz p5, :cond_3

    iget-object v1, p0, LX/6RZ;->g:Ljava/text/DateFormat;

    invoke-virtual {v1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 1090087
    :goto_2
    if-eqz p5, :cond_4

    iget-object v1, p0, LX/6RZ;->g:Ljava/text/DateFormat;

    invoke-virtual {v1, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1090088
    :goto_3
    iget-object v3, p0, LX/6RZ;->k:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v5

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1090089
    :cond_3
    invoke-virtual {p0, p2}, LX/6RZ;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_2

    .line 1090090
    :cond_4
    invoke-virtual {p0, p3}, LX/6RZ;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method private b(ZLjava/util/Date;Ljava/util/Date;Z)Ljava/lang/String;
    .locals 6
    .param p3    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1089975
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/6RZ;Ljava/util/Locale;Ljava/util/TimeZone;)V
    .locals 4

    .prologue
    .line 1089976
    iput-object p2, p0, LX/6RZ;->c:Ljava/util/TimeZone;

    .line 1089977
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    const v1, 0x7f081fda

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->j:Ljava/lang/String;

    .line 1089978
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    const v1, 0x7f081fdc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->k:Ljava/lang/String;

    .line 1089979
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    const v1, 0x7f081fdd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->l:Ljava/lang/String;

    .line 1089980
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    const v1, 0x7f080087

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->m:Ljava/lang/String;

    .line 1089981
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    const v1, 0x7f080088

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->n:Ljava/lang/String;

    .line 1089982
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    const v1, 0x7f081fde

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->o:Ljava/lang/String;

    .line 1089983
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    const v1, 0x7f081fdf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->p:Ljava/lang/String;

    .line 1089984
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    const v1, 0x7f081fe0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->q:Ljava/lang/String;

    .line 1089985
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    const v1, 0x7f081fe1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->v:Ljava/lang/String;

    .line 1089986
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f10003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->r:[Ljava/lang/String;

    .line 1089987
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, LX/6Ra;->a:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1089988
    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1089989
    move-object v0, v0

    .line 1089990
    iput-object v0, p0, LX/6RZ;->f:Ljava/text/DateFormat;

    .line 1089991
    invoke-static {p1, p2}, LX/6Ra;->a(Ljava/util/Locale;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->g:Ljava/text/DateFormat;

    .line 1089992
    iget-object v0, p0, LX/6RZ;->d:Landroid/content/Context;

    invoke-static {v0, p1, p2}, LX/6Ra;->b(Landroid/content/Context;Ljava/util/Locale;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/6RZ;->a:Ljava/text/DateFormat;

    .line 1089993
    iget-object v0, p0, LX/6RZ;->r:[Ljava/lang/String;

    .line 1089994
    const/4 v1, 0x3

    invoke-static {v1, p1}, Ljava/text/DateFormat;->getTimeInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v2

    .line 1089995
    instance-of v1, v2, Ljava/text/SimpleDateFormat;

    if-eqz v1, :cond_0

    .line 1089996
    new-instance v3, Ljava/text/DateFormatSymbols;

    invoke-direct {v3, p1}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 1089997
    invoke-virtual {v3, v0}, Ljava/text/DateFormatSymbols;->setAmPmStrings([Ljava/lang/String;)V

    move-object v1, v2

    .line 1089998
    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->setDateFormatSymbols(Ljava/text/DateFormatSymbols;)V

    .line 1089999
    :cond_0
    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090000
    move-object v0, v2

    .line 1090001
    iput-object v0, p0, LX/6RZ;->b:Ljava/text/DateFormat;

    .line 1090002
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, LX/6Ra;->c:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1090003
    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090004
    move-object v0, v0

    .line 1090005
    iput-object v0, p0, LX/6RZ;->s:Ljava/text/DateFormat;

    .line 1090006
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_2

    .line 1090007
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM yyyy"

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1090008
    :goto_0
    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090009
    move-object v0, v0

    .line 1090010
    iput-object v0, p0, LX/6RZ;->t:Ljava/text/DateFormat;

    .line 1090011
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_3

    .line 1090012
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM d"

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1090013
    :goto_1
    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090014
    move-object v0, v0

    .line 1090015
    iput-object v0, p0, LX/6RZ;->u:Ljava/text/DateFormat;

    .line 1090016
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM"

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1090017
    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090018
    move-object v0, v0

    .line 1090019
    iput-object v0, p0, LX/6RZ;->h:Ljava/text/DateFormat;

    .line 1090020
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "d"

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1090021
    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090022
    move-object v0, v0

    .line 1090023
    iput-object v0, p0, LX/6RZ;->i:Ljava/text/DateFormat;

    .line 1090024
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1090025
    const/4 v0, 0x1

    iput v0, p0, LX/6RZ;->w:I

    .line 1090026
    :goto_2
    return-void

    .line 1090027
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/6RZ;->w:I

    goto :goto_2

    .line 1090028
    :cond_2
    const-string v0, "MMMMyyyy"

    invoke-static {p1, v0}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1090029
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1090030
    invoke-virtual {v1, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090031
    move-object v0, v1

    .line 1090032
    goto :goto_0

    .line 1090033
    :cond_3
    const-string v0, "MMMd"

    invoke-static {p1, v0}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1090034
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1090035
    invoke-virtual {v1, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1090036
    move-object v0, v1

    .line 1090037
    goto :goto_1
.end method

.method public static c(LX/6RZ;JJ)Ljava/lang/String;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1090038
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 1090039
    invoke-virtual {v3, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1090040
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 1090041
    invoke-virtual {v4, p3, p4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1090042
    sub-long v6, p3, p1

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    .line 1090043
    cmp-long v0, p3, p1

    if-lez v0, :cond_0

    move v0, v1

    .line 1090044
    :goto_0
    iget-object v5, p0, LX/6RZ;->d:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 1090045
    const-wide v8, 0x757b12c00L

    cmp-long v8, v6, v8

    if-lez v8, :cond_2

    .line 1090046
    if-eqz v0, :cond_1

    const v0, 0x7f081fe4

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1090047
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 1090048
    goto :goto_0

    .line 1090049
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1090050
    :cond_2
    const-wide/32 v8, 0x36ee80

    cmp-long v8, v6, v8

    if-gez v8, :cond_5

    .line 1090051
    invoke-static {v6, v7}, LX/1lQ;->d(J)J

    move-result-wide v6

    long-to-int v3, v6

    .line 1090052
    if-eqz v0, :cond_4

    .line 1090053
    if-nez v3, :cond_3

    iget-object v0, p0, LX/6RZ;->n:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const v0, 0x7f0f00ed

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v5, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1090054
    :cond_4
    const v0, 0x7f0f00ec

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v5, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1090055
    :cond_5
    const-wide/32 v8, 0x6ddd00

    cmp-long v8, v6, v8

    if-gez v8, :cond_7

    .line 1090056
    invoke-static {v6, v7}, LX/1lQ;->c(J)J

    move-result-wide v6

    long-to-int v3, v6

    .line 1090057
    if-eqz v0, :cond_6

    const v0, 0x7f0f00ef

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v5, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_6
    const v0, 0x7f0f00ee

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v5, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1090058
    :cond_7
    const/4 v6, 0x6

    invoke-static {v3, v4, v6}, LX/6RZ;->a(Ljava/util/Calendar;Ljava/util/Calendar;I)I

    move-result v6

    .line 1090059
    const/4 v7, 0x3

    invoke-static {v3, v4, v7}, LX/6RZ;->a(Ljava/util/Calendar;Ljava/util/Calendar;I)I

    move-result v7

    .line 1090060
    const/4 v8, 0x2

    invoke-static {v3, v4, v8}, LX/6RZ;->a(Ljava/util/Calendar;Ljava/util/Calendar;I)I

    move-result v3

    .line 1090061
    if-nez v6, :cond_8

    .line 1090062
    const v0, 0x7f081fdf

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1090063
    :cond_8
    if-ne v6, v1, :cond_a

    .line 1090064
    if-eqz v0, :cond_9

    const v0, 0x7f081fde

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    const v0, 0x7f081fe0

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1090065
    :cond_a
    if-nez v7, :cond_c

    .line 1090066
    if-eqz v0, :cond_b

    const v0, 0x7f081fe3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v5, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_b
    const v0, 0x7f081fe2

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1090067
    :cond_c
    const/4 v4, 0x4

    if-lt v7, v4, :cond_d

    if-nez v3, :cond_f

    .line 1090068
    :cond_d
    if-eqz v0, :cond_e

    const v0, 0x7f0f00f1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v5, v0, v7, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_e
    const v0, 0x7f0f00f0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v5, v0, v7, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 1090069
    :cond_f
    if-eqz v0, :cond_10

    const v0, 0x7f0f00f3

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v5, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_10
    const v0, 0x7f0f00f2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v5, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public static d(LX/6RZ;ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1090070
    if-eqz p1, :cond_0

    .line 1090071
    invoke-virtual {p0, p2, p3}, LX/6RZ;->b(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1090072
    :goto_0
    return-object v0

    .line 1090073
    :cond_0
    invoke-virtual {p0, p2, p3}, LX/6RZ;->b(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1090074
    invoke-static {p0, p2}, LX/6RZ;->j(LX/6RZ;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1090075
    iget-object v2, p0, LX/6RZ;->j:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v0, v3, p1

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1090076
    goto :goto_0
.end method

.method public static j(LX/6RZ;Ljava/util/Date;)Ljava/lang/String;
    .locals 2
    .param p0    # LX/6RZ;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1089893
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1089894
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1089895
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 1089896
    if-eqz v0, :cond_0

    .line 1089897
    iget-object v0, p0, LX/6RZ;->b:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1089898
    :goto_0
    return-object v0

    .line 1089899
    :cond_0
    iget-object v0, p0, LX/6RZ;->a:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)LX/6RY;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1089900
    iget-object v0, p0, LX/6RZ;->c:Ljava/util/TimeZone;

    invoke-static {p3, p4, v0}, LX/6RZ;->a(JLjava/util/TimeZone;)I

    move-result v0

    .line 1089901
    iget-object v1, p0, LX/6RZ;->c:Ljava/util/TimeZone;

    invoke-static {p1, p2, v1}, LX/6RZ;->a(JLjava/util/TimeZone;)I

    move-result v1

    .line 1089902
    if-le v0, v1, :cond_1

    .line 1089903
    sub-int/2addr v0, v1

    if-ne v0, v3, :cond_0

    .line 1089904
    sget-object v0, LX/6RY;->YESTERDAY:LX/6RY;

    .line 1089905
    :goto_0
    return-object v0

    .line 1089906
    :cond_0
    sget-object v0, LX/6RY;->PAST:LX/6RY;

    goto :goto_0

    .line 1089907
    :cond_1
    if-ne v0, v1, :cond_2

    .line 1089908
    sget-object v0, LX/6RY;->TODAY:LX/6RY;

    goto :goto_0

    .line 1089909
    :cond_2
    sub-int v2, v1, v0

    if-ne v2, v3, :cond_3

    .line 1089910
    sget-object v0, LX/6RY;->TOMORROW:LX/6RY;

    goto :goto_0

    .line 1089911
    :cond_3
    iget v2, p0, LX/6RZ;->w:I

    invoke-static {v1, v2}, LX/6RZ;->a(II)I

    move-result v1

    iget v2, p0, LX/6RZ;->w:I

    invoke-static {v0, v2}, LX/6RZ;->a(II)I

    move-result v0

    sub-int v0, v1, v0

    .line 1089912
    if-nez v0, :cond_4

    .line 1089913
    sget-object v0, LX/6RY;->THIS_WEEK:LX/6RY;

    goto :goto_0

    .line 1089914
    :cond_4
    if-ne v0, v3, :cond_5

    .line 1089915
    sget-object v0, LX/6RY;->NEXT_WEEK:LX/6RY;

    goto :goto_0

    .line 1089916
    :cond_5
    sget-object v0, LX/6RY;->FUTURE:LX/6RY;

    goto :goto_0
.end method

.method public final a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .param p3    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1089857
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZLjava/util/Date;Ljava/util/Date;Z)Ljava/lang/String;
    .locals 4
    .param p3    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1089859
    if-eqz p3, :cond_0

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v0

    sget-object v1, LX/6RY;->TODAY:LX/6RY;

    if-eq v0, v1, :cond_0

    .line 1089860
    invoke-direct {p0, p1, p2, p3, p4}, LX/6RZ;->b(ZLjava/util/Date;Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v0

    .line 1089861
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1089862
    iget-object v0, p0, LX/6RZ;->u:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1089863
    sget-object v0, LX/6RX;->a:[I

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v1

    invoke-virtual {v1}, LX/6RY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1089864
    iget-object v0, p0, LX/6RZ;->g:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1089865
    :pswitch_0
    iget-object v0, p0, LX/6RZ;->g:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1089866
    :pswitch_1
    iget-object v0, p0, LX/6RZ;->o:Ljava/lang/String;

    goto :goto_0

    .line 1089867
    :pswitch_2
    iget-object v0, p0, LX/6RZ;->p:Ljava/lang/String;

    goto :goto_0

    .line 1089868
    :pswitch_3
    iget-object v0, p0, LX/6RZ;->q:Ljava/lang/String;

    goto :goto_0

    .line 1089869
    :pswitch_4
    iget-object v0, p0, LX/6RZ;->f:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .param p3    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1089870
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LX/6RZ;->b(ZLjava/util/Date;Ljava/util/Date;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1089858
    iget-object v0, p0, LX/6RZ;->h:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;
    .locals 9
    .param p3    # Ljava/util/Date;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1089871
    if-eqz p1, :cond_0

    .line 1089872
    iget-object v0, p0, LX/6RZ;->g:Ljava/text/DateFormat;

    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1089873
    :goto_0
    return-object v0

    .line 1089874
    :cond_0
    iget-object v0, p0, LX/6RZ;->g:Ljava/text/DateFormat;

    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1089875
    invoke-static {p0, p2}, LX/6RZ;->j(LX/6RZ;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1089876
    iget-object v2, p0, LX/6RZ;->j:Ljava/lang/String;

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1089877
    if-nez p3, :cond_1

    move-object v0, v1

    .line 1089878
    goto :goto_0

    .line 1089879
    :cond_1
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p0, v2, v3, v4, v5}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v0

    sget-object v2, LX/6RY;->TODAY:LX/6RY;

    if-ne v0, v2, :cond_2

    .line 1089880
    invoke-static {p0, p3}, LX/6RZ;->j(LX/6RZ;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1089881
    :goto_1
    iget-object v2, p0, LX/6RZ;->k:Ljava/lang/String;

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1089882
    :cond_2
    iget-object v0, p0, LX/6RZ;->g:Ljava/text/DateFormat;

    invoke-virtual {v0, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1089883
    invoke-static {p0, p3}, LX/6RZ;->j(LX/6RZ;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 1089884
    iget-object v3, p0, LX/6RZ;->j:Ljava/lang/String;

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v6

    aput-object v2, v4, v7

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final d(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1089885
    iget-object v0, p0, LX/6RZ;->i:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1089886
    iget-object v0, p0, LX/6RZ;->f:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1089887
    iget-object v0, p0, LX/6RZ;->s:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/util/Date;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1089888
    iget-object v0, p0, LX/6RZ;->t:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h(Ljava/util/Date;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1089889
    iget-object v0, p0, LX/6RZ;->v:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, LX/6RZ;->f(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/util/Date;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1089890
    invoke-virtual {p0, p1}, LX/6RZ;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1089891
    invoke-static {p0, p1}, LX/6RZ;->j(LX/6RZ;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1089892
    iget-object v2, p0, LX/6RZ;->j:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
