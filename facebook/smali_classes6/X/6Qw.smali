.class public LX/6Qw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile f:LX/6Qw;


# instance fields
.field private b:LX/0Zb;

.field public c:LX/03V;

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/17d;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1088279
    const-class v0, LX/6Qw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Qw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/03V;LX/0Ot;LX/17d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/17d;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1088273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1088274
    iput-object p1, p0, LX/6Qw;->b:LX/0Zb;

    .line 1088275
    iput-object p2, p0, LX/6Qw;->c:LX/03V;

    .line 1088276
    iput-object p3, p0, LX/6Qw;->d:LX/0Ot;

    .line 1088277
    iput-object p4, p0, LX/6Qw;->e:LX/17d;

    .line 1088278
    return-void
.end method

.method public static a(LX/0QB;)LX/6Qw;
    .locals 7

    .prologue
    .line 1088260
    sget-object v0, LX/6Qw;->f:LX/6Qw;

    if-nez v0, :cond_1

    .line 1088261
    const-class v1, LX/6Qw;

    monitor-enter v1

    .line 1088262
    :try_start_0
    sget-object v0, LX/6Qw;->f:LX/6Qw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1088263
    if-eqz v2, :cond_0

    .line 1088264
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1088265
    new-instance v6, LX/6Qw;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0x97

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v5

    check-cast v5, LX/17d;

    invoke-direct {v6, v3, v4, p0, v5}, LX/6Qw;-><init>(LX/0Zb;LX/03V;LX/0Ot;LX/17d;)V

    .line 1088266
    move-object v0, v6

    .line 1088267
    sput-object v0, LX/6Qw;->f:LX/6Qw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1088268
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1088269
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1088270
    :cond_1
    sget-object v0, LX/6Qw;->f:LX/6Qw;

    return-object v0

    .line 1088271
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1088272
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/47G;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1088256
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    .line 1088257
    const-string v1, "route_to_google_reason"

    invoke-virtual {v0, v1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1088258
    const-string v1, "neko_di_install_routed_to_google_play"

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-virtual {p0, v1, p1, v0}, LX/6Qw;->a(Ljava/lang/String;LX/47G;LX/0P1;)V

    .line 1088259
    return-void
.end method

.method public final a(Ljava/lang/String;LX/47G;LX/0P1;)V
    .locals 3
    .param p2    # LX/47G;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/47G;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 1088243
    iget-object v0, p0, LX/6Qw;->b:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1088244
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1088245
    if-eqz p2, :cond_0

    .line 1088246
    const-string v1, "progress"

    iget-object v2, p2, LX/47G;->n:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1088247
    const-string v1, "story_cache_id"

    iget-object v2, p2, LX/47G;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1088248
    const-string v1, "package_name"

    iget-object v2, p2, LX/47G;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1088249
    iget-object v1, p2, LX/47G;->s:Ljava/util/Map;

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 1088250
    iget-object v1, p2, LX/47G;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1088251
    iget-object v1, p2, LX/47G;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 1088252
    :cond_0
    if-eqz p3, :cond_1

    .line 1088253
    invoke-virtual {v0, p3}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 1088254
    :cond_1
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1088255
    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/47G;)V
    .locals 1

    .prologue
    .line 1088241
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LX/6Qw;->a(Ljava/lang/String;Ljava/lang/String;LX/47G;Ljava/util/Map;)V

    .line 1088242
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/47G;Ljava/util/Map;)V
    .locals 2
    .param p4    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/47G;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 1088235
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    .line 1088236
    const-string v1, "dialog"

    invoke-virtual {v0, v1, p2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1088237
    if-eqz p4, :cond_0

    .line 1088238
    invoke-virtual {v0, p4}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 1088239
    :cond_0
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-virtual {p0, p1, p3, v0}, LX/6Qw;->a(Ljava/lang/String;LX/47G;LX/0P1;)V

    .line 1088240
    return-void
.end method
