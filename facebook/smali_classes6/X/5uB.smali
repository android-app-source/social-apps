.class public final LX/5uB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 1018074
    const-wide/16 v12, 0x0

    .line 1018075
    const/4 v11, 0x0

    .line 1018076
    const/4 v10, 0x0

    .line 1018077
    const/4 v9, 0x0

    .line 1018078
    const/4 v8, 0x0

    .line 1018079
    const/4 v7, 0x0

    .line 1018080
    const/4 v6, 0x0

    .line 1018081
    const/4 v5, 0x0

    .line 1018082
    const/4 v4, 0x0

    .line 1018083
    const/4 v3, 0x0

    .line 1018084
    const/4 v2, 0x0

    .line 1018085
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 1018086
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1018087
    const/4 v2, 0x0

    .line 1018088
    :goto_0
    return v2

    .line 1018089
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_a

    .line 1018090
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1018091
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1018092
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1018093
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1018094
    const/4 v2, 0x1

    .line 1018095
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1018096
    :cond_1
    const-string v6, "creator"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1018097
    invoke-static/range {p0 .. p1}, LX/5u6;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 1018098
    :cond_2
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1018099
    invoke-static/range {p0 .. p1}, LX/4aV;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 1018100
    :cond_3
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1018101
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 1018102
    :cond_4
    const-string v6, "page_rating"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1018103
    const/4 v2, 0x1

    .line 1018104
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v13, v6

    goto :goto_1

    .line 1018105
    :cond_5
    const-string v6, "privacy_scope"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1018106
    invoke-static/range {p0 .. p1}, LX/5uE;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 1018107
    :cond_6
    const-string v6, "reviewer_context"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1018108
    invoke-static/range {p0 .. p1}, LX/5uA;->a(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1018109
    :cond_7
    const-string v6, "story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1018110
    invoke-static/range {p0 .. p1}, LX/5u7;->a(LX/15w;LX/186;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 1018111
    :cond_8
    const-string v6, "value"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1018112
    invoke-static/range {p0 .. p1}, LX/5u1;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 1018113
    :cond_9
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1018114
    :cond_a
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1018115
    if-eqz v3, :cond_b

    .line 1018116
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1018117
    :cond_b
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1018118
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1018119
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1018120
    if-eqz v8, :cond_c

    .line 1018121
    const/4 v2, 0x4

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13, v3}, LX/186;->a(III)V

    .line 1018122
    :cond_c
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1018123
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1018124
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1018125
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1018126
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_d
    move v14, v9

    move v15, v10

    move/from16 v16, v11

    move v11, v6

    move v9, v4

    move v10, v5

    move-wide v4, v12

    move v12, v7

    move v13, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 1018127
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1018128
    invoke-virtual {p0, p1, v3, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1018129
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 1018130
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1018131
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1018132
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1018133
    if-eqz v0, :cond_1

    .line 1018134
    const-string v1, "creator"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1018135
    invoke-static {p0, v0, p2, p3}, LX/5u6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1018136
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1018137
    if-eqz v0, :cond_2

    .line 1018138
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1018139
    invoke-static {p0, v0, p2, p3}, LX/4aV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1018140
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1018141
    if-eqz v0, :cond_3

    .line 1018142
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1018143
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1018144
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 1018145
    if-eqz v0, :cond_4

    .line 1018146
    const-string v1, "page_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1018147
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1018148
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1018149
    if-eqz v0, :cond_5

    .line 1018150
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1018151
    invoke-static {p0, v0, p2, p3}, LX/5uE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1018152
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1018153
    if-eqz v0, :cond_6

    .line 1018154
    const-string v1, "reviewer_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1018155
    invoke-static {p0, v0, p2, p3}, LX/5uA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1018156
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1018157
    if-eqz v0, :cond_7

    .line 1018158
    const-string v1, "story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1018159
    invoke-static {p0, v0, p2}, LX/5u7;->a(LX/15i;ILX/0nX;)V

    .line 1018160
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1018161
    if-eqz v0, :cond_8

    .line 1018162
    const-string v1, "value"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1018163
    invoke-static {p0, v0, p2}, LX/5u1;->a(LX/15i;ILX/0nX;)V

    .line 1018164
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1018165
    return-void
.end method
