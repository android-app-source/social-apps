.class public final LX/5L3;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 899405
    const-class v1, Lcom/facebook/composer/minutiae/graphql/FetchTaggableActivitiesGraphQLModels$FetchRankedVerbsQueryModel;

    const v0, -0x37c85cd5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchRankedVerbsQuery"

    const-string v6, "c480de1fbbc3356a46007350a8e2d6ba"

    const-string v7, "viewer"

    const-string v8, "10155069965781729"

    const-string v9, "10155259088156729"

    .line 899406
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 899407
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 899408
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 899409
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 899410
    sparse-switch v0, :sswitch_data_0

    .line 899411
    :goto_0
    return-object p1

    .line 899412
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 899413
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 899414
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 899415
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 899416
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 899417
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 899418
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 899419
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 899420
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 899421
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 899422
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 899423
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 899424
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 899425
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 899426
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 899427
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 899428
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f0f4707 -> :sswitch_0
        -0x789f2a94 -> :sswitch_3
        -0x6e761353 -> :sswitch_f
        -0x69f19a9a -> :sswitch_2
        -0x51484e72 -> :sswitch_1
        -0x3cc89b6d -> :sswitch_8
        -0xa0593af -> :sswitch_b
        0x1a19f -> :sswitch_4
        0x32c67c -> :sswitch_5
        0x23640cb -> :sswitch_c
        0x66f18c8 -> :sswitch_a
        0x14ed3c3b -> :sswitch_10
        0x4342e41b -> :sswitch_6
        0x4b3d9fb3 -> :sswitch_e
        0x54df6484 -> :sswitch_7
        0x630ddf64 -> :sswitch_d
        0x6ecd2753 -> :sswitch_9
    .end sparse-switch
.end method
