.class public LX/6Py;
.super LX/2g7;
.source ""


# instance fields
.field private a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1086572
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 1086573
    iput-object p1, p0, LX/6Py;->b:LX/0SG;

    .line 1086574
    iput-object p2, p0, LX/6Py;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1086575
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 6

    .prologue
    .line 1086576
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086577
    iget-object v0, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1086578
    iget-object v1, p0, LX/6Py;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dQ;->N:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 1086579
    iget-object v1, p0, LX/6Py;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 1086580
    int-to-long v0, v0

    const-wide/32 v4, 0xea60

    mul-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
