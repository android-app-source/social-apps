.class public final LX/5bI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 957813
    const/4 v10, 0x0

    .line 957814
    const/4 v9, 0x0

    .line 957815
    const/4 v8, 0x0

    .line 957816
    const/4 v7, 0x0

    .line 957817
    const/4 v6, 0x0

    .line 957818
    const/4 v5, 0x0

    .line 957819
    const/4 v4, 0x0

    .line 957820
    const/4 v3, 0x0

    .line 957821
    const/4 v2, 0x0

    .line 957822
    const/4 v1, 0x0

    .line 957823
    const/4 v0, 0x0

    .line 957824
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 957825
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 957826
    const/4 v0, 0x0

    .line 957827
    :goto_0
    return v0

    .line 957828
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 957829
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_8

    .line 957830
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 957831
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 957832
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 957833
    const-string v12, "count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 957834
    const/4 v3, 0x1

    .line 957835
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v10

    goto :goto_1

    .line 957836
    :cond_2
    const-string v12, "mute_until"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 957837
    const/4 v2, 0x1

    .line 957838
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v9

    goto :goto_1

    .line 957839
    :cond_3
    const-string v12, "nodes"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 957840
    invoke-static {p0, p1}, LX/5bG;->b(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 957841
    :cond_4
    const-string v12, "page_info"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 957842
    invoke-static {p0, p1}, LX/5bH;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 957843
    :cond_5
    const-string v12, "sync_sequence_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 957844
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 957845
    :cond_6
    const-string v12, "unread_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 957846
    const/4 v1, 0x1

    .line 957847
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 957848
    :cond_7
    const-string v12, "unseen_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 957849
    const/4 v0, 0x1

    .line 957850
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    goto :goto_1

    .line 957851
    :cond_8
    const/4 v11, 0x7

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 957852
    if-eqz v3, :cond_9

    .line 957853
    const/4 v3, 0x0

    const/4 v11, 0x0

    invoke-virtual {p1, v3, v10, v11}, LX/186;->a(III)V

    .line 957854
    :cond_9
    if-eqz v2, :cond_a

    .line 957855
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v9, v3}, LX/186;->a(III)V

    .line 957856
    :cond_a
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 957857
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 957858
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 957859
    if-eqz v1, :cond_b

    .line 957860
    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v5, v2}, LX/186;->a(III)V

    .line 957861
    :cond_b
    if-eqz v0, :cond_c

    .line 957862
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 957863
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 957864
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 957865
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 957866
    if-eqz v0, :cond_0

    .line 957867
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957868
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 957869
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 957870
    if-eqz v0, :cond_1

    .line 957871
    const-string v1, "mute_until"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957872
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 957873
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957874
    if-eqz v0, :cond_2

    .line 957875
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957876
    invoke-static {p0, v0, p2, p3}, LX/5bG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 957877
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957878
    if-eqz v0, :cond_3

    .line 957879
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957880
    invoke-static {p0, v0, p2}, LX/5bH;->a(LX/15i;ILX/0nX;)V

    .line 957881
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 957882
    if-eqz v0, :cond_4

    .line 957883
    const-string v1, "sync_sequence_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957884
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957885
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 957886
    if-eqz v0, :cond_5

    .line 957887
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957888
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 957889
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 957890
    if-eqz v0, :cond_6

    .line 957891
    const-string v1, "unseen_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957892
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 957893
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 957894
    return-void
.end method
