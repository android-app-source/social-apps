.class public final LX/5uK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1018822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;)LX/5uK;
    .locals 2

    .prologue
    .line 1018818
    new-instance v0, LX/5uK;

    invoke-direct {v0}, LX/5uK;-><init>()V

    .line 1018819
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->j()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    move-result-object v1

    iput-object v1, v0, LX/5uK;->a:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    .line 1018820
    invoke-virtual {p0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;->k()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    move-result-object v1

    iput-object v1, v0, LX/5uK;->b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    .line 1018821
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1018805
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1018806
    iget-object v1, p0, LX/5uK;->a:Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel$ReviewStoryModel;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1018807
    iget-object v3, p0, LX/5uK;->b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1018808
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1018809
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1018810
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1018811
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1018812
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1018813
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1018814
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1018815
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1018816
    new-instance v1, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;

    invoke-direct {v1, v0}, Lcom/facebook/reviews/protocol/graphql/UserReviewsFragmentsModels$UserReviewsModel$AuthoredReviewsModel$EdgesModel;-><init>(LX/15i;)V

    .line 1018817
    return-object v1
.end method
