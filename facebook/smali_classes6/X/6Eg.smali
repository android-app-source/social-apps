.class public LX/6Eg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6EB;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile d:LX/6Eg;


# instance fields
.field public b:Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;

.field public c:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1066454
    const-class v0, LX/6Eg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6Eg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1066452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066453
    return-void
.end method

.method public static a(LX/0QB;)LX/6Eg;
    .locals 4

    .prologue
    .line 1066455
    sget-object v0, LX/6Eg;->d:LX/6Eg;

    if-nez v0, :cond_1

    .line 1066456
    const-class v1, LX/6Eg;

    monitor-enter v1

    .line 1066457
    :try_start_0
    sget-object v0, LX/6Eg;->d:LX/6Eg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1066458
    if-eqz v2, :cond_0

    .line 1066459
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1066460
    new-instance p0, LX/6Eg;

    invoke-direct {p0}, LX/6Eg;-><init>()V

    .line 1066461
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    .line 1066462
    iput-object v3, p0, LX/6Eg;->c:LX/03V;

    .line 1066463
    move-object v0, p0

    .line 1066464
    sput-object v0, LX/6Eg;->d:LX/6Eg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1066465
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1066466
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1066467
    :cond_1
    sget-object v0, LX/6Eg;->d:LX/6Eg;

    return-object v0

    .line 1066468
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1066469
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6Cy;)V
    .locals 2

    .prologue
    .line 1066449
    iget-object v0, p0, LX/6Eg;->b:Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;

    if-eqz v0, :cond_0

    .line 1066450
    iget-object v0, p0, LX/6Eg;->b:Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;

    invoke-virtual {p1}, LX/6Cy;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(I)V

    .line 1066451
    :cond_0
    return-void
.end method

.method public final a(Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 1066436
    :try_start_0
    iget-object v0, p0, LX/6Eg;->b:Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;

    if-eqz v0, :cond_0

    .line 1066437
    iget-object v3, p0, LX/6Eg;->b:Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;

    iget-object v0, p0, LX/6Eg;->b:Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;

    invoke-virtual {v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e()Ljava/lang/String;

    move-result-object v4

    const-string v0, "latitude"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "latitude"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_0
    const-string v0, "longitude"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "longitude"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    const-string v0, "accuracy"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "accuracy"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1066438
    :goto_2
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1066439
    const-string p1, "callbackID"

    invoke-virtual {v5, p1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066440
    const-string p1, "latitude"

    invoke-virtual {v5, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066441
    const-string p1, "longitude"

    invoke-virtual {v5, p1, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066442
    const-string p1, "accuracy"

    invoke-virtual {v5, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066443
    move-object v0, v5

    .line 1066444
    invoke-virtual {v3, v0}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Landroid/os/Bundle;)V

    .line 1066445
    :cond_0
    :goto_3
    return-void

    .line 1066446
    :cond_1
    const-string v0, ""

    move-object v2, v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    move-object v1, v0

    goto :goto_1

    :cond_3
    const-string v0, ""
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1066447
    :catch_0
    move-exception v0

    .line 1066448
    iget-object v1, p0, LX/6Eg;->c:LX/03V;

    sget-object v2, LX/6Eg;->a:Ljava/lang/String;

    const-string v3, "Failed to create location result"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3
.end method
