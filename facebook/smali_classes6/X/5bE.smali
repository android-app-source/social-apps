.class public final LX/5bE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    .line 957362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 957363
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 957364
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 957365
    const/4 v2, 0x0

    .line 957366
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 957367
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 957368
    :goto_1
    move v1, v2

    .line 957369
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 957370
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 957371
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 957372
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 957373
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 957374
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 957375
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 957376
    const-string v5, "game_type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 957377
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    .line 957378
    :cond_3
    const-string v5, "high_score"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 957379
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 957380
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_b

    .line 957381
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 957382
    :goto_3
    move v1, v4

    .line 957383
    goto :goto_2

    .line 957384
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 957385
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 957386
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 957387
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_2

    .line 957388
    :cond_6
    const-string v9, "score"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 957389
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v5

    .line 957390
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 957391
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 957392
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 957393
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_7

    if-eqz v8, :cond_7

    .line 957394
    const-string v9, "player_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 957395
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 957396
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 957397
    :cond_9
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 957398
    invoke-virtual {p1, v4, v7}, LX/186;->b(II)V

    .line 957399
    if-eqz v1, :cond_a

    .line 957400
    invoke-virtual {p1, v5, v6, v4}, LX/186;->a(III)V

    .line 957401
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_3

    :cond_b
    move v1, v4

    move v6, v4

    move v7, v4

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 957402
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 957403
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 957404
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/5bE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 957405
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 957406
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 957407
    return-void
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 957408
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 957409
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 957410
    if-eqz v0, :cond_0

    .line 957411
    const-string v1, "game_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957412
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957413
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 957414
    if-eqz v0, :cond_3

    .line 957415
    const-string v1, "high_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957416
    const/4 p3, 0x0

    .line 957417
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 957418
    invoke-virtual {p0, v0, p3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 957419
    if-eqz v1, :cond_1

    .line 957420
    const-string p1, "player_id"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957421
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 957422
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, p3}, LX/15i;->a(III)I

    move-result v1

    .line 957423
    if-eqz v1, :cond_2

    .line 957424
    const-string p1, "score"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 957425
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 957426
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 957427
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 957428
    return-void
.end method
