.class public final LX/5z6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1033506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;
    .locals 15

    .prologue
    const/4 v4, 0x1

    const/4 v14, 0x0

    const/4 v2, 0x0

    .line 1033507
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1033508
    iget-object v1, p0, LX/5z6;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1033509
    iget-object v3, p0, LX/5z6;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1033510
    iget-object v5, p0, LX/5z6;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1033511
    iget-object v6, p0, LX/5z6;->e:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1033512
    iget-object v7, p0, LX/5z6;->f:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1033513
    iget-object v8, p0, LX/5z6;->g:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1033514
    iget-object v9, p0, LX/5z6;->h:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1033515
    iget-object v10, p0, LX/5z6;->i:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1033516
    iget-object v11, p0, LX/5z6;->j:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1033517
    iget-object v12, p0, LX/5z6;->k:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1033518
    const/16 v13, 0xb

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1033519
    invoke-virtual {v0, v14, v1}, LX/186;->b(II)V

    .line 1033520
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1033521
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1033522
    const/4 v1, 0x3

    iget-boolean v3, p0, LX/5z6;->d:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1033523
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1033524
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1033525
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1033526
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1033527
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1033528
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 1033529
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 1033530
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1033531
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1033532
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1033533
    invoke-virtual {v1, v14}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1033534
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1033535
    new-instance v1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;-><init>(LX/15i;)V

    .line 1033536
    return-object v1
.end method
