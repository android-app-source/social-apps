.class public final LX/6Sm;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 1096378
    const/16 v28, 0x0

    .line 1096379
    const/16 v27, 0x0

    .line 1096380
    const/16 v26, 0x0

    .line 1096381
    const/16 v25, 0x0

    .line 1096382
    const/16 v24, 0x0

    .line 1096383
    const/16 v23, 0x0

    .line 1096384
    const/16 v22, 0x0

    .line 1096385
    const/16 v21, 0x0

    .line 1096386
    const/16 v20, 0x0

    .line 1096387
    const/16 v19, 0x0

    .line 1096388
    const/16 v18, 0x0

    .line 1096389
    const/16 v17, 0x0

    .line 1096390
    const/16 v16, 0x0

    .line 1096391
    const/4 v15, 0x0

    .line 1096392
    const/4 v14, 0x0

    .line 1096393
    const/4 v13, 0x0

    .line 1096394
    const/4 v12, 0x0

    .line 1096395
    const/4 v11, 0x0

    .line 1096396
    const/4 v10, 0x0

    .line 1096397
    const/4 v9, 0x0

    .line 1096398
    const/4 v8, 0x0

    .line 1096399
    const/4 v7, 0x0

    .line 1096400
    const/4 v6, 0x0

    .line 1096401
    const/4 v5, 0x0

    .line 1096402
    const/4 v4, 0x0

    .line 1096403
    const/4 v3, 0x0

    .line 1096404
    const/4 v2, 0x0

    .line 1096405
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1

    .line 1096406
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1096407
    const/4 v2, 0x0

    .line 1096408
    :goto_0
    return v2

    .line 1096409
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1096410
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_12

    .line 1096411
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v29

    .line 1096412
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1096413
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1

    if-eqz v29, :cond_1

    .line 1096414
    const-string v30, "can_page_viewer_invite_post_likers"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2

    .line 1096415
    const/4 v11, 0x1

    .line 1096416
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto :goto_1

    .line 1096417
    :cond_2
    const-string v30, "can_see_voice_switcher"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_3

    .line 1096418
    const/4 v10, 0x1

    .line 1096419
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto :goto_1

    .line 1096420
    :cond_3
    const-string v30, "can_viewer_comment"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_4

    .line 1096421
    const/4 v9, 0x1

    .line 1096422
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v26

    goto :goto_1

    .line 1096423
    :cond_4
    const-string v30, "can_viewer_comment_with_photo"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_5

    .line 1096424
    const/4 v8, 0x1

    .line 1096425
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto :goto_1

    .line 1096426
    :cond_5
    const-string v30, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_6

    .line 1096427
    const/4 v7, 0x1

    .line 1096428
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto :goto_1

    .line 1096429
    :cond_6
    const-string v30, "can_viewer_comment_with_video"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_7

    .line 1096430
    const/4 v6, 0x1

    .line 1096431
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto :goto_1

    .line 1096432
    :cond_7
    const-string v30, "can_viewer_like"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_8

    .line 1096433
    const/4 v5, 0x1

    .line 1096434
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 1096435
    :cond_8
    const-string v30, "can_viewer_subscribe"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 1096436
    const/4 v4, 0x1

    .line 1096437
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 1096438
    :cond_9
    const-string v30, "comments_mirroring_domain"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_a

    .line 1096439
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 1096440
    :cond_a
    const-string v30, "does_viewer_like"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 1096441
    const/4 v3, 0x1

    .line 1096442
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v19

    goto/16 :goto_1

    .line 1096443
    :cond_b
    const-string v30, "id"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_c

    .line 1096444
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 1096445
    :cond_c
    const-string v30, "is_viewer_subscribed"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_d

    .line 1096446
    const/4 v2, 0x1

    .line 1096447
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 1096448
    :cond_d
    const-string v30, "legacy_api_post_id"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_e

    .line 1096449
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 1096450
    :cond_e
    const-string v30, "pinned_comment_events"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_f

    .line 1096451
    invoke-static/range {p0 .. p1}, LX/6TK;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1096452
    :cond_f
    const-string v30, "remixable_photo_uri"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_10

    .line 1096453
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 1096454
    :cond_10
    const-string v30, "video_timestamped_comments"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_11

    .line 1096455
    invoke-static/range {p0 .. p1}, LX/6TD;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1096456
    :cond_11
    const-string v30, "viewer_acts_as_page"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_0

    .line 1096457
    invoke-static/range {p0 .. p1}, LX/5AX;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1096458
    :cond_12
    const/16 v29, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1096459
    if-eqz v11, :cond_13

    .line 1096460
    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 1096461
    :cond_13
    if-eqz v10, :cond_14

    .line 1096462
    const/4 v10, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 1096463
    :cond_14
    if-eqz v9, :cond_15

    .line 1096464
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 1096465
    :cond_15
    if-eqz v8, :cond_16

    .line 1096466
    const/4 v8, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1096467
    :cond_16
    if-eqz v7, :cond_17

    .line 1096468
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1096469
    :cond_17
    if-eqz v6, :cond_18

    .line 1096470
    const/4 v6, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1096471
    :cond_18
    if-eqz v5, :cond_19

    .line 1096472
    const/4 v5, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 1096473
    :cond_19
    if-eqz v4, :cond_1a

    .line 1096474
    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1096475
    :cond_1a
    const/16 v4, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1096476
    if-eqz v3, :cond_1b

    .line 1096477
    const/16 v3, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1096478
    :cond_1b
    const/16 v3, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1096479
    if-eqz v2, :cond_1c

    .line 1096480
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1096481
    :cond_1c
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1096482
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1096483
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1096484
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1096485
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1096486
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1096487
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1096488
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096489
    if-eqz v0, :cond_0

    .line 1096490
    const-string v1, "can_page_viewer_invite_post_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096491
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096492
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096493
    if-eqz v0, :cond_1

    .line 1096494
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096495
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096496
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096497
    if-eqz v0, :cond_2

    .line 1096498
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096499
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096500
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096501
    if-eqz v0, :cond_3

    .line 1096502
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096503
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096504
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096505
    if-eqz v0, :cond_4

    .line 1096506
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096507
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096508
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096509
    if-eqz v0, :cond_5

    .line 1096510
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096511
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096512
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096513
    if-eqz v0, :cond_6

    .line 1096514
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096515
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096516
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096517
    if-eqz v0, :cond_7

    .line 1096518
    const-string v1, "can_viewer_subscribe"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096519
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096520
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096521
    if-eqz v0, :cond_8

    .line 1096522
    const-string v1, "comments_mirroring_domain"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096523
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096524
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096525
    if-eqz v0, :cond_9

    .line 1096526
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096527
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096528
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096529
    if-eqz v0, :cond_a

    .line 1096530
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096531
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096532
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1096533
    if-eqz v0, :cond_b

    .line 1096534
    const-string v1, "is_viewer_subscribed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096535
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1096536
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096537
    if-eqz v0, :cond_c

    .line 1096538
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096539
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096540
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096541
    if-eqz v0, :cond_d

    .line 1096542
    const-string v1, "pinned_comment_events"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096543
    invoke-static {p0, v0, p2, p3}, LX/6TK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096544
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1096545
    if-eqz v0, :cond_e

    .line 1096546
    const-string v1, "remixable_photo_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096547
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1096548
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096549
    if-eqz v0, :cond_f

    .line 1096550
    const-string v1, "video_timestamped_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096551
    invoke-static {p0, v0, p2, p3}, LX/6TD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096552
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1096553
    if-eqz v0, :cond_10

    .line 1096554
    const-string v1, "viewer_acts_as_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1096555
    invoke-static {p0, v0, p2, p3}, LX/5AX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1096556
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1096557
    return-void
.end method
