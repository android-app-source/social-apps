.class public final LX/5f3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public a:LX/2L0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 970965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/5f3;

    invoke-static {v0}, LX/2L0;->a(LX/0QB;)LX/2L0;

    move-result-object v0

    check-cast v0, LX/2L0;

    iput-object v0, p0, LX/5f3;->a:LX/2L0;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x59124da8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 970966
    invoke-static {p0, p1}, LX/5f3;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 970967
    const-string v1, "com.facebook.offlinemode.executor.OfflineMutationBroadcastReceiver.RETRY_MUTATIONS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 970968
    const/16 v1, 0x27

    const v2, -0x66964b6f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 970969
    :goto_0
    return-void

    .line 970970
    :cond_0
    iget-object v1, p0, LX/5f3;->a:LX/2L0;

    sget-object v2, LX/2LJ;->TIMER:LX/2LJ;

    invoke-virtual {v1, v2}, LX/2L0;->a(LX/2LJ;)V

    .line 970971
    const v1, 0x4d87be2c    # 2.84673408E8f

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
