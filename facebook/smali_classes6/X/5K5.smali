.class public LX/5K5;
.super LX/3me;
.source ""


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 898165
    invoke-direct {p0}, LX/3me;-><init>()V

    .line 898166
    const/4 v0, -0x1

    iput v0, p0, LX/5K5;->a:I

    .line 898167
    const/4 v0, 0x0

    iput v0, p0, LX/5K5;->b:I

    return-void
.end method

.method private c(II)V
    .locals 3

    .prologue
    .line 898168
    if-ltz p1, :cond_0

    .line 898169
    iget-object v0, p0, LX/3me;->a:LX/3mY;

    move-object v0, v0

    .line 898170
    invoke-virtual {v0}, LX/3mY;->e()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 898171
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid value for the first visible item position."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898172
    :cond_1
    if-ltz p2, :cond_2

    .line 898173
    iget-object v0, p0, LX/3me;->a:LX/3mY;

    move-object v0, v0

    .line 898174
    invoke-virtual {v0}, LX/3mY;->e()I

    move-result v0

    if-le p2, v0, :cond_3

    .line 898175
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid value for the item count."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 898176
    :cond_3
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 3

    .prologue
    .line 898177
    invoke-direct {p0, p1, p2}, LX/5K5;->c(II)V

    .line 898178
    iget v0, p0, LX/5K5;->a:I

    if-ne v0, p1, :cond_0

    iget v0, p0, LX/5K5;->b:I

    if-ne v0, p2, :cond_0

    .line 898179
    :goto_0
    return-void

    .line 898180
    :cond_0
    iput p1, p0, LX/5K5;->a:I

    .line 898181
    iput p2, p0, LX/5K5;->b:I

    .line 898182
    mul-int/lit8 v0, p2, 0x2

    .line 898183
    div-int/lit8 v1, v0, 0x2

    .line 898184
    sub-int/2addr v0, v1

    .line 898185
    const/4 v2, 0x0

    sub-int v1, p1, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 898186
    add-int/lit8 v2, p2, -0x1

    add-int/2addr v2, p1

    add-int/2addr v0, v2

    .line 898187
    iget-object v2, p0, LX/3me;->a:LX/3mY;

    move-object v2, v2

    .line 898188
    invoke-virtual {v2}, LX/3mY;->e()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 898189
    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 898190
    invoke-virtual {p0, v1, v0}, LX/3me;->b(II)V

    goto :goto_0
.end method
