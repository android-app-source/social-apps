.class public LX/64L;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/64J;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1pA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/33e;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1044469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1044470
    iput-object p1, p0, LX/64L;->a:LX/0Ot;

    .line 1044471
    iput-object p2, p0, LX/64L;->b:LX/0Ot;

    .line 1044472
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/64L;->c:Ljava/util/Set;

    .line 1044473
    return-void
.end method

.method public static b(LX/0QB;)LX/64L;
    .locals 3

    .prologue
    .line 1044482
    new-instance v0, LX/64L;

    const/16 v1, 0x13ec

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x13f1

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/64L;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1044483
    return-object v0
.end method


# virtual methods
.method public final a(LX/64J;)V
    .locals 1

    .prologue
    .line 1044480
    iget-object v0, p0, LX/64L;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1044481
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1044476
    iget-object v0, p0, LX/64L;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/33e;

    .line 1044477
    new-instance v3, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;

    iget-object v4, p0, LX/64L;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1pA;

    invoke-virtual {v4}, LX/1pA;->a()Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;

    move-result-object v4

    iget-object v5, p0, LX/64L;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1pA;

    invoke-virtual {v5}, LX/1pA;->b()Ljava/lang/String;

    move-result-object v5

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;-><init>(Lcom/facebook/zero/sdk/util/CarrierAndSimMccMnc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 1044478
    new-instance v2, LX/64K;

    invoke-direct {v2, p0}, LX/64K;-><init>(LX/64L;)V

    invoke-interface {v0, v1, v2}, LX/33e;->a(Lcom/facebook/zero/sdk/request/FetchZeroInterstitialContentParams;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1044479
    return-void
.end method

.method public final b(LX/64J;)V
    .locals 1

    .prologue
    .line 1044474
    iget-object v0, p0, LX/64L;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1044475
    return-void
.end method
