.class public final LX/5f8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# instance fields
.field public final synthetic a:Ljava/lang/Integer;

.field public final synthetic b:LX/5f9;


# direct methods
.method public constructor <init>(LX/5f9;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 970990
    iput-object p1, p0, LX/5f8;->b:LX/5f9;

    iput-object p2, p0, LX/5f8;->a:Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 6

    .prologue
    .line 970991
    sget-object v0, LX/5fQ;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "jpeg: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, LX/5f8;->b:LX/5f9;

    iget-wide v4, v4, LX/5f9;->a:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 970992
    iget-object v0, p0, LX/5f8;->b:LX/5f9;

    iget-object v0, v0, LX/5f9;->c:LX/5fQ;

    iget-object v0, v0, LX/5fQ;->d:Landroid/hardware/Camera;

    const v1, 0x40342d20

    invoke-static {v0, v1}, LX/0J2;->b(Landroid/hardware/Camera;I)V

    .line 970993
    iget-object v0, p0, LX/5f8;->b:LX/5f9;

    iget-object v0, v0, LX/5f9;->c:LX/5fQ;

    invoke-static {v0}, LX/5fQ;->y(LX/5fQ;)V

    .line 970994
    iget-object v0, p0, LX/5f8;->b:LX/5f9;

    iget-object v0, v0, LX/5f9;->c:LX/5fQ;

    const/4 v1, 0x1

    .line 970995
    iput-boolean v1, v0, LX/5fQ;->l:Z

    .line 970996
    new-instance v0, Lcom/facebook/optic/CameraDevice$12$1$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/optic/CameraDevice$12$1$1;-><init>(LX/5f8;[B)V

    invoke-static {v0}, LX/5fo;->a(Ljava/lang/Runnable;)V

    .line 970997
    return-void
.end method
