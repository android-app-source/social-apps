.class public final LX/5EY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 882972
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_c

    .line 882973
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 882974
    :goto_0
    return v1

    .line 882975
    :cond_0
    const-string v12, "is_music_item"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 882976
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v2

    .line 882977
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 882978
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 882979
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 882980
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 882981
    const-string v12, "android_urls"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 882982
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 882983
    :cond_2
    const-string v12, "application"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 882984
    invoke-static {p0, p1}, LX/40y;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 882985
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 882986
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 882987
    :cond_4
    const-string v12, "music_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 882988
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLMusicType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLMusicType;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 882989
    :cond_5
    const-string v12, "musicians"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 882990
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 882991
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_6

    .line 882992
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_6

    .line 882993
    invoke-static {p0, p1}, LX/5EW;->b(LX/15w;LX/186;)I

    move-result v11

    .line 882994
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 882995
    :cond_6
    invoke-static {v5, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 882996
    goto/16 :goto_1

    .line 882997
    :cond_7
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 882998
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 882999
    :cond_8
    const-string v12, "preview_urls"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 883000
    invoke-static {p0, p1}, LX/5EX;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 883001
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 883002
    :cond_a
    const/16 v11, 0x8

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 883003
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 883004
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 883005
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 883006
    if-eqz v0, :cond_b

    .line 883007
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 883008
    :cond_b
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 883009
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 883010
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 883011
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 883012
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 883013
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 883014
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 883015
    if-eqz v0, :cond_0

    .line 883016
    const-string v0, "android_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883017
    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 883018
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883019
    if-eqz v0, :cond_1

    .line 883020
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883021
    invoke-static {p0, v0, p2, p3}, LX/40y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 883022
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883023
    if-eqz v0, :cond_2

    .line 883024
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883025
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883026
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 883027
    if-eqz v0, :cond_3

    .line 883028
    const-string v1, "is_music_item"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883029
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 883030
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 883031
    if-eqz v0, :cond_4

    .line 883032
    const-string v0, "music_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883033
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883034
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883035
    if-eqz v0, :cond_6

    .line 883036
    const-string v1, "musicians"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883037
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 883038
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 883039
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/5EW;->a(LX/15i;ILX/0nX;)V

    .line 883040
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 883041
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 883042
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 883043
    if-eqz v0, :cond_7

    .line 883044
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883045
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883046
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 883047
    if-eqz v0, :cond_a

    .line 883048
    const-string v1, "preview_urls"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883049
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 883050
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_9

    .line 883051
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 883052
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 883053
    const/4 p1, 0x0

    invoke-virtual {p0, v2, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 883054
    if-eqz p1, :cond_8

    .line 883055
    const-string p3, "url"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 883056
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 883057
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 883058
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 883059
    :cond_9
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 883060
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 883061
    return-void
.end method
