.class public final LX/664;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/65Y;


# instance fields
.field public final a:LX/671;

.field private final b:Z

.field public final c:LX/65z;


# direct methods
.method public constructor <init>(LX/671;Z)V
    .locals 2

    .prologue
    .line 1049199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049200
    iput-object p1, p0, LX/664;->a:LX/671;

    .line 1049201
    new-instance v0, LX/65z;

    iget-object v1, p0, LX/664;->a:LX/671;

    invoke-direct {v0, v1}, LX/65z;-><init>(LX/671;)V

    iput-object v0, p0, LX/664;->c:LX/65z;

    .line 1049202
    iput-boolean p2, p0, LX/664;->b:Z

    .line 1049203
    return-void
.end method

.method private static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 1049302
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0, p1}, LX/65A;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lokhttp3/internal/framed/FramedConnection$Reader;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1049294
    const/16 v0, 0x8

    if-eq p2, v0, :cond_0

    const-string v0, "TYPE_RST_STREAM length: %d != 8"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/664;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1049295
    :cond_0
    iget-object v0, p0, LX/664;->a:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    move-result v0

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    .line 1049296
    iget-object v1, p0, LX/664;->a:LX/671;

    invoke-interface {v1}, LX/671;->j()I

    move-result v1

    .line 1049297
    invoke-static {v1}, LX/65X;->fromSpdy3Rst(I)LX/65X;

    move-result-object v2

    .line 1049298
    if-nez v2, :cond_1

    .line 1049299
    const-string v0, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, LX/664;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1049300
    :cond_1
    invoke-virtual {p1, v0, v2}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ILX/65X;)V

    .line 1049301
    return-void
.end method

.method private c(Lokhttp3/internal/framed/FramedConnection$Reader;I)V
    .locals 8

    .prologue
    const v2, 0x7fffffff

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1049286
    const/16 v0, 0x8

    if-eq p2, v0, :cond_0

    const-string v0, "TYPE_WINDOW_UPDATE length: %d != 8"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, LX/664;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1049287
    :cond_0
    iget-object v0, p0, LX/664;->a:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    move-result v0

    .line 1049288
    iget-object v1, p0, LX/664;->a:LX/671;

    invoke-interface {v1}, LX/671;->j()I

    move-result v1

    .line 1049289
    and-int/2addr v0, v2

    .line 1049290
    and-int/2addr v1, v2

    int-to-long v2, v1

    .line 1049291
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    const-string v0, "windowSizeIncrement was 0"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, LX/664;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1049292
    :cond_1
    invoke-virtual {p1, v0, v2, v3}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(IJ)V

    .line 1049293
    return-void
.end method

.method private c(Lokhttp3/internal/framed/FramedConnection$Reader;II)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1049271
    iget-object v2, p0, LX/664;->a:LX/671;

    invoke-interface {v2}, LX/671;->j()I

    move-result v3

    .line 1049272
    mul-int/lit8 v2, v3, 0x8

    add-int/lit8 v2, v2, 0x4

    if-eq p3, v2, :cond_0

    .line 1049273
    const-string v2, "TYPE_SETTINGS length: %d != 4 + 8 * %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v2, v4}, LX/664;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1049274
    :cond_0
    new-instance v4, LX/663;

    invoke-direct {v4}, LX/663;-><init>()V

    move v2, v1

    .line 1049275
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1049276
    iget-object v5, p0, LX/664;->a:LX/671;

    invoke-interface {v5}, LX/671;->j()I

    move-result v5

    .line 1049277
    iget-object v6, p0, LX/664;->a:LX/671;

    invoke-interface {v6}, LX/671;->j()I

    move-result v6

    .line 1049278
    const/high16 v7, -0x1000000

    and-int/2addr v7, v5

    ushr-int/lit8 v7, v7, 0x18

    .line 1049279
    const v8, 0xffffff

    and-int/2addr v5, v8

    .line 1049280
    invoke-virtual {v4, v5, v7, v6}, LX/663;->a(III)LX/663;

    .line 1049281
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1049282
    :cond_1
    and-int/lit8 v2, p2, 0x1

    if-eqz v2, :cond_2

    .line 1049283
    :goto_1
    invoke-virtual {p1, v0, v4}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZLX/663;)V

    .line 1049284
    return-void

    :cond_2
    move v0, v1

    .line 1049285
    goto :goto_1
.end method

.method private d(Lokhttp3/internal/framed/FramedConnection$Reader;I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1049265
    const/4 v2, 0x4

    if-eq p2, v2, :cond_0

    const-string v2, "TYPE_PING length: %d != 4"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v2, v0}, LX/664;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1049266
    :cond_0
    iget-object v2, p0, LX/664;->a:LX/671;

    invoke-interface {v2}, LX/671;->j()I

    move-result v3

    .line 1049267
    iget-boolean v4, p0, LX/664;->b:Z

    and-int/lit8 v2, v3, 0x1

    if-ne v2, v0, :cond_1

    move v2, v0

    :goto_0
    if-ne v4, v2, :cond_2

    .line 1049268
    :goto_1
    invoke-virtual {p1, v0, v3, v1}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZII)V

    .line 1049269
    return-void

    :cond_1
    move v2, v1

    .line 1049270
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private e(Lokhttp3/internal/framed/FramedConnection$Reader;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1049257
    const/16 v0, 0x8

    if-eq p2, v0, :cond_0

    const-string v0, "TYPE_GOAWAY length: %d != 8"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/664;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1049258
    :cond_0
    iget-object v0, p0, LX/664;->a:LX/671;

    invoke-interface {v0}, LX/671;->j()I

    move-result v0

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    .line 1049259
    iget-object v1, p0, LX/664;->a:LX/671;

    invoke-interface {v1}, LX/671;->j()I

    move-result v1

    .line 1049260
    invoke-static {v1}, LX/65X;->fromSpdyGoAway(I)LX/65X;

    move-result-object v2

    .line 1049261
    if-nez v2, :cond_1

    .line 1049262
    const-string v0, "TYPE_GOAWAY unexpected error code: %d"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, LX/664;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1049263
    :cond_1
    sget-object v1, LX/673;->b:LX/673;

    invoke-virtual {p1, v0, v1}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ILX/673;)V

    .line 1049264
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1049256
    return-void
.end method

.method public final a(Lokhttp3/internal/framed/FramedConnection$Reader;)Z
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1049207
    :try_start_0
    iget-object v2, p0, LX/664;->a:LX/671;

    invoke-interface {v2}, LX/671;->j()I

    move-result v3

    .line 1049208
    iget-object v2, p0, LX/664;->a:LX/671;

    invoke-interface {v2}, LX/671;->j()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 1049209
    const/high16 v2, -0x80000000

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    move v2, v1

    .line 1049210
    :goto_0
    const/high16 v5, -0x1000000

    and-int/2addr v5, v4

    ushr-int/lit8 v5, v5, 0x18

    .line 1049211
    const v6, 0xffffff

    and-int/2addr v4, v6

    .line 1049212
    if-eqz v2, :cond_2

    .line 1049213
    const/high16 v0, 0x7fff0000

    and-int/2addr v0, v3

    ushr-int/lit8 v0, v0, 0x10

    .line 1049214
    const v2, 0xffff

    and-int/2addr v2, v3

    .line 1049215
    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    .line 1049216
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "version != 3: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1049217
    :catch_0
    move v1, v0

    .line 1049218
    :goto_1
    return v1

    :cond_0
    move v2, v0

    .line 1049219
    goto :goto_0

    .line 1049220
    :cond_1
    packed-switch v2, :pswitch_data_0

    .line 1049221
    :pswitch_0
    iget-object v0, p0, LX/664;->a:LX/671;

    int-to-long v2, v4

    invoke-interface {v0, v2, v3}, LX/671;->f(J)V

    goto :goto_1

    .line 1049222
    :pswitch_1
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1049223
    iget-object v9, p0, LX/664;->a:LX/671;

    invoke-interface {v9}, LX/671;->j()I

    move-result v9

    .line 1049224
    iget-object v10, p0, LX/664;->a:LX/671;

    invoke-interface {v10}, LX/671;->j()I

    .line 1049225
    const v10, 0x7fffffff

    and-int/2addr v10, v9

    .line 1049226
    iget-object v9, p0, LX/664;->a:LX/671;

    invoke-interface {v9}, LX/671;->i()S

    .line 1049227
    iget-object v9, p0, LX/664;->c:LX/65z;

    add-int/lit8 v11, v4, -0xa

    invoke-virtual {v9, v11}, LX/65z;->a(I)Ljava/util/List;

    move-result-object v11

    .line 1049228
    and-int/lit8 v9, v5, 0x1

    if-eqz v9, :cond_4

    move v9, v8

    .line 1049229
    :goto_2
    and-int/lit8 v12, v5, 0x2

    if-eqz v12, :cond_5

    .line 1049230
    :goto_3
    sget-object v12, LX/65k;->SPDY_SYN_STREAM:LX/65k;

    move-object v7, p1

    invoke-virtual/range {v7 .. v12}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZZILjava/util/List;LX/65k;)V

    .line 1049231
    goto :goto_1

    .line 1049232
    :pswitch_2
    const/4 v8, 0x0

    .line 1049233
    iget-object v7, p0, LX/664;->a:LX/671;

    invoke-interface {v7}, LX/671;->j()I

    move-result v7

    .line 1049234
    const v9, 0x7fffffff

    and-int v10, v7, v9

    .line 1049235
    iget-object v7, p0, LX/664;->c:LX/65z;

    add-int/lit8 v9, v4, -0x4

    invoke-virtual {v7, v9}, LX/65z;->a(I)Ljava/util/List;

    move-result-object v11

    .line 1049236
    and-int/lit8 v7, v5, 0x1

    if-eqz v7, :cond_6

    const/4 v9, 0x1

    .line 1049237
    :goto_4
    sget-object v12, LX/65k;->SPDY_REPLY:LX/65k;

    move-object v7, p1

    invoke-virtual/range {v7 .. v12}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZZILjava/util/List;LX/65k;)V

    .line 1049238
    goto :goto_1

    .line 1049239
    :pswitch_3
    invoke-direct {p0, p1, v4}, LX/664;->a(Lokhttp3/internal/framed/FramedConnection$Reader;I)V

    goto :goto_1

    .line 1049240
    :pswitch_4
    invoke-direct {p0, p1, v5, v4}, LX/664;->c(Lokhttp3/internal/framed/FramedConnection$Reader;II)V

    goto :goto_1

    .line 1049241
    :pswitch_5
    invoke-direct {p0, p1, v4}, LX/664;->d(Lokhttp3/internal/framed/FramedConnection$Reader;I)V

    goto :goto_1

    .line 1049242
    :pswitch_6
    invoke-direct {p0, p1, v4}, LX/664;->e(Lokhttp3/internal/framed/FramedConnection$Reader;I)V

    goto :goto_1

    .line 1049243
    :pswitch_7
    const/4 v8, 0x0

    .line 1049244
    iget-object v7, p0, LX/664;->a:LX/671;

    invoke-interface {v7}, LX/671;->j()I

    move-result v7

    .line 1049245
    const v9, 0x7fffffff

    and-int v10, v7, v9

    .line 1049246
    iget-object v7, p0, LX/664;->c:LX/65z;

    add-int/lit8 v9, v4, -0x4

    invoke-virtual {v7, v9}, LX/65z;->a(I)Ljava/util/List;

    move-result-object v11

    .line 1049247
    sget-object v12, LX/65k;->SPDY_HEADERS:LX/65k;

    move-object v7, p1

    move v9, v8

    invoke-virtual/range {v7 .. v12}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZZILjava/util/List;LX/65k;)V

    .line 1049248
    goto/16 :goto_1

    .line 1049249
    :pswitch_8
    invoke-direct {p0, p1, v4}, LX/664;->c(Lokhttp3/internal/framed/FramedConnection$Reader;I)V

    goto/16 :goto_1

    .line 1049250
    :cond_2
    const v2, 0x7fffffff

    and-int/2addr v2, v3

    .line 1049251
    and-int/lit8 v3, v5, 0x1

    if-eqz v3, :cond_3

    move v0, v1

    .line 1049252
    :cond_3
    iget-object v3, p0, LX/664;->a:LX/671;

    invoke-virtual {p1, v0, v2, v3, v4}, Lokhttp3/internal/framed/FramedConnection$Reader;->a(ZILX/671;I)V

    goto/16 :goto_1

    :cond_4
    move v9, v7

    .line 1049253
    goto :goto_2

    :cond_5
    move v8, v7

    .line 1049254
    goto :goto_3

    :cond_6
    move v9, v8

    .line 1049255
    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1049204
    iget-object v0, p0, LX/664;->c:LX/65z;

    .line 1049205
    iget-object p0, v0, LX/65z;->c:LX/671;

    invoke-interface {p0}, LX/65D;->close()V

    .line 1049206
    return-void
.end method
