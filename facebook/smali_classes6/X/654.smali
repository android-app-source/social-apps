.class public final LX/654;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/650;

.field public b:LX/64x;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:LX/64l;

.field public f:LX/64m;

.field public g:LX/656;

.field public h:LX/655;

.field public i:LX/655;

.field public j:LX/655;

.field public k:J

.field public l:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1046406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046407
    const/4 v0, -0x1

    iput v0, p0, LX/654;->c:I

    .line 1046408
    new-instance v0, LX/64m;

    invoke-direct {v0}, LX/64m;-><init>()V

    iput-object v0, p0, LX/654;->f:LX/64m;

    .line 1046409
    return-void
.end method

.method public constructor <init>(LX/655;)V
    .locals 2

    .prologue
    .line 1046391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046392
    const/4 v0, -0x1

    iput v0, p0, LX/654;->c:I

    .line 1046393
    iget-object v0, p1, LX/655;->a:LX/650;

    iput-object v0, p0, LX/654;->a:LX/650;

    .line 1046394
    iget-object v0, p1, LX/655;->b:LX/64x;

    iput-object v0, p0, LX/654;->b:LX/64x;

    .line 1046395
    iget v0, p1, LX/655;->c:I

    iput v0, p0, LX/654;->c:I

    .line 1046396
    iget-object v0, p1, LX/655;->d:Ljava/lang/String;

    iput-object v0, p0, LX/654;->d:Ljava/lang/String;

    .line 1046397
    iget-object v0, p1, LX/655;->e:LX/64l;

    iput-object v0, p0, LX/654;->e:LX/64l;

    .line 1046398
    iget-object v0, p1, LX/655;->f:LX/64n;

    invoke-virtual {v0}, LX/64n;->newBuilder()LX/64m;

    move-result-object v0

    iput-object v0, p0, LX/654;->f:LX/64m;

    .line 1046399
    iget-object v0, p1, LX/655;->g:LX/656;

    iput-object v0, p0, LX/654;->g:LX/656;

    .line 1046400
    iget-object v0, p1, LX/655;->h:LX/655;

    iput-object v0, p0, LX/654;->h:LX/655;

    .line 1046401
    iget-object v0, p1, LX/655;->i:LX/655;

    iput-object v0, p0, LX/654;->i:LX/655;

    .line 1046402
    iget-object v0, p1, LX/655;->j:LX/655;

    iput-object v0, p0, LX/654;->j:LX/655;

    .line 1046403
    iget-wide v0, p1, LX/655;->k:J

    iput-wide v0, p0, LX/654;->k:J

    .line 1046404
    iget-wide v0, p1, LX/655;->l:J

    iput-wide v0, p0, LX/654;->l:J

    .line 1046405
    return-void
.end method

.method private static a(Ljava/lang/String;LX/655;)V
    .locals 3

    .prologue
    .line 1046382
    iget-object v0, p1, LX/655;->g:LX/656;

    if-eqz v0, :cond_0

    .line 1046383
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".body != null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046384
    :cond_0
    iget-object v0, p1, LX/655;->h:LX/655;

    if-eqz v0, :cond_1

    .line 1046385
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".networkResponse != null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046386
    :cond_1
    iget-object v0, p1, LX/655;->i:LX/655;

    if-eqz v0, :cond_2

    .line 1046387
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".cacheResponse != null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046388
    :cond_2
    iget-object v0, p1, LX/655;->j:LX/655;

    if-eqz v0, :cond_3

    .line 1046389
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".priorResponse != null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046390
    :cond_3
    return-void
.end method

.method public static d(LX/655;)V
    .locals 2

    .prologue
    .line 1046365
    iget-object v0, p0, LX/655;->g:LX/656;

    if-eqz v0, :cond_0

    .line 1046366
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "priorResponse.body != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046367
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/64n;)LX/654;
    .locals 1

    .prologue
    .line 1046380
    invoke-virtual {p1}, LX/64n;->newBuilder()LX/64m;

    move-result-object v0

    iput-object v0, p0, LX/654;->f:LX/64m;

    .line 1046381
    return-object p0
.end method

.method public final a(LX/655;)LX/654;
    .locals 1

    .prologue
    .line 1046377
    if-eqz p1, :cond_0

    const-string v0, "networkResponse"

    invoke-static {v0, p1}, LX/654;->a(Ljava/lang/String;LX/655;)V

    .line 1046378
    :cond_0
    iput-object p1, p0, LX/654;->h:LX/655;

    .line 1046379
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/654;
    .locals 1

    .prologue
    .line 1046375
    iget-object v0, p0, LX/654;->f:LX/64m;

    invoke-virtual {v0, p1, p2}, LX/64m;->a(Ljava/lang/String;Ljava/lang/String;)LX/64m;

    .line 1046376
    return-object p0
.end method

.method public final a()LX/655;
    .locals 3

    .prologue
    .line 1046371
    iget-object v0, p0, LX/654;->a:LX/650;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "request == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046372
    :cond_0
    iget-object v0, p0, LX/654;->b:LX/64x;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "protocol == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046373
    :cond_1
    iget v0, p0, LX/654;->c:I

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "code < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/654;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046374
    :cond_2
    new-instance v0, LX/655;

    invoke-direct {v0, p0}, LX/655;-><init>(LX/654;)V

    return-object v0
.end method

.method public final b(LX/655;)LX/654;
    .locals 1

    .prologue
    .line 1046368
    if-eqz p1, :cond_0

    const-string v0, "cacheResponse"

    invoke-static {v0, p1}, LX/654;->a(Ljava/lang/String;LX/655;)V

    .line 1046369
    :cond_0
    iput-object p1, p0, LX/654;->i:LX/655;

    .line 1046370
    return-object p0
.end method
