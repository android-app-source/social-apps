.class public final enum LX/55G;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/55G;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/55G;

.field public static final enum LOCAL_AND_SERVER:LX/55G;

.field public static final enum LOCAL_ONLY:LX/55G;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 829093
    new-instance v0, LX/55G;

    const-string v1, "LOCAL_ONLY"

    invoke-direct {v0, v1, v2}, LX/55G;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55G;->LOCAL_ONLY:LX/55G;

    new-instance v0, LX/55G;

    const-string v1, "LOCAL_AND_SERVER"

    invoke-direct {v0, v1, v3}, LX/55G;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/55G;->LOCAL_AND_SERVER:LX/55G;

    .line 829094
    const/4 v0, 0x2

    new-array v0, v0, [LX/55G;

    sget-object v1, LX/55G;->LOCAL_ONLY:LX/55G;

    aput-object v1, v0, v2

    sget-object v1, LX/55G;->LOCAL_AND_SERVER:LX/55G;

    aput-object v1, v0, v3

    sput-object v0, LX/55G;->$VALUES:[LX/55G;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 829095
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/55G;
    .locals 1

    .prologue
    .line 829096
    const-class v0, LX/55G;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/55G;

    return-object v0
.end method

.method public static values()[LX/55G;
    .locals 1

    .prologue
    .line 829097
    sget-object v0, LX/55G;->$VALUES:[LX/55G;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/55G;

    return-object v0
.end method
