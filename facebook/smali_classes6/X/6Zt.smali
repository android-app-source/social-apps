.class public final LX/6Zt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/maps/GenericMapsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/maps/GenericMapsFragment;)V
    .locals 0

    .prologue
    .line 1111745
    iput-object p1, p0, LX/6Zt;->a:Lcom/facebook/maps/GenericMapsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x3bd4e140

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1111746
    iget-object v1, p0, LX/6Zt;->a:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v1, v1, Lcom/facebook/maps/GenericMapsFragment;->c:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->a()LX/0yG;

    move-result-object v1

    sget-object v2, LX/0yG;->OKAY:LX/0yG;

    if-eq v1, v2, :cond_0

    .line 1111747
    iget-object v1, p0, LX/6Zt;->a:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v1, v1, Lcom/facebook/maps/GenericMapsFragment;->b:LX/6Zb;

    new-instance v2, LX/2si;

    invoke-direct {v2}, LX/2si;-><init>()V

    const-string v3, "surface_generic_map_fragment"

    const-string v4, "mechanism_get_direction_button"

    invoke-virtual {v1, v2, v3, v4}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 1111748
    iget-object v1, p0, LX/6Zt;->a:Lcom/facebook/maps/GenericMapsFragment;

    const-string v2, "mechanism_get_direction_button"

    .line 1111749
    iput-object v2, v1, Lcom/facebook/maps/GenericMapsFragment;->q:Ljava/lang/String;

    .line 1111750
    iget-object v1, p0, LX/6Zt;->a:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v1, v1, Lcom/facebook/maps/GenericMapsFragment;->d:LX/6a5;

    iget-object v2, p0, LX/6Zt;->a:Lcom/facebook/maps/GenericMapsFragment;

    iget-object v2, v2, Lcom/facebook/maps/GenericMapsFragment;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/6a5;->a(Ljava/lang/String;)V

    .line 1111751
    :goto_0
    const v1, -0x202e487d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1111752
    :cond_0
    iget-object v1, p0, LX/6Zt;->a:Lcom/facebook/maps/GenericMapsFragment;

    .line 1111753
    invoke-static {v1}, Lcom/facebook/maps/GenericMapsFragment;->d$redex0(Lcom/facebook/maps/GenericMapsFragment;)V

    .line 1111754
    goto :goto_0
.end method
