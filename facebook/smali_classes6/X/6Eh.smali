.class public final LX/6Eh;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;)V
    .locals 0

    .prologue
    .line 1066470
    iput-object p1, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1066471
    iget-object v0, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->o:LX/03V;

    sget-object v1, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1066472
    iget-object v0, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    .line 1066473
    invoke-virtual {v0}, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->l()V

    .line 1066474
    iget-object v0, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1066475
    iget-object v0, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const-string v1, "Fetching location failed."

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 1066476
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1066477
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    const/4 v2, 0x1

    .line 1066478
    iget-object v0, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->z:LX/6Ef;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Ef;->a(Ljava/lang/Boolean;)V

    .line 1066479
    iget-object v0, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->w:LX/6ET;

    sget-object v1, LX/6ET;->REQUEST_PERMISSION:LX/6ET;

    if-ne v0, v1, :cond_0

    .line 1066480
    iget-object v0, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->z:LX/6Ef;

    .line 1066481
    iput-boolean v2, v0, LX/6Ef;->b:Z

    .line 1066482
    :cond_0
    iget-object v0, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    .line 1066483
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 1066484
    :try_start_0
    const-string v4, "latitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 1066485
    const-string v4, "longitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 1066486
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v4

    .line 1066487
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1066488
    const-string v4, "accuracy"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v5

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1066489
    :goto_0
    iget-object v4, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->p:LX/6EP;

    invoke-virtual {v4, v3}, LX/6EP;->a(Lorg/json/JSONObject;)V

    .line 1066490
    const/4 v3, 0x0

    iput-object v3, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->p:LX/6EP;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1066491
    :goto_1
    iget-object v0, p0, LX/6Eh;->a:Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;

    iget-object v0, v0, Lcom/facebook/browserextensions/common/identity/RequestPermissionDialogFragment;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1066492
    return-void

    .line 1066493
    :cond_1
    :try_start_1
    const-string v4, "accuracy"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1066494
    :catch_0
    move-exception v3

    .line 1066495
    iget-object v4, v0, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->o:LX/03V;

    sget-object v5, Lcom/facebook/browserextensions/common/location/RequestCurrentPositionDialogFragment;->m:Ljava/lang/String;

    invoke-virtual {v4, v5, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
