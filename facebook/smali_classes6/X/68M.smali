.class public LX/68M;
.super LX/67m;
.source ""


# instance fields
.field private o:F

.field private p:F

.field private q:F

.field private final r:F

.field private final s:Landroid/graphics/Paint;

.field private final t:Landroid/graphics/Rect;

.field private final u:Landroid/graphics/RectF;

.field private final v:Landroid/graphics/RectF;

.field private final w:Ljava/lang/String;

.field private final x:I

.field public y:LX/3BT;


# direct methods
.method public constructor <init>(LX/680;ILX/3BT;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1054866
    invoke-direct {p0, p1}, LX/67m;-><init>(LX/680;)V

    .line 1054867
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/68M;->s:Landroid/graphics/Paint;

    .line 1054868
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/68M;->t:Landroid/graphics/Rect;

    .line 1054869
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/68M;->u:Landroid/graphics/RectF;

    .line 1054870
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/68M;->v:Landroid/graphics/RectF;

    .line 1054871
    iput-object p3, p0, LX/68M;->y:LX/3BT;

    .line 1054872
    const/high16 v0, 0x42400000    # 48.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, LX/68M;->r:F

    .line 1054873
    const/high16 v0, 0x41000000    # 8.0f

    iget v1, p0, LX/67m;->d:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, LX/68M;->o:F

    .line 1054874
    const/4 v0, 0x3

    iput v0, p0, LX/68M;->j:I

    .line 1054875
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, LX/68M;->k:F

    .line 1054876
    iget-object v0, p0, LX/68M;->s:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    .line 1054877
    iget-object v0, p0, LX/68M;->s:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1054878
    iget-object v0, p0, LX/68M;->s:Landroid/graphics/Paint;

    const/high16 v1, 0x3fc00000    # 1.5f

    iget v2, p0, LX/67m;->d:F

    mul-float/2addr v1, v2

    const v2, -0x3f000001    # -7.9999995f

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1054879
    iget-object v0, p0, LX/68M;->s:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, LX/67m;->d:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1054880
    iget-object v0, p0, LX/68M;->s:Landroid/graphics/Paint;

    const/high16 v1, -0x66000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1054881
    iput-object p4, p0, LX/68M;->w:Ljava/lang/String;

    .line 1054882
    iput p2, p0, LX/68M;->x:I

    .line 1054883
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68M;->l:Z

    .line 1054884
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/680;I)V
    .locals 2

    .prologue
    .line 1054864
    new-instance v0, LX/3BT;

    invoke-direct {v0, p1}, LX/3BT;-><init>(Landroid/content/Context;)V

    const-string v1, "Report"

    invoke-direct {p0, p2, p3, v0, v1}, LX/68M;-><init>(LX/680;ILX/3BT;Ljava/lang/String;)V

    .line 1054865
    return-void
.end method


# virtual methods
.method public final a(FF)I
    .locals 1

    .prologue
    .line 1054858
    iget-object v0, p0, LX/68M;->u:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1054859
    const/4 v0, 0x2

    .line 1054860
    :goto_0
    return v0

    .line 1054861
    :cond_0
    iget-object v0, p0, LX/68M;->v:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1054862
    const/4 v0, 0x1

    goto :goto_0

    .line 1054863
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1054856
    iget-object v0, p0, LX/68M;->w:Ljava/lang/String;

    iget v1, p0, LX/68M;->p:F

    iget v2, p0, LX/68M;->q:F

    iget-object v3, p0, LX/68M;->u:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, v3

    iget-object v3, p0, LX/68M;->u:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    iget-object v3, p0, LX/68M;->s:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1054857
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 1054825
    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1054826
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 1054827
    iget-object v1, p0, LX/68M;->s:Landroid/graphics/Paint;

    iget-object v2, p0, LX/68M;->w:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, LX/68M;->w:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, LX/68M;->t:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1054828
    iget v1, p0, LX/68M;->x:I

    packed-switch v1, :pswitch_data_0

    .line 1054829
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getWidth()I

    move-result v1

    iget-object v2, p0, LX/68M;->t:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, LX/68M;->o:F

    sub-float/2addr v1, v2

    iget-object v2, p0, LX/67m;->e:LX/680;

    iget v2, v2, LX/680;->e:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, p0, LX/68M;->p:F

    .line 1054830
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getHeight()I

    move-result v0

    iget-object v1, p0, LX/68M;->t:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/68M;->o:F

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->f:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/68M;->q:F

    .line 1054831
    :goto_0
    iget-object v0, p0, LX/68M;->u:Landroid/graphics/RectF;

    iget-object v1, p0, LX/68M;->t:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 1054832
    iget-object v0, p0, LX/68M;->u:Landroid/graphics/RectF;

    iget v1, p0, LX/68M;->p:F

    iget v2, p0, LX/68M;->q:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offsetTo(FF)V

    .line 1054833
    iget-object v0, p0, LX/68M;->u:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    .line 1054834
    iget-object v1, p0, LX/68M;->u:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    .line 1054835
    iget-object v2, p0, LX/68M;->v:Landroid/graphics/RectF;

    iget v3, p0, LX/68M;->r:F

    sub-float v3, v0, v3

    iget v4, p0, LX/68M;->r:F

    sub-float v4, v1, v4

    iget v5, p0, LX/68M;->r:F

    add-float/2addr v0, v5

    iget v5, p0, LX/68M;->r:F

    add-float/2addr v1, v5

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1054836
    return-void

    .line 1054837
    :pswitch_0
    iget v0, p0, LX/68M;->o:F

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->c:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, LX/68M;->p:F

    .line 1054838
    iget v0, p0, LX/68M;->o:F

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->d:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, LX/68M;->q:F

    goto :goto_0

    .line 1054839
    :pswitch_1
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getWidth()I

    move-result v0

    iget-object v1, p0, LX/68M;->t:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/68M;->o:F

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->e:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/68M;->p:F

    .line 1054840
    iget v0, p0, LX/68M;->o:F

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->d:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, LX/68M;->q:F

    goto :goto_0

    .line 1054841
    :pswitch_2
    iget v1, p0, LX/68M;->o:F

    iget-object v2, p0, LX/67m;->e:LX/680;

    iget v2, v2, LX/680;->c:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, LX/68M;->p:F

    .line 1054842
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getHeight()I

    move-result v0

    iget-object v1, p0, LX/68M;->t:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, LX/68M;->o:F

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/67m;->e:LX/680;

    iget v1, v1, LX/680;->f:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/68M;->q:F

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(FF)Z
    .locals 10

    .prologue
    .line 1054843
    iget-object v0, p0, LX/67m;->e:LX/680;

    invoke-virtual {v0}, LX/680;->c()LX/692;

    move-result-object v5

    .line 1054844
    iget-object v6, p0, LX/68M;->y:LX/3BT;

    iget-object v7, p0, LX/67m;->g:Landroid/content/Context;

    iget-object v0, p0, LX/67m;->e:LX/680;

    .line 1054845
    iget-object v1, v0, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v0, v1

    .line 1054846
    invoke-virtual {v0}, Lcom/facebook/android/maps/MapView;->getWidth()I

    move-result v0

    iget-object v1, p0, LX/67m;->e:LX/680;

    .line 1054847
    iget-object v2, v1, LX/680;->A:Lcom/facebook/android/maps/MapView;

    move-object v1, v2

    .line 1054848
    invoke-virtual {v1}, Lcom/facebook/android/maps/MapView;->getHeight()I

    move-result v1

    const/4 v2, 0x2

    iget-object v3, p0, LX/67m;->e:LX/680;

    .line 1054849
    iget-object v4, v3, LX/680;->z:Landroid/content/Context;

    move-object v3, v4

    .line 1054850
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v4, LX/3BU;->a:Ljava/lang/String;

    new-instance v8, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v9, "dynamic_map_report_button"

    invoke-direct {v8, v9}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iget-object v9, v5, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v8, v9}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Lcom/facebook/android/maps/model/LatLng;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v8

    iget v5, v5, LX/692;->b:F

    float-to-int v5, v5

    invoke-virtual {v8, v5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/3BP;->a(IIILandroid/content/res/Resources;Ljava/lang/String;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, LX/67m;->e:LX/680;

    .line 1054851
    iget-object v2, v1, LX/680;->l:LX/68m;

    .line 1054852
    iget-object v1, v2, LX/68m;->B:LX/68Y;

    iget v1, v1, LX/68Y;->o:I

    move v2, v1

    .line 1054853
    move v1, v2

    .line 1054854
    invoke-virtual {v6, v7, v0, v1}, LX/3BT;->a(Landroid/content/Context;Landroid/net/Uri;I)V

    .line 1054855
    const/4 v0, 0x1

    return v0
.end method
