.class public final LX/5Jw;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5Jx;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:D

.field public b:LX/1X1;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 897916
    invoke-static {}, LX/5Jx;->q()LX/5Jx;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 897917
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 897898
    const-string v0, "RatioLayout"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 897899
    if-ne p0, p1, :cond_1

    .line 897900
    :cond_0
    :goto_0
    return v0

    .line 897901
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 897902
    goto :goto_0

    .line 897903
    :cond_3
    check-cast p1, LX/5Jw;

    .line 897904
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 897905
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 897906
    if-eq v2, v3, :cond_0

    .line 897907
    iget-wide v2, p0, LX/5Jw;->a:D

    iget-wide v4, p1, LX/5Jw;->a:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 897908
    goto :goto_0

    .line 897909
    :cond_4
    iget-object v2, p0, LX/5Jw;->b:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/5Jw;->b:LX/1X1;

    iget-object v3, p1, LX/5Jw;->b:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 897910
    goto :goto_0

    .line 897911
    :cond_5
    iget-object v2, p1, LX/5Jw;->b:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 897912
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/5Jw;

    .line 897913
    iget-object v1, v0, LX/5Jw;->b:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/5Jw;->b:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/5Jw;->b:LX/1X1;

    .line 897914
    return-object v0

    .line 897915
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
