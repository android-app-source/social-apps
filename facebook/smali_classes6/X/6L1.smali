.class public LX/6L1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final a:LX/6JS;

.field public final b:Landroid/os/Handler;

.field public final c:LX/6Kz;

.field public volatile d:LX/6Kx;

.field public e:Landroid/media/MediaCodec;

.field public f:Landroid/media/MediaFormat;

.field public g:Landroid/media/MediaCodec$BufferInfo;


# direct methods
.method public constructor <init>(LX/6Kz;LX/6JS;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 1077817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077818
    iput-object p1, p0, LX/6L1;->c:LX/6Kz;

    .line 1077819
    iput-object p2, p0, LX/6L1;->a:LX/6JS;

    .line 1077820
    iput-object p3, p0, LX/6L1;->b:Landroid/os/Handler;

    .line 1077821
    sget-object v0, LX/6Kx;->STOPPED:LX/6Kx;

    iput-object v0, p0, LX/6L1;->d:LX/6Kx;

    .line 1077822
    return-void
.end method

.method private static b(LX/6L1;)V
    .locals 6

    .prologue
    .line 1077823
    :try_start_0
    iget-object v0, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1077824
    :goto_0
    iget-object v1, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    iget-object v2, p0, LX/6L1;->g:Landroid/media/MediaCodec$BufferInfo;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v1

    .line 1077825
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 1077826
    const/4 v2, -0x3

    if-ne v1, v2, :cond_0

    .line 1077827
    iget-object v0, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_0

    .line 1077828
    :cond_0
    const/4 v2, -0x2

    if-ne v1, v2, :cond_2

    .line 1077829
    iget-object v1, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v1

    iput-object v1, p0, LX/6L1;->f:Landroid/media/MediaFormat;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1077830
    :catch_0
    move-exception v0

    .line 1077831
    iget-object v1, p0, LX/6L1;->a:LX/6JS;

    invoke-virtual {v1, v0}, LX/6JS;->a(Ljava/lang/Exception;)V

    .line 1077832
    :cond_1
    :goto_1
    return-void

    .line 1077833
    :cond_2
    if-gez v1, :cond_3

    .line 1077834
    :try_start_1
    iget-object v0, p0, LX/6L1;->a:LX/6JS;

    new-instance v2, Ljava/io/IOException;

    const-string v3, "unexpected result from encoder.dequeueOutputBuffer: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/6JS;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 1077835
    :cond_3
    aget-object v2, v0, v1

    .line 1077836
    if-nez v2, :cond_4

    .line 1077837
    iget-object v0, p0, LX/6L1;->a:LX/6JS;

    new-instance v2, Ljava/io/IOException;

    const-string v3, "encoderOutputBuffer : %d was null"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/6JS;->a(Ljava/lang/Exception;)V

    goto :goto_1

    .line 1077838
    :cond_4
    iget-object v3, p0, LX/6L1;->g:Landroid/media/MediaCodec$BufferInfo;

    iget v3, v3, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v3

    iget-object v4, p0, LX/6L1;->g:Landroid/media/MediaCodec$BufferInfo;

    iget v4, v4, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v3, v4}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    .line 1077839
    iget-object v3, p0, LX/6L1;->a:LX/6JS;

    iget-object v4, p0, LX/6L1;->g:Landroid/media/MediaCodec$BufferInfo;

    invoke-virtual {v3, v2, v4}, LX/6JS;->a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 1077840
    iget-object v2, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 1077841
    iget-object v1, p0, LX/6L1;->g:Landroid/media/MediaCodec$BufferInfo;

    iget v1, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    and-int/lit8 v1, v1, 0x4

    if-nez v1, :cond_1

    goto/16 :goto_0
.end method

.method public static f(LX/6L1;LX/6JU;Landroid/os/Handler;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1077842
    iget-object v0, p0, LX/6L1;->d:LX/6Kx;

    sget-object v1, LX/6Kx;->STARTED:LX/6Kx;

    if-ne v0, v1, :cond_0

    .line 1077843
    invoke-static {p0}, LX/6L1;->b(LX/6L1;)V

    .line 1077844
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    if-eqz v0, :cond_1

    .line 1077845
    iget-object v0, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V

    .line 1077846
    iget-object v0, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 1077847
    iget-object v0, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1077848
    :cond_1
    sget-object v0, LX/6Kx;->STOPPED:LX/6Kx;

    iput-object v0, p0, LX/6L1;->d:LX/6Kx;

    .line 1077849
    iput-object v2, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    .line 1077850
    iput-object v2, p0, LX/6L1;->g:Landroid/media/MediaCodec$BufferInfo;

    .line 1077851
    iput-object v2, p0, LX/6L1;->f:Landroid/media/MediaFormat;

    .line 1077852
    invoke-static {p1, p2}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;)V

    .line 1077853
    :goto_0
    return-void

    .line 1077854
    :catch_0
    move-exception v0

    .line 1077855
    :try_start_1
    invoke-static {p1, p2, v0}, LX/6LA;->a(LX/6JU;Landroid/os/Handler;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1077856
    sget-object v0, LX/6Kx;->STOPPED:LX/6Kx;

    iput-object v0, p0, LX/6L1;->d:LX/6Kx;

    .line 1077857
    iput-object v2, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    .line 1077858
    iput-object v2, p0, LX/6L1;->g:Landroid/media/MediaCodec$BufferInfo;

    .line 1077859
    iput-object v2, p0, LX/6L1;->f:Landroid/media/MediaFormat;

    goto :goto_0

    .line 1077860
    :catchall_0
    move-exception v0

    sget-object v1, LX/6Kx;->STOPPED:LX/6Kx;

    iput-object v1, p0, LX/6L1;->d:LX/6Kx;

    .line 1077861
    iput-object v2, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    .line 1077862
    iput-object v2, p0, LX/6L1;->g:Landroid/media/MediaCodec$BufferInfo;

    .line 1077863
    iput-object v2, p0, LX/6L1;->f:Landroid/media/MediaFormat;

    throw v0
.end method


# virtual methods
.method public final a([BIJ)V
    .locals 7

    .prologue
    .line 1077864
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, LX/6L1;->b:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1077865
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "inputData must be invoked on the same thread as the other methods"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1077866
    :cond_0
    iget-object v0, p0, LX/6L1;->d:LX/6Kx;

    sget-object v1, LX/6Kx;->STARTED:LX/6Kx;

    if-eq v0, v1, :cond_1

    .line 1077867
    :goto_0
    return-void

    .line 1077868
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1077869
    iget-object v1, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v1

    .line 1077870
    if-ltz v1, :cond_2

    .line 1077871
    aget-object v0, v0, v1

    .line 1077872
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1077873
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 1077874
    iget-object v0, p0, LX/6L1;->e:Landroid/media/MediaCodec;

    const/4 v2, 0x0

    const/4 v6, 0x0

    move v3, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 1077875
    :cond_2
    invoke-static {p0}, LX/6L1;->b(LX/6L1;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1077876
    :catch_0
    move-exception v0

    .line 1077877
    iget-object v1, p0, LX/6L1;->a:LX/6JS;

    invoke-virtual {v1, v0}, LX/6JS;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method
