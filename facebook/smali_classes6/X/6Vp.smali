.class public LX/6Vp;
.super Ljava/lang/RuntimeException;
.source ""


# direct methods
.method private constructor <init>(LX/1RZ;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1104472
    invoke-static {p1, p2, p3}, LX/6Vp;->b(LX/1RZ;Ljava/lang/Throwable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 1104473
    invoke-virtual {p0, p2}, LX/6Vp;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1104474
    return-void
.end method

.method public static a(LX/1RZ;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1104475
    const-class v0, LX/6Vp;

    invoke-static {p1, v0}, LX/1Bz;->propagateIfInstanceOf(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 1104476
    new-instance v0, LX/6Vp;

    invoke-direct {v0, p0, p1, p2}, LX/6Vp;-><init>(LX/1RZ;Ljava/lang/Throwable;Ljava/lang/String;)V

    throw v0
.end method

.method private static b(LX/1RZ;Ljava/lang/Throwable;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1104477
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1104478
    const-string v0, "\nException thrown while "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104479
    instance-of v0, p1, LX/5Oo;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 1104480
    check-cast v0, LX/5Oo;

    iget-object v0, v0, LX/5Oo;->stage:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104481
    :goto_0
    const-string v0, " a part definition"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104482
    instance-of v0, p1, LX/5Oo;

    if-eqz v0, :cond_2

    .line 1104483
    const/4 v0, 0x0

    move p2, v0

    :goto_1
    move-object v0, p1

    check-cast v0, LX/5Oo;

    iget-object v0, v0, LX/5Oo;->parts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_1

    .line 1104484
    if-nez p2, :cond_0

    const-string v0, "\nThrown from "

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, p1

    .line 1104485
    check-cast v0, LX/5Oo;

    iget-object v0, v0, LX/5Oo;->parts:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104486
    add-int/lit8 v0, p2, 0x1

    move p2, v0

    goto :goto_1

    .line 1104487
    :cond_0
    const-string v0, "\nWith parent "

    goto :goto_2

    .line 1104488
    :cond_1
    const-string v0, "\nWith parent "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104489
    iget-object v0, p0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v0, v0

    .line 1104490
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104491
    :goto_3
    iget-object v0, p0, LX/1RZ;->b:LX/1RE;

    move-object v0, v0

    .line 1104492
    :goto_4
    if-eqz v0, :cond_3

    .line 1104493
    const-string p2, "\nWith parent "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104494
    iget-object p2, v0, LX/1RE;->a:Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    move-object p2, p2

    .line 1104495
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104496
    iget-object p2, v0, LX/1RE;->b:LX/1RE;

    move-object v0, p2

    .line 1104497
    goto :goto_4

    .line 1104498
    :cond_2
    const-string v0, "\nThrown from "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104499
    iget-object v0, p0, LX/1RZ;->a:Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-object v0, v0

    .line 1104500
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1104501
    :cond_3
    const-string v0, "\n  Original Throwable: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104502
    instance-of v0, p1, LX/5Oo;

    if-eqz v0, :cond_5

    .line 1104503
    check-cast p1, LX/5Oo;

    iget-object v0, p1, LX/5Oo;->originalException:Ljava/lang/Throwable;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104504
    :goto_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1104505
    :cond_4
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1104506
    :cond_5
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5
.end method
