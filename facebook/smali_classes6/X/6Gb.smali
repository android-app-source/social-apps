.class public final enum LX/6Gb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6Gb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6Gb;

.field public static final enum G2:LX/6Gb;

.field public static final enum G3:LX/6Gb;

.field public static final enum G4:LX/6Gb;

.field public static final enum OtherMobile:LX/6Gb;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1070737
    new-instance v0, LX/6Gb;

    const-string v1, "G2"

    invoke-direct {v0, v1, v2}, LX/6Gb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Gb;->G2:LX/6Gb;

    .line 1070738
    new-instance v0, LX/6Gb;

    const-string v1, "G3"

    invoke-direct {v0, v1, v3}, LX/6Gb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Gb;->G3:LX/6Gb;

    .line 1070739
    new-instance v0, LX/6Gb;

    const-string v1, "G4"

    invoke-direct {v0, v1, v4}, LX/6Gb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Gb;->G4:LX/6Gb;

    .line 1070740
    new-instance v0, LX/6Gb;

    const-string v1, "OtherMobile"

    invoke-direct {v0, v1, v5}, LX/6Gb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/6Gb;->OtherMobile:LX/6Gb;

    .line 1070741
    const/4 v0, 0x4

    new-array v0, v0, [LX/6Gb;

    sget-object v1, LX/6Gb;->G2:LX/6Gb;

    aput-object v1, v0, v2

    sget-object v1, LX/6Gb;->G3:LX/6Gb;

    aput-object v1, v0, v3

    sget-object v1, LX/6Gb;->G4:LX/6Gb;

    aput-object v1, v0, v4

    sget-object v1, LX/6Gb;->OtherMobile:LX/6Gb;

    aput-object v1, v0, v5

    sput-object v0, LX/6Gb;->$VALUES:[LX/6Gb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1070742
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6Gb;
    .locals 1

    .prologue
    .line 1070743
    const-class v0, LX/6Gb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6Gb;

    return-object v0
.end method

.method public static values()[LX/6Gb;
    .locals 1

    .prologue
    .line 1070744
    sget-object v0, LX/6Gb;->$VALUES:[LX/6Gb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6Gb;

    return-object v0
.end method
