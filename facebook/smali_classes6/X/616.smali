.class public LX/616;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile h:LX/616;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1039646
    const-class v0, LX/616;

    sput-object v0, LX/616;->a:Ljava/lang/Class;

    .line 1039647
    const-string v0, "OMX.ittiam.video.encoder.avc"

    const-string v1, "OMX.Exynos.avc.enc"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/616;->b:LX/0Rf;

    .line 1039648
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "OMX.qcom.video.encoder.avc"

    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/616;->c:LX/0P1;

    .line 1039649
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v0

    const-string v1, "OMX.qcom.video.decoder.avc"

    invoke-virtual {v0, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    move-result-object v0

    invoke-virtual {v0}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    sput-object v0, LX/616;->d:LX/0Rf;

    .line 1039650
    const-string v0, "OMX.ittiam.video.decoder.avc"

    const-string v1, "OMX.Exynos.AVC.Decoder"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/616;->e:LX/0Rf;

    .line 1039651
    const-string v0, "GT-S6812i"

    const-string v1, "GT-I8552"

    const-string v2, "GT-I8552B"

    const-string v3, "GT-I8262B"

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/616;->f:LX/0Rf;

    .line 1039652
    const-string v0, "OMX.SEC.AVC.Encoder"

    const-string v1, "OMX.SEC.avc.enc"

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/616;->g:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1039644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1039645
    return-void
.end method

.method public static a(LX/0QB;)LX/616;
    .locals 3

    .prologue
    .line 1039653
    sget-object v0, LX/616;->h:LX/616;

    if-nez v0, :cond_1

    .line 1039654
    const-class v1, LX/616;

    monitor-enter v1

    .line 1039655
    :try_start_0
    sget-object v0, LX/616;->h:LX/616;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1039656
    if-eqz v2, :cond_0

    .line 1039657
    :try_start_1
    new-instance v0, LX/616;

    invoke-direct {v0}, LX/616;-><init>()V

    .line 1039658
    move-object v0, v0

    .line 1039659
    sput-object v0, LX/616;->h:LX/616;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1039660
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1039661
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1039662
    :cond_1
    sget-object v0, LX/616;->h:LX/616;

    return-object v0

    .line 1039663
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1039664
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;LX/610;)LX/617;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1039665
    sget-object v0, LX/610;->SURFACE:LX/610;

    if-ne p2, v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v0, v3, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1039666
    invoke-virtual {p0, p1, v2, v2, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 1039667
    sget-object v0, LX/610;->SURFACE:LX/610;

    if-ne p2, v0, :cond_2

    .line 1039668
    invoke-virtual {p0}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;

    move-result-object v0

    .line 1039669
    :goto_1
    new-instance v1, LX/617;

    sget-object v2, LX/611;->ENCODER:LX/611;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, v0, v3}, LX/617;-><init>(LX/611;Landroid/media/MediaCodec;Landroid/view/Surface;Z)V

    move-object v0, v1

    .line 1039670
    return-object v0

    .line 1039671
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method public static a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;Landroid/view/Surface;)LX/617;
    .locals 2
    .param p2    # Landroid/view/Surface;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1039635
    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v1, v0}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 1039636
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 1039637
    :cond_0
    new-instance v1, LX/617;

    sget-object p1, LX/611;->DECODER:LX/611;

    const/4 p2, 0x0

    invoke-direct {v1, p1, p0, p2, v0}, LX/617;-><init>(LX/611;Landroid/media/MediaCodec;Landroid/view/Surface;Z)V

    move-object v0, v1

    .line 1039638
    return-object v0
.end method

.method public static final a(Ljava/lang/String;Landroid/media/MediaFormat;LX/610;)LX/617;
    .locals 1

    .prologue
    .line 1039639
    sget-object v0, LX/612;->CODEC_VIDEO_H264:LX/612;

    iget-object v0, v0, LX/612;->value:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 1039640
    if-eqz v0, :cond_0

    .line 1039641
    invoke-static {p0}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    .line 1039642
    invoke-static {v0, p1, p2}, LX/616;->a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;LX/610;)LX/617;

    move-result-object v0

    return-object v0

    .line 1039643
    :cond_0
    invoke-static {p0}, LX/60t;->a(Ljava/lang/String;)LX/60t;

    move-result-object v0

    throw v0
.end method

.method public static final a(Ljava/lang/String;Landroid/media/MediaFormat;Landroid/view/Surface;)LX/617;
    .locals 1
    .param p2    # Landroid/view/Surface;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1039631
    invoke-static {p0}, LX/616;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1039632
    invoke-static {p0}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    .line 1039633
    invoke-static {v0, p1, p2}, LX/616;->a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;Landroid/view/Surface;)LX/617;

    move-result-object v0

    return-object v0

    .line 1039634
    :cond_0
    invoke-static {p0}, LX/60t;->a(Ljava/lang/String;)LX/60t;

    move-result-object v0

    throw v0
.end method

.method public static a(Ljava/lang/String;I)LX/61A;
    .locals 2

    .prologue
    .line 1039627
    const/4 v0, 0x0

    .line 1039628
    const-string v1, "qcom"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1039629
    const/16 v0, 0x800

    .line 1039630
    :cond_0
    new-instance v1, LX/61A;

    invoke-direct {v1, p0, p1, v0}, LX/61A;-><init>(Ljava/lang/String;II)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1039626
    sget-object v0, LX/612;->CODEC_VIDEO_H264:LX/612;

    iget-object v0, v0, LX/612;->value:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/612;->CODEC_VIDEO_H263:LX/612;

    iget-object v0, v0, LX/612;->value:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/612;->CODEC_VIDEO_MPEG4:LX/612;

    iget-object v0, v0, LX/612;->value:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/612;->CODEC_VIDEO_VP8:LX/612;

    iget-object v0, v0, LX/612;->value:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()LX/60w;
    .locals 2

    .prologue
    .line 1039620
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 1039621
    const/4 v0, 0x0

    .line 1039622
    :goto_0
    return-object v0

    .line 1039623
    :cond_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9500"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1039624
    sget-object v0, LX/60w;->BGRA:LX/60w;

    goto :goto_0

    .line 1039625
    :cond_1
    sget-object v0, LX/60w;->RGBA:LX/60w;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/61A;
    .locals 11

    .prologue
    .line 1039537
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1039538
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v1

    .line 1039539
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_8

    .line 1039540
    invoke-static {v0}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v2

    .line 1039541
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1039542
    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1039543
    sget-object v3, LX/616;->c:LX/0P1;

    invoke-virtual {v3, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1039544
    sget-object v0, LX/616;->c:LX/0P1;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v2, v0}, LX/616;->a(Ljava/lang/String;I)LX/61A;

    move-result-object v0

    .line 1039545
    :goto_2
    move-object v0, v0

    .line 1039546
    if-eqz v0, :cond_2

    .line 1039547
    :cond_0
    return-object v0

    .line 1039548
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1039549
    :cond_2
    const/4 v1, 0x0

    .line 1039550
    const/4 v2, 0x0

    .line 1039551
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1039552
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v5

    move v3, v2

    .line 1039553
    :goto_3
    if-ge v3, v5, :cond_6

    .line 1039554
    invoke-static {v3}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v0

    .line 1039555
    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1039556
    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 1039557
    const-string v7, "video/avc"

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1039558
    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1039559
    sget-object v7, LX/616;->b:LX/0Rf;

    invoke-virtual {v7, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1039560
    const-string v7, "video/avc"

    invoke-virtual {v0, v7}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v0

    .line 1039561
    iget-object v7, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->colorFormats:[I

    array-length v8, v7

    move v0, v2

    :goto_4
    if-ge v0, v8, :cond_5

    aget v9, v7, v0

    .line 1039562
    const-string v10, "OMX.SEC.avc.enc"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/16 v10, 0x13

    if-eq v9, v10, :cond_4

    .line 1039563
    :cond_3
    sparse-switch v9, :sswitch_data_0

    .line 1039564
    :cond_4
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1039565
    :sswitch_0
    invoke-static {v6, v9}, LX/616;->a(Ljava/lang/String;I)LX/61A;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1039566
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1039567
    :cond_6
    move-object v3, v4

    .line 1039568
    const-string v0, ", "

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    new-instance v2, LX/615;

    invoke-direct {v2, p0}, LX/615;-><init>(LX/616;)V

    invoke-static {v3, v2}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0QK;)Ljava/lang/Iterable;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    .line 1039569
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 1039570
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    move-object v0, v1

    .line 1039571
    :goto_6
    move-object v0, v0

    .line 1039572
    if-nez v0, :cond_0

    .line 1039573
    sget-object v0, LX/612;->CODEC_VIDEO_H264:LX/612;

    iget-object v0, v0, LX/612;->value:Ljava/lang/String;

    invoke-static {v0}, LX/60t;->a(Ljava/lang/String;)LX/60t;

    move-result-object v0

    throw v0

    .line 1039574
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 1039575
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 1039576
    :cond_9
    invoke-static {}, LX/4z1;->u()LX/4z1;

    move-result-object v4

    .line 1039577
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61A;

    .line 1039578
    iget-object v5, v0, LX/61A;->a:Ljava/lang/String;

    invoke-interface {v4, v5, v0}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_7

    .line 1039579
    :cond_a
    sget-object v0, LX/616;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_8
    if-ge v2, v5, :cond_d

    sget-object v0, LX/616;->g:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1039580
    invoke-interface {v4, v0}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 1039581
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61A;

    .line 1039582
    if-eqz v0, :cond_b

    goto :goto_6

    .line 1039583
    :cond_c
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 1039584
    :cond_d
    invoke-static {v3, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/61A;

    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x15 -> :sswitch_0
        0x7f000100 -> :sswitch_0
    .end sparse-switch
.end method

.method public final b(Ljava/lang/String;)LX/619;
    .locals 4

    .prologue
    .line 1039585
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1039586
    invoke-static {p1}, LX/616;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1039587
    const/4 v1, 0x0

    .line 1039588
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v2

    move v0, v1

    .line 1039589
    :goto_1
    if-ge v0, v2, :cond_5

    .line 1039590
    invoke-static {v0}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v3

    .line 1039591
    invoke-virtual {v3}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result p0

    if-nez p0, :cond_4

    .line 1039592
    invoke-virtual {v3}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    .line 1039593
    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1039594
    invoke-virtual {v3}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1039595
    sget-object p0, LX/616;->d:LX/0Rf;

    invoke-virtual {p0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1039596
    new-instance v0, LX/619;

    invoke-static {}, LX/616;->e()LX/60w;

    move-result-object v2

    invoke-direct {v0, v3, v1, v2}, LX/619;-><init>(Ljava/lang/String;ZLX/60w;)V

    .line 1039597
    :goto_2
    move-object v0, v0

    .line 1039598
    if-eqz v0, :cond_2

    .line 1039599
    :cond_0
    return-object v0

    .line 1039600
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1039601
    :cond_2
    const/4 v0, 0x0

    .line 1039602
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v2

    move v1, v0

    .line 1039603
    :goto_3
    if-ge v1, v2, :cond_7

    .line 1039604
    invoke-static {v1}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v3

    .line 1039605
    invoke-virtual {v3}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result p0

    if-nez p0, :cond_6

    .line 1039606
    invoke-virtual {v3}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    .line 1039607
    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1039608
    invoke-virtual {v3}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1039609
    sget-object p0, LX/616;->e:LX/0Rf;

    invoke-virtual {p0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    .line 1039610
    const-string v1, "OMX.MTK.VIDEO.DECODER.AVC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1039611
    const/4 v0, 0x1

    .line 1039612
    :cond_3
    new-instance v1, LX/619;

    invoke-static {}, LX/616;->e()LX/60w;

    move-result-object v2

    invoke-direct {v1, v3, v0, v2}, LX/619;-><init>(Ljava/lang/String;ZLX/60w;)V

    move-object v0, v1

    .line 1039613
    :goto_4
    move-object v0, v0

    .line 1039614
    if-nez v0, :cond_0

    .line 1039615
    invoke-static {p1}, LX/60t;->a(Ljava/lang/String;)LX/60t;

    move-result-object v0

    throw v0

    .line 1039616
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1039617
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 1039618
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1039619
    :cond_7
    const/4 v0, 0x0

    goto :goto_4
.end method
