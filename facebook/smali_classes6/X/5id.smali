.class public final LX/5id;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:D

.field public c:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 983853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v13, 0x0

    const-wide/16 v4, 0x0

    const/4 v12, 0x0

    .line 983854
    new-instance v0, LX/186;

    const/16 v2, 0x80

    invoke-direct {v0, v2}, LX/186;-><init>(I)V

    .line 983855
    iget-object v2, p0, LX/5id;->a:Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 983856
    iget-object v3, p0, LX/5id;->c:Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 983857
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 983858
    invoke-virtual {v0, v13, v2}, LX/186;->b(II)V

    .line 983859
    iget-wide v2, p0, LX/5id;->b:D

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 983860
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 983861
    const/4 v7, 0x3

    iget-wide v8, p0, LX/5id;->d:D

    move-object v6, v0

    move-wide v10, v4

    invoke-virtual/range {v6 .. v11}, LX/186;->a(IDD)V

    .line 983862
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    .line 983863
    invoke-virtual {v0, v2}, LX/186;->d(I)V

    .line 983864
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 983865
    invoke-virtual {v3, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 983866
    new-instance v2, LX/15i;

    move-object v4, v12

    move-object v5, v12

    move v6, v1

    move-object v7, v12

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 983867
    new-instance v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;

    invoke-direct {v0, v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$FrameTextAssetsModel$NodesModel$LandscapeAnchoringModel;-><init>(LX/15i;)V

    .line 983868
    return-object v0
.end method
