.class public final LX/649;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:LX/0yW;


# direct methods
.method public constructor <init>(LX/0yW;)V
    .locals 0

    .prologue
    .line 1044208
    iput-object p1, p0, LX/649;->a:LX/0yW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 1044209
    const-class v1, LX/0yW;

    monitor-enter v1

    .line 1044210
    :try_start_0
    iget-object v0, p0, LX/649;->a:LX/0yW;

    .line 1044211
    iget-wide v2, v0, LX/0yW;->n:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, v0, LX/0yW;->n:J

    .line 1044212
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1044213
    iget-object v0, p0, LX/649;->a:LX/0yW;

    invoke-static {v0}, LX/0yW;->k(LX/0yW;)V

    .line 1044214
    return-void

    .line 1044215
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1044216
    const-class v1, LX/0yW;

    monitor-enter v1

    .line 1044217
    :try_start_0
    iget-object v0, p0, LX/649;->a:LX/0yW;

    .line 1044218
    iget-wide v2, v0, LX/0yW;->m:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, v0, LX/0yW;->m:J

    .line 1044219
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1044220
    iget-object v0, p0, LX/649;->a:LX/0yW;

    invoke-static {v0}, LX/0yW;->k(LX/0yW;)V

    .line 1044221
    return-void

    .line 1044222
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
