.class public final LX/657;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/64R;

.field public final b:Ljava/net/Proxy;

.field public final c:Ljava/net/InetSocketAddress;


# direct methods
.method public constructor <init>(LX/64R;Ljava/net/Proxy;Ljava/net/InetSocketAddress;)V
    .locals 2

    .prologue
    .line 1046440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1046441
    if-nez p1, :cond_0

    .line 1046442
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "address == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046443
    :cond_0
    if-nez p2, :cond_1

    .line 1046444
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "proxy == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046445
    :cond_1
    if-nez p3, :cond_2

    .line 1046446
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "inetSocketAddress == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1046447
    :cond_2
    iput-object p1, p0, LX/657;->a:LX/64R;

    .line 1046448
    iput-object p2, p0, LX/657;->b:Ljava/net/Proxy;

    .line 1046449
    iput-object p3, p0, LX/657;->c:Ljava/net/InetSocketAddress;

    .line 1046450
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1046451
    instance-of v1, p1, LX/657;

    if-eqz v1, :cond_0

    .line 1046452
    check-cast p1, LX/657;

    .line 1046453
    iget-object v1, p0, LX/657;->a:LX/64R;

    iget-object v2, p1, LX/657;->a:LX/64R;

    invoke-virtual {v1, v2}, LX/64R;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/657;->b:Ljava/net/Proxy;

    iget-object v2, p1, LX/657;->b:Ljava/net/Proxy;

    .line 1046454
    invoke-virtual {v1, v2}, Ljava/net/Proxy;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/657;->c:Ljava/net/InetSocketAddress;

    iget-object v2, p1, LX/657;->c:Ljava/net/InetSocketAddress;

    .line 1046455
    invoke-virtual {v1, v2}, Ljava/net/InetSocketAddress;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 1046456
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1046457
    iget-object v0, p0, LX/657;->a:LX/64R;

    invoke-virtual {v0}, LX/64R;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1046458
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/657;->b:Ljava/net/Proxy;

    invoke-virtual {v1}, Ljava/net/Proxy;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1046459
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/657;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1046460
    return v0
.end method
