.class public LX/5zm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/5zm;


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private final b:LX/0SG;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1035707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035708
    iput-object p1, p0, LX/5zm;->a:Landroid/content/ContentResolver;

    .line 1035709
    iput-object p2, p0, LX/5zm;->b:LX/0SG;

    .line 1035710
    return-void
.end method

.method public static a(LX/0QB;)LX/5zm;
    .locals 5

    .prologue
    .line 1035711
    sget-object v0, LX/5zm;->c:LX/5zm;

    if-nez v0, :cond_1

    .line 1035712
    const-class v1, LX/5zm;

    monitor-enter v1

    .line 1035713
    :try_start_0
    sget-object v0, LX/5zm;->c:LX/5zm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1035714
    if-eqz v2, :cond_0

    .line 1035715
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1035716
    new-instance p0, LX/5zm;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/5zm;-><init>(Landroid/content/ContentResolver;LX/0SG;)V

    .line 1035717
    move-object v0, p0

    .line 1035718
    sput-object v0, LX/5zm;->c:LX/5zm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1035719
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1035720
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1035721
    :cond_1
    sget-object v0, LX/5zm;->c:LX/5zm;

    return-object v0

    .line 1035722
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1035723
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zl;
    .locals 3

    .prologue
    .line 1035724
    new-instance v0, LX/5zl;

    iget-object v1, p0, LX/5zm;->a:Landroid/content/ContentResolver;

    iget-object v2, p0, LX/5zm;->b:LX/0SG;

    invoke-direct {v0, v1, v2, p1}, LX/5zl;-><init>(Landroid/content/ContentResolver;LX/0SG;Lcom/facebook/ui/media/attachments/MediaResource;)V

    return-object v0
.end method
