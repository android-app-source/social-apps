.class public final LX/5m1;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 999623
    const-class v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;

    const v0, -0x73909194

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FBCitySearchQuery"

    const-string v6, "21610d54455efdd829532ae748a5f2b8"

    const-string v7, "checkin_search_query"

    const-string v8, "10155069964266729"

    const-string v9, "10155259086831729"

    const-string v0, "place_id"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 999624
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 999615
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 999616
    sparse-switch v0, :sswitch_data_0

    .line 999617
    :goto_0
    return-object p1

    .line 999618
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 999619
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 999620
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 999621
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 999622
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ab91091 -> :sswitch_0
        -0x309006ed -> :sswitch_1
        -0xa0d333f -> :sswitch_2
        0x41f51a18 -> :sswitch_3
        0x537d1e94 -> :sswitch_4
    .end sparse-switch
.end method
