.class public final LX/5Jj;
.super Landroid/widget/HorizontalScrollView;
.source ""


# instance fields
.field public final a:Lcom/facebook/components/ComponentView;

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 897641
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 897642
    new-instance v0, Lcom/facebook/components/ComponentView;

    invoke-direct {v0, p1}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/5Jj;->a:Lcom/facebook/components/ComponentView;

    .line 897643
    iget-object v0, p0, LX/5Jj;->a:Lcom/facebook/components/ComponentView;

    invoke-virtual {p0, v0}, LX/5Jj;->addView(Landroid/view/View;)V

    .line 897644
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 897635
    iget-object v0, p0, LX/5Jj;->a:Lcom/facebook/components/ComponentView;

    iget v1, p0, LX/5Jj;->b:I

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget v2, p0, LX/5Jj;->c:I

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/components/ComponentView;->measure(II)V

    .line 897636
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/5Jj;->setMeasuredDimension(II)V

    .line 897637
    return-void
.end method

.method public final onScrollChanged(IIII)V
    .locals 0

    .prologue
    .line 897638
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/HorizontalScrollView;->onScrollChanged(IIII)V

    .line 897639
    iget-object p1, p0, LX/5Jj;->a:Lcom/facebook/components/ComponentView;

    invoke-virtual {p1}, Lcom/facebook/components/ComponentView;->j()V

    .line 897640
    return-void
.end method
