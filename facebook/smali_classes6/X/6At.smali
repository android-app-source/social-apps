.class public final LX/6At;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1060597
    const-class v1, Lcom/facebook/api/graphql/seenby/FetchSeenByGraphQLModels$FetchSeenByModel;

    const v0, 0x21d36df1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchSeenBy"

    const-string v6, "5ce93e87a9d1e44e97d9c7902bf1ad59"

    const-string v7, "node"

    const-string v8, "10155207367836729"

    const-string v9, "10155259088261729"

    .line 1060598
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1060599
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1060600
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1060601
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1060602
    sparse-switch v0, :sswitch_data_0

    .line 1060603
    :goto_0
    return-object p1

    .line 1060604
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1060605
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1060606
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1060607
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1060608
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1060609
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1060610
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1060611
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x513764de -> :sswitch_6
        -0x4da3e3eb -> :sswitch_2
        -0x41a91745 -> :sswitch_4
        -0x3b85b241 -> :sswitch_7
        0x291d8de0 -> :sswitch_5
        0x5199dd38 -> :sswitch_0
        0x776074c0 -> :sswitch_3
        0x7a56c3db -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1060612
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1060613
    :goto_1
    return v0

    .line 1060614
    :pswitch_0
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_1
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 1060615
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1060616
    :pswitch_3
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x36
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
