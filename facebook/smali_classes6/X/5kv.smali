.class public final LX/5kv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 996523
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 996524
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 996525
    :goto_0
    return v1

    .line 996526
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 996527
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 996528
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 996529
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 996530
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 996531
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 996532
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 996533
    :cond_2
    const-string v6, "object"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 996534
    invoke-static {p0, p1}, LX/5kw;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 996535
    :cond_3
    const-string v6, "taggable_activity"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 996536
    invoke-static {p0, p1}, LX/5Lo;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 996537
    :cond_4
    const-string v6, "taggable_activity_icon"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 996538
    invoke-static {p0, p1}, LX/5ku;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 996539
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 996540
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 996541
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 996542
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 996543
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 996544
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 996545
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 996546
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 996547
    if-eqz v0, :cond_0

    .line 996548
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996549
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 996550
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 996551
    if-eqz v0, :cond_1

    .line 996552
    const-string v1, "object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996553
    invoke-static {p0, v0, p2}, LX/5kw;->a(LX/15i;ILX/0nX;)V

    .line 996554
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 996555
    if-eqz v0, :cond_2

    .line 996556
    const-string v1, "taggable_activity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996557
    invoke-static {p0, v0, p2, p3}, LX/5Lo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 996558
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 996559
    if-eqz v0, :cond_3

    .line 996560
    const-string v1, "taggable_activity_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996561
    invoke-static {p0, v0, p2, p3}, LX/5ku;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 996562
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 996563
    return-void
.end method
