.class public final LX/534;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/52p;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/52p;"
    }
.end annotation


# static fields
.field private static final d:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicLongFieldUpdater",
            "<",
            "LX/534;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0zZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zZ",
            "<-TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private volatile c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 826608
    const-class v0, LX/534;

    const-string v1, "c"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    move-result-object v0

    sput-object v0, LX/534;->d:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    return-void
.end method

.method public constructor <init>(LX/0zZ;Ljava/util/Iterator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zZ",
            "<-TT;>;",
            "Ljava/util/Iterator",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 826609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 826610
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/534;->c:J

    .line 826611
    iput-object p1, p0, LX/534;->a:LX/0zZ;

    .line 826612
    iput-object p2, p0, LX/534;->b:Ljava/util/Iterator;

    .line 826613
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 9

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const-wide/16 v6, 0x0

    .line 826614
    sget-object v0, LX/534;->d:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->get(Ljava/lang/Object;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 826615
    :cond_0
    :goto_0
    return-void

    .line 826616
    :cond_1
    cmp-long v0, p1, v2

    if-nez v0, :cond_3

    .line 826617
    sget-object v0, LX/534;->d:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->set(Ljava/lang/Object;J)V

    .line 826618
    :goto_1
    iget-object v0, p0, LX/534;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 826619
    iget-object v0, p0, LX/534;->a:LX/0zZ;

    invoke-virtual {v0}, LX/0zZ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 826620
    iget-object v0, p0, LX/534;->a:LX/0zZ;

    iget-object v1, p0, LX/534;->b:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0zZ;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 826621
    :cond_2
    iget-object v0, p0, LX/534;->a:LX/0zZ;

    invoke-virtual {v0}, LX/0zZ;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 826622
    iget-object v0, p0, LX/534;->a:LX/0zZ;

    invoke-virtual {v0}, LX/0zZ;->R_()V

    goto :goto_0

    .line 826623
    :cond_3
    cmp-long v0, p1, v6

    if-lez v0, :cond_0

    .line 826624
    sget-object v0, LX/534;->d:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->getAndAdd(Ljava/lang/Object;J)J

    move-result-wide v0

    .line 826625
    cmp-long v0, v0, v6

    if-nez v0, :cond_0

    .line 826626
    :cond_4
    iget-wide v2, p0, LX/534;->c:J

    move-wide v0, v2

    .line 826627
    :goto_2
    iget-object v4, p0, LX/534;->b:Ljava/util/Iterator;

    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    const-wide/16 v4, 0x1

    sub-long/2addr v0, v4

    cmp-long v4, v0, v6

    if-ltz v4, :cond_5

    .line 826628
    iget-object v4, p0, LX/534;->a:LX/0zZ;

    invoke-virtual {v4}, LX/0zZ;->c()Z

    move-result v4

    if-nez v4, :cond_0

    .line 826629
    iget-object v4, p0, LX/534;->a:LX/0zZ;

    iget-object v5, p0, LX/534;->b:Ljava/util/Iterator;

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0zZ;->a(Ljava/lang/Object;)V

    goto :goto_2

    .line 826630
    :cond_5
    iget-object v0, p0, LX/534;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    .line 826631
    iget-object v0, p0, LX/534;->a:LX/0zZ;

    invoke-virtual {v0}, LX/0zZ;->R_()V

    goto :goto_0

    .line 826632
    :cond_6
    sget-object v0, LX/534;->d:Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;

    neg-long v2, v2

    invoke-virtual {v0, p0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLongFieldUpdater;->addAndGet(Ljava/lang/Object;J)J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_4

    goto/16 :goto_0
.end method
