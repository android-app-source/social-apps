.class public LX/5sU;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private final a:Landroid/view/View;

.field private final b:F

.field private final c:F


# direct methods
.method public constructor <init>(Landroid/view/View;FF)V
    .locals 1

    .prologue
    .line 1012870
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1012871
    iput-object p1, p0, LX/5sU;->a:Landroid/view/View;

    .line 1012872
    iput p2, p0, LX/5sU;->b:F

    .line 1012873
    sub-float v0, p3, p2

    iput v0, p0, LX/5sU;->c:F

    .line 1012874
    new-instance v0, LX/5sT;

    invoke-direct {v0, p1}, LX/5sT;-><init>(Landroid/view/View;)V

    invoke-virtual {p0, v0}, LX/5sU;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1012875
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 1012876
    iget-object v0, p0, LX/5sU;->a:Landroid/view/View;

    iget v1, p0, LX/5sU;->b:F

    iget v2, p0, LX/5sU;->c:F

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1012877
    return-void
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 1012878
    const/4 v0, 0x0

    return v0
.end method
