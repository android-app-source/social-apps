.class public LX/6ef;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final b:Ljava/lang/Object;


# instance fields
.field private final a:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "LX/6ee;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1118017
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/6ef;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1118018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118019
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/32 v2, 0x7b98a000

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/6ef;->a:LX/0QI;

    .line 1118020
    return-void
.end method

.method public static a(LX/0QB;)LX/6ef;
    .locals 7

    .prologue
    .line 1118021
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1118022
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1118023
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1118024
    if-nez v1, :cond_0

    .line 1118025
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1118026
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1118027
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1118028
    sget-object v1, LX/6ef;->b:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1118029
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1118030
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1118031
    :cond_1
    if-nez v1, :cond_4

    .line 1118032
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1118033
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1118034
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    .line 1118035
    new-instance v0, LX/6ef;

    invoke-direct {v0}, LX/6ef;-><init>()V

    .line 1118036
    move-object v1, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1118037
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1118038
    if-nez v1, :cond_2

    .line 1118039
    sget-object v0, LX/6ef;->b:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ef;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1118040
    :goto_1
    if-eqz v0, :cond_3

    .line 1118041
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1118042
    :goto_3
    check-cast v0, LX/6ef;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1118043
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1118044
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1118045
    :catchall_1
    move-exception v0

    .line 1118046
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1118047
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1118048
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1118049
    :cond_2
    :try_start_8
    sget-object v0, LX/6ef;->b:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ef;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1118050
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6ef;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ee;

    .line 1118051
    sget-object v1, LX/6ee;->FINISHED:LX/6ee;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/6ee;->CANCELED:LX/6ee;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 1118052
    :cond_0
    const/4 v0, 0x0

    .line 1118053
    :goto_0
    monitor-exit p0

    return v0

    .line 1118054
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/6ef;->a:LX/0QI;

    sget-object v1, LX/6ee;->CANCELED:LX/6ee;

    invoke-interface {v0, p1, v1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1118055
    const/4 v0, 0x1

    goto :goto_0

    .line 1118056
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1118057
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/6ef;->d(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 1118058
    const/4 v0, 0x0

    .line 1118059
    :goto_0
    monitor-exit p0

    return v0

    .line 1118060
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/6ef;->a:LX/0QI;

    sget-object v1, LX/6ee;->RUNNING:LX/6ee;

    invoke-interface {v0, p1, v1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1118061
    const/4 v0, 0x1

    goto :goto_0

    .line 1118062
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1118063
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/6ef;->d(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 1118064
    const/4 v0, 0x0

    .line 1118065
    :goto_0
    monitor-exit p0

    return v0

    .line 1118066
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/6ef;->a:LX/0QI;

    sget-object v1, LX/6ee;->FINISHED:LX/6ee;

    invoke-interface {v0, p1, v1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1118067
    const/4 v0, 0x1

    goto :goto_0

    .line 1118068
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1118069
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6ef;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/6ee;->CANCELED:LX/6ee;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 1118070
    const/4 v0, 0x1

    .line 1118071
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1118072
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
