.class public LX/60m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;",
        "Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1039025
    const-class v0, LX/60m;

    sput-object v0, LX/60m;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1039026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/1pN;)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;
    .locals 19

    .prologue
    .line 1039027
    invoke-virtual/range {p0 .. p0}, LX/1pN;->d()LX/0lF;

    move-result-object v4

    .line 1039028
    const-string v2, "rtmp_publish_url"

    invoke-virtual {v4, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 1039029
    const-string v3, "social_context_entity_id"

    invoke-virtual {v4, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 1039030
    const-string v5, "id"

    invoke-virtual {v4, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 1039031
    const-string v6, "min_broadcast_duration"

    invoke-virtual {v4, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 1039032
    const-string v7, "max_time_in_seconds"

    invoke-virtual {v4, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 1039033
    const-string v8, "speed_test_ui_timeout"

    invoke-virtual {v4, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    .line 1039034
    const-string v9, "send_stream_interrupted_interval_in_seconds"

    invoke-virtual {v4, v9}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    .line 1039035
    const-string v10, "stream_disk_recording_enabled"

    invoke-virtual {v4, v10}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    .line 1039036
    const-string v11, "audio_only_format_stream_bit_rate"

    invoke-virtual {v4, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v11

    .line 1039037
    const-string v12, "client_render_duration_ms"

    invoke-virtual {v4, v12}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v12

    .line 1039038
    const-string v13, "broadcaster_interruption_limit_in_seconds"

    invoke-virtual {v4, v13}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v13

    .line 1039039
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039040
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039041
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039042
    invoke-static {v4}, LX/60m;->a(LX/0lF;)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    move-result-object v14

    .line 1039043
    invoke-static {v4, v14}, LX/60m;->a(LX/0lF;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;

    move-result-object v15

    .line 1039044
    invoke-static {v4}, LX/60m;->b(LX/0lF;)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;

    move-result-object v16

    .line 1039045
    const-string v17, "commercial_break_settings"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->a(LX/0lF;)Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    move-result-object v17

    .line 1039046
    new-instance v18, LX/60p;

    invoke-direct/range {v18 .. v18}, LX/60p;-><init>()V

    invoke-virtual {v2}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, LX/60p;->a(Ljava/lang/String;)LX/60p;

    move-result-object v2

    invoke-virtual {v3}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/60p;->b(Ljava/lang/String;)LX/60p;

    move-result-object v2

    invoke-virtual {v5}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/60p;->c(Ljava/lang/String;)LX/60p;

    move-result-object v5

    if-nez v6, :cond_0

    const-wide/16 v2, 0x4

    :goto_0
    invoke-virtual {v5, v2, v3}, LX/60p;->a(J)LX/60p;

    move-result-object v5

    if-nez v7, :cond_1

    const-wide/16 v2, 0xe10

    :goto_1
    invoke-virtual {v5, v2, v3}, LX/60p;->b(J)LX/60p;

    move-result-object v5

    if-nez v8, :cond_2

    const-wide/16 v2, 0x7

    :goto_2
    invoke-virtual {v5, v2, v3}, LX/60p;->c(J)LX/60p;

    move-result-object v5

    if-nez v9, :cond_3

    const-wide/16 v2, 0x0

    :goto_3
    invoke-virtual {v5, v2, v3}, LX/60p;->d(J)LX/60p;

    move-result-object v2

    invoke-virtual {v2, v14}, LX/60p;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)LX/60p;

    move-result-object v2

    invoke-virtual {v2, v15}, LX/60p;->b(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)LX/60p;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/60p;->a(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;)LX/60p;

    move-result-object v2

    invoke-virtual {v4}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/60p;->d(Ljava/lang/String;)LX/60p;

    move-result-object v3

    if-nez v10, :cond_4

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v3, v2}, LX/60p;->a(Z)LX/60p;

    move-result-object v4

    if-nez v12, :cond_5

    const-wide/16 v2, 0xf

    :goto_5
    invoke-virtual {v4, v2, v3}, LX/60p;->e(J)LX/60p;

    move-result-object v3

    if-nez v13, :cond_6

    const/16 v2, 0xb4

    :goto_6
    invoke-virtual {v3, v2}, LX/60p;->a(I)LX/60p;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, LX/60p;->a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;)LX/60p;

    move-result-object v3

    if-eqz v11, :cond_7

    invoke-virtual {v11}, LX/0lF;->C()I

    move-result v2

    :goto_7
    invoke-virtual {v3, v2}, LX/60p;->b(I)LX/60p;

    move-result-object v2

    invoke-virtual {v2}, LX/60p;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v2

    return-object v2

    :cond_0
    invoke-virtual {v6}, LX/0lF;->D()J

    move-result-wide v2

    goto :goto_0

    :cond_1
    invoke-virtual {v7}, LX/0lF;->D()J

    move-result-wide v2

    goto :goto_1

    :cond_2
    invoke-virtual {v8}, LX/0lF;->D()J

    move-result-wide v2

    goto :goto_2

    :cond_3
    invoke-virtual {v9}, LX/0lF;->D()J

    move-result-wide v2

    goto :goto_3

    :cond_4
    invoke-virtual {v10}, LX/0lF;->F()Z

    move-result v2

    goto :goto_4

    :cond_5
    invoke-virtual {v12}, LX/0lF;->D()J

    move-result-wide v2

    goto :goto_5

    :cond_6
    invoke-virtual {v13}, LX/0lF;->C()I

    move-result v2

    goto :goto_6

    :cond_7
    const/4 v2, 0x0

    goto :goto_7
.end method

.method private static a(LX/0lF;)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1039047
    :try_start_0
    const-string v1, "android_video_profile"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1039048
    if-eqz v1, :cond_1

    .line 1039049
    invoke-virtual {v1}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 1039050
    :goto_0
    new-instance v1, LX/60r;

    invoke-direct {v1}, LX/60r;-><init>()V

    const-string v4, "stream_video_width"

    invoke-virtual {p0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->w()I

    move-result v4

    .line 1039051
    iput v4, v1, LX/60r;->a:I

    .line 1039052
    move-object v1, v1

    .line 1039053
    const-string v4, "stream_video_height"

    invoke-virtual {p0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->w()I

    move-result v4

    .line 1039054
    iput v4, v1, LX/60r;->b:I

    .line 1039055
    move-object v1, v1

    .line 1039056
    const-string v4, "stream_video_bit_rate"

    invoke-virtual {p0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->w()I

    move-result v4

    .line 1039057
    iput v4, v1, LX/60r;->c:I

    .line 1039058
    move-object v1, v1

    .line 1039059
    const-string v4, "stream_video_fps"

    invoke-virtual {p0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->w()I

    move-result v4

    .line 1039060
    iput v4, v1, LX/60r;->d:I

    .line 1039061
    move-object v4, v1

    .line 1039062
    const-string v1, "stream_video_allow_b_frames"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->w()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 1039063
    :goto_1
    iput-boolean v1, v4, LX/60r;->e:Z

    .line 1039064
    move-object v1, v4

    .line 1039065
    iput-object v3, v1, LX/60r;->f:Ljava/lang/String;

    .line 1039066
    move-object v1, v1

    .line 1039067
    invoke-virtual {v1}, LX/60r;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1039068
    :goto_2
    return-object v0

    :cond_0
    move v1, v2

    .line 1039069
    goto :goto_1

    .line 1039070
    :catch_0
    move-exception v1

    .line 1039071
    sget-object v3, LX/60m;->a:Ljava/lang/Class;

    const-string v4, "Error getting VideoStreamingConfig"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v1, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_1
    move-object v3, v0

    goto :goto_0
.end method

.method private static a(LX/0lF;Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1039072
    :try_start_0
    const-string v1, "audio_only_video_width"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->C()I

    move-result v1

    if-nez v1, :cond_0

    .line 1039073
    :goto_0
    return-object v0

    .line 1039074
    :cond_0
    new-instance v1, LX/60r;

    invoke-direct {v1, p1}, LX/60r;-><init>(Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;)V

    const-string v2, "audio_only_video_width"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->C()I

    move-result v2

    .line 1039075
    iput v2, v1, LX/60r;->a:I

    .line 1039076
    move-object v1, v1

    .line 1039077
    const-string v2, "audio_only_video_height"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->C()I

    move-result v2

    .line 1039078
    iput v2, v1, LX/60r;->b:I

    .line 1039079
    move-object v1, v1

    .line 1039080
    const-string v2, "audio_only_video_bitrate"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->C()I

    move-result v2

    .line 1039081
    iput v2, v1, LX/60r;->c:I

    .line 1039082
    move-object v1, v1

    .line 1039083
    const-string v2, "audio_only_video_iframe_interval"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->C()I

    move-result v2

    .line 1039084
    iput v2, v1, LX/60r;->g:I

    .line 1039085
    move-object v1, v1

    .line 1039086
    invoke-virtual {v1}, LX/60r;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastVideoStreamingConfig;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1039087
    :catch_0
    move-exception v1

    .line 1039088
    sget-object v2, LX/60m;->a:Ljava/lang/Class;

    const-string v3, "Error getting VideoStreamingConfig"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b(LX/0lF;)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;
    .locals 4

    .prologue
    .line 1039089
    :try_start_0
    new-instance v0, LX/60k;

    invoke-direct {v0}, LX/60k;-><init>()V

    const-string v1, "stream_audio_sample_rate"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->w()I

    move-result v1

    .line 1039090
    iput v1, v0, LX/60k;->a:I

    .line 1039091
    move-object v0, v0

    .line 1039092
    const-string v1, "stream_audio_channels"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->w()I

    move-result v1

    .line 1039093
    iput v1, v0, LX/60k;->c:I

    .line 1039094
    move-object v0, v0

    .line 1039095
    const-string v1, "stream_audio_bit_rate"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->w()I

    move-result v1

    .line 1039096
    iput v1, v0, LX/60k;->b:I

    .line 1039097
    move-object v0, v0

    .line 1039098
    invoke-virtual {v0}, LX/60k;->a()Lcom/facebook/video/videostreaming/protocol/VideoBroadcastAudioStreamingConfig;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1039099
    :goto_0
    return-object v0

    .line 1039100
    :catch_0
    move-exception v0

    .line 1039101
    sget-object v1, LX/60m;->a:Ljava/lang/Class;

    const-string v2, "Error getting AudioStreamingConfig"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1039102
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1039103
    check-cast p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;

    .line 1039104
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1039105
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1039106
    const-string v2, "android_video_profile"

    const-string v3, "baseline"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039107
    const-string v2, "stream_network_use_ssl_factory"

    const-string v3, "0"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039108
    const-string v2, "client_render_duration_ms"

    const-wide/16 v4, 0xf

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039109
    const-string v2, "broadcaster_interruption_limit_in_seconds"

    const/16 v3, 0xb4

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039110
    const-string v2, "broadcaster_update_log_interval_in_seconds"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039111
    iget-boolean v2, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->b:Z

    if-eqz v2, :cond_0

    .line 1039112
    const-string v2, "audio_only_format_stream_bit_rate"

    const-string v3, "128000"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039113
    const-string v2, "audio_only_video_bitrate"

    const-string v3, "100000"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039114
    const-string v2, "audio_only_video_height"

    const-string v3, "360"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039115
    const-string v2, "audio_only_video_width"

    const-string v3, "640"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039116
    const-string v2, "audio_only_video_iframe_interval"

    const-string v3, "1"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039117
    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "broadcast_default_settings"

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1039118
    iget-boolean v1, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->c:Z

    if-eqz v1, :cond_1

    .line 1039119
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "is_audio_only"

    iget-boolean v3, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->c:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1039120
    :cond_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "newVideoBroadcastAndroid"

    .line 1039121
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1039122
    move-object v1, v1

    .line 1039123
    const-string v2, "POST"

    .line 1039124
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1039125
    move-object v1, v1

    .line 1039126
    iget-object v2, p1, Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitRequest;->a:Ljava/lang/String;

    .line 1039127
    if-nez v2, :cond_2

    .line 1039128
    const-string v3, "video_broadcasts"

    .line 1039129
    :goto_0
    move-object v2, v3

    .line 1039130
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1039131
    move-object v1, v1

    .line 1039132
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1039133
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1039134
    move-object v1, v1

    .line 1039135
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1039136
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1039137
    move-object v0, v1

    .line 1039138
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/video_broadcasts"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1039139
    invoke-static {p2}, LX/60m;->a(LX/1pN;)Lcom/facebook/video/videostreaming/protocol/VideoBroadcastInitResponse;

    move-result-object v0

    return-object v0
.end method
