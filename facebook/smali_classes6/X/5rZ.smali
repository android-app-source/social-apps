.class public final LX/5rZ;
.super LX/5qn;
.source ""


# instance fields
.field public final synthetic a:LX/5rl;


# direct methods
.method public constructor <init>(LX/5rl;LX/5pX;)V
    .locals 0

    .prologue
    .line 1011874
    iput-object p1, p0, LX/5rZ;->a:LX/5rl;

    .line 1011875
    invoke-direct {p0, p2}, LX/5qn;-><init>(LX/5pX;)V

    .line 1011876
    return-void
.end method

.method public synthetic constructor <init>(LX/5rl;LX/5pX;B)V
    .locals 0

    .prologue
    .line 1011885
    invoke-direct {p0, p1, p2}, LX/5rZ;-><init>(LX/5rl;LX/5pX;)V

    return-void
.end method

.method private b(J)V
    .locals 7

    .prologue
    .line 1011886
    :goto_0
    const-wide/16 v0, 0x10

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    .line 1011887
    const-wide/16 v2, 0x8

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 1011888
    iget-object v0, p0, LX/5rZ;->a:LX/5rl;

    iget-object v1, v0, LX/5rl;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 1011889
    :try_start_0
    iget-object v0, p0, LX/5rZ;->a:LX/5rl;

    iget-object v0, v0, LX/5rl;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1011890
    monitor-exit v1

    .line 1011891
    :cond_0
    return-void

    .line 1011892
    :cond_1
    iget-object v0, p0, LX/5rZ;->a:LX/5rl;

    iget-object v0, v0, LX/5rl;->j:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->pollFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5rT;

    .line 1011893
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1011894
    invoke-interface {v0}, LX/5rT;->a()V

    goto :goto_0

    .line 1011895
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(J)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x2000

    .line 1011877
    const-string v0, "dispatchNonBatchedUIOperations"

    invoke-static {v2, v3, v0}, LX/018;->a(JLjava/lang/String;)V

    .line 1011878
    :try_start_0
    invoke-direct {p0, p1, p2}, LX/5rZ;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1011879
    invoke-static {v2, v3}, LX/018;->a(J)V

    .line 1011880
    iget-object v0, p0, LX/5rZ;->a:LX/5rl;

    .line 1011881
    invoke-static {v0}, LX/5rl;->f(LX/5rl;)V

    .line 1011882
    invoke-static {}, LX/5r6;->a()LX/5r6;

    move-result-object v0

    sget-object v1, LX/5r4;->DISPATCH_UI:LX/5r4;

    invoke-virtual {v0, v1, p0}, LX/5r6;->a(LX/5r4;Landroid/view/Choreographer$FrameCallback;)V

    .line 1011883
    return-void

    .line 1011884
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, LX/018;->a(J)V

    throw v0
.end method
