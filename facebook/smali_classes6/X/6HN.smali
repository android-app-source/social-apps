.class public final LX/6HN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# instance fields
.field public final synthetic a:LX/6HU;


# direct methods
.method public constructor <init>(LX/6HU;)V
    .locals 0

    .prologue
    .line 1071705
    iput-object p1, p0, LX/6HN;->a:LX/6HU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAutoFocus(ZLandroid/hardware/Camera;)V
    .locals 3

    .prologue
    .line 1071706
    iget-object v0, p0, LX/6HN;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->n:LX/6Ha;

    if-eqz v0, :cond_0

    .line 1071707
    iget-object v0, p0, LX/6HN;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->n:LX/6Ha;

    const/4 v2, 0x4

    const/4 v1, 0x3

    .line 1071708
    iget p0, v0, LX/6Ha;->c:I

    const/4 p2, 0x2

    if-ne p0, p2, :cond_2

    .line 1071709
    if-eqz p1, :cond_1

    .line 1071710
    iput v1, v0, LX/6Ha;->c:I

    .line 1071711
    :goto_0
    invoke-static {v0}, LX/6Ha;->l(LX/6Ha;)V

    .line 1071712
    invoke-static {v0}, LX/6Ha;->j(LX/6Ha;)V

    .line 1071713
    :cond_0
    :goto_1
    return-void

    .line 1071714
    :cond_1
    iput v2, v0, LX/6Ha;->c:I

    goto :goto_0

    .line 1071715
    :cond_2
    iget p0, v0, LX/6Ha;->c:I

    const/4 p2, 0x1

    if-ne p0, p2, :cond_0

    .line 1071716
    if-eqz p1, :cond_3

    :goto_2
    iput v1, v0, LX/6Ha;->c:I

    .line 1071717
    iget v1, v0, LX/6Ha;->A:I

    invoke-static {v0, v1}, LX/6Ha;->a(LX/6Ha;I)V

    goto :goto_1

    :cond_3
    move v1, v2

    .line 1071718
    goto :goto_2
.end method
