.class public LX/5JR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 896973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 896963
    invoke-virtual {p0, p1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 896964
    :goto_0
    return-object p0

    .line 896965
    :cond_0
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 896966
    check-cast p0, Landroid/view/ViewGroup;

    .line 896967
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 896968
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p1}, LX/5JR;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 896969
    if-eqz v1, :cond_1

    move-object p0, v1

    .line 896970
    goto :goto_0

    .line 896971
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 896972
    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method
