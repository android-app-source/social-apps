.class public abstract LX/546;
.super LX/545;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/545",
        "<TE;>;"
    }
.end annotation


# static fields
.field public static final e:J


# instance fields
.field public volatile g:J


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 827720
    :try_start_0
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    const-class v1, LX/546;

    const-string v2, "g"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v0

    sput-wide v0, LX/546;->e:J
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 827721
    return-void

    .line 827722
    :catch_0
    move-exception v0

    .line 827723
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 827724
    invoke-direct {p0, p1}, LX/545;-><init>(I)V

    .line 827725
    return-void
.end method


# virtual methods
.method public final a(JJ)Z
    .locals 9

    .prologue
    .line 827726
    sget-object v0, LX/54I;->a:Lsun/misc/Unsafe;

    sget-wide v2, LX/546;->e:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    return v0
.end method
