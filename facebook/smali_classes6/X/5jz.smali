.class public final LX/5jz;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 989266
    const-class v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel;

    const v0, -0x1be4998a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "FetchPhotosMetadataQuery"

    const-string v6, "c57656187523bf0d5f5557074d84452b"

    const-string v7, "nodes"

    const-string v8, "10155207367721729"

    const/4 v9, 0x0

    .line 989267
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 989268
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 989269
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 989270
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 989271
    sparse-switch v0, :sswitch_data_0

    .line 989272
    :goto_0
    return-object p1

    .line 989273
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 989274
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 989275
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 989276
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 989277
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 989278
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 989279
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 989280
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 989281
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 989282
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 989283
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 989284
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 989285
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 989286
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6a24640d -> :sswitch_d
        -0x680de62a -> :sswitch_6
        -0x6326fdb3 -> :sswitch_4
        -0x51484e72 -> :sswitch_2
        -0x4496acc9 -> :sswitch_7
        -0x421ba035 -> :sswitch_5
        -0x2c889631 -> :sswitch_a
        -0x1b87b280 -> :sswitch_3
        -0x12efdeb3 -> :sswitch_8
        0x64212b1 -> :sswitch_c
        0xa1fa812 -> :sswitch_1
        0x214100e0 -> :sswitch_9
        0x73a026b5 -> :sswitch_b
        0x7c7626df -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 989261
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 989262
    :goto_1
    return v0

    .line 989263
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 989264
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 989265
    new-instance v0, Lcom/facebook/photos/data/protocol/FetchPhotosMetadataQuery$FetchPhotosMetadataQueryString$1;

    const-class v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/photos/data/protocol/FetchPhotosMetadataQuery$FetchPhotosMetadataQueryString$1;-><init>(LX/5jz;Ljava/lang/Class;)V

    return-object v0
.end method
