.class public LX/6KM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1076573
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/6KM;->a:Ljava/util/List;

    .line 1076574
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, LX/6KM;->b:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1076575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076576
    return-void
.end method

.method private static a(Ljava/util/List;I)Lorg/json/JSONArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;I)",
            "Lorg/json/JSONArray;"
        }
    .end annotation

    .prologue
    .line 1076577
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 1076578
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 1076579
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1076580
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1076581
    :cond_0
    return-object v1
.end method

.method public static declared-synchronized a(J)V
    .locals 4

    .prologue
    .line 1076582
    const-class v1, LX/6KM;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/6KM;->a:Ljava/util/List;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076583
    monitor-exit v1

    return-void

    .line 1076584
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(J)V
    .locals 4

    .prologue
    .line 1076585
    const-class v1, LX/6KM;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/6KM;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 1076586
    :goto_0
    monitor-exit v1

    return-void

    .line 1076587
    :cond_0
    :try_start_1
    sget-object v0, LX/6KM;->b:Ljava/util/List;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1076588
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized clear()V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1076589
    const-class v1, LX/6KM;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/6KM;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1076590
    sget-object v0, LX/6KM;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076591
    monitor-exit v1

    return-void

    .line 1076592
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getFrameTimesJSON()Lorg/json/JSONObject;
    .locals 5
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1076593
    const-class v1, LX/6KM;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1076594
    sget-object v2, LX/6KM;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 1076595
    const-string v3, "frame_start_times"

    sget-object v4, LX/6KM;->a:Ljava/util/List;

    invoke-static {v4, v2}, LX/6KM;->a(Ljava/util/List;I)Lorg/json/JSONArray;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1076596
    const-string v3, "frame_end_times"

    sget-object v4, LX/6KM;->b:Ljava/util/List;

    invoke-static {v4, v2}, LX/6KM;->a(Ljava/util/List;I)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1076597
    monitor-exit v1

    return-object v0

    .line 1076598
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
