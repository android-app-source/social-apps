.class public final LX/5gX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 974909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;)Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 15
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGraphQLAlbum"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 974910
    if-nez p0, :cond_0

    .line 974911
    const/4 v0, 0x0

    .line 974912
    :goto_0
    return-object v0

    .line 974913
    :cond_0
    new-instance v2, LX/4Vp;

    invoke-direct {v2}, LX/4Vp;-><init>()V

    .line 974914
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->b()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;

    move-result-object v0

    .line 974915
    if-nez v0, :cond_4

    .line 974916
    const/4 v1, 0x0

    .line 974917
    :goto_1
    move-object v0, v1

    .line 974918
    iput-object v0, v2, LX/4Vp;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 974919
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->c()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v0

    .line 974920
    iput-object v0, v2, LX/4Vp;->c:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 974921
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->d()Z

    move-result v0

    .line 974922
    iput-boolean v0, v2, LX/4Vp;->d:Z

    .line 974923
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->e()Z

    move-result v0

    .line 974924
    iput-boolean v0, v2, LX/4Vp;->f:Z

    .line 974925
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->bQ_()Z

    move-result v0

    .line 974926
    iput-boolean v0, v2, LX/4Vp;->g:Z

    .line 974927
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->bR_()Z

    move-result v0

    .line 974928
    iput-boolean v0, v2, LX/4Vp;->h:Z

    .line 974929
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 974930
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 974931
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 974932
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;

    .line 974933
    if-nez v0, :cond_5

    .line 974934
    const/4 v4, 0x0

    .line 974935
    :goto_3
    move-object v0, v4

    .line 974936
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 974937
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 974938
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 974939
    iput-object v0, v2, LX/4Vp;->i:LX/0Px;

    .line 974940
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->j()J

    move-result-wide v0

    .line 974941
    iput-wide v0, v2, LX/4Vp;->j:J

    .line 974942
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->k()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;

    move-result-object v0

    .line 974943
    if-nez v0, :cond_7

    .line 974944
    const/4 v1, 0x0

    .line 974945
    :goto_4
    move-object v0, v1

    .line 974946
    iput-object v0, v2, LX/4Vp;->k:Lcom/facebook/graphql/model/GraphQLPlace;

    .line 974947
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 974948
    iput-object v0, v2, LX/4Vp;->m:Ljava/lang/String;

    .line 974949
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->m()Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;

    move-result-object v0

    const/4 v4, 0x0

    .line 974950
    if-nez v0, :cond_8

    .line 974951
    :cond_3
    :goto_5
    move-object v0, v4

    .line 974952
    iput-object v0, v2, LX/4Vp;->n:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 974953
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->n()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;

    move-result-object v0

    .line 974954
    if-nez v0, :cond_e

    .line 974955
    const/4 v1, 0x0

    .line 974956
    :goto_6
    move-object v0, v1

    .line 974957
    iput-object v0, v2, LX/4Vp;->p:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 974958
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->o()LX/174;

    move-result-object v0

    invoke-static {v0}, LX/5gX;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 974959
    iput-object v0, v2, LX/4Vp;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 974960
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->p()J

    move-result-wide v0

    .line 974961
    iput-wide v0, v2, LX/4Vp;->r:J

    .line 974962
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->q()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;

    move-result-object v0

    .line 974963
    if-nez v0, :cond_f

    .line 974964
    const/4 v1, 0x0

    .line 974965
    :goto_7
    move-object v0, v1

    .line 974966
    iput-object v0, v2, LX/4Vp;->t:Lcom/facebook/graphql/model/GraphQLActor;

    .line 974967
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->v()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 974968
    if-nez v0, :cond_10

    .line 974969
    const/4 v3, 0x0

    .line 974970
    :goto_8
    move-object v0, v3

    .line 974971
    iput-object v0, v2, LX/4Vp;->u:Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    .line 974972
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->r()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;

    move-result-object v0

    .line 974973
    if-nez v0, :cond_11

    .line 974974
    const/4 v1, 0x0

    .line 974975
    :goto_9
    move-object v0, v1

    .line 974976
    iput-object v0, v2, LX/4Vp;->v:Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    .line 974977
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->s()LX/174;

    move-result-object v0

    invoke-static {v0}, LX/5gX;->a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 974978
    iput-object v0, v2, LX/4Vp;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 974979
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkDetailAlbumModel;->t()Ljava/lang/String;

    move-result-object v0

    .line 974980
    iput-object v0, v2, LX/4Vp;->y:Ljava/lang/String;

    .line 974981
    invoke-virtual {v2}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    goto/16 :goto_0

    .line 974982
    :cond_4
    new-instance v1, LX/4Xy;

    invoke-direct {v1}, LX/4Xy;-><init>()V

    .line 974983
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$AlbumCoverPhotoModel;->a()LX/1Fb;

    move-result-object v3

    invoke-static {v3}, LX/5gX;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 974984
    iput-object v3, v1, LX/4Xy;->ab:Lcom/facebook/graphql/model/GraphQLImage;

    .line 974985
    invoke-virtual {v1}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    goto/16 :goto_1

    .line 974986
    :cond_5
    new-instance v4, LX/3dL;

    invoke-direct {v4}, LX/3dL;-><init>()V

    .line 974987
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 974988
    iput-object v5, v4, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 974989
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 974990
    iput-object v5, v4, LX/3dL;->E:Ljava/lang/String;

    .line 974991
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 974992
    iput-object v5, v4, LX/3dL;->ag:Ljava/lang/String;

    .line 974993
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$AlbumPermalinkContributorsModel;->e()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 974994
    if-nez v5, :cond_6

    .line 974995
    const/4 v7, 0x0

    .line 974996
    :goto_a
    move-object v5, v7

    .line 974997
    iput-object v5, v4, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 974998
    invoke-virtual {v4}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    goto/16 :goto_3

    .line 974999
    :cond_6
    new-instance v7, LX/2dc;

    invoke-direct {v7}, LX/2dc;-><init>()V

    .line 975000
    const/4 v0, 0x0

    invoke-virtual {v6, v5, v0}, LX/15i;->j(II)I

    move-result v0

    .line 975001
    iput v0, v7, LX/2dc;->c:I

    .line 975002
    const/4 v0, 0x1

    invoke-virtual {v6, v5, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 975003
    iput-object v0, v7, LX/2dc;->h:Ljava/lang/String;

    .line 975004
    const/4 v0, 0x2

    invoke-virtual {v6, v5, v0}, LX/15i;->j(II)I

    move-result v0

    .line 975005
    iput v0, v7, LX/2dc;->i:I

    .line 975006
    invoke-virtual {v7}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    goto :goto_a

    .line 975007
    :cond_7
    new-instance v1, LX/4Y6;

    invoke-direct {v1}, LX/4Y6;-><init>()V

    .line 975008
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 975009
    iput-object v3, v1, LX/4Y6;->U:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 975010
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 975011
    iput-object v3, v1, LX/4Y6;->n:Ljava/lang/String;

    .line 975012
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$ExplicitPlaceModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 975013
    iput-object v3, v1, LX/4Y6;->r:Ljava/lang/String;

    .line 975014
    invoke-virtual {v1}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    goto/16 :goto_4

    .line 975015
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 975016
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, 0x2c8ffc04

    if-ne v5, v6, :cond_3

    .line 975017
    new-instance v6, LX/4XE;

    invoke-direct {v6}, LX/4XE;-><init>()V

    .line 975018
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->b()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 975019
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 975020
    const/4 v4, 0x0

    move v5, v4

    :goto_b
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_9

    .line 975021
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;

    .line 975022
    if-nez v4, :cond_b

    .line 975023
    const/4 v8, 0x0

    .line 975024
    :goto_c
    move-object v4, v8

    .line 975025
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 975026
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_b

    .line 975027
    :cond_9
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 975028
    iput-object v4, v6, LX/4XE;->c:LX/0Px;

    .line 975029
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultMediaSetMediaConnectionModel;->c()LX/0us;

    move-result-object v4

    .line 975030
    if-nez v4, :cond_d

    .line 975031
    const/4 v5, 0x0

    .line 975032
    :goto_d
    move-object v4, v5

    .line 975033
    iput-object v4, v6, LX/4XE;->d:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 975034
    invoke-virtual {v6}, LX/4XE;->a()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v4

    goto/16 :goto_5

    .line 975035
    :cond_b
    new-instance v8, LX/4XB;

    invoke-direct {v8}, LX/4XB;-><init>()V

    .line 975036
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 975037
    iput-object v9, v8, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 975038
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->c()LX/1f8;

    move-result-object v9

    .line 975039
    if-nez v9, :cond_c

    .line 975040
    const/4 v10, 0x0

    .line 975041
    :goto_e
    move-object v9, v10

    .line 975042
    iput-object v9, v8, LX/4XB;->H:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 975043
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->d()Ljava/lang/String;

    move-result-object v9

    .line 975044
    iput-object v9, v8, LX/4XB;->T:Ljava/lang/String;

    .line 975045
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->e()LX/1Fb;

    move-result-object v9

    invoke-static {v9}, LX/5gX;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 975046
    iput-object v9, v8, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 975047
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->aj_()LX/1Fb;

    move-result-object v9

    invoke-static {v9}, LX/5gX;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 975048
    iput-object v9, v8, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 975049
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->ai_()LX/1Fb;

    move-result-object v9

    invoke-static {v9}, LX/5gX;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 975050
    iput-object v9, v8, LX/4XB;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 975051
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->j()LX/1Fb;

    move-result-object v9

    invoke-static {v9}, LX/5gX;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 975052
    iput-object v9, v8, LX/4XB;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 975053
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->k()LX/1Fb;

    move-result-object v9

    invoke-static {v9}, LX/5gX;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 975054
    iput-object v9, v8, LX/4XB;->af:Lcom/facebook/graphql/model/GraphQLImage;

    .line 975055
    invoke-virtual {v8}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    goto :goto_c

    .line 975056
    :cond_c
    new-instance v10, LX/4ZN;

    invoke-direct {v10}, LX/4ZN;-><init>()V

    .line 975057
    invoke-interface {v9}, LX/1f8;->a()D

    move-result-wide v12

    .line 975058
    iput-wide v12, v10, LX/4ZN;->b:D

    .line 975059
    invoke-interface {v9}, LX/1f8;->b()D

    move-result-wide v12

    .line 975060
    iput-wide v12, v10, LX/4ZN;->c:D

    .line 975061
    invoke-virtual {v10}, LX/4ZN;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v10

    goto :goto_e

    .line 975062
    :cond_d
    new-instance v5, LX/17L;

    invoke-direct {v5}, LX/17L;-><init>()V

    .line 975063
    invoke-interface {v4}, LX/0us;->a()Ljava/lang/String;

    move-result-object v7

    .line 975064
    iput-object v7, v5, LX/17L;->c:Ljava/lang/String;

    .line 975065
    invoke-interface {v4}, LX/0us;->b()Z

    move-result v7

    .line 975066
    iput-boolean v7, v5, LX/17L;->d:Z

    .line 975067
    invoke-interface {v4}, LX/0us;->c()Z

    move-result v7

    .line 975068
    iput-boolean v7, v5, LX/17L;->e:Z

    .line 975069
    invoke-interface {v4}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v7

    .line 975070
    iput-object v7, v5, LX/17L;->f:Ljava/lang/String;

    .line 975071
    invoke-virtual {v5}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v5

    goto/16 :goto_d

    .line 975072
    :cond_e
    new-instance v1, LX/25F;

    invoke-direct {v1}, LX/25F;-><init>()V

    .line 975073
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 975074
    iput-object v3, v1, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 975075
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 975076
    iput-object v3, v1, LX/25F;->C:Ljava/lang/String;

    .line 975077
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$MediaOwnerObjectModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 975078
    iput-object v3, v1, LX/25F;->T:Ljava/lang/String;

    .line 975079
    invoke-virtual {v1}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    goto/16 :goto_6

    .line 975080
    :cond_f
    new-instance v1, LX/3dL;

    invoke-direct {v1}, LX/3dL;-><init>()V

    .line 975081
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 975082
    iput-object v3, v1, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 975083
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$OwnerModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 975084
    iput-object v3, v1, LX/3dL;->E:Ljava/lang/String;

    .line 975085
    invoke-virtual {v1}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    goto/16 :goto_7

    .line 975086
    :cond_10
    new-instance v3, LX/4XE;

    invoke-direct {v3}, LX/4XE;-><init>()V

    .line 975087
    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4}, LX/15i;->j(II)I

    move-result v4

    .line 975088
    iput v4, v3, LX/4XE;->b:I

    .line 975089
    invoke-virtual {v3}, LX/4XE;->a()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v3

    goto/16 :goto_8

    .line 975090
    :cond_11
    new-instance v1, LX/4YL;

    invoke-direct {v1}, LX/4YL;-><init>()V

    .line 975091
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 975092
    iput-object v3, v1, LX/4YL;->c:Ljava/lang/String;

    .line 975093
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->b()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;

    move-result-object v3

    .line 975094
    if-nez v3, :cond_12

    .line 975095
    const/4 v4, 0x0

    .line 975096
    :goto_f
    move-object v3, v4

    .line 975097
    iput-object v3, v1, LX/4YL;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 975098
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 975099
    iput-object v3, v1, LX/4YL;->h:Ljava/lang/String;

    .line 975100
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 975101
    iput-object v3, v1, LX/4YL;->i:Ljava/lang/String;

    .line 975102
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel;->e()Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v3

    .line 975103
    if-nez v3, :cond_13

    .line 975104
    const/4 v4, 0x0

    .line 975105
    :goto_10
    move-object v3, v4

    .line 975106
    iput-object v3, v1, LX/4YL;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 975107
    invoke-virtual {v1}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    goto/16 :goto_9

    .line 975108
    :cond_12
    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    .line 975109
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 975110
    iput-object v5, v4, LX/2dc;->f:Ljava/lang/String;

    .line 975111
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$IconImageModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 975112
    iput-object v5, v4, LX/2dc;->h:Ljava/lang/String;

    .line 975113
    invoke-virtual {v4}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    goto :goto_f

    .line 975114
    :cond_13
    new-instance v4, LX/4YI;

    invoke-direct {v4}, LX/4YI;-><init>()V

    .line 975115
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/AlbumQueryModels$DefaultAlbumFieldsModel$PrivacyScopeModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v5

    .line 975116
    iput-object v5, v4, LX/4YI;->b:LX/0Px;

    .line 975117
    invoke-virtual {v4}, LX/4YI;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v4

    goto :goto_10
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 975118
    if-nez p0, :cond_0

    .line 975119
    const/4 v0, 0x0

    .line 975120
    :goto_0
    return-object v0

    .line 975121
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 975122
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 975123
    iput v1, v0, LX/2dc;->c:I

    .line 975124
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 975125
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 975126
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 975127
    iput v1, v0, LX/2dc;->i:I

    .line 975128
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/174;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    .line 975129
    if-nez p0, :cond_0

    .line 975130
    const/4 v0, 0x0

    .line 975131
    :goto_0
    return-object v0

    .line 975132
    :cond_0
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 975133
    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 975134
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 975135
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    goto :goto_0
.end method
