.class public final LX/63w;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F
    .locals 2

    .prologue
    .line 1044019
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1044020
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getRight()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getLeft()F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public static a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)I
    .locals 2

    .prologue
    .line 1044017
    invoke-virtual {p0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    .line 1044018
    iget v1, v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    iget v0, v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    sub-int v0, v1, v0

    return v0
.end method

.method public static a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;
    .locals 2
    .param p0    # Landroid/graphics/RectF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1044014
    if-nez p0, :cond_0

    .line 1044015
    const/4 v0, 0x0

    .line 1044016
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->newBuilder()Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    iget v1, p0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setLeft(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    iget v1, p0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setTop(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    iget v1, p0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setRight(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    iget v1, p0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->setBottom(F)Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/PersistableRect$Builder;->a()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1044013
    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1043998
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1043999
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1044000
    if-eqz v0, :cond_0

    .line 1044001
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1044002
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1044003
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1044004
    if-eqz v0, :cond_1

    .line 1044005
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1044006
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1044007
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1044008
    if-eqz v0, :cond_2

    .line 1044009
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1044010
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1044011
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1044012
    return-void
.end method

.method public static a$redex0(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1043972
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1043973
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1043974
    :goto_0
    return v1

    .line 1043975
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1043976
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1043977
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1043978
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1043979
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 1043980
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1043981
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 1043982
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1043983
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1043984
    :cond_4
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1043985
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1043986
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1043987
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1043988
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1043989
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1043990
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a$redex0(LX/15w;)LX/15i;
    .locals 13

    .prologue
    .line 1044021
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1044022
    const/4 v9, 0x0

    .line 1044023
    const/4 v8, 0x0

    .line 1044024
    const/4 v7, 0x0

    .line 1044025
    const/4 v6, 0x0

    .line 1044026
    const-wide/16 v4, 0x0

    .line 1044027
    const/4 v3, 0x0

    .line 1044028
    const/4 v2, 0x0

    .line 1044029
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_9

    .line 1044030
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1044031
    const/4 v2, 0x0

    .line 1044032
    :goto_0
    move v1, v2

    .line 1044033
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1044034
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 1044035
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v11, :cond_7

    .line 1044036
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1044037
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1044038
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v3, :cond_0

    .line 1044039
    const-string v11, "attachments"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1044040
    invoke-static {p0, v0}, LX/5Eg;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 1044041
    :cond_1
    const-string v11, "author"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1044042
    invoke-static {p0, v0}, LX/57p;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto :goto_1

    .line 1044043
    :cond_2
    const-string v11, "body"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1044044
    invoke-static {p0, v0}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 1044045
    :cond_3
    const-string v11, "comment_parent"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1044046
    invoke-static {p0, v0}, LX/5Eh;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 1044047
    :cond_4
    const-string v11, "created_time"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1044048
    const/4 v2, 0x1

    .line 1044049
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 1044050
    :cond_5
    const-string v11, "id"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1044051
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v8, v3

    goto :goto_1

    .line 1044052
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1044053
    :cond_7
    const/4 v3, 0x6

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1044054
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1044055
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1044056
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1044057
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1044058
    if-eqz v2, :cond_8

    .line 1044059
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object v2, v0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1044060
    :cond_8
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1044061
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v10, v9

    move v9, v8

    move v8, v3

    goto/16 :goto_1
.end method

.method public static a$redex0(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1043991
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1043992
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1043993
    if-eqz v0, :cond_0

    .line 1043994
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1043995
    invoke-static {p0, v0, p2, p3}, LX/4Rf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1043996
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1043997
    return-void
.end method

.method public static b(Lcom/facebook/photos/creativeediting/model/PersistableRect;)F
    .locals 2

    .prologue
    .line 1043970
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1043971
    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getBottom()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getTop()F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1043954
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1043955
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1043956
    :goto_0
    return v1

    .line 1043957
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1043958
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1043959
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1043960
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1043961
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1043962
    const-string v4, "error_text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1043963
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1043964
    :cond_2
    const-string v4, "input_type"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1043965
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_1

    .line 1043966
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1043967
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1043968
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1043969
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1043953
    invoke-virtual {p0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1043938
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1043939
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1043940
    if-eqz v0, :cond_0

    .line 1043941
    const-string v1, "info_fields"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1043942
    invoke-static {p0, v0, p2, p3}, LX/56j;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1043943
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1043944
    if-eqz v0, :cond_2

    .line 1043945
    const-string v1, "privacy_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1043946
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1043947
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_1

    .line 1043948
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2}, LX/56W;->a(LX/15i;ILX/0nX;)V

    .line 1043949
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1043950
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1043951
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1043952
    return-void
.end method

.method public static c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;
    .locals 5
    .param p0    # Lcom/facebook/photos/creativeediting/model/PersistableRect;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1043935
    if-nez p0, :cond_0

    .line 1043936
    const/4 v0, 0x0

    .line 1043937
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getLeft()F

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getTop()F

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getRight()F

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/PersistableRect;->getBottom()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0
.end method
