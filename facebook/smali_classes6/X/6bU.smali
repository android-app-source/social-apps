.class public LX/6bU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/01U;LX/0aU;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/01U;",
            "LX/0aU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1113670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1113671
    iput-object p1, p0, LX/6bU;->a:Landroid/content/Context;

    .line 1113672
    iput-object p2, p0, LX/6bU;->b:LX/0Or;

    .line 1113673
    invoke-virtual {p3}, LX/01U;->getPermission()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6bU;->c:Ljava/lang/String;

    .line 1113674
    const-string v0, "messages.ACTION_NEW_MESSAGE"

    invoke-virtual {p4, v0}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6bU;->d:Ljava/lang/String;

    .line 1113675
    const-string v0, "messages.ACTION_CLEAR_ALL_MESSAGES"

    invoke-virtual {p4, v0}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6bU;->e:Ljava/lang/String;

    .line 1113676
    const-string v0, "messages.ACTION_CLEAR_MESSAGE"

    invoke-virtual {p4, v0}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6bU;->f:Ljava/lang/String;

    .line 1113677
    return-void
.end method
