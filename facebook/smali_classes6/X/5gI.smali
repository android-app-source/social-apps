.class public final LX/5gI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 974321
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 974322
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 974323
    :goto_0
    return v1

    .line 974324
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 974325
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 974326
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 974327
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 974328
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 974329
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 974330
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 974331
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 974332
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 974333
    :cond_4
    const-string v5, "image"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 974334
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 974335
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_e

    .line 974336
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 974337
    :goto_2
    move v0, v4

    .line 974338
    goto :goto_1

    .line 974339
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 974340
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 974341
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 974342
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 974343
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 974344
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 974345
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 974346
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 974347
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_7

    if-eqz v10, :cond_7

    .line 974348
    const-string v11, "height"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 974349
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    move v9, v6

    move v6, v5

    goto :goto_3

    .line 974350
    :cond_8
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 974351
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_3

    .line 974352
    :cond_9
    const-string v11, "width"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 974353
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v7, v0

    move v0, v5

    goto :goto_3

    .line 974354
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 974355
    :cond_b
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 974356
    if-eqz v6, :cond_c

    .line 974357
    invoke-virtual {p1, v4, v9, v4}, LX/186;->a(III)V

    .line 974358
    :cond_c
    invoke-virtual {p1, v5, v8}, LX/186;->b(II)V

    .line 974359
    if-eqz v0, :cond_d

    .line 974360
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7, v4}, LX/186;->a(III)V

    .line 974361
    :cond_d
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_e
    move v0, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 974362
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 974363
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 974364
    if-eqz v0, :cond_0

    .line 974365
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974366
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 974367
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 974368
    if-eqz v0, :cond_1

    .line 974369
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974370
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 974371
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 974372
    if-eqz v0, :cond_5

    .line 974373
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974374
    const/4 p3, 0x0

    .line 974375
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 974376
    invoke-virtual {p0, v0, p3, p3}, LX/15i;->a(III)I

    move-result v1

    .line 974377
    if-eqz v1, :cond_2

    .line 974378
    const-string p1, "height"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974379
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 974380
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 974381
    if-eqz v1, :cond_3

    .line 974382
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974383
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 974384
    :cond_3
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, p3}, LX/15i;->a(III)I

    move-result v1

    .line 974385
    if-eqz v1, :cond_4

    .line 974386
    const-string p1, "width"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 974387
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 974388
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 974389
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 974390
    return-void
.end method
