.class public LX/5zt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/5zt;


# instance fields
.field private final a:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1035914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035915
    iput-object p1, p0, LX/5zt;->a:Landroid/content/ContentResolver;

    .line 1035916
    return-void
.end method

.method public static a(LX/0QB;)LX/5zt;
    .locals 4

    .prologue
    .line 1035917
    sget-object v0, LX/5zt;->b:LX/5zt;

    if-nez v0, :cond_1

    .line 1035918
    const-class v1, LX/5zt;

    monitor-enter v1

    .line 1035919
    :try_start_0
    sget-object v0, LX/5zt;->b:LX/5zt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1035920
    if-eqz v2, :cond_0

    .line 1035921
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1035922
    new-instance p0, LX/5zt;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-direct {p0, v3}, LX/5zt;-><init>(Landroid/content/ContentResolver;)V

    .line 1035923
    move-object v0, p0

    .line 1035924
    sput-object v0, LX/5zt;->b:LX/5zt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1035925
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1035926
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1035927
    :cond_1
    sget-object v0, LX/5zt;->b:LX/5zt;

    return-object v0

    .line 1035928
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1035929
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 2

    .prologue
    .line 1035930
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->ENT_PHOTO:LX/2MK;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
