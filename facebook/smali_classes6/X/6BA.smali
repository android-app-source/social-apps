.class public final LX/6BA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1061697
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1061698
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1061699
    if-eqz v0, :cond_0

    .line 1061700
    const-string v1, "col"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1061701
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1061702
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1061703
    if-eqz v0, :cond_1

    .line 1061704
    const-string v1, "face"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1061705
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1061706
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1061707
    if-eqz v0, :cond_2

    .line 1061708
    const-string v1, "level"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1061709
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1061710
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1061711
    if-eqz v0, :cond_3

    .line 1061712
    const-string v1, "row"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1061713
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1061714
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1061715
    if-eqz v0, :cond_4

    .line 1061716
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1061717
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1061718
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1061719
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1061720
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 1061721
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1061722
    :goto_0
    return v1

    .line 1061723
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 1061724
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1061725
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1061726
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 1061727
    const-string v12, "col"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1061728
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v10, v5

    move v5, v2

    goto :goto_1

    .line 1061729
    :cond_1
    const-string v12, "face"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1061730
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v9, v4

    move v4, v2

    goto :goto_1

    .line 1061731
    :cond_2
    const-string v12, "level"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1061732
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 1061733
    :cond_3
    const-string v12, "row"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1061734
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 1061735
    :cond_4
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1061736
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1061737
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1061738
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1061739
    if-eqz v5, :cond_7

    .line 1061740
    invoke-virtual {p1, v1, v10, v1}, LX/186;->a(III)V

    .line 1061741
    :cond_7
    if-eqz v4, :cond_8

    .line 1061742
    invoke-virtual {p1, v2, v9, v1}, LX/186;->a(III)V

    .line 1061743
    :cond_8
    if-eqz v3, :cond_9

    .line 1061744
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v8, v1}, LX/186;->a(III)V

    .line 1061745
    :cond_9
    if-eqz v0, :cond_a

    .line 1061746
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7, v1}, LX/186;->a(III)V

    .line 1061747
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1061748
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method
