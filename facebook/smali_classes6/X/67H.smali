.class public final LX/67H;
.super LX/673;
.source ""


# instance fields
.field public final transient e:[[B

.field public final transient f:[I


# direct methods
.method public constructor <init>(LX/672;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1052734
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/673;-><init>([B)V

    .line 1052735
    iget-wide v0, p1, LX/672;->b:J

    const-wide/16 v2, 0x0

    int-to-long v4, p2

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1052736
    iget-object v0, p1, LX/672;->a:LX/67F;

    move v1, v6

    move v2, v6

    :goto_0
    if-ge v2, p2, :cond_1

    .line 1052737
    iget v3, v0, LX/67F;->c:I

    iget v4, v0, LX/67F;->b:I

    if-ne v3, v4, :cond_0

    .line 1052738
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "s.limit == s.pos"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1052739
    :cond_0
    iget v3, v0, LX/67F;->c:I

    iget v4, v0, LX/67F;->b:I

    sub-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 1052740
    add-int/lit8 v1, v1, 0x1

    .line 1052741
    iget-object v0, v0, LX/67F;->f:LX/67F;

    goto :goto_0

    .line 1052742
    :cond_1
    new-array v0, v1, [[B

    iput-object v0, p0, LX/67H;->e:[[B

    .line 1052743
    mul-int/lit8 v0, v1, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/67H;->f:[I

    .line 1052744
    iget-object v0, p1, LX/672;->a:LX/67F;

    move-object v1, v0

    move v2, v6

    :goto_1
    if-ge v6, p2, :cond_3

    .line 1052745
    iget-object v0, p0, LX/67H;->e:[[B

    iget-object v3, v1, LX/67F;->a:[B

    aput-object v3, v0, v2

    .line 1052746
    iget v0, v1, LX/67F;->c:I

    iget v3, v1, LX/67F;->b:I

    sub-int/2addr v0, v3

    add-int/2addr v0, v6

    .line 1052747
    if-le v0, p2, :cond_2

    move v0, p2

    .line 1052748
    :cond_2
    iget-object v3, p0, LX/67H;->f:[I

    aput v0, v3, v2

    .line 1052749
    iget-object v3, p0, LX/67H;->f:[I

    iget-object v4, p0, LX/67H;->e:[[B

    array-length v4, v4

    add-int/2addr v4, v2

    iget v5, v1, LX/67F;->b:I

    aput v5, v3, v4

    .line 1052750
    const/4 v3, 0x1

    iput-boolean v3, v1, LX/67F;->d:Z

    .line 1052751
    add-int/lit8 v2, v2, 0x1

    .line 1052752
    iget-object v1, v1, LX/67F;->f:LX/67F;

    move v6, v0

    goto :goto_1

    .line 1052753
    :cond_3
    return-void
.end method

.method private b(I)I
    .locals 4

    .prologue
    .line 1052732
    iget-object v0, p0, LX/67H;->f:[I

    const/4 v1, 0x0

    iget-object v2, p0, LX/67H;->e:[[B

    array-length v2, v2

    add-int/lit8 v3, p1, 0x1

    invoke-static {v0, v1, v2, v3}, Ljava/util/Arrays;->binarySearch([IIII)I

    move-result v0

    .line 1052733
    if-ltz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    xor-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private g()LX/673;
    .locals 2

    .prologue
    .line 1052666
    new-instance v0, LX/673;

    invoke-virtual {p0}, LX/67H;->f()[B

    move-result-object v1

    invoke-direct {v0, v1}, LX/673;-><init>([B)V

    return-object v0
.end method


# virtual methods
.method public final a(I)B
    .locals 6

    .prologue
    .line 1052726
    iget-object v0, p0, LX/67H;->f:[I

    iget-object v1, p0, LX/67H;->e:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    int-to-long v0, v0

    int-to-long v2, p1

    const-wide/16 v4, 0x1

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1052727
    invoke-direct {p0, p1}, LX/67H;->b(I)I

    move-result v1

    .line 1052728
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 1052729
    :goto_0
    iget-object v2, p0, LX/67H;->f:[I

    iget-object v3, p0, LX/67H;->e:[[B

    array-length v3, v3

    add-int/2addr v3, v1

    aget v2, v2, v3

    .line 1052730
    iget-object v3, p0, LX/67H;->e:[[B

    aget-object v1, v3, v1

    sub-int v0, p1, v0

    add-int/2addr v0, v2

    aget-byte v0, v1, v0

    return v0

    .line 1052731
    :cond_0
    iget-object v0, p0, LX/67H;->f:[I

    add-int/lit8 v2, v1, -0x1

    aget v0, v0, v2

    goto :goto_0
.end method

.method public final a(II)LX/673;
    .locals 1

    .prologue
    .line 1052725
    invoke-direct {p0}, LX/67H;->g()LX/673;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/673;->a(II)LX/673;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1052724
    invoke-direct {p0}, LX/67H;->g()LX/673;

    move-result-object v0

    invoke-virtual {v0}, LX/673;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/672;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1052714
    iget-object v1, p0, LX/67H;->e:[[B

    array-length v3, v1

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_1

    .line 1052715
    iget-object v2, p0, LX/67H;->f:[I

    add-int v4, v3, v0

    aget v4, v2, v4

    .line 1052716
    iget-object v2, p0, LX/67H;->f:[I

    aget v2, v2, v0

    .line 1052717
    new-instance v5, LX/67F;

    iget-object v6, p0, LX/67H;->e:[[B

    aget-object v6, v6, v0

    add-int v7, v4, v2

    sub-int v1, v7, v1

    invoke-direct {v5, v6, v4, v1}, LX/67F;-><init>([BII)V

    .line 1052718
    iget-object v1, p1, LX/672;->a:LX/67F;

    if-nez v1, :cond_0

    .line 1052719
    iput-object v5, v5, LX/67F;->g:LX/67F;

    iput-object v5, v5, LX/67F;->f:LX/67F;

    iput-object v5, p1, LX/672;->a:LX/67F;

    .line 1052720
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 1052721
    :cond_0
    iget-object v1, p1, LX/672;->a:LX/67F;

    iget-object v1, v1, LX/67F;->g:LX/67F;

    invoke-virtual {v1, v5}, LX/67F;->a(LX/67F;)LX/67F;

    goto :goto_1

    .line 1052722
    :cond_1
    iget-wide v2, p1, LX/672;->b:J

    int-to-long v0, v1

    add-long/2addr v0, v2

    iput-wide v0, p1, LX/672;->b:J

    .line 1052723
    return-void
.end method

.method public final a(ILX/673;II)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1052699
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/67H;->e()I

    move-result v0

    sub-int/2addr v0, p4

    if-le p1, v0, :cond_1

    .line 1052700
    :cond_0
    :goto_0
    return v1

    .line 1052701
    :cond_1
    invoke-direct {p0, p1}, LX/67H;->b(I)I

    move-result v0

    move v2, v0

    :goto_1
    if-lez p4, :cond_3

    .line 1052702
    if-nez v2, :cond_2

    move v0, v1

    .line 1052703
    :goto_2
    iget-object v3, p0, LX/67H;->f:[I

    aget v3, v3, v2

    sub-int/2addr v3, v0

    .line 1052704
    add-int/2addr v3, v0

    sub-int/2addr v3, p1

    invoke-static {p4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1052705
    iget-object v4, p0, LX/67H;->f:[I

    iget-object v5, p0, LX/67H;->e:[[B

    array-length v5, v5

    add-int/2addr v5, v2

    aget v4, v4, v5

    .line 1052706
    sub-int v0, p1, v0

    add-int/2addr v0, v4

    .line 1052707
    iget-object v4, p0, LX/67H;->e:[[B

    aget-object v4, v4, v2

    invoke-virtual {p2, p3, v4, v0, v3}, LX/673;->a(I[BII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1052708
    add-int/2addr p1, v3

    .line 1052709
    add-int/2addr p3, v3

    .line 1052710
    sub-int/2addr p4, v3

    .line 1052711
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1052712
    :cond_2
    iget-object v0, p0, LX/67H;->f:[I

    add-int/lit8 v3, v2, -0x1

    aget v0, v0, v3

    goto :goto_2

    .line 1052713
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final a(I[BII)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1052754
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/67H;->e()I

    move-result v0

    sub-int/2addr v0, p4

    if-gt p1, v0, :cond_0

    if-ltz p3, :cond_0

    array-length v0, p2

    sub-int/2addr v0, p4

    if-le p3, v0, :cond_1

    .line 1052755
    :cond_0
    :goto_0
    return v1

    .line 1052756
    :cond_1
    invoke-direct {p0, p1}, LX/67H;->b(I)I

    move-result v0

    move v2, v0

    :goto_1
    if-lez p4, :cond_3

    .line 1052757
    if-nez v2, :cond_2

    move v0, v1

    .line 1052758
    :goto_2
    iget-object v3, p0, LX/67H;->f:[I

    aget v3, v3, v2

    sub-int/2addr v3, v0

    .line 1052759
    add-int/2addr v3, v0

    sub-int/2addr v3, p1

    invoke-static {p4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1052760
    iget-object v4, p0, LX/67H;->f:[I

    iget-object v5, p0, LX/67H;->e:[[B

    array-length v5, v5

    add-int/2addr v5, v2

    aget v4, v4, v5

    .line 1052761
    sub-int v0, p1, v0

    add-int/2addr v0, v4

    .line 1052762
    iget-object v4, p0, LX/67H;->e:[[B

    aget-object v4, v4, v2

    invoke-static {v4, v0, p2, p3, v3}, LX/67J;->a([BI[BII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1052763
    add-int/2addr p1, v3

    .line 1052764
    add-int/2addr p3, v3

    .line 1052765
    sub-int/2addr p4, v3

    .line 1052766
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1052767
    :cond_2
    iget-object v0, p0, LX/67H;->f:[I

    add-int/lit8 v3, v2, -0x1

    aget v0, v0, v3

    goto :goto_2

    .line 1052768
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1052698
    invoke-direct {p0}, LX/67H;->g()LX/673;

    move-result-object v0

    invoke-virtual {v0}, LX/673;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1052697
    invoke-direct {p0}, LX/67H;->g()LX/673;

    move-result-object v0

    invoke-virtual {v0}, LX/673;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/673;
    .locals 1

    .prologue
    .line 1052696
    invoke-direct {p0}, LX/67H;->g()LX/673;

    move-result-object v0

    invoke-virtual {v0}, LX/673;->d()LX/673;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 1052695
    iget-object v0, p0, LX/67H;->f:[I

    iget-object v1, p0, LX/67H;->e:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1052689
    if-ne p1, p0, :cond_0

    move v0, v1

    .line 1052690
    :goto_0
    return v0

    .line 1052691
    :cond_0
    instance-of v0, p1, LX/673;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, LX/673;

    .line 1052692
    invoke-virtual {v0}, LX/673;->e()I

    move-result v0

    invoke-virtual {p0}, LX/67H;->e()I

    move-result v3

    if-ne v0, v3, :cond_1

    check-cast p1, LX/673;

    .line 1052693
    invoke-virtual {p0}, LX/67H;->e()I

    move-result v0

    invoke-virtual {p0, v2, p1, v2, v0}, LX/67H;->a(ILX/673;II)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1052694
    goto :goto_0
.end method

.method public final f()[B
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1052682
    iget-object v1, p0, LX/67H;->f:[I

    iget-object v2, p0, LX/67H;->e:[[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    new-array v3, v1, [B

    .line 1052683
    iget-object v1, p0, LX/67H;->e:[[B

    array-length v4, v1

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 1052684
    iget-object v2, p0, LX/67H;->f:[I

    add-int v5, v4, v0

    aget v5, v2, v5

    .line 1052685
    iget-object v2, p0, LX/67H;->f:[I

    aget v2, v2, v0

    .line 1052686
    iget-object v6, p0, LX/67H;->e:[[B

    aget-object v6, v6, v0

    sub-int v7, v2, v1

    invoke-static {v6, v5, v3, v1, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1052687
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 1052688
    :cond_0
    return-object v3
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1052668
    iget v0, p0, LX/673;->c:I

    .line 1052669
    if-eqz v0, :cond_0

    .line 1052670
    :goto_0
    return v0

    .line 1052671
    :cond_0
    const/4 v0, 0x1

    .line 1052672
    iget-object v2, p0, LX/67H;->e:[[B

    array-length v5, v2

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_2

    .line 1052673
    iget-object v1, p0, LX/67H;->e:[[B

    aget-object v6, v1, v2

    .line 1052674
    iget-object v1, p0, LX/67H;->f:[I

    add-int v4, v5, v2

    aget v1, v1, v4

    .line 1052675
    iget-object v4, p0, LX/67H;->f:[I

    aget v4, v4, v2

    .line 1052676
    sub-int v3, v4, v3

    .line 1052677
    add-int/2addr v3, v1

    move v8, v1

    move v1, v0

    move v0, v8

    :goto_2
    if-ge v0, v3, :cond_1

    .line 1052678
    mul-int/lit8 v1, v1, 0x1f

    aget-byte v7, v6, v0

    add-int/2addr v1, v7

    .line 1052679
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1052680
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v4

    move v0, v1

    goto :goto_1

    .line 1052681
    :cond_2
    iput v0, p0, LX/67H;->c:I

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1052667
    invoke-direct {p0}, LX/67H;->g()LX/673;

    move-result-object v0

    invoke-virtual {v0}, LX/673;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
