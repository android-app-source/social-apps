.class public final LX/5NH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;)V
    .locals 0

    .prologue
    .line 906129
    iput-object p1, p0, LX/5NH;->a:Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x6c53c31e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 906130
    iget-object v1, p0, LX/5NH;->a:Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;

    const-string v2, "dialtone_ineligible_interstitial_upgrade_button_click"

    invoke-static {v1, v2}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->b(Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;Ljava/lang/String;)V

    .line 906131
    iget-object v1, p0, LX/5NH;->a:Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;

    iget-object v1, v1, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->q:LX/0yc;

    const-string v2, "dialtone_ineligible_interstitial_upgrade_button_click"

    invoke-virtual {v1, v2}, LX/0yc;->b(Ljava/lang/String;)Z

    .line 906132
    iget-object v1, p0, LX/5NH;->a:Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;

    invoke-virtual {v1}, Lcom/facebook/dialtone/activity/DialtoneUnsupportedCarrierInterstitialActivity;->finish()V

    .line 906133
    const v1, 0x47ff7b88

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
