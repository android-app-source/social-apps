.class public final LX/6M9;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactByProfileIdQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1079503
    const-class v1, Lcom/facebook/contacts/graphql/ContactGraphQLModels$FetchContactByProfileIdQueryModel;

    const v0, 0x4fd2e1da

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchContactByProfileIdQuery"

    const-string v6, "4620cc816a0a8e18db713981554779e4"

    const-string v7, "user"

    const-string v8, "10155069964756729"

    const-string v9, "10155259087226729"

    .line 1079504
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1079505
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1079506
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1079491
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1079492
    sparse-switch v0, :sswitch_data_0

    .line 1079493
    :goto_0
    return-object p1

    .line 1079494
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1079495
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1079496
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1079497
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1079498
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x719ba5ef -> :sswitch_1
        -0x55d248cb -> :sswitch_3
        -0x4e92d738 -> :sswitch_4
        -0x41b8e48f -> :sswitch_2
        0x2956b75c -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1079499
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1079500
    :goto_1
    return v0

    .line 1079501
    :pswitch_0
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1079502
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x34
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
