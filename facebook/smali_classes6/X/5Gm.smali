.class public final enum LX/5Gm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/5Gm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/5Gm;

.field public static final enum BASE_ONLY:LX/5Gm;

.field public static final enum COMMENTS_AND_LIKERS:LX/5Gm;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 889190
    new-instance v0, LX/5Gm;

    const-string v1, "BASE_ONLY"

    invoke-direct {v0, v1, v2}, LX/5Gm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Gm;->BASE_ONLY:LX/5Gm;

    .line 889191
    new-instance v0, LX/5Gm;

    const-string v1, "COMMENTS_AND_LIKERS"

    invoke-direct {v0, v1, v3}, LX/5Gm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/5Gm;->COMMENTS_AND_LIKERS:LX/5Gm;

    .line 889192
    const/4 v0, 0x2

    new-array v0, v0, [LX/5Gm;

    sget-object v1, LX/5Gm;->BASE_ONLY:LX/5Gm;

    aput-object v1, v0, v2

    sget-object v1, LX/5Gm;->COMMENTS_AND_LIKERS:LX/5Gm;

    aput-object v1, v0, v3

    sput-object v0, LX/5Gm;->$VALUES:[LX/5Gm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 889193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 889194
    return-void
.end method

.method public static getQueryType(Lcom/facebook/api/story/FetchSingleStoryParams;)LX/5Gm;
    .locals 2

    .prologue
    .line 889195
    sget-object v0, LX/5Gl;->a:[I

    iget-object v1, p0, Lcom/facebook/api/story/FetchSingleStoryParams;->d:LX/5Go;

    invoke-virtual {v1}, LX/5Go;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 889196
    sget-object v0, LX/5Gm;->BASE_ONLY:LX/5Gm;

    :goto_0
    return-object v0

    .line 889197
    :pswitch_0
    sget-object v0, LX/5Gm;->BASE_ONLY:LX/5Gm;

    goto :goto_0

    .line 889198
    :pswitch_1
    sget-object v0, LX/5Gm;->COMMENTS_AND_LIKERS:LX/5Gm;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/5Gm;
    .locals 1

    .prologue
    .line 889199
    const-class v0, LX/5Gm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Gm;

    return-object v0
.end method

.method public static values()[LX/5Gm;
    .locals 1

    .prologue
    .line 889200
    sget-object v0, LX/5Gm;->$VALUES:[LX/5Gm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/5Gm;

    return-object v0
.end method
