.class public final LX/6LZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRESU",
            "LT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/6La;


# direct methods
.method public constructor <init>(Ljava/lang/Object;LX/6La;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRESU",
            "LT;",
            "LX/6La;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1078562
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1078563
    iput-object p1, p0, LX/6LZ;->a:Ljava/lang/Object;

    .line 1078564
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6La;

    iput-object v0, p0, LX/6LZ;->b:LX/6La;

    .line 1078565
    return-void
.end method

.method public static a(Ljava/lang/Object;)LX/6LZ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RESU",
            "LT:Ljava/lang/Object;",
            ">(TRESU",
            "LT;",
            ")",
            "LX/6LZ",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1078566
    new-instance v0, LX/6LZ;

    sget-object v1, LX/6La;->INTERMEDIATE:LX/6La;

    invoke-direct {v0, p0, v1}, LX/6LZ;-><init>(Ljava/lang/Object;LX/6La;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Object;)LX/6LZ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<RESU",
            "LT:Ljava/lang/Object;",
            ">(TRESU",
            "LT;",
            ")",
            "LX/6LZ",
            "<TRESU",
            "LT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1078567
    new-instance v0, LX/6LZ;

    sget-object v1, LX/6La;->FINAL:LX/6La;

    invoke-direct {v0, p0, v1}, LX/6LZ;-><init>(Ljava/lang/Object;LX/6La;)V

    return-object v0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 1078568
    iget-object v0, p0, LX/6LZ;->b:LX/6La;

    sget-object v1, LX/6La;->NOT_AVAILABLE:LX/6La;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
