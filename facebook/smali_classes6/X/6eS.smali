.class public LX/6eS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2MM;

.field private final b:LX/0TD;

.field private final c:LX/2Ib;


# direct methods
.method public constructor <init>(LX/2MM;LX/0TD;LX/2Ib;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1117938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1117939
    iput-object p1, p0, LX/6eS;->a:LX/2MM;

    .line 1117940
    iput-object p2, p0, LX/6eS;->b:LX/0TD;

    .line 1117941
    iput-object p3, p0, LX/6eS;->c:LX/2Ib;

    .line 1117942
    return-void
.end method

.method public static b(LX/0QB;)LX/6eS;
    .locals 4

    .prologue
    .line 1117936
    new-instance v3, LX/6eS;

    invoke-static {p0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v0

    check-cast v0, LX/2MM;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static {p0}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v2

    check-cast v2, LX/2Ib;

    invoke-direct {v3, v0, v1, v2}, LX/6eS;-><init>(LX/2MM;LX/0TD;LX/2Ib;)V

    .line 1117937
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/6eR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1117915
    iget-object v0, p0, LX/6eS;->b:LX/0TD;

    new-instance v1, LX/6eQ;

    invoke-direct {v1, p0, p1}, LX/6eQ;-><init>(LX/6eS;Lcom/facebook/ui/media/attachments/MediaResource;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6eR;
    .locals 5

    .prologue
    .line 1117916
    const/4 v0, 0x0

    .line 1117917
    :try_start_0
    iget-object v1, p0, LX/6eS;->a:LX/2MM;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v1, v2, v3}, LX/2MM;->a(Landroid/net/Uri;LX/46h;)LX/46f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1117918
    :try_start_1
    iget-object v0, v1, LX/46f;->a:Ljava/io/File;

    if-eqz v0, :cond_1

    .line 1117919
    iget-object v0, v1, LX/46f;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1117920
    sget-object v0, LX/6eR;->VALID:LX/6eR;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1117921
    if-eqz v1, :cond_0

    .line 1117922
    invoke-virtual {v1}, LX/46f;->a()V

    .line 1117923
    :cond_0
    :goto_0
    return-object v0

    .line 1117924
    :cond_1
    if-eqz v1, :cond_2

    .line 1117925
    invoke-virtual {v1}, LX/46f;->a()V

    .line 1117926
    :cond_2
    :goto_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 1117927
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 1117928
    invoke-static {}, LX/2Ib;->a()Z

    move-result v1

    .line 1117929
    if-eqz v0, :cond_4

    if-nez v1, :cond_4

    sget-object v0, LX/6eR;->INACCESSIBLE:LX/6eR;

    goto :goto_0

    .line 1117930
    :catch_0
    :goto_2
    if-eqz v0, :cond_2

    .line 1117931
    invoke-virtual {v0}, LX/46f;->a()V

    goto :goto_1

    .line 1117932
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_3
    if-eqz v1, :cond_3

    .line 1117933
    invoke-virtual {v1}, LX/46f;->a()V

    :cond_3
    throw v0

    .line 1117934
    :cond_4
    sget-object v0, LX/6eR;->NONEXISTENT:LX/6eR;

    goto :goto_0

    .line 1117935
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-object v0, v1

    goto :goto_2
.end method
