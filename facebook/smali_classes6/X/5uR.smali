.class public final LX/5uR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 1019180
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 1019181
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1019182
    :goto_0
    return v1

    .line 1019183
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1019184
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 1019185
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1019186
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1019187
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1019188
    const-string v9, "address"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1019189
    invoke-static {p0, p1}, LX/4aS;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1019190
    :cond_2
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1019191
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1019192
    :cond_3
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1019193
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1019194
    :cond_4
    const-string v9, "profile_photo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1019195
    invoke-static {p0, p1}, LX/5uP;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1019196
    :cond_5
    const-string v9, "review_context"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1019197
    invoke-static {p0, p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1019198
    :cond_6
    const-string v9, "viewer_recommendation"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1019199
    invoke-static {p0, p1}, LX/5u2;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1019200
    :cond_7
    const-string v9, "viewer_star_rating"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1019201
    invoke-static {p0, p1}, LX/5uQ;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1019202
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1019203
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1019204
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1019205
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1019206
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1019207
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1019208
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1019209
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1019210
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1019149
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1019150
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1019151
    if-eqz v0, :cond_0

    .line 1019152
    const-string v1, "address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1019153
    invoke-static {p0, v0, p2}, LX/4aS;->a(LX/15i;ILX/0nX;)V

    .line 1019154
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1019155
    if-eqz v0, :cond_1

    .line 1019156
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1019157
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1019158
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1019159
    if-eqz v0, :cond_2

    .line 1019160
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1019161
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1019162
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1019163
    if-eqz v0, :cond_3

    .line 1019164
    const-string v1, "profile_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1019165
    invoke-static {p0, v0, p2, p3}, LX/5uP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1019166
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1019167
    if-eqz v0, :cond_4

    .line 1019168
    const-string v1, "review_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1019169
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1019170
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1019171
    if-eqz v0, :cond_5

    .line 1019172
    const-string v1, "viewer_recommendation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1019173
    invoke-static {p0, v0, p2, p3}, LX/5u2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1019174
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1019175
    if-eqz v0, :cond_6

    .line 1019176
    const-string v1, "viewer_star_rating"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1019177
    invoke-static {p0, v0, p2}, LX/5uQ;->a(LX/15i;ILX/0nX;)V

    .line 1019178
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1019179
    return-void
.end method
