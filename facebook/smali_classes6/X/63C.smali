.class public LX/63C;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1042972
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1042973
    const/4 v0, 0x0

    iput v0, p0, LX/63C;->a:I

    .line 1042974
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1042969
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1042970
    const/4 v0, 0x0

    iput v0, p0, LX/63C;->a:I

    .line 1042971
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1042966
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1042967
    const/4 v0, 0x0

    iput v0, p0, LX/63C;->a:I

    .line 1042968
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1042958
    iget v0, p0, LX/63C;->a:I

    if-eqz v0, :cond_1

    .line 1042959
    :cond_0
    :goto_0
    return-void

    .line 1042960
    :cond_1
    invoke-virtual {p0}, LX/63C;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1042961
    if-eqz v0, :cond_0

    .line 1042962
    invoke-virtual {p0}, LX/63C;->getRight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v0

    sub-int v0, v3, v0

    if-le v2, v0, :cond_2

    const/4 v0, 0x1

    .line 1042963
    :goto_1
    if-eqz v0, :cond_3

    const/4 v0, 0x4

    :goto_2
    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1042964
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1042965
    goto :goto_2
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1042955
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbTextView;->onLayout(ZIIII)V

    .line 1042956
    invoke-direct {p0}, LX/63C;->a()V

    .line 1042957
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1042952
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 1042953
    invoke-direct {p0}, LX/63C;->a()V

    .line 1042954
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 1042947
    iput p1, p0, LX/63C;->a:I

    .line 1042948
    iget v0, p0, LX/63C;->a:I

    if-nez v0, :cond_0

    .line 1042949
    invoke-direct {p0}, LX/63C;->a()V

    .line 1042950
    :goto_0
    return-void

    .line 1042951
    :cond_0
    iget v0, p0, LX/63C;->a:I

    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method
