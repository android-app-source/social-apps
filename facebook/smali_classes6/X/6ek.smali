.class public final enum LX/6ek;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6ek;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6ek;

.field private static final ALL_FOLDERS_BY_DB_NAME:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/6ek;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ARCHIVED:LX/6ek;

.field public static final CONVERSATION_REQUEST_FOLDERS:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6ek;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INBOX:LX/6ek;

.field public static final enum MONTAGE:LX/6ek;

.field public static final enum NONE:LX/6ek;

.field public static final enum OTHER:LX/6ek;

.field public static final enum PENDING:LX/6ek;

.field public static final enum SMS_BUSINESS:LX/6ek;

.field public static final enum SMS_SPAM:LX/6ek;

.field public static final enum SPAM:LX/6ek;

.field public static final SYNC_SUPPORT_FOLDERS:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6ek;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final dbName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1118129
    new-instance v1, LX/6ek;

    const-string v2, "NONE"

    const-string v3, "none"

    invoke-direct {v1, v2, v0, v3}, LX/6ek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6ek;->NONE:LX/6ek;

    .line 1118130
    new-instance v1, LX/6ek;

    const-string v2, "INBOX"

    const-string v3, "inbox"

    invoke-direct {v1, v2, v5, v3}, LX/6ek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6ek;->INBOX:LX/6ek;

    .line 1118131
    new-instance v1, LX/6ek;

    const-string v2, "PENDING"

    const-string v3, "pending"

    invoke-direct {v1, v2, v6, v3}, LX/6ek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6ek;->PENDING:LX/6ek;

    .line 1118132
    new-instance v1, LX/6ek;

    const-string v2, "OTHER"

    const-string v3, "other"

    invoke-direct {v1, v2, v7, v3}, LX/6ek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6ek;->OTHER:LX/6ek;

    .line 1118133
    new-instance v1, LX/6ek;

    const-string v2, "ARCHIVED"

    const-string v3, "archived"

    invoke-direct {v1, v2, v8, v3}, LX/6ek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6ek;->ARCHIVED:LX/6ek;

    .line 1118134
    new-instance v1, LX/6ek;

    const-string v2, "SPAM"

    const/4 v3, 0x5

    const-string v4, "spam"

    invoke-direct {v1, v2, v3, v4}, LX/6ek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6ek;->SPAM:LX/6ek;

    .line 1118135
    new-instance v1, LX/6ek;

    const-string v2, "MONTAGE"

    const/4 v3, 0x6

    const-string v4, "montage"

    invoke-direct {v1, v2, v3, v4}, LX/6ek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6ek;->MONTAGE:LX/6ek;

    .line 1118136
    new-instance v1, LX/6ek;

    const-string v2, "SMS_SPAM"

    const/4 v3, 0x7

    const-string v4, "sms_spam"

    invoke-direct {v1, v2, v3, v4}, LX/6ek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6ek;->SMS_SPAM:LX/6ek;

    .line 1118137
    new-instance v1, LX/6ek;

    const-string v2, "SMS_BUSINESS"

    const/16 v3, 0x8

    const-string v4, "sms_business"

    invoke-direct {v1, v2, v3, v4}, LX/6ek;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/6ek;->SMS_BUSINESS:LX/6ek;

    .line 1118138
    const/16 v1, 0x9

    new-array v1, v1, [LX/6ek;

    sget-object v2, LX/6ek;->NONE:LX/6ek;

    aput-object v2, v1, v0

    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    aput-object v2, v1, v5

    sget-object v2, LX/6ek;->PENDING:LX/6ek;

    aput-object v2, v1, v6

    sget-object v2, LX/6ek;->OTHER:LX/6ek;

    aput-object v2, v1, v7

    sget-object v2, LX/6ek;->ARCHIVED:LX/6ek;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, LX/6ek;->SPAM:LX/6ek;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, LX/6ek;->MONTAGE:LX/6ek;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, LX/6ek;->SMS_SPAM:LX/6ek;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, LX/6ek;->SMS_BUSINESS:LX/6ek;

    aput-object v3, v1, v2

    sput-object v1, LX/6ek;->$VALUES:[LX/6ek;

    .line 1118139
    sget-object v1, LX/6ek;->PENDING:LX/6ek;

    sget-object v2, LX/6ek;->OTHER:LX/6ek;

    invoke-static {v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sput-object v1, LX/6ek;->CONVERSATION_REQUEST_FOLDERS:LX/0Rf;

    .line 1118140
    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    sget-object v2, LX/6ek;->MONTAGE:LX/6ek;

    invoke-static {v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    sput-object v1, LX/6ek;->SYNC_SUPPORT_FOLDERS:LX/0Rf;

    .line 1118141
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 1118142
    invoke-static {}, LX/6ek;->values()[LX/6ek;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1118143
    iget-object v5, v4, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1118144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1118145
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/6ek;->ALL_FOLDERS_BY_DB_NAME:LX/0P1;

    .line 1118146
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1118156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1118157
    iput-object p3, p0, LX/6ek;->dbName:Ljava/lang/String;

    .line 1118158
    return-void
.end method

.method public static fromDbName(Ljava/lang/String;)LX/6ek;
    .locals 3

    .prologue
    .line 1118152
    sget-object v0, LX/6ek;->ALL_FOLDERS_BY_DB_NAME:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ek;

    .line 1118153
    if-eqz v0, :cond_0

    .line 1118154
    return-object v0

    .line 1118155
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid name "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/6ek;
    .locals 1

    .prologue
    .line 1118151
    const-class v0, LX/6ek;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6ek;

    return-object v0
.end method

.method public static values()[LX/6ek;
    .locals 1

    .prologue
    .line 1118150
    sget-object v0, LX/6ek;->$VALUES:[LX/6ek;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6ek;

    return-object v0
.end method


# virtual methods
.method public final isMessageRequestFolders()Z
    .locals 1

    .prologue
    .line 1118149
    sget-object v0, LX/6ek;->PENDING:LX/6ek;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/6ek;->OTHER:LX/6ek;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSpamOrArchivedFolder()Z
    .locals 1

    .prologue
    .line 1118148
    sget-object v0, LX/6ek;->SPAM:LX/6ek;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/6ek;->ARCHIVED:LX/6ek;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1118147
    iget-object v0, p0, LX/6ek;->dbName:Ljava/lang/String;

    return-object v0
.end method
