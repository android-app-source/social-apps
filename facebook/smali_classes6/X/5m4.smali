.class public abstract LX/5m4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 999638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 999639
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v1

    .line 999640
    const/4 v0, 0x0

    .line 999641
    if-eqz v1, :cond_2

    .line 999642
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 999643
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 999644
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 999645
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 999646
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 999647
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 999648
    const-string v0, ", "

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 999649
    :cond_2
    return-object v0
.end method
