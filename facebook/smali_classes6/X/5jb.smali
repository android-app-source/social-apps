.class public final LX/5jb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 63

    .prologue
    .line 988050
    const/16 v56, 0x0

    .line 988051
    const-wide/16 v54, 0x0

    .line 988052
    const/16 v53, 0x0

    .line 988053
    const/16 v52, 0x0

    .line 988054
    const/16 v51, 0x0

    .line 988055
    const/16 v50, 0x0

    .line 988056
    const/16 v47, 0x0

    .line 988057
    const-wide/16 v48, 0x0

    .line 988058
    const/16 v46, 0x0

    .line 988059
    const/16 v43, 0x0

    .line 988060
    const-wide/16 v44, 0x0

    .line 988061
    const/16 v42, 0x0

    .line 988062
    const/16 v41, 0x0

    .line 988063
    const/16 v40, 0x0

    .line 988064
    const/16 v37, 0x0

    .line 988065
    const-wide/16 v38, 0x0

    .line 988066
    const/16 v36, 0x0

    .line 988067
    const-wide/16 v34, 0x0

    .line 988068
    const/16 v31, 0x0

    .line 988069
    const-wide/16 v32, 0x0

    .line 988070
    const/16 v30, 0x0

    .line 988071
    const/16 v25, 0x0

    .line 988072
    const-wide/16 v28, 0x0

    .line 988073
    const-wide/16 v26, 0x0

    .line 988074
    const/16 v24, 0x0

    .line 988075
    const-wide/16 v22, 0x0

    .line 988076
    const-wide/16 v20, 0x0

    .line 988077
    const-wide/16 v18, 0x0

    .line 988078
    const/16 v17, 0x0

    .line 988079
    const/16 v16, 0x0

    .line 988080
    const/4 v15, 0x0

    .line 988081
    const/4 v14, 0x0

    .line 988082
    const/4 v13, 0x0

    .line 988083
    const/4 v12, 0x0

    .line 988084
    const/4 v11, 0x0

    .line 988085
    const/4 v10, 0x0

    .line 988086
    const/4 v9, 0x0

    .line 988087
    const/4 v8, 0x0

    .line 988088
    const/4 v7, 0x0

    .line 988089
    const/4 v6, 0x0

    .line 988090
    const/4 v5, 0x0

    .line 988091
    const/4 v4, 0x0

    .line 988092
    const/4 v3, 0x0

    .line 988093
    const/4 v2, 0x0

    .line 988094
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v57

    sget-object v58, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v57

    move-object/from16 v1, v58

    if-eq v0, v1, :cond_2e

    .line 988095
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 988096
    const/4 v2, 0x0

    .line 988097
    :goto_0
    return v2

    .line 988098
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_1e

    .line 988099
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 988100
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 988101
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 988102
    const-string v6, "animation_assets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 988103
    invoke-static/range {p0 .. p1}, LX/5jY;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v59, v2

    goto :goto_1

    .line 988104
    :cond_1
    const-string v6, "attraction_force_strength"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 988105
    const/4 v2, 0x1

    .line 988106
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 988107
    :cond_2
    const-string v6, "emitter_assets"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 988108
    invoke-static/range {p0 .. p1}, LX/5ja;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v58, v2

    goto :goto_1

    .line 988109
    :cond_3
    const-string v6, "extra_config"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 988110
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v57, v2

    goto :goto_1

    .line 988111
    :cond_4
    const-string v6, "gravity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 988112
    invoke-static/range {p0 .. p1}, LX/5jW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v56, v2

    goto :goto_1

    .line 988113
    :cond_5
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 988114
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v55, v2

    goto :goto_1

    .line 988115
    :cond_6
    const-string v6, "init_max_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 988116
    invoke-static/range {p0 .. p1}, LX/5jW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v54, v2

    goto/16 :goto_1

    .line 988117
    :cond_7
    const-string v6, "init_max_rotation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 988118
    const/4 v2, 0x1

    .line 988119
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v21, v2

    move-wide/from16 v52, v6

    goto/16 :goto_1

    .line 988120
    :cond_8
    const-string v6, "init_max_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 988121
    invoke-static/range {p0 .. p1}, LX/5jW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v51, v2

    goto/16 :goto_1

    .line 988122
    :cond_9
    const-string v6, "init_min_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 988123
    invoke-static/range {p0 .. p1}, LX/5jW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v50, v2

    goto/16 :goto_1

    .line 988124
    :cond_a
    const-string v6, "init_min_rotation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 988125
    const/4 v2, 0x1

    .line 988126
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v20, v2

    move-wide/from16 v48, v6

    goto/16 :goto_1

    .line 988127
    :cond_b
    const-string v6, "init_min_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 988128
    invoke-static/range {p0 .. p1}, LX/5jW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v47, v2

    goto/16 :goto_1

    .line 988129
    :cond_c
    const-string v6, "init_particles"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 988130
    const/4 v2, 0x1

    .line 988131
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v19, v2

    move/from16 v46, v6

    goto/16 :goto_1

    .line 988132
    :cond_d
    const-string v6, "max_color_hsva"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 988133
    invoke-static/range {p0 .. p1}, LX/5jc;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v45, v2

    goto/16 :goto_1

    .line 988134
    :cond_e
    const-string v6, "max_lifetime_millis"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 988135
    const/4 v2, 0x1

    .line 988136
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v18, v2

    move/from16 v44, v6

    goto/16 :goto_1

    .line 988137
    :cond_f
    const-string v6, "max_linear_dampening"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 988138
    const/4 v2, 0x1

    .line 988139
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move/from16 v17, v2

    move-wide/from16 v42, v6

    goto/16 :goto_1

    .line 988140
    :cond_10
    const-string v6, "max_particles"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 988141
    const/4 v2, 0x1

    .line 988142
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move/from16 v16, v2

    move/from16 v39, v6

    goto/16 :goto_1

    .line 988143
    :cond_11
    const-string v6, "max_pixel_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 988144
    const/4 v2, 0x1

    .line 988145
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v15, v2

    move-wide/from16 v40, v6

    goto/16 :goto_1

    .line 988146
    :cond_12
    const-string v6, "max_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 988147
    invoke-static/range {p0 .. p1}, LX/5jW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 988148
    :cond_13
    const-string v6, "max_rotation_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 988149
    const/4 v2, 0x1

    .line 988150
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v14, v2

    move-wide/from16 v36, v6

    goto/16 :goto_1

    .line 988151
    :cond_14
    const-string v6, "min_color_hsva"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 988152
    invoke-static/range {p0 .. p1}, LX/5jc;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v35, v2

    goto/16 :goto_1

    .line 988153
    :cond_15
    const-string v6, "min_lifetime_millis"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 988154
    const/4 v2, 0x1

    .line 988155
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v13, v2

    move/from16 v34, v6

    goto/16 :goto_1

    .line 988156
    :cond_16
    const-string v6, "min_linear_dampening"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 988157
    const/4 v2, 0x1

    .line 988158
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v12, v2

    move-wide/from16 v32, v6

    goto/16 :goto_1

    .line 988159
    :cond_17
    const-string v6, "min_pixel_size"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 988160
    const/4 v2, 0x1

    .line 988161
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v30, v6

    goto/16 :goto_1

    .line 988162
    :cond_18
    const-string v6, "min_position"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 988163
    invoke-static/range {p0 .. p1}, LX/5jW;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 988164
    :cond_19
    const-string v6, "min_rotation_velocity"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 988165
    const/4 v2, 0x1

    .line 988166
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v28, v6

    goto/16 :goto_1

    .line 988167
    :cond_1a
    const-string v6, "rotational_dampening"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 988168
    const/4 v2, 0x1

    .line 988169
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide/from16 v26, v6

    goto/16 :goto_1

    .line 988170
    :cond_1b
    const-string v6, "spawn_rate"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 988171
    const/4 v2, 0x1

    .line 988172
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide/from16 v24, v6

    goto/16 :goto_1

    .line 988173
    :cond_1c
    const-string v6, "velocity_scalar"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 988174
    invoke-static/range {p0 .. p1}, LX/5jV;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 988175
    :cond_1d
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 988176
    :cond_1e
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 988177
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v59

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988178
    if-eqz v3, :cond_1f

    .line 988179
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988180
    :cond_1f
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v58

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988181
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v57

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988182
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v56

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988183
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988184
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988185
    if-eqz v21, :cond_20

    .line 988186
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v52

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988187
    :cond_20
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988188
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988189
    if-eqz v20, :cond_21

    .line 988190
    const/16 v3, 0xa

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v48

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988191
    :cond_21
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988192
    if-eqz v19, :cond_22

    .line 988193
    const/16 v2, 0xc

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 988194
    :cond_22
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988195
    if-eqz v18, :cond_23

    .line 988196
    const/16 v2, 0xe

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 988197
    :cond_23
    if-eqz v17, :cond_24

    .line 988198
    const/16 v3, 0xf

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v42

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988199
    :cond_24
    if-eqz v16, :cond_25

    .line 988200
    const/16 v2, 0x10

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 988201
    :cond_25
    if-eqz v15, :cond_26

    .line 988202
    const/16 v3, 0x11

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v40

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988203
    :cond_26
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988204
    if-eqz v14, :cond_27

    .line 988205
    const/16 v3, 0x13

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v36

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988206
    :cond_27
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988207
    if-eqz v13, :cond_28

    .line 988208
    const/16 v2, 0x15

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 988209
    :cond_28
    if-eqz v12, :cond_29

    .line 988210
    const/16 v3, 0x16

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v32

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988211
    :cond_29
    if-eqz v11, :cond_2a

    .line 988212
    const/16 v3, 0x17

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v30

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988213
    :cond_2a
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988214
    if-eqz v10, :cond_2b

    .line 988215
    const/16 v3, 0x19

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v28

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988216
    :cond_2b
    if-eqz v9, :cond_2c

    .line 988217
    const/16 v3, 0x1a

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v26

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988218
    :cond_2c
    if-eqz v8, :cond_2d

    .line 988219
    const/16 v3, 0x1b

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v24

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 988220
    :cond_2d
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 988221
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_2e
    move/from16 v57, v52

    move/from16 v58, v53

    move/from16 v59, v56

    move-wide/from16 v52, v48

    move/from16 v56, v51

    move-wide/from16 v48, v44

    move/from16 v51, v46

    move/from16 v44, v37

    move/from16 v45, v40

    move/from16 v46, v41

    move-wide/from16 v40, v34

    move/from16 v34, v25

    move/from16 v35, v30

    move/from16 v60, v15

    move v15, v9

    move v9, v3

    move/from16 v3, v16

    move/from16 v16, v10

    move v10, v4

    move-wide/from16 v61, v22

    move/from16 v23, v24

    move/from16 v22, v17

    move-wide/from16 v24, v18

    move/from16 v17, v11

    move/from16 v19, v13

    move/from16 v18, v12

    move v11, v5

    move v13, v7

    move v12, v6

    move-wide/from16 v4, v54

    move/from16 v54, v47

    move/from16 v55, v50

    move/from16 v47, v42

    move/from16 v50, v43

    move-wide/from16 v42, v38

    move/from16 v38, v31

    move/from16 v39, v36

    move-wide/from16 v36, v32

    move-wide/from16 v30, v26

    move-wide/from16 v32, v28

    move-wide/from16 v26, v20

    move/from16 v20, v14

    move-wide/from16 v28, v61

    move/from16 v21, v60

    move v14, v8

    move v8, v2

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 988222
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 988223
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 988224
    if-eqz v0, :cond_0

    .line 988225
    const-string v1, "animation_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988226
    invoke-static {p0, v0, p2, p3}, LX/5jY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 988227
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988228
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 988229
    const-string v2, "attraction_force_strength"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988230
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988231
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988232
    if-eqz v0, :cond_2

    .line 988233
    const-string v1, "emitter_assets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988234
    invoke-static {p0, v0, p2, p3}, LX/5ja;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 988235
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 988236
    if-eqz v0, :cond_3

    .line 988237
    const-string v1, "extra_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988238
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 988239
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988240
    if-eqz v0, :cond_4

    .line 988241
    const-string v1, "gravity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988242
    invoke-static {p0, v0, p2}, LX/5jW;->a(LX/15i;ILX/0nX;)V

    .line 988243
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 988244
    if-eqz v0, :cond_5

    .line 988245
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988246
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 988247
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988248
    if-eqz v0, :cond_6

    .line 988249
    const-string v1, "init_max_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988250
    invoke-static {p0, v0, p2}, LX/5jW;->a(LX/15i;ILX/0nX;)V

    .line 988251
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988252
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_7

    .line 988253
    const-string v2, "init_max_rotation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988254
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988255
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988256
    if-eqz v0, :cond_8

    .line 988257
    const-string v1, "init_max_velocity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988258
    invoke-static {p0, v0, p2}, LX/5jW;->a(LX/15i;ILX/0nX;)V

    .line 988259
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988260
    if-eqz v0, :cond_9

    .line 988261
    const-string v1, "init_min_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988262
    invoke-static {p0, v0, p2}, LX/5jW;->a(LX/15i;ILX/0nX;)V

    .line 988263
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988264
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_a

    .line 988265
    const-string v2, "init_min_rotation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988266
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988267
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988268
    if-eqz v0, :cond_b

    .line 988269
    const-string v1, "init_min_velocity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988270
    invoke-static {p0, v0, p2}, LX/5jW;->a(LX/15i;ILX/0nX;)V

    .line 988271
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 988272
    if-eqz v0, :cond_c

    .line 988273
    const-string v1, "init_particles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988274
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 988275
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988276
    if-eqz v0, :cond_d

    .line 988277
    const-string v1, "max_color_hsva"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988278
    invoke-static {p0, v0, p2}, LX/5jc;->a(LX/15i;ILX/0nX;)V

    .line 988279
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 988280
    if-eqz v0, :cond_e

    .line 988281
    const-string v1, "max_lifetime_millis"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988282
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 988283
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988284
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_f

    .line 988285
    const-string v2, "max_linear_dampening"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988286
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988287
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 988288
    if-eqz v0, :cond_10

    .line 988289
    const-string v1, "max_particles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988290
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 988291
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988292
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_11

    .line 988293
    const-string v2, "max_pixel_size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988294
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988295
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988296
    if-eqz v0, :cond_12

    .line 988297
    const-string v1, "max_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988298
    invoke-static {p0, v0, p2}, LX/5jW;->a(LX/15i;ILX/0nX;)V

    .line 988299
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988300
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_13

    .line 988301
    const-string v2, "max_rotation_velocity"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988302
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988303
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988304
    if-eqz v0, :cond_14

    .line 988305
    const-string v1, "min_color_hsva"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988306
    invoke-static {p0, v0, p2}, LX/5jc;->a(LX/15i;ILX/0nX;)V

    .line 988307
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 988308
    if-eqz v0, :cond_15

    .line 988309
    const-string v1, "min_lifetime_millis"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988310
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 988311
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988312
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_16

    .line 988313
    const-string v2, "min_linear_dampening"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988314
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988315
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988316
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_17

    .line 988317
    const-string v2, "min_pixel_size"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988318
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988319
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988320
    if-eqz v0, :cond_18

    .line 988321
    const-string v1, "min_position"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988322
    invoke-static {p0, v0, p2}, LX/5jW;->a(LX/15i;ILX/0nX;)V

    .line 988323
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988324
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_19

    .line 988325
    const-string v2, "min_rotation_velocity"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988326
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988327
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988328
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1a

    .line 988329
    const-string v2, "rotational_dampening"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988330
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988331
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 988332
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1b

    .line 988333
    const-string v2, "spawn_rate"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988334
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 988335
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 988336
    if-eqz v0, :cond_1c

    .line 988337
    const-string v1, "velocity_scalar"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 988338
    invoke-static {p0, v0, p2}, LX/5jV;->a(LX/15i;ILX/0nX;)V

    .line 988339
    :cond_1c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 988340
    return-void
.end method
