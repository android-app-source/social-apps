.class public LX/6Hb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/hardware/Camera;

.field private final c:LX/6HF;

.field private d:I

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:F

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1072467
    const-class v0, LX/6Hb;

    sput-object v0, LX/6Hb;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/hardware/Camera;LX/6HF;)V
    .locals 1

    .prologue
    .line 1072452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1072453
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, LX/6Hb;->g:F

    .line 1072454
    iput-object p1, p0, LX/6Hb;->b:Landroid/hardware/Camera;

    .line 1072455
    iput-object p2, p0, LX/6Hb;->c:LX/6HF;

    .line 1072456
    return-void
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1072468
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    sub-float/2addr v0, v2

    .line 1072469
    iget v1, p0, LX/6Hb;->g:F

    mul-float/2addr v0, v1

    add-float v1, v2, v0

    .line 1072470
    iget-object v0, p0, LX/6Hb;->f:Ljava/util/List;

    iget v2, p0, LX/6Hb;->d:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v3, v0

    .line 1072471
    iget-object v0, p0, LX/6Hb;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v4

    .line 1072472
    invoke-virtual {v4}, Landroid/hardware/Camera$Parameters;->getZoom()I

    move-result v2

    .line 1072473
    if-ltz v2, :cond_0

    iget v0, p0, LX/6Hb;->e:I

    if-ge v2, v0, :cond_0

    .line 1072474
    iget-object v0, p0, LX/6Hb;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v3, v0, :cond_3

    move v1, v2

    .line 1072475
    :goto_0
    iget v0, p0, LX/6Hb;->e:I

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_1

    iget-object v0, p0, LX/6Hb;->f:Ljava/util/List;

    add-int/lit8 v5, v1, 0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v3, v0, :cond_1

    .line 1072476
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1072477
    :goto_1
    if-lez v1, :cond_1

    iget-object v0, p0, LX/6Hb;->f:Ljava/util/List;

    add-int/lit8 v5, v1, -0x1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 1072478
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_0
    move v1, v2

    .line 1072479
    :cond_1
    if-eq v2, v1, :cond_2

    .line 1072480
    invoke-virtual {v4, v1}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    .line 1072481
    :try_start_0
    iget-object v0, p0, LX/6Hb;->b:Landroid/hardware/Camera;

    invoke-virtual {v0, v4}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1072482
    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6Hb;->h:Z

    .line 1072483
    :cond_2
    const/4 v0, 0x0

    return v0

    .line 1072484
    :catch_0
    move-exception v0

    .line 1072485
    iget-object v1, p0, LX/6Hb;->c:LX/6HF;

    const-string v2, "onScale/setParameters failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1072458
    iget-object v0, p0, LX/6Hb;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 1072459
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getZoom()I

    move-result v1

    iput v1, p0, LX/6Hb;->d:I

    .line 1072460
    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getZoomRatios()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/6Hb;->f:Ljava/util/List;

    .line 1072461
    iget-object v0, p0, LX/6Hb;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, LX/6Hb;->e:I

    .line 1072462
    :goto_0
    iget v0, p0, LX/6Hb;->e:I

    if-le v0, v3, :cond_0

    iget-object v0, p0, LX/6Hb;->f:Ljava/util/List;

    iget v1, p0, LX/6Hb;->e:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x190

    if-le v0, v1, :cond_0

    .line 1072463
    iget v0, p0, LX/6Hb;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/6Hb;->e:I

    goto :goto_0

    .line 1072464
    :cond_0
    iget-object v0, p0, LX/6Hb;->f:Ljava/util/List;

    iget v1, p0, LX/6Hb;->e:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    .line 1072465
    const v1, 0x3f933333    # 1.15f

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LX/6Hb;->g:F

    .line 1072466
    return v3
.end method

.method public final onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0

    .prologue
    .line 1072457
    return-void
.end method
