.class public final LX/5bW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 958574
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 958575
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 958576
    :goto_0
    return v1

    .line 958577
    :cond_0
    const-string v8, "is_forwardable"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 958578
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    .line 958579
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 958580
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 958581
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 958582
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 958583
    const-string v8, "genie_attachment"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 958584
    invoke-static {p0, p1}, LX/5aU;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 958585
    :cond_2
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 958586
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 958587
    :cond_3
    const-string v8, "story_attachment"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 958588
    invoke-static {p0, p1}, LX/5bV;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 958589
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 958590
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 958591
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 958592
    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 958593
    if-eqz v0, :cond_6

    .line 958594
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 958595
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 958596
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 958597
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 958598
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958599
    if-eqz v0, :cond_0

    .line 958600
    const-string v1, "genie_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958601
    invoke-static {p0, v0, p2, p3}, LX/5aU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 958602
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 958603
    if-eqz v0, :cond_1

    .line 958604
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958605
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 958606
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 958607
    if-eqz v0, :cond_2

    .line 958608
    const-string v1, "is_forwardable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958609
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 958610
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 958611
    if-eqz v0, :cond_3

    .line 958612
    const-string v1, "story_attachment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 958613
    invoke-static {p0, v0, p2, p3}, LX/5bV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 958614
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 958615
    return-void
.end method
