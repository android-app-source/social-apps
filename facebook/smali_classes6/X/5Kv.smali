.class public LX/5Kv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 899304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 899296
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 899297
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 899298
    :sswitch_0
    const-string v1, "normal"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "light"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "medium"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "bold"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 899299
    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 899300
    :goto_1
    return-object v0

    .line 899301
    :pswitch_1
    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    goto :goto_1

    .line 899302
    :pswitch_2
    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    goto :goto_1

    .line 899303
    :pswitch_3
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4041708b -> :sswitch_2
        -0x3df94319 -> :sswitch_0
        0x2e3a85 -> :sswitch_3
        0x6233516 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/1De;Lcom/facebook/java2js/JSValue;)LX/1ne;
    .locals 4

    .prologue
    const/high16 v3, 0x41600000    # 14.0f

    .line 899261
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    .line 899262
    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->isString()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 899263
    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->asString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1ne;->g(F)LX/1ne;

    move-result-object v0

    .line 899264
    :goto_0
    return-object v0

    .line 899265
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->isNull()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->isUndefined()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 899266
    goto :goto_0

    .line 899267
    :cond_2
    const-string v0, "string"

    invoke-virtual {p1, v0}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 899268
    if-eqz v0, :cond_3

    .line 899269
    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    .line 899270
    :cond_3
    const-string v0, "attributes"

    invoke-virtual {p1, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    .line 899271
    if-eqz v2, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 899272
    const-string v0, "color"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 899273
    if-eqz v0, :cond_4

    .line 899274
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, LX/1ne;->m(I)LX/1ne;

    .line 899275
    :cond_4
    const-string v0, "font"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    .line 899276
    if-eqz v2, :cond_7

    .line 899277
    const-class v0, LX/5KJ;

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5KJ;

    .line 899278
    if-eqz v0, :cond_6

    .line 899279
    iget-object v2, v0, LX/5KJ;->a:Landroid/graphics/Typeface;

    move-object v2, v2

    .line 899280
    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    .line 899281
    iget v2, v0, LX/5KJ;->b:I

    move v0, v2

    .line 899282
    int-to-float v0, v0

    invoke-virtual {v1, v0}, LX/1ne;->g(F)LX/1ne;

    :goto_2
    move-object v0, v1

    .line 899283
    goto :goto_0

    .line 899284
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 899285
    :cond_6
    const-string v0, "weight"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/5Kv;->a(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    .line 899286
    const-string v0, "size"

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v2

    double-to-int v0, v2

    int-to-float v0, v0

    invoke-virtual {v1, v0}, LX/1ne;->g(F)LX/1ne;

    goto :goto_2

    .line 899287
    :cond_7
    invoke-virtual {v1, v3}, LX/1ne;->g(F)LX/1ne;

    goto :goto_2
.end method

.method public static b(Ljava/lang/String;)Landroid/text/TextUtils$TruncateAt;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 899288
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 899289
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 899290
    :sswitch_0
    const-string v1, "end"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "middle"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "start"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "none"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 899291
    :pswitch_0
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 899292
    :goto_1
    return-object v0

    .line 899293
    :pswitch_1
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    goto :goto_1

    .line 899294
    :pswitch_2
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    goto :goto_1

    .line 899295
    :pswitch_3
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x4009266b -> :sswitch_1
        0x188db -> :sswitch_0
        0x33af38 -> :sswitch_3
        0x68ac462 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
