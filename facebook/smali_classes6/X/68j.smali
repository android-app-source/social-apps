.class public LX/68j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final A:[F

.field private B:Landroid/view/VelocityTracker;

.field private C:F

.field private D:Z

.field public E:F

.field public F:F

.field private G:F

.field private final H:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

.field private final I:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

.field public a:LX/68G;

.field public b:Landroid/graphics/Matrix;

.field private c:I

.field private d:J

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:Z

.field private j:F

.field private k:F

.field private l:Z

.field public m:F

.field public n:F

.field private final o:I

.field private p:J

.field public q:Z

.field private final r:J

.field private s:F

.field private t:F

.field private u:J

.field private v:Z

.field private w:I

.field private x:I

.field private y:F

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/68G;)V
    .locals 4

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1056171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1056172
    iput v1, p0, LX/68j;->j:F

    .line 1056173
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, LX/68j;->A:[F

    .line 1056174
    iput v1, p0, LX/68j;->E:F

    .line 1056175
    iput v1, p0, LX/68j;->F:F

    .line 1056176
    iput v1, p0, LX/68j;->G:F

    .line 1056177
    new-instance v0, Lcom/facebook/android/maps/internal/GestureDetector$1;

    invoke-direct {v0, p0}, Lcom/facebook/android/maps/internal/GestureDetector$1;-><init>(LX/68j;)V

    iput-object v0, p0, LX/68j;->H:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 1056178
    new-instance v0, Lcom/facebook/android/maps/internal/GestureDetector$2;

    invoke-direct {v0, p0}, Lcom/facebook/android/maps/internal/GestureDetector$2;-><init>(LX/68j;)V

    iput-object v0, p0, LX/68j;->I:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    .line 1056179
    iput-object p2, p0, LX/68j;->a:LX/68G;

    .line 1056180
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 1056181
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, LX/68j;->o:I

    .line 1056182
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    int-to-long v2, v1

    iput-wide v2, p0, LX/68j;->r:J

    .line 1056183
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v1

    iput v1, p0, LX/68j;->w:I

    .line 1056184
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    iput v1, p0, LX/68j;->x:I

    .line 1056185
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/68j;->C:F

    .line 1056186
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/68j;->D:Z

    .line 1056187
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    .line 1056188
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 1056189
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    .line 1056190
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    .line 1056191
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    .line 1056192
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    .line 1056193
    iget-boolean v1, p0, LX/68j;->q:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    if-gt v4, v1, :cond_0

    iget v1, p0, LX/68j;->m:F

    sub-float v1, v5, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v7, p0, LX/68j;->o:I

    int-to-float v7, v7

    cmpl-float v1, v1, v7

    if-gtz v1, :cond_0

    iget v1, p0, LX/68j;->n:F

    sub-float v1, v6, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v7, p0, LX/68j;->o:I

    int-to-float v7, v7

    cmpl-float v1, v1, v7

    if-lez v1, :cond_1

    .line 1056194
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/68j;->q:Z

    .line 1056195
    const-string v1, "longPressTimeout"

    invoke-static {v1}, LX/31l;->b(Ljava/lang/String;)V

    .line 1056196
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_14

    .line 1056197
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1056198
    const/4 v0, 0x0

    move v12, v1

    move v1, v2

    move v2, v12

    :goto_0
    if-ge v0, v4, :cond_2

    .line 1056199
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    add-float/2addr v3, v1

    .line 1056200
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    add-float/2addr v1, v2

    .line 1056201
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v3

    goto :goto_0

    .line 1056202
    :cond_2
    int-to-float v0, v4

    div-float/2addr v1, v0

    .line 1056203
    int-to-float v0, v4

    div-float v0, v2, v0

    .line 1056204
    const/4 v3, 0x0

    .line 1056205
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_3

    .line 1056206
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    sub-float/2addr v7, v1

    float-to-double v8, v7

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    sub-float/2addr v7, v0

    float-to-double v10, v7

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v8

    double-to-float v7, v8

    add-float/2addr v3, v7

    .line 1056207
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1056208
    :cond_3
    int-to-float v2, v4

    div-float/2addr v3, v2

    .line 1056209
    iput v1, p0, LX/68j;->g:F

    .line 1056210
    iput v0, p0, LX/68j;->h:F

    .line 1056211
    iget-object v2, p0, LX/68j;->b:Landroid/graphics/Matrix;

    if-eqz v2, :cond_4

    .line 1056212
    iget-object v2, p0, LX/68j;->A:[F

    const/4 v7, 0x0

    aput v1, v2, v7

    .line 1056213
    iget-object v1, p0, LX/68j;->A:[F

    const/4 v2, 0x1

    aput v0, v1, v2

    .line 1056214
    iget-object v0, p0, LX/68j;->b:Landroid/graphics/Matrix;

    iget-object v1, p0, LX/68j;->A:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1056215
    iget-object v0, p0, LX/68j;->A:[F

    const/4 v1, 0x0

    aget v1, v0, v1

    .line 1056216
    iget-object v0, p0, LX/68j;->A:[F

    const/4 v2, 0x1

    aget v0, v0, v2

    .line 1056217
    :cond_4
    iget v2, p0, LX/68j;->c:I

    if-eq v4, v2, :cond_8

    .line 1056218
    iput v3, p0, LX/68j;->k:F

    .line 1056219
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, LX/68j;->j:F

    .line 1056220
    const/4 v2, 0x1

    if-le v4, v2, :cond_5

    .line 1056221
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float v2, v6, v2

    float-to-double v2, v2

    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    sub-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, LX/68j;->y:F

    .line 1056222
    :cond_5
    iget-object v2, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_6

    .line 1056223
    iget-object v2, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    .line 1056224
    :cond_6
    :goto_2
    iput v1, p0, LX/68j;->e:F

    .line 1056225
    iput v0, p0, LX/68j;->f:F

    .line 1056226
    iput v4, p0, LX/68j;->c:I

    .line 1056227
    :cond_7
    :goto_3
    const/4 v0, 0x1

    return v0

    .line 1056228
    :cond_8
    iget v2, p0, LX/68j;->e:F

    sub-float v2, v1, v2

    .line 1056229
    iget v7, p0, LX/68j;->f:F

    sub-float v7, v0, v7

    .line 1056230
    iget-boolean v8, p0, LX/68j;->i:Z

    if-nez v8, :cond_9

    iget v8, p0, LX/68j;->g:F

    iget v9, p0, LX/68j;->m:F

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v9, p0, LX/68j;->o:I

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-gtz v8, :cond_b

    iget v8, p0, LX/68j;->h:F

    iget v9, p0, LX/68j;->n:F

    sub-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    iget v9, p0, LX/68j;->o:I

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-gtz v8, :cond_b

    :cond_9
    iget-boolean v8, p0, LX/68j;->i:Z

    if-eqz v8, :cond_d

    iget-boolean v8, p0, LX/68j;->D:Z

    if-nez v8, :cond_a

    iget-boolean v8, p0, LX/68j;->l:Z

    if-nez v8, :cond_d

    :cond_a
    const/4 v8, 0x0

    cmpl-float v8, v2, v8

    if-nez v8, :cond_b

    const/4 v8, 0x0

    cmpl-float v8, v7, v8

    if-eqz v8, :cond_d

    .line 1056231
    :cond_b
    iget-object v8, p0, LX/68j;->a:LX/68G;

    iget v9, p0, LX/68j;->g:F

    iget v10, p0, LX/68j;->h:F

    iget v11, p0, LX/68j;->E:F

    mul-float/2addr v2, v11

    iget v11, p0, LX/68j;->E:F

    mul-float/2addr v7, v11

    invoke-interface {v8, v9, v10, v2, v7}, LX/68G;->a(FFFF)V

    .line 1056232
    iget-object v2, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_c

    .line 1056233
    iget-object v2, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1056234
    :cond_c
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/68j;->i:Z

    .line 1056235
    :cond_d
    const/4 v2, 0x1

    if-le v4, v2, :cond_6

    .line 1056236
    iget v2, p0, LX/68j;->k:F

    const/4 v7, 0x0

    cmpl-float v2, v2, v7

    if-eqz v2, :cond_12

    iget v2, p0, LX/68j;->k:F

    div-float v2, v3, v2

    .line 1056237
    :goto_4
    iget v7, p0, LX/68j;->j:F

    div-float v7, v2, v7

    .line 1056238
    iget-object v8, p0, LX/68j;->a:LX/68G;

    iget v9, p0, LX/68j;->G:F

    mul-float/2addr v7, v9

    iget v9, p0, LX/68j;->g:F

    iget v10, p0, LX/68j;->h:F

    invoke-interface {v8, v7, v9, v10}, LX/68G;->a(FFF)V

    .line 1056239
    iput v2, p0, LX/68j;->j:F

    .line 1056240
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/68j;->l:Z

    .line 1056241
    iget-boolean v2, p0, LX/68j;->D:Z

    if-eqz v2, :cond_11

    .line 1056242
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float v2, v6, v2

    float-to-double v6, v2

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    sub-float v2, v5, v2

    float-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v6

    double-to-float v5, v6

    .line 1056243
    iget v2, p0, LX/68j;->c:I

    if-eq v2, v4, :cond_e

    .line 1056244
    iput v5, p0, LX/68j;->y:F

    .line 1056245
    :cond_e
    iget v2, p0, LX/68j;->y:F

    sub-float v2, v5, v2

    .line 1056246
    const/high16 v6, 0x43340000    # 180.0f

    cmpl-float v6, v2, v6

    if-lez v6, :cond_13

    .line 1056247
    const/high16 v6, 0x43b40000    # 360.0f

    sub-float/2addr v2, v6

    .line 1056248
    :cond_f
    :goto_5
    iput v5, p0, LX/68j;->y:F

    .line 1056249
    const/high16 v5, -0x3e100000    # -30.0f

    cmpg-float v5, v5, v2

    if-gez v5, :cond_10

    const/high16 v5, 0x41f00000    # 30.0f

    cmpg-float v5, v2, v5

    if-gez v5, :cond_10

    .line 1056250
    iget-object v5, p0, LX/68j;->a:LX/68G;

    iget v6, p0, LX/68j;->F:F

    mul-float/2addr v6, v2

    iget v7, p0, LX/68j;->g:F

    iget v8, p0, LX/68j;->h:F

    invoke-interface {v5, v6, v7, v8}, LX/68G;->b(FFF)V

    .line 1056251
    const/4 v5, 0x1

    iput-boolean v5, p0, LX/68j;->z:Z

    .line 1056252
    :cond_10
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v6, v2

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    cmpl-double v2, v6, v8

    if-lez v2, :cond_11

    .line 1056253
    const-wide/16 v6, 0x0

    iput-wide v6, p0, LX/68j;->p:J

    .line 1056254
    :cond_11
    iget v2, p0, LX/68j;->k:F

    sub-float v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, LX/68j;->o:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_6

    .line 1056255
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/68j;->p:J

    goto/16 :goto_2

    .line 1056256
    :cond_12
    const/high16 v2, 0x3f800000    # 1.0f

    goto/16 :goto_4

    .line 1056257
    :cond_13
    const/high16 v6, -0x3ccc0000    # -180.0f

    cmpg-float v6, v2, v6

    if-gez v6, :cond_f

    .line 1056258
    const/high16 v6, 0x43b40000    # 360.0f

    add-float/2addr v2, v6

    goto :goto_5

    .line 1056259
    :cond_14
    if-nez v0, :cond_18

    .line 1056260
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68j;->i:Z

    .line 1056261
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68j;->l:Z

    .line 1056262
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68j;->z:Z

    .line 1056263
    iget-boolean v0, p0, LX/68j;->v:Z

    if-eqz v0, :cond_15

    iget-wide v0, p0, LX/68j;->u:J

    sub-long v0, v2, v0

    iget v4, p0, LX/68j;->w:I

    int-to-long v8, v4

    cmp-long v0, v0, v8

    if-gtz v0, :cond_15

    iget v0, p0, LX/68j;->s:F

    sub-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LX/68j;->x:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_15

    iget v0, p0, LX/68j;->t:F

    sub-float v0, v6, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LX/68j;->x:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_16

    .line 1056264
    :cond_15
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68j;->v:Z

    .line 1056265
    iput v5, p0, LX/68j;->s:F

    .line 1056266
    iput v6, p0, LX/68j;->t:F

    .line 1056267
    iput-wide v2, p0, LX/68j;->u:J

    .line 1056268
    :cond_16
    iput v5, p0, LX/68j;->m:F

    .line 1056269
    iput v6, p0, LX/68j;->n:F

    .line 1056270
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/68j;->q:Z

    .line 1056271
    iget-object v0, p0, LX/68j;->I:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    const-string v1, "longPressTimeout"

    iget-wide v2, p0, LX/68j;->r:J

    invoke-static {v0, v1, v2, v3}, LX/31l;->a(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;Ljava/lang/String;J)V

    .line 1056272
    iget-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    if-nez v0, :cond_17

    .line 1056273
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    .line 1056274
    :goto_6
    iget-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1056275
    iget-object v0, p0, LX/68j;->a:LX/68G;

    invoke-interface {v0, v5, v6}, LX/68G;->a(FF)V

    goto/16 :goto_3

    .line 1056276
    :cond_17
    iget-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_6

    .line 1056277
    :cond_18
    const/4 v1, 0x1

    if-ne v0, v1, :cond_20

    .line 1056278
    const/4 v0, 0x0

    iput v0, p0, LX/68j;->c:I

    .line 1056279
    iget-wide v0, p0, LX/68j;->p:J

    sub-long v0, v2, v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v4

    int-to-long v8, v4

    cmp-long v0, v0, v8

    if-gez v0, :cond_1a

    .line 1056280
    iget-object v0, p0, LX/68j;->a:LX/68G;

    invoke-interface {v0}, LX/68G;->d()V

    .line 1056281
    :cond_19
    :goto_7
    iget-boolean v0, p0, LX/68j;->v:Z

    if-nez v0, :cond_1f

    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, LX/68j;->v:Z

    .line 1056282
    iget-object v0, p0, LX/68j;->a:LX/68G;

    iget v1, p0, LX/68j;->m:F

    iget v2, p0, LX/68j;->n:F

    invoke-interface {v0, v1, v2}, LX/68G;->b(FF)V

    goto/16 :goto_3

    .line 1056283
    :cond_1a
    iget-boolean v0, p0, LX/68j;->v:Z

    if-eqz v0, :cond_1b

    iget-wide v0, p0, LX/68j;->u:J

    sub-long v0, v2, v0

    iget v4, p0, LX/68j;->w:I

    int-to-long v8, v4

    cmp-long v0, v0, v8

    if-gez v0, :cond_1b

    iget v0, p0, LX/68j;->s:F

    sub-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LX/68j;->x:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1b

    iget v0, p0, LX/68j;->t:F

    sub-float v0, v6, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, LX/68j;->x:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1b

    .line 1056284
    const-string v0, "clickTimeout"

    invoke-static {v0}, LX/31l;->b(Ljava/lang/String;)V

    .line 1056285
    iget-object v0, p0, LX/68j;->a:LX/68G;

    iget v1, p0, LX/68j;->s:F

    iget v2, p0, LX/68j;->t:F

    invoke-interface {v0, v1, v2}, LX/68G;->e(FF)V

    goto :goto_7

    .line 1056286
    :cond_1b
    iget-boolean v0, p0, LX/68j;->i:Z

    if-nez v0, :cond_1c

    .line 1056287
    iget-object v0, p0, LX/68j;->H:Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;

    const-string v1, "clickTimeout"

    iget v2, p0, LX/68j;->w:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, LX/31l;->a(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;Ljava/lang/String;J)V

    goto :goto_7

    .line 1056288
    :cond_1c
    iget-wide v0, p0, LX/68j;->d:J

    sub-long v0, v2, v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1e

    .line 1056289
    iget-boolean v0, p0, LX/68j;->z:Z

    if-eqz v0, :cond_1d

    .line 1056290
    iget-object v0, p0, LX/68j;->a:LX/68G;

    invoke-interface {v0}, LX/68G;->g()V

    .line 1056291
    :cond_1d
    iget-boolean v0, p0, LX/68j;->l:Z

    if-eqz v0, :cond_1e

    .line 1056292
    iget-object v0, p0, LX/68j;->a:LX/68G;

    invoke-interface {v0}, LX/68G;->f()V

    .line 1056293
    :cond_1e
    iget-boolean v0, p0, LX/68j;->i:Z

    if-eqz v0, :cond_19

    .line 1056294
    iget-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1056295
    iget-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 1056296
    iget-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    iget-object v1, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    .line 1056297
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v3, p0, LX/68j;->C:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_19

    .line 1056298
    iget-object v2, p0, LX/68j;->a:LX/68G;

    iget v3, p0, LX/68j;->E:F

    mul-float/2addr v0, v3

    iget v3, p0, LX/68j;->E:F

    mul-float/2addr v1, v3

    invoke-interface {v2, v0, v1}, LX/68G;->f(FF)V

    goto/16 :goto_7

    .line 1056299
    :cond_1f
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 1056300
    :cond_20
    const/4 v1, 0x5

    if-ne v0, v1, :cond_21

    .line 1056301
    iput-wide v2, p0, LX/68j;->p:J

    goto/16 :goto_3

    .line 1056302
    :cond_21
    const/4 v1, 0x6

    if-ne v0, v1, :cond_22

    .line 1056303
    const/4 v0, 0x2

    if-ne v4, v0, :cond_7

    .line 1056304
    iput-wide v2, p0, LX/68j;->d:J

    .line 1056305
    iget-boolean v0, p0, LX/68j;->D:Z

    if-nez v0, :cond_7

    .line 1056306
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68j;->l:Z

    .line 1056307
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/68j;->z:Z

    goto/16 :goto_3

    .line 1056308
    :cond_22
    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 1056309
    const/4 v0, 0x0

    iput v0, p0, LX/68j;->c:I

    .line 1056310
    iget-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_7

    .line 1056311
    iget-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 1056312
    const/4 v0, 0x0

    iput-object v0, p0, LX/68j;->B:Landroid/view/VelocityTracker;

    goto/16 :goto_3
.end method
