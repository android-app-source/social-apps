.class public final LX/6WQ;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/6WS;

.field private b:LX/5OK;

.field private c:Z

.field private d:F

.field private e:F


# direct methods
.method public constructor <init>(LX/6WS;)V
    .locals 0

    .prologue
    .line 1105941
    iput-object p1, p0, LX/6WQ;->a:LX/6WS;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 14

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 1105942
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1105943
    iget-object v1, p0, LX/6WQ;->b:LX/5OK;

    invoke-virtual {v1}, LX/5OK;->getFirstVisiblePosition()I

    move-result v12

    .line 1105944
    iget-object v1, p0, LX/6WQ;->b:LX/5OK;

    invoke-virtual {v1}, LX/5OK;->getLastVisiblePosition()I

    move-result v11

    .line 1105945
    sub-int v1, v11, v12

    add-int/lit8 v1, v1, 0x1

    .line 1105946
    if-nez v1, :cond_1

    .line 1105947
    :cond_0
    :goto_0
    return-void

    .line 1105948
    :cond_1
    float-to-double v2, v0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpg-double v2, v2, v4

    if-gez v2, :cond_2

    move v0, v12

    .line 1105949
    :goto_1
    if-gt v0, v11, :cond_0

    .line 1105950
    iget-object v1, p0, LX/6WQ;->b:LX/5OK;

    sub-int v2, v0, v12

    invoke-virtual {v1, v2}, LX/5OK;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1105951
    invoke-virtual {v1, v6}, Landroid/view/View;->setAlpha(F)V

    .line 1105952
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1105953
    :cond_2
    iget v2, p0, LX/6WQ;->d:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_3

    .line 1105954
    iput v0, p0, LX/6WQ;->d:F

    .line 1105955
    invoke-virtual {p1}, LX/0wd;->e()D

    move-result-wide v2

    iget v4, p0, LX/6WQ;->d:F

    float-to-double v4, v4

    sub-double/2addr v2, v4

    double-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    iput v1, p0, LX/6WQ;->e:F

    .line 1105956
    :cond_3
    iget v1, p0, LX/6WQ;->d:F

    sub-float/2addr v0, v1

    .line 1105957
    iget v1, p0, LX/6WQ;->e:F

    div-float v1, v0, v1

    float-to-int v10, v1

    .line 1105958
    float-to-double v0, v0

    int-to-float v2, v10

    iget v3, p0, LX/6WQ;->e:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    add-int/lit8 v4, v10, 0x1

    int-to-float v4, v4

    iget v5, p0, LX/6WQ;->e:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    const-wide/16 v6, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-float v2, v0

    .line 1105959
    iget-boolean v0, p0, LX/6WQ;->c:Z

    if-eqz v0, :cond_7

    move v0, v12

    .line 1105960
    :goto_2
    if-ge v0, v10, :cond_5

    .line 1105961
    iget-object v1, p0, LX/6WQ;->b:LX/5OK;

    sub-int v3, v0, v12

    invoke-virtual {v1, v3}, LX/5OK;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1105962
    if-eqz v1, :cond_4

    .line 1105963
    invoke-virtual {v1, v13}, Landroid/view/View;->setAlpha(F)V

    .line 1105964
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move v0, v10

    .line 1105965
    :cond_6
    iget-object v1, p0, LX/6WQ;->b:LX/5OK;

    invoke-virtual {v1, v0}, LX/5OK;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1105966
    if-eqz v0, :cond_0

    .line 1105967
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 1105968
    :cond_7
    sub-int v0, v11, v10

    move v1, v11

    .line 1105969
    :goto_3
    if-le v1, v0, :cond_6

    .line 1105970
    iget-object v3, p0, LX/6WQ;->b:LX/5OK;

    sub-int v4, v1, v12

    invoke-virtual {v3, v4}, LX/5OK;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1105971
    if-eqz v3, :cond_8

    .line 1105972
    invoke-virtual {v3, v13}, Landroid/view/View;->setAlpha(F)V

    .line 1105973
    :cond_8
    add-int/lit8 v1, v1, -0x1

    goto :goto_3
.end method

.method public final b(LX/0wd;)V
    .locals 5

    .prologue
    .line 1105974
    iget-object v1, p0, LX/6WQ;->b:LX/5OK;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 1105975
    iget-object v0, p0, LX/6WQ;->b:LX/5OK;

    invoke-virtual {v0}, LX/5OK;->getFirstVisiblePosition()I

    move-result v1

    .line 1105976
    iget-object v0, p0, LX/6WQ;->b:LX/5OK;

    invoke-virtual {v0}, LX/5OK;->getLastVisiblePosition()I

    move-result v2

    .line 1105977
    sub-int v0, v2, v1

    add-int/lit8 v0, v0, 0x1

    .line 1105978
    if-eqz v0, :cond_1

    move v0, v1

    .line 1105979
    :goto_0
    if-gt v0, v2, :cond_1

    .line 1105980
    iget-object v3, p0, LX/6WQ;->b:LX/5OK;

    sub-int v4, v0, v1

    invoke-virtual {v3, v4}, LX/5OK;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1105981
    if-eqz v3, :cond_0

    .line 1105982
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 1105983
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1105984
    :cond_1
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1105985
    iget-object v0, p0, LX/6WQ;->a:LX/6WS;

    iget-object v0, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/5OK;

    iput-object v0, p0, LX/6WQ;->b:LX/5OK;

    .line 1105986
    iget-object v0, p0, LX/6WQ;->a:LX/6WS;

    iget-object v0, v0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1105987
    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    and-int/lit8 v0, v0, 0x70

    const/16 v3, 0x50

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/6WQ;->c:Z

    .line 1105988
    iget-wide v8, p1, LX/0wd;->i:D

    move-wide v4, v8

    .line 1105989
    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-nez v0, :cond_0

    .line 1105990
    iget-boolean v0, p0, LX/6WQ;->c:Z

    if-nez v0, :cond_2

    :goto_1
    iput-boolean v1, p0, LX/6WQ;->c:Z

    .line 1105991
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/6WQ;->d:F

    .line 1105992
    iget-object v1, p0, LX/6WQ;->b:LX/5OK;

    invoke-static {v1}, LX/0wc;->a(Landroid/view/View;)V

    .line 1105993
    return-void

    :cond_1
    move v0, v2

    .line 1105994
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1105995
    goto :goto_1
.end method
