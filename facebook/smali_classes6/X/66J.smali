.class public final LX/66J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/66G;


# static fields
.field private static final a:LX/673;

.field private static final b:LX/673;

.field private static final c:LX/673;

.field private static final d:LX/673;

.field private static final e:LX/673;

.field private static final f:LX/673;

.field private static final g:LX/673;

.field private static final h:LX/673;

.field public static final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/673;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/673;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/673;",
            ">;"
        }
    .end annotation
.end field

.field private static final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/673;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final m:LX/64w;

.field public final n:LX/65W;

.field private final o:LX/65c;

.field private p:LX/65i;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1049947
    const-string v0, "connection"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/66J;->a:LX/673;

    .line 1049948
    const-string v0, "host"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/66J;->b:LX/673;

    .line 1049949
    const-string v0, "keep-alive"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/66J;->c:LX/673;

    .line 1049950
    const-string v0, "proxy-connection"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/66J;->d:LX/673;

    .line 1049951
    const-string v0, "transfer-encoding"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/66J;->e:LX/673;

    .line 1049952
    const-string v0, "te"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/66J;->f:LX/673;

    .line 1049953
    const-string v0, "encoding"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/66J;->g:LX/673;

    .line 1049954
    const-string v0, "upgrade"

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v0

    sput-object v0, LX/66J;->h:LX/673;

    .line 1049955
    const/16 v0, 0xb

    new-array v0, v0, [LX/673;

    sget-object v1, LX/66J;->a:LX/673;

    aput-object v1, v0, v3

    sget-object v1, LX/66J;->b:LX/673;

    aput-object v1, v0, v4

    sget-object v1, LX/66J;->c:LX/673;

    aput-object v1, v0, v5

    sget-object v1, LX/66J;->d:LX/673;

    aput-object v1, v0, v6

    sget-object v1, LX/66J;->e:LX/673;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/65j;->b:LX/673;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/65j;->c:LX/673;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/65j;->d:LX/673;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/65j;->e:LX/673;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/65j;->f:LX/673;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/65j;->g:LX/673;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/66J;->i:Ljava/util/List;

    .line 1049956
    const/4 v0, 0x5

    new-array v0, v0, [LX/673;

    sget-object v1, LX/66J;->a:LX/673;

    aput-object v1, v0, v3

    sget-object v1, LX/66J;->b:LX/673;

    aput-object v1, v0, v4

    sget-object v1, LX/66J;->c:LX/673;

    aput-object v1, v0, v5

    sget-object v1, LX/66J;->d:LX/673;

    aput-object v1, v0, v6

    sget-object v1, LX/66J;->e:LX/673;

    aput-object v1, v0, v7

    invoke-static {v0}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/66J;->j:Ljava/util/List;

    .line 1049957
    const/16 v0, 0xe

    new-array v0, v0, [LX/673;

    sget-object v1, LX/66J;->a:LX/673;

    aput-object v1, v0, v3

    sget-object v1, LX/66J;->b:LX/673;

    aput-object v1, v0, v4

    sget-object v1, LX/66J;->c:LX/673;

    aput-object v1, v0, v5

    sget-object v1, LX/66J;->d:LX/673;

    aput-object v1, v0, v6

    sget-object v1, LX/66J;->f:LX/673;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/66J;->e:LX/673;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/66J;->g:LX/673;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/66J;->h:LX/673;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/65j;->b:LX/673;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/65j;->c:LX/673;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/65j;->d:LX/673;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/65j;->e:LX/673;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/65j;->f:LX/673;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/65j;->g:LX/673;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/66J;->k:Ljava/util/List;

    .line 1049958
    const/16 v0, 0x8

    new-array v0, v0, [LX/673;

    sget-object v1, LX/66J;->a:LX/673;

    aput-object v1, v0, v3

    sget-object v1, LX/66J;->b:LX/673;

    aput-object v1, v0, v4

    sget-object v1, LX/66J;->c:LX/673;

    aput-object v1, v0, v5

    sget-object v1, LX/66J;->d:LX/673;

    aput-object v1, v0, v6

    sget-object v1, LX/66J;->f:LX/673;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/66J;->e:LX/673;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/66J;->g:LX/673;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/66J;->h:LX/673;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/65A;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/66J;->l:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(LX/64w;LX/65W;LX/65c;)V
    .locals 0

    .prologue
    .line 1049798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1049799
    iput-object p1, p0, LX/66J;->m:LX/64w;

    .line 1049800
    iput-object p2, p0, LX/66J;->n:LX/65W;

    .line 1049801
    iput-object p3, p0, LX/66J;->o:LX/65c;

    .line 1049802
    return-void
.end method

.method private static a(Ljava/util/List;)LX/654;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)",
            "LX/654;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1049915
    const/4 v2, 0x0

    .line 1049916
    const-string v1, "HTTP/1.1"

    .line 1049917
    new-instance v6, LX/64m;

    invoke-direct {v6}, LX/64m;-><init>()V

    .line 1049918
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v7

    move v5, v3

    :goto_0
    if-ge v5, v7, :cond_5

    .line 1049919
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65j;

    iget-object v8, v0, LX/65j;->h:LX/673;

    .line 1049920
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65j;

    iget-object v0, v0, LX/65j;->i:LX/673;

    invoke-virtual {v0}, LX/673;->a()Ljava/lang/String;

    move-result-object v9

    move-object v0, v1

    move v1, v3

    .line 1049921
    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 1049922
    invoke-virtual {v9, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 1049923
    const/4 v10, -0x1

    if-ne v4, v10, :cond_0

    .line 1049924
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v4

    .line 1049925
    :cond_0
    invoke-virtual {v9, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1049926
    sget-object v10, LX/65j;->a:LX/673;

    invoke-virtual {v8, v10}, LX/673;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1049927
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v12, v2

    move-object v2, v1

    move v1, v12

    .line 1049928
    goto :goto_1

    .line 1049929
    :cond_1
    sget-object v10, LX/65j;->g:LX/673;

    invoke-virtual {v8, v10}, LX/673;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    move-object v0, v1

    move-object v1, v2

    .line 1049930
    goto :goto_2

    .line 1049931
    :cond_2
    sget-object v10, LX/66J;->j:Ljava/util/List;

    invoke-interface {v10, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 1049932
    sget-object v10, LX/64t;->a:LX/64t;

    invoke-virtual {v8}, LX/673;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v6, v11, v1}, LX/64t;->a(LX/64m;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v1, v2

    goto :goto_2

    .line 1049933
    :cond_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v1, v0

    goto :goto_0

    .line 1049934
    :cond_5
    if-nez v2, :cond_6

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Expected \':status\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049935
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/66S;->a(Ljava/lang/String;)LX/66S;

    move-result-object v0

    .line 1049936
    new-instance v1, LX/654;

    invoke-direct {v1}, LX/654;-><init>()V

    sget-object v2, LX/64x;->SPDY_3:LX/64x;

    .line 1049937
    iput-object v2, v1, LX/654;->b:LX/64x;

    .line 1049938
    move-object v1, v1

    .line 1049939
    iget v2, v0, LX/66S;->b:I

    .line 1049940
    iput v2, v1, LX/654;->c:I

    .line 1049941
    move-object v1, v1

    .line 1049942
    iget-object v0, v0, LX/66S;->c:Ljava/lang/String;

    .line 1049943
    iput-object v0, v1, LX/654;->d:Ljava/lang/String;

    .line 1049944
    move-object v0, v1

    .line 1049945
    invoke-virtual {v6}, LX/64m;->a()LX/64n;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/654;->a(LX/64n;)LX/654;

    move-result-object v0

    .line 1049946
    return-object v0
.end method

.method private static b(Ljava/util/List;)LX/654;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/65j;",
            ">;)",
            "LX/654;"
        }
    .end annotation

    .prologue
    .line 1049893
    const/4 v1, 0x0

    .line 1049894
    new-instance v3, LX/64m;

    invoke-direct {v3}, LX/64m;-><init>()V

    .line 1049895
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    .line 1049896
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65j;

    iget-object v5, v0, LX/65j;->h:LX/673;

    .line 1049897
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65j;

    iget-object v0, v0, LX/65j;->i:LX/673;

    invoke-virtual {v0}, LX/673;->a()Ljava/lang/String;

    move-result-object v0

    .line 1049898
    sget-object v6, LX/65j;->a:LX/673;

    invoke-virtual {v5, v6}, LX/673;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1049899
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 1049900
    :cond_0
    sget-object v6, LX/66J;->l:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1049901
    sget-object v6, LX/64t;->a:LX/64t;

    invoke-virtual {v5}, LX/673;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v3, v5, v0}, LX/64t;->a(LX/64m;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 1049902
    :cond_2
    if-nez v1, :cond_3

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Expected \':status\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1049903
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "HTTP/1.1 "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/66S;->a(Ljava/lang/String;)LX/66S;

    move-result-object v0

    .line 1049904
    new-instance v1, LX/654;

    invoke-direct {v1}, LX/654;-><init>()V

    sget-object v2, LX/64x;->HTTP_2:LX/64x;

    .line 1049905
    iput-object v2, v1, LX/654;->b:LX/64x;

    .line 1049906
    move-object v1, v1

    .line 1049907
    iget v2, v0, LX/66S;->b:I

    .line 1049908
    iput v2, v1, LX/654;->c:I

    .line 1049909
    move-object v1, v1

    .line 1049910
    iget-object v0, v0, LX/66S;->c:Ljava/lang/String;

    .line 1049911
    iput-object v0, v1, LX/654;->d:Ljava/lang/String;

    .line 1049912
    move-object v0, v1

    .line 1049913
    invoke-virtual {v3}, LX/64m;->a()LX/64n;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/654;->a(LX/64n;)LX/654;

    move-result-object v0

    .line 1049914
    return-object v0
.end method


# virtual methods
.method public final a(LX/655;)LX/656;
    .locals 3

    .prologue
    .line 1049887
    new-instance v0, LX/66I;

    iget-object v1, p0, LX/66J;->p:LX/65i;

    .line 1049888
    iget-object v2, v1, LX/65i;->i:LX/65e;

    move-object v1, v2

    .line 1049889
    invoke-direct {v0, p0, v1}, LX/66I;-><init>(LX/66J;LX/65D;)V

    .line 1049890
    new-instance v1, LX/66P;

    .line 1049891
    iget-object v2, p1, LX/655;->f:LX/64n;

    move-object v2, v2

    .line 1049892
    invoke-static {v0}, LX/67B;->a(LX/65D;)LX/671;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/66P;-><init>(LX/64n;LX/671;)V

    return-object v1
.end method

.method public final a(LX/650;J)LX/65J;
    .locals 1

    .prologue
    .line 1049959
    iget-object v0, p0, LX/66J;->p:LX/65i;

    invoke-virtual {v0}, LX/65i;->h()LX/65J;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1049885
    iget-object v0, p0, LX/66J;->p:LX/65i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/66J;->p:LX/65i;

    sget-object v1, LX/65X;->CANCEL:LX/65X;

    invoke-virtual {v0, v1}, LX/65i;->b(LX/65X;)V

    .line 1049886
    :cond_0
    return-void
.end method

.method public final a(LX/650;)V
    .locals 12

    .prologue
    .line 1049810
    iget-object v0, p0, LX/66J;->p:LX/65i;

    if-eqz v0, :cond_0

    .line 1049811
    :goto_0
    return-void

    .line 1049812
    :cond_0
    iget-object v0, p1, LX/650;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1049813
    invoke-static {v0}, LX/66N;->b(Ljava/lang/String;)Z

    move-result v1

    .line 1049814
    iget-object v0, p0, LX/66J;->o:LX/65c;

    .line 1049815
    iget-object v2, v0, LX/65c;->a:LX/64x;

    move-object v0, v2

    .line 1049816
    sget-object v2, LX/64x;->HTTP_2:LX/64x;

    if-ne v0, v2, :cond_3

    .line 1049817
    const/4 v0, 0x0

    .line 1049818
    iget-object v2, p1, LX/650;->c:LX/64n;

    move-object v2, v2

    .line 1049819
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, LX/64n;->a()I

    move-result v4

    add-int/lit8 v4, v4, 0x4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1049820
    new-instance v4, LX/65j;

    sget-object v5, LX/65j;->b:LX/673;

    .line 1049821
    iget-object v6, p1, LX/650;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1049822
    invoke-direct {v4, v5, v6}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049823
    new-instance v4, LX/65j;

    sget-object v5, LX/65j;->c:LX/673;

    .line 1049824
    iget-object v6, p1, LX/650;->a:LX/64q;

    move-object v6, v6

    .line 1049825
    invoke-static {v6}, LX/66Q;->a(LX/64q;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049826
    new-instance v4, LX/65j;

    sget-object v5, LX/65j;->e:LX/673;

    .line 1049827
    iget-object v6, p1, LX/650;->a:LX/64q;

    move-object v6, v6

    .line 1049828
    invoke-static {v6, v0}, LX/65A;->a(LX/64q;Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049829
    new-instance v4, LX/65j;

    sget-object v5, LX/65j;->d:LX/673;

    .line 1049830
    iget-object v6, p1, LX/650;->a:LX/64q;

    move-object v6, v6

    .line 1049831
    iget-object v7, v6, LX/64q;->b:Ljava/lang/String;

    move-object v6, v7

    .line 1049832
    invoke-direct {v4, v5, v6}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049833
    invoke-virtual {v2}, LX/64n;->a()I

    move-result v4

    :goto_1
    if-ge v0, v4, :cond_2

    .line 1049834
    invoke-virtual {v2, v0}, LX/64n;->a(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v5

    .line 1049835
    sget-object v6, LX/66J;->k:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1049836
    new-instance v6, LX/65j;

    invoke-virtual {v2, v0}, LX/64n;->b(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v5, v7}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049837
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1049838
    :cond_2
    move-object v0, v3

    .line 1049839
    :goto_2
    iget-object v2, p0, LX/66J;->o:LX/65c;

    const/4 v3, 0x1

    .line 1049840
    const/4 v4, 0x0

    invoke-static {v2, v4, v0, v1, v3}, LX/65c;->a(LX/65c;ILjava/util/List;ZZ)LX/65i;

    move-result-object v4

    move-object v0, v4

    .line 1049841
    iput-object v0, p0, LX/66J;->p:LX/65i;

    .line 1049842
    iget-object v0, p0, LX/66J;->p:LX/65i;

    .line 1049843
    iget-object v1, v0, LX/65i;->j:LX/65h;

    move-object v0, v1

    .line 1049844
    iget-object v1, p0, LX/66J;->m:LX/64w;

    .line 1049845
    iget v2, v1, LX/64w;->x:I

    move v1, v2

    .line 1049846
    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/65f;->a(JLjava/util/concurrent/TimeUnit;)LX/65f;

    .line 1049847
    iget-object v0, p0, LX/66J;->p:LX/65i;

    .line 1049848
    iget-object v1, v0, LX/65i;->k:LX/65h;

    move-object v0, v1

    .line 1049849
    iget-object v1, p0, LX/66J;->m:LX/64w;

    .line 1049850
    iget v2, v1, LX/64w;->y:I

    move v1, v2

    .line 1049851
    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/65f;->a(JLjava/util/concurrent/TimeUnit;)LX/65f;

    goto/16 :goto_0

    .line 1049852
    :cond_3
    const/4 v3, 0x0

    .line 1049853
    iget-object v0, p1, LX/650;->c:LX/64n;

    move-object v5, v0

    .line 1049854
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v5}, LX/64n;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1049855
    new-instance v0, LX/65j;

    sget-object v2, LX/65j;->b:LX/673;

    .line 1049856
    iget-object v4, p1, LX/650;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1049857
    invoke-direct {v0, v2, v4}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049858
    new-instance v0, LX/65j;

    sget-object v2, LX/65j;->c:LX/673;

    .line 1049859
    iget-object v4, p1, LX/650;->a:LX/64q;

    move-object v4, v4

    .line 1049860
    invoke-static {v4}, LX/66Q;->a(LX/64q;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049861
    new-instance v0, LX/65j;

    sget-object v2, LX/65j;->g:LX/673;

    const-string v4, "HTTP/1.1"

    invoke-direct {v0, v2, v4}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049862
    new-instance v0, LX/65j;

    sget-object v2, LX/65j;->f:LX/673;

    .line 1049863
    iget-object v4, p1, LX/650;->a:LX/64q;

    move-object v4, v4

    .line 1049864
    invoke-static {v4, v3}, LX/65A;->a(LX/64q;Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049865
    new-instance v0, LX/65j;

    sget-object v2, LX/65j;->d:LX/673;

    .line 1049866
    iget-object v4, p1, LX/650;->a:LX/64q;

    move-object v4, v4

    .line 1049867
    iget-object v7, v4, LX/64q;->b:Ljava/lang/String;

    move-object v4, v7

    .line 1049868
    invoke-direct {v0, v2, v4}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049869
    new-instance v7, Ljava/util/LinkedHashSet;

    invoke-direct {v7}, Ljava/util/LinkedHashSet;-><init>()V

    .line 1049870
    invoke-virtual {v5}, LX/64n;->a()I

    move-result v8

    move v4, v3

    :goto_3
    if-ge v4, v8, :cond_7

    .line 1049871
    invoke-virtual {v5, v4}, LX/64n;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/673;->a(Ljava/lang/String;)LX/673;

    move-result-object v9

    .line 1049872
    sget-object v0, LX/66J;->i:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1049873
    invoke-virtual {v5, v4}, LX/64n;->b(I)Ljava/lang/String;

    move-result-object v10

    .line 1049874
    invoke-interface {v7, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1049875
    new-instance v0, LX/65j;

    invoke-direct {v0, v9, v10}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1049876
    :cond_4
    :goto_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_5
    move v2, v3

    .line 1049877
    :goto_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1049878
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65j;

    iget-object v0, v0, LX/65j;->h:LX/673;

    invoke-virtual {v0, v9}, LX/673;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1049879
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/65j;

    iget-object v0, v0, LX/65j;->i:LX/673;

    invoke-virtual {v0}, LX/673;->a()Ljava/lang/String;

    move-result-object v0

    .line 1049880
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object v0, v11

    .line 1049881
    new-instance v10, LX/65j;

    invoke-direct {v10, v9, v0}, LX/65j;-><init>(LX/673;Ljava/lang/String;)V

    invoke-interface {v6, v2, v10}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1049882
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 1049883
    :cond_7
    move-object v0, v6

    .line 1049884
    goto/16 :goto_2
.end method

.method public final b()LX/654;
    .locals 2

    .prologue
    .line 1049805
    iget-object v0, p0, LX/66J;->o:LX/65c;

    .line 1049806
    iget-object v1, v0, LX/65c;->a:LX/64x;

    move-object v0, v1

    .line 1049807
    sget-object v1, LX/64x;->HTTP_2:LX/64x;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/66J;->p:LX/65i;

    .line 1049808
    invoke-virtual {v0}, LX/65i;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/66J;->b(Ljava/util/List;)LX/654;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/66J;->p:LX/65i;

    .line 1049809
    invoke-virtual {v0}, LX/65i;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/66J;->a(Ljava/util/List;)LX/654;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1049803
    iget-object v0, p0, LX/66J;->p:LX/65i;

    invoke-virtual {v0}, LX/65i;->h()LX/65J;

    move-result-object v0

    invoke-interface {v0}, LX/65J;->close()V

    .line 1049804
    return-void
.end method
