.class public final LX/5Kf;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/5Kg;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/java2js/JSValue;

.field public b:LX/5KI;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 898917
    invoke-static {}, LX/5Kg;->q()LX/5Kg;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 898918
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 898919
    const-string v0, "CSFlexbox"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 898903
    if-ne p0, p1, :cond_1

    .line 898904
    :cond_0
    :goto_0
    return v0

    .line 898905
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 898906
    goto :goto_0

    .line 898907
    :cond_3
    check-cast p1, LX/5Kf;

    .line 898908
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 898909
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 898910
    if-eq v2, v3, :cond_0

    .line 898911
    iget-object v2, p0, LX/5Kf;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/5Kf;->a:Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, LX/5Kf;->a:Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 898912
    goto :goto_0

    .line 898913
    :cond_5
    iget-object v2, p1, LX/5Kf;->a:Lcom/facebook/java2js/JSValue;

    if-nez v2, :cond_4

    .line 898914
    :cond_6
    iget-object v2, p0, LX/5Kf;->b:LX/5KI;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/5Kf;->b:LX/5KI;

    iget-object v3, p1, LX/5Kf;->b:LX/5KI;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 898915
    goto :goto_0

    .line 898916
    :cond_7
    iget-object v2, p1, LX/5Kf;->b:LX/5KI;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
