.class public final LX/6En;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1066615
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1066616
    iput-object p1, p0, LX/6En;->a:Landroid/content/Context;

    .line 1066617
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1066618
    const-string v0, "app_id"

    iget-object v1, p0, LX/6En;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1066619
    const-string v0, "app_name"

    iget-object v1, p0, LX/6En;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1066620
    const-string v0, "autofill_setting_enabled"

    iget-boolean v1, p0, LX/6En;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1066621
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1066622
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/6En;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/browserextensions/common/menuitems/BrowserExtensionsManageSettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1066623
    invoke-direct {p0, v0}, LX/6En;->a(Landroid/content/Intent;)V

    .line 1066624
    return-object v0
.end method
