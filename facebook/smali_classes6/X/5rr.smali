.class public abstract LX/5rr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final e:[Ljava/lang/Object;

.field private static final f:[Ljava/lang/Object;

.field private static final g:[Ljava/lang/Object;

.field private static final h:[Ljava/lang/Object;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/reflect/Method;

.field public final d:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1012191
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, LX/5rr;->e:[Ljava/lang/Object;

    .line 1012192
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, LX/5rr;->f:[Ljava/lang/Object;

    .line 1012193
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, LX/5rr;->g:[Ljava/lang/Object;

    .line 1012194
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, LX/5rr;->h:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/react/uimanager/annotations/ReactProp;Ljava/lang/String;Ljava/lang/reflect/Method;)V
    .locals 2

    .prologue
    .line 1012195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1012196
    invoke-interface {p1}, Lcom/facebook/react/uimanager/annotations/ReactProp;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/5rr;->a:Ljava/lang/String;

    .line 1012197
    const-string v0, "__default_type__"

    invoke-interface {p1}, Lcom/facebook/react/uimanager/annotations/ReactProp;->customType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iput-object p2, p0, LX/5rr;->b:Ljava/lang/String;

    .line 1012198
    iput-object p3, p0, LX/5rr;->c:Ljava/lang/reflect/Method;

    .line 1012199
    const/4 v0, 0x0

    iput-object v0, p0, LX/5rr;->d:Ljava/lang/Integer;

    .line 1012200
    return-void

    .line 1012201
    :cond_0
    invoke-interface {p1}, Lcom/facebook/react/uimanager/annotations/ReactProp;->customType()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public synthetic constructor <init>(Lcom/facebook/react/uimanager/annotations/ReactProp;Ljava/lang/String;Ljava/lang/reflect/Method;B)V
    .locals 0

    .prologue
    .line 1012202
    invoke-direct {p0, p1, p2, p3}, LX/5rr;-><init>(Lcom/facebook/react/uimanager/annotations/ReactProp;Ljava/lang/String;Ljava/lang/reflect/Method;)V

    return-void
.end method

.method public constructor <init>(Lcom/facebook/react/uimanager/annotations/ReactPropGroup;Ljava/lang/String;Ljava/lang/reflect/Method;I)V
    .locals 2

    .prologue
    .line 1012203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1012204
    invoke-interface {p1}, Lcom/facebook/react/uimanager/annotations/ReactPropGroup;->a()[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p4

    iput-object v0, p0, LX/5rr;->a:Ljava/lang/String;

    .line 1012205
    const-string v0, "__default_type__"

    invoke-interface {p1}, Lcom/facebook/react/uimanager/annotations/ReactPropGroup;->customType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iput-object p2, p0, LX/5rr;->b:Ljava/lang/String;

    .line 1012206
    iput-object p3, p0, LX/5rr;->c:Ljava/lang/reflect/Method;

    .line 1012207
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/5rr;->d:Ljava/lang/Integer;

    .line 1012208
    return-void

    .line 1012209
    :cond_0
    invoke-interface {p1}, Lcom/facebook/react/uimanager/annotations/ReactPropGroup;->customType()Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public synthetic constructor <init>(Lcom/facebook/react/uimanager/annotations/ReactPropGroup;Ljava/lang/String;Ljava/lang/reflect/Method;IB)V
    .locals 0

    .prologue
    .line 1012210
    invoke-direct {p0, p1, p2, p3, p4}, LX/5rr;-><init>(Lcom/facebook/react/uimanager/annotations/ReactPropGroup;Ljava/lang/String;Ljava/lang/reflect/Method;I)V

    return-void
.end method


# virtual methods
.method public abstract a(LX/5rC;)Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5rC;)V
    .locals 4

    .prologue
    .line 1012211
    :try_start_0
    iget-object v0, p0, LX/5rr;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 1012212
    sget-object v0, LX/5rr;->g:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0, p2}, LX/5rr;->a(LX/5rC;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1012213
    iget-object v0, p0, LX/5rr;->c:Ljava/lang/reflect/Method;

    sget-object v1, LX/5rr;->g:[Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1012214
    sget-object v0, LX/5rr;->g:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1012215
    :goto_0
    return-void

    .line 1012216
    :cond_0
    sget-object v0, LX/5rr;->h:[Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/5rr;->d:Ljava/lang/Integer;

    aput-object v2, v0, v1

    .line 1012217
    sget-object v0, LX/5rr;->h:[Ljava/lang/Object;

    const/4 v1, 0x1

    invoke-virtual {p0, p2}, LX/5rr;->a(LX/5rC;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1012218
    iget-object v0, p0, LX/5rr;->c:Ljava/lang/reflect/Method;

    sget-object v1, LX/5rr;->h:[Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1012219
    sget-object v0, LX/5rr;->h:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1012220
    :catch_0
    move-exception v0

    .line 1012221
    const-class v1, Lcom/facebook/react/uimanager/ViewManager;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error while updating prop "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/5rr;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1012222
    new-instance v1, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error while updating property \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/5rr;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' in shadow node of type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/5pA;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/facebook/react/uimanager/ViewManager;Landroid/view/View;LX/5rC;)V
    .locals 4

    .prologue
    .line 1012223
    :try_start_0
    iget-object v0, p0, LX/5rr;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 1012224
    sget-object v0, LX/5rr;->e:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 1012225
    sget-object v0, LX/5rr;->e:[Ljava/lang/Object;

    const/4 v1, 0x1

    invoke-virtual {p0, p3}, LX/5rr;->a(LX/5rC;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1012226
    iget-object v0, p0, LX/5rr;->c:Ljava/lang/reflect/Method;

    sget-object v1, LX/5rr;->e:[Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1012227
    sget-object v0, LX/5rr;->e:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1012228
    :goto_0
    return-void

    .line 1012229
    :cond_0
    sget-object v0, LX/5rr;->f:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 1012230
    sget-object v0, LX/5rr;->f:[Ljava/lang/Object;

    const/4 v1, 0x1

    iget-object v2, p0, LX/5rr;->d:Ljava/lang/Integer;

    aput-object v2, v0, v1

    .line 1012231
    sget-object v0, LX/5rr;->f:[Ljava/lang/Object;

    const/4 v1, 0x2

    invoke-virtual {p0, p3}, LX/5rr;->a(LX/5rC;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1012232
    iget-object v0, p0, LX/5rr;->c:Ljava/lang/reflect/Method;

    sget-object v1, LX/5rr;->f:[Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1012233
    sget-object v0, LX/5rr;->f:[Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1012234
    :catch_0
    move-exception v0

    .line 1012235
    const-class v1, Lcom/facebook/react/uimanager/ViewManager;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error while updating prop "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/5rr;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1012236
    new-instance v1, LX/5pA;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error while updating property \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/5rr;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' of a view managed by: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ViewManager;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/5pA;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
