.class public final LX/683;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/67m;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1054191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1054192
    check-cast p1, LX/67m;

    check-cast p2, LX/67m;

    .line 1054193
    iget v0, p1, LX/67m;->j:I

    move v0, v0

    .line 1054194
    iget v1, p2, LX/67m;->j:I

    move v1, v1

    .line 1054195
    iget v2, p1, LX/67m;->k:F

    move v2, v2

    .line 1054196
    iget v3, p2, LX/67m;->k:F

    move v3, v3

    .line 1054197
    if-eq v0, v1, :cond_0

    .line 1054198
    sub-int/2addr v0, v1

    .line 1054199
    :goto_0
    return v0

    .line 1054200
    :cond_0
    cmpl-float v0, v2, v3

    if-eqz v0, :cond_1

    .line 1054201
    sub-float v0, v2, v3

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0

    .line 1054202
    :cond_1
    iget v0, p1, LX/67m;->b:I

    move v0, v0

    .line 1054203
    iget v1, p2, LX/67m;->b:I

    move v1, v1

    .line 1054204
    sub-int/2addr v0, v1

    goto :goto_0
.end method
