.class public final LX/5dx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5di;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5di",
        "<",
        "Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 966435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0lF;)Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;
    .locals 4

    .prologue
    .line 966436
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;

    const-string v1, "value"

    invoke-virtual {p1, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->c(LX/0lF;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;-><init>(J)V

    return-object v0
.end method

.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966437
    new-instance v0, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;

    invoke-direct {v0, p1}, Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 966438
    new-array v0, p1, [Lcom/facebook/messaging/model/messagemetadata/TimestampMetadata;

    return-object v0
.end method
