.class public final LX/6HK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# instance fields
.field public final synthetic a:LX/6HU;


# direct methods
.method public constructor <init>(LX/6HU;)V
    .locals 0

    .prologue
    .line 1071687
    iput-object p1, p0, LX/6HK;->a:LX/6HU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 3

    .prologue
    .line 1071688
    iget-object v0, p0, LX/6HK;->a:LX/6HU;

    iget-object v0, v0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1071689
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 1071690
    :cond_0
    iget-object v0, p0, LX/6HK;->a:LX/6HU;

    invoke-static {v0}, LX/6HU;->K(LX/6HU;)V

    .line 1071691
    iget-object v0, p0, LX/6HK;->a:LX/6HU;

    invoke-static {v0}, LX/6HU;->G(LX/6HU;)V

    .line 1071692
    :goto_0
    return-void

    .line 1071693
    :cond_1
    iget-object v0, p0, LX/6HK;->a:LX/6HU;

    invoke-static {v0}, LX/6HU;->D(LX/6HU;)I

    move-result v0

    .line 1071694
    iget-object v1, p0, LX/6HK;->a:LX/6HU;

    iget-object v1, v1, LX/6HU;->a:LX/6HO;

    invoke-interface {v1, p1, v0}, LX/6HO;->a([BI)V

    .line 1071695
    new-instance v0, LX/6HQ;

    iget-object v1, p0, LX/6HK;->a:LX/6HU;

    invoke-direct {v0, v1, p1}, LX/6HQ;-><init>(LX/6HU;[B)V

    .line 1071696
    iget-object v1, p0, LX/6HK;->a:LX/6HU;

    iget-object v1, v1, LX/6HU;->D:LX/0Sh;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    goto :goto_0
.end method
