.class public final LX/6Iw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1075164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075165
    return-void
.end method

.method public static a(ILandroid/hardware/camera2/CameraCharacteristics;LX/6JF;FF)[Landroid/hardware/camera2/params/MeteringRectangle;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 1075145
    sget-object v0, Landroid/hardware/camera2/CameraCharacteristics;->SENSOR_INFO_ACTIVE_ARRAY_SIZE:Landroid/hardware/camera2/CameraCharacteristics$Key;

    invoke-virtual {p1, v0}, Landroid/hardware/camera2/CameraCharacteristics;->get(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    .line 1075146
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 1075147
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1075148
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v6, v6, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v6, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v5, v4, v6}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1075149
    neg-int v5, p0

    int-to-float v5, v5

    invoke-virtual {v3, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 1075150
    sget-object v5, LX/6JF;->FRONT:LX/6JF;

    if-ne p2, v5, :cond_0

    :goto_0
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1075151
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 1075152
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    sget-object v5, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v4, v2, v5}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 1075153
    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 1075154
    const/4 v2, 0x2

    new-array v2, v2, [F

    aput p3, v2, v7

    aput p4, v2, v8

    .line 1075155
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 1075156
    aget v1, v2, v7

    float-to-int v1, v1

    .line 1075157
    aget v2, v2, v8

    float-to-int v2, v2

    .line 1075158
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v1, v2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1075159
    const/16 v1, -0x1e

    const/16 v2, -0x1e

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 1075160
    invoke-virtual {v3, v0}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1075161
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Focus region does not intersect with sensor region"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v1, v2

    .line 1075162
    goto :goto_0

    .line 1075163
    :cond_1
    new-array v0, v8, [Landroid/hardware/camera2/params/MeteringRectangle;

    new-instance v1, Landroid/hardware/camera2/params/MeteringRectangle;

    const/16 v2, 0x3e8

    invoke-direct {v1, v3, v2}, Landroid/hardware/camera2/params/MeteringRectangle;-><init>(Landroid/graphics/Rect;I)V

    aput-object v1, v0, v7

    return-object v0
.end method
