.class public abstract LX/5Jq;
.super LX/3mY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3mY",
        "<",
        "Landroid/support/v4/view/ViewPager;",
        "LX/5Jp;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field private final b:LX/5Jn;

.field private final c:LX/0hc;

.field public d:F

.field private e:I

.field public f:Landroid/support/v4/view/ViewPager;

.field public g:LX/0hc;


# direct methods
.method public constructor <init>(Landroid/content/Context;IF)V
    .locals 1

    .prologue
    .line 897804
    new-instance v0, LX/5Jp;

    invoke-direct {v0}, LX/5Jp;-><init>()V

    invoke-direct {p0, p1, v0, p2, p3}, LX/5Jq;-><init>(Landroid/content/Context;LX/5Jp;IF)V

    .line 897805
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;LX/5Jp;IF)V
    .locals 2

    .prologue
    .line 897792
    invoke-direct {p0, p1, p2}, LX/3mY;-><init>(Landroid/content/Context;LX/3me;)V

    .line 897793
    iput p3, p0, LX/5Jq;->a:I

    .line 897794
    iput p4, p0, LX/5Jq;->d:F

    .line 897795
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, LX/5Jq;->d:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, LX/5Jq;->e:I

    .line 897796
    new-instance v0, LX/5Jn;

    invoke-direct {v0, p0, p1}, LX/5Jn;-><init>(LX/5Jq;Landroid/content/Context;)V

    iput-object v0, p0, LX/5Jq;->b:LX/5Jn;

    .line 897797
    new-instance v0, LX/5Jo;

    invoke-direct {v0, p0}, LX/5Jo;-><init>(LX/5Jq;)V

    iput-object v0, p0, LX/5Jq;->c:LX/0hc;

    .line 897798
    iget-object v0, p0, LX/3mY;->i:LX/3me;

    move-object v0, v0

    .line 897799
    check-cast v0, LX/5Jp;

    iget v1, p0, LX/5Jq;->e:I

    .line 897800
    iput v1, v0, LX/5Jp;->a:I

    .line 897801
    iget-object v0, p0, LX/5Jq;->b:LX/5Jn;

    .line 897802
    iput-object v0, p0, LX/3mY;->h:LX/3mh;

    .line 897803
    return-void
.end method


# virtual methods
.method public final b(III)Z
    .locals 2

    .prologue
    .line 897788
    iget v1, p0, LX/5Jq;->a:I

    .line 897789
    iget-object v0, p0, LX/3mY;->i:LX/3me;

    move-object v0, v0

    .line 897790
    check-cast v0, LX/5Jp;

    invoke-virtual {v0}, LX/5Jp;->a()I

    move-result v0

    add-int/2addr v0, v1

    .line 897791
    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bF_()I
    .locals 3

    .prologue
    .line 897806
    const/4 v1, 0x0

    iget v2, p0, LX/5Jq;->a:I

    .line 897807
    iget-object v0, p0, LX/3mY;->i:LX/3me;

    move-object v0, v0

    .line 897808
    check-cast v0, LX/5Jp;

    invoke-virtual {v0}, LX/5Jp;->a()I

    move-result v0

    sub-int v0, v2, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 3

    .prologue
    .line 897785
    invoke-super {p0, p1}, LX/3mY;->e(I)I

    move-result v0

    .line 897786
    invoke-static {v0}, LX/1mh;->b(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, LX/5Jq;->d:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 897787
    invoke-static {v0}, LX/1mh;->a(I)I

    move-result v0

    invoke-static {v1, v0}, LX/1mh;->a(II)I

    move-result v0

    return v0
.end method

.method public final e(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 897778
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .line 897779
    iput-object p1, p0, LX/5Jq;->f:Landroid/support/v4/view/ViewPager;

    .line 897780
    iget-object v0, p0, LX/5Jq;->c:LX/0hc;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 897781
    iget-object v0, p0, LX/5Jq;->b:LX/5Jn;

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 897782
    iget v0, p0, LX/5Jq;->a:I

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 897783
    iget v0, p0, LX/5Jq;->e:I

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 897784
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 897775
    iget-object v0, p0, LX/3mY;->i:LX/3me;

    move-object v0, v0

    .line 897776
    check-cast v0, LX/5Jp;

    iget v1, p0, LX/5Jq;->a:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, LX/5Jp;->a(II)V

    .line 897777
    return-void
.end method

.method public final f(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 897770
    const/4 v1, 0x0

    .line 897771
    iget-object v0, p0, LX/5Jq;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 897772
    iget-object v0, p0, LX/5Jq;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 897773
    iput-object v1, p0, LX/5Jq;->f:Landroid/support/v4/view/ViewPager;

    .line 897774
    return-void
.end method
