.class public final LX/5Vk;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 930008
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 930009
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 930010
    :goto_0
    return v1

    .line 930011
    :cond_0
    const-string v10, "per_unit_price"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 930012
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    .line 930013
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 930014
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 930015
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 930016
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 930017
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 930018
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 930019
    :cond_2
    const-string v10, "node"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 930020
    invoke-static {p0, p1}, LX/5Vh;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 930021
    :cond_3
    const-string v10, "product_image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 930022
    invoke-static {p0, p1}, LX/5Vj;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 930023
    :cond_4
    const-string v10, "quantity"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 930024
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 930025
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 930026
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 930027
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 930028
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 930029
    if-eqz v3, :cond_7

    .line 930030
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 930031
    :cond_7
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 930032
    if-eqz v0, :cond_8

    .line 930033
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 930034
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 930035
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 930036
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 930037
    if-eqz v0, :cond_0

    .line 930038
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 930039
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 930040
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 930041
    if-eqz v0, :cond_1

    .line 930042
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 930043
    invoke-static {p0, v0, p2}, LX/5Vh;->a(LX/15i;ILX/0nX;)V

    .line 930044
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 930045
    if-eqz v0, :cond_2

    .line 930046
    const-string v1, "per_unit_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 930047
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 930048
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 930049
    if-eqz v0, :cond_3

    .line 930050
    const-string v1, "product_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 930051
    invoke-static {p0, v0, p2, p3}, LX/5Vj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 930052
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 930053
    if-eqz v0, :cond_4

    .line 930054
    const-string v1, "quantity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 930055
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 930056
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 930057
    return-void
.end method
