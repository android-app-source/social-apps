.class public LX/6PX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4VT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4VT",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "LX/4XS;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/facebook/graphql/enums/GraphQLSavedState;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V
    .locals 1

    .prologue
    .line 1086216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1086217
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/6PX;->a:Ljava/lang/String;

    .line 1086218
    iput-object p2, p0, LX/6PX;->b:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1086219
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086207
    iget-object v0, p0, LX/6PX;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/16f;LX/40U;)V
    .locals 2

    .prologue
    .line 1086210
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p2, LX/4XS;

    .line 1086211
    iget-object v0, p0, LX/6PX;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1086212
    :goto_0
    return-void

    .line 1086213
    :cond_0
    iget-object v0, p0, LX/6PX;->b:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1086214
    iget-object v1, p2, LX/40T;->a:LX/4VK;

    const-string p0, "viewer_saved_state"

    invoke-virtual {v1, p0, v0}, LX/4VK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1086215
    goto :goto_0
.end method

.method public final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1086209
    const-class v0, Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086208
    const-string v0, "SaveNodeMutatingVisitor"

    return-object v0
.end method
