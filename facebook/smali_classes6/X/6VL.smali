.class public final enum LX/6VL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/6VL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/6VL;

.field public static final enum AD:LX/6VL;

.field public static final enum OTHER_EGO:LX/6VL;

.field public static final enum PYMK:LX/6VL;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1103977
    new-instance v0, LX/6VL;

    const-string v1, "AD"

    const-string v2, "ad"

    invoke-direct {v0, v1, v3, v2}, LX/6VL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VL;->AD:LX/6VL;

    .line 1103978
    new-instance v0, LX/6VL;

    const-string v1, "PYMK"

    const-string v2, "pymk"

    invoke-direct {v0, v1, v4, v2}, LX/6VL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VL;->PYMK:LX/6VL;

    .line 1103979
    new-instance v0, LX/6VL;

    const-string v1, "OTHER_EGO"

    const-string v2, "other_ego"

    invoke-direct {v0, v1, v5, v2}, LX/6VL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/6VL;->OTHER_EGO:LX/6VL;

    .line 1103980
    const/4 v0, 0x3

    new-array v0, v0, [LX/6VL;

    sget-object v1, LX/6VL;->AD:LX/6VL;

    aput-object v1, v0, v3

    sget-object v1, LX/6VL;->PYMK:LX/6VL;

    aput-object v1, v0, v4

    sget-object v1, LX/6VL;->OTHER_EGO:LX/6VL;

    aput-object v1, v0, v5

    sput-object v0, LX/6VL;->$VALUES:[LX/6VL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1103981
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1103982
    iput-object p3, p0, LX/6VL;->value:Ljava/lang/String;

    .line 1103983
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/6VL;
    .locals 1

    .prologue
    .line 1103984
    const-class v0, LX/6VL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6VL;

    return-object v0
.end method

.method public static values()[LX/6VL;
    .locals 1

    .prologue
    .line 1103985
    sget-object v0, LX/6VL;->$VALUES:[LX/6VL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/6VL;

    return-object v0
.end method
