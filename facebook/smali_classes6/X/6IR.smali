.class public final LX/6IR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "Landroid/hardware/Camera$Size;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Ib;


# direct methods
.method public constructor <init>(LX/6Ib;)V
    .locals 0

    .prologue
    .line 1074030
    iput-object p1, p0, LX/6IR;->a:LX/6Ib;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 1074031
    iget-object v0, p0, LX/6IR;->a:LX/6Ib;

    const/16 v1, 0x8

    invoke-static {v0, v1}, LX/6Ib;->d(LX/6Ib;I)V

    .line 1074032
    iget-object v0, p0, LX/6IR;->a:LX/6Ib;

    iget-object v0, v0, LX/6Ib;->d:LX/6Ik;

    if-eqz v0, :cond_0

    .line 1074033
    iget-object v0, p0, LX/6IR;->a:LX/6Ib;

    iget-object v0, v0, LX/6Ib;->d:LX/6Ik;

    new-instance v1, LX/6JJ;

    const-string v2, "Failed to start preview"

    invoke-direct {v1, v2, p1}, LX/6JJ;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, LX/6Ik;->a(Ljava/lang/Throwable;)V

    .line 1074034
    iget-object v0, p0, LX/6IR;->a:LX/6Ib;

    const/4 v1, 0x0

    .line 1074035
    iput-object v1, v0, LX/6Ib;->d:LX/6Ik;

    .line 1074036
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1074037
    iget-object v0, p0, LX/6IR;->a:LX/6Ib;

    const/4 v1, 0x1

    .line 1074038
    iput-boolean v1, v0, LX/6Ib;->i:Z

    .line 1074039
    iget-object v0, p0, LX/6IR;->a:LX/6Ib;

    const/16 v1, 0x8

    invoke-static {v0, v1}, LX/6Ib;->c(LX/6Ib;I)V

    .line 1074040
    iget-object v0, p0, LX/6IR;->a:LX/6Ib;

    iget-object v0, v0, LX/6Ib;->d:LX/6Ik;

    if-eqz v0, :cond_0

    .line 1074041
    iget-object v0, p0, LX/6IR;->a:LX/6Ib;

    iget-object v0, v0, LX/6Ib;->d:LX/6Ik;

    invoke-interface {v0}, LX/6Ik;->b()V

    .line 1074042
    iget-object v0, p0, LX/6IR;->a:LX/6Ib;

    const/4 v1, 0x0

    .line 1074043
    iput-object v1, v0, LX/6Ib;->d:LX/6Ik;

    .line 1074044
    :cond_0
    return-void
.end method
