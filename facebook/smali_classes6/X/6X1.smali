.class public final LX/6X1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1106760
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;
    .locals 3

    .prologue
    .line 1106761
    if-nez p0, :cond_0

    .line 1106762
    const/4 v0, 0x0

    .line 1106763
    :goto_0
    return-object v0

    .line 1106764
    :cond_0
    new-instance v0, LX/4WW;

    invoke-direct {v0}, LX/4WW;-><init>()V

    .line 1106765
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0xb92b71b

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1106766
    iput-object v1, v0, LX/4WW;->F:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1106767
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v1

    .line 1106768
    iput-object v1, v0, LX/4WW;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    .line 1106769
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    .line 1106770
    iput-object v1, v0, LX/4WW;->m:Lcom/facebook/graphql/model/GraphQLUser;

    .line 1106771
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m()Ljava/lang/String;

    move-result-object v1

    .line 1106772
    iput-object v1, v0, LX/4WW;->n:Ljava/lang/String;

    .line 1106773
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106774
    iput-object v1, v0, LX/4WW;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106775
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1106776
    iput-object v1, v0, LX/4WW;->r:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1106777
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106778
    iput-object v1, v0, LX/4WW;->s:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106779
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->p()LX/0Px;

    move-result-object v1

    .line 1106780
    iput-object v1, v0, LX/4WW;->t:LX/0Px;

    .line 1106781
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106782
    iput-object v1, v0, LX/4WW;->w:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106783
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106784
    iput-object v1, v0, LX/4WW;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106785
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106786
    iput-object v1, v0, LX/4WW;->z:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106787
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->t()Ljava/lang/String;

    move-result-object v1

    .line 1106788
    iput-object v1, v0, LX/4WW;->A:Ljava/lang/String;

    .line 1106789
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106790
    iput-object v1, v0, LX/4WW;->C:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106791
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1106792
    iput-object v1, v0, LX/4WW;->D:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1106793
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->w()Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v1

    .line 1106794
    iput-object v1, v0, LX/4WW;->E:Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    .line 1106795
    new-instance v1, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;-><init>(LX/4WW;)V

    .line 1106796
    move-object v0, v1

    .line 1106797
    goto :goto_0
.end method
