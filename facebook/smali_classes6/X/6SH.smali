.class public final LX/6SH;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1091805
    const-class v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LivePinnedCommentEventCreateMutationModel;

    const v0, 0x66c0737a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "LivePinnedCommentEventCreateMutation"

    const-string v6, "22b0dda0a617f6e5de61f0132e51b99a"

    const-string v7, "pinned_comment_event_create"

    const-string v8, "0"

    const-string v9, "10155207368331729"

    const/4 v10, 0x0

    .line 1091806
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1091807
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1091808
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1091809
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1091810
    sparse-switch v0, :sswitch_data_0

    .line 1091811
    :goto_0
    return-object p1

    .line 1091812
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1091813
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1091814
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x65d81c4d -> :sswitch_2
        0x5fb57ca -> :sswitch_0
        0x69d55a61 -> :sswitch_1
    .end sparse-switch
.end method
