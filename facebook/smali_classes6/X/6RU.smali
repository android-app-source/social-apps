.class public LX/6RU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/6RU;


# instance fields
.field private a:Ljava/text/DateFormat;

.field private b:Ljava/text/DateFormat;

.field private c:Ljava/util/TimeZone;

.field private d:Ljava/text/DateFormat;

.field private e:Ljava/text/DateFormat;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1089837
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    sget-object v3, LX/6Ra;->b:Ljava/lang/String;

    sget-object v4, LX/6Ra;->a:Ljava/lang/String;

    sget-object v5, LX/6Ra;->c:Ljava/lang/String;

    move-object v0, p0

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, LX/6RU;-><init>(Ljava/util/Locale;Ljava/util/TimeZone;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1089838
    return-void
.end method

.method private constructor <init>(Ljava/util/Locale;Ljava/util/TimeZone;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1089827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1089828
    invoke-static {p3, p4, p5}, LX/6Ra;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1089829
    invoke-static {p0, p1, p2, p6}, LX/6RU;->a(LX/6RU;Ljava/util/Locale;Ljava/util/TimeZone;Landroid/content/Context;)V

    .line 1089830
    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    .line 1089831
    const-string p2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {p1, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1089832
    const-string p2, "android.intent.action.TIME_SET"

    invoke-virtual {p1, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1089833
    const-string p2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {p1, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1089834
    new-instance p2, LX/6RT;

    invoke-direct {p2, p0}, LX/6RT;-><init>(LX/6RU;)V

    .line 1089835
    invoke-virtual {p6, p2, p1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1089836
    return-void
.end method

.method public static a(LX/0QB;)LX/6RU;
    .locals 5

    .prologue
    .line 1089792
    sget-object v0, LX/6RU;->l:LX/6RU;

    if-nez v0, :cond_1

    .line 1089793
    const-class v1, LX/6RU;

    monitor-enter v1

    .line 1089794
    :try_start_0
    sget-object v0, LX/6RU;->l:LX/6RU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1089795
    if-eqz v2, :cond_0

    .line 1089796
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1089797
    new-instance v4, LX/6RU;

    const/16 v3, 0x1617

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, p0, v3}, LX/6RU;-><init>(LX/0Or;Landroid/content/Context;)V

    .line 1089798
    move-object v0, v4

    .line 1089799
    sput-object v0, LX/6RU;->l:LX/6RU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1089800
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1089801
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1089802
    :cond_1
    sget-object v0, LX/6RU;->l:LX/6RU;

    return-object v0

    .line 1089803
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1089804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/6RU;Ljava/util/Locale;Ljava/util/TimeZone;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1089805
    iput-object p2, p0, LX/6RU;->c:Ljava/util/TimeZone;

    .line 1089806
    const v0, 0x7f081fdb

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RU;->f:Ljava/lang/String;

    .line 1089807
    const v0, 0x7f081fdc

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RU;->g:Ljava/lang/String;

    .line 1089808
    const v0, 0x7f080088

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RU;->h:Ljava/lang/String;

    .line 1089809
    const v0, 0x7f081fdf

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RU;->i:Ljava/lang/String;

    .line 1089810
    const v0, 0x7f081fe0

    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/6RU;->j:Ljava/lang/String;

    .line 1089811
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, LX/6Ra;->b:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1089812
    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1089813
    move-object v0, v0

    .line 1089814
    iput-object v0, p0, LX/6RU;->d:Ljava/text/DateFormat;

    .line 1089815
    invoke-static {p1, p2}, LX/6Ra;->a(Ljava/util/Locale;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/6RU;->e:Ljava/text/DateFormat;

    .line 1089816
    invoke-static {p3}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1089817
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1089818
    :goto_0
    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1089819
    move-object v0, v0

    .line 1089820
    iput-object v0, p0, LX/6RU;->a:Ljava/text/DateFormat;

    .line 1089821
    invoke-static {p3, p1, p2}, LX/6Ra;->b(Landroid/content/Context;Ljava/util/Locale;Ljava/util/TimeZone;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, LX/6RU;->b:Ljava/text/DateFormat;

    .line 1089822
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1089823
    const/4 v0, 0x1

    iput v0, p0, LX/6RU;->k:I

    .line 1089824
    :goto_1
    return-void

    .line 1089825
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/6RU;->k:I

    goto :goto_1

    .line 1089826
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h:mm a"

    invoke-direct {v0, v1, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method
