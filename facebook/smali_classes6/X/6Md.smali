.class public final LX/6Md;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1081716
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1081717
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1081718
    :goto_0
    return v1

    .line 1081719
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1081720
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1081721
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1081722
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1081723
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1081724
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1081725
    invoke-static {p0, p1}, LX/3Ft;->b(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1081726
    :cond_2
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1081727
    invoke-static {p0, p1}, LX/3h6;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1081728
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1081729
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1081730
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1081731
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1081732
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1081733
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1081734
    if-eqz v0, :cond_0

    .line 1081735
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1081736
    invoke-static {p0, v0, p2, p3}, LX/3Ft;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1081737
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1081738
    if-eqz v0, :cond_1

    .line 1081739
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1081740
    invoke-static {p0, v0, p2}, LX/3h6;->a(LX/15i;ILX/0nX;)V

    .line 1081741
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1081742
    return-void
.end method
