.class public LX/5sD;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/5sD;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/5sD;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/view/MotionEvent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/5sF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:S

.field public e:F

.field public f:F


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1012632
    new-instance v0, LX/0Zi;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/5sD;->a:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1012633
    invoke-direct {p0}, LX/5r0;-><init>()V

    .line 1012634
    return-void
.end method

.method public static a(ILX/5sF;Landroid/view/MotionEvent;FFLX/5sE;)LX/5sD;
    .locals 7

    .prologue
    .line 1012599
    sget-object v0, LX/5sD;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5sD;

    .line 1012600
    if-nez v0, :cond_0

    .line 1012601
    new-instance v0, LX/5sD;

    invoke-direct {v0}, LX/5sD;-><init>()V

    :cond_0
    move v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    .line 1012602
    invoke-direct/range {v0 .. v6}, LX/5sD;->b(ILX/5sF;Landroid/view/MotionEvent;FFLX/5sE;)V

    .line 1012603
    return-object v0
.end method

.method private b(ILX/5sF;Landroid/view/MotionEvent;FFLX/5sE;)V
    .locals 4

    .prologue
    .line 1012616
    invoke-super {p0, p1}, LX/5r0;->a(I)V

    .line 1012617
    const/4 v0, 0x0

    .line 1012618
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 1012619
    packed-switch v1, :pswitch_data_0

    .line 1012620
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled MotionEvent action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1012621
    :pswitch_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    invoke-virtual {p6, v2, v3}, LX/5sE;->a(J)V

    .line 1012622
    :goto_0
    iput-object p2, p0, LX/5sD;->c:LX/5sF;

    .line 1012623
    invoke-static {p3}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, LX/5sD;->b:Landroid/view/MotionEvent;

    .line 1012624
    iput-short v0, p0, LX/5sD;->d:S

    .line 1012625
    iput p4, p0, LX/5sD;->e:F

    .line 1012626
    iput p5, p0, LX/5sD;->f:F

    .line 1012627
    return-void

    .line 1012628
    :pswitch_2
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    invoke-virtual {p6, v2, v3}, LX/5sE;->d(J)V

    goto :goto_0

    .line 1012629
    :pswitch_3
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    invoke-virtual {p6, v2, v3}, LX/5sE;->b(J)V

    goto :goto_0

    .line 1012630
    :pswitch_4
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    invoke-virtual {p6, v0, v1}, LX/5sE;->c(J)S

    move-result v0

    goto :goto_0

    .line 1012631
    :pswitch_5
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    invoke-virtual {p6, v2, v3}, LX/5sE;->d(J)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1012612
    iget-object v0, p0, LX/5sD;->b:Landroid/view/MotionEvent;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1012613
    const/4 v0, 0x0

    iput-object v0, p0, LX/5sD;->b:Landroid/view/MotionEvent;

    .line 1012614
    sget-object v0, LX/5sD;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1012615
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 2

    .prologue
    .line 1012635
    iget-object v0, p0, LX/5sD;->c:LX/5sF;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5sF;

    .line 1012636
    iget v1, p0, LX/5r0;->c:I

    move v1, v1

    .line 1012637
    invoke-static {p1, v0, v1, p0}, LX/5sG;->a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;LX/5sF;ILX/5sD;)V

    .line 1012638
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1012611
    iget-object v0, p0, LX/5sD;->c:LX/5sF;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5sF;

    invoke-virtual {v0}, LX/5sF;->getJSEventName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 1012607
    sget-object v1, LX/5sC;->a:[I

    iget-object v0, p0, LX/5sD;->c:LX/5sF;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5sF;

    invoke-virtual {v0}, LX/5sF;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1012608
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown touch event type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/5sD;->c:LX/5sF;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1012609
    :pswitch_0
    const/4 v0, 0x0

    .line 1012610
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f()S
    .locals 1

    .prologue
    .line 1012606
    iget-short v0, p0, LX/5sD;->d:S

    return v0
.end method

.method public final j()Landroid/view/MotionEvent;
    .locals 1

    .prologue
    .line 1012604
    iget-object v0, p0, LX/5sD;->b:Landroid/view/MotionEvent;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1012605
    iget-object v0, p0, LX/5sD;->b:Landroid/view/MotionEvent;

    return-object v0
.end method
