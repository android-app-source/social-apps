.class public LX/6HU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Z

.field public final B:LX/6HF;

.field public final C:LX/6Hj;

.field public final D:LX/0Sh;

.field private E:I

.field public final F:LX/0Zr;

.field public final G:LX/03V;

.field public H:I

.field public I:I

.field public J:I

.field public K:I

.field public L:I

.field public M:I

.field public N:I

.field public O:I

.field public P:I

.field public Q:I

.field public R:I

.field public S:I

.field public T:Z

.field public U:Z

.field public final V:Landroid/hardware/Camera$ShutterCallback;

.field public final W:Landroid/hardware/Camera$PictureCallback;

.field public a:LX/6HO;

.field public c:Landroid/app/Activity;

.field public d:Landroid/hardware/Camera;

.field public e:LX/6HV;

.field public final f:LX/6HI;

.field public final g:Landroid/content/Context;

.field public final h:LX/6HT;

.field public i:Landroid/media/MediaRecorder;

.field public j:LX/2Ib;

.field public k:Z

.field public l:Landroid/view/ScaleGestureDetector;

.field public m:LX/6Hb;

.field public n:LX/6Ha;

.field public o:LX/6He;

.field public p:LX/6Hh;

.field private final q:LX/6HN;

.field public r:LX/6HP;

.field public s:Z

.field public t:Ljava/lang/String;

.field private u:Landroid/net/Uri;

.field private v:Landroid/content/ContentResolver;

.field public w:I

.field public x:I

.field private y:Z

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1071950
    const-class v0, LX/6HU;

    sput-object v0, LX/6HU;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/6HO;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/6HF;LX/2Ib;LX/6Hj;LX/0Sh;LX/0Zr;LX/03V;)V
    .locals 22

    .prologue
    .line 1071951
    const/4 v10, 0x1

    const v11, 0x927c0

    const/4 v12, -0x1

    const/4 v13, -0x1

    const/4 v14, -0x1

    const/4 v15, -0x1

    const/16 v16, -0x1

    const/16 v17, -0x1

    const/16 v18, -0x1

    const/16 v19, -0x1

    const/16 v20, -0x1

    const/16 v21, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v21}, LX/6HU;-><init>(LX/6HO;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/6HF;LX/2Ib;LX/6Hj;LX/0Sh;LX/0Zr;LX/03V;IIIIIIIIIIII)V

    .line 1071952
    return-void
.end method

.method public constructor <init>(LX/6HO;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/6HF;LX/2Ib;LX/6Hj;LX/0Sh;LX/0Zr;LX/03V;IIIIIIIIIIII)V
    .locals 3

    .prologue
    .line 1071953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1071954
    new-instance v1, LX/6HT;

    invoke-direct {v1, p0}, LX/6HT;-><init>(LX/6HU;)V

    iput-object v1, p0, LX/6HU;->h:LX/6HT;

    .line 1071955
    const/4 v1, 0x0

    iput-object v1, p0, LX/6HU;->o:LX/6He;

    .line 1071956
    const/4 v1, 0x0

    iput-object v1, p0, LX/6HU;->p:LX/6Hh;

    .line 1071957
    new-instance v1, LX/6HN;

    invoke-direct {v1, p0}, LX/6HN;-><init>(LX/6HU;)V

    iput-object v1, p0, LX/6HU;->q:LX/6HN;

    .line 1071958
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/6HU;->s:Z

    .line 1071959
    const/4 v1, -0x1

    iput v1, p0, LX/6HU;->w:I

    .line 1071960
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/6HU;->y:Z

    .line 1071961
    const/4 v1, -0x1

    iput v1, p0, LX/6HU;->E:I

    .line 1071962
    new-instance v1, LX/6HJ;

    invoke-direct {v1, p0}, LX/6HJ;-><init>(LX/6HU;)V

    iput-object v1, p0, LX/6HU;->V:Landroid/hardware/Camera$ShutterCallback;

    .line 1071963
    new-instance v1, LX/6HK;

    invoke-direct {v1, p0}, LX/6HK;-><init>(LX/6HU;)V

    iput-object v1, p0, LX/6HU;->W:Landroid/hardware/Camera$PictureCallback;

    .line 1071964
    iput-object p1, p0, LX/6HU;->a:LX/6HO;

    .line 1071965
    iput-object p4, p0, LX/6HU;->B:LX/6HF;

    .line 1071966
    iput-object p6, p0, LX/6HU;->C:LX/6Hj;

    .line 1071967
    iput-object p7, p0, LX/6HU;->D:LX/0Sh;

    .line 1071968
    new-instance v1, LX/6HI;

    iget-object v2, p0, LX/6HU;->B:LX/6HF;

    invoke-direct {v1, p3, v2, p0}, LX/6HI;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/6HF;LX/6HU;)V

    iput-object v1, p0, LX/6HU;->f:LX/6HI;

    .line 1071969
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, LX/6HU;->g:Landroid/content/Context;

    .line 1071970
    iget-object v1, p0, LX/6HU;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, LX/6HU;->v:Landroid/content/ContentResolver;

    .line 1071971
    iput-object p5, p0, LX/6HU;->j:LX/2Ib;

    .line 1071972
    iput-object p8, p0, LX/6HU;->F:LX/0Zr;

    .line 1071973
    iput-object p9, p0, LX/6HU;->G:LX/03V;

    .line 1071974
    iput p10, p0, LX/6HU;->H:I

    .line 1071975
    const/4 v1, -0x1

    if-eq p11, v1, :cond_0

    :goto_0
    iput p11, p0, LX/6HU;->I:I

    .line 1071976
    iput p12, p0, LX/6HU;->J:I

    .line 1071977
    move/from16 v0, p13

    iput v0, p0, LX/6HU;->K:I

    .line 1071978
    move/from16 v0, p14

    iput v0, p0, LX/6HU;->L:I

    .line 1071979
    move/from16 v0, p15

    iput v0, p0, LX/6HU;->M:I

    .line 1071980
    move/from16 v0, p16

    iput v0, p0, LX/6HU;->N:I

    .line 1071981
    move/from16 v0, p17

    iput v0, p0, LX/6HU;->O:I

    .line 1071982
    move/from16 v0, p18

    iput v0, p0, LX/6HU;->P:I

    .line 1071983
    move/from16 v0, p19

    iput v0, p0, LX/6HU;->Q:I

    .line 1071984
    move/from16 v0, p20

    iput v0, p0, LX/6HU;->R:I

    .line 1071985
    move/from16 v0, p21

    iput v0, p0, LX/6HU;->S:I

    .line 1071986
    const/4 v1, -0x1

    iput v1, p0, LX/6HU;->E:I

    .line 1071987
    iget-object v1, p0, LX/6HU;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.hardware.camera.flash"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, LX/6HU;->T:Z

    .line 1071988
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/6HU;->U:Z

    .line 1071989
    return-void

    .line 1071990
    :cond_0
    const p11, 0x927c0

    goto :goto_0
.end method

.method private A()V
    .locals 6

    .prologue
    .line 1071991
    iget-object v0, p0, LX/6HU;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1071992
    :goto_0
    return-void

    .line 1071993
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1071994
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 1071995
    const-string v3, "datetaken"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1071996
    iget v0, p0, LX/6HU;->J:I

    packed-switch v0, :pswitch_data_0

    .line 1071997
    const-string v0, "video/3gpp"

    .line 1071998
    :goto_1
    const-string v1, "mime_type"

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071999
    const-string v0, "_data"

    iget-object v1, p0, LX/6HU;->t:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072000
    const-string v0, "_size"

    new-instance v1, Ljava/io/File;

    iget-object v3, p0, LX/6HU;->t:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1072001
    :try_start_0
    iget-object v0, p0, LX/6HU;->v:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/6HU;->u:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1072002
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current video URI: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/6HU;->u:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1072003
    :pswitch_0
    const-string v0, "video/mp4"

    goto :goto_1

    .line 1072004
    :pswitch_1
    const-string v0, "video/mp4v-es"

    goto :goto_1

    .line 1072005
    :catch_0
    move-exception v0

    .line 1072006
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, LX/6HU;->u:Landroid/net/Uri;

    .line 1072007
    iget-object v1, p0, LX/6HU;->B:LX/6HF;

    const-string v2, "save video file failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1072008
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current video URI: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/6HU;->u:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    :catchall_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Current video URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/6HU;->u:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    throw v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static C(LX/6HU;)V
    .locals 3

    .prologue
    .line 1071916
    iget-object v0, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_0

    .line 1071917
    iget-object v0, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 1071918
    iget-object v0, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 1071919
    const/4 v0, 0x0

    iput-object v0, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    .line 1071920
    :cond_0
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_1

    .line 1071921
    :try_start_0
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->reconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1071922
    :cond_1
    :goto_0
    return-void

    .line 1071923
    :catch_0
    move-exception v0

    .line 1071924
    iget-object v1, p0, LX/6HU;->B:LX/6HF;

    const-string v2, "initializeRecorder/reconnect failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static D(LX/6HU;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1072009
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 1072010
    const/4 v0, 0x0

    .line 1072011
    :goto_0
    return v0

    .line 1072012
    :cond_0
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0}, LX/6HO;->e()LX/6IG;

    move-result-object v1

    .line 1072013
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0}, LX/6HO;->g()I

    move-result v0

    .line 1072014
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1072015
    iget v3, p0, LX/6HU;->x:I

    invoke-static {v3, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1072016
    sget-boolean v3, LX/6Hs;->a:Z

    move v3, v3

    .line 1072017
    if-eqz v3, :cond_5

    .line 1072018
    iget v0, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v0, v4, :cond_4

    .line 1072019
    iget v0, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget v2, v1, LX/6IG;->mRotation:I

    add-int/2addr v0, v2

    .line 1072020
    sget-object v2, LX/6IG;->PORTRAIT:LX/6IG;

    if-eq v1, v2, :cond_1

    sget-object v2, LX/6IG;->REVERSE_PORTRAIT:LX/6IG;

    if-ne v1, v2, :cond_3

    .line 1072021
    :cond_1
    add-int/lit8 v0, v0, 0x5a

    .line 1072022
    :cond_2
    :goto_1
    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 1072023
    :cond_3
    add-int/lit8 v0, v0, -0x5a

    goto :goto_1

    .line 1072024
    :cond_4
    iget v0, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget v1, v1, LX/6IG;->mRotation:I

    sub-int/2addr v0, v1

    goto :goto_1

    .line 1072025
    :cond_5
    iget v3, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v3, v4, :cond_7

    .line 1072026
    iget v2, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget v3, v1, LX/6IG;->mRotation:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1072027
    sget-object v2, LX/6IG;->PORTRAIT:LX/6IG;

    if-eq v1, v2, :cond_6

    sget-object v2, LX/6IG;->REVERSE_PORTRAIT:LX/6IG;

    if-ne v1, v2, :cond_2

    .line 1072028
    :cond_6
    iget-object v1, p0, LX/6HU;->C:LX/6Hj;

    .line 1072029
    iget-object v2, v1, LX/6Hj;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 1072030
    if-eqz v1, :cond_2

    .line 1072031
    add-int/lit16 v0, v0, 0xb4

    goto :goto_1

    .line 1072032
    :cond_7
    iget v2, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget v1, v1, LX/6IG;->mRotation:I

    sub-int v1, v2, v1

    sub-int v0, v1, v0

    goto :goto_1
.end method

.method public static E(LX/6HU;)I
    .locals 2

    .prologue
    .line 1072033
    new-instance v0, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v0}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1072034
    iget v1, p0, LX/6HU;->x:I

    invoke-static {v1, v0}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1072035
    iget v0, v0, Landroid/hardware/Camera$CameraInfo;->facing:I

    return v0
.end method

.method public static G(LX/6HU;)V
    .locals 3

    .prologue
    .line 1072036
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    .line 1072037
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to load camera, even though we already have one"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1072038
    :cond_0
    iget-object v0, p0, LX/6HU;->B:LX/6HF;

    iget v1, p0, LX/6HU;->x:I

    iget-boolean v2, p0, LX/6HU;->y:Z

    invoke-interface {v0, v1, v2}, LX/6HF;->a(IZ)V

    .line 1072039
    iget-object v0, p0, LX/6HU;->r:LX/6HP;

    if-nez v0, :cond_1

    .line 1072040
    new-instance v0, LX/6HP;

    invoke-direct {v0, p0}, LX/6HP;-><init>(LX/6HU;)V

    iput-object v0, p0, LX/6HU;->r:LX/6HP;

    .line 1072041
    :cond_1
    iget-object v0, p0, LX/6HU;->r:LX/6HP;

    invoke-virtual {v0}, LX/6HP;->a()V

    .line 1072042
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6HU;->U:Z

    .line 1072043
    return-void
.end method

.method public static K(LX/6HU;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1072044
    iget-object v0, p0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1072045
    iput-boolean v4, p0, LX/6HU;->s:Z

    .line 1072046
    iget-object v0, p0, LX/6HU;->f:LX/6HI;

    if-eqz v0, :cond_0

    .line 1072047
    iget-object v0, p0, LX/6HU;->f:LX/6HI;

    invoke-virtual {v0, v3}, LX/6HI;->a(Landroid/hardware/Camera;)V

    .line 1072048
    :cond_0
    iput-object v3, p0, LX/6HU;->r:LX/6HP;

    .line 1072049
    iget-object v0, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_2

    .line 1072050
    iget-boolean v0, p0, LX/6HU;->k:Z

    if-eqz v0, :cond_1

    .line 1072051
    iget-object v0, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V

    .line 1072052
    :cond_1
    iget-object v0, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 1072053
    iget-object v0, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 1072054
    iput-object v3, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    .line 1072055
    :cond_2
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0}, LX/6HO;->h()LX/6Hd;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1072056
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0}, LX/6HO;->h()LX/6Hd;

    move-result-object v0

    invoke-virtual {v0}, LX/6Hd;->a()V

    .line 1072057
    :cond_3
    iget-object v0, p0, LX/6HU;->p:LX/6Hh;

    if-eqz v0, :cond_7

    .line 1072058
    iget-object v0, p0, LX/6HU;->p:LX/6Hh;

    iget-object v1, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v1}, LX/6HO;->h()LX/6Hd;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6Hh;->b(LX/6HZ;)V

    .line 1072059
    iget-object v0, p0, LX/6HU;->p:LX/6Hh;

    iget-object v1, p0, LX/6HU;->n:LX/6Ha;

    invoke-virtual {v0, v1}, LX/6Hh;->b(LX/6HZ;)V

    .line 1072060
    iget-object v0, p0, LX/6HU;->p:LX/6Hh;

    .line 1072061
    iget-object v1, v0, LX/6Hh;->c:LX/6He;

    const/4 v6, 0x0

    .line 1072062
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v7

    move v5, v6

    .line 1072063
    :goto_0
    iget-object v2, v1, LX/6He;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_5

    .line 1072064
    iget-object v2, v1, LX/6He;->g:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Hh;

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1072065
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1072066
    :cond_4
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_0

    .line 1072067
    :cond_5
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    :goto_1
    if-ge v6, v5, :cond_6

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1072068
    iget-object v8, v1, LX/6He;->g:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1072069
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1072070
    :cond_6
    iget-object v1, v0, LX/6Hh;->g:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->interrupt()V

    .line 1072071
    iput-object v3, p0, LX/6HU;->p:LX/6Hh;

    .line 1072072
    :cond_7
    iget-object v0, p0, LX/6HU;->o:LX/6He;

    if-eqz v0, :cond_9

    .line 1072073
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_8

    .line 1072074
    :try_start_0
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setFaceDetectionListener(Landroid/hardware/Camera$FaceDetectionListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1072075
    :cond_8
    :goto_2
    iput-object v3, p0, LX/6HU;->o:LX/6He;

    .line 1072076
    :cond_9
    iget-object v0, p0, LX/6HU;->n:LX/6Ha;

    if-eqz v0, :cond_a

    .line 1072077
    iget-object v0, p0, LX/6HU;->n:LX/6Ha;

    .line 1072078
    invoke-virtual {v0}, LX/6Ha;->d()V

    .line 1072079
    iput-object v3, p0, LX/6HU;->n:LX/6Ha;

    .line 1072080
    :cond_a
    iget-object v0, p0, LX/6HU;->e:LX/6HV;

    if-eqz v0, :cond_b

    .line 1072081
    iget-object v0, p0, LX/6HU;->e:LX/6HV;

    const/4 v2, 0x0

    .line 1072082
    iget-object v1, v0, LX/6HV;->c:Landroid/hardware/Camera;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 1072083
    iput-object v2, v0, LX/6HV;->c:Landroid/hardware/Camera;

    .line 1072084
    iput-object v2, v0, LX/6HV;->d:LX/6HS;

    .line 1072085
    iput-object v2, v0, LX/6HV;->e:Lcom/facebook/qrcode/QRCodeFragment;

    .line 1072086
    iput-object v3, p0, LX/6HU;->e:LX/6HV;

    .line 1072087
    :cond_b
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_c

    .line 1072088
    :try_start_1
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    const v1, 0xd630557

    invoke-static {v0, v1}, LX/0J2;->c(Landroid/hardware/Camera;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1072089
    :goto_3
    :try_start_2
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    const v1, 0x627a5502

    invoke-static {v0, v1}, LX/0J2;->a(Landroid/hardware/Camera;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1072090
    :goto_4
    iput-object v3, p0, LX/6HU;->d:Landroid/hardware/Camera;

    .line 1072091
    :cond_c
    iget-object v0, p0, LX/6HU;->B:LX/6HF;

    invoke-interface {v0}, LX/6HF;->e()V

    .line 1072092
    iput-object v3, p0, LX/6HU;->l:Landroid/view/ScaleGestureDetector;

    .line 1072093
    iput-object v3, p0, LX/6HU;->m:LX/6Hb;

    .line 1072094
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0}, LX/6HO;->a()V

    .line 1072095
    iput-boolean v4, p0, LX/6HU;->U:Z

    .line 1072096
    return-void

    .line 1072097
    :catch_0
    move-exception v0

    .line 1072098
    iget-object v1, p0, LX/6HU;->B:LX/6HF;

    const-string v2, "releaseCamera/setFaceDetectionListener failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 1072099
    :catch_1
    move-exception v0

    .line 1072100
    iget-object v1, p0, LX/6HU;->B:LX/6HF;

    const-string v2, "releaseCamera/stopPreview failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3

    .line 1072101
    :catch_2
    move-exception v0

    .line 1072102
    iget-object v1, p0, LX/6HU;->B:LX/6HF;

    const-string v2, "releaseCamera/release failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_4
.end method

.method public static L(LX/6HU;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1072103
    iget-object v0, p0, LX/6HU;->n:LX/6Ha;

    iget-object v1, p0, LX/6HU;->d:Landroid/hardware/Camera;

    .line 1072104
    iput-object v1, v0, LX/6Ha;->u:Landroid/hardware/Camera;

    .line 1072105
    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    .line 1072106
    const-string v3, "auto"

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v5

    invoke-static {v3, v5}, LX/6Ha;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v3

    iput-boolean v3, v0, LX/6Ha;->e:Z

    .line 1072107
    invoke-static {v2}, LX/6Ha;->a(Landroid/hardware/Camera$Parameters;)Z

    move-result v3

    iput-boolean v3, v0, LX/6Ha;->f:Z

    .line 1072108
    iget-boolean v3, v0, LX/6Ha;->f:Z

    if-eqz v3, :cond_0

    .line 1072109
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMaxNumFocusAreas()I

    move-result v3

    iput v3, v0, LX/6Ha;->w:I

    .line 1072110
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMaxNumMeteringAreas()I

    move-result v2

    iput v2, v0, LX/6Ha;->x:I

    .line 1072111
    :cond_0
    iget-object v0, p0, LX/6HU;->n:LX/6Ha;

    iget-object v1, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v1}, LX/6HO;->c()Lcom/facebook/camera/views/RotateLayout;

    move-result-object v1

    iget-object v2, p0, LX/6HU;->e:LX/6HV;

    invoke-static {p0}, LX/6HU;->E(LX/6HU;)I

    move-result v3

    if-ne v3, v4, :cond_1

    :goto_0
    iget-object v3, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v3}, LX/6HO;->f()LX/6IG;

    move-result-object v3

    iget v5, v3, LX/6IG;->mReverseRotation:I

    move-object v3, p0

    .line 1072112
    iput-object v1, v0, LX/6Ha;->i:Landroid/view/View;

    .line 1072113
    const p0, 0x7f0d11da

    invoke-virtual {v1, p0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/facebook/camera/views/FocusIndicatorView;

    iput-object p0, v0, LX/6Ha;->j:Lcom/facebook/camera/views/FocusIndicatorView;

    .line 1072114
    iput-object v2, v0, LX/6Ha;->k:Landroid/view/View;

    .line 1072115
    iput-object v3, v0, LX/6Ha;->a:LX/6HU;

    .line 1072116
    iput-boolean v4, v0, LX/6Ha;->r:Z

    .line 1072117
    iput v5, v0, LX/6Ha;->s:I

    .line 1072118
    const/4 p0, 0x0

    iput-object p0, v0, LX/6Ha;->h:Landroid/graphics/Matrix;

    .line 1072119
    new-instance p0, LX/6HW;

    invoke-direct {p0, v0}, LX/6HW;-><init>(LX/6Ha;)V

    iput-object p0, v0, LX/6Ha;->v:Ljava/util/Comparator;

    .line 1072120
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object p0

    iput-object p0, v0, LX/6Ha;->y:Ljava/util/HashSet;

    .line 1072121
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/6Ha;->d:Z

    .line 1072122
    return-void

    .line 1072123
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static a(LX/6HU;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1072124
    iget-object v0, p0, LX/6HU;->f:LX/6HI;

    .line 1072125
    iget-object v1, v0, LX/6HI;->j:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, p1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 1072126
    iput-object p1, v0, LX/6HI;->g:Ljava/lang/String;

    .line 1072127
    :try_start_0
    iget-object v1, v0, LX/6HI;->c:Landroid/hardware/Camera;

    iget-object v2, v0, LX/6HI;->j:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1072128
    :goto_0
    return-void

    .line 1072129
    :catch_0
    move-exception v1

    .line 1072130
    sget-object v2, LX/6HI;->d:Ljava/lang/Class;

    const-string v3, "Failed to change flash mode."

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static x(LX/6HU;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1072131
    iget-object v1, p0, LX/6HU;->c:Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/6HU;->d:Landroid/hardware/Camera;

    if-nez v1, :cond_1

    .line 1072132
    :cond_0
    :goto_0
    return-void

    .line 1072133
    :cond_1
    new-instance v1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1072134
    :try_start_0
    iget v2, p0, LX/6HU;->x:I

    invoke-static {v2, v1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1072135
    iget-object v2, p0, LX/6HU;->c:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1072136
    packed-switch v2, :pswitch_data_0

    .line 1072137
    :goto_1
    :pswitch_0
    iget v2, v1, Landroid/hardware/Camera$CameraInfo;->facing:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1072138
    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    .line 1072139
    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 1072140
    :goto_2
    iget-object v1, p0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    goto :goto_0

    .line 1072141
    :pswitch_1
    const/16 v0, 0x5a

    .line 1072142
    goto :goto_1

    .line 1072143
    :pswitch_2
    const/16 v0, 0xb4

    .line 1072144
    goto :goto_1

    .line 1072145
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_1

    .line 1072146
    :cond_2
    iget v1, v1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int v0, v1, v0

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto :goto_2

    .line 1072147
    :catch_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private y()V
    .locals 3

    .prologue
    .line 1071945
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6HU;->k:Z

    .line 1071946
    :try_start_0
    iget-object v0, p0, LX/6HU;->i:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1071947
    :goto_0
    return-void

    .line 1071948
    :catch_0
    move-exception v0

    .line 1071949
    iget-object v1, p0, LX/6HU;->B:LX/6HF;

    const-string v2, "stop MediaRecorder failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static z(LX/6HU;)V
    .locals 2

    .prologue
    .line 1072148
    iget-object v0, p0, LX/6HU;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1072149
    :goto_0
    return-void

    .line 1072150
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/6HU;->t:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1072151
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1072152
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Could not delete "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/6HU;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1072153
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/6HU;->t:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6HX;)V
    .locals 3

    .prologue
    .line 1071861
    iget-object v0, p0, LX/6HU;->D:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1071862
    :try_start_0
    iget-object v0, p0, LX/6HU;->d:Landroid/hardware/Camera;

    iget-object v1, p0, LX/6HU;->q:LX/6HN;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 1071863
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0, p1}, LX/6HO;->a(LX/6HX;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1071864
    :goto_0
    return-void

    .line 1071865
    :catch_0
    move-exception v0

    .line 1071866
    iget-object v1, p0, LX/6HU;->B:LX/6HF;

    const-string v2, "autoFocus failed"

    invoke-interface {v1, v2, v0}, LX/6HF;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1071867
    iget-object v0, p0, LX/6HU;->q:LX/6HN;

    const/4 v1, 0x0

    iget-object v2, p0, LX/6HU;->d:Landroid/hardware/Camera;

    invoke-virtual {v0, v1, v2}, LX/6HN;->onAutoFocus(ZLandroid/hardware/Camera;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1071868
    iget v1, p0, LX/6HU;->w:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1071869
    invoke-virtual {p0}, LX/6HU;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1071870
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Camera toggled without proper support from API"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1071871
    :cond_0
    invoke-static {p0}, LX/6HU;->K(LX/6HU;)V

    .line 1071872
    iget v0, p0, LX/6HU;->x:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LX/6HU;->w:I

    rem-int/2addr v0, v1

    iput v0, p0, LX/6HU;->x:I

    .line 1071873
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6HU;->y:Z

    .line 1071874
    iget v0, p0, LX/6HU;->x:I

    iput v0, p0, LX/6HU;->E:I

    .line 1071875
    invoke-static {p0}, LX/6HU;->G(LX/6HU;)V

    .line 1071876
    iget v0, p0, LX/6HU;->x:I

    return v0
.end method

.method public final e()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1071877
    iput-boolean v0, p0, LX/6HU;->y:Z

    .line 1071878
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    iput v1, p0, LX/6HU;->w:I

    .line 1071879
    iget v1, p0, LX/6HU;->E:I

    if-ltz v1, :cond_4

    iget v1, p0, LX/6HU;->E:I

    iget v2, p0, LX/6HU;->w:I

    if-ge v1, v2, :cond_4

    .line 1071880
    iget v1, p0, LX/6HU;->E:I

    iput v1, p0, LX/6HU;->x:I

    .line 1071881
    :cond_0
    :goto_0
    iget-object v1, p0, LX/6HU;->C:LX/6Hj;

    .line 1071882
    iget-object v2, v1, LX/6Hj;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v1, v2

    .line 1071883
    iput-boolean v1, p0, LX/6HU;->z:Z

    .line 1071884
    iget-boolean v1, p0, LX/6HU;->z:Z

    if-nez v1, :cond_2

    iget-object v1, p0, LX/6HU;->C:LX/6Hj;

    .line 1071885
    iget-object v2, v1, LX/6Hj;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03R;

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "GT-S5830"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "GT-S5363"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_1
    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v2, v3}, LX/03R;->asBoolean(Z)Z

    move-result v2

    move v1, v2

    .line 1071886
    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    iput-boolean v0, p0, LX/6HU;->A:Z

    .line 1071887
    return-void

    .line 1071888
    :cond_4
    invoke-virtual {p0}, LX/6HU;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1071889
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1071890
    const/4 v1, 0x0

    :goto_2
    iget v3, p0, LX/6HU;->w:I

    if-ge v1, v3, :cond_5

    .line 1071891
    invoke-static {v1, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1071892
    iget v3, v2, Landroid/hardware/Camera$CameraInfo;->facing:I

    iget-object v4, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v4}, LX/6HO;->d()I

    move-result v4

    if-ne v3, v4, :cond_7

    .line 1071893
    iput v1, p0, LX/6HU;->x:I

    .line 1071894
    :cond_5
    goto :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_1

    .line 1071895
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1071896
    invoke-direct {p0}, LX/6HU;->y()V

    .line 1071897
    invoke-static {p0}, LX/6HU;->z(LX/6HU;)V

    .line 1071898
    invoke-static {p0}, LX/6HU;->C(LX/6HU;)V

    .line 1071899
    invoke-static {p0}, LX/6HU;->K(LX/6HU;)V

    .line 1071900
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1071901
    invoke-direct {p0}, LX/6HU;->y()V

    .line 1071902
    invoke-direct {p0}, LX/6HU;->A()V

    .line 1071903
    invoke-static {p0}, LX/6HU;->C(LX/6HU;)V

    .line 1071904
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    iget-object v1, p0, LX/6HU;->u:Landroid/net/Uri;

    invoke-interface {v0, v1}, LX/6HO;->a(Landroid/net/Uri;)V

    .line 1071905
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 1071906
    invoke-static {p0}, LX/6HU;->K(LX/6HU;)V

    .line 1071907
    invoke-static {p0}, LX/6HU;->G(LX/6HU;)V

    .line 1071908
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1071909
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0}, LX/6HO;->h()LX/6Hd;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1071910
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    invoke-interface {v0}, LX/6HO;->h()LX/6Hd;

    move-result-object v0

    invoke-virtual {v0}, LX/6Hd;->a()V

    .line 1071911
    :cond_0
    iget-boolean v0, p0, LX/6HU;->k:Z

    if-eqz v0, :cond_1

    .line 1071912
    invoke-virtual {p0}, LX/6HU;->i()V

    .line 1071913
    invoke-static {p0}, LX/6HU;->C(LX/6HU;)V

    .line 1071914
    :cond_1
    invoke-static {p0}, LX/6HU;->K(LX/6HU;)V

    .line 1071915
    return-void
.end method

.method public final m()Landroid/view/View$OnTouchListener;
    .locals 1

    .prologue
    .line 1071925
    iget-object v0, p0, LX/6HU;->f:LX/6HI;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 1071926
    iget-object v0, p0, LX/6HU;->f:LX/6HI;

    invoke-virtual {v0}, LX/6HI;->a()Z

    move-result v0

    return v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1071927
    iget-boolean v1, p0, LX/6HU;->s:Z

    if-nez v1, :cond_1

    .line 1071928
    :cond_0
    :goto_0
    return v0

    .line 1071929
    :cond_1
    iget-object v1, p0, LX/6HU;->l:Landroid/view/ScaleGestureDetector;

    if-eqz v1, :cond_3

    .line 1071930
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 1071931
    iget-object v0, p0, LX/6HU;->m:LX/6Hb;

    .line 1071932
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/6Hb;->h:Z

    .line 1071933
    iget-object v0, p0, LX/6HU;->n:LX/6Ha;

    invoke-virtual {v0, p2}, LX/6Ha;->a(Landroid/view/MotionEvent;)Z

    .line 1071934
    :cond_2
    iget-object v0, p0, LX/6HU;->l:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1071935
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/6HU;->m:LX/6Hb;

    .line 1071936
    iget-boolean v2, v1, LX/6Hb;->h:Z

    move v1, v2

    .line 1071937
    if-nez v1, :cond_0

    .line 1071938
    iget-object v1, p0, LX/6HU;->n:LX/6Ha;

    invoke-virtual {v1, p2}, LX/6Ha;->a(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 1071939
    :cond_3
    iget-object v1, p0, LX/6HU;->n:LX/6Ha;

    if-eqz v1, :cond_0

    .line 1071940
    iget-object v0, p0, LX/6HU;->n:LX/6Ha;

    invoke-virtual {v0, p2}, LX/6Ha;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 1071941
    const-string v0, "off"

    invoke-static {p0, v0}, LX/6HU;->a(LX/6HU;Ljava/lang/String;)V

    .line 1071942
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6HU;->U:Z

    .line 1071943
    iget-object v0, p0, LX/6HU;->a:LX/6HO;

    iget-boolean v1, p0, LX/6HU;->U:Z

    invoke-interface {v0, v1}, LX/6HO;->c(Z)V

    .line 1071944
    return-void
.end method
