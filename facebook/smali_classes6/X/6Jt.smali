.class public LX/6Jt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/os/Handler;

.field public final c:LX/6Km;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/7Sc;",
            "Ljava/util/Set",
            "<",
            "LX/6Jv;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:LX/6Js;

.field private final f:LX/6Jq;

.field private final g:LX/6Jx;

.field private final h:LX/6KN;

.field public final i:Ljava/util/concurrent/ExecutorService;

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "LX/6Kd;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/Surface;",
            "LX/6Ku;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/6Jp;

.field private final m:LX/61H;

.field public n:LX/6Jk;

.field public o:LX/6Jh;

.field private p:I

.field public q:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/6Kt;",
            ">;"
        }
    .end annotation
.end field

.field public volatile r:Z

.field private s:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:LX/6Kg;

.field private u:LX/6Kr;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1075875
    const-class v0, LX/6Jt;

    sput-object v0, LX/6Jt;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zr;Landroid/content/res/Resources;Landroid/os/Handler;Ljava/util/concurrent/ExecutorService;LX/6KN;LX/6KB;ILX/6Jp;)V
    .locals 11
    .param p5    # LX/6KN;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1075888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075889
    new-instance v1, LX/6Jl;

    invoke-direct {v1, p0}, LX/6Jl;-><init>(LX/6Jt;)V

    iput-object v1, p0, LX/6Jt;->m:LX/61H;

    .line 1075890
    const/4 v1, 0x0

    iput-object v1, p0, LX/6Jt;->o:LX/6Jh;

    .line 1075891
    new-instance v1, Ljava/lang/ref/WeakReference;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/6Jt;->q:Ljava/lang/ref/WeakReference;

    .line 1075892
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/6Jt;->r:Z

    .line 1075893
    iput-object p4, p0, LX/6Jt;->i:Ljava/util/concurrent/ExecutorService;

    .line 1075894
    iput-object p3, p0, LX/6Jt;->b:Landroid/os/Handler;

    .line 1075895
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/6Jt;->d:Ljava/util/Map;

    .line 1075896
    new-instance v1, LX/6Js;

    invoke-direct {v1, p0}, LX/6Js;-><init>(LX/6Jt;)V

    iput-object v1, p0, LX/6Jt;->e:LX/6Js;

    .line 1075897
    new-instance v1, LX/6Jq;

    invoke-direct {v1, p0}, LX/6Jq;-><init>(LX/6Jt;)V

    iput-object v1, p0, LX/6Jt;->f:LX/6Jq;

    .line 1075898
    move/from16 v0, p7

    iput v0, p0, LX/6Jt;->p:I

    .line 1075899
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/6Jt;->j:Ljava/util/Map;

    .line 1075900
    new-instance v5, LX/5Pd;

    invoke-direct {v5, p2}, LX/5Pd;-><init>(Landroid/content/res/Resources;)V

    .line 1075901
    new-instance v1, LX/6Km;

    iget-object v2, p0, LX/6Jt;->f:LX/6Jq;

    const-string v3, "OpenGL Rendering Thread"

    sget-object v4, LX/0TP;->REALTIME_DO_NOT_USE:LX/0TP;

    invoke-virtual {p1, v3, v4}, LX/0Zr;->a(Ljava/lang/String;LX/0TP;)Landroid/os/HandlerThread;

    move-result-object v3

    new-instance v4, LX/6KZ;

    new-instance v6, LX/7Se;

    invoke-direct {v6}, LX/7Se;-><init>()V

    invoke-direct {v4, v5, v6}, LX/6KZ;-><init>(LX/5Pc;LX/7Se;)V

    invoke-static {}, LX/5PN;->a()LX/5PK;

    move-result-object v5

    invoke-static {}, LX/6Kc;->a()LX/6Kc;

    move-result-object v6

    new-instance v7, LX/7Se;

    invoke-direct {v7}, LX/7Se;-><init>()V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v8, p5

    move/from16 v9, p7

    invoke-direct/range {v1 .. v10}, LX/6Km;-><init>(LX/6Jq;Landroid/os/HandlerThread;LX/6KZ;LX/5PK;LX/6Kc;LX/7Se;LX/6KN;ILjava/lang/String;)V

    iput-object v1, p0, LX/6Jt;->c:LX/6Km;

    .line 1075902
    move-object/from16 v0, p5

    iput-object v0, p0, LX/6Jt;->h:LX/6KN;

    .line 1075903
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/6Jt;->k:Ljava/util/Map;

    .line 1075904
    invoke-virtual/range {p6 .. p6}, LX/6KB;->b()LX/6KK;

    move-result-object v1

    invoke-virtual {v1}, LX/6KK;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1075905
    new-instance v1, LX/6K6;

    iget-object v2, p0, LX/6Jt;->b:Landroid/os/Handler;

    invoke-virtual/range {p6 .. p6}, LX/6KB;->b()LX/6KK;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, LX/6K6;-><init>(LX/6Jt;Landroid/os/Handler;LX/6KK;)V

    iput-object v1, p0, LX/6Jt;->g:LX/6Jx;

    .line 1075906
    :goto_0
    invoke-static/range {p8 .. p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Jp;

    iput-object v1, p0, LX/6Jt;->l:LX/6Jp;

    .line 1075907
    return-void

    .line 1075908
    :cond_0
    new-instance v1, LX/6K8;

    iget-object v2, p0, LX/6Jt;->b:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, LX/6K8;-><init>(LX/6Jt;Landroid/os/Handler;)V

    iput-object v1, p0, LX/6Jt;->g:LX/6Jx;

    goto :goto_0
.end method

.method private static a(LX/6Jt;LX/7Sc;)LX/7Sb;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1075909
    iget-object v1, p0, LX/6Jt;->n:LX/6Jk;

    if-nez v1, :cond_0

    .line 1075910
    :goto_0
    return-object v0

    .line 1075911
    :cond_0
    sget-object v1, LX/6Jo;->a:[I

    invoke-virtual {p1}, LX/7Sc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1075912
    :pswitch_0
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    .line 1075913
    new-instance v1, LX/7Sl;

    iget-object v2, v0, LX/6Jk;->j:LX/6JR;

    iget v2, v2, LX/6JR;->a:I

    iget-object p0, v0, LX/6Jk;->j:LX/6JR;

    iget p0, p0, LX/6JR;->b:I

    invoke-direct {v1, v2, p0}, LX/7Sl;-><init>(II)V

    move-object v0, v1

    .line 1075914
    goto :goto_0

    .line 1075915
    :pswitch_1
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    invoke-virtual {v0}, LX/6Jk;->c()LX/7Sm;

    move-result-object v0

    goto :goto_0

    .line 1075916
    :pswitch_2
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    invoke-virtual {v0}, LX/6Jk;->d()LX/7Sj;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static declared-synchronized a$redex0(LX/6Jt;LX/6Jv;LX/7Sc;)V
    .locals 3

    .prologue
    .line 1075917
    monitor-enter p0

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    const-string v1, "Null listener registered"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1075918
    iget-object v1, p0, LX/6Jt;->d:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1075919
    :try_start_1
    iget-object v0, p0, LX/6Jt;->d:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1075920
    if-nez v0, :cond_0

    .line 1075921
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1075922
    iget-object v2, p0, LX/6Jt;->d:Ljava/util/Map;

    invoke-interface {v2, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1075923
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1075924
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1075925
    :try_start_2
    invoke-static {p0, p2}, LX/6Jt;->a(LX/6Jt;LX/7Sc;)LX/7Sb;

    move-result-object v0

    .line 1075926
    if-eqz v0, :cond_1

    .line 1075927
    invoke-virtual {p0, v0}, LX/6Jt;->a(LX/7Sb;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1075928
    :cond_1
    monitor-exit p0

    return-void

    .line 1075929
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1075930
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1075931
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(LX/6Jt;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1075932
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    invoke-virtual {v0, p1}, LX/6Km;->a(Ljava/util/Map;)V

    .line 1075933
    return-void
.end method

.method public static declared-synchronized b$redex0(LX/6Jt;LX/6Jv;LX/7Sc;)V
    .locals 2

    .prologue
    .line 1075934
    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    const-string v1, "Null listener unregistered"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1075935
    iget-object v1, p0, LX/6Jt;->d:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1075936
    :try_start_1
    iget-object v0, p0, LX/6Jt;->d:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1075937
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1075938
    :goto_1
    monitor-exit p0

    return-void

    .line 1075939
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1075940
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/6Jt;->d:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1075941
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1075942
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized h(LX/6Jt;)V
    .locals 3

    .prologue
    .line 1075984
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/6Jt;->o:LX/6Jh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1075985
    :goto_0
    monitor-exit p0

    return-void

    .line 1075986
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/6Jt;->o:LX/6Jh;

    invoke-interface {v0}, LX/6Jh;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/6Jt;->s:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1075987
    :cond_1
    iget-object v0, p0, LX/6Jt;->u:LX/6Kr;

    if-nez v0, :cond_2

    .line 1075988
    new-instance v0, LX/6Kr;

    invoke-direct {v0}, LX/6Kr;-><init>()V

    iput-object v0, p0, LX/6Jt;->u:LX/6Kr;

    .line 1075989
    :cond_2
    iget-object v0, p0, LX/6Jt;->u:LX/6Kr;

    iget-object v1, p0, LX/6Jt;->s:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/6Kr;->a(Ljava/lang/Integer;)V

    .line 1075990
    iget-object v0, p0, LX/6Jt;->u:LX/6Kr;

    .line 1075991
    :goto_1
    iget-object v1, p0, LX/6Jt;->c:LX/6Km;

    .line 1075992
    const/16 v2, 0x13

    invoke-static {v1, v2, v0}, LX/6Km;->a$redex0(LX/6Km;ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1075993
    goto :goto_0

    .line 1075994
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1075995
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/6Jt;->t:LX/6Kg;

    if-nez v0, :cond_4

    .line 1075996
    new-instance v0, LX/6Kg;

    invoke-direct {v0}, LX/6Kg;-><init>()V

    iput-object v0, p0, LX/6Jt;->t:LX/6Kg;

    .line 1075997
    :cond_4
    iget-object v0, p0, LX/6Jt;->t:LX/6Kg;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/6K9;
    .locals 1

    .prologue
    .line 1075943
    iget-object v0, p0, LX/6Jt;->g:LX/6Jx;

    invoke-interface {v0}, LX/6Jx;->a()LX/6K9;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1075975
    iget v0, p0, LX/6Jt;->p:I

    if-ne v0, p1, :cond_0

    .line 1075976
    :goto_0
    return-void

    .line 1075977
    :cond_0
    iput p1, p0, LX/6Jt;->p:I

    .line 1075978
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    if-eqz v0, :cond_1

    .line 1075979
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    invoke-virtual {v0, p1}, LX/6Jk;->a(I)V

    .line 1075980
    :cond_1
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    if-eqz v0, :cond_2

    .line 1075981
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    .line 1075982
    const/16 v1, 0x11

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/6Km;->a$redex0(LX/6Km;ILjava/lang/Object;)V

    .line 1075983
    :cond_2
    iget-object v0, p0, LX/6Jt;->g:LX/6Jx;

    invoke-interface {v0}, LX/6Jx;->b()V

    goto :goto_0
.end method

.method public final a(LX/6Ia;LX/6JB;LX/6JR;)V
    .locals 3

    .prologue
    .line 1075957
    iget-object v0, p0, LX/6Jt;->g:LX/6Jx;

    invoke-interface {v0, p1}, LX/6Jx;->a(LX/6Ia;)V

    .line 1075958
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    if-eqz v0, :cond_0

    .line 1075959
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1075960
    iput-boolean v1, v0, LX/6Jk;->f:Z

    .line 1075961
    iput-boolean v1, v0, LX/6Jk;->g:Z

    .line 1075962
    iput-object v2, v0, LX/6Jk;->k:LX/6Ik;

    .line 1075963
    iput-object v2, v0, LX/6Jk;->h:LX/6JC;

    .line 1075964
    invoke-virtual {p2}, LX/6JB;->a()LX/6JB;

    move-result-object v1

    iput-object v1, v0, LX/6Jk;->i:LX/6JB;

    .line 1075965
    iput-object p1, v0, LX/6Jk;->b:LX/6Ia;

    .line 1075966
    invoke-static {v0, p3}, LX/6Jk;->a(LX/6Jk;LX/6JR;)V

    .line 1075967
    :goto_0
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    iget v1, p0, LX/6Jt;->p:I

    invoke-virtual {v0, v1}, LX/6Jk;->a(I)V

    .line 1075968
    return-void

    .line 1075969
    :cond_0
    new-instance v0, LX/6Jk;

    new-instance v1, LX/6Jr;

    invoke-direct {v1, p0}, LX/6Jr;-><init>(LX/6Jt;)V

    invoke-direct {v0, v1, p1, p2, p3}, LX/6Jk;-><init>(LX/6Jr;LX/6Ia;LX/6JB;LX/6JR;)V

    iput-object v0, p0, LX/6Jt;->n:LX/6Jk;

    .line 1075970
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    .line 1075971
    iget-object v1, v0, LX/6Jk;->c:LX/6Ji;

    move-object v0, v1

    .line 1075972
    iput-object v0, p0, LX/6Jt;->o:LX/6Jh;

    .line 1075973
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    iget-object v1, p0, LX/6Jt;->o:LX/6Jh;

    invoke-virtual {v0, v1}, LX/6Km;->a(LX/6Jh;)V

    .line 1075974
    invoke-static {p0}, LX/6Jt;->h(LX/6Jt;)V

    goto :goto_0
.end method

.method public final a(LX/6Ik;LX/6JC;)V
    .locals 1

    .prologue
    .line 1075950
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    .line 1075951
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/6Jk;->f:Z

    .line 1075952
    iput-object p1, v0, LX/6Jk;->k:LX/6Ik;

    .line 1075953
    iput-object p2, v0, LX/6Jk;->h:LX/6JC;

    .line 1075954
    iget-object p0, v0, LX/6Jk;->c:LX/6Ji;

    iget-object p0, p0, LX/6Ji;->b:Landroid/graphics/SurfaceTexture;

    if-eqz p0, :cond_0

    .line 1075955
    invoke-static {v0}, LX/6Jk;->i(LX/6Jk;)V

    .line 1075956
    :cond_0
    return-void
.end method

.method public final a(LX/6JR;LX/6JU;)V
    .locals 2

    .prologue
    .line 1075948
    iget-object v0, p0, LX/6Jt;->g:LX/6Jx;

    iget v1, p0, LX/6Jt;->p:I

    invoke-interface {v0, p1, p2, v1}, LX/6Jx;->a(LX/6JR;LX/6JU;I)V

    .line 1075949
    return-void
.end method

.method public final a(LX/6Kd;)V
    .locals 2

    .prologue
    .line 1075944
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1075945
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075946
    iget-object v1, p0, LX/6Jt;->c:LX/6Km;

    invoke-virtual {v1, v0}, LX/6Km;->c(Ljava/util/List;)V

    .line 1075947
    return-void
.end method

.method public final a(LX/7Sb;)V
    .locals 5

    .prologue
    .line 1075876
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Null event passed in"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1075877
    iget-object v1, p0, LX/6Jt;->d:Ljava/util/Map;

    monitor-enter v1

    .line 1075878
    :try_start_0
    iget-object v0, p0, LX/6Jt;->d:Ljava/util/Map;

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Jt;->d:Ljava/util/Map;

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1075879
    :cond_0
    monitor-exit v1

    .line 1075880
    :goto_1
    return-void

    .line 1075881
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1075882
    :cond_2
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    invoke-virtual {v0}, LX/6Km;->e()LX/6Ko;

    move-result-object v2

    .line 1075883
    iput-object p1, v2, LX/6Ko;->a:LX/7Sb;

    .line 1075884
    iget-object v3, v2, LX/6Ko;->b:Ljava/util/Set;

    iget-object v0, p0, LX/6Jt;->d:Ljava/util/Map;

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1075885
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1075886
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    invoke-virtual {v0, v2}, LX/6Km;->a(LX/6Ko;)V

    goto :goto_1

    .line 1075887
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/7Sb;LX/6Jv;)V
    .locals 3

    .prologue
    .line 1075775
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Null param(s) passed in"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1075776
    iget-object v1, p0, LX/6Jt;->d:Ljava/util/Map;

    monitor-enter v1

    .line 1075777
    :try_start_0
    iget-object v0, p0, LX/6Jt;->d:Ljava/util/Map;

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6Jt;->d:Ljava/util/Map;

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1075778
    :cond_0
    sget-object v0, LX/6Jt;->a:Ljava/lang/Class;

    const-string v2, "Received an event for a renderer that wasn\'t registered"

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1075779
    monitor-exit v1

    .line 1075780
    :goto_1
    return-void

    .line 1075781
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1075782
    :cond_2
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    invoke-virtual {v0}, LX/6Km;->e()LX/6Ko;

    move-result-object v0

    .line 1075783
    iput-object p1, v0, LX/6Ko;->a:LX/7Sb;

    .line 1075784
    iget-object v2, v0, LX/6Ko;->b:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1075785
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1075786
    iget-object v1, p0, LX/6Jt;->c:LX/6Km;

    invoke-virtual {v1, v0}, LX/6Km;->a(LX/6Ko;)V

    goto :goto_1

    .line 1075787
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Landroid/view/SurfaceView;)V
    .locals 2

    .prologue
    .line 1075788
    iget-object v0, p0, LX/6Jt;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1075789
    new-instance v0, LX/6Kv;

    invoke-direct {v0, p1}, LX/6Kv;-><init>(Landroid/view/SurfaceView;)V

    .line 1075790
    iget-object v1, p0, LX/6Jt;->j:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1075791
    invoke-virtual {p0, v0}, LX/6Jt;->a(LX/6Kd;)V

    .line 1075792
    return-void

    .line 1075793
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1075794
    iget-object v0, p0, LX/6Jt;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Kd;

    .line 1075795
    if-nez v0, :cond_0

    .line 1075796
    :goto_0
    return-void

    .line 1075797
    :cond_0
    invoke-virtual {p0, v0}, LX/6Jt;->b(LX/6Kd;)V

    .line 1075798
    iget-object v0, p0, LX/6Jt;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/io/File;LX/6JG;)V
    .locals 2

    .prologue
    .line 1075799
    invoke-virtual {p0}, LX/6Jt;->f()V

    .line 1075800
    new-instance v0, LX/6Jn;

    invoke-direct {v0, p0, p2}, LX/6Jn;-><init>(LX/6Jt;LX/6JG;)V

    .line 1075801
    iget-object v1, p0, LX/6Jt;->g:LX/6Jx;

    invoke-interface {v1, p1, v0}, LX/6Jx;->a(Ljava/io/File;LX/6JG;)V

    .line 1075802
    return-void
.end method

.method public final a(Ljava/io/File;Landroid/view/View;LX/6JI;)V
    .locals 6
    .param p3    # LX/6JI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1075803
    iget-object v0, p0, LX/6Jt;->j:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Kd;

    .line 1075804
    const-string v1, "No video output found for taking a photo."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1075805
    new-instance v1, LX/6JR;

    invoke-interface {v0}, LX/6Kd;->getWidth()I

    move-result v2

    invoke-interface {v0}, LX/6Kd;->getHeight()I

    move-result v0

    invoke-direct {v1, v2, v0}, LX/6JR;-><init>(II)V

    const/4 v5, 0x1

    .line 1075806
    iget-boolean v0, p0, LX/6Jt;->r:Z

    if-eqz v0, :cond_1

    .line 1075807
    :cond_0
    :goto_0
    return-void

    .line 1075808
    :cond_1
    invoke-virtual {p0}, LX/6Jt;->f()V

    .line 1075809
    iput-boolean v5, p0, LX/6Jt;->r:Z

    .line 1075810
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, LX/6Jt;->b(I)V

    .line 1075811
    iget-object v0, p0, LX/6Jt;->q:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Kt;

    .line 1075812
    if-eqz v0, :cond_3

    .line 1075813
    :goto_1
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, LX/6Jt;->q:Ljava/lang/ref/WeakReference;

    .line 1075814
    iget-object v2, p0, LX/6Jt;->i:Ljava/util/concurrent/ExecutorService;

    iget v3, v1, LX/6JR;->a:I

    iget v4, v1, LX/6JR;->b:I

    const/4 v1, 0x0

    .line 1075815
    iput-object v2, v0, LX/6Kt;->e:Ljava/util/concurrent/ExecutorService;

    .line 1075816
    iput v3, v0, LX/6Kt;->g:I

    .line 1075817
    iput v4, v0, LX/6Kt;->h:I

    .line 1075818
    iget-object p2, v0, LX/6Kt;->f:[F

    invoke-static {p2, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1075819
    iget-object p2, v0, LX/6Kt;->f:[F

    invoke-static {p2}, LX/61I;->a([F)V

    .line 1075820
    iput-boolean v1, v0, LX/6Kt;->k:Z

    .line 1075821
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1075822
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075823
    iget-object v3, p0, LX/6Jt;->c:LX/6Km;

    invoke-virtual {v3, v2}, LX/6Km;->c(Ljava/util/List;)V

    .line 1075824
    new-instance v3, LX/6Jm;

    invoke-direct {v3, p0, p3, v2}, LX/6Jm;-><init>(LX/6Jt;LX/6JI;Ljava/util/List;)V

    .line 1075825
    iget-object v2, v0, LX/6Kt;->m:Ljava/io/File;

    if-nez v2, :cond_2

    .line 1075826
    iput-object v3, v0, LX/6Kt;->n:LX/6Jm;

    .line 1075827
    iput-object p1, v0, LX/6Kt;->m:Ljava/io/File;

    .line 1075828
    :cond_2
    if-eqz p3, :cond_0

    .line 1075829
    invoke-interface {p3}, LX/6JG;->a()V

    goto :goto_0

    .line 1075830
    :cond_3
    new-instance v0, LX/6Kt;

    invoke-direct {v0}, LX/6Kt;-><init>()V

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/6KY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1075831
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6KY;

    .line 1075832
    iget-object v2, p0, LX/6Jt;->e:LX/6Js;

    .line 1075833
    iget-object v3, v0, LX/6KY;->a:LX/61B;

    instance-of v3, v3, LX/6Jv;

    if-eqz v3, :cond_0

    .line 1075834
    iget-object v3, v0, LX/6KY;->a:LX/61B;

    check-cast v3, LX/6Jv;

    invoke-interface {v3, v2}, LX/6Jv;->a(LX/6Js;)V

    .line 1075835
    :cond_0
    iget-object v2, p0, LX/6Jt;->m:LX/61H;

    .line 1075836
    iput-object v2, v0, LX/6KY;->d:LX/61H;

    .line 1075837
    goto :goto_0

    .line 1075838
    :cond_1
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    .line 1075839
    const/16 v1, 0xf

    invoke-static {v0, v1, p1}, LX/6Km;->a$redex0(LX/6Km;ILjava/lang/Object;)V

    .line 1075840
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1075841
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    .line 1075842
    iget-object v1, v0, LX/6Km;->l:LX/6Kj;

    const/4 p0, 0x6

    invoke-virtual {v1, p0}, LX/6Kj;->sendEmptyMessage(I)Z

    .line 1075843
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1075844
    iget-object v0, p0, LX/6Jt;->h:LX/6KN;

    if-eqz v0, :cond_0

    .line 1075845
    iget-object v0, p0, LX/6Jt;->h:LX/6KN;

    iget-object v1, p0, LX/6Jt;->c:LX/6Km;

    .line 1075846
    iget-object p0, v1, LX/6Km;->o:Ljava/lang/String;

    move-object v1, p0

    .line 1075847
    invoke-virtual {v0, p1, v1}, LX/6KN;->a(ILjava/lang/String;)V

    .line 1075848
    :cond_0
    return-void
.end method

.method public final b(LX/6Kd;)V
    .locals 2

    .prologue
    .line 1075849
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 1075850
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1075851
    iget-object v1, p0, LX/6Jt;->c:LX/6Km;

    invoke-virtual {v1, v0}, LX/6Km;->b(Ljava/util/List;)V

    .line 1075852
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1075853
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    .line 1075854
    iget-object v1, v0, LX/6Km;->l:LX/6Kj;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, LX/6Kj;->sendEmptyMessage(I)Z

    .line 1075855
    invoke-virtual {p0}, LX/6Jt;->e()V

    .line 1075856
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 1075857
    iget-object v0, p0, LX/6Jt;->h:LX/6KN;

    if-eqz v0, :cond_0

    .line 1075858
    iget-object v0, p0, LX/6Jt;->h:LX/6KN;

    invoke-virtual {v0, p1}, LX/6KN;->a(I)V

    .line 1075859
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1075860
    iget-object v0, p0, LX/6Jt;->c:LX/6Km;

    .line 1075861
    iget-object v1, v0, LX/6Km;->l:LX/6Kj;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, LX/6Kj;->sendEmptyMessage(I)Z

    .line 1075862
    const/4 v0, 0x0

    iput-object v0, p0, LX/6Jt;->o:LX/6Jh;

    .line 1075863
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 1075864
    iget-object v0, p0, LX/6Jt;->h:LX/6KN;

    if-eqz v0, :cond_0

    .line 1075865
    iget-object v0, p0, LX/6Jt;->h:LX/6KN;

    invoke-virtual {v0, p1}, LX/6KN;->b(I)V

    .line 1075866
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1075867
    iget-object v0, p0, LX/6Jt;->g:LX/6Jx;

    invoke-interface {v0}, LX/6Jx;->b()V

    .line 1075868
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    if-eqz v0, :cond_0

    .line 1075869
    iget-object v0, p0, LX/6Jt;->n:LX/6Jk;

    .line 1075870
    iget-object p0, v0, LX/6Jk;->c:LX/6Ji;

    .line 1075871
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6Ji;->d:Z

    .line 1075872
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1075873
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/6Jt;->a$redex0(LX/6Jt;Ljava/util/Map;)V

    .line 1075874
    return-void
.end method
