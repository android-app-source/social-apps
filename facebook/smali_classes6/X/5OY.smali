.class public final LX/5OY;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public final synthetic a:LX/0ht;

.field private b:Z


# direct methods
.method public constructor <init>(LX/0ht;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 909082
    iput-object p1, p0, LX/5OY;->a:LX/0ht;

    .line 909083
    invoke-direct {p0, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 909084
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5OY;->b:Z

    .line 909085
    return-void
.end method


# virtual methods
.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 909086
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    .line 909087
    if-nez v1, :cond_0

    .line 909088
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, LX/5OY;->a:LX/0ht;

    iget-boolean v2, v2, LX/0ht;->w:Z

    if-eqz v2, :cond_0

    .line 909089
    iget-object v1, p0, LX/5OY;->a:LX/0ht;

    invoke-static {v1}, LX/0ht;->w(LX/0ht;)V

    .line 909090
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 909091
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 909092
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/5OY;->b:Z

    .line 909093
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 909094
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 909095
    iget-boolean v0, p0, LX/5OY;->b:Z

    if-eqz v0, :cond_0

    .line 909096
    iget-object v0, p0, LX/5OY;->a:LX/0ht;

    invoke-static {v0}, LX/0ht;->x(LX/0ht;)V

    .line 909097
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5OY;->b:Z

    .line 909098
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x58742e0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 909099
    iget-object v1, p0, LX/5OY;->a:LX/0ht;

    iget-object v1, v1, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    if-nez v1, :cond_0

    .line 909100
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, 0x2f4b5d4a

    invoke-static {v3, v3, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 909101
    :goto_0
    return v0

    .line 909102
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-nez v1, :cond_3

    .line 909103
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 909104
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 909105
    iget-object v4, p0, LX/5OY;->a:LX/0ht;

    iget-object v4, v4, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v4}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLeft()I

    move-result v4

    .line 909106
    iget-object v5, p0, LX/5OY;->a:LX/0ht;

    iget-object v5, v5, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v5}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getTop()I

    move-result v5

    .line 909107
    iget-object v6, p0, LX/5OY;->a:LX/0ht;

    iget-object v6, v6, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingTop()I

    move-result v6

    add-int/2addr v6, v5

    if-lt v3, v6, :cond_1

    iget-object v6, p0, LX/5OY;->a:LX/0ht;

    iget-object v6, v6, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, LX/5OY;->a:LX/0ht;

    iget-object v6, v6, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v6}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    if-ge v3, v5, :cond_1

    iget-object v3, p0, LX/5OY;->a:LX/0ht;

    iget-object v3, v3, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v3}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingLeft()I

    move-result v3

    add-int/2addr v3, v4

    if-lt v1, v3, :cond_1

    iget-object v3, p0, LX/5OY;->a:LX/0ht;

    iget-object v3, v3, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v3}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getWidth()I

    move-result v3

    add-int/2addr v3, v4

    iget-object v4, p0, LX/5OY;->a:LX/0ht;

    iget-object v4, v4, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v4}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    if-lt v1, v3, :cond_2

    :cond_1
    move v1, v0

    .line 909108
    :goto_1
    if-eqz v1, :cond_5

    iget-object v1, p0, LX/5OY;->a:LX/0ht;

    iget-boolean v1, v1, LX/0ht;->x:Z

    if-eqz v1, :cond_5

    .line 909109
    iget-object v1, p0, LX/5OY;->a:LX/0ht;

    invoke-static {v1}, LX/0ht;->w(LX/0ht;)V

    .line 909110
    const v1, -0x227cbf8a

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 909111
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 909112
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_5

    iget-object v1, p0, LX/5OY;->a:LX/0ht;

    iget-boolean v1, v1, LX/0ht;->x:Z

    if-eqz v1, :cond_5

    .line 909113
    iget-object v1, p0, LX/5OY;->a:LX/0ht;

    invoke-static {v1}, LX/0ht;->w(LX/0ht;)V

    .line 909114
    iget-object v1, p0, LX/5OY;->a:LX/0ht;

    iget-boolean v1, v1, LX/0ht;->b:Z

    if-eqz v1, :cond_4

    :goto_2
    const v1, -0x3397e07c    # -6.084968E7f

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto/16 :goto_0

    :cond_4
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_2

    .line 909115
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, -0x16e6dd1b

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method
