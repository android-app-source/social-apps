.class public LX/6JQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:[B
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:[LX/6JP;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1075478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075479
    const/4 v0, 0x0

    iput-object v0, p0, LX/6JQ;->a:[B

    .line 1075480
    new-array v0, p1, [LX/6JP;

    iput-object v0, p0, LX/6JQ;->b:[LX/6JP;

    .line 1075481
    return-void
.end method


# virtual methods
.method public final a(ILjava/nio/ByteBuffer;II)V
    .locals 3

    .prologue
    .line 1075482
    iget-object v0, p0, LX/6JQ;->b:[LX/6JP;

    array-length v0, v0

    if-lt p1, v0, :cond_0

    .line 1075483
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "planeIndex is greater then the number of planes available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1075484
    :cond_0
    iget-object v0, p0, LX/6JQ;->b:[LX/6JP;

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    .line 1075485
    iget-object v0, p0, LX/6JQ;->b:[LX/6JP;

    new-instance v1, LX/6JP;

    invoke-direct {v1}, LX/6JP;-><init>()V

    aput-object v1, v0, p1

    .line 1075486
    :cond_1
    iget-object v0, p0, LX/6JQ;->b:[LX/6JP;

    aget-object v0, v0, p1

    .line 1075487
    iput-object p2, v0, LX/6JP;->a:Ljava/nio/ByteBuffer;

    .line 1075488
    iget-object v0, p0, LX/6JQ;->b:[LX/6JP;

    aget-object v0, v0, p1

    .line 1075489
    iput p3, v0, LX/6JP;->b:I

    .line 1075490
    iget-object v0, p0, LX/6JQ;->b:[LX/6JP;

    aget-object v0, v0, p1

    .line 1075491
    iput p4, v0, LX/6JP;->c:I

    .line 1075492
    return-void
.end method
