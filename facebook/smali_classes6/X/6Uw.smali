.class public final LX/6Uw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/6V2;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/6V2;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1103255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103256
    iput-object p1, p0, LX/6Uw;->a:LX/0QB;

    .line 1103257
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1103254
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/6Uw;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1103232
    packed-switch p2, :pswitch_data_0

    .line 1103233
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1103234
    :pswitch_0
    new-instance v0, LX/JI2;

    invoke-direct {v0}, LX/JI2;-><init>()V

    .line 1103235
    move-object v0, v0

    .line 1103236
    move-object v0, v0

    .line 1103237
    :goto_0
    return-object v0

    .line 1103238
    :pswitch_1
    new-instance p0, LX/6V5;

    invoke-static {p1}, LX/6Uv;->a(LX/0QB;)LX/6Uv;

    move-result-object v0

    check-cast v0, LX/6Uv;

    invoke-static {p1}, LX/6V4;->a(LX/0QB;)LX/6V4;

    move-result-object v1

    check-cast v1, LX/6V4;

    invoke-direct {p0, v0, v1}, LX/6V5;-><init>(LX/6Uv;LX/6V4;)V

    .line 1103239
    move-object v0, p0

    .line 1103240
    goto :goto_0

    .line 1103241
    :pswitch_2
    new-instance v0, LX/6V6;

    invoke-direct {v0}, LX/6V6;-><init>()V

    .line 1103242
    move-object v0, v0

    .line 1103243
    move-object v0, v0

    .line 1103244
    goto :goto_0

    .line 1103245
    :pswitch_3
    new-instance v0, LX/6V7;

    invoke-direct {v0}, LX/6V7;-><init>()V

    .line 1103246
    move-object v0, v0

    .line 1103247
    move-object v0, v0

    .line 1103248
    goto :goto_0

    .line 1103249
    :pswitch_4
    new-instance v0, LX/6V8;

    invoke-direct {v0}, LX/6V8;-><init>()V

    .line 1103250
    move-object v0, v0

    .line 1103251
    move-object v0, v0

    .line 1103252
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1103253
    const/4 v0, 0x5

    return v0
.end method
