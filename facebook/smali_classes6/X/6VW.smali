.class public final LX/6VW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1104124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/6VW;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, LX/6VW;->a:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x4eaac078

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1104112
    invoke-static {p2}, Lcom/facebook/feed/platformads/AppInstallReceiver;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 1104113
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1104114
    const/16 v1, 0x27

    const v2, 0x4eea9306

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1104115
    :goto_0
    return-void

    .line 1104116
    :cond_0
    const-string v2, "android.intent.extra.REPLACING"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1104117
    const v1, -0x5ce8bdf3

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    .line 1104118
    :cond_1
    invoke-static {p0, p1}, LX/6VW;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1104119
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/feed/platformads/AppInstallService;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1104120
    const-string v3, "package_name"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1104121
    const-string v1, "action_type"

    const-string v3, "uninstall"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1104122
    iget-object v1, p0, LX/6VW;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v2, p1}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 1104123
    const v1, -0x697b6f37

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
