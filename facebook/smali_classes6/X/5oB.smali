.class public final LX/5oB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1006218
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1006219
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1006220
    :goto_0
    return v1

    .line 1006221
    :cond_0
    const-string v11, "has_location_constraints"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1006222
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    .line 1006223
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 1006224
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1006225
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1006226
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1006227
    const-string v11, "attribution_text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1006228
    invoke-static {p0, p1}, LX/5o8;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1006229
    :cond_2
    const-string v11, "attribution_thumbnail"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1006230
    invoke-static {p0, p1}, LX/5o9;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1006231
    :cond_3
    const-string v11, "best_mask_package"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1006232
    invoke-static {p0, p1}, LX/5jR;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1006233
    :cond_4
    const-string v11, "instructions"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1006234
    invoke-static {p0, p1}, LX/5oA;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1006235
    :cond_5
    const-string v11, "is_logging_disabled"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1006236
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1006237
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1006238
    :cond_7
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1006239
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 1006240
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1006241
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1006242
    if-eqz v3, :cond_8

    .line 1006243
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 1006244
    :cond_8
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1006245
    if-eqz v0, :cond_9

    .line 1006246
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 1006247
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1006248
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1006249
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006250
    if-eqz v0, :cond_0

    .line 1006251
    const-string v1, "attribution_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006252
    invoke-static {p0, v0, p2}, LX/5o8;->a(LX/15i;ILX/0nX;)V

    .line 1006253
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006254
    if-eqz v0, :cond_1

    .line 1006255
    const-string v1, "attribution_thumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006256
    invoke-static {p0, v0, p2}, LX/5o9;->a(LX/15i;ILX/0nX;)V

    .line 1006257
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006258
    if-eqz v0, :cond_2

    .line 1006259
    const-string v1, "best_mask_package"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006260
    invoke-static {p0, v0, p2, p3}, LX/5jR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1006261
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1006262
    if-eqz v0, :cond_3

    .line 1006263
    const-string v1, "has_location_constraints"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006264
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1006265
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1006266
    if-eqz v0, :cond_4

    .line 1006267
    const-string v1, "instructions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006268
    invoke-static {p0, v0, p2}, LX/5oA;->a(LX/15i;ILX/0nX;)V

    .line 1006269
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1006270
    if-eqz v0, :cond_5

    .line 1006271
    const-string v1, "is_logging_disabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1006272
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1006273
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1006274
    return-void
.end method
