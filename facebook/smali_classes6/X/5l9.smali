.class public final LX/5l9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 996924
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_a

    .line 996925
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 996926
    :goto_0
    return v1

    .line 996927
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 996928
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 996929
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 996930
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 996931
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 996932
    const-string v9, "application"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 996933
    invoke-static {p0, p1}, LX/5l4;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 996934
    :cond_2
    const-string v9, "attachments"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 996935
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 996936
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, v9, :cond_3

    .line 996937
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, v9, :cond_3

    .line 996938
    invoke-static {p0, p1}, LX/5l6;->b(LX/15w;LX/186;)I

    move-result v8

    .line 996939
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 996940
    :cond_3
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 996941
    goto :goto_1

    .line 996942
    :cond_4
    const-string v9, "cache_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 996943
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 996944
    :cond_5
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 996945
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 996946
    :cond_6
    const-string v9, "legacy_api_story_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 996947
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 996948
    :cond_7
    const-string v9, "sponsored_data"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 996949
    invoke-static {p0, p1}, LX/5l8;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 996950
    :cond_8
    const-string v9, "tracking"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 996951
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 996952
    :cond_9
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 996953
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 996954
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 996955
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 996956
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 996957
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 996958
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 996959
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 996960
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 996961
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 996962
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 996963
    if-eqz v0, :cond_0

    .line 996964
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996965
    invoke-static {p0, v0, p2}, LX/5l4;->a(LX/15i;ILX/0nX;)V

    .line 996966
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 996967
    if-eqz v0, :cond_2

    .line 996968
    const-string v1, "attachments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996969
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 996970
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 996971
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/5l6;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 996972
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 996973
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 996974
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 996975
    if-eqz v0, :cond_3

    .line 996976
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996977
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 996978
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 996979
    if-eqz v0, :cond_4

    .line 996980
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996981
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 996982
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 996983
    if-eqz v0, :cond_5

    .line 996984
    const-string v1, "legacy_api_story_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996985
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 996986
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 996987
    if-eqz v0, :cond_6

    .line 996988
    const-string v1, "sponsored_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996989
    invoke-static {p0, v0, p2, p3}, LX/5l8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 996990
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 996991
    if-eqz v0, :cond_7

    .line 996992
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 996993
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 996994
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 996995
    return-void
.end method
