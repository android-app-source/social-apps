.class public LX/6O0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/contacts/server/UploadBulkContactsParams;",
        "Lcom/facebook/contacts/server/UploadBulkContactsResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/6Or;

.field public final d:LX/0lp;

.field private final e:Landroid/telephony/TelephonyManager;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1084290
    const-class v0, LX/6O0;

    sput-object v0, LX/6O0;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/6Or;LX/0lp;Landroid/telephony/TelephonyManager;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/contacts/protocol/annotations/IsMessengerEmailUploadEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/6Or;",
            "LX/0lp;",
            "Landroid/telephony/TelephonyManager;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1084283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1084284
    iput-object p1, p0, LX/6O0;->b:Landroid/content/Context;

    .line 1084285
    iput-object p2, p0, LX/6O0;->c:LX/6Or;

    .line 1084286
    iput-object p3, p0, LX/6O0;->d:LX/0lp;

    .line 1084287
    iput-object p4, p0, LX/6O0;->e:Landroid/telephony/TelephonyManager;

    .line 1084288
    iput-object p5, p0, LX/6O0;->f:LX/0Or;

    .line 1084289
    return-void
.end method

.method public static a(LX/0QB;)LX/6O0;
    .locals 7

    .prologue
    .line 1084125
    new-instance v1, LX/6O0;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/6Or;->b(LX/0QB;)LX/6Or;

    move-result-object v3

    check-cast v3, LX/6Or;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v4

    check-cast v4, LX/0lp;

    invoke-static {p0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    const/16 v6, 0x30a

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, LX/6O0;-><init>(Landroid/content/Context;LX/6Or;LX/0lp;Landroid/telephony/TelephonyManager;LX/0Or;)V

    .line 1084126
    move-object v0, v1

    .line 1084127
    return-object v0
.end method

.method private static a(LX/1pN;)Lcom/facebook/contacts/server/UploadBulkContactsResult;
    .locals 13

    .prologue
    .line 1084224
    invoke-virtual {p0}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1084225
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1084226
    const-string v1, "import_id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 1084227
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1084228
    const-string v1, "contact_changes"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1084229
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v8

    .line 1084230
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1084231
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1084232
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1084233
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1084234
    const-string v1, "update_type"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 1084235
    const-string v3, "add"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1084236
    sget-object v1, LX/6OQ;->ADD:LX/6OQ;

    .line 1084237
    :goto_1
    const-string v3, "contact"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 1084238
    const-string v4, "id"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v3

    .line 1084239
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1084240
    const-string v4, "field_matches"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 1084241
    invoke-virtual {v4}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0lF;

    .line 1084242
    const-string v5, "match_type"

    invoke-virtual {v4, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 1084243
    const-string v11, "hard"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1084244
    sget-object v5, LX/6OS;->HARD:LX/6OS;

    .line 1084245
    :goto_3
    const-string v11, "value_type"

    invoke-virtual {v4, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v4

    .line 1084246
    const-string v11, "name"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1084247
    sget-object v4, LX/6OT;->NAME:LX/6OT;

    .line 1084248
    :goto_4
    new-instance v11, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;

    invoke-direct {v11, v5, v4}, Lcom/facebook/contacts/server/UploadBulkContactFieldMatch;-><init>(LX/6OS;LX/6OT;)V

    invoke-virtual {v9, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1084249
    :cond_0
    const-string v3, "modify"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1084250
    sget-object v1, LX/6OQ;->MODIFY:LX/6OQ;

    goto :goto_1

    .line 1084251
    :cond_1
    const-string v3, "remove"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1084252
    sget-object v1, LX/6OQ;->REMOVE:LX/6OQ;

    goto :goto_1

    .line 1084253
    :cond_2
    const-string v3, "none"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1084254
    sget-object v1, LX/6OQ;->NONE:LX/6OQ;

    goto :goto_1

    .line 1084255
    :cond_3
    sget-object v0, LX/6O0;->a:Ljava/lang/Class;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized contact change type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", skipping"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1084256
    :cond_4
    const-string v11, "soft"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1084257
    sget-object v5, LX/6OS;->SOFT:LX/6OS;

    goto :goto_3

    .line 1084258
    :cond_5
    sget-object v4, LX/6O0;->a:Ljava/lang/Class;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Unrecognized contact field match type: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, ", skipping"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1084259
    :cond_6
    const-string v11, "email"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1084260
    sget-object v4, LX/6OT;->EMAIL:LX/6OT;

    goto :goto_4

    .line 1084261
    :cond_7
    const-string v11, "phone"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1084262
    sget-object v4, LX/6OT;->PHONE:LX/6OT;

    goto/16 :goto_4

    .line 1084263
    :cond_8
    const-string v11, "email_public_hash"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1084264
    sget-object v4, LX/6OT;->EMAIL_PUBLIC_HASH:LX/6OT;

    goto/16 :goto_4

    .line 1084265
    :cond_9
    const-string v11, "phone_public_hash"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 1084266
    sget-object v4, LX/6OT;->PHONE_PUBLIC_HASH:LX/6OT;

    goto/16 :goto_4

    .line 1084267
    :cond_a
    sget-object v5, LX/6O0;->a:Ljava/lang/Class;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Unrecognized contact field value type: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, ", skipping"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1084268
    :cond_b
    const-string v4, "match_confidence"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 1084269
    const-string v4, "high"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1084270
    sget-object v5, LX/6OP;->HIGH:LX/6OP;

    .line 1084271
    :goto_5
    new-instance v0, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/contacts/server/UploadBulkContactChangeResult;-><init>(LX/6OQ;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/6OP;)V

    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 1084272
    :cond_c
    const-string v4, "medium"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1084273
    sget-object v5, LX/6OP;->MEDIUM:LX/6OP;

    goto :goto_5

    .line 1084274
    :cond_d
    const-string v4, "low"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1084275
    sget-object v5, LX/6OP;->LOW:LX/6OP;

    goto :goto_5

    .line 1084276
    :cond_e
    const-string v4, "very_low"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1084277
    sget-object v5, LX/6OP;->VERY_LOW:LX/6OP;

    goto :goto_5

    .line 1084278
    :cond_f
    const-string v4, "unknown"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1084279
    sget-object v5, LX/6OP;->UNKNOWN:LX/6OP;

    goto :goto_5

    .line 1084280
    :cond_10
    sget-object v4, LX/6O0;->a:Ljava/lang/Class;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v10, "Unrecognized confidence type: "

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1084281
    sget-object v5, LX/6OP;->UNKNOWN:LX/6OP;

    goto :goto_5

    .line 1084282
    :cond_11
    new-instance v0, Lcom/facebook/contacts/server/UploadBulkContactsResult;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v1, v6

    invoke-direct/range {v0 .. v5}, Lcom/facebook/contacts/server/UploadBulkContactsResult;-><init>(Ljava/lang/String;LX/0Px;LX/0ta;J)V

    return-object v0
.end method

.method public static a(LX/6O0;Lcom/facebook/contacts/model/PhonebookContact;LX/0nX;)V
    .locals 4

    .prologue
    .line 1084184
    const-string v0, "contact"

    invoke-virtual {p2, v0}, LX/0nX;->g(Ljava/lang/String;)V

    .line 1084185
    const-string v0, "name"

    invoke-virtual {p2, v0}, LX/0nX;->g(Ljava/lang/String;)V

    .line 1084186
    const-string v0, "formatted"

    iget-object v1, p1, Lcom/facebook/contacts/model/PhonebookContact;->b:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084187
    iget-object v0, p1, Lcom/facebook/contacts/model/PhonebookContact;->c:Ljava/lang/String;

    .line 1084188
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1084189
    const-string v1, "first"

    invoke-virtual {p2, v1, v0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084190
    :cond_0
    iget-object v0, p1, Lcom/facebook/contacts/model/PhonebookContact;->d:Ljava/lang/String;

    .line 1084191
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1084192
    const-string v1, "last"

    invoke-virtual {p2, v1, v0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084193
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1084194
    iget-object v0, p1, Lcom/facebook/contacts/model/PhonebookContact;->m:LX/0Px;

    .line 1084195
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1084196
    const-string v1, "phones"

    invoke-virtual {p2, v1}, LX/0nX;->f(Ljava/lang/String;)V

    .line 1084197
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookPhoneNumber;

    .line 1084198
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1084199
    const-string v2, "type"

    invoke-virtual {v0}, Lcom/facebook/contacts/model/PhonebookPhoneNumber;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084200
    const-string v2, "number"

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookPhoneNumber;->a:Ljava/lang/String;

    invoke-virtual {p2, v2, v0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084201
    invoke-virtual {p2}, LX/0nX;->g()V

    goto :goto_0

    .line 1084202
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1084203
    :cond_3
    iget-object v0, p0, LX/6O0;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1084204
    iget-object v0, p1, Lcom/facebook/contacts/model/PhonebookContact;->n:LX/0Px;

    .line 1084205
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1084206
    const-string v1, "emails"

    invoke-virtual {p2, v1}, LX/0nX;->f(Ljava/lang/String;)V

    .line 1084207
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/model/PhonebookEmailAddress;

    .line 1084208
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1084209
    const-string v2, "type"

    .line 1084210
    iget v3, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    const/4 p0, 0x1

    if-ne v3, p0, :cond_6

    .line 1084211
    const-string v3, "home"

    .line 1084212
    :goto_2
    move-object v3, v3

    .line 1084213
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084214
    const-string v2, "email"

    iget-object v0, v0, Lcom/facebook/contacts/model/PhonebookEmailAddress;->a:Ljava/lang/String;

    invoke-virtual {p2, v2, v0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084215
    invoke-virtual {p2}, LX/0nX;->g()V

    goto :goto_1

    .line 1084216
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1084217
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1084218
    return-void

    .line 1084219
    :cond_6
    iget v3, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    const/4 p0, 0x2

    if-ne v3, p0, :cond_7

    .line 1084220
    const-string v3, "work"

    goto :goto_2

    .line 1084221
    :cond_7
    iget v3, v0, Lcom/facebook/contacts/model/PhonebookContactField;->i:I

    const/4 p0, 0x4

    if-ne v3, p0, :cond_8

    .line 1084222
    const-string v3, "mobile"

    goto :goto_2

    .line 1084223
    :cond_8
    const-string v3, "other"

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 12

    .prologue
    .line 1084129
    check-cast p1, Lcom/facebook/contacts/server/UploadBulkContactsParams;

    .line 1084130
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1084131
    iget-object v0, p1, Lcom/facebook/contacts/server/UploadBulkContactsParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1084132
    if-eqz v0, :cond_0

    .line 1084133
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "import_id"

    .line 1084134
    iget-object v2, p1, Lcom/facebook/contacts/server/UploadBulkContactsParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1084135
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084136
    :cond_0
    iget-object v0, p0, LX/6O0;->e:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 1084137
    iget-object v1, p0, LX/6O0;->e:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 1084138
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1084139
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "sim_country"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084140
    :cond_1
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1084141
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "network_country"

    invoke-direct {v0, v2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084142
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "contact_changes"

    .line 1084143
    iget-object v2, p1, Lcom/facebook/contacts/server/UploadBulkContactsParams;->b:LX/0Px;

    move-object v2, v2

    .line 1084144
    new-instance v6, Ljava/io/StringWriter;

    invoke-direct {v6}, Ljava/io/StringWriter;-><init>()V

    .line 1084145
    iget-object v3, p0, LX/6O0;->d:LX/0lp;

    invoke-virtual {v3, v6}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v7

    .line 1084146
    invoke-virtual {v7}, LX/0nX;->d()V

    .line 1084147
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, v8, :cond_3

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/contacts/server/UploadBulkContactChange;

    .line 1084148
    invoke-virtual {v7}, LX/0nX;->f()V

    .line 1084149
    const-string v9, "client_contact_id"

    .line 1084150
    iget-object v10, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->a:Ljava/lang/String;

    move-object v10, v10

    .line 1084151
    invoke-virtual {v7, v9, v10}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084152
    const-string v9, "update_type"

    .line 1084153
    sget-object v10, LX/6Nz;->a:[I

    .line 1084154
    iget-object v11, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->d:LX/6ON;

    move-object v11, v11

    .line 1084155
    invoke-virtual {v11}, LX/6ON;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 1084156
    const/4 v10, 0x0

    :goto_1
    move-object v10, v10

    .line 1084157
    invoke-virtual {v7, v9, v10}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084158
    iget-object v9, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->d:LX/6ON;

    move-object v9, v9

    .line 1084159
    sget-object v10, LX/6ON;->DELETE:LX/6ON;

    if-eq v9, v10, :cond_4

    .line 1084160
    iget-object v9, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->c:Lcom/facebook/contacts/model/PhonebookContact;

    move-object v9, v9

    .line 1084161
    invoke-static {p0, v9, v7}, LX/6O0;->a(LX/6O0;Lcom/facebook/contacts/model/PhonebookContact;LX/0nX;)V

    .line 1084162
    :goto_2
    invoke-virtual {v7}, LX/0nX;->g()V

    .line 1084163
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 1084164
    :cond_3
    invoke-virtual {v7}, LX/0nX;->e()V

    .line 1084165
    invoke-virtual {v7}, LX/0nX;->flush()V

    .line 1084166
    invoke-virtual {v6}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1084167
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084168
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "contacts_surface"

    .line 1084169
    iget-object v2, p1, Lcom/facebook/contacts/server/UploadBulkContactsParams;->c:Lcom/facebook/contacts/ContactSurface;

    move-object v2, v2

    .line 1084170
    invoke-virtual {v2}, Lcom/facebook/contacts/ContactSurface;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084171
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "android_id"

    iget-object v2, p0, LX/6O0;->b:Landroid/content/Context;

    invoke-static {v2}, LX/6Or;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084172
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "phone_id"

    iget-object v2, p0, LX/6O0;->c:LX/6Or;

    invoke-virtual {v2}, LX/6Or;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084173
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Uploading contacts: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1084174
    new-instance v0, LX/14N;

    const-string v1, "graphUploadBulkContacts"

    const-string v2, "POST"

    const-string v3, "me/bulkcontacts"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 1084175
    :cond_4
    new-instance v9, LX/6NG;

    .line 1084176
    iget-object v10, v3, Lcom/facebook/contacts/server/UploadBulkContactChange;->a:Ljava/lang/String;

    move-object v10, v10

    .line 1084177
    invoke-direct {v9, v10}, LX/6NG;-><init>(Ljava/lang/String;)V

    const-string v10, "None"

    .line 1084178
    iput-object v10, v9, LX/6NG;->b:Ljava/lang/String;

    .line 1084179
    move-object v9, v9

    .line 1084180
    invoke-virtual {v9}, LX/6NG;->c()Lcom/facebook/contacts/model/PhonebookContact;

    move-result-object v9

    invoke-static {p0, v9, v7}, LX/6O0;->a(LX/6O0;Lcom/facebook/contacts/model/PhonebookContact;LX/0nX;)V

    goto :goto_2

    .line 1084181
    :pswitch_0
    const-string v10, "add"

    goto/16 :goto_1

    .line 1084182
    :pswitch_1
    const-string v10, "modify"

    goto/16 :goto_1

    .line 1084183
    :pswitch_2
    const-string v10, "delete"

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1084128
    invoke-static {p2}, LX/6O0;->a(LX/1pN;)Lcom/facebook/contacts/server/UploadBulkContactsResult;

    move-result-object v0

    return-object v0
.end method
