.class public abstract LX/6Ya;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

.field public b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1110265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1110261
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1110262
    const-string v1, "promo_id"

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110263
    const-string v1, "location"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110264
    return-object v0
.end method


# virtual methods
.method public final a(LX/6YN;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1110260
    new-instance v0, LX/6YZ;

    invoke-direct {v0, p0, p1}, LX/6YZ;-><init>(LX/6Ya;LX/6YN;)V

    return-object v0
.end method

.method public abstract a(Landroid/content/Context;)Landroid/view/View;
.end method

.method public a()V
    .locals 0

    .prologue
    .line 1110259
    return-void
.end method

.method public a(LX/6YP;)V
    .locals 0

    .prologue
    .line 1110258
    return-void
.end method

.method public final a(Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;Lcom/facebook/iorg/common/upsell/model/PromoDataModel;)V
    .locals 0

    .prologue
    .line 1110255
    iput-object p1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110256
    iput-object p2, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110257
    return-void
.end method

.method public final c()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1110254
    new-instance v0, LX/6YX;

    invoke-direct {v0, p0}, LX/6YX;-><init>(LX/6Ya;)V

    return-object v0
.end method

.method public d()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1110253
    new-instance v0, LX/6YY;

    invoke-direct {v0, p0}, LX/6YY;-><init>(LX/6Ya;)V

    return-object v0
.end method

.method public final e()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1110246
    iget-object v0, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    if-eqz v0, :cond_0

    .line 1110247
    iget-object v0, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110248
    iget-object v1, v0, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1110249
    iget-object v1, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110250
    iget-object p0, v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->i:LX/6Y4;

    move-object v1, p0

    .line 1110251
    invoke-virtual {v1}, LX/6Y4;->getParamName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/6Ya;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 1110252
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()LX/6Y5;
    .locals 3

    .prologue
    .line 1110235
    new-instance v0, LX/6Y5;

    invoke-direct {v0}, LX/6Y5;-><init>()V

    .line 1110236
    iget-object v1, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    if-eqz v1, :cond_0

    .line 1110237
    iget-object v1, p0, LX/6Ya;->b:Lcom/facebook/iorg/common/upsell/model/PromoDataModel;

    .line 1110238
    iget-object v2, v1, Lcom/facebook/iorg/common/upsell/model/PromoDataModel;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1110239
    invoke-virtual {v0, v1}, LX/6Y5;->a(Ljava/lang/String;)LX/6Y5;

    .line 1110240
    :cond_0
    iget-object v1, p0, LX/6Ya;->a:Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;

    .line 1110241
    iget-object v2, v1, Lcom/facebook/iorg/common/upsell/ui/UpsellDialogFragment;->u:Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;

    move-object v1, v2

    .line 1110242
    if-eqz v1, :cond_1

    .line 1110243
    iget-object v2, v1, Lcom/facebook/iorg/common/upsell/server/ZeroRecommendedPromoResult;->d:Ljava/lang/String;

    move-object v1, v2

    .line 1110244
    iput-object v1, v0, LX/6Y5;->t:Ljava/lang/String;

    .line 1110245
    :cond_1
    return-object v0
.end method
