.class public final LX/699;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Lcom/facebook/android/maps/model/LatLng;


# instance fields
.field public b:Lcom/facebook/android/maps/model/LatLng;

.field public c:LX/68w;

.field public d:F

.field public e:Z

.field public f:Z

.field public g:F

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:F

.field public k:Z

.field private final l:[F

.field private final m:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1057682
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v0, v2, v3, v2, v3}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    sput-object v0, LX/699;->a:Lcom/facebook/android/maps/model/LatLng;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1057683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1057684
    sget-object v0, LX/699;->a:Lcom/facebook/android/maps/model/LatLng;

    iput-object v0, p0, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 1057685
    const/high16 v0, 0x43700000    # 240.0f

    invoke-static {v0}, LX/690;->a(F)LX/68w;

    move-result-object v0

    move-object v0, v0

    .line 1057686
    iput-object v0, p0, LX/699;->c:LX/68w;

    .line 1057687
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/699;->d:F

    .line 1057688
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/699;->k:Z

    .line 1057689
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/699;->l:[F

    .line 1057690
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, LX/699;->m:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x0
    .end array-data

    .line 1057691
    :array_1
    .array-data 4
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(FF)LX/699;
    .locals 2

    .prologue
    .line 1057692
    iget-object v0, p0, LX/699;->m:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 1057693
    iget-object v0, p0, LX/699;->m:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 1057694
    return-object p0
.end method

.method public final b()F
    .locals 2

    .prologue
    .line 1057695
    iget-object v0, p0, LX/699;->m:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final c()F
    .locals 2

    .prologue
    .line 1057696
    iget-object v0, p0, LX/699;->m:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public final e()F
    .locals 2

    .prologue
    .line 1057697
    iget-object v0, p0, LX/699;->l:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final f()F
    .locals 2

    .prologue
    .line 1057698
    iget-object v0, p0, LX/699;->l:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method
