.class public final LX/6GI;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;)V
    .locals 0

    .prologue
    .line 1070190
    iput-object p1, p0, LX/6GI;->a:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 1070191
    iget-object v0, p0, LX/6GI;->a:Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;

    .line 1070192
    iget-object v1, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1070193
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1070194
    iget-object v1, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->g:LX/0kL;

    new-instance v2, LX/27k;

    const p0, 0x7f0818de

    invoke-direct {v2, p0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1070195
    :goto_0
    return-void

    .line 1070196
    :cond_0
    iget-object v1, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->y:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 1070197
    :goto_1
    new-instance p0, LX/4BY;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 1070198
    const-string p1, "Finalizing Bug Report"

    invoke-virtual {p0, p1}, LX/4BY;->setTitle(Ljava/lang/CharSequence;)V

    .line 1070199
    const-string p1, "Please wait"

    invoke-virtual {p0, p1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1070200
    invoke-virtual {p0}, LX/4BY;->show()V

    .line 1070201
    iget-object p1, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->z:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p2, LX/6GK;

    invoke-direct {p2, v0, v1, v2, p0}, LX/6GK;-><init>(Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;ZLjava/lang/String;LX/4BY;)V

    iget-object v1, v0, Lcom/facebook/bugreporter/activity/bugreport/BugReportFragment;->c:LX/0TD;

    invoke-static {p1, p2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 1070202
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
