.class public final LX/5Rw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/net/Uri;

.field private b:Ljava/lang/String;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/5Rr;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/5Rr;

.field private e:LX/5Rq;

.field public f:Z

.field public g:Z

.field private h:Ljava/lang/String;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

.field public n:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 916779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916780
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5Rw;->c:Ljava/util/List;

    .line 916781
    sget-object v0, LX/5Rr;->CROP:LX/5Rr;

    iput-object v0, p0, LX/5Rw;->d:LX/5Rr;

    .line 916782
    sget-object v0, LX/5Rq;->DEFAULT_CROP:LX/5Rq;

    iput-object v0, p0, LX/5Rw;->e:LX/5Rq;

    .line 916783
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5Rw;->f:Z

    .line 916784
    iput-boolean v1, p0, LX/5Rw;->g:Z

    .line 916785
    iput-object v2, p0, LX/5Rw;->h:Ljava/lang/String;

    .line 916786
    iput-boolean v1, p0, LX/5Rw;->i:Z

    .line 916787
    iput-object v2, p0, LX/5Rw;->j:Ljava/lang/String;

    .line 916788
    iput-object v2, p0, LX/5Rw;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 916789
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 916790
    iput-object v0, p0, LX/5Rw;->l:LX/0Px;

    .line 916791
    iput-boolean v1, p0, LX/5Rw;->n:Z

    .line 916792
    return-void
.end method

.method public constructor <init>(Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 916793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 916794
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/5Rw;->c:Ljava/util/List;

    .line 916795
    sget-object v0, LX/5Rr;->CROP:LX/5Rr;

    iput-object v0, p0, LX/5Rw;->d:LX/5Rr;

    .line 916796
    sget-object v0, LX/5Rq;->DEFAULT_CROP:LX/5Rq;

    iput-object v0, p0, LX/5Rw;->e:LX/5Rq;

    .line 916797
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/5Rw;->f:Z

    .line 916798
    iput-boolean v1, p0, LX/5Rw;->g:Z

    .line 916799
    iput-object v2, p0, LX/5Rw;->h:Ljava/lang/String;

    .line 916800
    iput-boolean v1, p0, LX/5Rw;->i:Z

    .line 916801
    iput-object v2, p0, LX/5Rw;->j:Ljava/lang/String;

    .line 916802
    iput-object v2, p0, LX/5Rw;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 916803
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 916804
    iput-object v0, p0, LX/5Rw;->l:LX/0Px;

    .line 916805
    iput-boolean v1, p0, LX/5Rw;->n:Z

    .line 916806
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 916807
    iput-object v0, p0, LX/5Rw;->a:Landroid/net/Uri;

    .line 916808
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->b:Ljava/lang/String;

    move-object v0, v0

    .line 916809
    iput-object v0, p0, LX/5Rw;->b:Ljava/lang/String;

    .line 916810
    iget-object v0, p0, LX/5Rw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 916811
    iget-object v0, p0, LX/5Rw;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 916812
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->e:LX/5Rq;

    move-object v0, v0

    .line 916813
    iput-object v0, p0, LX/5Rw;->e:LX/5Rq;

    .line 916814
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->c:LX/5Rr;

    move-object v0, v0

    .line 916815
    iput-object v0, p0, LX/5Rw;->d:LX/5Rr;

    .line 916816
    iget-boolean v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->f:Z

    move v0, v0

    .line 916817
    iput-boolean v0, p0, LX/5Rw;->f:Z

    .line 916818
    iget-boolean v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->g:Z

    move v0, v0

    .line 916819
    iput-boolean v0, p0, LX/5Rw;->g:Z

    .line 916820
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->h:Ljava/lang/String;

    move-object v0, v0

    .line 916821
    iput-object v0, p0, LX/5Rw;->h:Ljava/lang/String;

    .line 916822
    iget-boolean v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->i:Z

    move v0, v0

    .line 916823
    iput-boolean v0, p0, LX/5Rw;->i:Z

    .line 916824
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->j:Ljava/lang/String;

    move-object v0, v0

    .line 916825
    iput-object v0, p0, LX/5Rw;->j:Ljava/lang/String;

    .line 916826
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 916827
    iput-object v0, p0, LX/5Rw;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 916828
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->l:LX/0Px;

    move-object v0, v0

    .line 916829
    iput-object v0, p0, LX/5Rw;->l:LX/0Px;

    .line 916830
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-object v0, v0

    .line 916831
    iput-object v0, p0, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 916832
    iget-boolean v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;->n:Z

    move v0, v0

    .line 916833
    iput-boolean v0, p0, LX/5Rw;->n:Z

    .line 916834
    return-void
.end method


# virtual methods
.method public final a(LX/5Rq;)LX/5Rw;
    .locals 2

    .prologue
    .line 916835
    iget-object v0, p0, LX/5Rw;->c:Ljava/util/List;

    sget-object v1, LX/5Rr;->CROP:LX/5Rr;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 916836
    iput-object p1, p0, LX/5Rw;->e:LX/5Rq;

    .line 916837
    return-object p0

    .line 916838
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/5Rr;)LX/5Rw;
    .locals 1

    .prologue
    .line 916839
    if-nez p1, :cond_0

    .line 916840
    :goto_0
    return-object p0

    .line 916841
    :cond_0
    iget-object v0, p0, LX/5Rw;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 916842
    iput-object p1, p0, LX/5Rw;->d:LX/5Rr;

    goto :goto_0

    .line 916843
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;
    .locals 0

    .prologue
    .line 916844
    iput-object p1, p0, LX/5Rw;->a:Landroid/net/Uri;

    .line 916845
    iput-object p2, p0, LX/5Rw;->b:Ljava/lang/String;

    .line 916846
    return-object p0
.end method

.method public final a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;
    .locals 17

    .prologue
    .line 916847
    move-object/from16 v0, p0

    iget-object v1, v0, LX/5Rw;->h:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 916848
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LX/5Rw;->h:Ljava/lang/String;

    .line 916849
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    if-nez v1, :cond_1

    .line 916850
    new-instance v1, LX/5Ry;

    invoke-direct {v1}, LX/5Ry;-><init>()V

    invoke-virtual {v1}, LX/5Ry;->a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 916851
    :cond_1
    new-instance v1, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/5Rw;->a:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/5Rw;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/5Rw;->d:LX/5Rr;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/5Rw;->e:LX/5Rq;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/5Rw;->c:Ljava/util/List;

    move-object/from16 v0, p0

    iget-boolean v7, v0, LX/5Rw;->f:Z

    move-object/from16 v0, p0

    iget-boolean v8, v0, LX/5Rw;->g:Z

    move-object/from16 v0, p0

    iget-object v9, v0, LX/5Rw;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v10, v0, LX/5Rw;->i:Z

    move-object/from16 v0, p0

    iget-object v11, v0, LX/5Rw;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, LX/5Rw;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object/from16 v0, p0

    iget-object v13, v0, LX/5Rw;->l:LX/0Px;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-object/from16 v0, p0

    iget-boolean v15, v0, LX/5Rw;->n:Z

    const/16 v16, 0x0

    invoke-direct/range {v1 .. v16}, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;-><init>(Landroid/net/Uri;Ljava/lang/String;LX/5Rr;LX/5Rq;Ljava/util/List;ZZLjava/lang/String;ZLjava/lang/String;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;ZB)V

    return-object v1
.end method

.method public final b(LX/5Rr;)LX/5Rw;
    .locals 1

    .prologue
    .line 916852
    if-nez p1, :cond_1

    .line 916853
    :cond_0
    :goto_0
    return-object p0

    .line 916854
    :cond_1
    iget-object v0, p0, LX/5Rw;->d:LX/5Rr;

    if-eq p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 916855
    iget-object v0, p0, LX/5Rw;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 916856
    iget-object v0, p0, LX/5Rw;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 916857
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)LX/5Rw;
    .locals 1

    .prologue
    .line 916858
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 916859
    iput-object p1, p0, LX/5Rw;->h:Ljava/lang/String;

    .line 916860
    return-object p0

    .line 916861
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
