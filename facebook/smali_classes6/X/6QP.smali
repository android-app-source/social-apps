.class public final LX/6QP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/6Qc;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/47G;

.field public final synthetic d:Ljava/util/Map;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Landroid/os/Bundle;

.field public final synthetic g:LX/6Qa;


# direct methods
.method public constructor <init>(LX/6Qa;LX/6Qc;Landroid/content/Context;LX/47G;Ljava/util/Map;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1087382
    iput-object p1, p0, LX/6QP;->g:LX/6Qa;

    iput-object p2, p0, LX/6QP;->a:LX/6Qc;

    iput-object p3, p0, LX/6QP;->b:Landroid/content/Context;

    iput-object p4, p0, LX/6QP;->c:LX/47G;

    iput-object p5, p0, LX/6QP;->d:Ljava/util/Map;

    iput-object p6, p0, LX/6QP;->e:Ljava/lang/String;

    iput-object p7, p0, LX/6QP;->f:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1087383
    iget-object v0, p0, LX/6QP;->g:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->h:LX/03V;

    sget-object v1, LX/6Qa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1087384
    iget-object v0, p0, LX/6QP;->g:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->n:LX/2v6;

    iget-object v1, p0, LX/6QP;->c:LX/47G;

    iget-object v1, v1, LX/47G;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2v6;->b(Ljava/lang/String;)V

    .line 1087385
    iget-object v0, p0, LX/6QP;->g:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->n:LX/2v6;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->FAILED_INSTALL:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    iget-object v3, p0, LX/6QP;->c:LX/47G;

    iget-object v3, v3, LX/47G;->i:Ljava/lang/String;

    iget-object v4, p0, LX/6QP;->c:LX/47G;

    iget-object v4, v4, LX/47G;->e:Ljava/lang/String;

    iget-object v5, p0, LX/6QP;->c:LX/47G;

    iget-object v5, v5, LX/47G;->l:Ljava/lang/String;

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/2v6;->a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6VZ;)V

    .line 1087386
    iget-object v0, p0, LX/6QP;->d:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 1087387
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, LX/6QP;->d:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1087388
    const-string v1, "tracking"

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1087389
    const-string v1, "sponsored"

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1087390
    iget-object v1, p0, LX/6QP;->g:LX/6Qa;

    iget-object v1, v1, LX/6Qa;->j:LX/17W;

    iget-object v2, p0, LX/6QP;->b:Landroid/content/Context;

    iget-object v3, p0, LX/6QP;->e:Ljava/lang/String;

    iget-object v4, p0, LX/6QP;->f:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3, v4, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 1087391
    iget-object v0, p0, LX/6QP;->g:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->m:LX/6Qw;

    const-string v1, "neko_di_install_failed"

    iget-object v2, p0, LX/6QP;->c:LX/47G;

    const-string v3, "exception_message"

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/6Qw;->a(Ljava/lang/String;LX/47G;LX/0P1;)V

    .line 1087392
    :cond_0
    iget-object v0, p0, LX/6QP;->b:Landroid/content/Context;

    instance-of v0, v0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    if-eqz v0, :cond_1

    .line 1087393
    iget-object v0, p0, LX/6QP;->b:Landroid/content/Context;

    check-cast v0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    invoke-virtual {v0}, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->finish()V

    .line 1087394
    :cond_1
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1087395
    check-cast p1, Ljava/lang/Long;

    const/4 v4, 0x0

    .line 1087396
    if-nez p1, :cond_1

    .line 1087397
    iget-object v0, p0, LX/6QP;->g:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->h:LX/03V;

    sget-object v1, LX/6Qa;->a:Ljava/lang/String;

    const-string v2, "null update id returned onSuccess"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087398
    :cond_0
    :goto_0
    return-void

    .line 1087399
    :cond_1
    iget-object v0, p0, LX/6QP;->g:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->n:LX/2v6;

    iget-object v1, p0, LX/6QP;->a:LX/6Qc;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/2v6;->a(LX/6Qc;J)V

    .line 1087400
    iget-object v0, p0, LX/6QP;->b:Landroid/content/Context;

    instance-of v0, v0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6QP;->g:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->c:LX/0Uh;

    const/16 v1, 0x32c

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6QP;->g:LX/6Qa;

    iget-object v0, v0, LX/6Qa;->c:LX/0Uh;

    const/16 v1, 0x32d

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1087401
    iget-object v0, p0, LX/6QP;->b:Landroid/content/Context;

    check-cast v0, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;

    invoke-virtual {v0}, Lcom/facebook/directinstall/appdetails/AppDetailsActivity;->finish()V

    goto :goto_0
.end method
