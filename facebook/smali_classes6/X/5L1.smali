.class public final LX/5L1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/java2js/Invokable;


# instance fields
.field public final synthetic a:Lcom/facebook/java2js/JSContext;

.field public final synthetic b:LX/0dl;


# direct methods
.method public constructor <init>(LX/0dl;Lcom/facebook/java2js/JSContext;)V
    .locals 0

    .prologue
    .line 899372
    iput-object p1, p0, LX/5L1;->b:LX/0dl;

    iput-object p2, p0, LX/5L1;->a:Lcom/facebook/java2js/JSContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke([Lcom/facebook/java2js/JSValue;)Lcom/facebook/java2js/JSValue;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 899373
    iget-object v0, p0, LX/5L1;->b:LX/0dl;

    iget-object v0, v0, LX/0dl;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 899374
    iget-object v1, p0, LX/5L1;->a:Lcom/facebook/java2js/JSContext;

    invoke-static {v1}, Lcom/facebook/java2js/JSValue;->makeObject(Lcom/facebook/java2js/JSContext;)Lcom/facebook/java2js/JSValue;

    move-result-object v1

    .line 899375
    const-string v2, "NumberFormatConfig"

    iget-object v3, p0, LX/5L1;->b:LX/0dl;

    iget-object v3, v3, LX/0dl;->c:Landroid/content/Context;

    iget-object v4, p0, LX/5L1;->a:Lcom/facebook/java2js/JSContext;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0dl;->b(Landroid/content/Context;Lcom/facebook/java2js/JSContext;Ljava/lang/Boolean;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 899376
    const-string v2, "FallbackNumberFormatConfig"

    iget-object v3, p0, LX/5L1;->b:LX/0dl;

    iget-object v3, v3, LX/0dl;->c:Landroid/content/Context;

    iget-object v4, p0, LX/5L1;->a:Lcom/facebook/java2js/JSContext;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0dl;->b(Landroid/content/Context;Lcom/facebook/java2js/JSContext;Ljava/lang/Boolean;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 899377
    const-string v2, "translationsDictionary"

    iget-object v3, p0, LX/5L1;->b:LX/0dl;

    iget-object v3, v3, LX/0dl;->c:Landroid/content/Context;

    const v4, 0x7f07003b

    invoke-static {v3, v4}, LX/0dl;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 899378
    const-string v2, "localeIdentifier"

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 899379
    const-string v2, "localeCountryCode"

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 899380
    const-string v0, "isRTL"

    iget-object v2, p0, LX/5L1;->a:Lcom/facebook/java2js/JSContext;

    invoke-static {v2, v6}, Lcom/facebook/java2js/JSValue;->makeBoolean(Lcom/facebook/java2js/JSContext;Z)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 899381
    const-string v0, "allowRTL"

    iget-object v2, p0, LX/5L1;->a:Lcom/facebook/java2js/JSContext;

    invoke-static {v2, v6}, Lcom/facebook/java2js/JSValue;->makeBoolean(Lcom/facebook/java2js/JSContext;Z)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 899382
    const-string v0, "forceRTL"

    iget-object v2, p0, LX/5L1;->a:Lcom/facebook/java2js/JSContext;

    invoke-static {v2, v6}, Lcom/facebook/java2js/JSValue;->makeBoolean(Lcom/facebook/java2js/JSContext;Z)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/java2js/JSValue;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V

    .line 899383
    return-object v1
.end method
