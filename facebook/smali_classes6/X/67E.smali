.class public final LX/67E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/671;


# instance fields
.field public final a:LX/672;

.field public final b:LX/65D;

.field public c:Z


# direct methods
.method public constructor <init>(LX/65D;)V
    .locals 2

    .prologue
    .line 1052528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1052529
    new-instance v0, LX/672;

    invoke-direct {v0}, LX/672;-><init>()V

    iput-object v0, p0, LX/67E;->a:LX/672;

    .line 1052530
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052531
    :cond_0
    iput-object p1, p0, LX/67E;->b:LX/65D;

    .line 1052532
    return-void
.end method

.method private a(BJ)J
    .locals 8

    .prologue
    const-wide/16 v2, -0x1

    .line 1052533
    iget-boolean v0, p0, LX/67E;->c:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052534
    :cond_0
    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p2

    .line 1052535
    :cond_1
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0, p1, p2, p3}, LX/672;->a(BJ)J

    move-result-wide v0

    .line 1052536
    cmp-long v4, v0, v2

    if-eqz v4, :cond_2

    .line 1052537
    :goto_0
    return-wide v0

    .line 1052538
    :cond_2
    iget-object v0, p0, LX/67E;->a:LX/672;

    iget-wide v0, v0, LX/672;->b:J

    .line 1052539
    iget-object v4, p0, LX/67E;->b:LX/65D;

    iget-object v5, p0, LX/67E;->a:LX/672;

    const-wide/16 v6, 0x2000

    invoke-interface {v4, v5, v6, v7}, LX/65D;->a(LX/672;J)J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-nez v4, :cond_0

    move-wide v0, v2

    goto :goto_0
.end method

.method private b(J)Z
    .locals 5

    .prologue
    .line 1052540
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052541
    :cond_0
    iget-boolean v0, p0, LX/67E;->c:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052542
    :cond_1
    iget-object v0, p0, LX/67E;->a:LX/672;

    iget-wide v0, v0, LX/672;->b:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_2

    .line 1052543
    iget-object v0, p0, LX/67E;->b:LX/65D;

    iget-object v1, p0, LX/67E;->a:LX/672;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, LX/65D;->a(LX/672;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1052544
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    .line 1052545
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, LX/67J;->a(JJJ)V

    .line 1052546
    iget-object v0, p0, LX/67E;->a:LX/672;

    iget-wide v0, v0, LX/672;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1052547
    iget-object v0, p0, LX/67E;->b:LX/65D;

    iget-object v1, p0, LX/67E;->a:LX/672;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, LX/65D;->a(LX/672;J)J

    move-result-wide v0

    .line 1052548
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 1052549
    :goto_0
    return v0

    .line 1052550
    :cond_0
    int-to-long v0, p3

    iget-object v2, p0, LX/67E;->a:LX/672;

    iget-wide v2, v2, LX/672;->b:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 1052551
    iget-object v1, p0, LX/67E;->a:LX/672;

    invoke-virtual {v1, p1, p2, v0}, LX/672;->a([BII)I

    move-result v0

    goto :goto_0
.end method

.method public final a(B)J
    .locals 2

    .prologue
    .line 1052552
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/67E;->a(BJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(LX/672;J)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v0, -0x1

    .line 1052553
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052554
    :cond_0
    cmp-long v2, p2, v4

    if-gez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "byteCount < 0: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052555
    :cond_1
    iget-boolean v2, p0, LX/67E;->c:Z

    if-eqz v2, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052556
    :cond_2
    iget-object v2, p0, LX/67E;->a:LX/672;

    iget-wide v2, v2, LX/672;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 1052557
    iget-object v2, p0, LX/67E;->b:LX/65D;

    iget-object v3, p0, LX/67E;->a:LX/672;

    const-wide/16 v4, 0x2000

    invoke-interface {v2, v3, v4, v5}, LX/65D;->a(LX/672;J)J

    move-result-wide v2

    .line 1052558
    cmp-long v2, v2, v0

    if-nez v2, :cond_3

    .line 1052559
    :goto_0
    return-wide v0

    .line 1052560
    :cond_3
    iget-object v0, p0, LX/67E;->a:LX/672;

    iget-wide v0, v0, LX/672;->b:J

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 1052561
    iget-object v2, p0, LX/67E;->a:LX/672;

    invoke-virtual {v2, p1, v0, v1}, LX/672;->a(LX/672;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a()LX/65f;
    .locals 1

    .prologue
    .line 1052562
    iget-object v0, p0, LX/67E;->b:LX/65D;

    invoke-interface {v0}, LX/65D;->a()LX/65f;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 1052563
    invoke-direct {p0, p1, p2}, LX/67E;->b(J)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1052564
    :cond_0
    return-void
.end method

.method public final a([B)V
    .locals 6

    .prologue
    .line 1052565
    :try_start_0
    array-length v0, p1

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/67E;->a(J)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1052566
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0, p1}, LX/672;->a([B)V

    .line 1052567
    return-void

    .line 1052568
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1052569
    const/4 v0, 0x0

    .line 1052570
    :goto_0
    iget-object v2, p0, LX/67E;->a:LX/672;

    iget-wide v2, v2, LX/672;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 1052571
    iget-object v2, p0, LX/67E;->a:LX/672;

    iget-object v3, p0, LX/67E;->a:LX/672;

    iget-wide v4, v3, LX/672;->b:J

    long-to-int v3, v4

    invoke-virtual {v2, p1, v0, v3}, LX/672;->a([BII)I

    move-result v2

    .line 1052572
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1052573
    :cond_0
    add-int/2addr v0, v2

    .line 1052574
    goto :goto_0

    .line 1052575
    :cond_1
    throw v1
.end method

.method public final b(LX/672;J)V
    .locals 2

    .prologue
    .line 1052576
    :try_start_0
    invoke-virtual {p0, p2, p3}, LX/67E;->a(J)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1052577
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0, p1, p2, p3}, LX/672;->b(LX/672;J)V

    .line 1052578
    return-void

    .line 1052579
    :catch_0
    move-exception v0

    .line 1052580
    iget-object v1, p0, LX/67E;->a:LX/672;

    invoke-virtual {p1, v1}, LX/672;->a(LX/65D;)J

    .line 1052581
    throw v0
.end method

.method public final c()LX/672;
    .locals 1

    .prologue
    .line 1052582
    iget-object v0, p0, LX/67E;->a:LX/672;

    return-object v0
.end method

.method public final c(J)LX/673;
    .locals 1

    .prologue
    .line 1052583
    invoke-virtual {p0, p1, p2}, LX/67E;->a(J)V

    .line 1052584
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0, p1, p2}, LX/672;->c(J)LX/673;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1052521
    iget-boolean v0, p0, LX/67E;->c:Z

    if-eqz v0, :cond_0

    .line 1052522
    :goto_0
    return-void

    .line 1052523
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/67E;->c:Z

    .line 1052524
    iget-object v0, p0, LX/67E;->b:LX/65D;

    invoke-interface {v0}, LX/65D;->close()V

    .line 1052525
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->s()V

    goto :goto_0
.end method

.method public final e()Z
    .locals 4

    .prologue
    .line 1052526
    iget-boolean v0, p0, LX/67E;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052527
    :cond_0
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/67E;->b:LX/65D;

    iget-object v1, p0, LX/67E;->a:LX/672;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, LX/65D;->a(LX/672;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(J)[B
    .locals 1

    .prologue
    .line 1052470
    invoke-virtual {p0, p1, p2}, LX/67E;->a(J)V

    .line 1052471
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0, p1, p2}, LX/672;->e(J)[B

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 1052472
    new-instance v0, LX/67D;

    invoke-direct {v0, p0}, LX/67D;-><init>(LX/67E;)V

    return-object v0
.end method

.method public final f(J)V
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    .line 1052473
    iget-boolean v0, p0, LX/67E;->c:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052474
    :cond_0
    iget-object v0, p0, LX/67E;->a:LX/672;

    .line 1052475
    iget-wide v7, v0, LX/672;->b:J

    move-wide v0, v7

    .line 1052476
    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 1052477
    iget-object v2, p0, LX/67E;->a:LX/672;

    invoke-virtual {v2, v0, v1}, LX/672;->f(J)V

    .line 1052478
    sub-long/2addr p1, v0

    .line 1052479
    :cond_1
    cmp-long v0, p1, v4

    if-lez v0, :cond_2

    .line 1052480
    iget-object v0, p0, LX/67E;->a:LX/672;

    iget-wide v0, v0, LX/672;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, LX/67E;->b:LX/65D;

    iget-object v1, p0, LX/67E;->a:LX/672;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, LX/65D;->a(LX/672;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1052481
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 1052482
    :cond_2
    return-void
.end method

.method public final h()B
    .locals 2

    .prologue
    .line 1052483
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, LX/67E;->a(J)V

    .line 1052484
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->h()B

    move-result v0

    return v0
.end method

.method public final i()S
    .locals 2

    .prologue
    .line 1052485
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, LX/67E;->a(J)V

    .line 1052486
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->i()S

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1052487
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, LX/67E;->a(J)V

    .line 1052488
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->j()I

    move-result v0

    return v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 1052489
    const-wide/16 v0, 0x8

    invoke-virtual {p0, v0, v1}, LX/67E;->a(J)V

    .line 1052490
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->k()J

    move-result-wide v0

    return-wide v0
.end method

.method public final l()S
    .locals 2

    .prologue
    .line 1052491
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, LX/67E;->a(J)V

    .line 1052492
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->l()S

    move-result v0

    return v0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 1052493
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, LX/67E;->a(J)V

    .line 1052494
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->m()I

    move-result v0

    return v0
.end method

.method public final n()J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1052495
    const-wide/16 v2, 0x1

    invoke-virtual {p0, v2, v3}, LX/67E;->a(J)V

    move v0, v1

    .line 1052496
    :goto_0
    add-int/lit8 v2, v0, 0x1

    int-to-long v2, v2

    invoke-direct {p0, v2, v3}, LX/67E;->b(J)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1052497
    iget-object v2, p0, LX/67E;->a:LX/672;

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, LX/672;->b(J)B

    move-result v2

    .line 1052498
    const/16 v3, 0x30

    if-lt v2, v3, :cond_0

    const/16 v3, 0x39

    if-le v2, v3, :cond_3

    :cond_0
    const/16 v3, 0x61

    if-lt v2, v3, :cond_1

    const/16 v3, 0x66

    if-le v2, v3, :cond_3

    :cond_1
    const/16 v3, 0x41

    if-lt v2, v3, :cond_2

    const/16 v3, 0x46

    if-le v2, v3, :cond_3

    .line 1052499
    :cond_2
    if-nez v0, :cond_4

    .line 1052500
    new-instance v0, Ljava/lang/NumberFormatException;

    const-string v3, "Expected leading [0-9a-fA-F] character but was %#x"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 1052501
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v4, v1

    .line 1052502
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052503
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1052504
    :cond_4
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->n()J

    move-result-wide v0

    return-wide v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1052505
    iget-object v0, p0, LX/67E;->a:LX/672;

    iget-object v1, p0, LX/67E;->b:LX/65D;

    invoke-virtual {v0, v1}, LX/672;->a(LX/65D;)J

    .line 1052506
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 10

    .prologue
    .line 1052507
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, LX/67E;->a(B)J

    move-result-wide v0

    .line 1052508
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1052509
    new-instance v1, LX/672;

    invoke-direct {v1}, LX/672;-><init>()V

    .line 1052510
    iget-object v0, p0, LX/67E;->a:LX/672;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x20

    iget-object v6, p0, LX/67E;->a:LX/672;

    .line 1052511
    iget-wide v8, v6, LX/672;->b:J

    move-wide v6, v8

    .line 1052512
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, LX/672;->a(LX/672;JJ)LX/672;

    .line 1052513
    new-instance v0, Ljava/io/EOFException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\\n not found: size="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/67E;->a:LX/672;

    .line 1052514
    iget-wide v8, v3, LX/672;->b:J

    move-wide v4, v8

    .line 1052515
    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " content="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1052516
    invoke-virtual {v1}, LX/672;->o()LX/673;

    move-result-object v1

    invoke-virtual {v1}, LX/673;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u2026"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1052517
    :cond_0
    iget-object v2, p0, LX/67E;->a:LX/672;

    invoke-virtual {v2, v0, v1}, LX/672;->d(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final r()[B
    .locals 2

    .prologue
    .line 1052519
    iget-object v0, p0, LX/67E;->a:LX/672;

    iget-object v1, p0, LX/67E;->b:LX/65D;

    invoke-virtual {v0, v1}, LX/672;->a(LX/65D;)J

    .line 1052520
    iget-object v0, p0, LX/67E;->a:LX/672;

    invoke-virtual {v0}, LX/672;->r()[B

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1052518
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "buffer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/67E;->b:LX/65D;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
