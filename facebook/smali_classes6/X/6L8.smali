.class public LX/6L8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/6L9;

.field private final c:Ljava/lang/String;

.field private final d:LX/6L1;

.field private final e:LX/6LC;

.field public volatile f:Z

.field private volatile g:Z

.field public h:J

.field public i:J

.field public j:J

.field public k:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1077985
    const-class v0, LX/6L8;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/6L8;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/6L1;LX/6LC;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1077986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1077987
    new-instance v0, LX/6L9;

    invoke-direct {v0}, LX/6L9;-><init>()V

    move-object v0, v0

    .line 1077988
    iput-object v0, p0, LX/6L8;->b:LX/6L9;

    .line 1077989
    iput-object p1, p0, LX/6L8;->c:Ljava/lang/String;

    .line 1077990
    iput-object p2, p0, LX/6L8;->d:LX/6L1;

    .line 1077991
    iput-object p3, p0, LX/6L8;->e:LX/6LC;

    .line 1077992
    iput-wide v2, p0, LX/6L8;->h:J

    .line 1077993
    iput-wide v2, p0, LX/6L8;->i:J

    .line 1077994
    iput-wide v2, p0, LX/6L8;->j:J

    .line 1077995
    iput-wide v2, p0, LX/6L8;->k:J

    .line 1077996
    return-void
.end method

.method public static declared-synchronized b(LX/6L8;)V
    .locals 4

    .prologue
    .line 1077997
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/6L8;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/6L8;->g:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 1077998
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1077999
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/6L8;->d:LX/6L1;

    .line 1078000
    iget-object v1, v0, LX/6L1;->f:Landroid/media/MediaFormat;

    move-object v0, v1

    .line 1078001
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/6L8;->e:LX/6LC;

    invoke-interface {v0}, LX/6LC;->b()Landroid/media/MediaFormat;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1078002
    iget-object v0, p0, LX/6L8;->b:LX/6L9;

    iget-object v1, p0, LX/6L8;->c:Ljava/lang/String;

    const/4 v3, 0x0

    .line 1078003
    new-instance v2, Landroid/media/MediaMuxer;

    invoke-direct {v2, v1, v3}, Landroid/media/MediaMuxer;-><init>(Ljava/lang/String;I)V

    iput-object v2, v0, LX/6L9;->a:Landroid/media/MediaMuxer;

    .line 1078004
    iput-boolean v3, v0, LX/6L9;->d:Z

    .line 1078005
    iput-boolean v3, v0, LX/6L9;->e:Z

    .line 1078006
    iget-object v0, p0, LX/6L8;->b:LX/6L9;

    iget-object v1, p0, LX/6L8;->d:LX/6L1;

    .line 1078007
    iget-object v2, v1, LX/6L1;->f:Landroid/media/MediaFormat;

    move-object v1, v2

    .line 1078008
    iget-object v2, v0, LX/6L9;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v2, v1}, Landroid/media/MediaMuxer;->addTrack(Landroid/media/MediaFormat;)I

    move-result v2

    iput v2, v0, LX/6L9;->b:I

    .line 1078009
    iget-object v0, p0, LX/6L8;->b:LX/6L9;

    iget-object v1, p0, LX/6L8;->e:LX/6LC;

    invoke-interface {v1}, LX/6LC;->b()Landroid/media/MediaFormat;

    move-result-object v1

    .line 1078010
    iget-object v2, v0, LX/6L9;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v2, v1}, Landroid/media/MediaMuxer;->addTrack(Landroid/media/MediaFormat;)I

    move-result v2

    iput v2, v0, LX/6L9;->c:I

    .line 1078011
    iget-object v0, p0, LX/6L8;->b:LX/6L9;

    const/4 v1, 0x0

    .line 1078012
    iget-object v2, v0, LX/6L9;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v2, v1}, Landroid/media/MediaMuxer;->setOrientationHint(I)V

    .line 1078013
    iget-object v0, p0, LX/6L8;->b:LX/6L9;

    .line 1078014
    iget-object v1, v0, LX/6L9;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v1}, Landroid/media/MediaMuxer;->start()V

    .line 1078015
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6L8;->f:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1078016
    :catch_0
    move-exception v0

    .line 1078017
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1078018
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 1078019
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/6L8;->f:Z

    if-eqz v0, :cond_1

    .line 1078020
    iget-object v0, p0, LX/6L8;->b:LX/6L9;

    const/4 v2, 0x0

    .line 1078021
    iget-object v1, v0, LX/6L9;->a:Landroid/media/MediaMuxer;

    if-eqz v1, :cond_0

    .line 1078022
    iget-boolean v1, v0, LX/6L9;->d:Z

    if-eqz v1, :cond_0

    iget-boolean v1, v0, LX/6L9;->e:Z

    if-eqz v1, :cond_0

    .line 1078023
    iget-object v1, v0, LX/6L9;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v1}, Landroid/media/MediaMuxer;->stop()V

    .line 1078024
    iget-object v1, v0, LX/6L9;->a:Landroid/media/MediaMuxer;

    invoke-virtual {v1}, Landroid/media/MediaMuxer;->release()V

    .line 1078025
    :cond_0
    iput-boolean v2, v0, LX/6L9;->d:Z

    .line 1078026
    iput-boolean v2, v0, LX/6L9;->e:Z

    .line 1078027
    const/4 v1, 0x0

    iput-object v1, v0, LX/6L9;->a:Landroid/media/MediaMuxer;

    .line 1078028
    iput v2, v0, LX/6L9;->b:I

    .line 1078029
    iput v2, v0, LX/6L9;->c:I

    .line 1078030
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/6L8;->f:Z

    .line 1078031
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/6L8;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1078032
    monitor-exit p0

    return-void

    .line 1078033
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
