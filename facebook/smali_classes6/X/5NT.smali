.class public LX/5NT;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/4CK;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/4CK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 906343
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/4CK;
    .locals 7

    .prologue
    .line 906344
    sget-object v0, LX/5NT;->a:LX/4CK;

    if-nez v0, :cond_1

    .line 906345
    const-class v1, LX/5NT;

    monitor-enter v1

    .line 906346
    :try_start_0
    sget-object v0, LX/5NT;->a:LX/4CK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 906347
    if-eqz v2, :cond_0

    .line 906348
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 906349
    invoke-static {v0}, LX/4eT;->a(LX/0QB;)LX/1G9;

    move-result-object v3

    check-cast v3, LX/1G9;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object v5

    check-cast v5, LX/1Ft;

    invoke-static {v0}, LX/4eU;->a(LX/0QB;)LX/1bw;

    move-result-object v6

    check-cast v6, LX/1bw;

    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-static {v3, v4, v5, v6, p0}, LX/2uL;->a(LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/1Ft;LX/1bw;Landroid/content/Context;)LX/4CK;

    move-result-object v3

    move-object v0, v3

    .line 906350
    sput-object v0, LX/5NT;->a:LX/4CK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 906351
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 906352
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 906353
    :cond_1
    sget-object v0, LX/5NT;->a:LX/4CK;

    return-object v0

    .line 906354
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 906355
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 906356
    invoke-static {p0}, LX/4eT;->a(LX/0QB;)LX/1G9;

    move-result-object v0

    check-cast v0, LX/1G9;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/1Fs;->a(LX/0QB;)LX/1Fs;

    move-result-object v2

    check-cast v2, LX/1Ft;

    invoke-static {p0}, LX/4eU;->a(LX/0QB;)LX/1bw;

    move-result-object v3

    check-cast v3, LX/1bw;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0, v1, v2, v3, v4}, LX/2uL;->a(LX/1G9;Ljava/util/concurrent/ScheduledExecutorService;LX/1Ft;LX/1bw;Landroid/content/Context;)LX/4CK;

    move-result-object v0

    return-object v0
.end method
