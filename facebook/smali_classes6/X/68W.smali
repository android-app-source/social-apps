.class public abstract LX/68W;
.super LX/68V;
.source ""


# static fields
.field public static final d:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "LX/69E;",
            ">;"
        }
    .end annotation
.end field

.field public static e:Ljava/lang/Thread;

.field public static volatile f:LX/68h;

.field private static final g:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field public final h:Ljava/util/concurrent/atomic/AtomicLong;

.field public final i:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1055507
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    sput-object v0, LX/68W;->d:Ljava/util/concurrent/BlockingQueue;

    .line 1055508
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    sput-object v0, LX/68W;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1055509
    invoke-direct {p0, p2, p3}, LX/68V;-><init>(II)V

    .line 1055510
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/68W;->h:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1055511
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/68W;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1055512
    sget-object v0, LX/68W;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1055513
    new-instance v0, Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/android/maps/internal/CacheableUrlTileProvider$1;-><init>(LX/68W;Landroid/content/Context;)V

    invoke-static {v0}, LX/31l;->a(Lcom/facebook/android/maps/internal/GrandCentralDispatch$Dispatchable;)V

    .line 1055514
    :cond_0
    return-void
.end method

.method private static b(Ljava/lang/String;)LX/69E;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1055416
    sget-object v1, LX/68W;->f:LX/68h;

    if-nez v1, :cond_1

    .line 1055417
    :cond_0
    :goto_0
    return-object v0

    .line 1055418
    :cond_1
    :try_start_0
    sget-object v1, LX/68W;->f:LX/68h;

    invoke-virtual {v1, p0}, LX/68h;->a(Ljava/lang/String;)LX/68e;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 1055419
    if-nez v2, :cond_2

    .line 1055420
    if-eqz v2, :cond_0

    .line 1055421
    invoke-virtual {v2}, LX/68e;->close()V

    goto :goto_0

    .line 1055422
    :cond_2
    const/4 v1, 0x0

    .line 1055423
    :try_start_1
    iget-object v3, v2, LX/68e;->d:[Ljava/io/InputStream;

    aget-object v3, v3, v1

    move-object v1, v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1055424
    if-nez v1, :cond_3

    .line 1055425
    :goto_1
    if-eqz v2, :cond_0

    .line 1055426
    invoke-virtual {v2}, LX/68e;->close()V

    goto :goto_0

    .line 1055427
    :cond_3
    const/4 v3, 0x0

    :try_start_2
    invoke-static {v1, v3}, LX/68V;->a(Ljava/io/InputStream;Z)LX/69E;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    goto :goto_1

    .line 1055428
    :catch_0
    move-exception v1

    move-object v2, v0

    .line 1055429
    :goto_2
    :try_start_3
    sget-object v3, LX/31U;->o:LX/31U;

    invoke-virtual {v3, v1}, LX/31U;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1055430
    if-eqz v2, :cond_0

    .line 1055431
    invoke-virtual {v2}, LX/68e;->close()V

    goto :goto_0

    .line 1055432
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v2, :cond_4

    .line 1055433
    invoke-virtual {v2}, LX/68e;->close()V

    :cond_4
    throw v0

    .line 1055434
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1055435
    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method public static d(LX/69E;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1055467
    :try_start_0
    iget-object v0, p0, LX/69E;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1055468
    sget-object v0, LX/31U;->m:LX/31U;

    const-string v2, "Tile stringKey is null"

    invoke-virtual {v0, v2}, LX/31U;->a(Ljava/lang/String;)V

    .line 1055469
    :cond_0
    :goto_0
    return-void

    .line 1055470
    :cond_1
    sget-object v0, LX/68W;->f:LX/68h;

    iget-object v2, p0, LX/69E;->a:Ljava/lang/String;

    .line 1055471
    const-wide/16 v5, -0x1

    invoke-static {v0, v2, v5, v6}, LX/68h;->a(LX/68h;Ljava/lang/String;J)LX/68c;

    move-result-object v5

    move-object v2, v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1055472
    if-nez v2, :cond_2

    .line 1055473
    if-eqz v2, :cond_0

    .line 1055474
    invoke-virtual {v2}, LX/68c;->c()V

    goto :goto_0

    .line 1055475
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v2, v0}, LX/68c;->a(I)Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 1055476
    if-nez v1, :cond_4

    .line 1055477
    if-eqz v2, :cond_3

    .line 1055478
    invoke-virtual {v2}, LX/68c;->c()V

    .line 1055479
    :cond_3
    if-eqz v1, :cond_0

    .line 1055480
    :try_start_2
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0

    .line 1055481
    :cond_4
    :try_start_3
    iget-object v0, p0, LX/69E;->b:[B

    const/4 v3, 0x0

    iget v4, p0, LX/69E;->c:I

    invoke-virtual {v1, v0, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 1055482
    const/4 v4, 0x1

    .line 1055483
    iget-boolean v0, v2, LX/68c;->d:Z

    if-eqz v0, :cond_9

    .line 1055484
    iget-object v0, v2, LX/68c;->a:LX/68h;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/68h;->a$redex0(LX/68h;LX/68c;Z)V

    .line 1055485
    iget-object v0, v2, LX/68c;->a:LX/68h;

    iget-object v3, v2, LX/68c;->b:LX/68d;

    iget-object v3, v3, LX/68d;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/68h;->c(Ljava/lang/String;)Z

    .line 1055486
    :goto_1
    iput-boolean v4, v2, LX/68c;->e:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1055487
    if-eqz v2, :cond_5

    .line 1055488
    invoke-virtual {v2}, LX/68c;->c()V

    .line 1055489
    :cond_5
    if-eqz v1, :cond_0

    .line 1055490
    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 1055491
    :catch_1
    goto :goto_0

    .line 1055492
    :catch_2
    move-exception v0

    move-object v2, v1

    .line 1055493
    :goto_2
    :try_start_5
    sget-object v3, LX/31U;->n:LX/31U;

    invoke-virtual {v3, v0}, LX/31U;->a(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1055494
    if-eqz v2, :cond_6

    .line 1055495
    invoke-virtual {v2}, LX/68c;->c()V

    .line 1055496
    :cond_6
    if-eqz v1, :cond_0

    .line 1055497
    :try_start_6
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 1055498
    :catch_3
    goto :goto_0

    .line 1055499
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    if-eqz v2, :cond_7

    .line 1055500
    invoke-virtual {v2}, LX/68c;->c()V

    .line 1055501
    :cond_7
    if-eqz v1, :cond_8

    .line 1055502
    :try_start_7
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 1055503
    :cond_8
    :goto_4
    throw v0

    :catch_4
    goto :goto_4

    .line 1055504
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1055505
    :catch_5
    move-exception v0

    goto :goto_2

    .line 1055506
    :cond_9
    iget-object v0, v2, LX/68c;->a:LX/68h;

    invoke-static {v0, v2, v4}, LX/68h;->a$redex0(LX/68h;LX/68c;Z)V

    goto :goto_1
.end method


# virtual methods
.method public abstract a(III)Ljava/lang/String;
.end method

.method public b(III)LX/69C;
    .locals 5

    .prologue
    .line 1055440
    invoke-virtual {p0, p1, p2, p3}, LX/68V;->c(III)Ljava/net/URL;

    move-result-object v1

    .line 1055441
    if-nez v1, :cond_0

    .line 1055442
    sget-object v0, LX/68W;->a:LX/69C;

    .line 1055443
    :goto_0
    return-object v0

    .line 1055444
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, LX/68W;->a(III)Ljava/lang/String;

    move-result-object v2

    .line 1055445
    sget-object v0, LX/68W;->f:LX/68h;

    if-eqz v0, :cond_1

    .line 1055446
    invoke-static {v2}, LX/68W;->b(Ljava/lang/String;)LX/69E;

    move-result-object v3

    .line 1055447
    if-eqz v3, :cond_1

    .line 1055448
    iget-object v0, v3, LX/69E;->b:[B

    iget v4, v3, LX/69E;->c:I

    invoke-static {v0, v4}, LX/69C;->a([BI)LX/69C;

    move-result-object v0

    .line 1055449
    invoke-static {v3}, LX/68V;->c(LX/69E;)V

    .line 1055450
    if-eqz v0, :cond_1

    .line 1055451
    iget-object v1, p0, LX/68W;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    goto :goto_0

    .line 1055452
    :cond_1
    iget-object v0, p0, LX/68W;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 1055453
    invoke-virtual {p0, v1}, LX/68V;->a(Ljava/net/URL;)LX/69E;

    move-result-object v1

    .line 1055454
    if-nez v1, :cond_2

    .line 1055455
    const/4 v0, 0x0

    goto :goto_0

    .line 1055456
    :cond_2
    iget-object v0, v1, LX/69E;->b:[B

    iget v3, v1, LX/69E;->c:I

    invoke-static {v0, v3}, LX/69C;->a([BI)LX/69C;

    move-result-object v0

    .line 1055457
    if-eqz v0, :cond_6

    .line 1055458
    sget-object v3, LX/68W;->f:LX/68h;

    if-nez v3, :cond_3

    .line 1055459
    invoke-static {v1}, LX/68V;->c(LX/69E;)V

    .line 1055460
    :cond_3
    iput-object v2, v1, LX/69E;->a:Ljava/lang/String;

    .line 1055461
    :cond_4
    :goto_1
    sget-object v3, LX/68W;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3, v1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1055462
    sget-object v3, LX/68W;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/69E;

    .line 1055463
    if-eqz v3, :cond_4

    .line 1055464
    invoke-static {v3}, LX/68V;->c(LX/69E;)V

    goto :goto_1

    .line 1055465
    :cond_5
    goto :goto_0

    .line 1055466
    :cond_6
    invoke-static {v1}, LX/68V;->c(LX/69E;)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1055436
    invoke-super {p0}, LX/68V;->b()V

    .line 1055437
    iget-object v0, p0, LX/68W;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1055438
    iget-object v0, p0, LX/68W;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1055439
    return-void
.end method
