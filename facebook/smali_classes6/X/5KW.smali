.class public LX/5KW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 898708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/java2js/JSValue;Z)LX/1dc;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/java2js/JSValue;",
            "Z)",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 898709
    move v0, v1

    .line 898710
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/java2js/JSValue;->getPropertyAtIndex(I)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    .line 898711
    invoke-virtual {v2}, Lcom/facebook/java2js/JSValue;->isUndefined()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 898712
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 898713
    :cond_0
    invoke-virtual {v2, v1}, Lcom/facebook/java2js/JSValue;->getPropertyAtIndex(I)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/java2js/JSValue;->asString()Ljava/lang/String;

    move-result-object v3

    .line 898714
    const-string v4, "selected"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-nez p1, :cond_2

    :cond_1
    const-string v4, "normal"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-nez p1, :cond_3

    .line 898715
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/facebook/java2js/JSValue;->getPropertyAtIndex(I)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/java2js/JSValue;->getJavaObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dc;

    return-object v0

    .line 898716
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
